#!/usr/bin/perl -w

use strict;
use Data::Dumper;
use Mioga2::Search::Finder;
use Mioga2::Search::Result;
use Mioga2::Search::Query;
use Mioga2::Search::Document;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::Search::Config;
use Getopt::Long;
use Error qw(:try);

my $debug = 0;
# ================================================================
# Get args
# ================================================================
my $search_tags = 0;
my $search_description = 0;
my $search_text = 0;
my $verbose = 0;
my $help = 0;
my $path_mioga_conf = "/var/lib/Mioga2/conf/Mioga.conf";
my $path_search_conf = "/var/lib/Mioga2/conf/search_conf.xml";

my $result = GetOptions ("help" => \$help,
			          "conf=s"  => \$path_mioga_conf,
			          "search_conf=s"  => \$path_search_conf,
			          "search_tags"  => \$search_tags,
			          "search_description"  => \$search_description,
			          "search_text"  => \$search_text,
			          "verbose"  => \$verbose);

print "Getopt result = $result\n" if ($debug);

if ( ($help) or (@ARGV < 3) ) {
	print "Usage : $0 [options] instance user_id 'query' [offset]\n";
	print "    Options :\n";
	print "        --conf='path to Mioga.conf' : default to /var/lib/Mioga2/conf/Mioga.conf\n";
	print "        --search_conf='path to search_conf.xml' : default to /var/lib/Mioga2/conf/search_conf.xml\n";
	print "        --search_tags    : tags search\n";
	print "        --search_description    : description search\n";
	print "        --search_text    : text search\n";
	print "        --verbose    : verbose mode\n";
	print "        --help       : print usage\n";
	exit(-1);
}

my $miogaconf = new Mioga2::MiogaConf($path_mioga_conf);
my $search_config = new Mioga2::Search::Config($path_search_conf);
my $instance = $ARGV[0];
my $user_id = $ARGV[1];
my $qstring = $ARGV[2];
my $config = Mioga2::Config->new($miogaconf, $instance);

my $offset = 0;
my $count = 10;
if (@ARGV > 3) {
	$offset = $ARGV[3];
}

# ================================================================
# Create Finder object and process query
# ================================================================
my $finder;
my $query;
my $mioga_files = $config->GetMiogaFilesDir();
my $applications = $search_config->GetApplications();


try {
	foreach my $app (keys(%$applications)) {
		print STDERR "  app = $app   package = $applications->{$app}->{package}\n" if ($debug > 1);
		my $mod = $applications->{$app}->{package};
		$mod =~ s/::/\//g;
		require $mod.".pm"; 
	}

	$finder = Mioga2::Search::Finder->new( { config => $config,
												base_dir => $mioga_files,
												search_config => $search_config, } );
	$finder->Process( { mode => 'standard', lang => 'french', qstring => $qstring, search_tags => $search_tags, search_text => $search_text, search_description => $search_description } );

	my $result = $finder->GetResult($user_id, $offset, $count);
	print "  " . $result->GetCount() . " result from offset $offset on total of : " . $result->GetTotalCount() . "\n";
	foreach my $res (@{$result->GetTerms()}) {
		print "term : $res\n";
		print "\n";
	}
	foreach my $res (@{$result->GetResults()}) {
		print STDERR Dumper($res) . "\n" if ($debug);
		print "percent : $res->{percent} group_id : $res->{group_id} url : $res->{url}\n";
		print "highlight : $res->{highlight}\n";
		print "\n";
	}
}
catch Mioga2::Exception::Simple with {
	my $err = shift;
	print STDERR "Exception::Simple error : ". $err->as_string."\n";
}
otherwise {
	my $err = shift;
	#print STDERR "Error : " . Dumper($err) . "\n";
	print STDERR "Other error : ". $err->stringify."\n";
};
