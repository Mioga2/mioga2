#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================

use strict;
use Data::Dumper;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Getopt::Long;
use Mioga2::Search::Config;
use Mioga2::tools::database;
use Mioga2::Search::Document::File;
use Mioga2::Search::Document::News;

use Error qw(:try);

my $debug = 0;

# ================================================================
# PrintInstanceList
# ================================================================
sub PrintInstanceList {
	my ($miogaconf) = @_;

	my $dbh = $miogaconf->GetDBH();
	my $sql = "select ident from m_mioga";
	my @instances;
	my $res = SelectMultiple($dbh, $sql);
	foreach my $r (@$res) {
		push @instances, $r->{ident};
	}
	print join(' ', @instances) . "\n";
}
# ================================================================
# PrintInstanceWithAppList
# ================================================================
sub PrintInstanceWithAppList {
	my ($miogaconf, $app) = @_;

	my $dbh = $miogaconf->GetDBH();
	my $sql = "SELECT m_mioga.ident FROM m_mioga, m_application, m_instance_application WHERE m_instance_application.mioga_id = m_mioga.rowid AND m_instance_application.application_id = m_application.rowid AND m_application.ident = " . $dbh->quote ($app);
	my @instances;
	my $res = SelectMultiple($dbh, $sql);
	foreach my $r (@$res) {
		push @instances, $r->{ident};
	}
	print join(' ', @instances) . "\n";
}
# ================================================================
# PrintThemesList
# ================================================================
sub PrintThemesList {
	my ($miogaconf, $instance) = @_;

	my $dbh = $miogaconf->GetDBH();
	my $sql = "select rowid from m_mioga where ident='$instance'";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		die "Cannot get instance <$instance>\n";
	}
	my @themes;
	$sql = "select ident from m_theme where mioga_id=$res->{rowid}";
	$res = SelectMultiple($dbh, $sql);
	foreach my $r (@$res) {
		push @themes, $r->{ident};
	}
	print join(' ', @themes) . "\n";
}
# ================================================================
# PrintDir
# ================================================================
sub PrintDir {
	my ($miogaconf, $instance, $dir) = @_;

	my $config = Mioga2::Config->new($miogaconf, $instance);
	if ($dir eq 'xsl_dir') {
		 print $config->GetXSLDir . "\n";
	}
	elsif ($dir eq 'theme_dir') {
		 print $config->GetThemesDir . "\n";
	}
}
# ================================================================
# Get args and conf
# ================================================================


my $verbose = 0;
my $help = 0;
my $instance = "";
my $path_mioga_conf = "/var/lib/Mioga2/conf/Mioga.conf";

my $result = GetOptions ("help" => \$help,
			          "conf=s"  => \$path_mioga_conf,
			          "instance=s"  => \$instance,
			          "verbose"  => \$verbose);

print "Getopt result = $result\n" if ($debug);

if ( ($help) or (@ARGV < 1) ) {
	print "Usage : $0 [options] command\n";
	print "    Options :\n";
	print "        --conf='path to Mioga.conf'    : default to /var/lib/Mioga2/conf/Mioga.conf\n";
	print "        --verbose                      : verbose mode\n";
	print "        --instance='name'              : name of instance for some commands\n";
	print "        --help                         : print usage\n";
	print "     Commands\n";
	print "        instances                      : return instance list\n";
	print "        instanceswithapp <app_ident>   : return instance list\n";
	print "        themes                         : return theme list for instance\n";
	print "        xsl_dir                        : return xsl directory\n";
	print "        theme_dir                      : return theme directory\n";
	exit(-1);
}

my $command = $ARGV[0];
my $arg = $ARGV[1];
# ================================================================
# Main prog
# ================================================================
my $miogaconf;
my $config;
try {
	$miogaconf = Mioga2::MiogaConf->new($path_mioga_conf);

	if ($command eq 'instances') {
		PrintInstanceList($miogaconf);
	}
	elsif ($command eq 'instanceswithapp') {
		if ($arg && $arg ne '') {
			PrintInstanceWithAppList($miogaconf, $arg);
		}
		else {
			print STDERR "instancewithapp command takes application ident as argument\n";
			exit (-1);
		}
	}
	elsif ($command eq 'themes') {
		PrintThemesList($miogaconf, $instance);
	}
	elsif ($command eq 'xsl_dir') {
		PrintDir($miogaconf, $instance, $command);
	}
	elsif ($command eq 'theme_dir') {
		PrintDir($miogaconf, $instance, $command);
	}
	else {
		print STDERR "Unknown comand : <$command>\n";
		exit(-1);
	}
}
catch Mioga2::Exception::DB with {
	my $err = shift;
	print STDERR "Exception::DB " . $err->as_string;
	exit(-1);
}
catch Mioga2::Exception::Simple with {
	my $err = shift;
	print STDERR "Exception::Simple error : $err\n";
	exit(-1);
}
otherwise {
	my $err = shift;
	print STDERR "Error : " . Dumper($err) . "\n";
	exit(-1);
};
