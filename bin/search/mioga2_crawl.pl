#!/usr/bin/perl -w -I.

use strict;
use Data::Dumper;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Getopt::Long;
use Mioga2::tools::database;
use Mioga2::Classes::Time;
use Mioga2::Search::Config;
use Error qw(:try);

my $debug = 0;
# ================================================================
# Get args and conf
# ================================================================

my $force = 0;
my $verbose = 0;
my $force_all = 0;
my $instance = '';
my $help = 0;
my $path_mioga_conf = "/var/lib/Mioga2/conf/Mioga.conf";
my $path_search_conf = "/var/lib/Mioga2/conf/search_conf.xml";

my $result = GetOptions ("force=s" => \$force,
			          "force_all"  => \$force_all,
					  "instance=s" => \$instance,
			          "conf=s"  => \$path_mioga_conf,
			          "search_conf=s"  => \$path_search_conf,
			          "help"  => \$help,
			          "verbose"  => \$verbose);

print "Getopt result = $result\n" if ($debug);

if ($help) {
	print "Usage : $0 [options]\n";
	print "    Options :\n";
	print "        --force=instance : mark all documents to be indexed\n";
	print "        --force_all      : force all instances\n";
	print "        --instance=name  : only process specified instance\n";
	print "        --conf='path to Mioga.conf' : default to /var/lib/Mioga2/conf/Mioga.conf\n";
	print "        --search_conf='path to search_conf.xml' : default to /var/lib/Mioga2/conf/search_conf.xml\n";
	print "        --verbose    : verbose mode\n";
	print "        --help       : print usage\n";
	exit(0);
}

my $miogaconf = new Mioga2::MiogaConf($path_mioga_conf);
my $searchconf = new Mioga2::Search::Config($path_search_conf);

# ================================================================
# Process instance to get datas to index :
#  - verify if new datas exists
#  - set flag to delete old ones
# ================================================================

my $dbh = $miogaconf->GetDBH();
my $applications = $searchconf->GetApplications();
my $res;
my $sql;
try {
	#push @INC, "/mnt/home/gpolart/projets/Mioga/Search/lib";
	foreach my $app (keys(%$applications)) {
		print STDERR "  app = $app   package = $applications->{$app}->{package}\n" if ($debug);
		#eval "require $applications->{$app}->{package}"; 
		#print STDERR "eval error : $@\n";
		my $mod = $applications->{$app}->{package};
		$mod =~ s/::/\//g;
		require $mod.".pm"; 
	}
	# mark deleted documents (one action for all instances)
	print " Mark deleted documents ...\n" if ($verbose or $debug);
	foreach my $app (keys(%$applications)) {
		do {
			$res = $applications->{$app}->{package}->GetDeletedDocuments($applications->{$app}->{app_code}, $dbh);
			print STDERR "  got " . scalar(@$res). " results\n" if ($debug);
			if (scalar(@$res) > 0) {
				$sql = "UPDATE search_doc set modified=now(), fl_deleted='t' WHERE rowid in (" . join(',', map {$_->{rowid}} @$res) . ")";
				printf STDERR "sql = $sql\n" if ($debug);
				ExecSQL($dbh, $sql);
			}
		} while (scalar(@$res) > 0);
	}
	# get instance list
	print " Get instance list...\n" if ($verbose or $debug);
	my $sql = "select * from m_mioga";
	my $instances = SelectMultiple($dbh, $sql);
	if (!defined($instances)) {
		print STDERR "Database error\n";
		exit(-1);
	}
	# loop on instances
	foreach my $entry (@$instances) {
		next if (($instance ne '') && ($entry->{ident} ne $instance));
		printf STDERR " process instance : $entry->{rowid}\n" if ($verbose or $debug);
		my $config = Mioga2::Config->new($miogaconf, $entry->{ident});
		my $mioga_id = $config->GetMiogaId();
		foreach my $app (keys(%$applications)) {
			print STDERR "  process app : $app\n" if ($verbose or $debug);
			# get new document
			do {
				$res = $applications->{$app}->{package}->GetNewDocuments($applications->{$app}->{app_code}, $config);
				print STDERR "  got " . scalar(@$res). " results\n" if ($debug);
				foreach my $entry (@$res) {
					ExecSQL($dbh, "INSERT INTO search_doc (created, modified, app_code, app_id, mioga_id)"
										." VALUES (now(), now(), $applications->{$app}->{app_code}, $entry->{app_id}, $mioga_id)");
				}
			} while (scalar(@$res) > 0);
			# verify modification for documents
			my $force_flag = 0;
			if ( ($force_all) or ($force eq $entry->{ident}) ) {
				$force_flag = 1;
			}
			$applications->{$app}->{package}->SetModifiedDocuments($applications->{$app}->{app_code}, $config, $force_flag);
		}
	}
}
catch Mioga2::Exception::Simple with {
	my $err = shift;
	print STDERR "Exception Simple = " . $err->as_string();
	exit(-1);
}
catch Mioga2::Exception::DB with {
	my $err = shift;
	print STDERR "Exception::DB " . $err->as_string;
	exit(-1);
}
otherwise {
	my $err = shift;
	print STDERR "Other exception " . Dumper($err) . "\n";
	exit(-1);
};


