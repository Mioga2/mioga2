#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#

use strict;
use Data::Dumper;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Getopt::Long;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use File::MimeInfo::Magic;
use Mioga2::Classes::Time;
use Error qw(:try);

my $debug = 0;
my $verbose = 0;
my $help = 0;
my $all = 0;
my $path_mioga_conf = "/var/lib/Mioga2/conf/Mioga.conf";
my $instance;

# ================================================================
# ProcessInstance
# ================================================================
sub ProcessInstance
{
	my ($config) = @_;
	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	my $mioga_files = $config->GetMiogaFilesDir();
	my $base_uri = $config->GetBaseURI();
	my $base_path = $config->GetWebdavDir();
	print STDERR "base URI = $base_uri   base_path = $base_path\n";


	my $offset = 0;
	my $limit = 1000;
	my $count;

	my $sql;
	my $res;
	$sql = "SELECT count(m_uri.*) FROM m_uri, m_group_base WHERE m_uri.group_id=m_group_base.rowid AND m_group_base.mioga_id=$mioga_id";
	$res = SelectSingle($dbh, $sql);
	$count = $res->{count};
	my $mime;
	my $path;
	do {
		print "offset = $offset   count = $count\n" if ($verbose or $debug);
		$sql = "SELECT m_uri.* FROM m_uri,m_group_base WHERE m_uri.group_id=m_group_base.rowid AND m_group_base.mioga_id=$mioga_id order by m_uri.rowid offset $offset limit $limit";
		$res = SelectMultiple($dbh, $sql);
		print "res count = ".scalar(@$res)." \n" if ($verbose or $debug);
		if (scalar(@$res) > 0) {
			foreach my $entry (@$res) {
				$path = $entry->{uri};
				$path =~ s/^$base_uri/$base_path/;

				my @stats = stat($path);
				my $modified = gmtime($stats[9]);
				print STDERR " modified = ".$modified->datetime."\n" if($debug);

				eval { $mime = mimetype($path); };
				if ($@) {
					print STDERR "path = $path error = $@\n";
					$mime = "unknown";
				}
				else {
					$mime =~ s/([^;]*);(.*)/$1/;
					print STDERR "1 = $1 error = $2\n" if(defined($2));
				}
				utf8::encode($mime);
				print "path = $path  mime = $mime modfied = $modified\n" if ($verbose or $debug);
				if ($mime ne "inode/directory") {
					ExecSQL($dbh, "UPDATE m_uri set mimetype='". st_FormatPostgreSQLString($mime) ."',modified='".st_FormatPostgreSQLString($modified->datetime) ."' where rowid=$entry->{rowid}");
				}
			}
		}
		$offset += $limit;
	} while (scalar(@$res) > 0);
	#} while ($offset < $count);
}
# ================================================================
# Main program
# ================================================================
# Get args and conf
# 

my $result = GetOptions ("help" => \$help,
			          "conf=s"  => \$path_mioga_conf,
			          "all"  => \$all,
			          "verbose"  => \$verbose);

print "Getopt result = $result\n" if ($debug);

if ( ($help) or (!$all and @ARGV < 1) ) {
	print "Usage : $0 [options] [instance]\n";
	print "    Options :\n";
	print "        --conf='path to Mioga.conf' : default to /var/lib/Mioga2/conf/Mioga.conf\n";
	print "        --verbose    : verbose mode\n";
	print "        --all        : process all instances\n";
	print "        --help       : print usage\n";
	exit(-1);
}

if (!$all) {
	$instance = $ARGV[0];
}
#
# Create indexer
#
my $miogaconf;
my $search_config;
try {
	$miogaconf = Mioga2::MiogaConf->new($path_mioga_conf);

	my $dbh = $miogaconf->GetDBH();
	#
	# Get instances list
	#
	if ($all) {
		my $sql = "select * from m_mioga";
		my $res = SelectMultiple($dbh, $sql);
		if (!defined($res)) {
			print STDERR "Database error\n";
			exit(-1);
		}
		foreach my $entry (@$res) {
			my $config = Mioga2::Config->new($miogaconf, $entry->{ident});
			ProcessInstance($config);
		}
	}
	else {
		my $sql = "select * from m_mioga where ident='$instance'";
		my $res = SelectSingle($dbh, $sql);
		if (!defined($res)) {
			print STDERR "No such instance $instance\n";
			exit(-1);
		}
		my $config = Mioga2::Config->new($miogaconf, $res->{ident});
		ProcessInstance($config);
	}
}
catch Mioga2::Exception::DB with {
	my $err = shift;
	print STDERR "Exception::DB " . $err->as_string;
	exit(-1);
}
catch Mioga2::Exception::Simple with {
	my $err = shift;
	print STDERR "Exception::Simple error : $err\n";
	exit(-1);
}
otherwise {
	my $err = shift;
	print STDERR "Error : " . Dumper($err) . "\n";
	exit(-1);
};
