#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#

use strict;
use Data::Dumper;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Getopt::Long;
use Mioga2::Search::Config;
use Mioga2::Search::Indexer;
use Mioga2::tools::database;
use Error qw(:try);

my $debug = 0;
my $verbose = 0;

# ================================================================
# GetIdsToDelete
# ================================================================
sub GetIdsToDelete
{
	my ($dbh, $mioga_id) = @_;
	my $sql = "SELECT search_doc.xapian_docid FROM search_doc WHERE fl_deleted='t'"
							." AND mioga_id=$mioga_id limit 1000";
	my $res = SelectMultiple($dbh, $sql);

	return $res;
}
# ================================================================
# ProcessInstance
# ================================================================
sub ProcessInstance
{
	my ($config, $search_config) = @_;
	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	my $mioga_files = $config->GetMiogaFilesDir();

	my $indexer = Mioga2::Search::Indexer->new( { base_dir => $mioga_files, search_config => $search_config, } );
	#
	# Delete old records from xapian DB and then from search_doc table
	#
	my $res;
	my $sql;
	$sql = "SELECT xapian_docid FROM search_doc WHERE fl_deleted='t' AND xapian_docid is not null"
							." AND mioga_id=$mioga_id limit 200";
	print "Remove doc in Xapian and search_doc\n" if ($verbose or $debug);
	do {
		$res = SelectMultiple($dbh, $sql);
		if (scalar(@$res) > 0) {
			foreach my $entry (@$res) {
				print "docid $entry->{xapian_docid}\n" if ($verbose or $debug);
				$indexer->RemoveDoc($entry->{xapian_docid});
			}
			#TODO purge cache
			ExecSQL($dbh, "DELETE FROM search_doc WHERE xapian_docid in (" . join(',', map {$_->{xapian_docid}} @$res) . ")");
		}
	} while (scalar(@$res) > 0);
	#
	# Delete old records from xapian DB and then from search_doc table
	#
	print "Remove not indexed doc in search_doc\n" if ($verbose or $debug);
	ExecSQL($dbh, "DELETE FROM search_doc WHERE fl_deleted='t' AND xapian_docid is NULL");
	#
	# Add or modify documents
	#
	print "Add or Modify documents in Xapian\n" if ($verbose or $debug);
	my $applications = $search_config->GetApplications();
	foreach my $app (keys(%$applications)) {
		print "Application : $app .....\n" if ($verbose or $debug);
		print STDERR "  package = $applications->{$app}->{package}\n" if ($debug);

		my $select = "SELECT search_doc.* FROM search_doc WHERE fl_error='f' AND app_code=$applications->{$app}->{app_code}"
						." AND (indexed IS NULL OR modified > indexed)"
							." AND mioga_id=$mioga_id limit 200";
		do {
			$res = SelectMultiple($dbh, $select);
			print STDERR " Must process : " . scalar(@$res) . " documents\n", if ($debug);

			foreach my $d (@$res) {
				my $document;
				$document = $applications->{$app}->{package}->new( { search_config => $search_config,
				                                                     base_dir => $mioga_files,
				                                                     create => { config => $config,
																	             search_doc => $d,
																			   },
																	} );
				print $document->as_string . "\n" if ($verbose);
				if ($document->GetError() == 0) {
					$indexer->Process($document);
					my $docid = $document->GetXapianDocid;
					$sql = "UPDATE search_doc set modified=now(), indexed = now(), xapian_docid=$docid";
				}
				else {
					$sql = "UPDATE search_doc set fl_error = 't'";
				}
				$sql .= " WHERE rowid=$d->{rowid}";
				print STDERR " sql = $sql\n" if ($debug);
				ExecSQL($dbh, $sql);
			}
			$indexer->Flush();
		} while (scalar(@$res));
	}
}
# ================================================================
# Main program
# ================================================================
# Get args and conf
# 
my $help = 0;
my $all = 0;
my $path_mioga_conf = "/var/lib/Mioga2/conf/Mioga.conf";
my $path_search_conf = "/var/lib/Mioga2/conf/search_conf.xml";
my $instance;

my $result = GetOptions ("help" => \$help,
			          "conf=s"  => \$path_mioga_conf,
			          "search_conf=s"  => \$path_search_conf,
			          "all"  => \$all,
			          "verbose"  => \$verbose);

print "Getopt result = $result\n" if ($debug);

if ( ($help) or (!$all and @ARGV < 1) ) {
	print "Usage : $0 [options] [instance]\n";
	print "    Options :\n";
	print "        --conf='path to Mioga.conf' : default to /var/lib/Mioga2/conf/Mioga.conf\n";
	print "        --search_conf='path to search_conf.xml' : default to /var/lib/Mioga2/conf/search_conf.xml\n";
	print "        --verbose    : verbose mode\n";
	print "        --all        : process all instances\n";
	print "        --help       : print usage\n";
	exit(-1);
}

if (!$all) {
	$instance = $ARGV[0];
}
#
# Create indexer
#
my $miogaconf;
my $search_config;
try {
	$miogaconf = Mioga2::MiogaConf->new($path_mioga_conf);
	$search_config = Mioga2::Search::Config->new($path_search_conf);

	my $applications = $search_config->GetApplications();
	foreach my $app (keys(%$applications)) {
		print STDERR "  app = $app   package = $applications->{$app}->{package}\n" if ($debug);
		my $mod = $applications->{$app}->{package};
		$mod =~ s/::/\//g;
		require $mod.".pm"; 
	}
	my $dbh = $miogaconf->GetDBH();
	#
	# Get instances list
	#
	if ($all) {
		my $sql = "select * from m_mioga";
		my $res = SelectMultiple($dbh, $sql);
		if (!defined($res)) {
			print STDERR "Database error\n";
			exit(-1);
		}
		foreach my $entry (@$res) {
			my $config = Mioga2::Config->new($miogaconf, $entry->{ident});
			ProcessInstance($config, $search_config);
		}
	}
	else {
		my $sql = "select * from m_mioga where ident='$instance'";
		my $res = SelectSingle($dbh, $sql);
		if (!defined($res)) {
			print STDERR "No such instance $instance\n";
			exit(-1);
		}
		my $config = Mioga2::Config->new($miogaconf, $res->{ident});
		ProcessInstance($config, $search_config);
	}
}
catch Mioga2::Exception::DB with {
	my $err = shift;
	print STDERR "Exception::DB " . $err->as_string;
	exit(-1);
}
catch Mioga2::Exception::Simple with {
	my $err = shift;
	print STDERR "Exception::Simple error : $err\n";
	exit(-1);
}
otherwise {
	my $err = shift;
	print STDERR "Error : " . Dumper($err) . "\n";
	exit(-1);
};
