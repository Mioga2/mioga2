#!/usr/bin/perl -w -I.

use strict;
use Data::Dumper;
use Search::Xapian;
use Getopt::Long;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::Search::Config;

my $debug = 1;
# ================================================================
# Get args
# ================================================================
my $verbose = 0;
my $help = 0;
my $path_mioga_conf = "/var/lib/Mioga2/conf/Mioga.conf";
my $path_search_conf = "/var/lib/Mioga2/conf/search_conf.xml";

my $result = GetOptions ("help" => \$help,
			          "conf=s"  => \$path_mioga_conf,
			          "search_conf=s"  => \$path_search_conf,
			          "verbose"  => \$verbose);

print "Getopt result = $result\n" if ($debug);

if ( ($help) or (@ARGV < 3) ) {
	print "Usage : $0 [options] instance doc_id term\n";
	print "    Options :\n";
	print "        --conf='path to Mioga.conf' : default to /var/lib/Mioga2/conf/Mioga.conf\n";
	print "        --search_conf='path to search_conf.xml' : default to /var/lib/Mioga2/conf/search_conf.xml\n";
	print "        --verbose    : verbose mode\n";
	print "        --help       : print usage\n";
	exit(-1);
}
my $miogaconf = new Mioga2::MiogaConf($path_mioga_conf);
my $search_config = new Mioga2::Search::Config($path_search_conf);
my $instance = $ARGV[0];
my $docid = $ARGV[1];
my $term = $ARGV[2];
my $config = Mioga2::Config->new($miogaconf, $instance);

my $mioga_files = $config->GetMiogaFilesDir();
my $xapian_path = $mioga_files . "/" . $search_config->GetXapianDBDir();


# ================================================================
# Connect to xapianDB and initialize stemmer
# ================================================================
my $xapiandb = Search::Xapian::WritableDatabase->new($xapian_path,  Search::Xapian::DB_OPEN );
print "DB description : " . $xapiandb->get_description() . "\n";
print "doc count: " . $xapiandb->get_doccount() . "\n";

my $doc = $xapiandb->get_document($docid);
print "doc description : " . $doc->get_description() . "\n";
print "doc term count : " . $doc->termlist_count() . "\n";

my $term_iter = $doc->termlist_begin();
my $i = 1;
while ($term_iter != $doc->termlist_end()) {
	print "$i : term = ".$term_iter->get_termname()." wdf = ". $term_iter->get_wdf()."\n";
	#print $term_iter->get_termfreq() . "\n";
	#print $term_iter->positionlist_count() . "\n";
	my $pos_iter = $term_iter->positionlist_begin();
	while ($pos_iter != $term_iter->positionlist_end()) {
		print "   pos = ".$pos_iter->get_termpos()."\n";
		$pos_iter->inc();
	}
	$term_iter->inc();
}

print "Search pos for <$term> ...\n";

my $pl_iter = $xapiandb->positionlist_begin($docid, $term);
while ($pl_iter != $xapiandb->positionlist_end($docid, $term)) {
		print "   pos = ".$pl_iter->get_termpos()."\n";
		$pl_iter->inc();
}

print "Search posting for <$term> ...\n";

$pl_iter = $xapiandb->postlist_begin($term);
while ($pl_iter != $xapiandb->postlist_end($term)) {
		print "   docid = ".$pl_iter->get_docid()."\n";
		$pl_iter->inc();
}
