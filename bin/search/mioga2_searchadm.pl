#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#

use strict;
use Data::Dumper;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Getopt::Long;
use Mioga2::Search::Config;
use Mioga2::tools::database;
use Mioga2::Search::Document::File;
use Mioga2::Search::Document::News;

use Error qw(:try);

my $debug = 1;

# ================================================================
# Initall
# ================================================================
sub Initall {
	my ($config, $search_config) = @_;

	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	my $miogafiles = $config->GetMiogaFilesDir();

	my $sql = "delete from search_doc where mioga_id = $mioga_id";
	ExecSQL($dbh, $sql);
	$sql = "delete from search_file where mioga_id = $mioga_id";
	ExecSQL($dbh, $sql);

	my $cmd = "rm -rf $miogafiles/" . $search_config->GetCacheDir() . "/* $miogafiles/" . $search_config->GetXapianDBDir() . "/*";
	print STDERR "cmd = $cmd\n" if ($debug);
	system($cmd);
}
# ================================================================
# ListError
# ================================================================
sub ListError {
	my ($config, $search_config) = @_;

	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	my $miogafiles = $config->GetMiogaFilesDir();

	my $applications = $search_config->GetApplications;

	foreach my $app (keys(%$applications)) {
		print STDERR "  app = $app   package = $applications->{$app}->{package}\n" if ($debug);

		my $select = "SELECT search_doc.* FROM search_doc WHERE fl_error='t' AND app_code=$applications->{$app}->{app_code}"
							." AND mioga_id=$mioga_id";
		my $res = SelectMultiple($dbh, $select);
		print STDERR " Must process : " . scalar(@$res) . " documents\n", if ($debug);

		foreach my $d (@$res) {
			my $document;
			$document = $applications->{$app}->{package}->new( { search_config => $search_config,
			                                                     base_dir => $miogafiles,
			                                                     create => { config => $config,
																             search_doc => $d,
																		   },
																} );
		}
	}
}
# ================================================================
# Get args and conf
# ================================================================


my $verbose = 0;
my $help = 0;
my $path_mioga_conf = "/var/lib/Mioga2/conf/Mioga.conf";
my $path_search_conf = "/var/lib/Mioga2/conf/search_conf.xml";

my $result = GetOptions ("help" => \$help,
			          "conf=s"  => \$path_mioga_conf,
			          "search_conf=s"  => \$path_search_conf,
			          "verbose"  => \$verbose);

print "Getopt result = $result\n" if ($debug);

if ( ($help) or (@ARGV < 2) ) {
	print "Usage : $0 [options] instance command\n";
	print "    Options :\n";
	print "        --conf='path to Mioga.conf' : default to /var/lib/Mioga2/conf/Mioga.conf\n";
	print "        --search_conf='path to search_conf.xml' : default to /var/lib/Mioga2/conf/search_conf.xml\n";
	print "        --verbose    : verbose mode\n";
	print "        --help       : print usage\n";
	exit(-1);
}

my $instance = $ARGV[0];
my $command = $ARGV[1];
# ================================================================
# Create indexer
# ================================================================
my $miogaconf;
my $config;
my $search_config;
try {
	$miogaconf = Mioga2::MiogaConf->new($path_mioga_conf);
	$search_config = Mioga2::Search::Config->new($path_search_conf);
	my $config = Mioga2::Config->new($miogaconf, $instance);

	if ($command eq 'initall') {
		Initall($config, $search_config);
	}
	elsif ($command eq 'listerror') {
		ListError($config, $search_config);
	}
	else {
		print STDERR "Unknown comand : <$command>\n";
	}
}
catch Mioga2::Exception::DB with {
	my $err = shift;
	print STDERR "Exception::DB " . $err->as_string;
	exit(-1);
}
catch Mioga2::Exception::Simple with {
	my $err = shift;
	print STDERR "Exception::Simple error : $err\n";
	exit(-1);
}
otherwise {
	my $err = shift;
	print STDERR "Error : " . Dumper($err) . "\n";
	exit(-1);
};
