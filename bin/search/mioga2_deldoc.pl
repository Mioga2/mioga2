#!/usr/bin/perl -w -I.

use strict;
use Data::Dumper;
use Search::Xapian;

my $debug = 1;
# ================================================================
# Get args
# ================================================================
if(@ARGV < 2) {
	print "Usage : $0 xapiandb_path docid\n";
	exit(-1);
}
print " database path = $ARGV[0]\n";
# ================================================================
# Connect to xapianDB and initialize stemmer
# ================================================================
my $xapiandb = Search::Xapian::WritableDatabase->new($ARGV[0],  Search::Xapian::DB_OPEN );
print "DB description : " . $xapiandb->get_description() . "\n";
print "doc count: " . $xapiandb->get_doccount() . "\n";

# ================================================================
# Delete given docid
# ================================================================

$xapiandb->delete_document($ARGV[1]);
