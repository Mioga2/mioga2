#!/usr/bin/perl -w -I.

use strict;
use Data::Dumper;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::Search::Config;
use Search::Xapian;
use Mioga2::tools::database;

use Error qw(:try);

my $debug = 0;
# ================================================================
# Main program
# ================================================================
# Get args and conf
# 
if(@ARGV < 3) {
	print "Usage : $0 /path/to/Mioga.conf /path/to/search_conf.xml instance\n";
	exit(-1);
}
my $miogaconf;
my $search_config;
my $config;
try {
	$miogaconf = Mioga2::MiogaConf->new($ARGV[0]);
	$search_config = Mioga2::Search::Config->new($ARGV[1]);
	$config = Mioga2::Config->new($miogaconf, $ARGV[2]);
 ;
}
catch Mioga2::Exception::DB with {
	my $err = shift;
	print STDERR "Exception::DB " . $err->as_string;
	exit(-1);
}
catch Mioga2::Exception::Simple with {
	my $err = shift;
	print STDERR "Exception::Simple error : $err\n";
	exit(-1);
}
otherwise {
	my $err = shift;
	print STDERR "Error : " . Dumper($err) . "\n";
	exit(-1);
};
my $mioga_id = $config->GetMiogaId();
my $mioga_files = $config->GetMiogaFilesDir();
# ================================================================
# Connect to xapianDB
# ================================================================
my $path = $mioga_files . "/" . $search_config->GetXapianDBDir();
my $xapiandb = Search::Xapian::WritableDatabase->new($path, Search::Xapian::DB_OPEN);
print "DB description : " . $xapiandb->get_description() . "\n";
print "doc count: " . $xapiandb->get_doccount() . "\n";
print "average lenth of docs : " . $xapiandb->get_avlength() . "\n";
my $last_docid = $xapiandb->get_lastdocid();
print "last docid : $last_docid\n";

# ================================================================
# Process doc list
# ================================================================
my $docid = 1;
my $doc;
my $data;
print "Process each record of Xapian DB ...\n";
while ($docid <= $last_docid) {
	try {
		print STDERR "docid = $docid\n" if ($debug);
		eval { $doc = $xapiandb->get_document($docid); };
		if (! $@) {
			$data = $doc->get_data();
			print STDERR "data = $data\n" if ($debug);
			my @lines = split('\n', $data);
			my $app_code;
			my $app_id;
			foreach my $l (@lines) {
				if ($l =~ /app_code:([0-9]*)/) {
					$app_code = $1;
				}
				if ($l =~ /app_id:([0-9]*)/) {
					$app_id = $1;
				}
			}
			if (!defined($app_code) or !defined($app_id)) {
				print STDERR "No app_code or app_id for data = $data\n";
			}
			else {
				my $sql = "select xapian_docid from search_doc where app_code=$app_code and app_id=$app_id and mioga_id=$mioga_id";
				my $res = SelectSingle($config->GetDBH, $sql);
				if (!defined($res)) {
					print "No xapian_docid for doc app_code = $app_code  app_id= $app_id mioga_id = $mioga_id\n";
					$xapiandb->delete_document($docid);
				}
				else {
					if ($docid != $res->{xapian_docid}) {
						print "xapian_docid($res->{xapian_docid}) != docid ($docid) for app_code = $app_code  app_id= $app_id mioga_id = $mioga_id\n";
						$xapiandb->delete_document($docid);
					}
				}
			}
		}
	};
	$docid++;
}
print "Process each record of MiogaDB search_doc ...\n";

my $sql = "SELECT rowid, xapian_docid from search_doc";
my $sth = $config->GetDBH->prepare($sql);
$sth->execute();
my @row;
while(@row = $sth->fetchrow_array ) {
	if (defined($row[2])) {
		$docid = $row[2];
		eval { $doc = $xapiandb->get_document($docid); };
		if ($@) {
			print "xapian_docid($docid) doesn't exists for rowid <$row[0]\n";
		}
	}
}
