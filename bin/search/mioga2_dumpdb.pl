#!/usr/bin/perl -w -I.

use strict;
use Data::Dumper;
use Search::Xapian;
use Error qw(:try);

my $debug = 1;
# ================================================================
# Get args
# ================================================================
if(@ARGV < 1) {
	print "Usage : $0 xapiandb_path\n";
	exit(-1);
}
print " database path = $ARGV[0]\n";
# ================================================================
# Connect to xapianDB and initialize stemmer
# ================================================================
my $xapiandb = Search::Xapian::Database->new($ARGV[0]);
print "DB description : " . $xapiandb->get_description() . "\n";
print "doc count: " . $xapiandb->get_doccount() . "\n";
print "average lenth of docs : " . $xapiandb->get_avlength() . "\n";
my $last_docid = $xapiandb->get_lastdocid();
print "last docid : $last_docid\n";

# ================================================================
# Process term list
# ================================================================

print "Term list\n";
my $iterm = $xapiandb->allterms_begin();
my $term_count = 0;
my $t;
while ($iterm ne $xapiandb->allterms_end()) {
	$t = $iterm->get_termname();
	use Devel::Peek;
	if ($t =~/^Eapr/) {
		print Dump($t);
		utf8::decode($t);
		print Dump($t);
	}
	else {
		utf8::decode($t);
	}
	print " " . $t. "\n";
	$iterm++;
	$term_count++;
}
print "term count: $term_count\n";

=head2

# ================================================================
# Process spelling list
# ================================================================
print "Spelling list\n";

$iterm = $xapiandb->spellings_begin();
$term_count = 0;
while ($iterm ne $xapiandb->spellings_end()) {
	$t = $iterm->get_termname();
	use Devel::Peek;
	if ($t =~/^Eapr/) {
		print Dump($t);
		utf8::decode($t);
		print Dump($t);
	}
	else {
		utf8::decode($t);
	}
	print " " . $t. "\n";
	$iterm++;
	$term_count++;
}
print "term count: $term_count\n";

=cut

# ================================================================
# Process doc list
# ================================================================

my $docid = 1;
my $doc;
my $data;
while ($docid <= $last_docid) {
	try {
		eval { $doc = $xapiandb->get_document($docid); };
		if (!$@) {
			print "docid = $docid\n";
			$data = $doc->get_data();
			$data =~ s/\n/\n\t/g;
			print "\t$data\n";
		}
	};
	$docid++;
}
