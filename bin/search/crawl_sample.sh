#!/bin/sh

date=`date +"%Y_%m_%d"`
log_file="/var/tmp/crawl_$date"
#
# Default path to configuration scripts
#
config=/var/lib/Mioga2/conf/Mioga.conf
search_conf=/var/lib/Mioga2/conf/search_conf.xml

# Process optional argument (instance name)
if [ -z $1 ]; then
	instances=`/usr/local/bin/mioga2_info.pl --conf=$config instances`
	mioga2_crawl_arg=""
else
	instances=$1
	mioga2_crawl_arg="--instance=$instances"
fi

begin=`date`
echo "===Start  : $begin" >> $log_file
echo "================ CRAWL ======================" >> $log_file
echo "====================================================================" >> $log_file
echo " Working on the following instance: $instances" >> $log_file
echo "" >> $log_file
#
# launch crawl process for all instances
# The crawl process look at new documents and insert them into search_doc table in mioga database
# It tracks the modified and deleted docs too.
/usr/local/bin/mioga2_crawl.pl --conf=$config --search_conf=$search_conf --verbose $mioga2_crawl_arg >> $log_file  2>&1
#
#
# the next loop process all instances on the server
# (mioga2_info gives list of instances)
#
echo "Instances: $instances"

for inst in $instances
do
        echo "INSTANCE => $inst" >> $log_file
        echo "-------------------------------------" >> $log_file
        echo "   INDEX" >> $log_file
        echo "-------------------------------------" >> $log_file
        # mioga2_index look at search_doc table to get documents to parse and index
		# It adds or modifies documents in Xapian database
        /usr/local/bin/mioga2_index.pl --conf=$config --search_conf=$search_conf $inst >> $log_file  2>&1
        echo "====================================================================" >> $log_file
        echo "" >> $log_file
done

