#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  export.pl
#
#        USAGE:  ./export.pl /path/to/Mioga.conf [instances]
#
#  DESCRIPTION:  Export Mioga local users to CSV file
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  Sébastien NOBILI <technique@alixen.fr>
#      COMPANY:  Alixen (http://www.alixen.fr)
#      CREATED:  14/10/2009 14:10
#
#===============================================================================
#
#  Copyright (c) year 2009, Alixen (http://www.alixen.fr)
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================

use strict;
use warnings;

use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::tools::database;
use Data::Dumper;

use vars qw/$dbh $outfile/;

my $debug = 0;


#===============================================================================

=head2 WriteToFile

Write an array of arrays to a CSV file

=cut

#===============================================================================
sub WriteToFile {
	my (@content) = @_;

	print "WriteToFile $outfile\n" if ($debug);

	open (FILE, ">$outfile");

	for (@content) {
		my $line = '';
		for (@$_) {
			$line .= "$_,";
		}
		$line =~ s/,$//;
		print FILE "$line\n";
	}

	close (FILE);

	return ();
}	# ----------  end of subroutine WriteToFile  ----------


#===============================================================================

=head2 ExtractUsers

Extract users and store their information to an array of arrays

=cut

#===============================================================================
sub ExtractUsers {
	my ($instance) = @_;

	print "ExtractUsers\n" if ($debug);

	# Initialize SQL query
	my $sql = "SELECT rowid, lastname, firstname, email, ident FROM m_user_base WHERE type_id = (SELECT rowid FROM m_group_type WHERE ident = 'local_user')";
	$sql .= " AND mioga_id = (SELECT rowid FROM m_mioga WHERE ident = '$instance') AND rowid != (SELECT admin_id FROM m_mioga WHERE ident = '$instance')" if ($instance);
	print "SQL: $sql\n" if ($debug);
	
	# Query users
	my $users = SelectMultiple ($dbh, $sql);
	print Dumper $users if ($debug);

	my @list = ();

    foreach my $user (@$users) {
		my @params = ();
		for (qw/email firstname lastname ident rowid/) {
			push (@params, $user->{$_});
		}
		push (@list, \@params);
	}

	return (@list);
}	# ----------  end of subroutine ExtractUsers  ----------


#===============================================================================

=head2 GetInstances

Get instances from DB

=cut

#===============================================================================
sub GetInstances {
	my $miogaconf = shift;
	
	my $dbh = $miogaconf->GetDBH();

	return SelectMultiple($dbh, "SELECT * FROM m_mioga");
}	# ----------  end of subroutine GetInstances ----------


############## Main script ##############

if(@ARGV < 1) {
	print "Usage : $0 /path/to/Mioga.conf [instances]\n";
	exit(-1);
}

my $miogaconf = new Mioga2::MiogaConf (shift);
$dbh = $miogaconf->GetDBH ();

# Initialize list of instances to process
my @instances;
my $instances = GetInstances($miogaconf);
my $dynamic = 0;
while (my $inst = shift) {
	$dynamic = 1;
	if (SelectSingle ($dbh, "SELECT * FROM m_mioga WHERE ident='$inst'")) {
		push (@instances, $inst)
	}
	else {
		warn "Unknown instance $inst\n";
	}
}
if (!$dynamic) {
	for (@$instances) {
		push (@instances, $_->{ident});
	}
}

# Export each instance individually
for (@instances) {
	$outfile = "$_.csv";
	
	my @list = &ExtractUsers ($_);
	print Dumper \@list if ($debug);

	&WriteToFile (@list);
}
