#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  populate.pl
#
#        USAGE:  ./populate.pl /path/to/Mioga.conf /path/to/file.csv
#
#  DESCRIPTION:  Populate a LDAP directory from a CSV extract of user accounts
#
#      OPTIONS:  ---
# REQUIREMENTS:  File name should be <instance_name>.csv
#        NOTES:  ---
#       AUTHOR:  Sébastien NOBILI <technique@alixen.fr>
#      COMPANY:  Alixen (http://www.alixen.fr)
#      CREATED:  14/10/2009 15:52
#
#===============================================================================
#
#  Copyright (c) year 2009, Alixen (http://www.alixen.fr)
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================

use strict;
use warnings;

use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::tools::database;
use Mioga2::LDAP;
use File::Basename;
use File::Copy;
use Data::Dumper;

use vars qw/$dbh $csvfile/;

my $debug = 1;


#===============================================================================

=head2 GetInstance

Get instance from DB according to its ident

=cut

#===============================================================================
sub GetInstance {
	my ($ident) = @_;
	
	return SelectSingle ($dbh, "SELECT * FROM m_mioga WHERE ident='$ident'");
}	# ----------  end of subroutine GetInstances ----------



#-------------------------------------------------------------------------------
# Main script
#-------------------------------------------------------------------------------
if(@ARGV < 2) {
	print "Usage : $0 /path/to/Mioga.conf /path/to/file.csv [/path/to/file.csv]\n";
	exit(-1);
}

# Get Mioga config and initialize database connection object
my $miogaconf = new Mioga2::MiogaConf (shift);
$dbh = $miogaconf->GetDBH ();


#-------------------------------------------------------------------------------
# Process each CSV file from command-line
#-------------------------------------------------------------------------------
while (my $csvfile = shift) {
	my $instname = basename ($csvfile);
	$instname =~ s/\.csv$//;
	print "INSTANCE: $instname\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Check command-line arguments and initialize configuration
	#-------------------------------------------------------------------------------
	# Check if CSV file exists
	if (! -e $csvfile) {
		warn "Can't find file $csvfile";
		next;
	}

	# Check if corresponding instance exists
	my $instance = GetInstance($instname);
	if (!$instance) {
		warn "Unknown instance $instname";
		next;
	}

	# Get instance configuration
	my $config = new Mioga2::Config ($miogaconf, $instname);
	my $basedir = $config->GetWebdavDir ();

	# Initialize LDAP access parameters
	my $ldap_bind_dn = $instance->{ldap_bind_dn};
	my $ldap_bind_pwd = $instance->{ldap_bind_pwd};
	my $ldap_user_base_dn = $instance->{ldap_user_base_dn};

	# Connect to LDAP
	my $ldap = new Mioga2::LDAP ($config);
	$ldap->BindWith ($ldap_bind_dn, $ldap_bind_pwd);

	my %content = ();

	#-------------------------------------------------------------------------------
	# Parse CSV file
	#-------------------------------------------------------------------------------
	open (CSV, $csvfile);
	for (<CSV>) {
		print $_ if ($debug);
		chomp ($_);
		my ($email, $firstname, $lastname, $ident, $rowid) = ($_ =~ m/^([^,]*),([^,]*),([^,]*),([^,]*),([^,]*)$/);
		my %desc = (
			rowid => $rowid,
			lastname => $lastname,
			firstname => $firstname,
			ident => $ident
		);
		$content{$email} = \%desc;
	}
	close (CSV);


	#-------------------------------------------------------------------------------
	# Process each record of CSV file
	#-------------------------------------------------------------------------------
	while (my ($email, $desc) = each %content) {
		my $dn = '';

		# Update LDAP contents
		my $users = $ldap->SearchUsers (mail => $email);
		if (!scalar (@$users)) {
			# Get missing information from DB
			my $dbuser = SelectSingle ($dbh, "SELECT * FROM m_user_base WHERE rowid='$desc->{rowid}'");

			# No entry with this e-mail, create user
			my $user = ();
			$dn = "cn=$desc->{firstname} $desc->{lastname},$ldap_user_base_dn";
			$user->{dn} = $dn;
			$user->{givenName} = $desc->{firstname};
			$user->{sn} = $desc->{lastname};
			$user->{mail} = $email;
			$user->{ou} = $instname;
			$user->{uid} = $desc->{ident};
			$user->{userPassword} = $dbuser->{password};
			$ldap->AddUser ($user);
		}
		else {
			# An entry exists with this e-mail, add instance name to its 'ou'
			my $user = $users->[0];
			if (!grep (/^$instname$/, @{$user->{instances}})) {
				$dn = $user->{dn};
				push (@{$user->{instances}}, $instname);
				$ldap->ModifyUser (delete ($user->{dn}), $ldap->TranslateAttributes($user));
				
				# If user ident differs between instances, update m_user_base and m_uri to match new ident
				if ($user->{ident} ne $desc->{ident}) {
					ExecSQL($dbh, "UPDATE m_user_base SET lastname='$user->{lastname}', firstname='$user->{firstname}', ident='$user->{ident}', email='$email' WHERE rowid=$desc->{rowid}");
					ExecSQL($dbh, "UPDATE m_uri SET uri = regexp_replace (uri, '/$instname/home/$desc->{ident}', '/$instname/home/$user->{ident}') WHERE uri ~ '/$instname/home/$desc->{ident}' AND group_id=$desc->{rowid}");
					if(-d "$basedir/home/$desc->{ident}") {
						move ("$basedir/home/$desc->{ident}", "$basedir/home/$user->{ident}");
					}
				}
			}
		}

		# Finally change user type to LDAP
		ExecSQL($dbh, "UPDATE m_user_base SET dn='$dn', type_id=(SELECT rowid FROM m_group_type WHERE ident='ldap_user') WHERE rowid=$desc->{rowid}");
	}
}
