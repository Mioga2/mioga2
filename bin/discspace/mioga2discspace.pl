#! /usr/bin/perl
# $Header$
# ============================================================================
# MiogaII Project (C) 2003-2004 The MiogaII Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#	Description: 

=head1 NAME
	
mioga2discspace.pl : utility to calculate ddisk space for mioga2 Users/Groups
		use it in cron


=head1 USAGE

mioga2discspace.pl mioga_conf_file mioga_instance_name

=cut

#
# ============================================================================
# $Log$
# Revision 1.2  2004/12/09 10:21:18  lsimonneau
# A lot of mlinor bugfixes and features enhancements.
#
# Revision 1.1  2004/04/20 13:17:40  lsimonneau
# Add MiogaSearch in the Mioga Repository,
# reorganize the bin directory and
# update INSTALL file.
#
# Revision 1.2  2004/04/16 09:12:59  clargeau
# adding modification date
#
# Revision 1.1  2004/04/13 16:17:29  clargeau
# first add for disc space checker
#
#
# ============================================================================
#


use strict;
use Mioga2::Exception::DB;
use Mioga2::Exception::Simple;
use Mioga2::tools::database;
use Error qw(:try);
use Mioga2::Config;
use Mioga2::Old::GroupList;
use Mioga2::Old::UserList;
use Data::Dumper;

my $debug = 0;

if (@ARGV != 1)
{
	print STDERR "Usage : mioga2discspace.pl /path/to/Mioga.conf\n";
	exit -1;
}

my ($MIOGA_CONF_FILE) = @ARGV;
unless (-f $MIOGA_CONF_FILE)
{
	print STDERR "GroupDiskSpace : $MIOGA_CONF_FILE is not a valid Mioga.conf file!\n";
	exit -1;
}

my $mioga_conf = new Mioga2::MiogaConf($MIOGA_CONF_FILE);

my $dbh = $mioga_conf->GetDBH();

my $instance = SelectMultiple($dbh, "SELECT * FROM m_mioga");

foreach my $inst (@$instance) {

	my $config  = new Mioga2::Config($mioga_conf, $inst->{ident});

	my $private = $config->GetPrivateDir();
	my $public = $config->GetPublicDir();
	
	
	my $all_groups = new Mioga2::Old::GroupList($config, all => "");
	my $group_idents = $all_groups->GetIdents();
	my $group_rowids = $all_groups->GetRowids();
	
	my $all_users = new Mioga2::Old::UserList($config, all => "");
	my $user_idents = $all_users->GetIdents();
	my $user_rowids = $all_users->GetRowids();

	my $i_usr = 0;
	foreach my $usr (@$user_idents)
	{
		UpdateDiskSpace($config, $user_rowids->[$i_usr], $user_idents->[$i_usr]);
		$i_usr++;
	}
	
	my $i_grp = 0;
	foreach my $grp (@$group_idents)
	{
		UpdateDiskSpace($config, $group_rowids->[$i_grp], $group_idents->[$i_grp]);
		$i_grp++;
	}
}
	
# --------------------------------------------------------
# UpdateDiskSpace($rowid, $ident)
# --------------------------------------------------------
sub UpdateDiskSpace
{
	my ($config, $rowid, $ident) = @_;
	my $publicSize = 0;
	my $privateSize = 0;
	my $dir;

	my $private = $config->GetPrivateDir();
	my $public = $config->GetPublicDir();
	
	# public part size
	# ----------------
	if(-e "$public/$ident")
	{
		open(CMD, qq{du -sk "$public/$ident" |});
		$_ = <CMD>;
		($publicSize, $dir) = split;
		print STDERR "$public/$ident: size=$publicSize, dir=$dir\n" if ($debug);
		close(CMD);
	}

	# private part size
	# -----------------
	if(-e "$private/$ident")
	{
		open(CMD, qq{du -sk "$private/$ident" |});
		$_ = <CMD>;
		($privateSize, $dir) = split;
		print STDERR "$private/$ident: size=$privateSize, dir=$dir\n" if ($debug);
		close CMD;

	}

	my $disk_space_used = ($publicSize + $privateSize) * 1024;
	if ($disk_space_used > 2147483647) {
		$disk_space_used = 2147483647;
	}

	ExecSQL($dbh, "update m_group_base set disk_space_used = $disk_space_used, modified = now()  where rowid=$rowid");
}
