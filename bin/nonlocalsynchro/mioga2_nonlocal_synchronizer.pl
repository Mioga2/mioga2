#!/usr/bin/perl -w

use strict;
use Error qw(:try);
use POSIX qw(strftime);
use Proc::ProcessTable;
use Data::Dumper;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIGroup;
use Mioga2::ExternalMioga;
use Mioga2::XML::Simple;
use Mioga2::InstanceList;
use Mioga2::UserList;
use Mioga2::tools::string_utils;

my $debug = 0;

my $miogaconffile;
my @instnames;
my $dn;

if(@ARGV < 1) {
	print "Usage : $0 /path/to/Mioga.conf [inst_names]\n";
	exit(-1);
}


#-------------------------------------------------------------------------------
# Ensure no other instance of synchronizer is currently running
#-------------------------------------------------------------------------------
my $p = new Proc::ProcessTable ('cache_ttys' => 1);
my $ref = $p->table ();
if (my @process = grep { ($_->{cmndline} =~ /$0/) && ($_->{exec} eq '/usr/bin/perl') && ($_->{pid} != $$) } @$ref) {
	print "Already running, aborting.\n" if ($debug);
	exit;
}

# Process command-line arguments
for (@ARGV) {
	if (m/Mioga\.conf$/) {
		$miogaconffile = $_;
	}
	elsif (m/=/) {
		$dn = $_;
	}
	else {
		push (@instnames, $_);
	}
}

my $miogaconf = new Mioga2::MiogaConf($miogaconffile);

# If no instance specified on the command-line, check all instances
if (!scalar (@instnames)) {
	my $instances = GetInstances($miogaconf);
	for (@$instances) {
		push (@instnames, $_->{ident});
	}
}

for (@instnames) {
	print "Synchronize instance : $_\n" if ($debug);
	my $inst = GetInstance ($miogaconf, $_);
	my $config = new Mioga2::Config($miogaconf, $inst->{ident});

	SynchronizeExternalUser($config);
	if ($inst->{use_ldap}) {
		SynchronizeLdapUser($config, $dn);
	}
}


sub GetInstances {
	my $miogaconf = shift;
	
	my $dbh = $miogaconf->GetDBH();

	return SelectMultiple($dbh, "SELECT * FROM m_mioga");
}


sub GetInstance {
	my ($miogaconf, $ident) = @_;
	
	my $dbh = $miogaconf->GetDBH();

	return SelectSingle ($dbh, "SELECT * FROM m_mioga WHERE ident='$ident'");
}

sub SynchronizeLdapUser {
	my $config = shift;
	my $dn = shift;

	my $dbh = $config->GetDBH();
	my $ldap = new Mioga2::LDAP($config);

	my $ldap_users;
	if (!$dn) {
		# Get the list of all LDAP user.
		$ldap_users = $ldap->SearchUsers();
	}
	else {
		# Get the LDAP user according to specified DN.
		my $ident_field = $config->{ldap_user_ident_attr};
		my ($ident) = ($dn =~ m/^$ident_field=([^,]*),.*$/);
		my %filter = (
			$ident_field => $ident
		);
		$ldap_users = $ldap->SearchUsers(%filter);
	}

	# Ensure LDAP admin record is in the list, even if it doesn't have current instance in its membership list (usually "ou" attribute)
	my $instance = Mioga2::InstanceList->new ($config, { attributes => { rowid => $config->GetMiogaId () } });
	my $admin = Mioga2::UserList->new ($config, { attributes => { rowid => $instance->Get ('admin_id'), type => 'ldap_user' } });
	if ($admin->Count ()) {
		my $transl = $config->GetLDAPTranslationTable ();
		my $ldap_admin = $ldap->SearchUsers($transl->{email} => $admin->Get ('email'), $transl->{ident} => $admin->Get ('ident'), use_instance_user_filter => 0);
		if (@$ldap_admin) {
			# A record has been found in LDAP, push it to list
			push (@$ldap_users, $ldap_admin->[0]);
		}
	}

	# sort the to lists by dn
	my @sorted_ldap_users = sort {$a->{ident} cmp $b->{ident}} @$ldap_users;

	my $db_users;
	if (!$dn) {
		# Get the list of all LDAP user declared in DB.
		$db_users = SelectMultiple($dbh, "SELECT * FROM m_user_base ".
											"WHERE mioga_id = ".$config->GetMiogaId().
											"AND dn != '' ORDER BY dn ASC");
	}
	else {
		# Get the list of all LDAP user declared in DB.
		$db_users = SelectMultiple($dbh, "SELECT * FROM m_user_base ".
											"WHERE mioga_id = ".$config->GetMiogaId().
											"AND dn = '$dn' ORDER BY dn ASC");
	}


	# Get difference between to lists
	my %ldap_dn;
	my %db_dn;
	foreach my $elem (@sorted_ldap_users) {
		delete $elem->{password};
		$ldap_dn{$elem->{dn}} = $elem;
	}

	foreach my $elem (@$db_users) {
		delete $elem->{password};
		$db_dn{$elem->{dn}} = $elem;
	}

	# Create New Users
	foreach my $elem (@sorted_ldap_users) {
		# Cleanup useless fields
		for (qw/description/) {
			delete ($elem->{$_});
		}

		next unless ($elem->{dn} ne '');

		if(! exists $db_dn{$elem->{dn}}) {
			my $instance = Mioga2::InstanceList->new ($config, { attributes => { rowid => $config->GetMiogaId () } });
			$elem->{skeleton} = $config->GetSkeletonDir () . "/" . $instance->Get ('lang') . "/user/LDAP.xml";
			$elem->{type} = 'ldap';
			$elem->{data_expiration_date} = strftime("%Y-%m-%d %H:%M:%S", gmtime(time + 600));
			try {
				print STDERR "Create User : " . $elem->{dn} . "\n" if ($debug);
				delete $elem->{instances};

				# Check if database already contains a LDAP user matching ident / email but having no DN
				# This will be the case when instance admin is a LDAP user, the account is created in database but has no DN until LDAP synchro is done
				my $user = Mioga2::UserList->new ($config, { attributes => { ident => $elem->{ident}, email => $elem->{email}, type => 'ldap_user' } });
				if ($user->Count () == 1) {
					# LDAP user matches ident / email in database, update user
					delete ($elem->{skeleton});
					delete ($elem->{type});
					for my $attr (keys (%$elem)) {
						$user->Set ($attr, $elem->{$attr});
					}
				}
				else {
					# No user matches ident / email in database, create user
					$user = Mioga2::UserList->new ($config, { attributes => $elem });
				}
				$user->Store ();
			}
			catch Mioga2::Exception::User with {
				my $err = shift;
				warn("==> Mioga2::UserLDAPCreate : user $elem->{ident} already exists err = " . Dumper($err));
			}
			otherwise {
				my $err = shift;
				warn ("Problem with UserLDAPCreate " . Dumper($err));
			};
		}
		else {
			if ($db_dn{$elem->{dn}}->{status_id} == 3) {
				print STDERR "Enable User : " . $elem->{dn} . "\n" if ($debug);
				try {
					my $user = Mioga2::UserList->new ($config, { attributes => { dn => $elem->{dn} } });
					$user->Set ('status', 'active');
					$user->Store ();
				}
				otherwise {
					my $err = shift;
					warn ("Problem enabling user $elem->{dn} " . Dumper($err));
				};
			}
			my $attrs = $ldap_dn{$elem->{dn}};
			$attrs->{data_expiration_date} = strftime("%Y-%m-%d %H:%M:%S", gmtime(time + 600));
			delete $attrs->{instances};
			try {
				my $user = Mioga2::UserList->new ($config, { attributes => { dn => $attrs->{dn} } });
				for (keys (%$attrs)) {
					$user->Set ($_, st_CheckUTF8 ($attrs->{$_}));
				}
				$user->Store ();
			}
			otherwise {
				my $err = shift;
				warn ("Problem updating user $elem->{dn} " . Dumper($err));
			};
		}
	}

	# Disable old users
	if (!$dn) {
		foreach my $elem (@$db_users) {
			next if ($elem->{dn} eq '');

			if(! exists $ldap_dn{$elem->{dn}}) {
				print STDERR "Delete User : " . $elem->{dn} . "\n" if ($debug);
				try {
					my $user = Mioga2::UserList->new ($config, { attributes => { dn => $elem->{dn} } });
					if (!$config->GetMiogaConf ()->IsCentralizedUsers ()) {
						$user->Set ('status', 'deleted_not_purged');
						$user->Store ();
					}
					else {
						$user->Delete ();
					}
				}
				otherwise {
					my $err = shift;
					warn ("Problem disabling user $elem->{dn} " . Dumper($err));
				};
			}
		}
	}
	else {
		if((! exists $ldap_dn{$dn}) && exists $db_dn{$dn}) {
			print STDERR "Delete User : " . $dn . "\n" if ($debug);
			try {
				my $user = Mioga2::UserList->new ($config, { attributes => { dn => $dn } });
				$user->Set ('status', 'deleted_not_purged');
				$user->Store ();
			}
			otherwise {
				my $err = shift;
				warn ("Problem deleting user $dn " . Dumper($err));
			};
		}
	}

	# Modify expired users
	foreach my $elem (@$db_users) {
		next if ($elem->{dn} eq '');

		if(du_ISOToSecond($elem->{data_expiration_date}) < time) {
			my $attrs = $ldap_dn{$elem->{dn}};
			next if (!defined ($attrs->{dn}) || ($attrs->{dn} eq ''));
			$attrs->{data_expiration_date} = strftime("%Y-%m-%d %H:%M:%S", gmtime(time + 600));
			# $user->{rowid} = $elem->{rowid};
			print STDERR "Modify User : " . $elem->{dn} . "\n" if ($debug);
			try {
				my $user = Mioga2::UserList->new ($config, { attributes => { dn => $attrs->{dn} } });
				for (keys (%$attrs)) {
					$user->Set ($_, $attrs->{$_});
				}
				$user->Store ();
			}
			otherwise {
				my $err = shift;
				warn ("Problem updating user $elem->{dn} " . Dumper($err));
			};
		}
	}
}

sub SynchronizeExternalUser {
	my $config = shift;

	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();

	my $sql =  "SELECT * FROM m_external_mioga WHERE mioga_id = $mioga_id";
	my $res = SelectMultiple($dbh,$sql);
	print STDERR "res = ".Dumper($res)."\n" if ($debug);
	
	#
	# Loop on each external Migoa for this instance
	#
	foreach my $ext (@$res) {
		my $ext_mioga = new Mioga2::ExternalMioga($config, rowid => $ext->{rowid});
		if (!defined($ext->{last_synchro})) {
			$ext->{last_synchro} = "1970-01-01";
		}

		#
		# Get Modified or destroyed Users
		#
		$sql =  "SELECT external_ident FROM m_user_base WHERE external_mioga_id = $ext->{rowid} AND  mioga_id = $mioga_id";
		my $users = SelectMultiple($dbh,$sql);
		if (scalar(@$users) > 0) {
			my @idents;
			foreach my $entry (@$users) {
				push @idents, $entry->{external_ident};
			}

			try {
				my $response = $ext_mioga->SendRequest(application => 'Colbert',
													   method => 'SXMLGetModifiedUserList',
													   args => { last_synchro => $ext->{last_synchro},
																  idents => join(",",@idents) });

				print STDERR "response = $response\n" if ($debug);
				my $xs = new Mioga2::XML::Simple();
				my $xmltree = $xs->XMLin($response);
				print STDERR "xmltree = ".Dumper($xmltree)."\n" if ($debug);

				@idents = ();
				if (exists($xmltree->{DeletedUser})) {
					if (ref($xmltree->{DeletedUser}) eq 'ARRAY') {
						foreach my $entry (@{$xmltree->{DeletedUser}}) {
							push @idents, "'".$entry."'";
						}
					}
					else {
						push @idents, "'".$xmltree->{DeletedUser}."'";
					}
					print STDERR "idents = ".Dumper(\@idents) if ($debug);
					if (@idents > 0) {
						$sql =  "SELECT * FROM m_user_base WHERE external_mioga_id = $ext->{rowid} AND  mioga_id = $mioga_id AND external_ident in (".join(",", @idents).")";
						print STDERR "sql = $sql\n" if ($debug);
						$users = SelectMultiple($dbh,$sql);
						print STDERR "users = ".Dumper($users)."\n" if ($debug);
						foreach my $u (@$users) {
							UserDisable($config, $u->{rowid});
						}
					}
				}

				@idents = ();
				my %user_hash;
				if (exists($xmltree->{User})) {
					if (ref($xmltree->{User}) eq 'ARRAY') {
						foreach my $entry (@{$xmltree->{User}}) {
							push @idents, "'".$entry->{ident}."'";
							$user_hash{$entry->{ident}} = $entry;
						}
					}
					else {
						push @idents, "'".$xmltree->{User}->{ident}."'";
						$user_hash{$xmltree->{User}->{ident}} = $xmltree->{User};
					}
					if (@idents > 0) {
						$sql =  "SELECT * FROM m_user_base WHERE external_mioga_id = $ext->{rowid} AND  mioga_id = $mioga_id AND external_ident in (".join(",", @idents).")";
						print STDERR "sql = $sql\n" if ($debug);
						$users = SelectMultiple($dbh,$sql);
						print STDERR "users = ".Dumper($users)."\n" if ($debug);
						foreach my $u (@$users) {
							my $values;
							$values->{rowid} = $u->{rowid};
							$values->{firstname} = $user_hash{$u->{external_ident}}->{firstname};
							$values->{lastname} = $user_hash{$u->{external_ident}}->{lastname};
							$values->{email} = $user_hash{$u->{external_ident}}->{email};
							print STDERR "values = ".Dumper($values)."\n" if ($debug);
							try {
								UserExternalModify($config, $values);
							}
							otherwise {
								my $err = shift;
								print "Caught exception: " . $err->as_string ();
								return;
							};
						}
					}
				}
				$sql = "UPDATE m_external_mioga set last_synchro=now() WHERE rowid=$ext->{rowid}";
				ExecSQL($dbh, $sql);
			}
			catch Mioga2::Exception::ExternalMioga with {
				print STDERR "Caught Mioga2::Exception::ExternalMioga\n";
				return;
			}
			otherwise {
				my $err = shift;
				print "Caught exception: " . $err->as_string ();
				return;
			};
		}
	}
}
