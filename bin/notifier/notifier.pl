#!/usr/bin/perl -w

# package notifier;

use strict;

use Data::Dumper;
use Error qw(:try);
use Fcntl;
use Mioga2::MiogaConf;
use Mioga2::tools::Convert;
use Mioga2::Notifier::Louvre;
use Mioga2::Notifier::RSS;
use Mioga2::Notifier::Search_Engine;
use Mioga2::Notifier::FileSystem;
use POSIX qw(mkfifo);
use Sys::Syslog;
#use Time::HiRes qw(usleep);


#######################
## Main program
#######################

if (@ARGV != 1) {
    print "usage: $0 path_to_miogaconf\n";
    exit 1;
}

my $miogaconf   = $ARGV[0];
my $config      = Mioga2::MiogaConf->new($miogaconf);
my $dbh         = $config->GetDBH;
my $fifo        = $config->{notifierfifo};

openlog('[Mioga2 notifier]', 'pid', 'user') or die "[Mioga2 notifier] Can't open syslog!";
syslog('info', 'Starting Mioga2 notifier...');

close STDIN;
close STDOUT;
close STDERR;
open(STDOUT, '>>/dev/null');
open(STDERR, '>>/dev/null');

if (!open(FIFO, "+>$fifo")) {
    syslog('err', "Can't open fifo: $!. Exiting...");
}
my $flags   = fcntl(FIFO, F_GETFL, 0);


# PLUGIN INITIALISATION

my %plugin_for_type = (
	'louvre' => Mioga2::Notifier::Louvre->new($config),
	'rss'    => Mioga2::Notifier::RSS->new($config),
	'search' => Mioga2::Notifier::Search_Engine->new($config),
	'filesystem' => Mioga2::Notifier::FileSystem->new($config),
);


# MAIN LOOP

while (1) {
    my %need_flushing = ();
    while (<FIFO>) {
        fcntl(FIFO, F_SETFL, $flags|O_NONBLOCK);
        chomp;
        my $obj;
		utf8::encode ($_);
        try {
            $obj = Mioga2::tools::Convert::JSONToPerl ($_);
        }
        otherwise {
            syslog("warning", "Received malformed data: $_");
        };
        next unless ref($obj) eq 'HASH';
        if (!$obj->{type}) {
            syslog("warning", "Received data with no type specified!");
            next;
        } elsif (ref($obj->{type} ne 'ARRAY')) {
        	syslog("warning", "TYPE must be an arrayref");
        	next;
        }
        foreach my $type_m (@{$obj->{type}}) {
        	if ($type_m) {
        		my $type = lc($type_m);
        		my $plugin = $plugin_for_type{ $type };
	        	unless (defined $plugin) {
    	        	syslog("warning", "Don't know how to handle type \"$type\"");
        	    	next;
        		}
        		syslog('debug', 'calling '.$type.'->notify');
        		$plugin->notify($obj);
        		$need_flushing{$type} = 1;
        	}
        }
    }
    fcntl(FIFO, F_SETFL, $flags);
    foreach my $type (keys %need_flushing) {
    	syslog('debug', 'calling '.$type.'->flush');
        $plugin_for_type{$type}->flush();
    }
}
