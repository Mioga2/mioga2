#!/usr/bin/perl -w

# This process wait on a fifo for ident of instance to crawl
# It runs only one mioga2 crawler at the time

use strict;

use Data::Dumper;
use Error qw(:try);
use Fcntl;
use Mioga2::MiogaConf;
use Sys::Syslog;


#######################
## Main program
#######################

if (@ARGV != 1) {
    print "usage: $0 path_to_miogaconf\n";
    exit 1;
}

my $miogaconf   = $ARGV[0];
my $config      = Mioga2::MiogaConf->new($miogaconf);
my $dbh         = $config->GetDBH;
my $fifo        = $config->{searchenginefifo};

openlog('[Mioga2 searchengine]', 'pid', 'user') or die "[Mioga2 searchengine] Can't open syslog!";
syslog('info', 'Starting Mioga2 searchengine...');

close STDIN;
close STDOUT;
close STDERR;
open(STDOUT, '>>/dev/null');
open(STDERR, '>>/dev/null');

if (!open(FIFO, "+>$fifo")) {
    syslog('err', "Can't open fifo: $!. Exiting...");
}
my $flags   = fcntl(FIFO, F_GETFL, 0);


# MAIN LOOP

my %instances;
my $line;
while (1) {
    while ($line = <FIFO>) {
        fcntl(FIFO, F_SETFL, $flags|O_NONBLOCK);
        chomp $line;
    	syslog("info", "line = $line");
		$instances{$line} += 1;
    }
	foreach my $inst (keys(%instances)) {
		system("/usr/local/bin/crawl.sh $inst");
	}
    fcntl(FIFO, F_SETFL, $flags);
}
