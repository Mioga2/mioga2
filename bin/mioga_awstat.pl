#! /usr/bin/perl -w
# $Header$
# ============================================================================
# MiogaII Project (C) 2003-2004 The MiogaII Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#	Description: 

=head1 NAME
	
mioga_awstat.pl : utility for creation of stats (based oni awstat) for each Mioga Instance. It creates a group named statistics if it doesn't exists and does apache filtering based on instance name and run awstat
		use it in cron


=head1 USAGE

 mioga_awstat.pl mioga_conf_file apache_log_dir

=cut

#
# ============================================================================
# ============================================================================
#


use strict;
use Mioga2::Exception::DB;
use Mioga2::Exception::Simple;
use Mioga2::tools::database;
use Error qw(:try);
use Mioga2::Config;
use Mioga2::Old::GroupList;
use Mioga2::Old::UserList;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIAuthz;
use Data::Dumper;

my $debug = 0;
my $stat_group = "statistics";
my $group_skel = "Standard.xml";

if (@ARGV != 2)
{
	print STDERR "Usage : $0 /path/to/Mioga.conf /apache/log/dir\n";
	exit -1;
}

my ($MIOGA_CONF_FILE, $APACHE_LOG_DIR) = @ARGV;
unless (-f $MIOGA_CONF_FILE)
{
	print STDERR "$0 : $MIOGA_CONF_FILE is not a valid Mioga.conf file!\n";
	exit -1;
}

unless (-d $APACHE_LOG_DIR)
{
	print STDERR "$0 : $APACHE_LOG_DIR is not a valid apache directory!\n";
	exit -1;
}

my $mioga_conf = new Mioga2::MiogaConf($MIOGA_CONF_FILE);

my $dbh = $mioga_conf->GetDBH();

my $instance = SelectMultiple($dbh, "SELECT * FROM m_mioga");

# ---------------------------------------------------------------
# GenerateAwstatsConf
# ---------------------------------------------------------------
sub GenerateAwstatsConf {
	my ($modele, $domain, $awstat_dir, $apache_log) = @_;
	print STDERR "-- $modele, $domain, $awstat_dir, $apache_log\n" if $debug;
	my $content = "";
	open (FILE, $modele) or print STDERR "$0 cannont open file <$modele> for reading\n";
	while (<FILE>)
	{
		my $line = $_;
		$line =~ s/MIOGA_SITE_DOMAIN/$domain/;
		$line =~ s/MIOGA_DIR_DATA/$awstat_dir/;
		$line =~ s/MIOGA_APACHE_LOG_FILE/$apache_log/;
		$content .= $line;
	}
	close (FILE);

	my $out_file = "$awstat_dir/awstats.$domain.conf";
	warn $out_file if $debug;
	open (OUTFILE, ">$out_file ") or print STDERR "$0 cannont open file <$$out_file> for writing\n";
	print OUTFILE $content;
	close (OUTFILE);
}

# ---------------------------------------------------------------
# Main
# ---------------------------------------------------------------
foreach my $inst (@$instance) {

	print "$inst->{ident}\n";

	my $inst_config = new Mioga2::Config($mioga_conf, $inst->{ident});
	my $inst_base_path = $inst_config->GetBasePath().'/'.$inst->{ident};
	warn $inst_base_path if $debug;
	my $domain_name = $inst_config->GetDomainName();
	warn $domain_name if $debug;
	my $install_path = $inst_config->GetInstallPath();
	warn $install_path if $debug;
	my $inst_tmp_apache = $inst_config->GetTmpDir().'/statistics/apache'; # where to write instance specific apache log
	my $inst_tmp_awstats = $inst_config->GetTmpDir().'/statistics/awstats'; # where to write instance specific awstats file
	warn $inst_tmp_apache if $debug;
	warn $inst_tmp_awstats if $debug;
	# création des répertoires temporaires si nécessaire
	system("mkdir -p \"$inst_tmp_apache\"");
	system("mkdir -p \"$inst_tmp_awstats\"");

	# grep pour ne récupérer que les fichiers de l'instance
	system("grep -h \'$inst_base_path\' $APACHE_LOG_DIR/*access.log.1 $APACHE_LOG_DIR/*access.log | grep -v css | grep -v theme > $inst_tmp_apache/access.log");

	# creation des fichiers awstats par instance
	GenerateAwstatsConf ("$install_path/conf/awstats.conf", $domain_name, $inst_tmp_awstats, "$inst_tmp_apache/access.log");

	# generation des satistiques
	system("export AWSTATS_ENABLE_CONFIG_DIR=1; /usr/share/awstats/tools/awstats_buildstaticpages.pl  -configdir=$inst_tmp_awstats -config=$domain_name -update -lang=fr -awstatsprog=/usr/lib/cgi-bin/awstats.pl -dir=$inst_tmp_awstats; cp $inst_tmp_awstats/awstats.$domain_name.html $inst_tmp_awstats/index.html");

	my $sql = "select * from m_mioga where ident='$inst->{ident}'";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
		next;
	}

	my $mioga_id = $res->{rowid};
	my $admin_id = $res->{admin_id};

	my $values = {};
	$values->{public_part} = 0;
	$values->{ident} = $stat_group;
	$values->{anim_id} = $admin_id;
	$values->{skeleton} = $group_skel;
	$values->{creator_id} = $admin_id;

	# create groups for statistics
	try {
		my $group_id = GroupCreate($inst_config, $values);
		print "Create group $values->{ident} : $group_id\n";
	}
	otherwise {
		# nothing to do : group exists
	}

	# we need to move all html files to statistic group and register them with AuthzDeclareURI
	# AuthzDeclareURI($dbh, $uri, $admin_id, 'text/html');
	my @files;
	opendir(DIR, "$inst_tmp_awstats") or warn ("Cannot open dir <$inst_tmp_awstats> for reading");
		@files = sort grep { /\.html$/ } readdir(DIR);
	close DIR;

	foreach my $file (@files)
	{
		print STDERR "----> $file\n" if $debug;
		print STDERR "-- >cp $inst_tmp_awstats/$file $install_path/$inst->{ident}/web/htdocs/home/statistics/\n" if $debug;
		system("cp $inst_tmp_awstats/$file $install_path/$inst->{ident}/web/htdocs/home/statistics/");
		my $uri = $inst_base_path."/home/statistics/$file";
		print STDERR "-- > adding uri in database $uri\n" if $debug;
		AuthzDeclareURI($dbh, $uri, $admin_id, 'text/html', -s "$inst_tmp_awstats/$file");
	}
}


