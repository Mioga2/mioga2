#!/usr/bin/perl
#===============================================================================
#
#         FILE:  replace_app.pl
#
#        USAGE:  ./replace_app.pl -h
#
#  DESCRIPTION:  Replace an application by another. Admin app has been
#  				 deprecated by Colbert, so all users and groups that are using 
#  				 Admin should now be using Colbert. This script enables and 
#  				 activate Colbert for all users and groups that have Admin
#  				 enabled.
#  				 Note: According to applications ident passed on the command-line
#  				 it also works with other applications !!!
#
#      OPTIONS:  --- 
# REQUIREMENTS:  --- 
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org> 
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org) 
#      CREATED:  20/10/2010 14:42
#
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project This program is free software;
#  you can redistribute it and/or modify it under the terms of the gnu general
#  public license as published by the free software foundation; either version
#  2 of the license, or (at your option) any later version.
#
#===============================================================================

use strict;
use warnings;

use Mioga2::UserList;
use Mioga2::GroupList;
use Mioga2::ApplicationList;
use Mioga2::tools::APIApplication;

use Getopt::Long;
use Data::Dumper;

my $debug = 0;

my $instance = "";
my $mioga_id = 0;
my $verbose = 0;
my $help = 0;
my $miogaconf_path = '/var/lib/Mioga2/conf/Mioga.conf';
my $delete = 0;
my $old_app = '';
my $new_app = '';
my $miogaconf;


#===============================================================================

=head2 Usage

Display command usage.

=cut

#===============================================================================
sub Usage {
	print "Usage : $0 [options] --instance <instance_ident> --old-app <old_application_ident> --new-app <new_application_ident>\n";
	print "    Options :\n";
	print "        --conf='path to Mioga.conf'                  : default to /var/lib/Mioga2/conf/Mioga.conf\n";
	print "        --verbose                                    : verbose mode\n";
	print "        --instance='name'                            : name of instance for some commands\n";
	print "        --help                                       : print usage\n";

	return ();
}	# ----------  end of subroutine Usage  ----------



my $result = GetOptions ("help" => \$help,
			          "conf=s"  => \$miogaconf_path,
			          "instance=s"  => \$instance,
			          "verbose"  => \$verbose,
					  "delete"   => \$delete,
					  "old-app=s" => \$old_app,
					  "new-app=s" => \$new_app
		);

print "Getopt result = $result\n" if ($debug);
print "MiogaConf path: $miogaconf_path\n" if ($debug);
print "Instance: $instance\n" if ($debug);
print "Old app: $old_app\n" if ($debug);
print "New app: $new_app\n" if ($debug);
print "Delete: $delete\n" if ($debug);
print "Verbose: $verbose\n" if ($debug);

if ($help || ($instance eq '') || ($old_app eq '') || ($new_app eq '')) {
	Usage ();
	exit(-1);
}

$miogaconf = new Mioga2::MiogaConf ($miogaconf_path);

# Initialize from Mioga2::Config
my $config = new Mioga2::Config ($miogaconf, $instance);

print "Replacing application $old_app by $new_app\n" if ($verbose);

my $applist = Mioga2::ApplicationList->new ($config, { attributes => { ident => $old_app } });

my $userlist = Mioga2::UserList->new ($config, { attributes => { applications => $applist } });
for ($userlist->GetUsers ()) {
	print "Enabling $new_app for user $_->{label}\n" if ($verbose);
	my $user = Mioga2::UserList->new ($config, { attributes => { rowid => $_->{rowid} } });
	my @app = ( { ident => $new_app, active => 1 } );
	for ($user->GetActiveApplicationList ()->GetApplications ()) {
		push (@app, { ident => $_->{ident}, active => 1 });
	}
	$user->EnableApplications (@app);
	$user->ActivateApplications (@app);
}

my $grouplist = Mioga2::GroupList->new ($config, { attributes => { applications => $applist } });
for ($grouplist->GetGroups ()) {
	print "Enabling $new_app for group $_->{ident}\n" if ($verbose);
	my $group = Mioga2::GroupList->new ($config, { attributes => { rowid => $_->{rowid} } });
	my @app = ( { ident => $new_app } );
	for ($group->GetActiveApplicationList ()->GetApplications ()) {
		push (@app, { ident => $_->{ident}, active => 1 });
	}
	# print Dumper \@app;
	$group->EnableApplications (@app);
	$group->ActivateApplications (@app);
}

if ($delete) {
	print "Removing application $old_app from instance $instance\n" if ($verbose);
	ApplicationRemove($config->GetDBH (), $applist->Get ('rowid'));
}
