#!/usr/bin/perl -w
# $Header$
# ============================================================================
# MiogaII Project (C) 2003 The MiogaII Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#	Description: 

=head1 NAME
	
miogamailinglist.pl : Daemon to handle miogaqueue request and store mail in 
	                  database and dispatch to the group members


=head1 USAGE

miogamailinglist.pl /path/to/Mioga.conf

=cut

#
# ============================================================================
# $Log$
# Revision 1.5  2004/12/20 13:44:01  lsimonneau
# Fixing bug on blobs. Change path of pidfile.
#
# Revision 1.4  2004/12/06 13:41:07  lsimonneau
# Add use UserList; in GroupListBase.pm because of crash in miogamailinglist.pl
#
# Revision 1.3  2004/06/18 08:29:29  clargeau
# fixing bug 0000408
#
# Revision 1.2  2004/06/02 15:54:06  lsimonneau
# Minor bugfixes.
#
# Revision 1.1  2004/04/20 13:17:40  lsimonneau
# Add MiogaSearch in the Mioga Repository,
# reorganize the bin directory and
# update INSTALL file.
#
# Revision 1.4  2004/04/13 15:09:54  lsimonneau
# Remove boring warn.
#
# Revision 1.3  2004/04/13 13:23:43  lsimonneau
# Remove stupid warning !!
#
# Revision 1.2  2004/04/13 13:07:45  lsimonneau
# Major bugfixes in ML and mioga core.
#
# Revision 1.1  2004/03/09 10:58:56  lsimonneau
# First release of MailingList archive browser and mailing list daemon and scripts.
#
# Major features enhancements in Admin : Add hooks and enhance skeletons.
#
# ============================================================================
#

use strict;
use vars qw(@ISA);
use Error qw(:try);
use DBI;
use Mioga2::Exception::Simple;
use Mioga2::Exception::Group;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Data::Dumper;
use Date::Manip;
use Mioga2::Old::UserList;
use Mioga2::Old::Team;
use Mioga2::Old::Group;
use Mioga2::Old::User;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::XML::Simple;
use MIME::Parser;
use MIME::Entity;
use Text::Iconv;
use Encode::Detect::Detector;
use Data::Dumper;
use POSIX qw(tmpnam setsid);
use Sys::Syslog qw(:DEFAULT setlogsock);
use Fcntl qw ( O_RDWR );
use Email::Valid;

my $debug = 0;


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
sub DatabaseInsert
{
	my ($config, $group_id, $mailfrom, $subject, $content) = @_;

	syslog('mail|debug', 'miogamailinglist : DatabaseInsert: Entering\n') if ($debug);

	my $tmpFile = tmpnam();
	open(FILEOUT, ">$tmpFile") or die "cannot open file <$tmpFile> for writing : $!\n";
	print FILEOUT $content;
	close(FILEOUT);

	$subject = st_FormatPostgreSQLString($subject);
	$mailfrom = st_FormatPostgreSQLString($mailfrom);

	my $dbh = $config->GetDBH();
		try {
		BeginTransaction($dbh);

		# lo_* are definitively stupid ...
		ExecSQL($dbh, "SELECT * FROM mailinglist WHERE rowid=0");

		my $oid = $dbh->func($tmpFile, "lo_import");

		my $sql = "INSERT INTO mailinglist (created, modified, group_id, mail_from, subject, date, content) ".
			      "VALUES (now(), now(), $group_id, '$mailfrom', '$subject', now(), $oid)";

		ExecSQL($config->GetDBH(), $sql);

		unlink($tmpFile);

		EndTransaction($dbh);
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};

	syslog('mail|debug', 'miogamailinglist : DatabaseInsert: Leaving\n') if ($debug);
}

# ------------------------------------------------------------------------------
# envoi du message et stockage dans la BDD
# ------------------------------------------------------------------------------
sub MsgSendStock
{
	my ($config, $group_name, $msgfile, $filecontent) = @_;

	syslog('mail|debug', 'miogamailinglist : MsgSendStock: Entering\n') if ($debug);

	my $tmp = tmpnam;
	my $instance = $config->{ident};
	my $mailing_list_description = $config->GetMailingListDescription();
	$mailing_list_description =~ s/\$\{group\}/$group_name/;
	$mailing_list_description =~ s/\$\{instance\}/$instance/;
	my $reply_to = "$mailing_list_description\@$config->{domain_name}";
	
	mkdir($tmp, 0700);

	my $parser = new MIME::Parser();
	$parser->decode_headers(1);
	$parser->output_dir("$tmp");

	# parse the mail with header decoding option.
	my $entity = $parser->parse_data($filecontent);

	my $dbh = $config->GetDBH();
	my $subject = CheckUTF8($entity->get('Subject'));
	my $from = CheckUTF8($entity->get('From'));

	# B1216 - Ensure sender is member of target group
	chomp ($from);
	try {
		my $sender = Mioga2::Old::User->new ($config, email => lc (Email::Valid->address ($from)));
		my $groups = $sender->GetExpandedGroups ()->GetIdents ();
		if (grep (/^$group_name$/, @$groups)) {
			my $group = new Mioga2::Old::Group($config, ident => $group_name);
			my $group_id = $group->GetRowid();

			DatabaseInsert($config, $group_id, $from, $subject, $filecontent);

			# parse the mail without header decoding option.
			$parser->decode_headers(0);
			$entity = $parser->parse_data($filecontent);

			$entity->replace("Reply-To" => "$reply_to");
			$entity->delete("Cc");


			my $users = $group->GetInvitedUsers();
			$users->SendMail($entity);
			
			my $teams = $group->GetInvitedTeams();
			$teams->SendMail($entity);
		}
		else {
			syslog('mail|err', "miogamailinglist.pl : Sender $from rejected, not member of group $group_name");
		}
	}
	otherwise {
		syslog('mail|err', "miogamailinglist.pl : Sender $from rejected, no such user");
	};

	unlink $msgfile;
	system("rm -rf $tmp");


	syslog('mail|debug', 'miogamailinglist : MsgSendStock: Leaving\n') if ($debug);
}

## check if string is in UTF8 and convert it if needed.
sub CheckUTF8 {
    my ($str) = @_;
    
    my $conv    = Text::Iconv->new('utf8', 'utf8');
    my $tmp_str = $conv->convert($str);
    unless ($tmp_str) {
        my $charset = detect($str) || 'iso-8859-15'; # defaults to latin9
        $conv = Text::Iconv->new($charset, "utf8");
        $str  = $conv->convert($str);
    }
    return $str;
}

# ------------------------------------------------------------------------------
# Programme principal
# ------------------------------------------------------------------------------
setlogsock('unix');
openlog ('miogamailinglist', 'cons, pid', 'user');

my $miogaconf_file = $ARGV[0];
if(! defined $miogaconf_file) {
	print STDERR "miogamailinglist.pl: Too few paramaters.\nUsage : $0 /path/to/Mioga.conf\n";
	exit -1;
}

my $miogaconf = new Mioga2::MiogaConf($miogaconf_file);

if(!defined $miogaconf) {
    print STDERR "miogamailinglist.pl: Invalid Mioga.conf file.\n";
    exit -1;
}

my $maildir = $miogaconf->{mailinglist}->{maildir};
my $maildirerror = $miogaconf->{mailinglist}->{maildirerror};
my $fifo = $miogaconf->{mailinglist}->{mailfifo};

if($maildir !~ /\/$/) {
    $maildir .= "/";
}

if($maildirerror !~ /\/$/) {
    $maildirerror .= "/";
}
fork && exit(0);

setsid();

# daemon mode
# -----------
open(STDIN, "/dev/null");
open(STDOUT, "/dev/null");
open(STDERR, "/dev/null");


open( FILE, ">/var/run/mioga/miogamailinglist.pid" ) || return;
printf FILE "$$\n";
close FILE;


my $sc = "";

sysopen (FIFO, "$fifo", O_RDWR);

my $config;
my @groupdirs = ();

while (1)
{
	sysread(FIFO, $sc, 1);

	$miogaconf = new Mioga2::MiogaConf($miogaconf_file);
	if(!defined $miogaconf) {
	    syslog('mail|err', "miogamailinglist.pl : Invalid Mioga.conf file.");
	    exit -1;
	}

	# getting list of directories into maildir
	# ----------------------------------------
	if (opendir(DIR, $maildir))
	{
		syslog('mail|debug', 'miogamailinglist : Succcessfully opened %s\n', $maildir) if ($debug);
	    my $instance = "";
	    while ($instance = readdir(DIR)) {

			next if($instance eq '..' or $instance eq '.');

			syslog('mail|debug', 'miogamailinglist : Processing instance %s\n', $instance) if ($debug);

			try {
				$config = new Mioga2::Config($miogaconf, $instance);
				
				if (opendir(DIRINST, "$maildir/$instance"))
				{
					my $filename = "";
					while ($filename = readdir(DIRINST))
					{
						if (-d "$maildir/$instance/$filename" && $filename ne '.' && $filename ne '..')
						{
							syslog('mail|debug', 'miogamailinglist : Processing file %s\n', $filename) if ($debug);
							try
							{
								my $group = new Mioga2::Old::Group($config, ident => $filename);
								my $groupid = $group->{$filename};
								push (@groupdirs, "$instance/$filename");
							}
							otherwise
							{
								syslog('mail|warning', 'miogamailinglist : <%s> is not a group\n', $filename);
								rename ("$maildir/$instance/$filename", "$maildirerror$filename");
							};
						}
					}
					
					closedir(DIRINST);
					
				}
			}

			otherwise {
				my $err = shift;

				syslog('mail|warning', 'miogamailinglist : <%s> is not an instance %s\n', $instance);
			};
		}
		closedir(DIR);

	}
	else
	{
	    syslog('mail|warning', 'miogamailinglist : cannot open dir <%s> for reading\n', $maildir);
	    exit -1;
	}
	
	# process all msg in each groupdir
	# --------------------------------
	foreach my $groupdir (@groupdirs)
	{
		my ($instance, $group) = split(/\//, $groupdir);

		syslog('mail|debug', 'miogamailinglist : Processing messages for %s\n', "$instance, $group") if ($debug);

		$config = new Mioga2::Config($miogaconf, $instance);

	    if (opendir(DIR, "$maildir$groupdir"))
	    {
			syslog('mail|debug', 'miogamailinglist : Successfully opened directory %s\n', "$maildir$groupdir") if ($debug);
			
			my $filename;
			my $filecontent;
			while($filename = readdir(DIR))
			{
				
				if (-f "$maildir$groupdir/$filename")
				{
					syslog('mail|debug', 'miogamailinglist : Processing message file %s\n', "$maildir$groupdir/$filename") if ($debug);
					my $msgfile = "$maildir$groupdir/$filename";
					if ($filename =~ /^msg.*/)
					{
						if (open(MSG, $msgfile))
						{
							{
								local $/ = undef;
								$filecontent = <MSG>;
								close(MSG);
							}
							MsgSendStock($config, $group, $msgfile, $filecontent);
						}
						else
						{
							syslog('mail|warning', 'miogamailinglist : cannot open <%s> for reading\n', $msgfile);
						}
					}
					else
					{
						syslog('mail|warning', 'miogamailinglist :  <%s> not generated by miogaqueue\n', $msgfile);
					}
				}
			}
			closedir(DIR);
			
	    }
	    else
	    {
		syslog('mail|warning', 'miogamailinglist : cannot open dir <%s> for reading\n', $groupdir);
	    }
	}

} # while daemon
closelog();
