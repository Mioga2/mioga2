// $Header$
// ============================================================================
// MIOGA Project (C) 1999-2001 ATRID SYSTEMES
//               (C) 2003 Alixen
//
//   This program is free software; you can redistribute it and/or modify it
//   under the terms of the GNU General Public License as published by the
//   Free Software Foundation; either version 2, or (at your option) any
//   later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ============================================================================
//
// Description:
// Attributes :
// ============================================================================
// $Log$
// Revision 1.1  2004/04/20 13:17:40  lsimonneau
// Add MiogaSearch in the Mioga Repository,
// reorganize the bin directory and
// update INSTALL file.
//
// Revision 1.3  2004/04/15 11:37:31  lsimonneau
// Minor bugfixes in MailingList.
// CHange session filename to user ident.
//
// Revision 1.2  2004/04/13 13:07:45  lsimonneau
// Major bugfixes in ML and mioga core.
//
// Revision 1.1  2004/03/09 10:58:56  lsimonneau
// First release of MailingList archive browser and mailing list daemon and scripts.
//
// Major features enhancements in Admin : Add hooks and enhance skeletons.
//
// Revision 1.4  2003/11/26 08:32:40  lsimonneau
// Minor bug fixes.
//
// Revision 1.3  2003/05/26 13:38:51  lsimonneau
// Update copyright informations.
//
// Major feature enhancement :
//  Add the SelectUser widget, build to select a list of Mioga users/groups.
//  Integrate this widget in Meeting/CreateMeating and AnimGroup/InviteUser.
//
// Revision 1.2  2003/02/21 14:35:38  lsimonneau
// Major code enhancements in MailingList daemon and mail handler.
// These two programs can read Mioga.conf (required arguments).
// /etc/mailinglist.conf does not exists anymore.
// All mailing list parameters were added to Mioga.conf.
//
// Revision 1.1.1.1  2003/01/06 14:24:38  lsimonneau
// Start Mioga
//
// Revision 1.2  2003/01/03 10:50:00  lsimonneau
// Minor bug fixes.
//
// Revision 1.1.1.1  2002/10/31 09:00:56  lsimonneau
// Alixen CVS creation
//
// Revision 1.1.1.1  2002/10/21 13:26:49  lsimonneau
// initial import for Mioga
//
// Revision 1.3  2002/07/12 08:29:02  clargeau
// changing mailing list to daemon mode
//
// Revision 1.2  2002/07/04 16:03:09  clargeau
// syslog error vs stderr
//
// Revision 1.1  2002/07/04 14:18:59  clargeau
// adding miogaqueue.c
//
//
// ============================================================================
#define BUFSIZE 1024
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <syslog.h>
#include <libxml/parser.h>


#if 0 
#define DEBUG(format, ...)    fprintf(stderr, format, __VA_ARG__)
#else
#define DEBUG(format, ...)
#endif


// ============================================================================
// Main program
// ============================================================================
main (int argc, char *argv[])
{
	char *confile;
	char *instance;
	char *group;

	char buf[BUFSIZE];

	char *maildir = NULL;
	char *maildirerror = NULL;
	char *fifo = NULL;
	char *writedir;
	char *writefile;
	struct stat filestat;

	int n;
	int tmpfd;
	int fp;

	xmlDocPtr  xmlDoc;
	xmlNodePtr rootElem;
	xmlNodePtr node;
	xmlNodePtr mailNode;
	
	openlog ("miogaqueue", LOG_PID, LOG_MAIL);
	
	if (argc != 4)
	{
		syslog (LOG_ERR, "miogaqueue [ERROR] usage : miogaqueue /path/to/Mioga.conf instance groupname\n");
		closelog();
		return -1;
	}

	confile  = argv[1];
	instance = argv[2];
	group    = argv[3];

	DEBUG ("miogaqueue [DEBUG] conf file : %s\n", confile);	
	DEBUG ("miogaqueue [DEBUG] groupname : %s\n", argv[2]);
	
	if ((xmlDoc = xmlParseFile(confile)) == NULL)
	{
		syslog (LOG_ERR, "miogaqueue [ERROR] cannot parse confile < %s >", confile);
		closelog();
		exit(-1);
	}

	// getting conf params
	// -------------------
	rootElem = xmlDocGetRootElement(xmlDoc);
		
	for(mailNode = rootElem->children; mailNode != NULL; mailNode = mailNode->next) 
	{
		if(mailNode->type != XML_ELEMENT_NODE) 
		{
			continue;
		}
	
		if(!strcmp(mailNode->name, "mailinglist")) {
	
			for(node = mailNode->children; node != NULL; node = node->next) 
			{
				if(node->type != XML_ELEMENT_NODE) 
				{
					continue;
				}
				
				if(!strcmp(node->name, "maildir")) 
				{
					maildir = (char *) malloc((strlen(node->children->content) + 1) * sizeof(char));
					strcpy(maildir, node->children->content);

					DEBUG ("miogaqueue [DEBUG] maildir : %s\n", maildir);
				}
				else if(!strcmp(node->name, "maildirerror")) 
				{
					maildirerror = (char *) malloc((strlen(node->children->content) + 1) * sizeof(char));
					strcpy(maildirerror, node->children->content);

					DEBUG ("miogaqueue [DEBUG] maildirerror : %s\n", maildirerror);
				}
				else if(!strcmp(node->name, "mailfifo")) 
				{
					fifo = (char *) malloc((strlen(node->children->content) + 1) * sizeof(char));
					strcpy(fifo, node->children->content);

					DEBUG ("miogaqueue [DEBUG] fifo : %s\n", fifo);
				}
			}
			
			break;
		}
	}

	xmlFreeDoc(xmlDoc);

	if(!fifo || !maildirerror || ! maildir) {
		syslog (LOG_ERR, "miogaqueue [ERROR] Invalid Mioga.conf\n");
		exit(-1);
	}

	// testing writedir existence
	// --------------------------
	if (stat (maildir, &filestat) == -1)
	{
		//fprintf (stderr, "miogaqueue [ERROR] cannot access < %s > for reading\n", writedir);
		syslog (LOG_ERR, "miogaqueue [ERROR] cannot access < %s > for reading\n", writedir);
		closelog();
		exit(-1);
	}

	if ((filestat.st_mode & S_IFMT) != S_IFDIR)
	{
		//fprintf (stderr, "miogaqueue [ERROR] < %s > is not a directory\n", writedir);
		syslog (LOG_ERR,  "miogaqueue [ERROR] < %s > is not a directory\n", writedir);
		closelog();
		exit(-1);
	}

	// create instance dir if needed
	writedir = (char *) malloc((strlen(maildir) + strlen(instance) + 2) * sizeof(char));

	strcpy (writedir, maildir);
	strcat (writedir, "/");
	strcat (writedir, instance);

	DEBUG ("miogaqueue [DEBUG] instancedir : <%s>\n", writedir);

	if ((mkdir(writedir, 0755) != -1))
	{
		// dir is created
		DEBUG ("miogaqueue [DEBUG] writedir : <%s> created\n", writedir);
	}
	else
	{
		// cannot create dir or already exists
		if (errno == EEXIST)
		{
			DEBUG ("miogaqueue [DEBUG] instancedir already exists : <%s>\n", writedir);
		}
		else
		{
			syslog (LOG_ERR, "miogaqueue [ERROR] cannot create instancedir : <%s>, errno : <%d> \n", writedir, errno);
			closelog();
			exit(-1);
		}
	}

	free(writedir);


	// create groupdir if needed
	writedir = (char *) malloc((strlen(maildir) + strlen(instance) + strlen(group) + 3) * sizeof(char));
	
	strcpy (writedir, maildir);
	strcat (writedir, "/");
	strcat (writedir, instance);
	strcat (writedir, "/");
	strcat (writedir, group);
	
	DEBUG ("miogaqueue [DEBUG] writedir : <%s>\n", writedir);

	if ((mkdir(writedir, 0755) != -1))
	{
		// dir is created
		DEBUG ("miogaqueue [DEBUG] writedir : <%s> created\n", writedir);
	}
	else
	{
		// cannot create dir or already exists
		if (errno == EEXIST)
		{
			DEBUG ("miogaqueue [DEBUG] writedir already exists : <%s>\n", writedir);
		}
		else
		{
			syslog (LOG_ERR, "miogaqueue [ERROR] cannot create writedir : <%s>, errno : <%d> \n", writedir, errno);
			closelog();
			exit(-1);
		}
	}

	// write stdin to output file
	// --------------------------
	writefile = (char *) malloc((strlen(writedir) + strlen("/msg.XXXXXX") + 1) * sizeof(char));

	strcpy(writefile, writedir);
	strcat(writefile, "/msg.XXXXXX");

	DEBUG ("miogaqueue [DEBUG] writefile : <%s>\n", writefile);

	if( (tmpfd = mkstemp(writefile)) < 0)
	{
		syslog (LOG_ERR, "miogaqueue [ERROR] cannot create message file : <%s>\n", writefile);
		closelog();
		exit(-1);
	}
	else
	{
		flock (tmpfd, LOCK_EX);
		fchmod(tmpfd, 0644);
		while ((n = read(0, buf, BUFSIZE)) > 0)
		{
			write(tmpfd, buf, n);
		}
		flock (tmpfd, LOCK_UN);
		close(tmpfd);
		
		// create fifo if needed
		// ---------------------
		if ((mkfifo(fifo, 0755) != -1))
		{
			// fifo is created
			DEBUG ("miogaqueue [DEBUG] fifo : <%s> created\n", fifo);
		}
		else
		{
			// cannot create fifo or already exists
			if (errno == EEXIST)
			{
				DEBUG ("miogaqueue [DEBUG] fifo already exists : <%s>\n", fifo);
			}
			else
			{
				syslog (LOG_ERR, "miogaqueue [ERROR] cannot create fifo : <%s>, errno : <%d> \n", fifo, errno);
				closelog();
				exit(0);
			}
		}
		
		if((fp = open(fifo, O_NONBLOCK|O_WRONLY)) == -1)
		{
			syslog (LOG_ERR, "miogaqueue [ERROR] cannot open fifo <%s> : %s", fifo, strerror(errno));
			closelog();
			exit(0);
		}
		else
		{
			if((write(fp, "Y", 1)) == -1)
			{
				DEBUG ("-----------> cannot write fifo errno : %s \n", strerror(errno));
			}
			else
			{
				DEBUG ("-----------> write fifo Ok\n");
			}
			close(fp);
		}
		
		return 0;
		
	}
}
