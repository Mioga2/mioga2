#!/usr/bin/perl -w

use Data::Dumper;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::tools::database;

if(@ARGV != 1) {
	warn "Usage : $0 /path/to/Mioga.conf";
	exit(-1);
}

my $miogaconf_file = $ARGV[0];
my $miogaconf = new Mioga2::MiogaConf($miogaconf_file);
my $dbh = $miogaconf->GetDBH();

my $mail = lc($ENV{EXT});

if($mail !~ /^(.+)\.([^\.]+)$/) {
        warn "Error : Can't deliver message. Bad email schemes.";

	exit(100);
		
}

my ($group, $instname) = ($1, $2);

$group = lc($group);
$instname = lc($instname);

my $res = SelectMultiple($dbh, "SELECT * FROM m_mioga WHERE lower(ident) = '$instname'");

if(@$res > 1) {
	warn "Error : Can't deliver message. There are more than one instance with ident $instname (case insensitive).";

	exit(100);
}


if(!@$res) {
	warn "Error : No instance named $instname.";
	exit(100);
}


my $config = new Mioga2::Config($miogaconf, $res->[0]->{ident});

$res = SelectMultiple($dbh, "SELECT m_group.* FROM m_group, m_group_type ".
					        "WHERE lower(m_group.ident) = '$group' AND ".
					        "      m_group.mioga_id = ".$config->GetMiogaId() . " AND ".
					        "      m_group_type.rowid = m_group.type_id AND ".
					        "      m_group_type.ident = 'group'");

if(@$res > 1) {
	warn "Error : Can't deliver message. There are more than one group with ident $group (case insensitive).";

	exit(100);
}


if(!@$res) {
	warn "Error : No group named $group.";
	exit(100);
}

exec($config->GetBinariesDir()."/miogaqueue", $config->GetMiogaConfPath(), $config->GetMiogaIdent(), $res->[0]->{ident});
