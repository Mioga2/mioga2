#!/usr/bin/perl -w

use strict;
use Sys::Syslog qw(:DEFAULT setlogsock);
use Mioga2::MiogaConf;
use Mioga2::tools::database;

my $debug = 0;

setlogsock('unix');
openlog ('mioga-postfix', 'cons, pid', 'user');

if(@ARGV != 3) {
    syslog('mail|err', "usage : mioga-postfix.pl domain_name /path/to/Mioga.conf recipient_addr");	
	exit(-1);
}

my ($domain, $miogaconf_file, $rcpt) = @ARGV;

my ($grp, $instance) = ($rcpt =~ /^([^\.]+)\.([^\@]+)\@$domain$/);

if(defined $grp and defined $instance) {
        ($grp, $instance) = ($1, $2);

		my $miogaconf = new Mioga2::MiogaConf($miogaconf_file);
		my $dbh = $miogaconf->GetDBH();
		
		my $mioga_id;
		my $res = SelectMultiple($dbh, "SELECT * FROM m_mioga WHERE lower(ident) = lower('$instance')");
		if(@$res > 1) {
			syslog("mail|err", "Error : Can't deliver message. There are more than one instance with ident $instance (case insensitive).");
			exit(-1);
		}
		elsif(@$res == 1) {
			$instance = $res->[0]->{ident};
			$mioga_id = $res->[0]->{rowid};
		}
		else {
			syslog("mail|err", "Error : Can't deliver message. Unknown instance $instance.");
			exit(-1);
		}

		$res = SelectMultiple($dbh, "SELECT * FROM m_group WHERE mioga_id=$mioga_id and lower(ident) = lower('$grp')");
		if(@$res > 1) {
			syslog("mail|err", "Error : Can't deliver message. There are more than one group with ident $grp (case insensitive).");
			exit(-1);
		}
		elsif(@$res == 1) {
			$grp = $res->[0]->{ident};
		}
		else {
			syslog("mail|err", "Error : Can't deliver message. Unknown group $grp in instance $instance.");
			exit(-1);
		}

        syslog('mail|debug', "miogaqueue.pl : instance => $instance, group => $grp\n") if $debug;
        syslog('mail|debug', "run : miogaqueue $miogaconf_file $instance $grp\n") if $debug;
		exec "miogaqueue $miogaconf_file $instance $grp";
}
else {
        syslog('mail|info', "miogaqueue.pl : $rcpt declined\n") if $debug;
}

