#!/usr/bin/perl -w

use strict;
use Sys::Syslog qw(:DEFAULT setlogsock);

my $debug = 0;

setlogsock('unix');
openlog ('mioga-postfix', 'cons, pid', 'user');

if(@ARGV != 3) {
    syslog('mail|err', "usage : mioga-postfix.pl domain_name /path/to/Mioga.conf recipient_addr");	
	exit(-1);
}

my ($domain, $miogaconf_file, $rcpt) = @ARGV;

my ($grp) = ($rcpt =~ /^([^\@]+)\@$domain$/);
my $instance = "Mioga";

if(defined $grp and defined $instance) {
        ($grp, $instance) = ($1, $2);

        syslog('mail|info', "miogaqueue.pl : instance => $instance, group => $grp\n") if $debug;
        syslog('mail|info', "run : miogaqueue $miogaconf_file $instance $grp\n") if $debug;
		exec "miogaqueue $miogaconf_file $instance $grp";
}
else {
        syslog('mail|info', "miogaqueue.pl : $rcpt declined\n") if $debug;
}

