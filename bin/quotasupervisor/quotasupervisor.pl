#!/usr/bin/perl -w
# $Header$
# ============================================================================
# MiogaII Project (C) 2003 The MiogaII Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#	Description: 

=head1 NAME
	
quotasupervisor.pl : Check instances quotas, warn administrators,
	                 lock/unlock the instances.
	                 Must be launched by cron.

=head1 USAGE

quotasupervisor.pl /path/to/Mioga.conf

=cut

#
# ============================================================================
# $Log$
# Revision 1.1  2004/09/20 07:48:00  lsimonneau
# Add quota support and Mioga2::Redirector
#
# ============================================================================
#

use strict;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::Old::User;
use Mioga2::tools::database;
use Mioga2::tools::date_utils;
use MIME::Parser;
use Error qw(:try);
use Data::Dumper;

# ------------------------------------------------------------------------------
# Return instance list
# ------------------------------------------------------------------------------
sub GetInstances {
	my $miogaconf = shift;

	my $dbh = $miogaconf->GetDBH();
	
	my $instances = SelectMultiple($dbh, "SELECT * FROM m_mioga");

	my @inst_ident = map {$_->{ident}} @$instances;
	return \@inst_ident;
}


# ------------------------------------------------------------------------------
# Display Usage and exit
# ------------------------------------------------------------------------------
sub Usage {
	print "Usage: $0 /path/to/Mioga.conf\n";
	exit(-1);
}




# ------------------------------------------------------------------------------
# Return current disk usage for an instance
# ------------------------------------------------------------------------------
sub GetDiskUsage {
	my $config = shift;

	my $private_dir = $config->GetPrivateDir();
	my $public_dir = $config->GetPublicDir();

	open(F_DU, "du -ks \"$private_dir\" | ")
		or die "Can't run du -ks \"$private_dir\": $!";

	my $private_dir_size = <F_DU>;
	chomp($private_dir_size);
	$private_dir_size =~ s/^([0-9]+).*$/$1/g;

	close(F_DU);

	open(F_DU, "du -ks \"$public_dir\" | ")
		or die "Can't run du -ks \"$public_dir\": $!";

	my $public_dir_size = <F_DU>;
	chomp($public_dir_size);
	$public_dir_size =~ s/^([0-9]+).*$/$1/g;

	close(F_DU);

	my $size = ($private_dir_size + $public_dir_size) / 1024;
	$size =~ s/^([0-9]+)(\.[0-9]+)$/$1/g;

	return $size;	
}


# ------------------------------------------------------------------------------
# Update warning date  for an instance
# ------------------------------------------------------------------------------
sub UpdateQuotaWarningDate {
	my $config = shift;

	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	
	ExecSQL($dbh, "UPDATE m_mioga SET quota_warning_date = now() WHERE rowid = $mioga_id");
}


# ------------------------------------------------------------------------------
# Lock an instance
# ------------------------------------------------------------------------------
sub LockInstance {
	my $config = shift;

	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	
	ExecSQL($dbh, "UPDATE m_mioga SET locked = TRUE WHERE rowid = $mioga_id");
}

# ------------------------------------------------------------------------------
# Unlock an instance
# ------------------------------------------------------------------------------
sub UnlockInstance {
	my $config = shift;

	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	
	ExecSQL($dbh, "UPDATE m_mioga SET locked = FALSE WHERE rowid = $mioga_id");
}

# ------------------------------------------------------------------------------
# Update disk usage for an instance
# ------------------------------------------------------------------------------
sub UpdateDiskUsage {
	my ($config, $disk_usage) = @_;

	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	
	ExecSQL($dbh, "UPDATE m_mioga SET disk_usage = $disk_usage WHERE rowid = $mioga_id");
}


# ------------------------------------------------------------------------------
# Send email to mioga administrator.
# ------------------------------------------------------------------------------
sub SendMailToAdmin {
	my ($config, $type, $current_du) = @_;

	my $admin_id = $config->GetAdminId();
	my $admin = new Mioga2::Old::User($config, rowid => $admin_id);

	my $file = $config->GetInstallPath()."/conf/mail/".$type.".mail";

	open(F_MAIL, $file) 
		or die "Can't open email message $file: $!";

	my $data;
	{
		local $/ = undef;
		$data = <F_MAIL>;
	}

	close(F_MAIL);

	my $quota_hard = $config->GetQuotaHard();
	my $mioga_ident = $config->GetMiogaIdent ();

	$data =~ s/\$\{quota_hard\}/$quota_hard/g;
	$data =~ s/\$\{current_du\}/$current_du/g;
	$data =~ s/\$\{instance_ident\}/$mioga_ident/g;

	my $parser = new MIME::Parser;
	$parser->output_under("/tmp");
	my $entity = $parser->parse_data($data);
	$admin->SendMail($entity);

	$parser->filer->purge;
}


# ------------------------------------------------------------------------------
# Check quota for an instance
# ------------------------------------------------------------------------------
sub CheckQuota {
	my $config = shift;

	my $current_du = GetDiskUsage($config);
	my $old_du = $config->GetDiskUsage();
	if(!defined $old_du) {
		$old_du = 0;
	}

	my $quota_hard = $config->GetQuotaHard();
	my $quota_soft = $config->GetQuotaSoft();
	my $locked = $config->IsLocked();

	my $warning_date = $config->GetWarningDate();
	if(defined $warning_date) {
		$warning_date = du_ISOToSecond($warning_date);
	}

	if(defined $quota_hard && $quota_hard != 0) {
		#
		# 2*hard quota excedeed ! Lock immediately !
		#	
		if($old_du < 2 * $quota_hard and 
		   $current_du >= 2 * $quota_hard) {
			UpdateQuotaWarningDate($config);
			LockInstance($config);
			SendMailToAdmin($config, "2hard_quota", $current_du);
		}
		
		#
		# hard quota excedeed since 1 day. Lock the instance.
		#
		elsif(! $locked and 
			  $old_du >= $quota_hard and $current_du >= $quota_hard and
			  ($warning_date + 86400) <= time) {
			UpdateQuotaWarningDate($config);
			LockInstance($config);
			SendMailToAdmin($config, "hard_quota_since_1_day", $current_du);
		}

		#
		# hard quota excedeed. Warn administrator.
		#
		elsif($old_du < $quota_hard and $current_du >= $quota_hard) {
			UpdateQuotaWarningDate($config);
			SendMailToAdmin($config, "hard_quota", $current_du);
		}

		#
		# Unlock when hard quota not excedeed anymore
		#
		elsif($old_du >= $quota_hard and $current_du < $quota_hard) {
			if($locked) {
				UnlockInstance($config);
				SendMailToAdmin($config, "finish_hard_quota_since_1_day", $current_du);
			}
			else {
				SendMailToAdmin($config, "finish_hard_quota", $current_du);
			}
			
			UpdateQuotaWarningDate($config);
		}

		elsif(defined $quota_soft && $quota_soft != 0) {
			#
			# soft quota excedeed. Warn administrator.
			#
			if($old_du < $quota_soft and $current_du >= $quota_soft) {
				UpdateQuotaWarningDate($config);
				SendMailToAdmin($config, "soft_quota", $current_du);
			}
			
			#
			# Update warning_date when soft quota not excedeed anymore
			#
			elsif($old_du >= $quota_soft and $current_du < $quota_soft) {
				UpdateQuotaWarningDate($config);
			}
		}
		
	}

	UpdateDiskUsage($config, $current_du);

	$config->{force_reload} = 1;
}


if(@ARGV != 1) {
	Usage();
}

try {
	
	my $miogaconf = new Mioga2::MiogaConf($ARGV[0]);
	
	my $instances = GetInstances($miogaconf);
	
	foreach my $inst (@$instances) {
		my $config = new Mioga2::Config($miogaconf, $inst);
		
		CheckQuota($config);
	}
}
otherwise {
	my $err = shift;
	warn Dumper($err);
};
