#!/usr/bin/perl -w
# ============================================================================
# MiogaII Project (C) 2003 The MiogaII Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description: 
#     This script checks the m_uri table for unconsistent values of user_id field.
#     Because of bug #776, some m_uri records have not been re-affected to group
#     animator, so user deletion becomes impossible. This bug is fixed and user
#     deletion is now possible unless the user has been revoked from a group where
#     he owned files.
#
#     This script will not change the DB. It will only scan it and create two SQL
#     scripts. One script will correct things and the second will revert to original
#     in case something goes wrong state.
#
#     The two scripts are named "do.sql" and "undo.sql".

use strict;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::APIApplication;
use Mioga2::tools::APIProfile;
use Error qw(:try);
use Term::ReadLine;
use Text::Iconv;
use Encode::Guess;
use File::Basename;
use Data::Dumper;

my $debug = 0;

my @params = qw//;
my %params = ();

my %usage = (
);

my %flags = (
);



# ================================================================
# Display usage
# ================================================================
sub Usage {
	print "Usage: " . basename($0) . " [OPTIONS]... INSTANCE\n";
}

# ================================================================
# Process command-line args
# ================================================================
sub ProcessArgs {
	my $arg = '';
	my $uninitialized = 1;

	# If at least a value is passed on the command-line, reset all values to zero
	if (grep (/^--/, @ARGV)) {
		for (@params) {
			$params{$_} = 0;
		}
	}

	# Process command-line arguments
	foreach (@ARGV) {
		if (/^--/) {
			my ($argname) = ($_ =~ m/^--(.*)/);
			if (grep (/^$argname$/, @params)) {
				$arg = $argname;
				$params{$arg} = 1 if ($arg eq 'debug');
			}
			else {
				print STDERR "Unknown argument $_\n";
				Usage;
				exit (-1);
			}
		}
		elsif (/^-[^-]/) {
			my ($flagname) = ($_ =~ m/^-(.*)/);
			if (grep (/^$flagname$/, %flags)) {
				$params{$flags{$flagname}} = 1;
			}
			else {
				print STDERR "Unknown flag $_\n";
				Usage;
				exit (-1);
			}
		}
		else {
			$arg = 'instance' if $arg eq '';
			$params{$arg} = $_;
			$arg = '';
		}
	}

	print STDERR "Parameters: " . Dumper \%params if ($debug);
}

# ================================================================
# GetInstance : Get id and values for instance
# ================================================================
sub GetInstance
{
	my ($dbh, $ident) = @_;

	my $sql = "select * from m_mioga where ident='$ident'";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		print "Instance : $ident not known\n";
		exit (-1);
	}
	return $res->{rowid};
}

# ================================================================
# GetWrongURIs : Get id and values for URIs whose user_id
#                is not member of the corresponding group
# ================================================================
sub GetWrongURIs {
	my ($dbh) = @_;

	my $sql = "SELECT URI.rowid, URI.uri, URI.group_id, URI.user_id, GRP.anim_id FROM m_uri URI, m_group_base GRP WHERE (URI.rowid,GRP.rowid) IN (SELECT rowid,group_id FROM m_uri WHERE rowid IN (SELECT rowid FROM m_uri WHERE (group_id,user_id) NOT IN (SELECT * FROM m_group_invited_user UNION SELECT * FROM m_group_expanded_user))) ORDER BY URI.rowid;";
	my $res = SelectMultiple($dbh, $sql);
	if (!defined($res)) {
		print "Can't access URIs\n";
		exit (-1);
	}

	return $res;
}

# ================================================================
# Main program
# ================================================================

# Get args and initialize things
if(@ARGV < 1) {
	Usage;
	exit(-1);
}

&ProcessArgs;

my $miogaconf_path = "/var/lib/Mioga2/conf/Mioga.conf";
my $miogaconf = new Mioga2::MiogaConf($miogaconf_path);
my $dbh = $miogaconf->GetDBH();
my $mioga_id = GetInstance($dbh, $params{instance});
my $config = new Mioga2::Config($miogaconf, $params{instance});

my $uris = &GetWrongURIs ($dbh);

my $do_script = 'do.sql';
my $undo_script = 'undo.sql';

if (-e $do_script || -e $undo_script) {
	print STDERR "$do_script or $undo_script files already exist, please move to another directory...\n";
	exit -1;
}

open (DO, ">$do_script");
open (UNDO, ">$undo_script");

my $do_header = "This script has been automatically generated to remove references from m_uri table orphaned because of bug #776.\nYou should have two complementary scripts, one to do things, the other one to undo.\nPlease ensure that you have the \"undo\" script (usually undo.sql) in case something goes wrong.";
my $undo_header = "This script has been automatically generated to recreate references from m_uri table orphaned because of bug #776.\nYou should have two complementary scripts, one to do things, the other one to undo.\nThis script can reverse things done by the \"do\" script (usually do.sql) in case something went wrong.";

$do_header =~ s/\n/\n-- /g;
$undo_header =~ s/\n/\n-- /g;
print DO "-- $do_header\n\n";
print UNDO "-- $undo_header\n\n";

foreach (@{$uris}) {
	my $comment = "${$_}{uri} (id=${$_}{rowid}, user=${$_}{user_id}, group=${$_}{group_id}, anim=${$_}{anim_id})\n";

	print $comment;

	print DO "-- $comment";
	print DO "UPDATE m_uri SET user_id=${$_}{anim_id} WHERE rowid=${$_}{rowid};\n";
	print DO "\n";

	print UNDO "-- $comment";
	print UNDO "UPDATE m_uri SET user_id=${$_}{user_id} WHERE rowid=${$_}{rowid};\n";
	print UNDO "\n";
}

close (DO);
close (UNDO);
