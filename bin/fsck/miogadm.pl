#!/usr/bin/perl -w
# ============================================================================
# MiogaII Project (C) 2003 The MiogaII Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description: 
#   This script permits to administrate easily th Mioga2 database
#   It's an interactive tool targeted to be use by system administrator
#   rather than Mioga2 administrator.
#
#   The commands are described in Help sub.
#

use strict;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::APIApplication;
use Mioga2::tools::APIProfile;
use Error qw(:try);
use Term::ReadLine;
use Text::Iconv;
use Encode::Guess;
use Getopt::Long;
use Data::Dumper;

my $debug = 0;

my $history_file = `echo ~/.miogadm`;

my $instance = "";
my $mioga_id = 0;
my $verbose = 0;
my $help = 0;
my $command = undef;
my $miogaconf_path = '/var/lib/Mioga2/conf/Mioga.conf';
my $miogaconf;
my $dbh;
my $prompt = 'miogadm # ';


# ================================================================
# FormatSimpleOutput : print Content of hash
# ================================================================
sub FormatSimpleOuput
{
	my ($hash) = @_;
	foreach my $key (keys(%$hash)) {
		print "$key = " . $hash->{$key} . "\n";
	}
}
# ================================================================
# FormatMultipleOutput : print Content of table of hash
# ================================================================
sub FormatMultipleOuput
{
	my ($table) = @_;
	foreach my $entry (@$table) {
		foreach my $key (keys(%$entry)) {
			print $entry->{$key} . "\t";
		}
		print "\n";
	}
}


#===============================================================================

=head2 Usage

Display command usage.

=cut

#===============================================================================
sub Usage {
	print "Usage : $0 [options]\n";
	print "    Options :\n";
	print "        --conf='path to Mioga.conf'                  : default to /var/lib/Mioga2/conf/Mioga.conf\n";
	print "        --verbose                                    : verbose mode\n";
	print "        --instance='name'                            : name of instance for some commands\n";
	print "        --help                                       : print usage\n";
	print "        -c \"command1 ; command2 ; ... ; commandN\"    : run commands then exit\n";

	return ();
}	# ----------  end of subroutine Usage  ----------

# ================================================================
# PrintHelp : print help text
# ================================================================
sub PrintHelp
{
	print "miogadm : administrate Migoa2 database\n";
	print "In commands description below, words enclosed by < > are variable\n";
	print " words enclosed by [ ] are optionnal\n";
	print "Commands :\n";
	print "instances :\n";
	print "\tList instances\n";
	print "instance [instance] :\n";
	print "\tinstance choice or print current instance\n";
	print "check :\n";
	print "\tcheck database integrity (could be long ...)\n";
	print "users :\n";
	print "\tlist users for instance\n";
	print "groups :\n";
	print "\tlist groups for instance\n";
	print "apps :\n";
	print "\tlist applications for instance\n";
	print "instapp package :\n";
	print "\tInstall application app so it can be added to instances. This function is automatically by addapp if required.\n";
	print "addapp package :\n";
	print "\tAdd application app in instance\n";
	print "updapp package :\n";
	print "\tUpdate application app in instance. This command is useful for applications that are not installed to instances by default.\n";
	print "delapp package :\n";
	print "\tDelete application app from instance\n";
	print "user rowid [all]:\n";
	print "\tprint infos on user defined by rowid\n";
	print "group rowid [all]:\n";
	print "\tprint infos on group defined by rowid\n";
	print "enapp group_id app_id :\n";
	print "\tEnable application app_id in group group_id\n";
	print "enappall (user|group) app_id (function_ident|-all) (profile_ident|-all) :\n";
	print "\tEnable application app_id for all users or groups enabeling function_id in named profile\n";
	print "reninst newname :\n";
	print "\tRename an instance to newname\n";
	print "impusers path :\n";
	print "\tImport users from CSV file\n";
	print "impgroup path :\n";
	print "\tImport groups from CSV file\n";
	print "\t --> lastname;firstname;email;ident;passwd;skeleton[;group1 group2 groupn]\n";
	print "delusers :\n";
	print "\tDelete ALL users for instance except admin\n";
	print "encoding :\n";
	print "\tChange file system encoding to file_system_encoding configuration variable\n";
}
# ================================================================
# GetInstance : Get id and values for instance
# ================================================================
sub GetInstance
{
	my ($dbh, $ident) = @_;

	my $sql = "select * from m_mioga where ident='$ident'";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		print "Instance : $ident not known\n";
	}
	$instance = $ident;
	$mioga_id = $res->{rowid};
	print "Instance  : " . $res->{ident} . "\n";
	print "rowid     : " . $res->{rowid} . "\n";
	print "modified  : " . $res->{modified} . "\n";
	print "admin_id  : " . $res->{admin_id} . "\n";
}
# ================================================================
# CheckDatabase : Check database integrity
# ================================================================
sub CheckDatabase
{
	my ($dbh) = @_;


	# instances
	#
	print "check integrity for instance ";
	my $sql = "select m_mioga.rowid, m_external_mioga.rowid from m_external_mioga left join m_mioga on (m_external_mioga.mioga_id = m_mioga.rowid) where m_mioga.rowid is NULL";
	my $res = SelectMultiple($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
	}
	if (scalar(@$res) > 0) {
		print "NOT OK\n";
	}
	else {
		print "OK\n";
	}
	# m_group_rowids
	#
	print "check integrity for m_group_rowids ";
	$sql = "select m_group_rowids.rowid, m_group_base.rowid from m_group_rowids left join m_group_base on (m_group_rowids.rowid = m_group_base.rowid) where m_group_rowids.rowid is NULL";
	$res = SelectMultiple($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
	}
	if (scalar(@$res) > 0) {
		print "NOT OK\n";
	}
	else {
		print "OK\n";
	}
	# m_group
	#
	print "check integrity for m_group ";
	$sql = "select m_group_rowids.rowid, m_group_base.rowid from m_group_base left join m_group_rowids on (m_group_rowids.rowid = m_group_base.rowid) where m_group_rowids.rowid is NULL";
	$res = SelectMultiple($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
	}
	if (scalar(@$res) > 0) {
		print "NOT OK\n";
	}
	else {
		print "OK\n";
	}

}
# ================================================================
# ListInstances : List all instances in database
# ================================================================
sub ListInstances
{
	my ($dbh) = @_;

	my $sql = "select * from m_mioga";
	my $res = SelectMultiple($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
	}
	my $format = "%5s | %15s | %26s |\n";
	printf $format,  "rowid", "ident", "modified";
	print "------+-----------------+----------------------------+\n";
	foreach my $entry (@$res) {
		printf $format, $entry->{rowid}, $entry->{ident}, $entry->{modified};
	}
}
# ================================================================
# ListUsers : List all users for instance
# ================================================================
sub ListUsers
{
	my ($dbh) = @_;

	my $sql = "select m_user_base.*, m_group_type.ident as type_ident from m_user_base, m_group_type where mioga_id='$mioga_id' and m_user_base.type_id=m_group_type.rowid";
	my $res = SelectMultiple($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
	}
	my $format = "%5s | %15s | %26s | %10s |\n";
	printf $format,  "rowid", "ident", "modified", "type";
	print "------+-----------------+----------------------------+------------+\n";
	foreach my $entry (@$res) {
		printf $format, $entry->{rowid}, $entry->{ident}, $entry->{modified}, $entry->{type_ident};
	}
}
# ================================================================
# ListGroups : List all groups for instance
# ================================================================
sub ListGroups
{
	my ($dbh) = @_;

	my $sql = "select m_group.*, m_group_type.ident as type_ident from m_group, m_group_type where mioga_id='$mioga_id' and m_group.type_id=m_group_type.rowid";
	my $res = SelectMultiple($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
	}
	my $format = "%5s | %-15s | %-26s | %-10s |\n";
	printf $format,  "rowid", "ident", "modified", "type";
	print "------+-----------------+----------------------------+------------+\n";
	foreach my $entry (@$res) {
		printf $format, $entry->{rowid}, $entry->{ident}, $entry->{modified}, $entry->{type_ident};
	}
}
# ================================================================
# ListApplications : List all applications for instance
# ================================================================
sub ListApplications
{
	my ($dbh) = @_;

	my $sql = "select m_application.*, m_application_type.ident as type_ident from m_application, m_instance_application, m_application_type where m_instance_application.mioga_id='$mioga_id' AND m_instance_application.application_id = m_application.rowid and m_application.type_id=m_application_type.rowid";
	my $res = SelectMultiple($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
	}
	my $format = "%5s | %-15s | %-26s | %-10s | %-25s|\n";
	printf $format,  "rowid", "ident", "modified", "type", "package";
	print "------+-----------------+----------------------------+------------+--------------------------+\n";
	foreach my $entry (@$res) {
		printf $format, $entry->{rowid}, $entry->{ident}, $entry->{modified}, $entry->{type_ident}, $entry->{package};
	}
}
# ================================================================
# PrintMethodsForProfile : Fancy printing of Profile
# ================================================================
sub PrintMethodsForProfile
{
	my ($methods) = @_;
	my %apps;

	foreach my $m (@$methods) {
		my $met_list = "";
		if (exists($apps{$m->{application}}->{$m->{function}})) {
			$met_list = $apps{$m->{application}}->{$m->{function}};
		}
		$apps{$m->{application}}->{$m->{function}} = $met_list . " " . $m->{method};
	}
	my $format = "%15s | %-15s | %s\n";
	foreach my $a (keys(%apps)) {
		my $hash = $apps{$a};
		foreach my $f (keys(%$hash)) {
			printf($format, $a, $f, $hash->{$f});
		}
	}
}
# ================================================================
# PrintUserData : Print user informations
# ================================================================
sub PrintUserData
{
	my ($dbh, $user_id, $option) = @_;

	my $sql = "select m_user_base.*, m_user_status.ident as status_ident from m_user_base, m_user_status where mioga_id=$mioga_id and m_user_base.rowid=$user_id and m_user_base.status_id=m_user_status.rowid";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
		return;
	}
	print "rowid     : " . $res->{rowid} . "\n";
	print "ident     : " . $res->{ident} . "\n";
	print "email     : " . $res->{email} . "\n";
	print "firstname : " . $res->{firstname} . "\n";
	print "lastname  : " . $res->{lastname} . "\n";
	print "status    : " . $res->{status_ident} . "\n";
	print "modified  : " . $res->{modified} . "\n";
	print "\n";
	# -------------------
	# user space
	# -------------------
	$sql = "select m_user_base.rowid, m_user_base.ident, m_profile.ident as profile"
			. " from m_user_base, m_group_group, m_profile_group, m_profile"
			." where m_group_group.group_id = $user_id"
			. " and m_group_group.invited_group_id=m_user_base.rowid"
			. " and m_profile.group_id = $user_id"
			. " and m_profile.rowid=m_profile_group.profile_id"
			. " and m_profile_group.group_id=m_user_base.rowid" ;

	$res = SelectMultiple($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
		return;
	}
	my $format = "%5s | %-15s | %-15s\n";
	print "List of users in user's space\n";
	print "=============================\n";
	printf $format,  "rowid", "ident", "profile";
	print "------+-----------------+---------------\n";
	foreach my $entry (@$res) {
		printf $format, $entry->{rowid}, $entry->{ident}, $entry->{profile};
	}
	print "\n";
	# -----------------------------------
	# applications in user's space
	# -----------------------------------
	$sql = "select m_application.* from m_application, m_application_group_allowed"
				." where m_application_group_allowed.group_id = $user_id"
				. " and m_application.rowid=m_application_group_allowed.application_id";

	my $apps = SelectMultiple($dbh, $sql);
	if (!defined($apps)) {
		print "Database error\n";
		return;
	}
	print "\nList of applications in user's space\n";
	print "======================================\n";
	foreach my $entry (@$apps) {
		print "$entry->{ident}($entry->{rowid})\n";
	}
	print "\n";
	# --------------------------------
	# profiles in user's space
	# --------------------------------
	$sql = "select m_profile.* from m_profile where m_profile.group_id = $user_id";

	my $profiles = SelectMultiple($dbh, $sql);
	if (!defined($profiles)) {
		print "Database error\n";
		return;
	}
	$format = "%5s | %-15s | %-15s\n";
	print "List of profiles for user's space\n";
	print "=================================\n";
	foreach my $entry (@$profiles) {
		print "Profile : $entry->{ident}($entry->{rowid})\n";
		$sql = "select m_application.ident as application, m_method.ident as method, m_function.ident as function"
					. " from m_application, m_function, m_method, m_profile_function"
					. " where m_profile_function.profile_id=$entry->{rowid}"
						. " and m_profile_function.function_id = m_function.rowid"
						. " and m_function.application_id=m_application.rowid"
						. " and m_method.function_id=m_function.rowid";
		my $methods = SelectMultiple($dbh, $sql);
		if (!defined($methods)) {
			print "Database error\n";
			return;
		}
		PrintMethodsForProfile($methods);
		print "----------------------------------------------------------------------------------\n";
	}
	# -------------------------
	# groups for user
	# -------------------------
	$sql = "select m_group.rowid, m_group.ident, m_profile.rowid as profile_rowid, m_profile.ident as profile"
			. " from m_group, m_group_group, m_profile_group, m_profile"
			." where m_group_group.group_id = m_group.rowid"
			. " and m_group.rowid=m_profile.group_id"
			. " and m_profile.rowid=m_profile_group.profile_id"
			. " and m_profile_group.group_id=$user_id"
			. " and m_group_group.invited_group_id=$user_id";

	my $groups = SelectMultiple($dbh, $sql);
	if (!defined($groups)) {
		print "Database error\n";
		return;
	}
	$format = "%5s | %-15s | %-15s\n";
	print "List of group accessed by user\n";
	print "==============================\n";
	foreach my $entry (@$groups) {
		print "Group : $entry->{ident}($entry->{rowid}) profile : $entry->{profile}($entry->{profile_rowid})\n";
		$sql = "select m_application.ident as application, m_method.ident as method, m_function.ident as function"
				. " from m_application, m_function, m_method, m_profile_function"
				. " where m_profile_function.profile_id=$entry->{profile_rowid}"
					. " and m_profile_function.function_id = m_function.rowid"
					. " and m_function.application_id=m_application.rowid"
					. " and m_method.function_id=m_function.rowid";
		my $methods = SelectMultiple($dbh, $sql);
		if (!defined($methods)) {
			print "Database error\n";
			return;
		}
		PrintMethodsForProfile($methods);
		print "----------------------------------------------------------------------------------\n";
	}
	print "\n";

}
# ================================================================
# PrintTeamDetails : Print detailed information for teams
# ================================================================
sub PrintTeamDetails
{
	my ($dbh, $group_id, $option) = @_;
	my $sql = "select m_user_base.rowid, m_user_base.ident from m_user_base, m_group_group where m_group_group.group_id=$group_id and m_group_group.invited_group_id=m_user_base.rowid";
	my $res = SelectMultiple($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
	}
	my $format = "%5s | %-15s \n";
	print "\nList of users in team\n";
	print "=======================\n";
	foreach my $entry (@$res) {
		printf($format, $entry->{rowid}, $entry->{ident});
	}
}
# ================================================================
# PrintResourceDetails : Print detailed information for resources
# ================================================================
sub PrintResourceDetails
{
	my ($dbh, $group_id, $option) = @_;
}
# ================================================================
# PrintGroupDetails : Print detailed information for groups
# ================================================================
sub PrintGroupDetails
{
	my ($dbh, $group_id, $option) = @_;

	# users
	# 
	my $sql = "select m_user_base.rowid, m_user_base.ident, m_profile.ident as profile"
			. " from m_user_base, m_group_group, m_profile_group, m_profile"
			." where m_group_group.group_id = $group_id"
			. " and m_group_group.invited_group_id=m_user_base.rowid"
			. " and m_profile.group_id = $group_id"
			. " and m_profile.rowid=m_profile_group.profile_id"
			. " and m_profile_group.group_id=m_user_base.rowid" ;

	my $res = SelectMultiple($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
	}
	my $format = "%5s | %-15s | %-15s\n";
	print "\nList of users in group\n";
	print "========================\n";
	printf $format,  "rowid", "ident", "profile";
	print "------+-----------------+---------------\n";
	foreach my $entry (@$res) {
		printf $format, $entry->{rowid}, $entry->{ident}, $entry->{profile};
	}
}
# ================================================================
# PrintGroupData : Print group informations
# ================================================================
sub PrintGroupData
{
	my ($dbh, $group_id, $option) = @_;

	my $sql = "select m_group.*, m_group_type.ident as type_ident from m_group, m_group_type where mioga_id=$mioga_id and m_group.rowid=$group_id and m_group.type_id=m_group_type.rowid";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
	}
	else {
		print "rowid     : " . $res->{rowid} . "\n";
		print "ident     : " . $res->{ident} . "\n";
		print "type      : " . $res->{type_ident} . "\n";
		print "modified  : " . $res->{modified} . "\n";

		if ($res->{type_ident} eq 'team') {
			PrintTeamDetails($dbh, $group_id, $option);
		}
		elsif ($res->{type_ident} eq 'resource') {
			PrintResourceDetails($dbh, $group_id, $option);
		}
		elsif ($res->{type_ident} eq 'group') {
			PrintGroupDetails($dbh, $group_id, $option);
		}
		else {
			print "Cannot unterstand type : $res->{type_ident}\n";
		}
	}
}
# ================================================================
# EnableAppInGroup : Enable an application in group
# ================================================================
sub EnableAppInGroup
{
	my ($dbh, $group_id, $app_id) = @_;
	# ----------------------------------
	# Get profiles for group
	# ----------------------------------
	my $sql = "select m_profile.rowid from m_profile where group_id=$group_id";

	my $profiles = SelectMultiple($dbh, $sql);
	if (!defined($profiles)) {
		print "Database error\n";
		return;
	}
	# ----------------------------------
	# Add apps if not present
	# ----------------------------------
	ApplicationEnableInGroup($dbh, $app_id, $group_id);
	# ----------------------------------
	# Get functions for application
	# ----------------------------------
	$sql = "select rowid from m_function where application_id=$app_id";

	my $functions = SelectMultiple($dbh, $sql);
	if (!defined($functions)) {
		print "Database error\n";
		return;
	}
	# ----------------------------------
	# Set profiles
	# ----------------------------------
	foreach my $prof (@$profiles) {
		foreach my $func (@$functions) {
			ProfileCreateFunction($dbh, $prof->{rowid}, $func->{rowid});
		}
	}
}
# ================================================================
# EnableAppForAll : Enable an application for all user or groups
# ================================================================
sub EnableAppForAll
{
	my ($dbh, $type, $app_id, $function, $profile) = @_;
	# ----------------------------------
	# sub request for group_id function of type
	# ----------------------------------
	my $sql_group_id;
	if ($type eq 'user') {
		$sql_group_id = "SELECT rowid FROM m_group_base WHERE type_id=3 or type_id=4";
	}
	else {
		$sql_group_id = "SELECT rowid FROM m_group_base WHERE type_id=1";
	}
	# ----------------------------------
	# Add apps if not present
	# ----------------------------------
	my $groups = SelectMultiple($dbh, $sql_group_id);
	if (!defined($groups)) {
		print "Database error\n";
	}
	if (scalar(@$groups) == 0) {
		print "No groups to process return\n";
		return;
	}
	foreach my $g (@$groups) {
		print STDERR "Enable application $app_id in group $g->{rowid}\n" if ($debug);
		ApplicationEnableInGroup($dbh, $app_id, $g->{rowid});
	}
	# ----------------------------------
	# Get function list
	# ----------------------------------
	my @func_ids;
	my $sql = "select ident, rowid from m_function where application_id=$app_id";

	my $functions = SelectMultiple($dbh, $sql);
	if (!defined($functions)) {
		print "Database error\n";
		return;
	}
	foreach my $f (@$functions) {
		if ( ($function eq '-all') || ($function eq $f->{ident}) ) {
			push @func_ids, $f->{rowid};
		}
	}
	if (scalar(@func_ids) == 0) {
		print "No function to process return\n";
		return;
	}
	# ----------------------------------
	# Set profiles
	# ----------------------------------
	$sql = "select m_profile.rowid, m_profile.ident from m_profile  where group_id in ($sql_group_id)";

	my $profiles = SelectMultiple($dbh, $sql);
	if (!defined($profiles)) {
		print "Database error\n";
		return;
	}
	foreach my $prof (@$profiles) {
		if ( ($profile eq '-all') || ($profile eq $prof->{ident}) ) {
			foreach my $f (@func_ids) {
				print "Process profile $prof->{ident} ($prof->{rowid}) for function $f\n";
				$sql = "SELECT * FROM m_profile_function WHERE profile_id = $prof->{rowid} AND function_id = $f";
				my $res = SelectSingle($dbh, $sql);
				if (!defined($res)) {
					ProfileCreateFunction($dbh, $prof->{rowid}, $f);
				}
				else {
					print "     already set\n";
				}
			}
		}
	}
}
# ================================================================
# AddApplication : add given application to instance
# ================================================================
sub AddApplication
{
	my ($dbh, $config, $package) = @_;

	my $mioga_id = $config->GetMiogaId ();

	# ----------------------------------
	# Get application data from package
	# ----------------------------------
	eval "require $package";
	my $apps = "$package"->new ($config);
	my $appdesc = $apps->GetAppDesc();

	# Create application if it does not exist yet
	if (!SelectSingle ($dbh, "SELECT * FROM m_application WHERE package = '" . st_FormatPostgreSQLString ($package) . "'")) {
		CreateApplication ($dbh, $config->GetMiogaConf (), $package);
	}

	# Set all_* flags to correct boolean value
	for my $flag (qw/all_users all_groups all_resources/) {
		if (!$appdesc->{$flag}) {
			$appdesc->{$flag} = 'f';
		}
		else {
			$appdesc->{$flag} = 't';
		}
	}

	# Make application available into instance
	ExecSQL ($dbh, "INSERT INTO m_instance_application (application_id, mioga_id, all_users, all_groups, all_resources) (SELECT m_application.rowid AS application_id, $mioga_id AS mioga_id, '$appdesc->{all_users}' AS all_users, '$appdesc->{all_groups}' AS all_groups, '$appdesc->{all_resources}' AS all_resources FROM m_application WHERE package = '" . st_FormatPostgreSQLString ($package) . "');");
}


# ================================================================
# CreateApplication : add given application to database
# ================================================================
sub CreateApplication
{
	my ($dbh, $miogaconf, $package) = @_;
	# ----------------------------------
	# Get application datas from package
	# ----------------------------------
	eval "require $package";
	my $apps = "$package"->new ($miogaconf);
	my $appdesc = $apps->GetAppDesc();
	# ----------------------------------
	# Create application, functions and methods
	# ----------------------------------
	my $app_rowid = ApplicationCreate($miogaconf, $appdesc);

	foreach my $func (keys %{$appdesc->{functions}})
	{
		my $func_rowid = FunctionCreate($dbh, $func, $appdesc->{functions}->{$func}, $app_rowid);

		foreach my $method (@{$appdesc->{func_methods}->{$func}})
		{
			my $method_rowid = MethodCreate($dbh, $method, $func_rowid, "normal");
		}

		foreach my $method (@{$appdesc->{xml_methods}->{$func}})
		{
			my $method_rowid = MethodCreate($dbh, $method, $func_rowid, "xml");
		}

		foreach my $method (@{$appdesc->{sxml_methods}->{$func}})
		{
			my $method_rowid = MethodCreate($dbh, $method, $func_rowid, "sxml");
		}
	}
}
# ================================================================
# UpdateApplication : update given application
# ================================================================
sub UpdateApplication
{
	my ($dbh, $config, $package) = @_;

	# ----------------------------------
	# Get application info from DB
	# ----------------------------------
	my $app = SelectSingle($dbh, "SELECT * FROM m_application WHERE m_application.package = '$package'");
	if (!defined($app)) {
		print ("Application $package not installed\n");
		return;
	}

	# ----------------------------------
	# Get application data from package
	# ----------------------------------
	eval "require $package";
	my $apps = "$package"->new ($config);
	my $appdesc = $apps->GetAppDesc();

	# ----------------------------------
	# Update application, functions and methods
	# ----------------------------------
	ApplicationUpdate ($config, $app->{rowid}, $appdesc);
	foreach my $func (keys %{$appdesc->{functions}})
	{
		my $res = SelectSingle($dbh, "SELECT * FROM m_function WHERE ident = '$func' AND
		                                                             application_id = $app->{rowid}");
		if(defined $res) {
			my $func_rowid = $res->{rowid};
			print "Update function $func\n" if ($verbose);
			FunctionUpdate ($dbh, $res->{rowid}, $func, $appdesc->{functions}->{$func});
			my $tbl_metd = $appdesc->{func_methods}->{$func};
			foreach my $metd (@$tbl_metd)
			{
				my $res = SelectSingle($dbh, "SELECT * FROM m_method WHERE ident = '$metd' AND
																		   function_id = $func_rowid");
				
				if(! defined $res) {
					print "Add method $metd\n" if $verbose;
					MethodCreate($dbh, $metd, $func_rowid, "normal");
				}
			}

			$tbl_metd = $appdesc->{xml_methods}->{$func};
			foreach my $metd (@$tbl_metd)
			{
				my $res = SelectSingle($dbh, "SELECT * FROM m_method WHERE ident = '$metd' AND
																		   function_id = $func_rowid");
				
				if(! defined $res) {
					print "Add method $metd\n" if $verbose;
					MethodCreate($dbh, $metd, $func_rowid, "xml");
				}
			}

			$tbl_metd = $appdesc->{sxml_methods}->{$func};
			foreach my $metd (@$tbl_metd)
			{
				my $res = SelectSingle($dbh, "SELECT * FROM m_method WHERE ident = '$metd' AND
																		   function_id = $func_rowid");
				
				if(! defined $res) {
					print "Add method $metd\n" if $verbose;
					MethodCreate($dbh, $metd, $func_rowid, "sxml");
				}
			}
		}
		else {
			print "Create function $func\n" if ($verbose);
			my $func_rowid = FunctionCreate ($dbh, $func, $appdesc->{functions}->{$func}, $app->{rowid});

			foreach my $method (@{$appdesc->{func_methods}->{$func}})
			{
				print "Create method $method\n" if ($verbose);
				my $method_rowid = MethodCreate ($dbh, $method, $func_rowid, "normal");
			}

			foreach my $method (@{$appdesc->{xml_methods}->{$func}})
			{
				print "Create method $method\n" if ($verbose);
				my $method_rowid = MethodCreate ($dbh, $method, $func_rowid, "xml");
			}

			foreach my $method (@{$appdesc->{sxml_methods}->{$func}})
			{
				print "Create method $method\n" if ($verbose);
				my $method_rowid = MethodCreate ($dbh, $method, $func_rowid, "sxml");
			}
		}
	}

	# Clean any removed functions
	for my $func (@{SelectMultiple ($dbh, "SELECT * FROM m_function WHERE application_id = $app->{rowid} AND ident NOT IN ('" . join ("', '", keys %{$appdesc->{functions}}) . "')")}) {
		print "Remove function $func->{ident}\n" if ($verbose);
		FunctionRemove ($dbh, $func->{rowid});
	}
}
# ================================================================
# DeleteApplication : delete given application from instance
# ================================================================
sub DeleteApplication
{
	my ($dbh, $config, $package) = @_;

	my $mioga_id = $config->GetMiogaId ();

	#-------------------------------------------------------------------------------
	# Get application parameters from DB
	#-------------------------------------------------------------------------------
	my $app = SelectSingle($dbh, "SELECT * FROM m_application, m_instance_application WHERE m_instance_application.application_id = m_application.rowid AND m_application.package = '$package' AND m_instance_application.mioga_id = $mioga_id");

	if (!$app) {
		print ("Application $package not installed into instance $mioga_id\n");
		return;
	}
	my $app_id = $app->{rowid};

	my $sql = "DELETE FROM m_application_group_allowed USING m_group WHERE m_application_group_allowed.group_id = m_group.rowid AND m_group.mioga_id = $mioga_id AND application_id = $app_id";
	ExecSQL($dbh, $sql);

	$sql = "DELETE FROM m_application_group USING m_group WHERE m_application_group.group_id = m_group.rowid AND m_group.mioga_id = $mioga_id AND application_id=$app_id";
	ExecSQL($dbh, $sql);

$sql = "DELETE FROM m_profile_function USING m_function, m_group, m_profile WHERE m_profile.group_id = m_group.rowid AND m_profile.rowid = m_profile_function.profile_id AND m_group.mioga_id = $mioga_id AND m_function.application_id = $app_id AND m_profile_function.function_id = m_function.rowid";
	ExecSQL($dbh, $sql);

	ExecSQL ($dbh, "DELETE FROM m_instance_application WHERE mioga_id = $mioga_id AND application_id = $app_id");
}
# ================================================================
# ImportUsers : Import users from a CSV file
# ================================================================
sub ImportUsers
{
	my ($dbh, $config, $filename) = @_;

	# ----------------------------------
	# Get admin id for current instance
	# ----------------------------------
	my $sql = "select admin_id from m_mioga where rowid=$mioga_id";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
		return;
	}
	my $admin_id = $res->{admin_id};
	# ----------------------------------
	# Read file and create users
	# ----------------------------------
	if (open (FILE, "<$filename")) {
		my $line;
		while ($line = <FILE>) {
			chomp($line);
			my @fields = split(';', $line);
			my $values = {};
			$values->{autonomous} = 1;
			$values->{public_part} = 0;
			$values->{lastname} = $fields[0];
			$values->{firstname} = $fields[1];
			$values->{email} = $fields[2];
			$values->{ident} = $fields[3];
			$values->{password} = $fields[4];
			$values->{skeleton} = $fields[5];
			$values->{creator_id} = $admin_id;

			my $user_id = UserLocalCreate($config, $values);
			print "Create user $values->{ident} : $user_id\n";

			if (length($fields[6]) > 0) {
				my @groups = split(' ', $fields[6]);
				foreach my $grp (@groups) {
					$sql = "select rowid from m_group where ident = '$grp' and mioga_id=$mioga_id";
					my $res = SelectSingle($dbh, $sql);
					if (!defined($res)) {
						print "Database error\n";
						return;
					}
					GroupInviteGroup($config->GetDBH(), $res->{rowid}, $user_id);
				}
			}
		}
	}
	else {
		print "Cannot open file : $filename\n";
	}
}
# ================================================================
# ImportGroups : Import groups from a CSV file
# ================================================================
sub ImportGroups
{
	my ($dbh, $config, $filename) = @_;

	# ----------------------------------
	# Get admin id for current instance
	# ----------------------------------
	my $sql = "select admin_id from m_mioga where rowid=$mioga_id";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
		return;
	}
	my $admin_id = $res->{admin_id};
	# ----------------------------------
	# Read file and create groups
	# ----------------------------------
	if (open (FILE, "<$filename")) {
		my $line;
		while ($line = <FILE>) {
			chomp($line);
			my @fields = split(';', $line);
			my $values = {};
			$values->{public_part} = 0;
			$values->{ident} = $fields[0];
			$sql = "select rowid from m_user_base where ident = '$fields[1]' and mioga_id=$mioga_id";
			my $res = SelectSingle($dbh, $sql);
			if (!defined($res)) {
				print "Database error\n";
				return;
			}
			$values->{anim_id} = $res->{rowid};
			$values->{skeleton} = $fields[2];
			$values->{creator_id} = $admin_id;

			my $group_id = GroupCreate($config, $values);
			print "Create group $values->{ident} : $group_id\n";
		}
	}
	else {
		print "Cannot open file : $filename\n";
	}
}
# ================================================================
# DeleteUsers : Delete all users for instance execpt admin
# ================================================================
sub DeleteUsers
{
	my ($dbh, $config) = @_;

	# ----------------------------------
	# Get admin id for current instance
	# ----------------------------------
	my $sql = "select admin_id from m_mioga where rowid=$mioga_id";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
		return;
	}
	my $admin_id = $res->{admin_id};
	# ----------------------------------
	# ----------------------------------
	# Get users list
	# ----------------------------------
	$sql = "select m_user_base.*, m_group_type.ident as type_ident from m_user_base, m_group_type where mioga_id='$mioga_id' and m_user_base.type_id=m_group_type.rowid";
	$res = SelectMultiple($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
	}
	# ----------------------------------
	# Delete each users of the list
	# ----------------------------------
	foreach my $user (@$res) {
		if ($user->{rowid} != $admin_id) {
			UserDelete($config, $user->{rowid});
		}
	}
}
# ================================================================
# RenameInstance : Rename an existing instance
# ================================================================
sub RenameInstance
{
	my ($dbh, $miogaconf, $newname) = @_;

	BeginTransaction($dbh);
	try {
		# ----------------------------------------------
		# Rename in m_mioga table
		# ----------------------------------------------
		my $sql = "update m_mioga set ident = '$newname' where rowid=$mioga_id";
		print STDERR "sql = $sql\n";
		my $res = ExecSQL($dbh, $sql);
		if (!defined($res)) {
			throw Error::Simple("Database error on sql = $sql\n");
		}
		# ----------------------------------------------
		# Rename in m_uri table
		# ----------------------------------------------
		$sql = "select m_uri.* from m_uri, m_group_base where m_uri.group_id=m_group_base.rowid and m_group_base.mioga_id=$mioga_id";
		my $uris = SelectMultiple($dbh, $sql);
		if (!defined($uris)) {
			throw Error::Simple("Database error on sql = $sql\n");
		}
		foreach my $uri (@$uris) {
			my $newuri = $uri->{uri};
			$newuri =~ s/$instance/$newname/;
			print STDERR "uri = $uri->{uri}  newuri = $newuri\n";
			$sql = "update m_uri set uri = '$newuri' where rowid=$uri->{rowid}";
			$res = ExecSQL($dbh, $sql);
			if (!defined($res)) {
				throw Error::Simple("Database error on sql = $sql\n");
			}
		}
		# ----------------------------------------------
		# delete in parser_cache table
		# ----------------------------------------------
		$sql = "delete from parser_cache where m_uri_id in (select m_uri.rowid from m_uri, m_group_base where  m_uri.group_id=m_group_base.rowid and m_group_base.mioga_id=$mioga_id)";
		$res = ExecSQL($dbh, $sql);
		if (!defined($res)) {
			throw Error::Simple("Database error on sql = $sql\n");
		}
		# ----------------------------------------------
		# delete in crawl_doc table
		# ----------------------------------------------
		$sql = "delete from crawl_doc where group_id in (select m_group_base.rowid from m_group_base where m_group_base.mioga_id=$mioga_id)";
		$res = ExecSQL($dbh, $sql);
		if (!defined($res)) {
			throw Error::Simple("Database error on sql = $sql\n");
		}
		# ----------------------------------------------
		# delete miogasearch DB
		# ----------------------------------------------
		my $dbpath = "$miogaconf->{install_dir}/$instance" . "$miogaconf->{mioga_files}/.MiogaWordsDatabase";
		if (unlink($dbpath) != 1) {
			throw Error::Simple("Cannot delete $dbpath\n");
		}
		# ----------------------------------------------
		# Rename in file system
		# ----------------------------------------------
		my $oldpath = "$miogaconf->{install_dir}/$instance";
		my $newpath = "$miogaconf->{install_dir}/$newname";
		print STDERR "oldpath = $oldpath  newpath = $newpath\n";
		if (! rename($oldpath, $newpath)) {
			throw Error::Simple("Cannot rename $oldpath in $newpath\n");
		}
		# ----------------------------------------------
		# Finish
		# ----------------------------------------------
		$instance = $newname;
		EndTransaction($dbh);
	} catch Error::Simple with {
		my $err = shift;
		RollbackTransaction($dbh);
		print "Error in renaming instance : " .  Dumper($err) . "\n";
	} otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		print "Error in renaming instance : " .  Dumper($err) . "\n";
	};
}
# ================================================================
sub BrowseDirectory
{
	my ($dbh, $base_path, $base_uri, $target_enc) = @_;
	my @urls;
	print "BrowseDirectory: base_path = $base_path base_uri = $base_uri\n";
	my $fh;
	opendir($fh, $base_path) or die "Cannot open directory $base_path";
	my @files = grep { !/^\./ } readdir($fh);
	#print scalar(@files) . " files to process\n";
	my $convToUTF8 = Text::Iconv->new("ISO-8859-1", "UTF-8");
	foreach my $f (@files) {
		my $path = "$base_path/$f";
		my $uri = "$base_uri/$f";
		#
		# Si target_enc = UTF-8, try to find ISO8859-1 to process
		#
		my $encoding;
		my $enc;
		$target_enc = "UTF-8";
		if ($target_enc eq 'UTF-8') {
			$enc = guess_encoding($f, qw/ascii UTF8/);
			if (! ref($enc)) {
				$enc = guess_encoding($f, qw/iso8859-1/);
				if (! ref($enc)) {
					print "Unknown encoding for file $path\n";
				}
				else {
					my $new_f = $convToUTF8->convert($f);
					if (defined($new_f) && ($new_f ne $f) ) {
						my $new_path = "$base_path/$new_f";
						if (-e $new_path) {
              $new_f =~ s/(\.\w+)?$/-time.$1/e;
              $new_path = "$base_path/$new_f";
            }
						print "rename: path = $path to $new_path\n";
						rename $path, $new_path;
						$path = $new_path;

						my $new_uri = "$base_uri/$new_f";
						#my $length = length($uri);
						my $length = "char_length('" . st_FormatPostgreSQLString($uri) . "')";
						my $sql = "update m_uri set uri = overlay(uri placing '" . st_FormatPostgreSQLString($new_uri) . "' from 1 for $length) where uri like '" . st_FormatPostgreSQLString($uri) . "%'";
						print "sql = $sql\n";
						try {
							ExecSQL($dbh, $sql);
						}
						otherwise {
							my $err = shift;
							print STDERR "SQL error on uri : $uri\n" . Dumper($err) . "\n";
						};
						$uri = $new_uri;
					}
				}
			}
		}
		if (-d $path && !-l $path) {
			push @urls, BrowseDirectory($dbh, $path, $uri, $target_enc);
		}
	}
	closedir($fh);
	return @urls;
}
# ================================================================
# ChangeEncoding : Set encoding of filenames to filesystem_encoding
# configuration variable
# ================================================================
sub ChangeEncoding
{
	my ($dbh, $miogaconf) = @_;
	my $offset = 0;
	my $limit = 200;
	my $sql;
	my $converter;
	my $encoding = $miogaconf->GetFilenameEncoding();
	my $install_dir = $miogaconf->GetInstallDir();
	my $base_uri = $miogaconf->GetBasePath();
	my $base_dir = $miogaconf->GetBaseDir();
	my $private_uri = $miogaconf->GetPrivatePath();
	my $public_uri = $miogaconf->GetPublicPath();
	my $private_dir = $miogaconf->GetPrivateDir();
	my $public_dir = $miogaconf->GetPublicDir();
	#$install_dir =~ s/$base_uri$//;
	print "encoding: $encoding\n";

	if ($encoding eq 'UTF-8') {
		$converter = Text::Iconv->new("ISO-8859-1", "UTF-8");
	}
	else {
		$converter = Text::Iconv->new("UTF-8", "ISO-8859-1");
	}
	#
	# Get all instances
	#
	my $result = SelectMultiple($dbh, "select ident from m_mioga");
	my @instances;
	foreach my $ident (@$result) {
		push @instances, $ident->{ident};
	}
	#
	# Change filename on filesystem for each instances and construct
	# the URI list to change
	#
	my @urls;
	foreach my $inst (@instances) {
		my $base_path = "$install_dir/$inst$base_dir$private_dir";
		my $base_uri = "$base_uri/$inst$private_uri";
		print "instance: $inst\n       base_path = $base_path\n      base_uri = $base_uri\n";
		BrowseDirectory($dbh, $base_path, $base_uri, $encoding);
		$base_path = "$install_dir/$inst$base_dir$public_dir";
		$base_uri = "$base_uri/$inst$public_uri";
		print "instance: $inst\n       base_path = $base_path\n      base_uri = $base_uri\n";
		BrowseDirectory($dbh, $base_path, $base_uri, $encoding);
	}
}


#===============================================================================

=head2 RunCommand

Run a miogadm command.

=cut

#===============================================================================
sub RunCommand {
	my ($cmd) = @_;

	my @args = split /[\s]+/, $cmd;
		
	return if (scalar(@args) == 0);

	# instance choice
	#
	if ($args[0] eq 'instance') {
		if (defined($args[1])) {
			GetInstance($dbh, $args[1]);
		}
		else {
			print "\tcurrent instance = $instance ($mioga_id)\n";
		}
	}
	# Check integrity
	#
	elsif ($args[0] eq 'check') {
		CheckDatabase($dbh);
	}
	#
	# list instances
	#
	elsif ($args[0] eq 'instances') {
		ListInstances($dbh);
	}
	#
	# list users
	#
	elsif ($args[0] eq 'users') {
		ListUsers($dbh);
	}
	# list groups
	#
	elsif ($args[0] eq 'groups') {
		ListGroups($dbh);
	}
	# list applications
	#
	elsif ($args[0] eq 'apps') {
		ListApplications($dbh);
	}
	# addapp
	#
	elsif ($args[0] eq 'addapp') {
		if (defined($args[1])) {
			my $config = new Mioga2::Config($miogaconf, $instance);
			AddApplication($dbh, $config, $args[1]);
		}
		else {
			PrintHelp();
		}
	}
	# updapp
	#
	elsif ($args[0] eq 'updapp') {
		if (defined($args[1])) {
			my $config = new Mioga2::Config($miogaconf, $instance);
			UpdateApplication($dbh, $config, $args[1]);
		}
		else {
			PrintHelp();
		}
	}
	# delapp
	#
	elsif ($args[0] eq 'delapp') {
		if (defined($args[1])) {
			my $config = new Mioga2::Config($miogaconf, $instance);
			DeleteApplication($dbh, $config, $args[1]);
		}
		else {
			PrintHelp();
		}
	}
	# instapp
	#
	elsif ($args[0] eq 'instapp') {
		if (defined($args[1])) {
			CreateApplication($dbh, $miogaconf, $args[1]);
		}
		else {
			PrintHelp();
		}
	}
	
	# user datas
	#
	elsif ($args[0] eq 'user') {
		if (defined($args[1])) {
			if (defined($args[2])) {
				PrintUserData($dbh, $args[1], $args[2]);
			}
			else {
				PrintUserData($dbh, $args[1], 'all');
			}
		}
		else {
			PrintHelp();
		}
	}
	# group datas
	#
	elsif ($args[0] eq 'group') {
		if (defined($args[1])) {
			if (defined($args[2])) {
				PrintGroupData($dbh, $args[1], $args[2]);
			}
			else {
				PrintGroupData($dbh, $args[1], 'all');
			}
		}
		else {
			PrintHelp();
		}
	}
	#
	# Repair user's workspace
	#
	elsif ($args[0] eq 'enapp') {
		if (defined($args[1]) && defined($args[2]) ) {
			EnableAppInGroup($dbh, $args[1], $args[2]);
		}
		else {
			PrintHelp();
		}
	}
	#
	# enable app for user or group
	#
	elsif ($args[0] eq 'enappall') {
		if (defined($args[1]) && defined($args[2]) && defined($args[3]) && defined($args[4]) ) {
			EnableAppForAll($dbh, $args[1], $args[2], $args[3], $args[4]);
		}
		else {
			PrintHelp();
		}
	}
	#
	# Import users
	#
	elsif ($args[0] eq 'impusers') {
		my $config = new Mioga2::Config($miogaconf, $instance);
		if (defined($args[1])) {
			ImportUsers($dbh, $config, $args[1]);
		}
		else {
			PrintHelp();
		}
	}
	#
	# Import groups
	#
	elsif ($args[0] eq 'impgroups') {
		my $config = new Mioga2::Config($miogaconf, $instance);
		if (defined($args[1])) {
			ImportGroups($dbh, $config, $args[1]);
		}
		else {
			PrintHelp();
		}
	}
	#
	# Delete users
	#
	elsif ($args[0] eq 'delusers') {
		my $config = new Mioga2::Config($miogaconf, $instance);
		DeleteUsers($dbh, $config);
	}
	#
	# Rename instance
	#
	elsif ($args[0] eq 'reninst') {
		if (defined($args[1])) {
			RenameInstance($dbh, $args[1]);
		}
		else {
			PrintHelp();
		}
	}
	#
	# Change file system encoding
	#
	elsif ($args[0] eq 'encoding') {
		ChangeEncoding($dbh, $miogaconf);
	}
	#
	# quit miogadm
	#
	elsif ($args[0] eq 'quit') {
	}
	#
	# Verbose mode
	#
	elsif ($args[0] eq 'v') {
		$verbose = $verbose ? 0 : 1;
		print "Verbosity: $verbose\n";
	}
	# help text if command not recognized
	#
	else {
		PrintHelp();
	}
}	# ----------  end of subroutine RunCommand  ----------


# ================================================================
# Main program
# ================================================================

my $result = GetOptions ("help" => \$help,
			          "conf=s"  => \$miogaconf_path,
			          "instance=s"  => \$instance,
					  "c=s" => \$command,
			          "verbose"  => \$verbose);

print "Getopt result = $result\n" if ($debug);

if ($help) {
	Usage ();
	print "\n";
	PrintHelp ();
	exit(-1);
}

$miogaconf = new Mioga2::MiogaConf($miogaconf_path);
$dbh = $miogaconf->GetDBH();

if ($instance) {
	RunCommand ("instance $instance");
}

if ($command) {
	# Run in command-line mode
	print "miogadm command-line mode\n";
	my @commands = split (';', $command);
	for (@commands) {
		$_ =~ s/[ ]*(.*)[ ]*/$1/;
		print "$prompt$_\n";
		RunCommand ($_);
		print "\n";
	}
}
else {
	# Run in interactive mode
	print "miogadm hello, type in commands, h for help, v for verbose ...\n";

	my $line = "";
	my $continue = 1;
	my $term= new Term::ReadLine("miogadm");
	$term->ornaments(0);
	$term->using_history;
	$term->ReadHistory($history_file) || warn "Cannot read history file : $history_file";
	while (defined($line = $term->readline($prompt))) {
		chomp($line);
		$line =~ s/^[\s]*//;
		$line =~ s/[\s]*$//;
		#$term->addhistory(join(' ', @args)) || warn "Cannot add history";

		RunCommand ($line);
	}

	$term->WriteHistory($history_file) || warn "Cannot write history file : $history_file";
	$term->history_truncate_file($history_file, 100);
	print "\n ... good bye ...\n";
}
