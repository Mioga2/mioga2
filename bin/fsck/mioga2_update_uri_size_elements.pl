#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  mioga2_update_uri_size_elements.pl
#
#        USAGE:  ./mioga2_update_uri_size_elements.pl --help
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  11/05/2012 09:02
#
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME
	
mioga2_update_uri_size_elements.pl

=head1 DESCRIPTION

This script update m_uri contents.

=head1 SYNOPSIS

mioga2_update_uri_size_elements.pl [options]

 --help          show this help message
 --debug         enable debugging messages
 --verbose       enable debugging messages
 --conf          path to Mioga.conf, defauts to /var/lib/Mioga2/conf/Mioga.conf
 --path          base path to work on (as expressed in m_uri), defaults to /Mioga2
 --recurse       recurse through sub-directories, default mode, can be overriden with --norecurse
 --norecurse     do not recurse through sub-directories
 --parents       update parent directories, default mode, can be overriden with --noparents
 --noparents     do not update parent directories

=head1 METHODS DESRIPTION

=cut

#
#===============================================================================

use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;
use Error qw/:try/;
use Data::Dumper;

use Mioga2::MiogaConf;
use Mioga2::Database;


#-------------------------------------------------------------------------------
# Set default options and parse command line
#-------------------------------------------------------------------------------
my %options = (
		help    => sub { pod2usage (-verbose => 99, -sections => 'DESCRIPTION|SYNOPSIS', -exit => 0) },
		conf    => '/var/lib/Mioga2/conf/Mioga.conf',
		path    => '/Mioga2',
		recurse => 1,
		parents => 1,
	);
GetOptions (\%options, 'help', 'debug|verbose', 'conf=s', 'recurse!', 'path=s', 'parents!') or pod2usage (-verbose => 99, -sections => 'DESCRIPTION|SYNOPSIS', -exit => 2);

print "Selected options: " . Dumper \%options if ($options{debug});


#-------------------------------------------------------------------------------
# Perform various checks and processings on options
#-------------------------------------------------------------------------------
$options{path} =~ s/\/\//\//;
$options{path} =~ s/\/$//;
print "Working on path: $options{path}\n" if ($options{debug});


#-------------------------------------------------------------------------------
# Load configuration and connect to database
#-------------------------------------------------------------------------------
my $miogaconf = new Mioga2::MiogaConf ($options{conf});
# my $dbh = $miogaconf->GetDBH ();
my $db = $miogaconf->GetDBObject ();


my @uris;

#-------------------------------------------------------------------------------
# Push sub-directories to list
#-------------------------------------------------------------------------------
if ($options{recurse}) {
	my $request = $options{recurse} ? 'SELECT * FROM m_uri WHERE mimetype = ? AND (uri SIMILAR TO ? OR uri = ?) ORDER BY uri DESC' : 'SELECT * FROM m_uri WHERE mimetype = ? AND uri = ?';
	my $args = $options{recurse} ? ['directory', "$options{path}/%", $options{path}] : ['directory', $options{path}];
	for my $dir (@{$db->SelectMultiple ($request, $args)}) {
		push (@uris, $dir->{uri});
	}
}

#-------------------------------------------------------------------------------
# Push parent directories to list
#-------------------------------------------------------------------------------
if ($options{parents}) {
	my @parts = split (/\//, $options{path});
	while ((pop (@parts) ne '') && (scalar (@parts)) > 1) { # scalar (@parts) > 1 because split above introduced an empty element due to leading slash
		push (@uris, join ('/', @parts));
	}
}

for my $uri (@uris) {
	print "Updating URI $uri\n" if ($options{debug});
	try {
		$db->BeginTransaction ();
		UpdateInfo ($db, $uri);
		$db->EndTransaction ();
	}
	otherwise {
		my $err = shift;
		$db->RollbackTransaction ();

		print STDERR "Update failed for URI $uri\n";
		print STDERR $err->stringify () . "\n";
	};
}


#===============================================================================

=head2 UpdateInfo

Update directory information (elements and size) from sub-elements information

B<Note:> obviously, if sub-elements information is inconsistent, the resulting
information won't be more!

=head3 Incoming Arguments

=over

=item B$uri: a string containing the the uri to work on

=back

=head3 Return value

None. If something wrong happens, an exception is thrown.

=back

=cut

#===============================================================================
sub UpdateInfo {
	my ($db, $uri) = @_;

	my $request = "UPDATE m_uri SET elements = info.elements, size = info2.size FROM (SELECT count(*) AS elements FROM m_uri WHERE uri LIKE ?) AS info, (SELECT COALESCE (sum(size), 0) AS size FROM m_uri WHERE uri LIKE ? AND mimetype != 'directory') AS info2 WHERE uri = ?";
	my $args = [$uri . '/%', $uri . '/%', $uri];
	$db->ExecSQL ($request, $args);
}	# ----------  end of subroutine UpdateInfo  ----------
