#!/usr/bin/perl

use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::URI;
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::APIAuthz;
use File::MimeInfo::Magic;

my $debug = 1;

if(@ARGV < 1) {
	print "Usage : $0 /path/to/Mioga.conf [instance sub_path_to_check]\n";
	exit(-1);
} 


my $miogaconf_path = $ARGV[0];
my $instance = $ARGV[1];
my $path_to_check = $ARGV[2];


my $miogaconf = new Mioga2::MiogaConf($miogaconf_path);
my $dbh = $miogaconf->GetDBH();

my @instances;
if( defined $instance) {
	my $config = new Mioga2::Config($miogaconf, $instance);
	RemoveBadDBEntries($config, $path_to_check);
	DeclareNewFiles   ($config, $path_to_check);
}
else {

	my $res = SelectMultiple($dbh, "SELECT ident FROM m_mioga");

	@instances = map {$_->{ident}} @$res;


	foreach $instance (@instances) {
		print "Check $instance\n";
		my $config = new Mioga2::Config($miogaconf, $instance);
		RemoveBadDBEntries($config);
		DeclareNewFiles   ($config);	
	}
}




sub RemoveBadDBEntries {
	my ($config, $path_to_check) = @_;
	print STDERR "RemoveBadEntries path_to_check $path_to_check\n" if ($debug);

	my $base_path = $config->GetWebdavDir();
	my $base_uri = $config->GetBaseURI();

	my $dbh = $config->GetDBH();

	if(!defined $path_to_check) {
		$path_to_check = "";
	}

	my $uris = SelectMultiple($dbh, 
							  "SELECT m_uri.* FROM m_uri, m_group_base ".
							  "WHERE m_uri.uri ~ '^$path_to_check' AND ".
							  "      m_group_base.rowid = m_uri.group_id AND ".
							  "      m_group_base.mioga_id = ".$config->GetMiogaId());

	foreach my $uri (@$uris) {
		my $file = $uri->{uri};

		$file =~ s/^$base_uri/$base_path/g;

		if(! -e $file or $file =~ /\/\.DAV$/) {
			print "$uri->{uri} does not exists. Remove from database\n";
			AuthzDeleteURI($dbh, $uri->{uri});
		}
	}
	
}



sub DeclareNewFiles {
	my ($config, $path_to_check) = @_;
	print STDERR "DeclareNewFiles path_to_check $path_to_check\n" if ($debug);

	my $base_path = $config->GetWebdavDir();
	my $base_uri = $config->GetBaseURI();
	print STDERR "base_path $base_path    base_uri $base_uri\n" if ($debug);

	my $dbh = $config->GetDBH();

	if(!defined $path_to_check) {
		DeclareNewFiles( $config, $config->GetPrivateURI());
		DeclareNewFiles( $config, $config->GetPublicURI());
		return;
	}

	my $uri  = SelectMultiple ($dbh, 
							   "SELECT m_uri.* FROM m_uri, m_group_base ".
							   "WHERE m_uri.uri ~ '^$path_to_check' AND ".
							   "      m_group_base.rowid = m_uri.group_id AND ".
							   "      m_group_base.mioga_id = ".$config->GetMiogaId());

 	my %uris;
 	foreach my $file (@$uri) {
 		$uris{$file->{uri}} = 1;
 	}
	
	$path_to_check =~ s/^$base_uri/$base_path/;

	open(F_FIND, "find $path_to_check |") or die "Can't run find : $!";

	while(my $file = <F_FIND>) {
		chomp($file);

		next if $file =~ /\/\.DAV$/;
		$file =~ s/^$base_path/$base_uri/;
	
		if(! exists $uris{$file}) {
			print STDERR "doesn't exists ".$uris{$file}."\n" if ($debug);
			next if ($file eq $config->GetPrivateURI());
			next if ($file eq $config->GetPublicURI());
			print "$file does not exists. Add in database.\n";
			
			my $mioga_uri    = Mioga2::URI->new($config, uri => $file);
			my $group_ident  = $mioga_uri->GetGroupIdent;
			my $mioga_ident  = $mioga_uri->GetMiogaIdent;
			my $anim         = SelectSingle($dbh, "SELECT m_group_base.anim_id FROM m_group_base, m_mioga WHERE m_group_base.ident = '".st_FormatPostgreSQLString($group_ident)."' AND m_mioga.rowid=m_group_base.mioga_id AND m_mioga.ident = '".st_FormatPostgreSQLString($mioga_ident)."'");
			if (!defined($anim)) {
				print STDERR "CAnnot fin animator for group_ident = $group_ident and mioga_ident = $mioga_ident\n";
				exit(-1);
			}
			
			my $mime       = mimetype($mioga_uri->GetRealPath);
			utf8::encode($mime);
			$mime = 'directory' if $mime =~ /directory/;
			
			print STDERR "group_ident $group_ident    mioga_ident $mioga_ident  anim_id = $anim->{anim_id} mime = $mime\n" if ($debug);
			AuthzDeclareURI($dbh, $file, $anim->{anim_id}, $mime, 0);
		}
	}
	
}
