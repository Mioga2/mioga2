We create a custom Dojo build, which will include some standard classes and some
of our own, according to the official documentation:
http://www.dojotoolkit.org/reference-guide/build/index.html

We need two repository modules: Mioga2 (this file is part of it) and the Dojo module.
Suppose they are checked out in $MIOGA2_MODULE and $DOJO_MODULE,
and that our current working directory is the one of this file (tools/dojo/mioga_src).

In the subdirectory "layers/" there are definitions for different layers. Each layer
wraps a number of dojo.require() calls into one single big file. Dojo files that
are used by at least two applications should go in here. Files that are only needed
once might be used individually or contained in another layer.

There is one single build profile that should include all layers. This is
"mioga.profile.js", in this directory.

For the build process you need Java, at least version 1.4.
Then execute:
###

cp -R mioga_src $DOJO_MODULE/mioga
# maybe we want to delete other Dijit themes? 
cp -R dijit_themes/mioga $DOJO_MODULE/dijit/themes/

# Cleanup debug messages, useful to developers but useless in prod
find $DOJO_MODULE/mioga -name "*.js" -exec sed -i /"^[[:space:]]*console.\(log\|dir\|debug\)[[:space:]]*("/d {} \;

export TOOLSDOJO=$PWD
cd $DOJO_MODULE/util/buildscripts

export RELEASE=mioga-x.y.z
# you might want to take a SVN revision number or Mioga target version
./build.sh profileFile=$TOOLSDOJO/mioga.profile.js  action=clean,release releaseName=$RELEASE
for f in $TOOLSDOJO/mioga_src/layers/*.js; do BASE=`basename $f`; cp $f $DOJO_MODULE/release/$RELEASE/mioga/layers/${BASE%.js}.dev.js; done

# clean up our temporary modifications
rm -rf $DOJO_MODULE/dijit/themes/mioga
rm -rf $DOJO_MODULE/mioga

###
The release consists of all individual Dojo files as well as our compressed "layers".
It is in the directory $DOJO_MODULE/release/$RELEASE and can be copied
and checked into the Mioga2 module (web/jslib/dojotoolkit).
As there is probably already a versioned directory of this name, we must take care
of Subversion's state, as manifested in the .svn directories. The following is
based on a "poor man's svn addremove", see
http://www.selenic.com/pipermail/mercurial/2007-September/014709.html
###

cd $MIOGA2_MODULE/web/jslib/dojotoolkit

# Remove all versioned files, leave state intact. The single "rm"s can take some time.
find . -type f | fgrep -v .svn/ | while read fname ; do rm "${fname}" ; done

# Copy new content here
cp -R $DOJO_MODULE/release/$RELEASE/* .

# Update Subversion's state
export TMPFILE=/tmp/svn_state_mioga
svn stat > $TMPFILE
grep '^? ' $TMPFILE | cut -c 3- | while read fname ; do svn add "${fname}" ; done
grep '^! ' $TMPFILE | cut -c 3- | while read fname ; do svn rm "${fname}" ; done
rm $TMPFILE
