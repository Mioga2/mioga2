define([
	"dojo/_base/declare", // declare
	"dojo/dom-attr", // domAttr.set
	"dojo/_base/event", // event.stop
	"dojo/_base/kernel", // kernel.deprecated
	"dojo/_base/sniff", // has("ie")
	"dijit/_Widget",
	"dijit/_Templated",
	"dijit/form/_FormMixin"
], function(declare, domAttr, event, kernel, has, _Widget, _Templated, _FormMixin){

/* ----------------------------------------------------------
 * mioga.GroupAttributes
 * ---------------------------------------------------------- */
	return declare("mioga.GroupAttributes", [_Widget, _Templated, _FormMixin], {
	image: '',
	store: null,
	templatePath: dojo.moduleUrl('mioga', 'templates/GroupAttributes.html'),
	widgetsInTemplate: true,
	isContainer: true,

	startup: function () {
		// Translate labels
		var transl = dojo.i18n.getLocalization("mioga", "groupattributes");
		dojo.query("[i18n]", this.containerNode).forEach(function(node){
			var a = node.getAttribute("i18n");
			node.innerHTML = transl[a];
		});

		animatorStore.comparatorMap = {};
		animatorStore.comparatorMap['label'] = function (a, b) {
			if ((typeof (a) == 'string') && (typeof (b) == 'string')) {
				var a = unaccent (a.toLowerCase());
				var b = unaccent (b.toLowerCase());
			}

			if (a>b) return 1;
			if (a<b) return -1;
			return 0;
		};
		dijit.byId ('group_attributes_animator_id').attr ('queryExpr', '*${0}*');
		dijit.byId ('group_attributes_animator_id').attr ('autoComplete', false);
	}
});
});
