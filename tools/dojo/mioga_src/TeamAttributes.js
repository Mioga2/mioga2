dojo.require ('dijit.form.FilteringSelect');
dojo.require ('dijit.form.CheckBox');
dojo.require ('dijit.form.RadioButton');
dojo.requireLocalization ('mioga', 'teamattributes');

/* ----------------------------------------------------------
 * mioga.TeamAttributes
 * ---------------------------------------------------------- */
dojo.provide ('mioga.TeamAttributes');
dojo.declare("mioga.TeamAttributes", [dijit._Widget, dijit._Templated], {
	image: '',
	store: null,
	templatePath: dojo.moduleUrl('mioga', 'templates/TeamAttributes.html'),
	widgetsInTemplate: true,
	isContainer: true,

	startup: function () {
		// Translate labels
		var transl = dojo.i18n.getLocalization("mioga", "teamattributes");
		dojo.query("[i18n]", this.containerNode).forEach(function(node){
			var a = node.getAttribute("i18n");
			node.innerHTML = transl[a];
		});
	}
});
