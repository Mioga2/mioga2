dojo.require ('dijit.form.FilteringSelect');
dojo.require ('dijit.form.CheckBox');
dojo.require ('dijit.form.RadioButton');
dojo.requireLocalization ('mioga', 'userattributes');

/* ----------------------------------------------------------
 * mioga.UserAttributes
 * ---------------------------------------------------------- */
dojo.provide ('mioga.UserAttributes');
dojo.declare("mioga.UserAttributes", [dijit._Widget, dijit._Templated], {
	image: '',
	store: null,
	templatePath: dojo.moduleUrl('mioga', 'templates/UserAttributes.html'),
	widgetsInTemplate: true,
	isContainer: true,

	startup: function () {
		// Translate labels
		var transl = dojo.i18n.getLocalization("mioga", "userattributes");
		dojo.query("[i18n]", this.containerNode).forEach(function(node){
			var a = node.getAttribute("i18n");
			node.innerHTML = transl[a];
		});
	}
});
