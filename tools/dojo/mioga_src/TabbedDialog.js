/* ----------------------------------------------------------
 * mioga.TabbedDialog
 * ---------------------------------------------------------- */
dojo.requireLocalization("mioga", "tabbeddialog");
dojo.provide ('mioga.TabbedDialog');
dojo.declare("mioga.TabbedDialog", [dijit._Widget, dijit._Templated], {
	store: null,
	isContainer: true,
	templatePath: dojo.moduleUrl('mioga', 'templates/TabbedDialog.html'),
	widgetsInTemplate: true,
	tabs: null,
	tabnum: 0,
	onValidate: '',
	onCancel: '',
	widest: 0,

	// _started flag, a bit dirty but startup: is called twice...
	// http://groups.google.fr/group/dojo-interest/browse_thread/thread/80c515d708f074c/ef294e01a6af709d?hl=fr&ie=UTF-8&q=dojo+widget+startup+called+twice#ef294e01a6af709d
	_started: 0,

	_onButtonClick: function (id) {
		// Set tabs dimensions
		var width = 0;

		// Check if disabled
		var disabled = false;
		dojo.forEach (_tabs, function (tab) {
			if (tab.attributes.id.value === id && tab.disabled) {
				disabled = true;
			}
		});
		if (disabled) {
			return;
		}

		if (!this.widest) {
			// Get widest tab width
			var box_height = dojo.coords (dojo.query ('.tabbar', this.domNode)[0]).h;
			dojo.forEach (_tabs, function (tab) {
				var node = dojo.byId (tab.attributes.id.value);
				node.style.display = 'block';
				node.style.width = '';
				if (dojo.coords (node).w > width) {
					width = dojo.coords (node).w;
				}
			});

			// Set width and height
			if (box_height) {
				dojo.query ('.tab-item', this.domNode).forEach (function (node) {
					node.style.width = '100%';
					node.style.height = box_height.toString () + 'px';
				});
			}
			this.widest = width;
		}

		// Unselect all images
		dojo.forEach (_tabs,
			function (tab, index) {
				dojo.attr ('tab_' + tab.attributes.id.value, 'class', 'tab');
			}
		);

		// Select clicked
		dojo.attr ('tab_' + id, 'class', 'tab selected');

		// Hide all
		dojo.forEach (_tabs,
			function (tab, index) {
				var tab_id = tab.attributes.id.value;
				dojo.byId (tab_id).style.display = 'none';
			}
		);
		
		// Show clicked
		dojo.byId (id).style.display = 'block';

		// Focus first visible input element
		var firstinput = dojo.query ('input:not([type=hidden])', dojo.byId (id))[0];
		if (firstinput && firstinput.id) {
			setTimeout("dijit.byId ('" + firstinput.id + "').focus ()", 500);
		}
	},

	showFirst: function () {
		this.containerNode.parentNode.parentNode.style.height = '';
		var self = this;
		var clicked =  false;
		dojo.forEach (_tabs, function (tab) {
			if (!tab.disabled) {
				if (!clicked) {
					self._onButtonClick (tab.attributes.id.value);
					clicked = true;
				}
				dojo.attr ('image_' + tab.attributes.id.value, 'class', 'tabimage');
			}
			else {
				dojo.attr ('image_' + tab.attributes.id.value, 'class', 'tabimage disabled');
			}
		});
	},

	disable: function (id) {
		dojo.forEach (_tabs, function (tab) {
			if (tab.attributes.id.value === id) {
				tab.disabled = true;
			}
		});
	},

	startup: function(){
		if (this._started) return (0);

		var transl = dojo.i18n.getLocalization("mioga", "tabbeddialog");

		var tabbar = dojo.query(".vtabs")[0];
		var nl = dojo.query ('[widgetId].tab-item', this.containerNode);
		_tabnum = 0;
		_tabs = [];
		var _width = 0;
		var _onButtonClick = this._onButtonClick;
		var self = this;
		nl.forEach (
			function (node, index, nodeList) {
				var selclass = '';
				if (_tabnum == 0) {
					selclass = 'selected';
				}
				var tab = dojo.create ("li", {innerHTML: '<div class="tab ' + selclass + '" id="tab_' + node.attributes.tabid.value + '" title="' + node.attributes.legend.value + '"><div id="image_' + node.attributes.tabid.value + '" class="tabimage"/></div>', id: 'li_' + node.attributes.tabid.value}, tabbar);

				_tabs.push (node);
				_tabnum++;
				dojo.connect (tab, 'onclick', dojo.hitch (self, function () {_onButtonClick (node.attributes.tabid.value);}));
			}
		);

		// Prevent each form from submitting directly
		var func = this.onValidate;
		var form = dojo.query ("form", this.domNode)[0];
		form.onsubmit = function (evt) { eval (func); return (false); };

		// Translate buttons
		var buttons = dojo.query ('.button', this.domNode);
		dojo.forEach (buttons, function (node) {
			node.value = transl[node.value];
		});

		// Connect container dijit.Dialog hide signal
		var dialog = dijit.byId (this.domNode.parentNode.parentNode.attributes.id.value);
		dialog.connect (dialog, "hide", function (e) {
			dojo.forEach (_tabs, function (tab) {
				tab.disabled = false;
			});
		});

		// Hide all tabs but the first
		this.showFirst ();

		// Set started flag
		this._started = 1;
	}
});
