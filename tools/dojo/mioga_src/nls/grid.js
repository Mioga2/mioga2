({
	ConfirmMsg: "Are you sure ?",
	generic_error_title: "Internal error",
	generic_error: "An internal error has occured, please contact your Mioga2 administrator and give him the information below.",
	NoSelection: "You must select list elements first.",
	UnhandledError: "Server returned an unhandled error: "
})
