({
	Identifier: "Identifiant",
	Firstname: "Prénom",
	Lastname: "Nom",
	Email: "Adresse e-mail",
	Password: "Mot de passe",
	ConfirmPassword: "Confirmation du mot de passe",
	Skeleton: "Squelette",
	Language: "Langue",
	Autonomous: "Autonome",
	Shared: "Partagé",
	Status: "État",
	Active: "Actif",
	Disabled: "Désactivé",
	DeletedNotPurged: "Supprimé non-purgé",
	DisabledAttacked: "Désactivé après attaque"
})
