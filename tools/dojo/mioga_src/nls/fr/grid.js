({
	ConfirmMsg: "Êtes-vous sûr ?",
	generic_error_title: "Erreur interne",
	generic_error: "Une erreur interne s'est produite, veuillez contacter votre administrateur Mioga2 et lui communiquer les informations ci-dessous.",
	NoSelection: "Vous devez au préalable sélectionner des éléments de la liste.",
	UnhandledError: "Le serveur a renvoyé une erreur qui n'est pas gérée : "
})
