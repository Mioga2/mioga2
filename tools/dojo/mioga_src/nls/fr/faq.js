({
	deleteCategory:'Voulez-vous vraiment supprimer la catégorie "%CATEGORY%" ?',
	deleteEntry:'Voulez-vous vraiment suprimer l\'entrée\n"%TITLE%"?',
	titleLabel: 'Intitulé&nbsp;:',
	questionLabel: 'Question&nbsp;:',
	answerLabel: 'Réponse&nbsp;:',
	saveEntryLabel: 'Enregistrer',
	cancelEntryLabel: 'Annuler',
	fillAllFields: 'Merci de remplir tous les champs.',
	errorLabel: 'Erreur'
})