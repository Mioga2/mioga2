dojo.provide("mioga.InlineEditBox");

dojo.require("dijit.InlineEditBox");
dojo.require("dojo.cache");

dojo.declare("mioga.InlineEditBox", [ dijit.InlineEditBox ], {
	autoSave: false,
	editorWrapper: "mioga._InlineEditor",
		
	edit: function() {
		this.inherited(arguments);
		
		// This FORM helps to make the InlineEditBox submittable with ENTER
		var form_id = dijit.getUniqueId("mioga_InlineEditBox_form");
		var form = dojo.create("form", { id: form_id }, this.domNode.parentNode, "first");
		var form = new dijit.form.Form({}, form_id);
		dojo.place(this.wrapperWidget.domNode, form_id, "first");
		
		dojo.connect(form, 'onSubmit', this, function(evt) {
			console.log("FORM onSubmit");
			this.save(true);
			evt.stopPropagation();
		});
		this.wrapperWidgetFormID = form_id;
	},
	
	cancel: function() {
		this.inherited(arguments);
		dijit.byId(this.wrapperWidgetFormID).destroy();
	},
	
	_onFocus: function() {
		if (this.editing) {
			dijit.focus(dojo.query("#"+this.wrapperWidgetFormID+" input")[0]);
		}
	}
	
});



dojo.declare("mioga._InlineEditor", dijit._InlineEditor, {
	templateString: dojo.cache("mioga", "templates/InlineEditBox.html"),
	
	postCreate: function() {
		this.saveButton.set = function() {}; // so that the superclass method calls don't break
		this.inherited(arguments);
	}
});