/* ----------------------------------------------------------
 * MiogaTabItem
 * ---------------------------------------------------------- */
dojo.provide ('mioga.TabItem');
dojo.declare("mioga.TabItem", [dijit._Widget, dijit._Templated], {
	image: '',
	legend: '',
	tabid: '',
	templatePath: dojo.moduleUrl('mioga', 'templates/TabItem.html'),
	widgetsInTemplate: true,
	isContainer: true
});
