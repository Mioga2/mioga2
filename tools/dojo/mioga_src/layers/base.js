// This is a layer file. It's like any other Dojo module, except that we
// don't put any code other than require/provide statements in it. When we
// make a build, this will be replaced by a single minified copy of all
// the modules listed below, as well as their dependencies, all in the
// right order:

dojo.provide("mioga.layers.base");

// mioga.widgets, mioga.TabbedDialog, mioga.TabItem, mioga.MiniList,
// mioga.UserAttributes, mioga.TeamAttributes, mioga.GroupAttributes
dojo.require ('dijit._Widget');

// mioga.widgets, mioga.TabbedDialog, mioga.TabItem, mioga.MiniList,
// mioga.UserAttributes, mioga.TeamAttributes, mioga.GroupAttributes
dojo.require ('dijit._Templated');

dojo.require ('dijit.form.FilteringSelect'); // Many
dojo.require ('dijit.layout.BorderContainer');		// Everyone 
dojo.require ('dijit.layout.TabContainer');		// AnnuaireITER
dojo.require ('dijit.Toolbar');				// Everyone 
dojo.require ('dijit.Menu');				// Everyone 
dojo.require ('dojox.grid.DataGrid');		// Everyone 
dojo.require ('dijit.Tooltip');             // Everyone (base.xsl::AppNav) 
dojo.require ('dijit.form.Textarea'); 		//  Faq, mioga.TeamAttributes 
dojo.require ('dijit.form.TextBox');   // Faq, mioga.TeamAttributes 
dojo.require ("dijit.form.CheckBox");		// Colbert, Bottin
dojo.require ('dojo.i18n');            // Faq 
dojo.require ('dojo.data.ItemFileWriteStore');	// MiniList, SelectList 
dojo.require ('dojo.data.ItemFileReadStore');	// MiniList, SelectList 

dojo.require ('mioga.TabbedDialog');		// Colbert, ColbertSA 
dojo.require ('mioga.TabItem');				// Colbert, ColbertSA 
dojo.require ('mioga.MiniList');			// Colbert 
dojo.require ('mioga.UserAttributes');		// Colbert 
dojo.require ('mioga.GroupAttributes');		// Colbert 
dojo.require ('mioga.TeamAttributes');		// Colbert 
dojo.require ('mioga.widgets');

dojo.require ('dijit.form.NumberSpinner');    
dojo.require ('dijit.Dialog');         // Faq 
dojo.require ('dijit.TitlePane');      // Faq 
dojo.require ('dijit.form.Button');    // Faq 
dojo.require ('dijit.form.Form');      // Faq   
dojo.require ('dojo.dnd.Source');      // Faq 
dojo.require ('dijit.ColorPalette');	// Colbert 
dojo.require ("dijit.ProgressBar");		// Bottin

dojo.require("dijit._base.sniff"); // TabbedDialog, ColbertSA
