// All layers have an implicit dependency on dojo.js.
// Normally you should not specify a layer object for dojo.js, as it will
// be built by default with the right options. Custom dojo.js files are
// possible, but not recommended for most apps.

dependencies = {
	layers: [
        {
            // where to put the output relative to the Dojo root in a build
            name: "../mioga/layers/base.js",
            // what to name it (redundant w/ or example layer)
            resourceName: "mioga.layers.base",
            // what other layers to assume will have already been loaded
            // specifying modules here prevents them from being included in
            // this layer's output file
            // layerDependencies: [
            //    "dijit.dijit"
            // ],
            // which modules to pull in. All of the depedencies not
            // provided by dojo.js or other items in the "layerDependencies"
            // array are also included.
            dependencies: [
                // our layer specifies all the stuff our app will
                // need, so we don't need to list them all out here.
                "mioga.layers.base"
            ]
        }
    ],

    prefixes: [
        // the system knows where to find the "dojo/" directory, but we
        // need to tell it about everything else. Directories listed here
        // are, at a minimum, copied to the build directory.
        [ "dijit", "../dijit" ],
        [ "dojox", "../dojox" ],
        [ "mioga", "../mioga" ]
    ]
}

// If you choose to optimize the JS files in a prefix directory (via the
// optimize= build parameter), you can choose to have a custom copyright
// text prepended to the optimized file. To do this, specify the path to a
// file tha contains the copyright info as the third array item in the
// prefixes array. For instance:
    //      prefixes: [
    //              [ "acme", "/path/to/acme", "/path/to/acme/copyright.txt"]
    //      ]
    //
// NOTE:
//    If no copyright is specified in this optimize case, then by default,
//    the Dojo copyright will be used.
