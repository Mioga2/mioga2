# ============================================================================
# Mioga2 Project (C) 2010 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME

Louvre.pm : The Mioga2 Gallery creator.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Louvre::Const;
use strict;

$Mioga2::Louvre::Const::LOUVRE_DIR = 'mioga2-louvre';
$Mioga2::Louvre::Const::STATIC_DIR = 'louvre-static';

1;


# =========
#    Mioga2::Louvre, the Mioga application
# 		=========

package Mioga2::Louvre;
use strict;
use warnings;
use open ':encoding(utf8)';
use 5.008;

use base qw(Mioga2::Application);

use Data::Dumper;
use Locale::TextDomain::UTF8 'louvre';
use Mioga2::Content::XSLT;
use Mioga2::Exception::Application;
use Mioga2::tools::args_checker;
use Mioga2::tools::string_utils;
use Mioga2::tools::APIAuthz;
use Mioga2::URI;

my $debug = 0;

our $VERSION  = "1.0";
our $nr_cores;

BEGIN {
	## Don't use Parallel::ForkManager as long as it does not do CORE::exit
    # eval "use Parallel::ForkManager; use Sys::CPU;";
    # if ($@) {
#        $nr_cores = 1;
    # } else {
    #    $nr_cores = Sys::CPU::cpu_count();
    # }
}

# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialise
# the database. This method is mandatory for each Mioga2 application.
#
# ============================================================================

sub GetAppDesc {
    my $self    = shift;
    my %AppDesc = (
        ident              => 'Louvre',
        name		       => __('Louvre'),
        package            => 'Mioga2::Louvre',
        description        => __('Create and manage image slideshows'),
        type               => 'normal',
        all_groups         => 1,
        all_users          => 1,
        can_be_public      => 0,
		is_user            => 0,
		is_group           => 1,
		is_resource        => 0,
		api_version        => '2.3',

        functions => {
            'Anim'    => __("Add, modify or remove a slideshow"),
			'Read'    => __("Display slideshows"),
        },

        func_methods => {
            'Anim'     => [ 'CreateGallery', 'RemoveGallery' ],
			'Read'     => ['DisplayMain', 'GetGalleries'],
        },

        sxml_methods => {},

        xml_methods => {},

		non_sensitive_methods => [
			'DisplayMain',
			'GetGalleries',
			'CreateGallery',
			'RemoveGallery'
		]
    );
    return \%AppDesc;
}


#===============================================================================

=head2 DisplayMain

Main interface

=cut

#===============================================================================
sub DisplayMain {
    my ( $self, $context ) = @_;

	my $data = { DisplayMain => $context->GetContext () };

	my $base = $context->GetConfig ()->GetBaseURI ();

	my @galleries = map { $_->{uri} =~ s/^$base//; $_; } @{ac_ForceArray ($self->DBGetGalleries ($context->GetConfig (), '/', $context->GetUser ()))};
	print STDERR "[Mioga2::Louvre::DisplayMain] Galleries: " . Dumper \@galleries if ($debug);

	# Translate list of path to a nested hash of hash to be translated properly to XML
	# The array: [
	# 		'/Mioga/home/group1/dir1',
	# 		'/Mioga/home/group1/dir1/subdir',
	# 		'/Mioga/home/group1/dir2',
	# 		'/Mioga/home/group2/dir1'
	# ]
	# Becomes: 
	# 		folder => {
	#			name   => 'Mioga',
	#			folder => {
	#				name   => 'home',
	#				folder => [
	#					{
	#						name   => 'group1',
	#						folder => [
	#							{
	#								name => 'dir1',
	#								path => '/path/to/dir1-gallery',
	#								folder => [
	#									{
	#										name => 'subdir',
	#										path => '/path/to/dir1-gallery/subdir'
	#									}
	#								]
	#							},
	#							{
	#								name => 'dir2',
	#								path => '/path/to/dir2-gallery'
	#							}
	#						]
	#					},
	#					{
	#						name   => 'group2',
	#						folder => {
	#							name => 'dir1',
	#							path => '/path/to/dir1-gallery'
	#						}
	#					}
	#				]
	#			}
	# 		}
	$data->{DisplayMain}->{galleries} = { };
	for my $gallery (@galleries) {
		my $last = $data->{DisplayMain}->{galleries};
		for my $member (split (/\//, $gallery->{uri})) {
			next if ($member eq '');
			my $found = 0;
			map { if ($_->{name} eq $member) { $last = $_; $found = 1; } } @{$last->{folder}};
			unless ($found) {
				push (@{$last->{folder}}, { name => $member });
				$last = $last->{folder}->[@{$last->{folder}}-1];
			}
		}
		$last->{path} = $gallery->{target};
	}

	$data->{DisplayMain}->{louvre_dir} = $Mioga2::Louvre::Const::LOUVRE_DIR;

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "[Mioga2::Louvre::DisplayMain] XML: $xml\n" if ($debug);

	my $content = new Mioga2::Content::XSLT ($context, stylesheet => 'louvre.xsl', locale_domain => "louvre_xsl");
	$content->SetContent ($xml);

    return ($content);
}	# ----------  end of subroutine DisplayMain  ----------


#===============================================================================

=head2 GetGalleries

Get the list of available galleries

=head3 Incoming Arguments

=over

=item B<path>: A path to limit search results (eg. /Mioga2/Mioga/home/), optional

=back

=head3 Generated XML

=over

	<GetGalleries>
		<gallery>...</gallery>
		<gallery>...</gallery>
		...
	</GetGalleries>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=cut

#===============================================================================
sub GetGalleries {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Louvre::GetGalleries] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['path'], 'allow_empty', ['location', $context->GetConfig ()] ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	$data->{gallery} = $self->DBGetGalleries ($context->GetConfig (), $values->{path}, $context->GetUser ());

	print STDERR "[Mioga2::Louvre::GetGalleries] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetGalleries  ----------


sub CreateGallery {
    my ($self, $context) = @_;
	my ($args, $errors, $dir);
    my $xml = '<?xml version="1.0" encoding="UTF-8"?>';
    my $config = $context->GetConfig();

   	($args, $errors) = ac_CheckArgs($context, [ [ ['dir'], 'disallow_empty'], [['regenerate'], 'allow_empty'] ]);
   	if (@$errors) {
   		throw Mioga2::Exception::Application("Mioga2::Louvre::CreateGallery", Dumper($errors));
   	}
   	$dir = $args->{dir};

    my $access_method = $context->GetAccessMethod();
    if ($access_method eq 'GET') { # Show the configuration screen for a new gallery

		if (!$self->DBURIContainsGalleries ($dir)) {
			$xml .= "<CreateGallery_Config>";
			$xml .= $context->GetXML();
			$xml .= "<directory>".$dir."</directory>";
			$xml .= '<regenerate>' . (st_ArgExists ($context, 'regenerate') ? 1 : 0) . '</regenerate>';
			$self->{db}->Execute ("SELECT m_lang.locale, count(m_mioga.rowid) AS polularity FROM m_lang, m_mioga WHERE m_mioga.default_lang_id = m_lang.rowid GROUP BY m_lang.locale ORDER BY polularity DESC;");
			while (my $lang = $self->{db}->Fetch ()) {
				$xml .= "<locale><value>" . $lang->{locale} . "</value><label>" . __($lang->{locale}) . "</label></locale>";
			}
			$xml .= "</CreateGallery_Config>";
		}
		else {
			$xml .= '<ContainsGalleries>';
			$xml .= $context->GetXML();
			$xml .= '</ContainsGalleries>';
		}

    } elsif ($access_method eq 'POST') { # Start gallery creation
		my $locales = '^(' . join ('|', map { $_->{locale} } @{$self->{db}->SelectMultiple ("SELECT * FROM m_lang;")}) . ')$';

    	($args, $errors) = ac_CheckArgs($context, [
    			[ ['maxheight', 'maxwidth'], 'allow_empty', 'want_int'],
    			[ ['locale'], 'disallow_empty', ['match', $locales]],
				[ ['background'], 'disallow_empty', ['match', '(black|white)']],
				[ ['show_filename', 'show_description', 'show_tags', 'show_dimensions', 'show_weight', 'popup_display'], 'allow_empty', ['match', 'on'] ],
				[ ['regenerate'], 'allow_empty' ],
    			]);
    	if (@$errors) {
    		throw Mioga2::Exception::Application("Mioga2::Louvre::CreateGallery", Dumper($errors));
    	}
    	undef $errors;

		# Set default value for options
		my $default = 0;
		if ($args->{popup_display}) {
			$default = 1;
		}

		for my $flag (qw/show_filename show_description show_tags show_dimensions show_weight popup_display/) {
			if ($args->{$flag}) {
				$args->{$flag} = 1;
			}
			else {
				$args->{$flag} = $default;
			}
		}

		foreach my $val ('maxheight', 'maxwidth') {
			if ($args->{$val} < 10 or $args->{$val} > 999) {
				$errors = __('Thumbnail height and width must be between 10 and 999.');
				last;
			}
		}

		unless ($errors) {
			my $gallery;

			if (st_ArgExists ($context, 'regenerate')) {
				$gallery = Mioga2::Louvre::Gallery->new($config, $dir, 
						$context->GetUser(), load => 1);
				$errors = $gallery->remove();
			}

			$gallery = Mioga2::Louvre::Gallery->new($config, $dir,
					$context->GetUser(),
    				max_tn_width  => $args->{maxwidth},
    				max_tn_height => $args->{maxheight},
					locale        => $args->{locale},
					show_filename => $args->{show_filename},
					show_description => $args->{show_description},
					show_tags => $args->{show_tags},
					show_dimensions => $args->{show_dimensions},
					popup_display => $args->{popup_display},
					show_weight => $args->{show_weight},
					background => $args->{background},
    				);
    		$errors = $gallery->create();
		}

		my $success_label = __('Gallery successfully created');
		if (st_ArgExists ($context, 'regenerate')) {
			$success_label = __('Gallery successfully modified');
		}

        $xml .= "<Operation_Result>";
        $xml .= $context->GetXML();
		$xml .= "<status>" . ($errors ? 0 : 1) . "</status>";
        $xml .= "<result>"
        		. ($errors ? $errors : $success_label)
        		. "</result>";
        $xml .= "</Operation_Result>";

    }

    print STDERR "xml = $xml\n" if($debug);
    return ($self->_writeXML($context, $xml));
}


sub _writeXML {
    my ($self, $context, $xml) = @_;
    my $content = new Mioga2::Content::XSLT(
        $context,
        stylesheet    => 'louvre.xsl',
        locale_domain => 'louvre_xsl'
    );
    $content->SetContent( $xml );
    return $content;
}


sub RemoveGallery {
    my ($self, $context) = @_;
	my ($args, $errors, $dir);
    my $xml = '<?xml version="1.0" encoding="UTF-8"?>';
    my $config = $context->GetConfig();

   	($args, $errors) = ac_CheckArgs($context, [ [ ['dir'], 'disallow_empty'] ]);
   	if (@$errors) {
   		throw Mioga2::Exception::Application("Mioga2::Louvre::RemoveGallery", Dumper($errors));
   	}
   	$dir = $args->{dir};

    my $access_method = $context->GetAccessMethod();
    if ($access_method eq 'GET') { # TODO: switch to POST
		my $gallery = Mioga2::Louvre::Gallery->new($config, $dir, 
				$context->GetUser(), load => 1);
    	$errors = $gallery->remove();

        $xml .= "<Operation_Result>";
        $xml .= $context->GetXML();
		$xml .= "<status>" . ($errors ? 0 : 1) . "</status>";
        $xml .= "<result>"
        		. ($errors ? $errors : __('Gallery successfully removed'))
        		. "</result>";
        $xml .= "</Operation_Result>";
    }

    print STDERR "xml = $xml\n" if($debug);
    return ($self->_writeXML($context, $xml));
}


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


#===============================================================================

=head2 DBGetGalleries

Get the list of available galleries

=cut

#===============================================================================
sub DBGetGalleries {
	my ($self, $config, $path, $user) = @_;

	my $galleries;

	# No need to check URIs against mioga_id here as each URI will be submitted to AuthzTestRightAccessForURI below
	my $sql = "SELECT uris.rowid, replace (uris.uri, ?, '') AS uri, uris.uri AS target, uris.group_id, authztestaccessforuri (uris.uri, ?) AS access FROM (SELECT rowid, uri, group_id FROM m_uri WHERE uri SIMILAR TO ? AND uri NOT SIMILAR TO ? AND mimetype = 'directory' AND group_id IN (SELECT rowid FROM m_group WHERE mioga_id = (SELECT mioga_id FROM m_user_base WHERE rowid = ?))) AS uris ORDER BY uri;";
	$self->{db}->Execute ($sql, ['/' . $Mioga2::Louvre::Const::LOUVRE_DIR, $user->Get ('rowid'), "$path%" . $Mioga2::Louvre::Const::LOUVRE_DIR . '%', '%' . $Mioga2::Louvre::Const::STATIC_DIR . '%', $user->Get ('rowid')]);
	while (my $res = $self->{db}->Fetch ()) {
		push (@{$galleries}, $res) if ($res->{access} > 0);
	}

	return ($galleries);
}	# ----------  end of subroutine DBGetGalleries  ----------


#===============================================================================

=head2 DBURIContainsGalleries

Checks if a URI contains galleries

=cut

#===============================================================================
sub DBURIContainsGalleries {
	my ($self, $uri) = @_;

	my $galleries = $self->{db}->SelectMultiple ("SELECT m_uri.* FROM m_uri, m_louvre WHERE m_uri.rowid = m_louvre.uri_id AND m_uri.uri SIMILAR TO ?;", ["$uri/%"]);

	return (scalar (@$galleries));
}	# ----------  end of subroutine DBURIContainsGalleries  ----------


# =========
#    Mioga2::Louvre::Gallery, represents one gallery. Provides methods to
#    auto-generate a gallery folder from an existing image folder, moving
#    helper files (auto-generated CSS, theme-defined CSS, Lightbox) around,
#    to re-generate the gallery, or to remove it.
# 		=========

package Mioga2::Louvre::Gallery;
use strict;
use warnings;
use open ':encoding(utf8)';

use constant RECOGNISED_FILES => qw(.jpg .jpeg .png .tif .tiff .gif .bmp);
# see also method _create_single_thumbnail

use Error qw(:try);
use File::Basename ();
use File::Copy ();
use File::Path ();
use File::Spec ();
use File::stat ();
use Image::Magick;
use IO::Dir;
use IO::File ();
use POSIX qw(:locale_h);
use Locale::TextDomain::UTF8 'louvre';
use Mioga2::DAVFSUtils;
use Mioga2::Exception::Application;
use Mioga2::tools::string_utils;
use Mioga2::URI;
use Mioga2::Old::User;
use Data::Dumper;

=head2 my $gallery = Mioga2::Louvre::Gallery->new($config, $folder, $user, %options)
Entry point into Mioga2::Louvre::Gallery. The Mioga configuration
and the URI (string) of the base folder for the gallery must be specified.
After constructing this object, you can call ->create(), ->update() or ->remove().

$user specifies the user who will "execute" file system operations via DAVFS.
$user is either a Mioga2::Old::User or undef. When invoked by a user action
(creation or deletion of a gallery), it must be the corresponding User object.
If the notifier does updates to a gallery, however, it will be undef, and 
DAVFS actions will be done as the owner of the gallery base URL. 

Possible options:
    === either ===
	load :  If set to true, get settings for this gallery from the database
			(throws an exception if not found)
	=== or ===
    max_tn_width, max_tn_height : maximum width or height for the thumbnails (default: 100x100),
=cut

sub new {
    my ($class, $config, $folder, $user, %options) = @_;
    return unless $folder and $folder ne '/';
    my $self = {
    	config        => $config,
    	uri           => $folder,
    	user		  => $user
    };
    my $mioga2_uri = Mioga2::URI->new($self->{config}, uri => $folder);
    unless ($mioga2_uri) {
    	throw Mioga2::Exception::Application("Mioga2::Louvre::Gallery::new", "Cannot instantiate gallery, wrong folder: $folder");
    }
	
    my $filepattern = join('|', RECOGNISED_FILES);
    $self->{filepattern} = 	qr/(?:$filepattern)$/i;

    bless $self, $class;

    $self->{sql} = Mioga2::Louvre::Gallery::SQL->new($self);
    ($self->{group_id}, $self->{base_owner_id}) = $self->{sql}->load_group_and_user_IDs();
    unless (defined $self->{user}) {
    	$self->{user} = Mioga2::Old::User->new($config, rowid => $self->{base_owner_id}); 
    }

	$self->{db} = $config->GetDBObject ();
    
    $self->{davfsutils} = Mioga2::DAVFSUtils->new($config, $self->{user},
    			notify => 0); # We might have been triggered by a notification,
    			# but nothing of what _we_ do shall trigger any more notifications.

	unless($self->{davfsutils}->IsCollection($self->{uri}.'/')) {
		throw Mioga2::Exception::Application("Mioga2::Louvre::Gallery::new", "URI is not a collection: "
				.$self->{uri}."/");
	}

    if ($options{load}) {
    	my $stored_data = $self->{sql}->load();
    	if (ref($stored_data) eq 'HASH') {
    		foreach my $key (keys %$stored_data) {
	    		$self->{$key} = $stored_data->{$key};
    		}
    	} else {
    		throw Mioga2::Exception::Application("Mioga2::Louvre::Gallery::new", 
    				"Cannot load values for a gallery in $folder");
    	}
    } else {
        $self->{max_tn_width} = $options{max_tn_width}  || 100;
        $self->{max_tn_height}= $options{max_tn_height} || 100;
		$self->{locale} = $options{locale};
		$self->{show_filename} = $options{show_filename};
		$self->{show_description} = $options{show_description};
		$self->{show_tags} = $options{show_tags};
		$self->{show_dimensions} = $options{show_dimensions};
		$self->{popup_display} = $options{popup_display};
		$self->{show_weight} = $options{show_weight};
		$self->{background} = $options{background};
    };

	setlocale(LC_ALL, "$self->{locale}.UTF-8");
        
    return $self;
}


# Lazy loading the group object
sub getGroup {
	my ($self) = @_;
	unless (exists $self->{group}) {
		$self->{group} = Mioga2::Old::GroupBase->CreateObject($self->{config},
				rowid => $self->{group_id});
	}
	return $self->{group};
}


# Returns a Parallel::ForkManager for this gallery, if the machine has more than one core.
# Undef otherwise. The object is lazily created at the first call.
#sub pforkman {
#	my ($self) = @_;
	#unless (exists($self->{_pforkman})) {
	#	$self->{_pforkman} = ($nr_cores == 1 ? undef : Parallel::ForkManager->new($nr_cores));
	#}
	#return $self->{_pforkman};
#}


=head2 $louvre->create()
Creates a new gallery, containing all PNG, GIF, JPEG, TIF images in the file tree.
(The folder has been specified at object construction).
All generated files (HTML, thumbnails) will be put in the
directory $Mioga2::Louvre::Const::LOUVRE_DIR.

Preconditions:
    * "folder" must be readable and writable
    * "folder/LOUVRE_DIR" must not yet exist
Postconditions:
    * "folder/LOUVRE_DIR" is filled with the generated gallery data,
        "index.html" is the starting point.
Return value:
    * an error string in case of a fatal error;
    * 0 when there are no images yet
    * "undef" when a non-empty gallery has been created.
=cut
sub create {
    my ($self) = @_;

	my $status = undef;

	try {
		$status = $self->_init_slideshow_dir();
		unless ($status) {
			$self->{sql}->register();
			my $success = $self->update();
			unless ($success) {
				# There are no images yet in this gallery, we'll have to create a blank index page.
				$self->_create_index('', [], []);
				$status = 0;
			}
		}
	}
	otherwise {
		my $err = shift;
		print STDERR "[Mioga2::Louvre::Gallery::create] Gallery creation failed\n";
		print STDERR $err->stringify ();
		$self->remove ();
		$status = __('Cannot create gallery');
	};

    return $status;
}


=head2 Mioga2::Louvre->updateGallery($folder, %options)
Updates the gallery within "folder", overwriting all previous gallery content.
Invoke this after adding or removing files from the "folder" file tree.

Possible options: as for "createGallery"

Preconditions:
    * "folder/LOUVRE_DIR" must exist
Postconditions:
    * "folder/LOUVRE_DIR" is filled up-to-date gallery data,
        "index.html" is the starting point.
Return value:
    * an error string or "undef" when successful
=cut
sub update {
	my ($self) = @_;
	my $static_uri = $self->{uri}.'/'.$Mioga2::Louvre::Const::LOUVRE_DIR.'/'.$Mioga2::Louvre::Const::STATIC_DIR;
	my $gallery_exists = $self->{davfsutils}->IsCollection($static_uri); 
	unless ($gallery_exists) {
   		$self->_init_slideshow_dir();
   	}

	return $self->_update_rec('');
}


sub _update_rec {
    my ($self, $folder) = @_;
   	print STDERR "[Mioga2::Louvre::Gallery::_update_rec] Begin folder = $folder\n"  if ($debug);
	my $full_uri = $self->_fulluri($folder);
	
    # Recursively go through the file tree.
    # We'll do a depth-first walk here, this allows us to forget
    # subfolders without any images inside.
    my $items = $self->{davfsutils}->ReadCollection( $full_uri, 'short_name' => 1, 'lastmodified' => 1);
    if (ref($items) eq '') {
    	print STDERR "[Mioga2::Louvre::Gallery::_update_rec] Could not ReadCollection $full_uri: result $items\n" if $debug;
    	return undef;
    }

    my (@nonempty_subdirs, @images);
    foreach my $item (@$items) {
    	my $name = $item->{short_name};
        next if $name eq '.' or $name eq '..' or $folder eq '' && $name eq $Mioga2::Louvre::Const::LOUVRE_DIR;
        if ($item->{type} eq 'collection') {
        	print STDERR "[Mioga2::Louvre::Gallery::_update_rec] inspecting subdirectory $name of folder $folder\n" if $debug > 1;
            if ($self->_update_rec(($folder ? "$folder/$name" : $name))) {
                push @nonempty_subdirs, $name;
            }
        } elsif ($name =~ m/$self->{filepattern}/) {
        	print STDERR "[Mioga2::Louvre::Gallery::_update_rec] treating $name\n" if $debug > 1;
            push @images, $item;
        }
    }
    print STDERR " images = ".Dumper(\@images)."\n" if $debug > 1;
    print STDERR " nonempty_subdirs = ".Dumper(\@nonempty_subdirs)."\n" if $debug > 1;

    if (@images + @nonempty_subdirs > 0) {
        my $thumbnailed = $self->_create_thumbnails($folder, \@images);
       	print STDERR " thumbnailed = ".Dumper($thumbnailed)."\n" if $debug > 1;

        my %expected_tn;
        foreach my $origfile (@$thumbnailed) {
            # We expect all current thumbnails...
            $expected_tn{$self->_thumbnail_file($origfile)} = 1;
        }
        foreach my $subdir (@nonempty_subdirs) {
            # ...and also all current subdirectories.
            $expected_tn{$subdir} = 1;
        }
        $self->_cleanup_thumbnail_dir($self->_thumbnail_dir($folder), \%expected_tn);

        $self->_create_index($folder, $thumbnailed, \@nonempty_subdirs);

		#my $pforkman = $self->pforkman();
        #if (defined $pforkman) {
            #$pforkman->wait_all_children;
        #}
        return 1;
    }
	else {
        return 0;
    }
}


sub remove {
	my ($self) = @_;
	my $gallery_uri = $self->_fulluri($self->_thumbnail_dir(''));
	
	if ($self->{davfsutils}->Delete($gallery_uri.'/')) {
		$self->{sql}->unregister();
	} else {
		throw Mioga2::Exception::Application("Mioga2::Louvre::Gallery::remove", "Cannot remove URI $gallery_uri");
	}
}


# Create thumbnails for all given files.
# Takes a (relative) folder name and the list of image files therein (entries
# as returned by DAVFSUtils::ReadCollection).
# Returns a subset of that list, for which thumbnails have been successfully created
# or for which up-to-date thumbnails already existed!
sub _create_thumbnails {
    my ($self, $folder_name, $files_list) = @_;
    print STDERR " _create_thumnails = folder_name = $folder_name  files_list = ".Dumper($files_list)."\n" if $debug > 1;
    my @thumbnailed_files;
    my $thumbnails_baseUri = $self->_fulluri($Mioga2::Louvre::Const::LOUVRE_DIR.($folder_name ? "/$folder_name" : ''));

	my $thumbnails_list = $self->{davfsutils}->ReadCollection($thumbnails_baseUri, 'short_name' => 1, 'lastmodified' => 1);	
    print STDERR " thumbnails_list = ".Dumper($thumbnails_list)."\n" if $debug > 1;
	my %thumbnails_hash = ();
	
	if (ref($thumbnails_list) eq '') {
		if ($thumbnails_list eq '404') {
			unless ($self->{davfsutils}->MakeCollection( $thumbnails_baseUri, 'robust' => 1)) {
				throw Mioga2::Exception::Application("Mioga2::Louvre::Gallery::_create_thumbnails", "Cannot create $thumbnails_baseUri");
			}
		}
		else {
			throw Mioga2::Exception::Application("Mioga2::Louvre::Gallery::_create_thumbnails", "Cannot write or read inside $thumbnails_baseUri: $thumbnails_list");
		}
	} else {
		# Convert the $thumbnails_list into a hashref for more efficient access
		foreach my $thumbnail (@$thumbnails_list) {
			$thumbnails_hash{$thumbnail->{name}} = $thumbnail;
		}
	}
    print STDERR " thumbnails_hash = ".Dumper(\%thumbnails_hash)."\n" if $debug > 1;
	
    foreach my $file (@$files_list) {
    	my $thumbnail_uri = $thumbnails_baseUri . '/' . $self->_thumbnail_file($file->{short_name});
		my $thumbnail = $thumbnails_hash{$thumbnail_uri};
		
		if (defined($thumbnail)) {
			if ($thumbnail->{type} ne 'file') {
        		throw Mioga2::Exception::Application("Mioga2::Louvre::Gallery::_create_thumbnails", "Logic error: $thumbnail->{name} is not a file");
        	}
        	print STDERR "[Mioga2::Louvre::Gallery::_create_thumbnails] comparing thumbnail $thumbnail->{mod}" . " with file date $file->{mod}\n" if ($debug > 2);
        	#if ($thumbnail->{mod} gt $file->{mod}) {
           		# up-to-date thumbnail
           	#	print STDERR "[Mioga2::Louvre::Gallery::_create_thumbnails] thumbnail is up-to-date: $thumbnail->{short_name}\n" if $debug > 1;
           	#	push @thumbnailed_files, $file->{short_name};
           	#	next;
        	#}
		}		
		
		if ($self->_create_single_thumbnail($file->{name}, $thumbnail_uri)) {
	        push @thumbnailed_files, $file->{short_name};			
		} else {
	       	print STDERR "[Mioga2::Louvre::Gallery::_create_thumbnails] Could not create thumbnail for $file->{name}\n" if $debug;
		}				
    }
    return \@thumbnailed_files;
}


# Gives back the thumbnail directory for the given image directory
# (relative to the root directory).
sub _thumbnail_dir {
    my ($self, $imagedir) = @_;
    return $Mioga2::Louvre::Const::LOUVRE_DIR . ($imagedir eq '' ? '' : "/$imagedir");
}


# Gives back the thumbnail file name for the given image file name
# (just base names, without paths). Always JPEG.
sub _thumbnail_file {
    my ($self, $filename) = @_;
    $filename =~ s#\.[^.]+$#_tn.jpg#;
    return $filename;
}


# Gives back the path to the directory for static files
# (which is inside the gallery directory).
# The result is relative to the gallery root directory.
sub _path_to_static_files {
	my ($self) = @_;
	return $self->_thumbnail_dir($Mioga2::Louvre::Const::STATIC_DIR);
}


# Turn an URI relative to the gallery root into an absolute URI.
sub _fulluri {
	my ($self, $relative_path) = @_;
	return $self->{uri} . ($relative_path ? "/$relative_path" : "");
}


# Make the (relative) folder/file readable/writeable for the Apache user, if necessary.
# Used by Mioga2::Louvre::SQL::declareURI.
=head2
sub _clean_rights {
	my ($self, $folder, $is_recursive) = @_;
	return unless $folder and exists($ENV{USER}) and $ENV{USER} eq 'root';
	my $recursion = ($is_recursive ? '-R' : '');
	my $user  = $self->{config}->GetApacheUser();
	my $group = $self->{config}->GetApacheGroup();
	my $path  = $self->_fullpath($folder);
	system("/bin/chown $recursion '$user':'$group' '$path'");
}
=cut

# Creates a thumbnail for a given file.
# Takes the full URIs to the original file and to the thumbnail file to be created.
# Returns 1 when successful, 0 otherwise.
sub _create_single_thumbnail {
    my ($self, $image_uri, $thumbnail_uri) = @_;
    print STDERR "[Mioga2::Louvre::Gallery::_create_single_thumbnail] image_uri $image_uri thumbnail_uri $thumbnail_uri\n" if $debug;
    my $ret;
    #my $pforkman = $self->pforkman();
    #if (defined $pforkman) {
    #    $pforkman->start and return 1;
    #    setpriority(0, 0, 10);
    #}
    #unless (exists $self->{im}) {
      #  $self->{im} = Image::Magick->new();
    #}
    #my $im = $self->{im};
    my $im = Image::Magick->new();

	# TODO: reduce memory consumption for in-memory files
	my $image_dataref = $self->{davfsutils}->GetFile($image_uri);
	my $format;
	if    ($image_uri =~ /\.jpe?g$/i) { $format = 'JPEG'; }
	elsif ($image_uri =~ /\.png$/i)   { $format = 'PNG'; }
	elsif ($image_uri =~ /\.tiff?$/i) { $format = 'TIFF'; }
	elsif ($image_uri =~ /\.gif$/i)   { $format = 'GIF'; }
	elsif ($image_uri =~ /\.bmp$/i)   { $format = 'BMP'; }
	else {
		throw Mioga2::Exception::Application("Mioga2::Louvre::Gallery::_create_single_thumbnail", "Unknown image format: $image_uri");
	}	
	
	$im->SetAttribute(magick => $format);
	$ret = $im->BlobToImage($$image_dataref);
	
    if ($ret ne '') {
        print STDERR "[Mioga2::Louvre::Gallery::_create_single_thumbnail] Cannot read $image_uri : $ret\n" if $debug;
        @$im = ();
        return 0;
    }

    # $im->AutoOrient();
    # does not work in Image::Magick 6.3.7, see
    # http://www.imagemagick.org/discourse-server/viewtopic.php?f=3&t=11925

	# We construct our own AutoOrient().
	# See ImageMagick, Magick.xs 6.3.2 (line 7921 in v6.3.2), and
	# http://sylvana.net/jpegcrop/exif_orientation.html
	my $exif = $im->Get('format', '%[EXIF:*]'); # two arguments
	if ($exif =~ m#^exif:Orientation=(.*)$#m) {
    	my $orientation = $1;
	       if ($orientation == 6) { $im->Rotate(90);   } # right-top
	    elsif ($orientation == 8) { $im->Rotate(270);  } # left-bottom
	    elsif ($orientation == 3) { $im->Rotate(180);  } # bottom-right
	    elsif ($orientation == 2) { $im->Flop();       } # top-right
	    elsif ($orientation == 4) { $im->Flip();       } # bottom-left
	    elsif ($orientation == 5) { $im->Transpose();  } # left-top
	    elsif ($orientation == 7) { $im->Transverse(); } # right-bottom
	}

    # No way to set the "-define" option in PerlMagick :-/
    # $im->Set(define => 'jpeg:size='.2*$self->{max_tn_width}.'x'.2*$self->{max_tn_height});
    $im->Thumbnail(geometry => ($self->{max_tn_width} * 2).'x'.($self->{max_tn_height} * 2));

	my $thumbnail_data = ( $im->ImageToBlob(magick => 'JPEG') )[0];
	
    if ($ret ne '') {
        @$im = ();
        print STDERR "[Mioga2::Louvre::Gallery::_create_single_thumbnail] Cannot write thumbnail image for $thumbnail_uri into memory : $ret\n"
        		if $debug;
        return 0;
    }
    $self->{davfsutils}->PutFile($thumbnail_uri, \$thumbnail_data);
    
    # Looks horrible, but that is what the documentation says...
    @$im = ();

    #if (defined $pforkman) {
       # $pforkman->finish;
    #}
    print STDERR "[Mioga2::Louvre::Gallery::_create_single_thumbnail] created thumbnail for " . $image_uri . "\n" if $debug; 
    return 1;
}


# Delete thumbnails that do not correspond any longer to existing images in a folder.
# Takes the name of the thumbnail folder, and a hashref with everything that is to be expected
# (thumbnails _and_ subdirectories). Other entries of the folder will be removed.
# Does not return anything.
sub _cleanup_thumbnail_dir {
    my ($self, $folder, $expected) = @_;
    return unless $folder; # can't be paranoid enough
    $expected->{'index.html'} = 1;
    if ($folder eq $Mioga2::Louvre::Const::LOUVRE_DIR) {
    	$expected->{''.$Mioga2::Louvre::Const::STATIC_DIR} = 1;
    }
    my $folder_uri = $self->_fulluri($folder);
    
    my $items = $self->{davfsutils}->ReadCollection($folder_uri, 'short_name' => 1);
    unless (ref($items) eq 'ARRAY') {
    	throw Mioga2::Exception::Application("Mioga2::Louvre::Gallery::_cleanup_thumbnail_dir",
				"Cannot access $folder_uri : $items");
    }
    foreach my $item (@$items) {
    	unless( exists $expected->{$item->{short_name}} ) {
	        $self->{davfsutils}->Delete($item->{name});		
    	}
    }
}


# Find _once_ all relevant CSS files.
sub _css_files {
	my ($self) = @_;
	unless (exists $self->{cssfiles}) {
		$self->{cssfiles} = [];

		my $items = $self->{davfsutils}->ReadCollection(
				$self->_fulluri($self->_path_to_static_files()),
				'short_name' => 1);
		if (ref($items) eq 'ARRAY') {
			foreach my $item (@$items) {
				if ($item->{short_name} =~ /\.css$/i) {
					push @{$self->{cssfiles}}, $item->{short_name}
				}
			}
		}
	}
	return $self->{cssfiles};
}


# Find _once_ all relevant JS files.
sub _js_files {
	my ($self) = @_;
	unless (exists $self->{jsfiles}) {
		$self->{jsfiles} = [];

		my $items = $self->{davfsutils}->ReadCollection(
				$self->_fulluri($self->_path_to_static_files()),
				'short_name' => 1);
		if (ref($items) eq 'ARRAY') {
			foreach my $item (@$items) {
				if ($item->{short_name} =~ /\.js$/i) {
					push @{$self->{jsfiles}}, $item->{short_name}
				}
			}
		}
	}
	return $self->{jsfiles};
}


# Creates an "index.html" with the thumbnailed images _and_ the subdirs.
# Takes the folder name, a listref with the _original files for which thumbnails have been created_
# and a listref with the subdirs in that folder.
# Does not return anything.
sub _create_index {
    my ($self, $folder, $files, $subdirs) = @_;

    #unless (exists $self->{im}) {
    #    $self->{im} = Image::Magick->new();
    #}
    #my $im = $self->{im};
    my $im = Image::Magick->new();
        
    my $rel_prefix    = $self->_relative_link_prefix_to_root($folder);
    my $static_folder = ${rel_prefix} . $Mioga2::Louvre::Const::STATIC_DIR . '/';
	my $theme_uri = $self->_theme_uri ($self->{config}, $self->{group});
    my $index_data = '';

    # Create the title and its links
    my @path_components = grep { $_ ne '' }
    		File::Spec->splitdir($folder);
    unshift @path_components, File::Basename::basename($self->{uri});
  	my $title = join('&nbsp;&rarr;&nbsp;', @path_components);

  	my $highest_uplevel = scalar(@path_components) - 1;
	my @title_with_links;
	for(my $i = 0; $i < $highest_uplevel; $i++) {
		push @title_with_links, '<a href="'. ("../" x ($highest_uplevel - $i)) . 'index.html">'
				. $path_components[$i] . "</a>";
	}
	push @title_with_links, $path_components[$highest_uplevel]; # no link for current title
	my $title_with_links = join('&nbsp;&rarr;&nbsp;', @title_with_links);

	my $background = $self->{background} ? $self->{background} : 'black';

	my $filename_chk = $self->{show_filename} ? 'checked="1"' : ''; my $toggle_filename_label = __('Filename');
	my $description_chk = $self->{show_description} ? 'checked="1"' : ''; my $toggle_description_label = __('Description');
	my $tags_chk = $self->{show_tags} ? 'checked="1"' : ''; my $toggle_tags_label = __('Tags');
	my $dimensions_chk = $self->{show_dimensions} ? 'checked="1"' : ''; my $toggle_dimensions_label = __('Dimensions');
	my $weight_chk = $self->{show_weight} ? 'checked="1"' : ''; my $toggle_weight_label = __('Weight');

	my $popup_display = $self->{popup_display};

    $index_data .= qq~<!DOCTYPE html>
<html>
  <head>
    <title>${title}</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">~;
	# Include all CSS files found in the static folder.
    foreach my $css_file (@{$self->_css_files()}) {
    	$index_data .= qq~
    <link rel="stylesheet" type="text/css" href="${static_folder}${css_file}">~;
    }
	# Include all JS files found in the static folder.
    foreach my $js_file (@{$self->_js_files()}) {
    	$index_data .= qq~
    <script type="text/javascript" src="${static_folder}${js_file}"></script>~;
    }
	$index_data .= qq~
    <script type="text/javascript" src="${static_folder}lightbox/js/prototype.js"></script>
    <script type="text/javascript" src="${static_folder}lightbox/js/scriptaculous.js"></script>
    <script id='lightboxScript' type="text/javascript" src="${static_folder}lightbox/js/lightbox.js"></script>
    <link rel="stylesheet" href="${static_folder}lightbox/css/lightbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${theme_uri}theme.css" type="text/css" media="screen" />
	<script type="text/javascript">
		var popup_display = $popup_display;
	</script>
  </head>
  <body class="louvre $background">
    <h1>${title_with_links}</h1>
              ~;

	if (!$popup_display) {
		$index_data .= qq~
		<div class="options-box">
			<div class="option-item">
				<input type="checkbox" $filename_chk onclick="\$\$('div.filename').invoke ('toggle');" id="toggle_filename" name="toggle_filename"/>
				<label for="toggle_filename">$toggle_filename_label</label>
			</div>
			<div class="option-item">
				<input type="checkbox" $description_chk onclick="\$\$('div.description').invoke ('toggle');" id="toggle_description" name="toggle_description"/>
				<label for="toggle_description">$toggle_description_label</label>
			</div>
			<div class="option-item">
				<input type="checkbox" $tags_chk onclick="\$\$('div.tags').invoke ('toggle');" id="toggle_tags" name="toggle_tags"/>
				<label for="toggle_tags">$toggle_tags_label</label>
			</div>
			<div class="option-item">
				<input type="checkbox" $dimensions_chk onclick="\$\$('div.dimensions').invoke ('toggle');" id="toggle_dimensions" name="toggle_dimensions"/>
				<label for="toggle_dimensions">$toggle_dimensions_label</label>
			</div>
			<div class="option-item">
				<input type="checkbox" $weight_chk onclick="\$\$('div.weight').invoke ('toggle');" id="toggle_weight" name="toggle_weight"/>
				<label for="toggle_weight">$toggle_weight_label</label>
			</div>
		</div>~;
	}

    # Subdirectories
    foreach my $item (sort @$subdirs) {
        my $item_name = $self->_sanitise($item);
        my $item_link = "${item_name}/index.html";
        $index_data .= qq~
    <div class="subgallery">
      <a href="${item_link}"><span title="${item_name}">${item_name}</span></a>
    </div>
                ~;
    }
    $index_data .= qq~
    <div class="thumbnail_start"></div>~; # to separate subgalleries from thumbnails, if desired

	my $height = $self->{max_tn_height};
	my $width = $self->{max_tn_width};

	# Get group ident for link to Magellan
	my $gallery_uri = Mioga2::URI->new($self->{config}, uri => $self->{uri});
	my $group_ident = $gallery_uri->GetGroupIdent ();

    # Pictures
    foreach my $item (sort @$files) {
		my $data = $self->{db}->SelectSingle ('SELECT uri_data.tags, uri_data.description FROM uri_data, m_uri WHERE uri_data.uri_id = m_uri.rowid AND m_uri.uri = ?', [$self->{uri} . "/$item"]);
        my $item_name = $self->_sanitise("$item");
		my $item_name_visibility = $self->{show_filename} ? 'block' : 'none';
        my $item_img = $self->_thumbnail_file($item); 
        my $item_path = "${rel_prefix}../${folder}/${item_name}";
		$data->{description} ||= ''; my $item_description = st_FormatXMLString ($data->{description}); $item_description ||= '';
		my $item_description_visibility = $self->{show_description} ? 'block' : 'none';
		$data->{tags} ||= ''; my $item_tags = st_FormatXMLString ($data->{tags}); $item_tags ||= ''; $item_tags =~ s/;/ /g;
		my $item_tags_visibility = $self->{show_tags} ? 'block' : 'none';

		# Get download URL
		my $download_url = $self->{config}->GetBinURI () . '/' . $group_ident . '/Magellan/DownloadFiles?path=' . $gallery_uri->GetURI () . '&entries=' . $gallery_uri->GetURI () . '/' . $folder . '/' . $item_name;

		# Get image weight and dimensions
		my $uri = Mioga2::URI->new ($self->{config}, uri => $self->_fulluri ("$folder/$item"));
		my $real_path = $uri->GetRealPath ();
		my $item_weight = $self->_ScaleValue (-s $real_path);
		my $item_weight_visibility = $self->{show_weight} ? 'block' : 'none';
		@$im = ();
		$im->Read ($real_path);
		my ($x, $y) = $im->Get ('columns', 'rows');
		my $item_dimensions = "$x x $y " . __('pixels');
		my $item_dimensions_visibility = $self->{show_dimensions} ? 'block' : 'none';

        # We need two different links, one for the thumbnail and one for its title.
        # To avoid having two links for the same image in Lightbox' slideshow,
        # we create two separate sets for each bunch of links.
		$item_path =~ s/\"/%22/g;
		$item_img =~ s/\"/%22/g;
	  	my $style_dimension = ($x > $y) ? "width: ${width}px;" : "height: ${height}px;";
	  	my $tn_orient = ($x > $y) ? "horizontal" : "vertical";
        $index_data .= qq~
    <div class="thumbnail $tn_orient" style="$style_dimension">
      <div class="thumbnail_img">
        <div class="thumbnail_img_inner">~;
		if (!$popup_display) {
			$index_data .= qq~<a href="${item_path}" rel="lightbox[tn]" tags="${item_tags}" description="${item_description}" title="${item_name}" download="$download_url" dimensions="$item_dimensions" weight="$item_weight">~;
		}
        $index_data .= qq~<img src="./${item_img}">~;
		if (!$popup_display) {
			$index_data .= qq~</a>~;
		}
		$index_data .= qq~
        </div>
      </div>~;
	  if ($popup_display) {
	  $index_data .= qq~
	  <div class="preview" style="display: none;">
          <a href="${item_path}" rel="lightbox[tn]" tags="${item_tags}" description="${item_description}" title="${item_name}" download="$download_url" dimensions="$item_dimensions" weight="$item_weight"><img src="./${item_img}"></a>
		  <a href="${item_path}" rel="lightbox[titles]" tags="${item_tags}" description="${item_description}" title="${item_name}" download="$download_url" dimensions="$item_dimensions" weight="$item_weight"><div class="filename" title="${item_name}" style="display: ${item_name_visibility};">${item_name}</div></a>
		  <div class="description" title="${item_description}" style="display: $item_description_visibility;">${item_description}</div>
		  <div class="tags" title="${item_tags}" style="display: $item_tags_visibility;">${item_tags}</div>
		  <div class="dimensions" title="${item_dimensions}" style="display: $item_dimensions_visibility;">${item_dimensions}</div>
		  <div class="weight" title="${item_weight}" style="display: $item_weight_visibility;">${item_weight}</div>
	  </div>
    </div>
                ~;
	  }
	  else {
	  	$index_data .= qq~
		  <a href="${item_path}" rel="lightbox[titles]" title="${item_name}"><div class="filename" title="${item_name}" style="display: ${item_name_visibility};">${item_name}</div></a>
		  <div class="description" title="${item_description}" style="display: $item_description_visibility;">${item_description}</div>
		  <div class="tags" title="${item_tags}" style="display: $item_tags_visibility;">${item_tags}</div>
		  <div class="dimensions" title="${item_dimensions}" style="display: $item_dimensions_visibility;">${item_dimensions}</div>
		  <div class="weight" title="${item_weight}" style="display: $item_weight_visibility;">${item_weight}</div>
		</div>
		~;
	  }
    }

    if (@$subdirs + @$files == 0) {
    	$index_data .= qq~<p class="no_image">~
    			. __('No image in this gallery.')
    			. qq~</p>~;
    }

    $index_data .= q~
  </body>
</html>
            ~;
    
    my $thumbdir_name = $self->_thumbnail_dir($folder);
    my $index_base = "$thumbdir_name/index.html";
    # my $index_full = $self->_fullpath($index_base);
    my $index_uri = $self->_fulluri($index_base);

	# Ensure index charset is correct
	utf8::encode ($index_data);
    
    unless ($self->{davfsutils}->PutFile($index_uri, \$index_data)) {
    	throw Mioga2::Exception::Application("Mioga2::Louvre::Gallery::_create_index",
    			"Cannot write index $index_uri");
    }
}

sub _ScaleValue {
	my ($self, $value) = @_;

	my @units = qw/B kB MB GB/;
	my $unit = shift (@units);

	while ($value > 1024) {
		$value /= 1024;
		$unit = shift (@units);
	}

	return (int ($value) . __($unit));
}


# Find out the number of levels of the given "folder" below our root dir.
# Example: _tree_depth("./subdir1/subdir2") == 2
# Returns an integer.
sub _tree_depth {
    my ($self, $folder) = @_;
    $folder =~ s#(?:^|/)\./##g; # forget about "./"
    $folder =~ s#/\+##g;        # fold multiple slashes
    
    my $result = ($folder eq '.' || $folder eq '' ? 0 : ($folder =~ s#/#/#g) + 1);
    print STDERR "[Mioga2::Louvre::Gallery::_tree_depth] for $folder is $result\n"
    		if $debug > 1;
    return $result;
}


# Imagine you edit a HTML file in "folder". You want to link to some
# general file (CSS file, folder symbol) in the "slideshow root dir"
# (which is $self->_thumbnail_dir('').
# This method gives you the correct prefix string (e.g. "../../") for the link.
sub _relative_link_prefix_to_root {
    my ($self, $folder) = @_;
    return '../' x $self->_tree_depth($folder);
}


# Generate automatically a file "auto_style.css" in the top-level gallery directory.
sub _create_auto_style_CSS {
    my ($self) = @_;
	my $h = int ($self->{max_tn_width} / 1.5);
	my $w = int ($self->{max_tn_height} / 1.5);
	my $margin = $self->{max_tn_height} - $h + 5;	# +5px because normal margin is 5px in louvre-gallery-default.css
    my $css_data = qq~
.thumbnail {
    float:left;
    text-align:center;
    white-space:nowrap;
}

.thumbnail.horizontal {
	margin-top: ${margin}px;
}

.thumbnail_img {
    position:relative;
    text-align:center;
}

.horizontal .thumbnail_img {
    width:  $self->{max_tn_width}px;
	height: ${h}px;
}

.vertical .thumbnail_img {
    height: $self->{max_tn_height}px;
	width: ${w}px;
}

.thumbnail_img_inner {
    position:absolute;
    bottom:0;
    left:0;
    width:100%;
	height:100%;
}

.horizontal .thumbnail_img_inner img {
	width: 100%;
}

.vertical .thumbnail_img_inner img {
	height: 100%;
}
            ~;
    my $css_uri = $self->_fulluri($self->_path_to_static_files().'/auto_style.css');
	$self->{davfsutils}->PutFile($css_uri, \$css_data);
}


# Create the slideshow directory and copy necessary files there
sub _init_slideshow_dir {
	my ($self) = @_;
	my ($success, $error);
    my $davfsutils = $self->{davfsutils};
    
    my $imagesUri = $self->_fulluri('');
    unless ($davfsutils->IsCollection($imagesUri)) {
    	print STDERR "[Mioga2::Louvre::Gallery::_init_slideshow_dir] Cannot create gallery: $imagesUri is not an accessible directory\n";
        return __("Cannot create gallery");
    }

    my $galleryUri = $self->_fulluri($self->_thumbnail_dir(''));
    
    if ( $davfsutils->Exists($galleryUri) ) {
    	print STDERR "[Mioga2::Louvre::Gallery::_init_slideshow_dir] Cannot create gallery: $galleryUri exists\n";
        return __x("Cannot create gallery: A file or folder with the name {subfolder} already exists",
        		subfolder => $Mioga2::Louvre::Const::LOUVRE_DIR);
    }

    $davfsutils->MakeCollection($galleryUri)
    		or return __("Cannot create gallery");
	$davfsutils->SetHidden ($galleryUri . '/', 1);
    
    my $staticfiles_uri = $self->_fulluri($self->_path_to_static_files());
	$davfsutils->MakeCollection($staticfiles_uri)
			or return __("Cannot create gallery");

	# Recursive copies
	$success = $davfsutils->PutCollection($staticfiles_uri.'/lightbox', 
			$self->{config}->GetJslibDir()."/lightbox");
	unless ($success) {
		print STDERR "[Mioga2::Louvre::Gallery::_init_slideshow_dir] Cannot copy Lightbox to $staticfiles_uri\n";
    	return __("Cannot create gallery");
	}
	
	# Copies of single files
    my $theme_path = $self->_theme_path();
	for my $basename (qw/louvre-gallery-default.css bindings.xml louvre-gallery.js/){
		my $subdir = 'css';
		$subdir = 'javascript' if ($basename =~ /\.js$/);
		$success = $self->{davfsutils}->PutFile(
				"${staticfiles_uri}/${basename}", "${theme_path}/$subdir/${basename}");
		unless ($success) {
			return __("Cannot create gallery");
		}
	}

	$self->_create_auto_style_CSS();

	return undef;
}

# Does some basic escaping for putting a string into HTML double quotes.
sub _sanitise {
    my ($self, $str) = @_;
    $str =~ s#"#&quot;#g ;
    return $str;
}


# === Theme functions. Be careful about vocabulary:
# "Theme" is the current Mioga theme. "Slideshow theme" refers just to a CSS file
# for the gallery generated by Louvre.

# Class or instance method, get the full path to the (Mioga) theme.
# If called as class method, Config and Group must be specified.
sub _theme_path {
	my ($clob, $config, $group) = @_;
	if (ref($clob) eq __PACKAGE__) {
		$config = $clob->{config};
		$group  = $clob->getGroup();
	}
	return $config->GetThemesDir() . '/' . $group->GetTheme();
}

sub _theme_uri {
	my ($clob, $config, $group) = @_;
	if (ref($clob) eq __PACKAGE__) {
		$config = $clob->{config};
		$group  = $clob->getGroup();
	}
	return $config->GetBaseURI() . '/themes/' . $group->GetTheme() . '/';
}


# Convenience class method: Return all "parent URIs" (and the URI itself).
# Takes a single URI string and returns an arrayref of URI strings.
sub parent_uris {
	my ($class, $uri) = @_;

	# Unescape slashes
	$uri =~ s~\\\/~~g;

	my @path_components = split('/', $uri);
	return [] if @path_components <= 1;
    shift(@path_components) if $path_components[0] eq '';

    my @results = ();
    my $constructed_path = '';
    foreach my $path_component (@path_components) {
    	$constructed_path .= "/$path_component";
    	push @results, $constructed_path;
    }

    return \@results;
}

# =========
#    Mioga2::Louvre::Gallery::SQL, a helper class for Mioga2::Louvre::Gallery
#    (1:1 relation) that contains all the interactions with the database.
#    May use internal properties of Mioga2::Louvre::Gallery.
# 		=========

package Mioga2::Louvre::Gallery::SQL;
use strict;
use warnings;
use open ':encoding(utf8)';

use Error qw(:try);
use Fcntl ':mode';
use File::MimeInfo::Magic;
use File::stat ();
use IO::Dir;
use Mioga2::Exception::Application;
use Mioga2::Exception::DB;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Sys::Syslog;

sub new {
    my ($class, $gallery) = @_;
    my $self = {
    	gallery => $gallery,
    	dbh     => $gallery->{config}->GetDBH(),
    };
    bless $self, $class;
    return $self;
}


# Register the newly created gallery into the database.
sub register {
	my ($self) = @_;
	my $gallery = $self->{gallery};

	try {
		ExecSQL($self->{dbh},
			 	"INSERT INTO m_louvre (uri_id, max_tn_width, max_tn_height, locale, show_filename, show_description, show_tags, show_dimensions, show_weight, popup_display)"
						. " SELECT " . join(',',
								"m_uri.rowid",
								$gallery->{max_tn_width},
								$gallery->{max_tn_height},
								$self->{dbh}->quote ($gallery->{locale}),
								$gallery->{show_filename} ? 'true' : 'false',
								$gallery->{show_description} ? 'true' : 'false',
								$gallery->{show_tags} ? 'true' : 'false',
								$gallery->{show_dimensions} ? 'true' : 'false',
								$gallery->{show_weight} ? 'true' : 'false',
								$gallery->{popup_display} ? 'true' : 'false',
								)
						. " FROM m_uri WHERE m_uri.uri=".$self->{dbh}->quote ($gallery->{uri})
				);
		ExecSQL ($self->{dbh}, "UPDATE m_uri SET modified = now() WHERE uri || '/' SIMILAR TO ? || '/%' AND uri NOT SIMILAR TO '%mioga2-louvre%';", [$gallery->{uri}]);
	} catch Mioga2::Exception::DB with {
		my $ex  = shift;
		print STDERR $ex->as_string."\n";
		throw Mioga2::Exception::Application("Mioga2::Louvre::Gallery::SQL::register", "Cannot register gallery");
	};
}


# Give back the attributes that have been stored for this gallery
sub load {
	my ($self) = @_;
	my $gallery = $self->{gallery};
	return SelectSingle($self->{dbh},
			"SELECT l.max_tn_width, l.max_tn_height, l.locale, l.show_filename, l.show_description, l.show_tags, l.show_dimensions, l.show_weight, l.popup_display FROM m_louvre l"
			. 	" INNER JOIN m_uri u ON l.uri_id = u.rowid"
			.   " WHERE u.uri=?", [ $gallery->{uri} ]);
}


sub unregister {
	my ($self) = @_;
	my $gallery = $self->{gallery};

	try {
		ExecSQL($self->{dbh},
				"DELETE FROM m_louvre WHERE uri_id IN"
				.	" (SELECT rowid FROM m_uri WHERE uri='".st_FormatPostgreSQLString($gallery->{uri})."')");
		ExecSQL ($self->{dbh}, "UPDATE m_uri SET modified = now() WHERE uri || '/' SIMILAR TO ? || '/%' AND uri NOT SIMILAR TO '%mioga2-louvre%';", [$gallery->{uri}]);
	} catch Mioga2::Exception::DB with {
		my $ex  = shift;
		print STDERR $ex->as_string."\n";
		throw Mioga2::Exception::Application("Mioga2::Louvre::Gallery::SQL::unregister", "Cannot unregister gallery");
	};

	return undef;
}


# Class method. Find all galleries whose base URI corresponds
# _exactly_ to one of the given URIs.
# Needs the _global_ configuration object (Mioga2::MiogaConf) and an arrayref of URIs to look for.
# (With the global configuration object and for each URI,
# instance configurations (Mioga2::Config) are loaded.
# These might also get cached inside Mioga2::Notifier::Louvre, for improved performance.)
# Returns a listref of instantiated Mioga2::Louvre::Gallery objects.
# Called from the notifier.
sub galleries_for_URIs {
	my ($class, $miogaconf, $uris) = @_;
	return [] unless (@$uris > 0);

	my $sql = "SELECT u.uri AS uri, l.max_tn_width AS max_tn_width,"
				. 	" l.max_tn_height AS max_tn_height, l.locale, l.show_filename, l.show_description, l.show_tags, l.show_dimensions, l.show_weight, l.popup_display"
				. " FROM m_louvre l"
				.	" INNER JOIN m_uri u ON u.rowid=l.uri_id"
				. " WHERE u.uri IN ("
				.		join(',', (map { "'".st_FormatPostgreSQLString($_)."'" } @$uris))
				.		")";
	my $results;
	try {
		$results = SelectMultiple($miogaconf->GetDBH(), $sql);
	} catch Mioga2::Exception::DB with {
		my $ex  = shift;
		syslog("warning", "Louvre::Gallery::SQL->galleries_for_URI: ".$ex->as_string);
		return [];
	};

	# Separate the found galleries according to their instances,
	# as different Mioga2::Config objects will be needed.
	my %instances_galleries = (); # hash: instance name => listref of galleries
	my $base_prefix = $miogaconf->GetBasePath();
	foreach my $result (@$results) {
		if ( $result->{uri} =~ m#^\Q$base_prefix\E/([^/]*)# ) {
			my $instance = $1;
			if (exists($instances_galleries{$instance})) {
				push @{$instances_galleries{$instance}}, $result;
			} else {
				$instances_galleries{$instance} = [ $result ];
			}
		}
	}

	my @galleries = ();
	foreach my $instance (keys %instances_galleries) {
		my $config = Mioga2::Config->new($miogaconf, $instance);
		foreach my $result (@{$instances_galleries{$instance}}) {
			my $uri = $result->{uri};
			delete $result->{uri};
			try {
				my $gallery = Mioga2::Louvre::Gallery->new($config, 
						$uri, undef, %$result);
				push @galleries, $gallery;
			} catch Mioga2::Exception::Application with {
				my $ex = shift;
				syslog("warning", "Louvre::Gallery::SQL->galleries_for_URI: ".$ex->as_string);
			}
		}
	}
	return \@galleries;
}


# Returns { type => 'g' } if the given URI is a gallery;
# returns { type => 's', path => $path } if the given URI is a gallery subfolder; "path" points to the root gallery folder;
# returns { type => 'n' } otherwise.
sub uri_is_gallery {
	my ($class, $dbh, $uri) = @_;

    # Cut URI into pieces
    my $uris = Mioga2::Louvre::Gallery->parent_uris($uri);
    return { type => 'n' } if @$uris == 0;

    my $sql = "SELECT m_uri.uri AS uri, m_uri.rowid AS rowid FROM m_uri"
        		. " INNER JOIN m_louvre ON m_uri.rowid = m_louvre.uri_id"
        		. " WHERE m_uri.uri IN ("
        		.		join(',', (map { "'".st_FormatPostgreSQLString($_)."'" } @$uris))
        		.       ")";

    my $louvre_results = SelectMultiple($dbh, $sql);

	if (grep({ $_->{uri} eq $uri} @$louvre_results)) {
		return { type => 'g' };
	} elsif (@$louvre_results > 0) {
		# Return the longest (most-specific) URI
		my $most_specific_uri = '';
		my $most_specific_uri_id;
		foreach my $louvre_result (@$louvre_results) {
			my $louvre_uri = $louvre_result->{uri};
			if (length($louvre_uri) > length($most_specific_uri)) {
				$most_specific_uri = $louvre_uri;
				$most_specific_uri_id = $louvre_result->{rowid};
			}
		}
		return { type => 's', path => $most_specific_uri, rowid => $most_specific_uri_id };
	} else {
		return { type => 'n' };
	}
}


sub load_group_and_user_IDs {
	my ($self) = @_;
	my $sql = "SELECT group_id, user_id FROM m_uri WHERE uri="
			. "'".st_FormatPostgreSQLString($self->{gallery}{uri})."'";
	my $result = SelectSingle($self->{dbh}, $sql);
	return ($result->{group_id}, $result->{user_id});
}

1;
