#===============================================================================
#
#         FILE:  Redirect.pm
#
#  DESCRIPTION:  Redirect handler.
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  03/02/2012 16:30
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME
	
Redirect.pm: Redirect handler.

=head1 DESCRIPTION

This module is a ResponseHandler that handles redirections. It will redirect
requests from /base/instance to
/base/instance/bin/default_group/default_app/DisplayMain.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Redirect;

use Mioga2::InstanceList;
use Data::Dumper;

my $debug = 0;


sub handler : method {
	my($class, $r, $data) = @_;

	my $uri = $r->uri ();
	print STDERR "[Mioga2::Redirect::handler -- $$] Entering, URI: $uri, data: " . Dumper $data if ($debug);


	#-----------------------------------------------------------
	# Load Mioga Config
	#-----------------------------------------------------------
	if(!defined ($r->dir_config("MiogaConfig"))) {
		$r->log_error("[Mioga2::Router::handler -- $$] Can't find MiogaConfig parameter");
		$r->log_error("[Mioga2::Router::handler -- $$] Add \"PerlSetVar MiogaConfig /path/to/Mioga.conf\" in your apache configuration file");
		return Mioga2::Apache::HTTP_SERVER_ERROR;
	}
	my $miogaconf = Mioga2::MiogaConf->new ($r->dir_config("MiogaConfig"));
	my $db = $miogaconf->{db};


	#-------------------------------------------------------------------------------
	# Initialize redirect URL
	#-------------------------------------------------------------------------------
	if ($data->{instance} && !$data->{group} && !$data->{user}) {
		# Only instance given, redirect to instance target_url
		my $instance = Mioga2::InstanceList->new ($miogaconf, { attributes => { ident => $data->{instance} } });
		if (my $redirect = $instance->Get ('target_url')) {
			$uri .= $redirect;
		}
		else {
			$r->log_error("[Mioga2::Redirect::handler -- $$] No redirection found for URI " . $r->uri);
			return Mioga2::Apache::HTTP_NOT_FOUND;
		}
	}
	elsif ($data->{instance} && $data->{group}) {
		# Instance and group, redirect to group's default application
		my $config = Mioga2::Config->new ($miogaconf, $data->{instance});
		my $group = Mioga2::GroupList->new ($config, { attributes => { ident => $data->{group} } });
		$uri .= '/' . $group->Get ('default_app') . '/DisplayMain';
		# $uri =~ s/$data->{group}/bin\/$data->{group}/;
		my $group_ident = $data->{group};
		$uri =~ s{/$group_ident/}{/bin/$group_ident/};
	}
	elsif ($data->{instance} && $data->{user}) {
		# Instance and user (may be __MIOGA-USER__ macro), redirect to user's Workspace unless application is defined
		my $application = ($data->{application}) ? $data->{application} : 'Workspace';

		# Add private bin member
		my $user_ident = $data->{user};
		$uri =~ s{/user(|/.*)$}{/bin/$user_ident/$application/DisplayMain};
	}
	elsif ($data->{url_hash}) {
		# Short URL redirect
		my $res = $db->SelectSingle ('SELECT target FROM short_url WHERE hash=?;', [$data->{url_hash}]);
		if ($res && $res->{target}) {
			$uri = $res->{target};
		}
		else {
			$r->log_error ("[Mioga2::Router::handler -- $$] Unknown short URL hash $data->{url_hash}");
			return Mioga2::Apache::HTTP_SERVER_ERROR;
		}
		print STDERR "Redirecting to $data->{url_hash}\n";
	}
	else {
		$r->log_error ("[Mioga2::Router::handler -- $$] Inconsistent data coming from previous handler: " . Dumper $data);
		return Mioga2::Apache::HTTP_SERVER_ERROR;
	}

	print STDERR "[Mioga2::Redirect::handler -- $$] Leaving, redirecting to $uri\n" if ($debug);
	$r->headers_out->{Location} = $uri;
	return Mioga2::Apache::HTTP_REDIRECT;
}



1;

