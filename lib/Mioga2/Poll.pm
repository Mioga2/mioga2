# ============================================================================
# Mioga2 Project (C) 2006-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#   Description:

=head1 NAME
	
Poll.pm: the Mioga2 Poll application

=head1 METHODS DESRIPTION

=cut

# ============================================================================

package Mioga2::Poll;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'poll';

use Error qw(:try);
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use MIME::Entity;
use Data::Dumper;
use POSIX;
use XML::Atom::SimpleFeed;

# constants
use constant ANIM        => "Anim";
use constant READ        => "Read";
use constant WAIT        => "wait";
use constant NOW         => "now";
use constant FIXED       => "fixed";
use constant PERCENT     => "percent";
use constant FREE_ANSWER => "FREE";

my $debug = 0;

# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc {
    my $self = shift;

    my %AppDesc = (
        ident              => 'Poll',
        name               => __('Poll'),
        package            => 'Mioga2::Poll',
        description        => __('The Mioga2 Poll application'),
        type               => 'normal',
        all_groups         => 1,
        all_users          => 0,
        can_be_public      => 0,
		is_user             => 0,
		is_group            => 1,
		is_resource         => 0,

        functions => {
            'Anim' => __('Animation methods'),
            'Read' => __('Consultation methods'),
        },

        func_methods => {
            'Anim' => [ 'DisplayAnim', 'CreatePoll', 'EditPoll',      'DisplayOptions', 'DeleteQuestion', 'UpdateOptions', 'AddQuestion', 'EditQuestion', 'UpdateQuestion', 'UpdatePoll', 'DeletePoll' ],
            'Read' => [ 'DisplayMain', 'AnswerPoll', 'UpdateAnswers', 'ViewResults' ],
        },

        sxml_methods => {},

        xml_methods => {},

		non_sensitive_methods => [
			'DisplayAnim',
			'CreatePoll',
			'EditPoll',
			'DisplayOptions',
			'UpdateOptions',
			'AddQuestion',
			'EditQuestion',
			'UpdateQuestion',
			'UpdatePoll',
			'DisplayMain',
			'AnswerPoll',
			'UpdateAnswers',
			'ViewResults',
		]
    );

    return \%AppDesc;
}

# ============================================================================
# Public Methods
# ============================================================================

# ============================================================================

=head2 DisplayMain ()

=over

=item Main method. It displays the list of currently available polls.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayMain {
    my ($self, $context) = @_;
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<DisplayMain>";
    $xml .= $context->GetXML();
    $xml .= $self->XMLGetAccessRights($context);
    $xml .= $self->GetPolls(context => $context, published => 1);

    $xml .= "</DisplayMain>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 AnswerPoll ()

=over

=item Displays a poll question by question to be answered by an user.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub AnswerPoll {
    my ($self, $context) = @_;

    #
    # Get and verify parameters
    #
    my ($values, $errors) = ac_CheckArgs($context, [ [ ['rowid'], 'disallow_empty', 'want_int' ],
	                                                 [ ['page'], 'allow_empty', 'stripxws', 'want_int' ] ]);

    if (scalar(@$errors) > 0) {
        throw Mioga2::Exception::Application("Mioga2::Poll::AnswerPoll", ac_FormatErrors($errors));
    }
    my $rowid = $values->{rowid};
    my $page = $values->{page};
	$page = 0 if ($page eq '');

    my $poll = $self->GetCurrentPoll($context);

    if (not defined $poll or $poll->{rowid} != $rowid) {
        my $poll_db = $self->DBGetPolls(context => $context, id => $rowid);
        $poll_db = pop @$poll_db;
        $poll = $self->InitSession(context => $context, poll => $poll_db);
    }
    if ($poll->{validated} == 1 or $poll->{closed}) {
        my $content = new Mioga2::Content::REDIRECT($context);
        $content->SetContent("DisplayMain");
        return $content;
    }

    my $total_questions = scalar @{ $poll->{questions_list} };
    throw Mioga2::Exception::Application("Poll::AnswerPoll", __ "'page' can't exceed the total number of pages!") if ($page >= $total_questions);

    my $question        = ${ $poll->{questions_list} }[$page];
    my $answers_percent = int(($page * 100) / $total_questions);
    my $answer          = $self->DBGetAnswer(context => $context, question_index => $page);

    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<AnswerPoll>";
    $xml .= $context->GetXML();
    $xml .= $self->XMLGetPoll(context => $context, poll => $poll);
    $xml .= $self->XMLGetQuestion(context => $context, question => $question);
    $xml .= $self->XMLGetAnswer(context => $context, answer => $answer);
    $xml .= "<CurrentQuestion>" . st_FormatXMLString(($page + 1)) . "</CurrentQuestion>";
    $xml .= "<AnswersPercent>" . st_FormatXMLString($answers_percent) . "</AnswersPercent>";
    $xml .= "</AnswerPoll>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 DBGetResults (I<%options>)

=over

=item database method to retrieve results of a poll.

=item B<parameters:>

=over

=item I<$options{context}>: current request context.

=item I<$options{rowid}>: rowid of a poll.

=back

=item B<return:> list of results.

=back

=cut

# ============================================================================

sub DBGetResults {
    my ($self, %options) = @_;
    my $context = $options{context};
    my $rowid   = $options{rowid};
    my $dbh     = $context->GetConfig()->GetDBH();

    # SQL request to retrieve all choices with selected ones by users.
    # One request selects all available choices for all questions of a given poll.
    # Second one gets all the answers of all users of the same given poll.
    my $sql = "SELECT all_choices.*, all_answers.* FROM (SELECT poll_question.label AS q_label, poll_question.type AS q_type, poll_question.rowid AS question_id, poll_choice.label AS c_label, poll_choice.rowid AS c_id FROM poll_question LEFT JOIN poll_choice ON poll_question.rowid=poll_choice.question_id WHERE poll_question.poll_id=$rowid) AS all_choices LEFT JOIN (SELECT poll_user_answer.user_id, poll_answer.text, poll_answer.question_id, poll_choice.label, poll_choice.rowid AS sel_id, (m_user_base.firstname||' '||m_user_base.lastname) AS user_name FROM m_user_base, poll_user_answer LEFT JOIN poll_answer ON poll_user_answer.rowid=poll_answer.ua_id LEFT JOIN poll_answer_choice ON poll_answer.rowid=poll_answer_choice.answer_id LEFT JOIN poll_choice ON poll_answer_choice.choice_id=poll_choice.rowid WHERE poll_user_answer.validated=true AND m_user_base.rowid=poll_user_answer.user_id AND poll_user_answer.poll_id=$rowid) AS all_answers ON all_choices.question_id=all_answers.question_id ORDER BY all_choices.question_id, all_answers.user_id, all_choices.c_id";

    my $results = SelectMultiple($dbh, $sql);
    return $results;
}

# ============================================================================

=head2 GetQuestionsForResults (I<%options>)

=over

=item make a hash of questions from results.

=item B<parameters:>

=over

=item I<$options{results}>: results as a hash.

=back

=item B<return:> a hash of questions.

=back

=cut

# ============================================================================

sub GetQuestionsForResults {
    my ($self, %options) = @_;
    my $results = $options{results};

    my $questions = [];
    foreach my $result (@$results) {

        $$questions[ $result->{question_id} ] = {} unless ($$questions[ $result->{question_id} ]);
        my $question = $$questions[ $result->{question_id} ];

        $question->{rowid}   = $result->{question_id};
        $question->{type}    = $result->{q_type};
        $question->{label}   = $result->{q_label};
        $question->{choices} = [] unless ($question->{choices});

        if ($result->{c_id}) {
            my $choices = $question->{choices};
            $$choices[ $result->{c_id} ] = {} unless ($$choices[ $result->{c_id} ]);

            my $choice = $$choices[ $result->{c_id} ];
            $choice->{rowid} = $result->{c_id};
            $choice->{label} = $result->{c_label};
        }
    }
    return $questions;
}

# ============================================================================

=head2 ViewResults ()

=over

=item Displays results of a poll.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub ViewResults {
    ####
    # NEED TO BE REFACTORED !!
    ####
    my ($self, $context) = @_;
    #
    # Get and verify parameters
    #
    my ($values, $errors) = ac_CheckArgs($context, [ [ ['rowid'], 'disallow_empty', 'want_int' ],
												     [ ['format'], 'allow_empty', 'stripxws' ] ]);

    if (scalar(@$errors) > 0) {
        throw Mioga2::Exception::Application("Mioga2::Poll::ViewResults", ac_FormatErrors($errors));
    }

    my $rowid  = $values->{rowid};
    my $format = $values->{format};

    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<ViewResults>";
    $xml .= $context->GetXML();

    my $poll = $self->DBGetPolls(context => $context, id => $rowid);
    $poll = pop @$poll;

    my $results = $self->DBGetResults(context => $context, rowid => $rowid);
    my $questions = $self->GetQuestionsForResults(results => $results);

    $xml .= "<Results poll_id=\"" . st_FormatXMLString($rowid) . "\">";
    my $current_question = undef;
    my $user_id          = undef;
    my $user_hash        = { text => "" };

    foreach my $result (@$results) {
        next unless $result;

        if ($user_id != $result->{user_id} or $current_question != $result->{question_id}) {
            if ($user_id) {
                $xml .= $self->AddRowToResults(poll => $poll, user_hash => $user_hash, questions => $questions, current_question => $current_question);
                $user_hash = { text => "" };
            }
            $user_id = $result->{user_id};
        }

        if ($current_question != $result->{question_id}) {
            $xml .= "</Question>" if ($current_question);
            $xml .= "<Question rowid=\"" . st_FormatXMLString($$questions[ $result->{question_id} ]->{rowid}) . "\">";
            $xml .= "<Headers>";
            $xml .= "<Header>" . st_FormatXMLString(__ "Question") . "</Header>";
            $xml .= "<Header>" . st_FormatXMLString(__ "User") . "</Header>";

            my $choices = $$questions[ $result->{question_id} ]->{choices};
            foreach my $choice (@$choices) {
                next unless $choice;
                $xml .= "<Header>" . st_FormatXMLString($choice->{label}) . "</Header>";
            }
            $xml .= "<Header>" . st_FormatXMLString(__ "Text") . "</Header>";
            $xml .= "</Headers>";
            $current_question = $result->{question_id};
        }

        $user_hash->{question}            = $result->{q_label};
        $user_hash->{name}                = $result->{user_name};
        $user_hash->{ $result->{sel_id} } = 1;
        $user_hash->{text}                = $result->{text} if ($result->{text});
    }

    if ($results) {
        $xml .= $self->AddRowToResults(poll => $poll, user_hash => $user_hash, questions => $questions, current_question => $current_question);
        $user_hash = { text => "" };
        $xml .= "</Question>";
    }

    $xml .= "</Results>";
    $xml .= "</ViewResults>";

    my $stylesheet = ($format eq 'csv') ? "poll-csv.xsl" : "poll.xsl";
    my %options = (stylesheet => $stylesheet, locale_domain => 'poll_xsl');
    $options{filename} = "poll-$poll->{rowid}.csv" if ($format eq 'csv');

    my $content = new Mioga2::Content::XSLT($context, %options);
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 AddRowToResults (I<%options>)

=over

=item add a row of information to results of a poll as xml.

=item B<parameters:>

=over

=item I<$options{poll}>: a poll object.

=item I<$options{user_hash}>: hash with current user infos.

=item I<$options{questions}>: hash containing a list of questions.

=item I<$current_question>: the current question processed.

=back

=item B<return:> a row of information as a xml string.

=back

=cut

# ============================================================================

sub AddRowToResults {
    my ($self, %options) = @_;
    my $poll             = $options{poll};
    my $user_hash        = $options{user_hash};
    my $questions        = $options{questions};
    my $current_question = $options{current_question};

    my $username = ($poll->{anonymous}) ? "anonymous" : $user_hash->{name};

    my $xml .= "<Row>";

    $xml .= "<Col>" . st_FormatXMLString($user_hash->{question}) . "</Col>";
    $xml .= "<Col>" . st_FormatXMLString($username) . "</Col>";

    my $choices = $$questions[$current_question]->{choices};
    foreach my $choice (@$choices) {
        next unless $choice;
        my $col_result = ($user_hash->{ $choice->{rowid} }) ? 1 : 0;
        $xml .= "<Col>" . st_FormatXMLString($col_result) . "</Col>";
    }

    $xml .= "<Col>" . st_FormatXMLString($user_hash->{text}) . "</Col>";
    $xml .= "</Row>";

    return $xml;
}

# ============================================================================

=head2 DisplayOptions ()

=over

=item Displays options of a poll in edit mode.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayOptions {
    my ($self, $context) = @_;
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<DisplayOptions>";
    $xml .= $context->GetXML();

    $xml .= $self->GetOptions(context => $context);

    $xml .= "</DisplayOptions>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll-js.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 DisplayAnim ()

=over

=item Animation page.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayAnim {
    my ($self, $context) = @_;

    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<DisplayAnim>";
    $xml .= $context->GetXML();
    $xml .= $self->GetPolls(context => $context, published => 0);
    $xml .= "<CanAnim/>";

    $xml .= "</DisplayAnim>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 CreatePoll ()

=over

=item Create a new poll.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub CreatePoll {
    my ($self, $context) = @_;

    my $rowid;
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<CreatePoll>";
    $xml .= $context->GetXML();
    my $poll;
    #
    # Get and verify parameters
    #
    my ($values, $errors) = ac_CheckArgs($context, [ [ ['rowid'], 'allow_empty', 'want_int' ],
	                                                 [ ['no_init'], 'allow_empty', 'bool' ] ]);

    if (scalar(@$errors) > 0) {
        throw Mioga2::Exception::Application("Mioga2::Poll::CreatePoll", ac_FormatErrors($errors));
    }
    $rowid = $values->{rowid};

    if (st_ArgExists($context, "no_init")) {
        $poll = $self->GetCurrentPoll($context);
        $xml .= $self->XMLGetCompletePoll(context => $context, poll => $poll, use_session => 1);
    }
    else {
        if ($rowid) {
            $poll = $self->DBGetPolls(context => $context, id => $rowid);
            $poll = pop @$poll;
        }

        $poll = $self->InitSession(context => $context, poll => $poll);
        $poll->{referer} = "CreatePoll?no_init=1";
        $poll->{rowid}   = 0;

        if ($rowid) {
            $xml .= $self->XMLGetCompletePoll(context => $context, poll => $poll, use_session => 1);
        }
        else {
            $xml .= "<Poll/>";
        }
    }

    $xml .= "</CreatePoll>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 GetCurrentPoll (I<$context>)

=over

=item retrieve current poll held in session.

=item B<parameters:>

=over

=item I<$context>: current request context.

=back

=item B<return:> a poll object.

=back

=cut

# ============================================================================

sub GetCurrentPoll {
    my ($self, $context) = @_;

    return $context->GetSession()->{poll};
}

# ============================================================================

=head2 EditPoll ()

=over

=item Edit a poll.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub EditPoll {
    my ($self, $context) = @_;

    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<EditPoll>";
    $xml .= $context->GetXML();

    my $values = {};
    my $errors = [];

    ($values, $errors) = ac_CheckArgs($context, [ [ ['rowid'], 'stripxws', 'disallow_empty', 'want_int' ], ]);

    if (not @$errors) {
        my $poll;

        if (st_ArgExists($context, "no_init")) {
            $poll = $self->GetCurrentPoll($context);
        }
        else {
            $poll            = $self->DBGetPolls(context => $context, id => $values->{rowid});
            $poll            = pop @$poll;
            $poll            = $self->InitSession(context => $context, poll => $poll);
            $poll->{referer} = "EditPoll?rowid=$values->{rowid}&no_init=1";
        }

        $xml .= $self->XMLGetCompletePoll(context => $context, poll => $poll, use_session => 1);
    }
    else {
        $xml .= ac_XMLifyErrors($errors);
    }

    $xml .= "</EditPoll>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 InitSession (I<%options>)

=over

=item Initialize session when editing or creating a poll.

=item B<parameters :>

=over

=item I<$options{context}>: current request context.

=item I<$options{poll}>: optional. Used in editing mode to initialize values from this poll.

=back

=item B<return :> a session object as a hash.

=back

=cut

# ============================================================================

sub InitSession {
    my ($self, %options) = @_;
    my $context = $options{context};

    my $session = $context->GetSession();
    $session->{poll} = {};
    $session = $session->{poll};

    $session->{title}          = "";
    $session->{description}    = "";
    $session->{questions}      = 0;
    $session->{questions_list} = [];
    $session->{rowid}          = 0;
    $session->{opening}        = WAIT;
    $session->{closing}        = undef;
    $session->{visible}        = 1;
    $session->{anonymous}      = 0;
    $session->{validated}      = 0;
    $session->{closed}         = 0;

    if (defined $options{poll}) {
        my $poll = $options{poll};

        $session->{title}       = $poll->{title};
        $session->{description} = $poll->{description};
        $session->{rowid}       = $poll->{rowid};
        if (not $poll->{opening}) {
            $session->{opening} = WAIT;
        }
        else {
            $session->{opening} = $poll->{opening};
        }
        $session->{closing}   = $poll->{closing};
        $session->{visible}   = $poll->{visible_for_all};
        $session->{anonymous} = $poll->{anonymous};

        my $questions = $self->DBGetQuestions(context => $context, poll_id => $session->{rowid});
        foreach my $question (@$questions) {
            push @{ $session->{questions_list} }, $question;
        }

        my $s_questions = $session->{questions_list};
        foreach my $idx (0 .. $#{$s_questions}) {
            $$s_questions[$idx]->{rowid} = $idx;

            my $s_choices = $$s_questions[$idx]->{choices};
            foreach my $c_idx (0 .. $#{$s_choices}) {
                $$s_choices[$c_idx]->{rowid} = $c_idx;
            }
        }
        $session->{questions} = scalar @$s_questions;
        $session->{validated} = $poll->{validated};
        $session->{closed}    = $self->IsPollClosed(context => $context, poll => $poll);
    }

    return $session;
}

# ============================================================================

=head2 GetPolls (I<%options>)

=over

=item Retrieve polls.

=item B<parameters :>

=over

=item I<$options{context}>: current request context.

=item I<$options{published}>: boolean for published polls or not (defaults to true).

=item I<$options{id}>: id for a particular poll.

=back

=item B<return :> Polls list as xml string.

=back

=cut

# ============================================================================

sub GetPolls {
    my ($self, %options) = @_;
    my $context = $options{context};

    my $xml   = "<Polls>";
    my $polls = $self->DBGetPolls(%options);

    foreach my $poll (@$polls) {
        $xml .= $self->XMLGetPoll(context => $context, poll => $poll);
    }
    $xml .= "</Polls>";
    return $xml;
}

# ============================================================================

=head2 GetOptions (I<%options>)

=over

=item Retrieve options from the current poll.

=item B<parameters :>

=over

=item I<$options{context}> : current request context.

=back

=item B<return :> options as a xml string.

=back

=cut

# ============================================================================

sub GetOptions {
    my ($self, %options) = @_;
    my $context = $options{context};
    my $poll    = $self->GetCurrentPoll($context);

    my $xml = "<Options>";
    $xml .= "<Visible>" . st_FormatXMLString($poll->{visible}) . "</Visible>";
    $xml .= "<Anonymous>" . st_FormatXMLString($poll->{anonymous}) . "</Anonymous>";

    my $start_date = WAIT;
    if ($poll->{opening} eq NOW) {
        $start_date = NOW;
    }
    elsif ($poll->{opening} =~ /\d{4}-\d{2}-\d{2}/) {
        $start_date = FIXED;
        $xml .= "<FixedStartDate>" . st_FormatXMLString($poll->{opening}) . "</FixedStartDate>";
        my @start_time = gmtime(du_ISOToSecond($poll->{opening}));
        $xml .= "<FixedStartDateFormatted>" . strftime(st_FormatXMLString(__ "%x"), @start_time) . "</FixedStartDateFormatted>";
    }
    $xml .= "<StartDate>" . st_FormatXMLString($start_date) . "</StartDate>";

    my $end_date;
    if ($poll->{closing} =~ /\d{4}-\d{2}-\d{2}/) {
        $end_date = FIXED;
        $xml .= "<FixedEndDate>" . st_FormatXMLString($poll->{closing}) . "</FixedEndDate>";
        my @end_time = gmtime(du_ISOToSecond($poll->{closing}));
        $xml .= "<FixedEndDateFormatted>" . strftime(st_FormatXMLString(__ "%x"), @end_time) . "</FixedEndDateFormatted>";
    }
    elsif ($poll->{closing} =~ /^\d{2,3}$/) {
        $end_date = PERCENT;
        $xml .= "<PercentEndDate>" . st_FormatXMLString($poll->{closing}) . "</PercentEndDate>";
    }
    $xml .= "<EndDate>" . st_FormatXMLString($end_date) . "</EndDate>";

    $xml .= "</Options>";

    return $xml;
}

# ============================================================================

=head2 UpdateOptions ()

=over

=item update options for a poll.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub UpdateOptions {
    my ($self, $context) = @_;

    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<UpdateOptions>";
    $xml .= $context->GetXML();

    my $values = {};
    my $errors = [];

    ($values, $errors) = ac_CheckArgs($context, [ [ [ 'visible', 'anonymous' ], 'stripxws', 'allow_empty', 'bool' ], [ [ 'start_date', 'end_date' ], 'stripxws', 'disallow_empty' ], [ [ 's_fixed_date', 'e_fixed_date', 'percent' ], 'stripxws', 'allow_empty' ], ]);

    if ($values->{e_fixed_date} eq '' and $values->{percent} eq '') {
        push @$errors, [ 'end_date', ['disallow_empty'] ];
    }

    if ($values->{percent} != '' and ($values->{percent} < 0 or $values->{percent} > 100)) {
        push @$errors, [ 'percent', ['want_percent'] ];
    }

    if ($values->{start_date} eq FIXED and $values->{s_fixed_date} eq '') {
        push @$errors, [ 'start_date', ['disallow_empty'] ];
    }

    if (not @$errors) {
        my $poll = $self->GetCurrentPoll($context);
        $poll->{visible}   = $values->{visible};
        $poll->{anonymous} = defined ($values->{anonymous}) ? $values->{anonymous} : 0;

        if ($values->{start_date} eq FIXED) {
            $poll->{opening} = $values->{s_fixed_date};
        }
        else {
            $poll->{opening} = $values->{start_date};
        }

        if ($values->{end_date} eq FIXED) {
            $poll->{closing} = $values->{e_fixed_date};
        }
        else {
            $poll->{closing} = $values->{percent};
        }
    }
    else {
        $xml .= ac_XMLifyErrors($errors);
        $xml .= $self->GetOptions(context => $context);
    }

    $xml .= "</UpdateOptions>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll-js.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 UpdateQuestion ()

=over

=item update or create question for a poll.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub UpdateQuestion {
    my ($self, $context) = @_;

    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<UpdateQuestion>";
    $xml .= $context->GetXML();

    my $values = {};
    my $errors = [];

    ($values, $errors) = ac_CheckArgs($context, [ [ [ 'q_label', 'q_type', 'page' ], 'stripxws', 'disallow_empty' ], [ [ 'fa_type', 'answers', 'rowid' ], 'stripxws', 'allow_empty' ], [ ['X-CSRF-Token'], 'disallow_empty', ['csrf_token', [$context->GetConfig(), $context->GetUser()]] ], ]);

    my $poll = $self->GetCurrentPoll($context);
    my $question = $self->MakeQuestionFrom(values => $values, poll => $poll);

    if ($values->{q_type} !~ /FREE/ and $values->{answers} eq '') {
        push @$errors, [ 'answers', ['disallow_empty'] ];
    }

    if (not @$errors) {
        if (exists $values->{rowid} and $values->{rowid} ne '') {
            ${ $poll->{question_list} }[ $values->{rowid} ] = $question;
        }
        else {
            push @{ $poll->{questions_list} }, $question;
        }
        my $content = new Mioga2::Content::REDIRECT($context);
        $content->SetContent($poll->{referer});
        return $content;
    }
    else {
        $question->{rowid} = undef;
        $xml .= ac_XMLifyErrors($errors);
    }

    $xml .= "<Referer>" . st_FormatXMLString($poll->{referer}) . "</Referer>";
    $xml .= $self->XMLGetQuestion(context => $context, question => $question);
    $xml .= "<Page>" . st_FormatXMLString($values->{page}) . "</Page>";
    $xml .= "</UpdateQuestion>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 UpdateAnswers ()

=over

=item add user answer to a poll.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub UpdateAnswers {
    my ($self, $context) = @_;

    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<UpdateAnswers>";
    $xml .= $context->GetXML();

    my $values = {};
    my $errors = [];

    ($values, $errors) = ac_CheckArgs($context, [ [ [ 'answer', 'poll_id', 'current_question' ], 'stripxws', 'disallow_empty', 'want_int' ], [ [ 'text-answer', 'continue', 'type' ], 'stripxws', 'allow_empty' ], [ ['X-CSRF-Token'], 'disallow_empty', ['csrf_token', [$context->GetConfig(), $context->GetUser()]] ], ]);

    my $poll      = $self->GetCurrentPoll($context);
    my $questions = $poll->{questions_list};

    if ((ref($values->{answer}) eq 'ARRAY' and grep(/text-answer/, @{ $values->{answer} })) or (ref($values->{answer}) ne 'ARRAY' and $values->{answer} eq "text-answer")) {
        if (not $values->{'text-answer'}) {
            push @$errors, [ 'text-answer', ['disallow_empty'] ];
        }
        else {
            if (${$questions}[ $values->{current_question} - 1 ]->{type} =~ /INTEGER/ and $values->{'text-answer'} !~ /^\d+$/) {
                push @$errors, [ 'text-answer', ['want_int'] ];
            }
            elsif (${$questions}[ $values->{current_question} - 1 ]->{type} =~ /FLOAT/ and $values->{'text-answer'} !~ /^\d+\.?\d*$/) {
                push @$errors, [ 'text-answer', ['want_float'] ];
            }
        }
    }

    if (not @$errors) {
        $self->DBInsertAnswer(context => $context, values => $values);

        $self->DBValidateAnswer(context => $context) if ($values->{current_question} == scalar @$questions);

        if ($values->{'continue'} or $values->{current_question} == scalar @$questions) {
            my $content = new Mioga2::Content::REDIRECT($context);
            $content->SetContent('DisplayMain');
            return $content;
        }

        my $content = new Mioga2::Content::REDIRECT($context);
        $content->SetContent('AnswerPoll?rowid=' . $values->{poll_id} . "&page=" . $values->{current_question});
        return $content;
    }

    $xml .= ac_XMLifyErrors($errors);

    my $total_questions = scalar @{ $poll->{questions_list} };
    my $question        = ${ $poll->{questions_list} }[ $values->{current_question} - 1 ];
    my $answers_percent = int((($values->{current_question} - 1) * 100) / $total_questions);

    my $answer = { text => $values->{text_answer} };
    $answer->{choices} = [];

    if (ref($values->{answer}) eq 'ARRAY') {
        foreach my $choice (@{ $values->{answer} }) {
            push @{ $answer->{choices} }, { label => $choice };
        }
    }
    else {
        push @{ $answer->{choices} }, { label => $values->{answer} };
    }

    $xml .= $self->XMLGetPoll(context => $context, poll => $poll);
    $xml .= $self->XMLGetQuestion(context => $context, question => $question);
    $xml .= $self->XMLGetAnswer(context => $context, answer => $answer);
    $xml .= "<CurrentQuestion>" . st_FormatXMLString($values->{current_question}) . "</CurrentQuestion>";
    $xml .= "<AnswersPercent>" . st_FormatXMLString($answers_percent) . "</AnswersPercent>";

    $xml .= "<Page>AnswerPoll</Page>";
    $xml .= "</UpdateAnswers>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 MakeQuestionFrom (I<%options>)

=over

=item make a question object from values.

=item B<parameters :>

=over

=item I<$options{values}>: values to describe the question.

=item I<$options{poll}>: poll of the question.

=back

=item B<return :> a question object (hash ref).

=back

=cut

# ============================================================================

sub MakeQuestionFrom {
    my ($self, %options) = @_;
    my $values = $options{values};
    my $poll   = $options{poll};

    my $question;
    if ($values->{rowid} ne "") {
        $question = ${ $poll->{questions_list} }[ $values->{rowid} ];
    }
    else {
        $question = { rowid => scalar @{ $poll->{questions_list} } };
    }

    $question->{type} = $values->{q_type};
    $question->{type} .= "_" . $values->{fa_type} if ($question->{type} =~ /(_F)|(FREE)/);
    $question->{label}   = $values->{q_label};
    $question->{choices} = [];

    my $choices = $question->{choices};
    if (ref($values->{answers}) eq 'ARRAY') {
        my $rowid = 0;
        foreach my $answer (@{ $values->{answers} }) {
            my $choice = { rowid => $rowid, label => $answer };
            push @$choices, $choice;
            $rowid++;
        }
    }
    elsif ($values->{answers}) {
        my $choice = { rowid => 0, label => $values->{answers} };
        push @$choices, $choice;
    }

    return $question;
}

# ============================================================================

=head2 UpdatePoll ()

=over

=item update or create a poll.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub UpdatePoll {
    my ($self, $context) = @_;

    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<UpdatePoll>";
    $xml .= $context->GetXML();

    my $values = {};
    my $errors = [];

	# text-answer is not used but it's a displayed field on form for questions
    ($values, $errors) = ac_CheckArgs($context, [ [ [ 'poll_title', 'poll_description', 'page' ], 'stripxws', 'disallow_empty' ],
	                                              [ [ 'text-answer', 'answer' ], 'stripxws', 'allow_empty' ],
												  [ ['X-CSRF-Token'], 'disallow_empty', ['csrf_token', [$context->GetConfig(), $context->GetUser()]] ], ]);

    my $poll = $self->GetCurrentPoll($context);
    $poll->{title}       = $values->{poll_title};
    $poll->{description} = $values->{poll_description};

    if (not @$errors) {
        if ($poll->{rowid} == 0) {
            $self->DBInsertPoll(context => $context, poll => $poll);
        }
        else {
            $self->DBUpdatePoll(context => $context, poll => $poll);
        }
        $self->NotifyUpdate($context, type => ['RSS']);
        my $content = new Mioga2::Content::REDIRECT($context);
        $content->SetContent('DisplayAnim');
        return $content;
    }

    $xml .= ac_XMLifyErrors($errors);

    $xml .= "<Page>" . st_FormatXMLString($values->{page}) . "</Page>";

    $xml .= "<Poll rowid=\"" . st_FormatXMLString($poll->{rowid}) . "\">";
    $xml .= "<Title>" . st_FormatXMLString($poll->{title}) . "</Title>";
    $xml .= "<Description>" . st_FormatXMLString($poll->{description}) . "</Description>";
    $xml .= $self->GetQuestionsFor(context => $context, use_session => 1);
    $xml .= "</Poll>";

    $xml .= "</UpdatePoll>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 GetQuestionsFor (I<%options>)

=over

=item Retrieve questions for a given poll.

=item B<parameters:>

=over

=item I<$options{context}>: current request context.

=item I<$options{poll}>: optional. rowid of a poll. needed if not using session.

=item I<$options{use_session}>: optional. If set to 1, use session instead of database.

=back

=item B<return:> list of questions for a poll as a xml string.

=back

=cut

# ============================================================================

sub GetQuestionsFor {
    my ($self, %options) = @_;
    my $context = $options{context};
    my $questions;
    $options{use_session} ||= 0;

    if ($options{use_session} == 1) {
        warn __ "using session" if $debug;
        $questions = $self->GetCurrentPoll($context)->{questions_list};
    }
    else {
        warn __ "using database" if $debug;
        $questions = $self->DBGetQuestions(poll_id => $options{poll}, context => $context);
    }

    my $xml .= "<Questions>";
    foreach my $question (@$questions) {
        $xml .= $self->XMLGetQuestion(context => $context, question => $question);
    }
    $xml .= "</Questions>";

    return $xml;
}

# ============================================================================

=head2 DeleteQuestion ()

=over

=item Delete an existing question from a poll.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DeleteQuestion {
    my ($self, $context) = @_;

    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<DeleteQuestion>";
    $xml .= $context->GetXML();

    my $values = {};
    my $errors = [];

    ($values, $errors) = ac_CheckArgs($context, [ [ ['idx'], 'stripxws', 'disallow_empty', 'want_int' ], ]);

    if (not @$errors) {
        my $poll = $self->GetCurrentPoll($context);
        splice @{ $poll->{questions_list} }, $values->{idx}, 1;
        $xml .= "<DeletedQuestion>" . st_FormatXMLString($values->{idx}) . "</DeletedQuestion>";
        $xml .= "<Questions>" . st_FormatXMLString((scalar @{ $poll->{questions_list} })) . "</Questions>";
    }
    else {
        $xml .= ac_XMLifyErrors($errors);
    }

    $xml .= "</DeleteQuestion>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll-js.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 DeletePoll ()

=over

=item Delete an existing poll.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DeletePoll {
    my ($self, $context) = @_;

    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<DeletePoll>";
    $xml .= $context->GetXML();
    #
    # Get and verify parameters
    #
    my ($values, $errors) = ac_CheckArgs($context, [ [ ['rowid'], 'disallow_empty', 'want_int' ], ]);

    if (scalar(@$errors) > 0) {
        throw Mioga2::Exception::Application("Mioga2::Poll::DeletePoll", ac_FormatErrors($errors));
    }

    my $rowid  = $values->{rowid};

    $self->DBDeletePoll(context => $context, rowid => $rowid);

    $xml .= "<DeletedPoll>" . st_FormatXMLString($rowid) . "</DeletedPoll>";
    $xml .= "</DeletePoll>";
    $self->NotifyUpdate($context, type => ['RSS']);

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll-js.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 AddQuestion ()

=over

=item Add a question to a poll.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub AddQuestion {
    my ($self, $context) = @_;

    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<AddQuestion>";
    $xml .= $context->GetXML();

    my $poll = $self->GetCurrentPoll($context);

    #
    # Get and verify parameters
    #
    my ($values, $errors) = ac_CheckArgs($context, [ [ ['poll_title'], 'stripxws', 'allow_empty' ],
	                                                 [ ['poll_description'], 'stripxws', 'allow_empty' ],
	                                                 [ ['test-answer', 'answer' ], 'stripxws', 'allow_empty' ],
	                                                 [ ['page'], 'stripxws', 'disallow_empty' ] ]);
    $poll->{title}       = $values->{poll_title};
    $poll->{description} = $values->{poll_description};

    $xml .= "<Referer>" . st_FormatXMLString($poll->{referer}) . "</Referer>";
    $xml .= "<Question/>";
    $xml .= "</AddQuestion>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 EditQuestion ()

=over

=item Edit an existing question of a poll.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub EditQuestion {
    my ($self, $context) = @_;

    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml .= "<EditQuestion>";
    $xml .= $context->GetXML();

    my $values = {};
    my $errors = [];

    ($values, $errors) = ac_CheckArgs($context, [ [ ['idx'], 'stripxws', 'disallow_empty', 'want_int' ], ]);

    if (not @$errors) {
        my $poll = $self->GetCurrentPoll($context);
        $poll->{title}       = $context->{args}->{poll_title};
        $poll->{description} = $context->{args}->{poll_description};
        my $questions = $poll->{questions_list};

        $xml .= $self->XMLGetQuestion(context => $context, question => $$questions[ $values->{idx} ]);
        $xml .= "<Referer>" . st_FormatXMLString($poll->{referer}) . "</Referer>";
    }
    else {
        $xml .= ac_XMLifyErrors($errors);
    }

    $xml .= "</EditQuestion>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'poll.xsl', locale_domain => 'poll_xsl');
    $content->SetContent($xml);

    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 XMLGetQuestion (I<%options>)

=over

=item XML representation of a question.

=item B<parameters :>

=over

=item I<$options{context}>: current context request.

=item I<$options{question}>: question to handle.

=back

=item B<return :> a question as a xml string.

=back

=cut

# ============================================================================

sub XMLGetQuestion {
    my ($self, %options) = @_;
    my $context  = $options{context};
    my $question = $options{question};

    my $xml .= "<Question type=\"" . st_FormatXMLString($question->{type}) . "\" rowid=\"" . st_FormatXMLString($question->{rowid}) . "\">";
    $xml .= "<Label>" . st_FormatXMLString($question->{label}) . "</Label>";

    my $choices = $question->{choices};
    $xml .= "<Choices>";
    foreach my $choice (@$choices) {
        $xml .= "<Choice type=\"" . st_FormatXMLString($self->GetTypeFor(type => $question->{type})) . "\" rowid=\"" . st_FormatXMLString($choice->{rowid}) . "\">";
        $xml .= "<Label>" . st_FormatXMLString($choice->{label}) . "</Label>";
        $xml .= "</Choice>";
    }
    $xml .= "</Choices>";
    $xml .= "</Question>";
}

# ============================================================================

=head2 XMLGetAnswer (I<%options>)

=over

=item XML representation of an answer.

=item B<parameters :>

=over

=item I<$options{context}>: current context request.

=item I<$options{answer}>: answer to handle.

=back

=item B<return :> an answer as a xml string.

=back

=cut

# ============================================================================

sub XMLGetAnswer {
    my ($self, %options) = @_;
    my $context = $options{context};
    my $answer  = $options{answer};

    my $xml .= "<Answer rowid=\"" . st_FormatXMLString($answer->{rowid}) . "\">";
    $xml .= "<Text>" . st_FormatXMLString($answer->{text}) . "</Text>";

    my $choices = $answer->{choices};
    $xml .= "<Choices>";
    foreach my $choice (@$choices) {
        $xml .= "<Choice rowid=\"" . st_FormatXMLString($choice->{rowid}) . "\">";
        $xml .= "<Label>" . st_FormatXMLString($choice->{label}) . "</Label>";
        $xml .= "</Choice>";
    }
    $xml .= "</Choices>";
    $xml .= "</Answer>";
}

# ============================================================================

=head2 GetTypeFor (I<%options>)

=over

=item Retrieve a type for a choice from the question type.

=item B<parameters :>

=over

=item I<$options{type}> : the question type.

=back

=item B<return :> type for a choice.

=back

=cut

# ============================================================================

sub GetTypeFor {
    my ($self, %options) = @_;

    return "checkbox" if ($options{type} =~ /MULTI/);
    return "radio";
}

# ============================================================================

=head2 DBGetPolls (I<%options>)

=over

=item Database method to retrieve polls.

=item B<parameters :> see I<GetPolls>.

=item B<return :> polls list as a hash.

=back

=cut

# ============================================================================

sub DBGetPolls {
    my ($self, %options) = @_;
    my $context  = $options{context};
    my $dbh      = $context->GetConfig()->GetDBH();
    my $group_id = ($options{group} || $context->GetGroup)->GetRowid;
    my $user_id  = $context->GetUser()->GetRowid();
    my $limit    = $options{max} ? "LIMIT $options{max}" : "";
    $options{published} = 1 unless (defined $options{published});

    my $sql_opts = "AND opening <= NOW()";

    if ($options{published} == 0) {
        $sql_opts = "AND (opening IS NULL OR opening > NOW())";
    }

    if (int($options{id})) {
        $sql_opts = " AND rowid=" . $options{id};
    }

    my $results = SelectMultiple($dbh, "SELECT poll.*, (SELECT count(rowid) from poll_question WHERE poll_id=poll.rowid) AS questions, (SELECT validated FROM poll_user_answer WHERE poll_id=poll.rowid AND user_id=$user_id) AS validated FROM poll WHERE group_id=$group_id $sql_opts ORDER BY created DESC $limit");
    return $results;
}

# ============================================================================

=head2 DBDeletePoll (I<%options>)

=over

=item Database method to delete a poll.

=item B<parameters:>

=over

=item I<$options{context}>: current request context.

=item I<$options{rowid}>: rowid of poll to delete.

=back

=item B<return:> nothing.

=back

=cut

# ============================================================================

sub DBDeletePoll {
    my ($self, %options) = @_;
    my $context  = $options{context};
    my $rowid    = $options{rowid};
    my $dbh      = $context->GetConfig()->GetDBH();
    my $group_id = $context->GetGroup()->GetRowid();

    ExecSQL($dbh, "DELETE FROM poll_answer_choice USING poll_choice, poll_question, poll WHERE poll_answer_choice.choice_id=poll_choice.rowid AND poll_choice.question_id=poll_question.rowid AND poll_question.poll_id=poll.rowid AND poll.group_id=$group_id AND poll.rowid=$rowid");
    ExecSQL($dbh, "DELETE FROM poll_answer USING poll_user_answer, poll WHERE poll_answer.ua_id=poll_user_answer.rowid AND poll_user_answer.poll_id=poll.rowid AND poll.group_id=$group_id AND poll.rowid=$rowid");
    ExecSQL($dbh, "DELETE FROM poll_user_answer USING poll WHERE poll_user_answer.poll_id=poll.rowid AND poll.group_id=$group_id AND poll.rowid=$rowid");
    ExecSQL($dbh, "DELETE FROM poll_choice USING poll_question, poll WHERE poll_choice.question_id=poll_question.rowid AND poll_question.poll_id=poll.rowid AND poll.group_id=$group_id AND poll.rowid=$rowid");
    ExecSQL($dbh, "DELETE FROM poll_question USING poll WHERE poll_question.poll_id=poll.rowid AND poll.group_id=$group_id AND poll.rowid=$rowid");
    ExecSQL($dbh, "DELETE FROM poll WHERE group_id=$group_id AND rowid=$rowid");
}

# ============================================================================

=head2 DBValidateAnswer (I<%options>)

=over

=item Database method to validate an answer.

=item B<parameters :> context request.

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBValidateAnswer {
    my ($self, %options) = @_;
    my $context = $options{context};
    my $dbh     = $context->GetConfig()->GetDBH();
    my $user_id = $context->GetUser()->GetRowid();

    my $poll = $self->GetCurrentPoll($context);
    ExecSQL($dbh, "UPDATE poll_user_answer SET validated='t' WHERE user_id=$user_id AND poll_id=" . $poll->{rowid});
}

# ============================================================================

=head2 DBInsertPoll (I<%options>)

=over

=item Database method to insert a new poll.

=item B<parameters :>

=over

=item I<$options{context}>: current request context.

=item I<$options{poll}>: the poll to insert.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBInsertPoll {
    my ($self, %options) = @_;
    my $context  = $options{context};
    my $dbh      = $context->GetConfig()->GetDBH();
    my $group_id = $context->GetGroup()->GetRowid();

    BeginTransaction($dbh);
    try {
        my $poll      = $self->GetCurrentPoll($context);
        my $opening   = $poll->{opening};
        my $closing   = $poll->{closing};
        my $visible   = $poll->{visible};
        my $anonymous = $poll->{anonymous};

        if ($opening eq NOW) {
            $opening = "NOW()";
        }
        elsif ($opening eq WAIT) {
            $opening = "NULL";
        }
        else {
            $opening = "'$opening'";
        }

        $visible   =~ s/0/'f'/g;
        $visible   =~ s/1/'t'/g;
        $anonymous =~ s/0/'f'/g;
        $anonymous =~ s/1/'t'/g;

        ExecSQL($dbh, "INSERT INTO poll(title, description, created, modified, opening, closing, visible_for_all, anonymous, group_id) VALUES('" . st_FormatPostgreSQLString($poll->{title}) . "', '" . st_FormatPostgreSQLString($poll->{description}) . "', NOW(), NOW(), $opening, '$closing', $visible, $anonymous, $group_id)");

        my $poll_id = GetLastInsertId($dbh, "poll");
        my $questions = $poll->{questions_list};

        foreach my $question (@$questions) {
            ExecSQL($dbh, "INSERT INTO poll_question(poll_id, created, modified, label, type) VALUES($poll_id, NOW(), NOW(), '" . st_FormatPostgreSQLString($question->{label}) . "','" . st_FormatPostgreSQLString($question->{type}) . "')");

            if ($question->{type} ne FREE_ANSWER) {
                my $question_id = GetLastInsertId($dbh, "poll_question");
                my $choices = $question->{choices};

                foreach my $choice (@$choices) {
                    ExecSQL($dbh, "INSERT INTO poll_choice(question_id, created, modified, label) VALUES($question_id, NOW(), NOW(), '" . st_FormatPostgreSQLString($choice->{label}) . "')");
                }
            }
        }
    }
    otherwise {
        my $ex = shift;
        warn Dumper($ex);

        RollbackTransaction($dbh);

        $ex->throw();
    };
    EndTransaction($dbh);
}

# ============================================================================

=head2 DBInsertAnswer (I<%options>)

=over

=item database method to insert user answer.

=item B<parameters :>

=over

=item I<$options{context}>: current request context.

=item I<$options{values}>: values to insert.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBInsertAnswer {
    my ($self, %options) = @_;
    my $context = $options{context};
    my $values  = $options{values};
    my $dbh     = $context->GetConfig()->GetDBH();
    my $user_id = $context->GetUser()->GetRowid();

    BeginTransaction($dbh);
    try {
        my $sql = "SELECT * FROM poll_user_answer WHERE poll_id=" . $values->{poll_id} . " AND user_id=$user_id";
        my $user_answer = SelectSingle($dbh, $sql);
        if (not defined $user_answer) {
            ExecSQL($dbh, "INSERT INTO poll_user_answer(created, modified, user_id, poll_id) VALUES(NOW(), NOW(), $user_id, " . $values->{poll_id} . ")");
            $user_answer = SelectSingle($dbh, $sql);
        }

        my $questions = $self->DBGetQuestions(context => $context, poll_id => $values->{poll_id});
        my $question = $$questions[ $values->{current_question} - 1 ];

        my $fa_text = "NULL";
        if ((ref($values->{answer}) eq 'ARRAY' and grep(/text-answer/, @{ $values->{answer} })) or (ref($values->{answer}) ne 'ARRAY' and $values->{answer} eq 'text-answer')) {
            $fa_text = "'" . st_FormatPostgreSQLString($values->{'text-answer'}) . "'";
        }

        my $answer = {};
        if ($answer = SelectSingle($dbh, "SELECT * FROM poll_answer WHERE ua_id=" . $user_answer->{rowid} . " AND question_id=" . $question->{rowid})) {
            ExecSQL($dbh, "UPDATE poll_answer SET modified=NOW(), text=$fa_text WHERE ua_id=" . $user_answer->{rowid} . " AND question_id=" . $question->{rowid});
        }
        else {
            ExecSQL($dbh, "INSERT INTO poll_answer(created, modified, ua_id, question_id, text) VALUES(NOW(), NOW(), " . $user_answer->{rowid} . ", " . $question->{rowid} . ", $fa_text)");
            $answer->{rowid} = GetLastInsertId($dbh, "poll_answer");
        }

        ExecSQL($dbh, "DELETE FROM poll_answer_choice WHERE answer_id=" . $answer->{rowid});

        my $choices = $question->{choices};
        if (ref($values->{answer}) eq 'ARRAY') {
            foreach my $choice (@{ $values->{answer} }) {
                next if ($choice eq "text-answer");

                my $choice_id = $$choices[$choice]->{rowid};
                ExecSQL($dbh, "INSERT INTO poll_answer_choice(choice_id, answer_id) VALUES($choice_id, " . $answer->{rowid} . ")");
            }
        }
        else {
            if ($values->{answer} ne "text-answer") {

                my $choice_id = $$choices[ $values->{answer} ]->{rowid};
                ExecSQL($dbh, "INSERT INTO poll_answer_choice(choice_id, answer_id) VALUES($choice_id, " . $answer->{rowid} . ")");
            }
        }
    }
    otherwise {
        my $ex = shift;
        warn Dumper($ex);

        RollbackTransaction($dbh);

        $ex->throw();
    };
    EndTransaction($dbh);
}

# ============================================================================

=head2 DBUpdatePoll (I<%options>)

=over

=item Database method to update an existing poll.

=item B<parameters :>

=over

=item I<$options{context}>: current request context.

=item I<$options{poll}>: the poll to update with its new data.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBUpdatePoll {
    my ($self, %options) = @_;
    my $context  = $options{context};
    my $dbh      = $context->GetConfig()->GetDBH();
    my $group_id = $context->GetGroup()->GetRowid();

    BeginTransaction($dbh);
    try {
        my $poll      = $self->GetCurrentPoll($context);
        my $opening   = $poll->{opening};
        my $closing   = $poll->{closing};
        my $visible   = $poll->{visible};
        my $anonymous = $poll->{anonymous};

        if ($opening eq NOW) {
            $opening = "NOW()";
        }
        elsif ($opening eq WAIT) {
            $opening = "NULL";
        }
        else {
            $opening = "'$opening'";
        }

        $visible   =~ s/0/'f'/g;
        $visible   =~ s/1/'t'/g;
        $anonymous =~ s/0/'f'/g;
        $anonymous =~ s/1/'t'/g;

        my $poll_id = $poll->{rowid};

        ExecSQL($dbh, "UPDATE poll SET title='" . st_FormatPostgreSQLString($poll->{title}) . "', description='" . st_FormatPostgreSQLString($poll->{description}) . "', modified=NOW(), opening=$opening, closing='$closing', visible_for_all=$visible, anonymous=$anonymous WHERE group_id=$group_id AND rowid=$poll_id");

        ExecSQL($dbh, "DELETE FROM poll_choice USING poll_question, poll WHERE poll_choice.question_id=poll_question.rowid AND poll_question.poll_id=poll.rowid AND poll.rowid=$poll_id AND poll.group_id=$group_id");
        ExecSQL($dbh, "DELETE FROM poll_question USING poll WHERE poll_question.poll_id=poll.rowid AND poll.rowid=$poll_id AND poll.group_id=$group_id");

        my $questions = $poll->{questions_list};

        foreach my $question (@$questions) {
            ExecSQL($dbh, "INSERT INTO poll_question(poll_id, created, modified, label, type) VALUES($poll_id, NOW(), NOW(), '" . st_FormatPostgreSQLString($question->{label}) . "','" . st_FormatPostgreSQLString($question->{type}) . "')");

            if ($question->{type} ne FREE_ANSWER) {
                my $question_id = GetLastInsertId($dbh, "poll_question");
                my $choices = $question->{choices};

                foreach my $choice (@$choices) {
                    ExecSQL($dbh, "INSERT INTO poll_choice(question_id, created, modified, label) VALUES($question_id, NOW(), NOW(), '" . st_FormatPostgreSQLString($choice->{label}) . "')");
                }
            }
        }
    }
    otherwise {
        my $ex = shift;
        warn Dumper($ex);

        RollbackTransaction($dbh);

        $ex->throw();
    };
    EndTransaction($dbh);
}

# ============================================================================

=head2 DBGetQuestions (I<%options>)

=over

=item Retrieve questions from database.

=item B<parameters :>

=over

=item I<$options{context}>: current request context.

=item I<$options{poll_id}>: the id of the poll.

=back

=item B<return :> list of questions as a hash.

=back

=cut

# ============================================================================

sub DBGetQuestions {
    my ($self, %options) = @_;
    my $context  = $options{context};
    my $dbh      = $context->GetConfig()->GetDBH();
    my $group_id = $context->GetGroup()->GetRowid();
    my $poll_id  = $options{poll_id};

    my $questions = SelectMultiple($dbh, "SELECT poll_question.* FROM poll_question,poll WHERE poll_id=poll.rowid AND poll.rowid=$poll_id AND poll.group_id=$group_id ORDER BY rowid ASC");

    my $choices = SelectMultiple($dbh, "SELECT poll_choice.* FROM poll_choice,poll_question,poll WHERE poll_choice.question_id=poll_question.rowid AND poll_question.poll_id=poll.rowid AND poll.rowid=$poll_id AND poll.group_id=$group_id ORDER BY rowid ASC");

    my %match;
    foreach my $idx (0 .. $#{$questions}) {
        $match{ $$questions[$idx]->{rowid} } = $idx;
    }

    foreach my $res (@$choices) {
        $$questions[ $match{ $res->{question_id} } ]->{choices} = [] if (not defined $$questions[ $match{ $res->{question_id} } ]->{choices});
        my $q_choices = $$questions[ $match{ $res->{question_id} } ]->{choices};

        push @$q_choices, $res;
    }

    return $questions;
}

# ============================================================================

=head2 DBGetAnswer (I<%options>)

=over

=item Retrieve an user answer from database.

=item B<parameters :>

=over

=item I<$options{context}>: current request context.

=item I<$options{question_index}>: index of question (not its rowid).

=back

=item B<return :> list of questions as a hash.

=back

=cut

# ============================================================================

sub DBGetAnswer {
    my ($self, %options) = @_;
    my $context  = $options{context};
    my $dbh      = $context->GetConfig()->GetDBH();
    my $group_id = $context->GetGroup()->GetRowid();
    my $user_id  = $context->GetUser()->GetRowid();
    my $q_idx    = $options{question_index};
    my $poll     = $self->GetCurrentPoll($context);

    my $question_id = SelectSingle($dbh, "SELECT rowid FROM poll_question WHERE poll_id=" . $poll->{rowid} . " ORDER BY rowid LIMIT 1 OFFSET $q_idx")->{rowid};

    my $answer = SelectSingle($dbh, "SELECT poll_answer.* FROM poll_answer, poll_user_answer WHERE poll_user_answer.user_id=$user_id AND poll_user_answer.poll_id=" . $poll->{rowid} . " AND poll_answer.ua_id=poll_user_answer.rowid AND poll_answer.question_id=$question_id");

    if ($answer) {
        my $choices = SelectMultiple($dbh, "SELECT poll_choice.* FROM poll_choice, poll_answer_choice WHERE poll_answer_choice.answer_id=" . $answer->{rowid} . " AND poll_choice.rowid=poll_answer_choice.choice_id");

        $answer->{choices} = $choices;
    }

    return $answer;
}

# ============================================================================

=head2 DBGetNumberOfAnswers (I<%options>)

=over

=item Retrieve number of users that answered a poll.

=item B<parameters :>

=over

=item I<$options{context}>: current request context.

=item I<$options{poll_id}>: id of a poll.

=back

=item B<return :> number of users.

=back

=cut

# ============================================================================

sub DBGetNumberOfAnswers {
    my ($self, %options) = @_;
    my $context = $options{context};
    my $poll_id = $options{poll_id};
    my $dbh     = $context->GetConfig()->GetDBH();
    my $answers = SelectSingle($dbh, "SELECT COUNT(rowid) FROM poll_user_answer WHERE poll_id=$poll_id AND validated=true");

    return $answers->{count};
}

# ============================================================================

=head2 XMLGetPoll (I<%options>)

=over

=item Retriveve a poll as xml.

=item B<parameters :>

=over

=item I<$options{context}> : current request context.

=item I<$options{poll}> : a poll object as a hash.

=back

=item B<return :> a poll as xml string.

=back

=cut

# ============================================================================

sub XMLGetPoll {
    my ($self, %options) = @_;
    my $context = $options{context};
    my $poll    = $options{poll};

    my $xml = "<Poll created=\"" . st_FormatXMLString($poll->{created}) . "\" modified=\"" . st_FormatXMLString($poll->{modified}) . "\" questions=\"" . st_FormatXMLString($poll->{questions}) . "\" rowid=\"" . st_FormatXMLString($poll->{rowid}) . "\" visible=\"" . int($poll->{visible_for_all}) . "\"";
    $xml .= " validated=\"1\"" if ($poll->{validated});
    $xml .= " closed=\"1\"" if ($self->IsPollClosed(context => $context, poll => $poll));
    $xml .= ">";
    $xml .= "<Title>" . st_FormatXMLString($poll->{title}) . "</Title>";
    $xml .= "<Description>" . st_FormatXMLString($poll->{description}) . "</Description>";
    $xml .= "</Poll>";

    return $xml;
}

# ============================================================================

=head2 XMLGetCompletePoll (I<%options>)

=over

=item retrieve complete definition of given poll.

=item B<parameters:>

=over

=item I<$options{context}>: current request context.

=item I<$options{poll}>: a poll object.

=item I<$options{use_session}>: boolean to use or not session. defaults to false.

=back

=item B<return:> a poll as xml string.

=back

=cut

# ============================================================================

sub XMLGetCompletePoll {
    my ($self, %options) = @_;
    my $context     = $options{context};
    my $poll        = $options{poll};
    my $use_session = 0 || $options{use_session};

    my $xml .= "<Poll rowid=\"" . st_FormatXMLString($poll->{rowid}) . "\">";
    $xml    .= "<Title>" . st_FormatXMLString($poll->{title}) . "</Title>";
    $xml    .= "<Description>" . st_FormatXMLString($poll->{description}) . "</Description>";
    $xml .= $self->GetQuestionsFor(context => $context, use_session => $use_session);
    $xml .= "</Poll>";

    return $xml;
}

# ============================================================================

=head2 IsPollClosed (I<%options>)

=over

=item Verify if a given poll is closed or not.

=item B<parameters:>

=over

=item I<$options{context}>: current request context.

=item I<$options{poll}>: poll to verify.

=back

=item B<return:> undef if not closed, true otherwise.

=back

=cut

# ============================================================================

sub IsPollClosed {
    my ($self, %options) = @_;
    my $context = $options{context};
    my $poll    = $options{poll};

    if ($poll->{closing} =~ /\d{4}-\d{2}-\d{2}/) {
        return 1 if (du_ISOToSecond($poll->{closing}) < du_ISOToSecond(du_GetNowISO()));
    }
    elsif ($poll->{closing} =~ /^\d{2,3}$/) {
        my $user_list   = $context->GetGroup()->GetExpandedUsers();
        my $total_users = scalar @{ $user_list->{list} };
        my $answers     = $self->DBGetNumberOfAnswers(context => $context, poll_id => $poll->{rowid});
        my $cur_percent = int($answers * 100 / $total_users);
        return 1 if ($cur_percent >= $poll->{closing});
    }

    return undef;
}

# ============================================================================

=head2 XMLGetAccessRights ()

=over

=item Retrieve access rights.

=item B<return :> access rights as a xml string.

=back

=cut

# ============================================================================

sub XMLGetAccessRights {
    my ($self, $context) = @_;

    my $xml;
    $xml .= "<CanAnim/>" if $self->UserHasRightTo(context => $context, right => ANIM);
    $xml .= "<CanRead/>" if $self->UserHasRightTo(context => $context, right => READ);

    return $xml;
}

# ============================================================================

=head2 UserHasRightTo (I<%options>)

=over

=item Utility method to know if a user can access a method.

=item B<parameters :>

=over

=item I<$options->{context}> : current request context.

=item I<$options->{right}>: the right to check.

=back

=item B<return :> false or true.

=back

=cut

# ============================================================================

sub UserHasRightTo {
    my ($self, %options) = @_;
    my $context = $options{context};

    return $self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), $options{right});
}

# ============================================================================

=head2 RevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

	Remove user data in database when a user is revoked of a group.

	This function is called when :
	- a user is revoked of a group
	- a team is revoked of a group for each team member not namly
	  invited in group
	- a user is revoked of a team  for each group where the team is
	  member and the user not namly invited in group

=cut

# ============================================================================

sub RevokeUserFromGroup {
    my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;

    my $dbh = $config->GetDBH();
    ExecSQL($dbh, "UPDATE poll_user_answer SET user_id=$group_animator_id WHERE user_id=$user_id");
}

# ============================================================================

=head2 DeleteGroupData ($config, $group_id)

	Remove group data in database when a group is deleted.

	This function is called when a group/user/resource is deleted.

=cut

# ============================================================================

sub DeleteGroupData {
    my ($self, $config, $group_id) = @_;

    my $dbh = $config->GetDBH();

    ExecSQL($dbh, "DELETE FROM poll_answer_choice USING poll_choice, poll_question, poll WHERE poll_answer_choice.choice_id=poll_choice.rowid AND poll_choice.question_id=poll_question.rowid AND poll_question.poll_id=poll.rowid AND poll.group_id=$group_id");
    ExecSQL($dbh, "DELETE FROM poll_answer USING poll_user_answer, poll WHERE poll_answer.ua_id=poll_user_answer.rowid AND poll_user_answer.poll_id=poll.rowid AND poll.group_id=$group_id");
    ExecSQL($dbh, "DELETE FROM poll_user_answer USING poll WHERE poll_user_answer.poll_id=poll.rowid AND poll.group_id=$group_id");
    ExecSQL($dbh, "DELETE FROM poll_choice USING poll_question, poll WHERE poll_choice.question_id=poll_question.rowid AND poll_question.poll_id=poll.rowid AND poll.group_id=$group_id");
    ExecSQL($dbh, "DELETE FROM poll_question USING poll WHERE poll_question.poll_id=poll.rowid AND poll.group_id=$group_id");
    ExecSQL($dbh, "DELETE FROM poll WHERE group_id=$group_id");
}

# ============================================================================
# GetRSSFeed
#   Retrieve feed of polls
# ============================================================================

sub GetRSSFeed {
    my ($self, $context, $ident, $feed, $max) = @_;

    my $config = $context->GetConfig;
    my $group;
    try {
        $group = Mioga2::Old::Group->new($config, ident => $ident);
    }
    otherwise {
        $group = Mioga2::Old::User->new($config, ident => $ident);
    };

    if (!$feed) {
        $feed = XML::Atom::SimpleFeed->new(
            title => __x("Polls feed of {group} group", group => $group->GetIdent),
            link => { href => $config->GetProtocol . "://" . $config->GetServerName . $context->{uri}->GetURI, rel => 'self' },
            link => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/Poll/DisplayMain",
            id   => "data:,group:" . $group->GetIdent . ":polls",
        );
    }

    $max = 20 unless $max;
    my $group_id    = $group->GetRowid;
    my $group_ident = $group->GetIdent;
    my $polls       = $self->DBGetPolls(context => $context, max => $max, group => $group);
    my $animator    = $group->GetAnimator->GetName;

    foreach my $poll (@$polls) {
        $feed->add_entry(
            title     => __x("[{group}] Poll: {title}", group => $group_ident, title => $poll->{title}),
            link      => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/Poll/AnswerPoll?rowid=$poll->{rowid}",
            id        => "data:,group:" . $group->GetIdent . ":polls:$poll->{rowid}",
            author    => $animator,
            published => Mioga2::Classes::Time->FromPSQL($poll->{created})->RFC3339DateTime,
            updated   => Mioga2::Classes::Time->FromPSQL($poll->{modified})->RFC3339DateTime,
            content   => $poll->{description},
        );
    }
    return $feed;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2006-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
