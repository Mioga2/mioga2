#===============================================================================
#
#         FILE:  Theme.pm
#
#  DESCRIPTION:  Mioga2 Theme management object.
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  13/10/2010 11:12
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Theme.pm: Mioga2 theme management object.

=head1 DESCRIPTION

This module handles Mioga2 themes.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Theme;

use Mioga2::DAVFSUtils;
use Mioga2::Exception::Simple;
use Mioga2::tools::database;
use Mioga2::URI;

use File::Path;

use Data::Dumper;

my $debug = 0;


#===============================================================================

=head2 new

Create new instance of object.

=cut

#===============================================================================
sub new {
	my ($class, $config, $user) = @_;
	print STDERR "[Mioga2::Theme::new] Entering.\n" if ($debug);

	my $self = { };
	bless($self, $class);

	$self->{config} = $config;
	$self->{dbh} = $config->GetDBH ();
	$self->{davfs} = new Mioga2::DAVFSUtils ($config, $user);

	$self->{default_xsl_dir} = $config->GetInstallPath () . '/conf/xsl';
	$self->{default_theme_dir} = $config->GetInstallPath () . '/conf/themes/default';
	$self->{target_theme_dir} = $config->GetThemesDir ();
	$self->{target_xsl_dir} = $config->GetXSLDir ();

	print STDERR "[Mioga2::Theme::new] Leaving\n" if ($debug);
	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 Create

Create a new theme

=cut

#===============================================================================
sub Create {
	my ($self, %attributes) = @_;
	print STDERR "[Mioga2::Theme::Create] Entering. Attributes: " . Dumper \%attributes if ($debug);

	my $theme_path = "$attributes{path}/$attributes{ident}";

	# Check if theme already exists
	if ($self->{davfs}->IsCollection ($theme_path)) {
		throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Theme already exists');
	}

	# Check if target directory exists, otherwise create it
	if (!$self->{davfs}->IsCollection ($attributes{path})) {
		$self->{davfs}->MakeCollection ($attributes{path}) or throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Can not create target directory.');
	}

	#-------------------------------------------------------------------------------
	# Create private DAV structure
	#-------------------------------------------------------------------------------
	$self->{davfs}->MakeCollection ($theme_path) or throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Can not create DAV path.');
	$self->{davfs}->PutCollection ("$theme_path/images", "$self->{default_theme_dir}/images") or throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Can not copy images to DAV path.');
	$self->{davfs}->PutFile ("$theme_path/theme.css", "/dev/null", 0) or throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Can not create theme empty CSS file.');

	$self->{davfs}->PutFile ("$theme_path/theme_print.css", "/dev/null", 0) or throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Can not create theme_print empty CSS file.');

	#-------------------------------------------------------------------------------
	# Create system FS structure
	#-------------------------------------------------------------------------------
	# Symlink target XSL directory to default
	symlink ($self->{default_xsl_dir}, "$self->{target_xsl_dir}/$attributes{ident}") or throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Can not create XSL symlink.');

	# Create theme directory
	mkdir ("$self->{target_theme_dir}/$attributes{ident}") or throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Can not create target directory.');

	# Symlink target JS directory to default
	symlink ("$self->{default_theme_dir}/javascript", "$self->{target_theme_dir}/$attributes{ident}/javascript") or throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Can not create JS symlink.');
	symlink ("$self->{default_theme_dir}/css", "$self->{target_theme_dir}/$attributes{ident}/css") or throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Can not create CSS symlink.');
	my $uri = new Mioga2::URI ($self->{config}, uri => $theme_path);
	my $theme_real_path = $uri->GetRealPath ();
	symlink ("$theme_real_path/images", "$self->{target_theme_dir}/$attributes{ident}/images") or throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Can not create images symlink.');
	symlink ("$theme_real_path/theme.css", "$self->{target_theme_dir}/$attributes{ident}/theme.css") or throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Can not create theme CSS symlink.');
	symlink ("$theme_real_path/theme_print.css", "$self->{target_theme_dir}/$attributes{ident}/theme_print.css") or throw Mioga2::Exception::Simple ('Mioga2::Theme::Create', 'Can not create theme_pint CSS symlink.');

	#-------------------------------------------------------------------------------
	# Record theme into DB
	#-------------------------------------------------------------------------------
	ExecSQL ($self->{dbh}, "INSERT INTO m_theme (created, modified, name, ident, mioga_id) VALUES (now (), now (), " . $self->{dbh}->quote ($attributes{name}) . ", " . $self->{dbh}->quote ($attributes{ident}) . ", " . $self->{config}->GetMiogaId () . ");");
	my $rowid = GetLastInsertId ($self->{dbh}, 'm_theme');

	print STDERR "[Mioga2::Theme::Create] Leaving. Theme rowid: $rowid\n" if ($debug);
	return ($rowid);
}	# ----------  end of subroutine Create  ----------


#===============================================================================

=head2 Delete

Delete a theme.

=cut

#===============================================================================
sub Delete {
	my ($self, $rowid) = @_;

	my $theme = SelectSingle ($self->{dbh}, "SELECT m_theme.*, (SELECT count (*) FROM m_group_base WHERE theme_id = m_theme.rowid) AS nb_groups FROM m_theme WHERE rowid = $rowid;");

	# Check if theme actually exists
	if (!$theme) {
		throw Mioga2::Exception::Simple ('Mioga2::Theme::Delete', 'Theme does not exist.');
	}

	# Check if theme is in use
	if ($theme->{nb_groups}) {
		throw Mioga2::Exception::Simple ('Mioga2::Theme::Delete', 'Theme is in use.');
	}

	# Get symlink target
	my ($theme_path) = (readlink ("$self->{target_theme_dir}/$theme->{ident}/images") =~ m/^.*home(\/.*\/)[^\/]*$/);
	$theme_path = $self->{config}->GetPrivateURI () . $theme_path;

	#-------------------------------------------------------------------------------
	# Delete private DAV structure
	#-------------------------------------------------------------------------------
	$self->{davfs}->Delete ($theme_path) or throw Mioga2::Exception::Simple ('Mioga2::Theme::Delete', 'Can not delete DAV path.');

	#-------------------------------------------------------------------------------
	# Delete system FS structure
	#-------------------------------------------------------------------------------
	# Delete XSL symlink
	unlink ("$self->{target_xsl_dir}/$theme->{ident}") or throw Mioga2::Exception::Simple ('Mioga2::Theme::Delete', 'Can not delete XSL symlink.');

	# Delete theme directory
	rmtree ("$self->{target_theme_dir}/$theme->{ident}") or throw Mioga2::Exception::Simple ('Mioga2::Theme::Delete', 'Can not delete target directory.');

	#-------------------------------------------------------------------------------
	# Delete theme from DB
	#-------------------------------------------------------------------------------
	ExecSQL ($self->{dbh}, "DELETE FROM m_theme WHERE rowid = $rowid;");
}	# ----------  end of subroutine Delete  ----------


#===============================================================================

=head2 GetList

Get themes for current instance

=cut

#===============================================================================
sub GetList {
	my ($self) = @_;

	my $list = SelectMultiple ($self->{dbh}, "SELECT m_theme.*, (SELECT count (*) FROM m_group_base WHERE theme_id = m_theme.rowid) AS nb_groups FROM m_theme WHERE mioga_id = " . $self->{config}->GetMiogaId ());

	return ($list);
}	# ----------  end of subroutine GetList  ----------


#===============================================================================

=head2 GetTheme

Get theme details from rowid.

=cut

#===============================================================================
sub GetTheme {
	my ($self, $rowid) = @_;

	my $theme = SelectSingle ($self->{dbh}, "SELECT m_theme.*, (SELECT count (*) FROM m_group_base WHERE theme_id = m_theme.rowid) AS nb_groups FROM m_theme WHERE rowid = $rowid");

	return ($theme);
}	# ----------  end of subroutine GetTheme  ----------

1;

