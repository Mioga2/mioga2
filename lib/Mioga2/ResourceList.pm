#===============================================================================
#
#         FILE:  ResourceList.pm
#
#  DESCRIPTION:  New generation Mioga2 resourcelist class
#
# REQUIREMENTS:  ---
#        NOTES:  TODO Rewrite the Store and Delete methods so this module is
#				 	  independant from APIGroup
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  16/04/2010 11:24
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

ResourceList.pm: New generation Mioga2 resourcelist class

=head1 DESCRIPTION

This class handles Mioga2 resources. It is designed to create, load, update or
delete resources.

=head1 SYNOPSIS

 use Mioga2::ResourceList;
 use Mioga2::Config;

 my $resourcelist;
 my $config = new Mioga2::Config (...);

 # Set all resources whose animator is user id 1 as active
 $resourcelist = Mioga2::ResourceList->new ($config, { attributes => { animator => 1 } });
 $resourcelist->Set ('status', 'active');
 $resourcelist->Store ();

 # Delete resource whose ident is res1
 $resourcelist = Mioga2::ResourceList->new ($config, { attributes => { ident => 'res1' } });
 $resourcelist->Delete ();

 # Create a new resource from attributes and skeleton
 $resourcelist = Mioga2::ResourceList->new ($config,
 		ident => 'res1',
 		location => 'Office',
		animator => 1,
 		skeleton => '/path/to/std_resource.xml'
 	);
 $resourcelist->Store ();

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::ResourceList;
use base qw(Mioga2::Old::Compatibility);

use Data::Dumper;

use Locale::TextDomain::UTF8 'resourcelist';

use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::APIGroup;
use Error qw(:try);
use Mioga2::Exception::Resource;
use Mioga2::GroupList;

my $debug = 0;

#===============================================================================

=head2 new

Create a new Mioga2::ResourceList object.

=over

=item B<arguments:>

=over

=item I<$config:> a Mioga2::Config object matching the instance to work into.

=item I<$data:> a hashref with the following keys:

=over

=item I<attributes:> a list of key / value pairs matching the resources.

=over

=item skeleton: the path to a skeleton file to apply to resource(s).

=back

=item I<journal:> a Mioga2::Journal-tied-array to record operations.

=back

=back

=item B<return:> a Mioga2::ResourceList object matching resources according to provided attributes.

=back

=cut

#===============================================================================
sub new {
	my ($class, $config, $data) = @_;
	print STDERR "[Mioga2::ResourceList::new] Entering, data: " . Dumper $data if ($debug);

	my $self = { };
	bless($self, $class);

	$self->{config} = $config;

	# Initialize attributes
	$self->{attributes} = $data->{attributes};

	# Journal of operations
	if (defined ($data->{journal})) {
		$self->{journal} = $data->{journal};
	}
	else {
		tie (@{$self->{journal}}, 'Mioga2::Journal');
	}

	# Initialize object state
	$self->{loaded} = 0;
	$self->{deleted} = 0;

	# Initialize WHERE statement before any update is made on the list
	$self->SetWhereStatement (keys (%{$self->{attributes}}));

	print STDERR "[Mioga2::ResourceList::new] Leaving\n" if ($debug);
	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 Set

Set an attribute value.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to set.

=item I<$value:> the attribute value to set.

=back

=back

=cut

#===============================================================================
sub Set {
	my ($self, $attribute, $value) = @_;

	$self->{update}->{$attribute} = $value;
}	# ----------  end of subroutine Set  ----------


#===============================================================================

=head2 Get

Get an attribute value.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get.

=back

=item B<return:> the attribute value.

=back

=cut

#===============================================================================
sub Get {
	my ($self, $attribute) = @_;
	print STDERR "[Mioga2::ResourceList::Get] Entering, attribute name is $attribute\n" if ($debug);

	my $value = '';

	#-------------------------------------------------------------------------------
	# Load the object from DB if attribute is not known already
	#-------------------------------------------------------------------------------
	if ((!grep /^$attribute$/, keys (%{$self->{attributes}})) && (!grep /^$attribute$/, keys (%{$self->{update}})) && !$self->{loaded}) {
		# The requested attribute is not set into object yet, get it from DB
		$self->Load ();
	}

	#-------------------------------------------------------------------------------
	# Retreive attribute value
	#-------------------------------------------------------------------------------
	if (exists ($self->{update}->{$attribute})) {
		# Attribute is in update list, get its value
		$value = $self->{update}->{$attribute};
	}
	elsif (exists ($self->{attributes}->{$attribute})) {
		# Attribute is in initial list, get its value
		$value = $self->{attributes}->{$attribute};
	}
	else {
		# Attribute is not in initial list, nor in update list, pick it from loaded resources
		if (scalar (@{$self->{resources}}) > 1) {
			# Multiple resources in the list, return an array of attribute values.
			$value = ();
			for (@{$self->{resources}}) {
				push (@$value, $_->{$attribute});
			}
		}
		else {
			# Only one resource in the list, return attribute value
			$value = $self->{resources}->[0]->{$attribute};
		}
	}

	if (ref ($value) eq 'ARRAY') {
		print STDERR "[Mioga2::ResourceList::Get] Leaving, attribute value is " . Dumper $value if ($debug);
	}
	elsif (ref ($value) eq 'Mioga2::GroupList') {
		my @rowids = $value->GetRowIds ();
		print STDERR "[Mioga2::ResourceList::Get] Leaving, attribute value is truncated, Mioga2::GroupList rowids " . Dumper @rowids if ($debug);
	}
	else {
		print STDERR "[Mioga2::ResourceList::Get] Leaving, attribute value is $value\n" if ($debug);
	}
	return ($value);
}	# ----------  end of subroutine Get  ----------


#===============================================================================

=head2 Store

Store changes to database. This method has to be called to actually create or
update one or more resources. To delete resources, see the "Delete" method.

=cut

#===============================================================================
sub Store {
	my ($self) = @_;
	print STDERR "[Mioga2::ResourceList::Store] Entering\n" if ($debug);

	if ((!$self->Count ()) && (!$self->{deleted})) {
		#-------------------------------------------------------------------------------
		# Create a resource into DB
		# TODO this part has to be rewritten to be completely autonomous from APIGroup
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::ResourceList::Store] Creating new resource\n" if ($debug);
		my $status = 1;
		try {
			ResourceCreate ($self->{config}, $self->GetDatabaseValues (), 1); # TODO (SNI - 21/04/2010 08:54): Handle this inside the module
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::ResourceList', step => __x('Resource "{ident}" creation', ident => $self->Get ('ident')), status => $status });
		};
	}
	elsif (scalar (keys (%{$self->{update}}))) {
		#-------------------------------------------------------------------------------
		# Update resources
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::ResourceList::Store] Updating existing resource\n" if ($debug);

		for my $resource ($self->GetResources ()) {
			my $dbvals = $self->GetDatabaseValues ();
			for (keys (%$dbvals)) {
				$resource->{$_} = $dbvals->{$_};
			}
			my $status = 1;
			try {
				ResourceModify ($self->{config}, $resource, 1); # TODO (SNI - 21/04/2010 08:54): Handle this inside the module
			}
			otherwise {
				my $err = shift;
				$status = 0;
				throw $err;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::ResourceList', step => __x('Resource "{ident}" update', ident => $self->Get ('ident')), status => $status });
			};
		}
	}
	else {
		print STDERR "[Mioga2::ResourceList::Store] Nothing has to be done\n" if ($debug);
	}

	#-------------------------------------------------------------------------------
	# Update WHERE statement, as some modifications were made, resources can not be
	# matched anymore by the same WHERE statement, matching according to rowids
	#-------------------------------------------------------------------------------
	$self->SetWhereStatement (qw/rowid/);

	#-------------------------------------------------------------------------------
	# Update-pending values are now stored into DB, they can be moved to the inner
	# attributes of the object
	#-------------------------------------------------------------------------------
	for (keys (%{$self->{update}})) {
		$self->{attributes}->{$_} = delete ($self->{update}->{$_});
	}

	print STDERR "[Mioga2::ResourceList::Store] Leaving\n" if ($debug);
}	# ----------  end of subroutine Store  ----------


#===============================================================================

=head2 Count

Count the resources in the list. This method loads the resource list from DB if
required.

=over

=item B<return:> the number of resources in the list.

=back

=cut

#===============================================================================
sub Count {
	my ($self) = @_;

	$self->Load () unless $self->{loaded};

	my $count = scalar (@{$self->{resources}});

	print STDERR "[Mioga2::ResourceList::Count] $count resources\n" if ($debug);

	return ($count);
}	# ----------  end of subroutine Count  ----------


#===============================================================================

=head2 GetRowIds

Get RowIds for resources in the list.

=over

=item B<return:> an array of rowids.

=back

=cut

#===============================================================================
sub GetRowIds {
	my ($self) = @_;

	my @list = ();

	$self->Load () unless $self->{loaded};

	for (@{$self->{resources}}) {
		push (@list, $_->{rowid});
	}

	return (@list);
}	# ----------  end of subroutine GetRowIds  ----------


#===============================================================================

=head2 GetResources

Get list of resources in the list.

=over

=item B<return:> an array of hashes describing the resources in the list. Each hash
of this array can be used to create a new Mioga2::ResourceList that
only matches this resource.

=back

=cut

#===============================================================================
sub GetResources {
	my ($self) = @_;

	$self->Load () unless $self->{loaded};

	for my $resource (@{$self->{resources}}) {
		for (keys (%{$self->{update}})) {
			$resource->{$_} = $self->{update}->{$_};
		}
	}

	return (@{$self->{resources}});
}	# ----------  end of subroutine GetResources  ----------


#===============================================================================

=head2 Delete

Delete resources.

=cut

#===============================================================================
sub Delete {
	my ($self) = @_;
	print STDERR "[Mioga2::ResourceList::Delete] Entering\n" if ($debug);

	for ($self->GetResources ()) {
		print STDERR "[Mioga2::ResourceList::Delete] Deleting resource rowid $_->{rowid}, mioga_id $_->{mioga_id} (ident '$_->{ident}')\n" if ($debug);
		my $status = 1;
		try {
			ResourceDelete ($self->{config}, $_->{rowid}, 1); # TODO (SNI - 21/04/2010 08:54): Handle this inside the module
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::ResourceList', step => __x('Resource "{ident}" deletion', ident => $self->Get ('ident')), status => $status });
		};
	}

	$self->{deleted} = 1;

	print STDERR "[Mioga2::ResourceList::Delete] Leaving\n" if ($debug);
}	# ----------  end of subroutine Delete  ----------


#===============================================================================

=head2 GetJournal

Get journal of operations as an object

=cut

#===============================================================================
sub GetJournal {
	my ($self) = @_;

	my $journal = tied (@{$self->{journal}});

	return ($journal);
}	# ----------  end of subroutine GetJournal  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


#===============================================================================

=head2 Load

Load object attributes from DB. This method is automatically called when trying to access an attribute whose value has not already been loaded from DB.

=cut

#===============================================================================
sub Load {
	my ($self) = @_;
	print STDERR "[Mioga2::ResourceList::Load] Entering\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Initialize and run SQL query
	#-------------------------------------------------------------------------------
	my $sql = 'SELECT m_resource.*, m_resource_status.ident AS status, m_user_base.firstname || \' \' || m_user_base.lastname || \' (\' || m_user_base.email || \')\' AS animator, m_theme.ident AS theme_ident, m_theme.name AS theme FROM m_resource, ' . $self->GetAdditionnalTables () . ' WHERE ' . $self->GetWhereStatement ();
	my $res = SelectMultiple ($self->{config}->GetDBH (), $sql);

	#-------------------------------------------------------------------------------
	# Update object contents
	#-------------------------------------------------------------------------------
	$self->{resources} = $res;

	# Raise "loaded" flag if query returned something
	$self->{loaded} = 1 if (scalar (@{$self->{resources}}));

	print STDERR "[Mioga2::ResourceList::Load] Leaving\n" if ($debug);
}	# ----------  end of subroutine Load  ----------


#===============================================================================

=head2 RequestForAttribute

Generate SQL statement that matches attribute into m_resource.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get associated SQL request.

=back

=item B<return:> an array containing an attribute name and the statement to
match this attribute in m_resource table. If the attribute is stored as-is
into table, this method returns the attribute and its value. If the attribute
is stores as the ID of a record in another table, this method returns the
attribute name and a SQL subquery picking the attribute ID from this other
table. If the attribute has to be ignored, this method returns empty strings.

=back

=cut

#===============================================================================
sub RequestForAttribute {
	my ($self, $attribute) = @_;

	my $request = '';
	my $value = '';
	my $operator = '=';
	my $attrval = '';

	#-------------------------------------------------------------------------------
	# Format string so that attribute value can be a scalar or an array
	#-------------------------------------------------------------------------------
	if (ref ($self->Get ($attribute)) eq 'ARRAY') {
		$operator = 'IN';
		for (@{$self->Get ($attribute)}) {
			$attrval = "(" . join (', ', map { $self->{config}->GetDBH()->quote ($_) } @{$self->Get ($attribute)}) . ")";
		}
		if ($attrval eq '') {
			$attrval = '(NULL)';
		}
	}
	elsif (ref ($self->Get ($attribute)) eq 'Mioga2::GroupList') {
		$operator = 'IN';
		my @rowids = $self->Get ($attribute)->GetRowIds ();
		$attrval = "(SELECT group_id FROM m_group_group WHERE invited_group_id IN (" . join (', ', @rowids) . ")";
		if ($attribute eq 'expanded_groups') {
			$attrval .= " UNION SELECT invited_group_id FROM m_group_expanded_user WHERE group_id IN (" . join (', ', @rowids) . ")";
		}
		$attrval .= ")";
		$attribute = 'rowid';
	}
	else {
		$attrval = $self->{config}->GetDBH()->quote ($self->Get ($attribute));
	}

	#-------------------------------------------------------------------------------
	# Process attribute name
	#-------------------------------------------------------------------------------
	if (grep (/^$attribute$/, qw/skeleton/)) {
		# Skeleton attribute is ignored as it doesn't match anything in DB
		# Password attribute is also ignored as it is stored encrypted and is not really significant to load user(s)
		$attribute = '';
		$value = '';
	}
	elsif (grep (/^$attribute$/, qw/status/)) {
		# Some attributes need their ID to be picked from another table which name is not m_$attribute
		$value = "(SELECT rowid FROM m_resource_$attribute WHERE ident $operator $attrval)";
		$attribute .= '_id';
	}
	elsif (grep (/^$attribute$/, qw/groups/)) {
		$value = $attrval;
		$attribute = 'rowid';
	}
	else {
		# Other attributes are simply associated to their text value
		$value = $attrval if ($self->Has ($attribute));
	}

	$request = "$attribute $operator $value" if ($attribute ne '' && $value ne '');

	return ($request);
}	# ----------  end of subroutine RequestForAttribute  ----------


#===============================================================================

=head2 Has

Check if a resource list has a given attribute set.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to check.

=back

=item B<return:> 1 or 0 whether the object has or not a given attribute in its
current state.

=back

B<Warning:> This method doesn't load the list from DB so it is possible that
the object does not have an attribute that the resources of the list will have
once they have been loaded.

=cut

#===============================================================================
sub Has {
	my ($self, $attribute) = @_;

	return (exists ($self->{update}->{$attribute}) || exists ($self->{attributes}->{$attribute}) || ($attribute eq 'rowid'));
}	# ----------  end of subroutine Has  ----------


#===============================================================================

=head2 Revert

Revert list.

=cut

#===============================================================================
sub Revert {
	my ($self) = @_;

	$self->{where} = 'm_resource.rowid NOT IN (SELECT m_resource.rowid FROM m_resource, ' . $self->GetAdditionnalTables () . ' WHERE ' . $self->{where} . ')';
	$self->{where} .= " AND m_resource.status_id = m_resource_status.rowid AND m_resource.theme_id = m_theme.rowid AND m_resource.anim_id = m_user_base.rowid AND m_resource.mioga_id = " . $self->{config}->GetMiogaId ();

	$self->{attributes} = { };
	$self->{update} = { };
	$self->{loaded} = 0;
	$self->{deleted} = 0;

	print STDERR "[Mioga2::ResourceList::Revert] New rules: " . Dumper $self->{where} if ($debug);
}	# ----------  end of subroutine Revert  ----------


#===============================================================================

=head2 GetDatabaseValues

Get a hash of keys / values that fit into database table. This method
translates the attributes that can not fit as-is into DB.

=cut

#===============================================================================
sub GetDatabaseValues {
	my ($self) = @_;
	print STDERR "[Mioga2::ResourceList::GetDatabaseValues] Entering\n" if ($debug);

	my %values = ();

	for (qw/ident location anim_id creator_id description/) {
		$values{$_} = $self->Get ($_);
	}

	# Get rowids from text attributes
	$values{status_id} = SelectSingle ($self->{config}->GetDBH (), "SELECT rowid FROM m_resource_status WHERE ident = " . ($self->Has ('status') ? $self->{config}->GetDBH()->quote ($self->Get ('status')) : "'active'"))->{rowid};

	print STDERR "[Mioga2::ResourceList::GetDatabaseValues] Values: " . Dumper \%values if ($debug);

	print STDERR "[Mioga2::ResourceList::GetDatabaseValues] Leaving\n" if ($debug);
	return (\%values);
}	# ----------  end of subroutine GetDatabaseValues  ----------


#===============================================================================

=head2 SetWhereStatement

Initialize a WHERE statement that matches resource list in m_resource according to
provided attributes.

=over

=item B<arguments:>

=over

=item I<@attributes:> a list of inner attributes to base the statement on.

=item B<return:> a string to paste as-is into a request, after the WHERE keyword.

=back

=back

=cut

#===============================================================================
sub SetWhereStatement {
	my ($self, @attributes) = @_;

	# Fetch attributes, if any
	for my $attr (@attributes) {
		$self->Get ($attr);
	}

	my $where = '';

	for my $attrname (@attributes) {
		next if (grep /^$attrname$/, qw/skeleton/);
		my $request = $self->RequestForAttribute ($attrname);
		$where .= "m_resource.$request AND " unless ($request eq '');
	}

	$where .= "m_resource.status_id = m_resource_status.rowid AND m_resource.theme_id = m_theme.rowid AND m_resource.anim_id = m_user_base.rowid AND m_resource.mioga_id = " . $self->{config}->GetMiogaId ();

	print STDERR "[Mioga2::ResourceList::SetWhereStatement] $where\n" if ($debug);

	$self->{where} = $where;

	return ($self->{where});
}	# ----------  end of subroutine SetWhereStatement  ----------


#===============================================================================

=head2 GetWhereStatement

Generate a WHERE statement that matches resource list in m_resource.

=over

=item B<return:> a string to paste as-is into a request, after the WHERE keyword.

=back

=cut

#===============================================================================
sub GetWhereStatement {
	my ($self) = @_;

	if (!$self->{where}) {
		$self->SetWhereStatement ();
	}

	return ($self->{where});
}	# ----------  end of subroutine GetWhereStatement  ----------


#===============================================================================

=head2 GetAdditionnalTables

Return list of additionnal tables to include into the FROM statement.

=cut

#===============================================================================
sub GetAdditionnalTables {
	my ($self) = @_;

	if (!$self->{additionnal_tables}) {
		$self->{additionnal_tables} = 'm_user_base, m_resource_status, m_theme';
	}

	return ($self->{additionnal_tables});
}	# ----------  end of subroutine GetAdditionnalTables  ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project

=head1 SEE ALSO

Mioga2 Mioga2::GroupList Mioga2::TeamList Mioga2::ApplicationList Mioga2::InstanceList

=head1 COPYRIGHT

Copyright (C) 2011, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;
