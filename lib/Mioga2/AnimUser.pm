# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
	
AnimUser.pm: The Mioga2 User animation application.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::AnimUser;
use strict;

use base qw(Mioga2::AnimGroup);
use Locale::TextDomain::UTF8 'animuser';

use Error qw(:try);
use Data::Dumper;
use Mioga2::AnimGroup;
use Mioga2::Content::XSLT;
use Mioga2::SimpleLargeList;
use Mioga2::LargeList::ApplicationList;
use Mioga2::AppDescList;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::APIProfile;
use Mioga2::tools::APIApplication;
use Mioga2::Exception::Simple;

my $debug = 0;

# ============================================================================
# Applications list description
# ============================================================================

my %applications_sll_desc = (
    'name' => 'anim_applications_sll',

    'sql_request' => {
        'select' => "m_application_user_status.*",

        "select_count" => "count(*)",

        "from" => "m_application_user_status, m_application_type",

        "where" => "m_application_user_status.mioga_id = ? AND m_application_user_status.group_id = ? AND " . "m_application_type.rowid = m_application_user_status.type_id AND " . "m_application_type.ident != 'resource'",
    },

    'fields' => [ [ 'rowid', 'rowid' ], [ 'ident', 'normal' ], [ 'realident', 'hidden' ], [ 'change_app_status', 'checkboxes', ' (! ($context->GetGroup()->GetRowid() == $config->GetAdminId() and $_->{realident} eq "Admin")) and $_->{realident} !~ /^Anim/' ], [ 'is_public', 'link', 'ChangeApplicationPublic', '$_->{can_be_public} and $_->{status} == 1' ], ],

);

# ============================================================================
# Profiles list description
# ============================================================================

my $profile_autonomous_user_fields = [ [ 'rowid', 'rowid' ], [ 'ident', 'link', 'ModifyProfile' ], [ 'nb_member', 'link', 'DisplayProfileUsers' ], [ 'delete', 'delete', '$_->{nb_member} == 0', \&Mioga2::AnimGroup::DeleteProfileHook ], ];

my $profile_not_autonomous_user_fields = [ [ 'rowid', 'rowid' ], [ 'ident', 'link', 'ModifyProfile' ], [ 'nb_member', 'link', 'DisplayProfileUsers' ], ];

my %profiles_sll_desc = (
    'name' => 'anim_profiles_sll',

    'sql_request' => {
        'select' => "m_profile.*, (SELECT count(*) FROM m_profile_group WHERE profile_id = m_profile.rowid) AS nb_member",

        "select_count" => "count(*)",

        "from" => "m_profile",

        "where" => "group_id = ?",
    },

    'fields' => $profile_not_autonomous_user_fields,

);

# ============================================================================
# Groups where the user is invited.
# ============================================================================

my %group_list_sll_desc = (
    'name' => 'user_group_list_sll',

    'sql_request' => {
        'select' => "DISTINCT groups.rowid AS rowid, groups.ident AS ident, groups.description AS description ",

        "select_count" => "count(*)",

        "from" => " ( SELECT DISTINCT m_group.rowid, m_group.ident, m_group.description FROM m_group_group, m_group, m_group_type " . "    WHERE  m_group_group.invited_group_id = ? AND " . "           m_group.rowid = m_group_group.group_id AND " . "           m_group_type.rowid = m_group.type_id AND " . "           m_group_type.ident = 'group' " .

          " UNION " .

          "  SELECT DISTINCT m_group.rowid, m_group.ident, m_group.description FROM m_group_expanded_user, m_group, m_group_type " . "   WHERE m_group_expanded_user.invited_group_id = ? AND " . "         m_group.rowid = m_group_expanded_user.group_id AND " . "         m_group_type.rowid = m_group.type_id AND " . "         m_group_type.ident = 'group'  ) AS groups ",

        "where" => "1=1",

    },

    'fields' => [ [ 'rowid', 'rowid' ], [ 'ident', 'normal' ], [ 'description', 'hidden' ], ],

    'search' => ['ident'],

);

# ============================================================================
#
# CheckUserAccessOnMethod ($context, $user, $group, $method_ident)
#
#	Check if the user $user working in group $group can access to
#	the method $method_ident in the current application .
#
# ============================================================================

sub CheckUserAccessOnMethod {
    my ($self, $context, $user, $group, $method_ident) = @_;

    if (
        !$user->IsAutonomous()
        and grep { $_ eq $method_ident }
        qw(DisplayApplications ChangeApplicationStatus
        ChangeApplicationPublic CreateProfile)
      )
    {
        return AUTHZ_FORBIDDEN;
    }

    return $self->SUPER::CheckUserAccessOnMethod($context, $user, $group, $method_ident);
}

# ============================================================================
#
# Initialize ()
#
#	Method that can be overloaded by Application implementation to initialize
#	variable, resource, etc, at the object creation time.
#
#	Return nothing.
#
# ============================================================================

sub Initialize {
    my ($self) = @_;

    $self->{stylesheet}    = "anim_user.xsl";
    $self->{appname}       = "AnimUser";
    $self->{defaultfunc}   = "DisplayUsers";
    $self->{locale_domain} = 'animuser_xsl';

    return;
}

# ============================================================================
#
# GetAppDesc ()
#
#	Return the current application description.
#
# ============================================================================

sub GetAppDesc {
    my ($self, $context) = @_;

    my %AppDesc = (
        ident              => 'AnimUser',
        name               => __("Home"),
        package            => 'Mioga2::AnimUser',
        description        => __('The Mioga2 User Animation Application '),
        type               => 'normal',
        all_groups         => 0,
        all_users          => 1,
        can_be_public      => 0,
        is_user            => 1,
        is_group           => 0,
        is_resource        => 0,

        functions => {
            'Animation' => __('Animation Functions'),
            'Users'     => __('User Functions'),
        },

        func_methods => {
            'Animation' => [ 'DisplayMain', 'DisplayUsers', 'DisplayTeams', 'DisplayThemes', 'InviteUsers', 'InviteTeams', 'DisplayApplications', 'DisplayProfiles', 'ChangeApplicationStatus', 'ChangeApplicationPublic', 'CreateProfile', 'ModifyProfile', 'DisplayProfileUsers', 'DisplayProfileTeams', 'InviteProfileUsers', 'InviteProfileTeams', 'GetGroups', 'GetAppList', 'RightsForUser' ],
            'Users'     => [ 'AppList',     'GroupList' ],
        },

        non_sensitive_methods => [ 'DisplayMain', 'DisplayUsers', 'DisplayTeams', 'DisplayThemes', 'InviteUsers', 'InviteTeams', 'DisplayApplications', 'DisplayProfiles', 'ChangeApplicationStatus', 'ChangeApplicationPublic', 'CreateProfile', 'ModifyProfile', 'DisplayProfileUsers', 'DisplayProfileTeams', 'InviteProfileUsers', 'InviteProfileTeams', 'GetGroups', 'GetAppList', 'RightsForUser',  'AppList',     'GroupList' ],

    );
    return \%AppDesc;
}

# ============================================================================

=head2 DisplayApplications ()

	Display the Mioga2 Application List.

	See Mioga2::AnimGroup->DisplayApplications

=cut

# ============================================================================

sub DisplayApplications {
    my ($self, $context) = @_;

    return $self->SUPER::DisplayApplications($context, \%applications_sll_desc);
}

# ============================================================================

=head2 DisplayProfiles ()

	Display the Mioga2 Profile List.

	See Mioga2::AnimGroup->DisplayProfiles

=cut

# ============================================================================

sub DisplayProfiles {
    my ($self, $context) = @_;

    my $xml;
    my $group = $context->GetGroup();
    if ($group->IsAutonomous()) {
        $profiles_sll_desc{fields} = $profile_autonomous_user_fields;
        $xml = "<is_autonomous/>";
    }
    else {
        $profiles_sll_desc{fields} = $profile_not_autonomous_user_fields;
    }

    my $sll_desc = \%profiles_sll_desc;

    return $self->SUPER::DisplayProfiles($context, $sll_desc, $xml);
}

# ============================================================================

=head2 InviteProfileUsers ()

	Invite users member of group in a profile.

	Redirect to referer with InlineMessage or redirect to SelectMultipleUsersMemberOfGroup

=cut

# ============================================================================

sub InviteProfileUsers {
    my ($self, $context) = @_;

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['rowids'], 'allow_empty', [ 'mioga_user_ids', $config ] ], [ ['select_users_back'], 'allow_empty' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimUser::InviteProfileUsers", __("Wrong argument value.") . Dumper $errors);
    }

    # ------------------------------------------------------
    # Check submited values
    # ------------------------------------------------------
    if (st_ArgExists($context, 'select_users_back')) {

        # Invite Users
        if (exists $context->{args}->{rowids}) {
            my $dbh     = $config->GetDBH();
            my $group   = $context->GetGroup();
            my $rowid   = $group->GetRowid();
            my $prof_id = $session->{AnimGroup}->{InviteProfileUsers}->{rowid};

            my @rowids = split(',', $context->{args}->{rowids});

            foreach my $grp_id (@rowids) {
                next if ($grp_id == $group->GetAnimId());

                ProfileAddGroup($dbh, $prof_id, $rowid, $grp_id);
            }

            $session->{__internal}->{message}{type} = 'info';
            $session->{__internal}->{message}{text} = __('Users successfully invited.');
            my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{InviteProfileUsers}->{referer});
            return $content;
        }

        else {
            my $content = new Mioga2::Content::REDIRECT($config, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{InviteProfileUsers}->{referer});
            return $content;
        }

    }

    if (exists $context->{args}->{rowid}) {
        $session->{AnimGroup}->{InviteProfileUsers}->{rowid} = $context->{args}->{rowid};
    }
    else {
        $session->{AnimGroup}->{InviteProfileUsers}->{rowid} = $session->{AnimGroup}->{DisplayProfileUsers}->{rowid};
    }

    $session->{AnimGroup}->{InviteProfileUsers}->{referer} = $context->GetReferer();
    $session->{AnimGroup}->{InviteProfileUsers}->{referer} =~ s/\?(.*)$//;

    return $self->LaunchSelectMultipleUsers($context);
}

# ============================================================================

=head2 InviteProfileTeams ()

	Invite teams member of group in a profile.

	Redirect to referer with InlineMessage or redirect to SelectMultipleTeamsMemberOfGroup

=cut

# ============================================================================

sub InviteProfileTeams {
    my ($self, $context) = @_;

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['rowids'], 'allow_empty', [ 'mioga_team_ids', $config ] ], [ ['select_teams_back'], 'allow_empty' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimUser::InviteProfileTeams", __("Wrong argument value.") . Dumper $errors);
    }

    # ------------------------------------------------------
    # Check submited values
    # ------------------------------------------------------
    if (st_ArgExists($context, 'select_teams_back')) {

        # Invite Teams
        if (exists $context->{args}->{rowids}) {
            my $dbh     = $config->GetDBH();
            my $rowid   = $context->GetGroup()->GetRowid();
            my $prof_id = $session->{AnimGroup}->{InviteProfileTeams}->{rowid};

            my @rowids = split(',', $context->{args}->{rowids});

            foreach my $grp_id (@rowids) {

                ProfileAddGroup($dbh, $prof_id, $rowid, $grp_id);
            }

            $session->{__internal}->{message}{type} = 'info';
            $session->{__internal}->{message}{text} = __('Teams successfully invited.');
            my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{InviteProfileTeams}->{referer});
            return $content;
        }

        else {
            my $content = new Mioga2::Content::REDIRECT($config, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{InviteProfileTeams}->{referer});
            return $content;
        }

    }

    if (exists $context->{args}->{rowid}) {
        $session->{AnimGroup}->{InviteProfileTeams}->{rowid} = $context->{args}->{rowid};
    }
    else {
        $session->{AnimGroup}->{InviteProfileTeams}->{rowid} = $session->{AnimGroup}->{DisplayProfileTeams}->{rowid};
    }

    $session->{AnimGroup}->{InviteProfileTeams}->{referer} = $context->GetReferer();
    $session->{AnimGroup}->{InviteProfileTeams}->{referer} =~ s/\?(.*)$//;

    return $self->LaunchSelectMultipleTeams($context);
}

# ============================================================================

=head2 GroupList ()

	Display the list of group where the current user is invited.



=head3 Generated XML :
    
    <GroupList>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <!--
            SimpleLargeList description. See Mioga2::SimpleLargeList
         -->
    
    </GroupList>

=head3 List fields :

    user_group_list_sll : List group where the current user is invited
    
    Available fields :
        name             type       description
    ----------------------------------------------------------------
      rowid           : rowid       : the group rowid 
      ident           : normal      : the group ident
      description     : normal      : the group description 

=head3 Form actions and fields

    SimpleLargeList actions. See Mioga2::SimpleLargeList.

=cut

# ============================================================================

sub GroupList {
    my ($self, $context) = @_;

    my $user = $context->GetUser();

    # ------------------------------------------------------
    # Generate the XML
    # ------------------------------------------------------

    my $sll = new Mioga2::SimpleLargeList($context, \%group_list_sll_desc);

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<GroupList>";

    $xml .= $context->GetXML();

    if (exists $context->{args}->{link_path}) {
        $xml .= "<link_path>" . st_FormatXMLString($context->{args}->{link_path}) . "</link_path>";
    }

    $xml .= $sll->Run($context, [ $user->GetRowid(), $user->GetRowid() ]);

    $xml .= "</GroupList>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});
    $content->SetContent($xml);

    return $content;
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================
#
# ModifyTheme ()
#
#	commit themes_lang selection in database.
#
# ============================================================================

sub ModifyThemeAndLang {
    my ($self, $config, $values) = @_;

    UserModify($config, $values);
}

# ============================================================================
#
#=head2 ModifyDefaultProfile ()
#
#	commit default profile modification in database.
#
# ============================================================================

sub ModifyDefaultProfile {
    my ($self, $config, $group, $profile_id) = @_;

    UserModify($config, { rowid => $group->GetRowid(), default_profile_id => $profile_id });
}

# ============================================================================
#
# GetSelectedApps ()
#
#	Return the list of selected apps for group
#
# ============================================================================

sub GetSelectedApps {
    my ($self, $config, $group_id) = @_;

    my $mioga_id = $config->GetMiogaId();
    my $dbh      = $config->GetDBH();

    return SelectMultiple($dbh, "select rowid from m_application_user_status where mioga_id = $mioga_id AND group_id = $group_id and status > 0");
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


