# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Authz.pm : Authorization Apache Handler

=head1 DESCRIPTION

This module is the Mioga Apache/mod_perl authorization handler. It : 

=over 4

=item 1 

Create Mioga2::RequestContext object from Apache request.
	
=item 2

Create current group ( and resource ) object from group ident given in URL

=item 3

For "applications" connection :

=over 4

=item 1

Create the Application Object

=item 2

Run TestAuthorize

=item 3

Store  Group (and Resource)  and Application in RequestContext.

=item 4

Store RequestContext in pnotes.

=back

=item 4

For WebDav connection :

=over 4

=item 1

Check ACLs

=back

=back

=head1 APACHE CONFIGURATION

 <Location /foo>
    SetHandler  perl-script
    AuthType Basic
    AuthName Mioga
    require valid-user
    PerlSetVar MiogaConfig /path/to/Mioga.conf
    PerlAuthenHandler Mioga2::Authen
    PerlAuthzHandler Mioga2::Authz
    PerlHandler Mioga2::Dispatch
 </Location>

The variable MiogaConfig must be set to the path of the Mioga.conf file.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Authz;

use Mioga2::Apache;

use strict;
use Error qw(:try);
use Data::Dumper;
use Mioga2::Old::User;
use Mioga2::Old::Group;
use Mioga2::Old::GroupBase;
use Mioga2::Old::Resource;
use Mioga2::Config;
use Mioga2::RequestContext;
use Mioga2::Exception::Group;
use Mioga2::Exception::User;
use Mioga2::Exception::Resource;
use Mioga2::Exception::Application;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::string_utils;
use Mioga2::Constants;

my $debug = 0;
my $timer = 0;

# ============================================================================

=head2 Authz::CreateGroup ($context, $request)

	Create the current group or resource object and store this object 
	in context.

	Return Mioga2::Apache::HTTP_OK or Mioga2::Apache::HTTP_SERVER_ERROR.
	
=cut

# ============================================================================

sub CreateGroup {
	my ($context, $r) = @_;

	my $uri = $context->GetURI();
	my $config = $context->GetConfig();
	my $group_ident = $context->GetGroupIdent();
	my $auth_return = Mioga2::Apache::HTTP_OK;

	my $group;
	my $bench;
	if ($timer) {
		$bench = new Benchmark::Timer();
		$bench->start("Mioga2::Authz::CreateGroup $$ ");
	}
	try {
		$group = Mioga2::Old::GroupBase->CreateObject($config, ident => $group_ident);
	}
	catch Mioga2::Exception::User with {
		my $err = shift;
		$r->warn("==> Mioga2::Authz::CreateGroup : No such user $group_ident.");
		$auth_return = Mioga2::Apache::HTTP_FORBIDDEN;
	}
	catch Mioga2::Exception::Group with {
		my $err = shift;
		$r->warn("==> Mioga2::Authz::CreateGroup : No such group $group_ident.");
		$auth_return = Mioga2::Apache::HTTP_FORBIDDEN;
	}
	catch Mioga2::Exception::Resource with {
		my $err = shift;
		$r->warn("==> Mioga2::Authz::CreateGroup : No such Group or Resource $group_ident.");
		$auth_return = Mioga2::Apache::HTTP_FORBIDDEN;
	}
	catch Mioga2::Exception::Simple with {
		my $err = shift;
		$r->log_error("==> Mioga2::Authz::handler : Exception Simple = " . $err->as_string());
		$auth_return = Mioga2::Apache::HTTP_SERVER_ERROR;
	}
	otherwise {
		my $err = shift;
		$r->log_error("==> Mioga2::Authz::handler : Other exception ");
		$r->log_error(Dumper($err));
		$auth_return = Mioga2::Apache::HTTP_SERVER_ERROR;
	};

	if($auth_return == Mioga2::Apache::HTTP_OK) {
		if($group->GetStatus() ne 'active') {
			$auth_return = Mioga2::Apache::HTTP_NOT_FOUND;
		}
		else {
			$context->SetGroup($group);
		}
	}
	
	if($timer)  {
		$bench->stop();
		$r->warn($bench->report());
	}
	return $auth_return;
}

	

# ============================================================================

=head2 Authz::IsUserInGroup ($context)

	Check the current user is invited in current group.
	
	return Mioga2::Apache::HTTP_OK, Mioga2::Apache::HTTP_FORBIDDEN or Mioga2::Apache::HTTP_SERVER_ERROR
	
=cut

# ============================================================================

sub IsUserInGroup {
	my ($context) = @_;
	
	my $group = $context->GetGroup();

	my $user = $context->GetUser();
	
	if($group->IsUserInGroup($user)) {
		return Mioga2::Apache::HTTP_OK;
	}


	return Mioga2::Apache::HTTP_AUTH_REQUIRED;
}


# ============================================================================

=head2 Authz::CheckAuthorization ($context, $request)

	Check application profiles or webdav ACLs.
	
	return Mioga2::Apache::HTTP_OK, Mioga2::Apache::HTTP_FORBIDDEN or Mioga2::Apache::HTTP_SERVER_ERROR
	
=cut

# ============================================================================

sub CheckAuthorization {
	my ($context, $r) = @_;
	
	my $uri = $context->GetURI();
	my $auth_return = Mioga2::Apache::HTTP_FORBIDDEN;

	try {
		#-----------------------------------------------------------
		# For "application" connection, create the Application 
		# object and run TestAutorise
		#-----------------------------------------------------------
		my $app_desc = $context->GetAppDesc();

		if($uri->IsXML() and ! $app_desc->CanBeXML($context->GetMethod())) {
			$auth_return = Mioga2::Apache::HTTP_FORBIDDEN;
		}
		elsif($uri->IsApplication() or $uri->IsXML()) {
			my $application = $context->CreateApplication();
			$auth_return = $application->TestAuthorize($context);
			$r->warn("Authz::CheckAuthorization : after TestAuthorize auth_return = $auth_return") if ($debug);

			if($auth_return == AUTHZ_OK) {
				$auth_return = Mioga2::Apache::HTTP_OK;
			}
			elsif($auth_return == AUTHZ_DISCONNECT) {
				$auth_return = Mioga2::Apache::HTTP_AUTH_REQUIRED;
			}
			else {
				$auth_return = Mioga2::Apache::HTTP_FORBIDDEN;
			}
			$r->warn("Authz::CheckAuthorization : after TestAuthorize 2 auth_return = $auth_return") if ($debug);
		}
	}
	catch Mioga2::Exception::DB with {
		my $err = shift;
		$r->log_error("==> Mioga2::Authz::handler : Exception DB = " . $err->as_string());
		$auth_return = Mioga2::Apache::HTTP_SERVER_ERROR;
	}
	catch Mioga2::Exception::Simple with {
		my $err = shift;
		$r->log_error("==> Mioga2::Authz::handler : Exception Simple = "  . $err->as_string());
		$auth_return = Mioga2::Apache::HTTP_SERVER_ERROR;
	}
	otherwise {
		my $err = shift;
		$r->log_error("==> Mioga2::Authz::handler : Other execption ");
		$r->log_error(Dumper($err));
		$auth_return = Mioga2::Apache::HTTP_SERVER_ERROR;
	};

	$r->warn("Authz::CheckAuthorization : auth_return = $auth_return") if ($debug);
	return $auth_return;
}


# ============================================================================

=head2 Authz::CheckWebDavAuthz ($context)

	Check URI ACLs for WebDav connections.

	return an Apache::Constant constant like Mioga2::Apache::HTTP_OK or Mioga2::Apache::HTTP_FORBIDDEN.
	
=cut

# ============================================================================

sub CheckWebDavAuthz {
	my ($r, $config, $uri_obj, $user) = @_;

	my $auth_return = AUTHZ_NONE;

	my $uri = $uri_obj->GetURI();
	my $access_method = $r->method();
	my $access;
	if ( grep { $access_method eq $_ } qw(COPY OPTIONS GET HEAD PROPFIND)) {
		$access = AUTHZ_READ;
	}
	else {
		$access = AUTHZ_WRITE;
	}
	my $private_uri = $config->GetPrivateURI();
	my $public_uri  = $config->GetPublicURI();

	#-----------------------------------------------------------
	# If the current group is a resource, return Mioga2::Apache::HTTP_FORBIDDEN 
	# because resources does not have WebDAV shares.
	#-----------------------------------------------------------
	#if($group->GetType() eq 'resource') {
	#	$r->warn("==> Mioga2::Authz::CheckWebDavAuthz : WebDav is forbidden on resources") if ($debug);
	#	return Mioga2::Apache::HTTP_FORBIDDEN;
	#}

	$r->warn("Test access on method $access_method for URI = $uri for user = ".$user->GetIdent()) if ($debug);

	#-----------------------------------------------------------
	# For COPY command look for read access on source and write 
	# acces on destination
	#-----------------------------------------------------------
	my $right;
	
	if(($access_method eq 'DELETE' or $access_method eq 'MOVE') and
	   ( $uri =~ m!^$private_uri/[^/]+$! or $uri =~ m!^$public_uri/[^/]+$! )) {
		return Mioga2::Apache::HTTP_FORBIDDEN;
	}
	elsif($access_method eq 'COPY') {
		$right = AuthzTestAccessForURI($config, $uri_obj, $user, undef, 1);

		if(defined $right and $right) {
			my $desturi = $r->headers_in->{'Destination'};
			$desturi =~ s/http(?:s?):\/\/[^\/]+//;
			$desturi = st_URIUnescape($desturi);

			$desturi = new Mioga2::URI($config, uri=>$desturi);
			
			$right = AuthzTestAccessForURI($config, $desturi, $user, undef, 2);
		}
	}
	else {	
		$right = AuthzTestAccessForURI($config, $uri_obj, $user, undef, $access);
	}

	if ( defined $right and $right ) {
		$auth_return = Mioga2::Apache::HTTP_OK;

	#	my $method = $context->GetAccessMethod();
		
	#	if($method =~ /^(UN)?LOCK$/) {
			#$auth_return = TestUriLock($config, $method, $user, $uri);
	#	}

	}
	elsif ( defined $right and ! $right ) {
		$auth_return = Mioga2::Apache::HTTP_FORBIDDEN;
	}
	elsif ($access_method ne 'GET') {
		# if method PROPFIND is invoked, let mod_dav return
		# a 207 error or 404 error. Some tools put a lock on
		# file before publishing and mak a PROPFIND after, just before
		# PUT datas.
		# Clearly this is done if read access on parent is granted !
		
		$auth_return = Mioga2::Apache::HTTP_NOT_FOUND;

		# If not found, check parent right (for MKCOL, DELETE, MOVE ...)

		$r->warn("   Resource not found test for parent") if ($debug);

		my ($parent_uri) = ($uri =~ /^(.*)\/[^\/]+(\/)?$/);
		$r->warn("   parent uri = $parent_uri") if $debug;
		$parent_uri = new Mioga2::URI($config, uri => $parent_uri);
		$right = AuthzTestAccessForURI($config, $parent_uri, $user, undef, $access);

		if ( defined $right and $right ) {
			$auth_return = Mioga2::Apache::HTTP_OK;
		}
		elsif(defined $right and ! $right ) {
			$auth_return = Mioga2::Apache::HTTP_FORBIDDEN;			
		}
	}
	else {
		$auth_return = Mioga2::Apache::HTTP_NOT_FOUND;
	}

	return $auth_return;
}

# ============================================================================

=head2 Authz::handler ($request)

	Apache/mod_perl handler main function. $request is a the Apache request 
	object.

	return an Apache::Constant constant like Mioga2::Apache::HTTP_OK or Mioga2::Apache::HTTP_FORBIDDEN.
	
=cut

# ============================================================================

sub handler : method {
    my($class, $r) = @_;

	$r->warn("==> Mioga2::Authz::handler") if ($debug > 0);

	my $cache_hit = 0;
	my $bench;
	my $overall_bench;

	if($timer) {
		$bench = new Benchmark::Timer();
		$bench->start("Mioga2::Authz $$ 1");
		$overall_bench = new Benchmark::Timer();
		$overall_bench->start("Mioga2::Authz $$ overall");
	}
	#-----------------------------------------------------------
	# Retrieve config and user from pnotes
   	#-----------------------------------------------------------
	my $config = $r->pnotes("config");
	my $user = $r->pnotes("user");
	my $uri_obj = $r->pnotes("uri_obj");
	my $authen_web = $r->pnotes ('authen_web');
	my $user_auth = { };
	if ($authen_web) {
		$user_auth->{session_token} = $r->pnotes ('session_token');
	}
	else {
		$user_auth->{plain_password} = $r->pnotes ('plain_password');
	}

	if((!defined($config)) or (!defined($user)) or (!defined($uri_obj)))
	{
		$r->warn("==> Mioga2::Authz::handler : Can't retrieve config or user from pnotes");
		return Mioga2::Apache::HTTP_SERVER_ERROR;
	}
	my $uri = $uri_obj->GetURI();
	$r->warn("User ident =  " . $user->GetIdent()) if ($debug);
	$r->warn("URI  =  " . $uri) if ($debug);
	$r->warn("method  =  " . $r->method()) if ($debug);
	$r->warn("IsWebDAV  =  " . $uri_obj->IsWebDav()) if ($debug);
	#-----------------------------------------------------------
	# Check if the instance is locked and if the user is not 
	# admin 
   	#-----------------------------------------------------------
	if($config->IsLocked() and $user->GetRowid() != $config->GetAdminId()) {
		return Mioga2::Apache::HTTP_FORBIDDEN;
	}
	my $auth_return = Mioga2::Apache::HTTP_OK;
	if($timer) {
		$bench->stop();
		$r->warn($bench->report());
		$bench = new Benchmark::Timer();
		$bench->start("Mioga2::Authz $$ 2");
	}
	#-----------------------------------------------------------
	# if WebDAV test for Authz, no need for session
	# else construct RequestContext and proceed
	#-----------------------------------------------------------
	my $context;

	if ($uri_obj->IsWebDav() && ! $uri_obj->IsRootWebDav())
	{
		$auth_return = CheckWebDavAuthz($r, $config, $uri_obj, $user);
		$context = new Mioga2::RequestContext($r, $config);
		$context->SetUser($user, $user_auth);
	}
	else
	{
		try  {
			#-----------------------------------------------------------
			# Create Request Context
			#-----------------------------------------------------------
			$context = new Mioga2::RequestContext($r, $config);
			if($timer) {
				$bench->stop();
				$r->warn($bench->report());
				$bench = new Benchmark::Timer();
				$bench->start("Mioga2::Authz $$ 3");
			}
			#-----------------------------------------------------------
			# Store user in Request Context
			#-----------------------------------------------------------
			$context->SetUser($user, $user_auth);
			if($timer) {
				$bench->stop();
				$r->warn($bench->report());
				$bench = new Benchmark::Timer();
				$bench->start("Mioga2::Authz $$ 4");
			}
			#-----------------------------------------------------------
			# For private uri root access, don't check ACLs, 
			# just return Mioga2::Apache::HTTP_OK and let the RootDav handler to do its job.
			#-----------------------------------------------------------
			my $private_uri = $config->GetPrivateURI();
			if($uri !~ /^$private_uri(\/)?$/) {
				#-----------------------------------------------------------
				# Create the current group object.
				# If the creation fail with "no such group error", try to
				# create a resource object.
				#-----------------------------------------------------------
				$auth_return = CreateGroup($context, $r);
				if($timer) {
					$bench->stop();
					$r->warn($bench->report());
					$bench = new Benchmark::Timer();
					$bench->start("Mioga2::Authz $$ 5");
				}
				#-----------------------------------------------------------
				# If user not invited in this group, return Mioga2::Apache::HTTP_AUTH_REQUIRED.
				#-----------------------------------------------------------
				if($auth_return == Mioga2::Apache::HTTP_OK) {
					$auth_return = IsUserInGroup($context);
					if (($auth_return != Mioga2::Apache::HTTP_OK) && $context->IsAuthenWeb ()) {
						# AuthenWeb, do not return HTTP-401, redirect to login instead
						$r->headers_out->{Location} = Mioga2::Login::GetLoginURI ($config->GetMiogaConf (), $uri_obj);
						$auth_return = Mioga2::Apache::HTTP_REDIRECT;
					}
					if($timer) {
						$bench->stop();
						$r->warn($bench->report());
						$bench = new Benchmark::Timer();
						$bench->start("Mioga2::Authz $$ 6");
					}
				}
				#-----------------------------------------------------------
				# Check Application profiles
				#-----------------------------------------------------------
				if($auth_return == Mioga2::Apache::HTTP_OK) {
					$auth_return = CheckAuthorization($context, $r);
				}

			}
			else {
				$auth_return = Mioga2::Apache::HTTP_OK;
			}
		}
		catch Mioga2::Exception::Application with {
			my $err = shift;
			$r->warn("==> Mioga2::Authz::handler : Exception Application = " . $err->as_string());
			$auth_return = Mioga2::Apache::HTTP_NOT_FOUND;
		}
		catch Mioga2::Exception::Simple with {
			my $err = shift;
			$r->warn("==> Mioga2::Authz::handler : Exception Simple = " . $err->as_string());
			$auth_return = Mioga2::Apache::HTTP_NOT_FOUND;
		}
		otherwise {
			my $err = shift;
			$r->warn("==> Mioga2::Authz::handler : Exception other = " . Dumper($err));
			$auth_return = Mioga2::Apache::HTTP_SERVER_ERROR;
		};
	}

	if($auth_return == Mioga2::Apache::HTTP_AUTH_REQUIRED) {
 		$r->note_basic_auth_failure;
	}
	$r->warn("Mioga2::Authz end : auth_return = $auth_return") if ($debug > 0);
	#-----------------------------------------------------------
	# Store the context in pnotes.
   	#-----------------------------------------------------------
	$r->pnotes("context" => $context);
	if($timer)  {
		$bench->stop();
		$r->warn($bench->report());
		$overall_bench->stop();
		$r->warn($overall_bench->report());
	}

	return $auth_return;
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga.conf Mioga

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
