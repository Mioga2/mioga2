# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
# Description:

=head1 NAME

Mioga2::SimpleLargeList - package used to build simple large list.

=head1 DESCRIPTION

The simple large list was designed to quickly developpe paginated lists.
It's a more "easy to use" implementation of LargeList.

=cut

package Mioga2::SimpleLargeList;
use strict;

use base qw(Mioga2::LargeList);
use Locale::TextDomain::UTF8 'simplelargelist';

use Mioga2::tools::database;
use Mioga2::tools::date_utils;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::Exception::Simple;
use Mioga2::Old::UserList;
use Mioga2::Old::ResourceList;
use Mioga2::Old::GroupList;
use Mioga2::Old::TeamList;
use Data::Dumper;
use Error qw(:try);

my $debug = 0;

# ============================================================================

=head2 new

    new SimpleLargeList($context, $params);

    Create a new LargeList.

    Required parameters are :


=over 4

=item - context :
    The current Mioga::RequestContext.

=item - params :
    A hasref like :
    

    { 'name' => $list_name,
      
      'select_request' => { sql request description },

      'fields'  => [ list of fields to display ],
      
      'search'  => [ list of fields which the user can search on ],

      'actions' => [ list of actions description ],

      'nb_lines_per_page' => default value of nb lines per page,

=back

=head3 SQL Request description

    'select_request' => { 'select' => select clause,

                          "select_count" => select clause for count request,
    
                          "from"   => from clause,

                          "where"  => where clause,
						  
                          "delete" => { "from"  => delete from clause,
                                        "where" => where clause for delete request,
                                      }
                         }


    With description, the SimpleLargeLists build a SQL request based on :

        SELECT <select clause> 

        FROM   <from clause>

        WHERE  <where clause>

    To display the list, and 

        SELECT <select_count clause> 

        FROM   <from clause>

        WHERE  <where clause>

    to count the entry to display.

    The count request must return a field named 'count'.
    The other request must return fields named exactly like fields described in the 'fields' section.


    The delete section is used to delete elements in the table.
    The from clause contains the name of table to delete in.
    The where clause must be like :
        "my_table.rowid = $_->{rowid}"
        
        $_ represent the currently selected field.
        So $_->{field_name} is the value of 'field_name' in the current selected field.


=head3 Displayed fields description

     'fields' => [ [ name, type, arguments],
				   ...
                 ],

	 
	 name is the name of the field and must be distinct.

	 There are 8 differents types of field :

=over 4 

=item rowid : 
    ["myrowid", "rowid"]

    This field is required and not displayed.
	The name of this field is the name of the rowid in the request.
	This field will be passed as arguments to Functions used in ohter field type.

=item normal :
    ["field1", "normal"]

    Represent a normal field. It is just display as is in the list.

=item link :
    ["field2", "link", "<url>", "auth_expr"]
    
	This field is displayed like a link pointing on <URL>.
	
	auth_expr is optional. It is a perl expression like :
	 '$_->{myfield} != $forbiddenvalue'
    $_ represent the current entry. If this expression return a false value (0).
	The link is displayed like a "normal" field.
	 
	An argument with the current field rowid is appended to the URL.

=item email :
    ["field3", "email"]
    
	This field is an email and is displayed like a mailto: link.

=item user_.* :
    ["field4", "user_.*", "user_rowid",  <other_type_desc>]
    
	This field is a user attribute (user_ident, user_firstname, ...).
	"user_rowid" is the name of the field in the SQL resquest representing
	the user rowid. <other_type_desc> if one of the other types of field 
	descrition (normal, link, ...). <other_type_desc> is "normal" by default.
	Ex : ["field4", "user_.*", "user_rowid", "link", "<url>", "auth_expr"];

=item group_.* :
    ["field5", "group_.*", "group_rowid", <other_type_desc>]
	
	This field is a group attribute (group_ident, group_description, ...).
	"group_rowid" is the name of the field in the SQL resquest representing
	the group rowid. <other_type_desc> if one of the other types of field 
	descrition (normal, link, ...). <other_type_desc> is "normal" by default.
	Ex : ["field4", "group_.*", "group_rowid", "link", "<url>", "auth_expr"];
		
=item team_.* :
    ["field5", "team_.*", "team_rowid", <other_type_desc>]
	
	This field is a team attribute (team_ident, team_description, ...).
	"team_rowid" is the name of the field in the SQL resquest representing
	the team rowid. <other_type_desc> if one of the other types of field 
	descrition (normal, link, ...). <other_type_desc> is "normal" by default.
	Ex : ["field4", "team_.*", "team_rowid", "link", "<url>", "auth_expr"];

=item resource_.* :
    ["field5", "resource_.*", "resource_rowid", <other_type_desc>]
	
	This field is a resource attribute (resource_ident, resource_location, ...).
	"resource_rowid" is the name of the field in the SQL resquest representing
	the resource rowid. <other_type_desc> if one of the other types of field 
	descrition (normal, link, ...). <other_type_desc> is "normal" by default.
	Ex : ["field4", "resource_.*", "resource_rowid", "link", "<url>", "auth_expr"];

=item checkboxes :
    ["checkboxes", "checkboxes", "auth_expr"]

    Display a checkboxe in as value.
	if auth_expr fails, the checkboxe is not displayed. (see link for auth_expr description).

=item modify :
    ["modify", "modify", "Function", "auth_expr"]

    Display a "modify" icon pointing on the function Function.
	if auth_expr fails, the icon is not displayed. (see link for auth_expr description).

=item delete :
    ["delete", "delete", "auth_expr"]

    Display a check box in the "delete" column.
	if auth_expr fails, the check box is not displayed. (see link for auth_expr description).

=back


=head3 'search' field description

     'search' => ['field1', 'field2', ...],



=head3 'action' field description

	 'actions' => [ ['Name',  'Function'],
					...
					],

	  Display a menu on top of the list.
	  
       
=cut

# ============================================================================

sub new {
    my ($class, $context, $params, $dbh) = @_;

    Mioga2::LargeList::CheckArgs($context);

    my $self = $class->SUPER::new($context, $params->{name});

    $self->{sll_params} = $params;

    if (exists $self->{sll_params}->{nb_lines_per_page}) {
        $self->{nb_elem_per_page} = int($self->{sll_params}->{nb_lines_per_page});
        if ($self->{nb_elem_per_page} > 999) {
            $self->{nb_elem_per_page} = 999;
        }
    }

    if (exists $self->{sll_params}->{sql_table}) {
        $self->{sll_params}->{sql_request} = {
            select         => '*',
            'select_count' => "count(*)",
            from           => $self->{sll_params}->{sql_table},
            where          => "'t'",
        };
    }

    $self->{config} = $context->GetConfig();

    if (defined $dbh) {
        $self->{dbh} = $dbh;
    }
    else {
        $self->{dbh} = $self->{config}->GetDBH();
    }

    if (exists $params->{default_sort_field}) {
        $self->{default_sort_field} = $params->{default_sort_field};
    }
    else {
        foreach my $field (@{ $params->{fields} }) {
            if ($field->[1] ne 'rowid') {
                $self->{default_sort_field} = $field->[0];
                last;
            }
        }
    }

    return $self;
}

# ============================================================================

=head2 Run

    $sll->Run($context, $args);

    Return the XML describing the Simple Large List

=head3 Generated XML

 <SimpleLargeList>

    <!-- Fields description -->
	<fields>
	    <field name="name" type="type"/>
	    <field name="name" type="type"/>
	    ...
    </fields>

    <!-- Actions list -->
	<actions>
	    <action name="$name" function="Method URL"/>
	     ...
    </actions>

    <!-- Search form description -->
    <searchform>

        <!-- Form advanced search mode -->
		<field name="field name" value="field current value"/>
        ...
		
        <!-- Form normal search mode -->
        <curval>

			<sll_start_with>Start With parameter (0|1)</sll_start_with>
			<sll_search_text>Search Text</sll_search_text>
			<sll_search_field>Current search field</sll_search_field>
			
		</curval>

	</searchform>


	<!-- 

	   LargeList descritpion : See Mioga2::LargeList->DisplayList and Mioga2::LargeList->RemoveElem47

      -->

 </SimpleLargeList>



=cut

# ============================================================================

sub Run {
    my ($self, $context, $args) = @_;

    print STDERR "SimpleLargeList::Run\n" if ($debug);
    $self->{curargs} = $args;

    my $session = $context->GetSession();

    if (st_ArgExists($context, 'sll_search_act') and $context->{args}->{sll_search_text} ne '') {
        $session->{LargeList}->{ $self->{list_name} }->{sll_current_state} = 'search';
    }

    elsif (st_ArgExists($context, 'sll_search_act')) {
        $session->{LargeList}->{ $self->{list_name} }->{sll_search_params}->{ $context->{args}->{sll_search_field} } = [''];
        $session->{LargeList}->{ $self->{list_name} }->{sll_current_state} = 'normal';
    }

    elsif (st_ArgExists($context, 'sll_adv_search_act')) {
        $session->{LargeList}->{ $self->{list_name} }->{sll_current_state} = 'adv_search';
    }

    elsif (st_ArgExists($context, 'sll_normal_view')) {
        $session->{LargeList}->{ $self->{list_name} }->{sll_current_state} = 'normal';
    }

    my $state = $session->{LargeList}->{ $self->{list_name} }->{sll_current_state};

    if (st_ArgExists($context, 'sll_delete_act')) {
        return $self->RemoveElem($context);
    }

    elsif ($state eq 'search' or $state eq 'adv_search') {

        if (!exists $self->{sll_params}->{actions}) {
            $self->{sll_params}->{actions} = [];
        }

        my $actions = $self->{sll_params}->{actions};

        if (!grep { $_->[0] eq 'SllSearchBack' } @$actions) {
            push @$actions, [ 'SllSearchBack', '' ];
        }

        print STDERR "SimpleLargeList::Run  call to SearchElem\n" if ($debug);
        return $self->SearchElem($context);
    }

    else {
        my @actions = grep { $_->[0] ne 'SllSearchBack' } @{ $self->{sll_params}->{actions} };
        $self->{sll_params}->{actions} = \@actions;

        print STDERR "SimpleLargeList::Run  call to DisplayList\n" if ($debug);
        return $self->DisplayList($context);

    }

}

# ============================================================================
#
# InitSelectedCheckboxes
#
#    $sll->InitSelectedCheckboxes($context, $checkbox_name, @selected_rowids);
#
#    set check checboxes with rowid in  @selected_rowids
#
# ============================================================================

sub InitSelectedCheckboxes {
    my ($self, $context, $checkbox_name, $selected_rowids) = @_;

    my $session = $context->GetSession();
    $session->{LargeList}->{ $self->{list_name} }->{selected_rowids}->{$checkbox_name} = $selected_rowids;

    foreach my $arg (@$selected_rowids) {
        $context->{args}->{"select_${checkbox_name}_$arg"} = 1;
    }
}

# ============================================================================
#
# InitSelectedSimpleCheckboxes
#
#    $sll->InitSelectedSimpleCheckboxes($context, $checkbox_name, @selected_rowids);
#
#    set check checboxes with rowid in  @selected_rowids
#
# ============================================================================

sub InitSelectedSimpleCheckboxes {
    my ($self, $context, $checkbox_name, $selected_rowids) = @_;

    my $session = $context->GetSession();
    $session->{LargeList}->{ $self->{list_name} }->{selected_rowids}->{$checkbox_name} = $selected_rowids;

    foreach my $arg (@$selected_rowids) {
        $context->{args}->{"select_simple_${checkbox_name}_$arg"} = 1;
    }
}

# ============================================================================
#
# GetResult
#
#    $sll->GetResult();
#
#    Return the ult hash of the last Run call.
#
# ============================================================================
sub GetResult {
    my ($self) = @_;

    if (exists($self->{result})) {
        return $self->{result};
    }
    return undef;
}

# ============================================================================
#
# GetElems
#
#    $sll->GetElems($context, $args);
#
#    Return the xml representing the elements of the list.
#
# ============================================================================

sub GetElems {
    my ($self, $context, $args) = @_;

    my $session = $context->GetSession();

    $self->{curargs} = $args;

    $self->{nb_elem_per_page} = int($self->GetNumberOfElems($context));

    my $list = $self->GetElemsList($context, 0);

    return $self->_GenListContent($context, $list);
}

# ============================================================================
#
# GetNumber
#
#    $sll->GetNumber($context, $args);
#
#    Return the number of elements of the list.
#
# ============================================================================

sub GetNumber {
    my ($self, $context, $args) = @_;

    $self->{curargs} = $args;

    return $self->GetNumberOfElems($context);
}

# ============================================================================
#
# SearchElems
#
#    $sll->SearchElems($context, $args);
#
#    Return the xml representing the elements of the list.
#
# ============================================================================

sub SearchElems {
    my ($self, $context, $pattern, $args) = @_;

    my $session = $context->GetSession();

    $self->{curargs} = $args;

    $self->{nb_elem_per_page} = int($self->GetNumberOfElemsMatching($context));

    my $list = $self->GetElemsMatching($context, $pattern, 0);

    return $self->_GenListContent($context, $list);
}

# ============================================================================
#
# SearchNumber
#
#    $sll->SearchNumber($context, $args);
#
#    Return the number of elements of the list.
#
# ============================================================================

sub SearchNumber {
    my ($self, $context, $pattern, $args) = @_;

    $self->{curargs} = $args;

    return $self->GetNumberOfElemsMatching($context, $pattern);
}

# ============================================================================
#
# GetCurrentMode
#
#    $sll->GetCurrentMode($context);
#
#    Return the current mode of the list (search, adv_search, normal).
#
# ============================================================================

sub GetCurrentMode {
    my ($self, $context) = @_;

    my $session = $context->GetSession();

    if (exists $session->{LargeList}->{ $self->{list_name} }->{sll_current_state}) {
        return $session->{LargeList}->{ $self->{list_name} }->{sll_current_state};
    }

    return 'normal';
}

# ============================================================================
#
# PRIVATE METHODS DESCRIPTION
#
# ============================================================================

# ============================================================================
#
# GetNumberOfElems
#
#    $self->GetNumberOfElems($context);
#
#    Return the number of element to display.
#    This method MUST be overloaded.
#
#    This method is used to process page index.
#
# ============================================================================

sub GetNumberOfElems {
    my ($self, $context) = @_;

    return $self->RunCountRequest($context);
}

# ============================================================================
#
# GetNumberOfElemsMatching
#
#    $self->GetNumberOfElems($context, $pattern);
#
#    Return the number of element matching given patterns.
#    This method MUST be overloaded.
#
#    This method is used by SearchElem to process page index.
#
#    See SearchElem for more details on $patterns
#
# ============================================================================

sub GetNumberOfElemsMatching {
    my ($self, $context, $pattern) = @_;

    $self->RunCountRequest($context, $pattern);
}

# ============================================================================
#
# GetElemsMatching
#
#    $self->GetElemsMatching($context, $pattern, $offset, $sort_field_name, $asc);
#
#    Return <nb_elem_per_page> elements begining to the <offset>th and matching given patterns.
#
#    Elements must be sorted on the field $sort_field_name.
#    If $asc = 0 elements are sorted descending, otherwise elements are sorted ascending.
#
#    This method MUST be overloaded.
#
#    See SearchElem for description of $pattern
#
# ============================================================================
sub GetElemsMatching {
    my ($self, $context, $pattern, $offset, $sort_field_name, $asc) = @_;

    return $self->RunSearchRequest($context, $offset, $sort_field_name, $asc, $pattern);
}

# ============================================================================
#
# GetElemsList
#
#    $self->GetElemsList($context, $offset, $sort_field_name, $asc);
#
#    Return <nb_elem_per_page> elements begining to the <offset>th
#
#    Elements must be sorted on the field $sort_field_name.
#    If $asc = 0 elements are sorted descending, otherwise elements are sorted ascending.
#
#    This method MUST be overloaded.
#
# ============================================================================
sub GetElemsList {
    my ($self, $context, $offset, $sort_field_name, $asc) = @_;

    return $self->RunSearchRequest($context, $offset, $sort_field_name, $asc);
}

# ============================================================================
#
# GetElem
#
#    $self->GetElem($context, $rowid);
#
#    Return a hash containing information about element $rowid
#
# ============================================================================
sub GetElem {
    my ($self, $context, $rowid) = @_;

    my $config = $context->GetConfig();
    my $dbh    = $self->{dbh};

    my $sql = $self->GenerateSqlRequest($context);

    my @fields = grep { $_->[1] eq 'rowid' } @{ $self->{sll_params}->{fields} };

    $sql = "SELECT * FROM ($sql) AS view WHERE $fields[0]->[0] = $rowid";

    my $res = SelectSingle($dbh, $sql, $self->{curargs});

    return $res;
}

# ============================================================================
#
# DeleteElem
#
#    $self->DeleteElem($context, $rowid);
#
#    Delete the element with id $rowid.
#
#    This method MUST be overloaded.
#
#    This method is called after the confirmation page if askConfirmForDelete
#    is set.
#
# ============================================================================
sub DeleteElem {
    my ($self, $context, $rowid) = @_;

    my $elem = $self->GetElem($context, $rowid);

    my @fields = grep { $_->[1] eq 'delete' } @{ $self->{sll_params}->{fields} };

    my ($name, $type, $check_auth, $hook) = @{ $fields[0] };

    if (!$self->CheckAuthzForField($context, $elem, $check_auth)) {
        return;
    }

    my $config = $context->GetConfig();
    my $dbh    = $self->{dbh};

    if (defined $hook) {
        &$hook($context, $rowid);
    }
    else {

        my $where = $self->{sll_params}->{sql_request}->{delete}->{where};
        $where =~ s/\$_->\{([^\}]+)\}/$elem->{$1}/g;

        my $sql = "DELETE FROM $self->{sll_params}->{sql_request}->{delete}->{from} " . "WHERE $where";

        ExecSQL($dbh, $sql);
    }
}

# ============================================================================
#
# DisplayList
#
#    $self->DisplayList($context);
#
#    Return a list.
#
# ==========================================================================='

sub DisplayList {
    my ($self, $context) = @_;

    $self->StoreCheckedBoxes($context);

    my $xml = "<SimpleLargeList>";
    $xml .= $self->GetFieldsDescXML($context);
    $xml .= $self->SUPER::DisplayList($context);
    $xml .= "</SimpleLargeList>";

    return $xml;
}

# ============================================================================
#
# RemoveElem
#
#    $self->RemoveElem($context);
#
#    Remove elements and ask confirmation for deletion
#
# ==========================================================================='

sub RemoveElem {
    my ($self, $context) = @_;

    $self->CurDeleteSelInArgs($context);

    my $xml = "<SimpleLargeList>";
    $xml .= $self->GetFieldsDescXML($context);
    my $tmpxml = $self->SUPER::RemoveElem($context);
    if (defined($tmpxml)) {
        $xml .= $tmpxml;
    }
    else {
        return (undef);
    }

    $xml .= "</SimpleLargeList>";

    return $xml;
}

# ============================================================================
#
# SearchElem
#
#    $self->SearchElem($context);
#
#    Return a search result.
#
# ==========================================================================='

sub SearchElem {
    my ($self, $context) = @_;

    $self->StoreCheckedBoxes($context);

    my $session = $context->GetSession();
    my $params  = {};

    if (st_ArgExists($context, "sll_search_act")) {
        my $search_field = $context->{args}->{sll_search_field};

        $session->{LargeList}->{ $self->{list_name} }->{"sll_search_mode"} = 'search';
        $params->{$search_field}                                           = [ $context->{args}->{"sll_search_text"} ];
        $session->{LargeList}->{ $self->{list_name} }->{"sll_start_with"}  = $context->{args}->{"sll_start_with"};
    }

    # Advanced Search Form
    elsif (st_ArgExists($context, "sll_adv_search_act")) {
        $session->{LargeList}->{ $self->{list_name} }->{sll_search_mode} = 'adv_search';

        foreach my $field (@{ $self->{sll_params}->{search} }) {

            if (exists $context->{args}->{"sll_search_$field"}
                and $context->{args}->{"sll_search_$field"} ne '')
            {

                $params->{$field} = [ $context->{args}->{"sll_search_$field"} ];

            }
        }
    }

    else {
        if (exists $session->{LargeList}->{ $self->{list_name} }->{sll_search_params}) {
            $params = $session->{LargeList}->{ $self->{list_name} }->{sll_search_params};
        }
    }

    if (!exists $session->{LargeList}->{ $self->{list_name} }->{sll_start_with}) {
        $session->{LargeList}->{ $self->{list_name} }->{sll_start_with} = 0;
    }

    $session->{LargeList}->{ $self->{list_name} }->{sll_search_params} = $params;
    my $xml = "<SimpleLargeList>";
    $xml .= $self->GetFieldsDescXML($context);
    $xml .= $self->SUPER::SearchElem($context, $params);
    $xml .= "</SimpleLargeList>";

    return $xml;
}

# ============================================================================
#
# RunSearchRequest
#
#    $self->RunSearchRequest($context, $offset, $sort_field_name, $asc, $pattern);
#
#    Return a hash containing information about elements
#
# ============================================================================

sub RunSearchRequest {
    my ($self, $context, $offset, $sort_field_name, $asc, $pattern) = @_;

    my $dbh     = $self->{dbh};
    my $session = $context->GetSession();

    my $sql;

    if (defined $pattern) {

        my $start_with = $session->{LargeList}->{ $self->{list_name} }->{sll_start_with};
        $pattern = $self->GetPostgresRegExp($pattern, $start_with);

        my $regexp = $self->FormatSqlSearchRequest($context, $pattern);

        $sql .= $self->GenerateSqlRequest($context);

        if (defined $regexp) {
            $sql = "SELECT  * FROM  ($sql) AS view WHERE $regexp";
        }
        else {
            $sql = "SELECT  * FROM  ($sql) AS view ";
        }

    }
    else {
        $sql .= "SELECT * FROM (" . $self->GenerateSqlRequest($context) . ") AS view";
    }

    # ------------------------------------------------------
    # Sort the result
    # ------------------------------------------------------
    my @fields;
    if (defined $sort_field_name) {
        @fields = grep { $sort_field_name eq $_->[0] } @{ $self->{sll_params}->{fields} };
        if (!@fields) {
            $sort_field_name = undef;
        }
    }

    if (!defined $sort_field_name) {
        @fields          = grep { $_->[0] ne 'rowid' } @{ $self->{sll_params}->{fields} };
        $sort_field_name = $fields[0]->[0];
        $asc             = 1;
    }

    my ($name, $type, $rowid_field) = @{ $fields[0] };

    # ------------------------------------------------------
    # For user "name" values, sort on lastname.
    # ------------------------------------------------------
    if ($type eq 'user_name') {
        @fields = grep { $_->[1] eq 'user_lastname' } @{ $self->{sll_params}->{fields} };
        if (!@fields) {
            @fields = ([ 'user_lastname', 'user_lastname', $rowid_field, 'hidden' ]);
            push @{ $self->{sll_params}->{fields} }, @fields;
        }

        ($name, $type, $rowid_field) = @{ $fields[0] };

    }

    # ------------------------------------------------------
    # For load_unit sort by time in seconds
    # ------------------------------------------------------
    if ($sort_field_name eq 'load_value') {

        #$sql .= " ORDER BY time_in_seconds ".($asc ? "ASC":"DESC");
        $sql .= " WHERE progress != 100 ORDER BY time_in_seconds " . ($asc ? "ASC" : "DESC");
    }
    elsif ($sort_field_name eq "progress") {

        #$sql .= " ORDER BY $sort_field_name ".($asc ? "ASC":"DESC");
        $sql .= " WHERE progress != 100 ORDER BY $sort_field_name " . ($asc ? "ASC" : "DESC");
    }
    else {
        $sql .= " ORDER BY $sort_field_name " . ($asc ? "ASC" : "DESC");
    }

    $sql .= " OFFSET $offset LIMIT $self->{nb_elem_per_page}";

    my $res = SelectMultiple($dbh, $sql, $self->{curargs});

    if (!defined $res) {
        throw Mioga2::Exception::Simple("Mioga2::SimpleLargeList::RunSearchRequest", __x("No result for count request: {request}", request => $sql));
    }

    $self->{result} = $res;
    return $res;
}

# ============================================================================
#
# RunCountRequest
#
#    $self->RunCountRequest($context, $pattern);
#
#    Return the number of elements.
#
# ============================================================================

sub RunCountRequest {
    my ($self, $context, $pattern) = @_;

    my $dbh     = $self->{dbh};
    my $session = $context->GetSession();
    my $config  = $context->GetConfig();

    my $sql;
    my $regexp;

    if (defined $pattern) {
        $regexp = $self->FormatSqlSearchRequest($context, $pattern);
    }

    if (defined $regexp) {
        $sql = $self->GenerateSqlRequest($context);

        if (defined $regexp) {
            $sql = "SELECT  count(*) FROM  ($sql) AS view WHERE $regexp";
        }
    }
    else {
        $sql = "SELECT $self->{sll_params}->{sql_request}->{select_count}
	 	         FROM   $self->{sll_params}->{sql_request}->{from} 	
 	 	         WHERE  ($self->{sll_params}->{sql_request}->{where})";
    }

    my $res = SelectSingle($dbh, $sql, $self->{curargs});

    if (!defined $res) {
        return 0;
    }

    return $res->{count};
}

# ============================================================================
#
# GetGroupUserResRowids
#
#    $self->GetGroupUserResRowids($context, $sql, $pattern);
#
#    Return the list of rowids for user/group/resource matching $pattern
#	in sql request $sql.
#
# ============================================================================

sub GetGroupUserResRowids {
    my ($self, $context, $sql, $pattern) = @_;

    my $config = $context->GetConfig();
    my $dbh    = $self->{dbh};

    # ------------------------------------------------------
    # Get all group/user/resource rowids
    # ------------------------------------------------------
    my $rowid_lists = {};
    my @regexp;
    my $object_lists = {};
    my $values_lists = {};

    foreach my $search_field (keys %$pattern) {
        my @fields = grep { $search_field eq $_->[0] } @{ $self->{sll_params}->{fields} };

        if (@fields and $fields[0]->[1] !~ /^((group)|(team)|(user)|(resource))_/) {
            next;
        }

        my ($name, $type, $rowid_field) = @{ $fields[0] };

        # ------------------------------------------------------
        # Get all group/user/resource rowids and create
        # user/group/resource lists objects
        # ------------------------------------------------------

        my $listobj;
        if (!exists $object_lists->{$rowid_field}) {

            my $res = SelectMultiple($dbh, $sql, $self->{curargs});

            my @rowids = map { $_->{$rowid_field} } @$res;

            if (!@rowids) {
                return undef;
            }

            $rowid_lists->{$rowid_field} = \@rowids;

            if ($type =~ /^group_(.*)/) {
                $listobj = new Mioga2::Old::GroupList($config, rowid_list => \@rowids);
            }

            elsif ($type =~ /^user_(.*)/) {
                $listobj = new Mioga2::Old::UserList($config, rowid_list => \@rowids);
            }

            elsif ($type =~ /^resource_(.*)/) {
                $listobj = new Mioga2::Old::ResourceList($config, rowid_list => \@rowids);
            }

            elsif ($type =~ /^team_(.*)/) {
                $listobj = new Mioga2::Old::TeamList($config, rowid_list => \@rowids);
            }

            $object_lists->{$rowid_field} = $listobj;
        }

        else {
            $listobj = $object_lists->{$rowid_field};
        }

        $type =~ /^(?:(?:group)|(?:team)|(?:user)|(?:resource))_(.*)/;

        my $attr = $1;
        $attr =~ s/(^|_)([^_]+)/ucfirst($2)/eg;

        my $method = "Get" . $attr;
        if (!$listobj->can($method)) {
            $method .= "s";
        }

        my $value_list  = $listobj->$method();
        my $rowid_list  = $rowid_lists->{$rowid_field};
        my $search_expr = $pattern->{$search_field}->[0];

        # ------------------------------------------------------
        # Extract values matching pattern from the list
        # ------------------------------------------------------
        my @rowids;
        my $nb_elem = @$value_list;

        for (my $i = 0 ; $i < $nb_elem ; $i++) {
            if ($value_list->[$i] =~ /$search_expr/i) {
                push @rowids, $rowid_list->[$i];
            }
        }

        if (@rowids) {
            push @regexp, "$rowid_field IN (" . join(', ', @rowids) . ")";
        }
        else {
            push @regexp, "$rowid_field != $rowid_field";
        }
    }

    if (@regexp) {
        return "(" . join(" AND ", @regexp) . ")";
    }

    return undef;
}

# ============================================================================
#
# GenerateSqlRequest
#
#    $self->GenerateSqlRequest($context);
#
#    Return SQL request corresponding to ssl description.
#
# ============================================================================

sub GenerateSqlRequest {
    my ($self, $context) = @_;

    my $select = $self->{sll_params}->{sql_request}->{select};
    my $from   = $self->{sll_params}->{sql_request}->{from};
    my $where  = $self->{sll_params}->{sql_request}->{where};

    my $fields = { user => [], group => [], team => [], resource => [] };

    foreach my $field (@{ $self->{sll_params}->{fields} }) {
        my ($name, $type, @args) = @$field;

        if ($type =~ /^((user)|(team)|(group)|(resource))_/) {
            my $rowid = $args[0];

            my ($type, $value) = ($type =~ /^([^_]+)_(.*)$/);
            my $table = { user => "m_user_base", group => "m_group", team => "m_group", resource => "m_resource" }->{$type};

            my $prev_fields = $fields->{$type};

            my $select_statement;

            if ($type eq 'user' and $value eq 'name') {
                $select_statement = "(m_user_base.firstname || ' ' || m_user_base.lastname) AS $name";
            }
            else {
                $select_statement = "$table.$value AS $name";
            }

            if (!@$prev_fields) {
                $select .= ", $select_statement";
                if ($table eq 'm_user_base') {
                    if ($from =~ /m_group,/) {
                        $from =~ s/m_group,/m_group CROSS JOIN m_user_base,/;
                    }
                    else {
                        $from .= ", $table";
                    }
                }
                else {
                    $from .= ", $table";
                }
                $where .= " AND $table.rowid = $rowid";

                push @$prev_fields, $rowid;
            }
            elsif ($prev_fields->[0] eq $rowid) {
                $select .= ", $select_statement";
            }
            else {
                $select .= " , (SELECT $select_statement FROM $table WHERE rowid = $rowid) AS $name";
            }

        }
    }

    my $sql = "SELECT $select FROM $from WHERE $where";
    print STDERR "SimpleLargeList::GenerateSqlRequest  sql = $sql\n" if ($debug);
    return $sql;
}

# ============================================================================
#
# GetFieldValues
#
#    $self->GetFieldValues($context, $field, $rowids);
#
#    Return the list of rowids for user/group/resource matching $search_expr
#
# ============================================================================

sub GetFieldValues {
    my ($self, $context, $field, $rowids) = @_;

    my ($name, $type, $rowid_field) = @$field;
    my $config = $context->GetConfig();

    # ------------------------------------------------------
    # Create user/group/resource list object
    # ------------------------------------------------------
    my $value_list;
    my $listobj;
    my $attr;

    if ($type =~ /^group_(.*)/) {
        $attr = $1;
        $listobj = new Mioga2::Old::GroupList($config, rowid_list => $rowids);
    }

    elsif ($type =~ /^user_(.*)/) {
        $attr = $1;
        $listobj = new Mioga2::Old::UserList($config, rowid_list => $rowids);
    }

    elsif ($type =~ /^resource_(.*)/) {
        $attr = $1;
        $listobj = new Mioga2::Old::ResourceList($config, rowid_list => $rowids);
    }

    elsif ($type =~ /^team_(.*)/) {
        $attr = $1;
        $listobj = new Mioga2::Old::TeamList($config, rowid_list => $rowids);
    }

    $attr =~ s/(^|_)([^_]+)/ucfirst($2)/eg;

    my $method = "Get" . $attr;
    if (!$listobj->can($method)) {
        $method .= "s";
    }

    $value_list = $listobj->$method();

    # ------------------------------------------------------
    # Extract values matching pattern from the list
    # ------------------------------------------------------
    my @values;
    my $nb_elem = @$value_list;

    for (my $i = 0 ; $i < $nb_elem ; $i++) {
        push @values, [ $rowids->[$i], $value_list->[$i] ];
    }

    return \@values;
}

# ============================================================================
#
# FormatSqlSearchRequest
#
#    $self->FormatSqlSearchRequest($context, $pattern);
#
#    Return the number of elements.
#
# ============================================================================

sub FormatSqlSearchRequest {
    my ($self, $context, $pattern) = @_;

    my $session = $context->GetSession();

    my @regexp_list;

    foreach my $var (@{ $self->{sll_params}->{search} }) {

        my @fields = grep { $var eq $_->[0] } @{ $self->{sll_params}->{fields} };

        if (exists $pattern->{$var}) {
            my @var_reg_exp_list;

            foreach my $regexp (@{ $pattern->{$var} }) {
                push @var_reg_exp_list, "$var ~* '" . st_FormatPostgreSQLRegExpString($regexp) . "'";
            }

            push @regexp_list, "(" . join(" OR ", @var_reg_exp_list) . ")";
        }

    }

    if (@regexp_list) {
        return join(" AND ", @regexp_list);
    }

    return undef;
}

# ============================================================================
#
# _GenList
#
#    $self->_GenList($context, $list, $nb_elem);
#
#    return XML string describing a list of hash
#
# ============================================================================

sub _GenList {
    my ($self, $context, $list, $nb_elem) = @_;

    my $session = $context->GetSession();

    # ------------------------------------------------------
    # Generate XML.
    # ------------------------------------------------------

    my $xml = "<List nb_elem_per_page=\"" . st_FormatXMLString($self->{nb_elem_per_page}) . "\" nb_elems=\"" . st_FormatXMLString($nb_elem) . "\" name=\"" . st_FormatXMLString($self->{list_name}) . "\"";
    $xml .= " cur_sort_field=\"" . st_FormatXMLString($session->{LargeList}->{ $self->{list_name} }->{cur_sort_field}) . "\"";
    $xml .= " cur_sort_asc=\"" . st_FormatXMLString($session->{LargeList}->{ $self->{list_name} }->{cur_sort_asc}) . "\">";

    $xml .= $self->_GenListContent($context, $list);

    $xml .= "</List>";
    return $xml;
}

# ============================================================================
#
# _GenListContent
#
#    $self->_GenListContent($context, $list);
#
#    return XML string describing a list of hash
#
# ============================================================================

sub _GenListContent {
    my ($self, $context, $list) = @_;

    my $fields = $self->{sll_params}->{fields};

    my $session = $context->GetSession();

    my $cur_page_rowids = [];

    my $i   = 0;
    my $xml = "";
    foreach my $elem (@$list) {

        $xml .= "<Row>\n";

        my $rowid;
        foreach my $field (@$fields) {
            my ($name, $type, @args) = @$field;

            if ($type eq 'rowid') {
                push @$cur_page_rowids, $elem->{$name};

                $rowid = $elem->{$name};

                $elem->{$name} = st_FormatXMLString($elem->{$name});

                $xml .= qq|<rowid type="rowid" name="$name">$elem->{$name}</rowid>|;
            }

            elsif ($type eq 'modify') {
                if (!$self->CheckAuthzForField($context, $elem, $args[1])) {
                    $xml .= qq|<modify type="modify" enabled="0" name="$name" function="$args[0]"/>|;
                }

                else {
                    $xml .= qq|<modify type="modify" enabled="1" name="$name" function="$args[0]"/>|;
                }
            }

            elsif ($type eq 'checkboxes') {
                if (!$self->CheckAuthzForField($context, $elem, $args[0])) {
                    $xml .= qq|<checkboxes type="checkboxes" enabled="0" name="$name" />|;
                }

                else {
                    my $checked = 0;
                    if (exists $session->{LargeList}->{ $self->{list_name} }->{selected_rowids}->{$name}) {
                        $checked = grep { $_ eq $rowid } @{ $session->{LargeList}->{ $self->{list_name} }->{selected_rowids}->{$name} };
                    }

                    $xml .= qq|<checkboxes type="checkboxes" enabled="1" name="$name" checked="$checked"/>|;
                }
            }

            elsif ($type eq 'simple_checkboxes') {
                if (!$self->CheckAuthzForField($context, $elem, $args[0])) {
                    $xml .= qq|<simple_checkboxes type="simple_checkboxes" enabled="0" name="$name" />|;
                }

                else {
                    my $checked = 0;
                    if (exists $session->{LargeList}->{ $self->{list_name} }->{selected_rowids}->{$name}) {
                        $checked = grep { $_ eq $rowid } @{ $session->{LargeList}->{ $self->{list_name} }->{selected_rowids}->{$name} };
                    }

                    $xml .= qq|<checkboxes type="simple_checkboxes" enabled="1" name="$name" checked="$checked"/>|;
                }
            }

            elsif ($type eq 'delete') {
                if (!$self->CheckAuthzForField($context, $elem, $args[0])) {
                    $xml .= qq|<delete type="delete" enabled="0" name="$name" />|;
                }

                else {
                    my $checked = 0;
                    if (exists $session->{LargeList}->{ $self->{list_name} }->{selected_rowids}->{$name}) {
                        $checked = grep { $_ eq $rowid } @{ $session->{LargeList}->{ $self->{list_name} }->{selected_rowids}->{$name} };
                    }

                    $xml .= qq|<delete type="delete" enabled="1" name="$name" checked="$checked"/>|;
                }
            }

            elsif ($type =~ /^((user)|(group)|(team)|(resource))_(.*)/) {
                my $nb_args = @args - 1;
                my @field_desc = ($name, @args[ 1 .. $nb_args ]);

                $xml .= $self->GenFieldXML($context, \@field_desc, $elem->{$name}, $elem);
            }

            else {
                $xml .= $self->GenFieldXML($context, $field, $elem->{$name}, $elem);
            }
        }

        $xml .= "</Row>\n";

        $i++;
    }

    $session->{LargeList}->{ $self->{list_name} }->{cur_page_rowids} = $cur_page_rowids;

    return $xml;
}

# ============================================================================
#
# GenFieldXML
#
#	Run XML code describing the given field.
#
# ============================================================================

sub GenFieldXML {
    my ($self, $context, $field, $value, $elem) = @_;

    if (@$field == 1) {
        $field = [ $field->[0], "normal" ];
    }

    my ($name, $type, @args) = @$field;
    my $xml;

    if ($type eq 'date' or $type eq 'datetime' or $type eq 'dateabr' or $type eq 'dateshort') {
        $xml .= qq|<$name type="$type">|;

        # Convert in current user local time
        my $second = du_ISOToSecond($value);

        if ($second > 0) {
            $xml .= du_GetDateXMLInUserLocale($second, $context->GetUser());
        }

        $xml .= qq|</$name>|;

        return $xml;
    }

    if ($type eq 'filesize') {
        $xml .= qq|<$name type="filesize">|;
        if (defined $value) {
            $xml .= st_ConvertOctetXML($value);
        }
        $xml .= qq|</$name>|;

        return $xml;
    }

    if ($type eq 'normal') {
        $xml .= qq|<$name type="normal">|;
    }
    elsif ($type eq 'numeric') {
        $xml .= qq|<$name type="numeric">|;
    }
    elsif ($type eq 'hidden') {
        $xml .= qq|<$name type="hidden">|;
    }

    elsif ($type eq 'link') {
        my $url = st_FormatXMLString($args[0]);

        # ------------------------------------------------------
        # Check authz expression
        # ------------------------------------------------------
        if (!$self->CheckAuthzForField($context, $elem, $args[1])) {
            $xml .= qq|<$name type="normal">|;
        }

        else {
            $xml .= qq|<$name type="link" url="$url" >|;
        }

    }

    elsif ($type eq 'email') {
        $xml .= qq|<$name type="email">|;
    }

    $xml .= st_FormatXMLString($value);
    $xml .= "</$name>\n";

    return $xml;
}

# ============================================================================
#
# CheckAuthzForField
#
#	Run the authz expression of fields description.
#
# ============================================================================

sub CheckAuthzForField {
    my ($self, $context, $elem, $expr) = @_;

    my $config = $context->GetConfig;

    if (!defined $expr) {
        return 1;
    }

    if (ref($expr) eq 'CODE') {
        return &$expr($context, $elem);
    }

    my $res = eval '$_ = $elem; return (' . $expr . ');';

    if ($@) {
        throw Mioga2::Exception::Simple("Mioga2::SimpleLargeList::CheckAuthzForField", $@);
    }

    return $res;
}

# ============================================================================
#
# GetFieldsDescXML
#
#	Run XML describing the field description.
#
# ============================================================================

sub GetFieldsDescXML {
    my ($self, $context) = @_;

    my $session = $context->GetSession();

    my $fields        = $self->{sll_params}->{fields};
    my $actions       = $self->{sll_params}->{actions};
    my $search_fields = $self->{sll_params}->{search};

    my $xml = "<fields>";

    foreach my $field (@$fields) {
        my ($name, $type, @args) = @$field;

        if (   $type =~ /^user_/
            or $type =~ /^group_/
            or $type =~ /^team_/
            or $type =~ /^resource_/ and @args >= 2)
        {
            $type = $args[1];
        }

        $xml .= qq|<field name="$name" type="$type"/>|;
    }

    $xml .= "</fields>";

    $xml .= "<actions>";

    foreach my $action (@$actions) {
        my ($name, $function) = @$action;

        $xml .= qq|<action name="$name" function="$function"/>|;
    }

    $xml .= "</actions>";

    if (defined $search_fields and @$search_fields) {
        $xml .= "<searchform>";

        my $params = $session->{LargeList}->{ $self->{list_name} }->{sll_search_params};
        my $mode   = $session->{LargeList}->{ $self->{list_name} }->{sll_search_mode};

        foreach my $field (@$search_fields) {

            $xml .= qq|<field name="$field"|;

            if (    defined $mode
                and $mode eq 'adv_search'
                and exists $params->{$field})
            {
                $xml .= qq| value="$params->{$field}->[0]"|;
            }

            $xml .= "/>";
        }

        if (defined $mode and $mode eq 'search') {
            my @keys = keys %$params;

            $xml .= "<curval>";

            $xml .= "<sll_start_with>" . $session->{LargeList}->{ $self->{list_name} }->{sll_start_with} . "</sll_start_with>";
            $xml .= "<sll_search_text>" . st_FormatXMLString($params->{ $keys[0] }->[0]) . "</sll_search_text>";
            $xml .= "<sll_search_field>" . st_FormatXMLString($keys[0]) . "</sll_search_field>";

            $xml .= "</curval>";
        }

        $xml .= "</searchform>";
    }

    return $xml;
}

# ============================================================================
#
# BuildGroupUserResLists
#
#	Create the Group, User and Resource lists for user_*, group_* and resource_* fields.
#
# ============================================================================

sub BuildGroupUserResLists {
    my ($self, $context, $list) = @_;

    if (!@$list) {
        return (undef, undef);
    }

    my $config = $context->GetConfig();
    my $fields = $self->{sll_params}->{fields};

    # ------------------------------------------------------
    # Build group, user and resource rowid lists.
    # ------------------------------------------------------

    my $userlists     = {};
    my $grouplists    = {};
    my $teamlists     = {};
    my $resourcelists = {};
    foreach my $elem (@$list) {
        my $done = {};

        # ------------------------------------------------------
        # Extract rowids from database result.
        # ------------------------------------------------------
        foreach my $field (@$fields) {
            my ($name, $type, $rowid_field, @args) = @$field;

            # ------------------------------------------------------
            # Skip this entry if the rowid was already added to
            # the list by another user_*, group_* or resource_*
            # ------------------------------------------------------
            if (    exists $done->{$name}
                and exists $done->{$name}->{$rowid_field})
            {
                next;
            }

            $done->{$name}->{$rowid_field} = 1;

            if ($type =~ /^user_.*/) {
                if (!exists $userlists->{$name}) {
                    $userlists->{$name} = [];
                }

                push @{ $userlists->{$name} }, $elem->{$rowid_field};
            }

            elsif ($type =~ /^group_.*/) {
                if (!exists $grouplists->{$name}) {
                    $grouplists->{$name} = [];
                }

                push @{ $grouplists->{$name} }, $elem->{$rowid_field};
            }

            elsif ($type =~ /^team_.*/) {
                if (!exists $teamlists->{$name}) {
                    $teamlists->{$name} = [];
                }

                push @{ $teamlists->{$name} }, $elem->{$rowid_field};
            }

            elsif ($type =~ /^resource_.*/) {
                if (!exists $resourcelists->{$name}) {
                    $resourcelists->{$name} = [];
                }

                push @{ $resourcelists->{$name} }, $elem->{$rowid_field};
            }
        }
    }

    # ------------------------------------------------------
    # Create GroupLists and UserLists
    # ------------------------------------------------------
    my $groupobjs    = {};
    my $teamobjs     = {};
    my $userobjs     = {};
    my $resourceobjs = {};

    foreach my $field (keys %$userlists) {
        my $rowids = $userlists->{$field};

        $userobjs->{$field} = new Mioga2::Old::UserList($config, rowid_list => $rowids);
    }

    foreach my $field (keys %$grouplists) {
        my $rowids = $grouplists->{$field};
        $groupobjs->{$field} = new Mioga2::Old::GroupList($config, rowid_list => $rowids);
    }
    foreach my $field (keys %$teamlists) {
        my $rowids = $teamlists->{$field};
        $teamobjs->{$field} = new Mioga2::Old::TeamList($config, rowid_list => $rowids);
    }

    foreach my $field (keys %$resourcelists) {
        my $rowids = $resourcelists->{$field};
        $resourceobjs->{$field} = new Mioga2::Old::ResourceList($config, rowid_list => $rowids);
    }

    # ------------------------------------------------------
    # Generate the list of values for these entries
    # ------------------------------------------------------
    my $user_values;
    foreach my $field (@$fields) {
        my ($name, $type, @args) = @$field;

        if ($type =~ /^user_(.*)/) {
            my $attr = $1;

            $attr =~ s/(^|_)([^_]+)/ucfirst($2)/eg;

            my $function = "Get" . $attr;
            if (!$userobjs->{$name}->can($function)) {
                $function .= "s";
            }

            $user_values->{$name} = $userobjs->{$name}->$function();
        }
    }

    my $group_values;
    foreach my $field (@$fields) {
        my ($name, $type, @args) = @$field;

        if ($type =~ /^group_(.*)/) {
            my $attr = $1;

            $attr =~ s/(^|_)([^_]+)/ucfirst($2)/eg;

            my $function = "Get" . $attr;
            if (!$groupobjs->{$name}->can($function)) {
                $function .= "s";
            }

            $group_values->{$name} = $groupobjs->{$name}->$function();
        }
    }

    my $team_values;
    foreach my $field (@$fields) {
        my ($name, $type, @args) = @$field;

        if ($type =~ /^team_(.*)/) {
            my $attr = $1;

            $attr =~ s/(^|_)([^_]+)/ucfirst($2)/eg;

            my $function = "Get" . $attr;
            if (!$teamobjs->{$name}->can($function)) {
                $function .= "s";
            }

            $team_values->{$name} = $teamobjs->{$name}->$function();
        }
    }

    my $resource_values;
    foreach my $field (@$fields) {
        my ($name, $type, @args) = @$field;

        if ($type =~ /^resource_(.*)/) {
            my $attr = $1;

            $attr =~ s/(^|_)([^_]+)/ucfirst($2)/eg;

            my $function = "Get" . $attr;
            if (!$resourceobjs->{$name}->can($function)) {
                $function .= "s";
            }

            $resource_values->{$name} = $resourceobjs->{$name}->$function();
        }
    }

    return ($user_values, $group_values, $team_values, $resource_values);
}

# ============================================================================
#
# StoreCheckedBoxes
#
#	Store the checked boxes rowid in the session
#
# ============================================================================

sub StoreCheckedBoxes {
    my ($self, $context) = @_;

    my $session = $context->GetSession();

    if (!exists $session->{LargeList}->{ $self->{list_name} }->{cur_page_rowids}) {
        $session->{LargeList}->{ $self->{list_name} }->{cur_page_rowids} = [];
    }

    if (!exists $session->{LargeList}->{ $self->{list_name} }->{selected_rowids}) {
        $session->{LargeList}->{ $self->{list_name} }->{selected_rowids} = {};
    }

    my $cur_page_rowids = $session->{LargeList}->{ $self->{list_name} }->{cur_page_rowids};
    my $selected_rowids = $session->{LargeList}->{ $self->{list_name} }->{selected_rowids};

    my @cb_fields = grep { $_->[1] eq 'checkboxes' or $_->[1] eq 'delete' } @{ $self->{sll_params}->{fields} };

    foreach my $field (@cb_fields) {
        my $name = $field->[0];

        if (!exists $selected_rowids->{$name}) {
            $selected_rowids->{$name} = [];
        }

        foreach my $key (keys %{ $context->{args} }) {
            if ($key =~ /^select_${name}_(.*)/) {

                my $rowid = $1;

                if (!grep { $_ eq $rowid } @{ $selected_rowids->{$name} }) {
                    push @{ $selected_rowids->{$name} }, $rowid;
                }
            }
        }

        foreach my $rowid (@$cur_page_rowids) {
            if (!exists $context->{args}->{"select_${name}_$rowid"}) {
                @{ $selected_rowids->{$name} } = grep { $_ ne $rowid } @{ $selected_rowids->{$name} };
            }
        }
    }
}

# ============================================================================
#
# CurDeleteSelInArgs
#
#	Store the checked boxes rowid in the session
#
# ============================================================================

sub CurDeleteSelInArgs {
    my ($self, $context) = @_;

    my $session = $context->GetSession();

    if (!exists $session->{LargeList}->{ $self->{list_name} }->{selected_rowids}->{delete}) {
        return;
    }

    foreach my $rowid (@{ $session->{LargeList}->{ $self->{list_name} }->{selected_rowids}->{delete} }) {
        $context->{args}->{"select_delete_$rowid"} = 'on';
    }

    delete $session->{LargeList}->{ $self->{list_name} }->{selected_rowids};
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LargeList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
