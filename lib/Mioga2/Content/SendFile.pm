# ============================================================================
# Mioga2 Project (C) 2008 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
	
Content::SendFile.pm: Content implementation to send a physical file with Apache2::RequestIO::sendfile.

=head1 DESCRIPTION

This module is a class for sending a file existing on local disk.

Parameters are:
	mime : the mime type of the file. (guessed by default)
	filename : the filename. (default to basename from path)
	disposition : send file as inline or attachment. (default to attachment)
	path : path to file on local disk.
=head1 METHODS DESRIPTION

=cut

package Mioga2::Content::SendFile;
use strict;

use base qw(Mioga2::Content);

use Error qw(:try);
use Data::Dumper;
use POSIX qw(tmpnam);
use Mioga2::Apache;
use File::MimeInfo::Magic;
use File::Basename;

# ============================================================================

=head2 Initialize ($context, %params)

	Initialize object and parse the parameters
	
    Parameters :
      mime => the mime type of content. (guessed by default)
      filename => the filename to return. (default to basename from path)
	  disposition => send file as inline or attachment. (default to attachment)
	  path => path to file on local disk.
=cut

# ============================================================================

sub Initialize {
    my ( $self, $context, %params ) = @_;

    $self->{IE}       = $context->IsIE();
    $self->{protocol} = $context->GetConfig()->GetProtocol();

    $self->{path} = $params{path};
    $self->{mime} = $params{mime} || mimetype( $self->{path} );
    $self->{disposition} = $params{disposition} || "attachment";
    $self->{filename} = $params{filename} || basename( $self->{path} );
}

# ============================================================================

=head2 CanCache ()

	Return 1 if the browser can cache the content, 0 otherwise.

=cut

# ============================================================================
sub CanCache {
    my ($self) = @_;
    return 1
      if ( $self->{filename} and $self->{IE} and $self->{protocol} eq 'https' );
    return 0;
}

# ============================================================================

=head2 GetMIME ()

	Return the MIME type of generated content.

=cut

# ============================================================================
sub GetMIME {
    my ($self) = @_;

    return $self->{mime};
}

# ============================================================================

=head2 Generate ($request)

	This method sends the file using Apache2::RequestIO::sendfile.

=cut

# ============================================================================
sub Generate {
    my ( $self, $r ) = @_;

    $r->headers_out->{'Content-Disposition'} = "$self->{disposition}; filename=\"$self->{filename}\"";
    $r->content_type( $self->{mime} );
    $r->sendfile($self->{path});

	# Must return undef, otherwise will return 0 and Dispatch will process contents and add extra "0" to end of file (see bug #1302)
	return ();
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Content

=head1 COPYRIGHT

Copyright (C) 2008, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
