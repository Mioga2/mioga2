# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Content::XML.pm: Content implementation class for XML/SXML methods.

=head1 DESCRIPTION

This module is a class for XML/SXML (soap based) response generation

Parameters are :
	     recipient : The recipient external mioga server name.
	or : recipient_keyident : The recipient external mioga public key ident.
	or : recipient_obj : The recipient ExternalMioga object.


	One of these parameter a required for SXML response.

=head1 METHODS DESRIPTION

=cut

package Mioga2::Content::XML;
use strict;

use base qw(Mioga2::Content);
use Locale::TextDomain::UTF8 'content_xml';

use Error qw(:try);
use Data::Dumper;
use Mioga2;
use Mioga2::ExternalMioga;

my $debug = 0;


# ============================================================================

=head2 Initialize ($context, %params)

	Initialize object and parse the parameters
	
    Parameters are :
	        recipient : The recipient external mioga server name.
	   or : recipient_keyident : The recipient external mioga public key ident.
	   or : recipient_obj : The recipient ExternalMioga object.

=cut

# ============================================================================

sub Initialize {
	my ($self, $context, %params) = @_;
	
	my $uri = $context->GetURI();

	my $rcpt;
	if(exists $params{recipient}) {
		$rcpt = new Mioga2::ExternalMioga($context->GetConfig(), 
										  server => $params{recipient});
	}

	elsif(exists $params{recipient_keyident}) {
		$rcpt = new Mioga2::ExternalMioga($context->GetConfig(), 
										  keyident => $params{recipient_keyident});
	}

	elsif(exists $params{recipient_obj}) {
		$rcpt = $params{recipient_obj};
	}

	if($uri->IsSXML and ! defined $rcpt) {
		throw Mioga2::Exception::Simple("Content::XML::Initialize", 
										__"Missing parameters.");
	}

	$self->{recipient} = $rcpt;
	
}



# ============================================================================

=head2 GetEncoding ()

	Return the encoding of generated content.

=cut

# ============================================================================
sub GetEncoding {
	my ($self) = @_;
	return "UTF-8";
}



# ============================================================================

=head2 CanCache ()

	Return 1 if the browser can cache the content, 0 otherwise.

=cut

# ============================================================================
sub CanCache {
	my ($self) = @_;
	return 0;
}



# ============================================================================

=head2 GetMIME ()

	Return the MIME type of generated content.

=cut

# ============================================================================
sub GetMIME {
	my ($self) = @_;
	
	my $uri = $self->{context}->GetURI();

	if($uri->IsSXML()) {
		return "application/pgp-encrypted";
	}
	else {
		return "application/xml";
	}
}



# ============================================================================

=head2 Generate ($request)

	This method generate the HTML from XML/XSLT.

=cut

# ============================================================================
sub Generate {
	my ($self, $r) = @_;
	my $stylesheet;
	my $result;
	
	my $xml = '<?xml version="1.0"?>';
	$xml .= "<Mioga2Response>";
	$xml .= qq|<Mioga version="$Mioga2::VERSION"/>|;
	
	$xml .= $self->{content};

	$xml .= "</Mioga2Response>";

	my $uri = $self->{context}->GetURI();

	my $data = $xml;
	if($uri->IsSXML()) {
		$data = $self->{recipient}->CryptAndSignData($xml);
	}

	return $data;
}



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Content

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
