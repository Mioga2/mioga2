# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Content::ASIS.pm: Content implementation class for ASIS generation.

=head1 DESCRIPTION

This module is a class for ASIS based content.

Parameters are :
	MIME : the mime type of the content.
    filename => the filename to return.

=head1 METHODS DESRIPTION

=cut

package Mioga2::Content::ASIS;
use strict;

use base qw(Mioga2::Content);

use Error qw(:try);
use Data::Dumper;
use URI::Escape;

# ============================================================================

=head2 Initialize ($context, %params)

	Initialize object and parse the parameters.

    Parameters :
      MIME => the mime type of content.
      filename => the filename to return.
	
=cut

# ============================================================================

sub Initialize {
	my ($self, $context, %params) = @_;
	
    $self->{IE}         = $context->IsIE();
    $self->{protocol}   = $context->GetConfig()->GetProtocol();
    
	if(! exists $params{MIME}) {
		$self->{mime} = 'application/octet-stream';
	}
	else {
		$self->{mime} = $params{MIME};
	}

	if(exists $params{filename}) {
		$self->{filename} =  $params{filename};
	}
}



# ============================================================================

=head2 CanCache ()

	Return 1 if the browser can cache the content, 0 otherwise.

=cut

# ============================================================================
sub CanCache {
	my ($self) = @_;
    return 1 if ($self->{filename} and $self->{IE} and $self->{protocol} eq 'https');
	return 0;
}



# ============================================================================

=head2 GetMIME ()

	Return the MIME type of generated content.

=cut

# ============================================================================
sub GetMIME {
	my ($self) = @_;

	return $self->{mime};
}



# ============================================================================

=head2 Generate ($request)

	This method just return the data.

=cut

# ============================================================================
sub Generate {
	my ($self, $r) = @_;

	if(exists $self->{filename}) {
	    my $filename = uri_escape($self->{filename});
		$r->headers_out->{'Content-Disposition'} = "attachment; filename=\"$filename\"";
	}

	if(exists $self->{cookies}) {
		foreach my $cook (@{$self->{cookies}}) {
			$r->headers_out->add("Set-Cookie" => $cook);
		}
	}

	return $self->{content};
}



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Content

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
