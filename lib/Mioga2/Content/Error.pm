# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Content::Error.pm: Content implementation class for XML/XSLT generation.

=head1 DESCRIPTION

This module is a class for XML/XSLT based content.

Parameters are :
	type : the error type
	string : the error description.

=head1 METHODS DESRIPTION

=cut

package Mioga2::Content::Error;
use strict;

use base qw(Mioga2::Content::XSLT);
use Locale::TextDomain::UTF8 'content_error';

use Error qw(:try);
use Data::Dumper;
use Mioga2::tools::string_utils;

my %XMLCache;



# ============================================================================

=head2 Initialize ($context, %params)

	Initialize object and parse the parameters

    Parameters :
	  string => the error description
      type => the error type.
	
=cut

# ============================================================================

sub Initialize {
	my ($self, $context, %params) = @_;
	
	if(! exists $params{string} or !exists $params{type}) {
		throw Mioga2::Exception::Simple("Content::Error::Initialize", 
										__"Missing parameters.");
	}

	$self->{stylesheet}     = "error.xsl";
    $self->{locale_domain}  = "error_xsl";

	my $message = $params{string};

	$message =~ s/</&lt;/gi;
	$message =~ s/>/&gt;/gi;
	$message =~ s/\n/<br\/>/gi;
	$message =~ s/\s/&\#160;/gi;

	my $xml = '<?xml version="1.0"?>';
	$xml .= "<DisplayErrorMessage>";
	$xml .= $context->GetXML();
	$xml .= "<ExceptionType>$params{type}</ExceptionType>";
	$xml .= "<ExceptionMessage>" .  st_FormatXMLString($message) . "</ExceptionMessage>";
	$xml .= "</DisplayErrorMessage>";

	$self->{content} = $xml;
}




# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Content

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
