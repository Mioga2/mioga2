# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Content::REDIRECT.pm: Content implementation class for Redirect response generation.

=head1 DESCRIPTION

This module is a class for XML/XSLT based content.

Parameters are :
	mode => external : return a 307 TEMP REDIRECT HTTP response
            internal : internaly redirected by apache. Nothing is returned to client. 


=head1 METHODS DESRIPTION

=cut

package Mioga2::Content::REDIRECT;
use strict;

use base qw(Mioga2::Content);

use Error qw(:try);
use Data::Dumper;
use Mioga2::Apache;

# ============================================================================

=head2 Initialize ($context, %params)

	Initialize object and parse the parameters

    Parameters :
      mode => external : return a 307 TEMP REDIRECT HTTP response
              internal : internaly redirected by apache. Nothing is returned to client. 
	
=cut

# ============================================================================

sub Initialize {
	my ($self, $context, %params) = @_;
	
	if(exists $params{mode}) {
		$self->{mode} = $params{mode};
	}
	else {
		$self->{mode} = "external";
	}
	
}



# ============================================================================

=head2 GetReturnCode ()

	Return the code to return to the user agent (OK, REDIRECT).

	OK by default.

=cut

# ============================================================================
sub GetReturnCode {
	my ($self) = @_;

	return Mioga2::Apache::HTTP_REDIRECT;
}

# ============================================================================

=head2 SetContent ($content, $params)

	$content is the URL to redirect to.

=cut

# ============================================================================
sub SetContent {
	my ($self, $content, %params) = @_;

	if(exists $params{'cookies'}) {
		$self->{cookies} = $params{cookies};
	}
	
	return $self->SUPER::SetContent($content);
}

# ============================================================================

=head2 Generate ($request)

	This method send the file using Apache::File.

=cut

# ============================================================================
sub Generate {
	my ($self, $r) = @_;

	if ($self->{mode} eq "internal") {
		$r->internal_redirect($self->{content});
	}
	elsif($self->{mode} eq "external") {
		$r->headers_out->{Location} = $self->{content};
		
		if(exists $self->{cookies}) {
			my $out = $r->err_headers_out();
			foreach my $cook (@{$self->{cookies}}) {
				$out->add("Set-Cookie" => $cook);
			}
		}
	}

	return undef;
}



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Content

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
