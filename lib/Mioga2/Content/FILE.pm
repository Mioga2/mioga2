# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Content::FILE.pm: Content implementation class for FILE generation.

=head1 DESCRIPTION

This module is a class for sending FILE .

Parameters are:
	MIME : the MIME type of the file.
	filename : the filename
=head1 METHODS DESRIPTION

=cut

package Mioga2::Content::FILE;
use strict;

use base qw(Mioga2::Content);

use Error qw(:try);
use Data::Dumper;
use POSIX qw(tmpnam);
use Mioga2::Apache;


# ============================================================================

=head2 Initialize ($context, %params)

	Initialize object and parse the parameters
	
    Parameters :
      MIME => the mime type of content.
      filename => the filename to return.
	
=cut

# ============================================================================

sub Initialize {
	my ($self, $context, %params) = @_;
	
    $self->{IE}         = $context->IsIE();
    $self->{protocol}   = $context->GetConfig()->GetProtocol();
    
	if(exists $params{MIME}) {
		$self->{MIME} = $params{MIME};
	}
	else {
		$self->{MIME} = "application/octet-stream";
	}
	
	if(exists $params{filename}) {
		$self->{filename} = $params{filename};
	}
}


# ============================================================================

=head2 CanCache ()

	Return 1 if the browser can cache the content, 0 otherwise.

=cut

# ============================================================================
sub CanCache {
	my ($self) = @_;
    return 1 if ($self->{filename} and $self->{IE} and $self->{protocol} eq 'https');
	return 0;
}



# ============================================================================

=head2 GetMIME ()

	Return the MIME type of generated content.

=cut

# ============================================================================
sub GetMIME {
	my ($self) = @_;

	return "application/octet-stream";
}


# ============================================================================

=head2 SetContent ($content)

	The content is the path of the file to send. 

=cut

# ============================================================================
sub SetContent {
	my ($self, $content) = @_;

	$self->{content} = $content;
}




# ============================================================================

=head2 Generate ($request)

	This method send the file using Apache::File.

=cut

# ============================================================================
sub Generate {
	my ($self, $r) = @_;

	# Get basename
	$self->{content} =~ m!([^/]+)$!;
	my $filename;
	if (exists($self->{filename}))
	{
		$filename = $self->{filename};
	}
	else
	{
		$filename = $1;
	}
	
	#mime type
	$r->headers_out->{'Content-Disposition'} = "attachment; filename=$filename";
    $r->content_type( $self->{MIME});
    $r->sendfile($self->{content});

	unlink($self->{content});

	return undef;
}



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Content

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
