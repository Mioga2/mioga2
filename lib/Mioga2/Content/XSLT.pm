# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Content::XSLT.pm: Content implementation class for XML/XSLT generation.

=head1 DESCRIPTION

This module is a class for XML/XSLT based content.

Parameters are:
	stylesheet : the file name of the stylesheet. This stylesheet must be in
	             directory defined in configuration (xsl_dir).

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Content::XSLT;
use strict;
use utf8;

use base qw(Mioga2::Content);
use Locale::Messages qw(:locale_h :libintl_h);
use Error qw(:try);
use Data::Dumper;
use XML::LibXSLT;
use XML::LibXML;
use Mioga2::Apache;

my $debug = 0;

# ============================================================================

=head2 Initialize ($context, %params)

	Initialize object and parse the parameters
	
    Parameters :
      stylesheet => the filename of the stylesheet to apply to XML.
	
=cut

# ============================================================================

sub Initialize {
	my ($self, $context, %params) = @_;
	
	if(! exists $params{stylesheet}) {
		throw Mioga2::Exception::Simple("Content::XSLT::Initialize", "Missing parameters.");
	}
    
    $self->{IE}         = $context->IsIE();
    $self->{protocol}   = $context->GetConfig()->GetProtocol();

	$self->{locale_domain} = $params{locale_domain};
	$self->{stylesheet} = $params{stylesheet};
	$self->{encoding} = undef; #GPT 27/10/2006
	$self->{media_type} = "text/html"; #GPT 27/10/2006
    
    if(exists $params{filename}) {
		$self->{filename} = $params{filename};
	}
	
	if (($self->{filename} and $self->{IE} and $self->{protocol} eq 'https')) {
		# http://support.microsoft.com/default.aspx?scid=316431
		$self->{can_cache} = 1;
	} elsif (exists $params{permit_cache}) {
		$self->{can_cache} = $params{permit_cache};
	} else {
		$self->{can_cache} = 0;
	}
	
}
# ============================================================================

=head2 GetEncoding ()

	Return the encoding of generated content.

=cut

# ============================================================================
sub GetEncoding {
	my ($self) = @_;
	return $self->{encoding}; #GPT 27/10/2006
}
# ============================================================================

=head2 CanCache ()

	Return 1 if the browser can cache the content, 0 otherwise.

=cut

# ============================================================================
sub CanCache {
	my ($self) = @_;
	return $self->{can_cache};
}
# ============================================================================

=head2 GetMIME ()

	Return the MIME type of generated content.

=cut

# ============================================================================
sub GetMIME {
	my ($self) = @_;

	return $self->{media_type}; #GPT 27/10/2006
}
# ============================================================================

=head2 Generate ($request)

	This method generate the HTML from XML/XSLT.

=cut

# ============================================================================
sub Generate {
	my ($self, $r) = @_;
	my $config = $self->{context}->GetConfig();
	
	# Parse XML to remove unwanted characters (http://www.w3.org/TR/REC-xml/#charsets)
	$self->{content} =~ s/[\x01-\x09\x0B\x0C\x0E-\x1F]//g;

	my $xml = $self->{content};

	print STDERR "xml = $xml\n" if $debug;

	if($config->DumpXML()) {
		my $tmpdir = $config->GetTmpDir();

		if(! -d "$tmpdir/xmldump") {
			mkdir("$tmpdir/xmldump");
		}

		my $uri = $self->{context}->GetURI();
		my $filename = $uri->GetAppIdent()."-".$uri->GetMethod();

		my $i;
		for($i = 0; -e "$tmpdir/xmldump/$filename-$i.xml"; $i++) {}

		open(F_URI, ">:utf8", "$tmpdir/xmldump/$filename-$i.uri");
		print F_URI $uri->GetURI();
		print F_URI "?".$r->args if $r->args ne "";
		print F_URI "\n";
		close(F_URI);

		open(F_XML, ">:utf8", "$tmpdir/xmldump/$filename-$i.xml");
		print F_XML $xml;
		close(F_XML);
	}
    
    if ($self->{filename}) {
        $r->headers_out->{'Content-Disposition'} = "attachment; filename=".$self->{filename};
    }

	return $self->XSLTransform();
}
# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


=head2 XSLTransform ()

	Return the content generated after transformation.

=cut

# ============================================================================

sub XSLTransform() {
	my ($self) = @_;

	my $xml     = $self->{content};
	my $config  = $self->{context}->GetConfig;
	my $parser  = XML::LibXML->new;
	my $xslt    = XML::LibXSLT->new;

	my $xsl_file = $self->GetStylesheetPath;
	$xsl_file =~ s!//!/!g;
	
	my $source = $parser->parse_string($xml);
	print STDERR "xml parsed\n" if $debug;

	my $style_doc = $parser->parse_file($xsl_file);
	print STDERR "xsl parsed\n" if $debug;

	#
	# Initalize I18n
	#
	textdomain($self->{locale_domain});
	bindtextdomain("$self->{locale_domain}" => $config->GetLocalesPath);
	$xslt->register_function("urn:mioga", "gettext", sub { 
															my ($string, @params) = @_;
															$string = gettext($string);
															utf8::decode($string);
															for (@params) {
																utf8::decode ($_);
															}
															return sprintf($string, @params);
														});

	print STDERR "xslt ".Dumper($xslt)."\n" if $debug;
	my $stylesheet;
	$stylesheet = $xslt->parse_stylesheet($style_doc);
	print STDERR "stylesheet parsed\n" if $debug;
	if (!defined($stylesheet)) {
		throw Mioga2::Exception::Simple("Content::XSLT::XSLTransform", "Cannot parse stylesheet = '$self->{stylesheet}'");
	}
	print STDERR "stylesheet ".Dumper($stylesheet)."\n" if $debug;
	$self->{encoding} = $stylesheet->output_encoding;
	$self->{media_type} = $stylesheet->media_type;

	my $result;
	$result = $stylesheet->transform($source);
    
	return $stylesheet->output_string($result);
}


# ============================================================================

=head2 GetStylesheetPath ()

	Return the real path to the stylesheet file (supporting themes and langs).

=cut

# ============================================================================
sub GetStylesheetPath {
    my ($self) = @_;

	my $context = $self->{context};
	my $config  = $context->GetConfig();

	my $xsl_dir = $config->GetXSLDir();
	my $theme_ident;
	my $lang_ident;

	try {
		my $group = $context->GetGroup();
		
		$theme_ident = $group->GetTheme();
		$lang_ident  = $group->GetLang();
		

		if(! defined $theme_ident) {
			$theme_ident = "";
		}
		else {
			$theme_ident .= "/";
		}
		
		if(! defined $lang_ident) {
			$lang_ident = "";
		}
		else {
			$lang_ident .= "/";	
		}
		
	}

	otherwise {
		$theme_ident = "";
		$lang_ident = "";
	};

	print STDERR "Content::XLST::GetStylesheetPath theme_ident = $theme_ident\n"
				."                                 lang_ident  = $lang_ident\n"
				."                                 xsl_dir     = $xsl_dir\n"
				."                                 stylesheet  = $self->{stylesheet}\n" if ($debug);
	foreach my $path ("${xsl_dir}/${theme_ident}/${lang_ident}/$self->{stylesheet}",
					  "${xsl_dir}/${theme_ident}/$self->{stylesheet}",
					  "${xsl_dir}/default/$self->{stylesheet}") {


		if (-e $path) {
			return $path;
		}
	}

	# ----------------------------------------------
	# If we are here, there is something wrong
	# ----------------------------------------------
	throw Mioga2::Exception::Simple("Content::XSLT::GetStylesheet",
	                                "No stylesheet file = '$self->{stylesheet}'");

}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Content

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
