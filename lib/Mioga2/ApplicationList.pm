#===============================================================================
#
#         FILE:  ApplicationList.pm
#
#  DESCRIPTION:  New generation Mioga2 application list class
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  17/09/2010 15:51
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

ApplicationList.pm: New generation Mioga2 application list class

=head1 DESCRIPTION

This class handles Mioga2 applications. It is designed to create, load, update or delete
applications, to link them to instances, users or groups.

=head1 SYNOPSIS

 use Mioga2::ApplicationList;
 use Mioga2::Config;

 my $applicationlist;
 my $config = new Mioga2::Config (...);

 # Get list of application available to current instance
 $applicationlist = Mioga2::ApplicationList->new ($config);

 # Get list of applications available to group 'Administrators'
 my $group = Mioga2::GroupList->new ($config, { attributes => { ident => 'Administrators' } });
 $applicationlist = Mioga2::ApplicationList->new ($config, { attributes => { groups => $group } });

 # Get list of applications enabled in group 'Administrators'
 my $group = Mioga2::GroupList->new ($config, { attributes => { ident => 'Administrators' } });
 $applicationlist = Mioga2::ApplicationList->new ($config, { attributes => { groups => $group, only_active => 1 } });

 # Get list of applications user 'admin' can access to in group 'Administrators'
 my $group = Mioga2::GroupList->new ($config, { attributes => { ident => 'Administrators' } });
 my $user = Mioga2::UserList->new ($config, { attributes => { ident => 'admin' } });
 $applicationlist = Mioga2::ApplicationList->new ($config, { attributes => { groups => $group, users => $user } });

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::ApplicationList;

use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::tools::args_checker;

my $debug = 0;

# Application types handled by the module
my @application_types = qw/normal shell portal resource/;

#===============================================================================

=head2 new

Create a new Mioga2::ApplicationList object.

=over

=item B<arguments:>

=over

=item I<$config:> a Mioga2::Config object matching the instance to work into.
          OR a Mioga2::MiogaConf object to access system-wide applications.

=item I<$data:> a hashref with the following keys:

=over

=item I<attributes:> a list of key / value pairs matching the applications.

=item I<short_list:> an optional flag to limit the amount of data describing the applications.

=item I<fields:> a list of fields describing the applications (this overrides the behavior of the flag above).

=item I<match:> a string indicating the match type for applications (may be 'begins', 'contains', 'ends' or may be empty for exact match).

=back

=item B<return:> a Mioga2::ApplicationList object matching applications according to provided attributes.

=back

=back

=cut

#===============================================================================
sub new {
	my ($class, $config, $data) = @_;
	print STDERR "[Mioga2::ApplicationList::new] Entering, data: " . Dumper $data if ($debug);

	my $self = { };
	bless($self, $class);

	if (ref ($config) eq 'Mioga2::Config') {
		# $config is Mioga2::Config, set internal config key to embedded Mioga2::MiogaConf and instance attribute to corresponding instance ident
		$data->{attributes}->{instance} = $config->GetMiogaIdent ();
		$self->{langdir} = $config->GetLangDir ();
		$self->{lang} = $config->GetDefaultLang ();
		$config = $config->GetMiogaConf ();
	}

	$self->{config} = $config;
	$self->{dbh} = $self->{config}->GetDBH ();

	$self->{langdir} = $self->{config}->GetInstallDir () . '/conf/lang/' unless (exists $self->{langdir});
	$self->{lang} = $config->GetDefaultLang () unless (exists ($self->{lang}));

	# Special mode if $attributes{only_active}, set global flag to pick data only from m_application_group_allowed
	if (exists ($data->{attributes}->{only_active})) {
		$self->{only_active} = delete ($data->{attributes}->{only_active});
	}

	# Initialize attributes
	$self->{attributes} = $data->{attributes};

	# Initialize object state
	$self->{loaded} = 0;
	$self->{deleted} = 0;

	# Initialize WHERE statement before any update is made on the list
	$self->SetWhereStatement (keys (%{$self->{attributes}}));

	print STDERR "[Mioga2::ApplicationList::new] Leaving\n" if ($debug);
	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 Get

Get an attribute value.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get.

=back

=item B<return:> the attribute value.

=back

=cut

#===============================================================================
sub Get {
	my ($self, $attribute) = @_;
	print STDERR "[Mioga2::ApplicationList::Get] Entering, attribute name is $attribute\n" if ($debug);

	my $value = undef;

	#-------------------------------------------------------------------------------
	# Load the object from DB if attribute is not known already
	#-------------------------------------------------------------------------------
	if ((!grep /^$attribute$/, keys (%{$self->{attributes}})) && (!grep /^$attribute$/, keys (%{$self->{update}})) && !$self->{loaded}) {
		# The requested attribute is not set into object yet, get it from DB
		$self->Load ();
	}

	#-------------------------------------------------------------------------------
	# Retreive attribute value
	#-------------------------------------------------------------------------------
	if (exists ($self->{update}->{$attribute})) {
		# Attribute is in update list, get its value
		$value = $self->{update}->{$attribute};
	}
	elsif (exists ($self->{attributes}->{$attribute})) {
		# Attribute is in initial list, get its value
		$value = $self->{attributes}->{$attribute};
	}
	else {
		# Attribute is not in initial list, nor in update list, pick it from loaded applications
		if (scalar (@{$self->{applications}}) > 1) {
			# Multiple applications in the list, return an array of attribute values.
			$value = ();
			for (@{$self->{applications}}) {
				push (@$value, $_->{$attribute});
			}
		}
		else {
			# Only one application in the list, return attribute value
			$value = $self->{applications}->[0]->{$attribute};
		}
	}

	if (ref ($value) eq 'ARRAY') {
		print STDERR "[Mioga2::ApplicationList::Get] Leaving, attribute value is " . Dumper $value if ($debug);
	}
	else {
		print STDERR "[Mioga2::ApplicationList::Get] Leaving, attribute value is $value\n" if ($debug);
	}
	return ($value);
}	# ----------  end of subroutine Get  ----------


#===============================================================================

=head2 Count

Count the applications in the list. This method loads the application list from
DB if required.

=over

=item B<return:> the number of applications in the list.

=back

=cut

#===============================================================================
sub Count {
	my ($self) = @_;

	$self->Load () unless $self->{loaded};

	my $count = scalar (@{$self->{applications}});

	print STDERR "[Mioga2::ApplicationList::Count] $count applications\n" if ($debug);

	return ($count);
}	# ----------  end of subroutine Count  ----------


#===============================================================================

=head2 GetRowIds

Get RowIds for applications in the list.

=over

=item B<return:> an array of rowids.

=back

=cut

#===============================================================================
sub GetRowIds {
	my ($self) = @_;

	my @list = ();

	$self->Load () unless $self->{loaded};

	for (@{$self->{applications}}) {
		push (@list, $_->{rowid});
	}

	return (@list);
}	# ----------  end of subroutine GetRowIds  ----------


#===============================================================================

=head2 GetApplications

Get list of applications in the list.

=over

=item B<return:> an array of hashes describing the applications in the list. Each hash
of this array can be used to create a new Mioga2::ApplicationList that
only matches this application.

=back

=cut

#===============================================================================
sub GetApplications {
	my ($self) = @_;

	$self->Load () unless $self->{loaded};

	for my $application (@{$self->{applications}}) {
		for (keys (%{$self->{update}})) {
			$application->{$_} = $self->{update}->{$_};
		}
	}

	return (@{$self->{applications}});
}	# ----------  end of subroutine GetApplications  ----------


#===============================================================================

=head2 Load

Load object attributes from DB. This method is automatically called when trying to access an attribute whose value has not already been loaded from DB.

=cut

#===============================================================================
sub Load {
	my ($self) = @_;
	print STDERR "[Mioga2::ApplicationList::Load] Entering\n" if ($debug);

	my $appslang = $self->LoadAppsLang ();

	my $group_mioga_id = '';
	my $user_mioga_id = '';
	my $additionnal_fields = '';
	my $additionnal_tables = '';
	if ($self->Has ('instance')) {
		$group_mioga_id = " AND m_group.mioga_id = (SELECT rowid FROM m_mioga WHERE ident = '" . $self->Get ('instance') . "')";
		$user_mioga_id = " AND m_user_base.mioga_id = (SELECT rowid FROM m_mioga WHERE ident = '" . $self->Get ('instance') . "')";
		$additionnal_fields = ", m_instance_application.all_groups, m_instance_application.all_users, (SELECT count(m_application_group) FROM m_application_group, m_group, m_group_type WHERE application_id = m_application.rowid AND group_id = m_group.rowid AND m_group.type_id = m_group_type.rowid and m_group_type.ident = 'group' $group_mioga_id) AS nb_groups, (SELECT count(m_application_group) FROM m_application_group, m_user_base WHERE application_id = m_application.rowid AND group_id = m_user_base.rowid $user_mioga_id) AS nb_users";
		$additionnal_tables = ', m_instance_application';
	}
	if ($self->Has ('groups')) {
		$additionnal_fields .= ", (SELECT is_public FROM m_application_group_allowed WHERE m_application_group_allowed.group_id IN (" . join (', ', @{ac_ForceArray ($self->Get ('groups')->Get ('rowid'))}) . ") AND m_application_group_allowed.application_id = m_application.rowid) AS is_public";
	}

	# Limit results to is_user, is_group or is_resouce according to attributes
	my $usable_apps = '';
	for my $type (qw/group user resource/) {
		if ($self->Has ($type . 's')) {
			$usable_apps .= " AND m_application.is_$type = 't'";

			# If object has both 'groups' and 'users', then we want group applications the users can use
			# It makes no sense to limit applications to is_group = 't' AND is_user = 't'
			# So just break the loop now
			last;
		}
	}

	#-------------------------------------------------------------------------------
	# Initialize and run SQL query
	#-------------------------------------------------------------------------------
	my $sql = "SELECT m_application.* $additionnal_fields FROM m_application $additionnal_tables" . (($self->GetWhereStatement () ne '') ? ' WHERE ' . $self->GetWhereStatement () : '') . $usable_apps . ' ORDER BY m_application.ident;';

	print STDERR "[Mioga2::ApplicationList::Load] SQL: $sql\n" if ($debug);

	my $res = SelectMultiple ($self->{dbh}, $sql);

	#-------------------------------------------------------------------------------
	# Drop undefined values
	#-------------------------------------------------------------------------------
	for my $application (@$res) {
		for (keys (%$application)) {
			delete ($application->{$_}) if (!defined ($application->{$_}));
		}
		$application->{label} = $appslang->{$application->{ident}}->{name} ? $appslang->{$application->{ident}}->{name} : $application->{ident};
	}

	#-------------------------------------------------------------------------------
	# Update object contents
	#-------------------------------------------------------------------------------
	$self->{applications} = $res;

	# Raise "loaded" flag if query returned something
	$self->{loaded} = 1 if (scalar (@{$self->{applications}}));

	print STDERR "[Mioga2::ApplicationList::Load] Leaving\n" if ($debug);
}	# ----------  end of subroutine Load  ----------


#===============================================================================

=head2 Revert

Revert list.

=over

=item B<arguments:>

=over

=item I<$same_instance> A flag indicating wether the reverted list shoud match applications from the same instance or not.

=back

=back

=cut

#===============================================================================
sub Revert {
	my ($self, $same_instance) = @_;

	my $inst_request = '';
	my $inst_from = '';
	my $inst_attr = undef;
	if ($self->Has ('instance')) {
		$inst_from = ', m_instance_application';
		if ($self->Has ('groups') || $self->Has ('users') || $same_instance) {
			$inst_attr = $self->Get ('instance');
			$inst_request = "AND m_application.rowid = m_instance_application.application_id AND " . $self->RequestForAttribute ('instance');
		}
	}

	# Limit results to is_user, is_group or is_resouce according to attributes
	my $usable_apps = '';
	for my $type (qw/group user resource/) {
		if ($self->Has ($type . 's')) {
			$usable_apps .= " AND m_application.is_$type = 't'";

			# If only active applications wanted, so do not revert list against m_instance_application but restrict it to group-accessible applications
			if ($self->{only_active}) {
				$usable_apps .= ' AND (m_instance_application.all_' . $type . "s = 't' OR m_application.rowid IN (SELECT application_id FROM m_application_group WHERE group_id IN (" . join (', ', $self->Get ($type . 's')->Get ('rowid')) .  ")))";
			}
		}
	}

	$self->{where} = "m_application.rowid NOT IN (SELECT m_application.rowid FROM m_application $inst_from" . (($self->GetWhereStatement () ne '') ? " WHERE " . $self->GetWhereStatement () : '') . ") AND m_application.type_id IN (SELECT rowid FROM m_application_type WHERE ident IN ('" . join ("', '", @application_types) . "')) $inst_request $usable_apps";

	$self->{attributes} = { };
	$self->{update} = { };
	$self->{loaded} = 0;
	$self->{deleted} = 0;

	# Add "instance" attribute to reverted list in some cases (list of applications for a user, for a group)
	if ($inst_attr) {
		$self->{attributes}->{instance} = $inst_attr;
	}

	print STDERR "[Mioga2::ApplicationList::Revert] New WHERE statement: $self->{where}\n" if ($debug);
}	# ----------  end of subroutine Revert  ----------


#===============================================================================

=head2 GetGroupList

Get list of groups that can use the applications from the list.

=cut

#===============================================================================
sub GetGroupList {
	my ($self) = @_;

	my $groups;

	if ($self->Get ('is_group')) {
		# Internal config key is a Mioga2::MiogaConf object. Convert it to Mioga2::Config for GroupList
		my $config = Mioga2::Config->new ($self->{config}, $self->{attributes}{instance});

		$groups = Mioga2::GroupList->new ($config, { attributes => { applications => $self } });
	}

	return ($groups);
}	# ----------  end of subroutine GetGroupList  ----------


#===============================================================================

=head2 GetUserList

Get list of users that can use the applications from the list.

=cut

#===============================================================================
sub GetUserList {
	my ($self) = @_;

	my $users;

	if ($self->Get ('is_user')) {
		# Internal config key is a Mioga2::MiogaConf object. Convert it to Mioga2::Config for UserList
		my $config = Mioga2::Config->new ($self->{config}, $self->{attributes}{instance});

		$users = Mioga2::UserList->new ($config, { attributes => { applications => $self } });
	}

	return ($users);
}	# ----------  end of subroutine GetUserList  ----------


#===============================================================================

=head2 SetUsers

Set list of users that can use the applications.

=cut

#===============================================================================
sub SetUsers {
	my ($self, $users) = @_;
	print STDERR "[Mioga2::ApplicationList::SetUsers] Entering\n" if ($debug);

	# Prevent non-user applications to be enabled to users
	if ($users->Count () && !$self->Get ('is_user')) {
		throw Mioga2::Exception::Simple ('Mioga2::ApplicationList::SetUsers', "Application " . $self->Get ('ident') . " is not a user-application");
	}

	my $config = Mioga2::Config->new ($self->{config}, $self->{attributes}{instance});

	my $mioga_id = $config->GetMiogaId ();
	my @user_rowids = @{ac_ForceArray ($users->Get ('rowid'))};
	my $user_rowids = join (', ', @user_rowids);
	$user_rowids = '-1' if ($user_rowids eq '');
	my $where = $self->GetWhereStatement ();

	my $instance_users = Mioga2::UserList->new ($config);

	my $sql;

	if ($instance_users->Count () != scalar (@user_rowids)) {
		#-------------------------------------------------------------------------------
		# Some users selected, application will not be all_users anymore if it was and allowed users will be recorded individually
		#-------------------------------------------------------------------------------

		# Delete application methods / profiles association for users that are not in the list
		$sql = "DELETE FROM m_profile_function USING m_function, m_application, m_instance_application, m_profile, m_user_base WHERE m_function.application_id = m_application.rowid AND m_profile.group_id = m_user_base.rowid AND m_profile_function.profile_id = m_profile.rowid AND m_profile_function.function_id = m_function.rowid AND $where AND m_user_base.rowid NOT IN ($user_rowids) AND m_user_base.mioga_id = $mioga_id;";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Disable applications for users that are not in the list
		$sql = "DELETE FROM m_application_group_allowed WHERE (application_id, group_id) IN (SELECT m_application.rowid AS application_id, m_user_base.rowid AS group_id FROM m_application, m_instance_application, m_user_base WHERE $where AND m_user_base.rowid NOT IN ($user_rowids) AND m_user_base.mioga_id = $mioga_id);";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Disallow applications for users that are not in the list
		$sql = "DELETE FROM m_application_group WHERE (application_id, group_id) IN (SELECT m_application.rowid AS application_id, m_user_base.rowid AS group_id FROM m_application, m_instance_application, m_user_base WHERE $where AND m_user_base.rowid NOT IN ($user_rowids) AND m_user_base.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE all_users = 't' AND mioga_id = $mioga_id));";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Process all_users applications
		$sql = "UPDATE m_instance_application SET all_users = 'f' FROM m_application WHERE m_instance_application.application_id = m_application.rowid AND $where;";
		ExecSQL ($self->{config}->GetDBH (), $sql);
		
		# Enable applications that are in the list
		$sql = "INSERT INTO m_application_group (application_id, group_id) SELECT m_application.rowid AS application_id, m_user_base.rowid AS group_id FROM m_application, m_instance_application, m_user_base WHERE $where AND m_user_base.rowid IN ($user_rowids) AND m_user_base.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_application_group WHERE group_id = m_user_base.rowid) AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE all_users = 't' AND mioga_id = $mioga_id);";
		ExecSQL ($self->{config}->GetDBH (), $sql);
	}
	else {
		#-------------------------------------------------------------------------------
		# All users selected, application will be all_users and individually allowed users will be removed from DB
		#-------------------------------------------------------------------------------

		# Disallow applications for all users. This will not change wether or not a user has enabled an application in his personal space
		$sql = "DELETE FROM m_application_group WHERE (application_id, group_id) IN (SELECT m_application.rowid AS application_id, m_user_base.rowid AS group_id FROM m_application, m_instance_application, m_user_base WHERE $where AND m_user_base.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE all_users = 't' AND mioga_id = $mioga_id));";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Set application as all_users
		$sql = "UPDATE m_instance_application SET all_users = 't' FROM m_application WHERE m_instance_application.application_id = m_application.rowid AND $where;";
		ExecSQL ($self->{config}->GetDBH (), $sql);
	}

	
	print STDERR "[Mioga2::ApplicationList::SetUsers] Leaving\n" if ($debug);
}	# ----------  end of subroutine SetUsers  ----------


#===============================================================================

=head2 SetGroups

Set list of groups that can use the applications.

=cut

#===============================================================================
sub SetGroups {
	my ($self, $groups) = @_;
	print STDERR "[Mioga2::ApplicationList::SetGroups] Entering\n" if ($debug);

	# Prevent non-group applications to be enabled to groups
	if ($groups->Count () && !$self->Get ('is_group')) {
		throw Mioga2::Exception::Simple ('Mioga2::ApplicationList::SetUsers', "Application " . $self->Get ('ident') . " is not a group-application");
	}

	my $config = Mioga2::Config->new ($self->{config}, $self->{attributes}{instance});

	my $mioga_id = $config->GetMiogaId ();
	my @group_rowids = @{ac_ForceArray ($groups->Get ('rowid'))};
	my $group_rowids = join (', ', @group_rowids);
	$group_rowids = '-1' if ($group_rowids eq '');
	my $where = $self->GetWhereStatement ();

	my $instance_groups = Mioga2::GroupList->new ($config);

	my $sql;

	if ($instance_groups->Count () != scalar (@group_rowids)) {
		#-------------------------------------------------------------------------------
		# Some groups selected, application will not be all_groups anymore if it was and allowed groups will be recorded individually
		#-------------------------------------------------------------------------------

		# Delete application methods / profiles association for groups that are not in the list
		$sql = "DELETE FROM m_profile_function USING m_function, m_application, m_instance_application, m_profile, m_group WHERE m_function.application_id = m_application.rowid AND m_profile.group_id = m_group.rowid AND m_profile_function.profile_id = m_profile.rowid AND m_profile_function.function_id = m_function.rowid AND $where AND m_group.rowid NOT IN ($group_rowids) AND m_group.mioga_id = $mioga_id;";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Disable applications for groups that are not in the list
		$sql = "DELETE FROM m_application_group_allowed WHERE (application_id, group_id) IN (SELECT m_application.rowid AS application_id, m_group.rowid AS group_id FROM m_application, m_instance_application, m_group WHERE $where AND m_group.rowid NOT IN ($group_rowids) AND m_group.mioga_id = $mioga_id);";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Disallow applications for groups that are not in the list
		$sql = "DELETE FROM m_application_group WHERE (application_id, group_id) IN (SELECT m_application.rowid AS application_id, m_group.rowid AS group_id FROM m_application, m_instance_application, m_group WHERE $where AND m_group.rowid NOT IN ($group_rowids) AND m_group.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE all_groups = 't' AND mioga_id = $mioga_id));";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Process all_groups applications
		$sql = "UPDATE m_instance_application SET all_groups = 'f' FROM m_application WHERE m_application.rowid = m_instance_application.application_id AND $where;";
		ExecSQL ($self->{config}->GetDBH (), $sql);
		
		# Enable applications that are in the list
		$sql = "INSERT INTO m_application_group (application_id, group_id) SELECT m_application.rowid AS application_id, m_group.rowid AS group_id FROM m_application, m_instance_application, m_group WHERE $where AND m_group.rowid IN ($group_rowids) AND m_group.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_application_group WHERE group_id = m_group.rowid) AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE all_groups = 't' AND mioga_id = $mioga_id);";
		ExecSQL ($self->{config}->GetDBH (), $sql);
	}
	else {
		#-------------------------------------------------------------------------------
		# All groups selected, application will be all_groups and individually allowed groups will be removed from DB
		#-------------------------------------------------------------------------------

		# Disallow applications for all groups. This will not change wether or not a group has enabled an application in his space
		$sql = "DELETE FROM m_application_group WHERE (application_id, group_id) IN (SELECT m_application.rowid AS application_id, m_group.rowid AS group_id FROM m_application, m_instance_application, m_group WHERE $where AND m_group.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE all_groups = 't' AND mioga_id = $mioga_id));";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Set applications as all_groups
		$sql = "UPDATE m_instance_application SET all_groups = 't' FROM m_application WHERE m_instance_application.application_id = m_application.rowid AND $where;";
		ExecSQL ($self->{config}->GetDBH (), $sql);
	}
	
	print STDERR "[Mioga2::ApplicationList::SetGroups] Leaving\n" if ($debug);
}	# ----------  end of subroutine SetGroups  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


#===============================================================================

=head2 RequestForAttribute

Generate SQL statement that matches attribute into m_application.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get associated SQL request.

=back

=item B<return:> an array containing an attribute name and the statement to
match this attribute in m_application table. If the attribute is stored as-is
into table, this method returns the attribute and its value. If the attribute
is stores as the ID of a record in another table, this method returns the
attribute name and a SQL subquery picking the attribute ID from this other
table. If the attribute has to be ignored, this method returns empty strings.

=back

=cut

#===============================================================================
sub RequestForAttribute {
	my ($self, $attribute) = @_;

	my $request = '';
	my $value = '';
	my $operator = '=';
	my $attrval = '';

	#-------------------------------------------------------------------------------
	# Format string so that attribute value can be a scalar or an array
	#-------------------------------------------------------------------------------
	if (ref ($self->Get ($attribute)) eq 'ARRAY') {
		$operator = 'IN';
		for (@{$self->Get ($attribute)}) {
			$attrval = "(" . join (', ', map { $self->{dbh}->quote ($_) } @{$self->Get ($attribute)}) . ")";
		}
		if ($attrval eq '') {
			$attrval = '(NULL)';
		}
	}
	elsif (ref ($self->Get ($attribute)) eq 'Mioga2::GroupList') {
		$operator = 'IN';
		my @rowids = $self->Get ($attribute)->GetRowIds ();
		my $mioga_id = $self->Get ($attribute)->Get ('mioga_id');

		if ($self->{only_active}) {
			$attrval = "(SELECT application_id FROM m_application_group_allowed WHERE group_id IN (" . join (', ', @rowids) . "))";
		}
		else {
			$attrval = "(SELECT application_id FROM m_application_group WHERE group_id IN (" . join (', ', @rowids) . ") UNION SELECT m_instance_application.application_id FROM m_instance_application WHERE m_instance_application.mioga_id = $mioga_id AND m_instance_application.all_groups = 't')";
		}
	}
	elsif (ref ($self->Get ($attribute)) eq 'Mioga2::UserList') {
		$operator = 'IN';
		my @rowids = $self->Get ($attribute)->GetRowIds ();
		my $mioga_id = $self->Get ($attribute)->Get ('mioga_id');

		if ($self->Has ('groups')) {
			my @group_rowids = $self->Get ('groups')->Get ('rowid');
			$attrval = '(SELECT DISTINCT app_id FROM m_profile_user_function WHERE group_id IN (' . join (', ', @group_rowids) . ') AND user_id IN (' . join (', ', @rowids) . ') UNION SELECT DISTINCT app_id FROM m_profile_expanded_user_method WHERE group_id IN (' . join (', ', @group_rowids) . ') AND user_id IN (' . join (', ', @rowids) . '))';
		}
		elsif ($self->{only_active}) {
			$attrval = "(SELECT application_id FROM m_application_group_allowed WHERE group_id IN (" . join (', ', @rowids) . "))";
		}
		else {
			$attrval = "(SELECT application_id FROM m_application_group WHERE group_id IN (" . join (', ', @rowids) . ") UNION SELECT m_instance_application.application_id FROM m_instance_application WHERE m_instance_application.mioga_id = $mioga_id AND m_instance_application.all_users = 't')";
		}
	}
	else {
		if ($self->Get ($attribute) eq '') {
			$operator = 'IS';
			$attrval = 'NULL';
		}
		else {
			$attrval = $self->{dbh}->quote ($self->Get ($attribute));
		}
	}

	#-------------------------------------------------------------------------------
	# Process attribute name
	#-------------------------------------------------------------------------------
	if (grep (/^$attribute$/, qw/instance/)) {
		$value = "(SELECT rowid FROM m_mioga WHERE ident $operator $attrval)";
		$operator = 'IN';
		$attribute = 'm_instance_application.mioga_id';
	}
	elsif (grep (/^$attribute$/, qw/groups users/)) {
		$value = $attrval;
		$attribute = 'm_application.rowid';
	}
	else {
		# Other attributes are simply associated to their text value
		$value = $attrval if ($self->Has ($attribute));
		$attribute = "m_application.$attribute";
	}

	$request = "$attribute $operator $value" if ($attribute ne '' && $value ne '');

	return ($request);
}	# ----------  end of subroutine RequestForAttribute  ----------


#===============================================================================

=head2 Has

Check if a application list has a given attribute set.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to check.

=back

=item B<return:> 1 or 0 whether the object has or not a given attribute in its
current state.

=back

B<Warning:> This method doesn't load the list from DB so it is possible that
the object does not have an attribute that the applications of the list will
have once they have been loaded.

=cut

#===============================================================================
sub Has {
	my ($self, $attribute) = @_;

	return (exists ($self->{update}->{$attribute}) || exists ($self->{attributes}->{$attribute}) || ($attribute eq 'rowid'));
}	# ----------  end of subroutine Has  ----------


#===============================================================================

=head2 SetWhereStatement

Initialize a WHERE statement that matches application list in m_application
according to provided attributes.

=over

=item B<arguments:>

=over

=item I<@attributes:> a list of inner attributes to base the statement on.

=item B<return:> a string to paste as-is into a request, after the WHERE keyword.

=back

=back

=cut

#===============================================================================
sub SetWhereStatement {
	my ($self, @attributes) = @_;

	# Fetch attributes, if any
	for my $attr (@attributes) {
		$self->Get ($attr);
	}

	# Fetch attributes, if any
	for my $attr (@attributes) {
		$self->Get ($attr);
	}

	my @where = ( );

	for my $attrname (@attributes) {
		my $request = $self->RequestForAttribute ($attrname);
		push (@where, "$request") unless ($request eq '');
	}

	# Force application type
	push (@where, "m_application.type_id IN (SELECT rowid FROM m_application_type WHERE ident IN ('" . join ("', '", @application_types) . "'))");

	# Limit to instance applications
	if ($self->Has ('instance')) {
		push (@where, "m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = (SELECT rowid FROM m_mioga WHERE ident = '" . $self->Get ('instance') . "')");
	}

	my $where = join (' AND ', @where);

	print STDERR "[Mioga2::ApplicationList::SetWhereStatement] $where\n" if ($debug);

	$self->{where} = $where;

	return ($self->{where});
}	# ----------  end of subroutine SetWhereStatement  ----------


#===============================================================================

=head2 GetWhereStatement

Generate a WHERE statement that matches user list in m_user_base.

=over

=item B<return:> a string to paste as-is into a request, after the WHERE keyword.

=back

=cut

#===============================================================================
sub GetWhereStatement {
	my ($self) = @_;

	$self->SetWhereStatement () if (!$self->{where});

	return ($self->{where});
}	# ----------  end of subroutine GetWhereStatement  ----------


# ============================================================================

=head2 LoadAppsLang ($lang)

	Load applications translation files.

=cut

# ============================================================================

my %LocaleCache;

sub LoadAppsLang {
	my ($self) = @_;

	my $lang = $self->{lang};

	my $appslang;
	if(exists $LocaleCache{$lang}) {
		$appslang = $LocaleCache{$lang};
	}
	else {
		my $langdir = $self->{langdir};

		my $file;
		if( -e "$langdir/${lang}.xml") {
			$file = "$langdir/${lang}.xml";
		} 
		else {
			$file = "$langdir/default.xml";
		}
			
		open(F_LANGFILE, $file)
        or die "Can't open file $file: $!";

		my $xml;

		{
			local $/ = undef;
			$xml = <F_LANGFILE>;
		}

		close(F_LANGFILE);

		my $xs = new Mioga2::XML::Simple;
		my $apps;
		for (@{$xs->XMLin($xml)->{application}}) {
			$apps->{$_->{ident}} = {
					name => $_->{name},
					description => $_->{description},
					icon => $_->{icon},
					function => $_->{function}
				};
		}
		$appslang = $LocaleCache{$lang} = $apps;
	}

    return $appslang;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project

=head1 SEE ALSO

Mioga2 Mioga2::GroupList Mioga2::TeamList Mioga2::ApplicationList Mioga2::InstanceList

=head1 COPYRIGHT

Copyright (C) 2011, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;

