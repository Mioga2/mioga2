# ============================================================================
# Mioga2 Project (C) 2006-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Booking.pm : The Mioga2 Booking application.

=head1 METHODS DESCRIPTION

=cut

# ============================================================================

package Mioga2::Booking;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'booking';

use Error qw(:try);
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::Organizer;
use Mioga2::Classes::Time;
use POSIX;
use MIME::Entity;
use XML::Atom::SimpleFeed;
use Data::Dumper;

my $debug = 0;

# Constants
use constant PLANNED    => "planned";
use constant PERIODIC   => "periodic";

# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc
{
	my $self = shift;
    
    my %AppDesc = ( ident   => 'Booking',
	    	name	=> __('Booking'),
                package => 'Mioga2::Booking',
                description => __('The Mioga2 Booking application'),
				type    => 'normal',
                all_groups => 1,
                all_users  => 0,
                can_be_public => 1,
                is_user             => 0,
                is_group            => 1,
                is_resource         => 0,

				functions => {
					'Booking'	=> __('Booking methods'),
				},

				func_methods => {
					'Booking'  => [ 'DisplayMain', 'CreateSimplePeriodicTask', 'EditSimplePeriodicTask', 'ViewBooking', 'UpdateBooking', 'CreateStrictOrPeriodicTask', 'EditStrictTask' ],
				},

				public_methods => [
							'DisplayMain',
							'ViewBooking'
						],
                
				sxml_methods => { },
				
				xml_methods  => { },				
				
				non_sensitive_methods => [
					'DisplayMain',
					'CreateSimplePeriodicTask',
					'EditSimplePeriodicTask',
					'ViewBooking',
					'UpdateBooking',
					'CreateStrictOrPeriodicTask',
					'EditStrictTask',
				]
                );
    
	return \%AppDesc;
}
            
# ============================================================================
# Public Methods
# ============================================================================

# ============================================================================

=head2 DisplayMain ()

=over

=item Main method.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayMain {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayMain>";
	$xml .= $context->GetXML;
    $xml .= $self->GetBookingToValidate($context);
    $xml .= $self->GetUserBookings($context);
    $xml .= $self->GetResources($context);
    
    $xml .= "</DisplayMain>";
	
	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'booking.xsl', locale_domain => 'booking_xsl');
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
	return $content;
}

# ============================================================================

=head2 ViewBooking ()

=over

=item View details of a book to validate or decline it.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub ViewBooking {
    my ($self, $context) = @_;
    
    my $book_id = int($context->{args}->{rowid});
    
    throw Mioga2::Exception::Application("Booking::ViewBooking", __"'rowid' wasn't provided or wasn't a numerical value!") unless ($book_id);
    
	my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    my $task = $self->DBGetTask($context, task_id => $book_id);
	if ($task) {
		$xml .= "<ViewBooking>";
		$xml .= $context->GetXML;
		
		$xml .= $self->XMLGetTask($context, task => $task);
		
		$xml .= "</ViewBooking>";
	}
	else {
		$xml .= "<NoSuchBooking>";
		$xml .= $context->GetXML;
		$xml .= "</NoSuchBooking>";
	}
	
	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'booking.xsl', locale_domain => 'booking_xsl');
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
	return $content;
}

# ============================================================================

=head2 UpdateBooking ()

=over

=item update booking (delete, validate, decline).

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub UpdateBooking {
    my ($self, $context) = @_;
    my $user_id = $context->GetUser()->GetRowid();
    my $task_id = int($context->{args}->{rowid});
    
    throw Mioga2::Exception::Application("Booking::UpdateBooking", __"'rowid' wasn't provided or wasn't a numerical value!") unless ($task_id);
    
    my $task     = $self->DBGetTask($context, task_id => $task_id);
    my $resource = new Mioga2::Old::Resource($context->GetConfig(), rowid => $task->{group_id});
    
    if ($context->{args}->{action} eq "decline" and $resource->GetAnimId() == $user_id) {
        $self->SendMailTo($context, msg => "decline", task_id => $task_id);
        $self->DBDeleteBooking($context, rowid => $task_id);
    }
    elsif ($context->{args}->{action} eq "delete" and $task->{user_id} == $user_id) {
        $self->DBDeleteBooking($context, rowid => $task_id);
    }
    elsif ($context->{args}->{action} eq "validate" and $resource->GetAnimId() == $user_id) {
        $self->SendMailTo($context, msg => "validate", task_id => $task_id);
        $self->DBValidateBooking($context, rowid => $task_id);
    }
    $self->NotifyUpdate($context, type => ['RSS']);
    
	return $self->DisplayMain($context);
}

# ============================================================================

=head2 DBGetTask ()

=over

=item getting task in database.

=item B<parameters :>

=over

=item I<$param> : param desc.

=back

=item B<return :> return value.

=back

=cut

# ============================================================================

sub DBGetTask {
    my ($self, $context, %options) = @_;
    
    my $dbh     = $context->GetConfig()->GetDBH();
    my $task_id = $options{task_id};
    
	my $result = undef;
    
    my $task_type   = SelectSingle($dbh, "SELECT org_task_type.ident AS task_type FROM org_task, org_task_type WHERE org_task.rowid=$task_id AND org_task_type.rowid=org_task.type_id");
	if ($task_type) {
		$task_type      = $task_type->{task_type};

		$result = SelectSingle($dbh, "SELECT org_${task_type}_task.*, task_category.name AS category, m_resource.ident AS resource, (m_user_base.firstname||' '||m_user_base.lastname) AS user_name FROM org_${task_type}_task, task_category, m_resource, m_user_base WHERE org_${task_type}_task.rowid=$task_id AND m_user_base.rowid=org_${task_type}_task.user_id AND m_resource.rowid=org_${task_type}_task.group_id AND task_category.rowid=org_${task_type}_task.category_id");
		
		$result->{task_type} = $task_type;
	}
    
    return $result;
}

# ============================================================================

=head2 CreateSimplePeriodicTask ()

=over

=item Ask for a booking on a resource.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub CreateSimplePeriodicTask {
    my ($self, $context) = @_;
    my $res_id = int($context->{args}->{resource_id});
    
    throw Mioga2::Exception::Application("Booking::CreateSimplePerdiodicTask", __"'resource_id' wasn't provided or wasn't a numerical value!") unless ($res_id or st_ArgExists($context, 'create'));
    
    $context->{args}->{mode}    = "booking";
    $context->{args}->{res_id}  = $res_id;
    $context->{args}->{referer} = "DisplayMain";
    
    my $organizer = new Mioga2::Organizer($context->GetConfig());
	my $content = $organizer->CreateSimplePeriodicTask($context);
    $self->NotifyUpdate($context, type => ['RSS']);

	return ($content);
}

# ============================================================================

=head2 CreateStrictOrPeriodicTask ()

=over

=item Advanced form for a task.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub CreateStrictOrPeriodicTask {
    my ($self, $context) = @_;
    
    $context->{args}->{startHour}   = 9;
    $context->{args}->{startMin}    = 0;
    $context->{args}->{stopHour}    = $context->{args}->{startHour} + 1;
    $context->{args}->{stopMin}     = 0;
    
    my $organizer = new Mioga2::Organizer($context->GetConfig());
    my $content = $organizer->CreateStrictOrPeriodicTask($context);
    $self->NotifyUpdate($context, type => ['RSS']);

	return ($content);
}

# ============================================================================

=head2 EditSimplePeriodicTask ()

=over

=item Edit a booking.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub EditSimplePeriodicTask {
    my ($self, $context) = @_;
    my $res_id = int($context->{args}->{resource_id});
    
    throw Mioga2::Exception::Application("Booking::EditSimplePerdiodicTask", __"'resource_id' wasn't provided or wasn't a numerical value!") unless ($res_id or st_ArgExists($context, 'change'));
    
    if (not st_ArgExists($context, 'change')) {
        $context->{args}->{resource_id} = $res_id;
        $context->{args}->{mode}        = "booking";
        $context->{args}->{referer}     = "DisplayMain";
    }
    
    my $organizer = new Mioga2::Organizer($context->GetConfig());
	my $content = $organizer->EditSimplePeriodicTask($context);
    $self->NotifyUpdate($context, type => ['RSS']);

	return ($content);
}

# ============================================================================

=head2 EditStrictTask ()

=over

=item edit a strict task.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub EditStrictTask {
    my ($self, $context) = @_;
    
    my $res_id = int($context->{args}->{resource_id});
    
    throw Mioga2::Exception::Application("Booking::EditSimplePerdiodicTask", __"'resource_id' wasn't provided or wasn't a numerical value!") unless ($res_id or st_ArgExists($context, 'change'));
    
    if (not st_ArgExists($context, 'change')) {
        $context->{args}->{resource_id} = $res_id;
        $context->{args}->{mode}        = "initial";
        $context->{args}->{referer}     = "DisplayMain";
    }
    
    my $organizer = new Mioga2::Organizer($context->GetConfig());
	my $content = $organizer->EditStrictTask($context);
    $self->NotifyUpdate($context, type => ['RSS']);

	return ($content);
}

# ============================================================================

=head2 GetUserAskedBookings ()

=over

=item Retrieve bookings for an user.

=item B<return :> xml representation of bookings as a string.

=back

=cut

# ============================================================================

sub GetUserBookings {
    my ($self, $context) = @_;
    
    my $xml         = "<UserBookings>";
    my $bookings    = $self->DBGetUserBookings($context);
    $xml           .= $self->XMLGetBookings($context, bookings => $bookings);
    $xml           .= "</UserBookings>";

    return $xml;
}

# ============================================================================

=head2 GetResources ()

=over

=item Retrieve resources as xml string.

=item B<return :> xml representation of resources as a string.

=back

=cut

# ============================================================================

sub GetResources {
    my ($self, $context) = @_;
    
    my $xml         = "<Resources>";
    my $resources   = $self->DBGetResources($context);
    
    foreach my $res (@$resources) {
    	$xml .= "<Resource id=\"$res->{rowid}\" owner=\"" . st_FormatXMLString($res->{owner}) ."\">" . st_FormatXMLString($res->{ident}) . "</Resource>";
    }
    $xml .= "</Resources>";

    return $xml;
}

# ============================================================================

=head2 GetBookingToValidate ()

=over

=item Retrieve booking waiting for validation.

=item B<return :> xml representation of articles as a string.

=back

=cut

# ============================================================================

sub GetBookingToValidate {
    my ($self, $context) = @_;
    
    my $xml = "<BookingsToValidate>";
    my $bookings = $self->DBGetBookingsToValidate($context);
    $xml .= $self->XMLGetBookings($context, bookings => $bookings);
    $xml .= "</BookingsToValidate>";
    return $xml;
}

# ============================================================================

=head2 XMLGetBookings (I<$context>, I<%options>)

=over

=item Retrieve a list of booking in xml.

=item B<parameters :>

=over

=item I<$context>: current request context.

=item I<$options{bookings}>: booking list from database

=back

=item B<return :> bookings as a xml string.

=back

=cut

# ============================================================================

sub XMLGetBookings {
    my ($self, $context, %options) = @_;
    my $bookings    = $options{bookings};
    my $user        = $context->GetUser();
    
    my $xml;
    foreach my $booking (@$bookings) {
    	$xml .= "<Booking id=\"$booking->{rowid}\" type=\"$booking->{task_type}\">";
        $xml .= "<Resource id=\"$booking->{res_id}\">" . st_FormatXMLString($booking->{res_ident}) . "</Resource>";
        if ($booking->{task_type} eq PERIODIC) {
            $xml .= "<Start>" . strftime(st_FormatXMLString(__"%x"), localtime(du_ISOToSecond($booking->{start}))) . "</Start>";
            $xml .= "<Stop>" . strftime(st_FormatXMLString(__"%x"), localtime(du_ISOToSecond($booking->{stop}))) . "</Stop>";
        }
        else {
            $xml .= "<Start>" . strftime(st_FormatXMLString(__"%x at %X"), localtime(du_ISOToSecond(du_ConvertGMTISOToLocale($booking->{start}, $user)))) . "</Start>";
            $xml .= "<Stop>" . strftime(st_FormatXMLString(__"%x at %X"), localtime(du_ISOToSecond(du_ConvertGMTISOToLocale($booking->{start}, $user)) + $booking->{duration})) . "</Stop>";
        }
        $xml .= "</Booking>";
    }
    return $xml;
}

# ============================================================================

=head2 XMLGetTask (I<$context>, I<%options>)

=over

=item Retrieve information about a task in xml.

=item B<parameters :>

=over

=item I<$context>: current request context.

=item I<$options{task}>: task object from database

=back

=item B<return :> task as a xml string.

=back

=cut

# ============================================================================

sub XMLGetTask {
    my ($self, $context, %options) = @_;
    my $task    = $options{task};
    my $user    = $context->GetUser();
    
    my $xml;

    $xml .= "<Task id=\"$task->{rowid}\" type=\"$task->{task_type}\">";
    if ($task->{task_type} eq PERIODIC) {
        $xml .= "<Start>" . strftime(st_FormatXMLString(__"%x"), localtime(du_ISOToSecond($task->{start}))) . "</Start>";
        $xml .= "<Stop>" . strftime(st_FormatXMLString(__"%x"), localtime(du_ISOToSecond($task->{stop}))) . "</Stop>";
    }
    else {
        $xml .= "<Start>" . strftime(st_FormatXMLString(__"%x at %X"), gmtime(du_ISOToSecond(du_ConvertGMTISOToLocale($task->{start}, $user)))) . "</Start>";
        $xml .= "<Stop>" . strftime(st_FormatXMLString(__"%x at %X"), gmtime(du_ISOToSecond(du_ConvertGMTISOToLocale($task->{start}, $user)) + $task->{duration})) . "</Stop>";
    }
    
    $xml .= "<Category>" . st_FormatXMLString($task->{category}) . "</Category>";
    $xml .= "<Name>" . st_FormatXMLString($task->{user_name}) . "</Name>";
    $xml .= "<Description>" . st_FormatXMLString($task->{description}) . "</Description>";
    $xml .= "<Resource>" . st_FormatXMLString($task->{resource}) . "</Resource>";
    $xml .= "</Task>";
    return $xml;
}

# ============================================================================

=head2 DBGetUserAskedBookings (I<$context>)

=over

=item Database method to retrieve bookings asked by current user.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> list of hashes containing data.

=back

=cut

# ============================================================================

sub DBGetUserBookings {
    my ($self, $context) = @_;
    
    my $user_id         = $context->GetUser()->GetRowid();
    my $resources       = $context->GetGroup()->GetInvitedResources();
    my $resources_ids   = join(", ", @{$resources->GetRowids()});
    return [] unless ($resources_ids);
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $periodic_sql    = "SELECT org_periodic_task.*, org_task_type.ident AS task_type, m_resource.ident AS res_ident, m_resource.rowid AS res_id FROM org_task_type, org_periodic_task, m_resource WHERE org_periodic_task.group_id=m_resource.rowid AND org_periodic_task.waiting_book=true AND org_task_type.rowid=org_periodic_task.type_id AND m_resource.rowid IN ($resources_ids) AND org_periodic_task.user_id=$user_id AND org_task_type.ident='".PERIODIC."' ORDER BY org_periodic_task.rowid DESC";
    
    my $planned_sql     = "SELECT org_planned_task.*, org_task_type.ident AS task_type, m_resource.ident AS res_ident, m_resource.rowid AS res_id FROM org_task_type, org_planned_task, m_resource WHERE org_planned_task.group_id=m_resource.rowid AND org_planned_task.waiting_book=true AND org_task_type.rowid=org_planned_task.type_id AND m_resource.rowid IN ($resources_ids) AND org_planned_task.user_id=$user_id AND org_task_type.ident='".PLANNED."' ORDER BY org_planned_task.rowid DESC";
    
    my $planned_res = SelectMultiple($dbh, $planned_sql);
    my $results     = SelectMultiple($dbh, $periodic_sql);
    
    push @$results, @$planned_res;
    
    return $results;
}

# ============================================================================

=head2 DBGetBookingsToValidate (I<$context>)

=over

=item Database method to retrieve bookings waiting validation from animator

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> list of hashes containing data.

=back

=cut

# ============================================================================

sub DBGetBookingsToValidate {
    my ($self, $context) = @_;
    
    my $user_id         = $context->GetUser()->GetRowid();
    my $resources       = $context->GetGroup()->GetInvitedResources();
    my $resources_ids   = join(", ", @{$resources->GetRowids()});
    my $dbh             = $context->GetConfig()->GetDBH();
    
    return [] unless ($resources_ids);
    
    my $periodic_sql = "SELECT org_periodic_task.*, org_task_type.ident AS task_type, m_resource.ident AS res_ident, m_resource.rowid AS res_id FROM org_task_type, org_periodic_task, m_resource WHERE org_periodic_task.group_id=m_resource.rowid AND org_periodic_task.waiting_book=true AND org_task_type.rowid=org_periodic_task.type_id AND m_resource.rowid IN ($resources_ids) AND m_resource.anim_id=$user_id AND org_task_type.ident='".PERIODIC."' ORDER BY res_ident";
    
    my $planned_sql = "SELECT org_planned_task.*, org_task_type.ident AS task_type, m_resource.ident AS res_ident, m_resource.rowid AS res_id FROM org_task_type, org_planned_task, m_resource WHERE org_planned_task.group_id=m_resource.rowid AND org_planned_task.waiting_book=true AND org_task_type.rowid=org_planned_task.type_id AND m_resource.rowid IN ($resources_ids) AND m_resource.anim_id=$user_id AND org_task_type.ident='".PLANNED."' ORDER BY res_ident";
    
    my $planned_res = SelectMultiple($dbh, $planned_sql);
    my $results     = SelectMultiple($dbh, $periodic_sql);
    
    push @$results, @$planned_res;
    
    return $results;
}

# ============================================================================

=head2 DBValidateBooking (I<$context>, I<%options>)

=over

=item Database method to validate a booking

=item B<parameters :>

=over

=item I<$context>: context of current request.

=item I<$options{rowid}>: id of the booking to validate.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBValidateBooking {
    my ($self, $context, %options) = @_;
    
    my $user_id         = $context->GetUser()->GetRowid();
    my $dbh             = $context->GetConfig()->GetDBH();
    my $task_id         = $options{rowid};
    my $user_name       = SelectSingle($dbh, "SELECT (m_user_base.firstname||' '||m_user_base.lastname) AS user_name FROM m_user_base, org_task WHERE org_task.rowid=$task_id AND m_user_base.rowid=org_task.user_id");
    
    my $name            = st_FormatPostgreSQLString(__x("Booked by {user}", user => $user_name->{user_name}));
    
    my $sql = "UPDATE org_task SET name='$name', waiting_book=false WHERE rowid=$task_id";
    ExecSQL($dbh, $sql);
}

# ============================================================================

=head2 DBDeleteBooking (I<$context>, I<%options>)

=over

=item Database method to delete a booking

=item B<parameters :>

=over

=item I<$context>: context of current request.

=item I<$options{rowid}>: id of the booking to delete.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBDeleteBooking {
    my ($self, $context, %options) = @_;
    
    my $user_id         = $context->GetUser()->GetRowid();
    my $dbh             = $context->GetConfig()->GetDBH();
    my $task_id         = $options{rowid};
    
    my $sql = "DELETE FROM org_task WHERE rowid=$task_id";
    ExecSQL($dbh, $sql);
}

# ============================================================================

=head2 DBGetResources (I<$context>)

=over

=item Retrieve infos about resources for the current group

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> hash containing data.

=back

=cut

# ============================================================================

sub DBGetResources {
    my ($self, $context) = @_;
    my $user_id          = $context->GetUser()->GetRowid();
    my $resources        = $context->GetGroup()->GetInvitedResources();
    my $resources_ids    = join(", ", @{$resources->GetRowids()});
    my $dbh              = $context->GetConfig()->GetDBH();
    return [] unless ($resources_ids);
    
    my $results = SelectMultiple($dbh, "SELECT m_resource.*, (m_user_base.firstname||' '||m_user_base.lastname) AS owner FROM m_resource, m_user_base WHERE m_resource.rowid IN ($resources_ids) AND m_user_base.rowid=m_resource.anim_id");
    
    return $results;
}

# ============================================================================

=head2 SendMailTo (I<$context>, I<%options>)

=over

=item Send a prewritten mail to user.

=item B<parameters :>

=over

=item I<$context> : request context.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub SendMailTo {
    my ($self, $context, %options) = @_;
    my $msg_id  = $options{msg};
    my $task_id = $options{task_id};
    
    my ($message, $subject);
    my $user    = $context->GetUser();
    my $sender  = $user->GetName() . " <" . $user->GetEmail() . ">";
    my $config  = $context->GetConfig();
    my $group   = $context->GetGroup();
    
    my $task      = $self->DBGetTask($context, task_id => $task_id);
    my $recipient = new Mioga2::Old::User($config, rowid => $task->{user_id});
    
    ## shared messages
    my $automatic   = __"This is an automatic message please do not answer.\n";
    
    if ($msg_id eq "decline") {
        $subject = __"Your booking has been declined"; 
        $message = $automatic . __x("Your booking for resource \"{resource}\" in group {group} has been declined.", resource => $task->{resource}, group => $group->GetIdent);
    }
    elsif ($msg_id eq "validate") {
        $subject = __"Your booking has been approved"; 
        $message = $automatic . __x("Your booking for resource \"{resource}\" in group {group} has been approved.", resource => $task->{resource}, group => $group->GetIdent);
    }
    my $entity = MIME::Entity->build(From   => $sender,
                                    Subject => $subject,
                                    Data    => $message,
                                    Charset => "UTF-8");
    $recipient->SendMail($entity);
}

# ============================================================================

=head2 GetRSSFeed ($context, [$feed])

Return a feed of pending bookings.

=cut

# ============================================================================

sub GetRSSFeed {
    my ($self, $context, $ident, $feed, $max)   = @_;
	print STDERR "[Mioga2::Booking::GetRSSFeed] Entering\n" if ($debug);

    my $config  = $context->GetConfig;
    my $group;
    try {
        $group = Mioga2::Old::Group->new($config, ident => $ident);
    }
    otherwise {
        $group = Mioga2::Old::User->new($config, ident => $ident);
    };

    if (!$feed) {
        $feed   = XML::Atom::SimpleFeed->new(
            title   => __x("Pending bookings feed of {group} group", group => $group->GetIdent),
            link    => { href => $config->GetProtocol . "://" . $config->GetServerName . $context->{uri}->GetURI, rel => 'self' },
            link    => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/Booking/DisplayMain",
            id      => "data:,group:".$group->GetIdent.":booking",
        );
    }
    $max    = 20 unless $max;
    
	my $bookings = $self->DBGetBookingsToValidate ($context);
    foreach my $booking (@$bookings) {
		my $user = Mioga2::Old::User->new ($config, rowid => $booking->{user_id});
		my $user_label = $user->GetFirstname () . " " . $user->GetLastname () . " (" . $user->GetEmail () . ")";
        $feed->add_entry(
            title       => __x("[{group}] Booking for resource: {resource}", resource => $booking->{res_ident}, group => $group->GetIdent),
            summary     => __x("{user_label} requires a booking approval for resource {resource}.", user_label => $user_label, resource => $booking->{res_ident}),
            link        => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/Booking/ViewBooking?rowid=$booking->{rowid}",
            id          => "data:,group:".$group->GetIdent.":booking:$booking->{rowid}",
			author      => $user_label,
            published   => Mioga2::Classes::Time->FromPSQL($booking->{created})->RFC3339DateTime,
            updated     => Mioga2::Classes::Time->FromPSQL($booking->{modified})->RFC3339DateTime,
        );
    }

	print STDERR "[Mioga2::Booking::GetRSSFeed] Leaving\n" if ($debug);
    return $feed;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2006-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
