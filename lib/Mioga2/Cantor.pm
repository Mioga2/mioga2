#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Cantor.pm: the Mioga2 Cantor application

=head1 DESCRIPTION

The Mioga2::Cantor application allows to manage the organization meeting by
presenting a selection of possible dates.

The goal of this application is to create a survey available for the internal
users of Mioga2 instances (with the possibility of integrating external users) in
order to get a list of possible dates. 

=head1 METHODS DESCRIPTION

=cut

#
# ============================================================================

package Mioga2::Cantor;
use base qw(Mioga2::Application);
use strict;
use warnings;
use open ':encoding(utf8)';

use Data::Dumper;
use Error qw(:try);
use Digest::MD5 qw(md5_hex);

use Locale::TextDomain::UTF8 'cantor';
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::Content::XSLT;
use Mioga2::Exception::Simple;

my $debug = 0;


#===============================================================================

=head1 INTERFACE METHODS

=cut

#===============================================================================

=head2 DisplayMain

Display Portal

=head3 Generated XML

=over

	<DisplayMain>
		<miogacontext>See Mioga2::RequestContext</miogacontext>
	</DisplayMain>

=back

=cut

# ============================================================================
sub DisplayMain {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Cantor::DisplayMain]\n" if ($debug);

	my $data = { DisplayMain => $context->GetContext () };

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "[Mioga2::Cantor::DisplayMain] XML: $xml\n" if ($debug);

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'cantor.xsl', locale_domain => 'cantor_xsl');
	$content->SetContent($xml);
	return $content;
}
# ============================================================================


#===============================================================================

=head1 WEBSERVICE METHODS

=cut

#===============================================================================


#===============================================================================

=head2 GetActiveSurveys

Get the list of currently-active surveys, it is called at the application start
when displaying the full list of surveys.

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		success: 'true' / 'false',
		message: '...', # if error
		surveys: [
			{
				rowid: ...,
				title: ...,
				description: ...,
				modified: ...,	# Date
				progress: ...	# Percent
			},
			...
		]
	}

=back

=cut

#===============================================================================
sub GetActiveSurveys {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Cantor::GetActiveSurveys] Entering\n" if ($debug);

	# Default return value
	my $data = {
		success => 'boolean::false',
		message => __('Failed to get active surveys.'),
		surveys => []
	};

	try {
		# Get survey attributes and progress (count of attendees who have answered / total count of attendees)
		$data->{surveys} = $self->{db}->SelectMultiple ('SELECT survey.rowid, survey.title, survey.description, survey.modified, COALESCE (100 * T1.answers / T2.attendees, 0) AS progress FROM survey LEFT JOIN (SELECT survey.rowid, count(DISTINCT survey_answer.attendee_id) AS answers FROM survey, survey_attendee, survey_answer WHERE survey_attendee.survey_id = survey.rowid AND survey_answer.attendee_id = survey_attendee.rowid GROUP BY survey.rowid) AS T1 ON T1.rowid = survey.rowid LEFT JOIN (SELECT survey.rowid, count(survey_attendee.rowid) AS attendees FROM survey, survey_attendee WHERE survey_attendee.survey_id = survey.rowid GROUP BY survey.rowid) AS T2 ON T2.rowid = survey.rowid WHERE survey.owner_id = ? ORDER BY modified DESC;', [$self->{user}->Get ('rowid')]);
		delete ($data->{message});
		$data->{success} = 'boolean::true';
	}
	otherwise {
		my $err = shift;
		print STDERR '[Mioga2::Cantor::GetActiveSurveys] Fetch failed: ' . $err->stringify ();
	};

	print STDERR "[Mioga2::Cantor::GetActiveSurveys] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetActiveSurveys  ----------



#===============================================================================

=head2 GetSurvey

Get survey details, called when you click on a survey.

=head3 Incoming Arguments

=over

=item I<rowid>: The survey rowid

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		success: 'true' / 'false',
		message: '...', # if error
		survey: {
			rowid: ...,
			title: ...,
			description: ...,
			attendee_id: ...,
			attendees: [
				{
					dates: {},
					email: ...,
					firstname: ...,
					fullname: ...,
					lastname: ...,
					rowid: ...,
					user_id: ...
				},
				...
			],
			created: ...,	# Date
			dates: ...,	# Possibles dates
			duration: ..., # Survey duration in minutes
			is_mine: 'true' / 'false',
			open_attendee_list: 'true' / 'false',
			owner_id: ...,
			ressources: [
				{
					dates: ...,
					ident: ..., # The ressource name
					rowid: ...
				}
			],
			short_url: ... # Short link for quick access
		}
	}

=back

=cut

# ============================================================================
sub GetSurvey {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Cantor::GetSurvey] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default return value
	my $data = {
		success => 'boolean::false',
		message => __('Failed to get survey.')
	};

	if (!@$errors) {
		try {
			$data->{survey} = $self->{db}->SelectSingle ('SELECT rowid, title, description, duration, open_attendee_list, short_url_id, owner_id, created FROM survey WHERE rowid = ?;', [$values->{rowid}]);

			# Add 'is_mine' field to survey
			if ((!$context->IsPublic ()) && ($self->{user} && ($self->{user}->Get ('rowid') == $data->{survey}->{owner_id}))) {
				$data->{survey}->{is_mine} = 'boolean::true';
			}
			else {
				$data->{survey}->{is_mine} = 'boolean::false';
			}

			# Convert short_url_id to full URL
			if ($data->{survey}->{short_url_id}) {
				my $res = $self->{db}->SelectSingle ('SELECT hash FROM short_url WHERE rowid = ?', [delete ($data->{survey}->{short_url_id})]);
				if ($res && $res->{hash}) {
					$data->{survey}->{short_url} = $self->{config}->GetProtocol () . '://' . $self->{config}->GetServerName () . $self->{config}->GetMiogaConf ()->GetBasePath () . $self->{config}->GetMiogaConf ()->GetInternalURI () . '/url/' . $res->{hash};
				}
			}

			# Convert boolean values to 'boolean::' equivalent
			$data->{survey}->{open_attendee_list} = ($data->{survey}->{open_attendee_list} == 1) ? 'boolean::true' : 'boolean::false';

			# Add connected Mioga user's attendee_id
			if (defined ($self->{user})) {
				my $res = $self->{db}->SelectSingle ('SELECT rowid FROM survey_attendee WHERE survey_id = ? AND user_id = ?;', [$values->{rowid}, $self->{user}->Get ('rowid')]);
				if ($res) {
					$data->{survey}->{attendee_id} = $res->{rowid};
				}
			}

			# Attendees
			$data->{survey}->{attendees} = $self->{db}->SelectMultiple ('SELECT survey_attendee.rowid AS rowid, m_user_base.rowid AS user_id, m_user_base.firstname, m_user_base.lastname, NULL AS fullname, m_user_base.email FROM m_user_base, survey_attendee WHERE survey_attendee.user_id = m_user_base.rowid AND survey_attendee.survey_id = ? UNION SELECT survey_attendee.rowid AS attendee_id, NULL AS rowid, NULL AS firstname, NULL AS lastname, survey_attendee.fullname, survey_attendee.email FROM survey_attendee WHERE survey_attendee.user_id IS NULL AND survey_attendee.survey_id = ?;', [$values->{rowid}, $values->{rowid}]);
			# Get dates from answers
			for my $index (0..(@{ac_ForceArray ($data->{survey}->{attendees})} - 1)) {
				my $attendee_id = $data->{survey}->{attendees}->[$index]->{rowid};
				my %dates = map { $_->{date} => 'boolean::true' } @{ac_ForceArray ($self->{db}->SelectMultiple ('SELECT survey_date.date FROM survey_date, survey_answer WHERE survey_date.rowid = survey_answer.date_id AND survey_answer.attendee_id = ?;', [$attendee_id]))};
				$data->{survey}->{attendees}->[$index]->{dates} = \%dates;
			}

			# Dates
			@{$data->{survey}->{dates}} = map { $_->{date} } @{$self->{db}->SelectMultiple ('SELECT date FROM survey_date WHERE survey_id = ?;', [$values->{rowid}])};

			# Resources
			$data->{survey}->{resources} = $self->{db}->SelectMultiple ('SELECT m_resource.rowid, m_resource.ident FROM m_resource, survey_resource WHERE survey_resource.resource_id = m_resource.rowid AND survey_resource.survey_id = ?;', [$values->{rowid}]);
			for my $resource (@{ac_ForceArray ($data->{survey}->{resources})}) {
				for my $date (@{ac_ForceArray ($data->{survey}->{dates})}) {
					$self->GetResourceAvailability ($resource, $date, $data->{survey}->{duration});
				}
			}

			# Message and status
			delete ($data->{message});
			$data->{success} = 'boolean::true';
		}
		otherwise {
			my $err = shift;
			print STDERR '[Mioga2::Cantor::GetActiveSurveys] Fetch failed: ' . $err->stringify ();
			delete ($data->{survey});
		};
	}
	else {
		print STDERR '[Mioga2::Cantor::GetSurvey] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			});
		}
	}

	print STDERR "[Mioga2::Cantor::GetSurvey] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}


#===============================================================================

=head2 SetSurvey

Store a survey, called after filling the form and clicking "Save survey".
It is also used for modifying a survey.

=head3 Incoming Arguments

=over

=item I<rowid>: The survey rowid, only if modifying (optional for creation).

=item I<title>: The survey title.

=item I<description>: The survey description.

=item I<duration>: The meeting duration.

=item I<open_attendee_list>: The survey attendee list openness.

=item I<dates>: The meeting possible dates.

=item I<attendees>: The meeting attendees list.

=item I<resources>: The meeting resources list.

=item I<short_url>: The short URL linking to the survey, optional.

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		success: true / false,
		message: ...,
		survey: {
			# See output of GetSurvey
		}
	}

=back

=cut

#===============================================================================
sub SetSurvey {
	my ($self, $context) = @_;
	print STDERR '[Mioga2::Cantor::SetSurvey] Entering, args: ' . Dumper $context->{args} if ($debug);

	my $config = $context->GetConfig ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'allow_empty', 'want_int', 'owned_survey' ],
			[ [ 'title' ], 'disallow_empty', 'stripxws' ],
			[ [ 'description' ], 'allow_empty', 'stripxws' ],
			[ [ 'duration' ], 'disallow_empty', 'want_int' ],
			[ [ 'open_attendee_list' ], 'disallow_empty', ['match', "^\(true|false\)"] ],
			[ [ 'attendees', 'resources', 'dates' ], 'allow_empty' ],
			[ [ 'short_url' ], 'allow_empty' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default return value
	my $data = {
		success => 'boolean::false',
		message => __('Failed to store survey.')
	};

	if (!@$errors) {
		try {
			$self->{db}->BeginTransaction ();

			if ($values->{rowid}) {
				# Update existing survey
				$self->{db}->ExecSQL ('UPDATE survey SET title=?, description=?, duration=?, open_attendee_list=?, modified=now () WHERE rowid = ? AND owner_id = ?;', [$values->{title}, $values->{description}, $values->{duration}, $values->{open_attendee_list}, $values->{rowid}, $self->{user}->Get ('rowid')]);

				# Remove short URL if required
				if (!st_ArgExists ($context, 'short_url')) {
					my $res = $self->{db}->SelectSingle ('SELECT short_url_id FROM survey WHERE rowid = ?;', [$values->{rowid}]);
					if ($res && $res->{short_url_id}) {
						$self->{db}->ExecSQL ('UPDATE survey SET short_url_id = NULL WHERE rowid = ?;', [$values->{rowid}]);
						$self->{db}->ExecSQL ('DELETE FROM short_url WHERE rowid = ?;', [$res->{short_url_id}]);
					}
				}
				elsif ($values->{short_url} eq '') {
					# Generate a short URL for a survey that doesn't have one
					# Create short URL
					my $url = $self->{config}->GetPubBinURI () . '/' . $self->{user}->Get ('ident') . '/Cantor/DisplayMain#Cantor-survey-' . $values->{rowid};
					my $hash = md5_hex ($url);
					$self->{db}->ExecSQL ('INSERT INTO short_url (hash, target) VALUES (?, ?);', [$hash, $url]);
					my $url_id = $self->{db}->GetLastInsertId ('short_url');

					# Link short URL to survey
					$self->{db}->ExecSQL ('UPDATE survey SET short_url_id = ? WHERE rowid = ?;', [$url_id, $values->{rowid}]);
				}
			}
			else {
				# Create survey
				$self->{db}->ExecSQL ('INSERT INTO survey (title, description, duration, open_attendee_list, owner_id, created, modified) VALUES (?, ?, ?, ?, ?, now (), now ())', [$values->{title}, $values->{description}, $values->{duration}, $values->{open_attendee_list}, $self->{user}->Get ('rowid')]);

				# Push rowid to args for GetSurvey
				$values->{rowid} = $self->{db}->GetLastInsertId ('survey');
				$context->{args}->{rowid} = $values->{rowid};

				# Create short URL
				my $url = $self->{config}->GetPubBinURI () . '/' . $self->{user}->Get ('ident') . '/Cantor/DisplayMain#Cantor-survey-' . $values->{rowid};
				my $hash = md5_hex ($url);
				$self->{db}->ExecSQL ('INSERT INTO short_url (hash, target) VALUES (?, ?);', [$hash, $url]);
				my $url_id = $self->{db}->GetLastInsertId ('short_url');

				# Link short URL to survey
				$self->{db}->ExecSQL ('UPDATE survey SET short_url_id = ? WHERE rowid = ?;', [$url_id, $values->{rowid}]);
			}

			# Attach attendees to survey
			if ($values->{attendees}) {
				# Prepare request arguments
				# $values->{attendees} can contains:
				# 	- a numeric value, in which case it is a Mioga2 user rowid
				# 	- a non-numeric value, in which case it is an external user "fullname" and next value will be user's email
				my (@mioga_users, @external_users);

				my $current_external_user;
				for my $entry (@{ac_ForceArray ($values->{attendees})}) {
					if ($entry =~ /^[0-9]+$/) {
						push (@mioga_users, $entry);
					}
					else {
						if (!defined ($current_external_user)) {
							$current_external_user->{fullname} = $entry;
						}
						else {
							$current_external_user->{email} = $entry;
							push (@external_users, $current_external_user);
							$current_external_user = undef;
						}
					}
				}

				# Process Mioga users
				if (@mioga_users) {
					# Delete old Mioga2 users
					$self->{db}->ExecSQL ('DELETE FROM survey_answer USING survey_attendee WHERE survey_answer.attendee_id = survey_attendee.rowid AND survey_attendee.survey_id = ? AND survey_attendee.user_id IS NOT NULL AND survey_attendee.user_id NOT IN (' . join (', ', ('?') x @mioga_users) . ');', [$values->{rowid}, @mioga_users]);
					$self->{db}->ExecSQL ('DELETE FROM survey_attendee WHERE survey_id = ? AND user_id IS NOT NULL AND user_id NOT IN (' . join (', ', ('?') x @mioga_users) . ');', [$values->{rowid}, @mioga_users]);

					# Insert new Mioga2 users
					$self->{db}->ExecSQL ('INSERT INTO survey_attendee (survey_id, user_id) SELECT survey.rowid AS survey_id, m_user_base.rowid AS user_id FROM survey, m_user_base WHERE survey.rowid = ? AND m_user_base.rowid IN (' . join (', ', ('?') x @mioga_users) . ') AND m_user_base.rowid NOT IN (SELECT user_id FROM survey_attendee WHERE user_id IS NOT NULL AND survey_id = ?);', [$values->{rowid}, @mioga_users, $values->{rowid}]);
				}

				# Process external users
				if (@external_users) {
					my ($sql, @args);

					# Delete old external users
					$sql = 'DELETE FROM survey_answer USING survey_attendee WHERE survey_answer.attendee_id = survey_attendee.rowid AND survey_attendee.user_id IS NULL AND (fullname, email) NOT IN (';
					$sql .= join (' UNION ', ('SELECT ?::text AS fullname, ?::text AS email') x @external_users);
					$sql .= ') AND survey_attendee.survey_id = ?';
					@args = ( );
					for my $user (@external_users) {
						push (@args, $user->{fullname}, $user->{email});
					}
					push (@args, $values->{rowid});
					$self->{db}->ExecSQL ($sql, [@args]);
					$sql = 'DELETE FROM survey_attendee WHERE user_id IS NULL AND (fullname, email) NOT IN (';
					$sql .= join (' UNION ', ('SELECT ?::text AS fullname, ?::text AS email') x @external_users);
					$sql .= ') AND survey_id = ?';
					@args = ( );
					for my $user (@external_users) {
						push (@args, $user->{fullname}, $user->{email});
					}
					push (@args, $values->{rowid});
					$self->{db}->ExecSQL ($sql, [@args]);

					# Insert new external users
					$sql = 'INSERT INTO survey_attendee (survey_id, fullname, email) SELECT * FROM (';
					$sql .= join (' UNION ', ('SELECT ?::int AS survey_id, ?::text AS fullname, ?::text AS email') x @external_users);
					$sql .= ') AS T1 WHERE (T1.fullname, T1.email) NOT IN (SELECT fullname, email FROM survey_attendee WHERE fullname IS NOT NULL AND email IS NOT NULL AND survey_id = ?);';
					@args = ( );
					for my $user (@external_users) {
						push (@args, $values->{rowid}, $user->{fullname}, $user->{email});
					}
					push (@args, $values->{rowid});
					$self->{db}->ExecSQL ($sql, [@args]);
				}
			}

			# Attach resources to survey
			if ($values->{resources}) {
				# Prepare request arguments
				my @args = ( $values->{rowid} );
				for my $rowid (@{ac_ForceArray ($values->{resources})}) {
					push (@args, $rowid);
				}

				# Delete old resources
				$self->{db}->ExecSQL ('DELETE FROM survey_resource WHERE survey_id = ? AND resource_id NOT IN (' . join (', ', ('?') x @{ac_ForceArray ($values->{resources})}) . ')', [@args]);

				# Insert new resources
				$self->{db}->ExecSQL ('INSERT INTO survey_resource SELECT survey.rowid AS survey_id, m_resource.rowid AS resource_id FROM survey, m_resource WHERE survey.rowid = ? AND m_resource.rowid IN (' . join (', ', ('?') x @{ac_ForceArray ($values->{resources})}) . ') AND m_resource.rowid NOT IN (SELECT resource_id FROM survey_resource WHERE survey_id = survey.rowid);', [@args])
			}

			# Store survey dates
			if ($values->{dates}) {
				my ($sql, @args);

				# Delete old dates
				@args = ( );
				$sql = 'DELETE FROM survey_date WHERE survey_id = ? AND date NOT IN (' . join (', ', ('?') x @{ac_ForceArray ($values->{dates})}) . ')';
				push (@args, $values->{rowid});
				for my $date (@{ac_ForceArray ($values->{dates})}) {
					push (@args, $date);
				}
				$self->{db}->ExecSQL ($sql, [@args]);

				# Insert new dates
				@args = ( );
				$sql = 'INSERT INTO survey_date (survey_id, date) SELECT * FROM (' . join (' UNION ', ('SELECT ?::int AS survey_id, ?::timestamp AS date') x @{ac_ForceArray ($values->{dates})}) . ') AS T1 WHERE (T1.survey_id, T1.date) NOT IN (SELECT survey_id, date FROM survey_date);';
				for my $date (@{ac_ForceArray ($values->{dates})}) {
					push (@args, $values->{rowid}, $date);
				}
				$self->{db}->ExecSQL ($sql, [@args]);
			}

			$self->{db}->EndTransaction ();

			# Set return value
			$data->{success} = 'boolean::true';
			$data->{message} = __('Survey successfully stored.');
			$data->{survey} = $self->GetSurvey ($context)->{survey};
		}
		otherwise {
			my $err = shift;
			print STDERR "[Mioga2::Cantor::SetSurvey] Failed: " . $err->stringify ();
			$self->{db}->RollbackTransaction ();
		};
	}
	else {
		print STDERR '[Mioga2::Cantor::SetSurvey] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	print STDERR '[Mioga2::Cantor::SetSurvey] Leaving, data: ' . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetSurvey  ----------


#===============================================================================

=head2 DeleteSurvey

Delete a survey

=head3 Incoming Arguments

=over

=item I<rowid>: The survey rowid

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		success: true / false,
		message: '...'
	}

=back

=cut

#===============================================================================
sub DeleteSurvey {
	my ($self, $context) = @_;
	print STDERR '[Mioga2::Cantor::DeleteSurvey] Entering, args: ' . Dumper $context->{args} if ($debug);

	my $config = $context->GetConfig ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int', 'owned_survey' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default return value
	my $data = {
		success => 'boolean::false',
		message => __('Failed to delete survey.')
	};

	if (!@$errors) {
		try {
			$self->{db}->BeginTransaction ();

			$self->DBDeleteSurvey ($values->{rowid});

			$self->{db}->EndTransaction ();

			# Set return value
			$data->{success} = 'boolean::true';
			$data->{message} = __('Survey successfully deleted.');
		}
		otherwise {
			my $err = shift;
			print STDERR "[Mioga2::Cantor::DeleteSurvey] Failed: " . $err->stringify ();
			$self->{db}->RollbackTransaction ();
		};
	}
	else {
		print STDERR '[Mioga2::Cantor::DeleteSurvey] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	print STDERR '[Mioga2::Cantor::DeleteSurvey] Leaving, data: ' . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DeleteSurvey  ----------


#===============================================================================

=head2 AnswerSurvey

Answer a survey

=head3 Incoming Arguments

=over

=item I<rowid>: The survey rowid

=item I<attendee_id>: The attendee rowid

=item I<dates>: The dates that are OK for the attendee

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		success: 'true' / 'false',
		message: '...',
		survey: {
			# See output of GetSurvey
		}
	}

=back

=cut

#===============================================================================
sub AnswerSurvey {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Cantor::AnswerSurvey] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	# TODO check attendee_id is correct (connected user, external user according to hash)
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int' ],
			[ [ 'attendee_id' ], 'disallow_empty', 'want_int', 'attendee_id' ],
			[ [ 'fullname', 'email' ], 'allow_empty', 'stripxws' ],
			[ [ 'dates' ], 'disallow_empty' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default return value
	my $data = {
		success => 'boolean::false',
		message => __('Failed to store survey answers.')
	};

	if (!@$errors) {
		try {
			$self->{db}->BeginTransaction ();

			# Create attendee, if required
			if ($values->{attendee_id} == 0) {
				$self->{db}->ExecSQL ('INSERT INTO survey_attendee (survey_id, fullname, email) SELECT rowid AS survey_id, ?::text AS fullname, ?::text AS email FROM survey WHERE open_attendee_list = \'t\' AND rowid = ?;',[$values->{fullname}, $values->{email}, $values->{rowid}]);
				$values->{attendee_id} = $self->{db}->GetLastInsertId ('survey_attendee');
			}

			my ($sql, $date_sql, @date_args);

			# Subrequest selecting dates for attendee
			$date_sql = 'SELECT rowid FROM survey_date WHERE survey_id = ? AND date IN (' . join (', ', ('?') x @{ac_ForceArray ($values->{dates})}) . ')';
			push (@date_args, $values->{rowid});
			push (@date_args, @{ac_ForceArray ($values->{dates})});

			# Delete dates that are not in list
			$self->{db}->ExecSQL ('DELETE FROM survey_answer WHERE attendee_id = ? AND date_id NOT IN (' . $date_sql . ');', [$values->{attendee_id}, @date_args]);

			# Insert dates for attendee
			$self->{db}->ExecSQL ('INSERT INTO survey_answer SELECT * FROM (SELECT ?::int AS attendee_id, dates.rowid AS date_id FROM (' . $date_sql . ') AS dates) AS attendee_dates WHERE (attendee_id, date_id) NOT IN (SELECT * FROM survey_answer);', [$values->{attendee_id}, @date_args]);

			$self->{db}->EndTransaction ();

			$data->{survey} = $self->GetSurvey ($context)->{survey};
			$data->{survey}->{attendee_id} = $values->{attendee_id};

			$data->{success} = 'boolean::true';
			$data->{message} = __('Survey successfully answered.');
		}
		otherwise {
			my $err = shift;
			print STDERR '[Mioga2::Cantor::AnswerSurvey] DB update failed: ' . $err->stringify ();
			$self->{db}->RollbackTransaction ();
		};
	}
	else {
		print STDERR '[Mioga2::Cantor::GetSurvey] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			});
		}
	}

	print STDERR "[Mioga2::Cantor::AnswerSurvey] Entering, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine AnswerSurvey  ----------


#===============================================================================

=head2 GetSolicitations

Get the list of surveys to be answered for the current user

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		success: 'true' / 'false',
		message: '...',
		surveys: [
			{
				rowid: ...,
				title: ...,
				description: ...
			},
			...
		]
	}

=back

=cut

#===============================================================================
sub GetSolicitations {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Cantor::GetSolicitations] Entering\n" if ($debug);

	my $data = {
		success => 'boolean::false',
		message => __('Could not fetch solicitations list')
	};

	try {
		# Get surveys to be answered by connected user, with status (true if already answered, false otherwise)
		$data->{surveys} = $self->{db}->SelectMultiple ("SELECT survey.rowid, survey.title, survey.description, CASE WHEN T1.attendee_id IS NOT NULL THEN 'boolean::true' ELSE 'boolean::false' END AS status FROM survey, survey_attendee LEFT JOIN (SELECT DISTINCT attendee_id FROM survey_answer) AS T1 ON T1.attendee_id = survey_attendee.rowid WHERE survey.owner_id != survey_attendee.user_id AND survey_attendee.survey_id = survey.rowid AND survey_attendee.user_id = ? ORDER BY status ASC, survey.created DESC;", [$self->{user}->Get ('rowid')]);

		$data->{success} = 'boolean::true';
		delete ($data->{message});
	}
	otherwise {
		my $err = shift;
		print STDERR '[Mioga2::Cantor::GetSolicitations] Fetch failed: ' . $err->stringify ();
		delete ($data->{surveys});
	};

	print STDERR "[Mioga2::Cantor::GetSolicitations] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetSolicitations  ----------


#===============================================================================

=head2 GetUsers

Get list of current instance users

=head3 Incoming Arguments

=over

=item I<max>: The max user count to be returned.

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	[
		{
			rowid: ...,
			ident: ...,
			firstname: ...,
			lastname: ...,
			email: ...
		},
		...
	]

=back

=cut

#===============================================================================
sub GetUsers {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Cantor::GetUsers] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'max' ], 'allow_empty', 'want_int' ],
			[ [ 'lastname' ], 'allow_empty', 'stripxws' ],
			[ [ 'app_name' ], 'allow_empty', [ 'match', 'Cantor' ] ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data;

	if (!@$errors) {
		my $config = $context->GetConfig ();

		# Get users
		my $userlist = Mioga2::UserList->new ($config);
		my @users = $userlist->GetUsers ();

		if ($values->{max} && (scalar (@users) <= $values->{max})) {
			$data = \@users;
		}
		elsif ($values->{max} && (scalar (@users) > $values->{max})) {
			$data = scalar (@users);
		}
		elsif ($values->{lastname}) {
			my ($lastname) = ($values->{lastname} =~ m/(.)\*/);
			if ($lastname =~ /[A-Z]/) {
				my $list = Mioga2::UserList->new ($config, { attributes => { lastname => $lastname }, match => 'begins' });
				my @users = $list->GetUsers ();
				$data = \@users;
			}
			else {
				my @users = $userlist->GetUsers ();
				@$data = grep { $_->{lastname} !~ /^[A-Z]/i } @users;
			}
		}
		else {
			$data = \@users;
		}
	}

	# Bug 1477: remove passwords
	foreach (@$data) {
		delete $_->{password};
	}

	print STDERR "[Mioga2::Cantor::GetUsers] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetUsers  ----------


#===============================================================================

=head2 GetResources

Get list of resources not being member of group

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML
	
		[
			{
				rowid: ...,
				ident: ...
			},
			...
		]

=back

=cut

#===============================================================================
sub GetResources {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Cantor::GetResources] Entering" if ($debug);

	my $data;

	# Get resources
	my $config = $context->GetConfig ();
	my $resourcelist = Mioga2::ResourceList->new ($config);
	my @resources = $resourcelist->GetResources ();

	$data = \@resources;

	print STDERR "[Mioga2::Cantor::GetResources] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetResources  ----------


#===============================================================================

=head2 GetTeams

Get list of teams not being member of group

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML
	
		[
			{
				rowid: ...,
				ident: ...
			},
			...
		]

=back

=cut

#===============================================================================
sub GetTeams {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Cantor::GetTeams] Entering" if ($debug);

	my $data;

	# Get teams
	my $config = $context->GetConfig ();
	my $teamlist = Mioga2::TeamList->new ($config, { short_list => 1 });
	my @teams = $teamlist->GetTeams ();

	# @teams = ( $teams[1] );
	for my $team (@teams) {
		my $obj = Mioga2::TeamList->new ($config, { attributes => { rowid => $team->{rowid} }, short_list => 1 });
		my @members = $obj->GetInvitedUserList ()->GetUsers ();
		$team->{members} = \@members;
	}

	$data = \@teams;

	print STDERR "[Mioga2::Cantor::GetTeams] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetTeams  ----------


#===============================================================================

=head2 SearchUser

Get list of current instance users

=head3 Incoming Arguments

=over

=item I<email>: The user email address

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		rowid: ...,
		ident: ...,
		firstname: ...,
		lastname: ...,
		email: ...
	}

=back

=cut

#===============================================================================
sub SearchUser {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Cantor::SearchUser] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'email' ], 'disallow_empty', 'want_email' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data;

	if (!@$errors) {
		my $config = $context->GetConfig ();

		# Get users
		my $userlist = Mioga2::UserList->new ($config, { attributes => { email => $values->{email} }, short_list => 1 });
		my @users = $userlist->GetUsers ();

		if (scalar (@users)) {
			$data = $users[0];
		}
	}

	print STDERR "[Mioga2::Cantor::SearchUser] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SearchUser  ----------


#===============================================================================

=head2 GetAttendeeId

Get attendee ID from email address

=head3 Incoming Arguments

=over

=item I<rowid>: The survey ID

=item I<email>: The attendee's email

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		success: true / false,
		attendee_id: ...
	}

=back

=cut

#===============================================================================
sub GetAttendeeId {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Cantor::GetAttendeeId] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int' ],
			[ [ 'email' ], 'disallow_empty', 'want_email' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default return value
	my $data = {
		success => 'boolean::false'
	};

	if (!@$errors) {
		my $res = $self->{db}->SelectSingle ('SELECT T1.rowid FROM (SELECT survey_attendee.survey_id, survey_attendee.rowid, survey_attendee.email FROM survey_attendee WHERE email IS NOT NULL UNION SELECT survey_attendee.survey_id, survey_attendee.rowid, m_user_base.email FROM survey_attendee, m_user_base WHERE m_user_base.rowid = survey_attendee.user_id) AS T1 WHERE T1.survey_id = ? AND T1.email = ?;', [$values->{rowid}, $values->{email}]);
		if ($res) {
			$data->{attendee_id} = $res->{rowid};
			$data->{success} = 'boolean::true';
		}
	}
	else {
		print STDERR '[Mioga2::Cantor::GetAttendeeId] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			});
		}
	}

	print STDERR "[Mioga2::Cantor::GetAttendeeId] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetAttendeeId  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut
# ============================================================================


#===============================================================================

=head2 InitApp

Initialize inner data for application. Not to be used, automatically called by Mioga2::Application.

=cut

#===============================================================================
sub InitApp {
	my ($self, $context) = @_;

	$self->{config} = $context->GetConfig ();
	$self->{db} = $context->GetConfig ()->GetDBObject ();

	if ($context->HasUser ()) {
		$self->{user} = $context->GetUser ();
	}

	# Register String::Checker extensions
	String::Checker::register_check ('owned_survey', sub {
		my $rowid = shift;
		return ($self->CheckOwnedSurvey ($$rowid));
	});
	String::Checker::register_check ('attendee_id', sub {
		my $rowid = shift;
		return ($self->CheckAttendeeId ($$rowid))
	});
}	# ----------  end of subroutine InitApp  ----------


#===============================================================================

=head2 CheckOwnedSurvey

Check current user owns the specified survey.
Internal use only, not called by webservices.

=head3 Incoming Arguments

=over

=item I<$rowid>: The survey rowid

=back

=head3 Return value

=over

undef if user owns the survey, 1 otherwise.

=back

=cut

#===============================================================================
sub CheckOwnedSurvey {
	my ($self, $rowid) = @_;

	my $res = 1;

	my $record = $self->{db}->SelectSingle ("SELECT * from survey WHERE rowid = ? AND owner_id = ?;", [$rowid, $self->{user}->Get ('rowid')]);
	if ($record) {
		$res = undef;
	}

	return ($res);
}	# ----------  end of subroutine CheckOwnedSurvey  ----------


#===============================================================================

=head2 DBDeleteSurvey

Delete a survey from DB.
Internal use only.

=head3 Incoming Arguments

=over

=item I<$rowid>: The survey rowid

=back

=head3 Return value

=over

None, throws exception if something goes wrong.

=back

=cut

#===============================================================================
sub DBDeleteSurvey {
	my ($self, $rowid) = @_;

	my $res = $self->{db}->SelectSingle ('SELECT short_url_id FROM survey WHERE rowid = ?;', [$rowid]);
	if ($res && $res->{short_url_id}) {
		$self->{db}->ExecSQL ('UPDATE survey SET short_url_id = NULL WHERE rowid = ?;', [$rowid]);
		$self->{db}->ExecSQL ('DELETE FROM short_url WHERE rowid = ?;', [$res->{short_url_id}]);
	}
	$self->{db}->ExecSQL ('DELETE FROM survey_answer USING survey_attendee WHERE survey_answer.attendee_id = survey_attendee.rowid AND survey_attendee.survey_id = ?;', [$rowid]);
	$self->{db}->ExecSQL ('DELETE FROM survey_attendee WHERE survey_id = ?;', [$rowid]);
	$self->{db}->ExecSQL ('DELETE FROM survey_date WHERE survey_id = ?;', [$rowid]);
	$self->{db}->ExecSQL ('DELETE FROM survey_resource WHERE survey_id = ?;', [$rowid]);
	$self->{db}->ExecSQL ('DELETE FROM survey WHERE rowid = ?;', [$rowid]);
}	# ----------  end of subroutine DBDeleteSurvey  ----------


#===============================================================================

=head2 CheckAttendeeId

Check attendee ID is matching the user

=head3 Incoming Arguments

=over

=item I<rowid>: The attendee rowid

=back

=head3 Return value

=over

undef if user owns the survey, 1 otherwise.

=back

=cut

#===============================================================================
sub CheckAttendeeId {
	my ($self, $rowid) = @_;

	my $res = 1;

	if (defined ($self->{user})) {
		# Request coming from connected Mioga2 user, ensure attendee_id matches connected user rowid
		my $record = $self->{db}->SelectSingle ('SELECT user_id FROM survey_attendee WHERE rowid = ?;', [$rowid]);
		if ($record && ($record->{user_id} == $self->{user}->Get ('rowid'))) {
			$res = undef;
		}
	}
	else {
		# Request coming from non-connected user, no real check can be done here
		$res = undef;
	}

	return ($res);
}	# ----------  end of subroutine CheckAttendeeId  ----------


#===============================================================================

=head2 GetResourceAvailability

Get availability for a resource in a given date range

=head3 Incoming Arguments

=over

=item I<$resource>: The resource

=item I<$date>: The start date

=item I<$duration>: The duration in minutes

=back

=head3 Return value

=over

None, updates $resource

=back

=cut

#===============================================================================
sub GetResourceAvailability {
	my ($self, $resource, $date, $duration) = @_;

	# Get availability
	# TODO check for recurrency
	my $events = $self->{db}->SelectMultiple ('SELECT cal_event.* FROM cal_event, group2cal WHERE group2cal.calendar_id = cal_event.calendar_id AND group2cal.group_id = ? AND (dstart, dend) OVERLAPS (?::timestamp, ?::timestamp + ?)', [$resource->{rowid}, $date, $date, "$duration MINUTES"]);

	my $available = (scalar (@$events)) ? 0 : 1;

	# Update resource
	if ($available) {
		$resource->{dates}->{$date} = 'boolean::true';
	}
	else {
		$resource->{dates}->{$date} = 'boolean::false';
	}
}	# ----------  end of subroutine GetResourceAvailability  ----------


#===============================================================================

=head2 DeleteGroupData

Remove group data in database when a group/user/resource is deleted

=head3 Incoming Arguments

=over

=item I<$config>: A Mioga2::Config object

=item I<$group_id>: The group rowid

=back

=head3 Return value

=over

None.

=back

=cut

#===============================================================================
sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;

	my $surveys = $self->{db}->SelectMultiple ('SELECT * FROM survey WHERE owner_id = ?;', [$group_id]);
	for my $survey (@$surveys) {
		$self->DBDeleteSurvey ($survey->{rowid});
	}
}	# ----------  end of subroutine DeleteGroupData  ----------


# ============================================================================
# GetAppDesc ()
# ============================================================================

sub GetAppDesc {
	my ($self, $context) = @_;
    my %AppDesc = ( 
    	ident   => 'Cantor',
	    name	=> __('Cantor'),
        package => 'Mioga2::Cantor',
		type => 'normal',
        description => __('The Mioga2 Cantor application'),
        api_version => '2.4',
		is_group    => 0,
		is_user     => 1,
		is_resource => 0,
        all_groups  => 0,
        all_users  => 1,
        can_be_public => 1,
        usable_by_resource => 0,

        functions   => { 
			'Use' => __('Create or answer surveys'),
		},
				
        func_methods  => { 
			Use => [ 'DisplayMain', 'GetActiveSurveys', 'GetSurvey', 'GetUsers', 'GetResources', 'GetTeams', 'SetSurvey', 'GetSolicitations', 'AnswerSurvey', 'GetAttendeeId', 'DeleteSurvey', 'SearchUser' ],
		},
		public_methods => [
			'DisplayMain', 'GetSurvey', 'AnswerSurvey', 'GetAttendeeId'
		],
		func_mioglets => { },
		public_mioglets => [  ],
		non_sensitive_methods => [
			'DisplayMain',
			'GetActiveSurveys',
			'GetSurvey',
			'GetUsers',
			'GetResources',
			'GetTeams',
			'GetSolicitations',
			'GetAttendeeId',
			'SearchUser',
		],
    );
	return \%AppDesc;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2011, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
