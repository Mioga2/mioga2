# ============================================================================
# Mioga2 Project (C) 2006-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Articles.pm : The Mioga2 Articles application.

=head1 METHODS DESCRIPTION

=cut

# ============================================================================

package Mioga2::Articles;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'articles';

use Error qw(:try);
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::Magellan::DAV;
use MIME::Entity;
use Data::Dumper;
use POSIX;
use XML::Atom::SimpleFeed;

# Constants to status
use constant PUBLISH    => "published";
use constant DRAFT      => "draft";
use constant WAITING    => "waiting";
use constant D_PUBLISH  => "DisplayMain";
use constant D_DRAFT    => "DisplayDraftArticles";
use constant D_WAITING  => "DisplayWaitingArticles";


my $debug = 0;


# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc
{
	my $self = shift;
    
    my %AppDesc = ( ident               => 'Articles',
	    	    name		=> __('Articles'),
                    package             => 'Mioga2::Articles',
                    description         => __('The Mioga2 Articles application'),
				     type                => 'normal',
                    all_groups          => 1,
                    all_users           => 0,
                    can_be_public       => 0,
                    is_user             => 0,
                    is_group            => 1,
                    is_resource         => 0,

                    functions           => {
					    'Anim'    => __('Animation methods'),
                        'Moder'   => __('Moderation methods'),
					    'Write'   => __('Creation and modification'),
					    'Read'    => __('Consultation methods'),
                        'ReadComment'   => __('Read comments'),
                        'WriteComment'  => __('Write comments'),
                    },

                    func_methods        => {
					    'Anim'    => [ 'DisplayOptions', 'CreateCategory', 'UpdateCategory', 'DeleteCategory', 'UpdateOptions'],
                        'Moder'  => [ 'DisplayWaitingArticles', 'ValidateArticle', 'DeleteArticle', 'UpdateArticle', 'EditArticle', 'DeclineArticle' ],
					    'Read'    => [ 'DisplayMain', 'DisplayArticle', 'SaveArticle', 'ViewArticle' ],
					    'Write'   => [ 'WriteArticle', 'UpdateArticle', 'EditArticle', 'DisplayDraftArticles' ],
                        'ReadComment'   => [],
                        'WriteComment'  => [ 'CommentArticle' ],
					 },

                    non_sensitive_methods        => [ 'DisplayOptions',
							'DisplayWaitingArticles', 'EditArticle',
							'DisplayMain', 'DisplayArticle', 'ViewArticle',
							'WriteArticle', 'DisplayDraftArticles',
							'CommentArticle', 'SaveArticle',
					 ],

					 public_methods => [ ],
				
                     sxml_methods => { },
				
                     xml_methods  => { },				
				
                );
	return \%AppDesc;
}
            
# ============================================================================
# Public Methods
# ============================================================================

# ============================================================================

=head2 DisplayMain ()

=over

=item Main method. It displays the list of currently available articles.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayMain {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayMain>";
    $xml .= $self->XMLGetArticlesWithState($context, state => PUBLISH, page => D_PUBLISH);

    $xml .= "</DisplayMain>";
	
	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
	return $content;
}

# ============================================================================

=head2 DisplayArticle ()

=over

=item Method to display a single article.

=back

=head3 Incoming Arguments

=over

=item I<article_id>: The article ID

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayArticle {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayArticle>";
    $xml .= $context->GetXML;
    $xml .= $self->XMLGetAccessRights($context);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'article_id'], 'disallow_empty', 'want_int'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Articles::DisplayArticle", ac_FormatErrors($errors));
	}
	my $article_id = $values->{article_id};
    
    my $article     = $self->DBGetArticle($context, values => { rowid => $article_id }, no_old_article => 1);
    $context->GetSession()->{article} = $article;
    
    $xml .= $self->XMLGetArticle($context, article => $article);
    
    $xml .= "</DisplayArticle>";
	
	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles.xsl', locale_domain => 'articles_xsl');
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
	return $content;
}

# ============================================================================

=head2 ViewArticle ()

=over

=item Method to view an article with its comments.

=back

=head3 Incoming Arguments

=over

=item I<article_id>: The article ID

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub ViewArticle {
    my ($self, $context) = @_;

	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'article_id'], 'stripxws', 'disallow_empty', 'want_int'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Articles::ViewArticle", ac_FormatErrors($errors));
	}
    my $article_id = $values->{article_id};
    
    my $data    = { ViewArticle => $context->GetContext, };
    %{$data->{ViewArticle}}    = (%{$data->{ViewArticle}}, %{$self->GetAccessRights($context)});
    
    
    my $article = $self->DBGetArticle($context, values => { rowid => $article_id }, no_old_article => 1);
    $context->GetSession->{article} = $article;
    
    %{$data->{ViewArticle}}    = (%{$data->{ViewArticle}}, %{$self->GetArticle($context, article => $article)});
    
    my $categories  = $self->GetCategories($context);
    %{$data->{ViewArticle}}    = (%{$data->{ViewArticle}}, %$categories);
    
    my $content = new Mioga2::Content::XSLT(
        $context,
        stylesheet    => 'articles.xsl',
        locale_domain => "articles_xsl"
    );
    $content->SetContent(Mioga2::tools::Convert::PerlToXML ($data));

    print STDERR Dumper($data) if $debug;
    return $content;
}

# ============================================================================

=head2 CommentArticle ()

=over

=item Method to comment an article.

=back

=head3 Incoming Arguments

=over

=item I<article_id>: The article ID

=item I<parent_id>: The parent comment ID (optional)

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub CommentArticle {
    my ($self, $context) = @_;
    
    my $config  = $context->GetConfig;
    my $dbh     = $config->GetDBH;
    my $user    = $context->GetUser;
    my $user_id = $user->GetRowid;
    
    my $data    = { CommentArticle => $context->GetContext, };
    %{$data->{CommentArticle}}  = (%{$data->{CommentArticle}}, %{$self->GetAccessRights($context)});
    
	my $values;
	my $errors;
	my $args_check = [ [ [ 'article_id'], 'stripxws', 'disallow_empty', 'want_int'],
	                   [ [ 'parent_id' ], 'stripxws', 'allow_empty', 'want_int' ] ];

    if (exists($context->{args}->{send_comment}) ) {
		push @$args_check, [ [ 'subject', 'comment', 'send_comment' ], 'stripxws', 'disallow_empty' ];
	}
	($values, $errors) = ac_CheckArgs($context, $args_check);
	if (scalar(@$errors) > 0) {
		# if error in article_id or parent_id throw exception
		# subject or comment are processed after
		my $ierr;
		for ($ierr= 0; $ierr < @$errors; $ierr++) {
			if ( ($errors->[$ierr]->[0] eq 'article_id') || ($errors->[$ierr]->[0] eq 'parent_id') ) {
				throw Mioga2::Exception::Application("Mioga2::Articles::CommentArticle", ac_FormatErrors($errors));
			}
		}
	}
    my $article_id = $values->{article_id};
    my $parent_id  = $values->{parent_id};
   	my $comment;
   	my $subject;
    my $error;
    
    if (exists($context->{args}->{send_comment}) ) {
		$comment  = $values->{comment};
		$subject  = $values->{subject};
		if (scalar(@$errors) > 0) {
			my $ierr;
			for ($ierr= 0; $ierr < @$errors; $ierr++) {
				if ( ($errors->[$ierr]->[0] eq 'subject') ) {
					$error  = __"You have to provide a subject."
				}
				elsif ( ($errors->[$ierr]->[0] eq 'comment') ) {
					$error  = __"You have to provide a comment."
				}
			}
		}
		if (!$error) {
			$parent_id  = 'NULL' if $parent_id == 0;

			ExecSQL($dbh, "INSERT INTO article_comment(comment, subject, user_id, article_id, parent_id) VALUES('" . st_FormatPostgreSQLString($comment) . "', '" . st_FormatPostgreSQLString($subject) . "', $user_id, $article_id, $parent_id)");

			my $content = Mioga2::Content::REDIRECT->new($context, mode => 'external');
			$content->SetContent( "ViewArticle?article_id=$article_id" );

			return $content;
		}
    }
    
    my $article = $self->DBGetArticle($context, values => { rowid => $article_id }, no_old_article => 1);
    $context->GetSession->{article} = $article;
    
    %{$data->{CommentArticle}}  = (%{$data->{CommentArticle}}, %{$self->GetArticle($context, article => $article)});
    
    my $categories  = $self->GetCategories($context);
    %{$data->{CommentArticle}}  = (%{$data->{CommentArticle}}, %$categories);
    if ($parent_id) {
        my $comment = SelectSingle($dbh, "SELECT article_comment.*, m_user_base.firstname, m_user_base.lastname FROM article_comment, m_user_base WHERE article_comment.article_id = $article_id AND m_user_base.rowid = article_comment.user_id AND article_comment.rowid = $parent_id");
        my $datetime        = Mioga2::Classes::Time->FromPSQL($comment->{created});
        $comment->{date}    = $datetime->strftime("%x");
        $comment->{time}    = $datetime->strftime("%X");
        $comment->{author}  = "$comment->{firstname} $comment->{lastname}";
        $data->{CommentArticle}->{Comment}  = $comment;
        $data->{CommentArticle}->{Reply}    = { subject => __x("Re: {subject}", subject => $comment->{subject}) }
    }
    
    if ($error) {
        $data->{CommentArticle}->{Error}    = $error;
        $data->{CommentArticle}->{Reply}  ||= {};
        $data->{CommentArticle}->{Reply}->{subject} = $subject if $subject;
        $data->{CommentArticle}->{Reply}->{comment} = $comment if $comment;
    }
    
    my $content = Mioga2::Content::XSLT->new($context, stylesheet => 'articles.xsl', locale_domain => "articles_xsl");
    $content->SetContent(Mioga2::tools::Convert::PerlToXML ($data));

    print STDERR Dumper($data) if $debug;
    return $content;
}
# ============================================================================

=head2 DisplayWaitingArticles ()

=over

=item Shows the articles that are waiting to be published when in moderated mode.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayWaitingArticles {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayWaitingArticles>";
	$xml .= $self->XMLGetArticlesWithState($context, state => WAITING, order => 'ASC', page => D_WAITING);
    
    $xml .= "</DisplayWaitingArticles>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
	return $content;
}

# ============================================================================

=head2 DisplayDraftArticles ()

=over

=item Shows the articles that are drafts.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayDraftArticles {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayDraftArticles>";
    $xml .= $self->XMLGetArticlesWithState($context, state => DRAFT, page => D_DRAFT);
    
    $xml .= "</DisplayDraftArticles>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
	return $content;
}

# ============================================================================

=head2 DisplayOptions ()

=over

=item Displays options of the application (only for animators).

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayOptions {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<DisplayOptions>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    $xml   .= Mioga2::tools::Convert::PerlToXML ($self->GetOptions($context));
    
    $xml   .= "</DisplayOptions>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles-js.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content
}

# ============================================================================

=head2 EditArticle ()

=over

=item Edit an article.

=back

=head3 Incoming Arguments

=over

=item I<rowid>: The article ID

=item I<page>: The webpage calling the function

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub EditArticle {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<EditArticle>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['rowid'], 'stripxws', 'disallow_empty', 'want_int' ],
        [ ['page'], 'stripxws', 'disallow_empty' ],
                                     ]);
    
    if (not @$errors) {
        my $article = $self->DBGetArticle($context, 'values' => $values);
        $xml .= $self->XMLGetArticle($context, article => $article, page => $values->{page});
        $xml .= Mioga2::tools::Convert::PerlToXML ($self->GetCategories($context, selected => $article->{categories}));
        $xml .= "<CanSaveAsDraft/>" if ($article->{owner_id} == $context->GetUser()->GetRowid() and $article->{status} ne WAITING);
        $xml .= "<Page>" . st_FormatXMLString($values->{page}) . "</Page>";
        $xml .= Mioga2::tools::Convert::PerlToXML ($self->GetOptions($context));
    }
    else {
        #$xml .= ac_XMLifyErrors($errors);
		throw Mioga2::Exception::Application("Mioga2::Articles::EditArticle", ac_FormatErrors($errors));
    }
    
    $xml   .= "</EditArticle>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 WriteArticle ()

=over

=item Write a new article. Allowed to writer users.

=back

=head3 Incoming Arguments

=over

=item I<page>: The page content

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub WriteArticle {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<WriteArticle>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['page'], 'stripxws', 'disallow_empty' ],
                                     ]);
    
    if (not @$errors) {
        $xml   .= Mioga2::tools::Convert::PerlToXML ($self->GetCategories($context));
        $xml   .= "<CanSaveAsDraft/>";
        $xml   .= "<Page>" . st_FormatXMLString($values->{page}) . "</Page>";
        $xml   .= Mioga2::tools::Convert::PerlToXML ($self->GetOptions($context));
    }
    else {
        $xml .= ac_XMLifyErrors($errors);
    }
    
    $xml   .= "</WriteArticle>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 ValidateArticle ()

=over

=item Write a new article. Allowed to writer users.

=back

=head3 Incoming Arguments

=over

=item I<rowid>: The article ID

=item I<category_id>: The category ID

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub ValidateArticle {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<ValidateArticle>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['rowid'], 'disallow_empty', 'want_int'],
        [ ['category_id'], 'stripxws', 'allow_empty', 'want_int'],
                                     ]);
    
    if (not @$errors) {
        my $article = $self->DBGetArticle($context, 'values' => $values);
        $self->DBValidateArticle($context, 'values' => $article);
        
        $values->{rowid} = $article->{old_id} if ($article->{old_id});
        my $options = $self->DBGetOptions($context);
        $self->SendMailTo($context, options => $options, who => 'user', 'values' => $values, msg => 'validated');
        
        warn __"Article successfully validated" if $debug;
        $xml .= $self->GetArticles($context, category_id => int($values->{category_id}), state => WAITING, page => D_WAITING);
        $xml .= "<OnlyCategory id=\"" . $values->{category_id} . "\"/>" if (int($values->{category_id}));
        $xml .= "<CanBeValidated/>";
        $xml .= "<Page>" . st_FormatXMLString(D_WAITING) . "</Page>";
        $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
    }
    else {
        #$xml .= ac_XMLifyErrors($errors);
		throw Mioga2::Exception::Application("Mioga2::Articles::ValidateArticle", ac_FormatErrors($errors));
    }
    
    $xml   .= "</ValidateArticle>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles-js.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 DeclineArticle ()

=over

=item Decline an existing article and send a mail to the user.

=back

=head3 Incoming Arguments

=over

=item I<rowid>: The article ID

=item I<category_id>: The category ID

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DeclineArticle {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<DeclineArticle>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['rowid'], 'disallow_empty', 'want_int'],
        [ ['category_id'], 'stripxws', 'allow_empty', 'want_int'],
                                     ]);
    
    if (not @$errors) {
        $self->DBDeclineArticle($context, 'values' => $values);
        
        my $options = $self->DBGetOptions($context);
        $self->SendMailTo($context, options => $options, 'values' => $values, who => 'user', msg => 'declined');
        
        warn __"Article declined." if $debug;
        $xml .= $self->GetArticles($context, category_id => int($values->{category_id}), state => WAITING, page => D_WAITING);
        $xml .= "<OnlyCategory id=\"" . $values->{category_id} . "\"/>" if (int($values->{category_id}));
        $xml .= "<CanBeValidated/>";
        $xml .= "<Page>" . st_FormatXMLString(D_WAITING) . "</Page>";
    }
    else {
        #$xml .= ac_XMLifyErrors($errors);
		throw Mioga2::Exception::Application("Mioga2::Articles::DeclineArticle", ac_FormatErrors($errors));
    }
    
    $xml   .= "</DeclineArticle>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles-js.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 GetCategories ()

=over

=item Retrieve categories.

=item B<return :> xml representation of categories as a string.

=back

=cut

# ============================================================================

sub GetCategories {
    my ($self, $context, %options) = @_;
    
	my $data = {
		Categories => {
			Create        => __"Add a new category",
			New           => __"New category",
			Edit          => __"Edit",
			Delete        => __"Delete this category",
			DeleteConfirm => __"Are you sure you want to delete this category ?",
		}
	};
	my $categories = $self->DBGetCategories($context);
	push (@{$data->{Categories}->{Category}}, $self->AddCategory (text => __("Show all")));
	for my $catg (@$categories) {
		push (@{$data->{Categories}->{Category}}, $self->AddCategory (text => $catg->{label}, rowid => $catg->{rowid}, selected => $options{selected}, in_use => $catg->{in_use}));
	}
	# $data->{Categories}->{Category} = $categories;
	$data->{NoCategory} = 1 unless (@$categories);
	return ($data);
}

# ============================================================================

=head2 XMLGetAccessRights ()

=over

=item Retrieve access rights.

=item B<return :> access rights as a xml string.

=back

=cut

# ============================================================================

sub XMLGetAccessRights {
    my ($self, $context) = @_;
    
    my $xml = Mioga2::tools::Convert::PerlToXML ($self->GetAccessRights($context));
    $xml =~ s/<\?xml version="1\.0" encoding="UTF-8"\?>\s*//;
    utf8::encode($xml);
    
    $xml .= "<Actions>";
    $xml .= "<DeleteArticle>" . st_FormatXMLString(__"Delete this article") . "</DeleteArticle>";
    $xml .= "<WriteArticle>" . st_FormatXMLString(__"Write a new article") . "</WriteArticle>";
    $xml .= "<WaitingArticles>" . st_FormatXMLString(__"Pending articles") . "</WaitingArticles>";
    $xml .= "<Options>" . st_FormatXMLString(__"Change options") . "</Options>";
    $xml .= "<Edit>" . st_FormatXMLString(__"Edit") . "</Edit>";
    $xml .= "<UpdateArticle>" . st_FormatXMLString(__"Modifying article") . "</UpdateArticle>";
    $xml .= "<CategoryChoices>" . st_FormatXMLString(__"Choose at least one category") . "</CategoryChoices>";
    $xml .= "<ArticleTitle>" . st_FormatXMLString(__"Title") . "</ArticleTitle>";
    $xml .= "<ArticleHeader>" . st_FormatXMLString(__"Header") . "</ArticleHeader>";
    $xml .= "<Article>" . st_FormatXMLString(__"Article") . "</Article>";
    $xml .= "<PublishArticle>" . st_FormatXMLString(__"Publish") . "</PublishArticle>";
    $xml .= "<Cancel>" . st_FormatXMLString(__"Cancel") . "</Cancel>";
    $xml .= "<MainArticles>" . st_FormatXMLString(__"Published articles") . "</MainArticles>";
    $xml .= "<Validate>" . st_FormatXMLString(__"Validate this article") . "</Validate>";
    $xml .= "<DeclineConfirm>" . st_FormatXMLString(__"Are you sure to decline this article ?") . "</DeclineConfirm>";
    $xml .= "<DeleteConfirm>" . st_FormatXMLString(__"Are you sure to delete this article ?") . "</DeleteConfirm>";
    $xml .= "<Decline>" . st_FormatXMLString(__"Decline") . "</Decline>";
    $xml .= "<DraftArticles>" . st_FormatXMLString(__"View my drafts") . "</DraftArticles>";
    $xml .= "<SaveDraft>" . st_FormatXMLString(__"Save as a draft") . "</SaveDraft>";
    $xml .= "</Actions>";
    return $xml;
}

# ============================================================================

=head2 GetAccessRights ()

=over

=item Retrieve access rights for the current user.

=item B<return :> access rights as a data hash.

=back

=cut

# ============================================================================

sub GetAccessRights {
    my ($self, $context) = @_;
    
    my $user    = $context->GetUser;
    my $group   = $context->GetGroup;
    my $rights  = { AccessRights => { User => { id => $context->GetUser->GetRowid } }};
    
    $rights->{AccessRights}->{Animation}    = {} if $self->CheckUserAccessOnFunction($context, $user, $group, "Anim");
    $rights->{AccessRights}->{Moderation}   = {} if $self->CheckUserAccessOnFunction($context, $user, $group, "Moder");
    $rights->{AccessRights}->{Write}        = {} if $self->CheckUserAccessOnFunction($context, $user, $group, "Write");
    $rights->{AccessRights}->{Read}         = {} if $self->CheckUserAccessOnFunction($context, $user, $group, "Read");
    $rights->{AccessRights}->{WriteComment} = {} if $self->CheckUserAccessOnFunction($context, $user, $group, "WriteComment");
    $rights->{AccessRights}->{ReadComment}  = {} if $self->CheckUserAccessOnFunction($context, $user, $group, "ReadComment");
    return $rights;
}

# ============================================================================

=head2 GetArticles ()

=over

=item Retrieve articles.

=item B<return :> xml representation of articles as a string.

=back

=cut

# ============================================================================

sub GetArticles {
    my ($self, $context, %options) = @_;
    
    my $xml = "<Articles>";
    
    my $articles = $self->DBGetArticles($context, %options);
    
    foreach my $article (@$articles) {
        $xml .= $self->XMLGetArticle($context, article => $article, page => $options{page});
    }

    $xml .= "</Articles>";
    return $xml;
}

# ============================================================================

=head2 GetOptions (I<$context>)

=over

=item retrieve options.

=item B<return :> Options in a hash

=back

=cut

# ============================================================================

sub GetOptions {
    my ($self, $context) = @_;

	my $options = $self->DBGetOptions($context);

	my $data = {
		Options => {
			Title  => __"Modify options",
			Cancel => __"Cancel",
			Ok     => __"Ok",
			Option => [
				{
					field   => 'public_access',
					label   => __"Public access",
					checked => $options->{public_access}
				},
				{
					field   => 'extended_ui',
					label   => __"Complete input area",
					checked => $options->{extended_ui}
				},
				{
					field   => 'moderated',
					label   => __"Moderate articles",
					checked => $options->{moderated}
				},
				{
					field   => 'moderator_mail',
					label   => __"Send mail to moderator",
					checked => $options->{moderator_mail}
				},
				{
					field   => 'user_mail',
					label   => __"Send mail to user",
					checked => $options->{user_mail}
				},
			],
		}
	};
	return ($data);
}


# ============================================================================

=head2 XMLAddMenuItem (I<$options>)

=over

=item Add an entry to the menu.

=item B<parameters :>

=over

=item I<$options->{id}> : id of action as a string.

=item I<$options->{uri}> : uri of action as a string.

=item I<$options->{text}> : text of action as a string.

=back

=item B<return :> the entry in xml as a string.

=back

=cut

# ============================================================================

sub XMLAddMenuItem {
    my ($self, %options) = @_;
    
    my $xml = "<Item id=\"" . st_FormatXMLString($options{id}) . "\">";
    $xml   .= "<Uri>" . st_FormatXMLString($options{uri}) . "</Uri>";
    $xml   .= "<Label>" . st_FormatXMLString($options{text}) . "</Label>";
    $xml   .= "</Item>";
    
    return $xml;
}

# ============================================================================

=head2 XMLAddCategory (I<$options>)

=over

=item Add a category item.

=item B<parameters :>

=over

=item I<$options->{id}> : id of the category (optional).

=item I<$options->{text}> : label to display.

=back

=item B<return :> the entry in xml as a string.

=back

=cut

# ============================================================================

sub XMLAddCategory {
    my ($self, %options) = @_;
    
    my $action = "DisplayMain";
    my $xml = "<Category> <action>" . st_FormatXMLString($action) . "</action>";
    $xml .= "<rowid>" . $options{rowid} . "</rowid>" if $options{rowid};

    if ($options{selected}) {
        my $categories = $options{selected};
        
        if (ref($categories) eq 'ARRAY') {
            foreach my $scatg (@$categories) {
                if ((ref($scatg) eq 'HASH' and $scatg->{rowid} == $options{rowid}) or ($scatg == $options{rowid})) {
                    $xml .= "<selected>1</selected>";
                    last;
                }
            }
        }
        else {
            $xml .= "<selected>1</selected>" if ($categories == $options{rowid});
        }
    }
    
    $xml .= "<in_use>" . int($options{in_use}) . "</in_use><name>" . st_FormatXMLString($options{text}) . "</name></Category>";
    
    return $xml;
}

# ============================================================================

=head2 AddCategory (I<$options>)

=over

=item Add a category item.

=item B<parameters :>

=over

=item I<$options->{id}> : id of the category (optional).

=item I<$options->{text}> : label to display.

=back

=item B<return :> the entry as a hash.

=back

=cut

# ============================================================================

sub AddCategory {
    my ($self, %options) = @_;
    
    my $action  = "DisplayMain";
    my $data    = { action => $action };
    
    $data->{rowid} = $options{rowid} if $options{rowid};

    if ($options{selected}) {
        my $categories = $options{selected};
        
        if (ref($categories) eq 'ARRAY') {
            foreach my $scatg (@$categories) {
                if ((ref($scatg) eq 'HASH' and $scatg->{rowid} == $options{rowid}) or ($scatg == $options{rowid})) {
                    $data->{selected}   = "true";
                    last;
                }
            }
        }
        else {
            $data->{selected}   = "true" if $categories == $options{rowid};
        }
    }
    
    $data->{in_use} = int($options{in_use});
    $data->{label}  = $options{text};
    
    return $data;
}

# ============================================================================

=head2 XMLGetArticle (I<%options>)

=over

=item XMLize an article hash.

=item B<parameters :>

=over

=item I<$options{article}>: article hash.

=item I<$options{add_context}>: set the context or not for a standalone use. defaults to undef.

=back

=item B<return :> return article in xml as a string.

=back

=cut

# ============================================================================

sub XMLGetArticle {
    my ($self, $context, %options) = @_;
    
    my $article     = $options{article};
    my $add_context = $options{add_context};
    
    my $xml .= "<Article> <created>" . st_FormatXMLString($article->{created}) . "</created><published>" . st_FormatXMLString($article->{published}) . "</published><modified>" . st_FormatXMLString($article->{modified}) . "</modified><author>" . st_FormatXMLString($article->{owner_id}) . "</author><moderator>" . st_FormatXMLString($article->{moderator_id}) . "</moderator><rowid>" . $article->{rowid} . "</rowid><status>" . st_FormatXMLString($article->{status}) . "</status>";
    $xml .= "<old_id>" . $article->{old_id} . "</old_id>" if ($article->{old_id});
    
    $xml .= $context->GetXML(no_header => 1) if ($add_context);
    
    if (($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Write") and $context->GetUser()->GetRowid() == int($article->{owner_id})) or $self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Moder")) {
        $xml .= "<CanBeEdited/>";
    }
    $xml .= "<CanBeValidated/>" if ($article->{status} eq WAITING and $self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Moder") and $options{page} eq D_WAITING);
    
	for ($article->{status}) {
		if (/published/) {
			# Publication date
			my @user_time = gmtime(du_ISOToSecond(du_ConvertGMTISOToLocale($article->{published}, $context->GetUser)));
			$xml .= "<Infos>" . st_FormatXMLString(__x("Published by {firstname} {lastname} on {date} at {time}.", firstname => $article->{firstname}, lastname => $article->{lastname}, date => strftime("%x", @user_time), 'time' => strftime("%X", @user_time) )) . "</Infos>";

			# Modification date
			if ($article->{published} ne $article->{modified}) {
				my @user_mod_time = gmtime(du_ISOToSecond(du_ConvertGMTISOToLocale($article->{modified}, $context->GetUser)));
				$xml .= "<Infos>" . __x("Last modification on {date} at {time}.", date => strftime("%x", @user_mod_time), 'time' => strftime("%X", @user_mod_time)) . "</Infos>";
			}
		}
		else {
			# Creation date
			my @user_time = gmtime(du_ISOToSecond(du_ConvertGMTISOToLocale($article->{created}, $context->GetUser)));
			$xml .= "<Infos>" . st_FormatXMLString(__x("Created by {firstname} {lastname} on {date} at {time}.", firstname => $article->{firstname}, lastname => $article->{lastname}, date => strftime("%x", @user_time), 'time' => strftime("%X", @user_time) )) . "</Infos>";

			# Modification date
			if ($article->{created} ne $article->{modified}) {
				my @user_mod_time = gmtime(du_ISOToSecond(du_ConvertGMTISOToLocale($article->{modified}, $context->GetUser)));
				$xml .= "<Infos>" . __x("Last modification on {date} at {time}.", date => strftime("%x", @user_mod_time), 'time' => strftime("%X", @user_mod_time)) . "</Infos>";
			}
		}
	}
    
    if (ref($article->{categories}) eq 'ARRAY') {
        $xml .= "<Categories>";
    
        foreach my $catg (@{$article->{categories}}) {
            $xml .= Mioga2::tools::Convert::PerlToXML ({ 'Category' => $self->AddCategory(text => $catg->{label}, rowid => $catg->{rowid}) }) if (ref($catg) eq 'HASH');
        }
        $xml .= "</Categories>";
    }
    
    $xml .= "<Title>" . st_FormatXMLString($article->{title}) . "</Title>";
    $xml .= "<Header>" . st_FormatXMLString($article->{header}) . "</Header>";
    $xml .= "<Contents>" . st_FormatXMLString($article->{contents}) . "</Contents>";
    $xml .= "<CommentsNumber>$article->{comments_number}</CommentsNumber>";
    $xml .= "</Article>";

    return $xml;
}

# ============================================================================

=head2 GetArticle (I<%options>)

=over

=item Convert an article object to a data hash XML compatible.

=item B<parameters :>

=over

=item I<$options{article}>: article hash.

=item I<$options{add_context}>: set the context or not for a standalone use. defaults to undef.

=back

=item B<return :> return article as a data hash.

=back

=cut

# ============================================================================

sub GetArticle {
    my ($self, $context, %options) = @_;
    
    my $article     = $options{article};
    my $add_context = $options{add_context};
    my $user        = $context->GetUser;
    my $group       = $context->GetGroup;
    
    my $data    = {Article => { created => $article->{created}, modified => $article->{modified}, author => $article->{owner_id}, moderator => $article->{moderator_id}, id => $article->{rowid}, status => $article->{status} } };
    
    $data->{Article}->{old_id}      = $article->{old_id} if $article->{old_id};
    $data->{Article}->{CanBeEdited} = 1 if (($self->CheckUserAccessOnFunction($context, $user, $group, "Write") and $user->GetRowid == int($article->{owner_id})) or $self->CheckUserAccessOnFunction($context, $user, $group, "Moder"));
    $data->{Article}->{CanBeValidated}  = 1 if ($article->{status} eq WAITING and $self->CheckUserAccessOnFunction($context, $user, $group, "Moder") and $options{page} eq D_WAITING);
    
    my @user_time = gmtime(du_ISOToSecond(du_ConvertGMTISOToLocale($article->{created}, $user)));
   
    $data->{Article}->{Infos}   = __x("Published by {firstname} {lastname} on {date} at {time} in", firstname => $article->{firstname}, lastname => $article->{lastname}, date => strftime("%x", @user_time), 'time' => strftime("%X", @user_time) );
    
    if (ref($article->{categories}) eq 'ARRAY') {
        $data->{Article}->{Categories}  = { Category => [] };
    
        foreach my $catg (@{$article->{categories}}) {
            push @{$data->{Article}->{Categories}->{Category}}, $self->AddCategory(text => $catg->{label}, id => $catg->{rowid}) if (ref($catg) eq 'HASH');
        }
    }
    
    $data->{Article}->{Title}       = $article->{title};
    $data->{Article}->{Header}      = $article->{header};
    $data->{Article}->{Contents}    = $article->{contents};

    $data->{Article}->{Comments}        = $self->Comments($article->{comments});
    $data->{Article}->{CommentsNumber}  = $article->{comments_number};

    return $data;
}

# ============================================================================

=head2 XMLGetArticlesWithState (I<$context>, I<%options>)

=over

=item Get all articles with the given state.

=item B<parameters :>

=over

=item I<$context> : request context.

=back

=item B<return :> xml describing articles with the given state.

=back

=cut

# ============================================================================

sub XMLGetArticlesWithState {
    my ($self, $context, %options) = @_;
    
    my $xml = $context->GetXML;
    $xml .= $self->XMLGetAccessRights($context);
    $xml .= Mioga2::tools::Convert::PerlToXML ($self->GetOptions($context));

    $xml .= Mioga2::tools::Convert::PerlToXML ($self->GetCategories($context));
    $xml .= $self->GetArticles($context, category_id => $context->{args}->{category_id}, state => $options{state}, page => $options{page});
    
    $xml .= "<OnlyCategory id=\"" . $context->{args}->{category_id} . "\"/>" if ($context->{args}->{category_id});
    $xml .= "<State>" . st_FormatXMLString($options{state}) . "</State>";
    
    return $xml;
}

# ============================================================================

=head2 CreateCategory (I<$context>)

=over

=item Create a new category.

=item B<parameters :>

=over

=item I<$context> : context of the current request.

=back

=item B<return :> error message or list of updated categories.

=back

=cut

# ============================================================================

sub CreateCategory {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<CreateCategory>";
	$xml .= $context->GetXML;
    $xml .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ [ 'label'], 'stripxws', 'disallow_empty', ['max' => 64] ],
        [ ['page'], 'stripxws', 'disallow_empty' ],
        [ [ 'description'], 'stripxws', 'allow_empty' ],
                                     ]);
    
    if (not @$errors) {
        try {
            $self->DBCreateCategory($context, 'values' => $values);
            warn __"Category successfully created" if $debug;
        }
        otherwise {
            $xml .= "<errors><err arg=\"categories\"><type>already_exists</type></err></errors>";
        };
        
        
        $xml .= Mioga2::tools::Convert::PerlToXML ($self->GetCategories($context));
    }
    else {
        #$xml .= ac_XMLifyErrors($errors);
		throw Mioga2::Exception::Application("Mioga2::Articles::CreateCategory", ac_FormatErrors($errors));
    }
    
    $xml .= "<Page>" . st_FormatXMLString($values->{page}) . "</Page>";
    $xml .= "</CreateCategory>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles-js.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
	return $content;
}

# ============================================================================

=head2 UpdateCategory ()

=over

=item update a category.

=back

=head3 Incoming Arguments

=over

=item I<value>: The category name (max 64 chars)

=item I<rowid>: The category ID

=item B<return :> error or the updated category list.

=back

=cut

# ============================================================================

sub UpdateCategory {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<UpdateCategory>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['value'], 'stripxws', 'disallow_empty', ['max' => 64] ],
        [ ['rowid'], 'stripxws', 'disallow_empty', 'want_int'],
                                     ]);
    
    if (not @$errors) {
        try {
            $self->DBUpdateCategory($context, 'values' => $values);
            warn $self->GetText("Category successfully updated") if $debug;
            $xml .= $self->XMLAddCategory(text => st_FormatXMLString($values->{value}), id => $values->{rowid});
        }
        otherwise {
            my $catg = $self->DBGetCategory($context, rowid => $values->{rowid});
            $xml .= $self->XMLAddCategory(text => st_FormatXMLString($catg->{label}), id => $catg->{rowid});
        };
        $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
    }
    else {
		throw Mioga2::Exception::Application("Mioga2::Articles::UpdateCategory", ac_FormatErrors($errors));
        #$xml .= ac_XMLifyErrors($errors);
        
        #my $catg = $self->DBGetCategory($context, rowid => $values->{rowid});
        #$xml .= $self->XMLAddCategory(text => st_FormatXMLString($catg->{label}), id => $catg->{rowid});
        #$xml .= $self->XMLAddCategory(text => st_FormatXMLString(__"Error !"), id => $values->{rowid});
    }
    
    $xml   .= "</UpdateCategory>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles-parts.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 ProcessDraft (I<$context>, I<%options>)

=over

=item Process an article in draft state.

=item B<parameters :>

=over

=item I<$context> : request context.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub ProcessDraft {
    my ($self, $context, %options) = @_;
    my $values = $options{'values'};
    
    if (not $values->{rowid}) {
        $self->DBInsertArticle($context, 'values' => $values, status => DRAFT);
    }
    else {
        my $article = $self->DBGetArticle($context, 'values' => $values);
        if ($article->{status} eq PUBLISH) {
            if ($values->{old_id}) {
                $self->DBUpdateArticle($context, 'values' => $values, status => DRAFT);
            }
            else {
                $self->DBInsertArticle($context, 'values' => $values, link_to => $values->{rowid}, status => DRAFT);
            }
        }
        else {
            if ($article->{status} eq WAITING) {
                throw Mioga2::Exception::Application("Articles::ProcessDraft", __"Can't make a draft from a waiting article.");
            }
            else {
                $self->DBUpdateArticle($context, 'values' => $values, status => DRAFT);
            }
        }
    }
}

# ============================================================================

=head2 SendMailTo (I<$context>, I<%options>)

=over

=item Send a prewritten mail to user or moderators.

=item B<parameters :>

=over

=item I<$context> : request context.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub SendMailTo {
    my ($self, $context, %options) = @_;
    my $values  = $options{'values'};
    my $who     = $options{who};
    my $msg_id  = $options{msg};
    my $options = $options{options};
    
    return unless ($options->{moderated} == 1);
    
    my ($message, $subject, $recipient);
    my $user    = $context->GetUser();
    my $sender  = $user->GetName() . " <" . $user->GetEmail() . ">";
    my $config  = $context->GetConfig();
    my $group   = $context->GetGroup();
    
    ## shared messages
    my $automatic   = __"This is an automatic message please do not answer.";
    my $uri_msg     = __"You can access to the moderation page directly by clicking the following link:\n%s";
    
    if ($options->{moderator_mail} == 1 and $who eq 'moderators') {
        my $app_desc    = $context->GetAppDesc();
        my @mod_list    = ProfileGetUsersForAuthzFunction($config->GetDBH(), $group->GetRowid(), $app_desc->GetIdent(), 'Moder');
        $recipient      = new Mioga2::Old::UserList($config, rowid_list => \@mod_list);
        my $uri         = new Mioga2::URI($config, group => $group->GetIdent(), public => 0, application => $app_desc->GetIdent(), method => 'DisplayWaitingArticles');
        my $uri_link    = $config->GetProtocol()."://".$config->GetServerName().$uri->GetURI();
        
        if ($msg_id eq 'new_article') {
            $subject = __x("A new article in group {group} is waiting for your approval", group => $group->GetIdent); 
            $message = "$automatic\n" . __x("The new article '{title}' awaits your approval.", title => $values->{title}) . "\n\n" . sprintf($uri_msg, $uri_link);
        }
        elsif ($msg_id eq 'updated_article') {
            $subject = __x("An existing article in group {group} has been updated", group => $group->GetIdent);
            $message = "$automatic\n" . __x("The article '{title}' has been updated and awaits your approval.", title => $values->{title}) . "\n\n" . sprintf($uri_msg, $uri_link);
        }
    }
    
    if ($options->{user_mail} == 1 and $who eq 'user') {
        my $article = $self->DBGetArticle($context, 'values' => $values);
        $recipient = new Mioga2::Old::User($config, rowid => $article->{owner_id});
        
        if ($msg_id eq 'validated') {
            $subject = __x("Article approved in group {group}", group => $group->GetIdent);
            $message = "$automatic\n" . __x("Your article '{title}' has been approved by a moderator.", title => $article->{title});
        }
        elsif ($msg_id eq 'declined') {
            $subject = __x("Article declined in group {group}", group => $group->GetIdent);
            $message = "$automatic\n" . __x("Your article '{title}' has been declined by a moderator. It is now available to you as a draft.", title => $article->{title});
        }
    }

    if (($options->{user_mail} == 1 and $who eq 'user') || ($options->{moderator_mail} == 1 and $who eq 'moderators')) {
		my $entity = MIME::Entity->build(From   => $sender,
										Subject => $subject,
										Data    => $message,
										Charset => "UTF-8");
		$recipient->SendMail($entity);
	}
}

# ============================================================================

=head2 ProcessPublish (I<$context>, I<%options>)

=over

=item Process an article (create, update, moderate).

=item B<parameters :>

=item I<$context> : request context.

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub ProcessPublish {
    my ($self, $context, %options) = @_;
    my $values = $options{'values'};
    
    my $options = $self->DBGetOptions($context);
    if (not $values->{rowid}) {
        if ($options->{moderated} == 1) {
            $self->DBInsertArticle($context, 'values' => $values, status => WAITING);
            $self->SendMailTo($context, 'values' => $values, msg => 'new_article', options => $options, who => 'moderators');
        }
        else {
            if ($values->{old_id}) {
                throw Mioga2::Exception::Application("Articles::ProcessPublish", __"No rowid set but old_id is set ?!");
            }
            else {
                $self->DBInsertArticle($context, 'values' => $values, status => PUBLISH);
                $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
            }
        }
    }
    else {
        if ($options->{moderated} == 1) {
            if ($values->{old_id} or $values->{status} eq DRAFT) {
                $self->DBUpdateArticle($context, 'values' => $values, status => WAITING);
            }
            else {
                $self->DBInsertArticle($context, 'values' => $values, status => WAITING, link_to => $values->{rowid});
            }
            $self->SendMailTo($context, 'values' => $values, msg => 'updated_article', options => $options, who => 'moderators');
        }
        else {
            if ($values->{old_id}) {
                $self->DBUpdateArticle($context, 'values' => $values, status => PUBLISH, update_old => 1);
                $self->DBDeleteArticle($context, 'values' => $values);
                $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
            }
            else {
                $self->DBUpdateArticle($context, 'values' => $values, status => PUBLISH);
                $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
            }
        }
    }
}

# ============================================================================

=head2 UpdateArticle ()

=over

=item update an article.

=back

=head3 Incoming Arguments

=over

=item I<title>: The article title (max 128 chars)

=item I<rowid>: The article ID (if editing)

=item I<categories>: The article's categories

=item I<contents>: The article content

=item I<page>: The webpage calling the function

=item B<return :> error or the updated article.

=back

=cut

# ============================================================================

sub UpdateArticle {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<UpdateArticle>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['title'], 'stripxws', 'disallow_empty', ['max' => 128] ],
        [ ['header', 'draft', 'validate', 'decline', 'status', 'candraft', 'editaction'], 'stripxws', 'allow_empty' ],
        [ ['rowid'], 'stripxws', 'want_int', 'allow_empty' ],
        [ ['categories', 'contents', 'page' ], 'stripxws', 'disallow_empty'],
                                     ]);

    if (not @$errors) {
        $values->{header} = st_FilterHTMLTags(\$values->{header});
        $values->{contents} =~ s/\n/ /gm;
        $values->{title} =~ s/\n/ /gm;
        
        if ($values->{draft}) {
            $self->ProcessDraft($context, 'values' => $values);
        }
        elsif ($values->{decline}) {
            $self->DeclineArticle($context);
        }
        elsif ($values->{validate}) {
            $self->DBUpdateArticle($context, 'values' => $values);
            $self->ValidateArticle($context);
        }
        else {
            $self->ProcessPublish($context, 'values' => $values);
        }
        
        my $content = new Mioga2::Content::REDIRECT($context);
        $content->SetContent($values->{page});
        return $content;
    }
    else {
        $xml .= ac_XMLifyErrors($errors);
        
        $xml .= $self->XMLGetArticle($context, article => $values);
        $xml .= Mioga2::tools::Convert::PerlToXML ($self->GetCategories($context, selected => $values->{categories}));
        $xml .= "<CanSaveAsDraft/>" if ($values->{candraft});
        $xml .= "<Page>" . st_FormatXMLString($values->{page}) . "</Page>";
        $xml .= "<EditAction>" . st_FormatXMLString($values->{editaction}) . "</EditAction>";
    }
    
    $xml   .= "</UpdateArticle>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 DeleteCategory ()

=over

=item delete a category.

=back

=head3 Incoming Arguments

=over

=item I<page>: The webpage calling the function

=item I<rowid>: The category ID

=item B<return :> error or the updated category list.

=back

=cut

# ============================================================================

sub DeleteCategory {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<DeleteCategory>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['rowid'], 'stripxws', 'disallow_empty', 'want_int'],
        [ ['page'], 'stripxws', 'disallow_empty' ],
                                     ]);
    
    if (not @$errors) {
        $self->DBDeleteCategory($context, 'values' => $values);
        
        warn __"Category successfully updated" if $debug;
        $xml .= Mioga2::tools::Convert::PerlToXML ($self->GetCategories($context));
    }
    else {
        #$xml .= ac_XMLifyErrors($errors);
		throw Mioga2::Exception::Application("Mioga2::Articles::DeleteCategory", ac_FormatErrors($errors));
    }
    
    $xml   .= "<Page>" . st_FormatXMLString($values->{page}) . "</Page>";
    $xml   .= "</DeleteCategory>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles-js.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 DeleteArticle ()

=over

=item delete an article.

=back

=head3 Incoming Arguments

=over

=item I<rowid>: The article ID

=item I<category_id>: The category ID

=item I<page>: The webpage calling the function

=item B<return :> error or the deleted article.

=back

=cut

# ============================================================================

sub DeleteArticle {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<DeleteArticle>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['rowid'], 'stripxws', 'disallow_empty', 'want_int'],
        [ ['category_id'], 'stripxws', 'allow_empty'],
        [ ['page' ], 'stripxws', 'disallow_empty'],
                                     ]);
    
    if (not @$errors) {
        $self->DBDeleteArticle($context, 'values' => $values);
        
        warn __"Article successfully deleted" if $debug;
        
        $xml .= $self->GetArticles($context, category_id => int($values->{category_id}), state => $self->GetStateFromPage($values->{page}), page => $values->{page});
        $xml .= "<OnlyCategory id=\"" . $values->{category_id} . "\"/>" if (int($values->{category_id}));
        
        $xml .= "<CanBeValidated/>" if ($values->{state} eq WAITING);
        $xml .= "<Page>" . st_FormatXMLString($values->{page}) ."</Page>";
        $xml .= "<DeletedArticle>$values->{rowid}</DeletedArticle>";
        $xml .= Mioga2::tools::Convert::PerlToXML ($self->GetCategories($context));
        $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
    }
    else {
        #$xml .= ac_XMLifyErrors($errors);
		throw Mioga2::Exception::Application("Mioga2::Articles::DeleteArticle", ac_FormatErrors($errors));
    }
    
    $xml   .= "</DeleteArticle>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles-js.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 UpdateOptions ()

=over

=item update options status.

=back

=head3 Incoming Arguments

=over

=item I<field>: The option field containing true or false

...

=item B<return :> error or the updated options list.

=back

=cut

# ============================================================================

sub UpdateOptions {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<UpdateOptions>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];

	my @fields = qw/public_access extended_ui moderated moderator_mail user_mail/;
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ \@fields, 'allow_empty', 'bool'],
                                     ]);
    
    if (not @$errors) {
		# Set fields value to 0 if not sent by interface
		for (@fields) {
			$values->{$_} = 0 unless ($values->{$_});
		}

        $self->DBUpdateOptions($context, 'values' => $values);
        
        warn __"Options successfully updated" if $debug;
        
        $xml .= "<Message>" . st_FormatXMLString(__"Options successfully updated.") .  "</Message>";
        $xml .= "<UiUpdated/>" if (int($context->{args}->{old_ui}) != int($values->{extended_ui}));
        
        $xml .= Mioga2::tools::Convert::PerlToXML ($self->GetOptions($context));
    }
    else {
        #$xml .= ac_XMLifyErrors($errors);
		throw Mioga2::Exception::Application("Mioga2::Articles::UpdateOptions", ac_FormatErrors($errors));
    }
    
    $xml   .= "</UpdateOptions>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles-js.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 DBGetCategories (I<$context>)

=over

=item Database method to retrieve categories.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> list of hashes containing data.

=back

=cut

# ============================================================================

sub DBGetCategories {
    my ($self, $context) = @_;
    
    my $group_id = $context->GetGroup()->GetRowid();
    my $sql = "SELECT article_category_list.* FROM article_category_list,article_pref WHERE article_pref.group_id = $group_id AND article_category_list.article_pref_id=article_pref.rowid ORDER BY label ASC";
    
    my $result = SelectMultiple($context->GetConfig()->GetDBH(), $sql);
    
    my $in_use_catg = SelectMultiple($context->GetConfig()->GetDBH(), "SELECT DISTINCT category_id FROM article_category, article WHERE article_category.article_id=article.rowid AND article.group_id=$group_id");
    
    my %match;
    foreach my $idx (0..$#{$result}) {
        $match{$$result[$idx]->{rowid}} = $idx;
		$$result[$idx]->{in_use} = 0;
    }
    
    foreach my $catg (@$in_use_catg) {
        $$result[$match{$catg->{category_id}}]->{in_use} = 1;
    }
    
    return $result;
}

# ============================================================================

=head2 DBGetCategory (I<$context>, I<%options>)

=over

=item Database method to retrieve a given category.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> hash containing data.

=back

=cut

# ============================================================================

sub DBGetCategory {
    my ($self, $context, %options) = @_;
    
    my $group_id = $context->GetGroup()->GetRowid();
    my $sql = "SELECT article_category_list.* FROM article_category_list,article_pref WHERE article_pref.group_id = $group_id AND article_category_list.article_pref_id=article_pref.rowid AND article_category_list.rowid=".$options{rowid};
    
    my $result = SelectSingle($context->GetConfig()->GetDBH(), $sql);
    
    return $result;
}

# ============================================================================

=head2 DBGetArticles (I<$context>)

=over

=item Database method to retrieve articles.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> list of hashes containing data.

=back

=cut

# ============================================================================

sub DBGetArticles {
    my ($self, $context, %options) = @_;
    
    my $group_id = ($options{group} || $context->GetGroup)->GetRowid;
    my $only_catg_sql = "";
    my $only_user = "";
    my $from_catg = "";
    
    if ($options{category_id}) {
        $only_catg_sql = "AND article.rowid=article_category.article_id AND article_category.category_id=" . $options{category_id};
        $from_catg     = ", article_category";
    }
    $options{state} ||= PUBLISH;
    $options{order} ||= 'DESC';
    $options{feed}  ||= 0;
    $options{max}   ||= undef;
    
    if ($options{state} eq DRAFT) {
        $only_user = "AND article.owner_id=" . $context->GetUser()->GetRowid();
    }
    my $limit   = ($options{max}) ? "LIMIT $options{max}" : "";
    
    my $sql = "SELECT (SELECT COUNT(*) FROM article_comment WHERE article_id=article.rowid) AS comments_number, article.*, m_user_base.firstname, m_user_base.lastname, article_and_news_status.label as status FROM article, article_and_news_status, m_user_base$from_catg WHERE article.group_id=$group_id AND article_and_news_status.label='" . $options{state} . "' AND article.status_id=article_and_news_status.rowid AND m_user_base.rowid=article.owner_id $only_catg_sql $only_user ORDER BY article.created $options{order} $limit";
    
    my $catg_sql = "SELECT article.rowid AS article_id,article_category_list.label,article_category_list.rowid FROM article_category_list,article,article_category,article_and_news_status WHERE article.group_id=$group_id AND article_category.article_id=article.rowid AND article_category_list.rowid=article_category.category_id AND article_and_news_status.label='" . $options{state} . "' AND article.status_id=article_and_news_status.rowid $only_catg_sql $only_user ORDER BY article_id, article_category_list.label";
    
    my $result = SelectMultiple($context->GetConfig()->GetDBH(), $sql);
    my $catg_result = SelectMultiple($context->GetConfig()->GetDBH(), $catg_sql);
    
    my %match;
    foreach my $idx (0..$#{$result}) {
        $match{$$result[$idx]->{rowid}} = $idx;
    }
    
    foreach my $res (@$catg_result) {
        $$result[$match{$res->{article_id}}]->{categories} = [] if (not defined $$result[$match{$res->{article_id}}]->{categories}) ;
        my $categories = $$result[$match{$res->{article_id}}]->{categories};
        push @$categories, { label => $res->{label}, rowid => $res->{rowid} };
    }
    
    return $result;
}

# ============================================================================

=head2 DBGetArticleInfos (I<$context>)

=over

=item Database method to retrieve an article.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> article.

=back

=cut

# ============================================================================

sub DBGetArticle {
    my ($self, $context, %options) = @_;
    
    my $values      = $options{'values'};
    my $group_id    = $context->GetGroup()->GetRowid();
    my $dbh         = $context->GetConfig()->GetDBH();
    
    my $old_article = {};
    my $rowid       = $values->{rowid};
    
    unless ($options{no_old_article}) {
        $old_article = SelectSingle($dbh, "SELECT * FROM article_moderation WHERE old_id=" . $values->{rowid} . " OR new_id=" . $values->{rowid});
        $rowid       = $old_article->{new_id} || $values->{rowid};
    }
    
    my $article = SelectSingle($dbh, "SELECT article.*, article_and_news_status.label as status, m_user_base.firstname, m_user_base.lastname FROM article, article_and_news_status, m_user_base WHERE article.group_id=$group_id AND article.rowid=$rowid AND m_user_base.rowid=article.owner_id AND article_and_news_status.rowid=article.status_id");
    
    $article->{firstname} = $article->{firstname};
    $article->{lastname} = $article->{lastname};
    
    my $catg_sql = "SELECT article_category_list.label,article_category_list.rowid FROM article_category_list,article,article_category,article_and_news_status WHERE article.group_id=$group_id AND article_category.article_id=article.rowid AND article_category_list.rowid=article_category.category_id AND article.status_id=article_and_news_status.rowid AND article.rowid=$rowid ORDER BY article_id, article_category_list.label";
    
    my $catg_result = SelectMultiple($context->GetConfig()->GetDBH(), $catg_sql);
    
    $article->{old_id} = $old_article->{old_id} if ($old_article->{old_id});
    $article->{categories} = [];
    foreach my $res (@$catg_result) {
        my $categories = $article->{categories};
        push @$categories, { label => $res->{label}, rowid => $res->{rowid} };
    }
    
    my $comments    = SelectMultiple($dbh, "SELECT article_comment.*, m_user_base.firstname, m_user_base.lastname FROM article_comment, m_user_base WHERE article_id = $rowid AND m_user_base.rowid = article_comment.user_id");
    
    $article->{comments}        = $comments;
    $article->{comments_number} = scalar @$comments;
    
    return $article;
}

# ============================================================================

=head2 DBInsertArticle (I<$context>)

=over

=item Database method to insert a new article.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBInsertArticle {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $user_id         = $context->GetUser()->GetRowid();
    my $values          = $options{'values'};
    
    BeginTransaction($dbh);
    try {
        my $prefs = $self->DBGetOptions($context);
        
        my $status_label = $options{status} || PUBLISH;
        
        my $status = SelectSingle($dbh, "SELECT rowid FROM article_and_news_status WHERE label='$status_label'");
        
        my $sql = "INSERT INTO article(created,modified,title,header,contents,owner_id,group_id,status_id) VALUES(NOW(),NOW(),'" . st_FormatPostgreSQLString($values->{title}) . "','" . st_FormatPostgreSQLString($values->{header}) . "','" . st_FormatPostgreSQLString($values->{contents}) . "',$user_id,$group_id," . $status->{rowid} . ")";
        
        ExecSQL($dbh, $sql);
        my $article_id = GetLastInsertId($dbh, "article");

		if ($status_label eq PUBLISH) {
			ExecSQL ($dbh, "UPDATE article SET published=NOW() WHERE rowid=$article_id");
		}
        
        if (ref($values->{categories}) eq 'ARRAY') {
            foreach (@{$values->{categories}}) {
                ExecSQL($dbh, "INSERT INTO article_category(category_id,article_id) VALUES($_,$article_id)");
            }
        }
        else {
            ExecSQL($dbh, "INSERT INTO article_category(category_id,article_id) VALUES(" . $values->{categories} . ",$article_id)");
        }
        
        if ($options{link_to}) {
            ExecSQL($dbh, "INSERT INTO article_moderation(new_id, old_id) VALUES($article_id,".$options{link_to}.")");
        }
    }
    otherwise {
        my $ex = shift;
        warn Dumper($ex);
        
        RollbackTransaction($dbh);
        
        $ex->throw();
    };
    EndTransaction($dbh);
}

# ============================================================================

=head2 DBCreateCategory (I<$context>, I<%options>)

=over

=item Database method to create a new category.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBCreateCategory {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $values          = $options{'values'};
    
    BeginTransaction($dbh);
    try {
        my $prefs = $self->DBGetOptions($context);
        
        my $catg_exists = SelectSingle($dbh, "SELECT count(*) FROM article_category_list WHERE article_pref_id=" . $prefs->{rowid} . " AND label='" . st_FormatPostgreSQLString($values->{label}) . "'");
        
        if (int($catg_exists->{count}) > 0 ) {
            throw Mioga2::Exception::DB("Articles::DBCreateCategory", $self->GetText("Category already exists !"));
        }
        
        my $sql = "INSERT INTO article_category_list(created,modified,label,description,article_pref_id) VALUES(NOW(),NOW(),'" . st_FormatPostgreSQLString($values->{label}) . "','" . st_FormatPostgreSQLString($values->{description}) . "'," . $prefs->{rowid} . ")";
        
        ExecSQL($dbh, $sql);
    }
    otherwise {
        my $ex = shift;
        warn Dumper($ex);
        
        RollbackTransaction($dbh);
        
        $ex->throw();
    };
    EndTransaction($dbh);
}

# ============================================================================

=head2 DBDeleteArticle (I<$context>, I<%options>)

=over

=item Database method to delete an existing article.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBDeleteArticle {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $values          = $options{'values'};
    
    BeginTransaction($dbh) unless ($options{no_transaction});
    try {
        my $moderated = SelectSingle($dbh, "SELECT * FROM article_moderation WHERE old_id=" . $values->{rowid});
        
        if (defined ($moderated)) {
            ExecSQL($dbh, "DELETE FROM article_moderation WHERE old_id=" . $values->{rowid});
            ExecSQL($dbh, "DELETE FROM article_category WHERE article_id=" . $moderated->{new_id});
            ExecSQL($dbh, "DELETE FROM article_comment WHERE article_id=$moderated->{new_id}");
            ExecSQL($dbh, "DELETE FROM article WHERE group_id=$group_id AND rowid=" . $moderated->{new_id});
        }
        ExecSQL($dbh, "DELETE FROM article_moderation WHERE new_id=" . $values->{rowid});
        ExecSQL($dbh, "DELETE FROM article_category WHERE article_id=" . $values->{rowid});
        ExecSQL($dbh, "DELETE FROM article_comment WHERE article_id=$values->{rowid}");
        ExecSQL($dbh, "DELETE FROM article WHERE group_id=$group_id AND rowid=" . $values->{rowid});
    }
    otherwise {
        my $ex = shift;
        warn Dumper($ex);
        
        RollbackTransaction($dbh) unless ($options{no_transaction});
        
        $ex->throw();
    };
    EndTransaction($dbh) unless ($options{no_transaction});
}

# ============================================================================

=head2 DBDeclineArticle (I<$context>, I<%options>)

=over

=item Database method to decline an article.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBDeclineArticle {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $values          = $options{'values'};
    
    my $draft_status = SelectSingle($dbh, "SELECT * FROM article_and_news_status WHERE label='" . st_FormatPostgreSQLString(DRAFT) . "'");
    ExecSQL($dbh, "UPDATE article SET status_id=" . $draft_status->{rowid} . " WHERE rowid=" . $values->{rowid});
}

# ============================================================================

=head2 DBDeleteCategory (I<$context>, I<%options>)

=over

=item Database method to delete an existing category.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBDeleteCategory {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $values          = $options{'values'};
    
    ExecSQL($dbh, "DELETE FROM article_category_list USING article_pref WHERE article_pref_id=article_pref.rowid AND article_pref.group_id=$group_id AND article_category_list.rowid=" . $values->{rowid});
}

# ============================================================================

=head2 DBUpdateCategory (I<$context>, I<%options>)

=over

=item Database method to update an existing category.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBUpdateCategory {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $values          = $options{'values'};
    
    my $prefs = $self->DBGetOptions($context);
    
    my $catg = SelectSingle($dbh, "SELECT COUNT(*) FROM article_category_list WHERE article_pref_id=" . $prefs->{rowid} . " AND label='" . st_FormatPostgreSQLString($values->{value}) . "'");

    if ($catg->{count} > 0) {
        throw Mioga2::Exception::DB("Articles::DBUpdateCategory", __"Category already exists !");
    }
    
    ExecSQL($dbh, "UPDATE article_category_list SET modified=NOW(), label='" . st_FormatPostgreSQLString($values->{value}) . "' WHERE article_pref_id=" . $prefs->{rowid} . " AND rowid=" . $values->{rowid});
}

# ============================================================================

=head2 DBUpdateArticle (I<$context>, I<%options>)

=over

=item Database method to update an existing article.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBUpdateArticle {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $user_id         = $context->GetUser()->GetRowid();
    my $values          = $options{'values'};
    
    BeginTransaction($dbh) unless ($options{no_transaction});
    try {
        my $status_id;
        my $rowid = ($options{update_old} == 1) ? $values->{old_id} : $values->{rowid};
        
        if ($options{status}) {
            $status_id = SelectSingle($dbh, "SELECT * FROM article_and_news_status WHERE label='".st_FormatPostgreSQLString($options{status})."'");
            $status_id = $status_id->{rowid};
        }
		my $oldstatus = SelectSingle ($dbh, "SELECT label FROM article_and_news_status WHERE rowid=(SELECT status_id FROM article WHERE rowid=$rowid)")->{label};
        
        my $sql = "UPDATE article SET modified=NOW(), title='".st_FormatPostgreSQLString($values->{title})."', header='".st_FormatPostgreSQLString($values->{header})."', contents='".st_FormatPostgreSQLString($values->{contents})."'";
        $sql .= ", status_id=$status_id" if ($status_id);
        $sql .= ", moderator_id=".$options{moderator} if ($options{moderator});
        $sql .= ", published=NOW()" if (($options{status} eq PUBLISH) && ($oldstatus ne PUBLISH));
        $sql .= " WHERE rowid=$rowid";
        
        ExecSQL($dbh, $sql);
        ExecSQL($dbh, "DELETE FROM article_category WHERE article_id=$rowid");
        
        if (ref($values->{categories}) eq 'ARRAY') {
            foreach (@{$values->{categories}}) {
                my $catg = $_;
                $catg = $catg->{rowid} if (ref($catg) eq 'HASH');
                
                ExecSQL($dbh, "INSERT INTO article_category(category_id,article_id) VALUES($catg,$rowid)");
            }
         }
         else {
            my $catg = $values->{categories};
            $catg = $catg->{rowid} if (ref($catg) eq 'HASH');
            ExecSQL($dbh, "INSERT INTO article_category(category_id,article_id) VALUES($catg,$rowid)");
         }
     }
     otherwise {
         my $ex = shift;
         warn Dumper($ex);

         RollbackTransaction($dbh) unless ($options{no_transaction});

         $ex->throw();
     };
     EndTransaction($dbh) unless ($options{no_transaction});
}

# ============================================================================

=head2 DBUpdateOptions (I<$context>, I<%options>)

=over

=item Database method to delete an existing category.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBUpdateOptions {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $values          = $options{'values'};
    
    my $sql = "UPDATE article_pref SET " . join(",", map({ "$_=".$values->{$_} } keys %$values));
    $sql =~ s/0/'f'/g;
    $sql =~ s/1/'t'/g;
    
    $sql .= ", modified=NOW() WHERE group_id=$group_id";
    
    ExecSQL($dbh, $sql);
}

# ============================================================================

=head2 DBValidateArticle (I<$context>, I<%options>)

=over

=item Database method to validate an article.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBValidateArticle {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $mod_id          = $context->GetUser()->GetRowid();
    my $values          = $options{'values'};
    
    BeginTransaction($dbh);
    try {
        my $update_old = ($values->{old_id}) ? 1 : 0;
        $self->DBUpdateArticle($context, 'values' => $values, status => PUBLISH, update_old => $update_old, no_transaction => 1, moderator => $context->GetUser()->GetRowid());
        $self->DBDeleteArticle($context, 'values' => $values, no_transaction => 1) if ($values->{old_id});
    }
    otherwise {
        my $ex = shift;
        warn Dumper($ex);
        
        RollbackTransaction($dbh);
        
        $ex->throw();
    };
    EndTransaction($dbh);
}

# ============================================================================

=head2 DBGetOptions (I<$context>)

=over

=item Database method to retrieve options.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> list of hashes containing data.

=back

=cut

# ============================================================================

sub DBGetOptions {
    my ($self, $context) = @_;
    
    my $group_id = $context->GetGroup()->GetRowid();
    my $dbh = $context->GetConfig()->GetDBH();
    my $result;

    my $sql = "SELECT * FROM article_pref WHERE group_id = $group_id";
    $result = SelectSingle($dbh, $sql);

    if (not defined $result) {
        ExecSQL($dbh, "INSERT INTO article_pref(created,modified,group_id) VALUES(NOW(),NOW(),$group_id)");
        $result = SelectSingle($dbh, $sql);
        
        throw Mioga2::Exception::Application("Articles::DBGetOptions", __"Options don't exist after INSERT !") if (not defined $result);
    }
    
    return $result;
}

# ============================================================================

=head2 RevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

	Remove user data in database when a user is revoked of a group.

	This function is called when :
	- a user is revoked of a group
	- a team is revoked of a group for each team member not namly
	  invited in group
	- a user is revoked of a team  for each group where the team is
	  member and the user not namly invited in group

=cut

# ============================================================================

sub RevokeUserFromGroup {
	my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;
    
    my $dbh     = $config->GetDBH();
    ExecSQL($dbh, "UPDATE article SET owner_id=$group_animator_id WHERE owner_id=$user_id AND group_id=$group_id");
    ExecSQL($dbh, "UPDATE article_comment SET user_id=$group_animator_id FROM article WHERE article_comment.user_id=$user_id AND article_comment.article_id=article.rowid AND article.group_id=$group_id");
}

# ============================================================================

=head2 DeleteGroupData ($config, $group_id)

	Remove group data in database when a group is deleted.

	This function is called when a group/user/resource is deleted.

=cut

# ============================================================================

sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;
    
    my $dbh = $config->GetDBH();

    ExecSQL($dbh, "DELETE FROM article_category USING article WHERE article_category.article_id=article.rowid AND article.group_id=$group_id");
    ExecSQL($dbh, "DELETE FROM article_category_list USING article_pref WHERE article_category_list.article_pref_id=article_pref.rowid AND article_pref.group_id=$group_id");
    ExecSQL($dbh, "DELETE FROM article_comment USING article WHERE article_comment.article_id=article.rowid AND article.group_id=$group_id");
    ExecSQL($dbh, "DELETE FROM article WHERE group_id=$group_id");
    ExecSQL($dbh, "DELETE FROM article_pref WHERE group_id=$group_id");
}

# ============================================================================

=head2 GetStateFromPage (I<$page>)

=over

=item Determine state from given page.

=item B<parameters :>

=over

=item I<$page> : the page.

=back

=item B<return :> return state accordingly to page.

=back

=cut

# ============================================================================

sub GetStateFromPage {
    my ($self, $page) = @_;
    
    my $state;
    $state = PUBLISH    if ($page eq D_PUBLISH);
    $state = WAITING    if ($page eq D_WAITING);
    $state = DRAFT      if ($page eq D_DRAFT);
    
    return $state;
}

# ============================================================================

=head2 SaveArticle ()

=over

=item save an article as html to an user's space.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub SaveArticle {
    my ($self, $context) = @_;
    my $error   = undef;
    my $err_403 = __"You don't have permission to write at this location.";
    my $err_405 = __"The path you provided doesn't exist.";
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'uri'], 'stripxws', 'disallow_empty'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Articles::SaveArticle", ac_FormatErrors($errors));
	}
	my $uri = $values->{uri};

    #throw Mioga2::Exception::Application("Articles::SaveArticle", __"'uri' was not given or was empty!") unless ($context->{args}->{uri} and $context->{args}->{uri} !~ /^\s*$/);
    
    $uri            =~ s/^([^\/].*)$/\/$1/;
    
    if ($uri =~ /\/$/) {
        $error = __"Please provide a filename.";
    }
    elsif ($uri !~ /\.htm(l)?$/) {
        $uri .= ".html";
    }
    
    unless ($error) {
        my $config      = $context->GetConfig();
        my $base_uri    = $config->GetProtocol() . "://" . $config->GetServerName() . $config->GetBaseURI();
        my $article     = $context->GetSession()->{article};
        my $art_xml     = '<?xml version="1.0" encoding="UTF-8"?>';
        $art_xml       .= $self->XMLGetArticle($context, article => $article, add_context => 1);
        
        my $art_content = new Mioga2::Content::XSLT($context, stylesheet => "articles.xsl", locale_domain => "articles_xsl");
        $art_content->SetContent($art_xml);
        my $html    = $art_content->XSLTransform();
        
        $error = Mioga2::Magellan::DAV::PutURI($context->GetUserAuth (), $html, $base_uri.$uri, "text/html");
        $error = undef if ($error =~ /2\d{2}/);
        $error = $err_403 if ($error =~ /403/);
        $error = $err_405 if ($error =~ /405/);
    }
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<SaveArticle>";
    $xml   .= $context->GetXML;
    
    $xml   .= "<errors><Description>" . st_FormatXMLString($error) . "</Description></errors>" if ($error);
    
    $xml   .= "</SaveArticle>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'articles-js.xsl', locale_domain => "articles_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 GetRSSFeed ($context, [$feed])

  Return a feed of articles.

=cut

# ============================================================================

sub GetRSSFeed {
    my ($self, $context, $ident, $feed, $max)   = @_;
    my $config  = $context->GetConfig;
    my $group;
    try {
        $group = Mioga2::Old::Group->new($config, ident => $ident);
    }
    otherwise {
        $group = Mioga2::Old::User->new($config, ident => $ident);
    };
    
    if (!$feed) {
        $feed   = XML::Atom::SimpleFeed->new(
            title   => __x("Articles feed of {group} group", group => $group->GetIdent),
            link    => { href => $config->GetProtocol . "://" . $config->GetServerName . $context->{uri}->GetURI, rel => 'self' },
            link    => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/Articles/DisplayMain",
            id      => "data:,group:".$group->GetIdent.":articles",
        );
    }
    $max    = 20 unless $max;
    
    my $articles    = $self->DBGetArticles($context, feed => 1, max => $max, group => $group);
    foreach my $article (@$articles) {
        my @categories  = map { $_->{label} } @{$article->{categories}};
        $feed->add_entry(
            title       => __x("[{group}] Article: {title}", title => $article->{title}, group => $group->GetIdent),
            summary     => $article->{header},
            link        => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/Articles/ViewArticle?article_id=$article->{rowid}",
            id          => "data:,group:".$group->GetIdent.":articles:$article->{rowid}",
            author      => "$article->{firstname} $article->{lastname}",
            published   => Mioga2::Classes::Time->FromPSQL($article->{created})->RFC3339DateTime,
            updated     => Mioga2::Classes::Time->FromPSQL($article->{modified})->RFC3339DateTime,
            content     => $article->{contents},
            category    => join(',', @categories),
        );
    }
    return $feed;
}

# ============================================================================
# Comments ($comments)
# make a tree of comments
# ============================================================================
sub Comments {
    my ( $self, $comments, $comment_id ) = @_;

    my $items = [];
    @$items  = grep { $_->{parent_id} == $comment_id } @$comments;
    foreach my $parent (@$items) {
        my $datetime        = Mioga2::Classes::Time->FromPSQL($parent->{created});
        $parent->{Comments} = [];
        $parent->{date}     = $datetime->strftime("%x");
        $parent->{time}     = $datetime->strftime("%X");
        $parent->{author}   = "$parent->{firstname} $parent->{lastname}";
        push @{$parent->{Comments}}, @{$self->Comments($comments, $parent->{rowid})};
    }
    return $items;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2006, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
