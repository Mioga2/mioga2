#===============================================================================
#
#         FILE:  GroupList.pm
#
#  DESCRIPTION:  New generation Mioga2 group list class
#
# REQUIREMENTS:  ---
#        NOTES:  TODO Rewrite the Store and Delete methods so this module is
#				 	  independant from APIGroup
#				 TODO Handle parent directories creation in CreateDirectory
#				      and CreateFile methods
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  16/04/2010 11:24
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

GroupList.pm: New generation Mioga2 grouplist class

=head1 DESCRIPTION

This class handles Mioga2 groups. It is designed to create, load, update or delete
groups.

=head1 SYNOPSIS

 use Mioga2::GroupList;
 use Mioga2::Config;

 my $grouplist;
 my $config = new Mioga2::Config (...);

 # Set all groups whose admin is jdoe as autonomous
 $grouplist = Mioga2::GroupList->new ($config, { attributes => { admin => 'jdoe' } });
 $grouplist->Set ('autonomous', 1);
 $grouplist->Store ();

 # Delete group whose ident is mygroup
 $grouplist = Mioga2::GroupList->new ($config, { attributes => { ident => 'mygroup' } });
 $grouplist->Delete ();

 # Create a new group from attributes and skeleton
 $grouplist = Mioga2::GroupList->new ($config, {
 		attributes => {
 			ident => 'mygroup',
 			skeleton => '/path/to/std_group.xml'
		}
 	});
 $grouplist->Store ();

 # Get groups from which user "jdoe" can run "Haussmann"
 $userlist  = Mioga2::UserList->new ($config, { attributes => { ident => 'jdoe' } });
 $applist   = Mioga2::ApplicationList->new ($config, { attributes => { ident => 'Haussmann' } });
 $grouplist = Mioga2::GroupList->new ($config, { attributes => { expanded_users => $userlist, applications => $applist } });

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::GroupList;
use base qw(Mioga2::Old::Compatibility);

use Data::Dumper;

use File::Copy;
use File::MimeInfo::Magic;

use Locale::TextDomain::UTF8 'grouplist';

use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIApplication;
use Mioga2::tools::APIAuthz;
use Error qw(:try);
use Mioga2::Exception::Group;
use Mioga2::UserList;
use Mioga2::TeamList;
use Mioga2::ApplicationList;
use Mioga2::CSV;
use Mioga2::DAVFSUtils;
use Mioga2::Journal;
use Mioga2::Constants;

my $debug = 0;

# List of text fields that can not be matched by a IS NULL, but by a = '' instead
my @text_fields = qw/description ident animator/;

@Mioga2::GroupList::ProtectedApplications = qw/Fouquet/;

#===============================================================================

=head2 new

Create a new Mioga2::GroupList object

=over

=item B<arguments:>

=over

=item I<$config:> a Mioga2::Config object matching the instance to work into.

=item I<$data:> a hashref with the following keys:

=over

=item I<attributes:> a list of key / value pairs matching the groups. Key
names correspond to m_group table field names. Other keys are supported:

=over

=item skeleton: the path to a skeleton file to apply to group(s).

=back

=item I<short_list:> an optional flag to limit the amount of data describing the groups.

=item I<fields:> a list of fields describing the groups (this overrides the behavior of the flag above).

=item I<match:> a string indicating the match type for groups (may be 'begins', 'contains', 'ends' or may be empty for exact match).

=item I<journal:> a Mioga2::Journal-tied-array to record operations.

=back

=back

=item B<return:> a Mioga2::GroupList object matching groups according
to provided attributes.

=back

=cut

#===============================================================================
sub new {
	my ($class, $config, $data) = @_;
	print STDERR "[Mioga2::GroupList::new] Entering, data: " . Dumper $data if ($debug);

	my $self = { };
	bless($self, $class);

	$self->{config} = $config;
	$self->{db} = $config->GetDBObject ();

	# Initialize attributes
	$self->{attributes} = $data->{attributes};

	# Initialize object state
	$self->{loaded} = 0;
	$self->{deleted} = 0;

	# List of protected applications that can not be disabled for group animator
	$self->{protected_applications} = ();
	for (@Mioga2::GroupList::ProtectedApplications) {
		push (@{$self->{protected_applications}}, $_);
	}

	# Fields obtained from DB can be expressed in a fuzzy way (short_list) initializing a pre-defined field-list
	if (exists ($data->{short_list}) && $data->{short_list}) {
		# Restricted list of fields
		$self->{fields} = "m_group.rowid, m_group.ident, m_group.disk_space_used, m_group.created, lc.last_connection";
		$self->{short_list} = delete ($data->{short_list});
	}
	else {
		# Full list of fields
		$self->{fields} = 'm_group.*, m_lang.ident AS lang, m_theme.ident AS theme_ident, m_theme.name AS theme, lc.last_connection';
		$self->{short_list} = delete ($data->{short_list});
	}

	# Match type can be 'begins', 'contains' or 'ends' to find user from a part of their attributes
	if (defined ($data->{match})) {
		my $match = $data->{match};
		if (grep (/^$match$/, qw/begins contains ends/)) {
			$self->{match} = $data->{match};
		}
		delete ($data->{match});
	}

	# Fields obtained from DB can be chosen by caller
	if (defined ($data->{fields})) {
		$self->{fields} = delete ($data->{fields});
	}

	# Journal of operations
	if (defined ($data->{journal})) {
		$self->{journal} = $data->{journal};
	}
	else {
		tie (@{$self->{journal}}, 'Mioga2::Journal');
	}

	# Initialize WHERE statement before any update is made on the list
	$self->SetWhereStatement (keys (%{$self->{attributes}}));

	# Process skeleton
	if (exists ($self->{attributes}->{skeleton})) {
		my $status = 1;
		try {
			$self->LoadSkeleton ();
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, {module => 'Mioga2::GroupList', step => __('Skeleton loading'), status => $status});
		};
	}

	print STDERR "[Mioga2::GroupList::new] Leaving\n" if ($debug);
	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 Set

Set an attribute value.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to set.

=item I<$value:> the attribute value to set.

=back

=back

=cut

#===============================================================================
sub Set {
	my ($self, $attribute, $value) = @_;

	$self->{update}->{$attribute} = $value;
}	# ----------  end of subroutine Set  ----------


#===============================================================================

=head2 Get

Get an attribute value.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get.

=back

=item B<return:> the attribute value.

=back

=cut

#===============================================================================
sub Get {
	my ($self, $attribute) = @_;
	print STDERR "[Mioga2::GroupList::Get] Entering, attribute name is $attribute\n" if ($debug);

	my $value = undef;

	#-------------------------------------------------------------------------------
	# Load the object from DB if attribute is not known already
	#-------------------------------------------------------------------------------
	if ((!grep /^$attribute$/, keys (%{$self->{attributes}})) && (!grep /^$attribute$/, keys (%{$self->{update}})) && !$self->{loaded}) {
		# The requested attribute is not set into object yet, get it from DB
		$self->Load ();
	}

	#-------------------------------------------------------------------------------
	# Retreive attribute value
	#-------------------------------------------------------------------------------
	if (exists ($self->{update}->{$attribute})) {
		# Attribute is in update list, get its value
		$value = $self->{update}->{$attribute};
	}
	elsif (exists ($self->{attributes}->{$attribute})) {
		# Attribute is in initial list, get its value
		$value = $self->{attributes}->{$attribute};
	}
	else {
		# Attribute is not in initial list, nor in update list, pick it from loaded groups
		if (scalar (@{$self->{groups}}) > 1) {
			# Multiple groups in the list, return an array of attribute values.
			$value = ();
			for (@{$self->{groups}}) {
				push (@$value, $_->{$attribute});
			}
		}
		else {
			# Only one group in the list, return attribute value
			$value = $self->{groups}->[0]->{$attribute};
		}
	}

	# Add '%' signs according to match attribute
	if (exists ($self->{match}) && (ref ($value) eq '')) {
		if ($self->{match} eq 'begins') {
			$value = "$value%";
		}
		elsif ($self->{match} eq 'contains') {
			$value = "%$value%";
		}
		elsif ($self->{match} eq 'ends') {
			$value = "%$value";
		}
		else {
			throw Mioga2::Exception::Simple ('Mioga2::GroupList::RequestForAttribute', "Unknown match type: $self->{match}");
		}
	}

	if (ref ($value) eq 'ARRAY') {
		print STDERR "[Mioga2::GroupList::Get] Leaving, attribute value is " . Dumper $value if ($debug);
	}
	elsif (ref ($value) eq 'Mioga2::UserList') {
		my @rowids = $value->GetRowIds ();
		print STDERR "[Mioga2::GroupList::Get] Leaving, attribute value is truncated, Mioga2::UserList rowids " . Dumper \@rowids if ($debug);
	}
	elsif (ref ($value) eq 'Mioga2::TeamList') {
		my @rowids = $value->GetRowIds ();
		print STDERR "[Mioga2::GroupList::Get] Leaving, attribute value is truncated, Mioga2::TeamList rowids " . Dumper \@rowids if ($debug);
	}
	elsif (ref ($value) eq 'Mioga2::ApplicationList') {
		my @rowids = $value->GetRowIds ();
		print STDERR "[Mioga2::GroupList::Get] Leaving, attribute value is truncated, Mioga2::ApplicationList rowids " . Dumper \@rowids if ($debug);
	}
	else {
		print STDERR "[Mioga2::GroupList::Get] Leaving, attribute value is " . ($value ? $value : 'undef') . "\n" if ($debug);
	}
	return ($value);
}	# ----------  end of subroutine Get  ----------


#===============================================================================

=head2 Store

Store changes to database. This method has to be called to actually create or
update one or more groups. To delete groups, see the "Delete" method.

=cut

#===============================================================================
sub Store {
	my ($self) = @_;
	print STDERR "[Mioga2::GroupList::Store] Entering\n" if ($debug);

	if ((!$self->Count ()) && (!$self->{deleted})) {
		#-------------------------------------------------------------------------------
		# Create a group into DB
		# TODO this part has to be rewritten to be completely autonomous from APIGroup
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::GroupList::Store] Creating new group\n" if ($debug);
		$self->ApplySkeleton (qw/attributes/);

		# Set creator and anim IDs to instance admin if not defined
		$self->Set ('creator_id', $self->{config}->GetAdminId ()) unless ($self->Has ('creator_id'));
		$self->Set ('anim_id', $self->{config}->GetAdminId ()) unless ($self->Has ('anim_id'));

		my $status = 1;
		try {
			GroupCreate ($self->{config}, $self->GetDatabaseValues (), 1); # TODO (SNI - 21/04/2010 08:54): Handle this inside the module
		}
		otherwise {
			my $err = shift;
			$status = 0;
			push (@{$self->{journal}}, { module => 'Mioga2::GroupList', step => $err->{"-text"}, status => $status});
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::GroupList', step => __x('Group "{ident}" creation', ident => $self->Get ('ident')), status => $status });
		};
		$self->ApplySkeleton (qw/applications profiles teams filesystem/);
	}
	elsif (scalar (keys (%{$self->{update}}))) {
		#-------------------------------------------------------------------------------
		# Update group
		# TODO this part has to be rewritten to be completely autonomous from APIGroup
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::GroupList::Store] Updating existing group\n" if ($debug);

		for my $group ($self->GetGroups ()) {
			my $dbvals = $self->GetDatabaseValues ();
			for (keys (%$dbvals)) {
				$group->{$_} = $dbvals->{$_};
			}
			my $status = 1;
			try {
				GroupModify ($self->{config}, $group, 1); # TODO (SNI - 21/04/2010 08:54): Handle this inside the module
			}
			otherwise {
				my $err = shift;
				$status = 0;
				throw $err;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::GroupList', step => __x('Group "{ident}" update', ident => $self->Get ('ident')), status => $status });
			};
		}
	}
	else {
		print STDERR "[Mioga2::GroupList::Store] Nothing has to be done\n" if ($debug);
	}

	#-------------------------------------------------------------------------------
	# Update WHERE statement, as some modifications were made, groups can not be
	# matched anymore by the same WHERE statement, matching according to rowids
	#-------------------------------------------------------------------------------
	$self->SetWhereStatement (qw/rowid/);

	#-------------------------------------------------------------------------------
	# Update-pending values are now stored into DB, they can be moved to the inner
	# attributes of the object
	#-------------------------------------------------------------------------------
	for (keys (%{$self->{update}})) {
		$self->{attributes}->{$_} = delete ($self->{update}->{$_});
	}

	print STDERR "[Mioga2::GroupList::Store] Leaving\n" if ($debug);
}	# ----------  end of subroutine Store  ----------


#===============================================================================

=head2 Count

Count the groups in the list. This method loads the group list from DB if
required.

=over

=item B<return:> the number of groups in the list.

=back

=cut

#===============================================================================
sub Count {
	my ($self) = @_;
	print STDERR "[Mioga2::GroupList::Count] Entering\n" if ($debug);

	my $count = undef;

	if ($self->{loaded}) {
		# Get count from loaded items
		$count = scalar (@{$self->{groups}});
	}
	else {
		# Get count from dedicated request
		my $sql = "SELECT count(*) FROM m_group, " . $self->GetAdditionnalTables () . " WHERE " . $self->GetWhereStatement ();
		$count = SelectSingle ($self->{config}->GetDBH (), $sql)->{count};
	}

	print STDERR "[Mioga2::GroupList::Count] Leaving: $count groups\n" if ($debug);
	return ($count);
}	# ----------  end of subroutine Count  ----------


#===============================================================================

=head2 GetRowIds

Get RowIds for groups in the list.

=over

=item B<return:> an array of rowids.

=back

=cut

#===============================================================================
sub GetRowIds {
	my ($self) = @_;

	my @list = ();

	$self->Load () unless $self->{loaded};

	for (@{$self->{groups}}) {
		push (@list, $_->{rowid});
	}

	return (@list);
}	# ----------  end of subroutine GetRowIds  ----------


#===============================================================================

=head2 GetGroups

Get list of groups in the list.

=over

=item B<return:> an array of hashes describing the groups in the list. Each hash
of this array can be used to create a new Mioga2::GroupList that
only matches this group.

=back

=cut

#===============================================================================
sub GetGroups {
	my ($self) = @_;
	print STDERR "[Mioga2::GroupList::GetGroups] Entering\n" if ($debug);

	$self->Load () unless $self->{loaded};

	for my $group (@{$self->{groups}}) {
		for (keys (%{$self->{update}})) {
			$group->{$_} = $self->{update}->{$_};
		}
	}

	print STDERR "[Mioga2::GroupList::GetGroups] Groups: " . Dumper $self->{groups} if ($debug);
	return (@{$self->{groups}});
}	# ----------  end of subroutine GetGroups  ----------


#===============================================================================

=head2 Delete

Delete groups.

=cut

#===============================================================================
sub Delete {
	my ($self) = @_;
	print STDERR "[Mioga2::GroupList::Delete] Entering\n" if ($debug);

	my $rowids = join (', ', @{ac_ForceArray ($self->Get ('rowid'))});
	print STDERR "[Mioga2::GroupList::Delete] Group rowids to delete: $rowids\n" if ($debug);

	if ($self->Count ()) {
		# Clear profiles
		ExecSQL ($self->{config}->GetDBH (), "DELETE FROM m_profile_group USING m_profile WHERE m_profile_group.profile_id = m_profile.rowid AND m_profile.group_id IN ($rowids);");

		for ($self->GetGroups ()) {
			print STDERR "[Mioga2::GroupList::Delete] Deleting group rowid $_->{rowid}, mioga_id $_->{mioga_id} (ident '$_->{ident}')\n" if ($debug);
			my $status = 1;
			try {
				GroupDelete ($self->{config}, $_->{rowid}, 1); # TODO (SNI - 21/04/2010 08:54): Handle this inside the module
			}
			otherwise {
				my $err = shift;
				$status = 0;
				throw $err;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::GroupList', step => __x('Group "{ident}" deletion', ident => $_->{ident}), status => $status });
			};
		}

		$self->{deleted} = 1;
	}

	print STDERR "[Mioga2::GroupList::Delete] Leaving\n" if ($debug);
}	# ----------  end of subroutine Delete  ----------


#===============================================================================

=head2 InviteTeams

Invite teams into groups.

=over

=item B<attributes:>

=over

=item I<@teams:> an array of team idents.

=back

=back

=cut

#===============================================================================
sub InviteTeams {
	my ($self, %teams) = @_;
	print STDERR "[Mioga2::GroupList::InviteTeams] Entering\n" if ($debug);

	my $where = $self->GetWhereStatement ();
	my $mioga_id = $self->{config}->GetMiogaId ();
	my $teams_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_) } keys (%teams));
	
	# Revoke teams from all groups they were member of and are not anymore
	for my $group_attrs ($self->GetGroups ()) {
		my $group_ident = $group_attrs->{ident};
		my $group = Mioga2::GroupList->new ($self->{config}, { attributes => { ident => $group_ident }, journal => $self->{journal} });
		for my $team_attrs ($group->GetInvitedTeamList ()->GetTeams ()) {
			my $team_ident = $team_attrs->{ident};
			if (!grep (/^$team_ident$/, keys (%teams))) {
	 			LaunchMethodInApps($self->{config}, "RevokeTeamFromGroup", $group_attrs->{rowid}, $team_attrs->{rowid}, $group_attrs->{anim_id});

				# Revoke each team member that is not namely member of the group
				my $team = Mioga2::TeamList->new ($self->{config}, { attributes => { ident => $team_ident }, journal => $self->{journal} });
				my $teamusers = ac_ForceArray ($team->GetInvitedUserList ()->Get ('rowid'));
				my $group_id = $group_attrs->{rowid};
				my $group_anim = $group_attrs->{anim_id};
				my $group = Mioga2::GroupList->new ($self->{config}, { attributes => { rowid => $group_id }, journal => $self->{journal} });
				my $groupusers = ac_ForceArray ($group->GetInvitedUserList ()->Get ('rowid'));
				for my $user_id (@$teamusers) {
					next if (grep (/^$user_id$/, @$groupusers));
					LaunchMethodInApps($self->{config}, "RevokeUserFromGroup", $group_id, $user_id, $group_anim);
				}
			}
		}
	}

	# Revoke not specified teams
	my $sql = "DELETE FROM m_profile_group USING m_profile, m_group, m_group_base, " . $self->GetAdditionnalTables () . " WHERE m_profile_group.group_id = m_group_base.rowid AND m_profile_group.profile_id = m_profile.rowid AND m_profile.group_id = m_group.rowid" . (($teams_idents eq '') ? '' : " AND m_group_base.ident NOT IN ($teams_idents)") . " AND m_group_base.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'team') AND $where;";
	ExecSQL ($self->{config}->GetDBH (), $sql);
	$sql = "DELETE FROM m_group_group USING m_group, m_group_base, " . $self->GetAdditionnalTables () . " WHERE m_group_group.group_id = m_group.rowid AND m_group_group.invited_group_id = m_group_base.rowid" . (($teams_idents eq '') ? '' : " AND m_group_base.ident NOT IN ($teams_idents)") . " AND m_group_base.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'team') AND m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'group') AND $where;";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	if ($teams_idents ne '') {
		# Invite teams into groups
		$sql = "INSERT INTO m_group_group SELECT m_group.rowid AS group_id, m_group_base.rowid AS invited_group_id FROM m_group, m_group_base, " . $self->GetAdditionnalTables () . " WHERE m_group_base.ident IN ($teams_idents) AND m_group_base.mioga_id = $mioga_id AND $where AND m_group_base.rowid NOT IN (SELECT invited_group_id FROM m_group_group WHERE group_id = m_group.rowid);";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Affect profiles to teams
		for my $team_ident (keys (%teams)) {
			my $profile_ident = ($teams{$team_ident} ne '') ? $self->{config}->GetDBH()->quote ($teams{$team_ident}) : $self->{config}->GetDBH ()->quote (SelectSingle ($self->{config}->GetDBH (), "SELECT m_profile.ident, 2 AS weight FROM m_profile, m_profile_group, m_group_base, m_group, " . $self->GetAdditionnalTables () . " WHERE m_profile_group.group_id = m_group_base.rowid AND m_profile.group_id = m_group.rowid AND m_profile_group.profile_id = m_profile.rowid AND $where AND m_group.mioga_id = $mioga_id AND m_group_base.ident = " . $self->{config}->GetDBH ()->quote ($team_ident) . " AND m_group_base.mioga_id = $mioga_id UNION SELECT m_profile.ident, 1 AS weight FROM m_profile, m_group, " . $self->GetAdditionnalTables () . " WHERE m_profile.rowid = m_group.default_profile_id AND $where AND m_group.mioga_id = $mioga_id ORDER BY weight DESC LIMIT 1")->{ident});

			# Delete any existing profile affectation
			$sql = "DELETE FROM m_profile_group USING m_group_base WHERE profile_id IN (SELECT m_profile.rowid FROM m_profile, m_profile_group, m_group, " . $self->GetAdditionnalTables () . " WHERE m_profile.group_id = m_group.rowid AND m_profile_group.profile_id = m_profile.rowid AND $where AND m_group.mioga_id = $mioga_id AND m_profile_group.group_id IN (SELECT m_group_base.rowid FROM m_group_base WHERE ident = '$team_ident')) AND m_profile_group.group_id = m_group_base.rowid AND m_group_base.ident = '$team_ident';";
			ExecSQL ($self->{config}->GetDBH (), $sql);

			# Create new profile affectation
			$sql = "INSERT INTO m_profile_group SELECT m_profile.rowid AS profile_id, m_group_base.rowid AS group_id FROM m_profile, m_group, m_group_base, " . $self->GetAdditionnalTables () . " WHERE m_profile.group_id = m_group.rowid AND m_group_base.ident = " . $self->{config}->GetDBH()->quote ($team_ident) . " AND m_group_base.mioga_id = $mioga_id AND m_group.mioga_id = $mioga_id AND $where AND m_profile.ident = $profile_ident;";
			ExecSQL ($self->{config}->GetDBH (), $sql);
		}

		# Set last connection to null
		my @team_idents = keys (%teams);
		my $teams = Mioga2::TeamList->new ($self->{config}, { attributes => { ident => \@team_idents } });
		my $users = $teams->GetInvitedUserList ();
		my @users = $users->GetUsers ();
		my @user_idents = map { $_->{ident} } @users;
		my $users_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_) } @user_idents);
		$sql = "INSERT INTO m_group_user_last_conn SELECT m_group.rowid AS group_id, m_user_base.rowid AS user_id, NULL AS last_conection FROM m_group, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_user_base.ident IN ($users_idents) AND m_group.mioga_id = $mioga_id AND $where AND m_user_base.rowid NOT IN (SELECT user_id FROM m_group_user_last_conn WHERE group_id = m_group.rowid) AND m_user_base.mioga_id = $mioga_id;";
		ExecSQL ($self->{config}->GetDBH (), $sql);
	}

	print STDERR "[Mioga2::GroupList::InviteTeams] Leaving\n" if ($debug);
}	# ----------  end of subroutine InviteTeams  ----------


#===============================================================================

=head2 InviteUsers

Invite users into groups.

=over

=item B<arguments:>

=over

=item I<%users> a hash whose keys are user idents and values are associated profile ident.

=back

=back

=cut

#===============================================================================
sub InviteUsers {
	my ($self, %users) = @_;
	print STDERR "[Mioga2::GroupList::InviteUsers] Entering\n" if ($debug);

	my $where = $self->GetWhereStatement ();
	my $mioga_id = $self->{config}->GetMiogaId ();
	my $users_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_) } keys (%users));
	
	# Revoke users from all groups they were member of and are not anymore
	for my $group_attrs ($self->GetGroups ()) {
		my $group_ident = $group_attrs->{ident};
		my $group = Mioga2::GroupList->new ($self->{config}, { attributes => { ident => $group_ident } });
		for my $user_attrs ($group->GetInvitedUserList ()->GetUsers ()) {
			my $user_ident = $user_attrs->{ident};
			if (!grep (/^$user_ident$/, keys (%users))) {
	 			LaunchMethodInApps($self->{config}, "RevokeUserFromGroup", $group_attrs->{rowid}, $user_attrs->{rowid}, $group_attrs->{anim_id});
			}
		}
	}

	# Revoke not specified users
	my $sql = "DELETE FROM m_profile_group USING m_profile, m_group, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_profile_group.group_id = m_user_base.rowid AND m_profile_group.profile_id = m_profile.rowid AND m_profile.group_id = m_group.rowid" . (($users_idents eq '') ? '' : " AND m_user_base.ident NOT IN ($users_idents)") . " AND m_user_base.rowid != m_group.anim_id AND $where;";
	ExecSQL ($self->{config}->GetDBH (), $sql);
	$sql = "DELETE FROM m_group_group USING m_group, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_group_group.group_id = m_group.rowid AND m_group_group.invited_group_id = m_user_base.rowid" . (($users_idents eq '') ? '' : " AND m_user_base.ident NOT IN ($users_idents)") . " AND m_user_base.rowid != m_group.anim_id AND m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'group') AND $where;";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	if ($users_idents ne '') {
		# Invite users into group
		$sql = "INSERT INTO m_group_group SELECT m_group.rowid AS group_id, m_user_base.rowid AS invited_group_id FROM m_group, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_user_base.ident IN ($users_idents) AND m_group.mioga_id = $mioga_id AND $where AND m_user_base.rowid NOT IN (SELECT invited_group_id FROM m_group_group WHERE group_id = m_group.rowid) AND m_user_base.mioga_id = $mioga_id;";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Set last connection to null
		$sql = "INSERT INTO m_group_user_last_conn SELECT m_group.rowid AS group_id, m_user_base.rowid AS user_id, NULL AS last_conection FROM m_group, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_user_base.ident IN ($users_idents) AND m_group.mioga_id = $mioga_id AND $where AND m_user_base.rowid NOT IN (SELECT user_id FROM m_group_user_last_conn WHERE group_id = m_group.rowid) AND m_user_base.mioga_id = $mioga_id;";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Affect profiles to users
		for (keys (%users)) {
			my $user_ident = $_;
			my $profile_ident = ($users{$_} ne '') ? $self->{config}->GetDBH()->quote ($users{$_}) : $self->{config}->GetDBH ()->quote (SelectSingle ($self->{config}->GetDBH (), "SELECT m_profile.ident FROM m_profile, m_profile_group, m_user_base, m_group, " . $self->GetAdditionnalTables () . " WHERE m_profile_group.group_id = m_user_base.rowid AND m_profile.group_id = m_group.rowid AND m_profile_group.profile_id = m_profile.rowid AND $where AND m_group.mioga_id = $mioga_id AND m_user_base.ident = " . $self->{config}->GetDBH ()->quote ($user_ident) . " AND m_user_base.mioga_id = $mioga_id UNION SELECT m_profile.ident FROM m_profile, m_group, " . $self->GetAdditionnalTables () . " WHERE m_profile.rowid = m_group.default_profile_id AND $where AND m_group.mioga_id = $mioga_id LIMIT 1")->{ident});

			# Create new profile affectation
			$sql = "INSERT INTO m_profile_group SELECT m_profile.rowid AS profile_id, m_user_base.rowid AS group_id FROM m_profile, m_group, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_profile.group_id = m_group.rowid AND m_user_base.ident = " . $self->{config}->GetDBH()->quote ($user_ident) . " AND m_user_base.rowid != m_group.anim_id AND m_group.mioga_id = $mioga_id AND m_user_base.mioga_id = $mioga_id AND $where AND m_profile.ident = $profile_ident AND m_user_base.rowid NOT IN (SELECT group_id FROM m_profile_group WHERE profile_id IN (SELECT m_profile.rowid FROM m_profile, m_user_base WHERE m_user_base.ident = " . $self->{config}->GetDBH()->quote ($user_ident) . " AND m_user_base.mioga_id = $mioga_id AND m_profile.group_id = m_group.rowid));";
			ExecSQL ($self->{config}->GetDBH (), $sql);
		}
	}

	print STDERR "[Mioga2::GroupList::InviteUsers] Leaving\n" if ($debug);
}	# ----------  end of subroutine InviteUsers  ----------


#===============================================================================

=head2 EnableApplications

Enable applications in group's space so that the group animator can activate
them to actually use them.

=over

=item B<arguments:>

=over

=item I<@applications:> an array of application idents.

=back

=back

=cut

#===============================================================================
sub EnableApplications {
	my ($self, @applications) = @_;
	print STDERR "[Mioga2::GroupList::EnableApplications] Entering. Applications: " . Dumper \@applications if ($debug);

	my $where = $self->GetWhereStatement ();
	my $mioga_id = $self->{config}->GetMiogaId ();
	my $apps_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_->{ident}) } @applications);

	my $sql;

	# Delete application methods / profiles association for applications that are not in the list
	$sql = "DELETE FROM m_profile_function USING m_function, m_application, m_instance_application, m_profile, m_group, " . $self->GetAdditionnalTables () . " WHERE m_function.application_id = m_application.rowid AND m_profile.group_id = m_group.rowid AND m_profile_function.profile_id = m_profile.rowid AND m_profile_function.function_id = m_function.rowid AND $where AND m_application.ident NOT IN ($apps_idents) AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_group.mioga_id = $mioga_id;";
	ExecSQL ($self->{config}->GetDBH (), $sql);
	# Disable applications that are not in the list
	$sql = "DELETE FROM m_application_group_allowed WHERE (application_id, group_id) IN (SELECT m_application.rowid AS application_id, m_group.rowid AS group_id FROM m_application, m_instance_application, m_group, " . $self->GetAdditionnalTables () . " WHERE $where AND m_application.ident NOT IN ($apps_idents) AND m_group.mioga_id = $mioga_id AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE all_groups = 't' AND mioga_id = $mioga_id) AND m_application.type_id IN (SELECT rowid FROM m_application_type WHERE ident IN ('normal', 'portal')));";
	ExecSQL ($self->{config}->GetDBH (), $sql);
	# Disallow applications that are not in the list
	$sql = "DELETE FROM m_application_group WHERE (application_id, group_id) IN (SELECT m_application.rowid AS application_id, m_group.rowid AS group_id FROM m_application, m_instance_application, m_group, " . $self->GetAdditionnalTables () . " WHERE $where AND m_application.ident NOT IN ($apps_idents) AND m_group.mioga_id = $mioga_id AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE all_groups = 't' AND mioga_id = $mioga_id) AND m_application.type_id IN (SELECT rowid FROM m_application_type WHERE ident IN ('normal', 'portal')));";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	# Process all_groups applications
	my $reverted_groups = Mioga2::GroupList->new ($self->{config}, { attributes => { rowid => $self->Get ('rowid') } });
	$reverted_groups->Revert ();
	my $reverted_where = $reverted_groups->GetWhereStatement ();
	$sql = "INSERT INTO m_application_group SELECT m_application.rowid AS application_id, m_group.rowid AS group_id FROM m_application, m_instance_application, m_group, " . $self->GetAdditionnalTables () . " WHERE m_application.ident NOT IN ($apps_idents) AND m_application.type_id IN (SELECT rowid FROM m_application_type WHERE ident IN ('normal', 'portal')) AND m_instance_application.all_groups = 't' AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND $reverted_where AND (m_application.rowid, m_group.rowid) NOT IN (SELECT * FROM m_application_group);";
	ExecSQL ($self->{config}->GetDBH (), $sql);
	$sql = "UPDATE m_instance_application SET all_groups = 'f' FROM m_application WHERE m_application.ident NOT IN ($apps_idents) AND m_application.type_id IN (SELECT rowid FROM m_application_type WHERE ident IN ('normal', 'portal')) AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id;";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	# Enable applications that are in the list
	$sql = "INSERT INTO m_application_group (application_id, group_id) SELECT m_application.rowid AS application_id, m_group.rowid AS group_id FROM m_application, m_instance_application, m_group, " . $self->GetAdditionnalTables () . " WHERE $where AND m_application.ident IN ($apps_idents) AND m_group.mioga_id = $mioga_id AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_application_group WHERE group_id = m_group.rowid) AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE all_groups = 't' AND mioga_id = $mioga_id);";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	print STDERR "[Mioga2::GroupList::EnableApplications] Leaving\n" if ($debug);
}	# ----------  end of subroutine EnableApplications  ----------


#===============================================================================

=head2 ActivateApplications

Activate applications that have been previously enabled via
Mioga2::GroupList::EnableApplications.

=over

=item B<arguments:>

=over

=item I<@applications:> an array of application idents.

=back

=back

=cut

#===============================================================================
sub ActivateApplications {
	my ($self, @applications) = @_;
	print STDERR "[Mioga2::GroupList::ActivateApplications] Entering. Applications: " . Dumper \@applications if ($debug);

	my $where = $self->GetWhereStatement ();
	my $mioga_id = $self->{config}->GetMiogaId ();

	# Only select applications that have active="1" set in their attributes
	my @active_apps = map { if ((defined $_->{active}) && ($_->{active} == 1)) { $_->{ident} } else { } } @applications;
	my $apps_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_) } @active_apps);

	# Deactivate applications that are not in list
	my $sql = "DELETE FROM m_application_group_allowed USING m_application, m_group WHERE m_application_group_allowed.group_id = m_group.rowid AND m_application_group_allowed.application_id = m_application.rowid AND m_group.rowid IN (" . join (', ', $self->Get ('rowid')) . ") AND m_application.ident NOT IN ($apps_idents) AND m_application.ident NOT IN (" . join (', ', map { $self->{config}->GetDBH ()->quote ($_) } @{$self->{protected_applications}}) . ");";
	ExecSQL ($self->{config}->GetDBH (), $sql);
	$sql = "DELETE FROM m_profile_function USING m_application, m_profile, m_function WHERE m_function.application_id = m_application.rowid AND m_profile_function.function_id = m_function.rowid AND m_profile_function.profile_id = m_profile.rowid AND m_profile.group_id IN (" . join (', ', $self->Get ('rowid')) . ") AND m_application.ident NOT IN ($apps_idents) AND m_application.ident NOT IN (" . join (', ', map { $self->{config}->GetDBH ()->quote ($_) } @{$self->{protected_applications}}) . ");";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	# Activate applications
	$sql = "INSERT INTO m_application_group_allowed (created, modified, application_id, group_id, is_public) SELECT now() AS created, now() AS modified, m_application.rowid AS application_id, m_group.rowid AS group_id, 'f' AS is_public FROM m_application, m_instance_application, m_group, " . $self->GetAdditionnalTables () . " WHERE $where AND m_application.ident IN ($apps_idents) AND m_group.mioga_id = $mioga_id AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_application_group_allowed WHERE group_id = m_group.rowid);";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	# Set public applications
	for (@applications) {
		if (exists ($_->{is_public})) {
			$sql = "UPDATE m_application_group_allowed SET is_public = 't' FROM m_application, m_instance_application, m_group, " . $self->GetAdditionnalTables () . " WHERE m_application_group_allowed.group_id = m_group.rowid AND m_application_group_allowed.application_id = m_application.rowid AND $where AND m_application.ident = " . $self->{config}->GetDBH()->quote ($_->{ident}) . " AND m_group.mioga_id = $mioga_id AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id;";
			ExecSQL ($self->{config}->GetDBH (), $sql);
		}
	}

	for my $group ($self->GetGroups ()) {
		for (@applications) {
			try {
				my $app_desc = new Mioga2::AppDesc ($self->{config}, ident => $_->{ident});
				my $app = $app_desc->CreateApplication();
				$app->InitializeGroupApplication($self->{config}, $group, $_);
			}
			otherwise {
				print STDERR "Failed to initialize application $_->{ident} for group $group->{ident} in instance " . $self->{config}->GetMiogaIdent() . "\n";
			};
		}
	}

	# Enable all functions for animator profile
	$sql = "INSERT INTO m_profile_function SELECT m_profile.rowid AS profile_id, m_function.rowid AS function_id FROM m_profile, m_group, " . $self->GetAdditionnalTables () . ", m_profile_user, m_application, m_instance_application, m_function WHERE $where AND m_profile.group_id = m_group.rowid AND m_profile_user.profile_id = m_profile.rowid AND m_profile_user.group_id = m_group.anim_id AND m_function.application_id = m_application.rowid AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_application.ident IN ($apps_idents) AND (m_profile.rowid, m_function.rowid) NOT IN (SELECT * FROM m_profile_function WHERE profile_id = m_profile.rowid);";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	print STDERR "[Mioga2::GroupList::ActivateApplications] Leaving\n" if ($debug);
}	# ----------  end of subroutine ActivateApplications  ----------


#===============================================================================

=head2 CreateProfile

Create a profile.

=over

=item B<arguments:>

=over

=item I<$profile:> a hash with the following keys:

=over

=item ident: the profile ident.

=item applications: a hash whose keys are application idents and values are a hash describing access rights to functions.

=over

=item function: an array of function idents to grant access to.

=item all_functions: if this key is defined, access to all functions is granted.

=back

=back

=back

=back

=cut

#===============================================================================
sub CreateProfile {
	my ($self, $profile) = @_;
	print STDERR "[Mioga2::GroupList::CreateProfile] Entering\n" if ($debug);

	my $where = $self->GetWhereStatement ();
	my $mioga_id = $self->{config}->GetMiogaId ();
	my $profile_ident = $profile->{ident};

	#-------------------------------------------------------------------------------
	# Create profile
	#-------------------------------------------------------------------------------
	my $sql = "INSERT INTO m_profile (created, modified, ident, group_id) SELECT DISTINCT now() AS created, now() AS modified, " . $self->{config}->GetDBH()->quote ($profile_ident) . " AS ident, m_group.rowid AS group_id FROM m_profile, m_group, " . $self->GetAdditionnalTables () . " WHERE $where AND " . $self->{config}->GetDBH()->quote ($profile_ident) . " NOT IN (SELECT ident FROM m_profile WHERE group_id = m_group.rowid);";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	#-------------------------------------------------------------------------------
	# Associate functions to profile
	#-------------------------------------------------------------------------------
	for (@{$profile->{application}}) {
		# Initialize functions idents list
		my $funcs_idents;
		if (exists ($_->{all_functions})) {
			$funcs_idents = "SELECT m_function.ident FROM m_function, m_application, m_instance_application WHERE m_function.application_id = m_application.rowid AND m_application.ident = " . $self->{config}->GetDBH()->quote ($_->{ident}) . " AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id";
		}
		else {
			$funcs_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_) } @{$_->{function}});
		}
		$sql = "INSERT INTO m_profile_function SELECT m_profile.rowid AS profile_id, m_function.rowid AS function_id FROM m_profile, m_function, m_application, m_instance_application, m_group, " . $self->GetAdditionnalTables () . " WHERE m_function.application_id = m_application.rowid AND m_profile.ident = " . $self->{config}->GetDBH()->quote ($profile_ident) . " AND m_profile.group_id = m_group.rowid AND $where AND m_application.ident = " . $self->{config}->GetDBH()->quote ($_->{ident}) . " AND m_function.ident IN ($funcs_idents) AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_function.rowid NOT IN (SELECT function_id FROM m_profile_function WHERE profile_id = m_profile.rowid);";
		ExecSQL ($self->{config}->GetDBH (), $sql);
	}

	print STDERR "[Mioga2::GroupList::CreateProfile] Leaving\n" if ($debug);
}	# ----------  end of subroutine CreateProfile  ----------


#===============================================================================

=head2 AffectProfileToUsers

This affects a profile to a Mioga2::UserList users into a Mioga2::GroupList groups space.

=over

=item B<arguments:>

=over

=item I<$users:> a Mioga2::UserList object to affect the profile to.

=item I<$profile_ident:> the profile ident to affect to users.

=back

=back

=cut

#===============================================================================
sub AffectProfileToUsers {
	my ($self, $users, $profile_ident) = @_;
	print STDERR "[Mioga2::GroupList::AffectProfileToUsers] Entering\n" if ($debug);

	my $users_from = "m_user_base, " . $users->GetAdditionnalTables ();
	my $users_where = $users->GetWhereStatement ();
	my $where = $self->GetWhereStatement ();

	# Delete any existing profile affectation
	my $sql = "DELETE FROM m_profile_group WHERE profile_id IN (SELECT m_profile.rowid FROM m_profile, m_group, m_profile_group, " . $self->GetAdditionnalTables () . " WHERE m_profile.group_id = m_group.rowid AND m_profile_group.profile_id = m_profile.rowid AND $where) AND m_profile_group.group_id IN (SELECT m_user_base.rowid FROM $users_from WHERE $users_where);";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	# Create new profile affectation
	$sql = "INSERT INTO m_profile_group SELECT m_profile.rowid AS profile_id, m_user_base.rowid AS group_id FROM m_profile, m_user_base, m_group, " . $self->GetAdditionnalTables () . " WHERE m_profile.group_id = m_group.rowid AND $where AND m_user_base.rowid IN (SELECT m_user_base.rowid FROM $users_from WHERE $users_where) AND m_profile.ident = " . $self->{config}->GetDBH()->quote ($profile_ident) . ";";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	print STDERR "[Mioga2::GroupList::AffectProfileToUsers] Leaving\n" if ($debug);
}	# ----------  end of subroutine AffectProfileToUsers  ----------


#===============================================================================

=head2 AffectProfileToTeams

This affects a profile to a Mioga2::TeamList teams into a Mioga2::GroupList groups space.

=over

=item B<arguments:>

=over

=item I<$teams:> a Mioga2::TeamList object to affect the profile to.

=item I<$profile_ident:> the profile ident to affect to teams.

=back

=back

=cut

#===============================================================================
sub AffectProfileToTeams {
	my ($self, $teams, $profile_ident) = @_;
	print STDERR "[Mioga2::GroupList::AffectProfileToTeams] Entering\n" if ($debug);

	my $teams_from = "m_group";
	my $teams_where = $teams->GetWhereStatement ();
	my $where = $self->GetWhereStatement ();

	# Delete any existing profile affectation
	my $sql = "DELETE FROM m_profile_group WHERE profile_id IN (SELECT m_profile.rowid FROM m_profile, m_group, m_profile_group, " . $self->GetAdditionnalTables () . " WHERE m_profile.group_id = m_group.rowid AND m_profile_group.profile_id = m_profile.rowid AND $where) AND m_profile_group.group_id IN (SELECT m_group.rowid FROM $teams_from WHERE $teams_where);";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	# Create new profile affectation
	$sql = "INSERT INTO m_profile_group SELECT m_profile.rowid AS profile_id, m_group_base.rowid AS group_id FROM m_profile, m_group_base, m_group, " . $self->GetAdditionnalTables () . " WHERE m_profile.group_id = m_group.rowid AND $where AND m_group_base.rowid IN (SELECT m_group.rowid FROM $teams_from WHERE $teams_where) AND m_profile.ident = " . $self->{config}->GetDBH()->quote ($profile_ident) . ";";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	print STDERR "[Mioga2::GroupList::AffectProfileToTeams] Leaving\n" if ($debug);
}	# ----------  end of subroutine AffectProfileToTeams  ----------


#===============================================================================

=head2 SetDefaultProfile

Set default profile for group.

=over

=item B<arguments>

=over

=item I<$profile_ident:> the default profile ident.

=back

=back

=cut

#===============================================================================
sub SetDefaultProfile {
	my ($self, $profile_ident) = @_;
	print STDERR "[Mioga2::GroupList::SetDefaultProfile] Entering\n" if ($debug);

	my $where = $self->GetWhereStatement ();

	my $sql = "UPDATE m_group SET default_profile_id = m_profile.rowid FROM m_profile, " . $self->GetAdditionnalTables () . " WHERE m_profile.group_id = m_group.rowid AND m_profile.ident = " . $self->{config}->GetDBH()->quote ($profile_ident) . " AND $where;";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	print STDERR "[Mioga2::GroupList::SetDefaultProfile] Leaving\n" if ($debug);
}	# ----------  end of subroutine SetDefaultProfile  ----------


#===============================================================================

=head2 GetInvitedUserList

Get user list of group members that are directly invited (ie not via teams).

=over

=item B<arguments:>

=over

=item I<%attributes:> attributes to initialize the resulting Mioga2::UserList.
This is mainly intented to pass short_list flag to Mioga2::UserList. It could
lead to inconsistent Mioga2::UserList contents if other keys (such as 'ident')
are passed.

=back

=back

=over

=item B<return:> a Mioga2::UserList object.

=back

=cut

#===============================================================================
sub GetInvitedUserList {
	my ($self, %attributes) = @_;

	my $short_list = (exists ($attributes{short_list})) ? $attributes{short_list} : $self->{short_list};

	my $users = Mioga2::UserList->new ($self->{config}, { attributes => { groups => $self }, short_list => $short_list, journal => $self->{journal} });

	return ($users);
}	# ----------  end of subroutine GetInvitedUserList  ----------


#===============================================================================

=head2 GetExpandedUserList

Get user list of group members either directly invited or via teams.

=over

=item B<arguments:>

=over

=item I<%attributes:> attributes to initialize the resulting Mioga2::UserList.
This is mainly intented to pass short_list flag to Mioga2::UserList. It could
lead to inconsistent Mioga2::UserList contents if other keys (such as 'ident')
are passed.

=back

=back

=over

=item B<return:> a Mioga2::UserList object.

=back

=cut

#===============================================================================
sub GetExpandedUserList {
	my ($self, %attributes) = @_;

	my $short_list = (exists ($attributes{short_list})) ? $attributes{short_list} : $self->{short_list};

	my $users = Mioga2::UserList->new ($self->{config}, { attributes => { expanded_groups => $self }, short_list => $short_list, journal => $self->{journal} });

	return ($users);
}	# ----------  end of subroutine GetExpandedUserList  ----------


#===============================================================================

=head2 GetInvitedTeamList

Get list of teams invited into the groups of the list.

=over

=item B<arguments:>

=over

=item I<%attributes:> attributes to initialize the resulting Mioga2::TeamList.
This is mainly intented to pass short_list flag to Mioga2::TeamList. It could
lead to inconsistent Mioga2::TeamList contents if other keys (such as 'ident')
are passed.

=back

=back

=over

=item B<return:> a Mioga2::TeamList object.

=back

=cut

#===============================================================================
sub GetInvitedTeamList {
	my ($self, %attributes) = @_;

	my $short_list = (exists ($attributes{short_list})) ? $attributes{short_list} : $self->{short_list};

	my $teams = Mioga2::TeamList->new ($self->{config}, { attributes => { groups => $self }, short_list => $short_list, journal => $self->{journal} });

	return ($teams);
}	# ----------  end of subroutine GetInvitedTeamList  ----------


#===============================================================================

=head2 GetApplicationList

Get list of applications the groups of the list can use.

=over

=item B<return:> a Mioga2::ApplicationList object.

=back

=cut

#===============================================================================
sub GetApplicationList {
	my ($self) = @_;

	my $applications = Mioga2::ApplicationList->new ($self->{config}, { attributes => { 'groups' => $self }, journal => $self->{journal} });

	return ($applications);
}	# ----------  end of subroutine GetApplicationList  ----------


#===============================================================================

=head2 GetActiveApplicationList

Get list of applications enabled for the groups of the list.

=over

=item B<return:> a Mioga2::ApplicationList object.

=back

=cut

#===============================================================================
sub GetActiveApplicationList {
	my ($self) = @_;

	my $applications = Mioga2::ApplicationList->new ($self->{config}, { attributes => { 'groups' => $self, only_active => 1 }, journal => $self->{journal} });

	return ($applications);
}	# ----------  end of subroutine GetActiveApplicationList  ----------


#===============================================================================

=head2 GetInvitedResourceList

Get list of resources invited into the groups of the list.

=over

=item B<arguments:>

=over

=item I<%attributes:> attributes to initialize the resulting Mioga2::ResourceList.
This is mainly intented to pass short_list flag to Mioga2::ResourceList. It could
lead to inconsistent Mioga2::ResourceList contents if other keys (such as 'ident')
are passed.

=back

=back

=over

=item B<return:> a Mioga2::ResourceList object.

=back

=cut

#===============================================================================
sub GetInvitedResourceList {
	my ($self, %attributes) = @_;

	my $short_list = (exists ($attributes{short_list})) ? $attributes{short_list} : $self->{short_list};

	my $resources = Mioga2::ResourceList->new ($self->{config}, { attributes => { groups => $self }, short_list => $short_list, journal => $self->{journal} });

	return ($resources);
}	# ----------  end of subroutine GetInvitedResourceList  ----------


#===============================================================================

=head2 Revert

Revert list.

=cut

#===============================================================================
sub Revert {
	my ($self) = @_;
	print STDERR "[Mioga2::GroupList::Revert] Entering\n" if ($debug);

	$self->{where}->{m_group} = 'm_group.rowid NOT IN (SELECT m_group.rowid FROM m_group WHERE ' . $self->GetWhereStatement (qw/m_group/) . ') AND m_group.mioga_id = ' . $self->{config}->GetMiogaId ();

	$self->{attributes} = { };
	$self->{update} = { };
	$self->{loaded} = 0;
	$self->{deleted} = 0;

	print STDERR "[Mioga2::GroupList::Revert] New rules: " . Dumper $self->{where} if ($debug);
}	# ----------  end of subroutine Revert  ----------


#===============================================================================

=head2 GetProtectedApplications

Get list of applications idents that can not be disabled for a user.

=cut

#===============================================================================
sub GetProtectedApplications {
	my ($self) = @_;

	$self->Load () unless ($self->{loaded});

	return (@{$self->{protected_applications}});
}	# ----------  end of subroutine GetProtectedApplications  ----------


#===============================================================================

=head2 GetJournal

Get journal of operations as an object

=cut

#===============================================================================
sub GetJournal {
	my ($self) = @_;

	my $journal = tied (@{$self->{journal}});

	return ($journal);
}	# ----------  end of subroutine GetJournal  ----------


#===============================================================================

=head2 HasProfile

Check a profile rowid belongs to group

=head3 Incoming Arguments

=over

=item I<profile_id>: The profile rowid

=back

=head3 Return value

1 if group has profile, 0 otherwise.

=cut

#===============================================================================
sub HasProfile {
	my ($self, $profile_id) = @_;

	my $sql = 'SELECT * FROM m_profile WHERE rowid = ? AND group_id IN (?);';
	my $profile = $self->{db}->SelectSingle ($sql, [$profile_id, join (', ', $self->Get ('rowid'))]);

	return (defined ($profile) ? 1 : 0);
}	# ----------  end of subroutine HasProfile  ----------


#===============================================================================

=head2 HasApplication

Check a application rowid belongs to group

=head3 Incoming Arguments

=over

=item I<application_id>: The application rowid

=back

=head3 Return value

1 if group has application, 0 otherwise.

=cut

#===============================================================================
sub HasApplication {
	my ($self, $application_id) = @_;

	my $sql = 'SELECT * FROM m_application_group_allowed WHERE application_id = ? AND group_id IN (?);';
	my $application = $self->{db}->SelectSingle ($sql, [$application_id, join (', ', $self->Get ('rowid'))]);

	return (defined ($application) ? 1 : 0);
}	# ----------  end of subroutine HasApplication  ----------


#===============================================================================

=head2 SetResources

Enable resources in group

=head3 Incoming Arguments

=over

=item I<$resources>: A Mioga2::ResourceList

=back

=cut

#===============================================================================
sub SetResources {
	my ($self, $resources) = @_;
	print STDERR "[Mioga2::GroupList::SetResources] Entering, resources: " . Dumper $resources->Get ('ident') if ($debug);

	my @rowids = @{ac_ForceArray ($resources->Get ('rowid'))};
	push (@rowids, 0);	# Ensure there's at least one element

	# Disable resources outside list
	my $sql = 'DELETE FROM m_group_group WHERE invited_group_id IN (' . join (', ', @{ac_ForceArray ($self->Get ('rowid'))}) . ') AND group_id NOT IN (' . join (', ', @rowids) . ');';
	ExecSQL ($self->{config}->GetDBH (), $sql);

	# Enable resources of list
	$sql = 'INSERT INTO m_group_group (SELECT m_resource.rowid AS group_id, m_group.rowid AS invited_group_id FROM m_resource, m_group WHERE m_group.rowid IN (' . join (', ', @{ac_ForceArray ($self->Get ('rowid'))}) . ') AND m_resource.rowid IN (' . join (', ', @rowids) . ') AND (m_resource.rowid, m_group.rowid) NOT IN (SELECT * FROM m_group_group));';
	ExecSQL ($self->{config}->GetDBH (), $sql);

	print STDERR "[Mioga2::GroupList::SetResources] Leaving\n" if ($debug);
}	# ----------  end of subroutine SetResources  ----------


#===============================================================================

=head2 IsLocked

Check if a group is locked (can't be deleted) and return lock status and lock reason

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

=item I<$locked>: lock status (1 if locked, 0 otherwise)

=item I<$reason>: lock reason (if locked)

=back

=cut

#===============================================================================
sub IsLocked {
	my ($self) = @_;

	my $db = $self->{config}->GetDBObject ();

	# Default return value
	my $locked = 0;
	my $reason;

	# Load group active applications
	my $applist = $self->GetActiveApplicationList ();
	my @apps = $applist->GetApplications ();

	# Check if group has Haussmann projects
	if (grep { $_->{ident} eq 'Haussmann' } @apps) {
		my $res = $db->SelectMultiple ('SELECT * FROM project WHERE group_id = ?;', $self->Get ('rowid'));
		if (@$res) {
			$locked = 1;
			$reason = 'group has Haussmann projects';
		}
	}

	return ($locked, $reason);
}	# ----------  end of subroutine IsLocked  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


#===============================================================================

=head2 LoadSkeleton

Load provided skeleton file and store its contents as inner attributes of the object.

=cut

#===============================================================================
sub LoadSkeleton {
	my ($self) = @_;
	print STDERR "[Mioga2::GroupList::LoadSkeleton] Entering\n" if ($debug);

	my $data = '';

	my $skel_file = delete $self->{attributes}->{skeleton};
	$self->{attributes}->{skeleton}->{file} = $skel_file;

	#-------------------------------------------------------------------------------
	# Read file
	#-------------------------------------------------------------------------------
	open(FILE, "< $self->{attributes}->{skeleton}->{file}") or throw Mioga2::Exception::Group ('Mioga2::GroupList::LoadSkeleton', "Can't open skeleton file '$self->{attributes}->{skeleton}->{file}': $!.");
	while (<FILE>) { $data .= $_ };
	close (FILE);

	#-------------------------------------------------------------------------------
	# Parse XML
	#-------------------------------------------------------------------------------
	$data =~ s/<--!.*?-->//gsm; # remove comments

	my $xml		  = new Mioga2::XML::Simple(forcearray => 1);
	my $xmltree   = $xml->XMLin($data);
	my $skel_name = (exists($xmltree->{'name'}))? $xmltree->{'name'} : "";
	my $skel_type = (exists($xmltree->{'type'}))? $xmltree->{'type'} : "";

	if (lc ($skel_type) ne 'group') {
		throw Mioga2::Exception::Group ('Mioga2::GroupList::LoadSkeleton', "Skeleton type '$skel_type' can not be applied to a group");
	}

	#-------------------------------------------------------------------------------
	# Translate XML tree
	#-------------------------------------------------------------------------------
	my %contents = ();

	# Attributes
	for (keys (%{$xmltree->{attributes}->[0]})) {
		$contents{attributes}{$_} = $xmltree->{attributes}->[0]->{$_}->[0] if (!$self->Has ($_));
	}

	# Applications
	for (@{$xmltree->{applications}->[0]->{application}}) {
		push (@{$contents{applications}}, $_);
	}

	# Profiles
	$contents{profiles} = $xmltree->{profiles}->[0];

	# Teams
	for (@{$xmltree->{teams}->[0]->{team}}) {
		$contents{teams}->{$_->{ident}} = $_->{profile}->[0];
	}

	# Filesystem
	$contents{filesystem}{history} = 0;
	$contents{filesystem}{history} = $xmltree->{filesystem}->[0]->{history}->[0] if ($xmltree->{filesystem}->[0]->{history} && $xmltree->{filesystem}->[0]->{history}->[0]);
	for (@{$xmltree->{filesystem}->[0]->{space}}) {
		my $type = $_->{type};
		$contents{filesystem}{$type} = {};
		$contents{filesystem}{$type}{acls} = $_->{acls}->[0]->{acl};
		if ($_->{dir}) {
			for (@{$_->{dir}}) {
				push (@{$contents{filesystem}{$type}{dir}}, {name => $_->{name}, acls => $_->{acls}->[0]->{acl}});
			}
		}
		if ($_->{file}) {
			for (@{$_->{file}}) {
				push (@{$contents{filesystem}{$type}{file}}, {name => $_->{name}, src => $_->{src}, acls => $_->{acls}->[0]->{acl}});
			}
		}
	}

	#-------------------------------------------------------------------------------
	# Store skeleton definition into object
	#-------------------------------------------------------------------------------
	$self->{attributes}->{skeleton}->{name} = $skel_name;
	$self->{attributes}->{skeleton}->{contents} = \%contents;

	print STDERR "[Mioga2::GroupList::LoadSkeleton] Leaving\n" if ($debug);
}	# ----------  end of subroutine LoadSkeleton  ----------


#===============================================================================

=head2 Load

Load object attributes from DB. This method is automatically called when trying
to access an attribute whose value has not already been loaded from DB.

=cut

#===============================================================================
sub Load {
	my ($self) = @_;
	print STDERR "[Mioga2::GroupList::Load] Entering\n" if ($debug);

	my $mioga_id = $self->{config}->GetMiogaId ();

	#-------------------------------------------------------------------------------
	# Initialize and run SQL query
	#-------------------------------------------------------------------------------
	my $sql = 'SELECT ' . $self->{fields} . ', m_user_base.firstname || \' \' || m_user_base.lastname || \' (\' || m_user_base.email || \')\' AS animator, coalesce (T1.group_count, 0) AS nb_users, coalesce (T2.group_count, 0) AS nb_teams, (SELECT ident FROM m_application where rowid = m_group.default_app_id) AS default_app FROM m_group LEFT JOIN (SELECT count(m_group_group.*) AS group_count, m_group_group.group_id AS group_id FROM m_group_group, m_user_base WHERE m_user_base.mioga_id = ' . $mioga_id . ' AND m_user_base.rowid = m_group_group.invited_group_id GROUP BY m_group_group.group_id) AS T1 ON m_group.rowid = T1.group_id LEFT JOIN (SELECT count(m_group_group.*) AS group_count, m_group_group.group_id AS group_id FROM m_group_group, m_group_base, m_group_type WHERE m_group_base.mioga_id = ' . $mioga_id . ' AND m_group_base.rowid = m_group_group.invited_group_id AND m_group_base.type_id = m_group_type.rowid AND m_group_type.ident=\'team\' GROUP BY m_group_group.group_id) AS T2 on m_group.rowid = T2.group_id, ' . $self->GetAdditionnalTables () . ', m_user_base WHERE ' . $self->GetWhereStatement () . ' AND m_user_base.rowid = m_group.anim_id';

	# Quite complex request in case we want a GroupList from a UserList, overwrite the $sql variable
	if ($self->Has ('users') || $self->Has ('expanded_users')) {
		my $users_where = $self->Has ('users') ? $self->Get ('users')->GetWhereStatement () : $self->Get ('expanded_users')->GetWhereStatement ();
		my $users_tables = $self->Has ('users') ? $self->Get ('users')->GetAdditionnalTables () : $self->Get ('expanded_users')->GetAdditionnalTables ();

		$sql = 'SELECT ' . $self->{fields} . ', m_profile.ident AS profile, m_profile.rowid AS profile_id FROM m_group, ' . $self->GetAdditionnalTables () . ', m_profile, m_profile_user, m_group_invited_user WHERE ' . $self->GetWhereStatement () . " AND m_profile.group_id = m_group.rowid AND m_profile.rowid = m_profile_user.profile_id AND m_profile_user.group_id = m_group_invited_user.invited_group_id AND m_group_invited_user.invited_group_id IN (SELECT m_user_base.rowid FROM m_user_base, $users_tables WHERE $users_where) AND m_profile_user.group_id = m_group_invited_user.group_id";

		if ($self->Has ('expanded_users')) {
			my $users_where = $self->Get ('expanded_users')->GetWhereStatement ();
			my $users_tables = $self->Get ('expanded_users')->GetAdditionnalTables ();

			$sql .= ' UNION (SELECT ' . $self->{fields} . ', m_profile.ident AS profile, m_profile.rowid AS profile_id FROM m_group, ' . $self->GetAdditionnalTables () . ', m_profile, m_profile_expanded_user, m_group_expanded_user WHERE ' . $self->GetWhereStatement () . " AND m_group.rowid = m_group_expanded_user.group_id AND m_group.rowid = m_profile.group_id AND m_profile.rowid = m_profile_expanded_user.profile_id AND m_profile_expanded_user.user_id = m_group_expanded_user.invited_group_id AND m_group_expanded_user.invited_group_id IN (SELECT m_user_base.rowid FROM m_user_base, $users_tables WHERE $users_where))";
		}
	}
	elsif ($self->Has ('teams')) {
		$sql = 'SELECT ' . $self->{fields} . ', m_profile.ident AS profile, m_profile.rowid AS profile_id FROM m_group, ' . $self->GetAdditionnalTables () . ', m_profile, m_profile_group, m_group_group WHERE ' . $self->GetWhereStatement () . " AND m_group.rowid = m_group_group.group_id AND m_profile.group_id = m_group_group.group_id AND m_profile.rowid = m_profile_group.profile_id AND m_profile_group.group_id = m_group_group.invited_group_id AND m_group_group.invited_group_id IN (SELECT m_group.rowid FROM m_group  WHERE " . $self->Get ('teams')->GetWhereStatement () . ")";
	}

	print STDERR "[Mioga2::GroupList::Load] SQL: $sql\n" if ($debug);

	my $res = SelectMultiple ($self->{config}->GetDBH (), $sql);

	#-------------------------------------------------------------------------------
	# Drop undefined values
	#-------------------------------------------------------------------------------
	for my $group (@$res) {
		for (keys (%$group)) {
			delete ($group->{$_}) if (!defined ($group->{$_}));
		}
	}

	#-------------------------------------------------------------------------------
	# Update object contents
	#-------------------------------------------------------------------------------
	$self->{groups} = $res;

	#-------------------------------------------------------------------------------
	# Update list of protected applications so that default application for group
	# can not be deleted
	#-------------------------------------------------------------------------------
	for (@{$self->{groups}}) {
		my $default_app = $_->{default_app};
		next unless ($default_app);
		push (@{$self->{protected_applications}}, $default_app) unless (grep (/^$default_app$/, @{$self->{protected_applications}}));
	}

	# Raise "loaded" flag if query returned something
	$self->{loaded} = 1 if (scalar (@{$self->{groups}}));

	print STDERR "[Mioga2::GroupList::Load] Leaving\n" if ($debug);
}	# ----------  end of subroutine Load  ----------


#===============================================================================

=head2 RequestForAttribute

Generate SQL statement that matches attribute into m_group.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get associated SQL request.

=back

=item B<return:> an array containing an attribute name and the statement to
match this attribute in m_group table. If the attribute is stored as-is
into table, this method returns the attribute and its value. If the attribute
is stores as the ID of a record in another table, this method returns the
attribute name and a SQL subquery picking the attribute ID from this other
table. If the attribute has to be ignored, this method returns empty strings.

=back

=cut

#===============================================================================
sub RequestForAttribute { # TODO (SNI - 21/04/2010 10:03): Handle default_profile_id
	my ($self, $attribute) = @_;

	my $request = '';
	my $value = '';
	my $operator = '=';
	my $attrval = '';

	# Change operator if a match type is specified
	if (exists ($self->{match}) && grep (/^$attribute$/, @text_fields)) {
		$operator = 'ILIKE';
	}

	#-------------------------------------------------------------------------------
	# Format string so that attribute value can be a scalar or an array
	#-------------------------------------------------------------------------------
	if (ref ($self->Get ($attribute)) eq 'ARRAY') {
		$operator = 'IN';
		for (@{$self->Get ($attribute)}) {
			$attrval = "(" . join (', ', map { $self->{config}->GetDBH()->quote ($_) } @{$self->Get ($attribute)}) . ")";
		}
		if ($attrval eq '') {
			$attrval = '(NULL)';
		}
	}
	elsif (ref ($self->Get ($attribute)) eq 'Mioga2::UserList') {
		$operator = 'IN';
		my @rowids = $self->Get ($attribute)->GetRowIds ();
		$attrval = "(SELECT group_id FROM m_group_invited_user WHERE invited_group_id IN (" . join (', ', @rowids) . ")";
		if ($attribute eq 'expanded_users') {
			$attrval .= " UNION SELECT group_id FROM m_group_expanded_user WHERE invited_group_id IN (" . join (', ', @rowids) . ")";
		}
		$attrval .= ")";
	}
	elsif (ref ($self->Get ($attribute)) eq 'Mioga2::TeamList') {
		$operator = 'IN';
		my @rowids = $self->Get ($attribute)->GetRowIds ();
		$attrval = "(SELECT group_id FROM m_group_group WHERE invited_group_id IN (" . join (', ', @rowids) . "))";
	}
	elsif (ref ($self->Get ($attribute)) eq 'Mioga2::ApplicationList') {
		$operator = 'IN';
		my @rowids = $self->Get ($attribute)->GetRowIds ();
		if ($self->Has ('users') || $self->Has ('expanded_users')) {
			# Retreive the list of groups the users can user the applications in
			my $usrattr = ($self->Has ('users') ? 'users' : 'expanded_users');
			$attrval = "(SELECT m_profile.group_id FROM m_profile_function, m_profile_group, m_function, m_profile, m_group, m_application, m_user_base WHERE m_profile_function.profile_id = m_profile.rowid AND m_profile_function.function_id = m_function.rowid AND m_function.application_id = m_application.rowid AND m_profile_group.profile_id = m_profile.rowid AND m_profile_group.group_id = m_user_base.rowid AND m_profile.group_id = m_group.rowid AND m_user_base.rowid IN (" . join (', ', $self->Get ($usrattr)->GetRowIds ()) . ") AND m_application.rowid IN (" . join (', ', @rowids) . ")";
			if ($usrattr eq 'expanded_users') {
				$attrval .= "UNION SELECT m_profile.group_id FROM m_profile_function, m_profile_expanded_user, m_function, m_profile, m_group, m_application, m_user_base WHERE m_profile_function.profile_id = m_profile.rowid AND m_profile_function.function_id = m_function.rowid AND m_function.application_id = m_application.rowid AND m_profile_expanded_user.profile_id = m_profile.rowid AND m_profile_expanded_user.user_id = m_user_base.rowid AND m_profile.group_id = m_group.rowid AND m_user_base.rowid IN (" . join (', ', $self->Get ($usrattr)->GetRowIds ()) . ") AND m_application.rowid IN (" . join (', ', @rowids) . ")"
			}
			$attrval .= ')';
		}
		else {
			# Retreive list of groups that can use the applications
			$attrval = "(SELECT group_id FROM m_application_group WHERE application_id IN (" . join (', ', @rowids) . ") UNION SELECT m_group.rowid FROM m_group, m_application, m_instance_application WHERE m_group.mioga_id = m_instance_application.mioga_id AND m_application.rowid = m_instance_application.application_id AND m_application.rowid in (" . join (', ', @rowids) . ") AND m_instance_application.all_groups = 't')";
		}
	}
	else {
		if ($self->Has ($attribute) && ($self->Get ($attribute) eq '') && (!grep (/^$attribute$/, @text_fields))) {
			$operator = 'IS';
			$attrval = 'NULL';
		}
		else {
			$attrval = $self->{config}->GetDBH()->quote ($self->Get ($attribute));
		}
	}

	#-------------------------------------------------------------------------------
	# Process attribute name
	#-------------------------------------------------------------------------------
	if (grep (/^$attribute$/, qw/skeleton profile profile_id/)) {
		# Skeleton attribute is ignored as it doesn't match anything in DB
		$attribute = '';
		$value = '';
	}
	elsif (grep (/^$attribute$/, qw/animator/)) {
		# Some attributes need their ID to be picked from another table
		if ($self->{match}) {
			$value = "(SELECT rowid FROM m_user_base WHERE firstname " . $operator . " " . $self->{config}->GetDBH()->quote ($self->Get ($attribute)) . " OR lastname " . $operator . " " . $self->{config}->GetDBH()->quote ($self->Get ($attribute)) . " AND mioga_id = " . $self->{config}->GetMiogaId () . ")";
			$operator = 'IN';
		}
		else {
			$value = "(SELECT rowid FROM m_user_base WHERE ident = " . $self->{config}->GetDBH()->quote ($self->Get ($attribute)) . " AND mioga_id = " . $self->{config}->GetMiogaId () . ")";
		}
		$attribute = 'anim_id';
	}
	elsif (grep (/^$attribute$/, qw/lang/)) {
		# Some attributes need their ID to be picked from another table
		$value = "(SELECT rowid FROM m_$attribute WHERE ident = " . $self->{config}->GetDBH()->quote ($self->Get ($attribute)) . ")";
		$attribute .= '_id';
	}
	elsif (grep (/^$attribute$/, qw/theme/)) {
		# Some attributes need their ID to be picked from another table, matching is done through "name" field and Mioga ID
		$value = "(SELECT rowid FROM m_$attribute WHERE ident $operator $attrval AND mioga_id = " . $self->{config}->GetMiogaId () . ")";
		$attribute .= '_id';
	}
	elsif (grep (/^$attribute$/, qw/users expanded_users teams applications/)) {
		$value = $attrval;
		$attribute = 'rowid';
	}
	elsif (grep (/^$attribute$/, qw/default_app/)) {
		$value = "(SELECT m_application.rowid FROM m_application, m_instance_application WHERE m_application.rowid = m_instance_application.application_id AND m_application.ident $operator $attrval AND m_instance_application.mioga_id = " . $self->{config}->GetMiogaId () . ")";
		$attribute .= '_id';
	}
	else {
		# Other attributes are simply associated to their text value
		$value = $attrval if ($self->Has ($attribute));
	}

	my $prefix = ($self->{loading} ? 'm_group.' : '');
	$request = "$prefix$attribute $operator $value" if ($attribute ne '' && $value ne '');

	return ($request);
}	# ----------  end of subroutine RequestForAttribute  ----------


#===============================================================================

=head2 Has

Check if a group list has a given attribute set.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to check.

=back

=item B<return:> 1 or 0 whether the object has or not a given attribute in its
current state.

=back

B<Warning:> This method doesn't load the list from DB so it is possible that
the object does not have an attribute that the groups of the list will have once
they have been loaded.

=cut

#===============================================================================
sub Has {
	my ($self, $attribute) = @_;

	return (exists ($self->{update}->{$attribute}) || exists ($self->{attributes}->{$attribute}) || ($attribute eq 'rowid'));
}	# ----------  end of subroutine Has  ----------


#===============================================================================

=head2 ApplySkeleton

Apply skeleton to list of groups. This method is automatically called by the
"Store" method.

=cut

#===============================================================================
sub ApplySkeleton {
	my ($self, @parts) = @_;
	print STDERR "[Mioga2::GroupList::ApplySkeleton] Entering\n" if ($debug);

	my $mioga_id = $self->{config}->GetMiogaId ();
	my $where = $self->GetWhereStatement ();

	#-------------------------------------------------------------------------------
	# Check what has to be applied
	#-------------------------------------------------------------------------------
	my @all_parts = qw/attributes applications profiles teams filesystem/;

	if (!scalar (@parts)) {
		print STDERR "[Mioga2::GroupList::ApplySkeleton] List of parts to apply is empty, applying all parts\n" if ($debug);
		@parts = @all_parts;
	}

	my %apply = ();
	for (@parts) {
		$apply{$_} = 1;
	}

	#-------------------------------------------------------------------------------
	# Apply attributes
	#-------------------------------------------------------------------------------
	if ($apply{attributes}) {
		print STDERR "[Mioga2::GroupList::ApplySkeleton] Applying attributes\n" if ($debug);
		for (keys (%{$self->{attributes}->{skeleton}->{contents}->{attributes}})) {
			$self->{update}->{$_} = $self->{attributes}->{skeleton}->{contents}->{attributes}->{$_};
		}
	}

	#-------------------------------------------------------------------------------
	# Apply applications
	#-------------------------------------------------------------------------------
	if ($apply{applications}) {
		print STDERR "[Mioga2::GroupList::ApplySkeleton] Applying applications\n" if ($debug);
		my $status = 1;
		try {
			$self->EnableApplications (@{$self->{attributes}->{skeleton}->{contents}->{applications}});
			$self->ActivateApplications (@{$self->{attributes}->{skeleton}->{contents}->{applications}});
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::GroupList', step => __('Applications activation'), status => $status });
		};
	}

	#-------------------------------------------------------------------------------
	# Apply profiles
	#-------------------------------------------------------------------------------
	if ($apply{profiles}) {
		print STDERR "[Mioga2::GroupList::ApplySkeleton] Applying profiles\n" if ($debug);
		# Create profiles
		my $status = 1;
		try {
			for (@{$self->{attributes}->{skeleton}->{contents}->{profiles}->{profile}}) {
				$self->CreateProfile ($_);
			}
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::GroupList', step => __('Profiles creation'), status => $status });
		};

		# Add animator to its profile
		$status = 1;
		try {
			my $user = Mioga2::UserList->new ($self->{config}, { attributes => { rowid => $self->Get ('anim_id') } });
			$self->AffectProfileToUsers ($user, $self->{attributes}->{skeleton}->{contents}->{profiles}->{animator});
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::GroupList', step => __('Animator to profile affectation'), status => $status });
		};

		# Set default profile
		$status = 1;
		try {
			$self->SetDefaultProfile ($self->{attributes}->{skeleton}->{contents}->{profiles}->{default});
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::GroupList', step => __('Default profile selection'), status => $status });
		};
	}

	#-------------------------------------------------------------------------------
	# Apply teams
	#-------------------------------------------------------------------------------
	if ($apply{teams}) {
		print STDERR "[Mioga2::GroupList::ApplySkeleton] Applying teams\n" if ($debug);
		my $status = 1;
		try {
			$status = $self->InviteTeams (%{$self->{attributes}->{skeleton}->{contents}->{teams}});
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::GroupList', step => __('Teams affectation'), status => $status });
		};
	}

	#-------------------------------------------------------------------------------
	# Apply filesystem
	#-------------------------------------------------------------------------------
	if ($apply{filesystem}) {
		print STDERR "[Mioga2::GroupList::ApplySkeleton] Applying filesystem\n" if ($debug);

		for (qw/private public/) {
			my $space = $_;
			next if (($space eq 'public') && !$self->Get ('public_part'));

			my $status = 1;
			try {
				if (($space eq 'public') && $self->Get ('public_part') && !$self->{attributes}->{skeleton}->{contents}->{filesystem}->{$space}) {
					# Skeleton does not have a public space configuration but user asked for a public space, clone the private space ACLs
					$self->{attributes}->{skeleton}->{contents}->{filesystem}->{public}->{acls} = $self->{attributes}->{skeleton}->{contents}->{filesystem}->{private}->{acls};
				}

				my $status = 1;
				if ($self->{attributes}->{skeleton}->{contents}->{filesystem}->{$space}) {
					# Create base directory
					$self->CreateDirectory ($space, $self->{attributes}->{skeleton}->{contents}->{filesystem}->{$space});

					# Create subdirectories
					for (@{$self->{attributes}->{skeleton}->{contents}->{filesystem}->{$space}->{dir}}) {
						$self->CreateDirectory ($space, $_);
					}

					# Create files
					for (@{$self->{attributes}->{skeleton}->{contents}->{filesystem}->{$space}->{file}}) {
						$self->CreateFile ($space, $_)
					}
				}
			}
			otherwise {
				my $err = shift;
				$status = 0;
				throw $err;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::GroupList', step => __x('Creation of {space} space', space => __($space)), status => $status });
			};
		}
	}

	print STDERR "[Mioga2::GroupList::ApplySkeleton] Leaving\n" if ($debug);
}	# ----------  end of subroutine ApplySkeleton  ----------


#===============================================================================

=head2 GetDatabaseValues

Get a hash of keys / values that fit into database table. This method
translates the attributes that can not fit as-is into DB.

=cut

#===============================================================================
sub GetDatabaseValues {
	my ($self) = @_;
	print STDERR "[Mioga2::GroupList::GetDatabaseValues] Entering\n" if ($debug);

	my %values = ();

	for (qw/ident autonomous creator_id anim_id description public_part history lang_id theme_id default_app_id/) {
		next unless $self->Has ($_);
		$values{$_} = $self->Get ($_);
	}

	# Get rowids from text attributes
	$values{theme_id} = (exists ($values{theme_id})) ? $values{theme_id} : SelectSingle ($self->{config}->GetDBH (), "SELECT rowid FROM m_theme WHERE name = " . $self->{config}->GetDBH()->quote ($self->Get ('theme')) . " AND mioga_id = " . $self->{config}->GetMiogaId ())->{rowid};
	$values{lang_id} = (exists ($values{lang_id})) ? $values{lang_id} : SelectSingle ($self->{config}->GetDBH (), "SELECT rowid FROM m_lang WHERE locale = " . $self->{config}->GetDBH()->quote ($self->Get ('lang')))->{rowid};
	$values{default_app_id} = (exists ($values{default_app_id})) ? $values{default_app_id} : SelectSingle ($self->{config}->GetDBH (), "SELECT m_application.rowid FROM m_application, m_instance_application WHERE m_application.rowid = m_instance_application.application_id AND m_application.ident = " . $self->{config}->GetDBH()->quote ($self->Get ('default_app')) . " AND m_instance_application.mioga_id = " . $self->{config}->GetMiogaId ())->{rowid};

	print STDERR "[Mioga2::GroupList::GetDatabaseValues] Values: " . Dumper \%values if ($debug);

	print STDERR "[Mioga2::GroupList::GetDatabaseValues] Leaving\n" if ($debug);
	return (\%values);
}	# ----------  end of subroutine GetDatabaseValues  ----------


#===============================================================================

=head2 CreateDirectory

Create a directory in group's space.

=over

=item B<arguments:>

=over

=item I<$space:> a string containing "private" or "public" to select the space the directory has to be created in.

=item I<$info:> a hash with the following keys:

=over

=item name: the directory name

=item acls: a hash describing ACLs to apply to the directory

=over

=item profile: the profile to which the ACL is defined

=item acl: the value for the ACL (see Mioga2::Constants for values)

=back

=back

=back

=back

=cut

#===============================================================================
sub CreateDirectory { # TODO (SNI - 20/04/2010 16:54): Handle creation of parent directories if not exist
	my ($self, $space, $info) = @_;
	print STDERR "[Mioga2::GroupList::CreateDirectory] Entering ($space / " . ($info->{name} ? $info->{name} : '---') . ")\n" if ($debug);

	my ($dir, $uri);

	my $path = $info->{name};

	for ($self->GetGroups ()) {
		my $group = $_;

		#-------------------------------------------------------------------------------
		# Initialize directory and URI base
		#-------------------------------------------------------------------------------
		for ($space) {
			if (/private/) {
				$dir = $self->{config}->GetPrivateDir().'/'.$group->{ident};
				$uri = $self->{config}->GetPrivateURI().'/'.$group->{ident};
			}
			elsif (/public/) {
				$dir = $self->{config}->GetPublicDir().'/'.$group->{ident};
				$uri = $self->{config}->GetPublicURI().'/'.$group->{ident};
			}
			else {
				throw Mioga2::Exception::Group ('Mioga2::GroupList::CreateDirectory', "Incorrect space specification: $space");
			}
		}

		#-------------------------------------------------------------------------------
		# Append relative path
		#-------------------------------------------------------------------------------
		if ($path) {
			$path =~ s/^[\/]?(.*)[\/]?$/\/$1/;
			$dir .= $path;
			$uri .= $path;
		}

		#-------------------------------------------------------------------------------
		# Get parent URI
		#-------------------------------------------------------------------------------
		my $parent_sql;
		if ($path && !$info->{acls}) {
			my @uri_parts = split '/', $uri;
			pop @uri_parts;
			my @uris;
			foreach my $part (@uri_parts) {
				next unless $part;
				if (defined($uris[-1])) {
					print STDERR "[Mioga2::GroupList::CreateDirectory] uris[-1] = $uris[-1]  part = $part\n" if ($debug);
					push @uris, $uris[-1]."/$part";
				}
				else {
					push @uris, "/$part";
				}
			}
			@uris = map {$self->{config}->GetDBH()->quote ($_)} @uris;
	  
			$parent_sql = "(SELECT rowid FROM m_uri WHERE (uri = " . join(" OR uri = ", @uris) . ") AND rowid = parent_uri_id ORDER BY length(uri) DESC LIMIT 1)";
		}
		else {
			$parent_sql = "currval('m_uri_rowid_seq')";
		}

		if (!$path) {
			#-------------------------------------------------------------------------------
			# Relative path is not defined, we are creating group base directory.
			# This can not be done via Mioga2::DAVFS.
			#-------------------------------------------------------------------------------

			#-------------------------------------------------------------------------------
			# Create directory
			#-------------------------------------------------------------------------------
			if (! (-d $dir)) {
				mkdir ($dir, 0770) or throw Mioga2::Exception::Group ("Mioga2::GroupList::CreateDirectory", __x("Cannot create dir: {directory}", directory => $dir));
			}

			#-------------------------------------------------------------------------------
			# Declare URI
			#-------------------------------------------------------------------------------
			my $size = (-s $dir) || 0;
			my $sql = "INSERT INTO m_uri SELECT nextval('m_uri_rowid_seq') AS rowid, m_group.rowid AS group_id, " . $self->{config}->GetDBH()->quote ($uri) . " AS uri, $parent_sql AS parent_uri_id, 'f' AS stop_inheritance, now() AS modified, $group->{anim_id} AS user_id, '' AS lang, 'directory' AS mimetype, $size AS size FROM m_group WHERE m_group.rowid = $group->{rowid} AND " . $self->{config}->GetDBH()->quote ($uri) . " NOT IN (SELECT uri FROM m_uri WHERE group_id = $group->{rowid}) LIMIT 1;";
			ExecSQL ($self->{config}->GetDBH (), $sql);

			for (@{$info->{acls}}) {
				# Initialize access internal code
				my $access;
				for ($_->{acl}) {
					# Initialize specified access
					if (/readwrite/) {
						$access = AUTHZ_WRITE;
					}
					elsif (/read/) {
						$access = AUTHZ_READ;
					}
					else {
						$access = AUTHZ_NONE;
					}
				}

				# Declare specified access to URI
				$sql = "INSERT INTO m_authorize (uri_id, access) SELECT m_uri.rowid AS uri_id, $access AS access FROM m_uri WHERE m_uri.uri = " . $self->{config}->GetDBH()->quote ($uri) . " AND $access NOT IN (SELECT access FROM m_authorize WHERE uri_id = m_uri.rowid);";
				ExecSQL ($self->{config}->GetDBH (), $sql);

				# Delete any already-defined authorization for profile
				$sql = "DELETE FROM m_authorize_profile WHERE (authz_id, profile_id) IN (SELECT m_authorize.rowid AS authz_id, m_profile.rowid AS profile_id FROM m_authorize_profile, m_authorize, m_profile, m_uri WHERE m_authorize_profile.profile_id = m_profile.rowid AND m_authorize.rowid = m_authorize_profile.authz_id AND m_authorize.uri_id = m_uri.rowid AND m_uri.group_id = m_profile.group_id AND m_uri.uri = " . $self->{config}->GetDBH()->quote ($uri) . " AND m_profile.ident = " . $self->{config}->GetDBH()->quote ($_->{profile}) . " AND m_profile.group_id = $group->{rowid});";
				ExecSQL ($self->{config}->GetDBH (), $sql);

				# Attach profile to specified access
				$sql = "INSERT INTO m_authorize_profile SELECT m_authorize.rowid AS authz_id, m_profile.rowid AS profile_id FROM m_authorize, m_profile, m_uri WHERE m_profile.group_id = $group->{rowid} AND m_profile.ident = " . $self->{config}->GetDBH()->quote ($_->{profile}) . " AND m_authorize.uri_id = m_uri.rowid AND m_uri.uri = " . $self->{config}->GetDBH()->quote ($uri) . " AND m_authorize.access = $access;";
				ExecSQL ($self->{config}->GetDBH (), $sql);
			}
		}
		else {
			#-------------------------------------------------------------------------------
			# Relative path is defined, we are populating group base directory.
			# This can be done via Mioga2::DAVFS.
			#-------------------------------------------------------------------------------
			my $user = Mioga2::UserList->new ($self->{config}, { attributes => { rowid => $group->{anim_id} } });
			my $davfsutils = Mioga2::DAVFSUtils->new ($self->{config}, $user);
			$davfsutils->MakeCollection ($uri) or throw Mioga2::Exception::Group ('Mioga2::GroupList::CreateDirectory', 'DAVFSUtils::MakeCollection failed for ' . $uri);

			#-------------------------------------------------------------------------------
			# Set ACLs
			#-------------------------------------------------------------------------------
			my $uri_obj = SelectSingle ($self->{config}->GetDBH (), "SELECT rowid FROM m_uri WHERE uri = " . $self->{config}->GetDBH ()->quote ($uri));
			if ($uri_obj) {
				my $uri_id = $uri_obj->{rowid};

				for (@{$info->{acls}}) {
					# Initialize access internal code
					my $access;
					for ($_->{acl}) {
						# Initialize specified access
						if (/readwrite/) {
							$access = AUTHZ_WRITE;
						}
						elsif (/read/) {
							$access = AUTHZ_READ;
						}
						else {
							$access = AUTHZ_NONE;
						}
					}

					# Get profile ID
					my $profile = SelectSingle ($self->{config}->GetDBH (), "SELECT rowid FROM m_profile WHERE ident = " . $self->{config}->GetDBH ()->quote ($_->{profile}) . " AND group_id = $group->{rowid}");
					if ($profile) {
						my $profile_id = $profile->{rowid};

						AuthzDuplicateParentAuthz ($self->{config}->GetDBH (), $uri_id);
						AuthzUpdateAuthzForProfile ($self->{config}->GetDBH (), $uri_id, $profile_id, $access);
					}
				}
			}
		}
	}
	
	print STDERR "[Mioga2::GroupList::CreateDirectory] Leaving\n" if ($debug);
}	# ----------  end of subroutine CreateDirectory  ----------


#===============================================================================

=head2 CreateFile

Create a file in group's space.

=over

=item B<arguments:>

=over

=item I<$space:> a string containing "private" or "public" to select the space the file has to be created in.

=item I<$info:> a hash with the following keys:

=over

=item name: the file name

=item src: the path to a file that has to be copied to the user's personal space

=item acls: a hash describing ACLs to apply to the file

=over

=item profile: the profile to which the ACL is defined

=item acl: the value for the ACL (see Mioga2::Constants for values)

=back

=back

=back

=back

=cut

#===============================================================================
sub CreateFile { # TODO (SNI - 20/04/2010 16:54): Handle creation of parent directories if not exist
	my ($self, $space, $info) = @_;
	print STDERR "[Mioga2::GroupList::CreateFile] Entering\n" if ($debug);

	my $uri;

	# Get file size
	$info->{size} = -s $info->{src};

	for ($self->GetGroups ()) {
		my $group = $_;

		#-------------------------------------------------------------------------------
		# Generate URI
		#-------------------------------------------------------------------------------
		for ($space) {
			if (/private/) {
				$uri = $self->{config}->GetPrivateURI().'/'.$group->{ident};
			}
			elsif (/public/) {
				$uri = $self->{config}->GetPublicURI().'/'.$group->{ident};
			}
			else {
				print STDERR "[Mioga2::GroupList::CreateFile] Incorrect space specification: $space\n";
				return;
			}
		}

		my $path = $info->{name};
		if ($path) {
			$path =~ s/^[\/]?(.*)$/\/$1/;
			$uri .= $path;
		}

		#-------------------------------------------------------------------------------
		# Create file
		#-------------------------------------------------------------------------------
		my $user = Mioga2::UserList->new ($self->{config}, { attributes => { rowid => $group->{anim_id} } });
		my $davfsutils = Mioga2::DAVFSUtils->new ($self->{config}, $user);
		$davfsutils->PutFile ($uri, $info->{src}, $info->{size}) or throw Mioga2::Exception::Group ('Mioga2::GroupList::CreateFile', 'DAVFSUtils::PutFile failed for ' . $uri);

		#-------------------------------------------------------------------------------
		# Set ACLs
		#-------------------------------------------------------------------------------
		my $uri_obj = SelectSingle ($self->{config}->GetDBH (), "SELECT rowid FROM m_uri WHERE uri = " . $self->{config}->GetDBH ()->quote ($uri));
		if ($uri_obj) {
			my $uri_id = $uri_obj->{rowid};

			for (@{$info->{acls}}) {
				# Initialize access internal code
				my $access;
				for ($_->{acl}) {
					# Initialize specified access
					if (/readwrite/) {
						$access = AUTHZ_WRITE;
					}
					elsif (/read/) {
						$access = AUTHZ_READ;
					}
					else {
						$access = AUTHZ_NONE;
					}
				}

				# Get profile ID
				my $profile = SelectSingle ($self->{config}->GetDBH (), "SELECT rowid FROM m_profile WHERE ident = " . $self->{config}->GetDBH ()->quote ($_->{profile}) . " AND group_id = $group->{rowid}");
				if ($profile) {
					my $profile_id = $profile->{rowid};

					AuthzDuplicateParentAuthz ($self->{config}->GetDBH (), $uri_id);
					AuthzUpdateAuthzForProfile ($self->{config}->GetDBH (), $uri_id, $profile_id, $access);
				}
			}
		}
	}

	print STDERR "[Mioga2::GroupList::CreateFile] Leaving\n" if ($debug);
}	# ----------  end of subroutine CreateFile  ----------


#===============================================================================

=head2 SetWhereStatement

Initialize a WHERE statement that matches group list in m_group according to
provided attributes.

=over

=item B<arguments:>

=over

=item I<@attributes:> a list of inner attributes to base the statement on.

=item B<return:> a string to paste as-is into a request, after the WHERE keyword.

=back

=back

=cut

#===============================================================================
sub SetWhereStatement {
	my ($self, @attributes) = @_;

	$self->{loading} = 1;
	$self->{where} = {};

	# Set rules to m_group
	my @rules;
	for my $attrname (@attributes) {
		next if (grep /^$attrname$/, qw/skeleton password nb_users nb_teams theme_ident/);
		my $request = $self->RequestForAttribute ($attrname);
		push (@rules, $request) unless ($request eq '');
	}
	push (@rules, 'm_group.mioga_id = ' . $self->{config}->GetMiogaId ());
	$self->{where}->{m_group} = join (' AND ', @rules);

	# Set rules to other tables
	$self->{where}->{m_group_type} = "m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'group')";
	$self->{where}->{m_lang} = 'm_group.lang_id = m_lang.rowid';
	$self->{where}->{m_theme} = 'm_group.theme_id = m_theme.rowid';
	$self->{where}->{lc} = 'm_group.rowid = lc.group_id';

	print STDERR "[Mioga2::GroupList::SetWhereStatement] Rules: " . Dumper $self->{where} if ($debug);

	$self->{loading} = 0;
	delete ($self->{match});

	return ($self->{where});
}	# ----------  end of subroutine SetWhereStatement  ----------


#===============================================================================

=head2 GetWhereStatement

Generate a WHERE statement that matches group list in m_group.

=over

=item B<return:> a string to paste as-is into a request, after the WHERE keyword.

=back

=cut

#===============================================================================
sub GetWhereStatement {
	my ($self, @tables) = @_;
	print STDERR "[Mioga2::GroupList::GetWhereStatement] Entering\n" if ($debug);

	$self->SetWhereStatement () if (!$self->{where});

	@tables = qw/m_group m_group_type m_lang m_theme lc/ unless (@tables);

	my @parts;
	for my $table (@tables) {
		push (@parts, $self->{where}->{$table});
	}

	my $where = join (' AND ', @parts);

	print STDERR "[Mioga2::GroupList::GetWhereStatement] Leaving: $where\n" if ($debug);
	return ($where);
}	# ----------  end of subroutine GetWhereStatement  ----------


#===============================================================================

=head2 GetAdditionnalTables

Return list of additionnal tables to include into the FROM statement.

=cut

#===============================================================================
sub GetAdditionnalTables {
	my ($self) = @_;

	if (!$self->{additionnal_tables}) {
		$self->{additionnal_tables} = 'm_lang, m_theme, (SELECT m_group.rowid AS group_id, max(m_group_user_last_conn.last_connection) AS last_connection FROM m_group LEFT JOIN m_group_user_last_conn ON m_group.rowid = m_group_user_last_conn.group_id GROUP BY m_group.rowid) AS lc';
	}

	return ($self->{additionnal_tables});
}	# ----------  end of subroutine GetAdditionnalTables  ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project

=head1 SEE ALSO

Mioga2 Mioga2::GroupList Mioga2::TeamList Mioga2::ApplicationList Mioga2::InstanceList

=head1 COPYRIGHT

Copyright (C) 2011, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;
