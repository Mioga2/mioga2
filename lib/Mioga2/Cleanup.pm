#===============================================================================
#
#         FILE:  Cleanup.pm
#
#  DESCRIPTION:  Mioga2 mod_perl cleanup handler
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  06/12/2010 15:52
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Cleanup.pm: Mioga2 mod_perl cleanup handler

=head1 DESCRIPTION

Mioga2 mod_perl cleanup handler (see http://perl.apache.org/docs/2.0/user/handlers/http.html#PerlCleanupHandler)

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Cleanup;

use Mioga2::tools::database;
use Mioga2::MiogaConf;
use Mioga2::Login;

use Data::Dumper;

my $debug = 0;

sub handler : method {
    my($class, $r) = @_;
	#-----------------------------------------------------------
	# Load Mioga Config
	#-----------------------------------------------------------
	my $config_file = $r->dir_config("MiogaConfig");

	my $miogaconf;
	if(defined $config_file) {
		$miogaconf = new Mioga2::MiogaConf($config_file);
	}

	my $login_uri = $miogaconf->GetBasePath () . $miogaconf->GetLoginURI ();

	if ($r->uri =~ /^$login_uri/) {
		warn("==> Mioga2::Cleanup::handler Cleaning-up authentication sessions\n") if ($debug);
		if ($miogaconf) {
			Mioga2::Login::CleanupExpired ($miogaconf);
		}
		else {
			$r->log_error("==> Mioga2::Cleanup::handler : Can't find MiogaConfig parameter");
		}
	}
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Authen Mioga2::Authz Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;

