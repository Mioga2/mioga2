#===============================================================================
#
#         FILE:  TeamList.pm
#
#  DESCRIPTION:  New generation Mioga2 team list class
#
# REQUIREMENTS:  ---
#        NOTES:  
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  16/04/2010 11:24
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

TeamList.pm: New generation Mioga2 team list class

=head1 DESCRIPTION

This class handles Mioga2 teams. It is designed to create, load, update or delete
teams.

=head1 SYNOPSIS

 use Mioga2::TeamList;
 use Mioga2::Config;

 my $teamlist;
 my $config = new Mioga2::Config (...);

 # Delete team whose ident is myteam
 $teamlist = Mioga2::TeamList->new ($config, { attributes => { ident => 'myteam' } });
 $teamlist->Delete ();

 # Create a new team from attributes
 $teamlist = Mioga2::TeamList->new ($config, {
 		attributes => {
 			ident => 'myteam',
 		}
 	});
 $teamlist->Store ();

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::TeamList;
use base qw(Mioga2::Old::Compatibility);

use Data::Dumper;

use Locale::TextDomain::UTF8 'teamlist';

use Error qw(:try);
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::APIGroup;
use Mioga2::Exception::Group;
use Mioga2::GroupList;
use Mioga2::UserList;
use Mioga2::CSV;
use Mioga2::tools::args_checker;
use Mioga2::Journal;

my $debug = 0;

my @text_fields = qw/description ident animator/;

#===============================================================================

=head2 new

Create a new Mioga2::TeamList object

=over

=item B<arguments:>

=over

=item I<$config:> a Mioga2::Config object matching the instance to work into.

=item I<$data:> a hashref with the following keys:

=over

=item I<attributes:> a list of key / value pairs matching the teams.

=item I<short_list:> an optional flag to limit the amount of data describing the teams.

=item I<fields:> a list of fields describing the teams (this overrides the behavior of the flag above).

=item I<match:> a string indicating the match type for teams (may be 'begins', 'contains', 'ends' or may be empty for exact match).

=item I<journal:> a Mioga2::Journal-tied-array to record operations.

=back

=item B<return:> a Mioga2::TeamList object matching teams according
to provided attributes.

=back

=back

=cut

#===============================================================================
sub new {
	my ($class, $config, $data) = @_;
	print STDERR "[Mioga2::TeamList::new] Entering, data: " . Dumper $data if ($debug);

	my $self = { };
	bless($self, $class);

	$self->{config} = $config;

	# Initialize attributes
	$self->{attributes} = $data->{attributes};

	# Initialize object state
	$self->{loaded} = 0;
	$self->{deleted} = 0;

	# Fields obtained from DB can be expressed in a fuzzy way (short_list) initializing a pre-defined field-list
	if (exists ($data->{short_list}) && $data->{short_list}) {
		# Restricted list of fields
		$self->{fields} = "m_group.rowid, m_group.ident";
		$self->{short_list} = delete ($data->{short_list});
	}
	else {
		# Full list of fields
		$self->{fields} = 'm_group.*';
		$self->{short_list} = delete ($data->{short_list});
	}

	# Match type can be 'begins', 'contains' or 'ends' to find user from a part of their attributes
	if (defined ($data->{match})) {
		my $match = $data->{match};
		if (grep (/^$match$/, qw/begins contains ends/)) {
			$self->{match} = $data->{match};
		}
		delete ($data->{match});
	}

	# Fields obtained from DB can be chosen by caller
	if (defined ($data->{fields})) {
		$self->{fields} = delete ($data->{fields});
	}

	# Journal of operations
	if (defined ($data->{journal})) {
		$self->{journal} = $data->{journal};
	}
	else {
		tie (@{$self->{journal}}, 'Mioga2::Journal');
	}

	# Initialize WHERE statement before any update is made on the list
	$self->SetWhereStatement (keys (%{$self->{attributes}}));

	print STDERR "[Mioga2::TeamList::new] Leaving\n" if ($debug);
	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 Set

Set an attribute value.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to set.

=item I<$value:> the attribute value to set.

=back

=back

=cut

#===============================================================================
sub Set {
	my ($self, $attribute, $value) = @_;

	$self->{update}->{$attribute} = $value;
}	# ----------  end of subroutine Set  ----------


#===============================================================================

=head2 Get

Get an attribute value.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get.

=back

=item B<return:> the attribute value.

=back

=cut

#===============================================================================
sub Get {
	my ($self, $attribute) = @_;
	print STDERR "[Mioga2::TeamList::Get] Entering, attribute name is $attribute\n" if ($debug);

	my $value = undef;

	#-------------------------------------------------------------------------------
	# Load the object from DB if attribute is not known already
	#-------------------------------------------------------------------------------
	if ((!grep /^$attribute$/, keys (%{$self->{attributes}})) && (!grep /^$attribute$/, keys (%{$self->{update}})) && !$self->{loaded}) {
		# The requested attribute is not set into object yet, get it from DB
		$self->Load ();
	}

	#-------------------------------------------------------------------------------
	# Retreive attribute value
	#-------------------------------------------------------------------------------
	if (exists ($self->{update}->{$attribute})) {
		# Attribute is in update list, get its value
		$value = $self->{update}->{$attribute};
	}
	elsif (exists ($self->{attributes}->{$attribute})) {
		# Attribute is in initial list, get its value
		$value = $self->{attributes}->{$attribute};
	}
	else {
		# Attribute is not in initial list, nor in update list, pick it from loaded teams
		if (scalar (@{$self->{teams}}) > 1) {
			# Multiple teams in the list, return an array of attribute values.
			$value = ();
			for (@{$self->{teams}}) {
				push (@$value, $_->{$attribute});
			}
		}
		else {
			# Only one team in the list, return attribute value
			$value = $self->{teams}->[0]->{$attribute};
		}
	}

	# Add '%' signs according to match attribute
	if (exists ($self->{match}) && (ref ($value) eq '')) {
		if ($self->{match} eq 'begins') {
			$value = "$value%";
		}
		elsif ($self->{match} eq 'contains') {
			$value = "%$value%";
		}
		elsif ($self->{match} eq 'ends') {
			$value = "%$value";
		}
		else {
			throw Mioga2::Exception::Simple ('Mioga2::TeamList::RequestForAttribute', "Unknown match type: $self->{match}");
		}
	}

	if (ref ($value) eq 'ARRAY') {
		print STDERR "[Mioga2::TeamList::Get] Leaving, attribute value is " . Dumper $value if ($debug);
	}
	elsif (ref ($value) eq 'Mioga2::UserList') {
		my @rowids = $value->GetRowIds ();
		print STDERR "[Mioga2::TeamList::Get] Leaving, attribute value is truncated, Mioga2::UserList rowids " . Dumper \@rowids if ($debug);
	}
	elsif (ref ($value) eq 'Mioga2::GroupList') {
		my @rowids = $value->GetRowIds ();
		print STDERR "[Mioga2::TeamList::Get] Leaving, attribute value is truncated, Mioga2::GroupList rowids " . Dumper \@rowids if ($debug);
	}
	else {
		print STDERR "[Mioga2::TeamList::Get] Leaving, attribute value is $value\n" if ($debug);
	}
	return ($value);
}	# ----------  end of subroutine Get  ----------


#===============================================================================

=head2 Store

Store changes to database. This method has to be called to actually create or
update one or more teams. To delete teams, see the "Delete" method.

=cut

#===============================================================================
sub Store {
	my ($self) = @_;
	print STDERR "[Mioga2::TeamList::Store] Entering\n" if ($debug);

	if ((!$self->Count ()) && (!$self->{deleted})) {
		#-------------------------------------------------------------------------------
		# Create a team into DB
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::TeamList::Store] Creating new team\n" if ($debug);

		# Set creator and anim IDs to instance admin if not defined
		$self->Set ('creator_id', $self->{config}->GetAdminId ()) unless ($self->Has ('creator_id'));
		$self->Set ('anim_id', $self->{config}->GetAdminId ()) unless ($self->Has ('anim_id'));

		# Set Mioga ID
		$self->Set ('mioga_id', $self->{config}->GetMiogaId ());
		$self->Set ('status', 'active') unless ($self->Has ('status'));

		my @list = qw/ident description mioga_id creator_id anim_id status/;
		my $fields = join (', ', map  ($self->RequestForAttribute ($_)->[0], @list) );
		my $values = join (', ', map  ($self->RequestForAttribute ($_)->[2], @list) );

		my $sql = "INSERT INTO m_group ($fields, type_id, created, modified) VALUES ($values, (SELECT rowid FROM m_group_type WHERE ident='team'), NOW(), NOW());";

		print STDERR "[Mioga2::TeamList::Store] SQL: $sql\n" if ($debug);

		my $status = 1;
		try {
			ExecSQL ($self->{config}->GetDBH (), $sql);
		}
		otherwise {
			my $err = shift;
			$status = 0;
			push (@{$self->{journal}}, { module => 'Mioga2::TeamList', step => $err->{"-text"}, status => $status});
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::TeamList', step => __x('Team {ident} creation', ident => $self->Get ('ident')), status => $status });
		};
	}
	elsif (scalar (keys (%{$self->{update}}))) {
		#-------------------------------------------------------------------------------
		# Update team
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::TeamList::Store] Updating existing group\n" if ($debug);

		my $sql = "UPDATE m_group SET ";
		for (keys (%{$self->{update}})) {
			my $request = $self->StringRequestForAttribute ($_);
			$request =~ s/IS NULL$/ = ''/;
			$sql .= "$request, " if ($request ne '');
		}
		$sql .= "modified = now ()";
		$sql .= " FROM " . $self->GetAdditionnalTables () if ($self->GetAdditionnalTables () ne '');
		$sql .= " WHERE " . $self->GetWhereStatement ();

		my $status = 1;
		try {
			ExecSQL ($self->{config}->GetDBH (), $sql);
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::TeamList', step => __x('Team "{ident}" update', ident => $self->Get ('ident')), status => $status });
		};
	}
	else {
		print STDERR "[Mioga2::TeamList::Store] Nothing has to be done\n" if ($debug);
	}

	#-------------------------------------------------------------------------------
	# Update WHERE statement, as some modifications were made, teams can not be
	# matched anymore by the same WHERE statement, matching according to rowids
	#-------------------------------------------------------------------------------
	$self->SetWhereStatement (qw/rowid/);

	#-------------------------------------------------------------------------------
	# Update-pending values are now stored into DB, they can be moved to the inner
	# attributes of the object
	#-------------------------------------------------------------------------------
	for (keys (%{$self->{update}})) {
		$self->{attributes}->{$_} = delete ($self->{update}->{$_});
	}

	print STDERR "[Mioga2::TeamList::Store] Leaving\n" if ($debug);
}	# ----------  end of subroutine Store  ----------


#===============================================================================

=head2 Count

Count the teams in the list. This method loads the team list from DB if
required.

=over

=item B<return:> the number of teams in the list.

=back

=cut

#===============================================================================
sub Count {
	my ($self) = @_;
	print STDERR "[Mioga2::TeamList::Count] Entering\n" if ($debug);

	my $count = undef;

	if ($self->{loaded}) {
		# Get count from loaded items
		$count = scalar (@{$self->{teams}});
	}
	else {
		# Get count from dedicated request
		my $sql = "SELECT count(*) FROM m_group WHERE " . $self->GetWhereStatement ();
		$count = SelectSingle ($self->{config}->GetDBH (), $sql)->{count};
	}

	print STDERR "[Mioga2::TeamList::Count] Leaving: $count teams\n" if ($debug);
	return ($count);
}	# ----------  end of subroutine Count  ----------


#===============================================================================

=head2 GetRowIds

Get RowIds for teams in the list.

=over

=item B<return:> an array of rowids.

=back

=cut

#===============================================================================
sub GetRowIds {
	my ($self) = @_;

	my @list = ();

	$self->Load () unless $self->{loaded};

	for (@{$self->{teams}}) {
		push (@list, $_->{rowid});
	}

	return (@list);
}	# ----------  end of subroutine GetRowIds  ----------


#===============================================================================

=head2 GetTeams

Get list of teams in the list.

=over

=item B<return:> an array of hashes describing the teams in the list. Each hash
of this array can be used to create a new Mioga2::TeamList that
only matches this team.

=back

=cut

#===============================================================================
sub GetTeams {
	my ($self) = @_;
	print STDERR "[Mioga2::TeamList::GetTeams] Entering.\n" if ($debug);

	$self->Load () unless $self->{loaded};

	for my $team (@{$self->{teams}}) {
		for (keys (%{$self->{update}})) {
			$team->{$_} = $self->{update}->{$_};
		}
	}

	print STDERR "[Mioga2::TeamList::GetTeams] Teams: " . Dumper $self->{teams} if ($debug);
	return (@{$self->{teams}});
}	# ----------  end of subroutine GetTeams  ----------


#===============================================================================

=head2 Delete

Delete teams.

=cut

#===============================================================================
sub Delete {
	my ($self) = @_;
	print STDERR "[Mioga2::TeamList::Delete] Entering\n" if ($debug);

	if ($self->Count ()) {
		for ($self->GetTeams ()) {
			print STDERR "[Mioga2::TeamList::Delete] Deleting team rowid $_->{rowid}, mioga_id $_->{mioga_id} (ident '$_->{ident}')\n" if ($debug);
			my $status = 1;
			try {
				TeamDelete ($self->{config}, $_->{rowid}, 1); # TODO (SNI - 21/04/2010 08:54): Handle this inside the module
			}
			otherwise {
				my $err = shift;
				$status = 0;
				throw $err;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::TeamList', step => __x('Team "{ident}" deletion', ident => $_->{ident}), status => $status });
			};
		}

		$self->{deleted} = 1;
	}

	print STDERR "[Mioga2::TeamList::Delete] Leaving\n" if ($debug);
}	# ----------  end of subroutine Delete  ----------


#===============================================================================

=head2 InviteUsers

Invite users into teams.

=over

=item B<arguments:>

=over

=item I<@users:> an array of user idents.

=back

=back

=cut

#===============================================================================
sub InviteUsers {
	my ($self, @users) = @_;
	print STDERR "[Mioga2::TeamList::InviteUsers] Entering\n" if ($debug);

	my $where = $self->GetWhereStatement ();
	my $mioga_id = $self->{config}->GetMiogaId ();
	my $users_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_) } @users);

	# Revoke not specified users
	my $sql = "DELETE FROM m_group_group USING m_group, m_user_base WHERE m_group_group.group_id = m_group.rowid AND m_group_group.invited_group_id = m_user_base.rowid" . (($users_idents eq '') ? '' : " AND m_user_base.ident NOT IN ($users_idents)") . " AND $where;";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	if ($users_idents ne '') {
		# Generate and run SQL query
		$sql = "INSERT INTO m_group_group SELECT m_group.rowid AS group_id, m_user_base.rowid AS invited_group_id FROM m_group, m_user_base WHERE m_user_base.ident IN ($users_idents) AND m_user_base.mioga_id = $mioga_id AND $where AND m_user_base.rowid NOT IN (SELECT invited_group_id FROM m_group_group WHERE group_id = m_group.rowid);";
		ExecSQL ($self->{config}->GetDBH (), $sql);
	}

	print STDERR "[Mioga2::TeamList::InviteUsers] Leaving\n" if ($debug);
}	# ----------  end of subroutine InviteUsers  ----------


#===============================================================================

=head2 JoinGroups

Invite teams into groups.

=over

=item B<arguments:>

=over

=item I<%groups:> a hash whose keys are group idents and values are associated profile ident.

=back

=back

=cut

#===============================================================================
sub JoinGroups {
	my ($self, %groups) = @_;
	print STDERR "[Mioga2::TeamList::JoinGroups] Entering\n" if ($debug);

	my $where = $self->GetWhereStatement ();
	my $mioga_id = $self->{config}->GetMiogaId ();
	my $groups_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_) } keys (%groups));

	# Revoke teams from all groups they were member of and are not anymore
	for my $group_attrs ($self->GetInvitedGroupList ()->GetGroups ()) {
		my $group_ident = $group_attrs->{ident};
		if (!grep (/^$group_ident$/, keys (%groups))) {
			for my $team_id (@{ac_ForceArray ($self->Get ('rowid'))}) {
				LaunchMethodInApps($self->{config}, "RevokeTeamFromGroup", $group_attrs->{rowid}, $team_id, $group_attrs->{anim_id});

				# Revoke each team member that is not namely member of the group
				my $team = Mioga2::TeamList->new ($self->{config}, { attributes => { rowid => $team_id } });
				my $teamusers = ac_ForceArray ($team->GetInvitedUserList ()->Get ('rowid'));
				my $group_id = $group_attrs->{rowid};
				my $group_anim = $group_attrs->{anim_id};
				my $group = Mioga2::GroupList->new ($self->{config}, { attributes => { rowid => $group_id } });
				my $groupusers = ac_ForceArray ($group->GetInvitedUserList ()->Get ('rowid'));
				for my $user_id (@$teamusers) {
					next if (grep (/^$user_id$/, @$groupusers));
					LaunchMethodInApps($self->{config}, "RevokeUserFromGroup", $group_id, $user_id, $group_anim);
				}
			}
		}
	}

	# Revoke teams from not specified groups
	my $sql = "DELETE FROM m_profile_group USING m_profile, m_group, m_group_base, m_group_type WHERE m_group_base.type_id = m_group_type.rowid AND m_group_type.ident = 'group' AND m_profile_group.group_id = m_group.rowid AND m_profile_group.profile_id = m_profile.rowid AND m_profile.group_id = m_group_base.rowid" . (($groups_idents eq '') ? '' : " AND m_group_base.ident NOT IN ($groups_idents)") . " AND $where;";
	ExecSQL ($self->{config}->GetDBH (), $sql);
	$sql = "DELETE FROM m_group_group USING m_group, m_group_base, m_group_type WHERE m_group_base.type_id = m_group_type.rowid AND m_group_type.ident = 'group' AND m_group_group.group_id = m_group_base.rowid AND m_group_group.invited_group_id = m_group.rowid" . (($groups_idents eq '') ? '' : " AND m_group_base.ident NOT IN ($groups_idents)") . " AND m_group_base.mioga_id = $mioga_id AND $where;";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	if ($groups_idents ne '') {
		# Invite teams into group
		$sql = "INSERT INTO m_group_group SELECT m_group_base.rowid AS group_id, m_group.rowid AS invited_group_id FROM m_group, m_group_base WHERE m_group_base.ident IN ($groups_idents) AND m_group_base.mioga_id = $mioga_id AND $where AND m_group.rowid NOT IN (SELECT invited_group_id FROM m_group_group WHERE group_id = m_group_base.rowid);";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Affect profiles to teams
		for (keys (%groups)) {
			my $group_ident = $_;
			my $profile_ident = ($groups{$_} ne '') ? $self->{config}->GetDBH()->quote ($groups{$_}) : $self->{config}->GetDBH ()->quote (SelectSingle ($self->{config}->GetDBH (), "SELECT m_profile.ident FROM m_profile, m_profile_group, m_group_base, m_group WHERE m_profile_group.group_id = m_group.rowid AND m_profile.group_id = m_group_base.rowid AND m_profile_group.profile_id = m_profile.rowid AND $where AND m_group.mioga_id = $mioga_id AND m_group_base.ident = " . $self->{config}->GetDBH ()->quote ($group_ident) . " AND m_group_base.mioga_id = $mioga_id UNION SELECT m_profile.ident FROM m_profile, m_group WHERE m_profile.rowid = m_group.default_profile_id AND m_group.ident = " . $self->{config}->GetDBH ()->quote ($group_ident) . " AND m_group.mioga_id = $mioga_id LIMIT 1")->{ident});

			# Create new profile affectation
			$sql = "INSERT INTO m_profile_group SELECT m_profile.rowid AS profile_id, m_group.rowid AS group_id FROM m_profile, m_group, m_group_base WHERE m_profile.group_id = m_group_base.rowid AND m_group_base.ident = " . $self->{config}->GetDBH()->quote ($group_ident) . " AND m_group_base.mioga_id = $mioga_id AND $where AND m_profile.ident = $profile_ident AND m_group.rowid NOT IN (SELECT group_id FROM m_profile_group WHERE profile_id IN (SELECT m_profile.rowid FROM m_profile, m_group WHERE m_group.ident = " . $self->{config}->GetDBH()->quote ($group_ident) . " AND m_group.mioga_id = $mioga_id AND m_profile.group_id = m_group.rowid));";
			ExecSQL ($self->{config}->GetDBH (), $sql);
		}
	}

	print STDERR "[Mioga2::TeamList::JoinGroups] Leaving\n" if ($debug);
}	# ----------  end of subroutine JoinGroups  ----------


#===============================================================================

=head2 GetInvitedUserList

Get user list of team members.

=over

=item B<arguments:>

=over

=item I<%attributes:> attributes to initialize the resulting Mioga2::UserList.
This is mainly intented to pass short_list flag to Mioga2::UserList. It could
lead to inconsistent Mioga2::UserList contents if other keys (such as 'ident')
are passed.

=back

=back

=over

=item B<return:> a Mioga2::UserList object.

=back

=cut

#===============================================================================
sub GetInvitedUserList {
	my ($self, %attributes) = @_;

	my $short_list = (exists ($attributes{short_list})) ? $attributes{short_list} : $self->{short_list};

	my $users = Mioga2::UserList->new ($self->{config}, { attributes => { 'teams' => $self }, short_list => $short_list, journal => $self->{journal} });

	return ($users);
}	# ----------  end of subroutine GetInvitedUserList  ----------


#===============================================================================

=head2 GetInvitedGroupList

Get group list the team is member of.

=over

=item B<arguments:>

=over

=item I<%attributes:> attributes to initialize the resulting Mioga2::GroupList.
This is mainly intented to pass short_list flag to Mioga2::GroupList. It could
lead to inconsistent Mioga2::GroupList contents if other keys (such as 'ident')
are passed.

=back

=back

=over

=item B<return:> a Mioga2::GroupList object.

=back

=cut

#===============================================================================
sub GetInvitedGroupList {
	my ($self, %attributes) = @_;

	my $short_list = (exists ($attributes{short_list})) ? $attributes{short_list} : $self->{short_list};

	my $groups = Mioga2::GroupList->new ($self->{config}, { attributes => { 'teams' => $self }, short_list => $short_list, journal => $self->{journal} });

	return ($groups);
}	# ----------  end of subroutine GetInvitedGroupList  ----------


#===============================================================================

=head2 Revert

Revert list.

=cut

#===============================================================================
sub Revert {
	my ($self) = @_;

	$self->{where} = "m_group.rowid NOT IN (SELECT m_group.rowid FROM m_group WHERE " . $self->GetWhereStatement () . ") AND m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'team') AND m_group.mioga_id = " . $self->{config}->GetMiogaId ();

	$self->{attributes} = { };
	$self->{update} = { };
	$self->{loaded} = 0;
	$self->{deleted} = 0;

	print STDERR "[Mioga2::TeamList::Revert] New WHERE statement: $self->{where}\n" if ($debug);
}	# ----------  end of subroutine Revert  ----------


#===============================================================================

=head2 GetJournal

Get journal of operations as an object

=cut

#===============================================================================
sub GetJournal {
	my ($self) = @_;

	my $journal = tied (@{$self->{journal}});

	return ($journal);
}	# ----------  end of subroutine GetJournal  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


#===============================================================================

=head2 Load

Load object attributes from DB. This method is automatically called when trying
to access an attribute whose value has not already been loaded from DB.

=cut

#===============================================================================
sub Load {
	my ($self) = @_;
	print STDERR "[Mioga2::TeamList::Load] Entering\n" if ($debug);

	my $mioga_id = $self->{config}->GetMiogaId ();

	#-------------------------------------------------------------------------------
	# Initialize and run SQL query
	#-------------------------------------------------------------------------------
	my $sql = 'SELECT ' . $self->{fields} . ', coalesce (T1.group_count, 0) AS nb_users, coalesce (T2.group_count, 0) AS nb_groups FROM m_group LEFT JOIN (SELECT count(m_group_group.*) AS group_count, m_group_group.group_id AS team_id FROM m_group_group, m_user_base WHERE m_user_base.mioga_id = ' . $mioga_id . ' AND m_user_base.rowid = m_group_group.invited_group_id GROUP BY m_group_group.group_id) AS T1 ON m_group.rowid = T1.team_id LEFT JOIN (SELECT count(m_group_group.*) AS group_count, m_group_group.invited_group_id AS team_id FROM m_group_group, m_group_base, m_group_type WHERE m_group_base.mioga_id = ' . $mioga_id . ' AND m_group_base.rowid = m_group_group.group_id AND m_group_base.type_id = m_group_type.rowid AND m_group_type.ident = \'group\' GROUP BY m_group_group.invited_group_id) AS T2 ON m_group.rowid = T2.team_id WHERE ' . $self->GetWhereStatement ();

	# If loading teams for group, get profile information as well
	if ($self->Has ('groups')) {
		$sql = 'SELECT TMAIN.*, coalesce (T1.group_count, 0) AS nb_users FROM (SELECT ' . $self->{fields} . ', m_profile.ident AS profile, m_profile.rowid AS profile_id FROM m_group, m_profile, m_profile_group, m_group_group  WHERE ' . $self->GetWhereStatement () . " AND m_group.rowid = m_group_group.invited_group_id AND m_profile.group_id = m_group_group.group_id AND m_profile.rowid = m_profile_group.profile_id AND m_profile_group.group_id = m_group_group.invited_group_id AND m_group_group.group_id IN (SELECT m_group.rowid FROM m_group, " . $self->Get ('groups')->GetAdditionnalTables () . " WHERE " . $self->Get ('groups')->GetWhereStatement () . ") AND m_profile_group.group_id = m_group_group.invited_group_id) AS TMAIN LEFT JOIN (SELECT count(m_group_group.*) AS group_count, m_group_group.group_id AS team_id FROM m_group_group, m_user_base WHERE m_user_base.mioga_id = " . $mioga_id . " AND m_user_base.rowid = m_group_group.invited_group_id GROUP BY m_group_group.group_id) AS T1 ON TMAIN.rowid = T1.team_id";
	}

	print STDERR "[Mioga2::TeamList::Load] SQL: $sql\n" if ($debug);
	my $res = SelectMultiple ($self->{config}->GetDBH (), $sql);

	#-------------------------------------------------------------------------------
	# Drop undefined values
	#-------------------------------------------------------------------------------
	for my $team (@$res) {
		for (keys (%$team)) {
			delete ($team->{$_}) if (!defined ($team->{$_}));
		}
	}

	#-------------------------------------------------------------------------------
	# Update object contents
	#-------------------------------------------------------------------------------
	$self->{teams} = $res;

	# Raise "loaded" flag if query returned something
	$self->{loaded} = 1 if (scalar (@{$self->{teams}}));

	print STDERR "[Mioga2::TeamList::Load] Leaving\n" if ($debug);
}	# ----------  end of subroutine Load  ----------


#===============================================================================

=head2 StringRequestForAttribute

Format RequestForAttribute returned array as a string to paste as-is into a
select or update request.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get associated SQL request.

=back

=item B<return:> a string to update attribute value or to select from attribute
value.

=back

=cut

#===============================================================================
sub StringRequestForAttribute {
	my ($self, $argument) = @_;

	my $request = '';

	my $ret = $self->RequestForAttribute ($argument);

	$request = "$ret->[0] $ret->[1] $ret->[2]" if ($ret->[0] ne '' && $ret->[2] ne '');

	return ($request);
}	# ----------  end of subroutine StringRequestForAttribute  ----------


#===============================================================================

=head2 RequestForAttribute

Generate SQL statement that matches attribute into m_group.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get associated SQL request.

=back

=item B<return:> an array containing the attribute name, the operator to be
used and the statement to match this attribute in m_group table. If the
attribute is stored as-is into table, this method returns the attribute and its
value. If the attribute is stores as the ID of a record in another table, this
method returns the attribute name and a SQL subquery picking the attribute ID
from this other table. If the attribute has to be ignored, this method returns
empty strings.

=back

=cut

#===============================================================================
sub RequestForAttribute {
	my ($self, $attribute) = @_;

	my $value = '';
	my $operator = '=';
	my $attrval = '';

	# Change operator if a match type is specified
	if (exists ($self->{match}) && (grep (/^$attribute$/, @text_fields))) {
		$operator = 'ILIKE';
	}

	#-------------------------------------------------------------------------------
	# Format string so that attribute value can be a scalar or an array
	#-------------------------------------------------------------------------------
	if (ref ($self->Get ($attribute)) eq 'ARRAY') {
		$operator = 'IN';
		for (@{$self->Get ($attribute)}) {
			$attrval = "(" . join (', ', map { $self->{config}->GetDBH()->quote ($_) } @{$self->Get ($attribute)}) . ")";
		}
		if ($attrval eq '') {
			$attrval = '(NULL)';
		}
	}
	elsif (ref ($self->Get ($attribute)) eq 'Mioga2::UserList') {
		$operator = 'IN';
		my @rowids = $self->Get ($attribute)->GetRowIds ();
		$attrval = "(SELECT group_id FROM m_group_group WHERE invited_group_id IN (" . join (', ', @rowids) . "))";
	}
	elsif (ref ($self->Get ($attribute)) eq 'Mioga2::GroupList') {
		$operator = 'IN';
		my @rowids = $self->Get ($attribute)->GetRowIds ();
		$attrval = "(SELECT invited_group_id FROM m_group_group WHERE group_id IN (" . join (', ', @rowids) . "))";
	}
	else {
		if ($self->Has ($attribute) && ($self->Get ($attribute) eq '')) {
			$operator = 'IS';
			$attrval = 'NULL';
		}
		else {
			$attrval = $self->{config}->GetDBH()->quote ($self->Get ($attribute));
		}
	}

	#-------------------------------------------------------------------------------
	# Process attribute name
	#-------------------------------------------------------------------------------
	if (grep (/^$attribute$/, qw/status/)) {
		# Some attributes need their ID to be picked from another table which name is not m_$attribute
		$value = "(SELECT rowid FROM m_group_$attribute WHERE ident $operator $attrval)";
		$attribute .= '_id';
	}
	elsif (grep (/^$attribute$/, qw/users groups/)) {
		$value = $attrval;
		$attribute = 'rowid';
	}
	else {
		# Other attributes are simply associated to their text value
		$value = $attrval if ($self->Has ($attribute));
	}

	my @ret = ($attribute, $operator, $value);

	return (\@ret);
}	# ----------  end of subroutine RequestForAttribute  ----------


#===============================================================================

=head2 Has

Check if a team list has a given attribute set.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to check.

=back

=item B<return:> 1 or 0 whether the object has or not a given attribute in its
current state.

=back

B<Warning:> This method doesn't load the list from DB so it is possible that
the object does not have an attribute that the teams of the list will have once
they have been loaded.

=cut

#===============================================================================
sub Has {
	my ($self, $attribute) = @_;

	return (exists ($self->{update}->{$attribute}) || exists ($self->{attributes}->{$attribute}) || ($attribute eq 'rowid'));
}	# ----------  end of subroutine Has  ----------


#===============================================================================

=head2 GetDatabaseValues

Get a hash of keys / values that fit into database table. This method
translates the attributes that can not fit as-is into DB.

=cut

#===============================================================================
sub GetDatabaseValues {
	my ($self) = @_;
	print STDERR "[Mioga2::TeamList::GetDatabaseValues] Entering\n" if ($debug);

	my %values = ();

	for (qw/ident creator_id anim_id/) {
		$values{$_} = $self->Get ($_);
	}

	print STDERR "[Mioga2::TeamList::GetDatabaseValues] Values: " . Dumper \%values if ($debug);

	print STDERR "[Mioga2::TeamList::GetDatabaseValues] Leaving\n" if ($debug);
	return (\%values);
}	# ----------  end of subroutine GetDatabaseValues  ----------


#===============================================================================

=head2 SetWhereStatement

Initialize a WHERE statement that matches team list in m_group according to
provided attributes.

=over

=item B<arguments:>

=over

=item I<@attributes:> a list of inner attributes to base the statement on.

=item B<return:> a string to paste as-is into a request, after the WHERE keyword.

=back

=back

=cut

#===============================================================================
sub SetWhereStatement {
	my ($self, @attributes) = @_;

	# Fetch attributes, if any
	for my $attr (@attributes) {
		$self->Get ($attr);
	}

	my $where = '';

	for my $attrname (@attributes) {
		next if (grep /^$attrname$/, qw/skeleton password nb_groups nb_users/);
		my $request = $self->StringRequestForAttribute ($attrname);
		$where .= "m_group.$request AND " unless ($request eq '');
	}

	$where .= "m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'team') AND m_group.mioga_id = " . $self->{config}->GetMiogaId ();

	print STDERR "[Mioga2::TeamList::SetWhereStatement] $where\n" if ($debug);

	$self->{where} = $where;
	delete ($self->{match});

	return ($self->{where});
}	# ----------  end of subroutine SetWhereStatement  ----------


#===============================================================================

=head2 GetWhereStatement

Generate a WHERE statement that matches team list in m_group.

=over

=item B<return:> a string to paste as-is into a request, after the WHERE keyword.

=back

=cut

#===============================================================================
sub GetWhereStatement {
	my ($self) = @_;

	if (!$self->{where}) {
		my $where = '';

		for my $attrname (keys (%{$self->{attributes}})) {
			my $request = $self->StringRequestForAttribute ($attrname);
			$where .= "m_group.$request AND " unless ((grep /^$attrname$/, qw/skeleton/) || ($request eq ''));
		}

		$where .= "m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'team') AND ";
		$where .= "m_group.mioga_id = " . $self->{config}->GetMiogaId ();

		print STDERR "[Mioga2::TeamList::GetWhereStatement] $where\n" if ($debug);

		$self->{where} = $where;
	}

	return ($self->{where});
}	# ----------  end of subroutine GetWhereStatement  ----------


#===============================================================================

=head2 GetAdditionnalTables

Return list of additionnal tables to include into the FROM statement.

=cut

#===============================================================================
sub GetAdditionnalTables {
	my ($self) = @_;

	if (!$self->{additionnal_tables}) {
		$self->{additionnal_tables} = '';
	}

	return ($self->{additionnal_tables});
}	# ----------  end of subroutine GetAdditionnalTables  ----------


#===============================================================================

=head2 ToCSV

Export TeamList to CSV.

=cut

#===============================================================================
sub ToCSV {
	my ($self) = @_;

	my $csv = '';

	my $conv = new Mioga2::CSV (
			columns => [qw/lastname firstname email ident/]
		);

	$self->Load () if (!$self->{loaded});
	$csv = $conv->Convert ($self->{teams});

	print STDERR "[Mioga2::TeamList::ToCSV] $csv\n" if ($debug);

	return ($csv);
}	# ----------  end of subroutine ToCSV  ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project

=head1 SEE ALSO

Mioga2 Mioga2::GroupList Mioga2::TeamList Mioga2::ApplicationList Mioga2::InstanceList

=head1 COPYRIGHT

Copyright (C) 2011, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;
