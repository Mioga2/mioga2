# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
AnimUser.pm: The Mioga2 User animation application.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::AnimResource;
use strict;

use base qw(Mioga2::AnimUser);
use Locale::TextDomain::UTF8 'animresource';

use Error qw(:try);
use Data::Dumper;
use Mioga2::AnimUser;
use Mioga2::AnimGroup;
use Mioga2::Content::XSLT;
use Mioga2::SimpleLargeList;
use Mioga2::AppDescList;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::APIProfile;
use Mioga2::Exception::Simple;

my $debug = 0;


# ============================================================================
# Profiles list description
# ============================================================================

my %profiles_sll_desc = ( 'name' => 'anim_profiles_sll',
						  
						   'sql_request' => {
							   'select' => "m_profile.*, (SELECT count(*) FROM m_profile_group WHERE profile_id = m_profile.rowid) AS nb_member",
							   
							   "select_count" => "count(*)",
							   
							   "from"   => "m_profile",

							   "where"  => "group_id = ?",
                         },
                        
						  'fields' =>  [ [ 'rowid', 'rowid'],
										 [ 'ident', 'link', 'ModifyProfile'],
										 [ 'nb_member', 'link', 'DisplayProfileUsers'],
									   ],

						  'default_sort_field' => 'ident',
                       
                       );


my %applications_sll_desc = ( 'name' => 'anim_applications_sll',
						   
						   'sql_request' => {
							   'select' => "m_application_group_status.*",
							   
							   "select_count" => "count(*)",
							   
							   "from"   => "m_application_group_status",

  							   "where"  => "m_application_group_status.mioga_id = ? AND m_application_group_status.group_id = ? AND ".
								           "m_application_group_status.usable_by_resource IS TRUE",

							   },
                        
                        'fields' => [ [ 'rowid', 'rowid'],
                                      [ 'ident', 'normal'],
                                      [ 'realident', 'hidden'],
									  [ 'change_app_status', 'checkboxes', ' (! ($context->GetGroup()->GetRowid() == $config->GetAdminId() and $_->{realident} eq "Admin")) and $_->{realident} !~ /^Anim/'],
                                      [ 'is_public', 'link', 'ChangeApplicationPublic', '$_->{can_be_public} and $_->{status} == 1' ],
                                  ],
							  
						'default_sort_field' => 'ident',
                       );


# ============================================================================
#
# Initialize ()
#
#	Method that can be overloaded by Application implementation to initialize
#	variable, resource, etc, at the object creation time.
#
#	Return nothing.
#
# ============================================================================

sub Initialize {
	my ($self) = @_;
	
	$self->{stylesheet} = "anim_resource.xsl";
	$self->{appname} = "AnimResource";
	$self->{defaultfunc} = "DisplayUsers";
    $self->{locale_domain} = 'animresource_xsl';

	return;
}

# ============================================================================
#
# GetAppDesc ()
#
#	Return the current application description.
#
# ============================================================================

sub GetAppDesc {
	my ($self, $context) = @_;

    my %AppDesc = ( ident   => 'AnimResource',
	    	name	=> __("Management"),
                package => 'Mioga2::AnimResource',
                description => __('The Mioga2 Resource Animation Application '),
				type    => 'resource',
                all_groups => 0,
                all_users  => 1,
                can_be_public => 0,
				is_user             => 0,
				is_group            => 0,
				is_resource         => 1,

                functions   => { 'Animation' => __('Animation Functions') },

                func_methods  => { 'Animation' => [ 'DisplayMain', 'DisplayUsers', 
													'DisplayTeams', 'DisplayThemes',
													'InviteUsers', 'InviteTeams', 
													'DisplayProfiles',
													'CreateProfile', 'ModifyProfile', 'DisplayProfileUsers', 'DisplayProfileTeams',
													'InviteProfileUsers', 'InviteProfileTeams',
													],
							   },

                non_sensitive_methods  => [ 'DisplayMain', 'DisplayUsers',
													'DisplayTeams', 'DisplayThemes',
													'InviteUsers', 'InviteTeams',
													'DisplayProfiles',
													'CreateProfile', 'ModifyProfile', 'DisplayProfileUsers', 'DisplayProfileTeams',
													'InviteProfileUsers', 'InviteProfileTeams',
													],
				
                );
	return \%AppDesc;
}




# ============================================================================

=head2 DisplayProfiles ()

	Display the Mioga2 Profile List.

	See Mioga2::AnimGroup->DisplayProfiles

=cut

# ============================================================================

sub DisplayProfiles {
	my ($self, $context) = @_;

	my $sll_desc = \%profiles_sll_desc;

	return $self->Mioga2::AnimGroup::DisplayProfiles($context, $sll_desc);
}


# ============================================================================

=head2 DisplayApplications ()

	Display the Mioga2 Application List.

	See Mioga2::AnimGroup->DisplayApplications

=cut

# ============================================================================

sub DisplayApplications {
	my ($self, $context) = @_;
	
	return $self->SUPER::DisplayApplications($context, \%applications_sll_desc);
}


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================
#
# ModifyThemeAndLang ()
#
#	commit themes_lang selection in database.
#
# ============================================================================

sub ModifyThemeAndLang {
	my ($self, $config, $values) = @_;

	ResourceModify($config, $values);
}


# ============================================================================
#
# ModifyDefaultProfile ()
#
#	commit default profile modification in database.
#
# ============================================================================

sub ModifyDefaultProfile {
	my ($self, $config, $group, $profile_id) = @_;

	ResourceModify($config, { rowid => $group->GetRowid(), default_profile_id => $profile_id});
}


# ============================================================================
#
# GetSelectedApps ()
#
#	Return the list of selected apps for group
#
# ============================================================================

sub GetSelectedApps {
	my ($self, $config, $group_id) = @_;

	my $mioga_id = $config->GetMiogaId();
	my $dbh = $config->GetDBH();

	return SelectMultiple($dbh, "select rowid from m_application_group_status where mioga_id = $mioga_id AND group_id = $group_id and status > 0");
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


