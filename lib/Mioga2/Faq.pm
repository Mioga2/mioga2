# ============================================================================
# Mioga2 Project (C) 2003-2010 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Faq.pm: FAQ application for Mioga2.

=head1 DESCRIPTION

This is an application for creating and viewing Frequently Asked Questions.
They can be organised into categories. Each FAQ consists of a title, a question
and an answer.

=head1 METHODS DESRIPTION

=cut

package Mioga2::Faq;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'faq';

use Date::Manip;
use Data::Dumper;
use Mioga2::Application;
use Mioga2::Content::XSLT;
use Mioga2::Exception::Group;
use Mioga2::Exception::User;
use Mioga2::Exception::Resource;
use Mioga2::Exception::Simple;
use Mioga2::Old::Group;
use Mioga2::Old::User;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::args_checker;
use Mioga2::tools::database;
use Mioga2::tools::date_utils;
use Mioga2::tools::string_utils;

# ============================================================================
# Global vars
# ============================================================================

my $debug = 0;

# ============================================================================
# Public Methods
# ============================================================================
# ============================================================================

=head2 DisplayMainAnim

DisplayMainAnim is the same as DisplayMain but with anim actions displayed

See DisplayMain

=cut

# ============================================================================
sub DisplayMainAnim
{
	my ($self, $context) = @_;
	$context->{args}->{admin_mode} = "yes";
	return $self->DisplayMain($context);
}
# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc
{
	my $self = shift;
    my %AppDesc = ( ident   => 'Faq',
	    	name	=> __('FAQ'),
                package => 'Mioga2::Faq',
		type => 'normal',
                description => __('The Mioga2 FAQ Application'),
                all_groups  => 1,
                all_users  => 0,
                can_be_public => 0,
				is_user             => 0,
				is_group            => 1,
				is_resource         => 0,

                functions   => { 
					'Anim' => __('Animation functions'),
					'Read' => __('Read functions'),
		               },

		'func_methods' 	=> {
					'Anim' => [ 'DisplayMainAnim', 'DeleteEntry', 'DeleteCategory',
							'AddCategory',  'AddEntry',
							'SetOrder' ],
					'Read' 	=> [ 'DisplayMain', 'ViewFaq' ],
					},
		non_sensitive_methods => [
			'DisplayMainAnim',
			'DisplayMain',
			'ViewFaq'
		],

                );
    
	return \%AppDesc;
}


# ============================================================================

=head2 DisplayMain

DisplayMain is the main function. 

=head3 Generated XML

See AddEntry, AddCategory, DeleteCategory, ViewSingleEntry, DeleteEntry and ViewFaq

=cut

# ============================================================================

sub DisplayMain
{
	my ($self, $context) = @_;

	if($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), 'Anim')) {
		$context->{args}->{admin_mode} = "yes";
	} else {
		$context->{args}->{admin_mode} = "no";
	}

	if (st_ArgExists($context, 'act_insert_entry'))	{
		return $self->AddEntry($context);
	
	} elsif(st_ArgExists($context, 'act_insert_category')) {
		return $self->AddCategory($context);
		
	} elsif (st_ArgExists($context, 'act_delete_category'))	{
		return $self->DeleteCategory($context);
	
	} elsif (st_ArgExists($context, 'act_delete_elem'))	{
		return $self->DeleteEntry($context);
	
	} else {
		return $self->ViewFaq($context);
	}
}


# ============================================================================

=head2 ViewFaq

ViewFaq is used to display question/answer entries

=head3 Generated XML

    <ViewFaq>

        <miogacontext>See Mioga2::RequestContext</miogacontext>

		<!-- Navigation context -->
		<appnav>
			<group><ident>...</ident><url></url></group>
			...
			<step><label>...</label><url>...</url></step>
			...	
		</appnav>

        <!-- List of categories -->
		<category empty="{true|false}">
            <title>Category Title</title>
            <rowid>Category rowid</rowid>
		</category>
        ...

        <admin>Can animate FAQ ? (0|1)</admin>
        <idcat>ID of current category</idcat>

		<!-- List of entries -->
		<entry>
			<rowid>...</rowid>
		    <title>...</title>
		    <question>...</question>
		    <answer>...</answer>
		</entry>
		...

     </ViewFaq>

=cut

# ============================================================================

sub ViewFaq
{
	my ($self, $context)	= @_;
	my $method		= "DisplayMain";
	$method			.= "Anim" if ((st_ArgExists($context, 'admin_mode'))
			&& ($context->{args}->{admin_mode} eq "yes"));

	my $session = $context->GetSession();
	my $config = $context->GetConfig();
	my $dbh = $config->GetDBH();
	my $group = $context->GetGroup();
	my $user = $context->GetUser();
	my $id_group	= $group->GetRowid();
	my $group_ident = $group->GetIdent();
	my $id_user		= $user->GetRowid();
	
	my $id_cat = 0;
	if (st_ArgExists($context, 'idcat')) {
		my ($args, $errors) = ac_CheckArgs($context, 
				[ [ ['idcat'], 'want_int', ['max' => '10'] ]
				]);
		$id_cat = $args->{idcat} unless @$errors;
	}

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'faq.xsl',
			locale_domain => 'faq_xsl');

	my $admin_rights = "no";

	my $result = ProfileTestAuthzMethod($dbh, $id_user, $id_group, 'Faq', 'DisplayMainAnim');
	if (($result) && (defined($context->{args}->{admin_mode})) 
			&& ($context->{args}->{admin_mode} eq "yes")) {
		$admin_rights = "yes";
	}

	my $categories = $self->dbGetCategories($dbh, $id_group, $id_cat);
	my $entries = $self->dbGetEntries($dbh, $id_group, $id_cat);
	
	my $xml = '<?xml version="1.0"?>';
	$xml .= "<ViewFaq>";
	$xml .= $context->GetXML();
	
	$xml .= "<appnav>";
	my ($nr_instances, $xmlref_instances) = $self->AllSuchApplications($context);
	$xml .= $$xmlref_instances; # if $nr_instances > 1;
	
	my @steps = ();
	if ($id_cat) {
		$xml .= "<step><label>".__("FAQ start")."</label>"
			. ($id_cat == 0 ? '' : "<url>?idcat=0</url>")
			. "</step>";
		
		my $current_cat = $id_cat; # walk our way up to the root
		do {
			my $cat = $self->dbGetCategory($dbh, $current_cat);
			my $t	= $cat->{title};
			$t = st_FilterHTMLTags(\$t);
			unshift @steps, "<step><label>$t</label>"
					. ($cat->{rowid} == $id_cat ? '' : "<url>?idcat=".$cat->{rowid}."</url>")
					. "</step>";
			$current_cat = $cat->{parent_category};
		} while(defined($current_cat) && ($current_cat != 0));
	}
	$xml .= join('', @steps);
	$xml .= "</appnav>";
	
	foreach my $category (@$categories) {
		$xml .= "<category";
		if ($admin_rights) {
			$xml .= ($self->dbCategoryHasContent($dbh, $category->{rowid})
					? ' empty="false"' : ' empty="true"');
		}
		$xml .= ">"; 
		$xml .= "<title><![CDATA[$category->{title}]]></title>";
		$xml .= "<rowid>$category->{rowid}</rowid>";
		$xml .= "</category>";
	}
	
	foreach my $entry (@$entries) {
		$xml .= "<entry>"
				. "<rowid>$entry->{rowid}</rowid>"
				. "<title><![CDATA[$entry->{title}]]></title>"
				. "<question><![CDATA[$entry->{question}]]></question>"
				. "<answer><![CDATA[$entry->{answer}]]></answer>"
				. "</entry>";
	}

	$xml .= "<admin>$admin_rights</admin>";
	$xml .= "<idcat>$id_cat</idcat>";
	$xml .= "</ViewFaq>";

	$content->SetContent($xml);
	
	print STDERR $xml."\n" if $debug > 1;
	return $content;
}

# ============================================================================

=head2 AddCategory

AddCategory is used to add a new faq category

=head3 Generated output

	JSON: { success: "true" }
	   or { success: "false", error: "..." }

=head3 Form actions and fields :
    
    - act_insert_category : Try to insert or update a new category

      Arguments :
         name                type                  description
       ---------------------------------------------------------------------
1  		dbaction    :  "insert" or "update"
2       title       :  not empty string      : the category name
3       idcat       :  string                : the parent category rowid (root if empty)
4       rowid       :  not empty string      : the category rowid
 
When (1) is "insert", (2) and (3) are mandatory.
When (1) is "update", (2) and (4) are mandatory.      

=cut

# ============================================================================

sub AddCategory
{
	my ($self, $context) = @_;

	my $method = "DisplayMain";
	$method .= "Anim" if ((defined($context->{args}->{admin_mode})) && ($context->{args}->{admin_mode} eq "yes"));
	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'faq.xsl', locale_domain => 'faq_xsl');

	my $config = $context->GetConfig();
	my $dbh = $config->GetDBH();
	
	my ($args, $errors) = ac_CheckArgs($context, 
			[ [ ['idcat', 'rowid'], 'want_int', ['max' => '10'] ],
			  [ ['dbaction'], ['match' => /^(?:insert|update)$/ ] ],
			]);
	if (@$errors) {
		return { success => 'false', error => __("The supplied data is invalid") };
  	}

	my $group = $context->GetGroup();
	my $id_group = $group->GetRowid();
	my $id_cat = $args->{idcat} || 0;
	my $title = "";
	my $error = 0;
	my $dbaction = $args->{dbaction} || "insert";
	my $rowid = $args->{rowid} || 0;

	my %keep = ( # HTML tags allowed; A tag must not be allowed because we have a href on titles to access the categories
		b	=> 1,
		br	=> 1,
		i	=> 1,
		u	=> 1,
		hr	=> 1,
		li	=> 1,
		ol	=> 1,
		s	=> 1,
		ul	=> 1,
		);

	if (st_ArgExists($context, 'act_insert_category')) { # Called by XHR POST, return JSON or XML	
		$title = $context->{args}->{title};
		my $title_check = String::Checker::checkstring($title, ['stripxws', 'disallow_empty']);
		if (defined $title_check and @$title_check)
		{
			$error = $self->error("title");
		}
		$title = st_FilterHTMLTags(\$title, \%keep);
		
		return { success => "false", error => $error } if $error;
		
		if ($dbaction eq "insert") {
				$self->dbAddCategory($dbh, $id_group, $title, $id_cat);
		}
		elsif (($dbaction eq "update") && ($rowid != 0)) {
				$self->dbUpdateCategory($dbh, $rowid, $title);
		}
		return { success => "true" };
	
	} else {
		return { success => "false", error => __("The supplied data is invalid") };
	}		
}
# ============================================================================

=head2 AddEntry

AddEntry is used to add a new entry (title/question/answer)
or to update an existing entry.

=head3 Generated output

	JSON: { success: "true" }
	   or { success: "false", error: "..." }

=head3 Form actions and fields :
    
    - act_insert_entry : Try to insert or update a new entry

      Arguments :
         name                type                  description
       ---------------------------------------------------------------------
1       dbaction    :  "insert" or "update"
2       title       :  not empty string      : the category name
3       question    :  not empty string      : the category name
4       answer      :  not empty string      : the category name
5       idcat       :  string                : the parent category rowid (root if empty)
6       rowid       :  not empty string      : the category rowid
 
When (1) is "insert", (2)-(4) and (5) are mandatory.
When (1) is "update", (2)-(4) and (6) are mandatory.      

=cut

# ============================================================================
sub AddEntry
{
	my ($self, $context) = @_;
	my $method = "DisplayMain";
	$method .= "Anim" if ((defined($context->{args}->{admin_mode})) 
			&& ($context->{args}->{admin_mode} eq "yes"));

	my $config = $context->GetConfig();
	my $dbh    = $config->GetDBH();
	my $group  = $context->GetGroup();
	my $user   = $context->GetUser();

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'faq.xsl',
			locale_domain => 'faq_xsl');
	my $id_group = $group->GetRowid();


	my ($args, $errors) = ac_CheckArgs($context, 
			[ [ ['idcat', 'rowid'], 'want_int', ['max' => '10'] ],
			  [ ['dbaction'], ['match' => /^(?:insert|update)$/ ] ],
			  [ ['act_insert_entry', 'question', 'answer', 'title'], 'allow_empty' ]
			]);
	if (@$errors) {
		return { success => 'false', error => __("The supplied data is invalid") };
  	}
   	my $id_cat = $args->{idcat} || 0;

	my $title = "";
	my $question = "";
	my $answer = "";
	my $error = 0;
	my $dbaction = $args->{dbaction} || "insert";
	my $rowid = $args->{rowid} || 0;
	my %keep = ( # HTML tags allowed
		strong => 1,
		em => 1,
		span => {style => 1},
		p => {style => 1},
		br	=> 1,
		b	=> 1,
		i	=> 1,
		u	=> 1,
		strike => 1,
#		hr	=> 1,
		li	=> 1,
		ol	=> 1,
#		s	=> 1,
		ul	=> 1,
		div => { align => 1, style => 1 },
		a	=> { target => 1, href => 1 }
		);

	if (st_ArgExists($context, 'act_insert_entry')) { # Called by XHR POST, return JSON or XML
		$title    = $context->{args}->{title};
		$question = $context->{args}->{question};
		$answer   = $context->{args}->{answer};

		my $title_check = String::Checker::checkstring($title, ['stripxws', 'disallow_empty']);
		if (defined $title_check and @$title_check)	{
			$error = $self->error("title");
		}

		my $question_check = String::Checker::checkstring($question, ['stripxws', 'disallow_empty']);
		if (defined $question_check and @$question_check) {
			$error = $self->error("question");
		}

		my $answer_check = String::Checker::checkstring($answer, ['stripxws', 'disallow_empty']);
		if (defined $answer_check and @$answer_check) {
			$error = $self->error("answer");
		}
		
		$question = Mioga2::tools::string_utils::st_FilterHTMLTags(\$question, \%keep);
		$answer   = Mioga2::tools::string_utils::st_FilterHTMLTags(\$answer,   \%keep);
		# don't allow a href in title because a link on title is already used to access the Q/R
		delete $keep{a}; 
		$title    = Mioga2::tools::string_utils::st_FilterHTMLTags(\$title,    \%keep);
		
		$error = $self->error("title_too_long") if (length($title) > 128);
		$error = $self->error("answer_too_long") if (length($answer) > 65000);
		$error = $self->error("question_too_long") if (length($question) > 65000);

		return { success => "false", error => $error } if $error;

		# $title =~ s/\r\n/<BR \/>/g;
		# $question =~ s/\r\n/<BR \/>/g;
		# $answer =~ s/\r\n/<BR \/>/g;

		if ($dbaction eq "insert") {
			$self->dbAddEntry($dbh, $id_group, $title, $id_cat, $question, $answer);
		
		} elsif (($dbaction eq "update") && ($rowid != 0)) {
			$self->dbUpdateEntry($dbh, $rowid, $title, $question, $answer);
		}
	
		return { success => "true" };
	
	} else {
		return { success => "false", error => __("The supplied data is invalid") };
	}
}

# ============================================================================

=head2 SetOrder

SetOrder receives a new mapping of FAQ entry IDs to position numbers,
in the form of POST data "mode = {entries|categories}, row{ROWID} = {POSITION}". 

=head3 Generated output

	JSON: { success: "true" }
	   or { success: "false", error: "..." }

=cut

# ============================================================================

sub SetOrder
{
	my ($self, $context) = @_;
	
	my @rowids = ();
	foreach my $rowid (keys %{$context->{args}}) {
		push @rowids, $rowid if $rowid =~ m#^row\d+$#; 
	}
	my ($args, $errors) = ac_CheckArgs($context, [ [ \@rowids, 'want_int', ['max' => '4'] ] ]);
   	if (@$errors) {
   		return { 'success' => 'false', 'error' => __("The supplied data is invalid") };
   	}
   	my $table;
   	{
   		my $mode = $context->{args}->{mode};
   		unless (defined $mode) {
   			return { 'success' => 'false', 'error' => __("The supplied data is invalid") };
   		}
   		if    ($mode eq "entries")    { $table = "faq"; } 
   		elsif ($mode eq "categories") { $table = "faq_categories"; }
   		else { return { 'success' => 'false', 'error' => __("The supplied data is invalid") }; }
   	}
   	
   	my $dbh = $context->GetConfig()->GetDBH();
   
   	while (my ($key, $value) = each %$args) {
   		if ($key =~ m#^row(\d+)#) {
   			my $rowid = $1;
   			$self->dbSetPosition($dbh, $table, $rowid, $value);
   		}
   	}
   	
   	return { 'success' => 'true' };
}


# ============================================================================

=head2 DeleteEntry

DeleteEntry deletes the entry with the given rowid (POST parameter "entry_id").
 
=head3 Generated output

	JSON: { success: "true" }
	   or { success: "false", error: "..." }

=cut

# ============================================================================

sub DeleteEntry
{
	my ($self, $context) = @_;

	unless ($context->GetAccessMethod() eq 'POST') {
		return { success => 'false', error => 'Forbidden access method.' };
	}
	
	my ($args, $errors) = ac_CheckArgs($context, 
			[ [ ['entry_id'], 'want_int', ['max' => '10'] ] ]);
	if (@$errors) {
   		return { success => 'false', error => __("The supplied data is invalid") };
   	}
	
	$self->dbDeleteEntry($context->GetConfig->GetDBH, $args->{entry_id});
		
	return { success => 'true'};
}

# ============================================================================

=head2 DeleteCategory

DeleteCategory works only when the category does not contain 
neither sub-categories nor entries. Must be called via HTTP POST.

=head3 Form actions and fields :

=over
    
=item I<cat_id>: string - the rowid of the category to delete

=back

=head3 Generated output

	JSON: { success: "true" }
	   or { success: "false", error: "..." }

=cut

# ============================================================================
sub DeleteCategory
{
	my ($self, $context) = @_;
	my $config = $context->GetConfig();
	my $dbh = $config->GetDBH();
	
	unless ($context->GetAccessMethod() eq 'POST') {
		return { success => 'false', error => 'Forbidden access method.' };
	}
	
	my ($args, $errors) = ac_CheckArgs($context, 
			[ [ ['cat_id'], 'want_int', ['max' => '10'] ] ]);
	if (@$errors) {
   		return { success => 'false', error => __("The supplied data is invalid") };
   	}
	my $cat_id = $args->{cat_id};
	
	if ($self->dbCategoryHasContent($dbh, $cat_id)) {
		return { success => 'false', error => __("Only empty categories can be deleted.") };
	}
	
	$self->dbDeleteCategory($dbh, $cat_id);
	
	return { success => 'true'};
}

# ============================================================================
# DeleteGroupData ($config, $group_id)
#
#    Remove group data in database when a group is deleted.
#
#    This function is called when a group/user/resource is deleted.
#
# ============================================================================

sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;

	my $dbh = $config->GetDBH();

	ExecSQL($dbh, "DELETE FROM faq_categories WHERE group_id = $group_id");
	ExecSQL($dbh, "DELETE FROM faq WHERE group_id = $group_id");
}

# ============================================================================
#
# PRIVATE METHODS DESCRIPTION
#
# ============================================================================


# Returns the human-readable error string for the given code.
sub error {
	my ($self, $code) = @_;
	# This cannot be a global variable any longer if we want to get the translations.
	# A global variable would be pre-loaded by mod_perl, while the translation depends
	# on the individual request.
	unless (defined $self->{errorMessages}) {
		my %hash = ( 'no_error' => '',
			'title' => __("Invalid title"),
			'question' => __("Invalid question"),
			'answer' => __("Invalid answer"),
			'bad_html' => __("Html tags are incorrect") . "<br/>"
					. __("Allowed tags are B, BR, I, U, HR, LI, UL, OL, S") . "<br/>"
					. __("\"A\" tag is allowed but not in title") . "<br/>"
					. __("All opened tag must be closed"),
			'title_too_long' => __("Title too long (max. 128 characters)"),
			'answer_too_long' => __("Answer is too long (max. 65000 characters)"),
			'question_too_long' => __("Question is too long (max. 65000 characters)")
		);
		foreach my $k (keys %hash) {
			utf8::decode($hash{$k});
		}
		$self->{errorMessages} = \%hash;
	}
	return $self->{errorMessages}->{$code};
}



# Methods for everything SQL-related. All these methods are private and only use
# their parameter data (no usage of "$self" object data).

# Put a FAQ entry or category into a different position.
sub dbSetPosition {
	my ($self, $dbh, $table, $rowid, $position) = @_;
	return unless $table eq 'faq' or $table eq 'faq_categories';
	my $sql = "UPDATE $table SET position=$position WHERE rowid=$rowid";
	ExecSQL($dbh, $sql);
}


# Does the given FAQ category have sub-categories or entries? Returns 1 or 0.
sub dbCategoryHasContent {
	my ($self, $dbh, $cat_id) = @_;
	my $sql = "SELECT COUNT(*) FROM faq_categories c, faq f"
			. " WHERE c.parent_category=$cat_id OR f.category_id=$cat_id";
	my $res = SelectSingle($dbh, $sql);
	return $res->{'count'} == 0 ? 0 : 1;
}


# Add a new category for the given group and within the given parent category.
sub dbAddCategory {
	my ($self, $dbh, $group_id, $title, $id_cat) = @_;
	$title = st_FormatPostgreSQLString($title);
	st_CutString(\$title, 128);
	
	my $sql = "INSERT INTO faq_categories"
			. " (created, modified, title, group_id, parent_category)"
			. " VALUES(now(), now(), '$title', $group_id, $id_cat)";
	ExecSQL($dbh, $sql);
}


sub dbAddEntry {
	my ($self, $dbh, $group_id, $title, $id_cat, $question, $answer) = @_;

	$title = st_FormatPostgreSQLString($title);
	st_CutString(\$title, 128);

	$question = st_FormatPostgreSQLString($question);
	st_CutString(\$question, 65000);

	$answer = st_FormatPostgreSQLString($answer);
	st_CutString(\$answer, 65000);

	my $sql = "INSERT INTO faq"
			. " (created, modified, title, group_id, category_id, question, answer, position)"
			. " VALUES(now(), now(), '$title', $group_id, $id_cat, '$question', '$answer',"
			. 	" (SELECT max(position)+1 FROM faq WHERE category_id=$id_cat) )";
	ExecSQL($dbh, $sql);
}


# Return full data of all entries of the given category and group.
sub dbGetEntries {
	my ($self, $dbh, $group_id, $id_cat) = @_;
	my $sql = "SELECT * FROM faq"
			. " WHERE group_id=$group_id AND category_id=$id_cat"
			. " ORDER BY position, rowid";
	my $result = SelectMultiple($dbh, $sql);
	return $result;
}


# Return all sub-categories for the given group in the given category
sub dbGetCategories {
	my ($self, $dbh, $group_id, $id_cat) = @_;
	my $sql = "SELECT * FROM faq_categories"
			. " WHERE group_id=$group_id AND parent_category=$id_cat"
			. " ORDER BY position, rowid";
	my $result = SelectMultiple($dbh, $sql);
	return $result;
}


# Return full data of a single category
sub dbGetCategory {
	my ($self, $dbh, $id_cat) = @_;
	my $sql = "SELECT * FROM faq_categories WHERE rowid=$id_cat";
	my $result = SelectSingle($dbh, $sql);
	return $result;
}


sub dbDeleteEntry {
	my ($self, $dbh, $id_entry) = @_;
	my $sql = "DELETE FROM faq WHERE rowid=$id_entry";
	ExecSQL($dbh, $sql);
}


sub dbDeleteCategory {
	my ($self, $dbh, $category_id) = @_;
	my $sql = "DELETE FROM faq_categories WHERE rowid=$category_id";
	ExecSQL($dbh, $sql);
}


sub dbUpdateEntry {
	my ($self, $dbh, $rowid, $title, $question, $answer) = @_;

	$title = st_FormatPostgreSQLString($title);
	st_CutString(\$title, 128);

	$question = st_FormatPostgreSQLString($question);
	st_CutString(\$question, 65000);

	$answer = st_FormatPostgreSQLString($answer);
	st_CutString(\$answer, 65000);

	my $sql = "UPDATE faq"
			. " SET title='$title', question='$question', answer='$answer', modified=now()"
			. " WHERE rowid=$rowid";
	ExecSQL($dbh, $sql);
}


sub dbUpdateCategory {
	my ($self, $dbh, $rowid, $title) = @_;
	$title = st_FormatPostgreSQLString($title);
	st_CutString(\$title, 128);
	my $sql = "UPDATE faq_categories"
			. " SET title='$title', modified=now()"
			. " WHERE rowid=$rowid";
	ExecSQL($dbh, $sql);
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2010, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
