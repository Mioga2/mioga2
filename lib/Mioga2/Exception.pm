#===============================================================================
#
#         FILE:  Exception.pm
#
#  DESCRIPTION:  
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  30/03/2012 09:03
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME
	
Exception.pm: Base class for Mioga2 exceptions

=head1 DESCRIPTION

This module is a super class for Mioga2 exceptions. It should not be used directly.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Exception;

use Error;
use vars qw(@ISA);
@ISA = qw(Error);

use Data::Dumper;

my $debug = 0;

sub new {
	my($class, $function, $text) = @_;
	my $self = $class->SUPER::new(-text => $text);
	$self->{function} = $function;
	return $self;
}

sub stringify {
	my ($self, $context) = @_;

	return $self->as_string ($context);
}

sub as_string {
	my ($self, $context) = @_;

	my @trace = split ("\n", $self->stacktrace);

	my $str = '';
	$str .= $self->format_error ('##### [BEGIN] ' . ref ($self) . ' #####');
	$str .= $self->format_error ('Message: ' . shift (@trace));

	if ($context) {
		if (!$context->IsPublic ()) {
			#-------------------------------------------------------------------------------
			# Log user ident
			#-------------------------------------------------------------------------------
			$str .= $self->format_error ('User: ' . $context->GetUser->GetIdent ());
		}

		#-------------------------------------------------------------------------------
		# Log URI
		#-------------------------------------------------------------------------------
		$str .= $self->format_error ('URI: ' . $context->GetURI()->GetURI());

		#-------------------------------------------------------------------------------
		# Log access method
		#-------------------------------------------------------------------------------
		$str .= $self->format_error ('Method: ' . $context->{access_method});

		#-------------------------------------------------------------------------------
		# Log request arguments
		#-------------------------------------------------------------------------------
		my @args = split ("\n", Dumper ($context->{args}));
		# Drop first and last lines
		pop (@args);
		shift (@args);
		$str .= $self->format_error ("Arguments:");
		for (@args) {
			$str .= $self->format_error ($_);
		}
	}
	else {
		#-------------------------------------------------------------------------------
		# No context, inform the logfile
		#-------------------------------------------------------------------------------
		$str .= $self->format_error ("No context, can not get user, URI, method, params information");
	}

	#-------------------------------------------------------------------------------
	# Log trace
	#-------------------------------------------------------------------------------
	$str .= $self->format_error ("Trace:");
	# Drop useless two last lines
	pop (@trace);
	pop (@trace);
	if (@trace) {
		for (@trace) {
			$_ =~ s/\t/    /g;
			$str .= $self->format_error ($_);
		}
	}
	else {
		$str .= $self->format_error (Dumper ($self));
	}
	
	$str .= $self->format_error ('##### [END] ' . ref ($self) . ' #####');
	
	$str .= "\n";
	return ($str);
}

sub format_error {
	my ($self, $str) = @_;

	$str ||= '';

	return ('[' . gmtime (time) . '] [error] ' . $str . "\n");
}

1;

