# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Workspace.pm: Workspace management class

=head1 DESCRIPTION

This module is useful for a generic application management method

=head1 METHODS DESRIPTION

=cut

package Mioga2::Workspace;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'workspace';

use Error qw(:try);
use Data::Dumper;
use POSIX qw(tmpnam);
use Mioga2::Content::XSLT;
use Mioga2::tools::database;
use Mioga2::Old::Group;
use Mioga2::Old::User;
use Mioga2::Crypto;
use Mioga2::AppDescList;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIExternalMioga;
use Mioga2::tools::APIWorkspace;
use Mioga2::Exception::Group;
use Mioga2::Exception::User;
use Mioga2::Exception::Resource;
use Mioga2::Exception::Simple;
use Mioga2::tools::APIAuthz;

my $debug=0;

# ============================================================================
# linls large_list
# ============================================================================

my $link_field_list =  [ [ 'rowid', 'rowid'],
                          [ 'ident', 'normal'],
                          [ 'link', 'normal'],
                          [ 'modify', 'modify', 'ModifyLink'],
                          [ 'delete', 'delete'],
                        ];

# ============================================================================
# "Normal" Users list description
# ============================================================================

my %link_sll_desc = ( 'name' => 'link_users_sll',

                       'sql_request' => {
                           'select' => "rowid, ident, link",
       
                           "select_count" => "count(*)",
       
                           "from"   => "workspace_link",

			   "where" => "ancestor_id is NULL and user_id = ?",

                           "delete" => { "from"  => "workspace_link",
                                         "where" => 'workspace_link.rowid = $_->{rowid}',
                                       },
                         },
                       
                       'fields' => $link_field_list

                       );

# ============================================================================

=head2 InitializeUserApplication ()

	This method is useful to initialize
	application values for users

	Return nothing.

=cut

# ============================================================================

sub InitializeUserApplication {
	my ($self, $conf, $usr, $application) = @_;

	# initialize default url for user
	# -------------------------------
	WorkspaceInitializeUser($conf, $usr->{rowid});

	my %create_default_url;
	if (exists($application->{default_url}))
	{
		$create_default_url{default_url} = $application->{default_url}->[0];
		$create_default_url{user_id} = $usr->{rowid};

		WorkspaceURLModify($conf, \%create_default_url);
	}
	
	my %create_home_url;
	if (exists($application->{home_url}))
	{
		$create_home_url{home_url} = $application->{home_url}->[0];
		$create_home_url{user_id} = $usr->{rowid};

		WorkspaceURLModify($conf, \%create_home_url);
	}
	# initialize each workspace entry
	# -------------------------------
	if (exists($application->{workspace_entry}))
	{
		foreach my $entry (@{$application->{workspace_entry}})
		{
			my %create_user_entry;
			$create_user_entry{user_id} = $usr->{rowid};
			$create_user_entry{display_ok} = $entry->{display_ok}->[0];
			$create_user_entry{ident} = $entry->{ident}->[0];
			$create_user_entry{number} = $entry->{number}->[0];
			WorkspaceCreateUserEntry($conf, \%create_user_entry)
		}
	}

	my $dbh = $conf->GetDBH();

	if (exists($application->{link})) {
		foreach my $link (@{$application->{link}})
		{
			my $values = { ident => $link->{ident},
						   link => $link->{content}->[0], 
						   user_id => $usr->{rowid},
					   };
			WorkspaceLinkAdd($dbh, $values);
		}
	}

}

# ============================================================================

=head2 TestAuthorize ($context)

	Check if the current user can access to the current application and 
	methods in the current group.

=cut

# ============================================================================

sub TestAuthorize {
	my ($self, $context) = @_;

	my $user = $context->GetUser();
	my $group = $context->GetGroup();

	if($user->GetRowid() != $group->GetRowid()) {
		return AUTHZ_DISCONNECT;
	}

	return $self->SUPER::TestAuthorize($context);
}


# ============================================================================

=head2 DisplayMain ()

	Main mioga applications function.

=cut

# ============================================================================

sub DisplayMain {
	my ($self, $context) = @_;
	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();
	my $dbh = $config->GetDBH();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['reload_menu'], 'allow_empty'],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Workspace::DisplayMain", __("Wrong argument value."))
	}

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'workspace.xsl', locale_domain => 'workspace_xsl');
	my $error = 'ok';

	# test if we need to initialize menu from database or not
	# -------------------------------------------------------
	#if (exists($context->{args}->{reload_menu}) && $context->{args}->{reload_menu} eq "ok")
	#{
	#	warn ("DisplayMain : reset => force menu reload") if ($debug > 0);
	#	
	#	delete $session->{persistent}->{Workspace}->{default_app};
	#	delete $session->{persistent}->{Workspace}->{default_link};		
		
	$self->LoadMenuFromDatabase($context);
	#}
	#elsif (!exists($session->{persistent}->{Workspace}->{menu}))
	#{
	#	warn ("DisplayMain : no menu description in session => force menu reload") if ($debug > 0);
	#	$self->LoadMenuFromDatabase($context);
	#}


	$session->{persistent}->{Workspace}->{description} = $session->{persistent}->{Workspace}->{menu};

	warn (Dumper($session->{persistent}->{Workspace})) if ($debug > 0);

	my $xml = '<?xml version="1.0"?>';
	$xml .= "<DisplayMain";
	if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
	}
	$xml .= ">";

	$xml .= $context->GetXML();
	my $menu_uri =  new Mioga2::URI ($config, group  => $user_ident,
					     public => 0,
					     application => "Workspace",
					     method => "DisplayMenu",
					     args => $context->{args});
	$xml .= "<menu_uri>".st_FormatXMLString($menu_uri->GetURI())."</menu_uri>";

	my $uri;

	my $default_url = WorkspaceGetDefaultURL($dbh, $user_id);

	if (exists($context->{args}->{main_uri}))
	{
		my $main_uri =  new Mioga2::URI ($config, group  => $user_ident,
					     public => 0,
					     application => "Workspace",
				             method => $context->{args}->{main_uri});
		$uri = $main_uri->GetURI();
	}
	elsif (defined($default_url))
	{
		$uri = $default_url->{default_url};
	}
	else
	{
		my $main_uri =  new Mioga2::URI ($config, group  => $user_ident,
					     public => 0,
					     application => "Workspace",
				             method => "DisplayHello");
		$uri = $main_uri->GetURI();
	}

	$xml .= "<main_uri>".st_FormatXMLString($uri)."</main_uri>";

	if(WorkspaceGetResize($dbh, $user_id))
	{
		$xml .= "<resize />";
	}

	$xml .= "</DisplayMain>";

	$content->SetContent($xml);
	return $content;
}



# ============================================================================

=head2 DisplayHome ()

	Redirect to home_url

=cut

# ============================================================================

sub DisplayHome {
	my ($self, $context) = @_;

	my $user = $context->GetUser();

	my $config = $context->GetConfig();
	my $uri = $user->GetHomeURL();
	
	if(!defined $uri or $uri eq "") {
		$uri = $config->GetBinURI() . "/" . $user->GetIdent() . "/Workspace/DisplayMain";
	}

	my $content = new Mioga2::Content::REDIRECT($context, mode => "external");
	$content->SetContent($uri);

	return $content;
}

# ============================================================================

=head2 DisplayAppTitle ($context)

	Display application title frame
	user

=cut

# ============================================================================

sub DisplayAppTitle {
	my ($self, $context) = @_;
	
	my $config = $context->GetConfig();

	my $xml = '<?xml version="1.0"?>';
	$xml .= "<DisplayAppTitle>";
	
	$xml .= $context->GetXML();
	
	if((exists $context->{args}->{app_ident}) && (exists $context->{args}->{id}) && (exists $context->{args}->{ident}) && exists ($context->{args}->{type})) {

		$xml .= "<group_base>";
		$xml .= "<ident>".st_FormatXMLString($context->{args}->{ident})."</ident>";
		$xml .= "<id>".st_FormatXMLString($context->{args}->{id})."</id>";
		$xml .= "<type>".st_FormatXMLString($context->{args}->{type})."</type>";
		$xml .= "</group_base>";

		if($context->{args}->{app_ident} eq 'public_files' or 
		   $context->{args}->{app_ident} eq 'private_files') {
			$xml .= "<current>";
			$xml .= "<uri>".$context->{args}->{app_ident}."</uri>";
			$xml .= "</current>";
		}
		else {
			my $user = $context->GetUser();
			
			my $app_desc = new Mioga2::AppDesc($config, ident => $context->{args}->{app_ident});
			
			$xml .= "<current>";
			$xml .= "<application>";
			$xml .= "<ident>".st_FormatXMLString($app_desc->GetIdent())."</ident>";
			$xml .= "<name>".st_FormatXMLString($app_desc->GetLocaleName($user))."</name>";
			$xml .= "<description>".st_FormatXMLString($app_desc->GetLocaleDescription($user))."</description>";
			
			if(exists $context->{args}->{help}) {
				$xml .= "<help>".st_FormatXMLString($context->{args}->{help})."</help>";
			}
			
			$xml .= "</application>";
			$xml .= "</current>";
		}
	}
		
	$xml .= "</DisplayAppTitle>";

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'workspace.xsl', locale_domain => 'workspace_xsl');
	$content->SetContent($xml);

	return $content;
	
}


# ============================================================================

=head2 DisplayMenu ($context)

	Display menu according to what defined in the database for the current 
	user

=cut

# ============================================================================

sub DisplayMenu {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['reload_menu', 'expand_all', 'branch_path_0', 'branch_path_1'], 'allow_empty'],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Workspace::DisplayMenu", __("Wrong argument value."))
	}

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'workspace.xsl', locale_domain => 'workspace_xsl');
	my $error = 'ok';

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $user_fn = $user->GetFirstname();
	my $user_ln = $user->GetLastname();
	my $user_ident = $user->GetIdent();
	my $group = $context->GetGroup();
	my $group_ident = $group->GetIdent();
	my $user_type = $user->GetType();

	# getting menu from databas everytime
	# -----------------------------------
	$self->LoadMenuFromDatabase($context);
	$session->{persistent}->{Workspace}->{description} = $session->{persistent}->{Workspace}->{menu};

	my $xml = '<?xml version="1.0"?>';

	$xml .= "<DisplayMenu>";
	$xml .= $context->GetXML();

	# determine URL to display in mioga_main frame
	# --------------------------------------------
	if(exists($context->{args}->{default_app}) && exists($context->{args}->{default_method}) &&
	   exists($context->{args}->{group_ident}))
	{

		delete $session->{Workspace}->{default_link};
		$session->{Workspace}->{default_app} = $context->{args}->{default_app};
		$session->{Workspace}->{default_method} = $context->{args}->{default_method};
		$session->{Workspace}->{group_ident} = $context->{args}->{group_ident};		
	}
	

	elsif(exists $context->{args}->{default_link}) {
		delete $session->{Workspace}->{default_app};
		$session->{Workspace}->{default_link} = $context->{args}->{default_link};
	}

	elsif(! exists $session->{Workspace}->{default_link}) {
		my $defuri = new Mioga2::URI ($config, group       => $group_ident,
                                               public      => 0,
                                               application => 'Workspace',
									           method      => 'DisplayHello');
		$session->{Workspace}->{default_link} = $defuri->GetURI();
	}

	# collapse/expand 
	# ---------------
	if (exists($context->{args}->{branch_path_0}))
	{
		my $branch_path = $context->{args}->{branch_path_0};
		if ($branch_path =~ /^(.*)\#/)
		{
			$branch_path = $1;
		}
		$self->ChangeBranchStatus($context, $branch_path);
	}
	elsif (exists($context->{args}->{branch_path_1}))
	{
		my $branch_path = $context->{args}->{branch_path_1};
		if ($branch_path =~ /^(.*)\#/)
		{
			$branch_path = $1;
		}
		$self->ChangeBranchStatus($context, $branch_path);
	}
	elsif (exists ($context->{args}->{expand_all})) {
		$self->ExpandAll ($context);
	}

	$xml .= $self->GetXMLMenuFromSession($context);

	$xml .= "<User>";
	$xml .= "<nopref />" if ($user_type eq 'external_user');
	$xml .= "<firstname>".st_FormatXMLString($user_fn)."</firstname>";
	$xml .= "<lastname>".st_FormatXMLString($user_ln)."</lastname>";

	my $uri =  new Mioga2::URI ($config, group  => $user_ident,
					     public => 0,
					     application => "Workspace",
					     method => "ModifyUser");
	$xml .= "<modify>".st_FormatXMLString($uri->GetURI)."</modify>";

	$xml .= "</User>";
	$xml .= "</DisplayMenu>";
	$content->SetContent($xml);

	return $content;
}

# ============================================================================

=head2 MenuComponents ($context)

	Useful to select which components will appeared in the menu

=cut

# ============================================================================

sub MenuComponents {
	my ($self, $context) = @_;

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'workspace.xsl', locale_domain => 'workspace_xsl');
	my $error = 'ok';

	my $config = $context->GetConfig();
	my $dbh = $config->GetDBH();
	my $user = $context->GetUser();
	my $user_fn = $user->GetFirstname();
	my $user_ln = $user->GetLastname();
	my $user_ident = $user->GetIdent();
	my $user_id= $user->GetRowid();
	my $session = $context->GetSession();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['components_act_modify'], 'allow_empty'],
				[ ['down', 'up', 'search', 'my_groups', 'resize', 'my_applications', 'my_files', 'my_resources', 'my_links'], 'allow_empty', 'want_int'],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Workspace::MenuComponents", __("Wrong argument value."))
	}
	
	if (!exists($session->{Workspace}->{MenuComponents}))
	{
		$session->{Workspace}->{MenuComponents} = ();
		my $entries = WorkspaceGetEntries($dbh, $user_id);
		#if (defined ($entries))
		#{
		#	foreach my $entry (@$entries)
		#	{
		#		push (@{$session->{Workspace}->{MenuComponents}}, $entry);
		#	}
		#}
		$session->{Workspace}->{MenuComponents} = $entries;
	}
	
	if(exists($context->{args}->{up}))
	{
		my $buffer = $session->{Workspace}->{MenuComponents}->[$context->{args}->{up}];
		$session->{Workspace}->{MenuComponents}->[$context->{args}->{up}] = $session->{Workspace}->{MenuComponents}->[$context->{args}->{up} - 1];
		$session->{Workspace}->{MenuComponents}->[$context->{args}->{up} - 1] = $buffer;
	}
	elsif(exists($context->{args}->{down}))
	{
		my $buffer = $session->{Workspace}->{MenuComponents}->[$context->{args}->{down}];
		$session->{Workspace}->{MenuComponents}->[$context->{args}->{down}] = $session->{Workspace}->{MenuComponents}->[$context->{args}->{down} + 1];
		$session->{Workspace}->{MenuComponents}->[$context->{args}->{down} + 1] = $buffer;
	}
	elsif (st_ArgExists($context, 'components_act_modify'))
	{
		if (exists ($session->{Workspace}->{MenuComponents}) && ref ($session->{Workspace}->{MenuComponents}) eq 'ARRAY')
		{
			my $comp = 0;

			my %entry_hash;
			foreach my $entry (@{$session->{Workspace}->{MenuComponents}})
			{
				WorkspaceUpdateEntryOrder ($dbh, $user_id, $entry->{ident}, $comp);

				if (st_ArgExists($context, $entry->{ident}))
				{
					$session->{Workspace}->{MenuComponents}->[$comp]->{display_ok} = 1;
					$entry_hash{$entry->{ident}} = 1;
				}
				else
				{
					$session->{Workspace}->{MenuComponents}->[$comp]->{display_ok} = 0;
					$entry_hash{$entry->{ident}} = 0;
				}

				$comp++;
			}

			WorkspaceUpdateEntriesVisibility($dbh, $user_id, \%entry_hash);

		}

		my %resize = (user_id => $user_id);

		if (st_ArgExists($context, 'resize'))
		{
			$resize{resize} = 1;
		}
		else
		{
			$resize{resize} = 0;
		}

		WorkspaceResizeModify($config, \%resize);


		$session->{__internal}->{message}{type} = 'info';
		$session->{__internal}->{message}{text} = __('Menu successfully modified.');
		my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
		$content->SetContent('MenuComponents');
		return $content;
	}

	my $xml = '<?xml version="1.0"?>';

	$xml .= "<MenuComponents";
	if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
	}
	$xml .= ">";
	$xml .= $context->GetXML();

	if (exists ($session->{Workspace}->{MenuComponents}) && ref ($session->{Workspace}->{MenuComponents}) eq 'ARRAY')
	{
		foreach my $entry (@{$session->{Workspace}->{MenuComponents}})
		{
			$xml .= '<entry ident="'.st_FormatXMLString($entry->{ident}).'" display_ok="'.st_FormatXMLString($entry->{display_ok}).'" rowid="'.st_FormatXMLString($entry->{rowid}).'" />';
		}
	}
		
	if(WorkspaceGetResize($dbh, $user_id))
	{
		$xml .= "<resize />";
	}

	$xml .= "</MenuComponents>";
	$content->SetContent($xml);

	return $content;
}

# ============================================================================

=head2 DisplayDefaultURL ($context)

	Display page for managing default url for the workspace

=cut

# ============================================================================

sub DisplayDefaultURL {
	my ($self, $context) = @_;

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'workspace.xsl', locale_domain => 'workspace_xsl');
	my $error = 'ok';

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['default_url_act_modify'], 'allow_empty'],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Workspace::DisplayDefaultURL", __("Wrong argument value."))
	}
	
	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	
	# ------------------------------------------------------
	# Check submited values 
	# ------------------------------------------------------
	my $user = $context->GetUser();

	if(st_ArgExists($context, 'default_url_act_modify'))
	{
		my $params;
		
		$params = [
				[ [ 'default_url'], 'allow_empty'],
				[ [ 'home_url'], 'allow_empty'],
		];

		($values, $errors) = ac_CheckArgs($context, $params );

		if(! @$errors) {
			try {
				$values->{user_id} = $user->GetRowid();
				WorkspaceURLModify($config, $values);
				delete $session->{__internal}->{user_home_url};
			}

			catch Mioga2::Exception::Simple with {
				my $err = shift;
				push @$errors, [$err->as_string, ['__modify__'] ];
			};

			if(! @$errors) {
				$session->{__internal}->{message}{type} = 'info';
				$session->{__internal}->{message}{text} = __('Homepage successfully modified.');
				my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
				$content->SetContent('DisplayDefaultURL');
				return $content;
			}
		}
	}

	$self->FillURLValues($config, $user->GetRowid(), $values);

	my $xml = '<?xml version="1.0"?>';
	$xml .= "<DisplayDefaultURL";
	if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
	}
	$xml .= ">";
	$xml .= $context->GetXML();
	$xml .= "<fields>";

	$xml .= ac_XMLifyArgs( ['default_url', 'home_url'], $values );

	$xml .= "</fields>";

	$xml .= ac_XMLifyErrors($errors);

	$xml .= "</DisplayDefaultURL>";

	$content->SetContent($xml);
	return $content;
}

# ============================================================================

=head2 DisplayHello ($context)

	Display white page

=cut

# ============================================================================

sub DisplayHello {
	my ($self, $context) = @_;

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'workspace.xsl', locale_domain => 'workspace_xsl');
	my $error = 'ok';

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	
	my $xml = '<?xml version="1.0"?>';
	$xml .= "<DisplayHello>";
	$xml .= $context->GetXML();
	$xml .= "</DisplayHello>";
	$content->SetContent($xml);
	return $content;
}

# ============================================================================

=head2 DisplayLinks ()

	Links management tool

=cut

# ============================================================================

sub DisplayLinks {
	my ($self, $context) = @_;

	my $session = $context->GetSession();
	my $config = $context->GetConfig();
	my $user = $context->GetUser();
	my $group = $context->GetGroup();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();
	my $group_ident = $group->GetIdent();
	my $dbh = $config->GetDBH();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['down', 'up'], 'allow_empty', 'want_int'],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Workspace::DisplayLinks", __("Wrong argument value."))
	}
	
	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'workspace.xsl', locale_domain => 'workspace_xsl');
	my $error = 'ok';

	#my $sll = new Mioga2::SimpleLargeList($context, \%link_sll_desc);
	#my $args = [$user_id];

	my $xml = '<?xml version="1.0"?>';
	$xml .= "<DisplayLinks";
	if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
	}
	$xml .= ">";
	$xml .= $context->GetXML();
	#$xml .= $sll->Run($context, $args);
	
	my $links = WorkspaceGetLinks($dbh, $user_id);
	if (defined($links))
	{
		if(exists($context->{args}->{up}))
		{
			$links->[$context->{args}->{up}]->{number} -= 1;
			$links->[$context->{args}->{up} - 1]->{number} += 1;
			WorkspaceLinkModify($dbh, $links->[$context->{args}->{up}]);
			WorkspaceLinkModify($dbh, $links->[$context->{args}->{up} - 1]);
		}
		elsif(exists($context->{args}->{down}))
		{
			$links->[$context->{args}->{down}]->{number} += 1;
			$links->[$context->{args}->{down} + 1]->{number} -= 1;
			WorkspaceLinkModify($dbh, $links->[$context->{args}->{down}]);
			WorkspaceLinkModify($dbh, $links->[$context->{args}->{down} + 1]);
		}

	}

	undef $links;
	$links = WorkspaceGetLinks($dbh, $user_id);
	if (defined($links))
	{
		foreach my $link (@$links)
		{
			$xml .= "<Row>";
			foreach my $entry (qw(rowid ident link number))
			{
				$xml .= "<$entry>".st_FormatXMLString($link->{$entry})."</$entry>\n";
			}
			$xml .= "</Row>";
		}
	}

	$xml .= "</DisplayLinks>";
	$content->SetContent($xml);

	return $content;
}

# ============================================================================

=head2 DeleteLink ()

	link deletion management

=cut

# ============================================================================

sub DeleteLink {
	my ($self, $context) = @_;

	my $session = $context->GetSession();
	my $config = $context->GetConfig();
	my $user = $context->GetUser();
	my $group = $context->GetGroup();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();
	my $group_ident = $group->GetIdent();
	my $dbh = $config->GetDBH();
	my $referer = $context->GetReferer();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['rowid'], 'disallow_empty', 'want_int'],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Workspace::DeleteLink", __("Wrong argument value."))
	}
	
	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'workspace.xsl', locale_domain => 'workspace_xsl');
	my $error = 'ok';


	my $xml = '<?xml version="1.0"?>';
	$xml .= "<DeleteLink>";
	$xml .= $context->GetXML();
	$xml .= "<referer>".st_FormatXMLString($referer)."</referer>";
	
	my $link;

	if (exists($context->{args}->{rowid}))
	{
		$session->{DeleteLink}->{rowid} = $context->{args}->{rowid};
	}

	$link = WorkspaceGetLink($dbh, $session->{DeleteLink}->{rowid});

	if (st_ArgExists($context, 'act_delete_link'))
	{
		WorkspaceLinkDelete($dbh, $session->{DeleteLink}->{rowid});
		my $main_uri =  new Mioga2::URI ($config, group  => $user_ident,
					     public => 0,
					     application => "Workspace",
				             method => 'DisplayLinks');
		my $uri = $main_uri->GetURI();

		$session->{__internal}->{message}{type} = 'info';
		$session->{__internal}->{message}{text} = __('Link successfully deleted.');
		my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
		$content->SetContent($uri);
		return $content;
	}
	if (defined($link))
	{
		$xml .= "<Row>";
		foreach my $entry (qw(rowid ident link number))
		{
			$xml .= "<$entry>".st_FormatXMLString($link->{$entry})."</$entry>\n";
		}
		$xml .= "</Row>";
	}

	$xml .= "</DeleteLink>";
	$content->SetContent($xml);

	return $content;
}

# ============================================================================

=head2 ModifyLink ($context)

	Display mody link form

=cut

# ============================================================================

sub ModifyLink {
	my ($self, $context) = @_;
	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $group = $context->GetGroup();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();
	my $group_ident = $group->GetIdent();
	my $dbh = $config->GetDBH();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['link_act_modify'], 'allow_empty'],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Workspace::ModifyLink", __("Wrong argument value."))
	}
	

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'workspace.xsl', locale_domain => 'workspace_xsl');
	my $error = 'ok';

	# getting URI args
	# ----------------
	if(st_ArgExists($context, 'rowid'))
	{
		delete $session->{Workspace}->{ModifyLinks};
		$session->{Workspace}->{ModifyLink}->{referer} = $context->GetReferer();
		$session->{Workspace}->{ModifyLink}->{referer} =~ s/(.*)\?.*/$1/;
		my $params;
		
		$params = [
				[ [ 'rowid'], 'disallow_empty'],
		];

		($values, $errors) = ac_CheckArgs($context, $params );

		if(! @$errors)
		{
			$session->{Workspace}->{ModifyLink}->{rowid} = $values->{rowid};
			my $link = WorkspaceGetLink($dbh, $session->{Workspace}->{ModifyLink}->{rowid});

			if (defined($link))
			{
				$values->{link} =~ s/\s*$//;
				$session->{Workspace}->{ModifyLink}->{ident} = $link->{ident};
				$session->{Workspace}->{ModifyLink}->{link} = $link->{link};
			}
		}
	}
	elsif(st_ArgExists($context, 'ident') || st_ArgExists($context, 'link'))
	{
		my $params;
		
		$params = [
				[ [ 'ident', 'link'], 'disallow_empty'],
		];

		($values, $errors) = ac_CheckArgs($context, $params );

		$values->{link} =~ s/\s*$//;
		$session->{Workspace}->{ModifyLink}->{ident} = $values->{ident};
		$session->{Workspace}->{ModifyLink}->{link} = $values->{link};

		if(! @$errors)
		{
			WorkspaceLinkModify($dbh, $session->{Workspace}->{ModifyLink});

			#my $uri =  new Mioga2::URI ($config, group  => $group_ident,
			#							public => 0,
			#							application => 'Workspace',
			#							method => "DisplayLinks"
			#				     );
			#my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
			#$content->SetContent($uri->GetURI());
			#return $content;
			$session->{__internal}->{message}{type} = 'info';
			$session->{__internal}->{message}{text} = __('Link successfully modified.');
			my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
			$content->SetContent('DisplayLinks');
			return $content;
		}
		else
		{
			$error = 'all_fields';
		}
	}

	my $xml = '<?xml version="1.0"?>';
	$xml .= "<ModifyLink>";
	$xml .= $context->GetXML();

	$xml .= "<referer>".st_FormatXMLString($session->{Workspace}->{ModifyLink}->{referer})."</referer>";
	$xml .= "<error>".st_FormatXMLString($error)."</error>";

	$xml .= "<fields>";
	foreach my $key ('link', 'ident')
	{
		if (exists($session->{Workspace}->{ModifyLink}->{"$key"}))
		{
			$xml .= "<$key>".st_FormatXMLString($session->{Workspace}->{ModifyLink}->{"$key"})."</$key>";
		}
	}
	$xml .= "</fields>";

	$xml .= "</ModifyLink>";
	
	$content->SetContent($xml);

	return $content;
}

# ============================================================================

=head2 AddLink ($context)

	Display add link form

=cut

# ============================================================================

sub AddLink {
	my ($self, $context) = @_;
	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $group = $context->GetGroup();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();
	my $group_ident = $group->GetIdent();
	my $dbh = $config->GetDBH();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['link_act_add'], 'allow_empty'],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Workspace::AddLink", __("Wrong argument value."))
	}

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'workspace.xsl', locale_domain => 'workspace_xsl');
	my $error = 'ok';

	# getting URI args
	# ----------------
	if(st_ArgExists($context, 'ident') || st_ArgExists($context, 'link'))
	{
		my $params;
		
		$params = [
				[ [ 'ident', 'link'], 'disallow_empty'],
		];

		($values, $errors) = ac_CheckArgs($context, $params );

		$values->{link} =~ s/\s*$//;
		$session->{Workspace}->{AddLink}->{ident} = $values->{ident};
		$session->{Workspace}->{AddLink}->{link} = $values->{link};

		if(! @$errors)
		{
			WorkspaceLinkAdd($dbh, $session->{Workspace}->{AddLink});

			#my $uri =  new Mioga2::URI ($config, group  => $group_ident,
			#							public => 0,
			#							application => 'Workspace',
			#							method => "DisplayLinks"
			#							);
			#my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
			#$content->SetContent($uri->GetURI());
			#return $content;
			$session->{__internal}->{message}{type} = 'info';
			$session->{__internal}->{message}{text} = __('Link successfully created.');
			my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
			$content->SetContent('DisplayLinks');
			return $content;
		}
		else
		{
			$error = 'all_fields';
		}
	}
	else
	{
		delete $session->{Workspace}->{AddLink};
		$session->{Workspace}->{AddLink}->{user_id} = $user_id;
		$session->{Workspace}->{AddLink}->{link} = 'http://';
		$session->{Workspace}->{AddLink}->{referer} = $context->GetReferer();
		$session->{Workspace}->{AddLink}->{referer} =~ s/(.*)\?.*/$1/;
	}

	my $xml = '<?xml version="1.0"?>';
	$xml .= "<AddLink>";
	$xml .= $context->GetXML();

	$xml .= "<referer>".st_FormatXMLString($session->{Workspace}->{AddLink}->{referer})."</referer>";
	$xml .= "<error>".st_FormatXMLString($error)."</error>";

	$xml .= "<fields>";
	foreach my $key ('link', 'ident')
	{
		if (exists($session->{Workspace}->{AddLink}->{"$key"}))
		{
			$xml .= "<$key>".st_FormatXMLString($session->{Workspace}->{AddLink}->{"$key"})."</$key>";
		}
	}
	$xml .= "</fields>";

	$xml .= "</AddLink>";
	
	$content->SetContent($xml);

	return $content;
}

# ============================================================================

=head2 AddAppToLinks ($context)

	Display shortcut for adding application to links list

=cut

# ============================================================================

sub AddAppToLinks {
	my ($self, $context) = @_;
	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $group = $context->GetGroup();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();
	my $group_ident = $group->GetIdent();
	my $dbh = $config->GetDBH();


	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'workspace.xsl', locale_domain => 'workspace_xsl');
	my $error = '';

	# getting URI args
	# ----------------
	my $values = {};
	my $errors = [];

	if(st_ArgExists($context, 'app_name') && st_ArgExists($context, 'app_link'))
	{
		my $params;
		
		$params = [
				[ [ 'app_name', 'app_link'], 'disallow_empty'],
		];

		($values, $errors) = ac_CheckArgs($context, $params );

		$session->{Workspace}->{AddAppToLinks}->{user_id} = $user_id;
		$session->{Workspace}->{AddAppToLinks}->{referer} = $context->GetReferer();
		$session->{Workspace}->{AddAppToLinks}->{referer} =~ s/(.*)\?.*/$1/;
		$session->{Workspace}->{AddAppToLinks}->{ident} = $values->{app_name};
		$session->{Workspace}->{AddAppToLinks}->{link} = $values->{app_link};

		#if(@$errors)
		#{
		#	push @$errors, ['', ['bad_params'] ];
		#}
	}
	elsif (st_ArgExists($context, 'act_add_app_link'))
	{
		my $params;
		
		$params = [
				[ [ 'ident'], 'disallow_empty'],
		];

		($values, $errors) = ac_CheckArgs($context, $params );

		$session->{Workspace}->{AddAppToLinks}->{ident} = $values->{ident};

		if(! @$errors)
		{
			WorkspaceLinkAdd($dbh, $session->{Workspace}->{AddAppToLinks});

			return st_SuccessMessage($context, 'workspace.xsl', 'user', 'add', $session->{Workspace}->{AddAppToLinks}->{link}, 'workspace_xsl');
		}
	}

	my $xml = '<?xml version="1.0"?>';
	$xml .= "<AddAppToLinks>";
	$xml .= $context->GetXML();

	$xml .= "<referer>".st_FormatXMLString($session->{Workspace}->{AddAppToLinks}->{referer})."</referer>";
	$xml .= ac_XMLifyErrors($errors);

	$xml .= "<fields>";
	foreach my $key ('link', 'ident')
	{
		if (exists($session->{Workspace}->{AddAppToLinks}->{"$key"}))
		{
			$xml .= "<$key>".st_FormatXMLString($session->{Workspace}->{AddAppToLinks}->{"$key"})."</$key>";
		}
	}
	$xml .= "</fields>";

	$xml .= "</AddAppToLinks>";
	
	$content->SetContent($xml);

	return $content;
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head2 LoadMenuFromDatabase ($context)

	Load menu from database into session

=cut

# ============================================================================

sub LoadMenuFromDatabase {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $dbh = $config->GetDBH();
	my $user_id = $user->GetRowid();

	# delete resident menu in session
	# --------------------------------
	#if (exists($session->{persistent}->{Workspace}->{menu}))
	#{
	#	delete $session->{persistent}->{Workspace}->{menu};
	#	delete $session->{persistent}->{Workspace}->{description};
	#}

	#$session->{persistent}->{Workspace}->{menu} = ();

	# adding entries to Display into session
	# --------------------------------------
	if (exists($session->{persistent}->{Workspace}->{menu}) && ref ($session->{persistent}->{Workspace}->{menu}) eq "ARRAY")
	{
		my @tmp_entries;
		my $entries = WorkspaceGetVisibleEntries($dbh, $user_id);

		my %menu_entries;
		foreach my $row (@{$session->{persistent}->{Workspace}->{menu}})
		{
			$menu_entries{$row->{ident}} = $row;
		}
		delete $session->{Workspace}->{old_menu};
		$session->{Workspace}->{old_menu} = \%menu_entries;
		#warn(Dumper($session->{Workspace}->{old_menu}));

		if (defined($entries))
		{
			foreach my $entry (@$entries)
			{
				if ($self->DisplayEntry($context, $entry->{ident}))
				{
					my $entry_desc = $self->InitEntry($context, "$entry->{ident}");
					push (@tmp_entries, $entry_desc);
				}
			}
		}

		@{$session->{persistent}->{Workspace}->{menu}} = @tmp_entries;
	}
	else
	{
		my $entries = WorkspaceGetVisibleEntries($dbh, $user_id);

		if (defined($entries))
		{
			foreach my $entry (@$entries)
			{
				if ($self->DisplayEntry($context, $entry->{ident}))
				{
					my $entry_desc = $self->InitEntry($context, "$entry->{ident}");
					push (@{$session->{persistent}->{Workspace}->{menu}}, $entry_desc);
				}
			}
		}
	}

}

# ============================================================================

=head2 InitEntry ($context, $entry_ident)

	Initialize menu entry according to the entry ident

=cut

# ============================================================================

sub InitEntry {
	my ($self, $context, $entry_ident) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $dbh = $config->GetDBH();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();
	my $user_type = $user->GetType();

	my %entry = (ident => $entry_ident,
	             status => 'collapse'); # useful to tell if menu is collapsed or not
	if ($entry_ident eq "search")
	{
		$entry{description} = undef;
		$entry{type} = 'end_branch';
	}
	elsif ($entry_ident eq "my_applications" && $user_type ne 'external_user')
	{
		$entry{description} = $self->GetUserApplications($context);
		$entry{type} = 'branch';
		if (exists($session->{Workspace}->{old_menu}->{my_applications}->{status}))
		{
			$entry{status} = $session->{Workspace}->{old_menu}->{my_applications}->{status};
		}
	}
	elsif ($entry_ident eq "my_files" && $user_type ne 'external_user')
	{
		$entry{description} = $self->GetUserFileAccess($context, $user_ident);
		$entry{type} = 'branch';
		if (exists($session->{Workspace}->{old_menu}->{my_files}->{status}))
		{
			$entry{status} = $session->{Workspace}->{old_menu}->{my_files}->{status};
		}
	}
	elsif ($entry_ident eq "my_groups")
	{
		$entry{description} = $self->GetInvitedGroupsApplications($context);
		$entry{type} = 'branch';
		if (exists($session->{Workspace}->{old_menu}->{my_groups}->{status}))
		{
			$entry{status} = $session->{Workspace}->{old_menu}->{my_groups}->{status};
		}
	}
	elsif ($entry_ident eq "my_resources")
	{
		$entry{description} = $self->GetInvitedResourcesApplications($context);
		$entry{type} = 'branch';
		if (exists($session->{Workspace}->{old_menu}->{my_resources}->{status}))
		{
			$entry{status} = $session->{Workspace}->{old_menu}->{my_resources}->{status};
		}
	}
	elsif ($entry_ident eq "my_links" && $user_type ne 'external_user')
	{
		$entry{description} = $self->GetUserLinks($context);
		$entry{type} = 'branch';
		if (exists($session->{Workspace}->{old_menu}->{my_links}->{status}))
		{
			$entry{status} = $session->{Workspace}->{old_menu}->{my_links}->{status};
		}
	}

	return \%entry;

}

# ============================================================================

=head2 GetUserApplications ($context)

	return hash list of user's normal applications with their calling URL

=cut

# ==========================================================================='

sub GetUserApplications {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $dbh = $config->GetDBH();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();

	my %applications;

	# getting application list
	# ------------------------

	my $applications_idents = ();
	my $applications_names = ();
	
	foreach my $type (qw(normal portal)) {
		my $application_list = new Mioga2::AppDescList ($config, group_id => $user_id, user_id => $user_id,
								 	 app_type => $type, 
									 having_method => 'DisplayMain');

		my $idents = $application_list->GetIdents();
		my $names = $application_list->GetLocaleNames($user);
		
		push @$applications_idents, @$idents;
		push @$applications_names, @$names;
	}
	
	my $nb_app = @$applications_idents;

	for(my $i = 0; $i<$nb_app; $i++) 
	{
		my $app = $applications_idents->[$i];
		my $name = $applications_names->[$i];

		my $uri =  new Mioga2::URI ($config,
									group  => $user_ident,
									public => 0,
									application => $app,
									method => "DisplayMain");

		my %app_desc = (ident => $name,
						type => 'leaf',
						group_ident => $user_ident,
						app_ident   => $app,
						leaf_uri => $uri->GetURI());
		$applications{$app} = \%app_desc;
	}

	return \%applications;
}

# ============================================================================

=head2 GetUserFileAccess ($context, $user_ident)

	return hash list of user's Private files and public files if needed

=cut

# ==========================================================================='

sub GetUserFileAccess {
	my ($self, $context, $user_ident) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $dbh = $config->GetDBH();

	my $private_uri = $config->GetPrivateURI()."/$user_ident";
	my $public_uri = $config->GetPublicURI()."/$user_ident";
	my %files;

	# TODO
	# Testing filemanager availability
	# --------------------------------
	my $uri =  new Mioga2::URI ($config, uri  => $private_uri);
	my $priv_uri = $uri->GetURI();
	my %private = (ident => 'private_files',
				   app_ident => 'private_files',
	               type => 'leaf',
				   leaf_uri => $priv_uri);
	
	# testing if user/group has public part
	# -------------------------------------
	my $group;
	try {
		$group = new Mioga2::Old::Group ($config, ident => $user_ident);
	}
	catch Mioga2::Exception::Group with {
		$group = new Mioga2::Old::User ($config, ident => $user_ident);
	};

	if ($group->HasPublicPart())
	{
		# TODO construct the correct url for public part
		# ----------------------------------------------
		my $uri =  new Mioga2::URI ($config, uri  => $public_uri);
		my $pub_uri = $uri->GetURI();
		my %public = (ident => 'public_files',
					  app_ident => 'public_files',
					  type => 'leaf',
					  leaf_uri => $pub_uri);

		if (AuthzTestRightAccessForURI($config, $uri, $user, $group))
		{
			$files{public} = \%public;
		}
	}

	if (AuthzTestRightAccessForURI($config, $uri, $user, $group))
	{
		$files{private} = \%private;
	}

	return \%files;
}

# ============================================================================

=head2 GetUserLinks ($context)

	return hash list of user's normal applications with their calling URL

=cut

# ============================================================================'

sub GetUserLinks {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $dbh = $config->GetDBH();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();

	my %branch_list;

	my $branches = WorkspaceGetMenuBranchesForUser($dbh, $user_id);

	# Not for the moment
	#
	#if (defined($branches))
	#{
	#	foreach my $branch (@{$branches})
	#	{
	#		my %branch_desc = (ident => $branch->{ident},
	#		                   status => 'collapse',
	#		                   type => 'branch');
	#		$branch_desc{description} = $self->GetSubBranchLink($context, "$branch->{rowid}");
	#		push(@{$branch_desc{description}}, @{$self->GetSubLeafs($context, $branch->{rowid})});
	#		push (@branch_list, \%branch_desc);
	#	}
#
#	}


	my $main_links = WorkspaceGetMainMenuLeafs($dbh, $user_id);

	if (defined($main_links))
	{
		foreach my $leaf (@{$main_links})
		{
			my %leaf_desc = (ident => $leaf->{ident},
			                 leaf_uri => $leaf->{"link"},
					 app_ident => 'Link',
			                 type => 'leaf');
			$branch_list{$leaf->{number}} = \%leaf_desc;
		}
	}

	return \%branch_list;
}

# ============================================================================

=head2 GetSubBranchLink ($context, $ancestor_id)


=cut

# ============================================================================

sub GetSubBranchLink {
	my ($self, $context, $ancestor_id) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $dbh = $config->GetDBH();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();

	my @branch_list;

	my $branches = WorkspaceGetSubmenuBranches($dbh, $ancestor_id);
	my $leafs = WorkspaceGetMenuLeafs($dbh, $ancestor_id);

	if (defined($branches))
	{
		foreach my $branch (@$branches)
		{
			my %branch_desc = (ident => $branch->{ident},
			                   status => 'collapse',
			                   type => 'branch');
			my @sub_branches = $self->GetSubBranchLink($context, $branch->{rowid});
			$branch_desc{description} = $self->GetSubBranchLink($context, $branch->{rowid});

			push(@{$branch_desc{description}}, @{$self->GetSubLeafs($context, $branch->{rowid})});
			push (@branch_list, \%branch_desc);
		}

	}

	my @description = (@branch_list);

	return \@description;
}

# ============================================================================

=head2 GetSubLeafs ($context, $ancestor_id)

	return hash list of user's normal applications with their calling URL

=cut

# ==========================================================================='

sub GetSubLeafs {
	my ($self, $context, $ancestor_id) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $dbh = $config->GetDBH();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();

	my @leaf_list;

	my $leafs = WorkspaceGetMenuLeafs($dbh, $ancestor_id);

	if (defined($leafs))
	{
		foreach my $leaf (@$leafs)
		{
			my %leaf_desc = (ident => $leaf->{ident},
			                 leaf_uri => $leaf->{"link"},
			                 type => 'leaf');
			push (@leaf_list, \%leaf_desc);
		}

	}

	return \@leaf_list;
}

# ============================================================================

=head2 GetInvitedGroupsApplications ($context)

	build list of groups where current user is invited whith the allowed applications

=cut

# ============================================================================

sub GetInvitedGroupsApplications {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $dbh = $config->GetDBH();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();
	my $group_list = $user->GetExpandedGroups();

	my %invited_groups;
	my %applications;

	# getting group list
	# -------------------
	my $group_idents = $group_list->GetIdents();
	my $group_ids = $group_list->GetRowids();

	if (defined($group_idents) and ref($group_idents) eq 'ARRAY')
	{
		my $comp = 0 ;
		foreach my $group_ident (@$group_idents)
		{
			my %entry = (ident => $group_ident,
			             status => 'collapse',
						 type => 'branch');
			
			if(exists($session->{Workspace}->{old_menu}->{my_groups}->{description}->{"$group_ident"}->{status}))
			{
				$entry{status} = $session->{Workspace}->{old_menu}->{my_groups}->{description}->{"$group_ident"}->{status};
			}

			my %group_app = (ident => 'group_applications',
			                 status => 'expand',
							 type => 'branch');

			if(exists($session->{Workspace}->{old_menu}->{my_groups}->{description}->{"$group_ident"}->{description}->{group_applications}->{status}))
			{
				$group_app{status} = $session->{Workspace}->{old_menu}->{my_groups}->{description}->{"$group_ident"}->{description}->{group_applications}->{status};
			}

			# getting application list
			# ------------------------
			my $applications_idents = ();
			my $applications_names = ();
			
			foreach my $type (qw(normal portal)) {
				my $application_list = new Mioga2::AppDescList ($config, 
																group_id => $group_ids->[$comp], 
																user_id => $user_id,
																app_type => $type, 
																having_method => 'DisplayMain');
				
				my $idents = $application_list->GetIdents();
				my $names = $application_list->GetLocaleNames($user);
				
				push @$applications_idents, @$idents;
				push @$applications_names, @$names;
			}
			

			my $nb_app = @$applications_idents;

			my %applications;
			for(my $i = 0; $i<$nb_app; $i++) 
			{
				my $app = $applications_idents->[$i];
				my $name = $applications_names->[$i];

				my %group_app = (ident => $name,								 
								 type  => 'leaf');
				my $uri =  new Mioga2::URI ($config,
											group  => $group_ident,
											public => 0,
											application => $app,
											method => "DisplayMain");
				$group_app{leaf_uri} = $uri->GetURI();
				$group_app{group_ident} = $group_ident;
				$group_app{app_ident} = $app;
				
				$applications{$app} = \%group_app;
			}

			$group_app{description} = \%applications;

			
			my %group_files = (ident => 'group_files',
			                   status => 'expand',
							   description => $self->GetUserFileAccess($context, $group_ident),
							   type => 'branch');

			if(exists($session->{Workspace}->{old_menu}->{my_groups}->{description}->{"$group_ident"}->{description}->{group_files}->{status}))
			{
				$group_files{status} = $session->{Workspace}->{old_menu}->{my_groups}->{description}->{"$group_ident"}->{description}->{group_files}->{status};
			}

			my %description;
			$description{$group_app{ident}} = \%group_app;
			$description{$group_files{ident}} = \%group_files;

			$entry{description} = \%description;

			$invited_groups{$group_ident} = \%entry;
			$comp++;
		}
	}

	return \%invited_groups;
}

# ============================================================================

=head2 GetInvitedResourcesApplications ($context)

	build list of resources where current user is invited whith the allowed applications

=cut

# ============================================================================

sub GetInvitedResourcesApplications {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $resource = $context->GetGroup();
	my $dbh = $config->GetDBH();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();
	my $resource_list = $user->GetExpandedResources();

	my %invited_resources;
	my @applications;

	# getting group list
	# -------------------
	my $resource_idents = $resource_list->GetIdents();
	my $resource_ids = $resource_list->GetRowids();


	if (defined($resource_idents) and ref($resource_idents) eq 'ARRAY')
	{
		my $comp = 0;
		foreach my $resource_ident (@$resource_idents)
		{
			my %entry = (ident => $resource_ident,
			             status => 'collapse',
						 type => 'branch');

			if(exists($session->{Workspace}->{old_menu}->{my_resources}->{description}->{"$resource_ident"}->{status}))
			{
				$entry{status} = $session->{Workspace}->{old_menu}->{my_resources}->{description}->{"$resource_ident"}->{status};
			}

			# getting application list
			# ------------------------
			my $application_list = new Mioga2::AppDescList ($config, group_id=> $resource_ids->[$comp], user_id => $user_id, app_type => 'normal', having_method => 'DisplayMain');
			my $applications_idents = $application_list->GetIdents();
			my $applications_names = $application_list->GetLocaleNames($user);

			$application_list = new Mioga2::AppDescList ($config, group_id=> $resource_ids->[$comp], user_id => $user_id, app_type => 'resource', having_method => 'DisplayMain');
			my $res_applications_idents = $application_list->GetIdents();
			my $res_applications_names = $application_list->GetLocaleNames($user);

			push @$applications_idents, @$res_applications_idents;
			push @$applications_names, @$res_applications_names;

			my $nb_app = @$applications_idents;

			my %applications;
			for(my $i = 0; $i<$nb_app; $i++) 
			{
				my $app = $applications_idents->[$i];
				my $name = $applications_names->[$i];

				my %resource_app = (ident => $name,
									type  => 'leaf');

				my $uri =  new Mioga2::URI ($config,
											group  => $resource_ident,
											public => 0,
											application => $app,
											method => "DisplayMain");
				$resource_app{leaf_uri} = $uri->GetURI();
				$resource_app{group_ident} = $resource_ident;
				$resource_app{app_ident} = $app;
				
				$applications{$app} = \%resource_app;
			}
			
			$entry{description} = \%applications;
			
			$invited_resources{$resource_ident} = \%entry;
			$comp++;
		}
	}	

	return \%invited_resources;
}

# ============================================================================

=head2 GetXMLMenuFromSession ($context)

	return XML description of menu according to session

=cut

# ============================================================================

sub GetXMLMenuFromSession {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $dbh = $config->GetDBH();
	my $user_id = $user->GetRowid();
	my $user_ident = $user->GetIdent();

	my $xml = "<menu>";

	if (exists($session->{persistent}->{Workspace}->{menu}))
	{
		my $i_path = 0;
		foreach my $entry (@{$session->{persistent}->{Workspace}->{menu}})
		{
			$xml .= $self->BuildXMLSubMenu($context, $entry, $i_path);
			$i_path++;
		}
	}

	$xml .= "</menu>";

	return $xml;
}

# ============================================================================

=head2 ChangeBranchStatus ($context, $path)

	change collapse/expand status in session

=cut

# ============================================================================

sub ChangeBranchStatus {
	my ($self, $context, $path) = @_;

	my $session = $context->GetSession();

	my @menu_path = split('/', $path);


	my $cursor = $session->{persistent}->{Workspace};
	my $flag = 0;

	warn ("--------> path : $path") if ($debug > 0);
	foreach my $branch_ident (@menu_path)
	{
		warn ("--------> branch_ident : $branch_ident") if ($debug > 0);
		if ($flag)
		{
			$cursor = $cursor->{description}->{$branch_ident};
		}
		else
		{
			$cursor = $cursor->{description}->[$branch_ident];
			$flag++;
		}
		warn (Dumper($cursor)) if ($debug > 0);
	}

	warn ("--------> cursor status before : $cursor->{status}") if ($debug > 0);
	if ($cursor->{status} eq "collapse")
	{
		$cursor->{status} = "expand";
	}
	elsif ($cursor->{status} eq "expand")
	{
		$cursor->{status} = "collapse";
	}

	warn ("--------> cursor status after : $cursor->{status}") if ($debug > 0);
}


#===============================================================================

=head2 ExpandAll

Expand all branches

=cut

#===============================================================================
sub ExpandAll {
	my ($self, $context) = @_;

	my $session = $context->GetSession();
	my $cursor = $session->{persistent}->{Workspace};

	map { $_->{status} = 'expand' } @{$cursor->{menu}};

	return ();
}	# ----------  end of subroutine ExpandAll  ----------

# ============================================================================

=head2 BuildXMLSubMenu ($context, $entry_desc, $path)

	recursively build XML submenu description

=cut

# ============================================================================

sub BuildXMLSubMenu {
	my ($self, $context, $entry_desc, $path) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();

	my $xml = "";

	if ($entry_desc->{type} eq 'end_branch')
	{
		$xml .= "<".st_FormatXMLString($entry_desc->{ident})." />";
	}
	elsif ($entry_desc->{type} eq 'branch')
	{
		$xml .= '<branch ident="'.st_FormatXMLString($entry_desc->{ident}).'" status="'.st_FormatXMLString($entry_desc->{status}).'" path="'.st_FormatXMLString($path).'">';

		if (exists($entry_desc->{description}) && ref($entry_desc->{description}) eq "HASH")
		{
			foreach my $entry (sort keys %{$entry_desc->{description}})
			{
				$xml .= $self->BuildXMLSubMenu($context, $entry_desc->{description}->{$entry}, $path."/".$entry_desc->{description}->{$entry}->{ident});
			}
		}
		$xml .= '</branch>';
	}
	elsif ($entry_desc->{type} eq 'leaf')
	{
		$xml .= '<leaf ident="'.st_FormatXMLString($entry_desc->{ident}).'" app_ident="'.st_FormatXMLString($entry_desc->{app_ident}).'" uri="'.st_FormatXMLString($entry_desc->{leaf_uri}).'">';
		
		if(exists $entry_desc->{app_ident}) {
			$xml .= "<app>$entry_desc->{app_ident}</app>";
			$xml .= "<group_ident>$entry_desc->{group_ident}</group_ident>";
		}
		else {
			$xml .= "<default_link>$entry_desc->{leaf_uri}</default_link>";
		}
		$xml .= '</leaf>';
	}

	return $xml;
}

# ============================================================================

=head2 FillUserValues ()

	Fill the values with current values of the user with rowid $rowid

=cut

# ============================================================================

sub FillUserValues {
	my ($self, $config, $rowid, $values) = @_;

	my $user = new Mioga2::Old::User($config, rowid => $rowid);
	my $time_per_day = $user->GetTimePerDay();

	my $hour_per_day = int($time_per_day / 60);
	my $min_per_day = $time_per_day % 60;

	my $org = new Mioga2::Organizer;
	my $org_pref = $org->Org_GetUserPrefs($config, $user);

	my $def_values = {
					  'firstname' => $user->GetFirstname(), 
					  'lastname' => $user->GetLastname(), 
					  'email' => $user->GetEmail(), 
					  'type' => $user->GetType(), 
					  'rowid' => $user->GetRowid(),					   
					  'ident' => $user->GetIdent(),
					  'working_days' => $user->GetWorkingDays(),
					  'timezone_id' => $user->GetTimezoneId(),
					  'monday_first' => $user->IsMondayFirst(),
					  'hour_per_day' => $hour_per_day,
					  'min_per_day' => $min_per_day,
					  'start_time_hour' => $org_pref->{start_time_hour}, 
					  'start_time_min'  => $org_pref->{start_time_min},
					  'end_time_hour'   => $org_pref->{end_time_hour}, 
					  'end_time_min'    => $org_pref->{end_time_min},
					  'interval'        => $org_pref->{interval},

				  };

	foreach my $key (qw( firstname lastname email rowid ident type working_days timezone_id monday_first hour_per_day min_per_day start_time_hour start_time_min end_time_hour end_time_min interval)) {
		if(!exists $values->{$key}) {
			$values->{$key} = $def_values->{$key};
		}
	}
}

# ============================================================================

=head2 FillURLValues ()

	Retrieve default URL values from the database

=cut

# ============================================================================

sub FillURLValues {
	my ($self, $config, $rowid, $values) = @_;

	my $dbh    = $config->GetDBH();
	my $user = new Mioga2::Old::User($config, rowid => $rowid);
	
	my $default_url = WorkspaceGetDefaultURL($dbh, $user->GetRowid());
	if (!defined($default_url))
	{
		$default_url->{default_url} = '';
	}

	my $def_values = {
					  'default_url' => $default_url->{default_url}, 
					  'home_url' => $default_url->{home_url}, 
				  };

	foreach my $key (qw( default_url home_url )) {
		if(!exists $values->{$key}) {
			$values->{$key} = $def_values->{$key};
		}
	}
}

# ============================================================================

=head2 GetXMLTimezones ()

	Return XML listing available timezones

=cut

# ============================================================================

sub GetXMLTimezones {
	my ($self, $context) = @_;

	my $xml = "<Timezones>";

	my $config = $context->GetConfig();
	my $dbh    = $config->GetDBH();
	my $user   = $context->GetUser();
	my $timezone = $user->GetTimezone();

	my $res = SelectMultiple($dbh, "SELECT * FROM m_timezone");

	foreach my $tz (@$res) {
		$xml .= "<timezone rowid=\"$tz->{rowid}\">".st_FormatXMLString($tz->{ident})."</timezone>";
	}

	$xml .= "</Timezones>";
	return $xml;
}

# ============================================================================

=head2 DisplayEntry ($context, $entry_name)

	Return 1 if the entry $entry name has to be displayed
	else return 0

=cut

# ============================================================================

sub DisplayEntry {
	my ($self, $context, $entry_name) = @_;

	my $config = $context->GetConfig();
	my $dbh = $config->GetDBH();
	my $user = $context->GetUser();
	my $user_id = $user->GetRowid();

	if ($entry_name eq "search"
            or $entry_name eq "my_applications"
            or $entry_name eq "my_groups"
            or $entry_name eq "my_resources"
            or $entry_name eq "my_links"
            or $entry_name eq "my_files"
	)
	{
		my $sql = "select display_ok
		           from workspace_display_entry 
			   where 
			   user_id = $user_id 
			   and ident = '$entry_name' 
			   ";
		my $res = SelectSingle ($dbh, $sql);

		if (defined($res))
		{
			if ($res->{display_ok} == 1)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}
	

}

# ============================================================================

=head2 GetAppDesc ()

	Return the current application description.

=cut

# ============================================================================

sub GetAppDesc {
	my ($self, $context) = @_;

    my %AppDesc = ( ident   => 'Workspace',
	    	name	=> __('Workspace'),
                package => 'Mioga2::Workspace',
		type => 'shell',
                description => __('The Mioga2 Workspace Application'),
                all_groups  => 0,
                all_users  => 1,
                can_be_public => 0,
				is_user             => 1,
				is_group            => 0,
				is_resource         => 0,

                functions   => { 'All' => __('All Functions') },

                func_methods  => { 'All' => [ 'DisplayMain', 'DisplayHome', 'DisplayMenu', 'DisplayHello', 'DisplayDefaultURL', 'MenuComponents', 'DisplayLinks', 'ModifyLink', 'AddLink', 'AddAppToLinks',  'DisplayAppTitle', 'DeleteLink'  ]
                                 },

				non_sensitive_methods => [ 'DisplayMain', 'DisplayHome', 'DisplayMenu', 'DisplayHello', 'DisplayDefaultURL', 'MenuComponents', 'DisplayLinks', 'ModifyLink', 'AddLink', 'AddAppToLinks',  'DisplayAppTitle', 'DeleteLink' ]

                );

	return \%AppDesc;
}


# ============================================================================

=head2 DeleteGroupData ($config, $group_id)

	Remove group data in database when a group is deleted.

	This function is called when a group/user/resource is deleted.

=cut

# ============================================================================

sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;

	my $dbh = $config->GetDBH();

	ExecSQL($dbh, "DELETE FROM workspace_resize WHERE user_id = $group_id");
	ExecSQL($dbh, "DELETE FROM workspace_display_entry WHERE user_id = $group_id");
	ExecSQL($dbh, "DELETE FROM workspace_default_url WHERE user_id = $group_id");
	ExecSQL($dbh, "DELETE FROM workspace_link WHERE user_id = $group_id");
	ExecSQL($dbh, "DELETE FROM workspace_branch WHERE user_id = $group_id");
	ExecSQL($dbh, "DELETE FROM m_home_url WHERE user_id = $group_id");
}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
