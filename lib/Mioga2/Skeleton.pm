#===============================================================================
#
#         FILE:  Skeleton.pm
#
#  DESCRIPTION:  Mioga2 skeleton class
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  05/01/2011 10:00
#===============================================================================
#
#  Copyright (c) year 2011, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Skeleton.pm: Mioga2 skeleton class

=head1 DESCRIPTION

This module provides a convenient interface to Mioga2 skeletons, allowing to
get different lists (users, groups, applications) from a skeleton file. It
provides a way to check a skeleton for consistency.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Skeleton;

use Mioga2::UserList;
use Mioga2::GroupList;
use Mioga2::TeamList;
use Mioga2::ApplicationList;

use Data::Dumper;

my $debug = 0;

my @supported_types = qw/user group instance/;

#===============================================================================

=head2 new

Create a new Mioga2::Skeleton object.

=over

=item B<arguments:>

=over

=item I<$config:> a Mioga2::Config object matching the instance to work into.

=item I<%attributes:> a list of key / value pairs matching the skeleton. The following keys are supported:

=over

=item type: the skeleton type (user, group, instance)

=item lang: the skeleton language

=item file: the skeleton filename (without path)

=back

=back

=item B<return:> a Mioga2::Skeleton object.

=back

=cut

#===============================================================================
sub new {
	my ($class, $config, %attributes) = @_;
	print STDERR "[Mioga2::Skeleton::new] Entering\n" if ($debug);

	my $self = { };
	bless($self, $class);

	$self->{config} = $config;

	# Initialize attributes
	$self->{attributes} = \%attributes;

	# Set skeleton type to lowercase
	$self->{attributes}->{type} = lc ($self->{attributes}->{type});

	# Initialize file path
	$self->{attributes}->{path} = $self->{config}->GetSkeletonDir () . '/' . $self->{attributes}->{lang} . '/' . $self->{attributes}->{type} . '/' . $self->{attributes}->{file};

	# Check if file exists
	if (! -e $self->{attributes}->{path}) {
		throw Mioga2::Exception::Simple ('Mioga2::Skeleton::new', "Skeleton file '$self->{attributes}->{path}' does not exist.");
	}

	# Load from file
	$self->Load ();

	print STDERR "[Mioga2::Skeleton::new] Leaving\n" if ($debug);
	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 GetAttributes

Get skeleton inner attributes.

=cut

#===============================================================================
sub GetAttributes {
	my ($self) = @_;

	return ($self->{contents}->{attributes});
}	# ----------  end of subroutine GetAttributes  ----------


#===============================================================================

=head2 GetUsers

Get list of users contained in the skeleton.

=cut

#===============================================================================
sub GetUsers {
	my ($self) = @_;
	print STDERR "[Mioga2::Skeleton::GetUsers] Entering\n" if ($debug);

	# Initialize list of user idents
	my @idents = ();
	for (keys (%{$self->{contents}->{users}})) {
		push (@idents, $_);
	}

	my $users = Mioga2::UserList->new ($self->{config}, { attributes => { ident => \@idents } });

	print STDERR "[Mioga2::Skeleton::GetUsers] Leaving (" . $users->Count () . " users)\n" if ($debug);
	return ($users);
}	# ----------  end of subroutine GetUsers  ----------


#===============================================================================

=head2 GetGroups

Get list of groups contained in the skeleton.

=cut

#===============================================================================
sub GetGroups {
	my ($self) = @_;
	print STDERR "[Mioga2::Skeleton::GetGroups] Entering\n" if ($debug);

	# Initialize list of group idents
	my @idents = ();
	for (keys (%{$self->{contents}->{groups}})) {
		push (@idents, $_);
	}

	my $groups = Mioga2::GroupList->new ($self->{config}, { attributes => { ident => \@idents } });

	print STDERR "[Mioga2::Skeleton::GetGroups] Leaving (" . $groups->Count () . " groups)\n" if ($debug);
	return ($groups);
}	# ----------  end of subroutine GetGroups  ----------


#===============================================================================

=head2 GetTeams

Get list of teams contained in the skeleton.

=cut

#===============================================================================
sub GetTeams {
	my ($self) = @_;
	print STDERR "[Mioga2::Skeleton::GetTeams] Entering\n" if ($debug);

	# Initialize list of team idents
	my @idents = ();
	for (@{$self->{contents}->{teams}}) {
		push (@idents, $_);
	}

	my $teams = Mioga2::TeamList->new ($self->{config}, { attributes => { ident => \@idents } });

	print STDERR "[Mioga2::Skeleton::GetTeams] Leaving (" . $teams->Count () . " teams)\n" if ($debug);
	return ($teams);
}	# ----------  end of subroutine GetTeams  ----------


#===============================================================================

=head2 GetApplications

Get list of applications contained in the skeleton.

=cut

#===============================================================================
sub GetApplications {
	my ($self) = @_;
	print STDERR "[Mioga2::Skeleton::GetApplications] Entering\n" if ($debug);

	# initialize list of application idents
	my @idents = ();
	for (@{$self->{contents}->{applications}}) {
		push (@idents, $_->{ident});
	}

	# If instance skeleton, initialize Mioga2::ApplicationList from Mioga2::MiogaConf, otherwise from instance Mioga2::Config
	my $conf = $self->{config};
	if ($self->{attributes}->{type} eq 'instance') {
		$conf = $self->{config}->GetMiogaConf ();
	}

	my $applications = Mioga2::ApplicationList->new ($conf, { attributes => { ident => \@idents } });

	print STDERR "[Mioga2::Skeleton::GetApplications] Leaving (" . $applications->Count () . " applications)\n" if ($debug);
	return ($applications);
}	# ----------  end of subroutine GetApplications  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


#===============================================================================

=head2 Load

Load skeleton file and store its contents as inner attributes of the object.

=cut

#===============================================================================
sub Load {
	my ($self) = @_;
	print STDERR "[Mioga2::Skeleton::Load] Entering\n" if ($debug);

	my $data = '';

	#-------------------------------------------------------------------------------
	# Read file
	#-------------------------------------------------------------------------------
	open(FILE, "< $self->{attributes}->{path}") or throw Mioga2::Exception::Simple ('Mioga2::Skeleton::Load', "Can't open skeleton file '$self->{attributes}->{path}': $!.");
	while (<FILE>) { $data .= $_ };
	close (FILE);

	#-------------------------------------------------------------------------------
	# Parse XML
	#-------------------------------------------------------------------------------
	$data =~ s/<--!.*?-->//gsm; # remove comments

	my $xml		  = new Mioga2::XML::Simple(forcearray => 1);
	my $xmltree   = $xml->XMLin($data);
	my $skel_name = (exists($xmltree->{'name'}))? $xmltree->{'name'} : "";
	my $skel_type = (exists($xmltree->{'type'}))? lc ($xmltree->{'type'}) : "";

	if (!grep (/^$skel_type$/, @supported_types)) {
		throw Mioga2::Exception::User ('Mioga2::Skeleton::Load', "Skeleton type '$skel_type' is not supported.");
	}

	if ($skel_type ne $self->{attributes}->{type}) {
		throw Mioga2::Exception::User ('Mioga2::Skeleton::Load', "Skeleton type obtained from file contents ('$skel_type') does not match expected type ('$self->{attributes}->{type}').");
	}

	#-------------------------------------------------------------------------------
	# Translate XML tree
	#-------------------------------------------------------------------------------
	my %contents = ();

	# Attributes
	$contents{attributes} = $self->GetHash ($xmltree->{attributes});

	# Specific attributes for groups
	if ($self->{attributes}->{type} eq 'group') {
		# Public space is automatically enabled if skeleton contains a public space configuration
		for (@{$xmltree->{filesystem}->[0]->{space}}) {
			if ($_->{type} eq 'public') {
				$contents{attributes}->{public_part} = 1;
			}
		}
		if ($xmltree->{filesystem}->[0]->{history}->[0] && $xmltree->{filesystem}->[0]->{history}->[0] eq '1') {
			$contents{attributes}->{history} = 1;
		}
	}

	# Applications
	for (@{$xmltree->{applications}->[0]->{application}}) {
		push (@{$contents{applications}}, $_);
	}

	# Profiles
	$contents{profiles} = $xmltree->{profiles}->[0];

	# Users
	for (@{$xmltree->{users}->[0]->{user}}) {
		$contents{users}->{$_->{ident}} = $_->{profile}->[0];
	}

	# Groups
	for (@{$xmltree->{groups}->[0]->{group}}) {
		$contents{groups}->{$_->{ident}} = $_->{profile}->[0];
	}

	# Teams
	for (@{$xmltree->{teams}->[0]->{team}}) {
		push (@{$contents{teams}}, $_->{ident});
	}

	#-------------------------------------------------------------------------------
	# Store skeleton contents into object
	#-------------------------------------------------------------------------------
	$self->{contents} = \%contents;

	print STDERR "[Mioga2::Skeleton::Load] Leaving\n" if ($debug);
}	# ----------  end of subroutine LoadSkeleton  ----------


#===============================================================================

=head2 GetHash

Get a clean hash from the output of the XML parser.

=cut

#===============================================================================
sub GetHash {
	my ($self, $data) = @_;

	my $contents = undef;

	for (keys (%{$data->[0]})) {
		if ((ref ($data->[0]->{$_}->[0]) eq 'HASH')) {
			$contents->{$_} = $self->GetHash ($data->[0]->{$_});
		}
		else {
			$contents->{$_} = $data->[0]->{$_}->[0];
		}
	}

	return ($contents);
}	# ----------  end of subroutine GetHash  ----------

1;

