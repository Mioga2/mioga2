#===============================================================================
#
#         FILE:  CSV.pm
#
#  DESCRIPTION:  CSV handling module.
#
# REQUIREMENTS:  ---
#        NOTES:  TODO Handle system and user skeletons, merging of both, etc.
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  22/04/2010 16:40
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

CSV.pm: CSV handling module.

=head1 DESCRIPTION

This module provides import from / export to CSV facilities.

=head1 SYNOPSIS

 use Mioga2::CSV;

 my $converter = new Mioga2::CSV (
 		columns => [ qw/col1 col2 col3/ ],
		separator => ',',
		quote => '"'
	);

 my $array = (
 	{
		col1 => 'value1.1',
		col2 => 'value1.2',
		col3 => 'value1.3'
	},
 	{
		col1 => 'value2.1',
		col2 => 'value2.2',
		col3 => 'value2.3'
	},
 	{
		col1 => 'value3.1',
		col2 => 'value3.2',
		col3 => 'value3.3'
	}
 );
 my $csv = $converter->Convert ($array);

 $csv = '"value1.1", "value1.2", "value1.3"
 "value2.1", "value2.2", "value2.3"
 "value3.1", "value3.2", "value3.3"';
 $array = $converter->Convert ($csv);

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::CSV;

use Data::Dumper;
use Text::ParseWords;

my $debug = 0;


#===============================================================================

=head2 new

Create a new Mioga2::CSV object.

=head3 Incoming arguments

=over

=over

=item B<%attributes:> a list of key / value pairs describing converter configuration.

=over

=item B<columns:> a list of hash key names corresponding to CSV columns.

=item B<separator:> the separator character to use, defaults to ','.

=item B<quote:> the quoting characted to use, defaults to '"'.

=back

=back

=back

=head3 Return value

=over

A Mioga2::CSV object.

=back

=cut

#===============================================================================
sub new {
	my ($class, %attributes) = @_;
	print STDERR "[Mioga2::Classes::CSV::new] Entering\n" if ($debug);

	my $self = { };
	bless($self, $class);

	for (keys (%attributes)) {
		$self->{$_} = $attributes{$_};
	}

	$self->{separator} = ',' unless defined ($self->{separator});
	$self->{quote} = '"' unless defined ($self->{quote});

	print STDERR "[Mioga2::CSV::new] Leaving\n" if ($debug);
	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 Convert

Convert an array hashes to a CSV string of a CSV string to an array hashes.

=head3 Incoming Arguments

=over

=over

=item B<$obj:> the object to convert. It can be a CSV string or an array of hashes.

=back

=back

=head3 Return value

Depending of the type of argument B<$obj>, this method will return a CSV string
or an array of hashes.

=cut

#===============================================================================
sub Convert {
	my ($self, $obj) = @_;

	my $converted = undef;

	if (ref ($obj) eq 'ARRAY') {
		# Convert array to CSV string
		$converted = '';
		for (@$obj) {
			my @line;
			for (my $i = 0; $i < scalar (@{$self->{columns}}); $i++) {
				my $key = $self->{columns}->[$i];
				push (@line, $_->{$key} ? $_->{$key} : '');
			}
			$converted .= $self->{quote} . join ("$self->{quote}$self->{separator}$self->{quote}", @line) . "$self->{quote}\n";
		}
	}
	else {
		# Convert CSV string to array
		my @lines = split (/\n/, $obj);
		for (@lines) {
			my $record;
			my @fields = quotewords ($self->{separator} . '[ ]*', 0, $_);
			for (my $i = 0; $i < scalar (@{$self->{columns}}); $i++) {
				$record->{$self->{columns}->[$i]} = $fields[$i] if (defined ($fields[$i]));
			}
			push (@$converted, $record);
		}
	}

	print STDERR "[Mioga2::CSV::Convert] Converted: " . Dumper $converted if ($debug);

	return ($converted);
}	# ----------  end of subroutine Convert  ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2

=head1 COPYRIGHT

Copyright (C) 2003-2010, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;
