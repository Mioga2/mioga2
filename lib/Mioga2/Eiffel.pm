#===============================================================================
#
#  Copyright (c) year 2013, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Eiffel.pm: the Mioga2 group planning application

=head1 DESCRIPTION

The Mioga2::Eiffel shows group planning with consolidation of all members calendars in the respect of access rights

Eiffel can be used as a group application.

=head1 METHODS DESCRIPTION

=cut

#
# ============================================================================

package Mioga2::Eiffel;
use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'eiffel';

use strict;
use warnings;
use open ':encoding(utf8)';

use Error qw(:try);
use Mioga2::Document;
use Data::Dumper;
use List::MoreUtils qw(uniq);
use Mioga2::tools::string_utils;
use Mioga2::tools::Convert;
use Mioga2::tools::args_checker;
use Mioga2::Content::XSLT;
use Mioga2::Exception::Simple;
use Mioga2::TaskCategory;
use Mioga2::Classes::Time;
use Mioga2::Classes::CalEvent;

my $debug = 0;

# ============================================================================
# GetAppDesc ()
# ============================================================================

sub GetAppDesc {
	my ($self, $context) = @_;
    my %AppDesc = ( 
    	ident   => 'Eiffel',
	    name	=> __('Eiffel'),
        package => 'Mioga2::Eiffel',
		type => 'portal',
        description => __('The Mioga2 planning application'),
        api_version => '2.4',
        all_groups  => 1,
        all_users  => 0,
        all_resources  => 0,
        is_user  => 0,
        is_group  => 1,
        is_resource  => 0,
        can_be_public => 0,
        usable_by_resource => 0,

        functions   => { 
			'Read' => __('Read only functions'),
			'Anim' => __('Animation functions'),
		},
				
        func_methods  => { 
			Read => [ 'DisplayMain', 'GetConfiguration', 'GetTasks' ],
			Anim => [ 'SetPreferences' ],
		},
		public_methods => [
		],
		func_mioglets => { },
		public_mioglets => [  ],
		non_sensitive_methods => [
			'DisplayMain',
			'GetConfiguration',
			'GetTasks'
		]
    );
	return \%AppDesc;
}

#===============================================================================

=head1 INTERFACE METHODS

=cut

#===============================================================================

=head2 DisplayMain

Display main window for Eiffel.

=head3 Generated XML

=over

	<DisplayMain>
		<miogacontext>See Mioga2::RequestContext</miogacontext>
	</DisplayMain>

=back

=cut

# ============================================================================
sub DisplayMain {
	my ($self, $context) = @_;
	print STDERR "Eiffel::DisplayMain\n" if ($debug);
	my $xml;
	$xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= '<DisplayMain>';
	$xml .= $context->GetXML();
	#TODO read configuration to limit server requests
	$xml .= '</DisplayMain>';
	print STDERR "xml = $xml\n" if $debug;
	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'eiffel.xsl', locale_domain => 'eiffel_xsl');
	$content->SetContent($xml);
	return $content;
}
#===============================================================================

=head1 WEBSERVICE METHODS

=cut

#===============================================================================

=head2 GetConfiguration

	Get the planning configuration.

=cut

# ============================================================================
sub GetConfiguration {
	my ($self, $context) = @_;
	print STDERR "Eiffel::GetConfiguration\n" if ($debug);

	my $data = {};
	my $config = {};
	# User list
	my $user_list = $context->GetGroup()->GetExpandedUserList();
	my @users = $user_list->GetUsers();
	print STDERR "users = ".Dumper(\@users)."\n" if ($debug);
	foreach my $u (@users) {
		push @{$config->{users}}, { rowid => $u->{rowid},
									firstname => $u->{firstname},
									lastname => $u->{lastname},
									ident => $u->{ident} };
	}
	# Resource list
	my $res_list = $context->GetGroup()->GetInvitedResourceList();
	my @resources = $res_list->GetResources();
	print STDERR "resources = ".Dumper(\@resources)."\n" if ($debug);
	foreach my $r (@resources) {
		push @{$config->{resources}}, { rowid => $r->{rowid},
									ident => $r->{ident} };
	}
	#
	# Task categories defined
	#
	my $taskcat = new Mioga2::TaskCategory ($context->GetConfig());
	my $cats = $taskcat->GetList ();
	print STDERR "cats = ".Dumper($cats)."\n" if ($debug);
	$config->{categories} = [];
	foreach my $c (@$cats) {
		print STDERR "c = ".Dumper($c)."\n" if ($debug);
		push @{$config->{categories}}, { name => $c->{name}, category_id => $c->{rowid},  bgcolor => $c->{bgcolor}, fgcolor => $c->{fgcolor} };
	}
	# TODO preferences
	$config->{first_day} = 1;
	$config->{anim_right} = 0;
	$config->{anim_right} = 1 if ($self->GetUserRights()->{Anim});

	my $group_id = $context->GetGroup()->Get('rowid');
	my $prefs = $self->DBReadPreferences($group_id);
	if (defined($prefs->{week_count})) {
		$config->{week_count} = $prefs->{week_count};
	}
	else {
		$config->{week_count} = 3;
	}
	if (defined($prefs->{display})) {
		$config->{display} = $prefs->{display};
	}
	else {
		$config->{display} = "[]";
	}
	if (defined($prefs->{show_groups})) {
		$config->{show_groups} = $prefs->{show_groups};
	}
	else {
		$config->{show_groups} = "0";
	}

	$data->{status} = "OK";
	$data->{config} = $config;
	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 SetPreferences

	Records calendar preferences for user or group

=cut

# ============================================================================
sub SetPreferences {
	my ($self, $context) = @_;
	print STDERR "Eiffel::SetPreferences\n" if ($debug);

	my $params = [	
			[ [ 'week_count' ], 'disallow_empty', 'wantint' ],
			[ [ 'show_groups' ], 'disallow_empty', 'wantint' ],
			[ [ 'display' ], 'disallow_empty', 'stripxws' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $group_id = $context->GetGroup()->Get('rowid');
		$self->DBWritePreferences($group_id, $values);
		# reread configuration and send
		$data = $self->GetConfiguration($context);
	}

	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 GetTasks

	Return tasks for given period

=cut

# ============================================================================
sub GetTasks {
	my ($self, $context) = @_;
	print STDERR "Eiffel::GetTasks =========================================\n" if ($debug);

	my $params = [	
			[ [ 'start_year', 'start_month', 'start_day' ], 'disallow_empty', 'stripxws', 'wantint' ],
			[ [ 'end_year', 'end_month', 'end_day' ], 'disallow_empty', 'stripxws', 'wantint' ],
			[ [ 'show_groups' ], 'disallow_empty', 'stripxws', 'wantint' ],
			[ [ 'users_rowid', 'res_rowid' ], 'disallow_empty', 'stripxws' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $data = {};
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
		$data->{errors} = $errors;
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		#
		# get calendar list and verify access right
		#
		my $users_rowid = Mioga2::tools::Convert::JSONToPerl($values->{users_rowid});
		print STDERR "users_rowid = " . Dumper($users_rowid) . "\n" if ($debug);
		my $res_rowid = Mioga2::tools::Convert::JSONToPerl($values->{res_rowid});
		print STDERR "res_rowid = " . Dumper($res_rowid) . "\n" if ($debug);

		my $cals = $self->DBGetCalendarList($users_rowid, $res_rowid, $values->{show_groups});
		my @cids;
		foreach my $c (@$cals) {
			push @cids, $c->{calendar_id};
		}
		@{$values->{cal_ids}} = uniq @cids;
		print STDERR "cal_ids = " . Dumper($values->{cal_ids}) . "\n" if ($debug);
		$values->{db} = $self->{db};
		$values->{user_id} = $user_id;

		$data->{tasks} = Mioga2::Classes::CalEvent::GetEventsForCals($values);
		$data->{project_tasks} = $self->DBGetProjectTasksForUsers($users_rowid, $values);

		$data->{calendars} = $cals;
		$data->{status} = "OK";
	}
	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

=head2 InitApp

	Initialize application parameters

=cut

# ============================================================================
sub InitApp {
	my ($self, $context) = @_;
	print STDERR "Eiffel::InitApp\n" if ($debug);

	$self->{mioga_id} = $context->GetConfig()->GetMiogaId();
	$self->{app_id} = $context->GetAppDesc()->GetRowid();
	print STDERR "mioga_id = ".$self->{mioga_id}."   app_id = ".$self->{app_id}."\n" if ($debug);
}
# ============================================================================

=head2 DBWritePreferences

	Write preferences to database.

=cut

# ============================================================================
sub DBWritePreferences {
	my ($self, $group_id, $params) = @_;
	print STDERR "Eiffel::DBWritePreferences\n" if ($debug);

	my $show_groups = 'f';
	if ($params->{show_groups}) {
		$show_groups = 't';
	}

	my $sql = "SELECT rowid FROM eiffel_pref WHERE group_id = ?";
	my $res = $self->{db}->SelectSingle($sql, [$group_id]);
	if (defined($res)) {
		$sql = "UPDATE eiffel_pref SET modified=now(), week_count=?, show_groups = ?, display=? WHERE rowid = ?";
		$self->{db}->ExecSQL($sql, [$params->{week_count}, $show_groups, $params->{display}, $res->{rowid}]);
	}
	else {
		$sql = "INSERT INTO eiffel_pref (created, modified, group_id, week_count, show_groups, display) VALUES (now(), now(), ?, ?, ?, ?)";
		$self->{db}->ExecSQL($sql, [$group_id, $params->{week_count}, $show_groups, $params->{display}]);
	}
}
# ============================================================================

=head2 DBReadPreferences

	Read preferences from database function of group or user access.

=cut

# ============================================================================
sub DBReadPreferences {
	my ($self, $group_id) = @_;
	print STDERR "Eiffel::DBReadPreferences\n" if ($debug);

	my $sql = "SELECT * FROM eiffel_pref WHERE group_id = ?";
	my $res = $self->{db}->SelectSingle($sql, [$group_id]);

	return $res;
}
# ============================================================================

=head2 DBGetCalendarList

	Return IDs of calendar owned by users and resources

=cut

# ============================================================================
sub DBGetCalendarList {
	my ($self, $users_rowid, $res_rowid, $show_groups) = @_;
	print STDERR "Eiffel::DBGetCalendarList\n" if ($debug);
	my @sql_parts;
	# users calendars
	if (scalar(@$users_rowid) > 0) {
		my $type = 'type = 0';
		if ($show_groups) {
			$type = "($type OR type=2)";
		}
		push @sql_parts, "SELECT rowid as calendar_id, 'user' as type,user2cal.user_id as item_id FROM calendar,user2cal WHERE user2cal.user_id in (".join(",", @$users_rowid).") AND user2cal.calendar_id=calendar.rowid AND calendar.private='f' AND $type";
	}

	#resources calendars
	if (scalar(@$res_rowid) > 0) {
		push @sql_parts, "SELECT rowid as calendar_id, 'res' as type, group2cal.group_id as item_id FROM calendar,group2cal WHERE group2cal.group_id in (".join(",", @$res_rowid).") AND group2cal.calendar_id=calendar.rowid AND calendar.private='f'";
	}

	my $sql = join(" UNION ", @sql_parts);
	print STDERR "sql = $sql\n" if ($debug);

	my $res = $self->{db}->SelectMultiple($sql);

	return $res;
}
# ============================================================================

=head2 DBGetProjectTasksForUsers

	Return List of project tasks for users list

=cut

# ============================================================================
sub DBGetProjectTasksForUsers {
	my ($self, $users_rowid, $values) = @_;
	print STDERR "Eiffel::DBGetProjectTasksForUsers\n" if ($debug);
	my $res;
	if (scalar(@$users_rowid) > 0) {
		my $start_limit = sprintf("%.4d-%.2d-%.2d 00:00:00", $values->{start_year}, $values->{start_month}, $values->{start_day});
		my $end_limit = sprintf("%.4d-%.2d-%.2d 23:59:59", $values->{end_year}, $values->{end_month}, $values->{end_day});

		my $sql = "SELECT group_ident,project_label,label, dstart,dend,user_id, bgcolor FROM project_task_view"
					." WHERE user_id in (".join(",", @$users_rowid).") AND dstart IS NOT NULL AND dend IS NOT NULL"
							." AND ((dstart >= ? OR dend >= ?) AND (dstart <= ? OR dend <= ?))";

		print STDERR "sql = $sql\n" if ($debug);
		$res = $self->{db}->SelectMultiple($sql, [$start_limit, $start_limit, $end_limit, $end_limit]);
	}
	return $res;
}

# ============================================================================

=head2 RevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

	Remove user data in database when a user is revoked of a group.

	This function is called when :
	- a user is revoked of a group
	- a team is revoked of a group for each team member not namly
	  invited in group
	- a user is revoked of a team  for each group where the team is
	  member and the user not namly invited in group

=cut

# ============================================================================

sub RevokeUserFromGroup {
	my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;

	print STDERR "Eiffel::RevokeUserFromGroup\n" if ($debug);
}
# ============================================================================

=head2 RevokeTeamFromGroup ($config, $group_id, $team_id)

	Remove team data in database when a team is revoked of a group.

	This function is called when :
	- a team is revoked of a group

=cut

# ============================================================================
sub RevokeTeamFromGroup {
	my ($self, $config, $group_id, $team_id) = @_;
	print STDERR "Eiffel::RevokeTeamFromGroup nothing to do \n" if ($debug);

	#Nothing to do
}
# ============================================================================

=head2 DeleteGroupData ($config, $group_id)

	Remove group data in database when a group is deleted.

	This function is called when a group/user/resource is deleted.

=cut

# ============================================================================

sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;
	print STDERR "Eiffel::DeleteGroupData for group_id $group_id\n" if ($debug);
	# TODO verify
	my $sql = "DELETE FROM eiffel_pref WHERE group_id = ?";
	$self->{db}->ExecSQL($sql, [$group_id]);

}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2013, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
