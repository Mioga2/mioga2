# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
AppDescList.pm: object used to manipulate list of application descriptions

=head1 DESCRIPTION

This module permits to manipulate list of application descriptions

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
package Mioga2::AppDescList;
use strict;

use Locale::TextDomain::UTF8 'appdesclist';

use Error qw(:try);
use Data::Dumper;
use Mioga2::LDAP;
use Mioga2::AppDesc;
use Mioga2::tools::database;
use Mioga2::tools::date_utils;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::Exception::User;
use Mioga2::Exception::Application;
use Mioga2::tools::APIAuthz;
use vars qw(@ISA);

my $debug=0;

# ============================================================================

=head2 new ($config, %params)

	Create a new AppDescList object.

	There are five creation methods :

=over 4

=item create from nothing (empty list)
	
	new Mioga2::AppDescList ($config)

=item create from the application ident list
	
	new Mioga2::AppDescList ($config, ident_list => $app_idents)

=item create from the group rowid
	
	new Mioga2::AppDescList ($config, group_id => $group_rowid, app_type => TYPE)

=item create from the group ident
	
	new Mioga2::AppDescList ($config, group_ident => $group_ident, app_type => TYPE)

=item create from the method name and group_id (with method $method_ident enabled for user $user_rowid)
	
	new Mioga2::AppDescList ($config, having_method => $method_ident, user_id => $user_rowid, group_id => $group_rowid)

=back

	app_type (normal | library | shell | portal) is optional.

=cut

# ============================================================================

sub new {
	my ($class, $config, %param) = @_;

	my $self = { };

	if (!defined($config)) {
		throw Mioga2::Exception::Simple("Mioga2::AppDescList", __"Missing parameters.");
	}

	$self->{config} = $config;

	bless($self, $class);


	if(!exists($param{app_type}))
	{
		$param{app_type} = "";
	}

	if(exists $param{ident_list}) {
		$self->InitializeFromIdentList($param{ident_list});
	}	
	elsif(exists $param{having_method}) {
		$self->InitializeFromMethodIdent($param{having_method}, $param{user_id},
										 $param{group_id}, $param{app_type});
	}	
	elsif(exists $param{group_id} && exists $param{user_id}) {
		$self->InitializeFromGroupUserRowid($param{group_id}, $param{user_id}, $param{app_type});
	}
	elsif(exists $param{group_id}) {
		$self->InitializeFromGroupRowid($param{group_id}, $param{app_type});
	}
	elsif(exists $param{group_ident}) {
		$self->InitializeFromGroupIdent($param{group_ident}, $param{app_type});
	}
	else {
		# nothing to do
		$self->{list} = [];
	}
	
	return $self;
}

# ============================================================================

=head2  GetIdents ()

	return list of idents for applications

=cut

# ============================================================================

sub GetIdents {
	my ($self) = @_;
	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my @rowid = @{$self->{list}};

	if(! @rowid) {
		return [];
	}

	my $sql = "select rowid, ident as attr from m_application where rowid in ( ";
	$sql .= join (',',@rowid);
	$sql .= " )";

	my $attribute_list = SelectMultiple ($dbh, $sql);

	if (! defined($attribute_list))
	{
		throw Mioga2::Exception::DB("Mioga2::GetApplicationsIdents", $DBI::err,  $DBI::errstr, $sql);
	}
	my $attributes = $self->GetAttributesForRowids($attribute_list);

	return $attributes;

}


# ============================================================================

=head2  GetType ()

	return list of idents for applications

=cut

# ============================================================================

sub GetType {
	my ($self) = @_;
	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetDBH();

	my @rowid = @{$self->{list}};
	if(! @rowid) {
		return [];
	}

	my $sql = "select m_application.rowid, m_application_type.ident as attr from m_application, m_application_type where m_application.rowid in ( ";
	$sql .= join (',',@rowid);
	$sql .= " )";

	my $attribute_list = SelectMultiple ($dbh, $sql);

	if (! defined($attribute_list))
	{
		throw Mioga2::Exception::DB("Mioga2::GetType", $DBI::err,  $DBI::errstr, $sql);
	}
	my $attributes = $self->GetAttributesForRowids($attribute_list);

	return $attributes;

}

# ============================================================================

=head2  GetDescriptions ()

	return list of descriptions for applications

=cut

# ============================================================================

sub GetDescriptions {
	my ($self) = @_;
	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetDBH();

	my @rowid = @{$self->{list}};
	if(! @rowid) {
		return [];
	}

	my $sql = "select rowid, description as attr from m_application where rowid in ( ";
	$sql .= join (',',@rowid);
	$sql .= " )";

	my $attribute_list = SelectMultiple ($dbh, $sql);

	if (! defined($attribute_list))
	{
		throw Mioga2::Exception::DB("Mioga2::GetApplicationsDescriptions", $DBI::err,  $DBI::errstr, $sql);
	}
	my $attributes = $self->GetAttributesForRowids($attribute_list);

	return $attributes;

}

# ============================================================================

=head2  GetLocaleNames ($user)

	return list of name translated in given user locale

=cut

# ============================================================================

sub GetLocaleNames {
	my ($self, $user) = @_;
	
	my $idents = $self->GetIdents();
	
	my $lang = $user->GetLang();

	my $ad = new Mioga2::AppDesc($self->{config});

	my @res;
	foreach my $ident (@$idents) {
		my $applang = $ad->LoadAppsLang($lang);

		my @tmp = grep {$_->{ident} eq $ident} @{$applang->{application}};

		if(@tmp) {
			push @res, $tmp[0];
		}
		else {
			push @res, { name => $ident};
		}
	}


	@res = map {$_->{name}} @res;

	return \@res;
}


# ============================================================================

=head2  GetLocaleDescriptions ($user)

	return list of description translated in given user locale

=cut

# ============================================================================

sub GetLocaleDescriptions {
	my ($self, $user) = @_;
	
	my $idents = $self->GetIdents();

	my $lang = $user->GetLang();

	my $ad = new Mioga2::AppDesc($self->{config});

	my @res;
	foreach my $ident (@$idents) {
		my $applang = $ad->LoadAppsLang($lang);

		my @tmp = grep {$_->{ident} eq $ident} @{$applang->{application}};

		if(@tmp) {
			push @res, $tmp[0];
		}
		else {
			push @res, { description => $ident};
		}
	}



	@res = map {ref($_->{description}) eq 'HASH' ? $_->{name}:$_->{description}} @res;

	return \@res;
}


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head2 InitializeFromIdentList ($ident_list)

	Initialize the AppDescList object from ident list

=cut

# ============================================================================

sub InitializeFromIdentList {
	my ($self, $ident_list) = @_;
	my $dbh = $self->{config}->GetDBH();

	my $application_rowid_list = SelectMultiple($dbh, "SELECT * FROM m_application, m_instance_application ".
												      "WHERE m_application.ident IN (".join(',', map {"'$_'"} @$ident_list).") AND ".
													  "      m_application.rowid = m_instance_application.application_id AND".
												      "      m_instance_application.mioga_id = ".$self->{config}->GetMiogaId());
	

	my $ident_to_rowid;
	foreach my $val (@$application_rowid_list)
	{
		if(!defined($val->{ident}))
		{
			throw Mioga2::Exception::Application("Mioga2::AppDescList::InitializeFromIdent", __"Invalid ident");
		}
		$ident_to_rowid->{$val->{ident}} = $val->{rowid};
	}

	my @rowids;
	foreach my $id (@$ident_list)
	{
		push (@rowids, $ident_to_rowid->{$id});
	}

	$self->{list} = \@rowids;
}

# ============================================================================

=head2 InitializeFromGroupRowid ($group_rowid, $app_type)

	Initialize the AppDescList object from group_rowid

	$app_type is optionnal

=cut

# ============================================================================

sub InitializeFromGroupRowid {
	my ($self, $group_rowid, $app_type) = @_;
	my $dbh = $self->{config}->GetDBH();
	my @rowids;

	my $sql;
	if ($app_type eq "normal" || $app_type eq "shell" || $app_type eq "library" )
	{
		$sql = "select m_application_group_allowed.application_id
			   from m_application_group_allowed, m_application_type, m_application 
			   where 
			   m_application_group_allowed.group_id = $group_rowid
			   and m_application_group_allowed.application_id = m_application.rowid 
			   and m_application.type_id = m_application_type.rowid 
			   and m_application_type.ident = '$app_type'
		";
	}
	else
	{
		$sql = "select application_id
			   from m_application_group_allowed 
			   where 
			   group_id = $group_rowid
		";
	}

	my $application_rowid_list = SelectMultiple($dbh, $sql);

	if (!defined($application_rowid_list)) {
		throw Mioga2::Exception::Simple("Mioga2::AppDescList", __x("cannot get Application for group_rowid: {group_id}", group_id => $group_rowid));
	}

	foreach my $app_id (@$application_rowid_list)
	{
		push (@rowids, $app_id->{application_id});
	}
	$self->{list} = \@rowids;

}

# ============================================================================

=head2 InitializeFromGroupUserRowid ($group_rowid, $user_rowid, $app_type)

	Initialize the AppDescList object from with available apps for
	user $user_rowid in group $group_rowid

	$app_type is optionnal

=cut

# ============================================================================

sub InitializeFromGroupUserRowid {
	my ($self, $group_rowid, $user_rowid, $app_type) = @_;
	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my @rowids;

	my $sql1;
	my $sql2;
	if ($app_type eq "normal" || $app_type eq "shell" || $app_type eq "library" )
	{
		$sql1 = "select distinct m_application.*
		        from m_application, m_instance_application, m_profile_user_function, m_application_type
			where
			      m_application.ident = m_profile_user_function.app_ident
			      and m_profile_user_function.group_id = $group_rowid
			      and m_profile_user_function.user_id = $user_rowid
			      and m_application.type_id = m_application_type.rowid 
			      and m_application_type.ident = '$app_type'
				  and m_application.rowid = m_instance_application.application_id
			      and m_instance_application.mioga_id = $mioga_id
		";
		$sql2 = "select distinct m_application.*
		        from m_application, m_instance_application, m_profile_expanded_user_functio, m_application_type
			where
			      m_application.ident = m_profile_expanded_user_functio.app_ident
			      and m_profile_expanded_user_functio.group_id = $group_rowid
			      and m_profile_expanded_user_functio.user_id = $user_rowid
			      and m_application.type_id = m_application_type.rowid 
			      and m_application_type.ident = '$app_type'
				  and m_application.rowid = m_instance_application.application_id
			      and m_instance_application.mioga_id = $mioga_id
		";
	}
	else
	{
		$sql1 = "select distinct m_application.*
		        from m_application, m_instance_application, m_profile_user_function
			where
			      m_application.ident = m_profile_user_function.app_ident
			      and m_profile_user_function.group_id = $group_rowid
			      and m_profile_user_function.user_id = $user_rowid
				  and m_application.rowid = m_instance_application.application_id
			      and m_instance_application.mioga_id = $mioga_id
		";
		$sql2 = "select distinct m_application.*
		        from m_application, m_instance_application, m_profile_expanded_user_functio
			where
			      m_application.ident = m_profile_expanded_user_functio.app_ident
			      and m_profile_expanded_user_functio.group_id = $group_rowid
			      and m_profile_expanded_user_functio.user_id = $user_rowid
				  and m_application.rowid = m_instance_application.application_id
			      and m_instance_application.mioga_id = $mioga_id
		";
	}

	my $application_rowid_list1 = SelectMultiple($dbh, $sql1);
	my $application_rowid_list2 = SelectMultiple($dbh, $sql2);

	if (!defined($application_rowid_list1)) {
		throw Mioga2::Exception::Simple("Mioga2::AppDescList", __x("cannot get Application for group_rowid: {group_id}", group_id => $group_rowid));
	}

	my @application_rowid_list = (@$application_rowid_list1, @$application_rowid_list2);
	foreach my $app_id (@application_rowid_list)
	{
		push (@rowids, $app_id->{rowid});
	}

	my %seen = ();
	foreach my $app_id (@rowids)
	{
		$seen{$app_id}++;
	}
	my @uniq_app_list = keys %seen;

	$self->{list} = \@uniq_app_list;

}

# ============================================================================

=head2 InitializeFromGroupIdent (@group_ident, $app_type)

	Initialize the AppDescList object from group_ident.

	$app_type is optionnal

=cut

# ============================================================================

sub InitializeFromGroupIdent {
	my ($self, $group_ident, $app_type) = @_;
	my $dbh = $self->{config}->GetDBH();
	my @rowids;

	my $sql;
	if ($app_type eq "normal" || $app_type eq "shell" || $app_type eq "library" )
	{
		$sql = "select m_application_group_allowed.application_id
			   from m_application_group_allowed, m_application_type, m_application, m_group 
			   where 
			   m_group.ident = '$group_ident' 
			   and m_application_group_allowed.group_id = m_group.rowid 
			   and m_application_group_allowed.application_id = m_application.rowid 
			   and m_application.type_id = m_application_type.rowid 
			   and m_application_type.ident = '$app_type'
		";
	}
	else
	{
		$sql = "select application_id
			   from m_application_group_allowed, m_group 
			   where 
			   m_application_group_allowed.group_id = m_group.rowid 
			   and m_group.ident = '$group_ident'
		";
	}


	my $application_rowid_list = SelectMultiple($dbh, $sql);

	if (!defined($application_rowid_list)) {
		throw Mioga2::Exception::Simple("Mioga2::AppDescList", __x("cannot get Application for group_ident: {group_ident}", group_ident => $group_ident));
	}

	foreach my $app_id (@$application_rowid_list)
	{
		push (@rowids, $app_id->{application_id});
	}
	$self->{list} = \@rowids;

}


# ============================================================================

=head2 InitializeFromMethodIdent ($func_ident)

	Initialize the AppDescList object from method_ident.

=cut

# ============================================================================

sub InitializeFromMethodIdent {
	my ($self, $method_ident, $user_id, $group_id, $app_type) = @_;
	my $dbh = $self->{config}->GetDBH();
	
	my $sql = "SELECT DISTINCT m_application.rowid 

               FROM   m_application, m_instance_application, m_method, m_function, m_profile_function, ".
			  "       m_profile, m_profile_group ";

	$sql   .= "     , m_application_type " if $app_type ne '';

    $sql   .= "WHERE  m_method.ident         = '$method_ident' AND
                      m_function.rowid       = m_method.function_id AND 
					  m_application.rowid    = m_instance_application.application_id AND
                      m_instance_application.mioga_id = ".$self->{config}->GetMiogaId()." AND 
                      m_application.rowid    = m_function.application_id AND ";
					  
	$sql   .= "       m_application_type.rowid = m_application.type_id AND ".
	          "       m_application_type.ident = '$app_type' AND " if $app_type ne '';
					  

    $sql   .= "       m_profile_function.function_id = m_function.rowid  AND
                      m_profile.rowid    = m_profile_function.profile_id AND
                      m_profile.group_id = $group_id AND 
                      m_profile_group.profile_id = m_profile.rowid AND 
                      ( m_profile_group.group_id = $user_id OR 
                        m_profile_group.group_id IN (
                               SELECT m_group.rowid

                               FROM m_group, m_group_group

                               WHERE  m_group_group.invited_group_id = $user_id AND 
		                              m_group.rowid = m_group_group.group_id
                        )
                      ) ";

	my $application_rowid_list = SelectMultiple($dbh, $sql);
	
	if (!defined($application_rowid_list)) {
		throw Mioga2::Exception::Simple("Mioga2::AppDescList", __x("cannot get Application with method: {method}", method => $method_ident));
	}
	
	
	my @rowids = map {$_->{rowid}} @$application_rowid_list;
	$self->{list} = \@rowids;
}

# ============================================================================

=head2 GetAttributesForRowids ($request_result)

	Returns list of attributes $attributes according to list of couples (rowid, attr)

=cut

# ============================================================================

sub GetAttributesForRowids {
	my ($self, $attribute_list) = @_;

	my $rowid_to_res;

	foreach my $entry (@{$attribute_list})
	{
		$rowid_to_res->{$entry->{rowid}} = $entry->{attr};
	}

	my @attr_list;

	foreach my $rowid (@{$self->{list}})
	{
		push (@attr_list, $rowid_to_res->{$rowid});
	}

	return \@attr_list;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
