# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME

Mioga2::Forum - The Mioga2 Forum application

=cut

# ============================================================================

package Mioga2::Forum;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'forum';

use Mioga2::Forum::Database;
use Mioga2::Forum::WebServices;
use Mioga2::Forum::OldUI;

use Error qw(:try);
use POSIX qw(ceil);
use Mioga2::URI;
use Mioga2::Content::XSLT;
use Mioga2::Content::ASIS;
use Mioga2::tools::database;
use Mioga2::tools::args_checker;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Mioga2::tools::APIAuthz;
use Data::Dumper;
use XML::Atom::SimpleFeed;

my $debug = 0;

# ============================================================================

=head1 OTHER METHODS

=cut

# ============================================================================

# ============================================================================
# Public Methods
# ============================================================================
# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================
sub GetAppDesc
{
	my $self = shift;
    
    my %AppDesc = ( ident   => 'Forum',
	    	name	=> __('Forum'),
                package => 'Mioga2::Forum',
                description => __('The Mioga2 Forum Application'),
				type => 'normal',
                all_groups => 1,
                all_users  => 0,
                can_be_public => 0,
				is_user             => 0,
				is_group            => 1,
				is_resource         => 0,

                functions   => { 'Anim' => __('Administration Functions'),
								 'Moder' => __('Moderation functions'),
								 'Read'	=> __('Read functions'),
								 'Write' => __('Write functions'),
								 },

                func_methods  => { 'Anim' => [ 'ProcessAnimAction', 'DeleteMessage', 'DeleteSubject', 'SetThematic', 'DeleteThematic', 'SetCategory', 'DeleteCategory' ],
								   'Read'   => [ 'DisplayMain', 'DisplayThematic', 'DisplaySubject', 'DisplayNewMessages', 
										'ProcessMainAction', 'ProcessViewAction', 'GetCategories', 'GetCategory', 'GetThematics', 'GetThematic', 'GetSubjects', 'GetSubject', 'GetMessages', 'GetMessage' ],
								   'Moder' => [ ],
								   'Write' => [ 'SetMessage', 'SetSubject' ],
                                 },

				sxml_methods => {},

				xml_methods  => {},

				non_sensitive_methods => [
					'DisplayMain',
					'DisplayThematic',
					'DisplaySubject',
					'DisplayNewMessages',
					'ProcessMainAction',
					'ProcessViewAction',
					'GetCategories',
					'GetCategory',
					'GetThematics',
					'GetThematic',
					'GetSubjects',
					'GetSubject',
					'GetMessages',
					'GetMessage',
					'SetMessage',
				]

                );
    
	return \%AppDesc;
}


#===============================================================================

=head2 InitApp

Initialize inner data from Mioga2::RequestContext

=cut

#===============================================================================
sub InitApp {
	my ($self, $context) = @_;

	$self->DBGetForumInfo ($context->GetConfig ()->GetDBH (), $context->GetGroup ()->GetRowid ());
}	# ----------  end of subroutine InitApp  ----------

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application Mioga2::Forum::WebServices Mioga2::Forum::Database Mioga2::Forum::OldUI

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


