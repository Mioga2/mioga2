# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
LDAP.pm: Access class to the LDAP Directory.

=head1 DESCRIPTION

This module permits to search User into the LDAP Directory.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
package Mioga2::LDAP;
use strict;

use Locale::TextDomain::UTF8 'ldap';

use Error qw(:try);
use Data::Dumper;
use Mioga2::Exception::Simple;
use Mioga2::Exception::LDAP;
use Mioga2::tools::string_utils;

use Net::LDAP;

my $debug = 0;

my %miogaLdap;
# ============================================================================

=head2 new ($config)

	Create a new LDAP object.

=cut

# ============================================================================

sub new {
	my ($class, $config) = @_;

	if (!defined($config)) {
		throw Mioga2::Exception::Simple("Mioga2::LDAP::new", __"Missing parameters.");
	}

	my $self = {};
	bless($self, $class);
	$self->{config} = $config;
	$self->{ident} = $config->{ident};
	$self->ConnectLDAP($config);
	

	return $self;
}
# ============================================================================

=head2 ConnectLDAP ()

	Establish the LDAP connection.
	
=cut

# ============================================================================

sub ConnectLDAP {
	my ($self, $config) = @_;

	print STDERR "LDAP::ConnectLDAP\n" if($debug);
	# ------------------------------------------------------
	# Check if the ldap object for Mioga2::Config  is
	# cached.
	# If not, create a new object.
	# ------------------------------------------------------
	if( exists $miogaLdap{$self->{ident}}) {
		print STDERR "LDAP::ConnectLDAP reusing existing connection\n" if ($debug);
		$self->{ldap} =	$miogaLdap{$self->{ident}};
		return;
	}
   
	# ------------------------------------------------------
	# Connect to LDAP Directory
	# ------------------------------------------------------

	my $netldap = new Net::LDAP($config->GetLDAPHost(), port => $config->GetLDAPPort(), debug => $debug, raw => qr/(?i:^jpegPhoto|;binary)/);

	if(!defined $netldap) {
		throw Mioga2::Exception::LDAP("Mioga2::LDAP::ConnectLDAP", "Connection Failed");
	}

	print STDERR "LDAP::ConnectLDAP connected\n" if ($debug);

	$miogaLdap{$self->{ident}} = $netldap;
	$self->{ldap} = $netldap;
}
# ============================================================================

=head2  BindWith($dn, $passwd)

	Try to bind to the server with given dn and password.

=cut

# ============================================================================
sub BindWith {
	my ($self, $dn, $passwd) = @_;

	print STDERR "LDAP::BindWidth dn = $dn  passwd = $passwd\n" if ($debug);

	my $mesg =	$self->{ldap}->bind($dn, password => $passwd);

	if($mesg->is_error()) {
		if ($mesg->error () eq 'Unexpected EOF') {
			print STDERR "[Mioga2::LDAP::BindWidth] LDAP connection lost, reconnecting.\n";
			delete ($miogaLdap{$self->{ident}});
			$self->ConnectLDAP ($self->{config});
		}
		else {
			throw Mioga2::Exception::LDAP("Mioga2::LDAP::BindWith", __x("LDAP Directory login failed: {error}", error => $mesg->error));
		}
	}

	print STDERR "LDAP::BindWidth successfully bound\n" if ($debug);

	return;
}



# ============================================================================

=head2  SearchUsers($search_filter)

B<TO BE DEPRECATED>

Search users in LDAP Directory with the filter parameters and
configuration parameter.

=over

=item B<Parameters:>

=over

=item I<$search_filter:> A hash containing the search parameters (LDAP attribute => expected value).

=item I<$search_filter{operator}:> (optional) indicates the operator ('|' or '&'), defaults to '&'.

=item I<$search_filter{use_instance_user_filter}:> (optional) Indicates if the instance-configured LDAP filter should be used in conjunction to the provided filter. Defaults to 1.

=back

=item B<Return:> An array of matches.

=back

=cut

# ============================================================================

sub SearchUsers {
	my ($self, %search_filter) = @_;

	if(! $self->{config}->{use_ldap}) {
		return [];
	}

	$search_filter{use_instance_user_filter} = 1 unless (defined $search_filter{use_instance_user_filter});

	print STDERR "Mioga2::LDAP::SearchUsers Filter: " . Dumper \%search_filter if ($debug);

	my $filter;

	# No values = empty hash in XML::Simple
	if((ref($self->{config}->{ldap_user_filter}) ne 'HASH') && ($search_filter{use_instance_user_filter} == 1)) {
		$filter .= '(& ';
	}

	if (%search_filter) {
		$filter .= '(' . ($search_filter{operator} ? $search_filter{operator} : '&');

		while (my ($key, $value) = each (%search_filter)) {
			next if $key eq 'operator';
			next if $key eq 'use_instance_user_filter';
			$filter .= " ($key=$value)";
		}
		$filter .= ') ';
	}

	# No values = empty hash in XML::Simple
	if((ref($self->{config}->{ldap_user_filter}) ne 'HASH') && ($search_filter{use_instance_user_filter} == 1)) {
		$filter .= "$self->{config}->{ldap_user_filter}";
		$filter .= ')';
	}


	print STDERR "Mioga2::LDAP::SearchUsers LDAP filter: $filter\n" if ($debug);

	$self->BindWith($self->{config}->{ldap_bind_dn}, $self->{config}->{ldap_bind_pwd});
	my $mesg = $self->{ldap}->search ( base   => $self->{config}->{ldap_user_base_dn},
	                                   filter => $filter,
	                                   scope  => $self->{config}->{ldap_user_scope});

	if($mesg->is_error) {
		throw Mioga2::Exception::LDAP("Mioga2::LDAP::SearchUsers",
									  $mesg->error, $filter);
	}
	
	print STDERR "Mioga2::LDAP::SearchUsers Search successfull, results: " . Dumper $mesg->all_entries . "\n" if ($debug);
	
	# ------------------------------------------------------
	# For each LDAP entry, map ldap attributes to internal 
	# Mioga user description.
	# ------------------------------------------------------
	
	my @users;
	foreach my $entry ($mesg->all_entries) {
		try {
			my $user = $self->CheckUserEntry($entry);
			push @users, $user;
		}
		catch Mioga2::Exception::Simple with {
			warn "Skipping bad LDAP entry : ".$entry->dn;
		};
	}

	print STDERR "Mioga2::LDAP::SearchUsers Users: " . Dumper \@users if ($debug);
	return \@users;
}



# ============================================================================

=head2  SearchSingleUser($user_dn)

B<TO BE DEPRECATED>

	Search user in LDAP Directory with given dn.

=cut

# ============================================================================

sub SearchSingleUser {
	my ($self, $user_dn) = @_;

	print STDERR "LDAP::SearchSingleUser searching for user dn: $user_dn\n" if ($debug);


	if(! $self->{config}->{use_ldap}) {
		return ;
	}
	
	my $filter;

	# No values = empty hash in XML::Simple	
	if(ref($self->{config}->{ldap_user_filter}) eq 'HASH') {
		$filter = "()";
	}
	else {
		$filter = "$self->{config}->{ldap_user_filter}";
	}

	$self->BindWith($self->{config}->{ldap_bind_dn}, $self->{config}->{ldap_bind_pwd});
	my $mesg = $self->{ldap}->search ( base   => $user_dn, filter => '(objectclass=*)', scope  => "sub");
	

	if($mesg->is_error) {
		throw Mioga2::Exception::LDAP("Mioga2::LDAP::SearchSingleUser", $mesg->error);
	}

	print STDERR "Mioga2::LDAP::SearchSingleUser search successfull, result: " . Dumper $mesg->entry(0) if ($debug);
	
	if($mesg->count == 0) {
		return undef;
	}
	# ------------------------------------------------------
	# For each LDAP entry, map ldap attributes to internal 
	# Mioga user description.
	# ------------------------------------------------------
	my $entry = $mesg->entry(0);

	return $self->CheckUserEntry($entry);
}
# ============================================================================

=head2  CheckUserEntry($ldap_entry)

B<TO BE DEPRECATED>

	Check if all the user parameter required by Mioga are defined in ldap 
	entry, and map the ldap attributes to internal Mioga user description.

=cut

# ============================================================================

sub CheckUserEntry {
	my ($self, $entry) = @_;
	print STDERR "LDAP::CheckUserEntry, entry: " . Dumper $entry if ($debug);
	
	my $user = {};
	$user->{dn}          = $entry->dn;
	$user->{ident}       = $entry->get_value($self->{config}->{ldap_user_ident_attr}) if ($entry->get_value($self->{config}->{ldap_user_ident_attr}));
	$user->{lastname}    = $entry->get_value($self->{config}->{ldap_user_lastname_attr}) if ($entry->get_value($self->{config}->{ldap_user_lastname_attr}));
	$user->{firstname}   = $entry->get_value($self->{config}->{ldap_user_firstname_attr}) if ($entry->get_value($self->{config}->{ldap_user_firstname_attr}));
	$user->{email}       = $entry->get_value($self->{config}->{ldap_user_email_attr}) if ($entry->get_value($self->{config}->{ldap_user_email_attr}));
	$user->{description} = $entry->get_value('description') if ($entry->get_value('description'));
	push (@{$user->{instances}}, $entry->get_value($self->{config}->{ldap_user_inst_attr}));
	#$user->{password}  = st_Utf8ToISO($entry->get_value($self->{config}->{ldap_user_pwd_attr}));
	$user->{password}  = $entry->get_value($self->{config}->{ldap_user_pwd_attr}) if ($entry->get_value($self->{config}->{ldap_user_pwd_attr}));
	
	# ------------------------------------------------------
	# Check if all the parameters are defined
	# ------------------------------------------------------
	#my @missing_params = grep {!defined $user->{$_} or $user->{$_} eq ''} qw(ident lastname firstname email password);
	my @missing_params = grep {!defined $user->{$_} or $user->{$_} eq ''} qw(ident lastname firstname email);
	if(@missing_params ) {
		throw Mioga2::Exception::Simple("Mioga2::LDAP::CheckUserEntry", __x("bad entry: missing arguments ({args}) for user {user}.", args => join(" ", @missing_params), user => $entry->dn));
	}

	print STDERR "LDAP::CheckUserEntry, user entry is correct\n" if ($debug);

	return $user;
}

# ============================================================================

=head2 AddUser ($user)

B<TO BE DEPRECATED>

Create a new user into LDAP.

=over

=item B<Parameters:>

=over

=item I<$user{dn}:> The user's DN.

=item I<$user{givenName}:> The user's firstname.

=item I<$user{sn}:> The user's lastname.

=item I<$user{mail}:> The user's e-mail.

=item I<$user{description}:> A text description that will be hiddent to users.

=item I<$user{ou}:> An array containing the names of Mioga Instances the user is affected to.

=back

=back

=cut

# ============================================================================
sub AddUser {
	my ($self, $user) = @_;

	print STDERR "Mioga2::LDAP::AddUser\n" if ($debug);
	print STDERR Dumper ($user) if ($debug);

	if (!$self->{config}->CanModifyLdapPassword ()) {
		throw Mioga2::Exception::LDAP ("Mioga2::LDAP::AddUser", "Insufficient LDAP directory access level for instance");
	}

	$user->{objectClass} = ['top', 'inetOrgPerson'];

	if ($user->{description} eq '') {
		delete ($user->{description});
	}

	print STDERR "Mioga2::LDAP::AddUser binding\n" if ($debug);
	$self->BindWith($self->{config}->{ldap_bind_dn}, $self->{config}->{ldap_bind_pwd});
	my $result = $self->{ldap}->add ( delete ($user->{dn}),
		attr => [
			%{$user}
		]
	);
	print STDERR "Mioga2::LDAP::AddUser successfully bound\n" if ($debug);

	if ($result->code) {
		throw Mioga2::Exception::LDAP ("Mioga2::LDAP::AddUser", $result->error);
	}

	print STDERR "Mioga2::LDAP::AddUser user added\n" if ($debug);
}

# ============================================================================

=head2 ModifyUser ($dn, $fields)

B<TO BE DEPRECATED>

Modify a user.

=over

=item B<Parameters:>

=over

=item I<$dn:> The user's DN.

=item I<$fields:> A hash containing the fields to modify (LDAP attribute => value).

=back

=back

=cut

# ============================================================================
sub ModifyUser {
	my ($self, $dn, $fields) = @_;

	print STDERR "Mioga2::LDAP::ModifyUser ($dn), fields: " . Dumper $fields if ($debug);

	if ((!$self->{config}->CanModifyLdapPassword ()) && (!$self->{config}->CanModifyLdapInstance ())) {
		throw Mioga2::Exception::LDAP ("Mioga2::LDAP::AddUser", "Insufficient LDAP directory access level for instance");
	}

	my $ldap_user = $self->TranslateAttributes ($self->SearchSingleUser ($dn));

	# $instance_attribute used as a flag. undef if full access to directory, contains instance affectation attribute name if directory access is restricted to affectation
	# will be used later-on to filter attributes to be modified into directory (see below)
	my $instance_attribute;
	if (!$self->{config}->CanModifyLdapPassword ()) {
		# LDAP access only allowed for instance affectation
		my $table = $self->{config}->GetLDAPTranslationTable ();
		$instance_attribute = $table->{inst};
	}

	my @add     = ();
	my @replace = ();
	my @delete  = ();

	for (keys(%$fields)) {
		next if ($fields->{$_} eq $ldap_user->{$_});

		# Ensure only instance field is modified if access is insufficient
		if ($instance_attribute && ($_ ne $instance_attribute)) {
			throw Mioga2::Exception::LDAP ("Mioga2::LDAP::AddUser", "Insufficient LDAP directory access level for instance");
		}

		if (($fields->{$_} && $ldap_user->{$_}) || ($_ eq 'cn')) {
			push (@replace, $_, $fields->{$_});
		}
		elsif ($ldap_user->{$_}) {
			push (@delete, $_);
		}
		else {
			push (@add, $_, $fields->{$_}) if ($fields->{$_} ne '');
		}
	}

	print STDERR "Mioga2::LDAP::ModifyUser binding\n" if ($debug);
	$self->BindWith($self->{config}->{ldap_bind_dn}, $self->{config}->{ldap_bind_pwd});
	print STDERR "Mioga2::LDAP::ModifyUser successfully bound\n" if ($debug);

	my $result = $self->{ldap}->modify ( $dn,
		add     => \@add,
		replace => \@replace,
		delete  => \@delete,
	);

	if ($result->code) {
		throw Mioga2::Exception::LDAP ("Mioga2::LDAP::ModifyUser", $result->error);
	}

	print STDERR "Mioga2::LDAP::ModifyUser modification successfull\n" if ($debug);
}

# ============================================================================

=head2 DeleteUser ($dn)

B<TO BE DEPRECATED>

Delete a user.

=over

=item B<Parameters:>

=over

=item I<$dn:> The user's DN.

=back

=item B<Return:> true if success, false otherwise.

=back

=cut

# ============================================================================
sub DeleteUser {
	my ($self, $dn) = @_;

	print STDERR "Mioga2::LDAP::DeleteUser, dn: $dn\n" if ($debug);

	if (!$self->{config}->CanModifyLdapPassword ()) {
		throw Mioga2::Exception::LDAP ("Mioga2::LDAP::DeleteUser", "Insufficient LDAP directory access level for instance");
	}

	print STDERR "Mioga2::LDAP::DeleteUser binding\n" if ($debug);
	$self->BindWith($self->{config}->{ldap_bind_dn}, $self->{config}->{ldap_bind_pwd});
	print STDERR "Mioga2::LDAP::DeleteUser successfully bound\n" if ($debug);

	my $result = $self->{ldap}->delete ($dn);

	if ($result->code) {
		throw Mioga2::Exception::LDAP ("Mioga2::LDAP::DeleteUser", $result->error);
	}

	print STDERR "Mioga2::LDAP::DeleteUser deletion successfull\n" if ($debug);
}


# ============================================================================

=head2 TranslateAttributes ($values)

Translate internal Mioga attributes to LDAP attributes

=over

=item B<Parameters:>

=over

=item I<$values:> A hash containing user's attributes in "Mioga language".

=back

=item B<Return:> A hash containing the user's attributes in "LDAP language".

=back

=cut

# ============================================================================
sub TranslateAttributes {
	my ($self, $values) = @_;
	
	my $params;
	
	my %convert = (
		'dn' => 'dn',
		'cn' => 'cn',
		'ident' => $self->{config}->{ldap_user_ident_attr},
		'lastname' => $self->{config}->{ldap_user_lastname_attr},
		'firstname' => $self->{config}->{ldap_user_firstname_attr},
		'email' => $self->{config}->{ldap_user_email_attr},
		'description' => 'description',
		'instances' => $self->{config}->{ldap_user_inst_attr},
		'password' => $self->{config}->{ldap_user_pwd_attr}
	);
	for (keys (%{$values})) {
		$params->{$convert{$_}} = $values->{$_} if ($convert{$_});
	}
	return ($params);
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Old::User Mioga2::Old::Group Mioga2::Old::Resource Mioga2::Config

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
