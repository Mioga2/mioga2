# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Tasks.pm: The Mioga2 flexible tasks application.

=head1 METHODS DESRIPTION

=cut

package Mioga2::Tasks;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'tasks';

use Error qw(:try);
use Data::Dumper;
use POSIX qw(strftime);
use Mioga2::Content::XSLT;
use Mioga2::SimpleLargeList;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;

my $debug = 0;

# ============================================================================
# Current user tasks list description
# ============================================================================

my $time_in_seconds = " CASE 
	when load_unit=0 then load_value 
	when load_unit=1 then 60*load_value 
	when load_unit=2 then 3600*load_value 
	when load_unit=3 then 3600*24*load_value 
	end as time_in_seconds ";

my %cur_user_tasks_sll_desc = ( 'name' => 'current_user_tasks_sll',

                        'sql_request' => {
                            'select' => "org_flexible_task.*, $time_in_seconds, task_category.bgcolor, task_category.fgcolor, task_category.private",
       
                            "select_count" => "count(org_flexible_task.rowid)",
       
                            "from"   => "org_flexible_task, org_task_type, task_category",
       
                            "where"  => "org_flexible_task.delegate_id = ? AND ".
                                        "org_flexible_task.type_id = org_task_type.rowid AND ".
                                        "org_task_type.ident = 'flexible' AND ".
					"task_category.rowid = org_flexible_task.category_id ",


                            "delete" => { "from"  => "org_flexible_task",
                                          "where" => 'org_flexible_task.rowid = $_->{rowid}',
                                        },
                         },
                        
                        'fields' => [ [ 'rowid', 'rowid'],
                                      [ 'name', 'normal'],
                                      [ 'creator', 'user_name', 'org_flexible_task.creator_id' ],
                                      [ 'start', "normal" ],
                                      [ 'stop', "normal" ],
									  [ 'load_value', 'normal'],
									  [ 'progress', 'normal'],
                                      [ 'modify', 'modify', 'EditTask', \&CheckWriteAccess],
                                      [ 'delete', 'delete', \&CheckWriteAccess],
                                      [ 'category_id', "hidden" ],
                                      [ 'start_constraint', "hidden" ],
                                      [ 'stop_constraint', "hidden" ],
									  [ 'time_in_seconds', 'hidden'],
									  [ 'load_unit', 'hidden'],
									  [ 'bgcolor', "hidden" ],
									  [ 'fgcolor', "hidden" ],
									  [ 'private', "hidden" ],
                                  ],
 
                        'search' => ['name', 'creator'],
                      
                       );


# ============================================================================
# Delegated tasks list description
# ============================================================================

my %delegated_tasks_sll_desc = ( 'name' => 'delegated_tasks_sll',

								 'sql_request' => {
									 'select' => "org_flexible_task.*, task_category.bgcolor, task_category.fgcolor, task_category.private",
									 
									 "select_count" => "count(org_flexible_task.rowid)",
									 
									 "from"   => "org_flexible_task, org_task_type, task_category",
									 
									 "where"  => "org_flexible_task.creator_id = ? AND ".
										         "org_flexible_task.delegate_id != org_flexible_task.creator_id AND ".
										         "org_flexible_task.type_id = org_task_type.rowid AND ".
												 "org_task_type.ident = 'flexible' AND ".
												 "task_category.rowid = org_flexible_task.category_id",
										 
												 "delete" => { "from"  => "org_flexible_task",
															   "where" => 'org_flexible_task.rowid = $_->{rowid}',
														   },
											 },
								 
								 'fields' => [ [ 'rowid', 'rowid'],
											   [ 'name', 'normal'],
											   [ 'delegate', 'user_name', 'org_flexible_task.delegate_id' ],
											   [ 'start', "normal" ],
											   [ 'stop', "normal" ],
											   [ 'load_value', 'normal'],
											   [ 'progress', 'normal'],
											   [ 'modify', 'modify', 'EditTask', \&CheckWriteAccess],
											   [ 'delete', 'delete', \&CheckWriteAccess],
											   [ 'category_id', "hidden" ],
											   [ 'start_constraint', "hidden" ],
											   [ 'stop_constraint', "hidden" ],
											   [ 'load_unit', 'hidden'],
											   [ 'bgcolor', "hidden" ],
											   [ 'fgcolor', "hidden" ],
											   [ 'private', "hidden" ],
											   ],
								 
								 'search' => ['name', 'delegate'],
								 
								 );



# ============================================================================
# Current group tasks list description
# ============================================================================

my %cur_group_tasks_sll_desc = ( 'name' => 'cur_group_tasks_sll',

								 'sql_request' => {
									 'select' => "org_flexible_task.*, task_category.bgcolor, task_category.fgcolor, task_category.private",
									 
									 "select_count" => "count(org_flexible_task.rowid)",
									 
									 "from"   => "org_flexible_task, org_task_type, task_category",
									 
									 "where"  => "org_flexible_task.group_id = ? AND ".
										         "org_flexible_task.type_id = org_task_type.rowid AND ".
												 "org_task_type.ident = 'flexible' AND ".
												 "task_category.rowid = org_flexible_task.category_id",
										 
												 "delete" => { "from"  => "org_flexible_task",
															   "where" => 'org_flexible_task.rowid = $_->{rowid}',
														   },
											 },
								 
								 'fields' => [ [ 'rowid', 'rowid'],
											   [ 'name', 'normal'],
											   [ 'creator', 'user_name', 'org_flexible_task.creator_id' ],
											   [ 'delegate', 'user_name', 'org_flexible_task.delegate_id' ],
											   [ 'start', "normal" ],
											   [ 'stop', "normal" ],
											   [ 'load_value', 'normal'],
											   [ 'progress', 'normal'],
											   [ 'modify', 'modify', 'EditTask', \&CheckWriteAccess],
											   [ 'delete', 'delete', \&CheckWriteAccess],
											   [ 'start_constraint', "hidden" ],
											   [ 'stop_constraint', "hidden" ],
											   [ 'load_unit', 'hidden'],
											   [ 'bgcolor', "hidden" ],
											   [ 'fgcolor', "hidden" ],
											   [ 'private', "hidden" ],
											   ],
								 
								 'search' => ['name', 'creator', 'delegate'],
								 
								 );



# ============================================================================

=head2 GetAppDesc ()

	Return the current application description.

=cut

# ============================================================================

sub GetAppDesc {
	my ($self, $context) = @_;

    my %AppDesc = ( ident   => 'Tasks',
	    	name	=> __('Tasks'),
                package => 'Mioga2::Tasks',
                description => __('The Mioga2 Flexible tasks manager'),
				type    => 'normal',
                all_groups => 1,
                all_users  => 1,
                can_be_public => 0,
				is_user             => 1,
				is_group            => 1,
				is_resource         => 0,

				functions => {
					'Write' => __('Creation, modification and deletion functions'),
					'Read'  => __('Consultation functions'),
				},

				func_methods => {
					
					Write => [ 'EditTask',
							  ],
					Read  => [
							   'DisplayMain', 'UserTasks', 'GroupTasks', 'DelegatedTasks', 'DisplayTask'
							  ],
					},
				
				sxml_methods => { },
				
				xml_methods  => { },				

				non_sensitive_methods => [
						'DisplayMain',
						'UserTasks',
						'GroupTasks',
						'DelegatedTasks',
						'DisplayTask',
						'EditTask',
					]
				
                );
    
	return \%AppDesc;
}



# ============================================================================

=head2 DisplayMain ()

	Main Mioga entry point.

=cut

# ============================================================================

sub DisplayMain {
	my ($self, $context) = @_;
	
	my $session = $context->GetSession ();

	my $group = $context->GetGroup();
	
	my ($tmpxml, $group_tasks_sll, $user_tasks_sll);
	if($group->GetType eq 'group') {
		$group_tasks_sll = new Mioga2::SimpleLargeList($context, \%cur_group_tasks_sll_desc);
		$tmpxml = $group_tasks_sll->Run($context, [$group->GetRowid()]);
	}
	else {
		$user_tasks_sll = new Mioga2::SimpleLargeList($context, \%cur_user_tasks_sll_desc);
		$tmpxml = $user_tasks_sll->Run($context, [$group->GetRowid()]);
	}
	if (!defined ($session->{__internal}->{message}) && !defined ($tmpxml)) {
		# $sll->Run returned nothing (display success message)
		if (st_ArgExists($context, 'sll_delete_act')) {
			$session->{__internal}->{message}{text} = __('Tasks successfully deleted.');
			$session->{__internal}->{message}{type} = 'info';
		}
	}


	my $xml = '<?xml version="1.0"?>';
	$xml .= "<DisplayMain";
	if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
	}
	$xml .= ">";
	
	$xml .= $context->GetXML();

	if($group->GetType eq 'group') {
		$xml .= "<GroupTasks>";
		
		if (defined ($tmpxml)) {
			# $sll->Run returned something (confirmation page)
			$xml .= $tmpxml;
		}
		else {
			# $sll->Run returned nothing (display collection)
			$xml .= $group_tasks_sll->DisplayList ($context);
		}
		
		$xml .= "</GroupTasks>";
	}
	
	else {
		$xml .= "<UserTasks>";	   
		
		if (defined ($tmpxml)) {
			# $sll->Run returned something (confirmation page)
			$xml .= $tmpxml;
		}
		else {
			# $sll->Run returned nothing (display collection)
			$xml .= $user_tasks_sll->DisplayList ($context);
		}
		
		$xml .= "</UserTasks>";


		$xml .= "<DelegatedTasks>";	   
		
		my $del_tasks_sll = new Mioga2::SimpleLargeList($context, \%delegated_tasks_sll_desc);
		$xml .= $del_tasks_sll->Run($context, [$group->GetRowid()]);
		
		$xml .= "</DelegatedTasks>";
	}
	
	$xml .= "</DisplayMain>";
	
	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'tasks.xsl', locale_domain => 'tasks_xsl');
	$content->SetContent($xml);
	

	return $content;
}


# ============================================================================

=head2 UserTasks ()

	Handle UserTasks list actions

=cut

# ============================================================================

sub UserTasks {
	my ($self, $context) = @_;
	
	my $user_tasks_sll = new Mioga2::SimpleLargeList($context, \%cur_user_tasks_sll_desc);
	my $xml = $user_tasks_sll->Run($context, [$context->GetGroup()->GetRowid()]);

	my $session = $context->GetSession ();

	if(st_ArgExists($context, 'sll_delete_act')) {
		if (!defined ($xml)) {
			# $sll->Run returned nothing (display collection)
			$session->{__internal}->{message}{type} = 'info';
			$session->{__internal}->{message}{text} = __('Tasks successfully deleted.');
			my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
			$content->SetContent('DisplayMain');
			return $content;
		}

		$xml = '<?xml version="1.0"?>'.
			   "<UserTasks>$xml</UserTasks>";

		my $content = new Mioga2::Content::XSLT($context, stylesheet => 'tasks.xsl', locale_domain => 'tasks_xsl');
		$content->SetContent($xml);

		return $content;

	}
	else {
		delete $context->{args};
		$context->{args} = {};
	}

	return $self->DisplayMain($context);
}
	
	
# ============================================================================

=head2 GroupTasks ()

	Handle GroupTasks list actions

=cut

# ============================================================================

sub GroupTasks {
	my ($self, $context) = @_;
	
	my $group_tasks_sll = new Mioga2::SimpleLargeList($context, \%cur_group_tasks_sll_desc);
	my $xml = $group_tasks_sll->Run($context, [$context->GetGroup()->GetRowid()]);

	my $session = $context->GetSession ();

	if(st_ArgExists($context, 'sll_delete_act')) {
		if (!defined ($xml)) {
			# $sll->Run returned nothing (display collection)
			$session->{__internal}->{message}{type} = 'info';
			$session->{__internal}->{message}{text} = __('Tasks successfully deleted.');
			my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
			$content->SetContent('DisplayMain');
			return $content;
		}
	
		$xml = '<?xml version="1.0"?>'.
			   "<GroupTasks>$xml</GroupTasks>";
	
		my $content = new Mioga2::Content::XSLT($context, stylesheet => 'tasks.xsl', locale_domain => 'tasks_xsl');
		$content->SetContent($xml);
		
		return $content;
		
	}
	else {
		delete $context->{args};
		$context->{args} = {};
	}

	return $self->DisplayMain($context);
}
	
	

# ============================================================================

=head2 DelegatedTasks ()

	Handle DelegatedTasks list actions

=cut

# ============================================================================

sub DelegatedTasks {
	my ($self, $context) = @_;
	
	my $delegated_tasks_sll = new Mioga2::SimpleLargeList($context, \%delegated_tasks_sll_desc);
	my $xml = $delegated_tasks_sll->Run($context, [$context->GetGroup()->GetRowid()]);

	my $session = $context->GetSession ();

	if(st_ArgExists($context, 'sll_delete_act')) {
		if (!defined ($xml)) {
			# $sll->Run returned nothing (display collection)
			$session->{__internal}->{message}{type} = 'info';
			$session->{__internal}->{message}{text} = __('Tasks successfully deleted.');
			my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
			$content->SetContent('DisplayMain');
			return $content;
		}

		$xml = '<?xml version="1.0"?>'.
			   "<DelegatedTasks>$xml</DelegatedTasks>";
	
		my $content = new Mioga2::Content::XSLT($context, stylesheet => 'tasks.xsl', locale_domain => 'tasks_xsl');
		$content->SetContent($xml);
		
		return $content;
		
	}
	else {
		delete $context->{args};
		$context->{args} = {};
	}

	return $self->DisplayMain($context);
}
	
	

# ============================================================================

=head2 DisplayTask ()

	Display a task

=cut

# ============================================================================

sub DisplayTask {
	my ($self, $context) = @_;

	my $session = $context->GetSession();
	delete $session->{Tasks};
	
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'rowid' ], 'disallow_empty' ] ]);

	my $xml = '<?xml version="1.0"?>';
	$xml .= "<DisplayTask>";

	$xml .= $context->GetXML();

	if($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), 'Write')) {
		$xml .= "<CanWrite/>";
	}
	
	if(!@$errors) {
		$self->InitializeTaskValues($context, $values);

		$xml .= "<fields>";
		$xml .= ac_XMLifyArgs(['rowid', 'name', 'category_id', 'load_unit', 'start_constraint', 'stop_constraint',
							   'start_year', 'stop_year', 'start_month', 'stop_month', 'start_day', 'stop_day',
							   'progress', 'load_value', 'description', 'delegate_id', 'delegate_name', 
							   'group_ident', 'group_id'], 
							  $values );
#print STDERR "stop =".$stop."\n";

		foreach my $key (qw(start stop)) {
			if(defined $values->{$key} and $values->{$key} ne '') {
				$xml .= "<$key>";
				$xml .= du_ISOToXML($values->{$key});
				$xml .= "</$key>";
			}
		}

		$xml .= "</fields>";
	}
	
	my $org = new Mioga2::Organizer;
	$xml .= $org->GetXMLTaskCat($context);	
	
	$xml .= "<referer>".st_FormatXMLString($context->GetReferer())."</referer>";
	$xml .= "<escreferer>".st_FormatXMLString(st_URIEscape($context->GetReferer()))."</escreferer>";
	
	$xml .= ac_XMLifyErrors($errors);
	
	$xml .= "</DisplayTask>";

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'tasks.xsl', locale_domain => 'tasks_xsl');
	$content->SetContent($xml);

	return $content;
	
}
	
# ============================================================================

=head2 EditTask ()

	Task creation form.

=cut

# ============================================================================

sub EditTask {
	my ($self, $context) = @_;
	
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	
	my ($values, $errors) = ({}, []);	
	if(st_ArgExists($context, 'create_task_act')) {
		($values, $errors) = ac_CheckArgs($context, [ [ [ 'name', 'category_id', 'load_unit'], 'stripxws', 'disallow_empty'],
													  [ [ 'start_year', 'stop_year', 'start_month', 
														  'stop_month', 'start_day', 'stop_day',
 														  'start_constraint', 'stop_constraint',
														  'progress', 'load_value', 'description'], 'stripxws', 'allow_empty'],
													  [ ['X-CSRF-Token'], 'disallow_empty', ['csrf_token', [$context->GetConfig(), $context->GetUser()]] ],
													]);


		if(! @$errors) {

			$self->InitializeTaskValues($context, $values);

			
			if($values->{'start_day'} ne '' and $values->{'start_month'} ne '' and 
			   $values->{'start_year'} ne '') {
				$values->{start} = du_YMDInUserLocaleToISOGMT($values->{'start_year'}, $values->{'start_month'},$values->{'start_day'}, $user); 

				#$values->{start} = strftime("%Y-%m-%d",0, 0, 0,
					  					          #     $values->{'start_day'}, $values->{'start_month'}-1, 
										          #     $values->{'start_year'}-1900); 
			}

			if($values->{'stop_day'} ne '' and $values->{'stop_month'} ne '' and 
			   $values->{'stop_year'} ne '') {
				$values->{stop} = du_YMDInUserLocaleToISOGMT($values->{'stop_year'}, $values->{'stop_month'},$values->{'stop_day'}, $user); 
				#$values->{stop} = strftime("%Y-%m-%d",0, 0, 0,
				 						          #     $values->{'stop_day'}, $values->{'stop_month'}-1, 
										          #     $values->{'stop_year'}-1900); 
			}

			try {
				$self->TaskEditTask($context, $values);
			}

			catch Mioga2::Exception::DB with {
				my $error = shift;

				push @$errors, [$error->as_string(), ["__create__"]];
			};

			if(! @$errors) {
				my $action = (exists $session->{Tasks}->{values}->{rowid}) ? 'modify' : 'create';
				
				$session->{__internal}->{message}{type} = 'info';
				for ($action) {
					if (/create/) {
						$session->{__internal}->{message}{text} = __('Task successfully created.');
					}
					elsif (/modify/) {
						$session->{__internal}->{message}{text} = __('Task successfully modified.');
					}
				}
				my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
				$content->SetContent($session->{Tasks}->{referer});
				return $content;
			}
		}

	}

	elsif(st_ArgExists($context, 'select_group_act')) {
		return $self->LaunchSelectSingleGroup($context);
	}

	elsif(st_ArgExists($context, 'select_delegate_act')) {
		return $self->LaunchSelectSingleUser($context);
	}

	elsif(st_ArgExists($context, 'select_group_back')) {

		if(exists $context->{args}->{rowid}) {
			$values->{group_id} = $context->{args}->{rowid};
			delete $context->{args}->{rowid};
		}		
	}

	elsif(st_ArgExists($context, 'select_user_back')) {

		if(exists $context->{args}->{rowid}) {
			$values->{delegate_id} = $context->{args}->{rowid};
			delete $context->{args}->{rowid};
		}		
	}

	else {
		delete $session->{Tasks}->{values};
		if(exists $context->{args}->{referer}) {
			$session->{Tasks}->{referer} = $context->{args}->{referer};
		}
		else {
			$session->{Tasks}->{referer} = $context->GetReferer();
		}
	}


	$self->InitializeTaskValues($context, $values);

	my $xml = '<?xml version="1.0"?>';
	$xml .= "<EditTask>";

	$xml .= $context->GetXML();
	
	$xml .= "<fields>";
	$xml .= ac_XMLifyArgs(['rowid', 'name', 'category_id', 'load_unit', 'start_constraint', 'stop_constraint',
						   'start_year', 'stop_year', 'start_month', 'stop_month', 'start_day', 'stop_day',
						   'progress', 'load_value', 'description', 'delegate_id', 'delegate_name', 
						   'group_ident', 'group_id'], 
						  $values );

	$xml .= "</fields>";
	
	$xml .= "<referer>".st_FormatXMLString($session->{Tasks}->{referer})."</referer>";

	$xml .= ac_XMLifyErrors($errors);
	
	my $org = new Mioga2::Organizer;
	$xml .= $org->GetXMLTaskCat($context);

	$xml .= $self->GetXMLLoadUnits();

	$xml .= $self->GetXMLTimeConstraints();


	$xml .= "<FirstYear>".(strftime("%Y", localtime()) - 1)."</FirstYear>";

	if($values->{group_id} == $context->GetUser()->GetRowid()) {
		$xml .= "<IsUser>1</IsUser>";
	}
	else {
		$xml .= "<IsUser>0</IsUser>";
	}

	$xml .= "</EditTask>";

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'tasks.xsl', locale_domain => 'tasks_xsl');
	$content->SetContent($xml);

	return $content;
}



# ============================================================================

=head1 DATABASE ACCESS METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 RevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

	Remove user data in database when a user is revoked of a group.

	This function is called when :
	- a user is revoked of a group
	- a team is revoked of a group for each team member not namly
	  invited in group
	- a user is revoked of a team  for each group where the team is
	  member and the user not namly invited in group

=cut

# ============================================================================

sub RevokeUserFromGroup {
	my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;

	my $dbh = $config->GetDBH();
	ExecSQL($dbh, "UPDATE org_flexible_task SET creator_id = $group_animator_id WHERE group_id = $group_id AND creator_id = $user_id");
	ExecSQL($dbh, "UPDATE org_flexible_task SET delegate_id = creator_id WHERE group_id = $group_id AND delegate_id = $user_id");

}


# ============================================================================

=head2 TaskCreateTask ()

	Add a task into the database

=cut

# ============================================================================

sub TaskCreateTask {
	my ($self, $context, $values) = @_;

	my $config = $context->GetConfig();
	my $dbh = $config->GetDBH();

	my $res = SelectSingle($dbh, "SELECT * FROM org_task_type WHERE ident='flexible'");

	$values->{type_id} = $res->{rowid};
	$values->{created} = 'now()';
	$values->{modified} = 'now()';
	$values->{creator_id} = $context->GetUser()->GetRowid();

	foreach my $key (keys %$values) {
		if(!defined $values->{$key} or
		   $values->{$key} eq '') {
			delete $values->{$key};
		}
	}

	my $sql = BuildInsertRequest($values, table  => 'org_flexible_task', 
								          string => ['start', 'stop', 'name', 'description'],
								          other  => ['created', 'modified', 'type_id', 'group_id', 
													 'category_id', 'creator_id', 'delegate_id', 
													 'start_constraint', 'stop_constraint',
													 'load_value', 'load_unit', 'progress'] );

	ExecSQL($dbh, $sql);
}


# ============================================================================

=head2 TaskModifyTask ()

	Modify a task into the database

=cut

# ============================================================================

sub TaskModifyTask {
	my ($self, $context, $rowid, $values) = @_;

	my $config = $context->GetConfig();
	my $dbh = $config->GetDBH();

	$values->{modified} = 'now()';

	my $sql = BuildUpdateRequest($values, table  => 'org_flexible_task', 
								          string => ['start', 'stop', 'name', 'description'],
								          other  => ['modified', 'group_id', 
													 'category_id', 'delegate_id', 
													 'start_constraint', 'stop_constraint',
													 'load_value', 'load_unit', 'progress'] );

	ExecSQL($dbh, $sql." WHERE rowid=$rowid");
}


# ============================================================================

=head2 TaskGetTask ()

	Select a task in the database.

=cut

# ============================================================================

sub TaskGetTask {
	my ($self, $context, $rowid) = @_;

	my $config = $context->GetConfig();
	my $dbh = $config->GetDBH();


	return SelectSingle($dbh, "SELECT * FROM org_flexible_task WHERE rowid=$rowid");
}
# ============================================================================

=head2 GetExtendedTaskList ($context, $user_id, $minDate, $maxDate)

	Get all global tasks belonging to a user, within two date
	or not finished, if date are not fixed.

=cut

# ============================================================================

sub GetExtendedTaskList {
	my ($self, $context, $user_id, $minDate, $maxDate) = @_;

	my $config = $context->GetConfig();
	my $dbh = $config->GetDBH();

	my $tasks = SelectMultiple($dbh, "SELECT org_flexible_task.*, 'flexible' AS type, task_category.bgcolor, task_category.fgcolor, task_category.private ".
							         "FROM org_flexible_task, task_category"
							         . " WHERE org_flexible_task.delegate_id = $user_id"
										 . " AND task_category.rowid = org_flexible_task.category_id"
										 . " AND (org_flexible_task.stop >= '$minDate'"
										 			. " OR (org_flexible_task.progress < 100 AND org_flexible_task.stop_constraint != 1) )"
										 . " ORDER by org_flexible_task.start ASC");

	return $tasks;
}

# ============================================================================

=head2 TaskGetTaskList ($context, $user_id, $minDate, $maxDate)

	Select a task in the database.

=cut

# ============================================================================

sub TaskGetTaskList {
	my ($self, $context, $user_id, $minDate, $maxDate) = @_;

	my $config = $context->GetConfig();
	my $dbh = $config->GetDBH();

	my $tasks = SelectMultiple($dbh, "SELECT org_flexible_task.*, 'flexible' AS type, task_category.bgcolor, task_category.fgcolor, task_category.private ".
							         "FROM org_flexible_task, task_category ".
							         "WHERE org_flexible_task.delegate_id = $user_id AND ".
							         "      org_flexible_task.stop >= '$minDate' AND ". #org_flexible_task.start <= '$maxDate' AND ".
							         "      task_category.rowid = org_flexible_task.category_id");


	return $tasks;
}


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 CheckWriteAccess ()

	Check if the current user have write access on the given task.

=cut

# ============================================================================

sub CheckWriteAccess {
	my($context, $values) = @_;

	my $app = new Mioga2::Tasks;
	
	my $config = $context->GetConfig();


	my $group;
	
	try {
		$group = new Mioga2::Old::Group($config, rowid => $values->{group_id});
	}
	otherwise {
		my $err = shift;

		$group = new Mioga2::Old::User($config, rowid => $values->{group_id});
	};
	
	return $app->CheckUserAccessOnFunction($context, $context->GetUser(), $group, 'Write');
}


# ============================================================================

=head2 InitializeTaskValues ()

	Initialize Values with default values.
	Store values in session.

=cut

# ============================================================================

sub InitializeTaskValues {
	my($self, $context, $values) = @_;

	my $session = $context->GetSession();

	my $default = { 'name'        => '',
					'category_id' => '',
					'load_unit'   => '',
					'start_constraint' => 1,
					'stop_constraint' => 1,
					'start_day'   => "",
					'stop_day'    => "",
					'start_month' => '',
					'stop_month'  => '',
					'start_year'  => '',
					'stop_year'   => '',
					'progress'    => 0,
					'load_value'  => '',
					'load_unit'   => '',
					'description' => '',
					'delegate_id' => $context->GetUser()->GetRowid(),
					'group_id' => $context->GetGroup()->GetRowid(),
				};
		
	my $now      = du_SecondToISOInUserLocale(time, $context->GetUser());
	my $tomorrow = du_SecondToISOInUserLocale(time + 24*60*60, $context->GetUser());

	($default->{'start_year'}, $default->{'start_month'}, $default->{'start_day'}) = map {int($_)} split(/[- ]/, $now);
	($default->{'stop_year'}, $default->{'stop_month'}, $default->{'stop_day'}) = map {int($_)} split(/[- ]/, $tomorrow);
	

	if(exists $context->{args}->{rowid}) {
		$default = $self->TaskGetTask($context, $context->{args}->{rowid});
		($default->{'start_year'}, $default->{'start_month'}, $default->{'start_day'}) = map {int($_)} split(/[- ]/, $default->{start});
		($default->{'stop_year'}, $default->{'stop_month'}, $default->{'stop_day'}) = map {int($_)} split(/[- ]/, $default->{stop});
	}

	if(exists $session->{Tasks}->{values}->{rowid}) {
		$values->{rowid} = $session->{Tasks}->{values}->{rowid};
	}

	foreach my $key (keys %$default) {
		if(exists $context->{args}->{$key}) {
			$values->{$key} = $context->{args}->{$key};
		}

		elsif(exists $session->{Tasks}->{values}->{$key} and !exists $values->{$key}) {
			$values->{$key} = $session->{Tasks}->{values}->{$key};
		}

		elsif(!exists $values->{$key}) {
			$values->{$key} = $default->{$key};
		}
	}

	
	my $user = new Mioga2::Old::User($context->GetConfig(), rowid => $values->{delegate_id});
	$values->{'delegate_name'} = $user->GetName();


	if($values->{group_id} == $context->GetUser()->GetRowid()) {
		$values->{'group_ident'} = '__none__';
	}
	else {
		try {
			my $group = new Mioga2::Old::Group($context->GetConfig(), rowid => $values->{group_id});
			$values->{'group_ident'} = $group->GetIdent();
		}
		otherwise {
			$values->{'group_ident'} = '__none__';			
		};

	}

	$session->{Tasks}->{values} = $values;
}


# ============================================================================

=head2 GetXMLLoadUnits ()

	Return XML describing possible load units	

=cut

# ============================================================================

sub GetXMLLoadUnits {
	

	return "<LoadUnits>".
		   "<LoadUnit>1</LoadUnit>".
		   "<LoadUnit>2</LoadUnit>".
		   "<LoadUnit>3</LoadUnit>".
		   "</LoadUnits>";
}


# ============================================================================

=head2 GetXMLTimeConstraints ()

	Return XML describing possible time constraints

=cut

# ============================================================================

sub GetXMLTimeConstraints {
	

	return "<TimeConstraints>".
		   "<Constraint>0</Constraint>".
		   "<Constraint>1</Constraint>".
		   "<Constraint>2</Constraint>".
		   "<Constraint>3</Constraint>".
		   "</TimeConstraints>";
}



# ============================================================================

=head2 LaunchSelectSingleUser ()

	Launch the select single user member of the current group list

	Build Select application URI
	And return a REDIRECT content.

=cut

# ============================================================================

sub LaunchSelectSingleUser {
	my($self, $context) = @_;
	
	my $config  = $context->GetConfig();
	my $session  = $context->GetSession();

	foreach my $arg (keys %{$context->{args}}) {
		$session->{Tasks}->{values}->{$arg} = $context->{args}->{$arg};
	}


	my $group   = $context->GetGroup();
	my $cur_uri = $context->GetURI();
	
    my $uri = new Mioga2::URI($config, group  => $group->GetIdent(),
	                                   public => 0,
	                                   application => 'Select',
	                                   method => 'SelectSingleExtendedUserMemberOfGroup',
	                                   args   => { action  => 'select_user_back',
	                                               referer  => $cur_uri->GetURI(),
												   group_id => $session->{Tasks}->{values}->{group_id},
	                                               }
	                                   );
	
	my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	$content->SetContent($uri->GetURI());

	return $content;
}


# ============================================================================

=head2 LaunchSelectSingleGroup ()

	Launch the select single group that the current user is member

	Build Select application URI
	And return a REDIRECT content.

=cut

# ============================================================================

sub LaunchSelectSingleGroup {
	my($self, $context) = @_;
	
	my $config  = $context->GetConfig();
	my $session  = $context->GetSession();

	foreach my $arg (keys %{$context->{args}}) {
		$session->{Tasks}->{values}->{$arg} = $context->{args}->{$arg};
	}


	my $group   = $context->GetGroup();
	my $cur_uri = $context->GetURI();
	
    my $uri = new Mioga2::URI($config, group  => $group->GetIdent(),
	                                   public => 0,
	                                   application => 'Select',
	                                   method => 'SelectSingleGroupForUserWithAppliAndFunc',
	                                   args   => { action  => 'select_group_back',
	                                               referer => $cur_uri->GetURI(),
	                                               app_ident => 'Tasks',
	                                               func_ident => 'Write'
	                                               }
	                                   );
	
	my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	$content->SetContent($uri->GetURI());

	return $content;
}


# ============================================================================

=head2 TaskEditTask ()

	Commit values in database (create or update)

=cut

# ============================================================================

sub TaskEditTask {
	my ($self, $context, $values) = @_;
	my $session = $context->GetSession();


	foreach my $key (keys %$values) {
		if(!defined $values->{$key} or
		   $values->{$key} eq '') {
			delete $values->{$key};
		}
	}

	if(exists $session->{Tasks}->{values}->{rowid}) {
		$self->TaskModifyTask($context, $session->{Tasks}->{values}->{rowid}, $values);
	}
	
	else {
		$self->TaskCreateTask($context, $values);
	}
}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


