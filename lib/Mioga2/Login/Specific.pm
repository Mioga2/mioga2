#===============================================================================
#
#         FILE:  Specific.pm
#
#  DESCRIPTION:  Login module-specific methods.
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  14/09/2011 12:04
#===============================================================================
#
#  Copyright (c) year 2011, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Specific.pm: Login module-specific methods.

=head1 DESCRIPTION

	This module contains specific methods for login module.
	This file is meant to be overwritten by your own specificities.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Login;

use Data::Dumper;
use Error qw(:try);

my $debug = 0;

#===============================================================================

=head2 Initialize

Specific initializations

=cut

#===============================================================================
sub Initialize {
	my ($config) = @_;
}	# ----------  end of subroutine Initialize  ----------

#===============================================================================

=head2 GetLoginURI

Get login form URI

=cut

#===============================================================================
sub GetLoginURI {
	my ($miogaconf, $target) = @_;

	my $targetstr = (ref ($target) eq 'Mioga2::URI') ? $target->GetURI () : $target;

	my $uri = $miogaconf->GetBasePath () . $miogaconf->GetLoginURI () . '/DisplayMain?target=' . $targetstr;

	return ($uri);
}	# ----------  end of subroutine GetLoginURI  ----------


#===============================================================================

=head2 GetLogoutURI

Get logout URI

=cut

#===============================================================================
sub GetLogoutURI {
	my ($miogaconf) = @_;

	my $uri = $miogaconf->GetBasePath () . $miogaconf->GetLoginURI () . '/Logout';

	return ($uri);
}	# ----------  end of subroutine GetLogoutURI  ----------

#===============================================================================

=head2 CheckForToken

If a system, like CAS, can get a token from a given ticket in url args.

=head3 Incoming Arguments

=over

=item B<$config>: A Mioga2::Config object

=item B<$args>: A hash of args

=back

=head3 Return value

token if all OK

=back

=cut

#===============================================================================
sub CheckForToken {
	my ($config, $target, $args) = @_;

	return undef;
}	# ----------  end of subroutine CheckForToken  ----------

#===============================================================================

=head2 CheckRequest

Check request for any login-related contents coming to a non-login URL. This
method can be used, for example, to process CAS Single-Sign-Out requests.

=head3 Incoming Arguments

=over

=item B<$config>: A Mioga2::Config object

=item B<$r>: A Apache2::RequestRec object

=back

=head3 Return value

None

=back

=cut

#===============================================================================
sub CheckRequest {
	my ($config, $r) = @_;

	return ();
}	# ----------  end of subroutine CheckRequest  ----------


1;

