#===============================================================================
#
#         FILE:  Contact.pm
#
#  DESCRIPTION:  Mioga2::Login contact feature functions
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  01/02/2012 09:59
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME
	
Contact.pm: Mioga2::Login contact feature functions

=head1 DESCRIPTION

These functions are used by Mioga2::Login to provide contact (e-mail) features.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Login;

use Mioga2::Captcha;
use Data::Dumper;

my $debug = 0;


#===============================================================================

=head2 ContactForm

Display login form

=cut

#===============================================================================
sub ContactForm {
	my ($context) = @_;
	print STDERR "[Mioga2::Login::ContactForm] Entering\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'target' ], 'disallow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "[Mioga2::Login::ContactForm] Args: " . Dumper $values if ($debug);

	if (@$errors) {
		throw Mioga2::Exception::Simple ('Mioga2::Login::ContactForm', '[Mioga2::Login::ContactForm] Incorrect argument value: ' . Dumper $errors);
	}

	#-------------------------------------------------------------------------------
	# Get lang from user browser
	#-------------------------------------------------------------------------------
	my ($lang) = ($context->GetDefaultLang () =~ m/^([a-z]*)/);

	#-------------------------------------------------------------------------------
	# Parse page and translate macros
	#-------------------------------------------------------------------------------
	open (FH, '<', "$Mioga2::Login::login_data_path/contact-$lang.html") or die "File $Mioga2::Login::login_data_path/contact-$lang.html not found";
	my $page = join ('', <FH>);
	ProcessPageMacros ($context, \$page);
	close (FH);

	#-------------------------------------------------------------------------------
	# Initialize content
	#-------------------------------------------------------------------------------
	my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/html');
	$content->SetContent ($page);

	return ($content);
}	# ----------  end of subroutine ContactForm  ----------


#===============================================================================

=head2 ProcessContact

Process contact form

=cut

#===============================================================================
sub ProcessContact {
	my ($context) = @_;
	print STDERR "[Mioga2::Login::ProcessContact] Entering\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'target' ], 'disallow_empty' ],
			[ [ 'message' ], 'disallow_empty' ],
			[ [ 'email' ], 'disallow_empty', 'want_email' ],
			[ [ 'captcha', 'captcha_id' ], 'disallow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "[Mioga2::Login::ProcessContact] Args: " . Dumper $values if ($debug);

	#-------------------------------------------------------------------------------
	# Check captcha
	#-------------------------------------------------------------------------------
	my $captcha = new Mioga2::Captcha ($context->GetConfig ()->GetDBH (), $context->GetConfig ()->GetCaptchaDefaults ());
	if (!$captcha->Check ($values->{captcha_id}, $values->{captcha})) {
		push (@$errors, ['captcha', ['incorrect_value']]);
	}

	#-------------------------------------------------------------------------------
	# Get lang from user browser
	#-------------------------------------------------------------------------------
	my ($lang) = ($context->GetDefaultLang () =~ m/^([a-z]*)/);

	#-------------------------------------------------------------------------------
	# Display error page if errors
	#-------------------------------------------------------------------------------
	if (@$errors) {
		my $data = { errors => $errors };

		#-------------------------------------------------------------------------------
		# Parse page and translate macros
		#-------------------------------------------------------------------------------
		open (FH, '<', "$Mioga2::Login::login_data_path/contact-$lang.html") or die "File $Mioga2::Login::login_data_path/contact-$lang.html not found";
		my $page = join ('', <FH>);
		ProcessPageMacros ($context, \$page, $data);
		close (FH);

		#-------------------------------------------------------------------------------
		# Initialize content
		#-------------------------------------------------------------------------------
		my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/html');
		$content->SetContent ($page);

		print STDERR "[Mioga2::Login::ProcessContact] Leaving\n" if ($debug);
		return ($content);
	}

	#-------------------------------------------------------------------------------
	# Set instance config
	#-------------------------------------------------------------------------------
	my $target_uri = Mioga2::URI->new ($context->GetConfig (), uri => $values->{target});
	my $config = Mioga2::Config->new ($context->GetConfig (), $target_uri->GetMiogaIdent ());

	#-------------------------------------------------------------------------------
	# Initialize e-mail contents
	#-------------------------------------------------------------------------------
	my $subject = __("Message from user on Mioga2 login manager");
	my @data = (__("Someone used the Mioga2 login manager to send you the message below."), "\n\n", $values->{message});
	$subject = encode_mimeword($subject, "Q", "UTF-8");
	my $entity = MIME::Entity->build(
		Charset => 'UTF8',
		From    => $values->{email},
		Type    => "text/plain",
		Subject => $subject,
		Data    => \@data
	);
	MailAdmins ($config, $entity);

	#-------------------------------------------------------------------------------
	# Parse page and translate macros
	#-------------------------------------------------------------------------------
	open (FH, '<', "$Mioga2::Login::login_data_path/contact-sent-$lang.html") or die "File $Mioga2::Login::login_data_path/contact-sent-$lang.html not found";
	my $page = join ('', <FH>);
	ProcessPageMacros ($context, \$page);
	close (FH);

	#-------------------------------------------------------------------------------
	# Initialize content
	#-------------------------------------------------------------------------------
	my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/html');
	$content->SetContent ($page);

	print STDERR "[Mioga2::Login::ProcessContact] Leaving\n" if ($debug);
	return ($content);
}	# ----------  end of subroutine ProcessContact  ----------


#===============================================================================

=head2 MailAdmins

Send a mail to administrators

=over

=item B<arguments:>

=over

=item I<$entity> a MIME::Entity object.

=back

=cut

#===============================================================================
sub MailAdmins {
	my ($config, $entity) = @_;

	my $apps = Mioga2::ApplicationList->new ($config, { attributes => { ident => ['Colbert', 'Bottin'] } });
	my $groups = Mioga2::GroupList->new ($config, { attributes => { applications => $apps } });
	my $admins = Mioga2::UserList->new ($config, { attributes => { groups => $groups, applications => $apps, functions => ['Notifications'] } });
	$admins->SendMail ($entity);
}	# ----------  end of subroutine MailAdmins  ----------


#===============================================================================

=head2 GetCaptcha

Generate, record and return a captcha image

=cut

#===============================================================================
sub GetCaptcha {
	my ($context) = @_;
	print STDERR "[Mioga2::Login::GetCaptcha] Entering\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['hash'], 'disallow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	
	if (@$errors) {
		throw Mioga2::Exception::Simple ('Mioga2::Login::GetCaptcha', 'Missing arguments');
	}

	my $captcha = new Mioga2::Captcha ($context->GetConfig ()->GetDBH (), $context->GetConfig ()->GetCaptchaDefaults ());

	my $content = Mioga2::Content::ASIS->new($context, MIME => 'image/gif', filename => 'captcha.gif');
	$content->SetContent($captcha->GetImage ($values->{hash}));

	print STDERR "[Mioga2::Login::GetCaptcha] Leaving\n" if ($debug);
	return ($content);
}	# ----------  end of subroutine GetCaptcha  ----------

1;

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Authen, Mioga2::Dispatch, Mioga2::Cleanup, Mioga2::Login::Specific,
Mioga2::Login::Doris, Mioga2::Login::PageMacros, Mioga2::Login::Contact

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
