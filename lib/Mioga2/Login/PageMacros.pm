#===============================================================================
#
#         FILE:  PageMacros.pm
#
#  DESCRIPTION:  Mioga2::Login page macros
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  31/01/2012 12:54
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

PageMacros.pm: Function for processing Mioga2::Login page macros

=head1 DESCRIPTION

The functions in this module are called by Mioga2::Login when processing
page macros.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Login;

use Locale::TextDomain::UTF8 'login';

use Error qw(:try);
use Mioga2::tools::args_checker;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;
use Mioga2::InstanceList;
use Digest::MD5  qw(md5_hex);
use POSIX qw(strftime);
use MIME::Entity;
use MIME::Words qw (:all);
use Data::Dumper;

my $debug = 0;

#===============================================================================

=head2 ProcessPageMacros

Replaces macros from page template

=cut

#===============================================================================
my $macros = {
	TARGET          => 'SetTarget',
	INSTANCES       => 'SetInstanceList',
	DORIS_LINK      => 'SetDorisLink',
	ERRORS          => 'SetErrors',
	SECRET_QUESTION => 'SetSecretQuestion',
	CAPTCHA         => 'SetCaptcha',
	LOGIN_URL       => 'SetLoginURL',
	LOGIN_STATUS    => 'SetLoginStatus',
};
sub ProcessPageMacros {
	my ($context, $page, $data) = @_;

	# Sanitize arguments to prevent injection
	for my $key (keys (%{$context->{args}})) {
		$context->{args}->{$key} = st_FormatXMLString($context->{args}->{$key});
	}

	for my $macro (keys (%$macros)) {
		if ($$page =~ /$macro/) {
			print STDERR "[Mioga2::Login::PageMacros::ProcessPageMacros] Found macro $macro in page\n" if ($debug);
			my $handler = $macros->{$macro};
			&{\&{$handler}} ($context, $page, $data);
		}
	}
}	# ----------  end of subroutine ProcessPageMacros  ----------


#===============================================================================

=head2 SetSecretQuestion

Set secret question (page macro)

=cut

#===============================================================================
sub SetSecretQuestion {
	my ($context, $page, $data) = @_;

	if (defined ($data->{secret_question})) {
		$$page =~ s/%SECRET_QUESTION%/$data->{secret_question}/gm;
	}

	return ();
}	# ----------  end of subroutine SetSecretQuestion  ----------


#===============================================================================

=head2 SetErrors

Set errors (page macro)

=cut

#===============================================================================
sub SetErrors {
	my ($context, $page, $data) = @_;

	if ($data->{errors} && @{$data->{errors}}) {
		my $display_password_policy = 0;
		my $errors = $data->{errors};
		my @strings = map { if ($_->[1]->[0] eq 'password_policy') { $display_password_policy = 1 }; __($_->[0]) . __(": ") . __($_->[1]->[0]) } @$errors;
		my $str = join ('</li><li>', @strings);
		if ($display_password_policy) {
			$str .= '<li>' . __("Password policy:") . '</li>';
			$str .= '<ul id="password-policy">';
			for my $key (qw/pwd_min_length pwd_min_letter pwd_min_digit pwd_min_special pwd_min_chcase/) {
				$str .= '<li>' . __($key) . $data->{password_policy}->{$key} . '</li>';
			}
			$str .= '</ul>';
		}
		if ($str) {
			$$page =~ s/%ERRORS%/<ul><li>$str<\/li><\/ul>/gm;
		}
	}
	else {
		$$page =~ s/%ERRORS%//gm;
	}

}	# ----------  end of subroutine SetErrors  ----------


#===============================================================================

=head2 SetDorisLink

Set Doris link (page macro)

=cut

#===============================================================================
sub SetDorisLink {
	my ($context, $page) = @_;

	#-------------------------------------------------------------------------------
	# Initialize target (currently mandatory, will be optional once users are
	# decorrelated from instances)
	#-------------------------------------------------------------------------------
	my $target;
	if ($context->{args}->{target}) {
		$target = $context->{args}->{target};
	}
	else {
		$target = $context->GetReferer ();
	}

	my $link = $context->GetConfig ()->GetBasePath . $context->GetConfig ()->GetLoginURI () . '/PasswordRecovery' . ($target ? "?target=$target" : '');
	$$page =~ s/%DORIS_LINK%/$link/gm;
}	# ----------  end of subroutine SetDorisLink  ----------


#===============================================================================

=head2 SetInstanceList

Set instance list (page macro)

=cut

#===============================================================================
sub SetInstanceList {
	my ($context, $page, $data) = @_;

	if (!$data->{instances}) {
		# No instance given in aguments, initialize full list
		my $all_instances = Mioga2::InstanceList->new ($context->GetConfig ());
		@{$data->{instances}} = map { $_->{ident} } $all_instances->GetInstances ();
	}

	my $list = '<ul>';
	for my $inst (sort { lc ($a) cmp lc ($b) } @{$data->{instances}}) {
		my $cfg = Mioga2::Config->new ($context->GetConfig (), $inst);
		$list .= "<li><a href='" . $cfg->GetBaseURI () . "'>$inst</a></li>";
	}
	$list .= '</ul>';

	$$page =~ s/%INSTANCES%/$list/gm;

	return ();
}	# ----------  end of subroutine SetInstanceList  ----------


#===============================================================================

=head2 SetTarget

Set target URL (page macro)

=cut

#===============================================================================
sub SetTarget {
	my ($context, $page) = @_;

	my $target;
	if ($context->{args}->{target}) {
		$target = $context->{args}->{target};
	}
	else {
		$target = $context->GetReferer ();
	}

	if (!$target) {
		# No real targe, disable link and add generic message
		$$page =~ s/href="%TARGET%"//gm;
		$target = __('Instance selection');
	}

	$$page =~ s/%TARGET%/$target/gm;
}	# ----------  end of subroutine SetTarget  ----------


#===============================================================================

=head2 SetCaptcha

Insert a captcha

=cut

#===============================================================================
sub SetCaptcha {
	my ($context, $page) = @_;

	# Generate captha
	my $captcha = Mioga2::Captcha->new ($context->GetConfig ()->GetDBH (), $context->GetConfig ()->GetCaptchaDefaults ());
	my ($hash) = $captcha->GetRandom ();

	my $str = '';
	$str .= '<img src="Captcha?hash=' . $hash . '" class="captcha"/>';
	$str .= '<input type="hidden" name="captcha_id" value="' . $hash . '"/>';

	$$page =~ s/%CAPTCHA%/$str/gm;
}	# ----------  end of subroutine SetCaptcha  ----------


#===============================================================================

=head2 SetLoginURL

Insert login URL

=cut

#===============================================================================
sub SetLoginURL {
	my ($context, $page) = @_;

	my $config = $context->GetConfig ();

	my $url = $config->GetBasePath . $config->GetLoginURI () . '/DisplayMain' . ($context->{args}->{target} ? ('?target=' . $context->{args}->{target}) : '');

	$$page =~ s/%LOGIN_URL%/$url/gm;
}	# ----------  end of subroutine SetLoginURL  ----------


#===============================================================================

=head2 SetLoginStatus

Set an error message to visible in case of unsuccessful login.

=cut

#===============================================================================
sub SetLoginStatus {
	my ($context, $page, $data) = @_;

	my $visibility = $data->{failed} ? 'block' : 'none';

	$$page =~ s/%LOGIN_STATUS%/$visibility/gm;
}	# ----------  end of subroutine SetLoginStatus  ----------

1;


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Login

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
