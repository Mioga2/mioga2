# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME

Doris.pm : The Mioga2 password assistance application, for those who have memory problems.

=head1 METHODS DESCRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Login;

use Locale::TextDomain::UTF8 'login';

use Error qw(:try);
use Mioga2::tools::args_checker;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;
use Mioga2::UserList;
use Digest::MD5  qw(md5_hex);
use POSIX qw(strftime);
use MIME::Entity;
use MIME::Words qw (:all);
use Data::Dumper;

my $debug = 0;

my $expiration_delay = 60*60;



#===============================================================================

=head2 PasswordRecovery

Display password recovery form

=cut

#===============================================================================
sub PasswordRecovery {
	my ($context) = @_;
	print STDERR "[Mioga2::Login::PasswordRecovery] Entering\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Get lang from user browser
	#-------------------------------------------------------------------------------
	my ($lang) = ($context->GetDefaultLang () =~ m/^([a-z]*)/);

	#-------------------------------------------------------------------------------
	# Initialize content
	#-------------------------------------------------------------------------------
	my $content = Mioga2::Content::SendFile->new ($context, disposition => 'inline', path => "$Mioga2::Login::login_data_path/recovery-$lang.html");

	print STDERR "[Mioga2::Login::PasswordRecovery] Leaving\n" if ($debug);
	return ($content);
}	# ----------  end of subroutine PasswordRecovery  ----------


#===============================================================================

=head2 GenerateRecoveryLink

Generate and send password recovery link

=cut

#===============================================================================
sub GenerateRecoveryLink {
	my ($context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'email' ], 'disallow_empty', 'want_email' ],
			[ [ 'target' ], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		# User submitter rubbish, send him back to previous page
		print STDERR "[Mioga2::Login::GenerateRecoveryLink] Errors: " . Dumper $errors;
		my $content = new Mioga2::Content::REDIRECT ($context, mode => 'external');
		$content->SetContent($context->GetReferer ());
		return ($content);
	}

	#-------------------------------------------------------------------------------
	# Get lang from user browser
	#-------------------------------------------------------------------------------
	my ($lang) = ($context->GetDefaultLang () =~ m/^([a-z]*)/);


	#-------------------------------------------------------------------------------
	# Get instance ident from target or from database
	#-------------------------------------------------------------------------------
	my $instance;
	if ($values->{target}) {
		# Target specified, get instance from URL
		my $target = new Mioga2::URI ($context->GetConfig (), uri => $values->{target}, missing_domain => 1);
		$instance = $target->GetMiogaIdent ();
	}
	else {
		# No target specified, get instance from user email address
		my $db = $context->GetConfig ()->GetDBObject ();
		my $res = $db->SelectSingle ('SELECT ident FROM m_mioga WHERE rowid = (SELECT mioga_id FROM m_user_base WHERE email = ? LIMIT 1)', [$values->{email}]);
		if (!$res) {
			# User is unknown, get first existing instance so that further processing doesn't fail
			# This will obviously lead the password recovery request to be rejected as email is unknown in randomly-picked instance.
			$res = $db->SelectSingle ('SELECT ident FROM m_mioga LIMIT 1');
		}

		$instance = $res->{ident};
	}


	#-------------------------------------------------------------------------------
	# Set target instance config
	#-------------------------------------------------------------------------------
	my $config = Mioga2::Config->new ($context->GetConfig (), $instance);

	my $user = Mioga2::UserList->new ($config, { attributes => { email => $values->{email} } });

	if ($user->Count ()) {
		# Check if user can reinitialize his password
		my ($can_reinit, $mail_admin, $set_secret_question) = CheckUserCanReinit ($config, $user);

		if ($can_reinit) {
			my $subject = __("Mioga2 Password Assistance");
			my @data = __("Someone, presumably you, requested a direct link to Mioga2 password reinitialization page.\nIf you requested it, please click the link below to access the page.\nIf you are not the requester, please inform your system administrator.") . "\n\n" . GetReinitLink ($context, $user->Get ('rowid'));
			$subject = encode_mimeword($subject, "Q", "UTF-8");
			my $content = MIME::Entity->build(
				Charset => 'UTF8',
				From    => $user->Get ('email'),
				To      => $user->Get ('email'),
				Type    => "text/plain",
				Subject => $subject,
				Data    => \@data
			);
			$user->SendMail ($content);
		}
		elsif (!$can_reinit && $mail_admin) {
			my $subject = __("Mioga2 Password Assistance");
			my @data = __("Someone, presumably you, requested a direct link to Mioga2 password reinitialization page.\nUnfortunately, your request can not be processed automatically and will require an administrator's intervention.\nIf you wish to inform an administrator about your request, please click the link below.") . "\n\n" . GetReinitLink ($context, $user->Get ('rowid'));
			$subject = encode_mimeword($subject, "Q", "UTF-8");
			my $content = MIME::Entity->build(
				Charset => 'UTF8',
				From    => $user->Get ('email'),
				To      => $user->Get ('email'),
				Type    => "text/plain",
				Subject => $subject,
				Data    => \@data
			);
			$user->SendMail ($content);
		}
		else {
			#-------------------------------------------------------------------------------
			# Parse page and translate macros
			#-------------------------------------------------------------------------------
			print STDERR "[Mioga2::Login::GenerateRecoveryLink] page to parse: $Mioga2::Login::login_data_path/recovery-impossible-$lang.html\n" if ($debug);
			open (FH, '<', "$Mioga2::Login::login_data_path/recovery-impossible-$lang.html") or die "File $Mioga2::Login::login_data_path/recovery-impossible-$lang.html not found";
			my $page = join ('', <FH>);
			ProcessPageMacros ($context, \$page);
			close (FH);

			#-------------------------------------------------------------------------------
			# Initialize content
			#-------------------------------------------------------------------------------
			my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/html');
			$content->SetContent ($page);

			return ($content);
		}
	}

	#-------------------------------------------------------------------------------
	# Parse page and translate macros
	#-------------------------------------------------------------------------------
	print STDERR "[Mioga2::Login::DisplayMain] page to parse: $Mioga2::Login::login_data_path/recovery-linksent-$lang.html\n" if ($debug);
	open (FH, '<', "$Mioga2::Login::login_data_path/recovery-linksent-$lang.html") or die "File $Mioga2::Login::login_data_path/recovery-linksent-$lang.html not found";
	my $page = join ('', <FH>);
	ProcessPageMacros ($context, \$page);
	close (FH);

	#-------------------------------------------------------------------------------
	# Initialize content
	#-------------------------------------------------------------------------------
	my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/html');
	$content->SetContent ($page);

	return ($content);
}	# ----------  end of subroutine GenerateRecoveryLink  ----------


#===============================================================================

=head2 GetReinitLink

Generate a hash and return reinitialization link

=cut

#===============================================================================
sub GetReinitLink {
	my ($context, $rowid, $delay) = @_;

	my $config = $context->GetConfig ();
	my $hash = GenerateHash ($config, $rowid, $delay);
	my $link = $config->GetProtocol () . "://" . $config->GetServerName () . $config->GetBasePath . $config->GetLoginURI () . '/ReinitPassword?hash=' . $hash . ($context->{args}->{target} ? ('&target=' . $context->{args}->{target}) : '');

	return ($link);
}	# ----------  end of subroutine GetReinitLink  ----------


#===============================================================================

=head2 ReinitForm

Display password reinitialization form.

=cut

#===============================================================================
sub ReinitForm {
	my ($context) = @_;

	#-------------------------------------------------------------------------------
	# Get lang from user browser
	#-------------------------------------------------------------------------------
	my ($lang) = ($context->GetDefaultLang () =~ m/^([a-z]*)/);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'hash' ], 'disallow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		# No hash, display an error
		return (Mioga2::Content::SendFile->new ($context, disposition => 'inline', path => "$Mioga2::Login::login_data_path/recovery-error-$lang.html"));
	}

	#-------------------------------------------------------------------------------
	# Check hash validity
	#-------------------------------------------------------------------------------
	my $user = CheckHashValidity ($context->GetConfig (), $values->{hash});
	if (!$user || !$user->Count ()) {
		# No user, display an error
		return (Mioga2::Content::SendFile->new ($context, disposition => 'inline', path => "$Mioga2::Login::login_data_path/recovery-error-$lang.html"));
	}

	# Set target instance config
	my $instance = Mioga2::InstanceList->new ($context->GetConfig (), { attributes => { rowid => $user->Get ('mioga_id') } });
	my $config = Mioga2::Config->new ($context->GetConfig (), $instance->Get ('ident'));

	#-------------------------------------------------------------------------------
	# Select appropriate form (with or without secret question)
	#-------------------------------------------------------------------------------
	my $data = {};
	my $filename = 'recovery-form';
	my ($can_reinit, $mail_admin, $set_secret_question, $reason) = CheckUserCanReinit ($config, $user);
	if ($mail_admin) {
		# User request can not be processed automatically, send mail to administrators
		my $subject = __("Mioga2 Password Assistance");
		my @data = __x("Your intervention is required for a password assistance operation that can not be processed automatically.\n\nMioga instance: {mioga_instance}\nUser ident: {user_ident}\nUser information: {user_label}\nReason: {reason}", mioga_instance => $config->GetMiogaIdent (), user_ident => $user->GetIdent (), user_label => $user->Get ('firstname') . ' ' . $user->Get ('lastname') . ' (' . $user->Get ('email') . ')', reason => $reason);
		$subject = encode_mimeword($subject, "Q", "UTF-8");
		my $entity = MIME::Entity->build(
			Charset => 'UTF8',
			From    => $user->Get ('email'),
			Type    => "text/plain",
			Subject => $subject,
			Data    => \@data
		);
		MailAdmins ($config, $entity);

		$filename = 'recovery-mail-admins';

		# Destroy hash from DB
		ExecSQL ($config->GetDBH (), "DELETE FROM m_passwd_reinit WHERE hash='$values->{hash}'");
	}
	elsif ($config->UseSecretQuestion () && $user->GetSecretQuestion ()) {
		$filename = 'recovery-form-secret-question';
		$data->{secret_question} = $user->GetSecretQuestion ()->{question};
	}
	elsif (!$user->GetSecretQuestion () && $config->UseSecretQuestion () && $config->DorisCanSetSecretQuestion ()) {
		$filename = 'recovery-form-new-secret-question';
	}
	else {
		$filename = 'recovery-form-no-secret-question';
	}
	print STDERR "[Mioga2::Login::Doris::ReinitForm] Using template $filename\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Display form
	#-------------------------------------------------------------------------------
	# Parse page and translate macros
	open (FH, '<', "$Mioga2::Login::login_data_path/$filename-$lang.html") or die "File $Mioga2::Login::login_data_path/$filename-$lang.html not found";
	my $page = join ('', <FH>);
	ProcessPageMacros ($context, \$page, $data);
	close (FH);
	# Initialize content
	my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/html');
	$content->SetContent ($page);
	return $content;
}	# ----------  end of subroutine ReinitForm  ----------


#===============================================================================

=head2 ReinitPassword

Process password reinitialization request

=cut

#===============================================================================
sub ReinitPassword {
	my ($context) = @_;


	#-------------------------------------------------------------------------------
	# Get lang from user browser
	#-------------------------------------------------------------------------------
	my ($lang) = ($context->GetDefaultLang () =~ m/^([a-z]*)/);


	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ ['hash'], 'disallow_empty'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	if (@$errors) {
		# No hash, display an error
		print STDERR "[Mioga2::Login::ReinitPassword] Errors: " . Dumper $errors;
		my $content = new Mioga2::Content::REDIRECT ($context, mode => 'external');
		$content->SetContent($context->GetReferer ());
	}


	#-------------------------------------------------------------------------------
	# Check hash validity
	#-------------------------------------------------------------------------------
	my $user = CheckHashValidity ($context->GetConfig (), $values->{hash});
	if (!$user || !$user->Count ()) {
		# No user, display an error
		return (Mioga2::Content::SendFile->new ($context, disposition => 'inline', path => "$Mioga2::Login::login_data_path/recovery-error-$lang.html"));
	}


	#-------------------------------------------------------------------------------
	# Process request
	#-------------------------------------------------------------------------------
	my $done = 0;
	my $delete = 0;
	my $clear_passwd;

	my $policy_fail = 0;
	my $passwords_match = 0;
	my $wrong_answer = 0;

	# Set target instance config
	my $instance = Mioga2::InstanceList->new ($context->GetConfig (), { attributes => { rowid => $user->Get ('mioga_id') } });
	my $config = Mioga2::Config->new ($context->GetConfig (), $instance->Get ('ident'));

	my $lastcon = ($user->Count ()) ? SelectSingle ($config->GetDBH (), "SELECT max(last_connection) AS last_connection FROM m_group_user_last_conn WHERE user_id = " . $user->Get ('rowid'))->{last_connection} : undef;
	my ($can_reinit, $mail_admin, $set_secret_question, $reason) = CheckUserCanReinit ($config, $user);

	if ($can_reinit) {
		if ($config->UseSecretQuestion ()) {
			#-------------------------------------------------------------------------------
			# Check incoming arguments
			#-------------------------------------------------------------------------------
			my $question_status = $user->GetSecretQuestion () ? 'allow_empty' : 'disallow_empty';
			$params = [
					[ ['hash'], 'disallow_empty'],
					[ ['password1', 'password2'], 'disallow_empty', [ 'password_policy', $config ] ],
					[ ['secret_question'], $question_status],
					[ ['secret_question_answer'], 'disallow_empty'],
				];
		}
		else {
			#-------------------------------------------------------------------------------
			# Check incoming arguments
			#-------------------------------------------------------------------------------
			$params = [
					[ ['hash'], 'disallow_empty'],
					[ ['password1', 'password2'], 'disallow_empty', [ 'password_policy', $config ] ],
				];
		}
		($values, $errors) = ac_CheckArgs ($context, $params);

		if ($values->{password1} eq $values->{password2}) {
			$passwords_match = 1;
		}
		print STDERR "[Mioga2::Login::Doris::ReinitPassword] Passwords match: $passwords_match\n" if ($debug);

		if ($config->UseSecretQuestion () && $user->GetSecretQuestion () && !$user->CheckSecretQuestion ($values->{secret_question_answer})) {
			$wrong_answer = 1;
		}

		if (! $policy_fail && $passwords_match && !$wrong_answer && !@$errors) {
			# Check provided password is OK
			if($values->{password1} ne '' and $values->{password2} ne '' and $values->{password1} eq $values->{password2}) {
				$values->{password} = delete ($values->{password1});
				delete $values->{password2};
				$values->{firstname} = $user->GetFirstname ();
				$values->{lastname} = $user->GetLastname ();
				$values->{email} = $user->GetEmail ();

				# Modify user password into LDAP
				if (($user->Get ('type') eq 'ldap_user') && exists ($config->{ldap_bind_dn})) {
					my $ldap = new Mioga2::LDAP($config);
					$ldap->BindWith ($config->{ldap_bind_dn}, $config->{ldap_bind_pwd});
					
					# Crypt password for LDAP
					$clear_passwd = $values->{password};
					my $crypter = $config->LoadPasswordCrypter();
					$values->{password} = $crypter->CryptPassword($config, $values);
					my $dn = $user->Get ('dn');
					$ldap->ModifyUser ($dn, $ldap->TranslateAttributes($values));
					$values->{password} = $clear_passwd;
				}
				
				# Modify user password into DB
				$user->Set ('password', $values->{password});
				$user->Store ();

				# Global password update, set new password for user into each instance
				my $crypter = $config->LoadPasswordCrypter();
				my $encrypted_password = $crypter->CryptPassword($config, $values);
				my $db = $context->GetConfig ()->GetDBObject ();
				$db->ExecSQL ('UPDATE m_user_base SET password = ? WHERE email = ?;', [$encrypted_password, $values->{email}]);

				# Record secret question
				if ($config->UseSecretQuestion () && ($values->{secret_question})) {
					$user->SetSecretQuestion ($values->{secret_question}, $values->{secret_question_answer});
				}
				
				$done = 1;
				$delete = 1;
			}
		}

		#-------------------------------------------------------------------------------
		# Remove any duplicate 'password_policy' error
		#-------------------------------------------------------------------------------
		my $id = -1;
		for (@$errors) {
			if ($_->[1][0] eq 'password_policy') {
				$id++;
			}
		}
		if ($id > 0) {
			splice (@$errors, $id, 1);
		}
	}

	if ($delete) {
		# Destroy hash from DB
		ExecSQL ($config->GetDBH (), "DELETE FROM m_passwd_reinit WHERE hash='$values->{hash}'");
	}

	if ($done) {
		#-------------------------------------------------------------------------------
		# Parse page and translate macros
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::Login::GenerateRecoveryLink] page to parse: $Mioga2::Login::login_data_path/recovery-done-$lang.html\n" if ($debug);
		open (FH, '<', "$Mioga2::Login::login_data_path/recovery-done-$lang.html") or die "File $Mioga2::Login::login_data_path/recovery-done-$lang.html not found";
		my $page = join ('', <FH>);
		ProcessPageMacros ($context, \$page);
		close (FH);

		#-------------------------------------------------------------------------------
		# Initialize content
		#-------------------------------------------------------------------------------
		my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/html');
		$content->SetContent ($page);

		return ($content);
	}
	else {
		# Some errors to display
		push (@$errors, [ 'password1', ['passwords_differ']]) if (!$passwords_match);
		push (@$errors, [ 'secret_question_answer', ['wrong_answer']]) if ($wrong_answer);
		print STDERR "[Mioga2::Login::Doris::ReinitPassword] Errors: " . Dumper $errors;

		#-------------------------------------------------------------------------------
		# Select appropriate form (with or without secret question)
		#-------------------------------------------------------------------------------
		my $data = {};
		$data->{errors} = $errors;
		$data->{password_policy} = $config->GetPasswordPolicy ();
		my $filename = 'recovery-form';
		if ($config->UseSecretQuestion () && $user->GetSecretQuestion ()) {
			$filename = 'recovery-form-secret-question';
			$data->{secret_question} = $user->GetSecretQuestion ()->{question};
		}
		elsif (!$user->GetSecretQuestion ()) {
			$filename = 'recovery-form-new-secret-question';
		}
		print STDERR "[Mioga2::Login::Doris::ReinitPassword] Using template $filename\n" if ($debug);

		#-------------------------------------------------------------------------------
		# Parse page and translate macros
		#-------------------------------------------------------------------------------
		open (FH, '<', "$Mioga2::Login::login_data_path/$filename-$lang.html") or die "File $Mioga2::Login::login_data_path/$filename-$lang.html not found";
		my $page = join ('', <FH>);
		ProcessPageMacros ($context, \$page, $data);
		close (FH);

		#-------------------------------------------------------------------------------
		# Initialize content
		#-------------------------------------------------------------------------------
		my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/html');
		$content->SetContent ($page);
		return $content;
	}
}	# ----------  end of subroutine ReinitPassword  ----------


#===============================================================================
# Private Methods
#===============================================================================

#===============================================================================

=head2 CheckHashValidity

Check if given hash is valid (recorded in DB and not expired).

=over

=item B<Parameters:>

=over

=item I<$config:> A Mioga2::MiogaConf object.

=item I<$hash> The hash to check.

=back

=item B<Return:> The associated Mioga2::Old::User object.

=back

=cut

#===============================================================================
sub CheckHashValidity {
	my ($miogaconf, $hash) = @_;
	
	print STDERR "[Mioga2::Login::Doris::CheckHashValidity] Entering\n" if ($debug);
	
	my $dbh = $miogaconf->GetDBH();

	my $user = undef;
	$hash = st_FormatPostgreSQLString ($hash);

	# Get hash information
	my $res = SelectSingle ($dbh, "SELECT m_passwd_reinit.rowid, m_passwd_reinit.expiration_date, m_mioga.ident FROM m_passwd_reinit, m_user_base, m_mioga WHERE m_passwd_reinit.hash='$hash' AND m_passwd_reinit.rowid = m_user_base.rowid AND m_user_base.mioga_id = m_mioga.rowid");
	print STDERR Dumper $res if ($debug);

	# Compare expiration date to current date
	if ($res->{expiration_date} gt strftime("%Y-%m-%d %H:%M:%S", gmtime(time))) {
		my $config = Mioga2::Config->new ($miogaconf, $res->{ident});
		$user = Mioga2::UserList->new ($config, { attributes => { rowid => $res->{rowid} } });
		print STDERR "[Mioga2::Login::Doris::CheckHashValidity] User email is " . $user->Get ('email') . "\n" if ($debug);
	}

	return ($user);
}	# ----------  end of subroutine CheckHashValidity  ----------


#===============================================================================

=head2 GenerateHash

Generate a hash and record it to DB for temporary access to password reinitialization.

=over

=item B<Parameters:>

=over

=item I<$config:> A Mioga2::Config object.

=item I<$rowid:> The user rowid.

=back

=item B<Return:> The hash.

=back

=cut

#===============================================================================
sub GenerateHash {
	my ($config, $rowid, $delay) = @_;

	$delay = 600 unless ($delay);

	my $dbh = $config->GetDBH();
	
	# Generate hash
	my ($hash) = (`dd if=/dev/urandom bs=1k count=5 2>/dev/null | md5sum` =~ m/^([0-9a-f]*).*/);

	# Prepare values for DB insert
	my %values = (
		rowid           => $rowid,
		hash            => $hash,
		expiration_date => strftime("%Y-%m-%d %H:%M:%S", gmtime(time + $delay))
	);

	# Delete eventually existing hash for user
	ExecSQL ($dbh, "DELETE FROM m_passwd_reinit WHERE rowid='$values{rowid}'");

	# Record hash to DB
	my $sql = BuildInsertRequest (
		\%values,
		table  => 'm_passwd_reinit',
		string => [ qw(hash rowid expiration_date) ]
	);
	ExecSQL($dbh, $sql);

	return ($hash);
}	# ----------  end of subroutine GenerateHash  ----------


#===============================================================================

=head2 CheckUserCanReinit

Check if a given user can reinit his password.

=cut

#===============================================================================
sub CheckUserCanReinit {
	my ($config, $user) = @_;

	my $can_reinit = 1;
	my $mail_admin = 0;
	my $set_secret_question = 0;
	my $reason = __('Unknown');

	# Check if the user can reinitialize his password
	my $lastcon = ($user) ? SelectSingle ($config->GetDBH (), "SELECT max(last_connection) AS last_connection FROM m_group_user_last_conn WHERE user_id = " . $user->Get ('rowid'))->{last_connection} : undef;

	if (!$user->Get ('autonomous')) {
		print STDERR "[Mioga2::Login::Doris::CheckUserCanReinit] User is not autonomous.\n" if ($debug);
		$can_reinit = 0;
	}
	elsif ($user->Get ('status') ne 'active') {
		print STDERR "[Mioga2::Login::Doris::CheckUserCanReinit] User is not active.\n" if ($debug);
		$can_reinit = 0;
		$mail_admin = 1;
		$reason = __('User account is not active.');
	}
	elsif (($user->Get ('type') eq 'ldap_user') && (!$config->CanModifyLdapPassword ())) {
		print STDERR "[Mioga2::Login::Doris::CheckUserCanReinit] User is LDAP but access to directory is restricted.\n" if ($debug);
		$can_reinit = 0;
	}
	elsif ($config->UseSecretQuestion () && !$user->GetSecretQuestion () && !$config->DorisCanSetSecretQuestion ()) {
		print STDERR "[Mioga2::Login::Doris::CheckUserCanReinit] User has not defined a secret question but Doris can not set secret question.\n" if ($debug);
		$can_reinit = 0;
		$mail_admin = 1;
		$reason = __('User did not define a secret question.');
	}
	elsif ($config->UseSecretQuestion () && !$user->GetSecretQuestion () && $config->DorisCanSetSecretQuestion () && ($lastcon ne '')) {
		print STDERR "[Mioga2::Login::Doris::CheckUserCanReinit] User has not defined a secret question, Doris can set secret question, but user has already connected.\n" if ($debug);
		$can_reinit = 0;
		$mail_admin = 1;
		$reason = __('User did not define a secret question.');
	}
	elsif ($config->UseSecretQuestion () && !$user->GetSecretQuestion () && $config->DorisCanSetSecretQuestion () && ($lastcon eq '')) {
		$set_secret_question = 1;
	}

	print STDERR "[Mioga2::Login::Doris::CheckUserCanReinit] can_reinit: $can_reinit, mail_admin: $mail_admin, set_secret_question: $set_secret_question\n" if ($debug);
	return ($can_reinit, $mail_admin, $set_secret_question, $reason);
}	# ----------  end of subroutine CheckUserCanReinit  ----------

1;


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Login

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
