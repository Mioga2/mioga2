#===============================================================================
#
#         FILE:  ColbertSA.pm
#
#  DESCRIPTION:  The Mioga2 instance administration application.
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  05/07/2010 14:26
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

ColbertSA.pm: The Mioga2 instance administration application.

=head1 DESCRIPTION

This application can manage Mioga2 instances.

=cut

use strict;
use warnings;

package Mioga2::ColbertSA;
use vars qw($VERSION);

$VERSION = "1.0.0";

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'colbertsa';

use Mioga2::InstanceList;
use Mioga2::UserList;
use Mioga2::GroupList;
use Mioga2::TeamList;
use Mioga2::Classes::View;
use Mioga2::Classes::Grid;
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::tools::database;
use Mioga2::tools::args_checker;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Mioga2::Skeleton;
use Mioga2::tools::date_utils;

use File::Path;
use IO::Scalar;
use Archive::Zip qw/:ERROR_CODES/;
use File::Temp qw/tempfile tempdir/;
use File::Copy::Recursive qw/dirmove/;

use Error qw(:try);
use Data::Dumper;

my $debug = 0;


#===============================================================================

=head1 INTERFACE METHODS

=cut

#===============================================================================


#===============================================================================

=head2 DisplayMain

Display home page.

=head3 Generated XML

=over

	<DisplayMain>
		<miogacontext>See Mioga2::RequestContext</miogacontext>
		<instances>
			<total>...</total>
			<groups>
				<total>...</total>
				<group>
					<ident>...</ident>
					<admin>...</admin>
					<users>
					</users>
				</group>
			</groups>
			<users count="...">
				<total>...</total>
				<user>
					<ident>...</ident>
				</user>
			</users>
			<teams count="...">
				<total>...</total>
				<team>
					<ident>...</ident>
				</team>
			</teams>
		</instances>
	</DisplayMain>

=back

=cut

#===============================================================================
sub DisplayMain {
	my ($self, $context) = @_;
	
	#-------------------------------------------------------------------------------
	# Create Instance View and Grid
	#-------------------------------------------------------------------------------
	my %view_desc = (
                       'fields' => [ [ 'rowid', 'rowid'],
                              [ 'ident', 'normal', __('Identifier')],
                              [ 'referent', 'normal', __('Referent')],
                              [ 'referent_comment', 'normal', __('Referent Comment')],
                              [ 'groups', 'normal', __('Groups'), '5em'],
                              [ 'users', 'normal', __('Users'), '7em'],
                              [ 'teams', 'normal', __('Teams'), '5em'],
                              [ 'use_ldap', 'boolean', __('Uses LDAP'), '4em'],
                              [ 'created', 'date', __('Created')],
                              [ 'last_connection', 'date', __('Last Connection')],
                              [ 'disk_space', 'filesize', __('Disk Space')],
                            ],

                       'filter' => [ { ident => 'ident', label => __('Identifier'), type => 'text' },
					   				 { ident => 'referent', label => __('Referent'), type => 'text' },
								],
                      
                       );

	my $instance_view = new Mioga2::Classes::View ({desc => \%view_desc});
	my $grid_params = { ident => 'instances',
	                    view => $instance_view,
	                    get_data => 'GetInstances.json',
						nodename => 'instance',
						identifier => 'ident',
						'label' => __('Identifier'),
						confirm_fields => ['ident', 'users', 'groups', 'last_connection'],
	                    default_action => 'DisplayInstanceEdit',
						select_actions => [ { label => __("Delete"), function => 'DeleteInstance.json', type => 'json', post_var => 'rowid', check_function => "CheckInstanceForDelete ('grid_instances')" },
						                  ],
	                  };

	# Get custom sort field and column widths from session
	my $session = $context->GetSession ();
	if (exists ($session->{persistent}->{sort_field})) {
		$grid_params->{sort_field} = $session->{persistent}->{sort_field};
	}
	if (exists ($session->{persistent}->{column_widths})) {
		$grid_params->{column_widths} = $session->{persistent}->{column_widths};
	}

	my $instance_grid = new Mioga2::Classes::Grid ($grid_params);

	my $xml = '<DisplayMain>';

	$xml .= $context->GetXML ();

	$xml .= $instance_grid->GetXML();

	my ($instances);

	# Get instances
	$xml .= '<instances>';
	$instances = Mioga2::InstanceList->new ($context->GetConfig ()->GetMiogaConf ());
	$xml .= '<total>' . $instances->Count () . '</total>';
	$xml .= '</instances>';

	$xml .= '</DisplayMain>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbertsa.xsl', locale_domain => "colbertsa_xsl");
    $content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine DisplayMain  ----------


#===============================================================================

=head1 WEBSERVICE METHODS

=cut

#===============================================================================


#===============================================================================

=head2 GetInstances

Get existing instances as XML or JSON.

=head3 Incoming Arguments

=over

=over

None.

=back

=back

=head3 Generated XML

=over

	<GetInstances>
		<instance>
			<rowid>...</rowid>
			<ident>...</ident>
		</instance>
		...
	</GetInstances>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetInstances {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::ColbertSA::GetInstances] Entering.\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['ident', 'referent'], 'allow_empty', 'stripwxps' ],
			[ ['match'], 'allow_empty', [ 'match', "\(begins|contains|ends\)" ] ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };
	$data->{instance} = [ ];

	my %attrs;
	for my $attr (qw/ident referent match/) {
		$attrs{$attr} = $values->{$attr} if (st_ArgExists ($context, $attr) && $values->{$attr} ne '');
	}
	my $instances = Mioga2::InstanceList->new ($context->GetConfig ()->GetMiogaConf (), { attributes => \%attrs });

	# Load list according to input arguments
	$instances->Load ();
	for (@{$instances->{instances}}) {
		my $instcfg = new Mioga2::Config ($context->GetConfig ()->GetMiogaConf (), $_->{ident});
		my $inst;
		$inst->{rowid} = $_->{rowid};
		$inst->{ident} = $_->{ident};
		$inst->{use_ldap} = $_->{use_ldap};
		$inst->{created} = $_->{created};
		if ($_->{referent_id}) {
			my $ref = Mioga2::UserList->new ($instcfg, { attributes => { rowid => $_->{referent_id} } });
			$inst->{referent} = $ref->Get ('label');
		}
		if ($_->{default_group_id}) {
			my $defgrp = Mioga2::GroupList->new ($instcfg, { attributes => { rowid => $_->{default_group_id} } });
			$inst->{default_group} = $defgrp->Get ('ident');
		}
		$inst->{referent_comment} = $_->{referent_comment};
		$inst->{disk_space} = SelectSingle ($context->GetConfig ()->GetDBH (), "SELECT sum(size) AS disk_space FROM m_uri WHERE group_id IN (SELECT rowid FROM m_group_base WHERE mioga_id = $_->{rowid});")->{disk_space};
		$inst->{last_connection} = du_ConvertGMTISOToLocale (SelectSingle ($context->GetConfig ()->GetDBH (), "SELECT max(last_connection) AS last_connection FROM m_group_user_last_conn WHERE group_id IN (SELECT rowid FROM m_group_base WHERE mioga_id = $_->{rowid});")->{last_connection}, $context->GetUser ());
		my $groups = Mioga2::GroupList->new ($instcfg);
		$inst->{groups} = $groups->Count ();
		my $users = Mioga2::UserList->new ($instcfg);
		$inst->{users} = $users->Count ();
		my $teams = Mioga2::TeamList->new ($instcfg);
		$inst->{teams} = $teams->Count ();

		# Set current_instance flag
		if ($inst->{rowid} == $context->GetConfig ()->GetMiogaId ()) {
			$inst->{current_instance} = 1;
		}

		push @{$data->{instance}}, $inst;
	}

	print STDERR "[Mioga2::ColbertSA::GetInstances] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetInstances  ----------


#===============================================================================

=head2 GetInstanceDetails

Get an instance's details as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: the instance rowid to get details.

=back

=back

=head3 Generated XML

=over

	<GetInstanceDetails>
		<ident>...</ident>
		...
	</GetInstanceDetails>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetInstanceDetails {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::ColbertSA::GetInstanceDetails] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ],
			[ ['full_list'], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		# Get instance details
		my $instance = Mioga2::InstanceList->new ($context->GetConfig ()->GetMiogaConf (), { attributes => { rowid => $values->{rowid} } });
		$instance->Load ();
		$data->{instance} = $instance->{instances}->[0];

		# Get instance applications
		my $applications = $instance->GetApplicationList ();
		$applications->Load ();
		for (@{$applications->{applications}}) {
			$_->{selected}->{'$a'} = 1 if (st_ArgExists ($context, 'full_list'));
			push (@{$data->{applications}->{application}}, $_);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$applications->Revert ();
			$applications->Load ();
			for (@{$applications->{applications}}) {
				push (@{$data->{applications}->{application}}, $_);
			}
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::ColbertSA::GetInstanceDetails] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetInstanceDetails  ----------


#===============================================================================

=head2 SetInstance

Create or update an instance.

=head3 Incoming Arguments

=over

=over

=item B<rowid> (I<optional>): The rowid of the instance to update. At creation time, this argument doesn't exist.

=item B<ident>: The instance identifier.

=item B<lang>: The instance default language ident.

=item B<lang_id>: The instance default language rowid.

=item B<skeleton>: The instance skeleton file name.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<SetInstance>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</SetInstance>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub SetInstance {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::ColbertSA::SetInstance] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ [ 'rowid' ], 'allow_empty' ],
			[ [ 'lang' ], 'disallow_empty', 'stripxws' ],
			[ [ 'referent_comment', 'comment' ], 'allow_empty', 'stripxws' ],
			[ [ 'lang_id', 'referent_id' ], 'want_int', 'allow_empty' ],
			[ [ 'pwd_min_length', 'pwd_min_letter', 'pwd_min_digit', 'pwd_min_special', 'pwd_min_chcase', 'auth_fail_timeout', 'max_allowed_group' ], 'disallow_empty', 'want_int' ],
			[ [ 'quota_hard', 'quota_soft' ], 'allow_empty', 'want_int' ],
			[ [ 'domain_name' ], 'disallow_empty', 'stripxws', 'valid_domain_name' ],
			[ [ 'use_secret_question', 'doris_can_set_secret_question', 'use_ldap' ], 'disallow_empty', [ 'match', "\(0|1\)" ] ],
			[ [ 'password_crypt_method' ], 'disallow_empty', [ 'match', "\(md5|sha|crypt|clear\)" ] ],
			[ [ 'applications' ], 'allow_empty' ],
			[ [ 'skeleton' ], 'allow_empty', 'filename' ],
		];

	if (!st_ArgExists ($context, 'rowid' )) {
		push (@$params, [ [ 'skeleton' ], 'disallow_empty', 'filename' ]);
		push (@$params, [ [ 'ident' ], 'disallow_empty', 'stripxws' ]);

		# Admin account information
		if (!$context->GetConfig ()->GetMiogaConf ()->IsSharedAdmin ()) {
			push (@$params, [ [ 'admin_ident' ], 'disallow_empty' ]);
			push (@$params, [ [ 'admin_email' ], 'disallow_empty', 'want_email' ]);
			if (!$context->GetConfig ()->GetMiogaConf ()->IsCentralizedUsers ()) {
				push (@$params, [ [ 'admin_password', 'admin_password_confirm' ], 'disallow_empty' ]);
			}
		}
	}
	else {
		push (@$params, [ [ 'default_theme_id' ], 'disallow_empty', 'want_int' ]);
		push (@$params, [ [ 'homepage' ], 'allow_empty', 'stripxws' ]);
		push (@$params, [ [ 'default_group_id' ], 'allow_empty', 'want_int' ]);
	}

	if (st_ArgExists ($context, 'use_ldap')) {
		push (@$params, [ [ 'ldap_host', 'ldap_bind_dn', 'ldap_bind_pwd', 'ldap_user_base_dn', 'ldap_user_filter', 'ldap_user_scope', 'ldap_user_ident_attr', 'ldap_user_firstname_attr', 'ldap_user_lastname_attr', 'ldap_user_email_attr', 'ldap_user_pwd_attr', 'ldap_user_desc_attr', 'ldap_user_inst_attr' ], 'disallow_empty', 'stripxws' ]);
		push (@$params, [ [ 'ldap_port' ], 'disallow_empty', 'want_int' ]);
		push (@$params, [ [ 'ldap_access' ], 'disallow_empty', [ 'match', "\(0|1|2\)" ] ]);
	}

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if ($values->{ident} && $values->{ident} =~ m/ /) {
		push (@$errors, [ 'ident', [ 'invalid_ident' ]]);
	}

	if ($values->{ident} && (($context->IsAuthenWeb () && ($context->GetConfig ()->GetMiogaConf ()->GetLoginURI () =~ /\/$values->{ident}$/)) || ($context->GetConfig ()->GetMiogaConf ()->GetExternalURI () =~ /\/$values->{ident}$/) || ($context->GetConfig ()->GetMiogaConf ()->GetInternalURI () =~ /\/$values->{ident}$/))) {
		push (@$errors, [ 'ident', [ 'invalid_ident' ]]);
	}

	if ($values->{ident} && ($context->GetConfig ()->GetMiogaConf ()->GetUserURI () =~ /\/$values->{ident}$/)) {
		push (@$errors, [ 'ident', [ 'invalid_ident' ]]);
	}

	if ($values->{ident} && ($context->GetConfig ()->GetCDNURI () =~ /\/$values->{ident}$/)) {
		push (@$errors, [ 'ident', [ 'invalid_ident' ]]);
	}

	if (st_ArgExists ($context, 'admin_password') && (!st_ArgExists ($context, 'admin_password_confirm') || ($values->{admin_password} ne $values->{admin_password_confirm}))) {
		push (@$errors, [ 'admin_password_confirm', [ 'different_passwords' ]]);
	}
	delete ($values->{admin_password_confirm});

	# Check quota values are consistent
	if (st_ArgExists ($context, 'quota_soft') && st_ArgExists ($context, 'quota_hard') && ($values->{quota_soft} > $values->{quota_hard})) {
		push (@$errors, [ 'quota_soft', [ 'invalid_value' ] ]);
	}

	# Check LDAP is enabled if admin user is LDAP
	my $admin_macros = $context->GetConfig ()->GetMiogaConf ()->GetAdminMacros ();
	if (($admin_macros->{ADMIN_TYPE} eq 'ldap_user') && !st_ArgExists ($context, 'use_ldap' )) {
		push (@$errors, [ 'use_ldap', [ 'invalid_value' ] ]);
	}

	my $data = { };

	if (!@$errors) {
		my $instance;
		my $list = delete ($values->{applications});
		@{$values->{applications}} = map { { ident => $_ } } @$list;
		if (st_ArgExists ($context, 'rowid')) {
			$self->{db}->BeginTransaction ();
			try {
				#-------------------------------------------------------------------------------
				# Update existing instance
				#-------------------------------------------------------------------------------
				# Load instance from rowid
				$instance = Mioga2::InstanceList->new ($context->GetConfig ()->GetMiogaConf (), { attributes => { rowid => $values->{rowid} } });

				# Update instance
				for my $key (keys (%$values)) {
					next if (grep (/^$key$/, qw/groups teams/));
					$instance->Set ($key, $values->{$key});
				}
				$instance->Store ();
				$self->{db}->EndTransaction ();
				$data->{success} = 1;
			}
			otherwise {
				my $err = shift;
				print STDERR "[Mioga2::ColbertSA::SetInstance] Instance update failed.\n";
				print STDERR $err->stringify ($context);
				$self->{db}->RollbackTransaction ();
				$data->{success} = 0;
			};
		}
		else {
			try {
				#-------------------------------------------------------------------------------
				# Create new instance
				#-------------------------------------------------------------------------------
				# Prefix skeleton path to the file name
				if (st_ArgExists ($context, 'skeleton')) {
					$values->{skeleton} = $context->GetConfig ()->GetSkeletonDir () . "/$values->{lang}/instance/$values->{skeleton}";
				}

				# Actually create instance
				$instance = Mioga2::InstanceList->new ($context->GetConfig ()->GetMiogaConf (), { attributes => $values });
				$instance->Store ();
				$data->{success} = 1;
			}
			otherwise {
				my $err = shift;
				print STDERR "[Mioga2::ColbertSA::SetInstance] Instance creation failed.\n";
				print STDERR $err->stringify ($context);
				my $journal = $instance->GetJournal ();
				$journal->SetMetadata ({ stage => 'rollback' });
				$instance->Delete ();
				$data->{success} = 0;
			};
		}

		# Return rowid of instance
		$data->{rowid} = $instance->Get ('rowid');

		$data->{journal} = $instance->GetJournal ()->ToArray ();
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::ColbertSA::SetInstance] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetInstance  ----------


#===============================================================================

=head2 DeleteInstance

Delete instances.

=cut

#===============================================================================
sub DeleteInstance {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::ColbertSA::DeleteInstance] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		for (@{ac_ForceArray ($values->{rowid})}) {
			if ($_ == $context->GetConfig ()->GetMiogaId ()) {
				push (@$errors, [ 'rowid', [ 'can_not_delete_current_instance' ] ]);
			}
		}

		if (!@$errors) {
			my $instance = Mioga2::InstanceList->new ($context->GetConfig ()->GetMiogaConf (), { attributes => { rowid => \@{ac_ForceArray ($values->{rowid})} } });
			$instance->Delete ();
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::ColbertSA::DeleteInstance] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DeleteInstance  ----------


#===============================================================================

=head2 GetLanguages

Get registered languages.

=head3 Incoming Arguments

=over

None.

=back

=head3 Generated XML

=over

	<GetLanguages>
		<language rowid="...">...</language>
		...
	</GetLanguages>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetLanguages {
	my ($self, $context) = @_;

	my $data = { };

	my $lang = SelectMultiple ($context->GetConfig ()->GetDBH (), "SELECT rowid, ident FROM m_lang;");
	for (@$lang) {
		$_->{label} = __($_->{ident});
		$_->{value} = $_->{rowid};
	}
	$data->{language} = $lang;

	print STDERR "[Mioga2::ColbertSA::GetLanguages] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetLanguages  ----------


#===============================================================================

=head2 GetSkeletons

Get skeletons list as XML or JSON.

=head3 Incoming Arguments

=over

=over

None.

=back

=back

=head3 Generated XML

=over

	<GetSkeletons>
		<skeleton>
			<name>...</name>
			<file>...</file>
		</skeleton>
		...
	</GetSkeletons>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetSkeletons {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::ColbertSA::GetSkeletons] Entering. Args: " . Dumper $context->{args} if ($debug);

	my $data = { };

	my $lang = $context->GetGroup ()->Get ('lang');
	my $skelpath = $context->GetConfig ()->GetSkeletonDir () . "/$lang/instance";
	my @skellist = <$skelpath/*>;
	for (@skellist) {
		my ($skelfile) = ($_ =~ m/.*\/([^\/]*)$/);

		#-------------------------------------------------------------------------------
		# Read file
		#-------------------------------------------------------------------------------
		my $skeldata = '';
		open(FILE, "< $_") or throw Mioga2::Exception::Simple ('Mioga2::ColbertSA::GetSkeletons', "Can't open skeleton file '$_': $!.");
		while (<FILE>) { $skeldata .= $_ };
		close (FILE);

		#-------------------------------------------------------------------------------
		# Parse XML
		#-------------------------------------------------------------------------------
		$skeldata =~ s/<--!.*?-->//gsm; # remove comments

		my $xml		  = new Mioga2::XML::Simple(forcearray => 1);
		my $xmltree   = $xml->XMLin($skeldata);
		my $skelname = (exists($xmltree->{'name'}))? $xmltree->{'name'} : "";

		push (@{$data->{skeleton}}, {file => $skelfile, name => $skelname});
	}

	print STDERR "[Mioga2::ColbertSA::GetSkeletons] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetSkeletons  ----------


#===============================================================================

=head2 GetSkeletonDetails

Get skeleton details as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<type>: Type of skeleton. Value must be 'instance' and is case insensitive.

=item B<lang>: Language of the skeleton. Value is a supported language with associated skeletons.

=item B<file>: Skeleton filename without path.

=back

=back

=head3 Generated XML

=over

	<GetSkeletonDetails>
		<attributes>
			<!-- Inner attributes of the skeleton -->
			...
		</attributes>
	</GetSkeletonDetails>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetSkeletonDetails {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::ColbertSA::GetSkeletonDetails] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['type'], 'disallow_empty', [ 'match', "^instance\$" ] ],
			[ [ 'lang' ], 'disallow_empty', 'stripxws' ],
			[ [ 'file' ], 'disallow_empty', 'filename' ],
			[ ['full_list'], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	# Some fields names do not correspond between skeleton and DB, this hash allows to redefine field name if required
	# The top-level keys correspond to top-level keys in the ->{attributes} part of the skeleton
	my %translations = (
		ldap => {
			access => 'ldap_access',
			host => 'ldap_host',
			port => 'ldap_port',
			bind_dn => 'ldap_bind_dn',
			bind_pwd => 'ldap_bind_pwd',
			user_base_dn => 'ldap_user_base_dn',
			user_filter => 'ldap_user_filter',
			user_scope => 'ldap_user_scope',
			user_ident_attr => 'ldap_user_ident_attr',
			user_firstname_attr => 'ldap_user_firstname_attr',
			user_lastname_attr => 'ldap_user_lastname_attr',
			user_email_attr => 'ldap_user_email_attr',
			user_pwd_attr => 'ldap_user_pwd_attr',
			user_desc_attr => 'ldap_user_desc_attr',
			user_inst_attr => 'ldap_user_inst_attr',
		}
	);

	if (!@$errors) {
		my $skel = Mioga2::Skeleton->new ($context->GetConfig (), type => $values->{type}, lang => $values->{lang}, file => $values->{file});
		my $attributes = $skel->GetAttributes ();
		for my $node (keys (%$attributes)) {
			$data->{attributes}{use_ldap} = 1 if ($node eq 'ldap');
			for (keys (%{$attributes->{$node}})) {
				my $keyname = ($translations{$node}{$_}) ? $translations{$node}{$_} : $_;
				$data->{attributes}{$keyname} = $attributes->{$node}{$_} if (defined ($attributes->{$node}{$_}));
			}
		}


		#-------------------------------------------------------------------------------
		# Skeleton doesn't specify LDAP configuration,
		# fill attributes with default values
		#-------------------------------------------------------------------------------
		if (!$data->{attributes}{use_ldap}) {
			if (!($data->{attributes}{use_ldap})) {
				my $defaults = $context->GetConfig ()->GetMiogaConf ()->GetLDAPDefaults ();
				for my $attr (qw/ldap_host ldap_port ldap_bind_dn ldap_user_base_dn ldap_user_filter ldap_user_scope ldap_user_ident_attr ldap_user_firstname_attr ldap_user_lastname_attr ldap_user_email_attr ldap_user_pwd_attr ldap_user_desc_attr ldap_user_inst_attr/) {
					$data->{attributes}->{$attr} = $defaults->{$attr} if ($defaults->{$attr});
				}
			}
		}

		# Initialize application list
		my $applications = $skel->GetApplications ();
		for ($applications->GetApplications ()) {
			$_->{selected} = 1 if ($values->{full_list});
			push (@{$data->{applications}->{application}}, $_);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$applications->Revert ();
			$applications->Load ();
			for ($applications->GetApplications ()) {
				push (@{$data->{applications}->{application}}, $_);
			}
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::ColbertSA::GetSkeletonDetails] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetSkeletonDetails  ----------


#===============================================================================

=head2 GetUsers

Get existing users as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<instance>: the instance ident to get users.

=back

=back

=head3 Generated XML

=over

	<GetUsers>
		<user>
			<rowid>...</rowid>
			<firstname>...</firstname>
			<lastname>...</lastname>
			<email>...</email>
			<type>...</type>
			<lang>...</lang>
			<autonomous>...</autonomous>
			<shared>...</shared>
			<status>...</status>
			<last_conn>...</last_conn>
		</user>
		...
	</GetUsers>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetUsers {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::ColbertSA::GetUsers] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['instance'], 'disallow_empty', 'stripxws' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };
	$data->{user} = [ ];

	if (!@$errors) {
		my $instcfg = new Mioga2::Config ($context->GetConfig ()->GetMiogaConf (), $values->{instance});

		my $users = Mioga2::UserList->new ($instcfg);

		# Load list according to input arguments
		$users->Load ();
		for (@{$users->{users}}) {
			delete $_->{password};
			push @{$data->{user}}, $_;
		}
	}

	print STDERR "[Mioga2::ColbertSA::GetUsers] Data: " . Dumper $data if ($debug);
	return ($data);
}

#===============================================================================

=head2 GetGroups

Get existing users as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<instance>: the instance ident to get users.

=back

=back

=head3 Generated XML

=over

	<GetGroups>
		<group>
			<rowid>...</rowid>
			<ident>...</ident>
		</group>
		...
	</GetGroups>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetGroups {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::ColbertSA::GetGroups] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['instance'], 'disallow_empty', 'stripxws' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };
	$data->{group} = [ ];

	if (!@$errors) {
		my $instcfg = new Mioga2::Config ($context->GetConfig ()->GetMiogaConf (), $values->{instance});

		my $groups = Mioga2::GroupList->new ($instcfg);

		# Load list according to input arguments
		$groups->Load ();
		for (@{$groups->{groups}}) {
			push @{$data->{group}}, $_;
		}
	}

	print STDERR "[Mioga2::ColbertSA::GetGroups] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetGroups  ----------


#===============================================================================

=head2 GetThemes

Get existing users as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<instance>: the instance ident to get users.

=back

=back

=head3 Generated XML

=over

	<GetThemes>
		<theme>
			<rowid>...</rowid>
			<ident>...</ident>
		</theme>
		...
	</GetThemes>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetThemes {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::ColbertSA::GetThemes] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['instance'], 'disallow_empty', 'stripxws' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };
	$data->{theme} = [ ];

	if (!@$errors) {
		my $instcfg = new Mioga2::Config ($context->GetConfig ()->GetMiogaConf (), $values->{instance});

		my $themes = SelectMultiple ($context->GetConfig ()->GetDBH (), "SELECT * FROM m_theme WHERE mioga_id = " . $instcfg->GetMiogaId ());

		for (@{$themes}) {
			push @{$data->{theme}}, $_;
		}
	}

	print STDERR "[Mioga2::ColbertSA::GetThemes] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetThemes  ----------


#===============================================================================

=head2 DownloadSkeletons

Download instance user and group skeletons as an archive.

=head3 Incoming Arguments

=over

=item B<type>: The skeleton type to download. Value can be 'instance'.

=back

=head3 Generated output

=over

This method returns a ZIP file containing skeleton filesystem structure.

=back

=cut

#===============================================================================
sub DownloadSkeletons {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['type'], 'disallow_empty', [ 'match', "^instance\$" ] ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $content;

	if (!@$errors) {
		my $zip = Archive::Zip->new ();
		my $zipmember = $zip->addTreeMatching ($context->GetConfig ()->GetSkeletonDir (), '', ".*$values->{type}.*");

		my $zipContents = '';
		my ($tempfh, $tempfile) = tempfile ();
		$zip->writeToFileNamed ($tempfile);
		open (FH, $tempfile);
		while (<FH>) {
			$zipContents .= $_;
		}
		close (FH);
		unlink ($tempfile);

		$content = Mioga2::Content::ASIS->new ($context, MIME => 'application/zip', filename => $context->GetConfig ()->GetMiogaIdent () . "-$values->{type}-skeletons.zip");
		$content->SetContent ($zipContents);
	}

	print STDERR "[Mioga2::ColbertSA::DownloadSkeletons] Leaving." if ($debug);
	return ($content);
}	# ----------  end of subroutine DownloadSkeletons  ----------


#===============================================================================

=head2 UploadSkeletons

Upload an instance user and group skeletons archive and overwrite current ones.

=head3 Incoming Arguments

=over

=item B<type>: The skeleton type to overwrite. Value can be 'instance'.

=item B<file>: The uploaded file name. This argument will not be processed but the uploaded file content will.

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<UploadSkeletons>
		<errors>
			<error>...</error>
			...
		</errors>
	</UploadSkeletons>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub UploadSkeletons {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::ColbertSA::UploadSkeletons] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['skeletons_archive'], 'disallow_empty' ],
			[ ['type'], 'disallow_empty', [ 'match', "^instance\$" ] ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $upload_data = $context->GetUploadData ();

	my $data = { };

	if (!@$errors) {
		my $tmpdir = tempdir ();

		try {
			# Extract ZIP contents to temp dir
			my $SH = IO::Scalar->new (\$upload_data);
			my $zip = Archive::Zip->new ();
			if ($zip->readFromFileHandle ($SH) != AZ_OK) {
				throw Mioga2::Exception::Simple ('Mioga2::ColbertSA::UploadSkeletons', __("Unable to load ZIP data." . " $!"));
			}
			if ($zip->extractTree ('', "$tmpdir/") != AZ_OK) {
				throw Mioga2::Exception::Simple ('Mioga2::ColbertSA::UploadSkeletons', __("Unable to extract ZIP data." . " $!"));
			}

			# Overwrite skeletons with extracted ZIP contents
			my @langs = <$tmpdir/*>;
			foreach my $lang (@langs) {
				# Skip invalid subdirs
				$lang =~ s/^$tmpdir\///;
				next if ($lang !~ m/^[a-z]{2}_[A-Z]{2}$/);

				# Overwrite data
				rmtree ($context->GetConfig ()->GetSkeletonDir () . "/$lang/$values->{type}") or throw Mioga2::Exception::Simple ('Mioga2::ColbertSA::UploadSkeletons', __("Unable to delete original skeletons.") . " $!");
				dirmove ("$tmpdir/$lang/$values->{type}", $context->GetConfig ()->GetSkeletonDir () . "/$lang/$values->{type}") or throw Mioga2::Exception::Simple ('Mioga2::ColbertSA::UploadSkeletons', __("Unable to copy new skeletons.") . " $!");
			}
		}
		otherwise {
			my $err = shift;
			print STDERR "[Mioga2::ColbertSA::UploadSkeletons] Skeletons extracting failed.\n";
			print STDERR $err->stringify ($context);
			push (@$errors, [ 'global', [ $err->{'-text'} ] ]);
		};

		# Remove temp dir
		rmtree ($tmpdir);

	}

	print STDERR "[Mioga2::ColbertSA::UploadSkeletons] Leaving. Data: " . Dumper $data if ($debug);

	my $xml = '<UploadSkeletons>';

	$xml .= $context->GetXML ();

	ac_I18NizeErrors($errors);
	$xml .= '<errors>';
	for (@$errors) {
		$xml .= '<err field="' . $_->[0] . '">' . $_->[1]->[0] . '</err>';
	}
	$xml .= '</errors>';

	$xml .= '</UploadSkeletons>';

	print STDERR "ColbertSA::UploadSkeletons xml: $xml \n" if ($debug);

	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbertsa.xsl', locale_domain => "colbertsa_xsl");
    $content->SetContent($xml);

	return $content;
}	# ----------  end of subroutine UploadSkeletons  ----------


#===============================================================================

=head2 StorePreferences

Store user preferences (column widths, sort field).

=cut

#===============================================================================
sub StorePreferences {
	my ($self, $context) = @_;
	print STDERR "[ColbertSA::StorePreferences] Entering, prefs: " . Dumper $context->{args} if ($debug);

	my $data = {};
	$data->{success} = 1;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ ['sort_field', 'columns', 'widths'], 'disallow_empty', 'stripxws'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (!@$errors) {
		my $prefs = {};
		$prefs->{sort_field} = $values->{sort_field};
		for my $col (@{$values->{columns}}) {
			$prefs->{column_widths}->{$col} = shift (@{$values->{widths}});
		}

		my $session = $context->GetSession ();
		$session->{persistent} = $prefs;

		$data->{success} = 1;
	}
	else {
		$data->{errors} = $errors;
	}

	print STDERR "[ColbertSA::StorePreferences] Leaving.\n" if ($debug);
	return ($data);
}	# ----------  end of subroutine StorePreferences  ----------


#===============================================================================

=head2 DownloadLoginResources

Download instance user and group skeletons as an archive.

=head3 Incoming Arguments

=over

None.

=back

=head3 Generated output

=over

This method returns a ZIP file containing skeleton filesystem structure.

=back

=cut

#===============================================================================
sub DownloadLoginResources {
	my ($self, $context) = @_;

	my $content;
	my $miogaconf = $context->GetConfig ()->GetMiogaConf ();

	my $zip = Archive::Zip->new ();
	my $zipmember = $zip->addTreeMatching ($miogaconf->GetInstallDir () . $miogaconf->GetLoginURI (), '', ".*");

	my $zipContents = '';
	my ($tempfh, $tempfile) = tempfile ();
	$zip->writeToFileNamed ($tempfile);
	open (FH, $tempfile);
	while (<FH>) {
		$zipContents .= $_;
	}
	close (FH);
	unlink ($tempfile);

	$content = Mioga2::Content::ASIS->new ($context, MIME => 'application/zip', filename => 'Mioga-login-resources.zip');
	$content->SetContent ($zipContents);

	print STDERR "[Mioga2::ColbertSA::DownloadLoginResources] Leaving." if ($debug);
	return ($content);
}	# ----------  end of subroutine DownloadLoginResources  ----------


#===============================================================================

=head2 UploadLoginResources

Upload an instance user and group skeletons archive and overwrite current ones.

=head3 Incoming Arguments

=over

=item B<file>: The uploaded file name. This argument will not be processed but the uploaded file content will.

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<UploadLoginResources>
		<errors>
			<error>...</error>
			...
		</errors>
	</UploadLoginResources>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub UploadLoginResources {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::ColbertSA::UploadLoginResources] Entering. Args: " . Dumper $context->{args} if ($debug);

	my $miogaconf = $context->GetConfig ()->GetMiogaConf ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['login_resources_archive'], 'disallow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $upload_data = $context->GetUploadData ();

	my $data = { };

	if (!@$errors) {
		my $tmpdir = tempdir ();

		try {
			# Extract ZIP contents to temp dir
			my $SH = IO::Scalar->new (\$upload_data);
			my $zip = Archive::Zip->new ();
			if ($zip->readFromFileHandle ($SH) != AZ_OK) {
				throw Mioga2::Exception::Simple ('Mioga2::ColbertSA::UploadLoginResources', __("Unable to load ZIP data." . " $!"));
			}
			if ($zip->extractTree ('', "$tmpdir/") != AZ_OK) {
				throw Mioga2::Exception::Simple ('Mioga2::ColbertSA::UploadLoginResources', __("Unable to extract ZIP data." . " $!"));
			}

			# Overwrite login resources with extracted ZIP contents
			rmtree ($miogaconf->GetInstallDir () . $miogaconf->GetLoginURI ()) or throw Mioga2::Exception::Simple ('Mioga2::ColbertSA::UploadLoginResources', __("Unable to delete original login resources.") . " $!");
			dirmove ("$tmpdir", $miogaconf->GetInstallDir () . $miogaconf->GetLoginURI ()) or throw Mioga2::Exception::Simple ('Mioga2::ColbertSA::UploadLoginResources', __("Unable to copy new login resources.") . " $!");
		}
		otherwise {
			my $err = shift;
			print STDERR "[Mioga2::ColbertSA::UploadLoginResources] Login resources extracting failed.\n";
			print STDERR $err->stringify ($context);
			push (@$errors, [ 'global', [ $err->{'-text'} ] ]);
		};

		# Remove temp dir
		rmtree ($tmpdir);

	}

	print STDERR "[Mioga2::ColbertSA::UploadLoginResources] Leaving. Data: " . Dumper $data if ($debug);

	my $xml = '<UploadLoginResources>';

	$xml .= $context->GetXML ();

	ac_I18NizeErrors($errors);
	$xml .= '<errors>';
	for (@$errors) {
		$xml .= '<err field="' . $_->[0] . '">' . $_->[1]->[0] . '</err>';
	}
	$xml .= '</errors>';

	$xml .= '</UploadLoginResources>';

	print STDERR "ColbertSA::UploadLoginResources xml: $xml \n" if ($debug);

	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbertsa.xsl', locale_domain => "colbertsa_xsl");
    $content->SetContent($xml);

	return $content;
}	# ----------  end of subroutine UploadLoginResources  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

#===============================================================================

=head2 GetAppDesc

Returns application description.

=cut

#===============================================================================
sub GetAppDesc {
	my %AppDesc = (
		ident   => 'ColbertSA',
		package => 'Mioga2::ColbertSA',
		name => __('Mioga2 Instance Administration'),
		description => __('Manage Mioga2 Instances'),
		type    => 'normal',
		api_version => '2.3',
		all_groups => 0,
		all_users  => 0,
		can_be_public => 0,
		is_user             => 1,
		is_group            => 1,
		is_resource         => 0,
		functions => { 'Administration' => __('Administration Functions') },
		func_methods => {
			'Administration' => [
				'DisplayMain',
				'DisplayInstances', 'GetInstances', 'GetInstanceDetails', 'SetInstance', 'DeleteInstance',
				'GetLanguages', 'GetSkeletons', 'GetSkeletonDetails', 'GetUsers', 'GetGroups', 'GetThemes',
				'DownloadSkeletons', 'UploadSkeletons',
				'StorePreferences',
				'DownloadLoginResources', 'UploadLoginResources',
			],
		},
		sxml_methods => { },
		xml_methods  => { },
		non_sensitive_methods => [
			'DisplayMain',
			'DisplayInstances',
			'GetInstances',
			'GetInstanceDetails',
			'GetLanguages',
			'GetSkeletons',
			'GetSkeletonDetails',
			'GetUsers',
			'GetGroups',
			'GetThemes',
			'DownloadSkeletons',
			'DownloadLoginResources',
		],
	);

	return \%AppDesc;
}	# ----------  end of subroutine GetAppDesc  ----------

#===============================================================================

=head2 InitApp

Initialize application internal values

=cut

#===============================================================================
sub InitApp {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::ColbertSA::InitApp]\n" if ($debug);

	$self->{dbh} = $context->GetConfig ()->GetDBH ();

	# Set default column widths if none defined
	my $session = $context->GetSession ();
	if (!exists ($session->{persistent}->{column_widths})) {
		$session->{persistent}->{column_widths} = {
			groups => '55px',
			users => '70px',
			teams => '55px',
			use_ldap => '45px',
			last_connection => '130px',
			disk_space => '100px',
		};
	}
}	# ----------  end of subroutine InitApp  ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2010, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;

# Override the _isSeekable function in Archive::Zip
# because of unfixed bug http://rt.cpan.org/Public/Bug/Display.html?id=7855
no warnings 'redefine';
package Archive::Zip::Archive;
sub _isSeekable {
    my $fh = shift;
    if ( UNIVERSAL::isa( $fh, 'IO::Scalar' ) )
    {
        return $] >= 5.006;
    }
    elsif ( UNIVERSAL::isa( $fh, 'IO::String' ) )
    {
        return $] >= 5.006;
    }
    elsif ( UNIVERSAL::can( $fh, 'stat') )
    {
        return -f $fh;
    }
    return UNIVERSAL::can( $fh, 'seek');
}
use warnings 'all';

