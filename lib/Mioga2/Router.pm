#===============================================================================
#
#         FILE:  Router.pm
#
#  DESCRIPTION:  Handler for the translate phase.
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  02/02/2012 15:01
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME
	
Router.pm: Handler for the translate phase.

=head1 DESCRIPTION

The translate phase is used to perform the manipulation of a request's URI.
http://perl.apache.org/docs/2.0/user/handlers/http.html#PerlTransHandler

In Mioga2, this module routes requests to the appropriate handlers.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Router;

use Apache2::Const;
use Apache2::RequestUtil;
use Data::Dumper;

my $debug = 0;

#-------------------------------------------------------------------------------
# If the URI contains a single member, it can be:
# 	- the login handler (login_uri),
# 	- the user handler (user_uri) -- not implemented yet,
# 	- an instance name
#
# If the URI contains two members.
# The first member must be an instance name or the login handler.
# The second member can be:
# 	- a WebDAV root (home or public),
# 	- an existing group,
# 	- a login module method.
#-------------------------------------------------------------------------------

sub handler : method {
	my($class, $r) = @_;
	print STDERR "[Mioga2::Router::handler -- $$] ---------------------------------------------------------\n" if ($debug);


	#-----------------------------------------------------------
	# Load Mioga Config
	#-----------------------------------------------------------
	if(!defined ($r->dir_config("MiogaConfig"))) {
		$r->log_error ("[Mioga2::Router::handler -- $$] Can't find MiogaConfig parameter");
		$r->log_error ("[Mioga2::Router::handler -- $$] Add \"PerlSetVar MiogaConfig /path/to/Mioga.conf\" in your apache configuration file");
		return Apache2::Const::SERVER_ERROR;
	}
	my $miogaconf = Mioga2::MiogaConf->new ($r->dir_config("MiogaConfig"));
	$r->pnotes ('miogaconf', $miogaconf);

	#-------------------------------------------------------------------------------
	# Create object
	#-------------------------------------------------------------------------------
	my $self = {
		miogaconf => $miogaconf,
		db        => $miogaconf->{db},
	};
	bless ($self, $class);

	#-------------------------------------------------------------------------------
	# Cleanup the URI from rubbish
	#-------------------------------------------------------------------------------
	print STDERR "[Mioga2::Router::handler -- $$] Entering, URI: " . $r->uri () . "\n" if ($debug > 1);
	#$self->CleanupURI ($r);
	#my $uri = $r->uri ();
	my $uri = $self->Cleanup2URI ($r);
	print STDERR "[Mioga2::Router::handler -- $$] cleaned uri: " . $r->uri () . "\n" if ($debug > 1);

	#-------------------------------------------------------------------------------
	# Handler /dav-mioga2 url
	#-------------------------------------------------------------------------------
	if ($uri =~ /^\/dav_mioga2/) {
		print STDERR "[Mioga2::Router::handler -- $$] URI $uri is /dav_mioga2\n" if ($debug > 1);
		return Apache2::Const::DECLINED;
	}

	#-------------------------------------------------------------------------------
	# Handler /ext url
	#-------------------------------------------------------------------------------
	my $ext_handler = $miogaconf->GetExternalURI ();
	if ($uri =~ /^$ext_handler/) {
		print STDERR "[Mioga2::Router::handler -- $$] URI $uri is handled externally\n" if ($debug > 1);
		return Apache2::Const::DECLINED;
	}

	#-------------------------------------------------------------------------------
	# Don't handle URI if it doesn't begin with base_uri
	#-------------------------------------------------------------------------------
	my $base_uri = $self->{miogaconf}->GetBasePath ();
	print STDERR "[Mioga2::Router::handler -- $$] Base URI is $base_uri\n" if ($debug > 1);
	if ($uri !~ /^$base_uri/) {
		print STDERR "[Mioga2::Router::handler -- $$] URI $uri is not a Mioga2 URI, giving up\n" if ($debug > 1);
		return Apache2::Const::DECLINED;
	}

	#-------------------------------------------------------------------------------
	# Globally set authen_web flag, will be unset for WebDAV access
	#-------------------------------------------------------------------------------
	$r->pnotes ('authen_web', exists ($self->{miogaconf}->{html_login}));

	#-------------------------------------------------------------------------------
	# URI is a Mioga2 URI, don't bother with base_uri anymore
	# Extract members for further processing
	#-------------------------------------------------------------------------------
	print STDERR "[Mioga2::Router::handler -- $$] URI $uri is a Mioga2 URI, starting deep inspection\n" if ($debug);
	$uri =~ s/^$base_uri//;
	print STDERR "[Mioga2::Router::handler -- $$] Relative URI is $uri  origine = ".$r->uri."\n" if ($debug);
	my @members = ($uri =~ m/\/([^\/?]+)/g);
	my $member_count = @members;
	print STDERR "[Mioga2::Router::handler -- $$] members count $member_count\n" if ($debug);
	#-------------------------------------------------------------------------------
	# base_uri access for MS Web folders
	#-------------------------------------------------------------------------------
	if ($member_count == 0) {
		if ($self->IsMSWebFolder ($r)) {
			#-------------------------------------------------------------------------------
			# Request is a MS-WebFolder trying to access above the instance name:
			# 	- disable Authen & Authz,
			# 	- handle the request with ie_handler (see below)
			#-------------------------------------------------------------------------------
			print STDERR "[Mioga2::Router::handler -- $$] Request is MS-WebFolder\n" if ($debug);
			$r->server->method_register ('PROPFIND');
			$r->handler ("perl-script");
			$r->set_handlers (PerlAuthenHandler => sub { return Apache2::Const::OK; });
			$r->set_handlers (PerlAuthzHandler => sub { return Apache2::Const::OK; });
			$r->user ('');
			$r->set_handlers (PerlResponseHandler => [ sub { $self->ie_handler ($r); } ]);
			return Apache2::Const::OK;
		}
		else {
			$r->log_error ("[Mioga2::Router::handler -- $$] Only base_uri request " . $r->uri ());
			return Apache2::Const::FORBIDDEN;
		}
	}
	#-------------------------------------------------------------------------------
	# Check for CDN resources
	#-------------------------------------------------------------------------------
	elsif ($self->IsCDNPath ($members[0])) {
		my $filename = $self->{miogaconf}->GetInstallDir () . $self->{miogaconf}->{cdn_uri} . '/' . join ('/', @members[1..(@members - 1)]);
		print STDERR "[Mioga2::Router::handler -- $$] First member is a CDN resource: $filename\n" if ($debug);
		$r->set_handlers (PerlAuthenHandler => sub { return Apache2::Const::OK; });
		$r->set_handlers (PerlAuthzHandler => sub { return Apache2::Const::OK; });
		$r->user ('');
		$r->filename ($filename);
		if (-e $filename) {
			$r->finfo(APR::Finfo::stat($filename, APR::Const::FINFO_NORM, $r->pool));
		}
		$r->headers_out->{'Cache-Control'} = 'max-age=31536000, public';	# Cache static data for 1 year
		return Apache2::Const::OK;
	}
	#-------------------------------------------------------------------------------
	# Login stuff
	#-------------------------------------------------------------------------------
	elsif ($self->IsLoginHandler ($members[0])) {
		print STDERR "[Mioga2::Router::handler -- $$] First member is the login handler\n" if ($debug);
		#-------------------------------------------------------------------------------
		# Login handler:
		# 	- disable Authen & Authz,
		# 	- handle request with Mioga2::Login or serve a file
		#     a file must be /files/xxxxxxxxxxx
		# Valid URL can be 1, 2 or 3 members : /Login, /Login/files/xxxx or /Login/METHOD
		#-------------------------------------------------------------------------------
		$r->set_handlers (PerlAuthenHandler => sub { return Apache2::Const::OK; });
		$r->set_handlers (PerlAuthzHandler => sub { return Apache2::Const::OK; });
		$r->user ('');

		if (($member_count > 2) && ($self->IsValidFilename ($members[1]))) {
			#-------------------------------------------------------------------------------
			# Third member is  resource:
			# 	- set the resource file path.
			#-------------------------------------------------------------------------------
			my $filename = $self->{miogaconf}->GetInstallDir () . $self->{miogaconf}->GetLoginURI () . '/files/' . $members[2];
			print STDERR "[Mioga2::Router::handler -- $$] Second member is a resource: $filename\n" if ($debug);
			$r->filename ($filename);
			if (-e $filename) {
				$r->finfo(APR::Finfo::stat($filename, APR::Const::FINFO_NORM, $r->pool));
			}
			return Apache2::Const::OK;
		}
		else {
			#-------------------------------------------------------------------------------
			# Second member can be a method:
			# 	- handle the request with Mioga2::Login.
			#-------------------------------------------------------------------------------
			print STDERR "[Mioga2::Router::handler -- $$] Second member is a method\n" if ($debug);
			$r->handler("perl-script");
			$r->set_handlers (PerlResponseHandler => 'Mioga2::Login');
			return Apache2::Const::OK;
		}
	}
	#-------------------------------------------------------------------------------
	# Cross-instance user stuf TODO 
	#-------------------------------------------------------------------------------
	elsif ($self->IsUserHandler ($members[0])) {
		#-------------------------------------------------------------------------------
		# User handler -- TODO
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::Router::handler -- $$] Member is the user handler\n" if ($debug);
		return Apache2::Const::FORBIDDEN;
	}
	#-------------------------------------------------------------------------------
	# Instance stuff
	#-------------------------------------------------------------------------------
	elsif ($self->IsValidInstance ($members[0])) {
		if ($member_count == 1) {
			if ($self->IsMSWebFolder ($r)) {
				#-------------------------------------------------------------------------------
				# Request is a MS-WebFolder trying to access above the instance name:
				# 	- disable Authen & Authz,
				# 	- handle the request with ie_handler (see below)
				#-------------------------------------------------------------------------------
				print STDERR "[Mioga2::Router::handler -- $$] Request is MS-WebFolder\n" if ($debug);
				$r->server->method_register ('PROPFIND');
				$r->handler ("perl-script");
				$r->set_handlers (PerlAuthenHandler => sub { return Apache2::Const::OK; });
				$r->set_handlers (PerlAuthzHandler => sub { return Apache2::Const::OK; });
				$r->user ('');
				$r->set_handlers (PerlResponseHandler => [ sub { $self->ie_handler ($r); } ]);
				return Apache2::Const::OK;
			}
			else {
				#-------------------------------------------------------------------------------
				# It is default redirection for instance
				# 	- disable Authen & Authz,
				# 	- handle request with Mioga2::Redirect.
				#-------------------------------------------------------------------------------
				print STDERR "[Mioga2::Router::handler -- $$] Member is an instance\n" if ($debug);
				my $data = {
					instance => $members[0],
				};
				$r->handler("perl-script");
				$r->pnotes ('redirect', 1);
				$r->set_handlers (PerlAuthenHandler => 'Mioga2::Authen');
				$r->set_handlers (PerlAuthzHandler => sub { return Apache2::Const::OK; });
				$r->set_handlers (PerlResponseHandler => [ sub { return Mioga2::Redirect->handler ($r, $data); } ]);
				return Apache2::Const::OK;
			}
		}
		#-------------------------------------------------------------------------------
		# More than one member could be webDAV access, method call or default group redirection
		#-------------------------------------------------------------------------------
		else {
			if ($members[1] eq 'home') {
				#-------------------------------------------------------------------------------
				# Second member is a private WebDAV domain:
				# 	- fallback to HTTP-Basic,
				# 	- enable Authen & Authz,
				# 	- handle request with Mioga2::DAVProxy.
				#-------------------------------------------------------------------------------
				print STDERR "[Mioga2::Router::handler -- $$] Second member is a private WebDAV domain\n" if ($debug);
				$r->handler("perl-script");
				$r->pnotes ('authen_web', 0);
				$r->set_handlers (PerlAuthenHandler => 'Mioga2::Authen');
				$r->set_handlers (PerlAuthzHandler => 'Mioga2::Authz');
				$r->set_handlers (PerlResponseHandler => 'Mioga2::DAVProxy');
				$r->set_handlers (PerlCleanupHandler => [ sub { unlink $r->pnotes('preserved_file_for_PUT') if (defined $r->pnotes('preserved_file_for_PUT')); } ]);

				$r->set_handlers (PerlLogHandler => 'Mioga2::LogHandler');
				return Apache2::Const::OK;
			}
			elsif ($members[1] eq 'public') {
				#-------------------------------------------------------------------------------
				# Second member is a public WebDAV domain ('public'):
				# 	- disable Authen & Authz unless request is protected,
				# 	- handle request with Mioga2::DAVProxy.
				#-------------------------------------------------------------------------------
				print STDERR "[Mioga2::Router::handler -- $$] Second member is a public WebDAV domain\n" if ($debug);
				$r->handler("perl-script");
				if ($self->IsProtectedDAVRequest ($r)) {
					$r->set_handlers (PerlAuthenHandler => 'Mioga2::Authen');
					$r->set_handlers (PerlAuthzHandler => 'Mioga2::Authz');
				}
				else {
					$r->set_handlers (PerlAuthenHandler => sub { return Apache2::Const::OK; });
					$r->set_handlers (PerlAuthzHandler => sub { return Apache2::Const::OK; });
					$r->user ('');
				}
				#$r->set_handlers (PerlFixupHandler => 'Mioga2::DAVProxy');
				$r->set_handlers (PerlResponseHandler => 'Mioga2::DAVProxy');

				$r->set_handlers (PerlLogHandler => 'Mioga2::LogHandler');
				return Apache2::Const::OK;
			}
			elsif ($members[1] eq 'bin') {
				#-------------------------------------------------------------------------------
				# Second member is a private application domain:
				# 	- enable Authen & Authz,
				# 	- handle the request with Mioga2::Dispatch.
				#-------------------------------------------------------------------------------
				print STDERR "[Mioga2::Router::handler -- $$] Second member is a private application domain\n" if ($debug);
				if ($member_count > 5) {
					print STDERR "[Mioga2::Router::handler -- $$] Bad request URI\n" if ($debug);
					return Apache2::Const::HTTP_FORBIDDEN;
				}
				$r->handler("perl-script");
				#TODO Put an array of execption application in config
				if ($members[3] eq 'RSS') {
					$r->pnotes ('authen_web', 0);
				}
				$r->set_handlers (PerlAuthenHandler => 'Mioga2::Authen');
				$r->set_handlers (PerlAuthzHandler => 'Mioga2::Authz');
				$r->set_handlers (PerlResponseHandler => 'Mioga2::Dispatch');

				$r->set_handlers (PerlLogHandler => 'Mioga2::LogHandler');
				return Apache2::Const::OK;
			}
			elsif ($members[1] eq 'pubbin') {
				#-------------------------------------------------------------------------------
				# Second member is a public application domain:
				# 	- disable Authen & Authz,
				# 	- handle the request with Mioga2::Dispatch.
				#-------------------------------------------------------------------------------
				print STDERR "[Mioga2::Router::handler -- $$] Second member is a public application domain\n" if ($debug);
				if ($member_count > 5) {
					print STDERR "[Mioga2::Router::handler -- $$] Bad request URI\n" if ($debug);
					return Apache2::Const::HTTP_FORBIDDEN;
				}
				$r->handler("perl-script");
				$r->set_handlers (PerlAuthenHandler => sub { return Apache2::Const::OK; });
				$r->set_handlers (PerlAuthzHandler => sub { return Apache2::Const::OK; });
				$r->user ('');
				$r->set_handlers (PerlResponseHandler => 'Mioga2::Dispatch');

				$r->set_handlers (PerlLogHandler => 'Mioga2::LogHandler');
				return Apache2::Const::OK;
			}
			elsif ($members[1] eq 'sxml') {
				#-------------------------------------------------------------------------------
				# Second member is a SXML domain:
				# 	- disable Authen & Authz,
				# 	- handle the request with Mioga2::Dispatch.
				#-------------------------------------------------------------------------------
				print STDERR "[Mioga2::Router::handler -- $$] Second member is a SXML domain\n" if ($debug);
				$r->handler("perl-script");
				$r->set_handlers (PerlAuthenHandler => sub { return Apache2::Const::OK; });
				$r->set_handlers (PerlAuthzHandler => sub { return Apache2::Const::OK; });
				$r->user ('');
				$r->set_handlers (PerlResponseHandler => 'Mioga2::Dispatch');

				$r->set_handlers (PerlLogHandler => 'Mioga2::LogHandler');
				return Apache2::Const::OK;
			}
			elsif ($self->IsValidResource ($members[1])) {
				my $subdir = ($members[1] eq 'jslib') ? '/' . $members[1] : $self->{miogaconf}->{$members[1] . '_dir'};
				my $filename = $self->{miogaconf}->GetInstallDir () . '/' . $members[0] . '/web/htdocs' . $subdir . '/' . join ("/", @members[2..($member_count - 1)]);
				print STDERR "[Mioga2::Router::handler -- $$] Second member is a resource. Associated filename is $filename\n" if ($debug);
				$r->set_handlers (PerlAuthenHandler => sub { return Apache2::Const::OK; });
				$r->set_handlers (PerlAuthzHandler => sub { return Apache2::Const::OK; });
				$r->user ('');
				$r->filename ($filename);
				if (-e $filename) {
					$r->finfo(APR::Finfo::stat($filename, APR::Const::FINFO_NORM, $r->pool));
				}
				return Apache2::Const::OK;
			}
			elsif ( ($member_count == 2) && ($self->IsValidGroup ($members[0], $members[1]))) {
				#-------------------------------------------------------------------------------
				# Only one second member and it is a group name:
				# 	- disable Authen & Authz,
				# 	- handle the request with Mioga2::Redirect.
				#-------------------------------------------------------------------------------
				print STDERR "[Mioga2::Router::handler -- $$] Second member is a group\n" if ($debug);
				my $data = {
					instance => $members[0],
					group    => $members[1],
				};
				$r->handler("perl-script");
				$r->pnotes ('redirect', 1);
				$r->set_handlers (PerlAuthenHandler => 'Mioga2::Authen');
				$r->set_handlers (PerlAuthzHandler => sub { return Apache2::Const::OK; });
				$r->set_handlers (PerlResponseHandler => [ sub { return Mioga2::Redirect->handler ($r, $data); } ]);

				$r->set_handlers (PerlLogHandler => 'Mioga2::LogHandler');
				return Apache2::Const::OK;
			}
			elsif (($member_count >= 2) &&  ($self->IsUserHandler ($members[1]))) {
				#-------------------------------------------------------------------------------
				# User handler within an instance
				# 	- disable Authen & Authz,
				# 	- handle the request with Mioga2::Redirect.
				#-------------------------------------------------------------------------------
				print STDERR "[Mioga2::Router::handler -- $$] Second member is the user handler\n" if ($debug);
				my $data = {
					instance => $members[0],
					user     => '__MIOGA-USER__',
				};

				# If a third member is present pass it as application name to the redirect handler
				$data->{application} = $members[2] if ($members[2]);

				$r->handler("perl-script");
				$r->pnotes ('redirect', 1);
				$r->set_handlers (PerlAuthenHandler => 'Mioga2::Authen');
				$r->set_handlers (PerlAuthzHandler => sub { return Apache2::Const::OK; });
				$r->set_handlers (PerlResponseHandler => [ sub { return Mioga2::Redirect->handler ($r, $data); } ]);

				$r->set_handlers (PerlLogHandler => 'Mioga2::LogHandler');
				return Apache2::Const::OK;
			}
			else {
				#-------------------------------------------------------------------------------
				# Invalid URI:
				# 	- return Mioga2::Apache::HTTP_FORBIDDEN as it can be a typing mistake from a valid user.
				#-------------------------------------------------------------------------------
				$r->log_error ("[Mioga2::Router::handler -- $$] Rejecting (HTTP_FORBIDDEN) request to " . $r->uri () . ": Second member is invalid");
				return Apache2::Const::NOT_FOUND;
			}
		}
	}
	#-------------------------------------------------------------------------------
	# Internally-dedicated stuff
	#-------------------------------------------------------------------------------
	elsif ("/$members[0]" eq $miogaconf->GetInternalURI ()) {
		print STDERR "[Mioga2::Router::handler -- $$] URI $uri is internally-dedicated\n" if ($debug > 1);
		if ($members[1] eq 'url') {
			my $data = {
				url_hash => $members[2],
			};

			$r->handler("perl-script");
			$r->set_handlers (PerlAuthenHandler => sub { return Apache2::Const::OK; });
			$r->set_handlers (PerlAuthzHandler => sub { return Apache2::Const::OK; });
			$r->user ('');
			$r->set_handlers (PerlResponseHandler => [ sub { return Mioga2::Redirect->handler ($r, $data); } ]);

			$r->set_handlers (PerlLogHandler => 'Mioga2::LogHandler');
			return Apache2::Const::OK;
		}
		else {
			#-------------------------------------------------------------------------------
			# Invalid URI:
			# 	- return Apache2::Const::NOT_FOUND as it can be a typing mistake from a valid user.
			#-------------------------------------------------------------------------------
			$r->log_error ("[Mioga2::Router::handler -- $$] Rejecting (HTTP_NOT_FOUND) request to " . $r->uri () . ": Second member is invalid");
			return Apache2::Const::NOT_FOUND;
		}
	}
	#-------------------------------------------------------------------------------
	# Otherwise, bad URI, access is not granted
	#-------------------------------------------------------------------------------
	else {
		#-------------------------------------------------------------------------------
		# Invalid URI:
		# 	- return Apache2::Const::NOT_FOUND as it can be a typing mistake from a valid user.
		#-------------------------------------------------------------------------------
		$r->log_error ("[Mioga2::Router::handler -- $$] Rejecting (HTTP_NOT_FOUND) request to " . $r->uri () . ": First member is invalid");
		return Apache2::Const::NOT_FOUND;
	}
	#-------------------------------------------------------------------------------
	# If we arrive here => crash
	#-------------------------------------------------------------------------------
	$r->log_error ("[Mioga2::Router::handler -- $$] No rule matches request " . $r->uri ());
	return Apache2::Const::SERVER_ERROR;
}


#===============================================================================

=head2 IsValidFilename

Check if member is a valid filename. This doesn't ensure file exists but checks it looks like a filename.

=cut

#===============================================================================
sub IsValidFilename {
	my ($self, $member) = @_;

	my $status = 0;

	#if ($filename =~ /^[\w\-_]+\.[\w\-_]+$/) {
		#$status = 1;
	#}

	#return ($status);
	return ("$member" eq 'files');
}	# ----------  end of subroutine IsValidFilename  ----------


#===============================================================================

=head2 IsValidInstance

Check if a member is a valid instance.

=cut

#===============================================================================
sub IsValidInstance {
	my ($self, $member) = @_;

	my $instance = $self->{db}->SelectMultiple ("SELECT * FROM m_mioga WHERE ident = ?;", [$member]);

	return (@$instance);
}	# ----------  end of subroutine IsValidInstance  ----------


#===============================================================================

=head2 IsValidGroup

Check if a pair of members is a valid instance / group.

=cut

#===============================================================================
sub IsValidGroup {
	my ($self, $member1, $member2) = @_;

	my $group = $self->{db}->SelectMultiple ("SELECT m_group.ident FROM m_group, m_group_type, m_mioga WHERE m_group.type_id = m_group_type.rowid AND m_group.mioga_id = m_mioga.rowid AND m_group_type.ident = 'group' AND m_mioga.ident = ? AND m_group.ident = ?;", [$member1, $member2]);

	return (@$group);
}	# ----------  end of subroutine IsValidGroup  ----------


#===============================================================================

=head2 IsLoginHandler

Check if a member is the login handler

=cut

#===============================================================================
sub IsLoginHandler {
	my ($self, $member) = @_;

	return ("/$member" eq $self->{miogaconf}->GetLoginURI ());
}	# ----------  end of subroutine IsLoginHandler  ----------


#===============================================================================

=head2 IsUserHandler

Check if a member is the user handler.

=cut

#===============================================================================
sub IsUserHandler {
	my ($self, $member) = @_;

	return ("/$member" eq $self->{miogaconf}->GetUserURI ());
}	# ----------  end of subroutine IsUserHandler  ----------


#===============================================================================

=head2 IsValidResource

Check if a member is a valid resource category.

=cut

#===============================================================================
sub IsValidResource {
	my ($self, $member) = @_;

	my $status = 0;

	if (grep (/^$member$/, qw/themes help jslib/)) {
		$status = 1;
	}

	return ($status);
}	# ----------  end of subroutine IsValidResource  ----------


#===============================================================================

=head2 CleanupURI

Remove rubbish from URI

=cut

#===============================================================================
sub Cleanup2URI {
	my ($self, $r) = @_;

	my $uri = $r->uri ();

	$uri =~ s/\.\.//g;
	$uri =~ s/[\/]+/\//g;

	return $uri;
}	# ----------  end of subroutine CleanupURI  ----------
sub CleanupURI {
	my ($self, $r) = @_;

	my $uri = $r->uri ();

	$uri =~ s/\.\.//g;
	$uri =~ s/[\/]+/\//g;

	$r->uri ($uri);
}	# ----------  end of subroutine CleanupURI  ----------


#===============================================================================

=head2 IsProtectedDAVRequest

Test if request is a protected WebDAV. A protected WebDAV request is a request
for which access to 'public' needs to be authenticated.

=cut

#===============================================================================
sub IsProtectedDAVRequest {
	my ($self, $r) = @_;

	my $status = 0;

	my $method = $r->method;
	if (!grep (/^$method$/, qw/GET PROPFIND OPTIONS/)) {
		print STDERR "[Mioga2::Router::IsProtectedDAVRequest -- $$] Request $method on " . $r->uri () . " is protected\n" if ($debug);
		$status = 1;
	}

	return ($status);
}	# ----------  end of subroutine IsProtectedDAVRequest  ----------


#===============================================================================

=head2 IsMSWebFolder

Test if request is a MS-WebFolder

=cut

#===============================================================================
sub IsMSWebFolder {
	my ($self, $r) = @_;

	my $status = 0;

	my $method = $r->method;
	if (grep (/^$method$/, qw/PROPFIND OPTIONS/)) {
		$status = 1;
	}

	return ($status);
}	# ----------  end of subroutine IsMSWebFolder  ----------


#===============================================================================

=head2 ie_handler

Special PerlResponseHandler for MS-WebFolder accessing Mioga root (ie above the
instance part of the URL).

=cut

#===============================================================================
sub ie_handler : method {
	my ($self, $r) = @_;

	print STDERR "[Mioga2::Router::ie_handler -- $$] Sending HTTP status 200\n" if ($debug);

	$r->status (200);
	return Apache2::Const::OK;
}


#===============================================================================

=head2 IsCDNPath

Check if a member is the CDN URI

=cut

#===============================================================================
sub IsCDNPath {
	my ($self, $member) = @_;

	my $status = 0;

	if (((ref ($self->{miogaconf}->{cdn_server}) eq 'HASH') || (-z $self->{miogaconf}->{cdn_server})) && ("/$member" eq $self->{miogaconf}->{cdn_uri})) {
		$status = 1;
	}

	return ($status);
}	# ----------  end of subroutine IsCDNPath  ----------


1;

