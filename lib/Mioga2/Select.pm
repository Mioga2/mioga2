# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
	
Admin.pm: The Mioga2 Administration application.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Select;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'select';

use Error qw(:try);
use Data::Dumper;
use POSIX qw(tmpnam);

use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::SimpleLargeList;
use Mioga2::LargeList::ValidateSelectList;
use Mioga2::tools::database;
use Mioga2::Old::Group;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::APIAuthz;
use Mioga2::Exception::Group;
use Mioga2::Exception::User;
use Mioga2::Exception::Resource;
use Mioga2::Exception::Simple;

my $debug = 0;

# ============================================================================
# Users list description
# ============================================================================

my $users_single_fields = [ [ 'rowid', 'rowid' ], [ 'ident', 'link', 'SelectSingleUser', \&IsSelectable ], [ 'firstname', 'normal' ], [ 'lastname', 'normal' ], [ 'email', 'email' ], ];

my $users_multiple_fields = [ [ 'rowid', 'rowid' ], [ 'rowid', 'simple_checkboxes', \&IsSelectable ], [ 'ident', 'normal' ], [ 'firstname', 'normal' ], [ 'lastname', 'normal' ], [ 'email', 'email' ], ];

my $users_validate_fields = [ [ 'rowid', 'rowid' ], [ 'user_ident', 'user_ident', 'rowid', 'normal' ], [ 'user_firstname', 'user_firstname', 'rowid', 'normal' ], [ 'user_lastname', 'user_lastname', 'rowid', 'normal' ], [ 'user_email', 'user_email', 'rowid', 'email' ], [ 'delete', 'delete' ], ];

my $users_search = [ 'ident', 'firstname', 'lastname' ];

my %users_sll_desc = (
    'name' => 'mioga_select_user_sll',

    'sql_request' => {
        'select' => "m_user_base.*",

        "select_count" => "count(m_user_base.rowid)",

        "from" => "m_user_base, m_user_status",

        "where" => "m_user_status.rowid = m_user_base.status_id AND " . "m_user_status.ident = 'active' AND " . "m_user_base.mioga_id = ?",

    },

    'fields' => $users_single_fields,

    'search' => $users_search,

);

# ============================================================================
# Users member of group list description
# ============================================================================

my $users_member_multiple_fields = [ [ 'rowid', 'rowid' ], [ 'rowid', 'simple_checkboxes', \&IsSelectable ], [ 'ident', 'normal' ], [ 'firstname', 'normal' ], [ 'lastname', 'normal' ], [ 'email', 'email' ], [ 'profile', 'normal' ], ];

my $users_member_single_fields = [ [ 'rowid', 'rowid' ], [ 'ident', 'link', 'SelectSingleUserMemberOfGroup', \&IsSelectable ], [ 'firstname', 'normal' ], [ 'lastname', 'normal' ], [ 'email', 'email' ], [ 'profile', 'normal' ], ];

my %users_member_sll_desc = (
    'name' => 'mioga_select_user_member_sll',

    'sql_request' => {
        'select' => "m_user_base.*, m_profile.ident AS profile",

        "select_count" => "count(m_user_base.rowid)",

        "from" => "m_user_base, m_user_status, m_group_group, m_profile, m_profile_group",

        "where" => "m_user_status.rowid = m_user_base.status_id AND " . "m_user_status.ident = 'active' AND " . "m_user_base.rowid = m_group_group.invited_group_id AND " . "m_group_group.group_id = ? AND " . "m_profile.group_id = m_group_group.group_id AND " . "m_profile_group.group_id = m_user_base.rowid AND " . "m_profile.rowid = m_profile_group.profile_id",

    },

    'default_sort_field' => 'lastname',

    'fields' => $users_member_multiple_fields,

    'search' => $users_search,

);

# ============================================================================
# Extented Users member of group list description
# ============================================================================

my $users_extended_member_single_fields = [ [ 'rowid', 'rowid' ], [ 'ident', 'link', 'SelectSingleUserMemberOfGroup', \&IsSelectable ], [ 'firstname', 'normal' ], [ 'lastname', 'normal' ], [ 'email', 'email' ], ];
my %users_extended_member_sll_desc = (
    'name' => 'mioga_select_extended_user_member_sll',

    'sql_request' => {
        'select' => "DISTINCT users.* AS rowid",

        "select_count" => "count(*)",

        "from" => " ( SELECT DISTINCT m_user_base.* FROM m_group_group, m_user_base " . "    WHERE  m_group_group.group_id = ? AND " . "           m_user_base.rowid = m_group_group.invited_group_id " .

          " UNION " .

          "  SELECT DISTINCT m_user_base.* FROM m_group_expanded_user, m_user_base " . "   WHERE m_group_expanded_user.group_id = ? AND " . "         m_user_base.rowid = m_group_expanded_user.invited_group_id " . " ) AS users ",

        "where" => "'t'"
    },

    'fields' => $users_extended_member_single_fields,

    'default_sort_field' => 'lastname',

    'search' => $users_search,

);

# ============================================================================
# Users member of team list description
# ============================================================================

my $users_member_team_multiple_fields = [ [ 'rowid', 'rowid' ], [ 'rowid', 'simple_checkboxes', \&IsSelectable ], [ 'ident', 'normal' ], [ 'firstname', 'normal' ], [ 'lastname', 'normal' ], [ 'email', 'email' ], ];

my $users_member_team_single_fields = [ [ 'rowid', 'rowid' ], [ 'ident', 'link', 'SelectSingleUserMemberOfGroup', \&IsSelectable ], [ 'firstname', 'normal' ], [ 'lastname', 'normal' ], [ 'email', 'email' ], ];

my %users_member_team_sll_desc = (
    'name' => 'mioga_select_user_member_sll',

    'sql_request' => {
        'select' => "m_user_base.*",

        "select_count" => "count(m_user_base.rowid)",

        "from" => "m_user_base, m_user_status, m_group_group",

        "where" => "m_user_status.rowid = m_user_base.status_id AND " . "m_user_status.ident = 'active' AND " . "m_user_base.rowid = m_group_group.invited_group_id AND " . "m_group_group.group_id = ?",

    },

    'fields' => $users_member_team_multiple_fields,

    'default_sort_field' => 'lastname',

    'search' => $users_search,

);

# ============================================================================
# Users not member of group list description
# ============================================================================

my $users_not_member_multiple_fields = [ [ 'rowid', 'rowid' ], [ 'rowid', 'simple_checkboxes', \&IsSelectable ], [ 'ident', 'normal' ], [ 'firstname', 'normal' ], [ 'lastname', 'normal' ], [ 'email', 'email' ], ];

my %users_not_member_sll_desc = (
    'name' => 'mioga_select_user_not_member_sll',

    'sql_request' => {
        'select' => "m_user_base.*",

        "select_count" => "count(m_user_base.rowid)",

        "from" => "m_user_base, m_user_status",

        "where" => "m_user_status.rowid = m_user_base.status_id AND " . "m_user_status.ident = 'active' AND " . "m_user_base.rowid  NOT IN ( " . "          SELECT m_group_group.invited_group_id " . "          FROM   m_group_group " . "          WHERE  m_group_group.group_id = ? " . "          ) AND " . "m_user_base.rowid != ? AND " . "m_user_base.mioga_id = ?",

    },

    'fields' => $users_not_member_multiple_fields,

    'default_sort_field' => 'lastname',

    'search' => $users_search,

);

# ============================================================================
# Groups list description
# ============================================================================

my $groups_single_fields = [ [ 'rowid', 'rowid' ], [ 'ident', 'link', 'SelectSingleGroup', \&IsSelectable ], [ 'anim_ident', 'user_ident', 'm_group.anim_id' ], ];

my $groups_multiple_fields = [ [ 'rowid', 'rowid' ], [ 'rowid', 'simple_checkboxes', \&IsSelectable ], [ 'ident', 'normal' ], [ 'anim_ident', 'user_ident', 'm_group.anim_id' ], ];

my $groups_validate_fields = [ [ 'rowid', 'rowid' ], [ 'group_ident', 'group_ident', 'rowid', 'normal' ], [ 'anim_ident', 'user_ident', 'm_group.anim_id' ], [ 'delete', 'delete' ], ];

my $groups_search = [ 'ident', 'anim_ident' ];

my %groups_sll_desc = (
    'name' => 'mioga_select_group_sll',

    'sql_request' => {
        'select' => "m_group.*",

        "select_count" => "count(m_group.rowid)",

        "from" => "m_group, m_group_type, m_group_status",

        "where" => "m_group_status.rowid = m_group.status_id AND " . "m_group_status.ident = 'active' AND " . "m_group.type_id = m_group_type.rowid AND " . "m_group_type.ident = 'group' AND " . "m_group.mioga_id = ?",

    },

    'fields' => $groups_single_fields,

    'search' => $groups_search,

);

# ============================================================================
# Teams list description
# ============================================================================

my $teams_single_fields = [ [ 'rowid', 'rowid' ], [ 'ident', 'link', 'SelectSingleTeam', \&IsSelectable ], ];

my $teams_multiple_fields = [ [ 'rowid', 'rowid' ], [ 'rowid', 'simple_checkboxes', \&IsSelectable ], [ 'ident', 'normal' ], ];

my $teams_validate_fields = [ [ 'rowid', 'rowid' ], [ 'team_ident', 'team_ident', 'rowid', 'normal' ], [ 'delete', 'delete' ], ];

my $teams_search = ['ident'];

my %teams_sll_desc = (
    'name' => 'mioga_select_team_sll',

    'sql_request' => {
        'select' => "m_group.*",

        "select_count" => "count(m_group.rowid)",

        "from" => "m_group, m_group_type, m_group_status",

        "where" => "m_group_status.rowid = m_group.status_id AND " . "m_group_status.ident = 'active' AND " . "m_group.type_id = m_group_type.rowid AND " . "m_group_type.ident = 'team' AND " . "m_group.mioga_id = ?",

    },

    'fields' => $teams_single_fields,

    'search' => $teams_search,

);

# ============================================================================
# Groups for user list description
# ============================================================================

my %groups_for_user_sll_desc = (
    'name' => 'mioga_select_group_for_user_sll',

    'sql_request' => {
        'select' => "m_group.*",

        "select_count" => "count(m_group.rowid)",

        "from" => "m_group, m_group_type, m_group_status, m_group_group",

        "where" => "m_group_status.rowid = m_group.status_id AND " . "m_group_status.ident = 'active' AND " . "m_group.type_id = m_group_type.rowid AND " . "m_group_type.ident = 'group' AND " . "m_group.rowid = m_group_group.group_id AND " . "m_group_group.invited_group_id = ?",

    },

    'fields' => $groups_single_fields,

    'search' => $groups_search,

);

# ============================================================================
# Groups for user list description
# ============================================================================

my %groups_for_user_app_func_sll_desc = (
    'name' => 'mioga_select_group_for_user_app_func_sll',

    'sql_request' => {
        'select' => "m_group.*",

        "select_count" => "count(m_group.rowid)",

        "from" => "m_group, m_group_type, m_group_status ",

        "where" => "m_group_status.rowid = m_group.status_id AND " . "m_group_status.ident = 'active' AND " . "m_group.type_id = m_group_type.rowid AND " . "m_group_type.ident = 'group' AND " . "m_group.rowid IN ( " . "   SELECT m_profile_user_function.group_id " . "   FROM   m_profile_user_function " . "   WHERE  m_profile_user_function.user_id = ? AND " . "          m_profile_user_function.app_ident = ? AND " . "          m_profile_user_function.function_ident = ? " .

          "   UNION " .

          "   SELECT m_profile_expanded_user_functio.group_id " . "   FROM   m_profile_expanded_user_functio " . "   WHERE  m_profile_expanded_user_functio.user_id = ? AND " . "          m_profile_expanded_user_functio.app_ident = ? AND " . "          m_profile_expanded_user_functio.function_ident = ? " . " ) ",

    },

    'fields' => $groups_single_fields,

    'search' => $groups_search,

);

# ============================================================================
# Teams for user list description
# ============================================================================

my %teams_for_user_sll_desc = (
    'name' => 'mioga_select_team_for_user_sll',

    'sql_request' => {
        'select' => "m_group.*",

        "select_count" => "count(m_group.rowid)",

        "from" => "m_group, m_group_type, m_group_status, m_group_group",

        "where" => "m_group_status.rowid = m_group.status_id AND " . "m_group_status.ident = 'active' AND " . "m_group.type_id = m_group_type.rowid AND " . "m_group_type.ident = 'team' AND " . "m_group.rowid = m_group_group.group_id AND " . "m_group_group.invited_group_id = ?",

    },

    'fields' => $teams_single_fields,

    'search' => $teams_search,

);

# ============================================================================
# Groups memberlist description
# ============================================================================

my $groups_member_multiple_fields = [ [ 'rowid', 'rowid' ], [ 'rowid', 'simple_checkboxes', \&IsSelectable ], [ 'ident', 'normal' ], [ 'anim_ident', 'user_ident', 'm_group.anim_id' ], [ 'profile', 'normal' ], ];

my %groups_member_sll_desc = (
    'name' => 'mioga_select_group_member_sll',

    'sql_request' => {
        'select' => "m_group.*, m_profile.ident",

        "select_count" => "count(m_group.rowid)",

        "from" => "m_group, m_group_type, m_group_status, m_profile, m_profile_group, m_group_group",

        "where" => "m_group_status.rowid = m_group.status_id AND " . "m_group_status.ident = 'active' AND " . "m_group.type_id = m_group_type.rowid AND " . "m_group_type.ident = 'group' AND " . "m_group_group.group_id = ? AND " . "m_group.rowid = m_group_group.invited_group_id AND " . "m_profile.group_id = m_group_group.group_id AND " . "m_profile_group.group_id = m_group.rowid AND " . "m_profile.rowid = m_profile_group.profile_id",

    },

    'fields' => $groups_member_multiple_fields,

    'search' => $groups_search,

);

# ============================================================================
# Teams memberlist description
# ============================================================================

my $teams_member_multiple_fields = [ [ 'rowid', 'rowid' ], [ 'rowid', 'simple_checkboxes', \&IsSelectable ], [ 'ident', 'normal' ], [ 'profile', 'normal' ], ];

my %teams_member_sll_desc = (
    'name' => 'mioga_select_team_member_sll',

    'sql_request' => {
        'select' => "m_group.*",

        "select_count" => "count(m_group.rowid)",

        "from" => "m_group, m_group_type, m_group_status, m_group_group",

        "where" => "m_group_status.rowid = m_group.status_id AND " . "m_group_status.ident = 'active' AND " . "m_group.type_id = m_group_type.rowid AND " . "m_group_type.ident = 'team' AND " . "m_group_group.group_id = ? AND " . "m_group.rowid = m_group_group.invited_group_id",

    },

    'fields' => $teams_member_multiple_fields,

    'search' => $teams_search,

);

# ============================================================================
# Groups not member of group list description
# ============================================================================

my $teams_not_member_multiple_fields = [ [ 'rowid', 'rowid' ], [ 'rowid', 'simple_checkboxes', \&IsSelectable ], [ 'ident', 'normal' ], ];

my %teams_not_member_sll_desc = (
    'name' => 'mioga_select_team_not_member_sll',

    'sql_request' => {

        'select' => "m_group.*",

        "select_count" => "count(m_group.rowid)",

        "from" => "m_group, m_group_type, m_group_status",

        "where" => "m_group_status.rowid = m_group.status_id AND " . "m_group_status.ident = 'active' AND " . "m_group.type_id = m_group_type.rowid AND " . "m_group_type.ident = 'team' AND " . "m_group.rowid  NOT IN ( " . "          SELECT m_group_group.invited_group_id " . "          FROM   m_group_group " . "          WHERE  m_group_group.group_id = ? " . "          ) AND " . "m_group.mioga_id = ?",

    },

    'fields' => $teams_not_member_multiple_fields,

    'search' => $teams_search,

);

# ============================================================================
# Resources list description
# ============================================================================

my $resources_single_fields = [ [ 'rowid', 'rowid' ], [ 'ident', 'link', 'SelectSingleResource', \&IsSelectable ], [ 'location', 'normal' ], [ 'anim_ident', 'user_ident', 'm_resource.anim_id' ], ];

my $resources_multiple_fields = [ [ 'rowid', 'rowid' ], [ 'rowid', 'simple_checkboxes', \&IsSelectable ], [ 'ident', 'normal' ], [ 'location', 'normal' ], [ 'anim_ident', 'user_ident', 'm_resource.anim_id' ], ];

my $resources_validate_fields = [ [ 'rowid', 'rowid' ], [ 'resource_ident', 'resource_ident', 'rowid', 'normal' ], [ 'resource_location', 'resource_location', 'rowid', 'normal' ], [ 'anim_ident', 'user_ident', 'm_resource.anim_id' ], [ 'delete', 'delete' ], ];

my $resources_search = [ 'ident', 'location', 'anim_ident' ];

my %resources_sll_desc = (
    'name' => 'mioga_select_resource_sll',

    'sql_request' => {
        'select' => "m_resource.*",

        "select_count" => "count(m_resource.rowid)",

        "from" => "m_resource, m_resource_status",

        "where" => "m_resource_status.rowid = m_resource.status_id AND " . "m_resource_status.ident = 'active' AND " . "m_resource.mioga_id = ?",

    },

    'fields' => $resources_single_fields,

    'search' => $resources_search,

);

# ============================================================================
# Resources allowed in a group list description
# ============================================================================
my %resources_allowed_sll_desc = (
    'name' => 'mioga_select_resource_allowed_sll',

    'sql_request' => {
        'select' => "m_resource.*",

        "select_count" => "count(*)",

        "from" => "m_resource, m_resource_status, m_group_group",

        "where" => "m_resource_status.rowid = m_resource.status_id AND " . "m_resource_status.ident = 'active' AND " . "m_resource.rowid = m_group_group.group_id AND " . "m_group_group.invited_group_id = ?",

    },

    'fields' => [ [ 'rowid', 'rowid' ], [ 'ident', 'link', 'SelectMultipleResourcesAllowedInGroup', \&IsSelectable ], [ 'location', 'normal' ], [ 'anim_ident', 'user_ident', 'm_resource.anim_id' ], ],

    'search' => $resources_search,

);

# ============================================================================
# Resources not member of resource list description
# ============================================================================

my $resources_not_member_multiple_fields = [ [ 'rowid', 'rowid' ], [ 'rowid', 'simple_checkboxes', \&IsSelectable ], [ 'ident', 'normal' ], [ 'anim_ident', 'user_ident', 'm_resource.anim_id' ], ];

my %resources_not_member_sll_desc = (
    'name' => 'mioga_select_resource_not_member_sll',

    'sql_request' => {

        'select' => "m_resource.*",

        "select_count" => "count(m_resource.rowid)",

        "from" => "m_resource, m_group_status",

        "where" => "m_group_status.rowid = m_resource.status_id AND " . "m_group_status.ident = 'active' AND " . "m_resource.rowid  NOT IN ( " . "          SELECT m_group_group.group_id " . "          FROM   m_group_group " . "          WHERE  m_group_group.invited_group_id = ? " . "          ) AND " . "m_resource.rowid != ? AND " . "m_resource.mioga_id = ?",

    },

    'fields' => $resources_not_member_multiple_fields,

    'search' => $resources_search,

);

# ============================================================================
# External Mioga list description
# ============================================================================

my %external_mioga_sll_desc = (
    'name' => 'mioga_select_external_mioga_sll',

    'sql_request' => {
        'select' => "m_external_mioga.*",

        "select_count" => "count(m_external_mioga.rowid)",

        "from" => "m_external_mioga, m_external_mioga_status",

        "where" => "m_external_mioga_status.rowid = m_external_mioga.status_id AND " . "m_external_mioga_status.ident = 'active' AND " . "m_external_mioga.mioga_id = ?",

    },

    'fields' => [ [ 'rowid', 'rowid' ], [ 'server', 'link', 'SelectSingleExternalMioga' ], ],

    'search' => ['server'],

);

# ============================================================================
# External Mioga Users List
# ============================================================================

my $external_mioga_users_multiple_fields = [ [ 'rowid', 'rowid' ], [ 'rowid', 'simple_checkboxes', \&IsSelectable ], [ 'ident', 'normal' ], [ 'firstname', 'normal' ], [ 'lastname', 'normal' ], [ 'email', 'email' ], ];

my $external_mioga_users_validate_fields = [ [ 'rowid', 'rowid' ], [ 'ident', 'normal' ], [ 'firstname', 'normal' ], [ 'lastname', 'normal' ], [ 'email', 'email' ], [ 'delete', 'delete' ], ];

my $external_mioga_users_search_fields = [ 'ident', 'lastname', 'firstname' ];

my %external_mioga_users_sll_desc = (
    'name' => 'external_mioga_users_sll',

    'fields' => $external_mioga_users_multiple_fields,

    'search' => $external_mioga_users_search_fields,

    'default_sort_field' => 'lastname',

);

# ============================================================================

=head2 GetAppDesc ()

	Return the current application description.

=cut

# ============================================================================

sub GetAppDesc {
    my ($self, $context) = @_;

    my %AppDesc = (
        ident              => 'Select',
        package            => 'Mioga2::Select',
        type               => 'library',
        description        => __('The Mioga2 Users Groups and Resources selection lists'),
        all_groups         => 1,
        all_users          => 1,
        can_be_public      => 0,
		is_user            => 1,
		is_group           => 1,
		is_resource        => 0,

        functions => { 'All' => __('All Functions') },

        func_methods => { 'All' => [ 'SelectSingleUser', 'SelectSingleTeam', 'SelectSingleGroup', 'SelectSingleResource', 'SelectSingleExternalMioga', 'SelectMultipleUsers', 'SelectMultipleTeams', 'SelectMultipleGroups', 'SelectMultipleResources', 'SelectMultipleResourcesAllowedInGroup', 'SelectMultipleResourcesNotInGroup', 'SelectMultipleTeamsNotInGroup', 'SelectMultipleUsersNotInGroup', 'SelectSingleUserMemberOfGroup', 'SelectSingleExtendedUserMemberOfGroup', 'SelectSingleGroupForUserWithAppliAndFunc', 'SelectSingleUserMemberOfTeam', 'SelectSingleGroupForUser', 'SelectMultipleUsersMemberOfGroup', 'SelectMultipleUsersMemberOfTeam', 'SelectMultipleTeamsMemberOfGroup', 'SelectMultipleExternalMiogaUsers', 'ValidateSelectMultipleUsers', 'ValidateSelectMultipleGroups', 'ValidateSelectMultipleTeams', 'ValidateSelectMultipleResources', 'ValidateSelectMultipleUsersMemberOfGroup', 'ValidateSelectMultipleUsersMemberOfTeam', 'ValidateSelectMultipleTeamsMemberOfGroup', 'ValidateSelectMultipleResourcesAllowedInGroup', 'ValidateSelectMultipleResourcesNotInGroup', 'ValidateSelectMultipleUsersNotInGroup', 'ValidateSelectMultipleTeamsNotInGroup', 'ValidateSelectMultipleExternalMiogaUsers', ], },

		non_sensitive_methods => [
			'SelectSingleUser',
			'SelectSingleTeam',
			'SelectSingleGroup',
			'SelectSingleResource',
			'SelectSingleExternalMioga',
			'SelectMultipleUsers',
			'SelectMultipleTeams',
			'SelectMultipleGroups',
			'SelectMultipleResources',
			'SelectMultipleResourcesAllowedInGroup',
			'SelectMultipleResourcesNotInGroup',
			'SelectMultipleTeamsNotInGroup',
			'SelectMultipleUsersNotInGroup',
			'SelectSingleUserMemberOfGroup',
			'SelectSingleExtendedUserMemberOfGroup',
			'SelectSingleGroupForUserWithAppliAndFunc',
			'SelectSingleUserMemberOfTeam',
			'SelectSingleGroupForUser',
			'SelectMultipleUsersMemberOfGroup',
			'SelectMultipleUsersMemberOfTeam',
			'SelectMultipleTeamsMemberOfGroup',
			'SelectMultipleExternalMiogaUsers',
			'ValidateSelectMultipleUsers',
			'ValidateSelectMultipleGroups',
			'ValidateSelectMultipleTeams',
			'ValidateSelectMultipleResources',
			'ValidateSelectMultipleUsersMemberOfGroup',
			'ValidateSelectMultipleUsersMemberOfTeam',
			'ValidateSelectMultipleTeamsMemberOfGroup',
			'ValidateSelectMultipleResourcesAllowedInGroup',
			'ValidateSelectMultipleResourcesNotInGroup',
			'ValidateSelectMultipleUsersNotInGroup',
			'ValidateSelectMultipleTeamsNotInGroup',
			'ValidateSelectMultipleExternalMiogaUsers',
		]
    );

    return \%AppDesc;
}

# ============================================================================

=head2 TestAuthorize ($context)

	Check if the current user can access to the current application and 
	methods in the current group.

	For this application, no specific authorization are required, 
	trust evrybody.

=cut

# ============================================================================

sub TestAuthorize {
    my ($self, $context) = @_;

    return AUTHZ_OK;
}

# ============================================================================

=head2 SelectSingleUser ()

	List used to select just one user. 
	
=cut

# ============================================================================

sub SelectSingleUser {
    my ($self, $context) = @_;

    # ------------------------------------------------------
    # Synchronize not local with database
    # ------------------------------------------------------
    my $config = $context->GetConfig();

    $users_sll_desc{fields} = $users_single_fields;
    $users_sll_desc{search} = $users_search;
    $users_sll_desc{name}   = "mioga_select_user_sll";

    return $self->SelectSingleList($context, \%users_sll_desc, "SelectSingleUser");
}

# ============================================================================

=head2 SelectSingleUserMemberOfGroup ()

	List used to select a user member of the current group. 
	
=cut

# ============================================================================

sub SelectSingleUserMemberOfGroup {
    my ($self, $context) = @_;

    my $session = $context->GetSession();

    if (exists $context->{args}->{action}) {
        delete $session->{Select}->{UserMemberOfGroup};
    }

    # ------------------------------------------------------
    # Synchronize not local with database
    # ------------------------------------------------------
    my $config = $context->GetConfig();

    $users_member_sll_desc{fields} = $users_member_single_fields;

    my $group_id;
    if (exists $context->{args}->{group_id}) {
        $group_id = $context->{args}->{group_id};
        $session->{Select}->{UserMemberOfGroup}->{group_id} = $group_id;
    }

    elsif (exists $session->{Select}->{UserMemberOfGroup}->{group_id}) {
        $group_id = $session->{Select}->{UserMemberOfGroup}->{group_id};
    }

    else {
        $group_id = $context->GetGroup()->GetRowid();
    }

    return $self->SelectSingleList($context, \%users_member_sll_desc, "SelectSingleUserMemberOfGroup", [$group_id]);
}

# ============================================================================

=head2 SelectSingleExtendedUserMemberOfGroup ()

	List used to select a user member of the current group and member of teams invited in group. 
	
=cut

# ============================================================================

sub SelectSingleExtendedUserMemberOfGroup {
    my ($self, $context) = @_;

    my $session = $context->GetSession();

    if (exists $context->{args}->{action}) {
        delete $session->{Select}->{UserMemberOfGroup};
    }

    # ------------------------------------------------------
    # Synchronize not local with database
    # ------------------------------------------------------
    my $config = $context->GetConfig();

    $users_extended_member_sll_desc{fields} = $users_extended_member_single_fields;

    my $group_id;
    if (exists $context->{args}->{group_id}) {
        $group_id = $context->{args}->{group_id};
        $session->{Select}->{ExtendedUserMemberOfGroup}->{group_id} = $group_id;
    }

    elsif (exists $session->{Select}->{ExtendedUserMemberOfGroup}->{group_id}) {
        $group_id = $session->{Select}->{ExtendedUserMemberOfGroup}->{group_id};
    }

    else {
        $group_id = $context->GetGroup()->GetRowid();
    }

    return $self->SelectSingleList($context, \%users_extended_member_sll_desc, "SelectSingleExtendedUserMemberOfGroup", [ $group_id, $group_id ]);
}

# ============================================================================

=head2 SelectSingleUserMemberOfTeam ()

	List used to select a user member of the given team. 
	
=cut

# ============================================================================

sub SelectSingleUserMemberOfTeam {
    my ($self, $context) = @_;

    my $session = $context->GetSession();

    if (exists $context->{args}->{action}) {
        delete $session->{Select}->{UserMemberOfTeam};
    }

    # ------------------------------------------------------
    # Synchronize not local with database
    # ------------------------------------------------------
    my $config = $context->GetConfig();

    $users_member_sll_desc{fields} = $users_member_single_fields;

    my $team_id;
    if (exists $context->{args}->{team_id}) {
        $team_id = $context->{args}->{team_id};
        $session->{Select}->{UserMemberOfTeam}->{team_id} = $team_id;
    }

    elsif (exists $session->{Select}->{UserMemberOfTeam}->{team_id}) {
        $team_id = $session->{Select}->{UserMemberOfTeam}->{team_id};
    }

    return $self->SelectSingleList($context, \%users_member_team_sll_desc, "SelectSingleUserMemberOfTeam", [$team_id]);
}

# ============================================================================

=head2 SelectSingleGroup ()

	List used to select just one group. 
	
=cut

# ============================================================================

sub SelectSingleGroup {
    my ($self, $context) = @_;

    $groups_sll_desc{fields} = $groups_single_fields;
    $groups_sll_desc{search} = $groups_search;
    $groups_sll_desc{name}   = "mioga_select_group_sll";

    return $self->SelectSingleList($context, \%groups_sll_desc, "SelectSingleGroup");
}

# ============================================================================

=head2 SelectSingleTeam ()

	List used to select just one team. 
	
=cut

# ============================================================================

sub SelectSingleTeam {
    my ($self, $context) = @_;

    $teams_sll_desc{fields} = $teams_single_fields;
    $teams_sll_desc{search} = $teams_search;
    $teams_sll_desc{name}   = "mioga_select_team_sll";

    return $self->SelectSingleList($context, \%teams_sll_desc, "SelectSingleTeam");
}

# ============================================================================

=head2 SelectSingleGroupForUser ()

	List used to select just one group where the current user is invited. 
	
=cut

# ============================================================================

sub SelectSingleGroupForUser {
    my ($self, $context) = @_;

    my $user = $context->GetUser();
    return $self->SelectSingleList($context, \%groups_for_user_sll_desc, "SelectSingleGroupForUser", [ $user->GetRowid() ]);
}

# ============================================================================

=head2 SelectSingleGroupForUserWithAppliAndFunc ()

	List used to select just one group where the current user is invited 
	where this user have right to the given function in the given apps.
	
=cut

# ============================================================================

sub SelectSingleGroupForUserWithAppliAndFunc {
    my ($self, $context) = @_;

    my $session = $context->GetSession();

    if (exists $context->{args}->{app_ident}) {
        $session->{Select}->{app_ident} = $context->{args}->{app_ident};
    }

    if (exists $context->{args}->{func_ident}) {
        $session->{Select}->{func_ident} = $context->{args}->{func_ident};
    }

    my $user = $context->GetUser();
    return $self->SelectSingleList($context, \%groups_for_user_app_func_sll_desc, "SelectSingleGroupForUserWithAppliAndFunc", [ $user->GetRowid(), $session->{Select}->{app_ident}, $session->{Select}->{func_ident}, $user->GetRowid(), $session->{Select}->{app_ident}, $session->{Select}->{func_ident} ]);
}

# ============================================================================

=head2 SelectSingleResource ()

	List used to select just one resource. 
	
=cut

# ============================================================================

sub SelectSingleResource {
    my ($self, $context) = @_;

    $resources_sll_desc{fields} = $resources_single_fields;
    $resources_sll_desc{search} = $resources_search;
    $resources_sll_desc{name}   = "mioga_select_resources_sll";

    return $self->SelectSingleList($context, \%resources_sll_desc, "SelectSingleResource");
}

# ============================================================================

=head2 SelectSingleExternalMioga ()

	List used to select just one external mioga. 
	
=cut

# ============================================================================

sub SelectSingleExternalMioga {
    my ($self, $context) = @_;

    return $self->SelectSingleList($context, \%external_mioga_sll_desc, "SelectSingleExternalMioga");
}

# ============================================================================

=head2 SelectMultipleUsers ()

	List used to select multiple user. 
	
=cut

# ============================================================================

sub SelectMultipleUsers {
    my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowids'], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Select::SelectMultipleUsers", __("Wrong argument value.") . Dumper $errors);
	}

    # ------------------------------------------------------
    # Synchronize non local with database
    # ------------------------------------------------------
    my $config = $context->GetConfig();

    $users_sll_desc{fields} = $users_multiple_fields;
    $users_sll_desc{search} = $users_search;
    $users_sll_desc{name}   = "mioga_select_multiple_users_sll";

    return $self->SelectMultipleList($context, \%users_sll_desc, "SelectMultipleUsers");
}

# ============================================================================

=head2 SelectMultipleResourcesAllowedInGroup ()

	List used to select multiple resources allowed in a given group.
	
=cut

# ============================================================================

sub SelectMultipleResourcesAllowedInGroup {
    my ($self, $context) = @_;

    # ------------------------------------------------------
    # Synchronize non local with database
    # ------------------------------------------------------
    my $config = $context->GetConfig();

    my $group = $context->GetGroup();

    if ($group->GetType() eq 'group') {
        return $self->SelectMultipleList($context, \%resources_allowed_sll_desc, "SelectMultipleResourcesAllowedInGroup", { args => [ $group->GetRowid() ] });
    }
    else {
        return $self->SelectMultipleResources($context);
    }
}

# ============================================================================

=head2 SelectMultipleUsersMemberOfGroup ()

	List used to select multiple user member of the current group. 
	
=cut

# ============================================================================

sub SelectMultipleUsersMemberOfGroup {
    my ($self, $context) = @_;

    my $config = $context->GetConfig();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['rowids'], 'allow_empty', [ 'mioga_user_ids', $config ] ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("Select::SelectMultipleUsersMemberOfGroup", __("Wrong argument value.") . Dumper $errors);
    }

    # ------------------------------------------------------
    # Synchronize non local with database
    # ------------------------------------------------------
    my $group = $context->GetGroup();

    $users_member_sll_desc{fields} = $users_member_multiple_fields;

    return $self->SelectMultipleList($context, \%users_member_sll_desc, "SelectMultipleUsersMemberOfGroup", { args => [ $group->GetRowid() ] });
}

# ============================================================================

=head2 SelectMultipleUsersMemberOfTeam ()

	List used to select multiple user member of the given team. 
	
=cut

# ============================================================================

sub SelectMultipleUsersMemberOfTeam {
    my ($self, $context) = @_;

    my $session = $context->GetSession();

    my $team_id;
    if (exists $context->{args}->{team_id}) {
        $team_id = $context->{args}->{team_id};
        $session->{Select}->{MultipleUserMemberOfTeam}->{team_id} = $team_id;
    }

    elsif (exists $session->{Select}->{MultipleUserMemberOfTeam}->{team_id}) {
        $team_id = $session->{Select}->{MultipleUserMemberOfTeam}->{team_id};
    }

    # ------------------------------------------------------
    # Synchronize non local with database
    # ------------------------------------------------------
    my $config = $context->GetConfig();

    $users_member_team_sll_desc{fields} = $users_member_team_multiple_fields;

    return $self->SelectMultipleList($context, \%users_member_team_sll_desc, "SelectMultipleUsersMemberOfTeam", { args => [$team_id] });
}

# ============================================================================

=head2 SelectMultipleUsersNotInGroup ()

	List used to select multiple user not member of the current group. 
	
=cut

# ============================================================================

sub SelectMultipleUsersNotInGroup {
    my ($self, $context) = @_;

    # ------------------------------------------------------
    # Synchronize non local with database
    # ------------------------------------------------------
    my $config = $context->GetConfig();

    my $group = $context->GetGroup();

    $users_not_member_sll_desc{fields} = $users_not_member_multiple_fields;

    return $self->SelectMultipleList($context, \%users_not_member_sll_desc, "SelectMultipleUsersNotInGroup", { args => [ $group->GetRowid(), $group->GetRowid(), $config->GetMiogaId() ] });
}

# ============================================================================

=head2 SelectMultipleTeamsMemberOfGroup ()

	List used to select multiple team member of the current group. 
	
=cut

# ============================================================================

sub SelectMultipleTeamsMemberOfGroup {
    my ($self, $context) = @_;

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['rowids'], 'allow_empty', [ 'mioga_team_ids', $context->GetConfig() ] ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("Select::SelectMultipleUsersMemberOfGroup", __("Wrong argument value.") . Dumper $errors);
    }

    my $group = $context->GetGroup();

    return $self->SelectMultipleList($context, \%teams_member_sll_desc, "SelectMultipleTeamsMemberOfGroup", { args => [ $group->GetRowid() ] });
}

# ============================================================================

=head2 SelectMultipleTeamsNotInGroup ()

	List used to select multiple team not member of the current group. 
	
=cut

# ============================================================================

sub SelectMultipleTeamsNotInGroup {
    my ($self, $context) = @_;

    my $group  = $context->GetGroup();
    my $config = $context->GetConfig();

    return $self->SelectMultipleList($context, \%teams_not_member_sll_desc, "SelectMultipleTeamsNotInGroup", { args => [ $group->GetRowid(), $config->GetMiogaId() ] });
}

# ============================================================================

=head2 SelectMultipleResourcesNotInGroup ()

	List used to select multiple group not member of the current group. 
	
=cut

# ============================================================================

sub SelectMultipleResourcesNotInGroup {
    my ($self, $context) = @_;

    my $group  = $context->GetGroup();
    my $config = $context->GetConfig();

    return $self->SelectMultipleList($context, \%resources_not_member_sll_desc, "SelectMultipleResourcesNotInGroup", { args => [ $group->GetRowid(), $group->GetRowid(), $config->GetMiogaId() ] });
}

# ============================================================================

=head2 SelectMultipleGroups ()

	List used to select multiple group. 
	
=cut

# ============================================================================

sub SelectMultipleGroups {
    my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowids'], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Select::SelectMultipleGroups", __("Wrong argument value.") . Dumper $errors);
	}

    $groups_sll_desc{fields} = $groups_multiple_fields;
    $groups_sll_desc{search} = $groups_search;
    $groups_sll_desc{name}   = "mioga_select_multiple_groups_sll";

    return $self->SelectMultipleList($context, \%groups_sll_desc, "SelectMultipleGroups");
}

# ============================================================================

=head2 SelectMultipleTeams ()

	List used to select multiple teams. 
	
=cut

# ============================================================================

sub SelectMultipleTeams {
    my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowids'], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Select::SelectMultipleTeams", __("Wrong argument value.") . Dumper $errors);
	}

    $teams_sll_desc{fields} = $teams_multiple_fields;
    $teams_sll_desc{search} = $teams_search;
    $teams_sll_desc{name}   = "mioga_select_multiple_teams_sll";

    return $self->SelectMultipleList($context, \%teams_sll_desc, "SelectMultipleTeams");
}

# ============================================================================

=head2 SelectMultipleResources ()

	List used to select multiple resource. 
	
=cut

# ============================================================================

sub SelectMultipleResources {
    my ($self, $context) = @_;

    $resources_sll_desc{fields} = $resources_multiple_fields;
    $resources_sll_desc{search} = $resources_search;
    $resources_sll_desc{name}   = "mioga_select_multiple_resources_sll";

    return $self->SelectMultipleList($context, \%resources_sll_desc, "SelectMultipleResources");
}

#
# Path to fix problem with large list of external users
# TODO : MUST BE rewrited
#
#
sub GetInvitedExternalUser {
    my ($self, $config, $external_mioga_id) = @_;

    my $dbh = $config->GetDBH();

    my $res = SelectMultiple($dbh, "SELECT * FROM m_user_base WHERE external_mioga_id = $external_mioga_id");
    return $res;
}

# ============================================================================

=head2 SelectMultipleExternalMiogaUsers ()

	List used to select multiple external mioga users. 
	
=cut

# ============================================================================

sub SelectMultipleExternalMiogaUsers {
    my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['external_mioga_id'], 'allow_empty', 'want_int'],
			[ ['idents', 'select_all'], 'allow_empty'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Select::SelectMultipleExternalMiogaUsers", __("Wrong argument value.") . Dumper $errors);
	}

    my $session = $context->GetSession();
    my $config  = $context->GetConfig();

    my $external_mioga_id;
    if (exists $context->{args}->{external_mioga_id}) {
        $external_mioga_id = $context->{args}->{external_mioga_id};
        $session->{Select}->{MultipleExternalMiogaUsers}->{external_mioga_id} = $external_mioga_id;
    }

    elsif (exists $session->{Select}->{MultipleExternalMiogaUsers}->{external_mioga_id}) {
        $external_mioga_id = $session->{Select}->{MultipleExternalMiogaUsers}->{external_mioga_id};
    }

    if (exists $context->{args}->{idents}) {
        my $ext_users = $self->GetInvitedExternalUser($config, $external_mioga_id);

        my @idents = map { $_->{external_ident} } @$ext_users;

        #my @idents = split(',', $context->{args}->{idents});
        $session->{Select}->{MultipleExternalMiogaUsers}->{idents} = \@idents;
    }

    $external_mioga_users_sll_desc{name}   = 'external_mioga_users_multiple';
    $external_mioga_users_sll_desc{fields} = $external_mioga_users_multiple_fields;
    $external_mioga_users_sll_desc{search} = $external_mioga_users_search_fields;

    # ------------------------------------------------------
    # Synchronize non local with database
    # ------------------------------------------------------

    return $self->SelectMultipleList(
        $context,
        \%external_mioga_users_sll_desc,
        "SelectMultipleExternalMiogaUsers",
        {
            args        => [],
            create_args => [ $external_mioga_id, $session->{Select}->{MultipleExternalMiogaUsers}->{idents} ],
            sll         => 'Mioga2::LargeList::ExternalMiogaUsers'
        }
    );
}

# ============================================================================

=head2 ValidateSelectMultipleUsers ()

	Validation list used to select multiple user. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleUsers {
    my ($self, $context) = @_;

    $users_sll_desc{fields} = $users_validate_fields;
    $users_sll_desc{search} = undef;
    $users_sll_desc{name}   = "mioga_validate_users_sll";

    return $self->ValidateSelectMultipleList($context, \%users_sll_desc, "SelectMultipleUsers");
}

# ============================================================================

=head2 ValidateSelectMultipleUsersMemberOfGroup ()

	Validation list used to select multiple user member of the current group. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleUsersMemberOfGroup {
    my ($self, $context) = @_;

    $users_sll_desc{fields} = $users_validate_fields;
    $users_sll_desc{search} = undef;
    $users_sll_desc{name}   = "mioga_validate_users_sll";

    return $self->ValidateSelectMultipleList($context, \%users_sll_desc, "SelectMultipleUsersMemberOfGroup");
}

# ============================================================================

=head2 ValidateSelectMultipleUsersMemberOfTeam ()

	Validation list used to select multiple user member of the given team. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleUsersMemberOfTeam {
    my ($self, $context) = @_;

    $users_sll_desc{fields} = $users_validate_fields;
    $users_sll_desc{search} = undef;
    $users_sll_desc{name}   = "mioga_validate_users_sll";

    return $self->ValidateSelectMultipleList($context, \%users_sll_desc, "SelectMultipleUsersMemberOfTeam");
}

# ============================================================================

=head2 ValidateSelectMultipleUsersNotInGroup ()

	Validation list used to select multiple group member of the current group. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleUsersNotInGroup {
    my ($self, $context) = @_;

    $users_sll_desc{fields} = $users_validate_fields;
    $users_sll_desc{search} = undef;
    $users_sll_desc{name}   = "mioga_validate_users_sll";

    return $self->ValidateSelectMultipleList($context, \%users_sll_desc, "SelectMultipleUsersMemberOfGroup");
}

# ============================================================================

=head2 ValidateSelectMultipleTeamsMemberOfGroup ()

	Validation list used to select multiple team member of the given group. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleTeamsMemberOfGroup {
    my ($self, $context) = @_;

    $teams_sll_desc{fields} = $teams_validate_fields;
    $teams_sll_desc{search} = undef;
    $teams_sll_desc{name}   = "mioga_validate_teams_sll";

    return $self->ValidateSelectMultipleList($context, \%teams_sll_desc, "SelectMultipleTeamsMemberOfGroup");
}

# ============================================================================

=head2 ValidateSelectMultipleTeamsNotInGroup ()

	Validation list used to select multiple team not member of the current group. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleTeamsNotInGroup {
    my ($self, $context) = @_;

    $teams_sll_desc{fields} = $teams_validate_fields;
    $teams_sll_desc{search} = undef;
    $teams_sll_desc{name}   = "mioga_validate_teams_sll";

    return $self->ValidateSelectMultipleList($context, \%teams_sll_desc, "SelectMultipleTeamsNotInGroup");
}

# ============================================================================

=head2 ValidateSelectMultipleResourcesNotInGroup ()

	Validation list used to select multiple resources not member of the current group. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleResourcesNotInGroup {
    my ($self, $context) = @_;

    $resources_sll_desc{fields} = $resources_validate_fields;
    $resources_sll_desc{search} = undef;
    $resources_sll_desc{name}   = "mioga_validate_resources_sll";

    return $self->ValidateSelectMultipleList($context, \%resources_sll_desc, "SelectMultipleResourcesNotInGroup");
}

# ============================================================================

=head2 ValidateSelectMultipleResourcesAllowedInGroup ()

	Validation list used to select multiple group member of the current group. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleResourcesAllowedInGroup {
    my ($self, $context) = @_;

    $resources_sll_desc{fields} = $resources_validate_fields;
    $resources_sll_desc{search} = undef;
    $resources_sll_desc{name}   = "mioga_validate_resources_sll";

    return $self->ValidateSelectMultipleList($context, \%resources_sll_desc, "SelectMultipleResourcesAllowedInGroup");
}

# ============================================================================

=head2 ValidateSelectMultipleGroups ()

	Validation list used to select multiple group. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleGroups {
    my ($self, $context) = @_;

    $groups_sll_desc{fields} = $groups_validate_fields;
    $groups_sll_desc{search} = undef;
    $groups_sll_desc{name}   = "mioga_validate_groups_sll";

    return $self->ValidateSelectMultipleList($context, \%groups_sll_desc, "SelectMultipleGroups");
}

# ============================================================================

=head2 ValidateSelectMultipleTeams ()

	Validation list used to select multiple team. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleTeams {
    my ($self, $context) = @_;

    $teams_sll_desc{fields} = $teams_validate_fields;
    $teams_sll_desc{search} = undef;
    $teams_sll_desc{name}   = "mioga_validate_teams_sll";

    return $self->ValidateSelectMultipleList($context, \%teams_sll_desc, "SelectMultipleTeams");
}

# ============================================================================

=head2 ValidateSelectMultipleResources ()

	Validation list used to select multiple resource. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleResources {
    my ($self, $context) = @_;

    $resources_sll_desc{fields} = $resources_validate_fields;
    $resources_sll_desc{search} = undef;
    $resources_sll_desc{name}   = "mioga_validate_resources_sll";

    return $self->ValidateSelectMultipleList($context, \%resources_sll_desc, "SelectMultipleResources");
}

# ============================================================================

=head2 ValidateSelectMultipleExternalMiogaUsers ()

	List used to select multiple external mioga users. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleExternalMiogaUsers {
    my ($self, $context) = @_;

    my $session           = $context->GetSession();
    my $external_mioga_id = $session->{Select}->{MultipleExternalMiogaUsers}->{external_mioga_id};

    $external_mioga_users_sll_desc{name}   = 'external_mioga_users_validate';
    $external_mioga_users_sll_desc{fields} = $external_mioga_users_validate_fields;
    delete $external_mioga_users_sll_desc{search};

    return $self->ValidateSelectMultipleList(
        $context,
        \%external_mioga_users_sll_desc,
        "SelectMultipleExternalMiogaUsers",
        {
            create_args => [$external_mioga_id],
            sll         => 'Mioga2::LargeList::ValidateExternalMiogaUsers'
        }
    );
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 SelectSingleList ()

	Common function to SelectSingle*. 
	
=cut

# ============================================================================

sub SelectSingleList {
    my ($self, $context, $list, $name, $args) = @_;
    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowids'], 'allow_empty' ],
			[ ['rowid'], 'allow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Select::SelectSingleList", __("Wrong argument value.") . Dumper $errors);
	}

    if (!defined $args) {
        $args = [ $config->GetMiogaId() ];
    }

    $self->ParseArgs($context);

    #
    # If the argument rowid is present,
    # redirect to the referer
    #
    if (exists $context->{args}->{rowid}) {
        my $url = $session->{Select}->{referer} . "?" . $session->{Select}->{action} . "=1&rowid=" . $context->{args}->{rowid};

        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent($url);

        return $content;
    }

    elsif (st_ArgExists($context, "select_cancel_act")) {
        my $url = $session->{Select}->{referer} . "?" . $session->{Select}->{action} . "=cancel";

        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent($url);

        return $content;
    }

    #
    # Display the list
    #
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'select.xsl', locale_domain => 'select_xsl');

    my $sll = new Mioga2::SimpleLargeList($context, $list);

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<$name>";
    $xml .= $context->GetXML();
    $xml .= $sll->Run($context, $args);

    $xml .= "<title>" . st_FormatXMLString($session->{Select}->{title}) . "</title>";

    #
    # Add not selectable rowids
    #
    $xml .= "<notselectable>";

    foreach my $rowid (@{ $session->{Select}->{notselectable} }) {
        $xml .= "<rowid>" . $rowid . "</rowid>";
    }

    $xml .= "</notselectable>";

    $xml .= "</$name>";

    $content->SetContent($xml);
    return $content;
}

# ============================================================================

=head2 SelectMultipleList ()

	Common function to SelectMultiple*. 
	
=cut

# ============================================================================

sub SelectMultipleList {
    my ($self, $context, $list, $name, $params) = @_;
    print STDERR "Select::SelectMultipleList\n" if ($debug);
    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $chkparams = [	
			[ ['rowids'], 'allow_empty'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $chkparams);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Select::SelectMultipleList", __("Wrong argument value.") . Dumper $errors);
	}

    Mioga2::LargeList::CheckArgs($context);

    # Reset array of non-selectable items (will be initialized later by $self->ParseArgs)
    $session->{Select}->{notselectable} = ();

    $params = {} unless defined $params;

    my $args;
    if (!exists $params->{args}) {
        $args = [ $config->GetMiogaId() ];
    }
    else {
        $args = $params->{args};
    }

    my $sll_type = 'Mioga2::SimpleLargeList';
    if (exists $params->{sll}) {
        $sll_type = $params->{sll};
    }

    $self->ParseArgs($context);

    my $rowid_list;

    if (!exists $session->{Select}->{rowid_list}) {
        $session->{Select}->{rowid_list} = [];
        print STDERR "  rowid_list empty\n" if ($debug);
    }

    $rowid_list = $session->{Select}->{rowid_list};
    print STDERR " rowid_list = " . Dumper($rowid_list) . "\n" if ($debug);

    #
    # If the argument rowid is present,
    # add the rowid in the current list
    #
    if (exists $context->{args}->{rowid}) {
        if (grep { $_ == $context->{args}->{rowid} } @$rowid_list) {
            my @list = grep { $_ != $context->{args}->{rowid} } @$rowid_list;
            $session->{Select}->{rowid_list} = \@list;
            $rowid_list = $session->{Select}->{rowid_list};
        }
        else {
            push @$rowid_list, $context->{args}->{rowid};
        }
    }
    elsif (st_ArgExists($context, "unselect_all")) {
        print STDERR "Select::SelectMultipleList unselect_all\n" if ($debug);
        $session->{Select}->{rowid_list} = [];
        $rowid_list = $session->{Select}->{rowid_list};
    }
    elsif (st_ArgExists($context, "select_ok_act")) {
        my $rowids = join(',', @$rowid_list);
        my $url = $session->{Select}->{referer} . "?" . $session->{Select}->{action} . "=1&rowids=" . $rowids;

        warn "$url" if $debug;

        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent($url);

        return $content;
    }

    elsif (st_ArgExists($context, "select_cancel_act")) {
        my $url = $session->{Select}->{referer} . "?" . $session->{Select}->{action} . "=cancel";

        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent($url);

        return $content;
    }

    #
    # Display the list
    #
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'select.xsl', locale_domain => 'select_xsl');

    eval "require " . $sll_type . ";";

    my @sllargs = ();
    if (exists $params->{create_args}) {
        @sllargs = @{ $params->{create_args} };
    }
    print STDERR "sll_type = $sll_type  list = $list\n"  if ($debug);
    print STDERR "sllargs = " . Dumper(\@sllargs) . "\n" if ($debug);

    my $sll = $sll_type->new($context, $list, @sllargs);

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<$name>";
    $xml .= $context->GetXML();

    $sll->InitSelectedSimpleCheckboxes($context, 'rowid', $session->{Select}->{rowid_list}) if $session->{Select}->{rowid_list};
    $xml .= $sll->Run($context, $args);
    print STDERR " sll = " . Dumper($sll) . "\n" if ($debug);

    $xml .= "<title>" . st_FormatXMLString($session->{Select}->{title}) . "</title>";

    #
    # Add current selection
    #
    if (st_ArgExists($context, "select_all")) {
        print STDERR "Select::SelectMultipleList select_all\n" if ($debug);
        $rowid_list = [];
        my $result = $sll->GetResult();
        foreach my $res (@$result) {
            if (!grep { $_ == $res->{rowid} } @{ $session->{Select}->{notselectable} }) {
                push @$rowid_list, $res->{rowid};
            }
        }
        $session->{Select}->{rowid_list} = $rowid_list;
    }
    $xml .= "<selected>";

    foreach my $rowid (@$rowid_list) {
        $xml .= "<rowid>" . $rowid . "</rowid>";
    }

    $xml .= "</selected>";

    #
    # Add not selectable rowids
    #
    $xml .= "<notselectable>";

    foreach my $rowid (@{ $session->{Select}->{notselectable} }) {
        $xml .= "<rowid>" . $rowid . "</rowid>";
    }

    $xml .= "</notselectable>";

    $xml .= "</$name>";

    print STDERR " xml = $xml\n" if ($debug);

    $content->SetContent($xml);
    return $content;
}

# ============================================================================

=head2 ValidateSelectMultipleList ()

	Common function to ValidateSelectMultiple*. 
	
=cut

# ============================================================================

sub ValidateSelectMultipleList {
    my ($self, $context, $list, $name, $params) = @_;
    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

    $params = {} unless defined $params;

    my $rowid_list;
    my @rowids = grep /select_simple_rowid_\d+/, %{ $context->{args} };
    map { $_ =~ s/select_simple_rowid_(\d+)/$1/; $_ = int($_) } @rowids;
    if (!exists $session->{Select}->{rowid_list}) {
        $session->{Select}->{rowid_list} = [];
    }
    $session->{Select}->{rowid_list} = \@rowids if scalar @rowids;
    $rowid_list = $session->{Select}->{rowid_list};

    if (st_ArgExists($context, "select_ok_act")) {
        my $rowids = join(',', @$rowid_list);
        my $url = $session->{Select}->{referer} . "?" . $session->{Select}->{action} . "=1&rowids=" . $rowids;

        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent($url);

        return $content;
    }

    elsif (st_ArgExists($context, "select_modify_act")) {
        my $rowids = join(',', @$rowid_list);
        my $url = "$name";

        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent($url);

        return $content;
    }

    elsif (st_ArgExists($context, "select_cancel_act")) {
        my $url = $session->{Select}->{referer} . "?" . $session->{Select}->{action} . "=cancel";

        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent($url);

        return $content;
    }

    my $sll_type = "Mioga2::LargeList::ValidateSelectList";
    if (exists $params->{sll}) {
        $sll_type = $params->{sll};
    }

    my @sllargs;
    if (exists $params->{create_args}) {
        @sllargs = @{ $params->{create_args} };
    }

    #
    # Display the list
    #
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'select.xsl', locale_domain => 'select_xsl');

    eval "require " . $sll_type . ";";

    my $sll = $sll_type->new($context, $list, @sllargs);

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<Validate$name>";
    $xml .= $context->GetXML();
    $xml .= "<name>" . $name . "</name>";

    $xml .= "<title>" . st_FormatXMLString($session->{Select}->{title}) . "</title>";

    $xml .= $sll->Run($context);

    $xml .= "</Validate$name>";

    $content->SetContent($xml);
    return $content;
}

# ============================================================================

=head2 ParseArgs ()

	Parse arguments of lists
	
=cut

# ============================================================================

sub ParseArgs {
    my ($self, $context) = @_;

    my $config = $context->GetConfig();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my @action = grep (/^select_.*_act/, keys(%{ $context->{args} }));
    my $params = [ [ ['action', 'title', 'selusertitle'], 'allow_empty' ], [ ['referer'], 'allow_empty', [ 'location', $config ] ], [ \@action, 'allow_empty' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("Select::ParseArgs", __("Wrong argument value.") . Dumper $errors);
    }

    my $session = $context->GetSession();

    #
    # Parse Initialization Args (referer and action)
    #
    my ($action, $referer, $title);

    if (exists $context->{args}->{action}) {
        $action = $context->{args}->{action};
        delete $session->{Select}->{rowid_list};
        delete $session->{Select}->{title};
    }
    else {
        $action = $session->{Select}->{action};
    }

    if (exists $context->{args}->{referer}) {
        $referer = $context->{args}->{referer};
    }
    elsif (exists $session->{Select}->{referer}) {
        $referer = $session->{Select}->{referer};
    }
    else {
        $referer = $context->GetReferer();
    }

    if (exists $context->{args}->{title}) {
        $title = $context->{args}->{title};
    }
    elsif (exists $session->{Select}->{title}) {
        $title = $session->{Select}->{title};
    }
    else {
        $title = "";
    }

    $session->{Select}->{referer} = $referer;
    $session->{Select}->{action}  = $action;
    $session->{Select}->{title}   = $title;

    if (exists $context->{args}->{rowids}) {
        my @list = split(',', $context->{args}->{rowids});
        $session->{Select}->{notselectable} = \@list;
    }
    elsif (!exists $session->{Select}->{notselectable}) {
        $session->{Select}->{notselectable} = [];
    }

    # remove options in referer url
    $session->{Select}->{referer} =~ s/\?.*$//;
}

# ============================================================================

=head2 IsSelectable ()

	Check if the element is selectable
	
=cut

# ============================================================================

sub IsSelectable {
    my ($context, $values) = @_;

    my $session = $context->GetSession();
    $session = $session->{Select}->{notselectable};

    my $count = grep { $values->{rowid} eq $_ } @$session;

    return ($count == 0);
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
