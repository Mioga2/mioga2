# ============================================================================
# Mioga2 Project (C) 2007-2008 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#   Description:

=head1 NAME

DAVProxy.pm: Proxy to access DAV uris for Mioga

=head1 DESCRIPTION

This module is the Mioga Apache/mod_perl DAV proxy handler.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::DAVProxy;
use strict;

use utf8;

use Apache2::Const;
use Error qw(:try);
use Mioga2::Config;
use Mioga2::DAVFS;
use Mioga2::Exception::Simple;
use Mioga2::MiogaConf;


my $debug = 0;

# ============================================================================

=head2 handler ($request)

    Apache/mod_perl proxy_handler function. $request is a the Apache request
    object.

    return an Apache::Constant constant like OK or AUTH_REQUIRED.

=cut

# ============================================================================

#sub proxy_handler : method {
sub handler : method {
    my ( $self, $r ) = @_;
	print STDERR "[Mioga2::DAVProxy::handler -- $$]\n" if ($debug);

    my ( $config,  $uri_obj, $user );

	my $ret = Apache2::Const::OK;
	#
	# Get miogaconf sets by Router handler.
	#
	my $miogaconf = $r->pnotes('miogaconf');
	if ( !defined($miogaconf) ) {
		$r->warn("Mioga2::Proxy::handler miogaconf not defined" );
		return Apache2::Const::HTTP_INTERNAL_SERVER_ERROR;
	}

	$uri_obj = $r->pnotes('uri_obj');
    if (!defined($uri_obj)) {
        my $real_uri = Mioga2::Classes::URI->new( uri => $r->uri )->as_string;
		print STDERR "  real_uri = $real_uri\n" if ($debug);
        $real_uri =~ s!/*$!!;
		print STDERR "  after correct real_uri = $real_uri\n" if ($debug);
        $uri_obj = Mioga2::URI->new( $miogaconf, uri => $real_uri );
	}

	$config  = $r->pnotes('config');
	if ( !defined($config) ) {
        $config = Mioga2::Config->new( $miogaconf, $uri_obj->GetMiogaIdent );
	}

	$user = $r->pnotes('user');
	if (!defined($user) && !$uri_obj->IsPublic()) {
		$r->warn("Mioga2::Proxy::handler private access with no defined user ERROR" );
		return Apache2::Const::HTTP_INTERNAL_SERVER_ERROR;
	}

	if (!$user) {
		# Get cookie if exists
		my %cookies = CGI::Cookie->fetch($r);
		my $cookie = $cookies{mioga_session};
		my $token = $cookie->value () if ($cookie);
		$user = Mioga2::Login::GetUser ($config, $uri_obj, $token);
	}

	if (!$user && $r->get_basic_auth_pw) {
		# No $user from pnotes (didn't go through Mioga2::Authen, public access) nor from cookie
		# get user from HTTP-Basic authen
		my $login = $r->user;
		$user = new Mioga2::Old::User($config, login => $login);
	}

	try {
		my $method = $r->method();
		my $callbacks = Mioga2::DAVFS::Callbacks->CreateCallbacksFromApacheRequest($r);

		my $davfs = Mioga2::DAVFS->new($config);
		$ret = $davfs->DAVRequestAfterClearance($method, $uri_obj, $callbacks, $user);
	}
	otherwise {
		my $err = shift;
		my $context = $r->pnotes("context");
		$self->LogException ($context, $err, $r);

		$ret = Apache2::Const::HTTP_INTERNAL_SERVER_ERROR;
	};

	# Ensure text content-types have charset indication
	my $ct = $r->content_type ();
	if (($ct =~ /^text\//) && ($ct !~ /charset=/)) {
		$r->content_type ("$ct; charset=\"utf-8\"");
	}

	print STDERR "[Mioga2::DAVProxy::ResponseHandler] DAVProxy::ResponseHandler Leaving ($ret)\n" if ($debug);
    return $ret;
}


#===============================================================================

=head2 LogException

Generic exception log method

=over

=item B<Parameters:>

=over

=item I<$context> The current Mioga2::RequestContext.

=item I<$err> The current Error.

=item I<$r> The current Apache2::RequestRec.

=back

=item B<Return:> Nothing.

=back

=cut

#===============================================================================
sub LogException {
	my ($self, $context, $err, $r) = @_;

	print STDERR "\n";

	#-------------------------------------------------------------------------------
	# Get stack trace
	#-------------------------------------------------------------------------------
	my @trace = split ("\n", $err->stacktrace);

	#-------------------------------------------------------------------------------
	# Log exception type
	#-------------------------------------------------------------------------------
	$r->log_error ('##### [BEGIN] ' . ref ($err) . ' #####');

	#-------------------------------------------------------------------------------
	# Log error message
	#-------------------------------------------------------------------------------
	$r->log_error ('Message: ' . shift (@trace));

	if ($context) {
		#-------------------------------------------------------------------------------
		# Log user ident
		#-------------------------------------------------------------------------------
		$r->log_error ('User: ' . $context->GetUser->{values}->{ident});

		#-------------------------------------------------------------------------------
		# Log URI
		#-------------------------------------------------------------------------------
		$r->log_error ('URI: ' . $context->GetURI()->GetURI());

		#-------------------------------------------------------------------------------
		# Log access method
		#-------------------------------------------------------------------------------
		$r->log_error ('Method: ' . $context->{access_method});

		#-------------------------------------------------------------------------------
		# Log request headers
		#-------------------------------------------------------------------------------
		$r->log_error ("Headers:");
		for (keys %{$r->headers_in}) {
			next if /^Authorization$/;
			$r->log_error ("    $_: " . $r->headers_in->{$_});
		}
	}
	else {
		#-------------------------------------------------------------------------------
		# No context, inform the logfile
		#-------------------------------------------------------------------------------
		$r->log_error ("No context, can not get user, URI, method, params information");
	}

	#-------------------------------------------------------------------------------
	# Log SQL details
	#-------------------------------------------------------------------------------
	if (ref ($err) eq 'Mioga2::Exception::DB') {
		$r->log_error ('SQL details:');
		$r->log_error ('    SQL request: ' . $err->{sql});
		$r->log_error ('    SQL arguments: ' . join (', ', @{$err->{args}}));
	}

	#-------------------------------------------------------------------------------
	# Log trace
	#-------------------------------------------------------------------------------
	$r->log_error ("Trace:");
	# Drop useless two last lines
	pop (@trace);
	pop (@trace);
	for (@trace) {
		$_ =~ s/\t/    /g;
		$r->log_error ($_);
	}
	
	$r->log_error ('##### [END] ' . ref ($err) . ' #####');
	
	print STDERR "\n";
}	# ----------  end of subroutine LogException  ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga.conf Mioga2

=head1 COPYRIGHT

Copyright (C) 2007-2008, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
