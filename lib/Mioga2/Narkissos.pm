# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME

Narkissos.pm : The Mioga2 personal data management application.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Narkissos;
use strict;
use vars qw($VERSION);

$VERSION = "1.0.0";

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'narkissos';

use Error qw(:try);
use Mioga2::Content::XSLT;
use Mioga2::tools::args_checker;
use Mioga2::tools::string_utils;
use Mioga2::tools::APIGroup;
use Mioga2::tools::database;
use Mioga2::Exception::Group;
use Mioga2::Exception::User;
use Mioga2::Exception::Resource;
use Mioga2::Exception::Simple;
use Mioga2::LDAP;
use Data::Dumper;

my $debug = 0;


# ============================================================================
# Application Description
# ============================================================================


# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc {
	my %AppDesc = (
		ident   => 'Narkissos',
		package => 'Mioga2::Narkissos',
		name => __('Personal Data Manager'),
		description => __('Personal data management application'),
		api_version => '2.3',
		type    => 'normal',
		all_groups => 0,
		all_users  => 1,
		can_be_public => 0,
		is_user             => 1,
		is_group            => 0,
		is_resource         => 0,
		functions => { 'All' => __('All Functions') },
		func_methods => {
			'All' => [
				'DisplayMain', 'GetInstances', 'GetUser', 'GetGroups', 'GetApplications'
			],
		},
		sxml_methods => { },
		xml_methods  => { },
		non_sensitive_methods => [
			'DisplayMain',
			'GetInstances',
			'GetUser',
			'GetGroups',
			'GetApplications'
		],
	);

	return \%AppDesc;
}

# ============================================================================
# Public Methods
# ============================================================================

=head2 Displaymain

Display the main page using XSLT formatting.
It is also used to handle POSTED form values (optional).

=head3 Incoming Optional Arguments

=over

=item I<firstname>

=item I<lastname>

=item I<email>

=item I<password>: optional

=item I<new_password_1>: optional

=item I<new_password_2>: optional

=item I<user_act_modify>: the desired operation

=back

=head3 If using secret questions

=over

=item I<secret_question>

=item I<secret_question_answer>

=back

=cut

sub DisplayMain {
	my ($self, $context) = @_;
	print STDERR "Narkissos::DisplayMain()\n" if ($debug);
	print STDERR "Mioga2::Narkissos::DisplayMain args = " . Dumper $context->{args} if ($debug);

	my $config = $context->GetConfig ();
	my $session = $context->GetSession ();
	
	my $user = $context->GetUser ();

	my ($values, $errors);

	my $clear_passwd;
	my $status = 0;
	my $policy_fail = 0;
	my $password_match_fail = 0;
	my $wrong_password = 0;

	if(st_ArgExists($context, 'user_act_modify')) {
		print STDERR "Narkissos::DisplayMain Updating user information\n" if ($debug);
		#-------------------------------------------------------------------------------
		# Check incoming arguments
		#-------------------------------------------------------------------------------
		my $params = [	
				[ ['firstname', 'lastname'], 'disallow_empty'],
				[ ['email'], 'disallow_empty', 'want_email' ],
				[ ['password'], 'allow_empty' ],
				[ ['new_password_1', 'new_password_2'], 'allow_empty', [ 'password_policy', $config ] ],
				[ ['user_act_modify'], 'allow_empty'],
				[ ['X-CSRF-Token'], 'disallow_empty', ['csrf_token', [$config, $user]] ],
			];

		# If user wants to change his email, ensure it is not already used
		if ($context->{args}->{email} ne $user->Get ('email')) {
			push (@$params, [ ['email'], ['unused_email', $config] ]);
		}

		if ($config->UseSecretQuestion ()) {
			push @$params, [ ['secret_question', 'secret_question_answer'], 'disallow_empty' ];
		}

		($values, $errors) = ac_CheckArgs ($context, $params);

		if (@$errors) {
			for (@$errors) {
				if ($_->[1][0] =~ /password_policy/) {
					$policy_fail = 1;
				}
			}
		}
		delete ($values->{'X-CSRF-Token'});

		# Keep current password for LDAP bind
		my $passwd = $values->{password};

		if($values->{password} ne ''
		   and $values->{new_password_1} ne ''
		   and $values->{new_password_2} ne ''
		) {
			if (!$user->CheckPassword($values->{password})) {
				$wrong_password = 1;
			}
			else {
				if ( $values->{new_password_1} eq $values->{new_password_2} ) {
					$values->{password} = delete ($values->{new_password_1});
					delete $values->{new_password_2};
				}
				else {
					$password_match_fail = 1;
				}
			}
		}
		elsif ($values->{password} eq '' && $values->{new_password_1} ne '' && $values->{new_password_2} ne '') {
			$wrong_password = 1;
		}
		else {
			delete ($values->{password});
			delete ($values->{new_password_1});
			delete ($values->{new_password_2});
		}

		if(! @$errors && !$policy_fail && !$password_match_fail && !$wrong_password) {
			delete ($values->{user_act_modify});
			for ($user->Get ('type')) {
				if (/ldap_user/) {
					print STDERR "Narkissos::DisplayMain Connecting to LDAP\n" if ($debug);
					# Connect to LDAP
					my $dn = $user->Get ('dn');
					$self->{ldap} = new Mioga2::LDAP($config);
					$self->{ldap}->BindWith ($config->GetLDAPBindDN (), $config->GetLDAPBindPwd ());
					
					if ($values->{password}) {
						# Crypt password for LDAP
						$clear_passwd = $values->{password};
						my $crypter = $config->LoadPasswordCrypter();
						$values->{password} = $crypter->CryptPassword($config, $values);
					}

					try {
						# Modify user information in LDAP directory
						$self->{ldap}->ModifyUser ($dn, $self->{ldap}->TranslateAttributes($values));

						# Modify database
						$values->{rowid} = $user->Get('rowid');
						for my $attr (keys (%$values)) {
							next if ($attr =~ /^secret_question/);
							$user->Set ($attr, $values->{$attr});
						}
						$user->Store ();
						$status = 1;
					}
					catch Mioga2::Exception::LDAP with {
						my $err = shift;
						print STDERR "Narkissos::DisplayMain Failed to modify LDAP for dn $dn\n";
						warn Dumper $err;
					}
					catch Mioga2::Exception::DB with {
						my $err = shift;
						print STDERR "Narkissos::DisplayMain Failed to modify DB for dn $dn\n";
						warn Dumper $err;
					}
				}
				elsif (/local_user/) {
					print STDERR "Narkissos::DisplayMain Modifying local DB\n" if ($debug);
					$values->{rowid} = $user->Get ('rowid');
					for my $attr (keys (%$values)) {
						next if ($attr =~ /^secret_question/);
						$user->Set ($attr, $values->{$attr});
					}
					$user->Store ();
					$status = 1;
				}
			}

			# Update secret question
			$user->SetSecretQuestion ($values->{secret_question}, $values->{secret_question_answer});
		}
		else {
			$status = 0;
		}
		
		if ($status) {
			$session->{__internal}->{message}{type} = 'info';
			$session->{__internal}->{message}{text} = __('User information successfully modified.');
			my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
			$content->SetContent('DisplayMain');
			return $content;
		}
		else {
			$session->{__internal}->{message}{type} = 'error';
			$session->{__internal}->{message}{text} = __('Error modifying user information.');
		}
	}

	# Translate intance IDs to names
	my $dbh = $config->GetDBH;
	for (@{$user->{values}->{instances}}) {
		my $instance = SelectSingle($dbh, "SELECT ident FROM m_mioga WHERE rowid = $_");
		$_ = $instance->{ident};
	}

	#
	# Generate XML
	#

	my $xml = '<?xml version="1.0" encoding="UTF-8"?>';
	$xml .= "<DisplayMain";
	$xml .= " Autonomous='" . $user->Get ('autonomous') . "'";
	$xml .= " UseSecretQuestion='" . $config->UseSecretQuestion() . "'";
	if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
	}
	
	# Check if user information can be modified
	my $CanModify = 1;
	if ($user->Get ('type') eq 'ldap_user') {
		$CanModify = $config->CanModifyLdapPassword ();
	}
	$xml .= " CanModify='$CanModify' PasswordMatchFail='$password_match_fail' WrongPassword='$wrong_password'";
	$xml .= ">";
	$xml .= $context->GetXML();
	if ($policy_fail) {
		$xml .= $config->GetXMLPasswordPolicy ();
	}

	#-------------------------------------------------------------------------------
	# Remove any duplicate 'password_policy' error
	#-------------------------------------------------------------------------------
	my $id = -1;
	for (@$errors) {
		if ($_->[1][0] eq 'password_policy') {
			$id++;
		}
	}
	if ($id > 0) {
		splice (@$errors, $id, 1);
	}

	$xml .= ac_XMLifyArgs( ['ident', 'firstname', 'lastname', 'email', 'password', 'instances' ], $user->GetUsers ());

	my $qa = $user->GetSecretQuestion ();
	$xml .= "<question>$qa->{question}</question><answer>$qa->{answer}</answer>";
	
	$xml .= ac_XMLifyErrors($errors);
	
	$xml .= "</DisplayMain>";

	print STDERR "xml = $xml\n" if($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'narkissos.xsl', locale_domain => "narkissos_xsl");
    $content->SetContent($xml);

	return $content;
}

#===============================================================================

=head2 GetInstances

Webservice returning the list of instances the connected user is member of

=cut

#===============================================================================
sub GetInstances {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Narkissos::GetInstances] Entering\n" if ($debug);

	my $data = {};

	$self->{db}->Execute ("SELECT m_mioga.* FROM m_mioga, m_user_base WHERE m_mioga.rowid = m_user_base.mioga_id AND m_user_base.email = ?;", [$context->GetUser ()->Get ('email')]);
	while (my $row = $self->{db}->Fetch ()) {
		my $inst = Mioga2::InstanceList->new ($context->GetConfig ()->GetMiogaConf (), { attributes => { ident => $row->{ident} } });
		my $cfg  = Mioga2::Config->new ($context->GetConfig ()->GetMiogaConf (), $row->{ident});
		my $inst_data = {
			ident => $row->{ident},
			url   => $cfg->GetBaseURI (),
		};
		push (@{$data->{instance}}, $inst_data);
	}

	print STDERR "[Mioga2::Narkissos::GetInstances] Leaving. Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetInstances  ----------


#===============================================================================

=head2 GetUser

Webservice returning the list of instances the connected user is member of and information about the current user

=cut

#===============================================================================
sub GetUser {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Narkissos::GetUser] Entering\n" if ($debug);

	my $data = {};
	my $user = $context->GetUser();

	$data->{instances} = GetInstances($self, $context)->{instance};
	$data->{firstname} = $user->Get('firstname');
	$data->{lastname} = $user->Get('lastname');
	$data->{ident} = $user->Get('ident');
	$data->{email} = $user->Get('email');

	print STDERR "[Mioga2::Narkissos::GetUser] Leaving. Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetUser  ----------


#===============================================================================

=head2 GetGroups

Get groups the user is invited into and applications he can use in each.

=head3 Incoming Arguments

=over

None

=back

=head3 Generated XML

The generated XML is a group list.

	<GetGroups>
		<group>
			<ident>...</ident>
			<applications>
				<application>
					<ident>...</ident>
					<label>...</label>
				</application>
			</applications>
		</group>
		...
	</GetGroups>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=cut

#===============================================================================
sub GetGroups {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Narkissos::GetGroups] Entering\n" if ($debug);

	my $data = { };

	# Get groups the user is member of
	@{$data->{group}} = map { { ident => $_->{ident} } } $context->GetUser ()->GetExpandedGroupList ()->GetGroups ();

	# Get applications the user can access in groups
	for my $group (@{$data->{group}}) {
		my $user_obj = $context->GetUser ();
		my $group_obj = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { ident => $group->{ident} } });
		my $apps = Mioga2::ApplicationList->new ($context->GetConfig (), { attributes => { groups => $group_obj, users => $user_obj }});
		@{$group->{applications}->{application}} = map { { ident => $_->{ident}, label => $_->{label} } } $apps->GetApplications ();
	}

	print STDERR "[Mioga2::Narkissos::GetGroups] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetGroups  ----------


#===============================================================================

=head2 GetApplications

Get applications the user can use within a user-context.

=head3 Incoming Arguments

=over

None

=back

=head3 Generated XML

The generated XML is an application list.

	<GetApplications>
		<application>
			<ident>...</ident>
			<label>...</label>
		</application>
		...
	</GetApplications>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=cut

#===============================================================================
sub GetApplications {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Narkissos::GetApplications] Entering\n" if ($debug);

	my $data = { };

	@{$data->{applications}} = map { { ident => $_->{ident}, label => $_->{label} } } $context->GetUser ()->GetActiveApplicationList ()->GetApplications ();

	print STDERR "[Mioga2::Narkissos::GetApplications] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetApplications  ----------

1;
