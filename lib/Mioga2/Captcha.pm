#===============================================================================
#
#         FILE:  Captcha.pm
#
#  DESCRIPTION:  Captcha handling object.
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  Sébastien NOBILI <technique@alixen.fr>
#      COMPANY:  Alixen (http://www.alixen.fr)
#      CREATED:  21/06/2010 11:47
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Captcha.pm: Captcha handling object.

=head1 DESCRIPTION

This module provides captcha handling for Mioga2. It can pick a random captcha
or check a user-submitted value.

=head1 SYNOPSIS

	use Mioga2::Captcha;

	my $captcha = new Mioga2::Captcha ($dbh, $context->GetConfig ()->GetMiogaConf ()->GetCaptchaDefaults ());
	
	...
	# Get XML to be parsed with captcha.xsl
	$xml .= $captcha->GetRandomXML ();
	...

	# Check user-submitted captcha
	$captcha->Check ($values->{captcha_id}, $values->{captcha})

	B<Note:> Your application should not give the captcha image as
	base64-encoded inside the HTML as it is not supported by IE... Instead, you
	should provide a GetCaptcha method that call Mioga2::Captcha::GetImage and
	returns ASIS the image.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Captcha;

use Data::Dumper;
use Image::Magick;
use MIME::Base64;
use Digest::MD5 qw(md5_hex);
use POSIX qw(strftime);

use Mioga2::tools::database;

my $debug = 0;


#===============================================================================

=head2 new

Create a new Mioga2::Captcha object.

=cut

#===============================================================================
sub new {
	my ($class, $dbh, $args) = @_;

	my $self = {};
	bless ($self, $class);

	$self->{dbh} = $dbh;

	# Get args values
	for (keys (%$args)) {
		$self->{$_} = $args->{$_};
	}

	# Initialize computed values
	$self->{height} = $self->{width} / 3;
	$self->{ptsize} = $self->{width} / $self->{length};

	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 GetRandom

Get random captcha ID and base64 encoded.

=cut

#===============================================================================
sub GetRandom {
	my ($self) = @_;

	my $captcha = '';

	# Create blank image
	my $img=Image::Magick->new;
	$img->Set(size=>"$self->{width}x$self->{height}");
	$img->Read('xc:white');

	# Generate and place skewed characters
	my $count = 0;
	for ($self->GenerateRandomChars ($self->{length})) {
		$captcha .= $_;

		my $x = (1.2 * ($count + 1) / ($self->{length} + 1)) * $self->{width};
		my $y = $self->{height} / 2;
		my $skewX = ($self->{skew} && $count % 2) ? rand ($self->{width} / 10) + ($self->{width} / 10) : 0;
		my $skewY = ($self->{skew} && (!$count % 2)) ? rand ($self->{height} / 10) + ($self->{width} / 10) : 0;
		my $rotate = ($self->{rotate}) ? rand (90) - 45 : 0;
		my ($c_width, $c_height) = $img->QueryFontMetrics(text => $_, font => 'Generic.ttf', pointsize => $self->{ptsize});
		$img->Annotate(
				text => $_,
				font => 'Generic.ttf',
				fill => 'black',
				pointsize => $self->{ptsize},
				rotate => $rotate,
				skewX => $skewX,
				skewY => $skewY,
				x => $x - ($c_width / 2),
				y => $y + ($c_height / 4)
			);
		$count++;
	}

	# Alter image
	$img->Wave (geometry => '5x100') if ($self->{wave});
	$img->Swirl (degrees => 50) if $self->{swirl};

	# Generate and place lines
	$count = 0;
	while ($count < $self->{lines}) {
		my $coord = '';
		$coord .= rand ($self->{width}) . ',' . rand ($self->{height}) . " " . rand ($self->{width}) . ',' . rand ($self->{height});
		$img->Draw (primitive => 'line', points => $coord, stroke => 'black');
		$count++;
	}

	# Add noise
	$img->AddNoise (noise => 'Impulse') if ($self->{noise});

	# Add border
	$img->Border (bordercolor => 'Black', height => $self->{width} / 40, width => $self->{width} / 40) if ($self->{border});

	my $data = $img->ImageToBlob (magick => 'gif');

	# Record captcha to DB
	my $hash = md5_hex ($data);

	my $base64 = encode_base64 ($data);

	ExecSQL ($self->{dbh}, "INSERT INTO m_captcha (hash, captcha, base64, created) VALUES ('$hash', '$captcha', '$base64', NOW())");

	return (($hash, $base64));
}	# ----------  end of subroutine GetRandom  ----------


#===============================================================================

=head2 GetRandomXML

Get random captcha as XML.

=cut

#===============================================================================
sub GetRandomXML {
	my ($self) = @_;

	my ($id, $base64) = $self->GetRandom ();

	return ("<captcha id='$id'>$base64</captcha>");
}	# ----------  end of subroutine GetRandomXML  ----------


#===============================================================================

=head2 Check

Check user-submitted captcha.

=cut

#===============================================================================
sub Check {
	my ($self, $hash, $text) = @_;
	
	# Delete expired captchas
	my $time = strftime("%Y-%m-%d %H:%M:%S", gmtime(time - ($self->{expire} * 60)));
	ExecSQL ($self->{dbh}, "DELETE FROM m_captcha WHERE created < '$time'");

	my $captcha = SelectSingle ($self->{dbh}, "SELECT * FROM m_captcha WHERE hash=?;", [$hash]);

	ExecSQL ($self->{dbh}, "DELETE FROM m_captcha WHERE hash=?;", [$hash]);

	return (($text ne '') && (lc ($captcha->{captcha}) eq lc ($text)));
}	# ----------  end of subroutine Check  ----------


#===============================================================================

=head2 GetImage

Get image contents.

=cut

#===============================================================================
sub GetImage {
	my ($self, $hash) = @_;

	my $base64 = SelectSingle ($self->{dbh}, "SELECT base64 FROM m_captcha WHERE hash = ?;", [$hash])->{base64};

	my $image = decode_base64 ($base64);

	return ($image);
}	# ----------  end of subroutine GetImage  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


#===============================================================================

=head2 GenerateRandomChars

Generate a random array of characters

=cut

#===============================================================================
sub GenerateRandomChars {
	my ($self) = @_;

	my @result = ();
	my @chars = ('A'..'H', 'J'..'N', 'P'..'Z', '1'..'9');
	
	srand;

	my $_rand;
	for (my $i=0; $i < $self->{length} ;$i++) {
	    $_rand = int(rand scalar (@chars));
	    push (@result, $chars[$_rand]);
	}

	return (@result);
}	# ----------  end of subroutine GenerateRandomChars  ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 COPYRIGHT

Copyright (C) 2010, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;

