# ============================================================================
# Mioga2 Project (C) 2006-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
News.pm: The Mioga2 News application.

=head1 METHODS DESCRIPTION

=cut

# ============================================================================

package Mioga2::News;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'news';

use Error qw(:try);
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::Classes::Time;
use MIME::Entity;
use Data::Dumper;
use POSIX;

# Constants to status
use constant PUBLISH    => "published";
use constant DRAFT      => "draft";
use constant WAITING    => "waiting";
use constant D_PUBLISH  => "DisplayMain";
use constant D_DRAFT    => "DisplayDraftNews";
use constant D_WAITING  => "DisplayWaitingNews";


my $debug = 0;

# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc
{
	my $self = shift;
    
    my %AppDesc = ( ident   => 'News',
                package => 'Mioga2::News',
                name    => __('News'),
                description => __('The Mioga2 News application'),
				type    => 'normal',
                all_groups => 1,
                all_users  => 0,
                can_be_public => 0,
				is_user             => 0,
				is_group            => 1,
				is_resource         => 0,

				functions => {
					'Anim'	=> __('Animation methods'),
                    'Moder' => __('Moderation methods'),
					'Write' => __('Creation and modification'),
					'Read'  => __('Consultation methods'),
				},

				func_methods => {
					'Anim'  => [ 'DisplayOptions', 'UpdateOptions'],
                    'Moder' => [ 'DisplayWaitingNews', 'ValidateNewsItem', 'DeleteNewsItem', 'DeclineNewsItem' ],
					'Read' 	=> [ 'DisplayMain' ],
					'Write'	=> [ 'WriteNewsItem', 'UpdateNewsItem', 'EditNewsItem', 'DisplayDraftNews' ]
					},

				public_methods => [ ],
				
				sxml_methods => { },
				
				xml_methods  => { },				

				non_sensitive_methods => [
					'DisplayOptions',
                    'DisplayWaitingNews',
					'DisplayMain',
					'DisplayDraftNews',
					'EditNewsItem',
					'WriteNewsItem',
				],
				
                );
    
	return \%AppDesc;
}
            
# ============================================================================
# Public Methods
# ============================================================================

# ============================================================================

=head2 DisplayMain ()

=over

=item Main method. It displays the list of currently available news.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayMain {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayMain>";
	
	
	{
		my ($nr_instances, $xmlref_instances) = $self->AllSuchApplications($context);
		if ($nr_instances > 1) {
			$xml .= "<appnav>".$$xmlref_instances."</appnav>";
		}	
	}
	
    $xml .= "<Archives/>" if $context->{args}->{archive} == 1;
    $xml .= $self->XMLGetNewsWithState($context, state => PUBLISH, page => D_PUBLISH);
    
    $xml .= "</DisplayMain>";
	
	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'news.xsl', locale_domain => "news_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
	return $content;
}

# ============================================================================

=head2 DisplayWaitingNews ()

=over

=item Shows the news that are waiting to be published when in moderated mode.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayWaitingNews {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayWaitingNews>";
	$xml .= $self->XMLGetNewsWithState($context, state => WAITING, order => 'ASC', page => D_WAITING);
    
    $xml .= "</DisplayWaitingNews>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'news.xsl', locale_domain => "news_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
	return $content;
}

# ============================================================================

=head2 DisplayDraftNews ()

=over

=item Shows the news that are drafts.

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayDraftNews {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayDraftNews>";
    $xml .= $self->XMLGetNewsWithState($context, state => DRAFT, page => D_DRAFT);
    
    $xml .= "</DisplayDraftNews>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'news.xsl', locale_domain => "news_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
	return $content;
}

# ============================================================================

=head2 DisplayOptions ()

=over

=item Displays options of the application (only for animators).

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DisplayOptions {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<DisplayOptions>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    $xml   .= $self->GetOptions($context);
    
    $xml   .= "</DisplayOptions>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'news-js.xsl', locale_domain => "news_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content
}

# ============================================================================

=head2 EditNewsItem ()

=over

=item Edit a news item.

=back

=head3 Incoming Arguments

=over

=item I<rowid>: The news ID

=item I<page>: The webpage calling the function

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub EditNewsItem {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<EditNewsItem>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['rowid'], 'stripxws', 'disallow_empty', 'want_int' ],
        [ ['page'], 'stripxws', 'disallow_empty' ],
                                     ]);
    
    if (not @$errors) {
        my $news = $self->DBGetNewsItem($context, 'values' => $values);
        $xml .= $self->XMLGetNewsItem($context, news => $news, page => $values->{page});
        $xml .= "<CanSaveAsDraft/>" if ($news->{owner} == $context->GetUser()->GetRowid() and $news->{status} ne WAITING);
        $xml .= "<Page>" . st_FormatXMLString($values->{page}) . "</Page>";
    }
    else {
        #$xml .= ac_XMLifyErrors($errors);
		throw Mioga2::Exception::Application("Mioga2::News::EditNewsItem", ac_FormatErrors($errors));
    }
    
    $xml   .= "</EditNewsItem>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'news.xsl', locale_domain => "news_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 WriteNewsItem ()

=over

=item Write a news item. Allowed to writer users.

=back

=head3 Incoming Arguments

=over

=item I<page>: The webpage calling the function

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub WriteNewsItem {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<WriteNewsItem>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['page'], 'stripxws', 'disallow_empty' ],
                                     ]);
    
    if (not @$errors) {
        $xml   .= "<CanSaveAsDraft/>";
        $xml   .= "<Page>" . st_FormatXMLString($values->{page}) . "</Page>";
    }
    else {
        $xml .= ac_XMLifyErrors($errors);
    }
    
    $xml   .= "</WriteNewsItem>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'news.xsl', locale_domain => "news_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 ValidateNewsItem ()

=over

=item Validate an existing news item.

=back

=head3 Incoming Arguments

=over

=item I<rowid>: The news ID

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub ValidateNewsItem {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<ValidateNewsItem>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['rowid'], 'disallow_empty', 'want_int'],
                                     ]);
    
    if (not @$errors) {
        my $news = $self->DBGetNewsItem($context, 'values' => $values);
        $self->DBValidateNewsItem($context, 'values' => $news);
        
        $values->{rowid} = $news->{old_id} if ($news->{old_id});
        my $options = $self->DBGetOptions($context);
        $self->SendMailTo($context, options => $options, who => 'user', 'values' => $values, msg => 'validated');
        
        warn __"News item successfully validated." if $debug;
        $xml .= $self->GetNews($context, state => WAITING, page => D_WAITING);
        $xml .= "<CanBeValidated/>";
        $xml .= "<Page>" . st_FormatXMLString(D_WAITING) . "</Page>";
        $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
    }
    else {
        #$xml .= ac_XMLifyErrors($errors);
		throw Mioga2::Exception::Application("Mioga2::News::EditNewsItem", ac_FormatErrors($errors));
    }
    
    $xml   .= "</ValidateNewsItem>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'news-js.xsl', locale_domain => "news_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 DeclineNewsItem ()

=over

=item Decline an existing news and send a mail to the user.

=back

=head3 Incoming Arguments

=over

=item I<rowid>: The news ID

=item B<return :> xml content generated to be processed by the matching xsl stylesheet.

=back

=cut

# ============================================================================

sub DeclineNewsItem {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<DeclineNewsItem>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['rowid'], 'disallow_empty', 'want_int'],
                                     ]);
    
    if (not @$errors) {
        $self->DBDeclineNewsItem($context, 'values' => $values);
        
        my $options = $self->DBGetOptions($context);
        $self->SendMailTo($context, options => $options, 'values' => $values, who => 'user', msg => 'declined');
        
        warn __"News declined." if $debug;
        $xml .= $self->GetNews($context, state => WAITING, page => D_WAITING);
        $xml .= "<CanBeValidated/>";
        $xml .= "<Page>" . st_FormatXMLString(D_WAITING) . "</Page>";
    }
    else {
        #$xml .= ac_XMLifyErrors($errors);
		throw Mioga2::Exception::Application("Mioga2::News::EditNewsItem", ac_FormatErrors($errors));
    }
    
    $xml   .= "</DeclineNewsItem>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'news-js.xsl', locale_domain => "news_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 XMLGetAccessRights ()

=over

=item Retrieve access rights.

=item B<return :> access rights as a xml string.

=back

=cut

# ============================================================================

sub XMLGetAccessRights {
    my ($self, $context) = @_;
    
    my $xml .= "<AccessRights>";
    $xml .= "<User id=\"".$context->GetUser()->GetRowid()."\"/>";
    $xml .= "<Animation/>" if ($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Anim"));
    $xml .= "<Moderation/>" if ($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Moder"));
    $xml .= "<Write/>" if ($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Write"));
    $xml .= "<Read/>" if ($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Read"));
    $xml    .= "</AccessRights>";
    
    $xml .= "<Actions>";
    $xml .= "<DeleteNews>" . st_FormatXMLString(__"Delete this news item") . "</DeleteNews>";
    $xml .= "<WriteNews>" . st_FormatXMLString(__"Write a news item") . "</WriteNews>";
    $xml .= "<WaitingNews>" . st_FormatXMLString(__"Pending news") . "</WaitingNews>";
    $xml .= "<Options>" . st_FormatXMLString(__"Change options") . "</Options>";
    $xml .= "<Edit>" . st_FormatXMLString(__"Edit") . "</Edit>";
    $xml .= "<CreateNews>" . st_FormatXMLString(__"Writing news item") . "</CreateNews>";
    $xml .= "<UpdateNews>" . st_FormatXMLString(__"Modifying news item") . "</UpdateNews>";
    $xml .= "<NewsTitle>" . st_FormatXMLString(__"Title") . "</NewsTitle>";
    $xml .= "<News>" . st_FormatXMLString(__"News") . "</News>";
    $xml .= "<PublishNews>" . st_FormatXMLString(__"Publish") . "</PublishNews>";
    $xml .= "<Cancel>" . st_FormatXMLString(__"Cancel") . "</Cancel>";
    $xml .= "<MainNews>" . st_FormatXMLString(__"Published news") . "</MainNews>";
    $xml .= "<ArchivedNews>" . st_FormatXMLString(__"Archived news") . "</ArchivedNews>";
    $xml .= "<Validate>" . st_FormatXMLString(__"Validate this news item") . "</Validate>";
    $xml .= "<DeclineConfirm>" . st_FormatXMLString(__"Are you sure you want to decline this news item ?") . "</DeclineConfirm>";
    $xml .= "<DeleteConfirm>" . st_FormatXMLString(__"Are you sure you want to delete this news item ?") . "</DeleteConfirm>";
    $xml .= "<Decline>" . st_FormatXMLString(__"Decline") . "</Decline>";
    $xml .= "<DraftNews>" . st_FormatXMLString(__"View my drafts") . "</DraftNews>";
    $xml .= "<SaveDraft>" . st_FormatXMLString(__"Save as a draft") . "</SaveDraft>";
    $xml .= "</Actions>";
    return $xml;
}

# ============================================================================

=head2 GetNews ()

=over

=item Retrieve news.

=item B<return :> xml representation of news as a string.

=back

=cut

# ============================================================================

sub GetNews {
    my ($self, $context, %options) = @_;
    
    my $xml = "<News>";
    
    my $news = $self->DBGetNews($context, %options);
    
    foreach my $news (@$news) {
        $xml .= $self->XMLGetNewsItem($context, news => $news, page => $options{page});
    }

    $xml .= "</News>";
    return $xml;
}

# ============================================================================

=head2 GetOptions (I<$context>)

=over

=item retrieve options.

=item B<return :> xml representation of options as a string.

=back

=cut

# ============================================================================

sub GetOptions {
    my ($self, $context) = @_;
    
    my $xml = "<Options>";
    $xml .= "<Title>" . st_FormatXMLString(__"Modify options") . "</Title>";
    $xml .= "<Cancel>" . st_FormatXMLString(__"Cancel") . "</Cancel>";
    $xml .= "<Ok>" . st_FormatXMLString(__"Ok") . "</Ok>";
    
    my $options = $self->DBGetOptions($context);
    
    $xml .= "<Option field=\"moderated\" label=\"" . st_FormatXMLString(__"Moderate news") . "\">" . st_FormatXMLString($options->{moderated}) . "</Option>";
    $xml .= "<Option field=\"moderator_mail\" label=\"" . st_FormatXMLString(__"Send mail to moderator") ."\">" . st_FormatXMLString($options->{moderator_mail}) . "</Option>";
    $xml .= "<Option field=\"user_mail\" label=\"" . st_FormatXMLString(__"Send mail to user") ."\">" . st_FormatXMLString($options->{user_mail}) . "</Option>";
    $xml .= "<Option type=\"text\" field=\"delay\" label=\"" . st_FormatXMLString(__"News are archived after this number of days") . "\">" . st_FormatXMLString($options->{delay}) . "</Option>";
    
    $xml   .= "</Options>";
    return $xml;
}


# ============================================================================

=head2 XMLAddMenuItem (I<$options>)

=over

=item Add an entry to the menu.

=item B<parameters :>

=over

=item I<$options->{id}> : id of action as a string.

=item I<$options->{uri}> : uri of action as a string.

=item I<$options->{text}> : text of action as a string.

=item B<return :> the entry in xml as a string.

=back

=back

=cut

# ============================================================================

sub XMLAddMenuItem {
    my ($self, %options) = @_;
    
    my $xml = "<Item id=\"" . st_FormatXMLString($options{id}) . "\">";
    $xml   .= "<Uri>" . st_FormatXMLString($options{uri}) . "</Uri>";
    $xml   .= "<Label>" . st_FormatXMLString($options{text}) . "</Label>";
    $xml   .= "</Item>";
    
    return $xml;
}

# ============================================================================

=head2 XMLGetNewsItem (I<%options>)

=over

=item XMLize a news item hash.

=item B<parameters :>

=over

=item I<$options{news}> : news item hash.

=back

=item B<return :> return news in xml as a string.

=back

=cut

# ============================================================================

sub XMLGetNewsItem {
    my ($self, $context, %options) = @_;
    
    my $news = $options{news};
    my $xml .= "<NewsItem created=\"" . st_FormatXMLString($news->{created}) . "\" modified=\"" . st_FormatXMLString($news->{modified}) . "\" author=\"" . st_FormatXMLString($news->{owner}) . "\" moderator=\"" . st_FormatXMLString($news->{moderator_id}) . "\" id=\"" . st_FormatXMLString($news->{rowid}) . "\" status=\"" . st_FormatXMLString($news->{status}) . "\"";
    $xml .= " old_id=\"" . st_FormatXMLString($news->{old_id}) . "\"" if ($news->{old_id});
    $xml .= ">";
    
    if (($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Write") and $context->GetUser()->GetRowid() == int($news->{owner})) or $self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Moder")) {
        $xml .= "<CanBeEdited/>";
    }
    $xml .= "<CanBeValidated/>" if ($news->{status} eq WAITING and $self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Moder") and $options{page} eq D_WAITING);
    
	for ($news->{status}) {
		if (/published/) {
			# Publication date
			my @user_time = gmtime(du_ISOToSecond(du_ConvertGMTISOToLocale($news->{published}, $context->GetUser)));
			$xml .= "<Infos>" . st_FormatXMLString(__x("Published by {firstname} {lastname} on {date} at {time}.", firstname => $news->{firstname}, lastname => $news->{lastname}, date => strftime("%x", @user_time), time => strftime("%X", @user_time))) . "</Infos>";

			# Modification date
			if ($news->{published} ne $news->{modified}) {
				my @user_mod_time = gmtime(du_ISOToSecond(du_ConvertGMTISOToLocale($news->{modified}, $context->GetUser)));
				$xml .= "<Infos>" . st_FormatXMLString(__x("Last modification on {date} at {time}.", date => strftime("%x", @user_mod_time), time => strftime("%X", @user_mod_time))) . "</Infos>";
			}
		}
		else {
			my @user_time = gmtime(du_ISOToSecond(du_ConvertGMTISOToLocale($news->{created}, $context->GetUser)));
			$xml .= "<Infos>" . st_FormatXMLString(__x("Created by {firstname} {lastname} on {date} at {time}.", firstname => $news->{firstname}, lastname => $news->{lastname}, date => strftime("%x", @user_time), time => strftime("%X", @user_time))) . "</Infos>";

			# Modification date
			if ($news->{created} ne $news->{modified}) {
				my @user_mod_time = gmtime(du_ISOToSecond(du_ConvertGMTISOToLocale($news->{modified}, $context->GetUser)));
				$xml .= "<Infos>" . st_FormatXMLString(__x("Last modification on {date} at {time}.", date => strftime("%x", @user_mod_time), time => strftime("%X", @user_mod_time))) . "</Infos>";
			}
		}
	}
    
    $xml .= "<Title>" . st_FormatXMLString($news->{title}) . "</Title>";
    $xml .= "<Contents>" . st_FormatXMLString($news->{text}) . "</Contents>";
    $xml .= "</NewsItem>";
    
    return $xml;
}

# ============================================================================

=head2 XMLGetNewsWithState (I<$context>, I<%options>)

=over

=item Get all news with the given state.

=item B<parameters :>

=over

=item I<$context> : request context.

=back

=item B<return :> xml describing news with the given state.

=back

=cut

# ============================================================================

sub XMLGetNewsWithState {
    my ($self, $context, %options) = @_;
    
    my $xml .= $context->GetXML;
    $xml .= $self->XMLGetAccessRights($context);
    $xml .= $self->GetOptions($context);

    $xml .= $self->GetNews($context, state => $options{state}, page => $options{page});
    
    $xml .= "<State>" . st_FormatXMLString($options{state}) . "</State>";
}

# ============================================================================

=head2 ProcessDraft (I<$context>, I<%options>)

=over

=item Process a news item which is in a draft state.

=item B<parameters :>

=over

=item I<$context> : request context.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub ProcessDraft {
    my ($self, $context, %options) = @_;
    my $values = $options{'values'};
    
    if (not $values->{rowid}) {
        $self->DBInsertNewsItem($context, 'values' => $values, status => DRAFT);
    }
    else {
        my $news = $self->DBGetNewsItem($context, 'values' => $values);
        if ($news->{status} eq PUBLISH) {
            if ($values->{old_id}) {
                $self->DBUpdateNewsItem($context, 'values' => $values, status => DRAFT);
            }
            else {
                $self->DBInsertNewsItem($context, 'values' => $values, link_to => $values->{rowid}, status => DRAFT);
            }
        }
        else {
            if ($news->{status} eq WAITING) {
                throw Mioga2::Exception("News::ProcessDraft", __"Can't make a draft from a waiting news item.");
            }
            else {
                $self->DBUpdateNewsItem($context, 'values' => $values, status => DRAFT);
            }
        }
    }
}

# ============================================================================

=head2 SendMailTo (I<$context>, I<%options>)

=over

=item Send a prewritten mail to user or moderators.

=item B<parameters :>

=over

=item I<$context> : request context.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub SendMailTo {
    my ($self, $context, %options) = @_;
    my $values  = $options{'values'};
    my $who     = $options{who};
    my $msg_id  = $options{msg};
    my $options = $options{options};
    
    return unless ($options->{moderated} == 1);
    
    my ($message, $subject, $recipient);
    my $user    = $context->GetUser();
    my $sender  = $user->GetName() . " <" . $user->GetEmail() . ">";
    my $config  = $context->GetConfig();
    my $group   = $context->GetGroup();
    
    ## shared messages
    my $automatic   = __"This is an automatic message please do not answer.";
    my $uri_msg     = __"You can access to the moderation page directly by clicking the following link:\n%s";
    
    if ($options->{moderator_mail} == 1 and $who eq 'moderators') {
        my $app_desc    = $context->GetAppDesc();
        my @mod_list    = ProfileGetUsersForAuthzFunction($config->GetDBH(), $group->GetRowid(), $app_desc->GetIdent(), 'Moder');
        $recipient      = new Mioga2::Old::UserList($config, rowid_list => \@mod_list);
        my $uri         = new Mioga2::URI($config, group => $group->GetIdent(), public => 0, application => $app_desc->GetIdent(), method => 'DisplayWaitingNews');
        my $uri_link    = $config->GetProtocol()."://".$config->GetServerName().$uri->GetURI();
        
        if ($msg_id eq 'new_news') {
            $subject = __x("A news item in group {group} is waiting for your approval", group => $group->GetIdent); 
            $message = "$automatic\n" . __x("The news item '{title}' awaits your approval.", title => $values->{title}) . "\n\n" . sprintf($uri_msg, $uri_link);
        }
        elsif ($msg_id eq 'updated_news') {
            $subject = __x("An existing news item in group {group} has been updated", group => $group->GetIdent);
            $message = "$automatic\n" . __x("The news item '{title}' has been updated and awaits your approval.", title => $values->{title}) . "\n\n" . sprintf($uri_msg, $uri_link);
        }
    }
    
    if ($options->{user_mail} == 1 and $who eq 'user') {
        my $news = $self->DBGetNewsItem($context, 'values' => $values);
        $recipient = new Mioga2::Old::User($config, rowid => $news->{owner});
        
        if ($msg_id eq 'validated') {
            $subject = __x("News item approved in group {group}", group => $group->GetIdent);
            $message = "$automatic\n" . __x("Your news item '{title}' has been approved by a moderator.", title => $news->{title});
        }
        elsif ($msg_id eq 'declined') {
            $subject = __x("News item declined in group {group}", group => $group->GetIdent);
            $message = "$automatic\n" . __x("Your news item '{title}' has been declined by a moderator. It is now available to you as a draft.", title => $news->{title});
        }
    }

    if (($options->{user_mail} == 1 and $who eq 'user') || ($options->{moderator_mail} == 1 and $who eq 'moderators')) {
		my $entity = MIME::Entity->build(From   => $sender,
										Subject => $subject,
										Data    => $message,
										Charset => "UTF-8");
		$recipient->SendMail($entity);
	}
}

# ============================================================================

=head2 ProcessPublish (I<$context>, I<%options>)

=over

=item Process a news item (create, update, moderate).

=item B<parameters :>

=over

=item I<$context> : request context.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub ProcessPublish {
    my ($self, $context, %options) = @_;
    my $values = $options{'values'};
    
    my $options = $self->DBGetOptions($context);
    if (not $values->{rowid}) {
        if ($options->{moderated} == 1) {
            $self->DBInsertNewsItem($context, 'values' => $values, status => WAITING);
            $self->SendMailTo($context, 'values' => $values, msg => 'new_news', options => $options, who => 'moderators');
        }
        else {
            if ($values->{old_id}) {
                throw Mioga2::Exception("News::ProcessPublish", __"No rowid set but old_id is set ?!");
            }
            else {
                $self->DBInsertNewsItem($context, 'values' => $values, status => PUBLISH);
                $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
            }
        }
    }
    else {
        if ($options->{moderated} == 1) {
            if ($values->{old_id} or $values->{status} eq DRAFT) {
                $self->DBUpdateNewsItem($context, 'values' => $values, status => WAITING);
            }
            else {
                $self->DBInsertNewsItem($context, 'values' => $values, status => WAITING, link_to => $values->{rowid});
            }
            $self->SendMailTo($context, 'values' => $values, msg => 'updated_news', options => $options, who => 'moderators');
        }
        else {
            if ($values->{old_id}) {
                $self->DBUpdateNewsItem($context, 'values' => $values, status => PUBLISH, update_old => 1);
                $self->DBDeleteNewsItem($context, 'values' => $values);
                $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
            }
            else {
                $self->DBUpdateNewsItem($context, 'values' => $values, status => PUBLISH);
                $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
            }
        }
    }
}

# ============================================================================

=head2 UpdateNewsItem ()

=over

=item Update a news item.

=back

=head3 Incoming Arguments

=over

=item I<rowid>: The news ID

=item I<title>: The news title (max 128 chars)

=item I<text>: The news content

=item I<page>: The webpage calling this function

=item B<return :> error or the updated news.

=back

=cut

# ============================================================================

sub UpdateNewsItem {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<UpdateNewsItem>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['title'], 'stripxws', 'disallow_empty', ['max' => 128] ],
        [ ['draft', 'validate', 'decline', 'old_id', 'status', 'candraft', 'editaction'], 'stripxws', 'allow_empty' ],
        [ ['rowid'], 'want_int', 'stripxws', 'allow_empty' ],
        [ ['text', 'page' ], 'stripxws', 'disallow_empty'],
                                     ]);
    
    if (not @$errors) {
        if ($values->{draft}) {
            $self->ProcessDraft($context, 'values' => $values);
        }
        elsif ($values->{decline}) {
            $self->DeclineNewsItem($context);
        }
        elsif ($values->{validate}) {
            $self->DBUpdateNewsItem($context, 'values' => $values);
            $self->ValidateNewsItem($context);
        }
        else {
            $self->ProcessPublish($context, 'values' => $values);
        }
        
        my $content = new Mioga2::Content::REDIRECT($context);
        $content->SetContent($values->{page});
        return $content;
    }
    else {
        $xml .= ac_XMLifyErrors($errors);
        
        $xml .= $self->XMLGetNewsItem($context, news => $values);
        $xml .= "<CanSaveAsDraft/>" if ($values->{candraft});
        $xml .= "<Page>" . st_FormatXMLString($values->{page}) . "</Page>";
        $xml .= "<EditAction>" . st_FormatXMLString($values->{editaction}) . "</EditAction>";
    }
    
    $xml   .= "</UpdateNewsItem>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'news.xsl', locale_domain => "news_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 DeleteNewsItem ()

=over

=item delete a news item.

=back

=head3 Incoming Arguments

=over

=item I<rowid>: The news ID

=item I<page>: The webpage calling this function

=item B<return :> error or news available.

=back

=cut

# ============================================================================

sub DeleteNewsItem {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<DeleteNewsItem>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ ['rowid'], 'stripxws', 'disallow_empty', 'want_int'],
        [ ['page' ], 'stripxws', 'disallow_empty'],
                                     ]);
    
    if (not @$errors) {
        $self->DBDeleteNewsItem($context, 'values' => $values);
        
        warn $self->GetText("News item successfully deleted.") if $debug;
        
        $xml .= $self->GetNews($context, state => $self->GetStateFromPage($values->{page}), page => $values->{page});
        
        $xml .= "<CanBeValidated/>" if ($values->{state} eq WAITING);
        $xml .= "<Page>" . st_FormatXMLString($values->{page}) ."</Page>";
        $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
    }
    else {
        #$xml .= ac_XMLifyErrors($errors);
		throw Mioga2::Exception::Application("Mioga2::News::EditNewsItem", ac_FormatErrors($errors));
    }
    
    $xml   .= "</DeleteNewsItem>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'news-js.xsl', locale_domain => "news_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 UpdateOptions ()

=over

=item Update options status.

=back

=head3 Incoming Arguments

=over

=item I<fields>: moderated, moderator_mail, user_mail

=item I<delay>: The delay

=item B<return :> error or the updated options list.

=back

=cut

# ============================================================================

sub UpdateOptions {
    my ($self, $context) = @_;
    
    my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
    $xml   .= "<UpdateOptions>";
    $xml   .= $context->GetXML;
    $xml   .= $self->XMLGetAccessRights($context);
    
    my $values = {};
    my $errors = [];

	my @fields = qw/moderated moderator_mail user_mail/;
    
    ($values, $errors) = ac_CheckArgs($context, [ 
        [ \@fields, 'allow_empty', 'bool'],
        [ ['delay'], 'disallow_empty', 'want_int'],
                                     ]);
    
    if (not @$errors) {
		# Set fields value to 0 if not sent by interface
		for (@fields) {
			$values->{$_} = 0 unless ($values->{$_});
		}

        $self->DBUpdateOptions($context, 'values' => $values);
        
        warn $self->GetText("Options successfully updated.") if $debug;
        
        $xml .= "<Message>" . st_FormatXMLString(__"Options successfully updated.") .  "</Message>";
        
        $xml .= $self->GetOptions($context);
    }
    else {
        $xml .= ac_XMLifyErrors($errors);
    }
    
    $xml   .= "</UpdateOptions>";
    
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'news-js.xsl', locale_domain => "news_xsl");
    $content->SetContent($xml);
    
    warn Dumper($xml) if $debug;
    return $content;
}

# ============================================================================

=head2 DBGetNews (I<$context>)

=over

=item Database method to retrieve news.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> list of hashes containing data.

=back

=cut

# ============================================================================

sub DBGetNews {
    my ($self, $context, %options) = @_;
    
    my $group_id = ($options{group} || $context->GetGroup)->GetRowid;
    my $only_user = "";
    
    $options{state} ||= PUBLISH;
    $options{order} ||= 'DESC';
    $options{archive} = $context->{args}->{archive} == 1 ? '< DATE(NOW()) - news_pref.delay' : '>= DATE(NOW()) - news_pref.delay';
    $options{feed}  ||= 0;
    $options{max}   ||= undef;
    
    if ($options{state} eq DRAFT) {
        $only_user = "AND news.owner=" . $context->GetUser()->GetRowid();
    }
    my $archive         = ($options{feed}) ? "" : "AND news.created $options{archive}";
    my $limit           = ($options{max}) ? "LIMIT $options{max}" : "";
    my $sql             = "SELECT news.*, m_user_base.firstname, m_user_base.lastname, article_and_news_status.label as status FROM news_pref, news, article_and_news_status, m_user_base WHERE news.group_id=$group_id AND article_and_news_status.label='" . $options{state} . "' AND news.status_id=article_and_news_status.rowid AND news_pref.group_id=news.group_id AND m_user_base.rowid=news.owner $only_user $archive ORDER BY news.created " . $options{order} . " $limit";
    my $result = SelectMultiple($context->GetConfig()->GetDBH(), $sql);
    
    return $result;
}

# ============================================================================

=head2 DBGetNewsItem (I<$context>)

=over

=item Database method to retrieve a news item.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> news.

=back

=cut

# ============================================================================

sub DBGetNewsItem {
    my ($self, $context, %options) = @_;
    
    my $values      = $options{'values'};
    my $group_id    = $context->GetGroup()->GetRowid();
    my $dbh         = $context->GetConfig()->GetDBH();
    
    my $old_news = SelectSingle($dbh, "SELECT * FROM news_moderation WHERE old_id=" . $values->{rowid} . " OR new_id=" . $values->{rowid});
    my $rowid    = $old_news->{new_id} || $values->{rowid};
    my $news     = SelectSingle($dbh, "SELECT news.*, article_and_news_status.label as status, m_user_base.firstname, m_user_base.lastname FROM news, article_and_news_status, m_user_base WHERE news.group_id=$group_id AND news.rowid=$rowid AND m_user_base.rowid=news.owner AND article_and_news_status.rowid=news.status_id");
    
    $news->{old_id} = $old_news->{old_id} if ($old_news->{old_id});
    
    $news->{firstname} = $news->{firstname};
    $news->{lastname} = $news->{lastname};
    
    return $news;
}

# ============================================================================

=head2 DBInsertNewsItem (I<$context>)

=over

=item Database method to insert a news item.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBInsertNewsItem {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $user_id         = $context->GetUser()->GetRowid();
    my $values          = $options{'values'};
    
    BeginTransaction($dbh);
    try {
        my $prefs           = $self->DBGetOptions($context);
        my $status_label    = $options{status} || PUBLISH;
        my $status          = SelectSingle($dbh, "SELECT rowid FROM article_and_news_status WHERE label='$status_label'");
        my $sql             = "INSERT INTO news(created,modified,title,text,owner,group_id,status_id) VALUES(NOW(),NOW(),'" . st_FormatPostgreSQLString($values->{title}) . "','" . st_FormatPostgreSQLString($values->{text}) . "',$user_id,$group_id," . $status->{rowid} . ")";
        
        ExecSQL($dbh, $sql);
		my $news_id = GetLastInsertId($dbh, "news");

		if ($status_label eq PUBLISH) {
			ExecSQL ($dbh, "UPDATE news SET published=NOW() WHERE rowid=$news_id");
		}
        
        if ($options{link_to}) {
            ExecSQL($dbh, "INSERT INTO news_moderation(new_id, old_id) VALUES($news_id,".$options{link_to}.")");
        }
    }
    otherwise {
        my $ex = shift;
        warn Dumper($ex);
        
        RollbackTransaction($dbh);
        $ex->throw();
    };
    EndTransaction($dbh);
}


# ============================================================================

=head2 DBDeleteNewsItem (I<$context>, I<%options>)

=over

=item Database method to delete an existing news item.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBDeleteNewsItem {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $values          = $options{'values'};
    
    BeginTransaction($dbh) unless ($options{no_transaction});
    try {
        my $moderated = SelectSingle($dbh, "SELECT * FROM news_moderation WHERE old_id=" . $values->{rowid});
        
        if (defined ($moderated)) {
            ExecSQL($dbh, "DELETE FROM news_moderation WHERE old_id=" . $values->{rowid});
            ExecSQL($dbh, "DELETE FROM news WHERE group_id=$group_id AND rowid=" . $moderated->{new_id});
        }
        ExecSQL($dbh, "DELETE FROM news_moderation WHERE new_id=" . $values->{rowid});
        ExecSQL($dbh, "DELETE FROM news WHERE group_id=$group_id AND rowid=" . $values->{rowid});
    }
    otherwise {
        my $ex = shift;
        warn Dumper($ex);
        
        RollbackTransaction($dbh) unless ($options{no_transaction});
        $ex->throw();
    };
    EndTransaction($dbh) unless ($options{no_transaction});
}

# ============================================================================

=head2 DBDeclineNewsItem (I<$context>, I<%options>)

=over

=item Database method to decline a news.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBDeclineNewsItem {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $values          = $options{'values'};
    
    my $draft_status = SelectSingle($dbh, "SELECT * FROM article_and_news_status WHERE label='" . st_FormatPostgreSQLString(DRAFT) . "'");
    ExecSQL($dbh, "UPDATE news SET status_id=" . $draft_status->{rowid} . " WHERE rowid=" . $values->{rowid});
}

# ============================================================================

=head2 DBUpdateNewsItem (I<$context>, I<%options>)

=over

=item Database method to update an existing news item.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBUpdateNewsItem {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $user_id         = $context->GetUser()->GetRowid();
    my $values          = $options{'values'};
    
    BeginTransaction($dbh) unless ($options{no_transaction});
    try {
        my $status_id;
        my $rowid = ($options{update_old} == 1) ? $values->{old_id} : $values->{rowid};
        
        if ($options{status}) {
            $status_id = SelectSingle($dbh, "SELECT * FROM article_and_news_status WHERE label='".st_FormatPostgreSQLString($options{status})."'");
            $status_id = $status_id->{rowid};
        }

		my $oldstatus = SelectSingle ($dbh, "SELECT label FROM article_and_news_status WHERE rowid=(SELECT status_id FROM news WHERE rowid=$rowid)")->{label};
        
        my $sql = "UPDATE news SET modified=NOW(), title='".st_FormatPostgreSQLString($values->{title})."', text='".st_FormatPostgreSQLString($values->{text})."'";
        $sql .= ", status_id=$status_id" if ($status_id);
        $sql .= ", moderator_id=".$options{moderator} if ($options{moderator});
        $sql .= ", published=NOW()" if (($options{status} eq PUBLISH) && ($oldstatus ne PUBLISH));
        $sql .= " WHERE rowid=$rowid";
        
        ExecSQL($dbh, $sql);
     }
     otherwise {
         my $ex = shift;
         warn Dumper($ex);

         RollbackTransaction($dbh) unless ($options{no_transaction});
         $ex->throw();
     };
     EndTransaction($dbh) unless ($options{no_transaction});
}

# ============================================================================

=head2 DBUpdateOptions (I<$context>, I<%options>)

=over

=item Database method to update options.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBUpdateOptions {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $values          = $options{'values'};
    
    my $sql = "UPDATE news_pref SET " . join(",", map({ 
        my $v = $values->{$_};
        if ($_ ne 'delay') {
            $v =~ s/0/'f'/g;
            $v =~ s/1/'t'/g;
        }
        "$_=$v";
    } keys %$values));
    
    $sql .= ", modified=NOW() WHERE group_id=$group_id";
    
    ExecSQL($dbh, $sql);
}

# ============================================================================

=head2 DBValidateNewsItem (I<$context>, I<%options>)

=over

=item Database method to validate a news item.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub DBValidateNewsItem {
    my ($self, $context, %options) = @_;
    
    my $dbh             = $context->GetConfig()->GetDBH();
    my $group_id        = $context->GetGroup()->GetRowid();
    my $mod_id          = $context->GetUser()->GetRowid();
    my $values          = $options{'values'};
    
    BeginTransaction($dbh);
    try {
        my $update_old = ($values->{old_id}) ? 1 : 0;
        $self->DBUpdateNewsItem($context, 'values' => $values, status => PUBLISH, update_old => $update_old, no_transaction => 1, moderator => $context->GetUser()->GetRowid());
        $self->DBDeleteNewsItem($context, 'values' => $values, no_transaction => 1) if ($values->{old_id});
    }
    otherwise {
        my $ex = shift;
        warn Dumper($ex);
        
        RollbackTransaction($dbh);
        $ex->throw();
    };
    EndTransaction($dbh);
}

# ============================================================================

=head2 DBGetOptions (I<$context>)

=over

=item Database method to retrieve options.

=item B<parameters :>

=over

=item I<$context> : context of current request.

=back

=item B<return :> list of hashes containing data.

=back

=cut

# ============================================================================

sub DBGetOptions {
    my ($self, $context) = @_;
    
    my $group_id = $context->GetGroup()->GetRowid();
    my $dbh = $context->GetConfig()->GetDBH();
    my $result;

    my $sql = "SELECT * FROM news_pref WHERE group_id = $group_id";
    $result = SelectSingle($dbh, $sql);

    if (not defined $result) {
        ExecSQL($dbh, "INSERT INTO news_pref(created,modified,group_id) VALUES(NOW(),NOW(),$group_id)");
        $result = SelectSingle($dbh, $sql);
        
        throw Mioga2::Exception("News::DBGetOptions", $self->GetText("Options don't exist after INSERT !")) if (not defined $result);
    }
    
    return $result;
}

# ============================================================================

=head2 RevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

Remove user data in database when a user is revoked of a group.

This function is called when :

=over

=item - a user is revoked of a group

=item - a team is revoked of a group for each team member not namly invited in group

=item - a user is revoked of a team  for each group where the team is member and the user not namly invited in group

=back

=cut

# ============================================================================

sub RevokeUserFromGroup {
    my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;
    
    my $dbh     = $config->GetDBH();
    ExecSQL($dbh, "UPDATE news SET owner=$group_animator_id WHERE owner=$user_id AND group_id=$group_id");
}

# ============================================================================

=head2 DeleteGroupData ($config, $group_id)

Remove group data in database when a group is deleted.
This function is called when a group/user/resource is deleted.

=cut

# ============================================================================

sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;
    
    my $dbh = $config->GetDBH();

    ExecSQL($dbh, "DELETE FROM news WHERE group_id=$group_id");
    ExecSQL($dbh, "DELETE FROM news_pref WHERE group_id=$group_id");
}

# ============================================================================

=head2 GetStateFromPage (I<$page>)

=over

=item Determine state from given page.

=item B<parameters :>

=over

=item I<$page> : the page.

=back

=item B<return :> return state accordingly to page.

=back

=cut

# ============================================================================

sub GetStateFromPage {
    my ($self, $page) = @_;
    
    my $state;
    $state = PUBLISH    if ($page eq D_PUBLISH);
    $state = WAITING    if ($page eq D_WAITING);
    $state = DRAFT      if ($page eq D_DRAFT);
    
    return $state;
}

# ----------------------------------------------------------------------------
# NewsGetNews returns all news for a group if id_user == 0, or all news
# owned by $id_user if id_user!=0
# ----------------------------------------------------------------------------
sub NewsGetNews
{
	my ($self, $config, $group_id, $id_user, $delay) = @_;
	my $dbh = $config->GetDBH();

	my $sql = "select news.* from news, article_and_news_status where news.group_id=$group_id and news.status_id=article_and_news_status.rowid and article_and_news_status.label='".st_FormatPostgreSQLString(PUBLISH)."'";
	if ($id_user)
	{
		$sql .= " and news.owner=$id_user";
	}
    $sql .= " AND news.created >= DATE(NOW()) - $delay" if $delay;
	$sql .= " order by news.modified desc";
	my $result = SelectMultiple($dbh, $sql);
	return $result;
}
# ============================================================================

=head2 GetRSSFeed ($context, [$feed])

Return a feed of news.

=cut

# ============================================================================

sub GetRSSFeed {
    my ($self, $context, $ident, $feed, $max)   = @_;
    my $config  = $context->GetConfig;
    my $group;
    try {
        $group = Mioga2::Old::Group->new($config, ident => $ident);
    }
    otherwise {
        $group = Mioga2::Old::User->new($config, ident => $ident);
    };

    if (!$feed) {
        $feed   = XML::Atom::SimpleFeed->new(
            title   => __x("News feed of {group} group", group => $group->GetIdent),
            link    => { href => $config->GetProtocol . "://" . $config->GetServerName . $context->{uri}->GetURI, rel => 'self' },
            link    => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/News/DisplayMain",
            id      => "data:,group:".$group->GetIdent.":news",
        );
    }
    $max    = 20 unless $max;
    
    my $news = $self->DBGetNews($context, feed => 1, max => $max, group => $group);
    foreach my $news_item (@$news) {
        $feed->add_entry(
            title       => __x("[{group}] News: {title}", title => $news_item->{title}, group => $group->GetIdent),
            link        => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/News/DisplayMain",
            id          => "data:,group:".$group->GetIdent.":news:$news_item->{rowid}",
            author      => "$news_item->{firstname} $news_item->{lastname}",
            published   => Mioga2::Classes::Time->FromPSQL($news_item->{created})->RFC3339DateTime,
            updated     => Mioga2::Classes::Time->FromPSQL($news_item->{modified})->RFC3339DateTime,
            content     => $news_item->{text},
        );
    }
    return $feed;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2006-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
