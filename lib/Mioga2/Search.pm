# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Search.pm : The Mioga2 Search application.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Search;
use strict;
use utf8;
use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'search';

use Error qw(:try);
use POSIX qw(ceil floor);
use Mioga2::Content::XSLT;
use Mioga2::Classes::Time;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::Search::Config;
use Mioga2::Search::Finder;
use Mioga2::Search::Result;
use Mioga2::Search::Query;
use Mioga2::Search::Document;
use Mioga2::tools::Convert;
use Mioga2::UserList;
use Data::Dumper;
use Text::Iconv;
use Encode::Detect::Detector;
use Devel::Peek;

my $debug = 0;

# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc
{
	my $self = shift;
	my %AppDesc = ( ident   => 'Search',
		name	=> __('Search'),
                package => 'Mioga2::Search',
                description => __('The Mioga2 Search application'),
				type    => 'normal',
                all_groups => 1,
                all_users  => 1,
                can_be_public => 0,
				is_user             => 1,
				is_group            => 1,
				is_resource         => 0,
				 api_version => '2.3',


				functions => {
					'Standard'	=> __('User methods'),
				},

				func_methods => {
					'Standard'  => [ 'DisplayMain', 'SearchWS' ],
				},
				
				sxml_methods => { },
				
				xml_methods  => { },

				non_sensitive_methods => [
						'DisplayMain',
						'SearchWS',
					]
				
                );
	return \%AppDesc;
}

# ============================================================================
# TestAuthorize
#
# Grant access to everybody
#
# ============================================================================
sub TestAuthorize
{
	return AUTHZ_OK; # Everyone's authorized to use search engine
}

# ============================================================================
# Public Methods
# ============================================================================
# ============================================================================

=head2 DisplayMain ()

	Display main window and do search if args exists.

=cut

# ============================================================================

sub DisplayMain {
    my ($self, $context) = @_;
	print STDERR "Search::DisplayMain()\n" if ($debug);

	my $do_search = 0;
	my $data;
	if (exists($context->{args}->{query_string})) {
		utf8::decode($context->{args}->{query_string});
		Dump($context->{args}->{query_string}) if ($debug);
		$data = $self->SearchWS($context);
		$do_search = 1;
	}
	else {
		# Force check for search zones
		$data = { SearchInfos => { args => { search_text => 'on', search_tags => 'on', search_description => 'on' } } };
	}
	#
	# Generate XML response
	#
	my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayMain>";
	$xml .= $context->GetXML;
	$xml .= Mioga2::tools::Convert::PerlToXML($data);

	$xml .= "</DisplayMain>";
	print STDERR "xml = $xml\n" if $debug;
	#
	# Generate content
	#
	my $content;
	$content = new Mioga2::Content::XSLT($context, stylesheet => 'search.xsl', locale_domain => "search_xsl");
	$content->SetContent($xml);
	
	return $content;
}
# ============================================================================

=head2 SearchWS ()

	Web service to do search and get results.

=cut

# ============================================================================

sub SearchWS {
    my ($self, $context) = @_;
	print STDERR "Search::SearchWS()\n" if ($debug);

	$self->InitUserParams($context);
	my ($values, $errors) = $self->CheckArgs($context);

	$self->{search_config} = new Mioga2::Search::Config($context->GetConfig()->GetInstallPath . "/conf/search_conf.xml");
	my $result = {};
	if (length($self->{query_string}) > 0) {
		$result = $self->do_search($context, $values);
	}
	#
	# Lists of applications
	#
	my $apps = $self->{search_config}->GetApplications;
	$result->{application} = [];
	#first dummy application
	my $rec = { name => '', app_code => '0' };
	if ($values->{app_code} == 0) {
		$rec->{current} = 'yes';
	}
	push @{$result->{application}}, $rec;

	foreach my $a (keys(%$apps)) {
		$rec = { name => __($a), app_code => $apps->{$a}->{app_code} };
		if ($values->{app_code} == $apps->{$a}->{app_code}) {
			$rec->{current} = 'yes';
		}
		push @{$result->{application}}, $rec;
	}
	#
	# Lists of groups for user
	#
	$result->{group} = [];

	my $grouplist = $context->GetUser()->GetExpandedGroupList();
	my @list = $grouplist->GetGroups();
	print STDERR "group_list = ". Dumper(@list) ."\n" if ($debug > 2);
	my $config = $context->GetConfig();
	$rec = {name => '', rowid => 0 };
	if ($values->{group_id} == 0) {
		$rec->{current} = 'yes';
	}
	push @{$result->{group}}, $rec;

	foreach my $grp (@list) {
		$rec = {name => $grp->{ident}, rowid => $grp->{rowid} };
		if ($values->{group_id} == $grp->{rowid}) {
			$rec->{current} = 'yes';
		}
		push @{$result->{group}}, $rec;
	}

	print STDERR "result = ".Dumper($result)."\n" if ($debug > 1);
	return $result;
}
# ============================================================================
# Private methods
# ============================================================================
# InitUserParams
# Initialize some internal variables function of user
# ============================================================================
sub InitUserParams {
    my ($self, $context) = @_;
	print STDERR "Search::InitUserParams()\n" if ($debug);

	$self->{dbh} =	$context->GetConfig()->GetDBH();
	$self->{mioga_id} =	$context->GetConfig()->GetMiogaId();
	$self->{user_id} = $context->GetUser()->GetRowid();
	$self->{user_firstname} = $context->GetUser()->GetFirstname();
	$self->{user_lastname} = $context->GetUser()->GetLastname();
	$self->{user_email} = $context->GetUser()->GetEmail();
	$self->{group_id} =	$context->GetGroup()->GetRowid();
	$self->{fl_standard_user} =	$self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Standard");
}
# ============================================================================
# do_search ($self, $context, $query_string)
#
# ============================================================================
sub do_search
{
	my ($self, $context, $values) = @_;
	print STDERR "Search::do_search() \n" if($debug);
	my $xml = "";
	#
	# Prepare search request
	#
	my $config = $context->GetConfig();
	my $finder;
	my $mioga_files = $config->GetMiogaFilesDir();

	$finder = Mioga2::Search::Finder->new( { config => $config,
												base_dir => $mioga_files,
												search_config => $self->{search_config}, } );
	my $search_description = 1;
	if (!exists($values->{search_description}) || !$values->{search_description}) {
		$search_description = 0;
	}
	my $search_tags = 1;
	if (!exists($values->{search_tags}) || !$values->{search_tags}) {
		$search_tags = 0;
	}
	my $search_text = 1;
	if (!exists($values->{search_text}) || !$values->{search_text}) {
		$search_text = 0;
	}
	print STDERR "  values search_tags = ".$values->{search_tags}."  search_text = ".$values->{search_text}."  search_description = ".$values->{search_description}." \n" if($debug);
	print STDERR "  search_tags = $search_tags  search_text = $search_text  search_description = $search_description \n" if($debug);
	Dump($values->{query_string}) if($debug);
	my $params = { mode => 'standard',
	               lang => 'french',
				   qstring => $values->{query_string},
				   force_terms => $values->{force_terms},
				   search_description => $search_description,
				   search_tags => $search_tags,
				   search_text => $search_text,
				 };
	if ($values->{advanced_search}) {
		$params->{mode} = 'advanced';
		$params->{group_id} =  $values->{group_id};
		$params->{app_code} =  $values->{app_code};
	}
	$finder->Process( $params );

	my $group_list = $self->GetGroupList($config);
	my $bin_uri = $config->GetBinURI();
	my $RESULT_PER_PAGE = 10;
	my $MAX_PAGE_COUNT = 10;
	my $count = $RESULT_PER_PAGE;
	my $offset = $values->{offset};

	my $result = $finder->GetResult($self->{user_id}, $offset, $count);
	print STDERR "  " . $result->GetCount() . " result from offset $offset on total of : " . $result->GetTotalCount() . "\n" if ($debug);

	#
	# Format result
	#
	my $nb_pages = ceil($result->GetTotalCount()/$RESULT_PER_PAGE);
	my $current_page = floor($offset/$RESULT_PER_PAGE);
	print STDERR "  nb_pages = $nb_pages  current_page = $current_page\n" if ($debug);
	my $data = { SearchInfos => { args => $values,
	                              offset => (defined ($offset) && (int ($offset) > 0)) ? $offset : 1,
	                              last   => (int ($offset+$count) < int ($result->GetTotalCount ())) ? ($offset+$count) : $result->GetTotalCount (),
								  count  => $result->GetCount(),
								  total_count => $result->GetTotalCount(),
								  current_page => ($current_page + 1),
								  result_per_page => $RESULT_PER_PAGE,
								  page => [],
								  term => [],
								},
				  Result => [],
				};

	# Prepare corrected query string
	my $query = $finder->GetQuery();
	my $cq = $query->GetCorrectedQuery();
	print STDERR " cq = ".Dumper($cq)."\n" if ($debug);
	$data->{SearchInfos}->{query_terms} = [];
	#my $qtring = "";
	foreach my $t (@$cq) {
		my $rec;
		$rec->{term} = $t->{corrected};
		if ($t->{org} ne $t->{corrected}) {
			$data->{SearchInfos}->{corrected} = 1;
			$rec->{changed} = 1;
		}
		else {
			$rec->{changed} = 0;
		}
		push @{$data->{SearchInfos}->{query_terms}}, $rec;
	}
	#my $ct = $query->GetCorrectedTerms();
	#if (scalar(@$ct) > 0) {
		#$data->{SearchInfos}->{corrected_query} =  $query->GetCorrectedQuery();
		#$data->{SearchInfos}->{corrected_terms} =  $ct;
	#}

	my $page_count = 0;
	my $p = floor($current_page - ($MAX_PAGE_COUNT / 2));
	$p = 0 if ($p < 0);
	my $i = 0;
	while ( ($i < $MAX_PAGE_COUNT) and ($p < $nb_pages) ) {
		push @{$data->{SearchInfos}->{page}}, { number => ($p + 1),
		                                        offset => $p * $RESULT_PER_PAGE };
		$i++;
		$p++;
		$page_count++;
	}
	$data->{SearchInfos}->{nb_pages} = $page_count;
	if ($current_page > 0) {
		$data->{SearchInfos}->{previous_page} = { number => $current_page, offset => ($current_page - 1) * $RESULT_PER_PAGE, };
	}
	if (($current_page + 1) < $page_count) {
		$data->{SearchInfos}->{next_page} = { number => $current_page + 2, offset => ($current_page + 1) * $RESULT_PER_PAGE, };
	}
	foreach my $term (@{$result->GetTerms()}) {
		push @{$data->{SearchInfos}->{term}}, $term;
	}
	#
	# Results
	#
	foreach my $res (@{$result->GetResults()}) {
		print STDERR "res = ".Dumper($res) . "\n" if ($debug > 1);
		my $url = $res->{url};
		my $group = $group_list->{$res->{group_id}};
		$url =~ s/__MIOGA2-GROUP__/$group/;
		$url =~ s/__BIN-URI__/$bin_uri/;

		my $date = gmtime($res->{date});

		push @{$data->{Result}}, { percent => $res->{percent},
		                           app_type => $res->{app_code},
								   url => $url,
								   description => $res->{description},
								   group => $group,
								   mime => $res->{mime},
								   date => $date->strftime("%Y-%m-%d %H:%M:%S"),
								   highlight => $res->{highlight},
								   sup_data => $res->{data}
								};
	}

	print STDERR "data = ".Dumper($data)."\n" if ($debug > 1);
	return $data;
}
# ============================================================================
# GetGroupList
#
# ============================================================================
sub GetGroupList
{
	my ($self, $config) = @_;
	print STDERR "Search::GetGroupList() \n" if($debug);
	my $list = {};
	my $mioga_id = $config->GetMiogaId();
	my $sql = "SELECT rowid, ident FROM m_group_base WHERE mioga_id=$mioga_id";
	my $res = Mioga2::tools::database::SelectMultiple($config->GetDBH, $sql);
	if (!defined($res)) {
		throw Mioga2::Exception::Simple("Mioga2::Search::GetGroupList", __"Cannot read group list");
	}
	else {
		foreach my $r (@$res) {
			$list->{$r->{rowid}} = $r->{ident};
		}
	}

	return $list;
}
# ============================================================================
# CheckArgs
#
# ============================================================================
sub CheckArgs
{
	my ($self, $context) = @_;

	my $params = [	
			[ [ 'query_string'  ], 'disallow_empty', 'stripxws' ],
			[ [ 'app_code', 'group_id', 'offset', 'force_terms' ], 'want_int', 'allow_empty' ],
			[ [ 'search_text', 'search_tags', 'search_description' ], 'allow_empty', [ 'match', "on" ] ],
			[ [ 'advanced_search', 'act_search'  ], 'allow_empty', [ 'match', "on" ]  ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = ".Dumper($values)."\n" if ($debug);
	print STDERR "errors = ".Dumper($errors)."\n" if ($debug);

    #
	# Get query string. Convert it if necessary
	#
	my $query_string = "";
	if (exists($context->{args}->{query_string})) {
		$query_string = $context->{args}->{query_string};
	}
	elsif (exists($context->{args}->{query})) {
		$query_string = $context->{args}->{query};
	}
	my $conv    = Text::Iconv->new('utf8', 'utf8');
 	my $tmp_query = $conv->convert($query_string);
  
	unless ($tmp_query) {
		my $charset = detect($query_string) || 'iso-8859-15'; # defaults to latin9
		warn "charset = '$charset' for query_string = '$query_string'" if $debug;
		$conv = Text::Iconv->new($charset, "utf8");
		$query_string  = $conv->convert($query_string);
		warn "==> converted query_string = '$query_string'" if $debug;
	}
	utf8::decode($query_string);
	$query_string =~ s/\+/ /g;  # For GET request

	$self->{query_string} = $query_string;
    #
	# Get other parameters : offset and advanced search parameters
	#

	return ($values, $errors);
}
# ============================================================================
# RevokeUserFromGroup ($self, $config, $group_id, $user_id, $group_animator_id)
#
#    Remove user data in database when a user is revoked of a group.
#
#    This function is called when :
#    - a user is revoked of a group
#    - a team is revoked of a group for each team member not namly
#      invited in group
#    - a user is revoked of a team  for each group where the team is
#      member and the user not namly invited in group
#
# ============================================================================
sub RevokeUserFromGroup
{
	my ($self, $config, $group_id, $user_id, $anim_id) = @_;
	print STDERR "Search::RevokeUserFromGroup user_id = $user_id  group_id = $group_id  anim_id = $anim_id\n" if($debug);
	# Nothing to do

}
# ============================================================================
# DeleteGroupData ($self, $config, $group_id)
#
#    Remove group data in database when a group is deleted.
#
#    This function is called when a group/user/resource is deleted.
#
# ============================================================================
sub DeleteGroupData
{
	my ($self, $config, $group_id) = @_;
	print STDERR "Search::DeleteGroupData group_id = $group_id\n" if($debug);
	# Nothing to do
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__

