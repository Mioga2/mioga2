#===============================================================================
#
#         FILE:  Colbert.pm
#
#  DESCRIPTION:  The Mioga2 administration application.
#
# REQUIREMENTS:  ---
#        NOTES:  TODO: rewrite the external Mioga part to get rid
#        			of Mioga2::tools::API* modules.
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  05/07/2010 14:26
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Colbert.pm: The Mioga2 administration application.

=head1 DESCRIPTION

This application can manage Mioga2 users, groups, teams, resources and
connections with other Mioga2.

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Colbert;
use vars qw($VERSION);

$VERSION = "1.0.0";

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'colbert';

use Mioga2::UserList;
use Mioga2::GroupList;
use Mioga2::TeamList;
use Mioga2::ResourceList;
use Mioga2::ApplicationList;
use Mioga2::Classes::View;
use Mioga2::Classes::Grid;
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::tools::database;
use Mioga2::tools::args_checker;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Mioga2::CSV;
use Mioga2::XML::Simple;
use Mioga2::Crypto;
use Mioga2::ExternalMioga;
use Mioga2::Old::ResourceList;
use Mioga2::Old::Resource;
use Mioga2::tools::APIExternalMioga;
use Mioga2::Theme;
use Mioga2::TaskCategory;
use Mioga2::InstanceList;
use Mioga2::Skeleton;
use Mioga2::Login;
use Mioga2::tools::Convert;
use Mioga2::TagList;
use Mioga2::tools::date_utils;

use Mioga2::tools::APIAuthz; # TODO ( - 29/03/2011 16:43): To be removed once new animation application (and the related base objects) are written

use POSIX qw(tmpnam);
use XML::Simple qw(:strict);
use File::Path;
use IO::Scalar;
use Archive::Zip;
use File::Temp qw/tempfile tempdir/;
use File::Copy::Recursive qw/dirmove/;
use MIME::Entity;
use MIME::Words qw (:all);
use Encode::Detect::Detector;

use Error qw(:try);
use Data::Dumper;

my $debug = 0;


#===============================================================================

=head1 INTERFACE METHODS

=cut

#===============================================================================


#===============================================================================

=head2 DisplayMain

Display home page.

=head3 Generated XML

=over

	<DisplayMain>
		<miogacontext>See Mioga2::RequestContext</miogacontext>
		<users count="..."/>
		<groups count="..."/>
		<teams count="..."/>
	</DisplayMain>

=back

=cut

#===============================================================================
sub DisplayMain {
	my ($self, $context) = @_;

	my $xml = '<DisplayMain>';

	$xml .= $context->GetXML ();

	my ($users, $groups, $teams);

	# Get users
	$xml .= '<users>';
	$users = Mioga2::UserList->new ($context->GetConfig ());
	$xml .= '<total>' . $users->Count () . '</total>';
	$users = Mioga2::UserList->new ($context->GetConfig (), { attributes => { type => 'local_user' } });
	$xml .= '<local>' . $users->Count () . '</local>';
	$users = Mioga2::UserList->new ($context->GetConfig (), { attributes => { type => 'ldap_user' } });
	$xml .= '<ldap>' . $users->Count () . '</ldap>';
	$users = Mioga2::UserList->new ($context->GetConfig (), { attributes => { type => 'external_user' } });
	$xml .= '<external>' . $users->Count () . '</external>';
	$xml .= '</users>';

	# Get groups
	$xml .= '<groups>';
	$groups = Mioga2::GroupList->new ($context->GetConfig ());
	$xml .= '<total>' . $groups->Count () . '</total>';
	$xml .= '</groups>';

	# Get teams
	$xml .= '<teams>';
	$teams = Mioga2::TeamList->new ($context->GetConfig ());
	$xml .= '<total>' . $teams->Count () . '</total>';
	$xml .= '</teams>';

	$xml .= '</DisplayMain>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine DisplayMain  ----------


#===============================================================================

=head2 DisplayUsers

Display existing users.

=head3 Generated XML

=over

	<DisplayUsers>
		<miogacontext>See Mioga2::RequestContext</miogacontext>

		<!-- 
			List of users in grid format
		 -->
		<Grid name="users">
			...
		</Grid>
	</DisplayUsers>

=back

=cut

#===============================================================================
sub DisplayUsers {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['firstname', 'lastname', 'email', 'type', 'status', 'match' ], 'allow_empty' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);
	
	#-------------------------------------------------------------------------------
	# Create User View and Grid
	#-------------------------------------------------------------------------------
	my %view_desc = (
                       'fields' => [ [ 'rowid', 'rowid' ],
                              [ 'ident', 'normal', __('Identifier') ],
                              [ 'firstname', 'normal', __('Firstname') ],
                              [ 'lastname', 'normal', __('Lastname') ],
                              [ 'email', 'email', __('Email') ],
                              [ 'type', 'normal', __('Type') ],
                              [ 'status', 'normal', __('Status') ],
                              [ 'nb_groups', 'normal', __('Groups') ],
                              [ 'nb_teams', 'normal', __('Teams') ],
                              [ 'last_connection', 'date', __('Last connection') ],
                            ],

                       'filter' => [
                                    { ident =>  'firstname', label => __('Firstname'), type => 'text', value => $values->{firstname} },
									{ ident =>  'lastname', label => __('Lastname'), type => 'text', value => $values->{lastname}, focus => 1 },
									{ ident =>  'email', label => __('Email'), type => 'text', value => $values->{email} },
									{ ident =>  'type', label => __('Type'), type => 'list', value => $values->{type}, url => 'GetUserTypes.json', nodename => 'type', identifier => 'ident' },
									{ ident =>  'status', label => __('Status'), type => 'list', value => $values->{status}, url => 'GetUserStatuses.json', nodename => 'status', identifier => 'ident' },
								],
						'filter_match' => $values->{match},
                      
                       );

	my $user_view = new Mioga2::Classes::View ({desc => \%view_desc});
	my $grid_params = { ident => 'users',
	                    view => $user_view,
	                    get_data => 'GetUsers.json',
						nodename => 'user',
						identifier => 'ident',
						'label' => __('Identifier'),
	                    default_action => 'DisplayUserEdit',
						confirm_fields => ['firstname', 'lastname', 'email'],
						select_actions => [ 
								{ label => __("Export"), function => 'ExportUsers', type => 'file', post_var => 'rowid' },
						                  ],
	                  };

	# Add deletion and send-mail menu items if user can create
	if ($self->GetUserRights ()->{UsersCreate} || $self->GetUserRights ()->{Administration}) {
		push (@{$grid_params->{select_actions}}, { label => __("Delete"), function => 'DeleteUser.json', type => 'json', post_var => 'rowid', errors => 'HandleDeleteUserErrors', check_function => "CheckUserForDelete ('grid_users')" });
		push (@{$grid_params->{select_actions}}, { label => __("Send E-mail"), function => 'EmailUsers.json', type => 'json', post_var => 'rowid', errors => 'HandleMailErrors' });
	}

	# Get custom sort field and column widths from session
	my $session = $context->GetSession ();
	if (exists ($session->{persistent}->{DisplayUsers}->{sort_field})) {
		$grid_params->{sort_field} = $session->{persistent}->{DisplayUsers}->{sort_field};
	}
	if (exists ($session->{persistent}->{DisplayUsers}->{column_widths})) {
		$grid_params->{column_widths} = $session->{persistent}->{DisplayUsers}->{column_widths};
	}

	# Get SelectList-related preferences
	my $prefs = '<preferences>';
	if (exists ($session->{persistent}->{DisplayUsers}->{group_sort_field})) {
		$prefs .= '<grouplist><field>' . $session->{persistent}->{DisplayUsers}->{group_sort_field} . '</field><order>' . $session->{persistent}->{DisplayUsers}->{group_sort_order} . '</order></grouplist>';
	}
	if (exists ($session->{persistent}->{DisplayUsers}->{team_sort_field})) {
		$prefs .= '<teamlist><field>' . $session->{persistent}->{DisplayUsers}->{team_sort_field} . '</field><order>' . $session->{persistent}->{DisplayUsers}->{team_sort_order} . '</order></teamlist>';
	}
	if (exists ($session->{persistent}->{DisplayUsers}->{application_sort_field})) {
		$prefs .= '<applicationlist><field>' . $session->{persistent}->{DisplayUsers}->{application_sort_field} . '</field><order>' . $session->{persistent}->{DisplayUsers}->{application_sort_order} . '</order></applicationlist>';
	}
	$prefs .= '</preferences>';

	my $user_grid = new Mioga2::Classes::Grid ($grid_params);

	my $xml = '<DisplayUsers>';

	# Navigation
	$xml .= '<appnav>';
	$xml .= "<step><url>DisplayMain</url><label>" . __("Administration") . "</label></step>";
	$xml .= "<step><label>" . __("User list") . "</label></step>";
	$xml .= '</appnav>';

	$xml .= $context->GetXML ();

	$xml .= $prefs;

	$xml .= $user_grid->GetXML();

	$xml .= '</DisplayUsers>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine DisplayUsers  ----------


#===============================================================================

=head2 DisplayGroups

Display existing groups.

=head3 Generated XML

=over

	<DisplayGroups>
		<miogacontext>See Mioga2::RequestContext</miogacontext>

		<!-- 
			List of groups in grid format
		-->
		<Grid name="groups">
			...
		</Grid>
	</DisplayGroups>

=back

=cut

#===============================================================================
sub DisplayGroups {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['ident', 'animator', 'match' ], 'allow_empty' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);
	
	#-------------------------------------------------------------------------------
	# Create Group View and Grid
	#-------------------------------------------------------------------------------
	my %view_desc = (
                       'fields' => [ [ 'rowid', 'rowid'],
                              [ 'ident', 'normal', __('Identifier') ],
                              [ 'animator', 'normal', __('Animator') ],
							  [ 'nb_users', 'normal', __('Users') ],
							  [ 'nb_teams', 'normal', __('Teams') ],
							  [ 'created', 'date', __('Created') ],
							  [ 'last_connection', 'date', __('Last Connection') ],
                              [ 'disk_space_used', 'filesize', __('Disk space used') ],
                            ],

                       'filter' => [ { ident => 'ident', label => __('Identifier'), type => 'text', value => $values->{ident}, focus => 1 },
                                    { ident =>  'animator', label => __('Animator'), type => 'text', value => $values->{animator} },
								],
						'filter_match' => $values->{match},
                      
                       );

	my $group_view = new Mioga2::Classes::View ({desc => \%view_desc});
	my $grid_params = { ident => 'groups',
	                    view => $group_view,
	                    get_data => 'GetGroups.json',
						nodename => 'group',
						identifier => 'ident',
						'label' => __('Identifier'),
						confirm_fields => ['ident', 'nb_users', 'nb_teams'],
	                    default_action => 'DisplayGroupEdit',
						select_actions => [ ],
	                  };

	# Add deletion menu item if user can create
	if ($self->GetUserRights ()->{GroupsCreate} || $self->GetUserRights ()->{Administration}) {
		push (@{$grid_params->{select_actions}}, { label => __("Delete"), function => 'DeleteGroup.json', type => 'json', post_var => 'rowid', check_function => "CheckGroupForDelete ('grid_groups')", errors => 'GroupDeleteErrorCallback' });
	}

	# Get custom sort field and column widths from session
	my $session = $context->GetSession ();
	if (exists ($session->{persistent}->{DisplayGroups}->{sort_field})) {
		$grid_params->{sort_field} = $session->{persistent}->{DisplayGroups}->{sort_field};
	}
	if (exists ($session->{persistent}->{DisplayGroups}->{column_widths})) {
		$grid_params->{column_widths} = $session->{persistent}->{DisplayGroups}->{column_widths};
	}

	# Get SelectList-related preferences
	my $prefs = '<preferences>';
	if (exists ($session->{persistent}->{DisplayGroups}->{user_sort_field})) {
		$prefs .= '<userlist><field>' . $session->{persistent}->{DisplayGroups}->{user_sort_field} . '</field><order>' . $session->{persistent}->{DisplayGroups}->{user_sort_order} . '</order></userlist>';
	}
	if (exists ($session->{persistent}->{DisplayGroups}->{team_sort_field})) {
		$prefs .= '<teamlist><field>' . $session->{persistent}->{DisplayGroups}->{team_sort_field} . '</field><order>' . $session->{persistent}->{DisplayGroups}->{team_sort_order} . '</order></teamlist>';
	}
	if (exists ($session->{persistent}->{DisplayGroups}->{application_sort_field})) {
		$prefs .= '<applicationlist><field>' . $session->{persistent}->{DisplayGroups}->{application_sort_field} . '</field><order>' . $session->{persistent}->{DisplayGroups}->{application_sort_order} . '</order></applicationlist>';
	}
	$prefs .= '</preferences>';

	my $group_grid = new Mioga2::Classes::Grid ($grid_params);

	my $xml = '<DisplayGroups>';

	# Navigation
	$xml .= '<appnav>';
	$xml .= "<step><url>DisplayMain</url><label>" . __("Administration") . "</label></step>";
	$xml .= "<step><label>" . __("Group list") . "</label></step>";
	$xml .= '</appnav>';

	$xml .= $context->GetXML ();

	$xml .= $prefs;

	$xml .= $group_grid->GetXML();

	$xml .= '</DisplayGroups>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine DisplayGroups  ----------


#===============================================================================

=head2 DisplayTeams

Display existing teams.

=head3 Generated XML

=over

	<DisplayTeams>
		<miogacontext>See Mioga2::RequestContext</miogacontext>

		<!-- 
			List of teams in grid format
		-->
		<Grid name="teams">
			...
		</Grid>
	</DisplayTeams>

=back

=cut

#===============================================================================
sub DisplayTeams {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['ident', 'match' ], 'allow_empty' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);
	
	#-------------------------------------------------------------------------------
	# Create Team View and Grid
	#-------------------------------------------------------------------------------
	my %view_desc = (
                       'fields' => [ [ 'rowid', 'rowid'],
                              [ 'ident', 'normal', __('Identifier') ],
                              [ 'nb_users', 'normal', __('Users') ],
                              [ 'nb_groups', 'normal', __('Groups') ],
                              [ 'description', 'normal', __('Description') ],
                            ],

                       'filter' => [ { ident => 'ident', label => __('Identifier'), type => 'text', value => $values->{ident} },
								],
						'filter_match' => $values->{match},
                      
                       );

	my $team_view = new Mioga2::Classes::View ({desc => \%view_desc});
	my $grid_params = { ident => 'teams',
	                    view => $team_view,
	                    get_data => 'GetTeams.json',
						nodename => 'team',
						identifier => 'ident',
						'label' => __('Identifier'),
						confirm_fields => ['ident', 'nb_users', 'nb_groups'],
	                    default_action => 'DisplayTeamEdit',
						select_actions => [  ],
	                  };

	# Add deletion menu item if user can create
	if ($self->GetUserRights ()->{TeamsCreate} || $self->GetUserRights ()->{Administration}) {
		push (@{$grid_params->{select_actions}}, { label => __("Delete"), function => 'DeleteTeam.json', type => 'json', post_var => 'rowid' });
	}

	# Get custom sort field and column widths from session
	my $session = $context->GetSession ();
	if (exists ($session->{persistent}->{DisplayTeams}->{sort_field})) {
		$grid_params->{sort_field} = $session->{persistent}->{DisplayTeams}->{sort_field};
	}
	if (exists ($session->{persistent}->{DisplayTeams}->{column_widths})) {
		$grid_params->{column_widths} = $session->{persistent}->{DisplayTeams}->{column_widths};
	}

	# Get SelectList-related preferences
	my $prefs = '<preferences>';
	if (exists ($session->{persistent}->{DisplayTeams}->{user_sort_field})) {
		$prefs .= '<userlist><field>' . $session->{persistent}->{DisplayTeams}->{user_sort_field} . '</field><order>' . $session->{persistent}->{DisplayTeams}->{user_sort_order} . '</order></userlist>';
	}
	if (exists ($session->{persistent}->{DisplayTeams}->{group_sort_field})) {
		$prefs .= '<grouplist><field>' . $session->{persistent}->{DisplayTeams}->{group_sort_field} . '</field><order>' . $session->{persistent}->{DisplayTeams}->{group_sort_order} . '</order></grouplist>';
	}
	$prefs .= '</preferences>';

	my $team_grid = new Mioga2::Classes::Grid ($grid_params);

	my $xml = '<DisplayTeams>';

	# Navigation
	$xml .= '<appnav>';
	$xml .= "<step><url>DisplayMain</url><label>" . __("Administration") . "</label></step>";
	$xml .= "<step><label>" . __("Team list") . "</label></step>";
	$xml .= '</appnav>';

	$xml .= $context->GetXML ();

	$xml .= $prefs;

	$xml .= $team_grid->GetXML();

	$xml .= '</DisplayTeams>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine DisplayTeams  ----------


#===============================================================================

=head2 DisplayResources

Display existing resources.

=head3 Generated XML

=over

	<DisplayResources>
		<miogacontext>See Mioga2::RequestContext</miogacontext>

		<!-- 
			List of resources in grid format
		-->
		<Grid name="resources">
			...
		</Grid>
	</DisplayResources>

=back

=cut

#===============================================================================
sub DisplayResources {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	
	#-------------------------------------------------------------------------------
	# Create resource View and Grid
	#-------------------------------------------------------------------------------
	my %view_desc = (
                       'fields' => [ [ 'rowid', 'rowid'],
								[ 'ident', 'normal', __('Identifier') ],
								[ 'animator', 'normal', __('Animator') ],
								[ 'status', 'normal', __('Status') ],
								[ 'location', 'normal', __('Location') ],
                            ],

                       'filter' => [
								{ ident => 'ident', label => __('Identifier'), type => 'text' },
								{ ident => 'location', label => __('Location'), type => 'text' },
								{ ident => 'animator', label => __('Animator'), type => 'text' },
							],

						'filter_mode' => 'javascript',
                      
                       );

	my $resources_view = new Mioga2::Classes::View ({desc => \%view_desc});
	my $grid_params = { ident => 'resources',
	                    view => $resources_view,
	                    get_data => 'GetResources.json',
						nodename => 'resource',
						identifier => 'rowid',
						'label' => __('Identifier'),
						confirm_fields => ['ident'],
	                    default_action => 'DisplayResourceDialog',
						select_actions => [
									{ label => __("Change status"), function => 'ChangeResourceStatus.json', type => 'json', post_var => 'rowid' },
									
						                  ],
	                  };

	# Add deletion menu item if user can create
	if ($self->GetUserRights ()->{ResourcesCreate} || $self->GetUserRights ()->{Administration}) {
		push (@{$grid_params->{select_actions}}, { label => __("Delete"), function => 'DeleteResource.json', type => 'json', post_var => 'rowid' });
	}

	# Get custom sort field and column widths from session
	my $session = $context->GetSession ();
	if (exists ($session->{persistent}->{DisplayResources}->{sort_field})) {
		$grid_params->{sort_field} = $session->{persistent}->{DisplayResources}->{sort_field};
	}
	if (exists ($session->{persistent}->{DisplayResources}->{column_widths})) {
		$grid_params->{column_widths} = $session->{persistent}->{DisplayResources}->{column_widths};
	}

	my $resources_grid = new Mioga2::Classes::Grid ($grid_params);

	my $xml = '<DisplayResources>';

	# Navigation
	$xml .= '<appnav>';
	$xml .= "<step><url>DisplayMain</url><label>" . __("Administration") . "</label></step>";
	$xml .= "<step><label>" . __("Resources list") . "</label></step>";
	$xml .= '</appnav>';

	$xml .= $context->GetXML ();

	$xml .= $resources_grid->GetXML();

	$xml .= '</DisplayResources>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine DisplayResources  ----------


#===============================================================================

=head2 DisplayApplications

Display existing applications.

=head3 Generated XML

=over

	<DisplayApplications>
		<miogacontext>See Mioga2::RequestContext</miogacontext>

		<!-- 
			List of applications in grid format
		-->
		<Grid name="applications">
			...
		</Grid>
	</DisplayApplications>

=back

=cut

#===============================================================================
sub DisplayApplications {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	
	#-------------------------------------------------------------------------------
	# Create application View and Grid
	#-------------------------------------------------------------------------------
	my %view_desc = (
                       'fields' => [ [ 'rowid', 'rowid'],
								[ 'label', 'normal', __('Name') ],
								[ 'nb_users', 'normal', __('Users') ],
								[ 'nb_groups', 'normal', __('Groups') ],
								[ 'description', 'normal', __('Description') ],
                            ],

                       'filter' => [
								{ ident => 'label', label => __('Name'), type => 'text' },
							],

						'filter_mode' => 'javascript',
                      
                       );

	my $apps_view = new Mioga2::Classes::View ({desc => \%view_desc});
	my $grid_params = { ident => 'applications',
	                    view => $apps_view,
	                    get_data => 'GetApplications.json',
						nodename => 'application',
						identifier => 'rowid',
						confirm_fields => [],
						'label' => __('Identifier'),
	                    default_action => 'DisplayApplicationDialog',
	                  };

	# Get custom sort field and column widths from session
	my $session = $context->GetSession ();
	if (exists ($session->{persistent}->{DisplayApplications}->{sort_field})) {
		$grid_params->{sort_field} = $session->{persistent}->{DisplayApplications}->{sort_field};
	}
	if (exists ($session->{persistent}->{DisplayApplications}->{column_widths})) {
		$grid_params->{column_widths} = $session->{persistent}->{DisplayApplications}->{column_widths};
	}

	# Get SelectList-related preferences
	my $prefs = '<preferences>';
	if (exists ($session->{persistent}->{DisplayApplications}->{user_sort_field})) {
		$prefs .= '<userlist><field>' . $session->{persistent}->{DisplayApplications}->{user_sort_field} . '</field><order>' . $session->{persistent}->{DisplayApplications}->{user_sort_order} . '</order></userlist>';
	}
	if (exists ($session->{persistent}->{DisplayApplications}->{group_sort_field})) {
		$prefs .= '<grouplist><field>' . $session->{persistent}->{DisplayApplications}->{group_sort_field} . '</field><order>' . $session->{persistent}->{DisplayApplications}->{group_sort_order} . '</order></grouplist>';
	}
	$prefs .= '</preferences>';

	my $apps_grid = new Mioga2::Classes::Grid ($grid_params);

	my $xml = '<DisplayApplications>';

	# Navigation
	$xml .= '<appnav>';
	$xml .= "<step><url>DisplayMain</url><label>" . __("Administration") . "</label></step>";
	$xml .= "<step><label>" . __("Application list") . "</label></step>";
	$xml .= '</appnav>';

	$xml .= $context->GetXML ();

	$xml .= $prefs;

	$xml .= $apps_grid->GetXML();

	$xml .= '</DisplayApplications>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine DisplayApplications  ----------


#===============================================================================

=head2 DisplayExternalMiogas

Display existing external Mioga connections.

=head3 Generated XML

=over

	<DisplayExternalMiogas>
		<miogacontext>See Mioga2::RequestContext</miogacontext>

		<!-- 
			List of applications in grid format
		-->
		<Grid name="externalmiogas">
			...
		</Grid>
	</DisplayExternalMiogas>

=back

=cut

#===============================================================================
sub DisplayExternalMiogas {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	
	#-------------------------------------------------------------------------------
	# Create External Mioga View and Grid
	#-------------------------------------------------------------------------------
	my %view_desc = (
                       'fields' => [ [ 'rowid', 'rowid'],
                              [ 'server', 'normal', __('Server') ],
                              [ 'status', 'normal', __('Status') ],
                              [ 'last_synchro', 'date', __('Last synchro') ],
                              [ 'nb_user', 'normal', __('Users') ],
                            ],

                       'filter' => [ { ident => 'server', label => __('Server'), type => 'text' },
								],

						'filter_mode' => 'javascript',
                      
                       );

	my $ext_mioga_view = new Mioga2::Classes::View ({desc => \%view_desc});
	my $grid_params = { ident => 'external_miogas',
	                    view => $ext_mioga_view,
	                    get_data => 'GetExternalMiogas.json',
						nodename => 'external_mioga',
						identifier => 'rowid',
						confirm_fields => ['server', 'nb_user'],
						'label' => __('Identifier'),
						select_actions => [ { label => __("Change status"), function => 'ChangeExternalMiogaStatus.json', type => 'json', post_var => 'rowid' },
											{ label => __("Delete"), function => 'DeleteExternalMioga.json', type => 'json', post_var => 'rowid', check_function => "CheckExtMiogaForDelete ('grid_external_miogas')" },
						                  ],
	                  };

	# Get custom sort field and column widths from session
	my $session = $context->GetSession ();
	if (exists ($session->{persistent}->{DisplayExternalMiogas}->{sort_field})) {
		$grid_params->{sort_field} = $session->{persistent}->{DisplayExternalMiogas}->{sort_field};
	}
	if (exists ($session->{persistent}->{DisplayExternalMiogas}->{column_widths})) {
		$grid_params->{column_widths} = $session->{persistent}->{DisplayExternalMiogas}->{column_widths};
	}

	my $ext_mioga_grid = new Mioga2::Classes::Grid ($grid_params);

	my $xml = '<DisplayExternalMiogas>';

	# Navigation
	$xml .= '<appnav>';
	$xml .= "<step><url>DisplayMain</url><label>" . __("Administration") . "</label></step>";
	$xml .= "<step><label>" . __("External Mioga list") . "</label></step>";
	$xml .= '</appnav>';

	$xml .= $context->GetXML ();

	$xml .= $ext_mioga_grid->GetXML();

	$xml .= '</DisplayExternalMiogas>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine DisplayExternalMiogas  ----------


#===============================================================================

=head2 DisplayTaskCategories

Display existing task categories.

=head3 Generated XML

=over

	<DisplayTaskCategories>
		<miogacontext>See Mioga2::RequestContext</miogacontext>

		<!-- 
			List of categories in grid format
		-->
		<Grid name="taskcategories">
			...
		</Grid>
	</DisplayTaskCategories>

=back

=cut

#===============================================================================
sub DisplayTaskCategories {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	
	#-------------------------------------------------------------------------------
	# Create External Mioga View and Grid
	#-------------------------------------------------------------------------------
	my %view_desc = (
                       'fields' => [ [ 'rowid', 'rowid'],
								[ 'label', 'asis', __('Name') ],
					   			[ 'private', 'boolean', __('Private tasks') ],
								[ 'nb_tasks', 'normal', __('Associated tasks') ],
                            ],

                       'filter' => [
								],

						'filter_mode' => 'javascript',
                      
                       );

	my $task_categories_view = new Mioga2::Classes::View ({desc => \%view_desc});
	my $grid_params = { ident => 'task_categories',
	                    view => $task_categories_view,
	                    get_data => 'GetTaskCategories.json',
						nodename => 'category',
						identifier => 'rowid',
						confirm_fields => ['label'],
						'label' => __('Identifier'),
	                    default_action => 'DisplayTaskCategoryDialog',
						select_actions => [ { label => __("Delete"), function => 'DeleteTaskCategory.json', type => 'json', post_var => 'rowid', check_function => "CheckTaskCategoryForDelete ('grid_task_categories')" },
						                  ],
	                  };

	# Get custom sort field and column widths from session
	my $session = $context->GetSession ();
	if (exists ($session->{persistent}->{DisplayTaskCategories}->{sort_field})) {
		$grid_params->{sort_field} = $session->{persistent}->{DisplayTaskCategories}->{sort_field};
	}
	if (exists ($session->{persistent}->{DisplayTaskCategories}->{column_widths})) {
		$grid_params->{column_widths} = $session->{persistent}->{DisplayTaskCategories}->{column_widths};
	}

	my $task_category_grid = new Mioga2::Classes::Grid ($grid_params);

	my $xml = '<DisplayTaskCategories>';

	# Navigation
	$xml .= '<appnav>';
	$xml .= "<step><url>DisplayMain</url><label>" . __("Administration") . "</label></step>";
	$xml .= "<step><label>" . __("Task category list") . "</label></step>";
	$xml .= '</appnav>';

	$xml .= $context->GetXML ();

	$xml .= $task_category_grid->GetXML();

	$xml .= '</DisplayTaskCategories>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine DisplayTaskCategories  ----------


#===============================================================================

=head2 DisplayThemes

Display existing themes.

=head3 Generated XML

=over

	<DisplayThemes>
		<miogacontext>See Mioga2::RequestContext</miogacontext>

		<!-- 
			List of categories in grid format
		-->
		<Grid name="themes">
			...
		</Grid>
	</DisplayThemes>

=back

=cut

#===============================================================================
sub DisplayThemes {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	
	#-------------------------------------------------------------------------------
	# Create External Mioga View and Grid
	#-------------------------------------------------------------------------------
	my %view_desc = (
                       'fields' => [ [ 'rowid', 'rowid'],
								[ 'name', 'normal', __('Name') ],
								[ 'nb_groups', 'normal', __('Groups') ],
                            ],

                       'filter' => [ { ident => 'name', label => __('Name'), type => 'text' },
								],

						'filter_mode' => 'javascript',
                      
                       );

	my $themes_view = new Mioga2::Classes::View ({desc => \%view_desc});
	my $grid_params = { ident => 'themes',
	                    view => $themes_view,
	                    get_data => 'GetThemes.json',
						nodename => 'theme',
						identifier => 'rowid',
						'label' => __('Identifier'),
						confirm_fields => ['name'],
	                    # default_action => 'DisplayThemeDialog',
						select_actions => [  { label => __("Delete"), function => 'DeleteTheme.json', type => 'json', post_var => 'rowid', check_function => "CheckThemeForDelete ('grid_themes')" },
							],
	                  };

	# Get custom sort field and column widths from session
	my $session = $context->GetSession ();
	if (exists ($session->{persistent}->{DisplayThemes}->{sort_field})) {
		$grid_params->{sort_field} = $session->{persistent}->{DisplayThemes}->{sort_field};
	}
	if (exists ($session->{persistent}->{DisplayThemes}->{column_widths})) {
		$grid_params->{column_widths} = $session->{persistent}->{DisplayThemes}->{column_widths};
	}

	my $task_category_grid = new Mioga2::Classes::Grid ($grid_params);

	my $xml = '<DisplayThemes>';

	# Navigation
	$xml .= '<appnav>';
	$xml .= "<step><url>DisplayMain</url><label>" . __("Administration") . "</label></step>";
	$xml .= "<step><label>" . __("Themes list") . "</label></step>";
	$xml .= '</appnav>';

	$xml .= $context->GetXML ();

	$xml .= $task_category_grid->GetXML();

	$xml .= '</DisplayThemes>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine DisplayThemes  ----------


#===============================================================================

=head2 DisplayUserGroupReport

Display user to group report

=cut

#===============================================================================
sub DisplayUserGroupReport {
	my ($self, $context) = @_;

	my $data = $self->GetUserGroupReport ($context);

	my $xml = '';

	$xml .= '<DisplayUserGroupReport>';

	# Navigation
	$xml .= '<appnav>';
	$xml .= "<step><url>DisplayMain</url><label>" . __("Administration") . "</label></step>";
	$xml .= "<step><label>" . __("User to groups affectations") . "</label></step>";
	$xml .= '</appnav>';

	$xml .= $context->GetXML ();

	$xml .= '<Report>' . Mioga2::tools::Convert::PerlToXML ($data) . '</Report>';

	$xml .= '</DisplayUserGroupReport>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT ($context, stylesheet => 'colbert.xsl', locale_domain => "colbert_xsl");
	$content->SetContent ($xml);

	return ($content);
}	# ----------  end of subroutine DisplayUserGroupReport  ----------


#===============================================================================

=head2 ExportUserGroupReport

Export user to groups report as CSV

=cut

#===============================================================================
sub ExportUserGroupReport {
	my ($self, $context) = @_;

	my $csv = '';

	my $data = $self->GetUserGroupReport ($context);

	my $xml = '';

	$xml .= '<DisplayUserGroupReport>';
	$xml .= '<Report>' . Mioga2::tools::Convert::PerlToXML ($data) . '</Report>';
	$xml .= '</DisplayUserGroupReport>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT ($context, stylesheet => 'colbert-csv.xsl', locale_domain => "colbert_xsl", filename => $context->GetConfig ()->GetMiogaIdent () . '-UserGroupReport.csv');
	$content->SetContent ($xml);

	return ($content);
}	# ----------  end of subroutine ExportUserGroupReport  ----------


#===============================================================================

=head2 DisplayUserTeamReport

Display user to team report

=cut

#===============================================================================
sub DisplayUserTeamReport {
	my ($self, $context) = @_;

	my $data = $self->GetUserTeamReport ($context);

	my $xml = '';

	$xml .= '<DisplayUserTeamReport>';

	# Navigation
	$xml .= '<appnav>';
	$xml .= "<step><url>DisplayMain</url><label>" . __("Administration") . "</label></step>";
	$xml .= "<step><label>" . __("User to teams affectations") . "</label></step>";
	$xml .= '</appnav>';

	$xml .= $context->GetXML ();

	$xml .= '<Report>' . Mioga2::tools::Convert::PerlToXML ($data) . '</Report>';

	$xml .= '</DisplayUserTeamReport>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT ($context, stylesheet => 'colbert.xsl', locale_domain => "colbert_xsl");
	$content->SetContent ($xml);

	return ($content);
}	# ----------  end of subroutine DisplayUserTeamReport  ----------


#===============================================================================

=head2 ExportUserTeamReport

Export user to teams report as CSV

=cut

#===============================================================================
sub ExportUserTeamReport {
	my ($self, $context) = @_;

	my $csv = '';

	my $data = $self->GetUserTeamReport ($context);

	my $xml = '';

	$xml .= '<DisplayUserTeamReport>';
	$xml .= '<Report>' . Mioga2::tools::Convert::PerlToXML ($data) . '</Report>';
	$xml .= '</DisplayUserTeamReport>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT ($context, stylesheet => 'colbert-csv.xsl', locale_domain => "colbert_xsl", filename => $context->GetConfig ()->GetMiogaIdent () . '-UserTeamReport.csv');
	$content->SetContent ($xml);

	return ($content);
}	# ----------  end of subroutine ExportUserTeamReport  ----------


#===============================================================================

=head2 DisplayTeamGroupReport

Display team to group report

=cut

#===============================================================================
sub DisplayTeamGroupReport {
	my ($self, $context) = @_;

	my $data = $self->GetTeamGroupReport ($context);

	my $xml = '';

	$xml .= '<DisplayTeamGroupReport>';

	# Navigation
	$xml .= '<appnav>';
	$xml .= "<step><url>DisplayMain</url><label>" . __("Administration") . "</label></step>";
	$xml .= "<step><label>" . __("Team to groups affectations") . "</label></step>";
	$xml .= '</appnav>';

	$xml .= $context->GetXML ();

	$xml .= '<Report>' . Mioga2::tools::Convert::PerlToXML ($data) . '</Report>';

	$xml .= '</DisplayTeamGroupReport>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT ($context, stylesheet => 'colbert.xsl', locale_domain => "colbert_xsl");
	$content->SetContent ($xml);

	return ($content);
}	# ----------  end of subroutine DisplayTeamGroupReport  ----------


#===============================================================================

=head2 ExportTeamGroupReport

Export team to groups report as CSV

=cut

#===============================================================================
sub ExportTeamGroupReport {
	my ($self, $context) = @_;

	my $csv = '';

	my $data = $self->GetTeamGroupReport ($context);

	my $xml = '';

	$xml .= '<DisplayTeamGroupReport>';
	$xml .= '<Report>' . Mioga2::tools::Convert::PerlToXML ($data) . '</Report>';
	$xml .= '</DisplayTeamGroupReport>';

	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT ($context, stylesheet => 'colbert-csv.xsl', locale_domain => "colbert_xsl", filename => $context->GetConfig ()->GetMiogaIdent () . '-TeamGroupReport.csv');
	$content->SetContent ($xml);

	return ($content);
}	# ----------  end of subroutine ExportTeamGroupReport  ----------


#===============================================================================

=head2 DisplayTagLists

Display list of tags for instance

=cut

#===============================================================================
sub DisplayTagLists {
	my ($self, $context) = @_;

	my $data = { DisplayTagLists => $context->GetContext () };
	$data->{DisplayTagLists}->{appnav}->{step} = [
		{
			url   => 'DisplayMain',
			label => __('Administration')
		},
		{
			label => __('Tag lists')
		}
	];

	# Get instance tags
	my $tags = Mioga2::TagList->new ($context->GetConfig ());
	$data->{DisplayTagLists}->{catalog} = $tags->GetTagLists ();

	$data->{DisplayTagLists}->{free_tags} = $tags->GetFreeTagsStatus ();

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT ($context, stylesheet => 'colbert.xsl', locale_domain => "colbert_xsl");
	$content->SetContent ($xml);

	return ($content);
}	# ----------  end of subroutine DisplayTagLists  ----------


#===============================================================================

=head2 DisplayCatalogTags

Display tags for a specific catalog.

=cut

#===============================================================================
sub DisplayCatalogTags {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['catalog_id'], 'disallow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ('Mioga2::Colbert::DisplayCatalogTags', 'Wrong value for catalog_id');
	}

	my $data = { DisplayCatalogTags => $context->GetContext () };
	$data->{DisplayCatalogTags}->{appnav}->{step} = [
		{
			url   => 'DisplayMain',
			label => __('Administration')
		},
		{
			url   => 'DisplayTagLists',
			label => __('Tag lists')
		},
		{
			label => __('Tags catalog contents')
		},
	];

	# Get instance tags
	my $tags = Mioga2::TagList->new ($context->GetConfig ());
	$data->{DisplayCatalogTags}->{tag} = $tags->GetTags ($values->{catalog_id});

	# Get catalog name
	my @list = grep { $_->{rowid} == $values->{catalog_id} } @{$tags->GetTagLists ()->[0]};
	$data->{DisplayCatalogTags}->{catalog_name} = $list[0]->{ident};

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XSLT ($context, stylesheet => 'colbert.xsl', locale_domain => "colbert_xsl");
	$content->SetContent ($xml);

	return ($content);
}	# ----------  end of subroutine DisplayCatalogTags  ----------


#===============================================================================

=head1 WEBSERVICE METHODS

=cut

#===============================================================================


#===============================================================================

=head2 GetUsers

Get existing users as XML or JSON.

=head3 Incoming Arguments

=over

One of the following arguments can be passed to alter the content of the XML or
the "selected" attribute value (see note below):

=over

=over

=item B<group_id>

=item B<team_id>

=item B<application_id>

=back

=back

=back

=head3 Generated XML

=over

	<GetUsers>
		<user selected="...">
			<rowid>...</rowid>
			<firstname>...</firstname>
			<lastname>...</lastname>
			<email>...</email>
			<type>...</type>
			<lang>...</lang>
			<autonomous>...</autonomous>
			<shared>...</shared>
			<status>...</status>
			<last_conn>...</last_conn>
		</user>
		...
	</GetUsers>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

B<Note:> "selected" attribute is set if 'full_list' argument plus one of the following arguments is passed:

=over

=over

=item B<group_id>: members of the group will have their "selected" attribute set to 1

=item B<team_id>: members of the team will have their "selected" attribute set to 1

=item B<application_id>: users that can use the application will have their "selected" attribute set to 1

=item B<type>: the user type

=item B<external_mioga_id>: if type is 'external_user', this argument can restrict the list to a specific mioga.

=back

=back

=back

=cut

#===============================================================================
sub GetUsers {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetUsers] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['group_id', 'team_id', 'application_id', 'full_list', 'type', 'external_mioga_id'], 'allow_empty' ],
			[ ['firstname', 'lastname', 'email', 'type', 'status'], 'allow_empty', 'stripwxps' ],
			[ ['match'], 'allow_empty', [ 'match', "^\(begins|contains|ends\)\$" ] ],
			[ ['mode'], 'allow_empty', [ 'match', "^count\$" ] ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		$data->{user} = [ ];
		my $users;

		# Bug #1315 - Maintenance, re-enable disabled_attacked users can be re-enabled
		my $disabled = Mioga2::UserList->new ($context->GetConfig (), { attributes => { status => 'disabled_attacked' } });
		if ($disabled->Count ()) {
			for my $rowid (@{ac_ForceArray ($disabled->Get ('rowid'))}) {
				my $user = Mioga2::UserList->new ($context->GetConfig (), { attributes => { rowid => $rowid } });
				if (($user->Get ('status') eq 'disabled_attacked') && ((du_ISOToSecond ($user->Get ('last_auth_fail')) + $context->GetConfig ()->GetAuthFailTimeout ()) < time ())) {
					print STDERR "[Mioga2::Colbert::GetUsers] User $rowid has been disabled on " . $user->Get ('last_auth_fail') . " because of possible brute-force attacks and can now be re-enabled.\n" if $debug > 0;
					$user->Set ('auth_fail', 0);
					$user->Set ('status', 'active');
					$user->Store ();
				}
			}
		}

		my $revert = 0;
		if(st_ArgExists($context, 'group_id')) {
			my $group = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { rowid => $values->{group_id} } });
			$users = Mioga2::UserList->new ($context->GetConfig (), { attributes => { groups => $group }, short_list => 1 });
			$revert = 1;
		}
		elsif(st_ArgExists($context, 'team_id')) {
			my $team = Mioga2::TeamList->new ($context->GetConfig (), { attributes => { rowid => $values->{team_id} } });
			$users = Mioga2::UserList->new ($context->GetConfig (), { attributes => { teams => $team }, short_list => 1 });
			$revert = 1;
		}
		elsif(st_ArgExists($context, 'application_id')) {
			# TODO (SNI - 29/07/2010 09:34): 
			# my $app = Mioga2::ApplicationList->new ($context->GetConfig (), { attributes => { rowid => $values->{application_id} } });
			# $users = Mioga2::UserList->new ($context->GetConfig (), { attributes => { applications => $app }, short_list => 1 });
			# $revert = 1;
		}
		else {
			my $attrs;
			for my $attr (qw/type external_mioga_id firstname lastname email type status/) {
				$attrs->{$attr} = $values->{$attr} if (st_ArgExists ($context, $attr) && $values->{$attr} && $values->{$attr} ne '');
			}
			$users = Mioga2::UserList->new ($context->GetConfig (), { attributes => $attrs, match => $values->{match}, short_list => 1 });
		}

		# Load list according to input arguments
		$users->Load ();
		for (@{$users->{users}}) {
			$_->{selected}->{'$a'} = '1' if (st_ArgExists ($context, 'full_list')); # Only set "selected" attribute if full list is requested
			push @{$data->{user}}, $_;
		}

		# Revert list if required
		if ($revert && st_ArgExists ($context, 'full_list')) {
			$users->Revert ();
			$users->Load ();
			for (@{$users->{users}}) {
				push @{$data->{user}}, $_;
			}
		}

		# Translate last connection, type and status
		for my $user (@{$data->{user}}) {
			$user->{status} = __($user->{status});
			$user->{type} = __($user->{type});
			$user->{last_connection} = du_ConvertGMTISOToLocale ($user->{last_connection}, $context->GetUser ());
		}

		# if called with mode='count', just return user count
		if ($values->{mode}) {
			$data = { count => scalar (@{$data->{user}}) };
		}
	}
	else {
		@{$data->{errors}} = map { {$_->[0] => $_->[1]->[0]} } @$errors;
	}

	print STDERR "[Mioga2::Colbert::GetUsers] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetUsers  ----------


#===============================================================================

=head2 SXMLGetUsers

SXML Wrapper to GetUsers

See GetUsers method for instructions.

=cut

#===============================================================================
sub SXMLGetUsers {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SXMLGetUsers] Entering. Args: " . Dumper $context->{args} if ($debug);

	my $xml = $self->WSWrapper ($context, 'GetUsers', 'xml')->Generate ();

	my $content = new Mioga2::Content::XML ($context,
										    recipient_keyident => $context->GetSender());

	$content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine SXMLGetUsers  ----------

# ============================================================================

=head2 SXMLGetModifiedUserList ()

	Return a list of users modified since given date.

=cut

# ============================================================================
sub SXMLGetModifiedUserList {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SXMLGetModifiedUserList]\n" if ($debug);

	my $config = $context->GetConfig();
	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();


	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['last_synchro'], 'disallow_empty', 'pgdatetime'],
			[ ['idents'], 'disallow_empty'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("[Mioga2::Colbert::SXMLGetModifiedUserList]", __("Wrong argument value.") . Dumper $errors);
	}

	my $xml = "";
	try {
		if(!exists $values->{last_synchro}) {
			throw Mioga2::Exception::User("[Mioga2::Colbert::SXMLGetModifiedUserList]", __"No last_synchro date given");
		}
		if(!exists $values->{idents}) {
			throw Mioga2::Exception::User("[Mioga2::Colbert::SXMLGetModifiedUserList]", __"No idents list given");
		}
		my $last_synchro = $values->{last_synchro};
		print STDERR "date = $last_synchro\n" if ($debug);
	
		# Get modified users
		my $sql = "SELECT * FROM m_user_base WHERE modified > '$last_synchro' AND mioga_id=$mioga_id";
		my $res = SelectMultiple($dbh, $sql);
		
		print STDERR "res = ".Dumper($res)."\n" if ($debug);
		foreach my $user (@$res) {
			$xml .= "<User>";
   	
			foreach my $k (keys(%$user)) {
				$xml .= qq|<$k>|.st_FormatXMLString($user->{$k})."</$k>";
			}

			$xml .= "</User>";
		}
		# Get destroyed users
		my @ext_idents = sort split(/,/, $values->{idents});
		$sql = "SELECT ident FROM m_user_base WHERE mioga_id=$mioga_id";
		$res = SelectMultiple($dbh, $sql);
		my @idents;
		foreach my $u (@$res) {
			push @idents, $u->{ident};
		}
		foreach my $entry (@ext_idents) {
			if (!grep (/^$entry$/, @idents)) {
				$xml .= "<DeletedUser>$entry</DeletedUser>";
			}
		}

		print STDERR "xml = $xml\n" if ($debug);
	}

	catch Mioga2::Exception::User with {
		my $err = shift;
		
		$xml = qq|<Error type="NoSuchUser"/>|;
	};

	my $content = new Mioga2::Content::XML($context, recipient_keyident => $context->GetSender());

	$content->SetContent($xml);

	return $content;
}


#===============================================================================

=head2 GetExternalMiogaUsers

Get users from an external Mioga as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: the external Mioga rowid.

=back

=back

=head3 Generated XML

=over

	<GetUsers>
		<user selected="...">
			<rowid>...</rowid>
			<firstname>...</firstname>
			<lastname>...</lastname>
			<email>...</email>
			<type>...</type>
			<lang>...</lang>
			<autonomous>...</autonomous>
			<shared>...</shared>
			<status>...</status>
			<last_conn>...</last_conn>
		</user>
		...
	</GetUsers>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetExternalMiogaUsers {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetExternalMiogaUsers] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };
	$data->{user} = [ ];
	
	if (!@$errors) {
		my $external_mioga = new Mioga2::ExternalMioga($context->GetConfig (), rowid => $values->{rowid});
		my $response = $external_mioga->SendRequest(application => 'Colbert', method => 'SXMLGetUsers', 
											  # args => { user_ident => '*' },
											  );

		# Load already invited users
		my $invited_users = Mioga2::UserList->new ($context->GetConfig (), { attributes => { type => 'external_user', external_mioga_id => $values->{rowid} }, short_list => 1 });
		$invited_users->Load ();
		my @invited_emails = ();
		for (@{ac_ForceArray ($invited_users->{users})}) {
			push (@invited_emails, $_->{email});
		}

		for (@{ac_ForceArray (XMLin($response, ForceArray => 0, KeyAttr => '$t')->{GetUsers}->{user})}) {
			my $email = $_->{email};
			$_->{selected} = '1' if (grep (/^$email$/, @invited_emails));
			push (@{$data->{user}}, $_);
		}

	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::GetExternalMiogaUsers] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetExternalMiogaUsers  ----------


#===============================================================================

=head2 GetUserDetails

Get a user's details as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: the user to get details.

=back

=back

=head3 Generated XML

=over

	<GetUserDetails>
		<user rowid="...">
			<firstname>...</firstname>
			<lastname>...</lastname>
			<email>...</email>
			<type>...</type>
			<lang>...</lang>
			<autonomous>...</autonomous>
			<shared>...</shared>
			<status>...</status>
			<last_conn>...</last_conn>
		</user>
		<groups>
			<group rowid="..." is_member="..." is_animator="...">
				<ident>...</ident>
				<description>...</description>
			<group>
			...
		</groups>
		<teams>
			<team rowid="..." is_member="...">
				<ident>...</ident>
			<team>
			...
		</teams>
		<applications>
			<application rowid="..." is_active="...">
				<ident>...</ident>
				<description>...</description>
			</application>
		</applications>
	</GetUserDetails>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetUserDetails {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetUserDetails] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ],
			[ ['full_list'], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		# Get user details
		my $user = Mioga2::UserList->new ($context->GetConfig (), { attributes => { rowid => $values->{rowid} } });
		$user->Load ();
		$data->{user} = $user->{users}->[0];

		# Get user groups
		my $groups = $user->GetInvitedGroupList (short_list => 1);
		$groups->Load ();
		for (@{$groups->{groups}}) {
			$_->{selected}->{'$a'} = 1 if (st_ArgExists ($context, 'full_list'));
			push (@{$data->{groups}->{group}}, $_);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$groups->Revert ();
			$groups->Load ();
			for (@{$groups->{groups}}) {
				push (@{$data->{groups}->{group}}, $_);
			}
		}

		# Get user teams
		my $teams = $user->GetInvitedTeamList (short_list => 1);
		$teams->Load ();
		for (@{$teams->{teams}}) {
			$_->{selected}->{'$a'} = 1 if (st_ArgExists ($context, 'full_list'));
			push (@{$data->{teams}->{team}}, $_);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$teams->Revert ();
			$teams->Load ();
			for (@{$teams->{teams}}) {
				push (@{$data->{teams}->{team}}, $_);
			}
		}

		# Get user applications
		my $applications = $user->GetApplicationList ();
		$applications->Load ();
		for my $app (@{$applications->{applications}}) {
			my $ident = $app->{ident};
			$app->{selected}->{'$a'} = 1 if (st_ArgExists ($context, 'full_list'));
			$app->{protected} = 1 if (grep (/^$ident$/, $user->GetProtectedApplications ()));
			push (@{$data->{applications}->{application}}, $app);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$applications->Revert ();
			$applications->Load ();
			for (@{$applications->{applications}}) {
				push (@{$data->{applications}->{application}}, $_);
			}
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::GetUserDetails] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetUserDetails  ----------


#===============================================================================

=head2 SXMLGetUserDetails

SXML Wrapper to GetUserDetails

See GetUserDetails method for instructions.

=cut

#===============================================================================
sub SXMLGetUserDetails {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SXMLGetUserDetails] Entering. Args: " . Dumper $context->{args} if ($debug);

	my $xml = $self->WSWrapper ($context, 'GetUserDetails', 'xml')->Generate ();

	my $content = new Mioga2::Content::XML ($context,
										    recipient_keyident => $context->GetSender());

	$content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine SXMLGetUserDetails  ----------



# ============================================================================

=head2 SXMLCheckUserPassword ()

	Check the given user password.

=head3 Generated XML

    Standard Mioga2 XML Request Format (See Mioga2::Content::XML) with following body :
	
	<Authen status="ok|forbidden"/>|

=cut

# ============================================================================

sub SXMLCheckUserPassword {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	
	my $ident    = $context->{args}->{user_ident};
	my $password = $context->{args}->{user_password};

	my $xml;
	
	try {
		my $user = Mioga2::UserList->new ($config, { attributes => { ident => $ident } });
		if($user->CheckPassword($password)) {
			$xml = qq|<Authen status="ok"/>|;
		}
		else {
			$xml = qq|<Authen status="forbidden"/>|;
		}
	}

	catch Mioga2::Exception::User with {
		$xml = qq|<Authen status="forbidden"/>|;
	};

	my $content = new Mioga2::Content::XML($context,
										   recipient_keyident => $context->GetSender());

	$content->SetContent($xml);

	return $content;
}


#===============================================================================

=head2 SetUser

Create or update a user.

=head3 Incoming Arguments

=over

=over

=item B<rowid> (I<optional>): The rowid of the user to update. At creation time, this argument doesn't exist.

=item B<firstname>: The user's firstname.

=item B<lastname>: The user's lastname.

=item B<email>: The user's email.

=item B<password> (I<optional>): The user's password.

=item B<password2> (I<optional>): The user's password confirmation.

=item B<lang>: The user's language ident.

=item B<lang_id> (I<optional>): The user's language rowid.

=item B<theme> (I<optional>): The user's theme ident.

=item B<autonomous> (I<optional>): The user's autonomous status.

=item B<shared> (I<optional>): The user's shared status.

=item B<status>: The user's status ident.

=item B<status_id> (I<optional>): The user's status rowid.

=item B<skeleton>: The user's skeleton file name.

=item B<groups> (I<optional>): An array of group idents the user is member of.

=item B<teams> (I<optional>): An array of team idents the user is member of.

=item B<applications> (I<optional>): An array of application idents the user can use.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<SetUser>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</SetUser>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub SetUser {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SetUser] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Crash if trying to execute SetUser while centralized
	#-------------------------------------------------------------------------------
	if ($context->GetConfig ()->GetMiogaConf ()->IsCentralizedUsers ()) {
		throw Mioga2::Exception::Application ('Mioga2::Colbert::SetUser', 'Trying to update / create user while user base is centralized');
	}

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ [ 'rowid' ], 'allow_empty' ],
			[ [ 'firstname', 'lastname' ], 'disallow_empty', 'stripxws' ],
			[ [ 'autonomous', 'shared' ], 'allow_empty', [ 'match', "\(0|1\)" ] ],
			[ [ 'lang', 'status' ], 'disallow_empty', 'stripxws' ],
			[ [ 'theme' ], 'allow_empty', 'stripxws' ],
			[ [ 'groups', 'teams', 'applications' ], 'allow_empty' ],
			[ [ 'lang_id', 'status_id' ], 'want_int', 'allow_empty' ],
		];

	# Special treatment for email. If creating a user, first check the email is unused, else simply check it looks like an email.
	if (st_ArgExists ($context, 'rowid')) {
		push (@$params, [ [ 'email' ], 'disallow_empty', 'want_email', 'stripxws', ['max' => 128] ]);
		push (@$params, [ [ 'ident' ], 'allow_empty', 'stripxws', ['max' => 128] ]);
		push (@$params, [ [ 'skeleton' ], 'allow_empty', 'filename' ]);
		if (st_ArgExists ($context, 'password') || st_ArgExists ($context, 'password2')) {
			push (@$params, [ [ 'password', 'password2' ], 'disallow_empty', [ 'password_policy', $context->GetConfig () ], ['max' => 128] ]);
		}
	}
	else {
		push (@$params, [ [ 'ident' ], 'allow_empty', [ 'not_used_group_ident', $context->GetConfig () ], [ 'valid_ident', $context->GetConfig () ], ['max' => 128], 'stripxws' ]);
		push (@$params, [ [ 'email' ], 'disallow_empty', 'want_email', [ 'unused_email', $context->GetConfig () ], 'stripxws', ['max' => 128] ]);
		push (@$params, [ [ 'password', 'password2' ], 'allow_empty', [ 'password_policy', $context->GetConfig () ], ['max' => 128] ]);
		push (@$params, [ [ 'skeleton' ], 'disallow_empty', 'filename' ]);
	}

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	delete ($values->{ident}) if ($values->{ident} && $values->{ident} eq '');

	# Check if two passwords match
	if (st_ArgExists ($context, 'password') && st_ArgExists ($context, 'password2') && ($values->{password} ne $values->{password2})) {
		# Both passwords are different, push an error to the list
		push (@$errors, ['password2', ['different_passwords']]);
	}
	elsif (st_ArgExists ($context, 'password') && st_ArgExists ($context, 'password2')) {
		# Both passwords are the same, delete validation
		delete ($values->{password2});
	}

	# Check user can use the skeleton
	try {
		if ($values->{skeleton} && !$self->CheckSkeletonAccess ($context, 'user', $values->{skeleton})) {
			push (@$errors, ['skeleton', ['forbidden']]);
		}
	}
	otherwise {
		push (@$errors, ['skeleton', ['parse_error']]);
	};

	my $data = {
		success => 1
	};

	if (!@$errors) {
		my $user;
		my @groups = @{ac_ForceArray (delete ($values->{groups}))};
		my @teams = @{ac_ForceArray (delete ($values->{teams}))};
		my @applications = @{ac_ForceArray (delete ($values->{applications}))};
		if (st_ArgExists ($context, 'rowid')) {
			$self->{db}->BeginTransaction ();
			try {
				#-------------------------------------------------------------------------------
				# Update existing user
				#-------------------------------------------------------------------------------
				# Load user from rowid
				$user = Mioga2::UserList->new ($context->GetConfig (), { attributes => { rowid => $values->{rowid} } });

				# Update user
				for my $key (keys (%$values)) {
					next if (grep (/^$key$/, qw/ident groups teams applications/));
					$user->Set ($key, $values->{$key});
				}
				$user->Store ();
				$self->{db}->EndTransaction ();
			}
			otherwise {
				my $err = shift;
				if ($err->as_string =~ m/m_user_base_email_mioga_index/) {
					push (@$errors, [ 'email', [ 'already_used_email' ] ]);
				}
				else {
					print STDERR "[Mioga2::Colbert::SetUser] Update failed\n";
					print STDERR $err->stringify ($context);
					$self->{db}->RollbackTransaction ();
					$data->{success} = 0;
				}
			};
		}
		else {
			try {
				#-------------------------------------------------------------------------------
				# Create new user
				#-------------------------------------------------------------------------------
				# Prefix skeleton path to the file name
				if (st_ArgExists ($context, 'skeleton')) {
					$values->{skeleton} = $context->GetConfig ()->GetSkeletonDir () . "/$values->{lang}/user/$values->{skeleton}";
				}

				# Actually create user
				$user = Mioga2::UserList->new ($context->GetConfig (), { attributes => $values });
				$user->Store ();
			}
			otherwise {
				my $err = shift;
				print STDERR "[Mioga2::Colbert::SetUser] User creation failed, destroying.\n";
				print STDERR $err->stringify ($context);
				my $journal = $user->GetJournal ();
				$journal->SetMetadata ({ stage => 'rollback' });
				$user->Delete ();
				$data->{success} = 0;
			};
		}

		if (!@$errors && $data->{success}) {
			#-------------------------------------------------------------------------------
			# Invite user into groups
			#-------------------------------------------------------------------------------
			# Groups idents can't be passed as-is to Mioga2::UserList.
			# The group list must be a hash whose keys are groups idents and values are profile idents.
			# In this case, the profile ident is set to an empty string so that default profile is applied to user.
			my %groups;
			for (@groups) {
				$groups{$_} = '';
			}
			$user->JoinGroups (%groups);

			#-------------------------------------------------------------------------------
			# Invite user into teams
			#-------------------------------------------------------------------------------
			$user->JoinTeams (@teams);

			#-------------------------------------------------------------------------------
			# Enable applications for user
			#-------------------------------------------------------------------------------
			# Ensure Colbert is not getting disabled for admin user
			my $admin_id = $context->GetConfig ()->GetAdminId ();
			if (st_ArgExists ($context, 'rowid') && $values->{rowid} == $admin_id && !grep (/^Colbert$/, @applications)) {
				push (@$errors, [ 'applications', [ 'cannot_disable_Colbert' ] ]);
			}
			else {
				# Ensure protected applications are not getting disabled
				for my $app ($user->GetProtectedApplications ()) {
					if (!grep (/^$app$/, @applications)) {
						push (@$errors, [ 'applications', [ 'cannot_disable_' . $app ] ]);
					}
				}
				if (!@$errors) {
					# Applications idents can't be passed as-is to Mioga2::UserList.
					# The array must contain hashes with an 'ident' key and eventually other keys for application configuration (Workspace for example)
					my @apps = ();
					for (@applications) {
						push (@apps, { ident => $_ });
					}
					$user->EnableApplications (@apps);
				}
			}

			#-------------------------------------------------------------------------------
			# Return rowid of user
			#-------------------------------------------------------------------------------
			$data->{rowid} = $user->Get ('rowid');
		}

		$data->{journal} = $user->GetJournal ()->ToArray ();
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::SetUser] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetUser  ----------


#===============================================================================

=head2 InviteExternalUsers

Invite users from an external Mioga.

=head3 Incoming Arguments

=over

=over

=item B<ext_mioga_id>: The external Mioga rowid

=item B<rowid>: The user's rowid.

=item B<skeleton>: The user's skeleton.

=back

=back

=cut

#===============================================================================
sub InviteExternalUsers {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::InviteExternalUsers] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ [ 'ext_mioga_id', 'rowid' ], 'disallow_empty' ],
			[ [ 'skeleton' ], 'disallow_empty', 'filename' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $config = $context->GetConfig ();
		my $ext_mioga = new Mioga2::ExternalMioga($config, rowid => $values->{ext_mioga_id});

		for (@{ac_ForceArray ($values->{rowid})}) {
			my $response = $ext_mioga->SendRequest(application => 'Colbert', method => 'SXMLGetUserDetails',
												   args => {rowid => $_});

			my $xs = new Mioga2::XML::Simple();
			my $xmltree = $xs->XMLin($response);

			my $ext_user = $xmltree->{GetUserDetails}->{user};

			my $user = { };
			for (qw/firstname lastname email password lang autonomous shared status/) {
				$user->{$_} = $ext_user->{$_} if (exists ($ext_user->{$_}));
			}

			$user->{skeleton} = $context->GetConfig ()->GetSkeletonDir () . "/$user->{lang}/user/$values->{skeleton}";

			$user->{external_ident} = $ext_user->{ident};
			$user->{external_mioga_id} = $values->{ext_mioga_id};
			$user->{type} = 'external';

			try {
				my $u = Mioga2::UserList->new ($config, { attributes => $user });
				$u->Store ();
			}
			otherwise {
				push (@$errors, ['cannot_add', ["$user->{firstname} $user->{lastname} ($user->{email})", "$user->{firstname} $user->{lastname} ($user->{email})"]]);
			};
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::InviteExternalUsers] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine InviteExternalUsers  ----------


#===============================================================================

=head2 DeleteUser

Delete a user.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: The rowid of the user to delete.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<DeleteUser>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</DeleteUser>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub DeleteUser {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::DeleteUser] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		# Remove rowids of users that are animator of a group or a resource
		my @grps;
		my $groups = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { anim_id => \@{ac_ForceArray ($values->{rowid})} } });
		push (@grps, $groups->GetGroups ());
		my $resources = Mioga2::ResourceList->new ($context->GetConfig (), { attributes => { anim_id => \@{ac_ForceArray ($values->{rowid})} } });
		push (@grps, $resources->GetResources ());
		my @rowids = @{ac_ForceArray ($values->{rowid})};
		for (@grps) {
			push (@$errors, [ 'is_animator', [ $_->{animator} ] ]);
			my $anim_id = $_->{anim_id};
			@rowids = grep (!/^$anim_id$/, @rowids);
		}

		my $user = Mioga2::UserList->new ($context->GetConfig (), { attributes => { rowid => \@rowids } });
		$user->Delete ();
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::DeleteUser] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DeleteUser  ----------


#===============================================================================

=head2 ImportUsers

Import users from CSV file.

=head3 Incoming Arguments

=over

=over

=item B<file>: The uploaded file name. This argument will not be processed but the uploaded file content will.

=item B<mode>: The import mode, 'create' or 'update'.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<ImportUsers>
		<errors>
			<error line="...">...</error>
			...
		</errors>
	</ImportUsers>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub ImportUsers {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::ImportUsers] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['file'], 'disallow_empty' ],
			[ ['mode'], 'disallow_empty', [ 'match', "\(update|create\)" ] ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $upload_data = st_CheckUTF8($context->GetUploadData ());

	my $data = { };

	if (!@$errors) {
		# Convert CSV to array
		my $converter = Mioga2::CSV->new (
			columns => [ qw/lastname firstname email ident password skeleton/ ],
			separator => ',',
			quote => '"'
		);
		my $content = $converter->Convert ($upload_data);

		my $line = 1;
		for my $csvline (@$content) {
			if (@{ac_CheckValue ($csvline->{ident}, [ 'valid_ident', $context->GetConfig () ])}) {
				push (@{$data->{errors}}, ['import', [ $line, __('Invalid ident') ]]);
				next;
			}

			if (@{ac_CheckValue ($csvline->{email}, 'want_email')}) {
				push (@{$data->{errors}}, ['import', [ $line, __('Invalid email') ]]);
				next;
			}

			my %user_data;
			if ($values->{'mode'} eq 'update') {
				# If in update mode, select users from their ident
				$user_data{ident} = $csvline->{ident};
			}
			else {
				# If in create mode, no need to select users, all the fields are passed to Mioga2::UserList::New
				%user_data = %$csvline;
			}

			# Prefix skeleton path to the file name
			if (exists ($user_data{skeleton})) {
				$user_data{skeleton} = $context->GetConfig ()->GetSkeletonDir () . '/' . $context->GetUser ()->GetLang () . '/user/' . $user_data{skeleton};
			}

			# Initialize Mioga2::UserList
			my $user = Mioga2::UserList->new ($context->GetConfig (), { attributes => \%user_data });
			try {
				if ($values->{mode} eq 'update') {
					# If in update mode, set each field according to CSV contents
					for (keys (%$csvline)) {
						$user->Set ($_, $csvline->{$_});
					}
				}
				else {
					$user->Load ();
					if ($user->Count ()) {
						throw Mioga2::Exception::User ("User already exists");
					}
				}
				$user->Store ();
				$user->Load ();
				push (@{$data->{import}}, { rowid => $user->Get ('rowid') });
			}
			otherwise {
				my $err = shift;
				my $error;
				# Translate error to something that's user-friendly
				for ($err->as_string ()) {
					if (/m_user_base_email_mioga_index/) {
						$error = __("Email is already in use");
					}
					elsif (/Ident is already in use/ || /$user_data{ident}/) {
						$error = __("Ident is already in use");
					}
					elsif (/User already exists/) {
						$error = __("User already exists");
					}
					elsif (/Missing skeleton/) {
						$error = __("Missing skeleton");
					}
					else {
						$error = $err->as_string ();
					}
				}
				push (@{$data->{errors}}, ['import', [ $line, $error ]]);
			};
		}
		continue {
			$line++;
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0], [ $_->[1]->[0] ] });
	}

	ac_I18NizeErrors($errors);
	print STDERR "[Mioga2::Colbert::ImportUsers] Data: " . Dumper $data if ($debug);

	my $xml = '<ImportUsers>';

	$xml .= $context->GetXML ();

	for (@{$data->{errors}}) {
		my $err = $_->[1];
		if ($_->[0] eq 'import') {
			$xml .= "<Import><line>$err->[0]</line><error>$err->[1]</error></Import>";
		}
		else {
			print STDERR "Mioga2::Colbert::ImportUsers Unknown error code $_->[0]\n";
		}
	}

	$xml .= '</ImportUsers>';

	print STDERR "Colbert::ImportUsers xml: $xml \n" if ($debug);

	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert-parts.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return $content;
}	# ----------  end of subroutine ImportUsers  ----------


#===============================================================================

=head2 ExportUsers

Export users from CSV file.

=head3 Incoming Arguments

=over

=over

=item B<rowid> (I<optional>): An array of user-rowids to export.

=back

=back

=head3 Generated content

=over

The generated content is a CSV file containing user details.

=back

=cut

#===============================================================================
sub ExportUsers {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::ExportUsers] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'allow_empty' ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $csv = '';

	if (!@$errors) {
		my $users;
		if (st_ArgExists ($context, 'rowid')) {
			$users = Mioga2::UserList->new ($context->GetConfig (), { attributes => { rowid => $values->{rowid} } });
		}
		else {
			$users = Mioga2::UserList->new ($context->GetConfig ());
		}
		$csv = $users->ToCSV ();
	}

    my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/csv', filename => 'mioga-users.csv');
    $content->SetContent( $csv );

	print STDERR "[Mioga2::Colbert::ExportUsers] CSV " . Dumper $csv if ($debug);

    return ($content);
}	# ----------  end of subroutine ExportUsers  ----------


#===============================================================================

=head2 EmailUsers

E-mail a link to users so they can access Doris to define their password.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: An array of user-rowids to send mail to.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<EmailUsers>
		<errors>
			<error rowid="...">...</error>
			...
		</errors>
	</EmailUsers>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub EmailUsers {
	my ($self, $context) = @_;

	print STDERR "[Mioga2::Colbert::EmailUsers] Entering\n" if ($debug);
	
	print STDERR "args = ".Dumper($context->{args})."\n" if($debug);
	my $config = $context->GetConfig();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
		[ ['rowid'], 'disallow_empty'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Colbert::EmailUsers", __("Wrong argument value.") . Dumper $errors);
	}

	my $rowids = ac_ForceArray($values->{rowid});

	$errors = ();

	my $me = $context->GetUser ();

	my $base_url = $config->GetProtocol () . "://" . $config->GetServerName () . $config->GetBaseURI ();

	my $user;
	for (@$rowids) {
		$user = Mioga2::UserList->new ($config, { attributes => { rowid => $_ } });

		my $activation_delay = $config->GetMiogaConf()->GetActivationDelay ();
		my $password_url = Mioga2::Login::GetReinitLink ($context, $user->Get ('rowid'), $activation_delay * 3600 * 24);

		# Build and send mail
		my $subject = __('Your Mioga2 account has been created');
		my $body = __x("You Mioga2 account has been created on {url}.\n\nYour account is not active yet, you must first define a password connecting to the followind address: {password_url}\n\nThe link above will be valid for the next {delay} days. If you don't activate your account within this period, you can request a new activation link at the following address: {request_url}", url => $base_url, password_url => $password_url, delay => $activation_delay, request_url => Mioga2::Login::GetLoginURI ($config, $config->GetBaseURI ()));
		$subject = encode_mimeword($subject, "Q", "UTF-8");
		my $entity = MIME::Entity->build(
			Charset => 'UTF8',
			Type    => "text/plain",
			From    => $me->Get ('firstname')." ".$me->Get ('lastname')." <".$me->Get ('email').">",
			Subject => $subject,
			Data    => $body);
		$user->SendMail ($entity);

	}

	my $data = {};
	$data->{errors} = $errors;

	return ($data);
}	# ----------  end of subroutine EmailUsers  ----------


#===============================================================================

=head2 GetGroups

Get existing groups as XML or JSON.

=head3 Incoming Arguments

=over

One of the following arguments can be passed to alter the "selected" attribute value (see note below):

=over

=over

=item B<user_id>

=item B<team_id>

=item B<application_id>

=back

=back

=back

=head3 Generated XML

=over

	<GetGroups>
		<group rowid="..." selected="...">
			<ident>...</ident>
			<description>...</description>
			<animator rowid="...">...<animator>
			<lang>...</lang>
			<theme>...</theme>
			<public_part>...</public_part>
			<autonomous>...</autonomous>
			<disk_space>...</disk_space>
			<history>...</history>
			<status>...</status>
		</group>
		...
	</GetGroups>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

B<Note:> "selected" attribute is set if one of the following arguments is passed:

=over

=over

=item B<user_id>: groups the user is member of will have their "selected" attribute set to 1

=item B<team_id>: groups the team is member of will have their "selected" attribute set to 1

=item B<application_id>: groups that can use the application will have their "selected" attribute set to 1

=back

=back

=back

=cut

#===============================================================================
sub GetGroups {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetGroups] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['user_id', 'team_id', 'application_id', 'full_list'], 'allow_empty' ],
			[ ['ident', 'animator'], 'allow_empty', 'stripwxps' ],
			[ ['match'], 'allow_empty', [ 'match', "^\(begins|contains|ends\)\$" ] ],
			[ ['mode'], 'allow_empty', [ 'match', "^count\$" ] ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		$data->{group} = [ ];

		my $groups;

		my $revert = 0;
		if(st_ArgExists($context, 'user_id')) {
			my $user = Mioga2::UserList->new ($context->GetConfig (), { attributes => { rowid => $values->{user_id} } });
			$groups = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { users => $user }, short_list => 1 });
			$revert = 1;
		}
		elsif(st_ArgExists($context, 'team_id')) {
			my $team = Mioga2::TeamList->new ($context->GetConfig (), { attributes => { rowid => $values->{team_id} } });
			$groups = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { teams => $team }, short_list => 1 });
			$revert = 1;
		}
		elsif(st_ArgExists($context, 'application_id')) {
			# TODO (SNI - 29/07/2010 09:34): 
			# my $app = Mioga2::ApplicationList->new ($context->GetConfig (), { attributes => { rowid => $values->{application_id} } });
			# $groups = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { applications => $app }, short_list => 1 });
			# $revert = 1;
		}
		else {
			my %attrs;
			for my $attr (qw/ident animator/) {
				$attrs{$attr} = $values->{$attr} if (st_ArgExists ($context, $attr) && $values->{$attr} ne '');
			}
			$groups = Mioga2::GroupList->new ($context->GetConfig (), { attributes => \%attrs, match => $values->{match}, short_list => 1 });
		}

		# Load list according to input arguments
		$groups->Load ();
		for (@{$groups->{groups}}) {
			$_->{selected}->{'$a'} = '1' if (st_ArgExists ($context, 'full_list')); # Only set "selected" attribute if full list is requested
			push @{$data->{group}}, $_;
		}

		# Revert list if required
		if ($revert && st_ArgExists ($context, 'full_list')) {
			$groups->Revert ();
			$groups->Load ();
			for (@{$groups->{groups}}) {
				push @{$data->{group}}, $_;
			}
		}

		# If called with mode='count', just return group count
		if ($values->{mode}) {
			$data = { count => scalar (@{$data->{group}}) };
		}
	}
	else {
		@{$data->{errors}} = map { {$_->[0] => $_->[1]->[0]} } @$errors;
	}

	print STDERR "[Mioga2::Colbert::GetGroups] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetGroups  ----------


#===============================================================================

=head2 GetGroupDetails

Get a group's details as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: the group to get details.

=back

=back

=head3 Generated XML

=over

	<GetGroupDetails>
		<group rowid="...">
			<ident>...</ident>
			<description>...</description>
			<animator rowid="...">...<animator>
			<lang>...</lang>
			<theme>...</theme>
			<public_part>...</public_part>
			<autonomous>...</autonomous>
			<disk_space>...</disk_space>
			<history>...</history>
			<status>...</status>
		</group>
		<users>
			<user rowid="..." is_member="..." is_animator="...">
				<firstname>...</firstname>
				<lastname>...</lastname>
				<email>...</email>
				<type>...</type>
				<status>...</status>
				<last_conn>...</last_conn>
			<user>
			...
		</users>
		<teams>
			<team rowid="..." is_member="...">
				<ident>...</ident>
			<team>
			...
		</teams>
		<applications>
			<application rowid="..." is_active="...">
				<ident>...</ident>
				<description>...</description>
			</application>
		</applications>
	</GetGroupDetails>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetGroupDetails {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetGroupDetails] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ],
			[ ['full_list'], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		# Get group details
		my $group = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { rowid => $values->{rowid} } });
		$group->Load ();
		$data->{group} = $group->{groups}->[0];

		# Get group users
		my $users = $group->GetInvitedUserList (short_list => 1);
		$users->Load ();
		for (@{$users->{users}}) {
			$_->{selected}->{'$a'} = 1 if (st_ArgExists ($context, 'full_list'));
			push (@{$data->{users}->{user}}, $_);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$users->Revert ();
			$users->Load ();
			for (@{$users->{users}}) {
				push (@{$data->{users}->{user}}, $_);
			}
		}

		# Get group teams
		my $teams = $group->GetInvitedTeamList (short_list => 1);
		$teams->Load ();
		for (@{$teams->{teams}}) {
			$_->{selected}->{'$a'} = 1 if (st_ArgExists ($context, 'full_list'));
			push (@{$data->{teams}->{team}}, $_);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$teams->Revert ();
			$teams->Load ();
			for (@{$teams->{teams}}) {
				push (@{$data->{teams}->{team}}, $_);
			}
		}

		# Get group applications
		my $applications = $group->GetApplicationList ();
		$applications->Load ();
		for my $app (@{$applications->{applications}}) {
			my $ident = $app->{ident};
			$app->{selected}->{'$a'} = 1 if (st_ArgExists ($context, 'full_list'));
			$app->{protected} = 1 if (grep (/^$ident$/, $group->GetProtectedApplications ()));
			push (@{$data->{applications}->{application}}, $app);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$applications->Revert ();
			$applications->Load ();
			for my $app (@{$applications->{applications}}) {
				push (@{$data->{applications}->{application}}, $app);
			}
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::GetGroupDetails] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetGroupDetails  ----------


#===============================================================================

=head2 SetGroup

Create or update a group.

=head3 Incoming Arguments

=over

=over

=item B<rowid> (I<optional>): The rowid of the group to update. At creation time, this argument doesn't exist.

=item B<ident>: The group's ident.

=item B<description> (I<optional>): The group's description.

=item B<theme> (I<optional>): The group's theme.

=item B<anim_id>: The group's animator rowid.

=item B<public_part> (I<optional>): The group's public area status.

=item B<autonomous>: The group's autonomous status.

=item B<history> (I<optional>): The group's file history status.

=item B<status>: The group's status.

=item B<skeleton>: The group's skeleton file name.

=item B<users> (I<optional>): An array of user idents member of the group.

=item B<teams> (I<optional>): An array of team idents member of the group.

=item B<applications> (I<optional>): An array of application idents the group can use.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<SetGroup>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</SetGroup>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub SetGroup {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SetGroup] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ [ 'rowid' ], 'allow_empty' ],
			[ [ 'anim_id' ], 'disallow_empty', 'want_int' ],
			[ [ 'description' ], 'allow_empty' ],
			[ [ 'lang' ], 'disallow_empty', 'stripxws' ],
			[ [ 'theme' ], 'allow_empty', 'stripxws' ],
			[ [ 'users', 'teams', 'applications' ], 'allow_empty' ],
			[ [ 'public_part', 'history' ], 'allow_empty', [ 'match', "\(0|1\)" ] ],
			[ [ 'default_app' ], 'disallow_empty' ],
		];

	if (st_ArgExists ($context, 'rowid')) {
		push (@$params, [ [ 'ident' ], 'disallow_empty', [ 'valid_ident', $context->GetConfig () ], 'stripxws', ['max' => 128] ]);
		push (@$params, [ [ 'skeleton' ], 'allow_empty', 'filename' ]);
	}
	else {
		push (@$params, [ [ 'ident' ], 'disallow_empty', [ 'not_used_group_ident', $context->GetConfig () ], [ 'valid_ident', $context->GetConfig () ], ['max' => 128], 'stripxws' ]);
		push (@$params, [ [ 'skeleton' ], 'disallow_empty', 'filename' ]);
	}

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Check user can use the skeleton
	try {
		if ($values->{skeleton} && !$self->CheckSkeletonAccess ($context, 'group', $values->{skeleton})) {
			push (@$errors, ['skeleton', ['forbidden']]);
		}
	}
	otherwise {
		push (@$errors, ['skeleton', ['parse_error']]);
	};

	my $data = {
		success => 1
	};

	if (!@$errors) {
		my $group;
		my @users = @{ac_ForceArray (delete ($values->{users}))};
		my @teams = @{ac_ForceArray (delete ($values->{teams}))};
		my @applications = @{ac_ForceArray (delete ($values->{applications}))};
		if (st_ArgExists ($context, 'rowid')) {
			$self->{db}->BeginTransaction ();
			try {
				#-------------------------------------------------------------------------------
				# Update existing group
				#-------------------------------------------------------------------------------
				# Load group from rowid
				$group = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { rowid => $values->{rowid} } });

				# Update group
				for my $key (keys (%$values)) {
					next if (grep (/^$key$/, qw/users teams applications/));
					$group->Set ($key, $values->{$key});
				}
				$group->Store ();
				$self->{db}->EndTransaction ();
			}
			otherwise {
				my $err = shift;
				print STDERR "[Mioga2::Colbert::SetGroup] Update failed\n";
				print STDERR $err->stringify ($context);
				$self->{db}->RollbackTransaction ();
				$data->{success} = 0;
			};
		}
		else {
			try {
				#-------------------------------------------------------------------------------
				# Create new group
				#-------------------------------------------------------------------------------
				# Prefix skeleton path to the file name
				if (st_ArgExists ($context, 'skeleton')) {
					$values->{skeleton} = $context->GetConfig ()->GetSkeletonDir () . "/$values->{lang}/group/$values->{skeleton}";
				}

				# Actually create group
				$group = Mioga2::GroupList->new ($context->GetConfig (), { attributes => $values });
				$group->Store ();
			}
			catch Mioga2::Exception::Quota with {
				push (@$errors, [ 'global', [ 'over_quota' ] ]);
			}
			otherwise {
				my $err = shift;
				print STDERR '[Mioga2::Colbert::SetGroup] Group creation failed, destroying.';
				print STDERR $err->stringify ($context);
				my $journal = $group->GetJournal ();
				$journal->SetMetadata ({ stage => 'rollback' });
				$group->Delete ();
				$data->{success} = 0;
			};
		}

		if (!@$errors && $data->{success}) {
			#-------------------------------------------------------------------------------
			# Invite users into group
			#-------------------------------------------------------------------------------
			my %users;
			for (@users) {
				$users{$_} = '';
			}
			$group->InviteUsers (%users);

			#-------------------------------------------------------------------------------
			# Invite teams into group
			#-------------------------------------------------------------------------------
			my %teams;
			for (@teams) {
				$teams{$_} = '';
			}
			$group->InviteTeams (%teams);

			#-------------------------------------------------------------------------------
			# Enable applications for group
			#-------------------------------------------------------------------------------
			# Ensure protected applications are not getting disabled
			for my $app ($group->GetProtectedApplications ()) {
				if (!grep (/^$app$/, @applications)) {
					if ($app eq $group->Get ('default_app')) {
						push (@$errors, [ 'applications', [ 'cannot_disable_default_app' ] ]);
					}
					else {
						push (@$errors, [ 'applications', [ 'cannot_disable_' . $app ] ]);
					}
				}
			}
			if (!@$errors) {
				# Applications idents can't be passed as-is to Mioga2::GroupList.
				# The array must contain hashes with an 'ident' key and eventually other keys for application configuration (Workspace for example)
				my @apps = ();
				for (@applications) {
					push (@apps, { ident => $_ });
				}
				$group->EnableApplications (@apps);
			}

			#-------------------------------------------------------------------------------
			# Return rowid of group
			#-------------------------------------------------------------------------------
			$data->{rowid} = $group->Get ('rowid');
		}

		$data->{journal} = $group->GetJournal ()->ToArray ();
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::SetGroup] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetGroup  ----------


#===============================================================================

=head2 DeleteGroup

Delete a group.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: The rowid of the group to delete.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<DeleteGroup>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</DeleteGroup>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub DeleteGroup {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::DeleteGroup] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $group = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { rowid => \@{ac_ForceArray ($values->{rowid})} } });
		my ($locked, $reason) = $group->IsLocked ();
		if ($locked) {
			push (@{$data->{errors}}, { 'rowid' => __('This group is locked and can not be deleted. Reason: ') . __($reason) })
		}
		else {
			$group->Delete ();
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::DeleteGroup] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DeleteGroup  ----------


#===============================================================================

=head2 GetTeams

Get existing teams as XML or JSON.

=head3 Incoming Arguments

=over

One of the following arguments can be passed to alter the "selected" attribute value (see note below):

=over

=over

=item B<user_id>

=item B<group_id>

=back

=back

=back

=head3 Generated XML

=over

	<GetTeams>
		<team rowid="..." selected="...">
			<ident>...</ident>
		</team>
		...
	</GetTeams>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

B<Note:> "selected" attribute is set if one of the following arguments is passed:

=over

=over

=item B<user_id>: teams the user is member of will have their "selected" attribute set to 1

=item B<group_id>: teams that are member of the group will have their "selected" attribute set to 1

=back

=back

=back

=cut

#===============================================================================
sub GetTeams {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetTeams] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['user_id', 'group_id', 'full_list'], 'allow_empty' ],
			[ ['ident'], 'allow_empty', 'stripwxps' ],
			[ ['match'], 'allow_empty', [ 'match', "^\(begins|contains|ends\)\$" ] ],
			[ ['mode'], 'allow_empty', [ 'match', "^count\$" ] ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		$data->{team} = [ ];

		my $teams;

		my $revert = 0;
		if(st_ArgExists($context, 'user_id')) {
			my $user = Mioga2::UserList->new ($context->GetConfig (), { attributes => { rowid => $values->{user_id} } });
			$teams = Mioga2::TeamList->new ($context->GetConfig (), { attributes => { users => $user }, short_list => 1 });
			$revert = 1;
		}
		elsif(st_ArgExists($context, 'group_id')) {
			my $group = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { rowid => $values->{group_id} } });
			$teams = Mioga2::TeamList->new ($context->GetConfig (), { attributes => { groups => $group }, short_list => 1 });
			$revert = 1;
		}
		else {
			my %attrs;
			for my $attr (qw/ident/) {
				$attrs{$attr} = $values->{$attr} if (st_ArgExists ($context, $attr) && $values->{$attr} ne '');
			}
			$teams = Mioga2::TeamList->new ($context->GetConfig (), { attributes => \%attrs, match => $values->{match}, short_list => 1 });
		}

		# Load list according to input arguments
		$teams->Load ();
		for (@{$teams->{teams}}) {
			$_->{selected}->{'$a'} = '1' if (st_ArgExists ($context, 'full_list')); # Only set "selected" attribute if full list is requested
			push @{$data->{team}}, $_;
		}

		# Revert list if required
		if ($revert && st_ArgExists ($context, 'full_list')) {
			$teams->Revert ();
			$teams->Load ();
			for (@{$teams->{teams}}) {
				push @{$data->{team}}, $_;
			}
		}

		# If called with mode='count', just return team count
		if ($values->{mode}) {
			$data = { count => scalar (@{$data->{team}}) };
		}
	}
	else {
		@{$data->{errors}} = map { {$_->[0] => $_->[1]->[0]} } @$errors;
	}

	print STDERR "[Mioga2::Colbert::GetTeams] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetTeams  ----------


#===============================================================================

=head2 GetTeamDetails

Get a team's details as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: the team to get details.

=back

=back

=head3 Generated XML

=over

	<GetTeamDetails>
		<team rowid="...">
			<ident>...</ident>
		</team>
		<users>
			<user rowid="..." is_member="...">
				<firstname>...</firstname>
				<lastname>...</lastname>
				<email>...</email>
				<type>...</type>
				<status>...</status>
				<last_conn>...</last_conn>
			<user>
			...
		</users>
		<groups>
			<group rowid="..." is_member="...">
				<ident>...</ident>
				<description>...</description>
			<group>
			...
		</groups>
	</GetTeamDetails>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetTeamDetails {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetTeamDetails] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ],
			[ ['full_list'], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		# Get team details
		my $team = Mioga2::TeamList->new ($context->GetConfig (), { attributes => { rowid => $values->{rowid} } });
		$team->Load ();
		$data->{team} = $team->{teams}->[0];

		# Get team users
		my $users = $team->GetInvitedUserList (short_list => 1);
		$users->Load ();
		for (@{$users->{users}}) {
			$_->{selected}->{'$a'} = 1 if (st_ArgExists ($context, 'full_list'));
			push (@{$data->{users}->{user}}, $_);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$users->Revert ();
			$users->Load ();
			for (@{$users->{users}}) {
				push (@{$data->{users}->{user}}, $_);
			}
		}

		# Get team groups
		my $groups = $team->GetInvitedGroupList (short_list => 1);
		$groups->Load ();
		for (@{$groups->{groups}}) {
			$_->{selected}->{'$a'} = 1 if (st_ArgExists ($context, 'full_list'));
			push (@{$data->{groups}->{group}}, $_);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$groups->Revert ();
			$groups->Load ();
			for (@{$groups->{groups}}) {
				push (@{$data->{groups}->{group}}, $_);
			}
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::GetTeamDetails] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetTeamDetails  ----------


#===============================================================================

=head2 SetTeam

Create or update a team.

=head3 Incoming Arguments

=over

=over

=item B<rowid> (I<optional>): The rowid of the team to update. At creation time, this argument doesn't exist.

=item B<ident>: The team's ident.

=item B<description> (I<optional>): The team's description.

=item B<users>: An array of user-rowids member of the team.

=item B<groups>: An array of group-rowids the team is member of.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<SetTeam>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</SetTeam>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub SetTeam {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SetTeam] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ [ 'rowid' ], 'allow_empty' ],
			[ [ 'description', 'users', 'groups' ], 'allow_empty' ],
		];

	if (st_ArgExists ($context, 'rowid')) {
		push (@$params, [ [ 'ident' ], 'disallow_empty', 'stripxws', ['max' => 128] ]);
	}
	else {
		push (@$params, [ [ 'ident' ], 'disallow_empty', [ 'not_used_group_ident', $context->GetConfig () ], [ 'valid_ident', $context->GetConfig () ], ['max' => 128], 'stripxws' ]);
	}

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = {
		success => 1
	};

	if (!@$errors) {
		my $team;
		my @users = @{ac_ForceArray (delete ($values->{users}))};
		my @groups = @{ac_ForceArray (delete ($values->{groups}))};
		if (st_ArgExists ($context, 'rowid')) {
			$self->{db}->BeginTransaction ();
			try {
				#-------------------------------------------------------------------------------
				# Update existing team
				#-------------------------------------------------------------------------------
				# Load team from rowid
				$team = Mioga2::TeamList->new ($context->GetConfig (), { attributes => { rowid => $values->{rowid} } });

				# Update team
				for my $key (keys (%$values)) {
					next if (grep (/^$key$/, qw/users groups/));
					$team->Set ($key, $values->{$key});
				}
				$team->Store ();
				$self->{db}->EndTransaction ();
			}
			otherwise {
				my $err = shift;
				print STDERR "[Mioga2::Colbert::SetGroup] Group update failed.\n";
				print STDERR $err->stringify ($context);
				$self->{db}->RollbackTransaction ();
				$data->{success} = 0;
			};
		}
		else {
			try {
				#-------------------------------------------------------------------------------
				# Create new team
				#-------------------------------------------------------------------------------
				$team = Mioga2::TeamList->new ($context->GetConfig (), { attributes => $values });
				$team->Store ();
			}
			otherwise {
				my $err = shift;
				print STDERR "[Mioga2::Colbert::SetTeam] Team creation failed.\n";
				print STDERR $err->stringify ($context);
				my $journal = $team->GetJournal ();
				$journal->SetMetadata ({ stage => 'rollback' });
				$team->Delete ();
				$data->{success} = 0;
			};
		}

		if (!@$errors && $data->{success}) {
			$team->InviteUsers (@users);

			# Invite team into groups
			my %groups;
			for (@groups) {
				$groups{$_} = '';
			}
			$team->JoinGroups (%groups);

			# Return rowid of team
			$data->{rowid} = $team->Get ('rowid');
		}

		$data->{journal} = $team->GetJournal ()->ToArray ();
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::SetTeam] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetTeam  ----------


#===============================================================================

=head2 DeleteTeam

Delete a team.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: The rowid of the team to delete.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<DeleteTeam>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</DeleteTeam>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub DeleteTeam {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::DeleteTeam] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $team = Mioga2::TeamList->new ($context->GetConfig (), { attributes => { rowid => $values->{rowid} } });
		$team->Delete ();
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::DeleteTeam] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DeleteTeam  ----------


#===============================================================================

=head2 ImportUserTeams

Import a user to teams affectation CSV file

=head3 Incoming Arguments

=over

=over

=item B<file>: The uploaded file name. This argument will not be processed but the uploaded file content will.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<ImportUsers>
		<errors>
			<error line="...">...</error>
			...
		</errors>
	</ImportUsers>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub ImportUserTeams {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::ImportUserTeams] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['file'], 'disallow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $upload_data = st_CheckUTF8($context->GetUploadData ());

	my $data = { ImportUserTeams => $context->GetContext () };

	if (!@$errors) {
		# Convert CSV to array
		my $converter = Mioga2::CSV->new (
			columns => [ qw/email team/ ],
			separator => ',',
			quote => '"'
		);
		my $content = $converter->Convert ($upload_data);

		my $line = 1;
		for my $csvline (@$content) {
			my $user = Mioga2::UserList->new ($context->GetConfig (), { attributes => { email => $csvline->{email} } });

			# Ensure user exists
			if (!$user->Count ()) {
				push (@{$data->{ImportUserTeams}->{Import}}, { line => $line, error => __('No such user')});
				next;
			}

			# Ensure team exists
			my $team = Mioga2::TeamList->new ($context->GetConfig (), { attributes => { ident => $csvline->{team} } });
			if (!$team->Count ()) {
				push (@{$data->{ImportUserTeams}->{Import}}, { line => $line, error => __('No such team')});
				next;
			}

			my @teams = map { $_->{ident} } $user->GetInvitedTeamList ()->GetTeams ();
			push (@teams, $csvline->{team});
			$user->JoinTeams (@teams);
		}
		continue {
			$line++;
		}
	}

	for (@$errors) {
		push (@{$data->{ImportUserTeams}->{errors}}, { $_->[0], [ $_->[1]->[0] ] });
	}

	ac_I18NizeErrors($errors);

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "[Mioga2::Colbert::ImportUserTeams] xml: $xml \n" if ($debug);

	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine ImportUserTeams  ----------


#===============================================================================

=head2 ImportTeamGroups

Import a team to group affectation CSV file

=head3 Incoming Arguments

=over

=over

=item B<file>: The uploaded file name. This argument will not be processed but the uploaded file content will.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<ImportUsers>
		<errors>
			<error line="...">...</error>
			...
		</errors>
	</ImportUsers>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub ImportTeamGroups {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::ImportTeamGroups] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['file'], 'disallow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $upload_data = st_CheckUTF8($context->GetUploadData ());

	my $data = { ImportTeamGroups => $context->GetContext () };

	if (!@$errors) {
		# Convert CSV to array
		my $converter = Mioga2::CSV->new (
			columns => [ qw/team group profile/ ],
			separator => ',',
			quote => '"'
		);
		my $content = $converter->Convert ($upload_data);

		my $line = 1;
		for my $csvline (@$content) {
			my $team = Mioga2::TeamList->new ($context->GetConfig (), { attributes => { ident => $csvline->{team} } });

			# Ensure team exists
			if (!$team->Count ()) {
				push (@{$data->{ImportTeamGroups}->{Import}}, { line => $line, error => __('No such team')});
				next;
			}

			# Ensure group exists
			my $group = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { ident => $csvline->{group} } });
			if (!$group->Count ()) {
				push (@{$data->{ImportTeamGroups}->{Import}}, { line => $line, error => __('No such group')});
				next;
			}

			my %groups = map { $_->{ident} => $_->{profile} } $team->GetInvitedGroupList ()->GetGroups ();
			$groups{$csvline->{group}} = $csvline->{profile};
			$team->JoinGroups (%groups);
		}
		continue {
			$line++;
		}
	}

	for (@$errors) {
		push (@{$data->{ImportTeamGroups}->{errors}}, { $_->[0], [ $_->[1]->[0] ] });
	}

	ac_I18NizeErrors($errors);

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "[Mioga2::Colbert::ImportTeamGroups] xml: $xml \n" if ($debug);

	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine ImportTeamGroups  ----------


#===============================================================================

=head2 GetResources

Get existing resources as XML or JSON.

=head3 Incoming Arguments

=over

One of the following arguments can be passed to alter the "selected" attribute value (see note below):

=over

=over

=item B<anim_id>

=item B<group_id>

=back

=back

=back

=head3 Generated XML

=over

	<GetResources>
		<resource rowid="..." selected="...">
			<ident>...</ident>
		</resource>
		...
	</GetResources>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

B<Note:> "selected" attribute is set if one of the following arguments is passed:

=over

=over

=item B<anim_id>: resources the user is animator of will have their "selected" attribute set to 1

=item B<group_id>: resources that are member of the group will have their "selected" attribute set to 1

=back

=back

=back

=cut

#===============================================================================
sub GetResources {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetResources] Entering. Args: " . Dumper $context->{args} if ($debug);

	my $data = { };
	$data->{resource} = [ ];

	# my $resources = new Mioga2::Old::ResourceList ($context->GetConfig (), all => 1);
	my $resources = Mioga2::ResourceList->new ($context->GetConfig ());
	$resources->Load ();
	
	# Translate status
	for (@{$resources->{resources}}) {
		$_->{status} = __($_->{status});
	}

	$data->{resource} = $resources->{resources};

	print STDERR "[Mioga2::Colbert::GetResources] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetResources  ----------


#===============================================================================

=head2 GetResourceDetails

Get a resource's details as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: the resource to get details.

=back

=back

=head3 Generated XML

=over

	<GetResourceDetails>
		<resource rowid="...">
			<ident>...</ident>
			<animator rowid="...">...</animator>
			<description>...</description>
			<location>...</location>
			<description>...</description>
		</resource>
	</GetResourceDetails>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetResourceDetails {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetResourceDetails] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		# Get resource details
		my $resource = Mioga2::ResourceList->new ($context->GetConfig (), { attributes => { rowid => $values->{rowid} } });
		$resource->Load ();
		$data->{resource} = $resource->{resources}->[0];
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::GetResourceDetails] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetResourceDetails  ----------


#===============================================================================

=head2 SetResource

Create or update a resource.

=head3 Incoming Arguments

=over

=over

=item B<rowid> (I<optional>): The rowid of the resource to update. At creation time, this argument doesn't exist.

=item B<ident>: The resource's ident.

=item B<animator>: The rowid of the resource animator.

=item B<location> (I<optional>): The resource's location.

=item B<description> (I<optional>): The resource's description.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<SetResource>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</SetResource>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub SetResource {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SetResource] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ [ 'rowid' ], 'allow_empty' ],
			[ [ 'ident' ], 'disallow_empty', 'stripxws' ],
			[ [ 'location', 'description' ], 'allow_empty', 'stripxws' ],
			[ [ 'anim_id' ], 'disallow_empty', [ 'mioga_user_id', $context->GetConfig () ] ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = {
		success => 1
	};

	if (!@$errors) {
		my $resource;
		if (st_ArgExists ($context, 'rowid')) {
			$self->{db}->BeginTransaction ();
			try {
				# Update resource
				# Load resource from rowid
				$resource = Mioga2::ResourceList->new ($context->GetConfig (), { attributes => { rowid => $values->{rowid} } });

				# Update resource
				for my $key (keys (%$values)) {
					$resource->Set ($key, $values->{$key});
				}
				$resource->Store ();
				$self->{db}->EndTransaction ();
			}
			otherwise {
				my $err = shift;
				print STDERR "[Mioga2::Colbert::SetResource] Resource update failed.\n";
				print STDERR $err->stringify ($context);
				$self->{db}->RollbackTransaction ();
				$data->{success} = 0;
			};
		}
		else {
			try {
				# Create resource
				$values->{creator_id} = $context->GetUser ()->Get ('rowid');
				$resource = Mioga2::ResourceList->new ($context->GetConfig (), { attributes => $values });
				$resource->Store ();
			}
			otherwise {
				my $err = shift;
				print STDERR "[Mioga2::Colbert::SetResource] Resource creation failed.\n";
				print STDERR $err->stringify ($context);
				my $journal = $resource->GetJournal ();
				$journal->SetMetadata ({ stage => 'rollback' });
				$resource->Delete ();
				$data->{success} = 0;
			};
		}

		$data->{journal} = $resource->GetJournal ()->ToArray ();
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::SetResource] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetResource  ----------


#===============================================================================

=head2 ChangeResourceStatus

Change existing resource status.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: The resource rowid.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<SetExternalMioga>
		<error>...</error>
		...
	</SetExternalMioga>

=back

=cut

#===============================================================================
sub ChangeResourceStatus {
	my ($self, $context) = @_;

	print STDERR "[Mioga2::Colbert::ChangeResourceStatus] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		for (@{ac_ForceArray ($values->{rowid})}) {
			my $resource = Mioga2::ResourceList->new ($context->GetConfig (), { attributes => { rowid => $_ } });

			if($resource->Get ('status') eq 'active') {
				$resource->Set ('status', 'disabled');
			}
			else {
				$resource->Set ('status', 'active');
			}

			$resource->Store ();
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::ChangeResourceStatus] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine ChangeResourceStatus  ----------


#===============================================================================

=head2 DeleteResource

Delete a resource.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: The rowid of the resource to delete.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<DeleteResource>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</DeleteResource>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub DeleteResource {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::DeleteResource] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $resources = Mioga2::ResourceList->new ($context->GetConfig (), { attributes => { rowid => ac_ForceArray ($values->{rowid}) } });
		$resources->Delete ();
	}

	print STDERR "[Mioga2::Colbert::DeleteResource] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DeleteResource  ----------


#===============================================================================

=head2 GetApplications

Get existing applications as XML or JSON.

=head3 Incoming Arguments

=over

One of the following arguments can be passed to alter the "selected" attribute value (see note below):

=over

=over

=item B<user_id>

=item B<group_id>

=back

=back

=back

=head3 Generated XML

=over

	<GetApplications>
		<application rowid="..." selected="...">
			<ident>...</ident>
			<description>...</description>
			<can_be_public>...</can_be_public>
			<is_user>...</is_user>
			<is_group>...</is_group>
			<is_resource>...</is_resource>
			<all_groups>...</all_groups>
			<all_users>...</all_users>
		</application>
		...
	</GetApplications>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

B<Note:> "selected" attribute is set if one of the following arguments is passed:

=over

=over

=item B<user_id>: applications the user can use have their "selected" attribute set to 1

=item B<group_id>: applications the group can use will have their "selected" attribute set to 1

=back

=back

=back

=cut

#===============================================================================
sub GetApplications {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetApplications] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['user_id', 'group_id', 'full_list'], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };
	$data->{application} = [ ];

	my $apps;

	my $revert = 0;
	if(st_ArgExists($context, 'user_id')) {
		my $user = Mioga2::UserList->new ($context->GetConfig (), { attributes => { rowid => $values->{user_id} } });
		$apps = Mioga2::ApplicationList->new ($context->GetConfig (), { attributes => { users => $user } });
		$revert = 1;
	}
	elsif(st_ArgExists($context, 'group_id')) {
		my $group = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { rowid => $values->{group_id} } });
		$apps = Mioga2::ApplicationList->new ($context->GetConfig (), { attributes => { groups => $group } });
		$revert = 1;
	}
	else {
		$apps = Mioga2::ApplicationList->new ($context->GetConfig ());
	}

	# Load list according to input arguments
	$apps->Load ();
	for (@{$apps->{applications}}) {
		$_->{selected}->{'$a'} = '1' if (st_ArgExists ($context, 'full_list')); # Only set "selected" attribute if full list is requested
		$_->{nb_users} = __('All') if ($_->{all_users});
		$_->{nb_groups} = __('All') if ($_->{all_groups});
		push @{$data->{application}}, $_;
	}

	# Revert list if required
	if ($revert && st_ArgExists ($context, 'full_list')) {
		$apps->Revert ();
		$apps->Load ();
		for (@{$apps->{applications}}) {
			push @{$data->{application}}, $_;
		}
	}

	print STDERR "[Mioga2::Colbert::GetApplications] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetApplications  ----------


#===============================================================================

=head2 GetApplicationDetails

Get an application's details as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: the application to get details.

=back

=back

=head3 Generated XML

=over

	<GetApplicationDetails>
		<application rowid="...">
			<ident>...</ident>
			<description>...</description>
			<can_be_public>...</can_be_public>
			<is_user>...</is_user>
			<is_group>...</is_group>
			<is_resource>...</is_resource>
			<all_groups>...</all_groups>
			<all_users>...</all_users>
		</application>
		<users>
			<user rowid="..." is_active="...">
				<firstname>...</firstname>
				<lastname>...</lastname>
				<email>...</email>
				<type>...</type>
				<status>...</status>
				<last_conn>...</last_conn>
			<user>
			...
		</users>
		<groups>
			<group rowid="..." is_active="...">
				<ident>...</ident>
				<description>...</description>
			<group>
			...
		</groups>
	</GetApplicationDetails>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetApplicationDetails {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetApplicationDetails] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ],
			[ ['full_list'], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		# Get application details
		my $app = Mioga2::ApplicationList->new ($context->GetConfig (), { attributes => { rowid => $values->{rowid} } });
		$app->Load ();
		$data->{application} = $app->{applications}->[0];
		my $app_ident = $app->Get ('ident');

		# Get group that can use applications
		if ($app->Get ('is_group')) {
			my $groups = $app->GetGroupList ();
			$groups->Load ();
			my $protected = (grep (/^$app_ident$/, @Mioga2::GroupList::ProtectedApplications)) ? 1 : 0;
			for my $group (@{$groups->{groups}}) {
				$protected = 1 if ($app_ident eq $group->{default_app});
				$group->{selected}->{'$a'} = 1 if (st_ArgExists ($context, 'full_list'));
				$group->{protected} = $protected;
				push (@{$data->{groups}->{group}}, $group);
			}

			# Revert list if needed
			if (st_ArgExists ($context, 'full_list')) {
				$groups->Revert ();
				$groups->Load ();
				for my $group (@{$groups->{groups}}) {
					push (@{$data->{groups}->{group}}, $group);
				}
			}
		}

		# Get user that can use applications
		if ($app->Get ('is_user')) {
			my $users = $app->GetUserList ();
			$users->Load ();
			my $protected = (grep (/^$app_ident$/, @Mioga2::UserList::ProtectedApplications)) ? 1 : 0;
			for my $user (@{$users->{users}}) {
				$user->{selected}->{'$a'} = 1 if (st_ArgExists ($context, 'full_list'));
				$user->{protected} = $protected;
				push (@{$data->{users}->{user}}, $user);
			}

			# Revert list if needed
			if (st_ArgExists ($context, 'full_list')) {
				$users->Revert ();
				$users->Load ();
				for my $user (@{$users->{users}}) {
					push (@{$data->{users}->{user}}, $user);
				}
			}
		}
	}

	print STDERR "[Mioga2::Colbert::GetApplicationDetails] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetApplicationDetails  ----------


#===============================================================================

=head2 SetApplication

Update a application.

=head3 Incoming Arguments

=over

=over

=item B<rowid> (I<optional>): The rowid of the application to update.

=item B<users>: An array of user-rowids that can use the application.

=item B<all_users>: A flag indicating the application can be used by all users.

=item B<groups>: An array of group-rowids that can use the application.

=item B<all_groups>: A flag indicating the application can be used by all groups.

=back

B<Note>: The arguments 'users' and 'all_users' as well as 'groups' and 'all_groups' are mutually exclusive and will raise an error if passed together.

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<SetApplication>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</SetApplication>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub SetApplication {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SetApplication] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ [ 'rowid' ], 'allow_empty' ],
			[ [ 'groups', 'users' ], 'allow_empty' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		# Initialize user list
		my $users = Mioga2::UserList->new ($context->GetConfig (), { attributes => { ident => \@{ac_ForceArray ($values->{users})} } });

		# Initialize group list
		my $groups = Mioga2::GroupList->new ($context->GetConfig (), { attributes => { ident => \@{ac_ForceArray ($values->{groups})} } });

		# Set users and groups for applications
		my $applications = Mioga2::ApplicationList->new ($context->GetConfig (), { attributes => { rowid => @{ac_ForceArray ($values->{rowid})} } });
		$applications->SetUsers ($users);
		$applications->SetGroups ($groups);
	}

	print STDERR "[Mioga2::Colbert::SetApplication] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetApplication  ----------


#===============================================================================

=head2 GetExternalMiogas

Get external Mioga connections list as XML or JSON.

=head3 Incoming Arguments

=over

None.

=back

=head3 Generated XML

=over

	<GetExternalMiogas>
		<externalmioga rowid="...">
			<server>...</server>
			<status>...</status>
		</externalmioga>
		...
	</GetExternalMiogas>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetExternalMiogas {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetExternalMiogas] Entering. Args: " . Dumper $context->{args} if ($debug);

	my $data = { };

	my $em = new Mioga2::ExternalMioga ($context->GetConfig ());
	my $ext_mioga = $em->GetList ();

	for (@$ext_mioga) {
		$_->{status} = __($_->{status});
	}

	$data->{external_mioga} = \@$ext_mioga;

	print STDERR "[Mioga2::Colbert::GetExternalMiogas] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetExternalMiogas  ----------


#===============================================================================

=head2 GetExternalMiogaDetails

Get external Mioga connections list as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: The rowid of the external Mioga.

=back

=back

=head3 Generated XML

=over

	<GetExternalMiogaDetails>
		<externalmioga rowid="...">
			<server>...</server>
			<status>...</status>
		</externalmioga>
		...
	</GetExternalMiogaDetails>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetExternalMiogaDetails {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetExternalMiogaDetails] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $em = new Mioga2::ExternalMioga ($context->GetConfig (), rowid => $values->{rowid});
		my $ext_mioga = $em->{values};

		$ext_mioga->{status} = __($ext_mioga->{status});

		$data->{external_mioga} = $ext_mioga;
	}

	print STDERR "[Mioga2::Colbert::GetExternalMiogaDetails] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetExternalMiogaDetails  ----------


#===============================================================================

=head2 SetExternalMioga

Create an external Mioga connection.

=head3 Incoming Arguments

=over

=over

=item B<public_key>: The uploaded file name. This argument will not be processed but the uploaded file content will at creation time.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<SetExternalMioga>
		<error>...</error>
		<error>...</error>
		...
	</SetExternalMioga>

=back

=cut

#===============================================================================
sub SetExternalMioga {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SetExternalMiogas] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['public_key'], 'disallow_empty' ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $config = $context->GetConfig ();

	my $upload_data = st_CheckUTF8($context->GetUploadData);

	if(! @$errors) {
		my $crypto = new Mioga2::Crypto($config);
		my $keyname;

		try {
			$keyname = $crypto->ImportPublicKey($upload_data);
		}

		catch Mioga2::Exception::Crypto with {
			push @$errors, ["pubkey", ["already_in_database"]];
		}
		
		otherwise {
			my $err = shift;

			push @$errors, ["pubkey", ["bad_key"]];
		};
		

		if(! @$errors) {
			$keyname =~ /\((.*)\)/ ;
			my $server = $1;

			my $datafile = tmpnam();

			open(F_DATA, ">$datafile");
			print F_DATA $upload_data;
			close(F_DATA);

			$values = { server => $server,
						public_key => $datafile,
					  };


			ExtMiogaCreate($config, $values);

			unlink($datafile);
		}
	}

	my $xml = '<SetExternalMioga>';
	$xml .= $context->GetXML ();

	for (@$errors) {
		$xml .= "<error>" .__($_->[1]->[0]) . "</error>";
	}

	$xml .= '</SetExternalMioga>';

	print STDERR "Colbert::SetExternalMioga xml: $xml \n" if ($debug);

	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return $content;
}	# ----------  end of subroutine SetExternalMioga  ----------


#===============================================================================

=head2 ChangeExternalMiogaStatus

Change existing external Mioga connection state.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: The external Mioga rowid.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<SetExternalMioga>
		<error>...</error>
		...
	</SetExternalMioga>

=back

=cut

#===============================================================================
sub ChangeExternalMiogaStatus {
	my ($self, $context) = @_;

	print STDERR "[Mioga2::Colbert::ChangeExternalMiogaStatus] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		for (@{ac_ForceArray ($values->{rowid})}) {
			my $external_mioga = new Mioga2::ExternalMioga($context->GetConfig (), rowid => $_);
			my $status = $external_mioga->GetStatus();

			if($status eq 'active') {
				ExtMiogaDisable($self->{dbh}, $_);
			}
			else {
				ExtMiogaEnable($self->{dbh}, $_);
			}
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::ChangeExternalMiogaStatus] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine ChangeExternalMiogaStatus  ----------


#===============================================================================

=head2 DeleteExternalMioga

Delete an external Mioga connection.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: The rowid of the external Mioga connection to delete.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<DeleteExternalMioga>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</DeleteExternalMioga>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub DeleteExternalMioga {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::DeleteExternalMioga] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $config = $context->GetConfig();
		my $crypto = new Mioga2::Crypto($config);

		for (@{ac_ForceArray ($values->{rowid})}) {
			my $externalmioga = new Mioga2::ExternalMioga($config, rowid => $_);
			my $keyname = $externalmioga->GetServer();

			$crypto->DeletePublicKey($keyname);

			ExtMiogaDelete($self->{dbh}, $_);
		}
	}

	print STDERR "[Mioga2::Colbert::DeleteExternalMioga] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DeleteExternalMioga  ----------


#===============================================================================

=head2 GetPublicKey

Get Mioga instance public key.

=head3 Incoming Arguments

=over

None

=back

=head3 Generated content

=over

The generated content is a PGP public key.

=back

=cut

#===============================================================================
sub GetPublicKey {
	my ($self, $context) = @_;

	my $config = $context->GetConfig ();
	my $crypto = new Mioga2::Crypto ($config);

	my $key = $crypto->GetMyPublicKey ();
	
	my $content = new Mioga2::Content::ASIS ($context, MIME => 'application/pgp-keys', filename => $context->GetConfig ()->GetMiogaIdent () . "_public_key.pgp");
	$content->SetContent ($key);

	return ($content);
}	# ----------  end of subroutine GetPublicKey  ----------


#===============================================================================

=head2 GetUserStatuses

Get user statuses list as XML or JSON.

=head3 Incoming Arguments

=over

None.

=back

=head3 Generated XML

=over

	<GetUserStatuses>
		<status rowid="...">...</status>
		...
	</GetUserStatuses>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetUserStatuses {
	my ($self, $context) = @_;

	my $data = { };

	my $sql = "SELECT * FROM m_user_status;";
	my $statuses = SelectMultiple ($self->{dbh}, $sql);

	for my $status (@$statuses) {
		$status->{label} = __($status->{ident});
	}

	$data->{status} = $statuses;

	print STDERR "[Mioga2::Colbert::GetUserStatuses] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetUserStatuses  ----------


#===============================================================================

=head2 GetUserTypes

Get user types list as XML or JSON.

=head3 Incoming Arguments

=over

None.

=back

=head3 Generated XML

=over

	<GetUserTypes>
		<type rowid="...">...</type>
		...
	</GetUserTypes>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetUserTypes {
	my ($self, $context) = @_;

	my $data = { };

	my $sql = "SELECT * FROM m_group_type WHERE ident LIKE '%user%';";
	my $types = SelectMultiple ($self->{dbh}, $sql);

	for my $type (@$types) {
		$type->{label} = __($type->{ident});
	}

	$data->{type} = $types;

	print STDERR "[Mioga2::Colbert::GetUserTypes] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetUserTypes  ----------


#===============================================================================

=head2 GetLanguages

Get registered languages.

=head3 Incoming Arguments

=over

None.

=back

=head3 Generated XML

=over

	<GetLanguages>
		<language rowid="...">...</language>
		...
	</GetLanguages>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetLanguages {
	my ($self, $context) = @_;

	my $data = { };

	my $lang = SelectMultiple ($context->GetConfig ()->GetDBH (), "SELECT rowid, ident FROM m_lang;");
	for my $l (@$lang) {
		$l->{label} = __($l->{ident});
		$l->{value} = $l->{rowid};
	}
	$data->{language} = $lang;

	print STDERR "[Mioga2::Colbert::GetLanguages] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetLanguages  ----------


#===============================================================================

=head2 GetSkeletons

Get skeletons list as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<type>: Type of skeleton. Value can be 'group', 'user' or 'resource' and is case insensitive.

=back

=back

=head3 Generated XML

=over

	<GetSkeletons>
		<skeleton>
			<name>...</name>
			<file>...</file>
		</skeleton>
		...
	</GetSkeletons>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetSkeletons {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetSkeletons] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['type'], 'disallow_empty', [ 'match', "^\(user|group|resource\)\$" ] ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $lang = $context->GetGroup ()->Get ('lang');
		my $skelpath = $context->GetConfig ()->GetSkeletonDir () . "/$lang/$values->{type}";
		my @skellist = <$skelpath/*>;
		for (@skellist) {
			my ($skelfile) = ($_ =~ m/.*\/([^\/]*)$/);
			next if -d $_;
			
			#-------------------------------------------------------------------------------
			# Read file
			#-------------------------------------------------------------------------------
			my $skeldata = '';
			open(FILE, "< $_") or throw Mioga2::Exception::User ('Mioga2::Colbert::GetSkeletons', "Can't open skeleton file '$_': $!.");
			while (<FILE>) { $skeldata .= $_ };
			close (FILE);

			#-------------------------------------------------------------------------------
			# Parse XML
			#-------------------------------------------------------------------------------
			$skeldata =~ s/<--!.*?-->//gsm; # remove comments

			my $xml		  = new Mioga2::XML::Simple(forcearray => 1);
			my $xmltree   = $xml->XMLin($skeldata);
			my $skelname = (exists($xmltree->{'name'}))? $xmltree->{'name'} : "";

			# Ignore hidden skeletons
			next if ($xmltree->{hidden});

			# Ignore protected skeletons if user has no right to use them
			next if ($xmltree->{protected} && !$self->GetUserRights ()->{ProtectedSkeletons});

			push (@{$data->{skeleton}}, {file => $skelfile, name => $skelname});
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::GetSkeletons] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetSkeletons  ----------


#===============================================================================

=head2 GetSkeletonDetails

Get skeleton details as XML or JSON.

=head3 Incoming Arguments

=over

=over

=item B<type>: Type of skeleton. Value can be 'group' or 'user' and is case insensitive.

=item B<lang>: Language of the skeleton. Value is a supported language with associated skeletons.

=item B<file>: Skeleton filename without path.

=back

=back

=head3 Generated XML

=over

	<GetSkeletonDetails>
		<attributes>
			<!-- Inner attributes of the skeleton -->
			...
		</attributes>
		<users>
			<!-- List of users -->
			...
		</users>
		<groups>
			<!-- List of groups -->
			...
		</groups>
		<teams>
			<!-- List of teams -->
			...
		</teams>
		<applications>
			<!-- List of applications -->
			...
		</applications>
	</GetSkeletonDetails>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetSkeletonDetails {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetSkeletonDetails] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['type'], 'disallow_empty', [ 'match', "^\(user|group\)\$" ] ],
			[ [ 'lang' ], 'disallow_empty', 'stripxws' ],
			[ [ 'file' ], 'disallow_empty', 'filename' ],
			[ ['full_list'], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $skel = Mioga2::Skeleton->new ($context->GetConfig (), type => $values->{type}, lang => $values->{lang}, file => $values->{file});
		$data->{attributes} = $skel->GetAttributes ();

		# Convert animator email to animator rowid if applicable
		if (($values->{type} eq 'group') && (defined ($data->{attributes}->{animator}))) {
			my $anim = Mioga2::UserList->new ($context->GetConfig (), { attributes => { email => delete ($data->{attributes}->{animator}) } });
			$data->{attributes}->{animator_id} = $anim->Get ('rowid');
		}

		# Initialize user list
		my $users = $skel->GetUsers ();
		for ($users->GetUsers ()) {
			$_->{selected} = 1 if ($values->{full_list});
			push (@{$data->{users}->{user}}, $_);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$users->Revert ();
			$users->Load ();
			for ($users->GetUsers ()) {
				push (@{$data->{users}->{user}}, $_);
			}
		}

		# Initialize group list
		my $groups = $skel->GetGroups ();
		for ($groups->GetGroups ()) {
			$_->{selected} = 1 if ($values->{full_list});
			push (@{$data->{groups}->{group}}, $_);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$groups->Revert ();
			$groups->Load ();
			for ($groups->GetGroups ()) {
				push (@{$data->{groups}->{group}}, $_);
			}
		}

		# Initialize team list
		my $teams = $skel->GetTeams ();
		for ($teams->GetTeams ()) {
			$_->{selected} = 1 if ($values->{full_list});
			push (@{$data->{teams}->{team}}, $_);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$teams->Revert ();
			$teams->Load ();
			for ($teams->GetTeams ()) {
				push (@{$data->{teams}->{team}}, $_);
			}
		}

		# Initialize application list
		my $applications = $skel->GetApplications ();
		for my $app ($applications->GetApplications ()) {
			my $ident = $app->{ident};
			$app->{selected} = 1 if ($values->{full_list});
			$app->{protected} = 1 if ($values->{type} eq 'group' && grep (/^$ident$/, @Mioga2::GroupList::ProtectedApplications));
			$app->{protected} = 1 if ($values->{type} eq 'user' && grep (/^$ident$/, @Mioga2::UserList::ProtectedApplications));
			push (@{$data->{applications}->{application}}, $app);
		}

		# Revert list if needed
		if (st_ArgExists ($context, 'full_list')) {
			$applications->Revert ();
			$applications->Load ();
			for ($applications->GetApplications ()) {
				push (@{$data->{applications}->{application}}, $_);
			}
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::GetSkeletonDetails] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetSkeletonDetails  ----------


#===============================================================================

=head2 GetTaskCategories

Get task categories

=head3 Incoming Arguments

=over

None.

=back

=head3 Generated XML

=over

	<GetTaskCategories>
		<category>
			<rowid>...</rowid>
			<name>...</name>
			<bgcolor>...</bgcolor>
			<fgcolor>...</fgcolor>
			<private>...</private>
		</category>
		...
	</GetTaskCategories>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetTaskCategories {
	my ($self, $context) = @_;

	my $data = { };

	my $taskcat = new Mioga2::TaskCategory ($context->GetConfig ());
	my $cat = $taskcat->GetList ();
	for my $c (@$cat) {
		$c->{label} = '<div class="task-category-label" style="background-color: ' . $c->{bgcolor} . ';color: ' . $c->{fgcolor} . ';">' . $c->{name} . '</div>';
	}
	$data->{category} = $cat;

	print STDERR "[Mioga2::Colbert::GetTaskCategories] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetTaskCategories  ----------


#===============================================================================

=head2 GetTaskCategoryDetails

Get one task category

=head3 Incoming Arguments

=over

=item B<rowid>: the category rowid to get details.

=back

=head3 Generated XML

=over

	<GetTaskCategoryDetails>
		<category>
			<rowid>...</rowid>
			<name>...</name>
			<bgcolor>...</bgcolor>
			<fgcolor>...</fgcolor>
			<private>...</private>
		</category>
		...
	</GetTaskCategoryDetails>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetTaskCategoryDetails {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetUserDetails] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $taskcat = new Mioga2::TaskCategory ($context->GetConfig ());
		my $cat = $taskcat->GetTaskCategory ($values->{rowid});
		$data->{category} = $cat;
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::GetTaskCategoryDetails] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetTaskCategoryDetails  ----------


#===============================================================================

=head2 SetTaskCategory

Create or update a task category.

=head3 Incoming Arguments

=over

=over

=item B<rowid> (I<optional>): The rowid of the task category to update. At creation time, this argument doesn't exist.

=item B<name>: The task category name.

=item B<bgcolor>: The background color.

=item B<fgcolor>: The foreground color.

=item B<private>: The private flag.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<SetTaskCategory>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</SetTaskCategory>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub SetTaskCategory {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SetTaskCategory] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ [ 'rowid' ], 'allow_empty' ],
			[ [ 'name', 'bgcolor', 'fgcolor' ], 'disallow_empty', 'stripxws' ],
			[ [ 'private' ], 'disallow_empty', [ 'match', "\(0|1\)" ] ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $taskcat = new Mioga2::TaskCategory ($context->GetConfig ());

		# Convert "private" to boolean
		if ($values->{private} eq '0') {
			$values->{private} = 'f';
		}
		else {
			$values->{private} = 't';
		}

		if (st_ArgExists ($context, 'rowid')) {
			#-------------------------------------------------------------------------------
			# Update category
			#-------------------------------------------------------------------------------
			$data->{rowid} = $taskcat->Update (%$values);
		}
		else {
			#-------------------------------------------------------------------------------
			# Create new category
			#-------------------------------------------------------------------------------
			$data->{rowid} = $taskcat->Create (%$values);
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::SetTaskCategory] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetTaskCategory  ----------


#===============================================================================

=head2 DeleteTaskCategory

Delete a user.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: The rowid of the user to delete.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<DeleteTaskCategory>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</DeleteTaskCategory>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub DeleteTaskCategory {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::DeleteTaskCategory] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $taskcat = new Mioga2::TaskCategory ($context->GetConfig ());
		for my $rowid (@{ac_ForceArray ($values->{rowid})}) {
			$taskcat->Delete ($rowid);
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::DeleteTaskCategory] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DeleteTaskCategory  ----------


#===============================================================================

=head2 GetThemes

Get themes

=head3 Incoming Arguments

=over

None.

=back

=head3 Generated XML

=over

	<GetThemes>
		<theme>
			<rowid>...</rowid>
			<name>...</name>
			<ident>...</ident>
		</category>
		...
	</GetThemes>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetThemes {
	my ($self, $context) = @_;

	my $data = { };

	my $theme = new Mioga2::Theme ($context->GetConfig (), $context->GetUser ());
	$data->{theme} = $theme->GetList ();

	print STDERR "[Mioga2::Colbert::GetThemes] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetThemes  ----------


#===============================================================================

=head2 GetThemeDetails

Get themes

=head3 Incoming Arguments

=over

=over

=item B<rowid>: The theme rowid.

=back

=back

=head3 Generated XML

=over

	<GetThemeDetails>
		<theme>
			<rowid>...</rowid>
			<name>...</name>
			<ident>...</ident>
		</category>
		...
	</GetThemeDetails>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetThemeDetails {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::GetThemeDetails] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	my $theme = new Mioga2::Theme ($context->GetConfig (), $context->GetUser ());
	$data->{theme} = $theme->GetTheme ($values->{rowid});

	my $inst = Mioga2::InstanceList->new ($context->GetConfig ()->GetMiogaConf (), { attributes => { rowid => $context->GetConfig ()->GetMiogaId () } });
	if ($values->{rowid} == $inst->Get ('default_theme_id')) {
		$data->{theme}->{is_default} = 1;
	}


	print STDERR "[Mioga2::Colbert::GetThemeDetails] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetThemeDetails  ----------


# ============================================================================

=head2 SetTheme

Create a new theme.

=head3 Incoming Arguments

=over

=over

=item B<ident>: The theme ident.

=item B<name>: The theme name.

=item B<path>: The path to place the theme into DAV space.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<SetTheme>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</SetTheme>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut
#
#	Create a new theme.
#
# ============================================================================

sub SetTheme {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SetTheme] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['ident'], 'disallow_empty', [ 'valid_ident', $context->GetConfig () ] ],
			[ ['name'], 'disallow_empty', 'stripxws' ],
			[ ['path'], 'disallow_empty', ['location', $context->GetConfig ()]],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $config  = $context->GetConfig();
		my $theme = new Mioga2::Theme ($config, $context->GetUser ());
		try {
			$data->{rowid} = $theme->Create (%$values);
		}
		catch Mioga2::Exception::Simple with {
			my $err = shift;
			print STDERR "Theme creation failed\n";
			print STDERR $err->stringify ($context);
			push (@{$data->{errors}}, { 'ident', ['cannot_create_theme'] });
		};
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::SetTheme] Data: " . Dumper $data if ($debug);
	return ($data);
}


#===============================================================================

=head2 DeleteTheme

Delete a user.

=head3 Incoming Arguments

=over

=over

=item B<rowid>: The rowid of the user to delete.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<DeleteTheme>
		<errors>
			<error arg="...">...</error>
			...
		</errors>
	</DeleteTheme>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub DeleteTheme {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::DeleteTheme] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['rowid'], 'disallow_empty' ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $theme = new Mioga2::Theme ($context->GetConfig (), $context->GetUser ());
		for my $rowid (@{ac_ForceArray ($values->{rowid})}) {
			try {
				$theme->Delete ($rowid);
			}
			otherwise {
				my $err = shift;
				print STDERR "Theme deletion failed\n";
				print STDERR $err->stringify ($context);
				push (@$errors, [ $rowid, [ 'cannot_delete_theme' ] ]);
			};
		}
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::DeleteTheme] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DeleteTheme  ----------


#===============================================================================

=head2 GetPasswordPolicy

Get instance password policy

=head3 Incoming Arguments

=over

None.

=back

=head3 Generated XML

=over

	<GetPasswordPolicy>
		<policy>
			<pwd_min_length>...</pwd_min_length>
			<pwd_min_letter>...</pwd_min_letter>
			<pwd_min_digit>...</pwd_min_digit>
			<pwd_min_special>...</pwd_min_special>
			<pwd_min_chcase>...</pwd_min_chcase>
		</policy>
	</GetPasswordPolicy>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetPasswordPolicy {
	my ($self, $context) = @_;

	my $data = $context->GetConfig ()->GetPasswordPolicy ();

	print STDERR "[Mioga2::Colbert::GetPasswordPolicy] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetPasswordPolicy  ----------


#===============================================================================

=head2 SetPasswordPolicy

Set password policy for current instance

=head3 Incoming Arguments

=over

=over

=item B<pwd_min_length>: The password minimum length (number of characters).

=item B<pwd_min_letter>: The password minimum letters number.

=item B<pwd_min_special>: The password minimum special characters number.

=item B<pwd_min_chcase>: The password minimum uppercase characters number.

=item B<use_secret_question>: Whether to use a secret question (0 or 1).

=back

=back

=cut

#===============================================================================
sub SetPasswordPolicy {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SetPasswordPolicy] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ [ 'pwd_min_length', 'pwd_min_letter', 'pwd_min_digit', 'pwd_min_special', 'pwd_min_chcase' ], 'disallow_empty', 'want_int' ],
			[ [ 'use_secret_question' ], 'disallow_empty', [ 'match', "\(0|1\)" ] ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };
	
	if (!@$errors) {
		my $inst = Mioga2::InstanceList->new ($context->GetConfig ()->GetMiogaConf (), { attributes => { rowid => $context->GetConfig ()->GetMiogaId () } });
		for my $key (keys (%$values)) {
			$inst->Set ($key, $values->{$key});
		}
		$inst->Store ();
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	print STDERR "[Mioga2::Colbert::SetPasswordPolicy] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetPasswordPolicy  ----------


#===============================================================================

=head2 DownloadSkeletons

Download instance user and group skeletons as an archive.

=head3 Incoming Arguments

=over

=item B<type>: The skeleton type to download. Value can be 'user' or 'group'.

=back

=head3 Generated output

=over

This method returns a ZIP file containing skeleton filesystem structure.

=back

=cut

#===============================================================================
sub DownloadSkeletons {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['type'], 'disallow_empty', [ 'match', "^\(user|group\)\$" ] ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $content;

	if (!@$errors) {
		my $zip = Archive::Zip->new ();
		my $zipmember = $zip->addTreeMatching ($context->GetConfig ()->GetSkeletonDir (), '', ".*$values->{type}.*");

		my $zipContents = '';
		my ($tempfh, $tempfile) = tempfile ();
		$zip->writeToFileNamed ($tempfile);
		open (FH, $tempfile);
		while (<FH>) {
			$zipContents .= $_;
		}
		close (FH);
		unlink ($tempfile);

		$content = Mioga2::Content::ASIS->new ($context, MIME => 'application/zip', filename => $context->GetConfig ()->GetMiogaIdent () . "-$values->{type}-skeletons.zip");
		$content->SetContent ($zipContents);
	}

	print STDERR "[Mioga2::Colbert::DownloadSkeletons] Leaving." if ($debug);
	return ($content);
}	# ----------  end of subroutine DownloadSkeletons  ----------


#===============================================================================

=head2 UploadSkeletons

Upload an instance user and group skeletons archive and overwrite current ones.

=head3 Incoming Arguments

=over

=item B<type>: The skeleton type to overwrite. Value can be 'user' or 'group'.

=item B<skeletons_archive>: The uploaded file name. This argument will not be processed but the uploaded file content will.

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<UploadSkeletons>
		<errors>
			<error>...</error>
			...
		</errors>
	</UploadSkeletons>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub UploadSkeletons {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::UploadSkeletons] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['skeletons_archive'], 'disallow_empty' ],
			[ ['type'], 'disallow_empty', [ 'match', "^\(user|group\)\$" ] ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $upload_data = $context->GetUploadData ();

	my $data = { };

	if (!@$errors) {
		my $tmpdir = tempdir ();

		try {
			# Extract ZIP contents to temp dir
			my $SH = IO::Scalar->new (\$upload_data);
			my $zip = Archive::Zip->new ();
			$zip->readFromFileHandle ($SH);
			$zip->extractTree ('', "$tmpdir/");

			# Overwrite skeletons with extracted ZIP contents
			my @langs = <$tmpdir/*>;
			foreach my $lang (@langs) {
				# Skip invalid subdirs
				$lang =~ s/^$tmpdir\///;
				next if ($lang !~ m/^[a-z]{2}_[A-Z]{2}$/);

				# Overwrite data
				rmtree ($context->GetConfig ()->GetSkeletonDir () . "/$lang/$values->{type}") or throw Mioga2::Exception::Simple ('Mioga2::Colbert::UploadSkeletons', __("Unable to delete original skeletons.") . " $!");
				dirmove ("$tmpdir/$lang/$values->{type}", $context->GetConfig ()->GetSkeletonDir () . "/$lang/$values->{type}") or throw Mioga2::Exception::Simple ('Mioga2::Colbert::UploadSkeletons', __("Unable to copy new skeletons.") . " $!");
			}
		}
		otherwise {
			my $err = shift;
			print STDERR "[Mioga2::Colbert::UploadSkeletons] Skeletons extracting failed\n";
			print STDERR $err->stringify ($context);
			push (@$errors, [ 'global', [ $err->{'-text'} ] ]);
		};

		# Remove temp dir
		rmtree ($tmpdir);

	}

	print STDERR "[Mioga2::Colbert::UploadSkeletons] Leavind. Data: " . Dumper $data if ($debug);

	my $xml = '<UploadSkeletons>';

	$xml .= $context->GetXML ();

	ac_I18NizeErrors($errors);
	$xml .= '<errors>';
	for (@$errors) {
		$xml .= '<err field="' . $_->[0] . '">' . $_->[1]->[0] . '</err>';
	}
	$xml .= '</errors>';

	$xml .= '</UploadSkeletons>';

	print STDERR "Colbert::UploadSkeletons xml: $xml \n" if ($debug);

	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert.xsl', locale_domain => "colbert_xsl");
    $content->SetContent($xml);

	return $content;
}	# ----------  end of subroutine UploadSkeletons  ----------


#===============================================================================

=head2 StorePreferences

Store user preferences (column widths, sort field).

=cut

#===============================================================================
sub StorePreferences {
	my ($self, $context) = @_;
	print STDERR "[Colbert::StorePreferences] Entering, prefs: " . Dumper $context->{args} if ($debug);

	my $data = {};
	$data->{success} = 1;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ ['sort_field', 'columns', 'widths'], 'allow_empty', 'stripxws'],	# Grid-related preferences
			[ ['user_sort_field', 'user_sort_order', 'group_sort_field', 'group_sort_order', 'team_sort_field', 'team_sort_order', 'application_sort_field', 'application_sort_order'], 'allow_empty', 'stripxws'],	# SelectList-related preferences
			[ ['method'], 'disallow_empty', [ 'match', "^\(DisplayUsers|DisplayGroups|DisplayTeams|DisplayApplications|DisplayResources|DisplayTaskCategories|DisplayThemes|DisplayExternalMiogas\)\$" ] ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (!@$errors) {
		my $session = $context->GetSession ();

		for my $key (keys (%$values)) {
			next if ($key eq 'method');
			next if ($key eq 'width');

			if ($key eq 'columns') {
				for my $col (@{$values->{columns}}) {
					$session->{persistent}->{$values->{method}}->{column_widths}->{$col} = shift (@{$values->{widths}});
				}
			}
			else {
				$session->{persistent}->{$values->{method}}->{$key} = $values->{$key};
			}
		}

		$data->{success} = 1;
	}
	else {
		$data->{errors} = $errors;
	}

	print STDERR "[Colbert::StorePreferences] Leaving.\n" if ($debug);
	return ($data);
}	# ----------  end of subroutine StorePreferences  ----------


#===============================================================================

=head2 GetUserGroupReport

Get report of user affectations to groups

=cut

#===============================================================================
sub GetUserGroupReport {
	my ($self, $context) = @_;

	my $config = $context->GetConfig ();
	my $mioga_id = $config->GetMiogaId ();

	my $sql = "SELECT * FROM view_user_groups WHERE mioga_id = $mioga_id;";
	print STDERR "[Colbert::GetUserGroupReport] SQL: $sql\n" if ($debug);
	$self->{db}->Execute ($sql);

	# Process each line of output to generate an array of hashes
	# $data->{user} = (
	#     {
	#         rowid     => '...',
	#         firstname => '...',
	#         lastname  => '...',
	#         email     => '...',
	#         group     => (
	#             {
	#                 group_id    => '...',
	#                 group_ident => '...',
	#                 profile     => (
	#                     {
	#                         profile_id    => '...',
	#                         profile_ident => '...',
	#                         team_id       => '...',  # May be undef if user is directly invited into group
	#                         team_ident    => '...',  # May be undef if user is directly invited into group
	#                     }, # ...
	#                 )
	#             }, # ...
	#         )
	#     }, # ...
	# )
	my $data = ();
	my $user = undef;
	my $group = undef;
	while (my $row = $self->{db}->Fetch ()) {
		if (!$user || ($row->{rowid} != $user->{rowid})) {
			# User of current row is different from the one of previous row, add previous user to array
			push (@{$user->{group}}, $group) if ($group && $group->{group_id});
			push (@{$data->{user}}, $user) if ($user);

			# Initialize a new hash for current user
			$user = {};
			$group = undef;
			for my $attr (qw/rowid firstname lastname email/) {
				$user->{$attr} = $row->{$attr};
			}
		}

		if (!$group || ($row->{group_id} != $group->{group_id})) {
			# Group of current rowi is different from the one of previous row, add previous group to array
			push (@{$user->{group}}, $group) if ($group);

			# Initialize a new hash for current group
			$group = {};
			for my $attr (qw/group_id group_ident/) {
				$group->{$attr} = $row->{$attr};
			}
		}

		# Add profile information
		my $profile = {};
		for my $attr (qw/profile_id profile_ident team_id team_ident/) {
			$profile->{$attr} = $row->{$attr};
		}
		push (@{$group->{profile}}, $profile);
	}

	# Don't forget last user of the list
	push (@{$user->{group}}, $group) if ($group && $group->{group_id});
	push (@{$data->{user}}, $user) if ($user);

	print STDERR "[Colbert::GetUserGroupReport] Report: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetUserGroupReport  ----------


#===============================================================================

=head2 GetUserTeamReport

Get report of user affectations to teams

=cut

#===============================================================================
sub GetUserTeamReport {
	my ($self, $context) = @_;

	my $config = $context->GetConfig ();
	my $mioga_id = $config->GetMiogaId ();

	my $sql = "SELECT * FROM view_user_teams WHERE mioga_id = $mioga_id;";
	print STDERR "[Colbert::GetUserTeamReport] SQL: $sql\n" if ($debug);
	$self->{db}->Execute ($sql);

	# Process each line of output to generate an array of hashes
	# $data->{user} = (
	#     {
	#         rowid     => '...',
	#         firstname => '...',
	#         lastname  => '...',
	#         email     => '...',
	#         team      => (
	#             {
	#                 team_id    => '...',
	#                 team_ident => '...',
	#                 is_member  => 1,  # Can be undef if user is not member
	#             }, # ...
	#         )
	#     }, # ...
	# )
	my $data = ();
	my $user = undef;
	my $team = undef;
	while (my $row = $self->{db}->Fetch ()) {
		if (!$user || ($row->{rowid} != $user->{rowid})) {
			# User of current row is different from the one of previous row, add previous user to array
			push (@{$user->{team}}, $team) if ($team && $team->{team_id});
			push (@{$data->{user}}, $user) if ($user);

			# Initialize a new hash for current user
			$user = {};
			$team = undef;
			for my $attr (qw/rowid firstname lastname email/) {
				$user->{$attr} = $row->{$attr};
			}
		}

		if (!$team || ($row->{team_id} != $team->{team_id})) {
			# Team of current rowid is different from the one of previous row, add previous team to array
			push (@{$user->{team}}, $team) if ($team);

			# Initialize a new hash for current team
			$team = {};
			for my $attr (qw/team_id team_ident is_member/) {
				$team->{$attr} = $row->{$attr};
			}
		}
	}

	# Don't forget last user of the list
	push (@{$user->{team}}, $team) if ($team && $team->{team_id});
	push (@{$data->{user}}, $user) if ($user);

	print STDERR "[Colbert::GetUserTeamReport] Report: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetUserTeamReport  ----------


#===============================================================================

=head2 GetTeamGroupReport

Get report of team affectations to groups

=cut

#===============================================================================
sub GetTeamGroupReport {
	my ($self, $context) = @_;

	my $config = $context->GetConfig ();
	my $mioga_id = $config->GetMiogaId ();

	my $sql = "SELECT * FROM view_team_groups WHERE mioga_id = $mioga_id;";
	print STDERR "[Colbert::GetTeamGroupReport] SQL: $sql\n" if ($debug);
	$self->{db}->Execute ($sql);

	# Process each line of output to generate an array of hashes
	# $data->{team} = (
	#     {
	#         rowid     => '...',
	#         ident     => (
	#             {
	#                 group_id    => '...',
	#                 group_ident => '...',
	#                 profile_id    => '...',
	#                 profile_ident => '...',
	#             }, # ...
	#         )
	#     }, # ...
	# )
	my $data = ();
	my $team = undef;
	my $group = undef;
	while (my $row = $self->{db}->Fetch ()) {
		if (!$team || ($row->{rowid} != $team->{rowid})) {
			# Team of current row is different from the one of previous row, add previous team to array
			push (@{$data->{team}}, $team) if ($team && $team->{rowid});

			# Initialize a new hash for current team
			$team = {};
			for my $attr (qw/rowid ident/) {
				$team->{$attr} = $row->{$attr};
			}
		}

		# Initialize a new hash for current group
		$group = {};
		for my $attr (qw/group_id group_ident/) {
			$group->{$attr} = $row->{$attr};
		}

		# Add profile information
		my $profile = {};
		for my $attr (qw/profile_id profile_ident/) {
			$profile->{$attr} = $row->{$attr};
		}
		push (@{$group->{profile}}, $profile);

		push (@{$team->{group}}, $group);
	}

	# Don't forget last team of the list
	push (@{$data->{team}}, $team) if ($team && $team->{rowid});

	print STDERR "[Colbert::GetTeamGroupReport] Report: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetTeamGroupReport  ----------


#===============================================================================

=head2 GetTagLists

Returns the list of tag-lists available to current instance

=head3 Incoming Arguments

=over

None.

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<GetTagLists>
		<list>
			<name>...</name>
		</list>
		...
	</GetTagLists>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetTagLists {
	my ($self, $context) = @_;

	my $data = { };

	my $taglist = Mioga2::TagList->new ($context->GetConfig ());
	$data->{list} = $taglist->GetTagLists ();

	print STDERR "[Mioga2::Colbert::GetTagList] Leaving. Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetTagLists  ----------


#===============================================================================

=head2 SetTagList

Create or replace an existing tag-list from a file.

=head3 Incoming arguments

=over

=over

=item B<file>: The uploaded file name. This argument will not be processed but the uploaded file content will.

=back

=back

=head3 Generated XML

=over

The generated XML is an error list. If no errors are reported, the list is empty.

	<ImportUsers>
		<errors>
			<error>...</error>
			...
		</errors>
	</ImportUsers>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub SetTagList {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::SetTagList] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['file'], 'disallow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { SetTagList => $context->GetContext () };

	if (!@$errors) {
		my @list = split (/\n/, st_CheckUTF8($context->GetUploadData ()));
		my $taglist = Mioga2::TagList->new ($context->GetConfig ());
		$data->{SetTagList}->{errors} = $taglist->SetTags ($values->{file}, \@list);
	}

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "[Mioga2::Colbert::SetTagList] xml: $xml \n" if ($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'colbert-parts.xsl', locale_domain => "colbert_xsl");
	$content->SetContent($xml);

	return ($content);
}	# ----------  end of subroutine SetTagList  ----------


#===============================================================================

=head2 GetTagsCatalog

Download tags catalog

=head3 Incoming Arguments

=over

=over

=item B<catalog_id>: The catalog rowid.

=back

=back

=cut

#===============================================================================
sub GetTagsCatalog {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['catalog_id'], 'disallow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ('Mioga2::Colbert::DisplayCatalogTags', 'Wrong value for catalog_id');
	}

	# Get instance tags
	my $tags = Mioga2::TagList->new ($context->GetConfig ());

	# Get catalog name
	my @list = grep { $_->{rowid} == $values->{catalog_id} } @{$tags->GetTagLists ()};
	my $filename = ($list[0]->{ident}) ? $list[0]->{ident} : $context->GetConfig ()->GetMiogaIdent () . '_tags.csv';

	my $content = new Mioga2::Content::ASIS($context, MIME => "text/plain", filename => $filename);
	$content->SetContent(join ("\n", @{$tags->GetTags ($values->{catalog_id})}));

	return ($content);
}	# ----------  end of subroutine GetTagsCatalog  ----------


#===============================================================================

=head2 DeleteTagsCatalog

Delete tags catalog

=head3 Incoming Arguments

=over

=over

=item B<catalog_id>: The catalog rowid.

=back

=back

=cut

#===============================================================================
sub DeleteTagsCatalog {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['catalog_id'], 'disallow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ('Mioga2::Colbert::DisplayCatalogTags', 'Wrong value for catalog_id');
	}

	my $data = {
		success => 1
	};

	# Get instance tags
	try {
		my $tags = Mioga2::TagList->new ($context->GetConfig ());
		$tags->DeleteCatalog ($values->{catalog_id});
	}
	otherwise {
		$data->{success} = 0;
	};

	return ($data);
}	# ----------  end of subroutine DeleteTagsCatalog  ----------


#===============================================================================

=head2 ToggleFreeTags

Toggle the possibility to use free tags

=cut

#===============================================================================
sub ToggleFreeTags {
	my ($self, $context) = @_;

	my $data = {
		success => 1
	};

	try {
		my $tags = Mioga2::TagList->new ($context->GetConfig ());
		if ($tags->GetFreeTagsStatus ()) {
			$tags->DisableFreeTags ();
		}
		else {
			$tags->EnableFreeTags ();
		}
	}
	otherwise {
		$data->{success} = 0;
	};

	return ($data);
}	# ----------  end of subroutine ToggleFreeTags  ----------


#===============================================================================

=head2 GetDiskSpace

Get disk spaced used by a group or a user (in all groups he is member of)

=head3 Incoming Arguments

=over

=item I<type>: The object type to query ('user' or 'group')

=item I<rowid>: The object rowid (optional if 'email' or 'ident' is given)

=item I<email>: The user email (only for querying a user, renders 'rowid' argument optional)

=item I<ident>: The group ident (only for querying a group, renders 'rowid' argument optional)

=back

=head3 Return value

=over

A Perl data structure to be converted to XML or JSON:

	{
		success: true / false,
		errors: [	// In case of error
			{
				field: <argument_name>,
				message: "..."
			}
		],
		size: ...
	}

=back

=cut

#===============================================================================
sub GetDiskSpace {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['type'], 'disallow_empty', ['match', "^\(user|group\)\$"] ],
			[ ['rowid'], 'allow_empty', 'want_int' ],
			[ ['email'], 'allow_empty', 'want_email' ],
			[ ['ident'], 'allow_empty', ['valid_ident', $context->GetConfig ()] ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (!st_ArgExists ($context, 'rowid') && !st_ArgExists ($context, 'email') && !st_ArgExists ($context, 'ident')) {
		push (@$errors, ['email', ['disallow_empty']]);
		push (@$errors, ['ident', ['disallow_empty']]);
	}

	my $data = {
		success => 'boolean::false'
	};

	if (!@$errors) {
		my $sql;
		my $args;
		if ($values->{type} eq 'group') {
			$sql = 'SELECT size FROM m_uri WHERE directory_id IS NULL AND ';
			if (st_ArgExists ($context, 'rowid')) {
				$sql .= 'group_id = ?';
				push (@$args, $values->{rowid});
			}
			else {
				$sql .= 'group_id = (SELECT rowid FROM m_group WHERE ident = ? AND mioga_id = ?)';
				push (@$args, $values->{ident}, $context->GetConfig ()->GetMiogaId ());
			}
		}
		else {
			$sql = "SELECT sum(size) AS size FROM m_uri WHERE mimetype != 'directory' AND ";
			if (st_ArgExists ($context, 'rowid')) {
				$sql .= 'user_id = ?';
				push (@$args, $values->{rowid});
			}
			else {
				$sql .= 'user_id = (SELECT rowid FROM m_user_base WHERE email = ? AND mioga_id = ?)';
				push (@$args, $values->{email}, $context->GetConfig ()->GetMiogaId ());
			}
		}

		my $db = $context->GetConfig ()->GetDBObject ();
		my $res = $db->SelectSingle ($sql, $args);
		if ($res) {
			$data->{success} = 'boolean::true';
			$data->{size} = $res->{size};
		}
	}
	else {
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => $error->[1]->[0]
			})
		}
	}

	return ($data);
}	# ----------  end of subroutine GetDiskSpace  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


#===============================================================================

=head2 CheckSkeletonAccess

Check if a skeleton can be used by the user

=cut

#===============================================================================
sub CheckSkeletonAccess {
	my ($self, $context, $type, $file) = @_;

	my $can_use = 1;

	my $lang = $context->GetGroup ()->Get ('lang');
	my $skelpath = $context->GetConfig ()->GetSkeletonDir () . "/$lang/$type/$file";

	#-------------------------------------------------------------------------------
	# Read file
	#-------------------------------------------------------------------------------
	my $skeldata = '';
	open(FILE, "< $skelpath") or throw Mioga2::Exception::Simple ('Mioga2::Colbert::CheckSkeletonAccess', "Can't open skeleton file '$skelpath': $!.");
	while (<FILE>) { $skeldata .= $_ };
	close (FILE);

	#-------------------------------------------------------------------------------
	# Parse XML
	#-------------------------------------------------------------------------------
	$skeldata =~ s/<--!.*?-->//gsm; # remove comments
	my $xml		  = new Mioga2::XML::Simple(forcearray => 1);
	my $xmltree   = $xml->XMLin($skeldata);

	#-------------------------------------------------------------------------------
	# Check if skeleton can be used
	#-------------------------------------------------------------------------------
	# Hidden skeletons can't be used from the interface
	$can_use = 0 if ($xmltree->{hidden});
	# User can use protected skeletons if he has ProtectedSkeletons right
	$can_use = 0 if ($xmltree->{protected} && !$self->GetUserRights ()->{ProtectedSkeletons});

	return ($can_use);
}	# ----------  end of subroutine CheckSkeletonAccess  ----------

#===============================================================================

=head2 GetAppDesc

Returns application description.

=cut

#===============================================================================
sub GetAppDesc {
	my %AppDesc = (
		ident   => 'Colbert',
		package => 'Mioga2::Colbert',
		name => __('Mioga2 Administration'),
		description => __('Manage Mioga2 users, groups, teams and resources'),
		type    => 'normal',
		api_version => '2.3',
		all_groups => 0,
		all_users  => 0,
		can_be_public => 0,
		is_user => 1,
		is_group => 1,
		is_resource => 0,
		functions => {
			'Administration' => __('Administration Functions'),
			'Reporting' => __("Reporting functions"),
			'ProtectedSkeletons' => __('Access protected skeletons'),
			'UsersRead' => __('Read access to users'),
			'UsersWrite' => __('Write access to users'),
			'UsersCreate' => __('Creation access to users'),
			'GroupsRead' => __('Read access to groups'),
			'GroupsWrite' => __('Write access to groups'),
			'GroupsCreate' => __('Creation access to groups'),
			'TeamsRead' => __('Read access to teams'),
			'TeamsWrite' => __('Write access to teams'),
			'TeamsCreate' => __('Creation access to teams'),
			'ResourcesRead' => __('Read access to resources'),
			'ResourcesWrite' => __('Write access to resources'),
			'ResourcesCreate' => __('Creation access to resources'),
			'Base' => __('Base functions'),
			'Notifications' => __('Receive notifications'),
		},
		func_methods => ,{
			'Administration' => [
				'DisplayMain',
				'DisplayUsers', 'GetUserDetails', 'GetUsers', 'ExportUsers', 'GetExternalMiogaUsers', 'GetUserTypes', 'GetUserStatuses', 'SetUser', 'DeleteUser', 'ImportUsers', 'InviteExternalUsers', 'EmailUsers',
				'DisplayGroups', 'GetGroupDetails', 'GetGroups', 'SetGroup', 'DeleteGroup',
				'DisplayTeams', 'GetTeamDetails', 'GetTeams', 'SetTeam', 'DeleteTeam',
				'DisplayResources', 'GetResourceDetails', 'GetResources', 'SetResource', 'ChangeResourceStatus', 'DeleteResource', 
				'DisplayApplications', 'GetApplicationDetails', 'GetApplications', 'SetApplication',
				'DisplayTaskCategories', 'GetTaskCategories', 'GetTaskCategoryDetails', 'SetTaskCategory', 'DeleteTaskCategory',
				'DisplayThemes', 'GetThemes', 'GetThemeDetails', 'SetTheme', 'DeleteTheme', 
				'DisplayExternalMiogas', 'GetExternalMiogas', 'SetExternalMioga', 'DeleteExternalMioga', 'GetPublicKey', 'ChangeExternalMiogaStatus', 'GetExternalMiogaDetails',
				'GetPasswordPolicy', 'GetLanguages', 'GetSkeletons', 'GetSkeletonDetails',
				'SetPasswordPolicy',
				'DownloadSkeletons', 'UploadSkeletons',
				'StorePreferences',
				'ImportUserTeams', 'ImportTeamGroups',
				'DisplayTagLists', 'GetTagLists', 'SetTagList', 'DisplayCatalogTags', 'GetTagsCatalog', 'DeleteTagsCatalog', 'ToggleFreeTags',
				'GetDiskSpace',
			],
			'Reporting' => [
				'DisplayMain',
				'DisplayUsers', 'GetUserDetails', 'GetUsers', 'ExportUsers', 'GetExternalMiogaUsers',
				'GetUserTypes', 'GetUserStatuses',
				'DisplayTeams', 'GetTeamDetails', 'GetTeams',
				'StorePreferences',
				'DisplayUserGroupReport', 'ExportUserGroupReport', 'GetUserGroupReport',
				'DisplayUserTeamReport', 'ExportUserTeamReport', 'GetUserTeamReport',
				'DisplayTeamGroupReport', 'ExportTeamGroupReport',
				'GetDiskSpace',
			],
			'ProtectedSkeletons' => [],
			'UsersRead' => [
				'DisplayMain',
				'DisplayUsers', 'GetUserDetails', 'GetUsers', 'ExportUsers', 'GetExternalMiogaUsers', 
				'GetUserTypes', 'GetUserStatuses',
				'StorePreferences',
				'GetDiskSpace',
			],
			'UsersWrite' => [
				'DisplayMain',
				'DisplayUsers', 'GetUserDetails', 'GetUsers', 'ExportUsers', 'GetExternalMiogaUsers', 'SetUser', 
				'GetUserTypes', 'GetUserStatuses',
				'GetLanguages',
				'GetPasswordPolicy',
				'StorePreferences',
				'GetDiskSpace',
			],
			'UsersCreate' => [
				'DisplayMain',
				'DisplayUsers', 'GetUserDetails', 'GetUsers', 'ExportUsers', 'GetExternalMiogaUsers', 'SetUser', 'DeleteUser', 'ImportUsers', 'InviteExternalUsers', 'EmailUsers',
				'GetUserTypes', 'GetUserStatuses',
				'GetGroups', 'GetTeams', 'GetApplications',
				'GetSkeletons', 'GetSkeletonDetails',
				'GetLanguages',
				'GetPasswordPolicy',
				'StorePreferences',
				'GetDiskSpace',
			],
			'GroupsRead' => [
				'DisplayMain',
				'DisplayGroups', 'GetGroupDetails', 'GetGroups', 
				'StorePreferences',
				'GetDiskSpace',
			],
			'GroupsWrite' => [
				'DisplayMain',
				'DisplayGroups', 'GetGroupDetails', 'GetGroups', 'SetGroup', 
				'GetUsers', 'GetTeams', 'GetApplications',
				'GetLanguages',
				'StorePreferences',
				'GetDiskSpace',
			],
			'GroupsCreate' => [
				'DisplayMain',
				'DisplayGroups', 'GetGroupDetails', 'GetGroups', 'SetGroup', 'DeleteGroup',
				'GetSkeletons', 'GetSkeletonDetails',
				'GetUsers', 'GetTeams',
				'GetLanguages',
				'StorePreferences',
				'GetDiskSpace',
			],
			'TeamsRead' => [
				'DisplayMain',
				'DisplayTeams', 'GetTeamDetails', 'GetTeams', 
				'StorePreferences',
			],
			'TeamsWrite' => [
				'DisplayMain',
				'DisplayTeams', 'GetTeamDetails', 'GetTeams', 'SetTeam', 
				'StorePreferences',
				'ImportUserTeams', 'ImportTeamGroups',
			],
			'TeamsCreate' => [
				'DisplayMain',
				'DisplayTeams', 'GetTeamDetails', 'GetTeams', 'SetTeam', 'DeleteTeam',
				'GetUsers', 'GetGroups',
				'StorePreferences',
				'ImportUserTeams', 'ImportTeamGroups',
			],
			'ResourcesRead' => [
				'DisplayMain',
				'DisplayResources', 'GetResources', 
				'StorePreferences',
			],
			'ResourcesWrite' => [
				'DisplayMain',
				'DisplayResources', 'GetResources', 'GetResourceDetails', 'SetResource', 'ChangeResourceStatus',
				'GetUsers',
				'StorePreferences',
			],
			'ResourcesCreate' => [
				'DisplayMain',
				'DisplayResources', 'GetResources', 'GetResourceDetails', 'SetResource', 'ChangeResourceStatus', 'DeleteResource', 
				'GetUsers',
				'StorePreferences',
			],
			'Base' => [
				'DisplayMain',
				'DisplayTaskCategories', 'GetTaskCategories', 'GetTaskCategoryDetails', 'SetTaskCategory', 'DeleteTaskCategory',
				'StorePreferences',
			],
			'Notifications' => [],
		},
		non_sensitive_methods => [
			'DisplayApplications',
			'DisplayCatalogTags',
			'DisplayExternalMiogas',
			'DisplayGroups',
			'DisplayMain',
			'DisplayResources',
			'DisplayTagLists',
			'DisplayTaskCategories',
			'DisplayTeamGroupReport',
			'DisplayTeams',
			'DisplayThemes',
			'DisplayUserGroupReport',
			'DisplayUsers',
			'DisplayUserTeamReport',
			'DownloadSkeletons',
			'ExportTeamGroupReport',
			'ExportUserGroupReport',
			'ExportUsers',
			'ExportUserTeamReport',
			'GetApplicationDetails',
			'GetApplications',
			'GetDiskSpace',
			'GetExternalMiogaDetails',
			'GetExternalMiogas',
			'GetExternalMiogaUsers',
			'GetGroupDetails',
			'GetGroups',
			'GetLanguages',
			'GetPasswordPolicy',
			'GetPublicKey',
			'GetResourceDetails',
			'GetResources',
			'GetSkeletonDetails',
			'GetSkeletons',
			'GetTagLists',
			'GetTagsCatalog',
			'GetTaskCategories',
			'GetTaskCategoryDetails',
			'GetTeamDetails',
			'GetTeams',
			'GetThemeDetails',
			'GetThemes',
			'GetUserDetails',
			'GetUserGroupReport',
			'GetUsers',
			'GetUserStatuses',
			'GetUserTeamReport',
			'GetUserTypes',
			'InviteExternalUsers',
			'SXMLGetUsers', 'SXMLGetUserDetails', 'SXMLGetModifiedUserList', 'SXMLCheckUserPassword',
		],
		sxml_methods => {
			'Administration' => [
				'SXMLGetUsers', 'SXMLGetUserDetails', 'SXMLGetModifiedUserList', 'SXMLCheckUserPassword',
			],
		},
		xml_methods  => { },
	);

	return \%AppDesc;
}	# ----------  end of subroutine GetAppDesc  ----------

#===============================================================================

=head2 InitApp

Initialize application internal values

=cut

#===============================================================================
sub InitApp {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Colbert::InitApp]\n" if ($debug);

	$self->{dbh} = $context->GetConfig ()->GetDBH ();
	$self->{mioga_id} = $context->GetConfig ()->GetMiogaId ();

	#-------------------------------------------------------------------------------
	# Set default column widths if none defined
	#-------------------------------------------------------------------------------
	my $session = $context->GetSession ();

	# Users
	if (!exists ($session->{persistent}->{DisplayUsers}->{column_widths})) {
		$session->{persistent}->{DisplayUsers}->{column_widths} = {
			ident => '50px',
			type => '55px',
			status => '55px',
			nb_groups => '60px',
			nb_teams => '60px',
			last_connection => '130px',
		};
	}

	# Groups
	if (!exists ($session->{persistent}->{DisplayGroups}->{column_widths})) {
		$session->{persistent}->{DisplayGroups}->{column_widths} = {
			nb_users => '75px',
			nb_teams => '60px',
			disk_space_used => '100px',
		};
	}

	# Teams
	if (!exists ($session->{persistent}->{DisplayTeams}->{column_widths})) {
		$session->{persistent}->{DisplayTeams}->{column_widths} = {
			nb_users => '75px',
			nb_groups => '60px',
		};
	}

	# Applications
	if (!exists ($session->{persistent}->{DisplayApplications}->{column_widths})) {
		$session->{persistent}->{DisplayApplications}->{column_widths} = {
			label => '200px',
			nb_users => '75px',
			nb_groups => '60px',
		};
	}

	# Resources
	if (!exists ($session->{persistent}->{DisplayResources}->{column_widths})) {
		$session->{persistent}->{DisplayResources}->{column_widths} = {
			status => '55px',
		};
	}

	# TaskCategories
	if (!exists ($session->{persistent}->{DisplayTaskCategories}->{column_widths})) {
		$session->{persistent}->{DisplayTaskCategories}->{column_widths} = {
			private => '100px',
			nb_tasks => '120px',
		};
	}

	# Themes
	if (!exists ($session->{persistent}->{DisplayThemes}->{column_widths})) {
		$session->{persistent}->{DisplayThemes}->{column_widths} = {
			nb_groups => '60px',
		};
	}

	# ExternalMiogas
	if (!exists ($session->{persistent}->{DisplayExternalMiogas}->{column_widths})) {
		$session->{persistent}->{DisplayExternalMiogas}->{column_widths} = {
			nb_user => '75px',
			last_synchro => '130px',
			status => '55px',
		};
	}
}	# ----------  end of subroutine InitApp  ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2010, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;

# Override the _isSeekable function in Archive::Zip
# because of unfixed bug http://rt.cpan.org/Public/Bug/Display.html?id=7855
no warnings 'redefine';
package Archive::Zip::Archive;
sub _isSeekable {
    my $fh = shift;
    if ( UNIVERSAL::isa( $fh, 'IO::Scalar' ) )
    {
        return $] >= 5.006;
    }
    elsif ( UNIVERSAL::isa( $fh, 'IO::String' ) )
    {
        return $] >= 5.006;
    }
    elsif ( UNIVERSAL::can( $fh, 'stat') )
    {
        return -f $fh;
    }
    return UNIVERSAL::can( $fh, 'seek');
}
use warnings 'all';

