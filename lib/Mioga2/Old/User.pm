# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
User.pm: User access class

=head1 DESCRIPTION

This module permits to access to informations on user.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Old::User;
use strict;

use base qw(Mioga2::Old::GroupBase);
use Locale::TextDomain::UTF8 'user';

use Error qw(:try);
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Mioga2::tools::mail_utils;
use Mioga2::Exception::Simple;
use Mioga2::LDAP;
use Mioga2::Old::ResourceList;
use Mioga2::Old::GroupList;
use Mioga2::Old::UserList;
use Mioga2::tools::APIGroup;
use Mioga2::ExternalMioga;
use POSIX qw(strftime);;

my $debug = 0;

# ============================================================================

=head2 new ($config, %params)

	Create a new User object.

	There are two creation methods :

=over 4

=item create from the rowid
	
	new Mioga2::Old::User ($config, rowid => $rowid)

=item create from user ident

	new Mioga2::Old::User ($config, ident => $user_ident)

=back

	Inherited, not overridden.

=cut

# ============================================================================
sub new {
	my ($class, $config, %param) = @_;
	
	my $self = { };
	
	if (!defined($config)) {
		Mioga2::Exception::Simple->throw("Mioga2::Old::User::new", __"Missing parameters.");
	}
	if ($config->{use_ldap}) {
		try {
			$self->{ldap} = new Mioga2::LDAP($config);
		}
		catch Mioga2::Exception::LDAP with {
			print STDERR "[Mioga2::Old::User::new] LDAP connection failed.\n";
		};
	}
	
	$self->{config} = $config;
	$self->{mioga_id} = $config->GetMiogaId();
	
	bless($self, $class);

	if(exists $param{ident}) {
		$self->InitializeFromIdent($param{ident});
	}
	elsif(exists $param{rowid}) {
		$self->InitializeFromRowid($param{rowid});
	}
	elsif(exists $param{login}) {
		$self->InitializeFromLogin($param{login});
	}
	elsif (%param) {
		$self->InitializeFromFields (%param);
	}
	else {
		Mioga2::Exception::Simple->throw("Mioga2::Old::User::new", __"Missing parameters.");
	}
	
	return $self;
}
# ============================================================================

=head2 GetGroups ()

	Return a GroupList object representing all groups that this user is member.

	Don t return groups where the user is invited via teams

=cut

# ============================================================================

sub GetGroups {
	my ($self) = @_;

	my $group_list = new Mioga2::Old::GroupList($self->{config});
	$group_list->AddGroupsContainingUser($self->{values}->{rowid});

	return $group_list;
}

# ============================================================================

=head2 GetExpandedGroups ()

	Return a GroupList object representing all groups that this user is member.

	Includes groups where the user is invited via teams

=cut

# ============================================================================

sub GetExpandedGroups {
	my ($self) = @_;

	my $group_list = new Mioga2::Old::GroupList($self->{config});
	$group_list->AddExpandedGroupsContainingUser($self->{values}->{rowid});

	return $group_list;
}


# ============================================================================

=head2 GetTeams ()

	Return a TeamList object representing all teams that this user is member.

=cut

# ============================================================================

sub GetTeams {
	my ($self) = @_;

	my $team_list = new Mioga2::Old::TeamList($self->{config});
	$team_list->AddTeamsContainingUser($self->{values}->{rowid});

	return $team_list;
}


# ============================================================================

=head2 GetUsers ()

	Return a UserList object representing all users workspace that this user is 
	invited.

	Includes users where the user is invited via teams

=cut

# ============================================================================

sub GetUsers {
	my ($self) = @_;

	my $user_list = new Mioga2::Old::UserList($self->{config});
	$user_list->AddExpandedUsersContainingUser($self->{values}->{rowid});

	return $user_list;
}



# ============================================================================

=head2 GetUsersWithFunction ($apps, $func)

	Return a UserList object representing all users workspace that this user is 
	invited and can execute the given function in the given apps.

=cut

# ============================================================================

sub GetUsersWithFunction {
	my ($self, $apps, $func) = @_;

	my $user_list = new Mioga2::Old::UserList($self->{config});
	$user_list->AddExpandedUsersContainingUserWithFunction($self->{values}->{rowid}, $apps, $func);

	return $user_list;
}



# ============================================================================

=head2 GetGroupsWithFunction ($apps, $func)

	Return a GroupList object representing all groups that this user is 
	invited and can execute the given function in the given apps.

=cut

# ============================================================================

sub GetGroupsWithFunction {
	my ($self, $apps, $func) = @_;

	my $group_list = new Mioga2::Old::GroupList($self->{config});
	$group_list->AddExpandedGroupsContainingUserWithFunction($self->{values}->{rowid}, $apps, $func);

	return $group_list;
}




# ============================================================================

=head2 GetResources ()

	Return a ResourceList object representing all resources that this user is member.

	Don t includes resource where the user is invited via teams

=cut

# ============================================================================

sub GetResources {
	my ($self) = @_;

	my $resource_list = new Mioga2::Old::ResourceList($self->{config});
	$resource_list->AddGroupsContainingUser($self->{values}->{rowid});

	return $resource_list;
}


# ============================================================================

=head2 GetExpandedResources ()

	Return a ResourceList object representing all resources that this user is member.

	Includes resource where the user is invited via teams

=cut

# ============================================================================

sub GetExpandedResources {
	my ($self) = @_;

	my $resource_list = new Mioga2::Old::ResourceList($self->{config});
	$resource_list->AddExpandedGroupsContainingUser($self->{values}->{rowid});

	return $resource_list;
}



# ============================================================================

=head2 GetResourcesWithFunction ($apps, $func)

	Return a ResourceList object representing all resources that this user is 
	invited and can execute the given function in the given apps.

=cut

# ============================================================================

sub GetResourcesWithFunction {
	my ($self, $apps, $func) = @_;

	my $resource_list = new Mioga2::Old::ResourceList($self->{config});
	$resource_list->AddExpandedGroupsContainingUserWithFunction($self->{values}->{rowid}, $apps, $func);

	return $resource_list;
}


# ============================================================================

=head2 GetAnimatedGroups ()

	Return a GroupList object representing all groups animated by this user.

=cut

# ============================================================================

sub GetAnimatedGroups {
	my ($self) = @_;

	my $group_list = new Mioga2::Old::GroupList($self->{config});
	$group_list->AddGroupsAnimatedBy($self->{values}->{rowid});

	return $group_list;
}

# ============================================================================

=head2 GetAnimatedResources ()

	Return a ResourceList object representing all resources animated by this user.

=cut

# ============================================================================

sub GetAnimatedResources {
	my ($self) = @_;

	my $resource_list = new Mioga2::Old::ResourceList($self->{config});
	$resource_list->AddResourcesAnimatedBy($self->{values}->{rowid});

	return $resource_list;
}



# ============================================================================

=head2 GetRemoteIdent ()

	Return the user ident form external user.
	Throw an exception if the user is not external.

=cut

# ============================================================================

sub GetRemoteIdent {
	my ($self) = @_;

	if($self->GetType() ne 'external_user') {
		throw Mioga2::Exception::User("Mioga2::Old::User::GetRemoteIdent", __"Not an external user");
	}

	return $self->{values}->{external_ident};
}



# ============================================================================

=head2 GetExternalMiogaRowid ()

	Return the rowid of the external Mioga.
	Throw an exception if the user is not external.

=cut

# ============================================================================

sub GetExternalMiogaRowid {
	my ($self) = @_;

	if($self->GetType() ne 'external_user') {
		throw Mioga2::Exception::User("Mioga2::Old::User::GetExternalMiogaRowid", __"Not an external user");
	}

	return $self->{values}->{external_mioga_id};
}



# ============================================================================

=head2 GetAnimator ()

	Return the user himself.
	Not very usefull but inherited of Group.

=cut

# ============================================================================

sub GetAnimator {
	my ($self) = @_;
	return $self;
}



# ============================================================================

=head2 SendMail (MIME::Entity)

	Send the message passed in parameters to the user.

=cut

# ============================================================================

sub SendMail {
	my ($self, $entity) = @_;

	if($entity->head()->count("To") > 0) { 
		$entity->replace("To" => $self->{values}->{email});
	}
	else {
		$entity->add("To" => $self->{values}->{email});
	}
	
	mu_SendMail($self->{config}, $entity);
}



# ============================================================================

=head2 GetName ()

	Return the user name.

=cut

# ============================================================================

sub GetName {
	my ($self) = @_;

	return "$self->{values}->{firstname} $self->{values}->{lastname}";
}



# ============================================================================

=head2 GetFirstname ()

	Return the user firstname.

=cut

# ============================================================================

sub GetFirstname {
	my ($self) = @_;

	return $self->{values}->{firstname};
}



# ============================================================================

=head2 GetLastname ()

	Return the user lastname.

=cut

# ============================================================================

sub GetLastname {
	my ($self) = @_;

	return $self->{values}->{lastname};
}



# ============================================================================

=head2 GetPassword ()

	Return the user crypted password.

=cut

# ============================================================================

sub GetPassword {
	my ($self) = @_;

	return $self->{values}->{password};
}



# ============================================================================

=head2 CheckPassword ($clear_password)

	Check if the given password match the user password.

=cut

# ============================================================================

sub CheckPassword {
	my ($self, $passwd) = @_;

	return $self->CheckLocalPassword($passwd);
}



# ============================================================================

=head2 GetEmail ()

	Return the user email.

=cut

# ============================================================================

sub GetEmail {
	my ($self) = @_;

	return $self->{values}->{email};
}



# ============================================================================

=head2 GetTimezone ()

	Return the user timezone.

=cut

# ============================================================================

sub GetTimezone {
	my ($self) = @_;
	
	return $self->{values}->{timezone};
}

# ============================================================================

=head2 GetTimezoneId ()

	Return the user timezone_id.

=cut

# ============================================================================

sub GetTimezoneId {
	my ($self) = @_;
	
	return $self->{values}->{timezone_id};
}



# ============================================================================

=head2 GetTimePerDay ()

	Return the user working time per day.

=cut

# ============================================================================

sub GetTimePerDay {
	my ($self) = @_;

	return $self->{values}->{time_per_day};
}



# ============================================================================

=head2 GetWorkingDays ()

	Return an array ref containg the working days.

	1 = Monday, .... 0 = Sunday.

=cut

# ============================================================================

sub GetWorkingDays {
	my ($self) = @_;

	if(!exists $self->{working_days_list}) {
		my @days = split(/,/, $self->{values}->{working_days});
		$self->{working_days_list} = \@days;
	}
	
	return $self->{working_days_list};
}



# ============================================================================

=head2 IsWorkingDay ($day_num)

	Return true if the given day is worked by this user.

	1 = Monday, .... 0 = Sunday.

=cut

# ============================================================================

sub IsWorkingDay {
	my ($self, $day_num) = @_;

	if(!exists $self->{working_days_list}) {
		$self->GetWorkingDays();
	}
	

	if(grep { $_ == $day_num} @{$self->{working_days_list}}) {
		return 1;
	}

	return 0;
}

# ============================================================================

=head2 IsMondayFirst ()

	Return true if monday is the first day of the week for the
	current user

=cut

# ============================================================================

sub IsMondayFirst {
	my ($self) = @_;

	return $self->{values}->{monday_first};
}



# ============================================================================

=head2 GetStatus ()

	Return the user status.

	Supported status are : active, disabled, deleted_not_purged, purged.

=cut

# ============================================================================

sub GetStatus {
	my ($self) = @_;
	
	return $self->{values}->{status};
}



# ============================================================================

=head2 GetDN ()

	Return the user dn (for ldap user only).

=cut

# ============================================================================

sub GetDN {
	my ($self) = @_;

	if($self->GetType() ne 'ldap_user') {
		throw Mioga2::Exception::User("Mioga2::Old::User::GetDN", __"User is not ldap");
	}
	
	return $self->{values}->{dn};
}


# ============================================================================

=head2 GetHomeURL ()

	Return the user home url.

=cut

# ============================================================================

sub GetHomeURL {
	my ($self) = @_;

	my $res = SelectSingle($self->{config}->GetDBH(), 
						   "SELECT home_url FROM m_home_url ".
						   "WHERE user_id = $self->{values}->{rowid}");

	if(!defined $res) {
		return "";
	}

	return $res->{home_url};
}


#===============================================================================

=head2 GetRowids

Get different rowids for a single user.
If a user is LDAP, he can belong to different instances with different rowids
but the same dn.

=cut

#===============================================================================
sub GetRowids {
	my ($self) = @_;
	print STDERR "Mioga2::Old::User::GetRowids ()\n" if ($debug);

	my $config = $self->{config};

	my @rowids = ();

	if ($self->GetType() ne 'ldap_user') {
		# User is not LDAP, single rowid
		push (@rowids, $self->GetRowid ());
	}
	else {
		# User is LDAP, get all rowids
		my $dn = $self->GetDN ();
		my $rowids = SelectMultiple ($config->GetDBH (), "SELECT rowid FROM m_user_base WHERE dn='$dn'");
		for (@$rowids) {
			push (@rowids, $_->{rowid});
		}
	}

	return (@rowids);
}	# ----------  end of subroutine GetRowids  ----------


#===============================================================================

=head2 SetSecretQuestion

Set secret question and answer for a user

=cut

#===============================================================================
sub SetSecretQuestion {
	my ($self, $question, $answer) = @_;
	print STDERR "Mioga2::Old::User::SetSecretQuestion ()\n" if ($debug);

	my $config = $self->{config};

	# Escape strings
	$question = st_FormatPostgreSQLString ($question);
	$answer = st_FormatPostgreSQLString ($answer);

	# Get different user IDs in case user is LDAP and in several instances
	my @rowids = $self->GetRowids ();
	my $req = "SELECT DISTINCT question_id FROM m_user2question WHERE user_id in (" . join (',', @rowids) . ")";
	my $id = SelectSingle ($config->GetDBH(), $req);

	if (!$id) {
		# No question set yet, define it
		ExecSQL ($config->GetDBH(), "INSERT INTO m_secret_question (question, answer) VALUES ('$question', '$answer')");
		my $question_id = GetLastInsertId ($config->GetDBH(), 'm_secret_question');
		for (@rowids) {
			ExecSQL ($config->GetDBH(), "INSERT INTO m_user2question (user_id, question_id) VALUES ($_, $question_id)");
		}
	}
	else {
		# Question already set, update it
		ExecSQL ($config->GetDBH(), "UPDATE m_secret_question SET question='$question', answer='$answer' WHERE rowid=$id->{question_id}");
	}
}	# ----------  end of subroutine SetSecretQuestion  ----------


#===============================================================================

=head2 GetSecretQuestion

Get secret question and answer for a user

=cut

#===============================================================================
sub GetSecretQuestion {
	my ($self) = @_;
	print STDERR "Mioga2::Old::User::GetSecretQuestion ()\n" if ($debug);

	my $config = $self->{config};

	# Get different user IDs in case user is LDAP and in several instances
	my @rowids = $self->GetRowids ();
	my $qa = SelectSingle ($config->GetDBH(), "SELECT question, answer FROM m_secret_question WHERE rowid=(SELECT DISTINCT question_id FROM m_user2question WHERE user_id IN (" . join (',', @rowids) . "));");

	return ($qa);
}	# ----------  end of subroutine GetSecretQuestion  ----------


#===============================================================================

=head2 CheckSecretQuestion

Check answer for secret question

=cut

#===============================================================================
sub CheckSecretQuestion {
	my ($self, $answer) = @_;
	print STDERR "Mioga2::Old::User::CheckSecretQuestion ()\n" if ($debug);

	my $config = $self->{config};

	my $ret = 0;

	# Escape strings
	$answer = st_FormatPostgreSQLString ($answer);

	# Get different user IDs in case user is LDAP and in several instances
	my @rowids = $self->GetRowids ();
	my $qa = SelectSingle ($config->GetDBH(), "SELECT question, answer FROM m_secret_question WHERE rowid=(SELECT DISTINCT question_id FROM m_user2question WHERE user_id IN (" . join (',', @rowids) . ")) AND answer~*'^" . st_FormatPostgreSQLRegExpString ($answer) . "\$';");

	if ($qa) {
		$ret = 1;
	}

	return ($ret);
}	# ----------  end of subroutine CheckSecretQuestion  ----------


#===============================================================================

=head2 ResetSecretQuestion

Reset user's secret question

=cut

#===============================================================================
sub ResetSecretQuestion {
	my ($self) = @_;
	print STDERR "Mioga2::Old::User::ResetSecretQuestion ()\n" if ($debug);

	my $config = $self->{config};

	# Get different user IDs in case user is LDAP and in several instances
	my @rowids = $self->GetRowids ();
	my $qid = SelectSingle ($config->GetDBH(), "SELECT DISTINCT question_id FROM m_user2question WHERE user_id IN (" . join (',', @rowids) . ");");
	if ($qid->{question_id}) {
		ExecSQL ($config->GetDBH(), "DELETE FROM m_secret_question WHERE rowid=$qid->{question_id}");
		ExecSQL ($config->GetDBH(), "DELETE FROM m_user2question WHERE question_id=$qid->{question_id}");
	}
}	# ----------  end of subroutine ResetSecretQuestion  ----------


#===============================================================================

=head2 DeleteSecretQuestion

Delete a user's secret question, when the user is deleted.

=cut

#===============================================================================
sub DeleteSecretQuestion {
	my ($self) = @_;
	print STDERR "Mioga2::Old::User::DeleteSecretQuestion\n" if ($debug);

	my $config = $self->{config};

	my @rowids = $self->GetRowids ();
	if (scalar(@rowids) > 1) {
		# Delete link from m_user2question
		my $qa = SelectSingle ($config->GetDBH (), "SELECT question_id FROM m_user2question WHERE user_id=" . $self->GetRowid ());
		if ($qa) {
			ExecSQL ($config->GetDBH (), "DELETE FROM m_user2question WHERE user_id=" . $self->GetRowid ());
		}
	}
	else {
		# Delete question and link
		my $qa = SelectSingle ($config->GetDBH (), "SELECT question_id FROM m_user2question WHERE user_id=" . $self->GetRowid ());
		if ($qa) {
			ExecSQL ($config->GetDBH (), "DELETE FROM m_secret_question WHERE rowid=" . $qa->{question_id});
			ExecSQL ($config->GetDBH (), "DELETE FROM m_user2question WHERE user_id=" . $self->GetRowid ());
		}
	}

	return ();
}	# ----------  end of subroutine DeleteSecretQuestion  ----------

# ============================================================================

=head2 Methods Inherited from GroupBase

	See Mioga2::Old::GroupBase for documentation of methods inherited from the 
	GroupBase object.

=cut

# ============================================================================



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head2 InitializeFromRowid ($rowid)

	Initialize the User object from user rowid.

=cut

# ============================================================================

sub InitializeFromRowid {
	my ($self, $rowid) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $db_user = SelectSingle($dbh,
							   "SELECT m_user_base.*, m_timezone.ident AS timezone, m_user_status.ident AS status, ".
							   "       m_group_type.ident AS type, m_theme.ident AS theme, m_lang.ident AS lang ".
							   
							   "FROM   m_user_base, m_timezone, m_user_status, m_group_type, m_theme, m_lang ".
							   
							   "WHERE  m_user_base.rowid=$rowid AND m_user_base.mioga_id = $self->{mioga_id} AND ".
							   "       m_timezone.rowid = m_user_base.timezone_id AND ".
							   "       m_user_status.rowid = m_user_base.status_id AND ".
							   "       m_group_type.rowid  = m_user_base.type_id AND ".
							   "       m_lang.rowid = m_user_base.lang_id AND ".
							   "       m_theme.rowid = m_user_base.theme_id");
	
	
	if(!defined $db_user) {
		throw Mioga2::Exception::User("Mioga2::Old::User::InitializeFromRowid",
        __x("User with rowid {rowid} not found", rowid => $rowid));
	}

	$self->{values} = $db_user;

	if($db_user->{type} eq 'external_user') {
		$self->InitializeExternalUser();
	}
	elsif($db_user->{type} eq 'ldap_user') {
		$self->InitializeLdapUser();
	}
}



# ============================================================================

=head2 InitializeFromIdent ($ident)

	Initialize the User object from user ident.

=cut

# ============================================================================

sub InitializeFromIdent {
	my ($self, $ident) = @_;

	my $q_ident = st_FormatPostgreSQLString($ident);
	my $dbh = $self->{config}->GetDBH();

	my $db_user = SelectSingle($dbh, 
							   "SELECT m_user_base.*, m_timezone.ident AS timezone, m_user_status.ident AS status, ".
							   "       m_group_type.ident AS type, m_theme.ident AS theme, m_lang.ident AS lang ".
							   "FROM   m_user_base, m_timezone, m_user_status, m_group_type, m_theme, m_lang ".
							   "WHERE  m_user_base.ident='$q_ident' AND m_user_base.mioga_id = $self->{mioga_id} AND ".
							   "       m_timezone.rowid = m_user_base.timezone_id AND ".
							   "       m_user_status.rowid = m_user_base.status_id AND ".
							   "       m_group_type.rowid  = m_user_base.type_id AND ".
							   "       m_lang.rowid = m_user_base.lang_id AND ".
							   "       m_theme.rowid = m_user_base.theme_id");

	if(!defined $db_user) {
		throw Mioga2::Exception::User("Mioga2::Old::User::InitializeFromIdent",
        __x("User with ident {ident} not found", ident => $ident));
	}
		
	$self->{values} = $db_user;

	if($db_user->{type} eq 'external_user') {
		$self->InitializeExternalUser();
	}
	elsif($db_user->{type} eq 'ldap_user') {
		$self->InitializeLdapUser();
	}
}



# ============================================================================

=head2 InitializeFromFields (%fields)

	Initialize the User object from misc fields.

=cut

# ============================================================================

sub InitializeFromFields {
	my ($self, %fields) = @_;


	my $where = 
	   "WHERE  m_timezone.rowid = m_user_base.timezone_id AND ".
	   "       m_user_status.rowid = m_user_base.status_id AND ".
	   "       m_group_type.rowid  = m_user_base.type_id AND ".
	   "       m_lang.rowid = m_user_base.lang_id AND ".
	   "       m_theme.rowid = m_user_base.theme_id";
	for (keys(%fields)) {
		$where .= " AND m_user_base.$_ = '$fields{$_}'" unless ($_ eq 'mioga_id' && $fields{$_} eq '*');
	}
	if (! defined ($fields{mioga_id})) {
		$where .= " AND m_user_base.mioga_id = $self->{mioga_id}"
	}

	my $dbh = $self->{config}->GetDBH();

	my $db_user = SelectSingle($dbh,
							   "SELECT m_user_base.*, m_timezone.ident AS timezone, m_user_status.ident AS status, ".
							   "       m_group_type.ident AS type, m_theme.ident AS theme, m_lang.ident AS lang ".

							   "FROM   m_user_base, m_timezone, m_user_status, m_group_type, m_theme, m_lang ".

							   $where);
	
	
	if(!defined $db_user) {
		throw Mioga2::Exception::User("Mioga2::Old::User::InitializeFromFields",
        __x("User with the following parameters not found") . " " .  Dumper \%fields);
	}

	$self->{values} = $db_user;

	if($db_user->{type} eq 'external_user') {
		$self->InitializeExternalUser();
	}
	elsif($db_user->{type} eq 'ldap_user') {
		$self->InitializeLdapUser();
	}
}



# ============================================================================

=head2 InitializeFromLogin (%fields)

	Initialize the User object from login information (can be ident or email).

=cut

# ============================================================================

sub InitializeFromLogin {
	my ($self, $login) = @_;

	my $q_login = st_FormatPostgreSQLString($login);

	if ($login eq '') {
		throw Mioga2::Exception::User("Mioga2::Old::User::InitializeFromLogin",
        __x("User trying to authenticate with empty login."));
	}

	my $where = 
	   "WHERE  m_timezone.rowid = m_user_base.timezone_id AND ".
	   "       m_user_status.rowid = m_user_base.status_id AND ".
	   "       m_group_type.rowid  = m_user_base.type_id AND ".
	   "       m_lang.rowid = m_user_base.lang_id AND ".
	   "       m_theme.rowid = m_user_base.theme_id AND ".
	   "       m_user_base.mioga_id = $self->{mioga_id} AND ".
	   "       (m_user_base.ident='$q_login' OR lower(m_user_base.email)='" . lc($q_login) . "')";

	my $dbh = $self->{config}->GetDBH();

	my $db_user = SelectSingle($dbh,
							   "SELECT m_user_base.*, m_timezone.ident AS timezone, m_user_status.ident AS status, ".
							   "       m_group_type.ident AS type, m_theme.ident AS theme, m_lang.ident AS lang ".

							   "FROM   m_user_base, m_timezone, m_user_status, m_group_type, m_theme, m_lang ".

							   $where);
	
	
	if(!defined $db_user) {
		throw Mioga2::Exception::User("Mioga2::Old::User::InitializeFromLogin",
        __x("User with the following login not found") . " " .  $login);
	}

	$self->{values} = $db_user;

	if($db_user->{type} eq 'external_user') {
		$self->InitializeExternalUser();
	}
	elsif($db_user->{type} eq 'ldap_user') {
		$self->InitializeLdapUser();
	}
}



# ============================================================================

=head2 InitializeExternalUser ()

	Initialize an external user from currently loaded rowid.

=cut

# ============================================================================

sub InitializeExternalUser {
	my($self) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $ext_user = SelectSingle($dbh, "SELECT * FROM m_user_base WHERE rowid = $self->{values}->{rowid}");

	if(!defined $ext_user) {
		throw Mioga2::Exception::User("Mioga2::Old::User::GetRemoteIdent", 
        __x("External user with rowid {rowid} not in database !!?? Probably a Bug.", rowid => $self->{rowid}));
	}
	
	map {$self->{values}->{$_} = $ext_user->{$_}} keys %$ext_user;
}


# ============================================================================

=head2 InitializeLdapUser ()

	Initialize an ldap user from currently loaded rowid.

=cut

# ============================================================================

sub InitializeLdapUser {
	my($self) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $ldap_user = SelectSingle($dbh, "SELECT * FROM m_user_base WHERE rowid = $self->{values}->{rowid}");

	if(!defined $ldap_user) {
		throw Mioga2::Exception::User("Mioga2::Old::User::GetRemoteIdent", 
        __x("Ldap user with rowid {rowid} not in database !!?? Probably a Bug.", rowid => $self->{rowid}));
	}
	
	map {$self->{values}->{$_} = $ldap_user->{$_}} keys %$ldap_user;

	my $res = SelectMultiple ($dbh, "SELECT mioga_id FROM m_user_base WHERE ident='$self->{values}->{ident}'");
	for (@$res) {
		push (@{$self->{values}->{instances}}, $_->{mioga_id});
	}
}


# ============================================================================

=head2 CheckLocalPassword ($clear_password)

	Check if the given password match the local user password.

=cut

# ============================================================================

sub CheckLocalPassword {
	my ($self, $passwd) = @_;

	if ($debug) {
		print STDERR "User::CheckLocalPassword passwd = $passwd\n";
		my $values = $self->{values}; 
		print STDERR "   values = " . Dumper($values) . "\n";
	}
	my $crypted_passwd = $self->{values}->{password};
	$crypted_passwd =~ s/^\{([^\}]+)\}//;
	my $cryptage = defined $1?$1:"clear";
	my $crypter = $self->{config}->LoadPasswordCrypter($cryptage);
	if($crypter->CheckPassword($passwd, $crypted_passwd)) {
		return 1;
	}
    elsif($self->{values}->{type} ne 'local_user') {
		if ($self->{values}->{type} eq 'ldap_user') {
			my $newpasswd = $self->UpdateLdapPassword($passwd);
			if (!defined($newpasswd)) {
				return 0;
			}
			else {
				$self->{values}->{password} = $newpasswd;
				if ($self->{config}->GetMiogaConf ()->GetConfParameter ('store_ldap_password')) {
					# No need to go further and check encrypted password from database as it has not been stored
					return 1;
				}
			}
		}
		elsif($self->{values}->{type} eq 'external_user') {
			my $newpasswd = $self->UpdateExternalPassword($passwd);
			if (!defined($newpasswd)) {
				return 0;
			}
			else {
				$self->{values}->{password} = $newpasswd;
			}
		}
		$crypted_passwd = $self->{values}->{password};
		$crypted_passwd =~ s/^\{([^\}]+)\}//;
		$cryptage = defined $1?$1:"clear";
		$crypter = $self->{config}->LoadPasswordCrypter($cryptage);
		if($crypter->CheckPassword($passwd, $crypted_passwd)) {
			return 1;
		}
	}

	return 0;
}


# ============================================================================

=head2 UpdateLdapPassword ($clear_password)

        Check if the given password match the ldap user password.

=cut

# ============================================================================

sub UpdateLdapPassword {
	my ($self, $passwd) = @_;
	print STDERR "User::UpdateLdapPassword passwd = $passwd\n" if ($debug);
	
	my $retval;
	
	try {
		$self->{ldap}->BindWith($self->{values}->{dn}, $passwd);
		print STDERR "   ---> bind OK\n" if ($debug);
		my $user = $self->{ldap}->SearchSingleUser($self->{values}->{dn});
		$user->{password} = $passwd;
		if ($self->{config}->GetMiogaConf ()->GetConfParameter ('store_ldap_password')) {
			my $values = {rowid => $self->{values}->{rowid},
												 password => $user->{password},
												 dont_modify_on_ldap => 'yes'
						};

			UserNonLocalModify($self->{config}, $values);
		}

		$retval = $passwd;
	}
	otherwise {
		my $err = shift;
		print STDERR "User::UpdateLdapPassword err = " . Dumper($err) . "\n";
	};
	
	return $retval;
}



# ============================================================================

=head2 UpdateExternalPassword ($clear_password)

        Check if the given password match the external user password.

=cut

# ============================================================================

sub UpdateExternalPassword {
	my ($self, $passwd) = @_;
	print STDERR "User::UpdateExternalPassword passwd = $passwd\n" if ($debug);
	my $retval;
	
	my $ext_mioga = new Mioga2::ExternalMioga($self->{config}, rowid => $self->{values}->{external_mioga_id});
	print STDERR "User::UpdateExternalPassword ext_mioga = ". Dumper($ext_mioga). "\n" if ($debug);
	

	my $response = $ext_mioga->SendRequest(application => 'Colbert', method => 'SXMLCheckUserPassword',
                                                                   args => { user_ident => $self->{values}->{external_ident},
                                                                             user_password => $passwd,
                                                                           }
										   );

	print STDERR "User::UpdateExternalPassword response = ". Dumper($response). "\n" if ($debug);
	my $xs = new Mioga2::XML::Simple();
	my $xmltree = $xs->XMLin($response);
	print STDERR "User::UpdateExternalPassword xmltree = ". Dumper($xmltree). "\n" if ($debug);
		
	if(exists $xmltree->{Authen} and $xmltree->{Authen}->{status} eq 'ok' ) {
		my $values = {rowid => $self->{values}->{rowid},
					 password => $passwd,
					};

		UserNonLocalModify($self->{config}, $values);
		$retval = $values->{password};
	}
	return $retval;
}


# ============================================================================

=head2 GetRowid ()

	Return the user rowid.

=cut

# ============================================================================

sub GetRowid {
	my ($self) = @_;

	return $self->{values}->{rowid};
}


#===============================================================================

=head2 RegisterPasswordFail

Register into DB that authentication failed for user

=cut

#===============================================================================
sub RegisterPasswordFail {
	my ($self) = @_;

	print STDERR "Mioga2::Old::User::RegisterPasswordFail\n" if ($debug);

	my $status_update = '';

	$self->{values}->{auth_fail}++;
	$self->{values}->{last_auth_fail} = du_SecondToISO(time());
	if ($self->{values}->{auth_fail} >= 3) {
		$status_update = ", status_id = (SELECT rowid FROM m_user_status WHERE ident = 'disabled_attacked')";
	}
	ExecSQL ($self->{config}->GetDBH(), "UPDATE m_user_base SET auth_fail=$self->{values}->{auth_fail}, last_auth_fail='$self->{values}->{last_auth_fail}' $status_update WHERE rowid=$self->{values}->{rowid}");
}	# ----------  end of subroutine RegisterPasswordFail  ----------


#===============================================================================

=head2 GetLastAuthFail

Get last authentication failure time

=cut

#===============================================================================
sub GetLastAuthFail {
	my ($self) = @_;

	my $time = du_ISOToSecond ($self->{values}->{last_auth_fail});

	return ($time);
}	# ----------  end of subroutine GetLastAuthFail  ----------


#===============================================================================

=head2 ResetPasswordFail 

Reset password failure counters

=cut

#===============================================================================
sub ResetPasswordFail {
	my ($self) = @_;

	ExecSQL ($self->{config}->GetDBH (), "UPDATE m_user_base SET auth_fail = 0 WHERE rowid = $self->{values}->{rowid}");
	$self->{values}->{auth_fail} = 0;
}	# ----------  end of subroutine ResetPasswordFail ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LDAP Mioga2::Old::Group Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
