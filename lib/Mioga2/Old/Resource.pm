# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Resource.pm: Resource access class

=head1 DESCRIPTION

This module permits to access to informations on resource.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Old::Resource;
use strict;

use base qw(Mioga2::Old::Group);
use Locale::TextDomain::UTF8 'resource';

use Error qw(:try);
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Resource;

my $debug=0;



# ============================================================================

=head2 new ($config, %params)

	Create a new Resource object.

	There are two creation methods :

=over 4

=item create from the rowid
	
	new Mioga2::Old::Resource ($config, rowid => $rowid)

=item create from resource ident

	new Mioga2::Old::Resource ($config, ident => $resource_ident)

=back

	Inherited, not overridden.

=cut

# ============================================================================

sub new {
	my ($class, @params) = @_;

	my $self = $class->SUPER::new(@params, type => 'resource');

	return $self;
}



# ============================================================================

=head2 GetInvitedGroups ()

	Return a Migoa2::GroupList object representing groups invited in the resource.

=cut

# ============================================================================

sub GetInvitedGroups {
	my ($self, $entity) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $sql = "SELECT m_group.rowid FROM m_group, m_group_group ".
		      "WHERE m_group_group.group_id = $self->{values}->{rowid} AND ".
			  "      m_group.rowid = m_group_group.invited_group_id ";

	my $res = SelectMultiple($dbh, $sql);

	my @rowids = map {$_->{rowid}} @$res;

	return new Mioga2::Old::GroupList($self->{config}, rowid_list => \@rowids);
}


# ============================================================================

=head2 SendMail (MIME::Entity)

	Send the message passed in parameters to the resource animator.

=cut

# ============================================================================

sub SendMail {
	my ($self, $entity) = @_;

	my $animator = $self->GetAnimator();
	$animator->SendMail($entity);
}




# ============================================================================

=head2 GetLocation ()

	Return the resource location.

=cut

# ============================================================================

sub GetLocation {
	my ($self) = @_;

	return $self->{values}->{location};
}



# ============================================================================

=head2 GetTimezone ()

	Return the resource timezone.

=cut

# ============================================================================

sub GetTimezone {
	my ($self) = @_;
	
	return $self->{values}->{timezone};
}



# ============================================================================

=head2 GetTimePerDay ()

	Return the resource working time per day.

=cut

# ============================================================================

sub GetTimePerDay {
	my ($self) = @_;

	return $self->{values}->{time_per_day};
}



# ============================================================================

=head2 GetAvailableDays ()

	Return an array ref containg the available days.

	1 = Monday, .... 7 = Sunday.

=cut

# ============================================================================

sub GetAvailableDays {
	my ($self) = @_;

	if(!exists $self->{working_days_list}) {
		my @days = split(/,/, $self->{values}->{working_days});
		$self->{working_days_list} = \@days;
	}
	
	return $self->{working_days_list};
}



# ============================================================================

=head2 IsAvailableDay ($day_num)

	Return true if the resource is available the given days.

	1 = Monday, .... 7 = Sunday.

=cut

# ============================================================================

sub IsAvailableDay {
	my ($self, $day_num) = @_;

	if(!exists $self->{working_days_list}) {
		$self->GetAvailableDays();
	}
	

	if(grep { $_ == $day_num} @{$self->{working_days_list}}) {
		return 1;
	}

	return 0;
}



# ============================================================================

=head2 Methods Inherited from Group

	See Mioga2::Old::Group for documentation of methods inherited from the Group
	object.

=cut

# ============================================================================


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head2 InitializeFromRowid ($rowid)

	Initialize the Resource object from resource rowid.

=cut

# ============================================================================

sub InitializeFromRowid {
	my ($self, $rowid) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $sql = "SELECT m_resource.*, m_timezone.ident AS timezone, m_resource_status.ident AS status, ".
		      "       m_group_type.ident AS type, m_theme.ident AS theme, m_lang.ident AS lang ".
			  
			  "FROM   m_resource, m_timezone, m_resource_status, m_group_type, m_theme, m_lang ".
			  
			  "WHERE  m_resource.rowid=$rowid AND m_resource.mioga_id = $self->{mioga_id} AND ".
			  "       m_timezone.rowid = m_resource.timezone_id AND ".
			  "       m_resource_status.rowid = m_resource.status_id AND ".
			  "       m_group_type.rowid  = m_resource.type_id AND ".
			  "       m_lang.rowid = m_resource.lang_id AND ".
			  "       m_theme.rowid = m_resource.theme_id";

	my $resource = SelectSingle($dbh, $sql);
	
	if(!defined $resource) {
		throw Mioga2::Exception::Resource("Mioga2::Old::Resource::InitializeFromRowid",
        __x("Resource with rowid {rowid} not found", rowid => $rowid));
	}

	$self->{values} = $resource;
}



# ============================================================================

=head2 InitializeFromIdent ($ident)

	Initialize the Resource object from resource ident.

=cut

# ============================================================================


sub InitializeFromIdent {
	my ($self, $ident) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $q_ident = st_FormatPostgreSQLString($ident);
	my $sql = "SELECT m_resource.*, m_timezone.ident AS timezone, m_resource_status.ident AS status, ".
		      "       m_group_type.ident AS type, m_theme.ident AS theme, m_lang.ident AS lang ".
			  
			  "FROM   m_resource, m_timezone, m_resource_status, m_group_type, m_theme, m_lang ".
			  
			  "WHERE  m_resource.ident='$q_ident' AND m_resource.mioga_id = $self->{mioga_id} AND ".
			  "       m_timezone.rowid = m_resource.timezone_id AND ".
			  "       m_resource_status.rowid = m_resource.status_id AND ".
			  "       m_group_type.rowid  = m_resource.type_id AND ".
			  "       m_lang.rowid = m_resource.lang_id AND ".
			  "       m_theme.rowid = m_resource.theme_id";

	my $resource = SelectSingle($dbh, $sql);

	if(! defined $resource) {
		throw Mioga2::Exception::Resource("Mioga2::Old::Resource::InitializeFromIdent",
        __x("Resource with ident {ident} not found", ident => $ident));
	}

	$self->{values} = $resource;
}




# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LDAP Mioga2::Old::Group Mioga2::Old::User

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
