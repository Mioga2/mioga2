# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
GroupListBase.pm: object used to manipulation list of groups and list of users
                   it implements common features of GroupList

=head1 DESCRIPTION

This module permits to manipulate list of groups

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
package Mioga2::Old::GroupListBase;
use strict;

use Locale::TextDomain::UTF8 'grouplistbase';

use Error qw(:try);
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::Old::UserList;


my $debug=0;



# ============================================================================

=head2 new ($config, %params)

	Create a new GroupList object.

	There are two creation methods :

=over 4

=item create from nothing (empty list)
	
	new Mioga2::Old::GroupList ($config)

=item create from the rowid
	
	new Mioga2::Old::GroupList ($config, rowid_list => $rowid)

=item create from group ident

	new Mioga2::Old::GroupList ($config, ident_list => $group_ident)

=back

=cut

# ============================================================================

sub new {
	my ($class, $config, %param) = @_;

	my $self = { };

	if (!defined($config)) {
		throw Mioga2::Exception::Simple("Mioga2::Old::GroupListBase::new", __"Missing parameters.");
	}

	$self->{config} = $config;

	bless($self, $class);


	if(exists $param{ident_list}) {
		$self->InitializeFromIdent($param{ident_list})
	}

	elsif(exists $param{rowid_list}) {
		$self->InitializeFromRowid($param{rowid_list})
	}

	else {
		# nothing to do
		$self->{list} = [];
	}
	
	return $self;
}

# ============================================================================

=head2 GetUsers ()

	Return a Mioga2::Old::UserList containing users
	member of groups in the current list.

	Does not contains user invited via teams

=cut

# ============================================================================

sub GetUsers  {
	my ($self) = @_;

	my $user_list = new Mioga2::Old::UserList($self->{config});

	foreach my $group_rowid (@{$self->{list}})
	{
		$user_list->AddUsersMemberOfGroup($group_rowid);
	}

	return $user_list;
}

# ============================================================================

=head2 GetExpandedUsers ()


	Return a Mioga2::Old::UserList containing users
	member of groups in the current list.

	Contains user invited via teams

=cut

# ============================================================================

sub GetExpandedUsers  {
	my ($self) = @_;

	my $user_list = new Mioga2::Old::UserList($self->{config});

	my  @group_rowids = @{$self->{list}};

	foreach my $group_rowid (@group_rowids)
	{
		$user_list->AddExpandedUsersMemberOfGroup($group_rowid);
	}

	return $user_list;
}

# ============================================================================

=head2 GetRowids ()

	return list of rowids for groups in object GroupList

=cut

# ============================================================================

sub GetRowids  {
	my ($self) = @_;

	return $self->{list};

}

# ============================================================================

=head2 GetIdents ()

	virtual method : must be implemented in GroupList an UserList

=cut

# ============================================================================

sub GetIdents  {
	# virtual method
}

# ============================================================================

=head2 GetTypes ()

	return list of types for groups in object GroupList

=cut

# ============================================================================

sub GetTypes  {
	my ($self) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my $sql = "select distinct m_group_base.rowid, m_group_type.ident from m_group_base, m_group_type where m_group_base.rowid in ( ";
	$sql .= join (',',@{$self->{list}});
	$sql .= " ) and m_group_base.type_id = m_group_type.rowid and mioga_id = $mioga_id";

	my $type_list = SelectMultiple($dbh, $sql);

	if (!defined($type_list))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::GroupListBase::GetTypes", $DBI::err,  $DBI::errstr, $sql);
	}

	my $rowid_to_type;
	foreach my $val (@$type_list)
	{
		$rowid_to_type->{$val->{rowid}} = $val->{ident};
	}

	my @types;
	foreach my $rowid (@{$self->{list}})
	{
		push (@types, $rowid_to_type->{$rowid});
	}

	return \@types;
}



# ============================================================================

=head2 GetPublicParts ()

	return list of public_parts (boolean) for groups in object GroupList

=cut

# ============================================================================

sub GetPublicParts  {
	my ($self) = @_;

	return $self->GetAttributeList("public_part");

}

# ============================================================================

=head2 GetAutonomous ()

	return list of autonomous (boolean) for groups in object GroupList

=cut

# ============================================================================

sub GetAutonomous  {
	my ($self) = @_;

	return $self->GetAttributeList("autonomous");

}

# ============================================================================

=head2 GetThemes()

	return list of themes ident for groups in object GroupList

=cut

# ============================================================================

sub GetThemes{
	my ($self) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my $sql = "select distinct m_group_base.rowid, m_theme.ident from m_group_base, m_theme where m_group_base.rowid in ( ";
	$sql .= join (',',@{$self->{list}});
	$sql .= " ) and m_theme.rowid = m_group_base.theme_id";

	my $theme_list = SelectMultiple($dbh, $sql);

	if (!defined($theme_list))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::GroupListBase::GetThemes", $DBI::err,  $DBI::errstr, $sql);
	}

	my $rowid_to_theme;
	foreach my $val (@$theme_list)
	{
		$rowid_to_theme->{$val->{rowid}} = $val->{ident};
	}

	my @themes;
	foreach my $rowid (@{$self->{list}})
	{
		push (@themes, $rowid_to_theme->{$rowid});
	}

	return \@themes;
}

# ============================================================================

=head2 GetAnimIds ()

	return list of animator ids for groups in object GroupList

=cut

# ============================================================================

sub GetAnimIds  {
	my ($self) = @_;

	return $self->GetAttributeList("anim_id");

}

# ============================================================================

=head2 GetDefaultProfileIds ()

	return list of default profile rowids for groups in object GroupList

=cut

# ============================================================================

sub GetDefaultProfileIds  {
	my ($self) = @_;

	return $self->GetAttributeList("default_profile_id");

}

# ============================================================================

=head2 GetCreatorIds ()

	return list of creator rowids for groups in object GroupList

=cut

# ============================================================================

sub GetCreatorIds  {
	my ($self) = @_;

	return $self->GetAttributeList("creator_id");

}

# ============================================================================

=head2 GetLangs ()

	return list of lang ident for groups in object GroupList

=cut

# ============================================================================

sub GetLangs  {
	my ($self) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my $sql = "select distinct m_group_base.rowid, m_lang.ident from m_group_base, m_lang where m_group_base.rowid in ( ";
	$sql .= join (',',@{$self->{list}});
	$sql .= " ) and m_group_base.lang_id = m_lang.rowid";

	my $lang_list = SelectMultiple($dbh, $sql);

	if (!defined($lang_list))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::GroupListBase::GetLangs", $DBI::err,  $DBI::errstr, $sql);
	}

	my $rowid_to_lang;
	foreach my $val (@$lang_list)
	{
		$rowid_to_lang->{$val->{rowid}} = $val->{ident};
	}

	my @langs;
	foreach my $rowid (@{$self->{list}})
	{
		push (@langs, $rowid_to_lang->{$rowid});
	}

	return \@langs;
}

# ============================================================================

=head2 GetDiskSpaceUsed ()

	return list of disk_space_used for groups in object GroupList

=cut

# ============================================================================

sub GetDiskSpaceUsed  {
	my ($self) = @_;

	return $self->GetAttributeList("disk_space_used");

}


# ============================================================================

=head2 GetPublicPartGroups()

	return a Mioga2::Old::GroupList containing a subset of the current list
    containing the groups that have a pulic part.

=cut

# ============================================================================

sub GetPublicPartGroups{
	my ($self) = @_;
	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();
	my %param;

	my $class = ref($self);

	my $sql = "select rowid from m_group_base where public_part = 't' and mioga_id = $mioga_id";
	my $public_parts = SelectMultiple ($dbh, $sql);

	if (!defined($public_parts))
	{
		 throw Mioga2::Exception::DB("Mioga2::Old::GroupListBase::GetPublicPartGroups", $DBI::err,  $DBI::errstr, $sql);
	}

	foreach my $public_part (@$public_parts)
	{
		push (@{$param{rowid_list}}, $public_part->{rowid});
	}

	my $new_public_part_group = $class->new($self->{config},%param);

	return $new_public_part_group;
}

# ============================================================================

=head2 GetAutonomousGroups()

	return a Mioga2::Old::GroupList containing a subset of the current list
    containing the groups that are autonomous

=cut

# ============================================================================

sub GetAutonomousGroups{
	my ($self) = @_;
	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();
	my %param;

	my $class = ref($self);

	my $sql = "select rowid from m_group_base where autonomous = 't' and mioga_id = $mioga_id";
	my $autonoms = SelectMultiple ($dbh, $sql);

	if (!defined($autonoms))
	{
		 throw Mioga2::Exception::DB("Mioga2::Old::GroupListBase::GetAutonomousGroups", $DBI::err,  $DBI::errstr, $sql);
	}
	
	foreach my $autonomous (@$autonoms)
	{
		push (@{$param{rowid_list}}, $autonomous->{rowid});
	}

	my $new_autonomous_group = $class->new($self->{config},%param);

	return $new_autonomous_group;
}

# ============================================================================

=head2 SendMail ($mime_entity)

	Virtual method must be implemented in GroupList object

=cut

# ============================================================================

sub SendMail  {
	# virtual method
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head2 InitializeFromRowid (@rowid_list)

	Initialize the GroupList object from list of group rowids.

=cut

# ============================================================================

sub InitializeFromRowid {
	my ($self, $rowid) = @_;
	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my $db_group;
	my @valid_rowid_list;
	my @uniq_group_id_list;
	my %seen = ();

	# testing unicity
	foreach my $group_id (@$rowid)
	{
		$seen{$group_id}++;
	}
	@uniq_group_id_list = keys %seen;

	foreach my $group_id (@uniq_group_id_list)
	{
		my $sql = "select * from m_group_base where rowid = $group_id and mioga_id = $mioga_id";
		$db_group = SelectSingle ($dbh, $sql);

		if (!defined($db_group))
		{
			throw Mioga2::Exception::DB("Mioga2::Old::GroupListBase::InitializeFromRowid", $DBI::err,  $DBI::errstr, $sql);
		}
		else
		{
			push (@valid_rowid_list, $group_id);
		}
	}

	$self->{list} = \@valid_rowid_list;

}

# ============================================================================

=head2 InitializeFromIdent (@ident_list)

	Virtual method must be implemented in GroupList and UserList object

=cut

# ============================================================================

sub InitializeFromIdent {
	#virtual method
}

# ============================================================================

=head2 GetAttributeList ($attribute)

	Returns list of attributes $attributes for groups in group list

=cut

# ============================================================================

sub GetAttributeList {
	my ($self, $attribute) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	#my @rowid = $self->{list};
	my @attributes;
	my $sql = "select $attribute from m_group_base where rowid in ( ";
	$sql .= join (',',@{$self->{list}});
	$sql .= " ) and mioga_id = $mioga_id";

	my $attribute_list = SelectMultiple ($dbh, $sql);

	if (! defined($attribute_list))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::GroupListBase::GetAttributeList", $DBI::err,  $DBI::errstr, $sql);
	}

	foreach my $att (@$attribute_list)
	{
		push (@attributes, $att->{$attribute});
	}

	return \@attributes;

}

# ============================================================================

=head2 GetAttributesForRowids ($request_result)

	Returns list of attributes $attributes according to list of couples (rowid, attr)

=cut

# ============================================================================

sub GetAttributesForRowids {
	my ($self, $attribute_list) = @_;

	my $rowid_to_res;

	foreach my $entry (@{$attribute_list})
	{
		$rowid_to_res->{$entry->{rowid}} = $entry->{attr};
	}

	my @attr_list;

	foreach my $rowid (@{$self->{list}})
	{
		push (@attr_list, $rowid_to_res->{$rowid});
	}

	return \@attr_list;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Old::GroupList Mioga2::Old::UserList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
