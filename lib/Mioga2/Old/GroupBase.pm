# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
GroupBase.pm: GroupBase virtual class

=head1 DESCRIPTION

This module permits to access to informations on group and group's members.

This is a virtual class.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Old::GroupBase;
use strict;

use Locale::TextDomain::UTF8 'groupbase';

use Error qw(:try);
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Mioga2::Exception::DB;
use Mioga2::Exception::Simple;
use Mioga2::Old::TeamList;
use Mioga2::Config;
use Mioga2::Old::UserList;
use Mioga2::Old::ResourceList;
use Mioga2::Old::User;
use Mioga2::Old::Group;
use Mioga2::Old::Team;
use Mioga2::Old::Resource;

my $debug=0;


# ============================================================================

=head2 CreateObject ($config, %params)

	Create a new User, Group, Team or Resource object.

	It s an object class.

	There are two creation methods :

=over 4

=item create from the rowid
	
	Mioga2::Old::GroupBase->CreateObject ($config, rowid => $rowid)

=item create from group/user/Team/resource ident

	Mioga2::Old::GroupBase->CreateObject ($config, ident => $group_ident)

=back


=cut

# ============================================================================

sub CreateObject {
	my ($class, $config, %param) = @_;

	my $sql = "SELECT m_group.*, m_group_type.ident AS type FROM m_group, m_group_type WHERE ";
	if(exists $param{ident}) {
		$sql .= " m_group.ident = '".st_FormatPostgreSQLString($param{ident})."'";
	}
	elsif(exists $param{rowid}) {
		$sql .= " m_group.rowid = $param{rowid}";
	}

    $sql .= " AND m_group_type.rowid = m_group.type_id and mioga_id =" . $config->GetMiogaId();

	my $res = SelectSingle($config->GetDBH(), $sql);

	if(!defined $res) {
		return new Mioga2::Old::User($config, %param);
	}


	if($res->{type} eq 'group' ) {
		return new Mioga2::Old::Group($config, %param);
	}
	elsif($res->{type} eq 'resource') {
		return new Mioga2::Old::Resource($config, %param);
	}
	elsif($res->{type} eq 'team') {
		return new Mioga2::Old::Team($config, %param);
	}
	else {
		return new Mioga2::Old::User($config, %param);		
	}
}



# ============================================================================

=head2 new ($config, %params)

	Create a new Group object.

	There are two creation methods :

=over 4

=item create from the rowid
	
	new Mioga2::Old::Group ($config, rowid => $rowid)

=item create from group ident

	new Mioga2::Old::Group ($config, ident => $group_ident)

=back

=cut

# ============================================================================

sub new {
	my ($class, $config, %param) = @_;

	my $self = { };

	if(exists $param{type}) {
		$self->{type} = $param{type};
	}

	if (!defined($config)) {
		Mioga2::Exception::Simple->throw("Mioga2::Old::Group::new", __"Missing parameters.");
	}

	$self->{config} = $config;
	$self->{mioga_id} = $config->GetMiogaId();

	bless($self, $class);

	if(exists $param{ident}) {
		$self->InitializeFromIdent($param{ident})
	}

	elsif(exists $param{rowid}) {
		$self->InitializeFromRowid($param{rowid})
	}

	else {
		Mioga2::Exception::Simple->throw("Mioga2::Old::Group::new", __"Missing parameters.");
	}
	
	return $self;
}



# ============================================================================

=head2 GetInvitedTeams()

	Return a TeamList object representing teams invited in this group.

=cut

# ============================================================================

sub GetInvitedTeams {
	my ($self) = @_;

	my $grp_list = new Mioga2::Old::TeamList($self->{config});
	$grp_list->AddTeamsMemberOfGroup($self->{values}->{rowid});

	return $grp_list;
}



# ============================================================================

=head2 GetInvitedUsers ()

	Return a UserList object representing users invited in this group.
	(but not user invited via team, see ExpandedUsers)

=cut

# ============================================================================

sub GetInvitedUsers {
	my ($self) = @_;

	my $user_list = new Mioga2::Old::UserList($self->{config});
 	$user_list->AddUsersMemberOfGroup($self->{values}->{rowid});

	return $user_list;
}



# ============================================================================

=head2 GetGroupMembersNotNamlyInvited ($group_id)

	Return a UserList object representing group members not namly invited 
	in this group (invited via teams).

=cut

# ============================================================================

sub GetGroupMembersNotNamlyInvited {
	my ($self, $group_id) = @_;

	my $group = Mioga2::Old::GroupBase->CreateObject($self->{config}, rowid => $group_id);

	my $user_ids = $group->GetInvitedUsers()->GetRowids();
	my $group_user_ids = $self->GetInvitedUsers()->GetRowids();

	my @rowids;

	foreach my $rowid (@$user_ids) {
		if(! grep {$_ == $rowid} @$group_user_ids) {
			push @rowids, $rowid;
		}
	}

	my $user_list = new Mioga2::Old::UserList($self->{config}, rowid_list => \@rowids);
	return $user_list;
}


# ============================================================================

=head2 GetExpandedUsers ()

	Return a UserList object representing users invited in this group.
	This function also returns user invited in teams invited in this group.

=cut

# ============================================================================

sub GetExpandedUsers {
	my ($self) = @_;

	my $user_list = new Mioga2::Old::UserList($self->{config});
	$user_list->AddExpandedUsersMemberOfGroup($self->{values}->{rowid});

	return $user_list;
}



# ============================================================================

=head2 GetInvitedResources ()

	Return a ResourceList object representing resources invited in this group.

=cut

# ============================================================================

sub GetInvitedResources {
	my ($self) = @_;

	my $resource_list = new Mioga2::Old::ResourceList($self->{config});
 	$resource_list->AddResourcesMemberOfGroup($self->{values}->{rowid});

	return $resource_list;
}



# ============================================================================

=head2 GetAnimator ()

	Return a User object representing the group animator.

=cut

# ============================================================================

sub GetAnimator {
	my ($self) = @_;

	my $animator = new Mioga2::Old::User($self->{config}, rowid => $self->{values}->{anim_id});

	return $animator;
}



# ============================================================================

=head2 GetAnimId ()

	Return the group animator rowid.

=cut

# ============================================================================

sub GetAnimId {
	my ($self) = @_;

	return $self->{values}->{anim_id};
}



# ============================================================================

=head2 GetCreator ()

	Return a User object representing the group creator (a Mioga2 administrator).

=cut

# ============================================================================

sub GetCreator {
	my ($self) = @_;

	my $creator = new Mioga2::Old::User($self->{config}, rowid => $self->{values}->{creator_id});

	return $creator;
}



# ============================================================================

=head2 SendMail (MIME::Entity)

	Send the message passed in parameters to all groups members.
	Only the TO field of MIME::Entity is modified.

=cut

# ============================================================================

sub SendMail {
	my ($self, $entity) = @_;

	my $user_list = $self->GetExpandedUsers();
	$user_list->SendMail($entity);
}



# ============================================================================

=head2 GetRowid ()

	Return the group rowid.

=cut

# ============================================================================

sub GetRowid {
	my ($self) = @_;

	return $self->{values}->{rowid};
}



# ============================================================================

=head2 GetIdent ()

	Return the group ident.

=cut

# ============================================================================

sub GetIdent {
	my ($self) = @_;

	return $self->{values}->{ident};
}



# ============================================================================

=head2 GetType ()

	Return the group type (group, user or resource).

=cut

# ============================================================================

sub GetType {
	my ($self) = @_;
	
	return $self->{values}->{type};
}



# ============================================================================

=head2 GetDiskSpaceUsed ()

	Return the disk space used by the group.

=cut

# ============================================================================

sub GetDiskSpaceUsed {
	my ($self) = @_;
	
	return $self->{values}->{disk_space_used};
}



# ============================================================================

=head2 IsUserInGroup ($user)
	
	return true if the given user is member of the current group

=cut

# ============================================================================

sub IsUserInGroup {
	my ($self, $user) = @_;
	
	my $res = SelectMultiple($self->{config}->GetDBH(),
						   "SELECT invited_group_id FROM m_group_group WHERE group_id = $self->{values}->{rowid} AND ".
						   "                                                 invited_group_id = ".$user->GetRowid().
						   " UNION ".
						   "SELECT invited_group_id FROM m_group_expanded_user WHERE group_id = $self->{values}->{rowid} AND ".
						   "                                                         invited_group_id = ".$user->GetRowid());

	if(scalar (@$res)) {
		return 1;
	}


	return 0;
}



# ============================================================================

=head2 HasPublicPart ()

	Return true if the group has a public webdav directory.

=cut

# ============================================================================

sub HasPublicPart {
	my ($self) = @_;
	
	return $self->{values}->{public_part};
}



# ============================================================================

=head2 IsAutonomous ()

	Return true if the group is autonomous (not yet implemented)

=cut

# ============================================================================

sub IsAutonomous {
	my ($self) = @_;
	
	return $self->{values}->{autonomous};
}

# ============================================================================

=head2 HasHistory ()

  Return true if the group is versionned

=cut

# ============================================================================

sub HasHistory {
  my ($self) = @_;
  
  return $self->{values}->{history};
}


# ============================================================================

=head2 GetTheme ()

	Return the group theme ident.

=cut

# ============================================================================

sub GetTheme {
	my ($self) = @_;
	
	return $self->{values}->{theme};
}



# ============================================================================

=head2 GetLang ()

	Return the group language ident.

=cut

# ============================================================================

sub GetLang {
	my ($self) = @_;

	if($self->{values}->{lang} ne '') {
		return $self->{values}->{lang};
	}
	else {
		return $self->{config}->GetDefaultLang();		
	}
}



# ============================================================================

=head2 GetDefaultProfileId ()

	Return the group default profile id.

=cut

# ============================================================================

sub GetDefaultProfileId {
	my ($self) = @_;
	
	return $self->{values}->{default_profile_id};
}



# ============================================================================

=head2 GetUserConnectedSince (int secs)

	Return a UserList of users connected since secs seconds.

=cut

# ============================================================================

sub GetUserConnectedSince
{
    my ($self, $secs) = @_;
    my $res = SelectMultiple($self->{config}->GetDBH(),
			     "SELECT user_id FROM m_group_user_last_conn WHERE group_id = $self->{values}->{rowid} AND ".
			     "last_connection >= '" . du_SecondToISO(time() - $secs) . "'");
    my @rowids = map {$_->{user_id}} @{$res};
    my $users = new Mioga2::Old::UserList($self->{config}, rowid_list => \@rowids);

    return $users;
}


# ============================================================================

=head2 GetXML ()

	Return XML string representing the current group_base instance.

=cut

# ============================================================================

sub GetXML {
	my ($self) = @_;

	my @args = keys %{$self->{values}};

	# remove password of xmlified values
	@args = grep { $_ ne 'password'} @args;

	my $xml = "<User>";
   	
	foreach my $arg (@args) {
		$xml .= qq|<$arg>|.st_FormatXMLString($self->{values}->{$arg})."</$arg>";
	}

	$xml .= "</User>";

	return $xml;
	
}


# ============================================================================

=head2 ReloadValues()

	Reload values from database.

=cut

# ============================================================================

sub ReloadValues {
	my ($self) = @_;

	$self->InitializeFromRowid($self->{values}->{rowid});
}




# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LDAP Mioga2::Old::User Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
