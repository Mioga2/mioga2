# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Instance.pm: The Mioga2 instance creation application.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Old::Instance;

use strict;
use vars qw(@ISA);
use Error qw(:try);
use Data::Dumper;
use POSIX qw(tmpnam);

use Mioga2::Config;
use Mioga2::Content::XSLT;
use Mioga2::SimpleLargeList;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIApplication;
use MiogaConf;

my $debug = 0;


sub new {
	my ($class) = @_;

	my $self = { };
	bless($self, $class);

	return $self;
}


# ============================================================================

=head1 DATABASE ACCESS METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head2 CheckIfExists ($config, $ident)

	Check if the given instance already exists.

=cut

# ============================================================================

sub CheckIfExists {
	my ($self, $config, $ident) = @_;

	my $dbh = $config->GetDBH();

	my $res = SelectSingle($dbh, "SELECT count(*) FROM m_mioga WHERE ident='$ident'");

	return $res->{count};
}

# ============================================================================

=head2 InstanceAddInstance ($config, $values)

	Create the new instance.

	BE CAREFULL !! : the config parameter is a Mioga2::MiogaConf !!!
=cut

# ============================================================================

sub InstanceAddInstance {
	throw Mioga2::Exception::Simple ('Mioga2::Old::Instance::InstanceAddInstance', "Deprecated module, please fix calling code");
}



# ============================================================================

=head2 InstanceModifyInstance ($config, $values)

	Modify an instance.
=cut

# ============================================================================

sub InstanceModifyInstance {
	throw Mioga2::Exception::Simple ('Mioga2::Old::Instance::InstanceModifyInstance', "Deprecated module, please fix calling code");
}


# ============================================================================

=head2 InstanceDeleteInstance ($miogaconf, $instance)

	DELETE an instance.

	BE CAREFULL !!! : the config parameter is a Mioga2::MiogaConf !!!
	BE CAREFULL !!! ALL DELETION ARE IRREVERSIBLE !!!
=cut

# ============================================================================

sub InstanceDeleteInstance {
	throw Mioga2::Exception::Simple ('Mioga2::Old::Instance::InstanceDeleteInstance', "Deprecated module, please fix calling code");
}



# ============================================================================

=head2 CreateMiogaAdmin ($config, $values)

	Create instance administrator

=cut

# ============================================================================

sub CreateMiogaAdmin {
	my ($self, $config, $values) = @_;
	my $admin;

	my $dbh = $config->GetDBH();

	my $localuser = {ident => $values->{admin_ident}, 
					 creator_id => $values->{creator_id},
					 firstname => $values->{admin_fn},
					 lastname  => $values->{admin_ln},
					 email  => $values->{admin_email},
				    };

	if(exists $values->{admin_password}) {
		$localuser->{password} = $values->{admin_password};
	}
	else {
		$localuser->{password} = $values->{admin_ident};
	}

	try {
		UserLocalCreate($config, $localuser);
	}
	
	otherwise {
		my $err = shift;
		
		# warn Dumper($err);
		# UserLocalCreate must fail on skeletons application because of partialy initialized config object
	};
	
	my $sql = "SELECT * FROM m_user_base WHERE ident='$values->{admin_ident}' AND mioga_id = ".$config->GetMiogaId();
	my $res = SelectSingle($dbh, $sql);

	$localuser = $res;

	$sql = "UPDATE m_mioga SET admin_id=$res->{rowid} WHERE rowid = ".$config->GetMiogaId();
	ExecSQL($dbh, $sql);

	#######################################
	# Allow all applications for admin    #
	#######################################
	$res = SelectMultiple($dbh, "SELECT m_application.* FROM m_application

                                 WHERE m_application.mioga_id = ".$config->GetMiogaId()." AND
			 			               m_application.all_groups = 'f'");
	foreach my $app (@$res) {
		ApplicationAllowForGroup($dbh, $app->{rowid}, $localuser->{rowid});
	}


	# Recreate the config to reload admin values.

	my $newconfig = new Mioga2::Config($config->{miogaconf}, $values->{ident});

	$self->ApplySkeletons($newconfig, $localuser);

	system("chown -R ".$config->GetApacheUser().":".$config->GetApacheGroup()." ".$config->GetInstallDir());
}

# ============================================================================

=head2 CreateWebTree ($config, $values)

	Create web tree.

=cut

# ============================================================================

sub CreateWebTree {
	my ($self, $config, $values) = @_;
	print STDERR "Instance::CreateWebTree\n" if ($debug);

	my $tgz = $config->{miogaconf}->{install_dir}."/conf/webtree.tgz";

	mkdir($config->GetInstallDir());
	my $res = system("tar xzf $tgz -C ".$config->GetInstallDir());
	print STDERR "Instance::CreateWebTree system return = $res\n" if ($debug);
}



# ============================================================================

=head2 ApplySkeletons ($config, $values)

	Apply skeletons.

=cut

# ============================================================================

sub ApplySkeletons {
	my ($self, $config, $values) = @_;

	my $tgz = $config->{miogaconf}->{install_dir}."/conf/skeletons.tgz";

	my $dir = $config->GetSkeletonDir();

	system("mkdir -p $dir");
	system("tar xzf $tgz -C $dir");

	AuthzUpdateDirRights($config, $dir);
	
	SkelApplyDefaultUser($config, $values);
	SkelApplyUser($config, $values, $config->GetSkeletonDir()."/user/admin-skel.xml");
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


