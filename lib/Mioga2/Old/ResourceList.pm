# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
ResourceList.pm: object used to manipulate list of Resources

=head1 DESCRIPTION

This module permits to manipulate list of resources

=head1 METHODS DESRIPTION

=cut

package Mioga2::Old::ResourceList;
use strict;

use base qw(Mioga2::Old::GroupList);
use Locale::TextDomain::UTF8 'grouplist';

use Error qw(:try);
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::Exception::Resource;

my $debug=0;




# ============================================================================

=head2 new ($config, %params)

	Create a new ResourceList object.

	There are two creation methods :

=over 4

=item create from nothing
	
	new Mioga2::Old::ResourceList ($config)

=item create from the rowid
	
	new Mioga2::Old::ResourceList ($config, rowid_list => $rowid)

=item create from resource ident

	new Mioga2::Old::ResourceList ($config, ident_list => $resource_ident)

=item create for all resource of mioga

	new Mioga2::Old::ResourceList ($config, ident_list => $resource_ident)

=back

=cut

# ============================================================================

sub new {
	my ($class, @params) = @_;

	my $self = $class->SUPER::new(@params, type => 'resource');

	return $self;
}


# ============================================================================

=head2 GetLocations ()

	return list of locations for resource in object ResourceList

=cut

# ============================================================================

sub GetLocations  {
	my ($self) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my @rowid = @{$self->{list}};
	my $sql = "select distinct rowid, location as attr from m_resource where rowid in ( ";
	$sql .= join (',',@rowid);
	$sql .= " ) and mioga_id = $mioga_id";

	my $attribute_list = SelectMultiple ($dbh, $sql);

	if (! defined($attribute_list))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::ResourceList::GetLocations", $DBI::err,  $DBI::errstr, $sql);
	}

	my $locations = $self->GetAttributesForRowids($attribute_list);

	return $locations;
}

# ============================================================================

=head2 GetTimePerDays ()

	return list of time per day for resources in object ResourceList

=cut

# ============================================================================

sub GetTimePerDays  {
	my ($self) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my @rowid = @{$self->{list}};
	my $sql = "select distinct rowid, time_per_day as attr from m_resource where rowid in ( ";
	$sql .= join (',',@rowid);
	$sql .= " ) and mioga_id = $mioga_id";

	my $attribute_list = SelectMultiple ($dbh, $sql);

	if (! defined($attribute_list))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::ResourceList::GetTimePerDays", $DBI::err,  $DBI::errstr, $sql);
	}

	my $time_per_days = $self->GetAttributesForRowids($attribute_list);

	return $time_per_days;
}

# ============================================================================

=head2 GetAvailableDays ()

	return list of available days for resources in object ResourceList

=cut

# ============================================================================

sub GetAvailableDays  {
	my ($self) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my @rowid = @{$self->{list}};
	my $sql = "select distinct rowid, working_days as attr from m_resource where rowid in ( ";
	$sql .= join (',',@rowid);
	$sql .= " ) and mioga_id = $mioga_id";

	my $attribute_list = SelectMultiple ($dbh, $sql);

	if (! defined($attribute_list))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::ResourceList::GetTimePerDays", $DBI::err,  $DBI::errstr, $sql);
	}

	my $available_days = $self->GetAttributesForRowids($attribute_list);

	return $available_days;
}


# ============================================================================

=head2 GetTimezoneIds ()

	return list of timezone_id for users in object UserList

=cut

# ============================================================================

sub GetTimezoneIds  {
	my ($self) = @_;

	return $self->GetAttributeList("timezone_id");

}

# ============================================================================

=head2 GetTimezones ()

	return list of timezone for users in object UserList

=cut

# ============================================================================

sub GetTimezones  {
	my ($self) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $tzi = SelectMultiple($self->{config}->GetDBH(), 
							 "SELECT m_resource.rowid, m_timezone.ident as timezone ".
							 "FROM m_timezone, m_resource ".
							 "WHERE m_resource.rowid IN (". join(',', @{$self->{list}}).") AND m_timezone.rowid = m_resource.timezone_id");
	my %tz = map { ($_->{rowid}, $_->{timezone})} @$tzi;

	my @res;

	foreach my $rowid (@{$self->{list}}) {
		if(exists $tz{$rowid}) {
			push @res, $tz{$rowid};
		}
		else {
			push @res, $self->{config}->GetDefaultTimezone();
		}
	}

	return \@res;

}

# ============================================================================

=head2 AddResourcesAnimatedBy ($user_rowid)

	Add resources rowid to list of resources rowid for resources animated by $user_rowid

=cut

# ============================================================================

sub AddResourcesAnimatedBy  {
	my ($self, $user_rowid) = @_;
	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my @uniq_resource_ident_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select m_resource.* from m_resource, m_resource_status where anim_id = $user_rowid and mioga_id = $mioga_id and m_resource.status_id = m_resource_status.rowid and m_resource_status.ident = 'active'";
	my $animated_resources = SelectMultiple($dbh, $sql);

	if (! defined ($animated_resources))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::ResourceList::AddResourcesAnimatedBy", $DBI::err,  $DBI::errstr, $sql);
	}

	foreach my $resource (@$animated_resources)
	{
		push (@rowid, $resource->{rowid});
	}

	# testing unicity
	foreach my $resource_rowid (@rowid)
	{
		$seen{$resource_rowid}++;
	}
	@uniq_resource_ident_list = keys %seen;

	$self->{list} = \@uniq_resource_ident_list;

}


# ============================================================================

=head2 AddResourcesMemberOfGroup ($user_rowid)

	Add resources rowid to list of resources rowid for resources animated by $user_rowid

=cut

# ============================================================================

sub AddResourcesMemberOfGroup  {
	my ($self, $group_rowid) = @_;
	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my @uniq_resource_ident_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select m_resource.* from m_resource, m_resource_status, m_group_group where m_group_group.invited_group_id = $group_rowid AND m_resource.rowid = m_group_group.group_id AND m_resource.mioga_id = $mioga_id and m_resource.status_id = m_resource_status.rowid and m_resource_status.ident = 'active'";
	my $animated_resources = SelectMultiple($dbh, $sql);

	if (! defined ($animated_resources))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::ResourceList::AddResourcesMemberOfGroup", $DBI::err,  $DBI::errstr, $sql);
	}

	foreach my $resource (@$animated_resources)
	{
		push (@rowid, $resource->{rowid});
	}

	# testing unicity
	foreach my $resource_rowid (@rowid)
	{
		$seen{$resource_rowid}++;
	}
	@uniq_resource_ident_list = keys %seen;

	$self->{list} = \@uniq_resource_ident_list;

}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::GroupListMioga2::Old::UserList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
