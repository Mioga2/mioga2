#===============================================================================
#
#         FILE:  Compatibility.pm
#
#  DESCRIPTION:  Compatibility layer between Mioga2::Old::* and new Mioga2::*List objects
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  15/07/2010 14:43
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Compatibility.pm: Compatibility layer between Mioga2::Old::* and new Mioga2::*List objects

=head1 DESCRIPTION

This module provides compatibility methods to Mioga2::Old modules for new Mioga2::*List modules.

It is intented to give a smooth transition from Mioga2-2.2 to Mioga2-2.3 and
will be removed as soon as no Mioga2-2.2 objects are no longer used.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Old::Compatibility;

use Data::Dumper;

use Mioga2::tools::database;

my $debug = 0;



#===============================================================================

=head2 GetType

Get object type.

=cut

#===============================================================================
sub GetType {
	my ($self) = @_;

	my $type;

	for (ref ($self)) {
		if (/Mioga2::GroupList/) {
			$type = 'group';
		}
		elsif (/Mioga2::UserList/) {
			$type = 'user';
		}
		elsif (/Mioga2::ResourceList/) {
			$type = 'resource';
		}
		else {
			throw Mioga2::Exception::Simple ('Mioga2::Old::Compatibility::GetType', "Unhandled object type: " . ref ($self));
		}
	}

	return ($type);
}	# ----------  end of subroutine GetType  ----------


#===============================================================================

=head2 GetIdent

Get 'ident' property.

=cut

#===============================================================================
sub GetIdent {
	my ($self) = @_;

	return ($self->Get ('ident'));
}	# ----------  end of subroutine GetIdent  ----------


#===============================================================================

=head2 GetEmail

Get 'email' property.

=cut

#===============================================================================
sub GetEmail {
	my ($self) = @_;

	return ($self->Get ('email'));
}	# ----------  end of subroutine GetEmail  ----------


#===============================================================================

=head2 GetLang

Get 'lang' property.

=cut

#===============================================================================
sub GetLang {
	my ($self) = @_;

	return ($self->Get ('lang'));
}	# ----------  end of subroutine GetLang  ----------


#===============================================================================

=head2 GetTheme

Get 'theme' property.

=cut

#===============================================================================
sub GetTheme {
	my ($self) = @_;

	return ($self->Get ('theme_ident'));
}	# ----------  end of subroutine GetTheme  ----------


#===============================================================================

=head2 GetTimezone

Get 'timezone' property.

=cut

#===============================================================================
sub GetTimezone {
	my ($self) = @_;

	return ($self->Get ('timezone'));
}	# ----------  end of subroutine GetTimezone  ----------


#===============================================================================

=head2 IsAutonomous

Get 'autonomous' property.

=cut

#===============================================================================
sub IsAutonomous {
	my ($self) = @_;

	return ($self->Get ('autonomous'));
}	# ----------  end of subroutine IsAutonomous  ----------


#===============================================================================

=head2 GetName

Get user full name.

=cut

#===============================================================================
sub GetName {
	my ($self) = @_;

	return ($self->Get ('firstname') . ' ' . $self->Get ('lastname'));
}	# ----------  end of subroutine GetName  ----------


#===============================================================================

=head2 GetRowid

Get user rowid.

=cut

#===============================================================================
sub GetRowid {
	my ($self) = @_;

	return ($self->Get ('rowid'));
}	# ----------  end of subroutine GetRowid  ----------


#===============================================================================

=head2 GetFirstname

Get user firstname

=cut

#===============================================================================
sub GetFirstname {
	my ($self) = @_;

	return ($self->Get ('firstname'));
}	# ----------  end of subroutine GetFirstname  ----------


#===============================================================================

=head2 GetLastname

Get user lastname

=cut

#===============================================================================
sub GetLastname {
	my ($self) = @_;

	return ($self->Get ('lastname'));
}	# ----------  end of subroutine GetLastname  ----------


#===============================================================================

=head2 GetHomeURL

Get user home URL

=cut

#===============================================================================
sub GetHomeURL {
	my ($self) = @_;

	my $homeurl = undef;

	if ($self->Count == 1) {
		# Only get Home URL for single user. It is a non-sense for multiple users list. Anyway, it is only used in single-user context
		my $res = SelectSingle($self->{config}->GetDBH(), 
							   "SELECT home_url FROM m_home_url ".
							   "WHERE user_id = " . $self->Get ('rowid'));
		if ($res) {
			$homeurl = $res->{home_url};
		}
	}

	return ($homeurl);
}	# ----------  end of subroutine GetHomeURL  ----------


#===============================================================================

=head2 GetPassword

Get 'password' property

=cut

#===============================================================================
sub GetPassword {
	my ($self) = @_;

	return ($self->Get ('password'));
}	# ----------  end of subroutine GetPassword  ----------


1;

