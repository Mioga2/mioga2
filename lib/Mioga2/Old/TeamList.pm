# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
TeamList.pm: object used to maniulation list of groups

=head1 DESCRIPTION

This module permits to manipuate list of groups

=head1 METHODS DESRIPTION

=cut

package Mioga2::Old::TeamList;
use strict;

use base qw(Mioga2::Old::GroupList);
use Locale::TextDomain::UTF8 'teamlist';

use Error qw(:try);
use Data::Dumper;

use Mioga2::tools::database;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;

my $debug=0;



# ============================================================================

=head2 new ($config, %params)

	Create a new TeamList object.

	There are four creation methods :

=over 4

=item create from nothing
	
	new Mioga2::Old::TeamList ($config)

=item create from the rowid
	
	new Mioga2::Old::TeamList ($config, rowid_list => $rowid)

=item create from team ident

	new Mioga2::Old::TeamList ($config, ident_list => $team_ident)

=item create for all team of mioga

	new Mioga2::Old::TeamList ($config, ident_list => $team_ident)

=back

=cut

# ============================================================================

sub new {
	my ($class, @params) = @_;

	my $self = $class->SUPER::new(@params, type => 'team');

	return $self;
}

# ============================================================================

=head2 AddTeamsContainingUser ($user_rowid)

	Add teams containing user with rowid $user_rowid to the current list.

=cut

# ============================================================================

sub AddTeamsContainingUser  {
	my ($self, $user_rowid) = @_;

	$self->AddGroupsContainingUser($user_rowid);
}

# ============================================================================

=head2 AddTeamsMemberOfGroup ($group_rowid)

	Add teams invited iin group with rowid $group_rowid to the current list.

=cut

# ============================================================================

sub AddTeamsMemberOfGroup  {
	my ($self, $group_rowid) = @_;

	my $dbh = $self->{config}->GetDBH();

	my @uniq_group_ident_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select m_group_base.rowid from m_group_invited_group, m_group_base, m_group_type where group_id = $group_rowid and m_group_base.rowid = invited_group_id and type_id = m_group_type.rowid and m_group_type.ident = '$self->{type}'";
	my $invited_users = SelectMultiple($dbh, $sql);

	if (!defined ($invited_users))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::TeamList::AddTeamsMemberOfTeam", $DBI::err,  $DBI::errstr, $sql);
	}

	foreach my $group (@$invited_users)
	{
		push (@rowid, $group->{rowid});
	}

	# testing unicity
	foreach my $group_rowid (@rowid)
	{
		$seen{$group_rowid}++;
	}
	@uniq_group_ident_list = keys %seen;

	$self->{list} = \@uniq_group_ident_list;
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Old::GroupListBase Mioga2::Old::UserList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
