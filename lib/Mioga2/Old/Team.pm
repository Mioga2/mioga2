# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Team.pm: Team access class

=head1 DESCRIPTION

This module permits to access to informations on team and team's members.

=head1 METHODS DESRIPTION

=cut
package Mioga2::Old::Team;
use strict;

use base qw(Mioga2::Old::Group);
use Locale::TextDomain::UTF8 'team';

use Error qw(:try);
use Data::Dumper;
use Mioga2::Old::User;
use Mioga2::tools::database;

my $debug=0;



# ============================================================================

=head2 new ($config, %params)

	Create a new Team object.

	There are two creation methods :

=over 4

=item create from the rowid
	
	new Mioga2::Old::Team ($config, rowid => $rowid)

=item create from team ident

	new Mioga2::Old::Team ($config, ident => $team_ident)

=back

=cut

# ============================================================================

sub new {
	my ($class, @params) = @_;

	my $self = $class->SUPER::new(@params, type => 'team');

	return $self;
}


# ============================================================================

=head2 GetGroups ()

	Return a GroupList object representing groups where the team is invited.

=cut

# ============================================================================

sub GetGroups {
	my ($self) = @_;

	my $sql = "SELECT m_group.rowid FROM m_group, m_group_group ".
		      "WHERE  m_group_group.invited_group_id = $self->{values}->{rowid} AND ".
			  "       m_group.rowid = m_group_group.group_id ";

	my $res = SelectMultiple($self->{config}->GetDBH(), $sql);

	my @rowids = map {$_->{rowid}} @$res;

	my $group_list = new Mioga2::Old::GroupList($self->{config}, rowid_list => \@rowids);
	return $group_list;

}


# ============================================================================

=head2 GetUsers ()

	Return a UserList object representing users where the team is invited.

=cut

# ============================================================================

sub GetUsers {
	my ($self) = @_;

	my $sql = "SELECT m_user_base.rowid FROM m_user_base, m_group_group ".
		      "WHERE  m_group_group.invited_group_id = $self->{values}->{rowid} AND ".
			  "       m_user_base.rowid = m_group_group.group_id ";

	my $res = SelectMultiple($self->{config}->GetDBH(), $sql);

	my @rowids = map {$_->{rowid}} @$res;

	my $user_list = new Mioga2::Old::UserList($self->{config}, rowid_list => \@rowids);
	return $user_list;

}

# ============================================================================

=head2 GetGroupsWithUserNotNamlyInvited ($user_id)

	Return a GroupList object representing groups where the team is invited and where the user is
	not namly invited in these group. (invited via teams)

=cut

# ============================================================================

sub GetGroupsWithUserNotNamlyInvited {
	my ($self, $user_id) = @_;

	my $user = new Mioga2::Old::User($self->{config}, rowid => $user_id);
	my $groups = $user->GetGroups();
	my $group_ids = $groups->GetRowids();

	my $sql = "SELECT DISTINCT m_group.rowid FROM m_group, m_group_type, m_group_group ".
		      "WHERE m_group_group.invited_group_id = $self->{values}->{rowid} AND ".
			  "      m_group.rowid = m_group_group.group_id ";

	if(@$group_ids) {
		$sql .= " AND m_group_group.group_id NOT IN ( ".join(',', @$group_ids).")";
	}

	my $res = SelectMultiple($self->{config}->GetDBH(), $sql);

	my @rowids = map {$_->{rowid}} @$res;

	my $group_list = new Mioga2::Old::GroupList($self->{config}, rowid_list => \@rowids);
	return $group_list;
}

# ============================================================================

=head2 GetUsersWithUserNotNamlyInvited ($user_id)

	Return a UserList object representing users where the team is invited and where the user is
	not namly invited in these user. (invited via teams)

=cut

# ============================================================================

sub GetUsersWithUserNotNamlyInvited {
	my ($self, $user_id) = @_;

	my $user = new Mioga2::Old::User($self->{config}, rowid => $user_id);
	my $users = $user->GetUsers();
	my $user_ids = $users->GetRowids();

	my $sql = "SELECT DISTINCT m_user_base.rowid FROM m_user_base, m_group_group ".
		      "WHERE m_group_group.invited_group_id = $self->{values}->{rowid} AND ".
			  "      m_user_base.rowid = m_group_group.group_id ";

	if(@$user_ids) {
		$sql .= " AND m_group_group.group_id NOT IN ( ".join(',', @$user_ids).")";
	}

	my $res = SelectMultiple($self->{config}->GetDBH(), $sql);

	my @rowids = map {$_->{rowid}} @$res;

	my $user_list = new Mioga2::Old::UserList($self->{config}, rowid_list => \@rowids);
	return $user_list;
}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LDAP Mioga2::Old::User Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
