# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
UserList.pm: object used to manipulate list of users

=head1 DESCRIPTION

This module permits to manipulate list of users

=head1 METHODS DESRIPTION

=cut
package Mioga2::Old::UserList;
use strict;

use base qw(Mioga2::Old::GroupListBase);
use Locale::TextDomain::UTF8 'userlist';

use Error qw(:try);
use Data::Dumper;
use Mioga2::LDAP;
use Mioga2::tools::database;
use Mioga2::tools::APIGroup;
use Mioga2::tools::date_utils;
use Mioga2::tools::mail_utils;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::Exception::User;
use Time::HiRes qw(gettimeofday tv_interval);

my $debug=0;

# ============================================================================

=head2 new ($config, %params)

	Create a new UserList object.

	There are two creation methods :

=over 4

=item create from nothing
	
	new Mioga2::Old::UserList ($config)

=item create from the rowid
	
	new Mioga2::Old::UserList ($config, rowid_list => $rowid)

=item create from group ident

	new Mioga2::Old::UserList ($config, ident_list => $group_ident)

=item create object containing all users

	new Mioga2::Old::UserList ($config, all => 1)

=back

=cut

# ============================================================================

sub new {
	my ($class, $config, %param) = @_;

	my $self = { };

	if (!defined($config)) {
		throw Mioga2::Exception::Simple("Mioga2::Old::UserList::new", __"Missing parameters.");
	}

	$self->{config} = $config;

	bless($self, $class);


	if(exists $param{ident_list}) {
		$self->InitializeFromIdent($param{ident_list})
	}

	elsif(exists $param{rowid_list}) {
		$self->InitializeFromRowid($param{rowid_list})
	}

	elsif(exists $param{all}) {
		$self->InitializeFromAll();
	}

	else {
		# nothing to do
		$self->{list} = [];
	}
	
	return $self;
}

# ============================================================================

=head2 AddUsersMemberOfGroup ($group_rowid)

	Add users member of group with rowid $group_rowid to the current list.

	Don t add user invited via teams.

=cut

# ============================================================================

sub AddUsersMemberOfGroup  {
	my ($self, $group_rowid) = @_;
	my $dbh = $self->{config}->GetDBH();

	my @uniq_user_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select m_group_invited_user.invited_group_id from m_group_invited_user where group_id = $group_rowid";
	my $users = SelectMultiple($dbh, $sql);

	if (! defined ($users))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::UserList::AddUsersMemberOfGroup", $DBI::err,  $DBI::errstr, $sql);
	}

	foreach my $user (@$users)
	{
		push (@rowid, $user->{invited_group_id});
	}

	# testing unicity
	foreach my $user_rowid (@rowid)
	{
		$seen{$user_rowid}++;
	}
	@uniq_user_list = keys %seen;

	$self->{list} = \@uniq_user_list;

}



# ============================================================================

=head2 AddUsersMemberOfTeam ($team_rowid)

	Add users member of team with rowid $team_rowid to the current list.

	Don t add user invited via teams.

=cut

# ============================================================================

sub AddUsersMemberOfTeam  {
	my ($self, $team_rowid) = @_;

	$self->AddUsersMemberOfGroup($team_rowid);
}

# ============================================================================

=head2 AddUsersContainingUser ($user_rowid)

	Add users invited in personal space of user with rowid $user_rowid to the current list.

	Don t add user invited via teams.

=cut

# ============================================================================

sub AddUsersContainingUser  {
	my ($self, $user_rowid) = @_;
	my $dbh = $self->{config}->GetDBH();

	my @uniq_user_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select m_group_invited_user.group_id from m_group_invited_user, m_user_base ".
		      "where m_group_invited_user.invited_group_id = $user_rowid and ".
			  "      m_group_invited_user.group_id = m_user_base.rowid";

	my $users = SelectMultiple($dbh, $sql);

	if (! defined ($users))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::UserList::AddUsersContainingUser", $DBI::err,  $DBI::errstr, $sql);
	}


	push @rowid, map {$_->{group_id}} @$users;

	# testing unicity
	foreach my $user_rowid (@rowid)
	{
		$seen{$user_rowid}++;
	}
	@uniq_user_list = keys %seen;

	$self->{list} = \@uniq_user_list;

}

# ============================================================================

=head2 AddUsersContainingUserWithFunction ($user_rowid, $app, $func)

	Add users invited in personal space of user with rowid $user_rowid and 
	authorized to execute function func in application $app to the current list.

	Don t add user invited via teams.

=cut

# ============================================================================

sub AddUsersContainingUserWithFunction  {
	my ($self, $user_rowid, $app, $func) = @_;
	my $dbh = $self->{config}->GetDBH();

	my @uniq_user_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select m_profile_user_function.group_id from m_profile_user_function, m_user_base ".
		      "where m_profile_user_function.user_id = $user_rowid and ".
			  "      m_profile_user_function.group_id = m_user_base.rowid and ".
			  "      m_profile_user_function.app_ident = '$app' and ".
			  "      m_profile_user_function.function_ident = '$func' ";

	my $users = SelectMultiple($dbh, $sql);

	if (! defined ($users))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::UserList::AddUsersContainingUser", $DBI::err,  $DBI::errstr, $sql);
	}


	push @rowid, map {$_->{group_id}} @$users;

	# testing unicity
	foreach my $user_rowid (@rowid)
	{
		$seen{$user_rowid}++;
	}
	@uniq_user_list = keys %seen;

	$self->{list} = \@uniq_user_list;

}

# ============================================================================

=head2 AddExpandedUsersMemberOfGroup ($group_rowid)

	Add users member of group with rowid $group_rowid to the current list.

	Add user invited via teams.

=cut

# ============================================================================

sub AddExpandedUsersMemberOfGroup  {
	my ($self, $group_rowid) = @_;
	my $dbh = $self->{config}->GetDBH();

	$self->AddUsersMemberOfGroup($group_rowid);

	my @uniq_user_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select invited_group_id from m_group_expanded_user where group_id = $group_rowid";
	my $users = SelectMultiple($dbh, $sql);

	if (! defined ($users))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::UserList::AddExpandedUsersMemberOfGroup", $DBI::err,  $DBI::errstr, $sql);
	}

	foreach my $user (@$users)
	{
		push (@rowid, $user->{invited_group_id});
	}

	# testing unicity
	foreach my $user_rowid (@rowid)
	{
		$seen{$user_rowid}++;
	}
	@uniq_user_list = keys %seen;

	$self->{list} = \@uniq_user_list;

}


# ============================================================================

=head2 AddExpandedUsersMemberOfTeam ($team_rowid)

	Add users member of team with rowid $team_rowid to the current list.

	Add user invited via teams.

=cut

# ============================================================================

sub AddExpandedUsersMemberOfTeam  {
	my ($self, $team_rowid) = @_;

	$self->AddExpandedUsersMemberOfGroup($team_rowid);
}


# ============================================================================

=head2 AddExpandedUsersContainingUserWithFunction ($user_rowid)

	Add users invited in personal space of user with rowid $user_rowid and 
	authorized to execute function func in application $app to the current list.

	Add user invited via teams.

=cut

# ============================================================================

sub AddExpandedUsersContainingUserWithFunction  {
	my ($self, $user_rowid, $app, $func) = @_;
	my $dbh = $self->{config}->GetDBH();

	$self->AddUsersContainingUserWithFunction($user_rowid, $app, $func);
	
	my @uniq_user_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select m_profile_expanded_user_functio.group_id from m_profile_expanded_user_functio, m_user_base ".
		"where m_profile_expanded_user_functio.user_id = $user_rowid and ".
		"      m_profile_expanded_user_functio.group_id = m_user_base.rowid and ".
		"      m_profile_expanded_user_functio.app_ident = '$app' and ".
		"      m_profile_expanded_user_functio.function_ident = '$func' ";

	my $users = SelectMultiple($dbh, $sql);

	if (! defined ($users))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::UserList::AddUsersContainingUser", $DBI::err,  $DBI::errstr, $sql);
	}


	push @rowid, map {$_->{group_id}} @$users;

	# testing unicity
	foreach my $user_rowid (@rowid)
	{
		$seen{$user_rowid}++;
	}
	@uniq_user_list = keys %seen;

	$self->{list} = \@uniq_user_list;

}

# ============================================================================

=head2 AddExpandedUsersContainingUser ($user_rowid)

	Add users invited in personal space of user with rowid $user_rowid to the current list.

	Add user invited via teams.


=cut

# ============================================================================

sub AddExpandedUsersContainingUser  {
	my ($self, $user_rowid) = @_;

	$self->AddUsersContainingUser($user_rowid);
	
	my $dbh = $self->{config}->GetDBH();

	my @uniq_user_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select m_group_expanded_user.group_id from m_group_expanded_user, m_user_base ".
		      "where  m_group_expanded_user.invited_group_id = $user_rowid and ".
			  "       m_group_expanded_user.group_id = m_user_base.rowid";

	my $users = SelectMultiple($dbh, $sql);

	if (! defined ($users))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::UserList::AddUsersContainingUser", $DBI::err,  $DBI::errstr, $sql);
	}


	push @rowid, map {$_->{group_id}} @$users;

	# testing unicity
	foreach my $user_rowid (@rowid)
	{
		$seen{$user_rowid}++;
	}
	@uniq_user_list = keys %seen;

	$self->{list} = \@uniq_user_list;

}

# ============================================================================

=head2 GetTimezoneIds ()

	return list of timezone_id for users in object UserList

=cut

# ============================================================================

sub GetTimezoneIds  {
	my ($self) = @_;

	return $self->GetUserBaseAttributeList("timezone_id");

}

# ============================================================================

=head2 GetTimezones ()

	return list of timezone for users in object UserList

=cut

# ============================================================================

sub GetTimezones  {
	my ($self) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $tzi = SelectMultiple($self->{config}->GetDBH(), 
							 "SELECT m_user_base.rowid, m_timezone.ident as timezone ".
							 "FROM   m_timezone, m_user_base ".
							 "WHERE  m_user_base.rowid IN (". join(',', @{$self->{list}}).") AND m_timezone.rowid = m_user_base.timezone_id");
	my %tz = map { ($_->{rowid}, $_->{timezone})} @$tzi;

	my @res;

	foreach my $rowid (@{$self->{list}}) {
		if(exists $tz{$rowid}) {
			push @res, $tz{$rowid};
		}
		else {
			push @res, $self->{config}->GetDefaultTimezone();
		}
	}

	return \@res;

}

# ============================================================================

=head2 GetTimePerDays ()

	return list of time_per_day for users in object UserList

=cut

# ============================================================================

sub GetTimePerDays  {
	my ($self) = @_;

	return $self->GetUserBaseAttributeList("time_per_day");

}

# ============================================================================

=head2 GetWorkingDays ()

	return list of working_day for users in object UserList

=cut

# ============================================================================

sub GetWorkingDays  {
	my ($self) = @_;

	return $self->GetUserBaseAttributeList("working_days");

}

# ============================================================================

=head2 GetMondayFirst ()

	return list of monday_first for users in object UserList

=cut

# ============================================================================

sub GetMondayFirst  {
	my ($self) = @_;

	return $self->GetUserBaseAttributeList("monday_first");

}

# ============================================================================

=head2 GetStatus ()

	return list of statusfor users in object UserList

=cut

# ============================================================================

sub GetStatus{
	my ($self) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my $sql = "select distinct m_user_base.rowid, m_user_status.ident from m_user_base, m_user_status where m_user_base.rowid in ( ";
	$sql .= join (',',@{$self->{list}});
	$sql .= " ) and m_user_base.status_id = m_user_status.rowid mioga_id = $mioga_id";

	my $status_list = SelectMultiple($dbh, $sql);

	if (!defined($status_list))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::UserList", $DBI::err,  $DBI::errstr, $sql);
	}

	my $rowid_to_status;
	foreach my $val (@$status_list)
	{
		$rowid_to_status->{$val->{rowid}} = $val->{ident};
	}

	my @status;
	foreach my $rowid (@{$self->{list}})
	{
		push (@status, $rowid_to_status->{$rowid});
	}

	return \@status;
}

# ============================================================================

=head2 GetIdents ()

	return list of ident for users in object UserList

=cut

# ============================================================================

sub GetIdents  {
	my ($self) = @_;

	return $self->GetUserBaseAttributeList("ident");

}


# ============================================================================

=head2 GetNames ()

	return list of name for users in object UserList

=cut

# ============================================================================

sub GetNames {
	my ($self) = @_;

	my $fn = $self->GetUserBaseAttributeList("firstname");
	my $ln = $self->GetUserBaseAttributeList("lastname");

	my @name;

	my $nb_user = @$fn;
	for(my $i=0; $i<$nb_user; $i++) { 
		$name[$i] = "$fn->[$i] $ln->[$i]";
	}

	return \@name;
}


# ============================================================================

=head2 GetFirstnames ()

	return list of firstname for users in object UserList

=cut

# ============================================================================

sub GetFirstnames  {
	my ($self) = @_;

	return $self->GetUserBaseAttributeList("firstname");

}

# ============================================================================

=head2 GetLastnames ()

	return list of lastname for users in object UserList

=cut

# ============================================================================

sub GetLastnames  {
	my ($self) = @_;

	return $self->GetUserBaseAttributeList("lastname");

}

# ============================================================================

=head2 GetPasswords ()

	return list of password for users in object UserList

=cut

# ============================================================================

sub GetPasswords  {
	my ($self) = @_;

	return $self->GetUserBaseAttributeList("password");

}

# ============================================================================

=head2 GetEmails ()

	return list of email for users in object UserList

=cut

# ============================================================================

sub GetEmails  {
	my ($self) = @_;

	return $self->GetUserBaseAttributeList("email");

}

# ============================================================================

=head2 SendMail (MIME::Entity)

	Send the message passed in parameters to the users in UserList.

=cut

# ============================================================================

sub SendMail {
	my ($self, $entity) = @_;

	my $emails = $self->GetEmails();

	my $head = $entity->head();

	foreach my $email (@$emails)
	{
		if($head->count("To") > 0)
		{ 
			$head->replace("To" => $email);
		}
		else
		{
			$head->add("To" => $email);
		}

		mu_SendMail($self->{config}, $entity);
	}
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 InitializeFromRowid (@rowid_list)

	Initialize the UserList object from list of user rowids.

=cut

# ============================================================================

sub InitializeFromRowid {
	my ($self, $rowid) = @_;
	my $dbh = $self->{config}->GetDBH();

	my $db_user;
	my @valid_rowid_list;

	$self->{list} = $rowid;

}


# ============================================================================

=head2 InitializeFromAll ()

	Initialize the UserList object with evry users.

=cut

# ============================================================================

sub InitializeFromAll {
	my ($self, $rowid) = @_;
	my $dbh = $self->{config}->GetDBH();

	my $mioga_id = $self->{config}->GetMiogaId();

	my $res = SelectMultiple($dbh, "SELECT rowid FROM m_user_base WHERE mioga_id = $mioga_id");

	my @rowids = map {$_->{rowid}} @$res;

	$self->{list} = \@rowids;

}

# ============================================================================

=head2 InitializeFromIdent (@ident_list)

	Initialize the UserList object from list of user idents.

=cut

# ============================================================================

sub InitializeFromIdent {
	my ($self, $ident) = @_;
	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my $db_users;

	# Getting rowid from m_user_base table
	# -------------------------------------
	my $sql = "select distinct rowid, ident from m_user_base where ident in ('";
	$sql .=  join ("', '", @$ident);
	$sql .= "') and mioga_id = $mioga_id";
	
	$db_users = SelectMultiple($dbh, $sql);

	if (!defined ($db_users))
	{
			throw Mioga2::Exception::DB("Mioga2::Old::UserList::InitializeFromIdent", $DBI::err,  $DBI::errstr, $sql);
	}

	my @rowid_list = map {$_->{rowid}} @$db_users;
	$self->{list} = \@rowid_list;
}

# ============================================================================

=head2 GetUserBaseAttributeList ($attribute)

	Returns list of attribute $attributes for users in group list in the specific table m_user_base

=cut

# ============================================================================

sub GetUserBaseAttributeList {
	my ($self, $attribute) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my @rowid = @{$self->{list}};
	my $attributes;
	my $sql = "select rowid, $attribute as attr from m_user_base where rowid in ( ";
	$sql .= join (',',@rowid);
	$sql .= " ) and mioga_id = $mioga_id";

	my $attribute_list = SelectMultiple ($dbh, $sql);

	if (! defined($attribute_list))
	{
		throw Mioga2::Exception::DB("Mioga2::UserListBase::GetUserBaseAttributeList", $DBI::err,  $DBI::errstr, $sql);
	}
	$attributes = $self->GetAttributesForRowids($attribute_list);

	return $attributes;

}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LDAP Mioga2::Old::User Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
