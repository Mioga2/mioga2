# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Group.pm: Group access class

=head1 DESCRIPTION

This module permits to access to informations on group and group's members.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Old::Group;
use strict;

use base qw(Mioga2::Old::GroupBase);
use Locale::TextDomain::UTF8 'group';

use Error qw(:try);
use Data::Dumper;
use Mioga2::Old::User;
use Mioga2::Old::UserList;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;

my $debug=0;

# ============================================================================

=head2 new ($config, %params)

	Create a new Group object.

	There are two creation methods :

=over 4

=item create from the rowid
	
	new Mioga2::Old::Group ($config, rowid => $rowid)

=item create from group ident

	new Mioga2::Old::Group ($config, ident => $group_ident)

=back

=cut

# ============================================================================

sub new {
	my ($class, $config, %params) = @_;

	my $type = 'group';
	if(exists $params{type}) {
		$type = $params{type};
	}

	my $self = $class->SUPER::new($config, %params, type => $type);

	return $self;
}




# ============================================================================

=head2 GetDescription ()

	Return the group description.

=cut

# ============================================================================

sub GetDescription {
	my ($self) = @_;
	
	return $self->{values}->{description};
}



# ============================================================================

=head2 GetStatus ()

	Return the group status.

	Supported status are : active, deleted.

=cut

# ============================================================================

sub GetStatus {
	my ($self) = @_;
	
	return $self->{values}->{status};
}


#===============================================================================

=head2 GetAnimators

Get group animators list

=cut

#===============================================================================
sub GetAnimators {
	my ($self) = @_;

	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId ();
	my $group_ident = $self->GetIdent ();

	my $sql = "SELECT m_profile_user.group_id FROM m_profile_user, m_profile, m_group, m_profile_function, m_function, m_application, m_instance_application WHERE m_profile_user.profile_id = m_profile.rowid AND m_profile.group_id = m_group.rowid AND m_profile_function.profile_id = m_profile.rowid AND m_profile_function.function_id = m_function.rowid AND m_function.application_id = m_application.rowid AND m_instance_application.application_id = m_application.rowid AND m_application.ident = 'AnimGroup' AND m_function.ident = 'Animation' AND m_group.ident = '$group_ident' AND m_instance_application.mioga_id = $mioga_id AND m_group.mioga_id = $mioga_id";

	my $res = SelectMultiple ($dbh, $sql);

	my @rowids;
	for (@$res) {
		push (@rowids, $_->{group_id});
	}

	my $animators = Mioga2::Old::UserList->new ($self->{config}, rowid_list => \@rowids);

	return ($animators);
}	# ----------  end of subroutine GetAnimators  ----------


# ============================================================================

=head2 Methods Inherited from GroupBase

	See Mioga2::Old::GroupBase for documentation of methods inherited from the 
	GroupBase virtual class.

=cut

# ============================================================================



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head2 InitializeFromRowid ($rowid)

	Initialize the Group object from group rowid.

=cut

# ============================================================================

sub InitializeFromRowid {
	my ($self, $rowid) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $sql = "SELECT m_group.*, m_group_status.ident AS status, ".
		      "       m_group_type.ident AS type, m_theme.ident AS theme, m_lang.ident AS lang ".
			  
			  "FROM   m_group, m_group_status, m_group_type, m_theme, m_lang ".
			  
			  "WHERE  m_group.rowid=$rowid AND m_group.mioga_id = $self->{mioga_id} AND ".
			  "       m_group_status.rowid = m_group.status_id AND ".
			  "       m_group_type.rowid  = m_group.type_id AND ".
			  "       m_group_type.ident  = '$self->{type}' AND ".
			  "       (m_lang.rowid = m_group.lang_id OR (m_group.lang_id IS NULL AND m_lang.rowid = (SELECT rowid FROM m_lang LIMIT 1))) AND ".
			  "       (m_theme.rowid = m_group.theme_id OR (m_group.theme_id IS NULL AND m_theme.rowid = (SELECT rowid FROM m_theme WHERE mioga_id = m_group.mioga_id LIMIT 1)))";

	my $group = SelectSingle($dbh, $sql);
	
	if(!defined $group) {
		throw Mioga2::Exception::Group("Mioga2::Old::Group::InitializeFromRowid",
        __x("Group with rowid {rowid} not found", rowid => $rowid));
	}
	
	$self->{values} = $group;
}



# ============================================================================

=head2 InitializeFromIdent ($ident)

	Initialize the Group object from group ident.

=cut

# ============================================================================


sub InitializeFromIdent {
	my ($self, $ident) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $q_ident = st_FormatPostgreSQLString($ident);
	my $sql = "SELECT m_group.*, m_group_status.ident AS status, ".
		      "       m_group_type.ident AS type, m_theme.ident AS theme, m_lang.ident AS lang ".
			  
			  "FROM   m_group, m_group_status, m_group_type, m_theme, m_lang ".
			  
			  "WHERE  m_group.ident='$q_ident' AND m_group.mioga_id = $self->{mioga_id} AND ".
			  "       m_group_status.rowid = m_group.status_id AND ".
			  "       m_group_type.rowid  = m_group.type_id AND ".
			  "       m_group_type.ident  = '$self->{type}' AND ".
			  "       m_theme.rowid = m_group.theme_id AND ".
			  "       m_lang.rowid = m_group.lang_id";
	my $group = SelectSingle($dbh, $sql);
	
	if(! defined $group) {
		throw Mioga2::Exception::Group("Mioga2::Old::Group::InitializeFromIdent",
        __x("Group with ident {ident} not found", ident => $ident));
	}
	
	$self->{values} = $group;
}




# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LDAP Mioga2::Old::User Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
