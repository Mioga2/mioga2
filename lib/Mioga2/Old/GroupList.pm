# ============================================================================
# MiogaII Project (C) 2003 The MiogaII Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
GroupList.pm: object used to maniulation list of groups

=head1 DESCRIPTION

This module permits to manipuate list of groups

=head1 METHODS DESRIPTION

=cut

package Mioga2::Old::GroupList;
use strict;

use base qw(Mioga2::Old::GroupListBase);
use Locale::TextDomain::UTF8 'grouplist';

use Error qw(:try);
use Data::Dumper;

use Mioga2::Old::GroupListBase;
use Mioga2::tools::database;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;

my $debug=0;



# ============================================================================

=head2 new ($config, %params)

	Create a new GroupList object.

	There are four creation methods :

=over 4

=item create from nothing (empty list)
	
	new Mioga2::Old::GroupList ($config)

=item create from the rowid
	
	new Mioga2::Old::GroupList ($config, rowid_list => $rowid)

=item create from group ident

	new Mioga2::Old::GroupList ($config, ident_list => $group_ident)

=item create for all group of mioga

	new Mioga2::Old::GroupList ($config, all => 1)

=back

=cut

# ============================================================================

sub new {
	my ($class, $config, %param) = @_;

	my $self = { };

	if(exists $param{type}) {
		$self->{type} = $param{type};
	}
	else {
		$self->{type} = 'group';
	}


	if (!defined($config)) {
		throw Mioga2::Exception::Simple("Mioga2::Old::GroupList::new", __"Missing parameters.");
	}

	$self->{config} = $config;

	bless($self, $class);


	if(exists $param{ident_list}) {
		$self->InitializeFromIdent($param{ident_list});
	}

	elsif(exists $param{rowid_list}) {
		$self->InitializeFromRowid($param{rowid_list});
	}

	elsif(exists $param{all}) {
		$self->InitializeFromAll();
	}

	else {
		# nothing to do
		$self->{list} = [];
	}
	
	return $self;
}

# ============================================================================

=head2 GetIdents ()

	return list of idents for groups in object GroupList

=cut

# ============================================================================

sub GetIdents  {
	my ($self) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my @tab = @{$self->{list}};
	if ($#tab < 0)
	{
		return @tab;
	}

	my $sql = "select distinct rowid, ident as attr from m_group where rowid in ( ";
	$sql .= join (',',@{$self->{list}});
	$sql .= " ) and mioga_id = $mioga_id";

	my $ident_list = SelectMultiple ($dbh, $sql);
	my $idents;

	if (! defined($ident_list))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::GroupListBase::GetIdents", $DBI::err,  $DBI::errstr, $sql);
	}

	$idents = $self->GetAttributesForRowids($ident_list);

	return $idents;
}

# ============================================================================

=head2 AddGroupsAnimatedBy ($user_rowid)

	Add groups animated by user with rowid $user_rowid to the current list.

=cut

# ============================================================================

sub AddGroupsAnimatedBy  {
	my ($self, $user_rowid) = @_;
	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my @uniq_group_ident_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select * from m_group where anim_id = $user_rowid and mioga_id = $mioga_id";
	my $animated_groups = SelectMultiple($dbh, $sql);

	if (! defined ($animated_groups))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::GroupList::AddGroupsAnimatedBy", $DBI::err,  $DBI::errstr, $sql);
	}

	foreach my $group (@$animated_groups)
	{
		push (@rowid, $group->{rowid});
	}

	# testing unicity
	foreach my $group_rowid (@rowid)
	{
		$seen{$group_rowid}++;
	}
	@uniq_group_ident_list = keys %seen;

	$self->{list} = \@uniq_group_ident_list;

}

# ============================================================================

=head2 AddGroupsContainingUser ($user_rowid)

	Add groups containing user with rowid $user_rowid to the current list.

	Don t check user invited via teams

=cut

# ============================================================================

sub AddGroupsContainingUser  {
	my ($self, $user_rowid) = @_;
	my $dbh = $self->{config}->GetDBH();

	my @uniq_group_ident_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select * from m_group_invited_user, m_group_base, m_group_type where invited_group_id = $user_rowid and m_group_base.rowid = group_id and type_id = m_group_type.rowid and m_group_type.ident = '$self->{type}'";
	my $invited_users = SelectMultiple($dbh, $sql);

	if (!defined ($invited_users))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::GroupList::AddGroupsContainingUser", $DBI::err,  $DBI::errstr, $sql);
	}

	foreach my $group (@$invited_users)
	{
		push (@rowid, $group->{group_id});
	}

	# testing unicity
	foreach my $group_rowid (@rowid)
	{
		$seen{$group_rowid}++;
	}
	@uniq_group_ident_list = keys %seen;

	$self->{list} = \@uniq_group_ident_list;

}

# ============================================================================

=head2 AddExpandedGroupsContainingUser ($user_rowid)

	Add groups containing user with rowid $user_rowid to the current list.

	Check users invited via teams

=cut

# ============================================================================

sub AddExpandedGroupsContainingUser  {
	my ($self, $user_rowid) = @_;
	my $dbh = $self->{config}->GetDBH();

	my @uniq_group_ident_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select group_id
               from m_group_invited_user, m_group_base, m_group_type 
		       where invited_group_id = $user_rowid 
		         and m_group_base.rowid = group_id 
		         and type_id = m_group_type.rowid 
		         and m_group_type.ident = '$self->{type}'

               union

               select group_id
               from m_group_expanded_user, m_group_base, m_group_type 
               where invited_group_id = $user_rowid 
                 and m_group_base.rowid = group_id 
                 and type_id = m_group_type.rowid 
                 and m_group_type.ident = '$self->{type}'
		   ";

	my $invited_users = SelectMultiple($dbh, $sql);

	if (!defined ($invited_users))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::GroupList::AddExpandedGroupsContainingUser", $DBI::err,  $DBI::errstr, $sql);
	}

	foreach my $group (@$invited_users)
	{
		push (@rowid, $group->{group_id});
	}

	# testing unicity
	foreach my $group_rowid (@rowid)
	{
		$seen{$group_rowid}++;
	}
	@uniq_group_ident_list = keys %seen;


	$self->{list} = \@uniq_group_ident_list;
}

# ============================================================================

=head2 AddExpandedGroupsContainingUserWithFunction ($user_rowid, $app, $func)

	Add groups containing user with rowid $user_rowid and authorized 
	to execute $func function in $app application to the current list.

	Check users invited via teams

=cut

# ============================================================================

sub AddExpandedGroupsContainingUserWithFunction  {
	my ($self, $user_rowid, $app, $func) = @_;

	my $dbh = $self->{config}->GetDBH();

	my @uniq_user_list;
	my @rowid = @{$self->{list}};
	my %seen = ();

	my $sql = "select m_profile_user_function.group_id from m_profile_user_function, m_group, m_group_type ".
		      "where m_profile_user_function.user_id = $user_rowid and ".
			  "      m_profile_user_function.group_id = m_group.rowid and ".
			  "      m_group_type.rowid = m_group.type_id and ".
			  "      m_group_type.ident = '$self->{type}' and ".
			  "      m_profile_user_function.app_ident = '$app' and ".
			  "      m_profile_user_function.function_ident = '$func' " .

			  " union ".

			  "select m_profile_expanded_user_functio.group_id from m_profile_expanded_user_functio, m_group, m_group_type ".
		      "where m_profile_expanded_user_functio.user_id = $user_rowid and ".
			  "      m_profile_expanded_user_functio.group_id = m_group.rowid and ".
			  "      m_group_type.rowid = m_group.type_id and ".
			  "      m_group_type.ident = '$self->{type}' and ".
			  "      m_profile_expanded_user_functio.app_ident = '$app' and ".
			  "      m_profile_expanded_user_functio.function_ident = '$func' ";


	my $users = SelectMultiple($dbh, $sql);

	if (! defined ($users))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::UserList::AddUsersContainingUser", $DBI::err,  $DBI::errstr, $sql);
	}


	push @rowid, map {$_->{group_id}} @$users;

	# testing unicity
	foreach my $user_rowid (@rowid)
	{
		$seen{$user_rowid}++;
	}
	@uniq_user_list = keys %seen;

	$self->{list} = \@uniq_user_list;
}

# ============================================================================

=head2 GetDescriptions ()

	return list of descriptions for groups in object GroupList

=cut

# ============================================================================

sub GetDescriptions  {
	my ($self) = @_;

	if(! @{$self->{list}} ) {
		return [];
	}

	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my @rowid = @{$self->{list}};
	my $sql = "select distinct rowid, description as attr from m_group where rowid in ( ";
	$sql .= join (',',@rowid);
	$sql .= " ) and mioga_id = $mioga_id";

	my $attribute_list = SelectMultiple ($dbh, $sql);

	if (! defined($attribute_list))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::GroupList::Getdescriptions", $DBI::err,  $DBI::errstr, $sql);
	}

	my $idents = $self->GetAttributesForRowids($attribute_list);

	return $idents;
}

# ============================================================================

=head2 SendMail ($mime_entity)

	Get mail ($mime_entity in MIME::ENTITY format) and send it to all groups
	in group list

=cut

# ============================================================================

sub SendMail  {
	my ($self, $mime_entity) = @_;

	my $user_list = $self->GetUsers();

	$user_list->SendMail($mime_entity);
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 InitializeFromAll ()

	Initialize the GroupList object with every groups.

=cut

# ============================================================================

sub InitializeFromAll {
	my ($self, $rowid) = @_;
	my $dbh = $self->{config}->GetDBH();

	my $mioga_id = $self->{config}->GetMiogaId();

	my $res = SelectMultiple($dbh, "SELECT m_group.rowid FROM m_group, m_group_type WHERE mioga_id = $mioga_id AND m_group_type.ident = '$self->{type}' AND m_group.type_id = m_group_type.rowid");

	my @rowids = map {$_->{rowid}} @$res;

	$self->{list} = \@rowids;

}


# ============================================================================

=head2 InitializeFromRowid (@rowid_list)

	Initialize the GroupList object from list of group rowids.

=cut

# ============================================================================

sub InitializeFromRowid {
	my ($self, $rowid) = @_;

	$self->{list} = $rowid;

}


# ============================================================================

=head2 InitializeFromIdent (@ident_list)

	Initialize the GroupList object from list of group idents.

=cut

# ============================================================================

sub InitializeFromIdent {
	my ($self, $ident) = @_;
	my $dbh = $self->{config}->GetDBH();
	my $mioga_id = $self->{config}->GetMiogaId();

	my $db_group;

	my $sql = "select distinct m_group.rowid, m_group.ident from m_group, m_group_type where m_group.ident in ( '";
	$sql .= join ("', '",@$ident);
	$sql .= "' ) and m_group.mioga_id = $mioga_id AND m_group_type.rowid = m_group.type_id AND m_group_type.ident = '$self->{type}'";

	$db_group = SelectMultiple ($dbh, $sql);

	if (!defined($db_group))
	{
		throw Mioga2::Exception::DB("Mioga2::Old::GroupList::InitializeFromGroupIdent", $DBI::err,  $DBI::errstr, $sql);
	}

	my $ident_to_rowid;

	foreach my $val (@$db_group)
	{
		if(!defined($val->{ident}))
		{
			throw Mioga2::Exception::Group("Mioga2::Old::GroupList::InitializeFromGroupIdent", __"Invalid ident");
		}
		$ident_to_rowid->{$val->{ident}} = $val->{rowid};
	}

	my @rowids;
	foreach my $id (@$ident)
	{
		push (@rowids, $ident_to_rowid->{$id});
	}

	$self->{list} = \@rowids;

}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Old::GroupListBase Mioga2::Old::UserList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
