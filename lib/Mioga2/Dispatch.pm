# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Dispatch.pm: This is the main Mioga2 handler for applications

=head1 DESCRIPTION

It does :

=over 4

=item 1

 Create RequestContext and Application if they don't exist (case of public applications)

=item 2

 Run the Application and Method according to the URL

=item 3

 Check errors

=item 4

 Run the Content object returned by the application.

=back

=head1 APACHE CONFIGURATION

 <Location /foo>
    SetHandler  perl-script
    AuthType Basic
    AuthName Mioga
    require valid-user
    PerlSetVar MiogaConfig /path/to/Mioga.conf
    PerlAuthenHandler Mioga2::Authen
    PerlAuthzHandler Mioga2::Authz
    PerlHandler Mioga2::Dispatch
 </Location>

The variable MiogaConfig must be set to the path of the Mioga.conf file.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Dispatch;
use strict;

use Locale::TextDomain::UTF8 'dispatch';

use Error qw(:try);
use Mioga2::RequestContext;
use Mioga2::Config;
use Mioga2::Old::User;
use Mioga2::Old::Group;
use Mioga2::Old::Resource;
use Mioga2::MiogaConf;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::args_checker;
use Mioga2::Content::Error;
use Mioga2::Content::ASIS;
use Mioga2::Login;
use Benchmark::Timer;
use Data::Dumper;

use CGI::Cookie;

use Mioga2::Apache;

my $debug = 0;
my $timer = 0;

$Error::Debug = 1;

# ============================================================================

=head2 Dispatch::SendError ($context, %params)

	Create error message.
		
=cut

# ============================================================================

sub SendError {
	my ($context, %params) = @_;

	my $content;

	if(defined $context) {
		$content = new Mioga2::Content::Error($context, %params);
	}
	else {
		$content = new Mioga2::Content::ASIS($context, MIME => "text/plain");
		$content->SetContent($params{string});
	}

	return $content;
}
# ============================================================================

=head2 Dispatch::CreateGroup ($context, $request)

	Create the current group or resource object and store this object 
	in context.

	Return Mioga2::Apache::HTTP_OK or Mioga2::Apache::HTTP_SERVER_ERROR.
	
=cut

# ============================================================================
sub CreateGroup {
	my ($context, $r) = @_;

	my $uri = $context->GetURI();
	my $config = $context->GetConfig();
	my $group_ident = $context->GetGroupIdent();
	my $auth_return = Mioga2::Apache::HTTP_OK;

	my $group;
	try {
		try {
			try {
				$group = new Mioga2::Old::Group($config, ident => $group_ident);

			} catch Mioga2::Exception::Group with {
				my $error = shift;
				$group = new Mioga2::Old::User($config, ident => $group_ident);
			};
		}
		catch Mioga2::Exception::User with {
			my $error = shift;
			$group = new Mioga2::Old::Resource($config, ident => $group_ident);
		};
	}

	catch Mioga2::Exception::Group with {
		my $err = shift;
		$r->warn("==> Mioga2::Dispatch::CreateGroup: No such group $group_ident.");
		$auth_return = Mioga2::Apache::HTTP_FORBIDDEN;
	}

	catch Mioga2::Exception::Resource with {
		my $err = shift;
		$r->warn("==> Mioga2::Dispatch::CreateGroup: No such Group or Resource $group_ident.");
		$auth_return = Mioga2::Apache::HTTP_FORBIDDEN;
	}

	catch Mioga2::Exception::Simple with {
		my $err = shift;
		$r->log_error("==> Mioga2::Dispatch::handler: Exception Simple = " . $err->as_string());
		$auth_return = Mioga2::Apache::HTTP_SERVER_ERROR;
	}

	otherwise {
		my $err = shift;
		$r->log_error("==> Mioga2::Dispatch::handler: Other exception ");
		$r->log_error(Dumper($err));
		$auth_return = Mioga2::Apache::HTTP_SERVER_ERROR;
	}
	finally {
		$context->SetGroup($group);
	};
	


	return $auth_return;
}


# ============================================================================

=head2 Dispatch::CreateContext ($request)

	Create a public context from apache request.
	
=cut

# ============================================================================

sub CreateContext {
	my ($r) = @_;

	my $config_file = $r->dir_config("MiogaConfig");

	if(!defined $config_file) {
		$r->log_error("==> Mioga2::Authen::handler: Can't find MiogaConfig parameter");
		$r->log_error("==> Mioga2::Authen::handler: Add \"PerlSetVar MiogaConfig /path/to/Mioga.conf\" in your apache configuration file");
		
		return undef;
	}

	my $miogaconf = new Mioga2::MiogaConf($config_file);
	my $uri    = new Mioga2::URI($miogaconf, uri => $r->uri);

	my $config = new Mioga2::Config($miogaconf, $uri->GetMiogaIdent());
	my $context = new Mioga2::RequestContext($r, $config);

	#-----------------------------------------------------------
	# Create the Application object.
	#-----------------------------------------------------------
	$context->CreateApplication();

	return $context;
}



# ============================================================================

=head2 Dispatch::UpdateLastConnection ($context)

	Update the last connection date in the database for the current user or 
	group.
	
=cut

# ============================================================================

sub UpdateLastConnection {
	my ($context) = @_;
	
	my $config = $context->GetConfig();
	my $dbh    = $config->GetDBH();

	my $user  = $context->GetUser();
	my $group = $context->GetGroup();

	GroupSetLastConnectionDate($dbh, $group->GetRowid(), $user->GetRowid());
}



# ============================================================================

=head2 Dispatch::UpdateHistory ($context)

	Update the history information in session.
	
=cut

# ============================================================================

sub UpdateHistory {
	my ($context) = @_;
	
	my $uri     = $context->GetURI();

	return if $uri->IsPublic ();
	
	my $session = $context->GetSession();
	my $app_desc = $context->GetAppDesc();

	return if ! $uri->IsApplication();

	my $referer = $uri->GetURI();
  
	if ($app_desc->GetType() ne 'portal') {
		return;
	}

	$session->{__internal}->{history} = $referer;
}



# ============================================================================

=head2 Dispatch::handler ($request)

	Apache/mod_perl handler main function. $request is a the Apache request 
	object.

	return an Apache::Constant constant like Mioga2::Apache::HTTP_OK or REDIRECT.
	
=cut

# ============================================================================

sub handler : method {
    my($class, $r) = @_;
	my $isxml = 0;
	$r->warn("==> Mioga2::Dispatch::handler") if ($debug > 0);


	#-----------------------------------------------------------
	# Create a new Benchmark::Timer object used to count 
	# application processing time.
   	#-----------------------------------------------------------
	my $bench;
	my $benchuri;
	if($timer) {
		$benchuri = $r->uri;
		
		$benchuri =~ s!^.*((/[^/]+){2})$!$1!g;

		$bench = new Benchmark::Timer();
		$bench->start("Mioga2::Dispatch $$ ($benchuri)");
	}

	my $context = $r->pnotes("context");

	my $error = Mioga2::Apache::HTTP_OK;
	my $content;
	try {
		#-----------------------------------------------------------
		# If the context was not in pnotes (case of public apps and 
		# secure XML transaction), create it.
		#-----------------------------------------------------------

		if(!defined $context) {
			$context = CreateContext($r);

			my $uri = $context->GetURI();

			if(!defined $context) {
				$error = Mioga2::Apache::HTTP_SERVER_ERROR;
			}
			elsif(! $uri->IsSXML()) {
				$error = CreateGroup($context, $r);
			}

			if($error != Mioga2::Apache::HTTP_OK) {
				return;
			}
			#TODO for LogHandler $r->pnotes("context" => $context);

			# Check if user can be authenticated (in case of public access from a previously-authenticated user, it can be useful to know which user is performing the request)
			if (!defined ($context->{user})) {
				# Variables for HTML authentication
				my %cookies = CGI::Cookie->fetch($r);
				my $cookie = $cookies{mioga_session};
				my $token = $cookie->value () if ($cookie);
				if ($token) {
					my $t_uri  = Mioga2::Classes::URI->new(uri => $r->uri)->as_string;
					my $real_uri = $t_uri;
					$real_uri =~ s!/$!!;
					my $uri_obj    = new Mioga2::URI($context->GetConfig ()->GetMiogaConf (), uri => $real_uri);
					warn ("==> Session token provided, trying to authenticate user against $token") if ($debug);
					my $user = Mioga2::Login::GetUser ($context->GetConfig (), $uri_obj, $token);
					if ($user) {
						$context->SetUser ($user);
					}
				}
			}

		}

		else {
			#-----------------------------------------------------------
			# For private application request, update Last
			# connection date.
			#-----------------------------------------------------------
			UpdateLastConnection($context);
		}
		
		print STDERR "Dispatch context = ".Dumper($context)."\n" if ($debug > 1);
		
 		my $uri = $context->GetURI();
		if($uri->IsXML() or $uri->IsSXML())
		{
			$isxml = 1;
			print STDERR "Dispatch isxml = $isxml\n" if ($debug > 0);
		}

		my $application = $context->GetApplication();

		my $method = $context->GetMethod();
		
		#-----------------------------------------------------------
		# Update history information in current session
		#-----------------------------------------------------------
		UpdateHistory($context);

		#-----------------------------------------------------------
		# For public application request, check if the application
		# is configured to be run in public mode.
		# For XML and SXML transaction, check if the method can be 
		# executed in this mode
		#-----------------------------------------------------------
		my $app_desc = $context->GetAppDesc();

		$context->SetAPIVersion ();

		if($uri->IsPublic() and ! $app_desc->IsPublicForCurrentGroup($context)) {
			$error = Mioga2::Apache::HTTP_FORBIDDEN;
			return;
		}

		elsif($uri->IsSXML() and ! $app_desc->CanBeSXML($method)) {
			$error = Mioga2::Apache::HTTP_FORBIDDEN;
			return;
		}

		elsif($uri->IsXML() and ! $app_desc->CanBeXML($method)) {

			$error = Mioga2::Apache::HTTP_FORBIDDEN;
			return;
		}

		elsif(!$context->IsCSRFSafe()) {
			throw Mioga2::Exception::Simple("Dispatch::handler", "Rejecting CSRF unsafe request to " . $app_desc->GetPackage() . "::$method");
		}

		#-----------------------------------------------------------
		# Run the application with given method
		#-----------------------------------------------------------
		if($timer > 1) {
			$bench->stop();
			$r->warn($bench->report());
			$bench = new Benchmark::Timer();
			$bench->start("apps ($benchuri)");
		}

		print STDERR "Dispatch app_desc = ".Dumper($app_desc)."\n" if ($debug > 1);
		print STDERR "Dispatch Application : ".$app_desc->{values}->{ident}." Method : $method\n" if ($debug);

		$r->pnotes("args" => $context->{args});
		ac_SetParameters($context->{args});

		$application->I18nize($context->GetConfig, $app_desc->GetIdent, $context->GetGroup->GetLang);
		$application->InitializeFromContext ($context);

		if ($uri->IsWS) {
		  $content = $application->WSWrapper($context, $method, $uri->GetFormat);
		}
		else {
		  $content = $application->$method($context);
		}

		my $val = ac_IsParametersChecked();
		if($val > 0) {
			$r->warn("Parameters not checked : $val");
			$r->warn("  Application : ".$app_desc->{values}->{ident}." Method : $method");
			my $params = ac_GetParametersStatus();
			foreach my $k (keys(%$params)) {
				$r->warn("  Parameter : $k status : ".$params->{$k}->{status});
			}
		}
		if($timer > 1) {
			$bench->stop();
			$r->warn($bench->report());
			$bench = new Benchmark::Timer();
			$bench->start("xsl ($benchuri)");
		}

		if (!defined($content)) {
			throw Mioga2::Exception::Simple("Dispatch::handler", 
            __x("No content returned by {application}::{method}", application => $app_desc->{values}->{ident}, method => $content->{method}));
		}
	}
	catch Mioga2::Exception::DB with {
		my $err = shift;
		
		if($Error::Debug) {
			$class->LogException ($context, $err, $r);
		}

		if($isxml) {
			$error = Mioga2::Apache::HTTP_SERVER_ERROR;
		}
		else {
			$content = SendError($context, type =>'DB', string => $class->GenerateErrorMessage ($context));
		}
	}
	catch Mioga2::Exception::ExternalMioga with {
		my $err = shift;

		if($Error::Debug) {
			$class->LogException ($context, $err, $r);
		}

		$content = SendError($context, type =>'ExternalMioga', string => $class->GenerateErrorMessage ($context));
	}
	catch Mioga2::Exception::Simple with {
		my $err = shift;

		if($Error::Debug) {
			$class->LogException ($context, $err, $r);
		}

		if($isxml) {
			$error = Mioga2::Apache::HTTP_SERVER_ERROR;
		}
		else {
			$content = SendError($context, type =>'Simple', string => $class->GenerateErrorMessage ($context));
		}
	}
	catch Mioga2::Exception::Crypto with {
		my $err = shift;

		print STDERR Dumper($err);

		if($Error::Debug) {
			$class->LogException ($context, $err, $r);
		}

		$error = Mioga2::Apache::HTTP_FORBIDDEN;
	}
	catch Mioga2::Exception::HTTPError with {
		my $err = shift;

		print STDERR Dumper($err) if ($debug);

		if ($err->{code} eq "HTTP_AUTHZ") {
			$error = Mioga2::Apache::HTTP_FORBIDDEN;
		}
		else {
			$error = Mioga2::Apache::HTTP_SERVER_ERROR;
		}
	}
	otherwise {
		my $err = shift;

		if($Error::Debug) {
			$class->LogException ($context, $err, $r);
		}

		if($isxml) {
			$error = Mioga2::Apache::HTTP_SERVER_ERROR;
		}
		else {
			$content = SendError($context, type =>'other', string => $class->GenerateErrorMessage ($context));
		}
	};
	print STDERR "    error = $error\n" if ($debug);

	#-----------------------------------------------------------
	# If is_forbidden is set (cas of public application) return 
	# Mioga2::Apache::HTTP_FORBIDDEN
	#-----------------------------------------------------------
	if($error != Mioga2::Apache::HTTP_OK) {
		return $error;
	}
	
	#-----------------------------------------------------------
	# Generate the content
	#-----------------------------------------------------------
	print STDERR "    Generate content\n" if ($debug);
	my $data;
	try {
		# Re-generate sessionID cookie if required
		if (!$context->IsPublic () && $r->pnotes ('authen_web')) {
			my $token = $r->pnotes ('session_token');
			my $user = $context->GetUser ();
			my $cookie = Mioga2::Login::GetCookie ($context->GetConfig ()->GetMiogaConf (), $user, $token);
			$r->headers_out->add('Set-Cookie' => $cookie); # For Mioga2::Content::REDIRECT
			push (@{$content->{cookies}}, $cookie);
		}

		$data = $content->Generate($r);

		if($timer > 1) {
			$bench->stop();
			$r->warn( $bench->report());
			$bench = new Benchmark::Timer();
			$bench->start("generate ($benchuri)");
		}

	} otherwise {
		my $err = shift;
		$r->warn("Mioga2::Dispatch::handler");
		$r->warn(Dumper($err));
		return Mioga2::Apache::HTTP_SERVER_ERROR;
	};

	#-----------------------------------------------------------
	# If data is not defined, the content object have allready
	# send all the required informations.
	#-----------------------------------------------------------
	print STDERR "    Prepare response\n" if ($debug);
	if(defined $data) {
		my $encoding  = $content->GetEncoding();
		my $mime_type = $content->GetMIME();
		my $can_cache = $content->CanCache();
		
		if($timer > 1) {
			$bench->stop();
			$r->warn($bench->report());
			$bench = new Benchmark::Timer();
			$bench->start("beforehttp ($benchuri)");
		}

		if(defined $encoding) {
			$r->content_encoding($encoding);
			$mime_type .= "; charset=$encoding";
		}
		

		if(! $can_cache) {
			$r->no_cache(! $can_cache);
		}
		
		print STDERR "Dispatch data = $data\n" if ($debug > 1);
		
		if($timer > 1) {
			$bench->stop();
			print STDERR ($bench->report());
			$bench = new Benchmark::Timer();
			$bench->start("beforehttp1 ($benchuri)");
		}
		
        $r->content_type($mime_type);
		$r->print($data);
	}

	if($timer) {
		$bench->stop();
		print STDERR ($bench->report());
	}

	$r->pnotes('context', 0);

	print STDERR "Mioga2::Dispatch  end\n" if ($debug);
	return $content->GetReturnCode();
}


#===============================================================================

=head2 GenerateErrorMessage

Generate error message string to display to user

=over

=item B<Parameters:>

=over

=item I<$context> The current Mioga2::RequestContext.

=back

=item B<Return:> The message string.

=back

=cut

#===============================================================================
sub GenerateErrorMessage {
	my ($self, $context) = @_;

	my $message = '';

	# Add the current URI
	if ($context) {
		$message .= 'URI: ' . $context->GetURI()->GetURI() . "\n";
	}

	# Add the current date and time
	my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime (time);
	$message .= 'Date: ' . 0 x (2-length($mday)) . $mday . "/" . 0 x (2-length($mon)) . $mon . "/" . (1900+$year) . "\n";
	$message .= 'Time: ' . 0 x (2-length($hour)) . $hour . ":" . 0 x (2-length($min)) . $min . ":" . 0 x (2-length($sec)) . $sec . "\n";

	# Add the user login
	if ($context) {
		$message .= 'Login: ' . $context->GetUser()->GetIdent () . "\n";
	}

	return ($message);
}	# ----------  end of subroutine GenerateErrorMessage  ----------


#===============================================================================

=head2 LogException

Generic exception log method

=over

=item B<Parameters:>

=over

=item I<$context> The current Mioga2::RequestContext.

=item I<$err> The current Error.

=item I<$r> The current Apache2::RequestRec.

=back

=item B<Return:> Nothing.

=back

=cut

#===============================================================================
sub LogException {
	my ($self, $context, $err, $r) = @_;

	print STDERR "\n";

	#-------------------------------------------------------------------------------
	# Get stack trace
	#-------------------------------------------------------------------------------
	my @trace;
	if (ref ($err) =~ /^(Mioga2::Exception|Error::Simple)/) {
		@trace = split ("\n", $err->stacktrace);
	}
	elsif (ref ($err) =~ /XML::LibXML::Error/) {
		push (@trace, $err->as_string);
		my $dump = Dumper $err;
		push (@trace, $dump);
		push (@trace, '');
		push (@trace, '');
	}
	else {
		push (@trace, "Unknown error type: " . ref ($err));
		my $dump = Dumper $err;
		push (@trace, $dump);
		push (@trace, '');
		push (@trace, '');
	}

	#-------------------------------------------------------------------------------
	# Log exception type
	#-------------------------------------------------------------------------------
	$r->log_error ('##### [BEGIN] ' . ref ($err) . ' #####');

	#-------------------------------------------------------------------------------
	# Log error message
	#-------------------------------------------------------------------------------
	$r->log_error ('Message: ' . shift (@trace));

	if ($context) {
		if (!$context->IsPublic ()) {
			#-------------------------------------------------------------------------------
			# Log user ident
			#-------------------------------------------------------------------------------
			$r->log_error ('User: ' . $context->GetUser->GetIdent ());
		}

		#-------------------------------------------------------------------------------
		# Log URI
		#-------------------------------------------------------------------------------
		$r->log_error ('URI: ' . $context->GetURI()->GetURI());

		#-------------------------------------------------------------------------------
		# Log access method
		#-------------------------------------------------------------------------------
		$r->log_error ('Method: ' . $context->{access_method});

		#-------------------------------------------------------------------------------
		# Log request arguments
		#-------------------------------------------------------------------------------
		my @args = split ("\n", Dumper ($context->{args}));
		# Drop first and last lines
		pop (@args);
		shift (@args);
		$r->log_error ("Arguments:");
		for (@args) {
			$r->log_error ($_);
		}
	}
	else {
		#-------------------------------------------------------------------------------
		# No context, inform the logfile
		#-------------------------------------------------------------------------------
		$r->log_error ("No context, can not get user, URI, method, params information");
	}

	#-------------------------------------------------------------------------------
	# Log SQL details
	#-------------------------------------------------------------------------------
	if (ref ($err) eq 'Mioga2::Exception::DB') {
		$r->log_error ('SQL details:');
		$r->log_error ('    SQL request: ' . $err->{sql});
		$r->log_error ('    SQL arguments: ' . join (', ', @{$err->{args}}));
	}

	#-------------------------------------------------------------------------------
	# Log trace
	#-------------------------------------------------------------------------------
	$r->log_error ("Trace:");
	# Drop useless two last lines
	pop (@trace);
	pop (@trace);
	for (@trace) {
		$_ =~ s/\t/    /g;
		$r->log_error ($_);
	}
	
	$r->log_error ('##### [END] ' . ref ($err) . ' #####');
	
	print STDERR "\n";
}	# ----------  end of subroutine LogException  ----------

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Authen Mioga2::Authz Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
