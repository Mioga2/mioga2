# ============================================================================
# Mioga2::Portal Project (C) 2004-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
	
BaseMioglet.pm: Base mioglets for Mioga2::Portal

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Portal::BaseMioglet;
use strict;

use base qw(Mioga2::Portal::Mioglet);
use Locale::TextDomain::UTF8 'portal_basemioglet';

use Error qw(:try);
use Data::Dumper;
use Mioga2::AnimGroup;
use Mioga2::Portal::Exception;
use Mioga2::tools::APIAuthz;
use Mioga2::Exception::Simple;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;

my $debug = 0;

my %MiogletDesc = (
  ident       => 'Base',
  package     => 'Mioga2::Portal::BaseMioglet',
  xml_prefix  => 'base',
  application => 'Mioga2::AnimGroup',
  methods     => {
         'Users' =>
           [ 'OtherContent', 'Error', 'GroupList', 'AppList', 'MemberList', 'MiogletSelector' ],
  },

);

my %DefaultXML = (
  AppList => {
               mode   => "group",
               titled => {},
               title  => "",
               boxed  => {},
  },

  GroupList => {
                 mode   => "user",
                 titled => {},
                 title  => "",
                 boxed  => {},
  },

  MemberList => {
                  mode   => "group",
                  titled => {},
                  title  => "",
                  boxed  => {},
  },

  MiogletSelector => { mode => "user" },

);

# ============================================================================

=head2 GetDefaultXML ()
	
	Class method returning default XML chunk of Mioglets (for portal editor).

	
=cut

# ============================================================================

sub GetDefaultXML
{
  my ( $class, $miog_desc, $method_ident ) = @_;

  my $xml;
  if ( grep { $_ eq $method_ident } qw (html image) )
  {
    $xml = "<$method_ident href=\"about:blank\"></$method_ident>";
  }
  elsif ($method_ident eq 'iframe') {
    $xml = "<$method_ident><src>http://www.mioga2.org</src><name>iframe</name></$method_ident>";
  }
  else
  {

    $DefaultXML{$method_ident}->{stylesheet} = "base_mioglet.xsl";

    $xml = st_HashToXML( $DefaultXML{$method_ident} );
    $xml = "<mioglet type=\"Base/$method_ident\">$xml</mioglet>";
    return $xml;
  }

  return $xml;
}

# ============================================================================

=head2 CheckUserAccessOnMethod ()
	
	Authorisation method check.
	Allow base method.
	
=cut

# ============================================================================

sub CheckUserAccessOnMethod
{
  my ( $self, $context, $user, $group, $method_ident ) = @_;

  if ( grep { $method_ident eq $_ }
       qw(OtherContent Error GroupList AppList MemberList MiogletSelector) )
  {
    return AUTHZ_OK;
  }

  return $self->SUPER::CheckUserAccessOnMethod( $context, $user, $group,
                                                $method_ident );
}

# ============================================================================

=head2 GetMiogletDesc ()

	Return the current application description.

=cut

# ============================================================================

sub GetMiogletDesc
{
  my ( $self, $context ) = @_;

  return \%MiogletDesc;
}

# ============================================================================

=head2 Error ()

	The standard Mioga2::Portal error handler.

	Display Error Message.

=cut

# ============================================================================

sub Error
{
  my ( $self, $context ) = @_;

  return $self->ErrorXML( $self->{args}->{errortype}, $self->{args}->{text} );
}

# ============================================================================

=head2 ErrorXML ()

	Return XML for Error Message.

=cut

# ============================================================================

sub ErrorXML
{
  my ( $self, $type, $text, $initial_tag ) = @_;

  my $prefix = $self->{mioglet_desc}->GetPrefix();

  my $xml = "<$prefix:error>";

  if ( defined $initial_tag )
  {
    $xml .= "<InitialTag>";
    $xml .= "<$initial_tag/>";
    $xml .= "</InitialTag>";
  }

  $xml .= "<type>" . st_FormatXMLString($type) . "</type>";
  $xml .= "<text>" . st_FormatXMLString($text) . "</text>";
  $xml .= "</$prefix:error>";

  return $xml;
}

# ============================================================================

=head2 OtherContent ()

	The standard Mioga2::Portal external content handler.

	Display Information using MIME type of content.

	Supported MIME types are :
	text/html, application/xml, text/plain, image/*
	
=cut

# ============================================================================

my @ContentMimeHandler = (
                           'text/html'       => "OtherContentHtml",
                           'text/.*'         => "OtherContentTextPlain",
                           'iframe/.*'        => "OtherContentIFrame",
                           'image/.*'        => "OtherContentImage",
                           'application/xml' => "OtherContentXML",
);

sub OtherContent
{
  my ( $self, $context ) = @_;

  my $handler;
  my $nbcontent = @ContentMimeHandler;

  for ( my $i = 0 ; $i < $nbcontent ; $i += 2 )
  {
    my $mime = $ContentMimeHandler[$i];

    if ( $self->{args}->{mime} =~ m!^$mime$! )
    {
      $handler = $ContentMimeHandler[ $i + 1 ];
      last;
    }
  }

  if ( !defined $handler )
  {
      return $self->ErrorXML( "Base/OtherContent", __x("Unsupported mime type: {mimetype}", mimetype => $self->{args}->{mime}) );
  }

  return $self->$handler($context);
}

# ============================================================================

=head2 GroupList ()

	Return the list of group where the current user is invited.
	The size parameter control the number of group displayed.
	A "Other ..." link permit to acc�s to full group list.

	Exceptions are supported (see Mioga2::Portal::Exception).

=cut

# ============================================================================

sub GroupList
{
  my ( $self, $context ) = @_;

  my $config = $context->GetConfig();
  my $prefix = $self->{mioglet_desc}->GetPrefix();

  my $user = $context->GetUser();

  my $exception =
    new Mioga2::Portal::Exception( $config, $self->{args}->{exception} );

  my $args;
  if ( exists $self->{size} )
  {
    $args->{size} = $self->{size};
  }

  my $groups = $exception->GetGroupList( $user, $args );

  my $xml = "<$prefix:GroupList>";

  my $idents   = $groups->GetIdents();
  my $descs    = $groups->GetDescriptions();
  my $nb_group = @$idents;

  for ( my $i = 0 ; $i < $nb_group ; $i++ )
  {
    $xml .=
        '<group ident="'
      . st_FormatXMLString( $idents->[$i] ) . '">'
      . st_FormatXMLString( $descs->[$i] )
      . '</group>';
  }

  $xml .= "</$prefix:GroupList>";

  return $xml;
}

# ============================================================================

=head2 MemberList ()

	Return the list of group members.
	The size parameter control the number of users displayed.
	A "Other ..." link permit to acc�s to full group list.

	Exceptions are supported (see Mioga2::Portal::Exception).

=cut

# ============================================================================

sub MemberList
{
  my ( $self, $context ) = @_;

  my $config = $context->GetConfig();
  my $prefix = $self->{mioglet_desc}->GetPrefix();

  my $group;
  if ( $self->{mode} eq 'group' )
  {
    $group = $context->GetGroup();
  }
  else
  {
    $group = $context->GetUser();
  }

  my $exception =
    new Mioga2::Portal::Exception( $config, $self->{args}->{exception} );

  my $args;
  if ( exists $self->{size} )
  {
    $args->{size} = $self->{size};
  }

  my $users = $exception->GetMemberList( $group, $args );

  my $xml = "<$prefix:MemberList>";

  my $idents  = $users->GetIdents();
  my $names   = $users->GetNames();
  my $nb_user = @$idents;

  for ( my $i = 0 ; $i < $nb_user ; $i++ )
  {
    $xml .=
        '<user ident="'
      . st_FormatXMLString( $idents->[$i] ) . '">'
      . st_FormatXMLString( $names->[$i] )
      . '</user>';
  }

  $xml .= "</$prefix:MemberList>";

  return $xml;
}

# ============================================================================

=head2 AppList ()

	Return the list of application the current user can execute in current
	group (in group mode) or in his user space (in user mode)

	Exceptions are supported (see Mioga2::Portal::Exception).

=cut

# ============================================================================

sub AppList
{
  my ( $self, $context ) = @_;

  my $config = $context->GetConfig();
  my $prefix = $self->{mioglet_desc}->GetPrefix();

  my $user = $context->GetUser();
  my $group;
  if ( $self->{mode} eq 'group' )
  {
    $group = $context->GetGroup();
  }
  else
  {
    $group = $context->GetUser();
  }

  my $exception =
    new Mioga2::Portal::Exception( $config, $self->{args}->{exception} );

  my $apps = $exception->GetAppList( $user, $group );

  my $xml = "<$prefix:AppList mode=\"$self->{mode}\">";

  my $idents = $apps->GetIdents();
  my $names  = $apps->GetLocaleNames($user);

  my $nb_app = @$idents;

  for ( my $i = 0 ; $i < $nb_app ; $i++ )
  {
    $xml .=
        '<app ident="'
      . st_FormatXMLString( $idents->[$i] )
      . '" name="'
      . st_FormatXMLString( $names->[$i] ) . '"/>';
  }

  $xml .= "</$prefix:AppList>";

  return $xml;
}

# ============================================================================

=head2 OtherContentIFrame ()

	The standard Mioga2::Portal IFrame handler.

=cut

# ============================================================================
sub OtherContentIFrame
{
	my ( $self, $context ) = @_;

	my $prefix = $self->{mioglet_desc}->GetPrefix();

	my $src = $self->{args}->{src};
	my $name = $self->{args}->{name};
	my $xml = qq|<$prefix:iframe name="$name" src="$src"/>|;
	return $xml;
}
# ============================================================================

=head2 OtherContentImage ()

	The standard Mioga2::Portal Image handler.

=cut

# ============================================================================

sub OtherContentImage
{
  my ( $self, $context ) = @_;

  my $prefix = $self->{mioglet_desc}->GetPrefix();

  my $href = $self->GetFileURI( $context, $self->{args}->{href} );
  my $xml = qq|<$prefix:image href="$href"/>|;
  return $xml;
}

# ============================================================================

=head2 OtherContentTextPlain ()

	The standard Mioga2::Portal text/* (not html) handler.

=cut

# ============================================================================

sub OtherContentTextPlain
{
  my ( $self, $context ) = @_;

  my $prefix = $self->{mioglet_desc}->GetPrefix();

  my $xml;
  try
  {
    my $data = $self->GetFileData( $context, $self->{args}->{href} );

    $xml = "<$prefix:text>";
    $xml .= st_FormatXMLString($data);
    $xml .= "</$prefix:text>";
    }
    catch Mioga2::Exception::Simple with
  {
    my $err = shift;
    $xml =
      $self->ErrorXML( "Base/OtherContentTextPlain",
      __x("Error when reading file {filename}: {error}", filename => $self->{args}->{href}, error => $err->{-text}), "$prefix:text" );
  };

  return $xml;
}

# ============================================================================

=head2 OtherContentHtml ()

	The standard Mioga2::Portal text/html handler.

=cut

# ============================================================================

sub OtherContentHtml
{
  my ( $self, $context ) = @_;

  my $prefix = $self->{mioglet_desc}->GetPrefix();

  my $xml;
  try
  {
    my $data;
    if ( $self->{args}->{href} eq 'about:blank' )
    {
      $data = "<body></body>";
    }
    else
    {
      $data = $self->GetFileData( $context, $self->{args}->{href} );
    }

    $xml = "<$prefix:html>";
    if ( $data =~ m!<body>(.*)</body>!s )
    {
      $data = $1;
    }

    $xml .= st_FormatXMLString($data);
    $xml .= "</$prefix:html>";
    }
    catch Mioga2::Exception::Simple with
  {
    my $err = shift;
    $xml =
      $self->ErrorXML( "Base/OtherContent",
      __x("Error when reading html file {filename}: {error}", filename => $self->{args}->{href}, error => $err->{-text}),
            "$prefix:html" );
  };

  return $xml;
}

# ============================================================================

=head2 OtherContentXML ()

	The standard Mioga2::Portal text/html handler.

=cut

# ============================================================================

sub OtherContentXML
{
  my ( $self, $context ) = @_;

  my $prefix = $self->{mioglet_desc}->GetPrefix();

  my $xml;
  try
  {
    my $data = $self->GetFileData( $context, $self->{args}->{href} );

    my $parser = new XML::LibXML;
    my $doc    = $parser->parse_string($data);
    my @pi     = $doc->findnodes('processing-instruction("xml-stylesheet")');

    if (@pi)
    {
      my $pidata = $pi[0]->getData;
      if ( $pidata !~ m!type="text/xsl"! )
      {
        throw Mioga2::Exception::Simple(
                                 "Mioga2::Portal::BaseMioglet::OtherContentXML",
                                 __"Unsupported stylesheet type." );
      }

      my ($stylesheet_href) = ( $pidata =~ /href="(.*?)"/ );
      my $st_data = $self->GetFileData( $context, $stylesheet_href );

      my $stylesheet_doc = $parser->parse_string($st_data);
      my $xsl            = new XML::LibXSLT;
      my $stylesheet     = $xsl->parse_stylesheet($stylesheet_doc);
      my $results        = $stylesheet->transform($doc);

      $data = $stylesheet->output_string($results);
      $xml  = "<$prefix:html>";
      {
        local $/ = undef;
        if ( $data =~ m!<body>(.*)</body>! )
        {
          $data = $1;
        }
      }
      $xml .= st_FormatXMLString($data);
      $xml .= "</$prefix:html>";
    }

    else
    {
      $xml = $self->ErrorXML( "Base/OtherContent",
                            __"No xml-stylesheet processing instruction found." );
    }

    }
    catch Mioga2::Exception::Simple with
  {
    my $err = shift;
    $xml = $self->ErrorXML( "Base/OtherContent",
    __x("Error when reading XML file {filename}: {error}", filename => $self->{args}->{href}, error => $err->{-text}) );
    }
    otherwise
  {
    my $err = shift;
    $xml = $self->ErrorXML(
                    "Base/OtherContent",
                    __x("Error while transforming XML file {filename}: see apache log file for more details.", filename => $self->{args}->{href})
    );
    warn Dumper($err);
  };

  return $xml;
}

# ============================================================================

=head2 MiogletSelector ()

	The standard Mioga2::Portal MiogletSelector (for editor) handler.

=cut

# ============================================================================

sub MiogletSelector
{
  my ( $self, $context ) = @_;

  my $prefix = $self->{mioglet_desc}->GetPrefix();

  my $xml = "<$prefix:MiogletSelector>";

  my $mioglets = $self->GetMiogletList( $context->GetConfig() );
  foreach my $mioglet (@$mioglets)
  {
    next
      if (     $mioglet->{mioglet_ident} eq 'Base'
           and $mioglet->{method_ident} eq 'Error' );
    next
      if (     $mioglet->{mioglet_ident} eq 'Base'
           and $mioglet->{method_ident} eq 'OtherContent' );
    next
      if (     $mioglet->{mioglet_ident} eq 'Base'
           and $mioglet->{method_ident} eq 'MiogletSelector' );

    $xml .=
        "<$prefix:MiogletIdent>"
      . "<mioglet_ident>"
      . st_FormatXMLString( $mioglet->{mioglet_ident} )
      . "</mioglet_ident>"
      . "<method_ident>"
      . st_FormatXMLString( $mioglet->{method_ident} )
      . "</method_ident>"
      . "</$prefix:MiogletIdent>";
  }

  $xml .= "</$prefix:MiogletSelector>";

  return $xml;
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 GetFileData ()

	Return file data. The file MUST be in Mioga2 file system.

=cut

# ============================================================================

sub GetFileData
{
  my ( $self, $context, $file ) = @_;

  my $filepath = $self->GetFilePath( $context, $file );

  my $data = "";
  open( F_DATA, $filepath )
    or
    throw Mioga2::Exception::Simple( "Mioga2::Portal::BaseMioglet::GetFileData",
                                     $! );

  {
    local $/ = undef;
    $data = <F_DATA>;
  }

  close(F_DATA);

  return $data;
}

# ============================================================================

=head2 GetFilePath ()

	Return absolute Mioga file path. The file MUST be in Mioga2 file system.

=cut

# ============================================================================

sub GetFilePath
{
  my ( $self, $context, $file ) = @_;

  my $config = $context->GetConfig();
  my $uri    = $context->GetURI();

  my $filepath;
  if ( $uri->IsPublic() )
  {
    $filepath = $config->GetPublicDir();
  }
  else
  {
    $filepath = $config->GetPrivateDir();
  }

  if ( $self->{mode} eq 'group' )
  {
    my $group = $context->GetGroup();
    $filepath .= "/" . $group->GetIdent();
  }
  else
  {
    my $group = $context->GetUser();
    $filepath .= "/" . $group->GetIdent();
  }

  $filepath .= $file;

  return $filepath;
}

# ============================================================================

=head2 GetFileURI ()

	Return absolute Mioga file uri. The file MUST be in Mioga2 file system.

=cut

# ============================================================================

sub GetFileURI
{
  my ( $self, $context, $file ) = @_;

  my $config = $context->GetConfig();
  my $uri    = $context->GetURI();

  my $fileuri;
  if ( $uri->IsPublic() )
  {
    $fileuri = $config->GetPublicURI();
  }
  else
  {
    $fileuri = $config->GetPrivateURI();
  }

  if ( $self->{mode} eq 'group' )
  {
    my $group = $context->GetGroup();
    $fileuri .= "/" . $group->GetIdent();
  }
  else
  {
    my $group = $context->GetUser();
    $fileuri .= "/" . $group->GetIdent();
  }

  $fileuri .= $file;

  return $fileuri;
}

# ============================================================================

=head2 GetMiogletList ()

	Return the list of mioglets declared in database.

=cut

# ============================================================================

sub GetMiogletList
{
  my ( $self, $config ) = @_;

  my $dbh = $config->GetDBH();

  my $res = SelectMultiple( $dbh,
"SELECT portal_mioglet.ident AS mioglet_ident, portal_mioglet_method.ident AS method_ident "
      . "FROM   portal_mioglet_method, portal_mioglet, m_application, m_instance_application "
      . "WHERE  portal_mioglet_method.mioglet_id = portal_mioglet.rowid AND "
      . "       m_application.rowid = portal_mioglet.app_id AND "
	  . "       m_application.rowid = m_instance_application.application_id AND "
      . "       m_instance_application.mioga_id = "
      . $config->GetMiogaId() );

  return $res;
}

# ============================================================================

=head1 AUTHORS

	The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

	Mioga2::Application Mioga2::Portal::Mioglet Mioga2::Portal

=head1 COPYRIGHT

	Copyright (C) 2004-2007, The Mioga2 Project. All Rights Reserved.

	This module is free software; you can redistribute it and/or
	modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
