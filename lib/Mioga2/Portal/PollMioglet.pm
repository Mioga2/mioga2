# ============================================================================
# Mioga2::Portal Project (C) 2006-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
	
PollMioglet.pm: Poll mioglets for Mioga2::Portal

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Portal::PollMioglet;
use strict;

use base qw(Mioga2::Portal::Mioglet);
use Locale::TextDomain::UTF8 'portal_pollmioglet';

use Error qw(:try);
use Data::Dumper;
use POSIX qw(strftime tzset);
use Mioga2::AnimGroup;
use Mioga2::Portal::Exception;
use Mioga2::tools::APIAuthz;
use Mioga2::Exception::Simple;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::URI;
use Mioga2::Poll;

my $debug = 0;

my %MiogletDesc = (
  ident       => 'Poll',
  package     => 'Mioga2::Portal::PollMioglet',
  xml_prefix  => 'poll',
  application => 'Mioga2::Poll',
  methods     => { 'Read' => [ 'DisplayPolls' ] },

);

my %DefaultXML = (
                   DisplayPolls => {
                                 mode   => "user",
                                 titled => {},
                                 title  => "",
                                 boxed  => {},
                   },
);

# ============================================================================

=head2 GetDefaultXML ()
	
	Class method returning default XML chunk of Mioglets (for portal editor).

	
=cut

# ============================================================================

sub GetDefaultXML
{
  my ( $class, $miog_desc, $method_ident ) = @_;

  $DefaultXML{$method_ident}->{stylesheet} = "poll_mioglet.xsl";

  my $xml = st_HashToXML( $DefaultXML{$method_ident} );
  $xml = "<mioglet type=\"Poll/$method_ident\">$xml</mioglet>";
  warn("PollMioglet::GetDefaultXML xml = $xml") if ($debug);

  return $xml;
}

# ============================================================================

=head2 GetMiogletDesc ()

	Return the current application description.

=cut

# ============================================================================
sub GetMiogletDesc
{
  my ( $self, $context ) = @_;

  return \%MiogletDesc;
}
# ============================================================================

=head2 CheckUserAccessOnMethod ($self, $context, $user, $group, $method_ident)

	Check if the current user can access to the current mioglet and
	methods in the current group, depending of execution mode.
	
=cut

# ============================================================================
sub CheckUserAccessOnMethod
{
  my ( $self, $context, $user, $group, $method_ident ) = @_;
  if ( $method_ident eq 'DisplayPolls' )
  {
    return AUTHZ_OK;
  }

  return $self->SUPER::CheckUserAccessOnMethod( $context, $user, $group,
                                                $method_ident );
}
# ============================================================================

=head2 DisplayPolls ()

	Display all polls that can be answered (groups) for the current user.

=cut

# ============================================================================
sub DisplayPolls
{
	my ( $self, $context ) = @_;
	warn("PollMioglet::DisplayPolls self = " . Dumper($self) . "\n") if ($debug);

	my $config      = $context->GetConfig();
	my $prefix      = $self->{mioglet_desc}->GetPrefix();
	my $user        = $context->GetUser();
    my $user_id     = $user->GetRowid();
	my $group_id    = $context->GetGroup()->GetRowid();
    my $mode        = $self->{mode};
    my $group_list  = $mode eq 'user' ? $user->GetGroups->GetRowids : [$group_id];

	my $xml = "<$prefix:DisplayPolls>";
		
    my $sql = "SELECT poll.*, poll_user_answer.validated, poll_user_answer.user_id, m_group_base.ident as group_ident FROM m_group_base, poll LEFT JOIN poll_user_answer ON poll_user_answer.poll_id=poll.rowid WHERE (user_id IS NULL OR (user_id=$user_id AND validated = false)) AND group_id IN (" . join(",", @$group_list) . ") AND m_group_base.rowid=group_id ORDER BY poll.rowid DESC";
    
    my $results     = SelectMultiple($config->GetDBH(), $sql);
    my $poll_app    = new Mioga2::Poll($config);
    
    foreach my $res (@$results) {
        next if $poll_app->IsPollClosed(context => $context, poll => $res);
        
        my $txt = $res->{title};
        my $uri = new Mioga2::URI($config, group => $res->{group_ident}, public => 0, application => "Poll", method => "AnswerPoll", args => { rowid => $res->{rowid} });
        
    	$xml .= "<Poll rowid=\"$res->{rowid}\" link=\"" . st_FormatXMLString($uri->GetURI()) . "\">" . st_FormatXMLString($txt) . "</Poll>";
    }
	
	$xml .= "</$prefix:DisplayPolls>";
	warn("PollMioglet::DisplayPolls xml = $xml\n") if ($debug);

	return $xml;
}

# ============================================================================

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head1 AUTHORS

	The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

	Mioga2::Application Mioga2::Portal::Mioglet Mioga2::Portal

=head1 COPYRIGHT

	Copyright (C) 2006-2007, The Mioga2 Project. All Rights Reserved.

	This module is free software; you can redistribute it and/or
	modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
