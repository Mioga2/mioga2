# ============================================================================
# Mioga2::Portal Project (C) 2004-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Exception.pm: Mioga2::Portal Exception management.

=head1 METHODS DESRIPTION

=cut

package Mioga2::Portal::Exception;
use strict;

use vars qw(@ISA);
use Error qw(:try);
use Data::Dumper;
use Mioga2::Exception::Simple;
use Mioga2::AppDescList;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;

my $debug = 0;


# ============================================================================

=head2 new ($config, $exception)
	
	Create a new exception object.
	
=cut

# ============================================================================
	
sub new {
	my ($class, $config, $exception) = @_;
	
	my $self = { config    => $config,
				 exception => $exception
			   };

	foreach my $key (qw(teamMember group user application)) {
		if(! defined $exception or
		   ! exists $exception->{$key} or
		   ! defined $exception->{$key}) {
			$self->{exception}->{$key} = [];
		}

		if(ref($self->{exception}->{$key}) ne 'ARRAY') {
			$self->{exception}->{$key} = [$self->{exception}->{$key}];
		}
	}

	foreach my $team (@{$self->{exception}->{teamMember}}) {
		try {
			my $team = new Mioga2::Old::Team($config, ident => $team);
			my $users = $team->GetUsers();
			push @{$self->{exception}->{user}}, @{$users->GetIdents()};
		}
		otherwise {
			# Ignore but warn
			warn "Unknown team $team";
		};

	}
	
	bless( $self, $class);

	return $self;
}


# ============================================================================

=head2 GetGroupList ($user, $args)
	
	Return groups not in exception and where $user is member.
	Args :
	size => number of result to return.
	
=cut

# ============================================================================
	
sub GetGroupList {
	my ($self, $user, $args) = @_;

	$args = {} unless defined $args;

	my $groups = $user->GetExpandedGroups();

	my $idents = $groups->GetIdents();
	
	my @valid_idents;
	foreach my $ident (@$idents) {
		if(! grep {$_ eq $ident} @{$self->{exception}->{group}}) {
			push @valid_idents, $ident;
		}
	}

	if(@valid_idents) {
		my $sql = "SELECT m_group.ident, ".
			      "      COALESCE( ".
				  "         (SELECT last_connection FROM m_group_user_last_conn ".
				  "          WHERE  group_id = m_group.rowid AND ".
				  "                 user_id = ".$user->GetRowid()."), '1970-01-01 00:00:00') ".
				  "       AS last_connection ".
			      "FROM   m_group ".
				  "WHERE  m_group.ident IN (".join(',', map {"'".st_FormatPostgreSQLString($_)."'"} @valid_idents).") ".
				  " AND m_group.mioga_id = ".$self->{config}->GetMiogaId()." ".
				  "ORDER BY last_connection DESC";

		my $res = SelectMultiple($self->{config}->GetDBH(), $sql);

		@valid_idents = map {$_->{ident}} @$res;
	}

	if(exists $args->{size} and defined $args->{size}) {
		my $size = $args->{size};
		if($size > @valid_idents) {
			$size = @valid_idents;
		}

		$size--;

		my @list = @valid_idents[0..$size];
		@valid_idents = @list;
	}
	
	my $group_list = new Mioga2::Old::GroupList($self->{config}, 
										   ident_list => \@valid_idents);
	return $group_list;
}


# ============================================================================

=head2 GetAppList ($user, $group)
	
	Return applications that $user can execute in $group;

	Args :
	size => number of result to return.
	
=cut

# ============================================================================
	
sub GetAppList {
	my ($self, $user, $group) = @_;

	my $apps = new Mioga2::AppDescList ($self->{config},
										user_id => $user->GetRowid(),
										group_id => $group->GetRowid(),
										app_type => "normal");

	my $idents = $apps->GetIdents();

	my @valid_idents;
	foreach my $ident (@$idents) {
		if(! grep {$_ eq $ident} @{$self->{exception}->{application}}) {
			push @valid_idents, $ident;
		}
	}

	if(@valid_idents) {
		$apps = new Mioga2::AppDescList ($self->{config}, ident_list => \@valid_idents, having_method => "DisplayMain" );
	}

	return $apps;
}


# ============================================================================

=head2 GetMemberList ($user, $group)
	
	Return applications that $user can execute in $group;

	Args :
	size => number of result to return.
	
=cut

# ============================================================================
	
sub GetMemberList {
	my ($self, $group, $args) = @_;
	
	my $users = $group->GetExpandedUsers();

	my $rowids = $users->GetRowids();
	my $idents = $users->GetIdents();
	
	my $nb_users = @$rowids;
	my @valid_rowids;
	for(my $i=0; $i < $nb_users; $i++) {
		my $ident = $idents->[$i];
		my $rowid = $rowids->[$i];

		if(! grep {$_ eq $ident} @{$self->{exception}->{user}}) {
			push @valid_rowids, $rowid;
		}
	}

	if(@valid_rowids) {
		my $sql = "SELECT m_user_base.rowid, ".
			      "      COALESCE( ".
				  "         (SELECT last_connection FROM m_group_user_last_conn ".
				  "          WHERE  group_id = ".$group->GetRowid()." AND ".
				  "                 user_id = m_user_base.rowid), '1970-01-01 00:00:00') ".
				  "       AS last_connection ".
			      "FROM   m_user_base ".
				  "WHERE  m_user_base.rowid IN (".join(',', @valid_rowids).") ".
				  "ORDER BY last_connection DESC";

		my $res = SelectMultiple($self->{config}->GetDBH(), $sql);
		@valid_rowids = map {$_->{rowid}} @$res;
	}


	if(exists $args->{size} and defined $args->{size}) {
		my $size = $args->{size} - 1;
		if($size > @valid_rowids) {
			$size = @valid_rowids - 1;
		}
		
		my @list = @valid_rowids[0..$size];
		@valid_rowids = @list;
	}


	$users = new Mioga2::Old::UserList ($self->{config}, rowid_list => \@valid_rowids);
	return $users;
}


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head1 AUTHORS

	The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

	Mioga2::Application Mioga2::Portal::Mioglet Mioga2::Portal

=head1 COPYRIGHT

	Copyright (C) 2004-2007, The Mioga2 Project. All Rights Reserved.

	This module is free software; you can redistribute it and/or
	modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


