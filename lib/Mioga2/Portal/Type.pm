# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Type.pm: MIME Type Apache Handler

=head1 DESCRIPTION

This module is the Mioga2 Portal Apache/mod_perl mime type handler. 
It check if XML files are portal description and redirect to Portal.


=head1 APACHE CONFIGURATION

 <Location /foo>
    PerlTypeHandler Mioga2::Portal::Type
 </Location>

=head1 METHODS DESRIPTION

=cut

package Mioga2::Portal::Type;
use strict;

use Error qw(:try);
use Data::Dumper;
use Mioga2::Old::User;
use Mioga2::URI;
use Mioga2::Config;
use Mioga2::MiogaConf;
use Benchmark::Timer;

use Apache2::Const qw(OK AUTH_REQUIRED FORBIDDEN DECLINED NOT_FOUND SERVER_ERROR REDIRECT);

my $debug = 0;

my $timer = 1;




# ============================================================================

=head2 Type::CreateConfig ($request)

	Create the Mioga2::Config object
	
=cut

# ============================================================================

sub CreateConfig {
	my ($r, $uri) = @_;

	my $config_file = $r->dir_config("MiogaConfig");

	if(!defined $config_file) {
		$r->log_error("==> Mioga2::Authen::handler : Can't found MiogaConfig parameter");
		$r->log_error("==> Mioga2::Authen::handler : Add \"PerlSetVar MiogaConfig /path/to/Mioga.conf\" in your apache configuration file");
		
		return undef;
	}

	my $miogaconf = new Mioga2::MiogaConf($config_file);
	my $uri    = new Mioga2::URI($miogaconf, uri => $r->uri);
	my $config = new Mioga2::Config($miogaconf, $uri->GetMiogaIdent());

	return ($config, $uri);
}
# ============================================================================

=head2 Type::CheckXMLFile ($request, $config, $uri, $path)

	Check if the XML file is a Portal description.

	return an Apache::Constant constant like DECLINED or REDIRECT.
	
=cut

# ============================================================================

sub CheckXMLFile {
	my ($r, $config, $uri, $path) = @_;

	open(F_FILE, $path)
		or die "Can't open $path : $!";

	my $data;
	{
		local $/ = undef;
		$data = <F_FILE>;
	}

	close(F_FILE);

	if($data =~ m/^\s*<\?xml .*?>\s+<Mioga2Portal>/sg) {
		my $uri = new Mioga2::URI($config, group       => $uri->GetGroupIdent(),
								           public      => $uri->IsPublic(),
								           application => "Portal",
								           method      => "DisplayPortal",
								           args        => { path => $uri->GetWebDavPath() },
								  );

		$r->header_out("Location", $uri->GetURI());
		return REDIRECT;
	}

	$r->warn("==> Mioga2::Portal::Type::handler : XML file is not a portal description. Declined.") if ($debug > 0);
	return DECLINED;
}

# ============================================================================

=head2 Type::handler ($request)

	Apache/mod_perl handler main function. $request is a the Apache request 
	object.

	return an Apache::Constant constant like DECLINED or REDIRECT.
	
=cut

# ============================================================================

sub handler : method {
    my($class, $r) = @_;

	$r->warn("==> Mioga2::Portal::Type::handler") if ($debug > 0);

	if($r->method ne "GET") {
		$r->warn("==> Mioga2::Portal::Type::handler : Not a GET request. Declined.") if ($debug > 0);
		return DECLINED;
	}

	if(! $r->is_initial_req) {
		my $main_req = $r->main();
		if(defined $main_req  and $main_req->method ne 'GET') {
			$r->warn("==> Mioga2::Portal::Type::handler : Not a initial GET request. Declined.") if ($debug > 0);
			return DECLINED;
		}
	}

	my $bench;
	if($timer) {
		my $uri = $r->uri;		
		$uri =~ s!^.*((/[^/]+){2})$!$1!g;

		$bench = new Benchmark::Timer();
		$bench->start("Mioga2::Portal::Type $$ ($uri)");
	}
	
	my $type = $r->content_type;
	if( (defined $type and $type ne 'application/xml') or 
		($r->uri !~ /.xml$/)
	  ) {
		$r->warn("==> Mioga2::Portal::Type::handler : Not an XML file. Declined.") if ($debug > 0);
		return DECLINED;
	}

	my $type_return = DECLINED;
	try {

		my $uri = $r->pnotes("uri");
		my $config = $r->pnotes("config");
		if(!defined $config) {
			($config, $uri) = CreateConfig($r);
		}

		if(! $uri->IsWebDav()) {
			$r->warn("==> Mioga2::Portal::Type::handler : Not an WebDav Path. Declined.") if ($debug > 0);
			return;
		}

		my $path;
		if($uri->IsPublic()) {
			$path = $config->GetPublicDir();
		}
		else {
			$path = $config->GetPrivateDir();
		}

		$path .= "/" . $uri->GetGroupIdent() . "/" .$uri->GetWebDavPath();
		if(! -f $path) {
			$r->warn("==> Mioga2::Portal::Type::handler : File does not exists or not a file. Declined.") if ($debug > 0);
			return;
		}
		
		$type_return = CheckXMLFile($r, $config, $uri, $path);
	}
	
	otherwise {
		my $err = shift;

		$r->log_error("==> Mioga2::Authen::handler : Other exception ");
		$r->log_error(Dumper($err));

		return SERVER_ERROR;
	};


	if($timer)  {
		$bench->stop();
		$bench->report();
	}

	return $type_return;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga.conf Mioga2

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
