# ============================================================================
# Mioga2::Portal Project (C) 2004-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
	
NewsMioglet.pm: News mioglets for Mioga2::Portal

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Portal::NewsMioglet;
use strict;

use base qw(Mioga2::Portal::Mioglet);
use Locale::TextDomain::UTF8 'portal_newsmioglet';

use Error qw(:try);
use Data::Dumper;
use POSIX qw(strftime tzset);
use Mioga2::AnimGroup;
use Mioga2::Portal::Exception;
use Mioga2::tools::APIAuthz;
use Mioga2::Exception::Simple;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Mioga2::tools::APIAuthz;
use LWP::UserAgent;
use Mioga2::XML::Simple;

my $debug = 0;

my %MiogletDesc = (
  ident       => 'News',
  package     => 'Mioga2::Portal::NewsMioglet',
  xml_prefix  => 'news',
  application => 'Mioga2::News',
  methods     => { 'Read' => [ 'NewsList', 'RSSFeed' ] },

);

my %DefaultXML = (
                   NewsList => {
                                 mode           => "user",
                                 titled         => {},
                                 title          => "",
                                 boxed          => {},
                                 show_title     => 1,
                                 show_date      => 1,
                                 show_content   => 1,
                                 show_author    => 1,
                                 delay          => 90,
                   },
                   RSSFeed => {
                                mode   => "user",
                                titled => {},
                                title  => "",
                                boxed  => {},
                   },
);

# ============================================================================

=head2 GetDefaultXML ()
	
	Class method returning default XML chunk of Mioglets (for portal editor).

	
=cut

# ============================================================================

sub GetDefaultXML
{
  my ( $class, $miog_desc, $method_ident ) = @_;

  $DefaultXML{$method_ident}->{stylesheet} = "news_mioglet.xsl";

  my $xml = st_HashToXML( $DefaultXML{$method_ident} );
  $xml = "<mioglet type=\"News/$method_ident\">$xml</mioglet>";

  return $xml;
}

# ============================================================================

=head2 GetMiogletDesc ()

	Return the current application description.

=cut

# ============================================================================

sub GetMiogletDesc
{
  my ( $self, $context ) = @_;

  return \%MiogletDesc;
}

# ============================================================================

=head2 CheckUserAccessOnMethod ($self, $context, $user, $group, $method_ident)

	Check if the current user can access to the current mioglet and
	methods in the current group, depending of execution mode.
	
=cut

# ============================================================================

sub CheckUserAccessOnMethod
{
  my ( $self, $context, $user, $group, $method_ident ) = @_;
  if ( $method_ident eq 'NewsList' || $method_ident eq 'RSSFeed' )
  {
    return AUTHZ_OK;
  }

  return $self->SUPER::CheckUserAccessOnMethod( $context, $user, $group,
                                                $method_ident );
}

# ============================================================================

=head2 RSSFeed ()

	Display n last entries of a rss feed.

=cut

# ============================================================================

sub RSSFeed
{
  my ( $self, $context ) = @_;
  my $config   = $context->GetConfig();
  my $prefix   = $self->{mioglet_desc}->GetPrefix();
  my $user     = $context->GetUser();
  my $feed     = $self->{args}->{feed};
  my $size     = $self->{size};
  my $xml      = "<$prefix:RSSFeed>";
  my $session  = $context->GetSession();
  my $in_cache = 0;
  my $delta    = 5 * 60;
  my $rdf      = 0;
  my $req;
  my $res;

  my $ua = LWP::UserAgent->new;
  $ua->agent("RSS Feed Mioga2 Portal Mioglet/1.0");
  $ua->timeout(10);

  # return nothing if no url given
  if ( ref($feed) eq "HASH" or $feed =~ /^\s*$/ )
  {
    $xml .= "</$prefix:RSSFeed>";
    return $xml;
  }

  # LWP needs 'http'
  $feed = "http://" . $feed if ( not $feed =~ /^http:\/\// );

  # if $size not defined, defaults to 10
  $size = 5 if ( $size =~ /^\s*$/ || !( $size =~ /^\d+$/ ) || ref($size) eq "HASH" );

  # check session
  if ( $session->{$feed} )
  {
    if ( $session->{$feed}{timestamp} + $delta < time() )
    {
      delete $session->{$feed};
      $in_cache = 0;
      warn "Deleting key and not using cache" if ($debug);
      $req = HTTP::Request->new( GET => $feed );
      $res = $ua->request($req);
      if ( $res->is_error )
      {
        $xml .= "<error>" . __("Error while connecting") . "</error>";
        $xml .= "</$prefix:RSSFeed>";
        return $xml;
      }
    }
    else
    {
      $in_cache = 1;
      warn "Setting cache to : True" if ($debug);
    }
  }
  else
  {
    $req = HTTP::Request->new( GET => $feed );
    $res = $ua->request($req);
    if ( $res->is_error )
    {
      $xml .= "<error>" . __("Error while connecting") . "</error>";
      $xml .= "</$prefix:RSSFeed>";
      return $xml;
    }
  }

  if ( $in_cache == 0 )
  {
    if ( $res->is_success )
    {
      warn "Making request, not using cache" if ($debug);
      my $parser  = new Mioga2::XML::Simple( forcearray => 1 );
      my $xmltree = $parser->XMLin( $res->content );

      my $version = $xmltree->{version};
      my $title   = $xmltree->{channel}[0]->{title}[0];

      if ( $title eq "" )
      {
        $xml .=
"<error>" . __("Error, received data isn't well formatted.") . "</error>";
        $xml .= "</$prefix:RSSFeed>";
        return $xml;
      }

      $rdf = 1 if ( $xmltree->{item} );

      $session->{$feed}{timestamp} = time();
      $session->{$feed}{version}   = $version;
      $session->{$feed}{title}     = $title;

      $xml .= "<title>" . $title . "</title>";

      if ($rdf)
      {
        $size = @{ $xmltree->{item} } if ( $size > @{ $xmltree->{item} } );
      }
      else
      {
        $size = @{ $xmltree->{channel}[0]->{item} }
          if ( $size > @{ $xmltree->{channel}[0]->{item} } );
      }

      for ( my $i = 0 ; $i < $size ; $i++ )
      {
        my $item;
        if ($rdf) { $item = $xmltree->{item}[$i]; }
        else { $item = $xmltree->{channel}[0]->{item}[$i]; }

        $xml .=
            "<item link='"
          . $item->{link}[0] . "'>"
          . $item->{title}[0]
          . "</item>";
        push( @{ $session->{$feed}{item} }, $item );
      }
    }
    else
    {
      $xml .= "<error>" . $res->status_line . "</error>";
    }
  }
  else
  {
    warn "Using cache, not making a request" if ($debug);
    $size = @{ $session->{$feed}{item} }
      if ( $size > @{ $session->{$feed}{item} } );

    for ( my $i = 0 ; $i < $size ; $i++ )
    {
      my $item = $session->{$feed}{item}[$i];
      $xml .=
          "<item link='"
        . $item->{link}[0] . "'>"
        . $item->{title}[0]
        . "</item>";
    }
  }

  $xml .= "</$prefix:RSSFeed>";
  return $xml;
}

# ============================================================================

=head2 NewsList ()

	Display last news of :
	 - In user mode : all groups where the user can read the news (support exception).
	 - In group mode : the current group news.

=cut

# ============================================================================

sub NewsList
{
  my ( $self, $context ) = @_;
  my $config       = $context->GetConfig();
  my $prefix       = $self->{mioglet_desc}->GetPrefix();
  my $user         = $context->GetUser();
  my $show_title   = $self->{args}->{show_title};
  my $show_date    = $self->{args}->{show_date};
  my $show_author  = $self->{args}->{show_author};
  my $show_content = $self->{args}->{show_content};
  my $delay        = $self->{args}->{delay};

  my $group_list;
  if ( $self->{mode} eq 'group' )
  {
    $group_list = new Mioga2::Old::GroupList( $config,
                           rowid_list => [ $context->GetGroup()->GetRowid() ] );
  }
  else
  {
    $group_list = $user->GetExpandedGroups();
  }

  my $news = [];

  my $group_ids    = $group_list->GetRowids();
  my $group_idents = $group_list->GetIdents();
  my $group_descs  = $group_list->GetDescriptions();

  my $nb_groups = @$group_ids;

  for ( my $i = 0 ; $i < $nb_groups ; $i++ )
  {
    my $group_id    = $group_ids->[$i];
    my $group_ident = $group_idents->[$i];
    my $group_desc  = $group_descs->[$i];

    if (
         !ProfileTestAuthzFunction(
                                    $config->GetDBH(), $user->GetRowid(),
                                    $group_id,         'News',
                                    'Read'
         )
      )
    {
      next;
    }

    my $grp_news = $self->NewsGetNews( $config, $group_id, 0, $delay );

    push @$news, map {
      $_->{group_ident} = $group_ident;
      $_->{group_desc}  = $group_desc;
      $_
    } @$grp_news;
  }

  @$news = sort {
    du_ISOToSecond( $b->{modified} ) <=> du_ISOToSecond( $a->{modified} )
  } @$news;

  if ( exists $self->{size} )
  {
    my $size = $self->{size};
    if ( @$news < $size )
    {
      $size = @$news;
    }

    $size--;

    @$news = @$news[ 0 .. $size ];
  }

  my $xml   = "<$prefix:NewsList>";

  foreach my $new (@$news)
  {
    $xml .= "<news>";

    my $creator = new Mioga2::Old::User( $config, rowid => $new->{owner} );

    foreach my $key (qw(group_ident group_desc title text))
    {
      next if ( $key eq "title" && $show_title ne "on" );
      next
        if ( $key eq "text" && $show_content ne "on" );
      $xml .= "<$key>" . st_FormatXMLString( $new->{$key} ) . "</$key>";
    }

    if ( $show_date eq "on" )
    {
      foreach my $key (qw(created modified))
      {
        $xml .= "<$key>";
        $xml .= du_ISOToXMLInUserLocale( $new->{$key}, $creator );
        $xml .= "</$key>";
      }
    }

    if ( $show_author eq "on" )
    {
      $xml .=
          "<creator_name>"
        . st_FormatXMLString( $creator->GetName() )
        . "</creator_name>";
      $xml .=
          "<creator_email>"
        . st_FormatXMLString( $creator->GetEmail() )
        . "</creator_email>";
    }

    $xml .= "</news>";
  }

  $xml .= "</$prefix:NewsList>";

  return $xml;
}

# ============================================================================

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head1 AUTHORS

	The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

	Mioga2::Application Mioga2::Portal::Mioglet Mioga2::Portal

=head1 COPYRIGHT

	Copyright (C) 2004-2007, The Mioga2 Project. All Rights Reserved.

	This module is free software; you can redistribute it and/or
	modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
