# ============================================================================
# Mioga2::Portal Project (C) 2004-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
OrgMioglet.pm: Organizer mioglets for Mioga2::Portal

=head1 METHODS DESRIPTION

=cut

package Mioga2::Portal::OrgMioglet;
use strict;

use base qw(Mioga2::Portal::Mioglet);
use Locale::TextDomain::UTF8 'portal_orgmioglet';

use Error qw(:try);
use Data::Dumper;
use POSIX qw(strftime tzset);
use Mioga2::AnimGroup;
use Mioga2::Portal::Exception;
use Mioga2::tools::APIAuthz;
use Mioga2::Exception::Simple;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;

my $debug = 0;

my %MiogletDesc = ( ident   => 'Organizer',
					package => 'Mioga2::Portal::OrgMioglet',
					xml_prefix   => 'org',
					application => 'Mioga2::Organizer',
					methods  => { 'Read' => [ 'Calendar', 'TaskList', 'TodoList', 'EventList'] },
					
					);



my %DefaultXML = (Calendar => { mode => "user",
							  },

				  TaskList => { mode => "user",
								titled => {},
								title => "",
								boxed => {},
						       },

				  TodoList => { mode => "user",
								titled => {},
								title => "",
								boxed => {},
							},

				  EventList => { mode => "user",
								titled => {},
								title => "",
								boxed => {},
						       },

				  );



# ============================================================================

=head2 GetDefaultXML ()
	
	Class method returning default XML chunk of Mioglets (for portal editor).

	
=cut

# ============================================================================
	
sub GetDefaultXML {
	my ($class, $miog_desc, $method_ident) = @_;	

	$DefaultXML{$method_ident}->{stylesheet} = "org_mioglet.xsl";

	my $xml = st_HashToXML($DefaultXML{$method_ident});
	$xml = "<mioglet type=\"Organizer/$method_ident\">$xml</mioglet>";

	return $xml;
}

# ============================================================================

=head2 GetMiogletDesc ()

	Return the current application description.

=cut

# ============================================================================

sub GetMiogletDesc {
	my ($self, $context) = @_;

	return \%MiogletDesc;
}


# ============================================================================

=head2 Calendar ()

	The standard Mioga2::Organizer Calendar.

=cut

# ============================================================================

sub Calendar {
	my ($self, $context) = @_;

	my $prefix = $self->{mioglet_desc}->GetPrefix();

	my @date;
	{
		local $ENV{TZ} = $context->GetUser()->GetTimezone();
		tzset();

		@date = localtime();
	}
	tzset();
		
	my ($curmonth, $curyear) = @date[4,5];
	$curmonth++;
	$curyear += 1900;

	my $last_day = du_GetMonthDuration($curmonth, $curyear);
	
	my $first_dow = strftime("%u", 0, 0, 0, 1, $curmonth-1, $curyear-1900);
	my $last_dow  = strftime("%u", 0, 0, 0, $last_day, $curmonth-1, $curyear-1900);
	
	my $xml = "<$prefix:Calendar>";

	$xml .= "<Calendar>";
	$xml .= "<firstDay>$first_dow</firstDay>";
	$xml .= "<lastDay>$last_dow</lastDay>";

	for(my $day = 1; $day <= $last_day; ($day == 1)?($day += (7-$first_dow+1)):($day += 7)) {
		my $week = strftime("%V", 0, 0, 0, $day, $curmonth-1, $curyear-1900);
		$xml .= "<Week>$week</Week>";
	}

	if(length($curmonth) == 1) {
		$curmonth = "0$curmonth";
	}

	my $dates = du_GetNotWorkingDayForUserBetween($context->GetConfig(),
												  "$curyear-$curmonth-01 00:00:00", 
												  "$curyear-$curmonth-$last_day 23:59:59",
												  $context->GetUser());

	foreach my $date (@$dates) {
		my $daynum = strftime("%d", gmtime($date));
		$xml .= "<NotWorked>".int($daynum)."</NotWorked>";
	}

	$xml .= "</Calendar>";

	$xml .= "</$prefix:Calendar>";

	return $xml;
}


# ============================================================================

=head2 TaskList ()

	The standard Mioga2::Organizer Task List of the current day for the current
	group or user (depending of mode).

=cut

# ============================================================================

sub TaskList {
	my ($self, $context) = @_;

	my $user = $context->GetUser();
	
	my $group;
	if($self->{mode} eq 'group') {
		$group = $context->GetGroup();
	}
	else {
		$group = $context->GetUser();
	}

	my $starttime;
	my $stoptime;
	my $day;

	{
		local $ENV{TZ} = $user->GetTimezone();
		tzset();
		
		my @now = localtime();
		$now[2]--;
			
		$starttime = strftime("%Y-%m-%d 00:00:00", @now);
		$stoptime  = strftime("%Y-%m-%d 23:59:59", @now);
		
		$day = strftime("%Y-%m-%d", @now);
	}
	tzset();

	my $tasks = $self->Org_ListStrictPeriodicTasks($context, $group->GetRowid(), $user->GetTimezone(), 
												   $starttime, $stoptime);

	my $np_tasks = $self->Org_ListNotPlannedTasks($context, $group->GetRowid(), $day, $day);

	unshift @$tasks, @$np_tasks;
	
	if(exists $self->{size}) {
		my $size = $self->{size};

		if(@$tasks < $size) {
			$size = @$tasks;
		}

		$size --;

		@$tasks = @$tasks[0..$size];
	}
	
	my $prefix = $self->{mioglet_desc}->GetPrefix();

	my $xml = "<$prefix:TaskList>";

	foreach my $task (@$tasks) {
		$xml .= "<task>";

		foreach my $key (qw(type duration name first_name last_name fgcolor bgcolor private rowid is_planned)) {
			$xml .= "<$key>".st_FormatXMLString($task->{$key})."</$key>";
		}

		if(exists $task->{start} and $task->{start} ne '') {
			
			$xml .= "<start>";
			$xml .= du_ISOToXMLInUserLocale($task->{start}, $user);
			$xml .= "</start>";

			if(exists $task->{duration} and $task->{duration} != 0) {
				my $stop = du_ISOToSecond($task->{start}) + $task->{duration};
				
				$xml .= "<stop>";
				$xml .= du_GetDateXMLInUserLocale($stop, $user);
				$xml .= "</stop>";
			}
		}

		$xml .= "</task>";
	}

	$xml .= "</$prefix:TaskList>";

	return $xml;
}


# ============================================================================

=head2 TodoList ()

	The standard Mioga2::Organizer Todo Task List for the current
	group or user (depending of mode).

=cut

# ============================================================================

sub TodoList {
	my ($self, $context) = @_;

	my $user = $context->GetUser();
	
	my $group;
	if($self->{mode} eq 'group') {
		$group = $context->GetGroup();
	}
	else {
		$group = $context->GetUser();
	}

	my $tasks = $self->Org_ListTodoTasks($context, $group->GetRowid());

	if(exists $self->{size}) {
		my $size = $self->{size};

		if(@$tasks < $size) {
			$size = @$tasks;
		}

		$size --;

		@$tasks = @$tasks[0..$size];
	}
	
	my $prefix = $self->{mioglet_desc}->GetPrefix();

	my $xml = "<$prefix:TodoList>";

	foreach my $task (@$tasks) {
		$xml .= "<task>";

		foreach my $key (qw(rowid  done type name fgcolor bgcolor private rowid priority)) {
			$xml .= "<$key>".st_FormatXMLString($task->{$key})."</$key>";
		}

		$xml .= "</task>";
	}

	$xml .= "</$prefix:TodoList>";

	return $xml;
}

# ============================================================================

=head2 EventList ()

	The standard Mioga2::Organizer Event List of the current period for the current
	group or user (depending of mode).

=cut

# ============================================================================

sub EventList {
	my ($self, $context) = @_;

	my $user = $context->GetUser();
	
	my $group;
	if($self->{mode} eq 'group') {
		$group = $context->GetGroup();
	}
	else {
		$group = $context->GetUser();
	}

	my $duration = 1;
	if (defined($self->{args}->{duration}))
	{
		$duration = $self->{args}->{duration};
	}
	if ($duration > 12)
	{
		$duration = 12;
	}
	print STDERR "EventList duration = $duration\n";
	my $starttime;
	my $stoptime;
	my $day;

	{
		local $ENV{TZ} = $user->GetTimezone();
		tzset();
		
		my @now = localtime();
		$now[2]--;
	print STDERR "now = " . Dumper(\@now) . "\n";
			
		$starttime = strftime("%Y-%m-%d 00:00:00", @now);
		$day = strftime("%Y-%m-%d", @now);

		$now[4] += $duration;
		if ($now[4] > 12)
		{
			$now[5]++;
			$now[4] -= 12;
		}
		$stoptime  = strftime("%Y-%m-%d 23:59:59", @now);
	}
	tzset();

	my $tasks = $self->Org_ListStrictPeriodicTasks($context, $group->GetRowid(), $user->GetTimezone(), 
												   $starttime, $stoptime);

	print STDERR "tasks : " . Dumper($tasks) . "\n";
	my $np_tasks = $self->Org_ListNotPlannedTasks($context, $group->GetRowid(), $day, $day);

	unshift @$tasks, @$np_tasks;
	
	if(exists $self->{size}) {
		my $size = $self->{size};

		if(@$tasks < $size) {
			$size = @$tasks;
		}

		$size --;

		@$tasks = @$tasks[0..$size];
	}
	
	my $prefix = $self->{mioglet_desc}->GetPrefix();

	my $xml = "<$prefix:EventList mode=\"$self->{mode}\">";

	foreach my $task (@$tasks) {
		$xml .= "<task>";

		foreach my $key (qw(type duration name description first_name last_name fgcolor bgcolor private rowid is_planned)) {
			$xml .= "<$key>".st_FormatXMLString($task->{$key})."</$key>";
		}

		if(exists $task->{start} and $task->{start} ne '') {
			
			$xml .= "<start>";
			$xml .= du_ISOToXMLInUserLocale($task->{start}, $user);
			$xml .= "</start>";

			if(exists $task->{duration} and $task->{duration} != 0) {
				my $stop = du_ISOToSecond($task->{start}) + $task->{duration};
				
				$xml .= "<stop>";
				$xml .= du_GetDateXMLInUserLocale($stop, $user);
				$xml .= "</stop>";
			}
		}

		$xml .= "</task>";
	}

	$xml .= "</$prefix:EventList>";

	return $xml;
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head1 AUTHORS

	The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

	Mioga2::Application Mioga2::Portal::Mioglet Mioga2::Portal

=head1 COPYRIGHT

	Copyright (C) 2004-2007, The Mioga2 Project. All Rights Reserved.

	This module is free software; you can redistribute it and/or
	modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


