# ============================================================================
# Mioga2::Portal Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description:

=head1 NAME

MiogletDesc.pm: Mioglet description class

=head1 METHODS DESRIPTION

=cut

# ============================================================================
package Mioga2::Portal::MiogletDesc;
use strict;

use Locale::TextDomain::UTF8 'portal_miogletdesc';

use Data::Dumper;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;

my $debug = 0;
# ============================================================================

=head2 new ($config, $ident)

	Create a new MiogletDesc object.

=cut

# ============================================================================
sub new {
    my($class, $config, $ident) = @_;

	my $self = { config => $config};
	bless($self, $class);

	$self->InitializeFromIdent($ident);

	return $self;
}
# ============================================================================

=head2 GetRowid ()

	Return the mioglet rowid

=cut

# ============================================================================
sub GetRowid {
    my($self) = @_;

	return $self->{values}->{rowid};
}
# ============================================================================

=head2 GetIdent ()

	Return the mioglet ident

=cut

# ============================================================================
sub GetIdent {
    my($self) = @_;

	return $self->{values}->{ident};
}
# ============================================================================

=head2 GetPackage ()

	Return the mioglet package

=cut

# ============================================================================
sub GetPackage {
    my($self) = @_;

	return $self->{values}->{package};
}
# ============================================================================

=head2 GetAppPackage ()

	Return the mioglet parent application package

=cut

# ============================================================================
sub GetAppPackage {
    my($self) = @_;

	return $self->{values}->{app_package};
}
# ============================================================================

=head2 GetPrefix ()

	Return the mioglet xml prefix

=cut

# ============================================================================
sub GetPrefix {
    my($self) = @_;

	return $self->{values}->{xml_prefix};
}
# ============================================================================

=head2 CreateObject ($mioglet_dom)

	Create the mioglet object.

=cut

# ============================================================================

sub CreateObject {
    my($self, $mioglet_dom) = @_;
	print STDERR ("MiogletDesc::CreateObject package = " . $self->{values}->{package} . "\n") if ($debug);

	my $app;
	eval "require $self->{values}->{package};
          \$app = $self->{values}->{package}->CreateObject(\$self, \$mioglet_dom);";
	
	if($@) {
		throw Mioga2::Exception::Simple("$self->{values}->{package}", $@);
	}

	return $app;
}
# ============================================================================

=head2 GetFunctionForMethod ($method_ident)

	return the mioga2 application function ident for given method.

=cut

# ============================================================================
sub GetFunctionForMethod {
    my($self, $mioglet_ident) = @_;
	
	my $dbh = $self->{config}->GetDBH();

	my $res = SelectSingle($dbh, "SELECT m_function.ident FROM portal_mioglet_method, m_function ".
						         "WHERE portal_mioglet_method.mioglet_id = $self->{values}->{rowid} AND ".
						         "      portal_mioglet_method.ident = '".st_FormatPostgreSQLString($mioglet_ident)."' AND ".
						         "      m_function.rowid = portal_mioglet_method.function_id");

	if(! defined $res) {
			throw Mioga2::Exception::Simple("Mioga2::AppDesc::GetFunctionForMethod", 
            __x("Unknown method {method} for mioglet {mioglet}", method => $mioglet_ident, mioglet => $self->{values}->{ident}));
	}

	return $res->{ident};
}
# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

=head2 InitializeFromIdent ($ident)

	Initialize the object from mioglet idents

=cut

# ============================================================================
sub InitializeFromIdent {
    my($self, $ident) = @_;
	
	my $dbh = $self->{config}->GetDBH();

	my $res = SelectSingle($dbh, "SELECT portal_mioglet.*, m_application.package AS app_package"
									.  " FROM portal_mioglet, m_application, m_instance_application"
									.  " WHERE portal_mioglet.ident = '".st_FormatPostgreSQLString($ident)."'"
										. " AND m_application.rowid = portal_mioglet.app_id"
										. " AND m_instance_application.application_id = m_application.rowid"
										. " AND m_instance_application.mioga_id = ".$self->{config}->GetMiogaId());

	if(!defined $res) {
		throw Mioga2::Exception::Application("Mioga2::Portal::MiogaDesc::InitializeFromIdent", __x("Unknown mioglet {ident}", ident => $ident));
	}

	$self->{values} = $res;

	return $self;
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

        Error

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
