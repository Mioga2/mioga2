# ============================================================================
# Mioga2::Portal Project (C) 2004-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
	
WSMioglet.pm: WorkSpace mioglets for Mioga2::Portal

=head1 METHODS DESRIPTION

=cut

package Mioga2::Portal::WSMioglet;
use strict;

use base qw(Mioga2::Portal::Mioglet);
use Locale::TextDomain::UTF8 'portal_wsmioglet';

use Error qw(:try);
use Data::Dumper;
use POSIX qw(strftime tzset);
use Mioga2::AnimGroup;
use Mioga2::Portal::Exception;
use Mioga2::tools::APIAuthz;
use Mioga2::Exception::Simple;
use Mioga2::Old::UserList;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::APIWorkspace;

my $debug = 0;

my %MiogletDesc = (
  ident       => 'Workspace',
  package     => 'Mioga2::Portal::WSMioglet',
  xml_prefix  => 'ws',
  application => 'Mioga2::Workspace',
  methods => { 'All' => [ 'Links', 'TopBar', 'ConnectedUsers', 'Weather' ] },

);

my %DefaultXML = (
                   Links => {
                              mode   => "user",
                              titled => {},
                              title  => "",
                              boxed  => {},
                   },
                   TopBar => {
                               mode   => "user",
                               titled => {},
                               title  => "",
                               boxed  => {}
                   },
                   ConnectedUsers => {
                                       mode   => "user",
                                       titled => {},
                                       title  => "",
                                       boxed  => {}
                   },
                   Weather => {
                                mode   => "user",
                                titled => {},
                                title  => "",
                                boxed  => {}
                   },
);

# ============================================================================

=head2 GetDefaultXML ()
	
	Class method returning default XML chunk of Mioglets (for portal editor).

	
=cut

# ============================================================================

sub GetDefaultXML
{
  my ( $class, $miog_desc, $method_ident ) = @_;

  $DefaultXML{$method_ident}->{stylesheet} = "ws_mioglet.xsl";

  my $xml = st_HashToXML( $DefaultXML{$method_ident} );
  $xml = "<mioglet type=\"Workspace/$method_ident\">$xml</mioglet>";

  return $xml;
}

# ============================================================================

=head2 GetMiogletDesc ()

	Return the current application description.

=cut

# ============================================================================

sub GetMiogletDesc
{
  my ( $self, $context ) = @_;

  return \%MiogletDesc;
}

# ============================================================================

=head2 Weather (string $wcode)

	Return informations about weather in $loc location.

=cut

# ============================================================================

sub Weather
{
  my ( $self, $context ) = @_;
  my $prefix = $self->{mioglet_desc}->GetPrefix();
  my $loc    = $self->{args}->{wcode};
  my $xml    = "<$prefix:Weather>";
  my $days   = 3;

  warn Dumper($loc) if ( $debug == 1 );
  if ( $loc =~ /^\s*$/ || ref($loc) eq "HASH" )
  {
    $xml .= "</$prefix:Weather>";
    return $xml;
  }

  my $ua = LWP::UserAgent->new;
  $ua->agent("Weather Mioga2 Portal Mioglet/1.0");
  $ua->timeout(10);

  my $req =
    HTTP::Request->new( GET =>
"http://xoap.weather.com/weather/local/$loc?cc=*=xoap&par=1005561467&key=a388d0d4a8a3f714&unit=m&dayf=$days&link=xoap"
    );
  my $res = $ua->request($req);

  if ( $res->is_error
       || ( $res->is_success && !( $res->header('Content-Type') =~ /(xml)|(plain)/ ) ) )
  {
    $xml .= "<error>" . __("Error while fetching data") . "</error>";
    $xml .= "</$prefix:Weather>";
    return $xml;    
  }

  my $parser  = new Mioga2::XML::Simple( forcearray => 1 );
  my $xmltree = $parser->XMLin( $res->content );

  $xml .=
      "<location>"
    . st_FormatXMLString( $xmltree->{loc}[0]->{dnam}[0] )
    . "</location>";
  $xml .=
      "<current icon='"
    . $xmltree->{cc}[0]->{icon}[0]
    . "' temp='"
    . $xmltree->{cc}[0]->{tmp}[0] . "'>"
    . ucfirst( lc( $xmltree->{cc}[0]->{t}[0] ) )
    . "</current>";

  for ( my $i = 0 ; $i < $days ; $i++ )
  {

#first entry provides today information, so we skip it (already present in <cc>)
    next if ( $i == 0 );

    my $item = $xmltree->{dayf}[0]->{day}[$i];
    my $dw   = $item->{t};
    my ( $month, $day ) = split /\s/, $item->{dt};
    $xml .=
        "<day dw='$dw' d='$day' month='$month' temp='"
      . $item->{hi}[0]
      . "' icon='"
      . $item->{part}[0]->{icon}[0] . "'>"
      . ucfirst( lc( $item->{part}[0]->{t}[0] ) )
      . "</day>";
  }

  $xml .= "</$prefix:Weather>";
  return $xml;
}

# ============================================================================

=head2 ConnectedUsers (int $sec)

	Display a list of connected users since $sec seconds.

=cut

# ============================================================================

sub ConnectedUsers
{
  my ( $self, $context ) = @_;
  my $prefix = $self->{mioglet_desc}->GetPrefix();
  my $sec    = $self->{args}->{sec};
  my $xml    = "<$prefix:ConnectedUsers>";

  $sec = ( 5 * 60 ) if ( $sec =~ /^\s*$/ || ref($sec) eq "HASH" );

  my $users = $context->GetGroup()->GetUserConnectedSince($sec);

  foreach ( @{ $users->GetNames() } )
  {
    $xml .= "<user>$_</user>";
  }

  $xml .= "</$prefix:ConnectedUsers>";
  return $xml;
}

# ============================================================================

=head2 TopBar ()

	Draw the Mioga TopBar.

=cut

# ============================================================================

sub TopBar
{
  my ( $self, $context ) = @_;
  my $prefix = $self->{mioglet_desc}->GetPrefix();

  return "<$prefix:TopBar></$prefix:TopBar>";
}

# ============================================================================

=head2 Links ()

	Display the list of links created by the user.

=cut

# ============================================================================

sub Links
{
  my ( $self, $context ) = @_;

  my $config = $context->GetConfig();
  my $prefix = $self->{mioglet_desc}->GetPrefix();
  my $user   = $context->GetUser();

  my $dbh = $config->GetDBH();

  my $links = WorkspaceGetLinks( $dbh, $user->GetRowid() );

  my $xml = "<$prefix:Links>";

  foreach my $link (@$links)
  {
    $xml .= "<link>";
    $xml .= "<ident>" . st_FormatXMLString( $link->{ident} ) . "</ident>";
    $xml .= "<link>" . st_FormatXMLString( $link->{link} ) . "</link>";
    $xml .= "</link>";
  }

  $xml .= "</$prefix:Links>";

  return $xml;
}

# ============================================================================

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head1 AUTHORS

	The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

	Mioga2::Application Mioga2::Portal::Mioglet Mioga2::Portal

=head1 COPYRIGHT

	Copyright (C) 2004-2007, The Mioga2 Project. All Rights Reserved.

	This module is free software; you can redistribute it and/or
	modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
