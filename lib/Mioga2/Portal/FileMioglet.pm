# ============================================================================
# Mioga2::Portal Project (C) 2005-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
	
FileMioglet.pm: File mioglets for Mioga2::Portal

=head1 METHODS DESRIPTION

=cut

package Mioga2::Portal::FileMioglet;
use strict;

use base qw(Mioga2::Portal::Mioglet);
use Locale::TextDomain::UTF8 'portal_filemioglet';

use Error qw(:try);
use Data::Dumper;
use POSIX qw(strftime tzset);
use Mioga2::AnimGroup;
use Mioga2::Portal::Exception;
use Mioga2::tools::APIAuthz;
use Mioga2::Exception::Simple;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;
use Mioga2::tools::date_utils;
use Mioga2::tools::APIAuthz;
use Mioga2::Magellan::DAV;
use Mioga2::Classes::Time;
use Mioga2::URI;

my $debug = 0;

my %MiogletDesc = (
  ident       => 'File',
  package     => 'Mioga2::Portal::FileMioglet',
  xml_prefix  => 'file',
  application => 'Mioga2::Magellan',
  methods     => { "Read" => ["Folder", "LastModified"] },

);

my %DefaultXML = (
                   Folder => {
                               mode   => "user",
                               titled => {},
                               title  => "",
                               boxed  => {},
                   },
                   LastModified => {
                               mode  => "group",
                               titled => {},
                               title => "",
                               boxed => {},
                               limit => 10,
                   },
);

# ============================================================================

=head2 GetDefaultXML ()
	
	Class method returning default XML chunk of Mioglets (for portal editor).

	
=cut

# ============================================================================

sub GetDefaultXML
{
  my ( $class, $miog_desc, $method_ident ) = @_;

  $DefaultXML{$method_ident}->{stylesheet} = "file_mioglet.xsl";

  my $xml = st_HashToXML( $DefaultXML{$method_ident} );
  $xml = "<mioglet type=\"File/$method_ident\">$xml</mioglet>";

  return $xml;
}

# ============================================================================

=head2 GetMiogletDesc ()

	Return the current application description.

=cut

# ============================================================================

sub GetMiogletDesc
{
  my ( $self, $context ) = @_;

  return \%MiogletDesc;
}

# ============================================================================

=head2 Folder ($folder, $show_files)

	Display $folder contents and its files if $show_files is true.

=cut

# ============================================================================

sub Folder
{
  my ( $self, $context ) = @_;

  my $config     = $context->GetConfig();
  my $uri        = $context->GetURI();
  my $prefix     = $self->{mioglet_desc}->GetPrefix();
  my $folder     = $self->{args}->{folder};
  my $show_files = $self->{args}->{show_files};
  my $use_fb     = $self->{args}->{use_filebrowser};
  my $chroot     = $self->{args}->{chroot};

  my $group = $context->GetGroup();

  my $path;
  if ( $uri->IsPublic() )
  {
    $path = $config->GetPublicURI();
  }
  else
  {
    $path = $config->GetPrivateURI();
  }

  my $xml     = "<$prefix:Folder>";
  my $uri_tmp =
      $config->GetProtocol() . "://"
    . $config->GetServerName()
    . $path . "/"
    . $group->GetIdent();

  if ($folder =~ /^\s*$/ || ref($folder) eq "HASH") {
        $folder = '/';
  }

  $folder =~ /^(\/?.*)\/*$/;
  $folder = $1;

  my $collection =
    Mioga2::Magellan::DAV::GetCollection( $context, $context->GetUserAuth (),
                                        $uri_tmp, $folder );
  # Remove the consistency flag
  pop (@$collection);

  if ($chroot =~ /^\s*$/ || ref($chroot) eq "HASH") {
      $chroot = "";
  }
  else {
      $chroot = "/$chroot" unless $chroot =~ /^\//;
      $chroot = "$path/" . $group->GetIdent . "$chroot";
  }
  
  my @infos = split /\//, $folder;
  foreach ( @{$collection} )
  {
    next if ( $_->{short_name} eq $infos[$#infos] );
    next if ( $_->{type} eq "file" && $show_files ne "on" );
    $xml .=
        "<item type='"
      . $_->{type}
      . "' link='";
      
    if ($use_fb eq "on") {
        $xml .= $config->GetBinURI() . '/' 
                . $context->GetGroup()->GetIdent()
                . '/FileBrowser/DisplayURI?uri=' . $_->{name_esc};
        $xml .= "&amp;root=$chroot" if $chroot;
    }
    else {
        $xml .= $_->{name_esc};
    }
    
    $xml .= "'>"
      . $_->{short_name}
      . "</item>";
  }

  $xml .= "</$prefix:Folder>";
  return $xml;
}

# ============================================================================

=head2 LastModified ()

  Show last modified files in current instance.

=cut

# ============================================================================

sub LastModified {
  my ($self, $context) = @_;
  
  my $config    = $context->GetConfig;
  my $dbh       = $config->GetDBH;
  my $prefix    = $self->{mioglet_desc}->GetPrefix;
  my $user      = $context->GetUser;
  my $group     = $context->GetGroup;
  my $limit     = $self->{args}->{limit};
  my $instance  = $config->GetMiogaId;
  
  $limit = 10 if ($limit !~ /^\d+$/ || ref($limit) eq "HASH");
  
  my $offset      = 0;
  my $offset_step = 50;
            
  my @rowids;
  my $total = SelectSingle($dbh, "SELECT COUNT(m_uri.rowid) FROM m_uri, m_group_base WHERE 
            m_uri.group_id = m_group_base.rowid AND 
            m_group_base.mioga_id = $instance AND m_uri.mimetype <> 'directory'");
  $total  = $total->{count};
  while ($total > 0 && @rowids < $limit) {
    push @rowids, AuthzTestAccessForRequest($config, $user, "SELECT m_uri.rowid FROM m_uri, m_group_base WHERE 
            m_uri.group_id = m_group_base.rowid AND 
            m_group_base.mioga_id = $instance AND m_uri.mimetype <> 'directory' ORDER BY m_uri.modified DESC LIMIT $offset_step OFFSET $offset");
    $offset += $offset_step;
    $total -= $offset_step;
  }
  
  my $ids     = "(" . join(',', @rowids) . ")";
  my $res     = SelectMultiple($dbh, "SELECT m_uri.*, m_user_base.firstname, m_user_base.lastname 
                FROM m_uri, m_user_base 
                WHERE m_uri.rowid IN $ids AND m_uri.user_id = m_user_base.rowid
                ORDER BY modified DESC");
  splice @$res, $limit;
  
  my $xml = "<$prefix:LastModified>";
  foreach my $result (@$res) {
    my $uri   = Mioga2::URI->new($config, uri => $result->{uri});
    next if (-d $uri->GetRealPath);
    
    my $time  = Mioga2::Classes::Time->FromPSQL($result->{modified});
    my @parts = split "/", $uri->GetURI;
    $xml .= "<entry>";
    $xml .= "<uri cn=\"" . st_FormatXMLString($parts[-1]) . "\">" . st_FormatXMLString($uri->GetURI) . "</uri>";
    $xml .= "<modified>" . st_FormatXMLString($time->strftimetz(__"%x %X", $user->GetTimezone)) . "</modified>";
    $xml .= "<user rowid=\"" . st_FormatXMLString($result->{rowid}) . "\">"
         . st_FormatXMLString($result->{firstname}." ".$result->{lastname}) . "</user>";
    $xml .= "</entry>";
  }
  $xml   .= "</$prefix:LastModified>";
  return $xml;
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head1 AUTHORS

	The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

	Mioga2::Application Mioga2::Portal::Mioglet Mioga2::Portal

=head1 COPYRIGHT

	Copyright (C) 2005-2007, The Mioga2 Project. All Rights Reserved.

	This module is free software; you can redistribute it and/or
	modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
