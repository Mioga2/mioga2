# ============================================================================
# Mioga2::Portal Project (C) 2006-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
	
ArticlesMioglet.pm: Articles mioglets for Mioga2::Portal

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Portal::ArticlesMioglet;
use strict;

use base qw(Mioga2::Portal::Mioglet);
use Locale::TextDomain::UTF8 'portal_articlesmioglet';

use Error qw(:try);
use Data::Dumper;
use POSIX qw(strftime tzset);
use Mioga2::AnimGroup;
use Mioga2::Portal::Exception;
use Mioga2::tools::APIAuthz;
use Mioga2::Exception::Simple;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;

my $debug = 0;

my %MiogletDesc = (
  ident       => 'Articles',
  package     => 'Mioga2::Portal::ArticlesMioglet',
  xml_prefix  => 'articles',
  application => 'Mioga2::Articles',
  methods     => { 'Read' => [ 'DisplayArticle' ] },

);

my %DefaultXML = (
                   DisplayArticle => {
                                 mode   => "group",
                                 titled => {},
                                 title  => "",
                                 boxed  => {},
                   },
);

# ============================================================================

=head2 GetDefaultXML ()
	
	Class method returning default XML chunk of Mioglets (for portal editor).

	
=cut

# ============================================================================

sub GetDefaultXML
{
  my ( $class, $miog_desc, $method_ident ) = @_;

  $DefaultXML{$method_ident}->{stylesheet} = "articles_mioglet.xsl";

  my $xml = st_HashToXML( $DefaultXML{$method_ident} );
  $xml = "<mioglet type=\"Articles/$method_ident\">$xml</mioglet>";
  warn("ArticlesMioglet::GetDefaultXML xml = $xml") if ($debug);

  return $xml;
}

# ============================================================================

=head2 GetMiogletDesc ()

	Return the current application description.

=cut

# ============================================================================
sub GetMiogletDesc
{
  my ( $self, $context ) = @_;

  return \%MiogletDesc;
}
# ============================================================================

=head2 CheckUserAccessOnMethod ($self, $context, $user, $group, $method_ident)

	Check if the current user can access to the current mioglet and
	methods in the current group, depending of execution mode.
	
=cut

# ============================================================================
sub CheckUserAccessOnMethod
{
  my ( $self, $context, $user, $group, $method_ident ) = @_;
  if ( $method_ident eq 'DisplayArticle' )
  {
    return AUTHZ_OK;
  }

  return $self->SUPER::CheckUserAccessOnMethod( $context, $user, $group,
                                                $method_ident );
}
# ============================================================================

=head2 DisplayArticle ()

	Display last articles of :
	 - In user mode : all groups where the user can read the articles (support exception).
	 - In group mode : the current group articles.

=cut

# ============================================================================
sub DisplayArticle
{
	my ( $self, $context ) = @_;
	warn("ArticlesMioglet::DisplayArticle self = " . Dumper($self) . "\n") if ($debug);

	my $config       = $context->GetConfig();
	my $prefix       = $self->{mioglet_desc}->GetPrefix();
	my $user         = $context->GetUser();
	my $group_id     = $context->GetGroup()->GetRowid();

	my $category;
	my $number;
	if (exists($self->{args}->{category}) and ref($self->{args}->{category}) ne 'HASH')  {
		$category     = $self->{args}->{category};
	}
	if (exists($self->{args}->{number}) and ref($self->{args}->{number}) ne 'HASH')  {
		$number     = $self->{args}->{number};
	}
	warn("ArticlesMioglet::DisplayArticle category = $category\n") if ($debug);
	warn("ArticlesMioglet::DisplayArticle number = $number\n") if ($debug);

	my $xml = "<$prefix:DisplayArticle>";
	#
	# Get the right article, function of mioglet parameters
	#
	my $sql;
	if (defined($category)) {
		$sql =  "SELECT title, header, contents FROM article,article_category,article_category_list,article_pref"
				. " WHERE article.rowid=article_category.article_id"
					. " AND article_category.category_id=article_category_list.rowid"
					. " AND article_category_list.label='" . st_FormatPostgreSQLString($category) . "'"
					. " AND article_category_list.article_pref_id=article_pref.rowid"
					. " AND article_pref.group_id=$group_id ORDER BY article.modified DESC LIMIT 1";
	}
	elsif (defined($number)) {
		$sql .= "SELECT title, header, contents FROM article WHERE rowid=$number and group_id=$group_id";
	}
	else {
		$sql .= "SELECT title, header, contents FROM article WHERE group_id=$group_id ORDER BY modified DESC LIMIT 1";
	}
	warn("ArticlesMioglet::DisplayArticle sql = $sql\n") if ($debug);
	my $result = SelectSingle($config->GetDBH(), $sql);
	warn("ArticlesMioglet::DisplayArticle result = " . Dumper($result) . "\n") if ($debug);
	if (defined($result)) {
		$xml .= "<title>" . st_FormatXMLString($result->{title}) . "</title>";
		$xml .= "<header>" . st_FormatXMLString($result->{header}) . "</header>";
		$xml .= "<content>" . st_FormatXMLString($result->{contents}) . "</content>";
	}
	
	$xml .= "</$prefix:DisplayArticle>";
	warn("ArticlesMioglet::DisplayArticle xml = $xml\n") if ($debug);

	return $xml;
}

# ============================================================================

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head1 AUTHORS

	The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

	Mioga2::Application Mioga2::Portal::Mioglet Mioga2::Portal

=head1 COPYRIGHT

	Copyright (C) 2006-2007, The Mioga2 Project. All Rights Reserved.

	This module is free software; you can redistribute it and/or
	modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
