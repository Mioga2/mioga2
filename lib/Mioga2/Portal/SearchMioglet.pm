# ============================================================================
# Mioga2::Portal Project (C) 2004 The MiogaII Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#	Description: 

=head1 NAME
	
SearchMioglet.pm : Search mioglets for Mioga2::Portal

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Portal::SearchMioglet;

use strict;
use vars qw(@ISA);
use Error qw(:try);
use Data::Dumper;
use POSIX qw(strftime tzset);
use Mioga2::AnimGroup;
use Mioga2::Portal::Mioglet;
use Mioga2::Portal::Exception;
use Mioga2::tools::APIAuthz;
use Mioga2::Exception::Simple;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Mioga2::tools::APIAuthz;

@ISA = ('Mioga2::Portal::Mioglet');

my $debug = 0;

my %MiogletDesc = ( ident   => 'Search',
					package => 'Mioga2::Portal::SearchMioglet',
					xml_prefix   => 'search',
					application => 'Mioga2::Search',
					methods  => { 'Standard' => [ 'SearchForm'] },
					
					);



my %DefaultXML = (SearchForm => { mode => "user",
								  titled => {},
								  title => "",
								  boxed => {},
						     },
				  );



# ============================================================================

=head2 GetDefaultXML ()
	
	Class method returning default XML chunk of Mioglets (for portal editor).

	
=cut

# ============================================================================
	
sub GetDefaultXML {
	my ($class, $miog_desc, $method_ident) = @_;	

	$DefaultXML{$method_ident}->{stylesheet} = "search_mioglet.xsl";

	my $xml = st_HashToXML($DefaultXML{$method_ident});
	$xml = "<mioglet type=\"Search/$method_ident\">$xml</mioglet>";

	return $xml;
}


# ============================================================================

=head2 GetMiogletDesc ()

	Return the current application description.

=cut

# ============================================================================

sub GetMiogletDesc {
	my ($self, $context) = @_;

	return \%MiogletDesc;
}


# ============================================================================

=head2 CheckUserAccessOnMethod ($self, $context, $user, $group, $method_ident)

	Check if the current user can access to the current mioglet and
	methods in the current group, depending of execution mode.
	
=cut

# ============================================================================

sub CheckUserAccessOnMethod {
	my ($self, $context, $user, $group, $method_ident) = @_;
	
	return AUTHZ_OK;
}


# ============================================================================

=head2 SearchForm ()

	Display a Mioga2 search form.

=cut

# ============================================================================

sub SearchForm {
	my ($self, $context) = @_;

	my $prefix = $self->{mioglet_desc}->GetPrefix();

	return "<search:SearchForm/>";
}



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head1 AUTHORS

	The MiogaII Project <developers@mioga2.org>

=head1 SEE ALSO

	Mioga2::Application Mioga2::Portal::Mioglet Mioga2::Portal

=head1 COPYRIGHT

	Copyright (C) 2003, The MiogaII Project. All Rights Reserved.

	This module is free software; you can redistribute it and/or
	modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
