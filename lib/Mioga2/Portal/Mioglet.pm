# ============================================================================
# Mioga2::Portal Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description:

=head1 NAME

	MiogletDesc.pm: Mioglet base class

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
package Mioga2::Portal::Mioglet;
use strict;

use Locale::TextDomain::UTF8 'portal_mioglet';

my $debug=0;

use Data::Dumper;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::string_utils;
use Error qw(:try);
use vars qw (@ISA);
# ============================================================================

=head2 new ( )

	Create a new Mioglet object.

=cut

# ============================================================================
sub new {
	my($class) = @_;
	
	my $self = {};
	bless($self, $class);
	
	return $self;
}
# ============================================================================

=head2 CreateObject ( $mioglet_desc, $mioglet_dom)

	Create a new Mioglet object.

=cut

# ============================================================================
sub CreateObject {
	my($class, $mioglet_desc, $mioglet_dom) = @_;
	
	my $app_package = $mioglet_desc->GetAppPackage();

	eval "require $app_package;";
	
	@ISA = ($app_package);
	if($@) {
		throw Mioga2::Exception::Simple("Mioga2::Portal::Mioglet::new", $@);
	}
	
	my $self = { mioglet_desc => $mioglet_desc, mioglet_dom => $mioglet_dom };
	bless($self, $class);
	
	$self->InitializeStdParams($mioglet_dom);
	$self->Initialize($mioglet_dom);
	
	return $self;
}
# ============================================================================

=head2 InitializeStdParams($mioglet_dom)
	
	Initialize object from mioglet_dom.
	
=cut
	
# ============================================================================
sub InitializeStdParams {
	my ($self, $mioglet_dom) = @_;

	my $type = $mioglet_dom->getAttribute("type");
	my ($ident, $method) = ($type =~ m!^([^/]+)/([^/]+)$!);

	$self->{mioglet_method} = $method;

	my $res = st_XMLDOMToHash($mioglet_dom, ['mode', 'size'], []);

	foreach my $key (keys %$res) {
		$self->{$key} = $res->{$key};
	}

	if(exists $self->{size} and ref($self->{size}) eq 'HASH') {
		$self->{size} = "";
	}

	if(! exists $self->{mode}) {
		throw Mioga2::Exception::Simple("Mioga2::Portal::Mioglet::InitializeStdParams", __"Missing mode parameter");
	}

	return;
}
# ============================================================================

=head2 Initialize ($mioglet_dom)

	Method that can be overloaded by Application implementation to initialize
	variable, resource, etc, at the object creation time.

	By default, this function convert mioglet dom tree to perl hash.
	Result is stored in $self->{args}

	Return nothing.

=cut

# ============================================================================
sub Initialize {
	my ($self, $mioglet_dom) = @_;

	$self->{args} = st_XMLDOMToHash($mioglet_dom);

	return;
}
# ============================================================================

=head2 TestAuthorize ($context)

	Check if the current user can access to the current mioglet and
	methods in the current group, depending of execution mode.
	
=cut

# ============================================================================
sub TestAuthorize {
	my ($self, $context) = @_;
	
	my $config = $context->GetConfig();
	my $uri = $context->GetURI();

	my $group = $context->GetGroup();
	my $user  = $context->GetUser();

	if($self->{mode} eq 'user' and $uri->IsPublic()) {
		return AUTHZ_FORBIDDEN;
	}


	elsif($uri->IsPublic()) {
		my $appdesc = new Mioga2::AppDesc($config, package => $self->{mioglet_desc}->GetPackage());
		
		if($appdesc->IsPublicForGroup($config, $group->GetRowid())) {
			return AUTHZ_OK;
		}
		else {
			return AUTHZ_FORBIDDEN;
		}
	}
	
	elsif($self->{mode} eq 'user') {
		return $self->CheckUserAccessOnMethod($context, $user, $user,  $self->{mioglet_method});
	}
	
	elsif($self->{mode} eq 'group') {
		return $self->CheckUserAccessOnMethod($context, $user, $group, $self->{mioglet_method});
	}
	else {
		warn "Unknown mioglet schemes. mode parameter not supported ?";
	}

	return AUTHZ_FORBIDDEN;
}
# ============================================================================

=head2 CheckUserAccessOnMethod ($self, $context, $user, $group, $method_ident)

	Check if the current user can access to the current mioglet and
	methods in the current group, depending of execution mode.
	
=cut

# ============================================================================
sub CheckUserAccessOnMethod {
	my ($self, $context, $user, $group, $method_ident) = @_;
	
	my $config = $context->GetConfig();
	my $dbh    = $config->GetDBH();


	my $method;
	try {
		$method = $self->{mioglet_desc}->GetFunctionForMethod($method_ident);
	}
	otherwise {
	};

	if(!defined $method) {
		return AUTHZ_FORBIDDEN;
	}
	
	return $self->CheckUserAccessOnFunction($context, $user, $group, $method);
}
# ============================================================================

=head1 AUTHORS

	The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

	Mioga2::Application Mioga2::Portal

=head1 COPYRIGHT

	Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

	This module is free software; you can redistribute it and/or
	modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
