# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
# Description:

=head1 NAME

Mioga2::LargeList::RowidList - package used to build simple large list 
	from user/group/resource rowid list

=cut

package Mioga2::LargeList::RowidList;
use strict;

use base qw(Mioga2::SimpleLargeList);

use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Data::Dumper;


# ============================================================================

=head2 new

    new Mioga2::LargeList::RowidList($context, $params, $rowids);

    Create a new RowidList.

	$rowids is an array containing rowids to display.
	
	See SimpleLargeList for $params

=cut

# ============================================================================

sub new {
	my ($class, $context, $params, $rowids) = @_;

	my $self = $class->SUPER::new($context, $params);

	if(!defined $self) {
		return undef;
	}

	$self->{rowids} = $rowids;

	$self->CreateRequest($context);

	return $self;
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================



# ============================================================================

=head2 CreateRequest

	Create the request from rowid list

=cut

# ============================================================================

sub CreateRequest {
	my ($self, $context) = @_;

	my $rowids = $self->{rowids};

	$self->{sll_params}->{sql_request} =
	{ 
		select =>  '*',
		'select_count' => "count(*)",
		from   => "m_group_base",
		where  => @$rowids?"m_group_base.rowid IN (".join(',', @$rowids).")":"'f'",
	};

	foreach my $field (@{$self->{sll_params}->{fields}}) {
		if(@$field > 2 and $field->[1] =~ /^((user)|(group)|(team)|(resource))_/) {
			next if $field->[2] =~ /\./;
			$field->[2] = "m_group_base.$field->[2]";
		}
	}
}


# ============================================================================

=head2 GetNumberOfElems

    $self->GetNumberOfElems($context);

    Return the number of element to display. 
    This method MUST be overloaded.

    This method is used to process page index.

=cut
    
# ============================================================================

sub GetNumberOfElems {
	my ($self, $context) = @_;

	my $nb_elem = @{$self->{rowids}};

	return $nb_elem;
}


# ============================================================================

=head2 DeleteElem

    $self->DeleteElem($context, $rowid);

    Delete the element with id $rowid.
    
    This method MUST be overloaded.
    
    This method is called after the confirmation page if askConfirmForDelete 
    is set.

=cut
    
# ============================================================================
sub DeleteElem {
	my ($self, $context, $rowid) = @_;

	my $session = $context->GetSession();
	my @list = grep {$_ != $rowid} @{$self->{rowids}};
	@{$self->{rowids}} = @list;
}



# ============================================================================

=head2 RemoveElem

    $self->RemoveElem($context);

    Remove elements.
        
=cut
    
# ==========================================================================='

sub RemoveElem {
	my ($self, $context) = @_;

	if(exists $context->{args}->{sll_delete_act}) {
		
		foreach my $key (keys %{$context->{args}}) {
			next unless $key =~ /select_delete_(\d+)/;
			my $rowid = $1;
			
			$self->DeleteElem($context, $rowid);
		}
	}

	$self->CreateRequest($context);
	
	return $self->DisplayList($context);
}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LargeList Mioga2::SimpleLargeList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
