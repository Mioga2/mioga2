# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
# Description:

=head1 NAME

Mioga2::LargeList::ApplicationList - package used to build simple large list 
	used to view application list with translated name and description.

=cut

package Mioga2::LargeList::ApplicationList;
use strict;

use base qw(Mioga2::SimpleLargeList);

use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Mioga2::AppDescList;
use Data::Dumper;


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 GetElemsList 

    $self->GetElemsList($context, $offset, $sort_field_name, $asc);

    Return <nb_elem_per_page> elements begining to the <offset>th

    Elements must be sorted on the field $sort_field_name.
    If $asc = 0 elements are sorted descending, otherwise elements are sorted ascending.

    This method MUST be overloaded.
    
=cut
    
# ============================================================================
sub GetElemsList {
	my ($self, $context, $offset, $sort_field_name, $asc) = @_;

	my $old_nb_elem = $self->{nb_elem_per_page};
	$self->{nb_elem_per_page} = "1000";
	my $result = $self->SUPER::GetElemsList($context, 0, $sort_field_name, $asc);
	$self->{nb_elem_per_page} = $old_nb_elem;


	if(! @$result) {
		return [];
	}

	my @idents = map {$_->{ident}} @$result;

	my $adl = new Mioga2::AppDescList($context->GetConfig(), ident_list => \@idents);
	
	my $locales = $adl->GetLocaleNames($context->GetUser());

	my $nb_apps = @$locales;
	for(my $i=0; $i < $nb_apps; $i++) {
		$result->[$i]->{realident} = $result->[$i]->{ident};
		$result->[$i]->{ident} = $locales->[$i];
	}

	if($sort_field_name eq "ident") {
		my @sorted_result;
		if($asc) {
			@sorted_result = sort {$a->{ident} cmp $b->{ident}}  @$result;
		}
		else {
			@sorted_result = sort {$b->{ident} cmp $a->{ident}} @$result;
		}
		$result = \@sorted_result;
	}

	my $end = $offset + $old_nb_elem - 1;
	if($end >= @$result) {
		$end = @$result - 1;
	}

	my @real_result = @$result[$offset..$end];

	return \@real_result;
}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LargeList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
