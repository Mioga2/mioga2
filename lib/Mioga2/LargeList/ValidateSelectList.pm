# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
# Description:

=head1 NAME

Mioga2::LargeList::ValidateSelectList - package used to build simple large list 
	used to validate the current selection.

=cut

package Mioga2::LargeList::ValidateSelectList;
use strict;

use base qw(Mioga2::LargeList::RowidList);

use Mioga2::SimpleLargeList;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Data::Dumper;

# ============================================================================

=head2 new

    new Mioga2::LargeList::ValidateSelectList($context, $params);

    Create a new ValidateSelectList.

	$rowids is an array containing rowids to display.
	
	See SimpleLargeList for $params

=cut

# ============================================================================

sub new {
	my ($class, $context, $params) = @_;

	my $session = $context->GetSession();
	my $rowid_list = $session->{Select}->{rowid_list};

	my $newparams = { name => $params->{name},
					  fields => $params->{fields},
  					  search => $params->{search},
				  };

	my $self = $class->SUPER::new($context, $newparams, $rowid_list);

	return $self;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LargeList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
