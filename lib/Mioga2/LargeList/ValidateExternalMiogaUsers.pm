# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
# Description:

=head1 NAME

Mioga2::LargeList::RowidList - package used to build simple large list 
	from user/group/resource rowid list

=cut

package Mioga2::LargeList::ValidateExternalMiogaUsers;
use strict;

use base qw(Mioga2::LargeList::ExternalMiogaUsers);

use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Data::Dumper;


# ============================================================================

=head2 new

    new Mioga2::LargeList::validateExternalMiogaUsers($context, $params, $external_mioga_rowid);

    Create a new ValidateExternalMiogaUsers List.

	$external_mioga_rowid is the external mioga rowid.
	
	See SimpleLargeList for $params

=cut

# ============================================================================

sub new {
	my ($class, $context, $params, $external_mioga_rowid) = @_;

	my $self = $class->SUPER::new($context, $params, $external_mioga_rowid);

	if(!defined $self) {
		return undef;
	}

	$self->InitializeCache($context, $external_mioga_rowid, []);
	$self->CleanUpUserList($context);

	return $self;
}


# ============================================================================

=head2 DeleteElem

    $self->DeleteElem($context, $rowid);

    Delete the element with id $rowid.
    
    This method MUST be overloaded.
    
    This method is called after the confirmation page if askConfirmForDelete 
    is set.

=cut
    
# ============================================================================
sub DeleteElem {
	my ($self, $context, $rowid) = @_;

	my $session = $context->GetSession();
	my $rowid_list = $session->{Select}->{rowid_list};
	my @list = grep {$_ != $rowid} @$rowid_list;
	$session->{Select}->{rowid_list} = \@list;
}



# ============================================================================

=head2 RemoveElem

    $self->RemoveElem($context);

    Remove elements and ask confirmation for deletion
        
=cut
    
# ==========================================================================='

sub RemoveElem {
	my ($self, $context) = @_;

	foreach my $key (keys %{$context->{args}}) {
		next unless $key =~ /select_delete_(\d+)/;
		my $rowid = $1;

		$self->DeleteElem($context, $rowid);
	}

	$self->CleanUpUserList($context);

	return $self->DisplayList($context);
}



# ============================================================================

=head2 CleanUpUserList

    $self->CleanUpUserList();

    Remove not selected user from user list.
        
=cut
    
# ==========================================================================='

sub CleanUpUserList {
	my ($self, $context) = @_;

	my $session = $context->GetSession();

	my $rowid_list = $session->{Select}->{rowid_list};

	my @userlist;

	foreach my $user (@{$session->{LargeList}->{$self->{list_name}}->{ext_user_list_cache}->{data}}) {
		if(grep { $user->{rowid} eq $_ } @$rowid_list) {
			push @userlist, $user;
		}
	}

	$session->{LargeList}->{$self->{list_name}}->{ext_user_list_cache}->{data} = \@userlist;

}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LargeList Mioga2::SimpleLargeList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
