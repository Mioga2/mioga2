# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
# Description:

=head1 NAME

Mioga2::LargeList::Contact - LargeList implementation for the Contact's application

=head1 DESCRIPTION

=cut

package Mioga2::LargeList::Contact;
use strict;

use base qw(Mioga2::LargeList);

use Mioga2::tools::APIAuthz;
use Error qw(:try);
use Data::Dumper;


# ============================================================================
=head2 Initialize

    $self->Initialize($self, $context);

    Initialize LargeList parameters.

    Required parameters are :
      - max_index_size      : The max. diplayed pages.
  
 
    The following  method set default values.
    
    Initialize CAN be overloaded
=cut
    
# ============================================================================

sub Initialize {
	my ($self, $context) = @_;

	$self->SUPER::Initialize($context);

	$self->{default_sort_field} = "name";
}


# ============================================================================
=head2 DisplayList

    $self->DisplayList($context);

    initialize cur_group and run ancestor function

=cut
    
# ============================================================================

sub DisplayList {
	my ($self, $context) = @_;

	my $session = $context->GetSession();

	my $cur_group;
	if(exists  $context->{args}->{group}) {
		$cur_group = $context->{args}->{group};        
	}
	elsif (exists $session->{Contact}->{cur_group}) {
		$cur_group = $session->{Contact}->{cur_group};
	}
	else { 
		$cur_group = $context->GetGroup()->GetRowid();
	}

	my $cur_alpha;
	if(exists $context->{args}->{alpha}) {
		$cur_alpha = $context->{args}->{alpha};
	} 
	elsif(exists $session->{Contact}->{cur_alpha}->{$cur_group}) {
		$cur_alpha = $session->{Contact}->{cur_alpha}->{$cur_group};
	}
	else {
		$cur_alpha = 0;
	}

	$session->{Contact}->{cur_alpha}->{$cur_group} = $cur_alpha;
	$session->{Contact}->{cur_group} = $cur_group;

	if(!exists $context->{args}->{page}) {
		$context->{args}->{page} = 
		    $session->{Contact}->{cur_page}->{$cur_group}->{$cur_alpha} + 1;

		$context->{args}->{page} = 1 unless defined $context->{args}->{page}; 
	}


	my $nb_of_elem = $self->_GetNumberOfElemsForGroup($context, $cur_group);
	my $xml = $self->_GenerateGroupAndAlphaIndex($context, $nb_of_elem);

	$xml .= $self->SUPER::DisplayList($context);

	$session->{Contact}->{cur_page}->{$cur_group}->{$cur_alpha} = $self->{cur_page};

	return $xml;
}



# ============================================================================
=head2 SearchElem

    $self->SearchElem($context, $params, $group, $type);

    Search elements in the list.
    
    $params is a hashref like :
    { field_name => [list of regexp], ...  }

    regexp are simple regular expression
    (* for zero or more characters, ? for zero or one character)
    
    if $group is 1 search in current group, otherwise search in all groups.
    if $type is 1 search contacts begining with pattern, 
    otherwise search contacts containing pattern.

=cut
    
# ============================================================================

sub SearchElem {
	my ($self, $context, $params, $group, $type) = @_;
	
	$self->{_search_elem_group} = $group;
	$self->{_search_elem_type}  = $type;

	return $self->SUPER::SearchElem($context, $params);
}


# ============================================================================
=head2 GetNumberOfElems

    $self->GetNumberOfElems($context);

    Return the number of element to display (for the current group/alpha selection). 
    This method MUST be overloaded.

    This method is used to process page index.

=cut
    
# ============================================================================
sub GetNumberOfElems {
	my ($self, $context) = @_;

	my $session = $context->GetSession();

	my $cur_group = $session->{Contact}->{cur_group};
	my $alpha_ind = $session->{Contact}->{group_alpha_ind}->{$cur_group};
	my $cur_alpha = $session->{Contact}->{cur_alpha}->{$cur_group};

	my $regexp;
	if($alpha_ind->[$cur_alpha] =~ /z/i) {
		$regexp = "([$alpha_ind->[$cur_alpha]])";
	}
	else {
		$regexp = "[$alpha_ind->[$cur_alpha]]";
	}

	my $ctc = new Mioga2::Contact;
	return $ctc->ContactGetNumberMatchingForGroup($context, 
												  $cur_group,
												  { name => ["^$regexp"] });
}


# ============================================================================
=head2 _GetNumberOfElemsForGroup

    $self->_GetNumberOfElemsForGroup($context, $cur_group);

    Return the number of element to display for the given group. 

    This method is used to process group and alpha index.

=cut
    
# ============================================================================
sub _GetNumberOfElemsForGroup {
	my ($self, $context, $cur_group) = @_;

	my $ctc = new Mioga2::Contact;
	return $ctc->ContactGetNumberForGroup($context, $cur_group);
}



# ============================================================================
=head2 GetNumberOfElemsMatching

    $self->GetNumberOfElems($context, $params);

    Return the number of element matching given patterns. 
    This method MUST be overloaded.

    This method is used by SearchElem to process page index.

=cut
    
# ============================================================================
sub GetNumberOfElemsMatching {
	my ($self, $context, $params) = @_;

	my $session = $context->GetSession();

	my $regexp = $self->GetPostgresRegExp($params, $self->{_search_elem_type});

	my $ctc = new Mioga2::Contact;

	if($self->{_search_elem_group} == 1) {
		my $cur_group = $session->{Contact}->{cur_group};

		return $ctc->ContactGetNumberMatchingForGroup($context, $cur_group,
													  $regexp);
	}
	else {
		my $group_list = $context->GetUser()->GetExpandedGroups();
		my $group_id_list = $group_list->GetRowids();
		
		unshift @$group_id_list, $context->GetUser()->GetRowid();

		return $ctc->ContactGetNumberMatching($context, $group_id_list, $regexp);
	}

	
}


# ============================================================================
=head2 GetElemsMatching

    $self->GetElemsMatching($context, $pattern, $offset, $sort_field_name, $asc);

    Return <nb_elem_per_page> elements begining to the <offset>th matching given patterns.

    Elements must be sorted on the field $sort_field_name.
    If $asc = 0 elements are sorted descending, otherwise elements are sorted ascending.

    This method MUST be overloaded.

    See SearchElem for description of $pattern
    
=cut
    
# ============================================================================
sub GetElemsMatching {
	my ($self, $context, $params, $offset, $sort_field_name, $asc) = @_;

	my $session = $context->GetSession();

	my $regexp = $self->GetPostgresRegExp($params, $self->{_search_elem_type});

	my $ctc = new Mioga2::Contact;

	if($self->{_search_elem_group} == 1) {
		my $cur_group = $session->{Contact}->{cur_group};

		return $ctc->ContactGetMatchingForGroupWithOffset($context, $cur_group, 
														  $offset, $self->{nb_elem_per_page},
														  $sort_field_name, $asc,
														  $regexp);
	}
	else {

		my $group_list = $context->GetUser()->GetGroups();
		my $group_id_list = $group_list->GetRowids();
		unshift @$group_id_list, $context->GetUser()->GetRowid();

		return $ctc->ContactGetMatchingWithOffset($context, $group_id_list, 
												 $offset, $self->{nb_elem_per_page},
												 $sort_field_name, $asc,
												 $regexp);
	}
}



# ============================================================================
=head2 GetElemsList 

    $self->GetElemsList($context, $offset, $sort_field_name, $asc);

    Return <nb_elem_per_page> elements begining to the <offset>th
 
    Elements must be sorted on the field $sort_field_name.
    If $asc = 0 elements are sorted descending, otherwise elements are sorted ascending.

    This method MUST be overloaded.
    
=cut
    
# ============================================================================
sub GetElemsList {
	my ($self, $context, $offset, $sort_field_name, $asc) = @_;
	
	my $session = $context->GetSession();

	my $cur_group = $session->{Contact}->{cur_group};
	my $alpha_ind = $session->{Contact}->{group_alpha_ind}->{$cur_group};
	my $cur_alpha = $session->{Contact}->{cur_alpha}->{$cur_group};

	my $ctc = new Mioga2::Contact;

	my $regexp;
	if($alpha_ind->[$cur_alpha] =~ /z/i) {
		$regexp = "(([$alpha_ind->[$cur_alpha]])|([^a-z]))";
	}
	else {
		$regexp = "[$alpha_ind->[$cur_alpha]]";
	}

	return $ctc->ContactGetMatchingForGroupWithOffset($context, $cur_group,
													  $offset, $self->{nb_elem_per_page}, 
													  $sort_field_name, $asc,
													  { name => ["^$regexp"] });
}


# ============================================================================
=head2 GetElem

    $self->GetElem($context, $rowid);

    Return a hash containing information about element $rowid
    This method MUST be overloaded IF allow_view is set.

=cut
    
# ============================================================================
sub GetElem {
	my ($self, $context, $rowid) = @_;

	my $ctcapp = new Mioga2::Contact;
	my $ctc = $ctcapp->ContactGetContact($context, $rowid);

	my $group;
	try {
		$group = new Mioga2::Old::Group($context->GetConfig(), rowid => $ctc->{group_id});
	} 

	otherwise {
		$group = new Mioga2::Old::User($context->GetConfig(), rowid => $ctc->{group_id});
	};

	my $res = $ctcapp->CheckUserAccessOnFunction($context, $context->GetUser(), $group, "Write");
	
	if($res != AUTHZ_OK) {
		return undef;
	}
	
	return $ctc;
}


# ============================================================================
=head2 DeleteElem

    $self->DeleteElem($context, $rowid);

    Delete the element with id $rowid.
    This method MUST be overloaded if allow_delete id set.

    This method is called after the confirmation page if askConfirmForDelete 
    is set.

=cut
    
# ============================================================================
sub DeleteElem {
	my ($self, $context, $rowid) = @_;
	
	my $ctcapp = new Mioga2::Contact;
	my $ctc = $ctcapp->ContactGetContact($context, $rowid);
	

	my $group;
	try {
		$group = new Mioga2::Old::Group($context->GetConfig(), rowid => $ctc->{group_id});
	} 
	otherwise {
		$group = new Mioga2::Old::User($context->GetConfig(), rowid => $ctc->{group_id});
	};

	my $res = $ctcapp->CheckUserAccessOnFunction($context, $context->GetUser(), $group, "Write");
	
	if($res != AUTHZ_OK) {
		return undef;
	}
	
	$ctcapp->ContactRemove($context, $rowid);
}


# ============================================================================
=head2 _GenerateGroupAndAlphaIndex

    $self->_GenerateGroupAndAlphaIndex($context, $nb_of_elem);

    return XML string describing the current group and alpha index.

=cut
    
# ============================================================================
sub _GenerateGroupAndAlphaIndex {
	my ($self, $context, $nb_of_elem) = @_;
	my $xml = "";

	my $session = $context->GetSession();
	my $ctcapp = new Mioga2::Contact;

	# ---------------------------------------
	# Create the group index 
	# if there are more than one group
	# ---------------------------------------
	my $group_list = $context->GetUser()->GetExpandedGroups();
	my $group_rowids = $group_list->GetRowids();

	unshift @$group_rowids, $context->GetUser()->GetRowid();

	my $cur_group = $session->{Contact}->{cur_group};

	if(@$group_rowids > 1) {
		$xml .= "<group_index>";
        
		foreach my $group_id (@$group_rowids) {
			my $group;
			try {
				$group = new Mioga2::Old::Group($context->GetConfig(), rowid => $group_id);
			}
			otherwise {
				$group = new Mioga2::Old::User($context->GetConfig(), rowid => $group_id);
			};
			
			my $res = $ctcapp->CheckUserAccessOnFunction($context, $context->GetUser(), $group, "Read");

			next if ($res != AUTHZ_OK);

			if($group_id == $cur_group) {
				$xml .= qq|<item id="$group_id" is_selected="1">|.$group->GetIdent().qq|</item>|;
			}
			else {
				$xml .= qq|<item id="$group_id">|.$group->GetIdent().qq|</item>|;
			}
		}
		
		$xml .= "</group_index>";
	}

	if(! exists $session->{Contact}->{nb_of_elem}->{$cur_group} or
	   $nb_of_elem != $session->{Contact}->{nb_of_elem}->{$cur_group}) {
		$self->_RebuildAlphaIndex($context, $cur_group);
	}

	$session->{Contact}->{nb_of_elem}->{$cur_group} = $nb_of_elem;

	# ---------------------------------------
	# Create the alphabetic index 
	# ---------------------------------------    
	my $alpha_ind = $session->{Contact}->{group_alpha_ind}->{$cur_group};

	my $cur_alpha = $session->{Contact}->{cur_alpha}->{$cur_group};

	if($cur_alpha >= @$alpha_ind) {
		$cur_alpha = $session->{Contact}->{cur_alpha}->{$cur_group} = 0;
	}

	$xml .= qq|<alpha_index group="$cur_group">|;

	for(my $i=0; $i < @$alpha_ind; $i++) {
		if($i == $cur_alpha) {
			$xml .= qq|<item id="$i" is_selected="1">$alpha_ind->[$i]</item>|;
		}
		else {
			$xml .= qq|<item id="$i">$alpha_ind->[$i]</item>|;
		}
	}
	
	$xml .= "</alpha_index>";
	
	return $xml;
}


# ============================================================================
# Private methods
# ============================================================================

# ============================================================================

=head2 _RebuildAlphaIndex

    _Rebuild Alphabetic indices

=cut

# ============================================================================

sub _RebuildAlphaIndex {
	my ($self, $context, $cur_group) = @_;
	
	my $session = $context->GetSession();
	my $ctcapp = new Mioga2::Contact;

	my @num_per_letter;
	for(my $letter = 'a'; $letter le 'z'; $letter = chr(ord($letter)+1)) {
		my $res = $ctcapp->ContactGetNumberMatchingForGroup($context, $cur_group, { name => ["^$letter"] });
		
		push @num_per_letter, $res;
	}

	my $res =  $ctcapp->ContactGetNumberMatchingForGroup($context, $cur_group, { name => ["^[^a-z]"] });
	$num_per_letter[25] += $res;
	
	## Build an alphabetic index for each group
	my @ind_list;
	my $cur_sum = 0;
	my $start_id = 0;
	my $stop_id = 0;

	
	# Retrieve nb_elem_per_page
	my $nb_elem_per_page;
	if(exists $context->{args}->{"nb_elem_per_page_".$self->{list_name}}) {
		my $nb_elem = $context->{args}->{"nb_elem_per_page_".$self->{list_name}};

		my $res = String::Checker::checkstring($nb_elem, ['disallow_empty', 'want_int']);

		if( defined $res and @$res) {
			$nb_elem_per_page = $self->{nb_elem_per_page};
		}
		else {
			$nb_elem_per_page = $context->{args}->{"nb_elem_per_page_".$self->{list_name}};
		}
	}
	elsif(exists $session->{persistent}->{LargeList}->{$self->{list_name}}->{nb_elem_per_page}) {
		$nb_elem_per_page = $session->{persistent}->{LargeList}->{$self->{list_name}}->{nb_elem_per_page};
	}
	else {
		$nb_elem_per_page = $self->{nb_elem_per_page};
	}

	if($nb_elem_per_page == 0) {
		$nb_elem_per_page = $self->{nb_elem_per_page};
	}



	
	for (my $i = 0; $i < 26; $i++) {
		if(($num_per_letter[$i] + $cur_sum) <= $nb_elem_per_page) {
			$stop_id = $i;
			$cur_sum += $num_per_letter[$i];
		}
		else {
			if($start_id == $stop_id) {                
				push @ind_list, chr(ord('A')+$start_id);
				$cur_sum = 0;
				if($i == $start_id) {
					$start_id = $stop_id = $i+1;
				}
				else {
					$start_id = $stop_id = $i;
					redo;
				}
			}
			else {
				push @ind_list, chr(ord('A')+$start_id)."-".chr(ord('A')+$stop_id);
				$cur_sum = $num_per_letter[$i];
				$start_id = $stop_id = $i;
			}
		}
	}
	
	if($start_id < 25 and @ind_list != 0) { 
		push @ind_list, chr(ord('A')+$start_id)."-Z";
	}
	elsif($start_id < 25 and @ind_list == 0) { 
		push @ind_list, "A-Z";
	}
	else { 
		push @ind_list, "Z";
	}
	
	
	if(!exists $session->{Contact}->{cur_alpha}->{$cur_group}) {
		$session->{Contact}->{cur_alpha}->{$cur_group} = 0;
	}
	
	$session->{Contact}->{group_alpha_ind}->{$cur_group} = \@ind_list;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Contact Mioga2::LargeList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
