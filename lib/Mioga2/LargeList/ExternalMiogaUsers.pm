# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
# Description:

=head1 NAME

Mioga2::LargeList::RowidList - package used to build simple large list 
	from user/group/resource rowid list

=cut

package Mioga2::LargeList::ExternalMiogaUsers;
use strict;

use base qw(Mioga2::SimpleLargeList);

use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Data::Dumper;


# ============================================================================

=head2 new

    new Mioga2::LargeList::ExternalMiogaUsers($context, $params, $external_mioga_rowid, $not_selectable_idents);

    Create a new ExternalMiogaUsers List.

	$external_mioga_rowid is the external mioga rowid.
	
	See SimpleLargeList for $params

=cut

# ============================================================================

sub new {
	my ($class, $context, $params, $external_mioga_rowid, $not_selectable_idents) = @_;

	my $self = $class->SUPER::new($context, $params);

	if(!defined $self) {
		return undef;
	}

	my $session = $context->GetSession();

	$self->InitializeCache($context, $external_mioga_rowid, $not_selectable_idents);

	return $self;
}


sub InitializeCache {
	my ($self, $context, $external_mioga_rowid, $not_selectable_idents) = @_;

	my $session = $context->GetSession();
	my $config = $context->GetConfig();

	my $extmioga = new Mioga2::ExternalMioga($config, rowid => $external_mioga_rowid);		
			
	my $response = $extmioga->SendRequest(application => 'Admin', method => 'XMLSearchUser', 
										  args => { user_ident => '*' },
										  );
	
	my $xs = new Mioga2::XML::Simple(forcearray => 1);
	my $xmltree = $xs->XMLin($response);
	
	my @userlist = map { 
		my $res = {};
		foreach my $key (keys %$_) {
			$res->{$key} = $_->{$key}->[0]->{content}->[0];
		}
		$res;
	} @{$xmltree->{UserList}->[0]->{Row}};


	my @realuserlist;
	foreach my $user (@userlist) {
		if(!grep {$_ eq $user->{ident}} @$not_selectable_idents) {
			push @realuserlist, $user;
		}
	}

	$session->{LargeList}->{$self->{list_name}}->{ext_user_list_cache}->{data} = \@realuserlist;
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 GetNumberOfElems

    $self->GetNumberOfElems($context);

    Return the number of element to display. 
    This method MUST be overloaded.

    This method is used to process page index.

=cut
    
# ============================================================================

sub GetNumberOfElems {
	my ($self, $context) = @_;

	my $session = $context->GetSession();

	my $nb_elem = @{$session->{LargeList}->{$self->{list_name}}->{ext_user_list_cache}->{data}};

	return $nb_elem;
}


# ============================================================================

=head2 GetElemsList 

    $self->GetElemsList($context, $offset, $sort_field_name, $asc);

    Return <nb_elem_per_page> elements begining to the <offset>th

    Elements must be sorted on the field $sort_field_name.
    If $asc = 0 elements are sorted descending, otherwise elements are sorted ascending.

    This method MUST be overloaded.
    
=cut
    
# ============================================================================
sub GetElemsList {
	my ($self, $context, $offset, $sort_field_name, $asc) = @_;
	
	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $data = $session->{LargeList}->{$self->{list_name}}->{ext_user_list_cache}->{data};

	my $start = $offset;
	my $stop = $offset + $self->{nb_elem_per_page} - 1;
	
	if($stop >= @$data) {
		$stop = @$data - 1;
	}

	my @list = @$data[$start..$stop];

	return \@list;
}


# ============================================================================

=head2 GetNumberOfElemsMatching

    $self->GetNumberOfElems($context, $pattern);

    Return the number of element matching given patterns. 
    This method MUST be overloaded.

    This method is used by SearchElem to process page index.

    See SearchElem for more details on $patterns

=cut
    
# ============================================================================
sub GetNumberOfElemsMatching {
	my ($self, $context, $pattern) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $data = $session->{LargeList}->{$self->{list_name}}->{ext_user_list_cache}->{data};

	my @result = @$data;
	foreach my $key (qw(ident email firstname lastname)) {
		if(exists $pattern->{$key}) {
			@result = grep {$_->{$key} =~ /^$pattern->{$key}->[0]/} @result;
		}
	}

	return @result;
}


# ============================================================================

=head2 GetElemsMatching

    $self->GetElemsMatching($context, $pattern, $offset, $sort_field_name, $asc);

    Return <nb_elem_per_page> elements begining to the <offset>th and matching given patterns.

    Elements must be sorted on the field $sort_field_name.
    If $asc = 0 elements are sorted descending, otherwise elements are sorted ascending.

    This method MUST be overloaded.

    See SearchElem for description of $pattern
    
=cut
    
# ============================================================================
sub GetElemsMatching {
	my ($self, $context, $pattern, $offset, $sort_field_name, $asc) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $data = $session->{LargeList}->{$self->{list_name}}->{ext_user_list_cache}->{data};

	my @result = @$data;
	foreach my $key (qw(ident email firstname lastname)) {
		if(exists $pattern->{$key}) {
			@result = grep {$_->{$key} =~ /^$pattern->{$key}->[0]/} @result;
		}
	}

	my $start = $offset;
	my $stop = $offset + $self->{nb_elem_per_page} - 1;
	
	if($stop >= @result) {
		$stop = @result - 1;
	}

	my @list = @result[$start..$stop];
	$self->{result} = \@list;

	return \@list;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LargeList Mioga2::SimpleLargeList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
