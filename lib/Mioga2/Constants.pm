#===============================================================================
#
#         FILE:  Constants.pm
#
#  DESCRIPTION:  Mioga2 constants definitions
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  03/05/2010 15:36
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Constants.pm: Mioga2 constants definitions

=head1 DESCRIPTION

This module defines miscelaneous constants used in Mioga2.

=head1 CONSTANTS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Constants;

use vars qw( @ISA @EXPORT );

@ISA = qw(Exporter);

@EXPORT = qw(
		XSLDIR LANGDIR THEMESDIR HELPDIR JSLIBDIR
		AUTHZ_NONE AUTHZ_READ AUTHZ_WRITE
		MIOGA_DAV_PROP_NS MIOGA_DAV_PROP_NS_URL
	);

#===============================================================================

=head2 Paths

Some filesystem path definitions

=cut

#===============================================================================


#===============================================================================

=over

=item XSLDIR

The path to XSL stylesheets directory relative to instance base directory.

=back

=cut

#===============================================================================

use constant XSLDIR => 'web/xsl';


#===============================================================================

=over

=item LANGDIR

The path to languages definitions directory relative to instance base directory.

=back

=cut

#===============================================================================

use constant LANGDIR => 'web/lang';


#===============================================================================

=over

=item THEMESDIR

The path to themes directory relative to instance base directory.

=back

=cut

#===============================================================================

use constant THEMESDIR => 'web/htdocs/themes';


#===============================================================================

=over

=item HELPDIR

The path to help directory relative to instance base directory.

=back

=cut

#===============================================================================

use constant HELPDIR => 'web/htdocs/help';


#===============================================================================

=over

=item JSLIBDIR

The path to JSLib directory relative to instance base directory.

=back

=cut

#===============================================================================

use constant JSLIBDIR => 'web/htdocs/jslib';


#===============================================================================

=head2 Authorizations

Some authorization definitions

=cut

#===============================================================================


#===============================================================================

=over

=item AUTHZ_NONE

No access to a file / directory.

=back

=cut

#===============================================================================

use constant AUTHZ_NONE  => 0;


#===============================================================================

=over

=item AUTHZ_READ

Read-only access to a file / directory.

=back

=cut

#===============================================================================

use constant AUTHZ_READ  => 1;


#===============================================================================

=over

=item AUTHZ_WRITE

Read-write access to a file / directory.

=back

=cut

#===============================================================================

use constant AUTHZ_WRITE => 2;


#===============================================================================

=head2 DAV

Some DAV-related constants

=cut

#===============================================================================


#===============================================================================

=over

=item MIOGA_DAV_PROP_NS

Namespace for Mioga2 custom DAV properties

=back

=cut

#===============================================================================

use constant MIOGA_DAV_PROP_NS => 'Mioga';


#===============================================================================

=over

=item MIOGA_DAV_PROP_NS_URL

Namespace URL for Mioga2 custom DAV properties

=back

=cut

#===============================================================================

use constant MIOGA_DAV_PROP_NS_URL => 'http://www.alixen.org/projects/mioga2';



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2

=head1 COPYRIGHT

Copyright (C) 2003-2010, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;

