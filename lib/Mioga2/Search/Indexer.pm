# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Indexer.pm : Class for Indexer in Mioga2::Search

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Search::Indexer;

use strict;
use utf8;
use Search::Xapian;
use Mioga2::Exception::Simple;
use Mioga2::Search::WProc;
use Mioga2::Search::Document;
use Data::Dumper;
#use Devel::Peek;

my $debug = 0;
# ============================================================================
# new
# ============================================================================
sub new
{
	my ($class, $params) = @_;
	print STDERR "Search::Indexer new($class)\n" if ($debug);

	my $self = { };
	bless($self, $class);

	if (!exists($params->{base_dir})) {
		throw Mioga2::Exception::Simple("Search::Indexer::new", "Parameter base_dir not given");
	}
	$self->{base_dir} = $params->{base_dir};
	
	if (!exists($params->{search_config})) {
		throw Mioga2::Exception::Simple("Search::Indexer::new", "Parameter search_config not given");
	}
	$self->{search_config} = $params->{search_config};

	$self->Initialize();
	
	return $self;
}
# ============================================================================
# Process
# ============================================================================
sub Process
{
	my ($self, $document) = @_;
	print STDERR "Search::Indexer Process()\n" if ($debug);
	
	my $zones = $document->GetZones();
	my $name;
	my $weight;
	my $datas;
	my $lang       = $document->GetLang();
	$lang = 'french' if (!defined($lang));
	my $group_id   = $document->GetGroupId();
	my $mioga_id   = $document->GetMiogaId();
	my $app_code   = $document->GetAppCode();
	my $doc_id     = $document->GetXapianDocid();
	my $modif_date = sprintf("%020d", $document->GetDateInSec());
	print STDERR "   lang = $lang  mioga_id = $mioga_id   group_id = $group_id  doc_id = $doc_id\n" if ($debug);
	my $doc_data = $document->GetDocumentInfos();
	my $xap_doc = Search::Xapian::Document->new();
	print STDERR "document description : " . $xap_doc->get_description() . "\n" if ($debug);
	my $wproc =  Mioga2::Search::WProc->new( { lang => $lang } );

	#$xap_doc->add_posting("L$lang", 0, 1);
	#$xap_doc->add_posting("G$group_id", 0, 1);
	#$xap_doc->add_posting("A$app_code", 0, 1);
	$xap_doc->add_term("L$lang", 1);
	$xap_doc->add_term("G$group_id", 1);
	$xap_doc->add_term("A$app_code", 1);

	$xap_doc->add_value(0, $modif_date);

#use Devel::Peek;
	my $pos = 1;
	my @pw;   # processed words
	foreach my $z (@$zones) {
		$name = $z->GetName();
		$weight = $z->GetWeight();
		$datas = $z->GetData();
		my $fl_tags = ($name eq 'tags');
		my $fl_desc = ($name eq 'description');
		my $fl_title = ($weight > 1);
		print STDERR "    process zone : $name  weight : $weight fl_tags = $fl_tags  fl_desc = $fl_desc fl_title = $fl_title\n" if ($debug);
		foreach my $l (@$datas) {
			print STDERR " zone $name line $l\n" if ($debug);
			#print STDERR Dump($l) if ($debug);

			foreach my $word (split(' ', $l)) {
				print STDERR " process $word pos = $pos\n" if ($debug);
#print STDERR Dump($word);
				my ($exact, $simple, @parts) = $wproc->NormalizeWord($word);
				if (length($simple) > 1 and length($simple) < 50) {
					$self->{xapiandb}->add_spelling($exact);
					print STDERR " add $simple at $pos\n" if ($debug > 1);
					$xap_doc->add_posting($simple, $pos, $weight);
					
					if ($fl_title) {
						print STDERR " add T$simple at $pos\n" if ($debug > 1);
						#$xap_doc->add_posting("T$simple", $pos, $weight);
						$xap_doc->add_term("T$simple", $weight);
					}
					if ($fl_tags) {
						print STDERR " add W$simple at $pos\n" if ($debug > 1);
						#$xap_doc->add_posting("W$simple", $pos, $weight);
						$xap_doc->add_term("W$simple", $weight);
					}
					if ($fl_desc) {
						print STDERR " add C$simple at $pos\n" if ($debug > 1);
						#$xap_doc->add_posting("C$simple", $pos, $weight);
						$xap_doc->add_term("C$simple", $weight);
					}
					if ($simple ne $exact) {
						print STDERR " add E$simple at $pos\n" if ($debug > 1);
#print STDERR Dump($exact);

						#$xap_doc->add_posting("E$exact", $pos, $weight);
						$xap_doc->add_term("E$exact", $weight);
					}
					my $sw = $wproc->StemWord($simple);
					if ($sw ne $simple) {
						if ($weight > 1) {
							print STDERR " add U$sw at $pos\n" if ($debug > 1);
							#$xap_doc->add_posting("U$sw", $pos, $weight);
							$xap_doc->add_term("U$sw", $weight);
						}
						else {
							print STDERR " add S$sw at $pos\n" if ($debug > 1);
							#$xap_doc->add_posting("S$sw", $pos, $weight);
							$xap_doc->add_term("S$sw", $weight);
						}
					}
					if (@parts > 1) {
						foreach my $p (@parts) {
							if (length($p) > 1) {
								if ($fl_title) {
									#$xap_doc->add_posting("T$p", $pos, $weight);
									$xap_doc->add_term("T$p", $weight);
								}
								else {
									#$xap_doc->add_posting($p, $pos, $weight);
									#TODO ADD position for all parts
									$xap_doc->add_term($p, $weight);
								}
								if ($fl_tags) {
									#$xap_doc->add_posting("W$p", $pos, $weight);
									$xap_doc->add_term("W$p", $weight);
								}
								if ($fl_desc) {
									#$xap_doc->add_posting("C$p", $pos, $weight);
									$xap_doc->add_term("C$p", $weight);
								}
								my $sw = $wproc->StemWord($p);
								if ($sw ne $p) {
									if ($weight > 1) {
										#$xap_doc->add_posting("U$sw", $pos, $weight);
										$xap_doc->add_term("U$sw", $weight);
									}
									else {
										#$xap_doc->add_posting("S$sw", $pos, $weight);
										$xap_doc->add_term("S$sw", $weight);
									}
								}
							}
						}
					}
				}
				$pos++;
			}
		}
		$doc_data .= "zone:$name:$weight:".($pos-1)."\n";
		print STDERR "  zone $name pos = ".($pos-1)." weight = $weight\n" if ($debug);
	}
	print STDERR "    doc_data = $doc_data\n" if ($debug);
	$xap_doc->set_data($doc_data);

	if ($doc_id == 0) {
		$doc_id = $self->{xapiandb}->add_document($xap_doc);
		print STDERR "    doc_id = $doc_id\n" if ($debug);
		$document->SetXapianDocid($doc_id);
	}
	else {
		$self->{xapiandb}->replace_document($doc_id, $xap_doc);
	}
}
# ============================================================================
# Flush
# ============================================================================
sub Flush
{
	my ($self) = @_;
	print STDERR "Search::Indexer Flush()\n" if ($debug);
	$self->{xapiandb}->flush();
}
# ============================================================================
# RemoveDoc
# ============================================================================
sub RemoveDoc
{
	my ($self, $docid) = @_;
	print STDERR "Search::Indexer RemoveDoc($docid)\n" if ($debug);
	my $doc;
	eval { $doc = $self->{xapiandb}->get_document($docid) };
	if (!$@) {
		my $data = $doc->get_data();
		print STDERR "   data = $data\n" if ($debug);
		my @lines = split('\n', $data);
		my $path;
		foreach my $l (@lines) {
			if ($l =~ /^path:(.*)$/) {
				$path = $1;
			}
		}
		print STDERR "Search::Indexer path = $path\n" if ($debug);
		if (defined ($path)) {
			my $document = Mioga2::Search::Document->new( { search_config => $self->{search_config},
			                                     base_dir => $self->{base_dir},
												 xapian_docid => $docid,
			                                     in_cache => { path => $path,
														       },
												} );
			$document->PurgeCache();
		}
	}
	eval { $self->{xapiandb}->delete_document($docid) };
	if ($@) {
		throw Mioga2::Exception::Simple("Search::Indexer::RemoveDoc", "Cannot remove doc <$docid> : error = [$@]");
	}
}
# ============================================================================
# Private functions
# ============================================================================
# Initialize
# ============================================================================
sub Initialize
{
	my ($self, $params) = @_;
	print STDERR "Search::Indexer Initialize()\n" if ($debug);

	my $path = $self->{base_dir} . "/" . $self->{search_config}->GetXapianDBDir();

	$self->{xapiandb} =  Search::Xapian::WritableDatabase->new($path,  Search::Xapian::DB_CREATE_OR_OPEN );
	print STDERR "DB description : " . $self->{xapiandb}->get_description() . "\n" if ($debug);
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Search 

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


