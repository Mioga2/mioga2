# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Query.pm : The Query class parse a standard or advanced query in Mioga2::Search.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Search::Query;

use strict;
use Search::Xapian qw(:ops);
use Mioga2::Exception::Simple;
use Mioga2::Search::WProc;
use Data::Dumper;

my $debug = 0;
# ============================================================================
# new
# ============================================================================
sub new
{
	my ($class, $params) = @_;
	print STDERR "Search::Query new()\n" if ($debug);
	print STDERR "    params = ".Dumper($params)."\n" if ($debug);

	my $self = { };
	bless($self, $class);
	$self->{mode} = 'standard';
	if (exists($params->{mode})) {
		if ($params->{mode} ne 'standard' and $params->{mode} ne 'advanced' ) {
			throw Mioga2::Exception::Simple("Search::Query::new", "Bad query mode : $params->{mode}");
		}
		$self->{mode} = $params->{mode};
	}
	
	if (!exists($params->{xapiandb})) {
		throw Mioga2::Exception::Simple("Search::Query::new", "xapiandb not given");
	}
	$self->{xapiandb} = $params->{xapiandb};

	if (!exists($params->{WQF})) {
		throw Mioga2::Exception::Simple("Search::Query::new", "WQF not given");
	}
	$self->{WQF} = $params->{WQF};

	if (!exists($params->{lang})) {
		throw Mioga2::Exception::Simple("Search::Query::new", "lang not given");
	}
	$self->{lang} = $params->{lang};

	$self->{force_terms} = 0;
	if (exists($params->{force_terms})) {
		$self->{force_terms} = $params->{force_terms};
	}

	if (!exists($params->{qstring})) {
		throw Mioga2::Exception::Simple("Search::Query::new", "qstring not given");
	}
	$self->{qstring} = $params->{qstring};
	utf8::decode($self->{qstring});

	$self->{group_id} = 0;
	$self->{app_code} = 0;
	if (exists($params->{group_id})) {
		$self->{group_id} = $params->{group_id};
	}
	if (exists($params->{app_code})) {
		$self->{app_code} = $params->{app_code};
	}

	$self->{search_text} = 1;
	$self->{search_description} = 1;
	$self->{search_tags} = 1;
	$self->{search_text} = $params->{search_text} if (exists($params->{search_text}));
	$self->{search_tags} =  $params->{search_tags} if (exists($params->{search_tags}));
	$self->{search_description} = $params->{search_description} if (exists($params->{search_description}));

	$self->Initialize();
	
	return $self;
}
# ============================================================================
# GetQuery
# ============================================================================
sub GetQuery
{
	my ($self) = @_;
	print STDERR "Search::Query GetQuery()\n" if ($debug);

	if (exists($self->{query})) {
		return $self->{query};
	}
	return undef;
}
# ============================================================================
# GetTermCount
# ============================================================================
sub GetTermCount
{
	my ($self) = @_;
	return scalar(@{$self->{terms}});
}
# ============================================================================
# GetTerms
# ============================================================================
sub GetTerms
{
	my ($self) = @_;
	return $self->{terms};
}
# ============================================================================
# GetCorrectedQuery
# ============================================================================
sub GetCorrectedQuery
{
	my ($self) = @_;
	return $self->{corrected_query};
}
# ============================================================================
# Private functions
# ============================================================================
# Initialize
# ============================================================================
sub Initialize
{
	my ($self) = @_;
	print STDERR "Search::Query Initialize()\n" if ($debug);

	print STDERR "  search in text : ".$self->{search_text}." search in tags : ".$self->{search_tags}."  search in description : ".$self->{search_description}."\n" if ($debug);
	$self->{wproc} =  Mioga2::Search::WProc->new( { lang => $self->{lang} } );

	#
	# Extract sentences (words between double quotes) from query, if any
	#
	$self->{exact_matching} = ();
	print STDERR "      qstring = $self->{qstring}\n" if ($debug);
	while ($self->{qstring} =~ s/"([^"]*)"//) {
		print STDERR "       1 = $1   qstring = $self->{qstring}\n" if ($debug);
		push @{$self->{exact_matching}}, $1;
	}
	print STDERR "      qstring = $self->{qstring}\n" if ($debug);
	#
	# Parse
	#
	$self->{corrected} = 0;
	$self->{corrected_query} = [];
	$self->{terms} = [];
	$self->{not_terms} = [];
	$self->{qstring} = $self->NormalizeString($self->{qstring});
	$self->ParseExactMatching();
	$self->ParseStandardQuery();
	if ($self->{mode} eq 'advanced') {
		$self->AddAdvancedOptions();
	}

	print STDERR "corrected_query = ".Dumper($self->GetCorrectedQuery())."\n" if ($debug);
}
# ============================================================================
# AddTerm
# ============================================================================
sub AddTerm
{
	my ($self, $term, $all_terms) = @_;
	my $rec = { };
	$rec->{input} = $term;
	$rec->{direct} = $term;
	push @{$self->{terms}}, $rec;
	return
#	$rec->{direct} = [];
#	foreach my $t (@$all_terms) {
#		if ($t =~ /^E/) {
#			$rec->{exact} = $t;
#		}
#		else {
#			my $couple = { };
#			$couple->{direct} = $t;
#			#if (grep {$_ =~ /^S/} @$all_terms) {
#			#}
#		}
#	}
#	push @{$self->{terms}}, $rec;
}
# ============================================================================
# NormalizeString
# ============================================================================
sub NormalizeString
{
	my ($self, $string) = @_;

	$string =~ s/[\s]+/ /g;
	$string =~ s/^ //;
	$string =~ s/ $//;
	$string = lc($string);

	return $string;
}
# ============================================================================
# ParseExactMatching
# ============================================================================
sub ParseExactMatching
{
	my ($self) = @_;
	print STDERR "Search::Query ParseExactMatching()\n" if ($debug);
	#
	# Create subqueries objects
	#
	$self->{exact_queries} = ();
	foreach my $s (@{$self->{exact_matching}}) {
		print STDERR "      s = $s\n" if ($debug);
		my $corrected = [];
		$s = $self->NormalizeString($s);
		if (length($s) > 0) {
			my @terms = split(' ', $s);
			my @queries;
			foreach my $t (@terms) {
				if (!$self->{force_terms}) {
					my $suggest = $self->{xapiandb}->get_spelling_suggestion($t,2);
					if (defined($suggest) && length($suggest) > 0) {
						print STDERR " term = $t suggest = $suggest\n" if ($debug);
						if ($t ne $suggest) {
							$t = $suggest;
							$self->{corrected} = 1;
						}
					}
				}
				push @$corrected, $t;
				my @st = $self->{wproc}->ProcessWord($t, 1);
				$self->AddTerm($t, \@st);
				#push @queries, Search::Xapian::Query->new($st[0]);
				push @queries, Search::Xapian::Query->new($t);
			}
			push @{$self->{exact_queries}}, Search::Xapian::Query->new(OP_PHRASE, @queries);
		}
		push @{$self->{corrected_query}}, { org => '"'.$s.'"' , corrected => '"'.join(" ", @$corrected).'"'};
	}
}
# ============================================================================
# ParseStandardQuery
# ============================================================================
sub ParseStandardQuery
{
	my ($self) = @_;
	print STDERR "Search::Query ParseStandardQuery()\n" if ($debug);
	#
	# Create subqueries objects
	#
	my @queries;
	my @not_queries;
	if (length($self->{qstring}) > 0) {
		my @terms = split(' ', $self->{qstring});
		foreach my $t (@terms) {
			my $cor_rec;
			my $not_term = 0;
			my $oper = substr($t, 0,1);
			if ($oper eq '-') {
				$not_term = 1;
				$t = substr($t, 1);
			}
			$cor_rec->{org} = $t;
			if (!$self->{force_terms}) {
				my $suggest = $self->{xapiandb}->get_spelling_suggestion($t,2);
				if (defined($suggest) && length($suggest) > 0) {
					print STDERR " term = $t suggest = $suggest\n" if ($debug);
					if ($t ne $suggest) {
						$t = $suggest;
						$self->{corrected} = 1;
					}
				}
			}
			$cor_rec->{corrected} = $t;
			push @{$self->{corrected_query}}, $cor_rec;

			my ($exact, $simple, @parts) = $self->{wproc}->NormalizeWord($t, 1);
			print STDERR " term = $t simple = $simple exact = $exact  parts = ".join(',', @parts)."\n" if ($debug);
			my $rec = {};
			$rec->{input} = $t;

			my @subquery;
			# terms that must NOT be in result
			if ($not_term) {
				print STDERR "  term $t  query_code AND_NOT\n" if ($debug);
				push @subquery, Search::Xapian::Query->new_term($simple, $self->{WQF}->{wqf});
				push @subquery, Search::Xapian::Query->new_term("T$simple", $self->{WQF}->{wqf_T});
				$rec->{direct} = $simple;
				my $sw = $self->{wproc}->StemWord($simple);
				if ($sw ne $simple) {
					push @subquery, Search::Xapian::Query->new_term("S$sw", $self->{WQF}->{wqf_S});
					push @subquery, Search::Xapian::Query->new_term("U$sw", $self->{WQF}->{wqf_U});
					$rec->{stemmed} = $sw;
				}
				if ($simple ne $exact) {
					push @subquery, Search::Xapian::Query->new_term("E$exact", $self->{WQF}->{wqf_E});
					$rec->{exact} = $exact;
				}

				push @not_queries, Search::Xapian::Query->new(OP_OR, @subquery);
				push @{$self->{not_terms}}, $rec;
			}
			# terms that MUST be in result
			else {
				if ($self->{search_tags}) {
					push @subquery, Search::Xapian::Query->new_term("W$simple", $self->{WQF}->{wqf_W});
					$rec->{direct} = $simple;
				}
				if ($self->{search_description}) {
					push @subquery, Search::Xapian::Query->new_term("C$simple", $self->{WQF}->{wqf_C});
					$rec->{direct} = $simple;
				}

				if ($self->{search_text}) {
					push @subquery, Search::Xapian::Query->new_term($simple, $self->{WQF}->{wqf});
					push @subquery, Search::Xapian::Query->new_term("T$simple", $self->{WQF}->{wqf_T});
					$rec->{direct} = $simple;
					my $sw = $self->{wproc}->StemWord($simple);
					if ($sw ne $simple) {
						push @subquery, Search::Xapian::Query->new_term("S$sw", $self->{WQF}->{wqf_S});
						push @subquery, Search::Xapian::Query->new_term("U$sw", $self->{WQF}->{wqf_U});
						$rec->{stemmed} = $sw;
					}
					if ($simple ne $exact) {
						push @subquery, Search::Xapian::Query->new_term("E$exact", $self->{WQF}->{wqf_E});
						$rec->{exact} = $exact;
					}
					# if more than one part, we must contruct a AND query with all parts
					if (@parts > 1) {
						my @part_query;
						$rec->{parts} = [];
						foreach my $p (@parts) {
							if (length($p) > 1) {
								my @sub_part;
								my $rec_part = {};
								push @sub_part, Search::Xapian::Query->new_term($p, $self->{WQF}->{wqf});
								$rec_part->{direct} = $p;
								push @sub_part, Search::Xapian::Query->new_term("T$p", $self->{WQF}->{wqf_T});
								my $sw = $self->{wproc}->StemWord($p);
								if ($sw ne $p) {
									$rec_part->{stemmed} = $sw;
									push @sub_part, Search::Xapian::Query->new_term("S$sw", $self->{WQF}->{wqf_S});
									push @sub_part, Search::Xapian::Query->new_term("U$sw", $self->{WQF}->{wqf_U});
								}
								push @part_query, Search::Xapian::Query->new(OP_OR, @sub_part);
								push @{$rec->{parts}}, $rec_part;
							}
						}
						push @subquery, Search::Xapian::Query->new(OP_AND, @part_query);
					}
				}
				print STDERR " term = $t  rec = ".Dumper($rec)."\n" if ($debug);
				push @{$self->{terms}}, $rec;
				push @queries, Search::Xapian::Query->new(OP_OR, @subquery);
			}
		}
	}
	if ($self->{exact_queries} && @{$self->{exact_queries}}) {
		push @queries, @{$self->{exact_queries}};
	}
	print STDERR " queries = @queries\n" if ($debug);
	$self->{query} = Search::Xapian::Query->new(OP_AND, @queries);
	print STDERR " not_queries = @not_queries\n" if ($debug);
	if (scalar(@not_queries) > 0) {
		print STDERR "got AND_NOT queries\n" if ($debug);
		my $not_Q = Search::Xapian::Query->new(OP_OR, @not_queries);
		$self->{query} = Search::Xapian::Query->new(OP_AND_NOT, $self->{query}, $not_Q);
	}
	print STDERR "    query : " . $self->{query}->get_description . "\n" if ($debug);
}
# ============================================================================
# AddAdvancedOptions
# ============================================================================
sub AddAdvancedOptions
{
	my ($self) = @_;
	print STDERR "Search::Query AddAdvancedOptions($self->{group_id}, $self->{app_code})\n" if ($debug);
	if ($self->{group_id} > 0) {
		my $query = Search::Xapian::Query->new("G$self->{group_id}");
		$self->{query} = Search::Xapian::Query->new(OP_AND, $self->{query}, $query);
	}
	if ($self->{app_code} > 0) {
		my $query = Search::Xapian::Query->new("A$self->{app_code}");
		$self->{query} = Search::Xapian::Query->new(OP_AND, $self->{query}, $query);
	}

	print STDERR "    query : " . $self->{query}->get_description . "\n" if ($debug);
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Search 

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
