# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Result.pm : Class for Result in Mioga2::Search

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Search::Result;

use strict;
use utf8;
use Search::Xapian;
use Mioga2::Exception::Simple;
use POSIX qw(floor);
use Data::Dumper;

my $debug = 0;
# ============================================================================
# new
# ============================================================================
sub new
{
	my ($class, $params) = @_;
	print STDERR "Search::Result new($class)\n" if ($debug);

	my $self = { };
	bless($self, $class);
	# search_config parameter
	if (!exists($params->{search_config})) {
		throw Mioga2::Exception::Simple("Search::Result::new", "Parameter search_config not given");
	}
	$self->{search_config} = $params->{search_config};

	if (!exists($params->{config})) {
		throw Mioga2::Exception::Simple("Search::Result::new", "Parameter config not given");
	}
	$self->{config} = $params->{config};
	#print STDERR Dumper($self->{config});

	if (!exists($params->{user_id})) {
		throw Mioga2::Exception::Simple("Search::Result::new", "Parameter user_id not given");
	}
	$self->{user_id} = $params->{user_id};

	if (!exists($params->{max_result})) {
		throw Mioga2::Exception::Simple("Search::Result::new", "Parameter max_result not given");
	}
	$self->{max_result} = $params->{max_result};

	if (!exists($params->{offset})) {
		throw Mioga2::Exception::Simple("Search::Result::new", "Parameter offset not given");
	}
	$self->{offset} = $params->{offset};

	if (!exists($params->{max_count})) {
		throw Mioga2::Exception::Simple("Search::Result::new", "Parameter max_count not given");
	}
	$self->{max_count} = $params->{max_count};

	$self->{count} = 0;         # Actual number of matches
	$self->{cur_count} = 0;           # increment for each authorized result
	$self->{tested_candidate} = 0;    # increment for each potential result, compare to offset to store in result

	$self->{term_count} = 0;
	$self->{terms} = [];

	$self->{total_candidate} = 0;
	$self->{candidates} = [];
	$self->{results} = [];
	$self->{ind_result} = 0;   # Indice for GetNext function
	return $self;
}
# ============================================================================
# GetCount
# ============================================================================
sub GetCount
{
	my ($self) = @_;
	return $self->{count};
}
# ============================================================================
# GetTotalCount
# ============================================================================
sub GetTotalCount
{
	my ($self) = @_;
	print STDERR "max_result : $self->{max_result}\n" if ($debug);
	print STDERR "cur_count : $self->{cur_count}\n" if ($debug);
	print STDERR "total_candidate : $self->{total_candidate}\n" if ($debug);
	print STDERR "tested_candidate : $self->{tested_candidate}\n" if ($debug);
	if ($self->{tested_candidate} > 0) {
		return floor($self->{max_result} * ($self->{cur_count} / $self->{tested_candidate}));
	}
	return 0;
}
# ============================================================================
# AddTerm
# ============================================================================
sub AddTerm
{
	my ($self, $term) = @_;
	$self->{term_count}++;
	push @{$self->{terms}}, $term;
}
# ============================================================================
# AddCandidate
# ============================================================================
sub AddCandidate
{
	my ($self, $candidate) = @_;
	$self->{total_candidate}++;
	push @{$self->{candidates}}, $candidate;
}
# ============================================================================
# GetTerms
# ============================================================================
sub GetTerms
{
	my ($self) = @_;
	return $self->{terms};
}
# ============================================================================
# GetResults
# ============================================================================
sub GetResults
{
	my ($self) = @_;
	return $self->{results};
}
# ============================================================================
# ValidateCandidates
# Organize candidates by app_code and call Document::XXX::Validate
# ============================================================================
sub ValidateCandidates
{
	my ($self) = @_;
	print STDERR "Search::Result::ValidateCandidates\n" if ($debug);
	my $applications = $self->{search_config}->GetApplicationsByCodes();

	# Construct list of ids for each application
	foreach my $c (@{$self->{candidates}}) {
		if (exists($applications->{$c->{app_code}})) {
			push @{$applications->{$c->{app_code}}->{app_ids}}, $c->{app_id};
		}
		else {
			throw Mioga2::Exception::Simple("Search::Result::ValidateCandidates", "Unknown app_code : $c->{app_code}");
		}
	}
	# Get list of authorized ids
	foreach my $app (keys(%$applications)) {
		if (exists($applications->{$app}->{app_ids})) {
			print STDERR "  process app : $app  for (".join(',', @{$applications->{$app}->{app_ids}}).")\n" if ($debug);
			$applications->{$app}->{authz_uri} = $applications->{$app}->{package}->VerifyAuthz($applications->{$app}->{app_ids}, $self->{config}, $self->{user_id});
		}
	}
	# Tranfert candidates to result function of access right
	my $c;
	while ($c = shift @{$self->{candidates}}) {
		$self->{tested_candidate}++;
		if ($applications->{$c->{app_code}}->{authz_uri}->{$c->{app_id}}) {
			$self->{cur_count}++;
			if ($self->{cur_count} > $self->{offset}) {
				push @{$self->{results}}, $c;
				$self->{count}++;
				last if ($self->{count} >= $self->{max_count});
			}
		}
	}
	return $self->{count};
}
# ============================================================================
# Private functions
# ============================================================================
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Search 

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


