# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Zone.pm : A zone is a part of a document.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Search::Zone;

use strict;
use Mioga2::Exception::Simple;
use POSIX qw(floor);
use Data::Dumper;

my $debug = 0;
my $WORD_PER_LINE = 20;
# ============================================================================
# new
# ============================================================================
sub new
{
	my ($class, $params) = @_;
	print STDERR "Mioga2::Search::Zone new($class)\n" if ($debug);

	my $self = { };
	bless($self, $class);

	if (!exists($params->{name})) {
		throw Mioga2::Exception::Simple("Mioga2::Search::Zone::new", "Zone name not given");
	}
	$self->{name} = $params->{name};
	if (!exists($params->{weight})) {
		throw Mioga2::Exception::Simple("Mioga2::Search::Zone::new", "Zone weight not given");
	}
	$self->{weight} = $params->{weight};

	$self->{lang} = undef;
	if (exists($params->{lang})) {
		$self->{lang} = $params->{lang};
	}
	$self->{data} = [];
	$self->{cur_word_count} = 0;
	$self->{total_words} = 0;
	$self->{size} = 0;
	
	return $self;
}
# ============================================================================
# FormatDataString
# ============================================================================
sub FormatDataString
{
	my ($self, $line) = @_;
	print STDERR " FormatDataString($line)\n" if ($debug);
	$line =~ s/[\x{c2a0}\xa0\s]+/ /g;
	$line =~ s/\s+/ /g;
	my @words = split ' ', $line;
	$self->FormatDataArray(\@words);
}
# ============================================================================
# FormatDataArray
# ============================================================================
sub FormatDataArray
{
	my ($self, $words) = @_;
	print STDERR " FormatDataArray()\n" if ($debug);

	my $ind_cur_line = scalar(@{$self->{data}}) - 1;
	if ($ind_cur_line < 0) {
		$ind_cur_line = 0;
		$self->{data}->[0] = "";
	}
	print STDERR " ind_cur_line = $ind_cur_line avant = <$self->{data}->[$ind_cur_line]>\n" if ($debug);
	print STDERR " word count = ".scalar(@$words)."\n" if ($debug);
	foreach my $w (@$words) {
		print STDERR " w <$w>\n" if ($debug);
		next if (length($w) == 0);
		$self->{size} += length($w);
		if ($self->{cur_word_count} == 0) {
			$self->{data}->[$ind_cur_line] = $w;
		}
		else {
			$self->{data}->[$ind_cur_line] .= " ".$w;
			$self->{size} += 1;
		}
		$self->{cur_word_count} += 1;
		$self->{total_words} += 1;
		if ($self->{cur_word_count} >= $WORD_PER_LINE) {
			$ind_cur_line++;
			$self->{data}->[$ind_cur_line] = "";
			$self->{cur_word_count} = 0;
		}
	}
	print STDERR " cur_word_count = $self->{cur_word_count} total_words = $self->{total_words}\n" if ($debug);
}
# ============================================================================
# AddData
# ============================================================================
sub AddData
{
	my ($self, $line) = @_;
	print STDERR "Zonne::AddData  line = $line\n" if ($debug);
	push @{$self->{data}}, $line;
	print STDERR "Zone = ".Dumper($self)."\n" if ($debug);
}
# ============================================================================
# GetName
# ============================================================================
sub GetName
{
	my ($self) = @_;
	return $self->{name};
}
# ============================================================================
# GetNumberOfLines
# ============================================================================
sub GetNumberOfLines
{
	my ($self) = @_;
	return scalar(@{$self->{data}});
}
# ============================================================================
# GetSize
# ============================================================================
sub GetSize
{
	my ($self) = @_;
	return $self->{size};
}
# ============================================================================
# GetTotalWords
# ============================================================================
sub GetTotalWords
{
	my ($self) = @_;
	return $self->{total_words};
}
# ============================================================================
# GetWeight
# ============================================================================
sub GetWeight
{
	my ($self) = @_;
	return $self->{weight};
}
# ============================================================================
# GetData
# ============================================================================
sub GetData
{
	my ($self) = @_;
	return $self->{data};
}
# ============================================================================
# GetExtract
# ============================================================================
sub GetExtract
{
	my ($self, $pos, $half_width) = @_;
	my $extract = "";
	print STDERR "Search::Zone GetExtract($pos, $half_width)\n" if ($debug);
	print STDERR "Zone = ".Dumper($self)."\n" if ($debug);

	my $line_count = @{$self->{data}};
	my $begin = $pos - $half_width - 1; # indices begins at 0
	$begin = 0 if ($begin <= 0);
	my $beg_line = floor($begin / $WORD_PER_LINE);
	my $cur_pos = $beg_line * $WORD_PER_LINE;
	my $word_count = 2 * $half_width + 1;   # total number of words to get
	print STDERR "begin = $begin  cur_pos = $cur_pos beg_line = $beg_line line_count = $line_count  word_count = $word_count\n" if ($debug);
	my $iword = 0; # current count of word in extract
	my @words = split(' ', $self->{data}->[$beg_line]);
	print STDERR "words = ".join(" ", @words)."\n" if ($debug);
	my $i = 0; # indice on @words
	while ($iword < $word_count) {
		if ($cur_pos >= $begin) {
			$extract .= ' ' . $words[$i];
			$iword++;
		}
		$i++;
		$cur_pos++;
		print STDERR "i = $i  cur_pos = $cur_pos\n" if ($debug);
		if ($i >= scalar(@words)) {
			$beg_line += 1;
			if ($beg_line >= $line_count) {
				last;
			}
			else {
				@words = split(' ', $self->{data}->[$beg_line]);
				$i = 0;
				print STDERR "words = ".join(" ", @words)."\n" if ($debug);
			}
		}
	}

	print STDERR "Search::Zone extract = $extract\n" if ($debug);
	return $extract;
}
# ============================================================================
# Private functions
# ============================================================================

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Search 

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


