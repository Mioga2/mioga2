# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Finder.pm : Class for Finder in Mioga2::Search

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Search::Finder;

use strict;
use Search::Xapian;
use Mioga2::Search::Query;
use Mioga2::Exception::Simple;
use Mioga2::tools::database;
use Data::Dumper;
#	use Devel::Peek;

my $debug = 0;
# ============================================================================
# new
# ============================================================================
sub new
{
	my ($class, $params) = @_;
	print STDERR "Search::Finder new($class)\n" if ($debug);

	my $self = { };
	bless($self, $class);
	# config parameter
	if (!exists($params->{config})) {
		throw Mioga2::Exception::Simple("Search::Finder::new", "Parameter config not given");
	}
	$self->{config} = $params->{config};
	# base_dir parameter
	if (!exists($params->{base_dir})) {
		throw Mioga2::Exception::Simple("Search::Finder::new", "Parameter base_dir not given");
	}
	$self->{base_dir} = $params->{base_dir};
	# search_config parameter
	if (!exists($params->{search_config})) {
		throw Mioga2::Exception::Simple("Search::Finder::new", "Parameter search_config not given");
	}
	$self->{search_config} = $params->{search_config};
	#
	# Initialize other params
	#
	$self->{webdav_dir} = $self->{config}->GetWebdavDir();
	$self->{dbh} = $self->{config}->GetDBH();

	$self->Initialize();
	
	return $self;
}
# ============================================================================
# Process
# ============================================================================
sub Process
{
	my ($self, $params) = @_;
	print STDERR "Search::Finder Process()\n" if ($debug);

	#
	# Get Weight parameters
	#
	$self->{BM25} = $self->{search_config}->GetBM25Params();
	print STDERR "    BM25 = " . Dumper($self->{BM25}) . "\n" if ($debug);

	$self->{bmweight} = Search::Xapian::BM25Weight->new( $self->{BM25}->{bm25_k1},
															$self->{BM25}->{bm25_k2},
															$self->{BM25}->{bm25_k3},
															$self->{BM25}->{bm25_b},
															$self->{BM25}->{bm25_L});
	# Construct Query
	$params->{xapiandb} = $self->{xapiandb};
	$params->{WQF} = $self->{search_config}->GetWQFParams();
	$self->{query} = Mioga2::Search::Query->new($params);
	#TODO : get parameters to get more matches
	$self->{enquire} = $self->{xapiandb}->enquire($self->{query}->GetQuery);

	$self->{enquire}->set_weighting_scheme($self->{bmweight});
	#
	# Reset begining indice for list of result
	#
	$self->{current_begin} = 0;
}
# ============================================================================
# GetMatches
# ============================================================================
sub GetMatches
{
	my ($self, $begin, $count) = @_;
	print STDERR "Search::Finder GetMatches()\n" if ($debug);

	return $self->{enquire}->matches( $begin, $count );
}
# ============================================================================
# GetQuery
# Returns the Mioga2::Query object used for search
# ============================================================================
sub GetQuery {
	my ($self) = @_;
	return $self->{query};
}
# ============================================================================
# GetResult
# Returns an array of Mioga2::Search::Result
# We need to return the $count of result, but we must verify access rights oni
# results, so we must loop on getting xapian results to construct list of
# authorized docs.
# ============================================================================
sub GetResult
{
	my ($self, $user_id, $offset, $max_count) = @_;
	print STDERR "Search::Finder GetResult($offset, $max_count)\n" if ($debug);
	
	my $count = 0;
	my $fl_end = 0;
	my $matches;
	my $xap_offset = 0;
	my $xap_count;
	my $MAX_XAP_COUNT = 20;
	$self->{enquire}->set_sort_by_relevance_then_value(0, 0);
	$matches = $self->{enquire}->get_mset($xap_offset, $MAX_XAP_COUNT);
	my $max_result = $matches->get_matches_estimated();
	print STDERR "   max_result : $max_result\n" if ($debug);
	my $result = Mioga2::Search::Result->new( { search_config => $self->{search_config},
	                                            config => $self->{config},
												user_id => $user_id,
	                                            max_result => $max_result,
												offset => $offset,
												max_count => $max_count,
											} );

	#
	# Get extended set
	#
	my $rset = Search::Xapian::RSet->new();
	my $cur_match = $matches->begin();
	while ($cur_match ne $matches->end()) {
		$rset->add_document($cur_match->get_docid());
		$cur_match++;
	}
	my $eset = $self->{enquire}->get_eset(100, $rset);
	print STDERR "eset size = " . $eset->size() . "\n" if($debug);
	my $term = $eset->begin();
	my $MAX_TERM_COUNT = 10;
	my $term_count = 0;
	my $t;
	my $fc;
	while (($term ne $eset->end()) and ($term_count < $MAX_TERM_COUNT)) {
		$t = $term->get_termname;
		print STDERR "term $t\n" if($debug);
		if ($t !~/^[AGUS]/) {
			# TODO try to filter on length but it will be better to use e stop word dictionnary
			$t =~ s/[A-Z]//g;
			if ( (length($t) > 4) ) {
			#print STDERR Dump($t);
				utf8::decode($t);
			#print STDERR Dump($t);
				$result->AddTerm($t);
				$term_count++;
			}
		}
		$term++;
	}
	#
	# Loop to get current page of result
	#
	do {
		my $mset_size = $matches->size();
		print STDERR "   size : $mset_size\n" if ($debug);
		if ($mset_size == 0) {
			$fl_end = 1;
		}
		else {
			my $cur_match = $matches->begin();
			do {
				print STDERR "cur_match : $cur_match\n" if ($debug);
				my $doc_id = $cur_match->get_docid();
				my $percent = $cur_match->get_percent();
				my $doc = $cur_match->get_document();
				my $data = $doc->get_data();
				my $date = $doc->get_value(0);
				print STDERR "   doc_id = $doc_id  percent = $percent  data = $data\n" if ($debug);
				my $xap_data = $self->DecodeDocData($data);
				$result->AddCandidate( { xapian_id => $doc_id,
				                         percent => $percent,
										 app_code => $xap_data->{app_code},
										 app_id => $xap_data->{app_id},
										 path => $xap_data->{path},
										 zones => $xap_data->{zones},
										 date => $date,
										 } );

				$cur_match++;
			} while ($cur_match ne $matches->end());
			$count = $result->ValidateCandidates();
			print STDERR "   count = $count  max_count = $max_count\n" if ($debug);
			if ($count < $max_count) {
				$xap_offset += $MAX_XAP_COUNT;
				$matches = $self->{enquire}->get_mset($xap_offset, $MAX_XAP_COUNT);
			}
		}
	} while ( ($count < $max_count) and !$fl_end);
	
	#
	# Prepare highlight for document
	#
	my $terms = $self->{query}->GetTerms();
	print STDERR "terms = ".Dumper($terms) . "\n" if ($debug);
	$term_count = $self->{query}->GetTermCount();

	foreach my $res (@{$result->GetResults()}) {
		$res->{term_count} = $term_count;
		my ($highlight, $data) = $self->GetHighlight($res, $terms, $term_count);
		$res->{highlight} = $highlight;
		$res->{data} = $data;
	}

	return $result;
}
# ============================================================================
# Private functions
# ============================================================================
# Initialize
# ============================================================================
sub Initialize
{
	my ($self, $params) = @_;
	print STDERR "Search::Finder Initialize()\n" if ($debug);

	my $path = $self->{base_dir} . "/" . $self->{search_config}->GetXapianDBDir();
	$self->{xapiandb} =  Search::Xapian::Database->new($path);
}
# ============================================================================
# PrintPosHash
# ============================================================================
sub PrintPosHash
{
	my ($self, $pos_hash) = @_;
	my $values = "";
	my $pos = "";

	foreach my $k (sort {$a <=> $b} keys(%$pos_hash)) {
		print STDERR "$k => $pos_hash->{$k},";
	}
	print STDERR "\n";
}
# ============================================================================
# GetHighlight
# ============================================================================
sub GetHighlight
{
	my ($self, $res, $terms, $term_count) = @_;
	print STDERR "Search::Finder GetHighlight()\n" if ($debug);
	my $highlight = "";
	print STDERR " res = " . Dumper($res) ."\n" if ($debug);
	print STDERR " Process docid = $res->{xapian_id} to get highlight\n" if ($debug);

	$res->{terms} = [];
	my $HALF_WIDTH = 14;
	my $max_pos = 0;
	foreach my $z (@{$res->{zones}}) {
		$max_pos += $z->{pos};
	}
	print STDERR " max_pos = $max_pos\n" if ($debug);

	#
	# Construct an hash for position combining all words with ponderation curve.
	#
	my $global_pos = {};
	foreach my $t (@$terms) {
		print STDERR " Process term = ".Dumper($t)."\n" if ($debug);
	
		my $result = $self->ConstructPositionHash($res->{xapian_id}, $t, $max_pos, $HALF_WIDTH);
		$self->PrintPosHash($result->{pos_hash}) if ($debug);
		foreach my $p (keys(%{$result->{pos_hash}})) {
			$global_pos->{$p} += $result->{pos_hash}->{$p};
		}
	}
	print STDERR " global_pos\n" if ($debug);
	$self->PrintPosHash($global_pos) if ($debug);

	#
	# Search best positions for highlight
	#
	my @pos;
	my @sort_keys = sort { $global_pos->{$b} <=> $global_pos->{$a} } keys(%$global_pos);
	print STDERR " sort_keys" . join(",", @sort_keys)."\n" if ($debug);
	my $i = 1;
	my @best_pos;
	# search all the best pos (same as first)
	push @best_pos, $sort_keys[0];
	while ( ($i < @sort_keys) and ($global_pos->{$sort_keys[$i]} == $global_pos->{$sort_keys[0]}) ) {
		push @best_pos, $sort_keys[$i];
		$i++;
	}
	print STDERR " best_pos" . join(",", @best_pos)."\n" if ($debug);
	@sort_keys = sort { $a <=> $b } @best_pos;
	print STDERR " sort_keys" . join(",", @sort_keys)."\n" if ($debug);
	my $evaluation = 100 * $global_pos->{$sort_keys[0]} / $term_count / $HALF_WIDTH / 2;
	print STDERR " position = $sort_keys[0] value = $global_pos->{$sort_keys[0]} evaluation = $evaluation\n" if ($debug);

	push @pos, $sort_keys[0];
	foreach my $p (@sort_keys) {
		if (abs($p - $sort_keys[0]) > (2 * $HALF_WIDTH + 1)) {
			push @pos, $p;
			last;
		}
	}
	#
	# Get extract from document
	#
	my @extracts;
	my $document;
	$document = Mioga2::Search::Document->new( { search_config => $self->{search_config},
			                                     base_dir => $self->{base_dir},
												 xapian_docid => $res->{xapian_id},
			                                     from_cache => { path => $res->{path},
														       },
												} );
	foreach my $pos (@pos) {
		print STDERR " zone search for pos = $pos\n" if ($debug);
		my $izone = 0;
		foreach my $z (@{$res->{zones}}) {
			print STDERR " pos = $pos izone = $izone   max = $z->{pos}\n" if ($debug);
			last if ($pos <= $z->{pos});
			$izone++;
			#$pos -= $z->{pos};
		}
		push @extracts, $document->GetExtract($izone, $pos - 1, $HALF_WIDTH);
	}
	$highlight = "..." . join("...", @extracts) . "...";
	# unhtmlize hightligth to remove residual tags
	$highlight =~ s/<[^>]*>/ /g;
	# Add style to highlight terms
	foreach my $t (@$terms) {
		my $motif = $t->{input};
		$highlight =~ s/($motif)/<span class="highlight">$1<\/span>/gi;
		print STDERR "term = $motif  match = $1\n" if ($debug and defined($1));
	}
	$res->{url} = $document->GetDocumentURL;
	$res->{description} = $document->GetDocumentDescription;
	$res->{mime} = $document->GetDocumentMIME;
	$res->{group_id} = $document->GetDocumentGroupId;

	my $sup_data = $document->GetData();
	print STDERR " sup_data = ".Dumper($sup_data)."\n" if ($debug);

	return ($highlight, $sup_data);
}
# ============================================================================
# GetPositionsForWord
# ============================================================================
sub GetPositionsForWord
{
	my ($self, $xapian_id, $term) = @_;
	print STDERR "Search::Finder GetPositionsForWord($xapian_id, $term)\n" if ($debug);
	my $pos_iter;
	my $result = [];

	$pos_iter = $self->{xapiandb}->positionlist_begin($xapian_id, $term);
	while ($pos_iter != $self->{xapiandb}->positionlist_end($xapian_id, $term)) {
		print STDERR " term_pos = ".$pos_iter->get_termpos()."\n" if ($debug);
		push @$result, $pos_iter->get_termpos();
		$pos_iter->inc();
	}
	if (@$result < 1) {
		return undef;
	}
	print STDERR " result = ".Dumper($result)."\n" if ($debug);
	return $result;
}
# ============================================================================
# GetMiogaDBInfos
# ============================================================================
sub GetMiogaDBInfos
{
	my ($self, $uri_ids) = @_;
	my $result = {};
	my $sql = "SELECT search_file.*, m_uri.uri, m_uri.group_id FROM search_file, m_uri"
					." WHERE search_file.m_uri_id in (" . join(',', @$uri_ids) . ")"
						." AND m_uri.rowid = search_file.m_uri_id";
	my $res = SelectMultiple($self->{pg_dbh}, $sql);

	foreach my $r (@$res) {
		$result->{$r->{m_uri_id}} = $r;
	}
	return $result;
}
# ============================================================================
# DecodeDocData
# ============================================================================
sub DecodeDocData
{
	my ($self, $data) = @_;
	#print STDERR "Search::Finder DecodeData()\n" if ($debug);
	my $result = {};
	my @lines = split("\n", $data);
	foreach my $line (@lines) {
		#print STDERR "    line = $line\n" if ($debug);
		
		$line =~ /(^[^:]*):(.*$)/;
		if (!defined($1) or !defined($2)) {
			throw Mioga2::Exception::Simple("Search::Finder DecodeData", "Bad format for Xapian data : $data");
		}
		my $code = $1;
		my $param = $2;
		#print STDERR "     code = $codes[0]\n" if ($debug);
		if ($code eq 'path') {
			$result->{path} = $param;
		}
		elsif ($code eq 'app_code') {
			$result->{app_code} = $param;
		}
		elsif ($code eq 'app_id') {
			$result->{app_id} = $param;
		}
		elsif ($code eq 'zone') {
			my @codes = split(':', $param);
			my $zone = { name => $codes[0],
			             weight => $codes[1],
			             pos => $codes[2],
						};
						 
			push @{$result->{zones}}, $zone;
		}
	}
	if (!exists($result->{path}) or !exists($result->{app_code}) or !exists($result->{app_id}) or !exists($result->{zones})) {
		throw Mioga2::Exception::Simple("Search::Finder DecodeData", "Bad format for Xapian data : $data");
	}

	return $result;
}
# ============================================================================
# ConstructPositionHash
# ============================================================================
sub ConstructPositionHash
{
	my ($self, $xap_id, $term, $max_pos, $HALF_WIDTH) = @_;
	my $result = {};
	my $pos_hash = {};
	my $i;
	my $val;
	my $direct_weight = 2;
	my $stemmed_weight = 1;

	$result->{input} =  $term->{input};
	$result->{direct} =  $term->{direct};
	# title, direct and exact terms
	#$self->AddPositionsForWord($pos_hash, $xap_id, "T$term->{direct}", $direct_weight, $max_pos, $HALF_WIDTH);
	$self->AddPositionsForWord($pos_hash, $xap_id, $term->{direct}, $direct_weight, $max_pos, $HALF_WIDTH);
	if (exists($term->{exact})) {
		$result->{exact} =  $term->{exact};
		$self->AddPositionsForWord($pos_hash, $xap_id, $term->{exact}, $direct_weight, $max_pos, $HALF_WIDTH);
	}
	if (exists($term->{stemmed})) {
		$result->{stemmed} =  $term->{stemmed};
		#$self->AddPositionsForWord($pos_hash, $xap_id, "S$term->{stemmed}", $stemmed_weight, $max_pos, $HALF_WIDTH);
		#$self->AddPositionsForWord($pos_hash, $xap_id, "U$term->{stemmed}", $stemmed_weight, $max_pos, $HALF_WIDTH);
	}

	if (exists($term->{parts})) {
		$result->{parts} =  $term->{parts};
		foreach my $p (@{$term->{parts}}) {
			$self->AddPositionsForWord($pos_hash, $xap_id, $p->{direct}, $stemmed_weight, $max_pos, $HALF_WIDTH);
			if (exists($p->{stemmed})) {
				$self->AddPositionsForWord($pos_hash, $xap_id, "S$p->{stemmed}", $stemmed_weight, $max_pos, $HALF_WIDTH);
			}
			
		}
	}

	$result->{pos_hash} = $pos_hash;
	return $result;
}
# ============================================================================
# AddPositionsForWord
# ============================================================================
sub AddPositionsForWord
{
	my ($self, $pos_hash, $xap_id, $word, $weight, $max_pos, $HALF_WIDTH) = @_;
	my $i;
	my $val;

	my $pos = $self->GetPositionsForWord($xap_id, $word);
	if (defined($pos)) {
		foreach my $p (@$pos) {
			for ($i = $p - $HALF_WIDTH; $i < $p + $HALF_WIDTH; $i++) {
				if (($i > 0) and ($i <= $max_pos)) {
					$val =  ($HALF_WIDTH - abs($p - $i)) * $weight;
					if (!exists($pos_hash->{$i}) or ($val >= $pos_hash->{$i}) ) {
						$pos_hash->{$i} = $val;
					}
				}
			}
		}
		$self->PrintPosHash($pos_hash) if ($debug);
	}
	else {
		print STDERR "word $word not present in doc $xap_id\n" if ($debug);
	}
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Search 

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


