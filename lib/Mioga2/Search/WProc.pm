# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
WProc.pm : A zone is a part of a document.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Search::WProc;

use strict;
use utf8;
use Mioga2::Exception::Simple;
use Search::Xapian;

my $debug = 0;
# ============================================================================
# new
# ============================================================================
sub new
{
	my ($class, $params) = @_;
	print STDERR "Mioga2::Search::WProc new($class)\n" if ($debug);

	my $self = { };
	bless($self, $class);

	if (!exists($params->{lang})) {
		throw Mioga2::Exception::Simple("Mioga2::Search::WProc::new", "lang not given");
	}
	$self->{lang} = $params->{lang};

	$self->{lcode} = 0;
	if ($self->{lang} eq 'french') {
		$self->{lcode} = 1;
	}
	elsif ($self->{lang} eq 'english') {
		$self->{lcode} = 2;
	}
	else {
		throw Mioga2::Exception::Simple("Mioga2::Search::WProc::new", "lang not supported");
	}

	$self->{stem} = Search::Xapian::Stem->new($self->{lang});
	
	return $self;
}
# ============================================================================
# NormalizeWord
# ============================================================================
sub NormalizeWord
{
	my ($self, $word) = @_;
	print STDERR "Search::Mioga2::NormalizeWord($word)\n" if ($debug);

	# put all in lower case
	$word = lc($word);
	# Remove begining and ending blank or punctuation
	$word =~ s/^[\s[:punct:]]+//g;
	$word =~ s/[\s[:punct:]]+$//g;
	# Replace punctuation signs by blank, when they are more than 2 times 
	$word =~ s/([[:punct:]])\1+/ /g;
	# Replace control signs by blank
	$word =~ s/[[:cntrl:]]+/ /g;
	# Replace multiple blanks by one
	$word =~ s/[\s]+/ /g;

	my $exact = $word;
	# remove accent
	$word =~ tr/ÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ/aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn/;
	my $simple = $word;
	# Replace internal punctuation signs by blank
	$word =~ s/[[:punct:]]+/ /g;

	return $exact, $simple, split(' ', $word);
}
# ============================================================================
# StemWord
# ============================================================================
sub StemWord
{
	my ($self, $word) = @_;
	my $sw = $self->{stem}->stem_word($word);
	return $sw;
}
# ============================================================================
# ProcessWord
# returns array of words
# Eword => Exact word with accents
# Mword => total word with spaces in cas of multi-part word
# Tword => "title" word (weight > 1)
# Uword => stemmed "title" word (weight > 1)
# Sword => stemmed word (weight == 1)
# word  => standard word (weight == 1)
# ============================================================================
sub ProcessWord
{
	my ($self, $word, $weight) = @_;
	print STDERR "Search::Mioga2::ProcessWord($word, $weight)\n" if ($debug);
	my @result;

	my ($exact, $simple, @cw) = $self->NormalizeWord($word);
	if (length($word) > 1) {
		if ($self->{lcode} == 1) {
			#$word =~ s/^[a-z]+'//;  # suppress leading l', m', s', ...
		}
		elsif ($self->{lcode} == 2) {
		}

		if ($exact ne $word) {
	print STDERR "mot exact : $exact\n" if ($debug);
			push @result, "E$exact";
		}
		if (@cw > 1) {
			# case with a multi-part word
			push @result, "M$word";
			foreach my $w (@cw) {
				my $sw = $self->StemWord($w);
				if ($sw ne $w) {
					if ($weight > 1) {
						push @result, "U$sw";
					}
					else {
						push @result, "S$sw";
					}
				}
				if ($weight > 1) {
					push @result, "T$w";
				}
				else {
					push @result, "$w";
				}
			}
		}
		else {
			# standard word
			my $sw = $self->StemWord($word);
			if ($sw ne $word) {
				if ($weight > 1) {
					push @result, "U$sw";
				}
				else {
					push @result, "S$sw";
				}
			}
			if ($weight > 1) {
				push @result, "T$word";
			}
			else {
				push @result, "$word";
			}
		}
	}
	print STDERR "   word = $word  result = " . join(',', @result) . "\n" if ($debug);
	return @result;
}
# ============================================================================
# Private functions
# ============================================================================

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Search 

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


