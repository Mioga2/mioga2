# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Document.pm : Abstract class for document in Mioga2::Search.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Search::Document;

use strict;
use utf8;
use Mioga2::Search::Zone;
use Mioga2::Exception::Simple;
use Data::Dumper;
use HTML::TokeParser::Simple;
use HTML::Entities;
#use Devel::Peek;

my $debug = 0;
# ============================================================================
# new
# ============================================================================
sub new
{
	my ($class, $params) = @_;
	print STDERR "Search::Document new($class)\n" if ($debug);

	my $self = { };
	bless($self, $class);
	# error attribute is used to mark record in mioga DB
	$self->{error} = 0;
	# search engine configuration
	if (!exists($params->{search_config})) {
		throw Mioga2::Exception::Simple("Search::Document Initialize", "No search_config arg");
	}
	$self->{search_config} = $params->{search_config};
	# base dir for cache and Xapian
	if (!exists($params->{base_dir})) {
		throw Mioga2::Exception::Simple("Search::Document Initialize", "No base_dir arg");
	}
	$self->{base_dir} = $params->{base_dir};
	#
	#
	# Initialize global attributes
	#
	$self->{description} = "unknown";
	$self->{data} = {};
	$self->{lang} = undef;
	$self->{mioga_id} = undef;
	$self->{zones} = ();
	$self->{xapian_docid} = 0;
	$self->{error} = 0;
	$self->{doc_in_cache} = 0;
	$self->{cache_path} = $self->{base_dir} . "/" . $self->{search_config}->GetCacheDir();
	$self->{mode_create} = 0;
	$self->{document_infos} = "";
	if (exists($params->{create})) {
		$self->{mode_create} = 1;
		$self->{dbh} = $params->{create}->{config}->GetDBH();
		$self->{search_doc} = $params->{create}->{search_doc};
		
		$self->{mioga_id} = $self->{search_doc}->{mioga_id};
		if (defined($self->{search_doc}->{xapian_docid})) {
			$self->{xapian_docid} = $self->{search_doc}->{xapian_docid};
		}
	}
	elsif (exists($params->{from_cache})) {
		if (!exists($params->{from_cache}->{path})) {
			throw Mioga2::Exception::Simple("Search::Document Initialize", "No path arg");
		}
		$self->{path} = $params->{from_cache}->{path};
		$self->GetDocInCache();
	}
	elsif (exists($params->{in_cache})) {
		if (!exists($params->{in_cache}->{path})) {
			throw Mioga2::Exception::Simple("Search::Document Initialize", "No path arg");
		}
		$self->{path} = $params->{in_cache}->{path};
	}
	# xapian doc ID
	if (exists($params->{xapian_docid}) and defined($params->{xapian_docid})) {
		$self->{xapian_docid} = $params->{xapian_docid};
	}
	return $self;
}
# ============================================================================
# as_string
# ============================================================================
sub as_string
{
	my ($self) = @_;
	return $self->{description};
}
# ============================================================================
# DeleteAllZones
# ============================================================================
sub DeleteAllZones
{
	my ($self, $zone) = @_;
	# TODO verify if it destroy object ?
	$self->{zones} = ();
}
# ============================================================================
# AddZone
# ============================================================================
sub AddZone
{
	my ($self, $zone) = @_;
	push @{$self->{zones}}, $zone;
}
# ============================================================================
# GetError
# ============================================================================
sub GetError
{
	my ($self) = @_;
	return $self->{error};
}
# ============================================================================
# GetAppCode
# ============================================================================
sub GetAppCode
{
	my ($self) = @_;
	return $self->{app_code};
}
# ============================================================================
# GetGroupId
# ============================================================================
sub GetGroupId
{
	my ($self) = @_;
	return $self->{group_id};
}
# ============================================================================
# GetMiogaId
# ============================================================================
sub GetMiogaId
{
	my ($self) = @_;
	return $self->{mioga_id};
}
# ============================================================================
# GetLang
# ============================================================================
sub GetLang
{
	my ($self) = @_;
	return $self->{lang};
}
# ============================================================================
# GetZones
# ============================================================================
sub GetZones
{
	my ($self) = @_;
	return $self->{zones};
}
# ============================================================================
# GetXapianDocid
# ============================================================================
sub GetXapianDocid
{
	my ($self, $doc_id) = @_;

	return $self->{xapian_docid};
}
# ============================================================================
# SetXapianDocid
# ============================================================================
sub SetXapianDocid
{
	my ($self, $doc_id) = @_;
	$self->{xapian_docid} = $doc_id;
}
# ============================================================================
# GetData
# ============================================================================
sub GetData
{
	my ($self) = @_;
	return $self->{data};
}
# ============================================================================
# GetDocumentInfos
# ============================================================================
sub GetDocumentInfos
{
	my ($self) = @_;
	return $self->{document_infos};
}
# ============================================================================
# GetExtract
# ============================================================================
sub GetExtract
{
	my ($self, $izone, $pos, $half_width) = @_;
	print STDERR "Search::Document GetExtract($izone, $pos, $half_width)\n" if ($debug);
	print STDERR "zones ".Dumper($self->{zones})."\n" if ($debug);
	my $extract = $self->{zones}->[$izone]->GetExtract($pos, $half_width);

	return $extract;
}
# ============================================================================
# GetDocumentDescription
# ============================================================================
sub GetDocumentDescription
{
	my ($self) = @_;
	return $self->{description};
}
# ============================================================================
# GetDocumentMIME
# ============================================================================
sub GetDocumentMIME
{
	my ($self) = @_;
	return $self->{mime};
}
# ============================================================================
# GetDateInSec
# ============================================================================
sub GetDateInSec
{
	my ($self) = @_;
	return $self->{date};
}
# ============================================================================
# GetDocumentURL
# ============================================================================
sub GetDocumentURL
{
	my ($self) = @_;
	return $self->{url};
}
# ============================================================================
# GetDocumentGroupId
# ============================================================================
sub GetDocumentGroupId
{
	my ($self) = @_;
	return $self->{group_id};
}
# ============================================================================
# PurgeCache
# ============================================================================
sub PurgeCache
{
	my ($self) = @_;
	my $path = $self->{cache_path} . $self->{path};
	print STDERR "Search::Document PurgeCache($path)\n" if ($debug);
	if (-f $path) {
		unlink $path;
	}
	elsif (-d $path) {
		rmdir $path;
	}
}
# ============================================================================
# Private functions
# ============================================================================
# WriteInCache
# ============================================================================
sub WriteInCache
{
	my ($self, $file_path) = @_;
	print STDERR "Search::Document WriteInCache($file_path)\n" if ($debug);

	my @path_elems = split('/', $file_path);
	my $cache_path = $self->{cache_path};
	my $filename = pop @path_elems;
	print STDERR "   filename = $filename\n" if ($debug);
	foreach my $d (@path_elems) {
		print STDERR "   verify or create subdir = $d\n" if ($debug);
		$cache_path .= "$d";
		if (!-d $cache_path) {
			mkdir $cache_path or die "Cannot create dir $cache_path\n";
		}
		$cache_path .= "/";
	}
	print STDERR "   cache_path = $cache_path\n" if ($debug);
	#print Dump($cache_path) if ($debug);

	$cache_path .= $filename;
	if (open(FILE, ">:utf8", "$cache_path")) {
		print FILE "#app_code:$self->{app_code}\n";
		print FILE "#app_id:$self->{app_id}\n";
		print FILE "#url:$self->{url}\n";
		print FILE "#description:$self->{description}\n";
		print FILE "#mime:$self->{mime}\n";
		print FILE "#group_id:$self->{group_id}\n";
		print FILE "#date:$self->{date}\n";
		print STDERR "   data = ".Dumper($self->{data})."\n" if ($debug);
		foreach my $key (keys(%{$self->{data}})) {
			print STDERR "   data key = $key\n" if ($debug);
			foreach my $entry (keys(%{$self->{data}->{$key}})) {
				print STDERR "   data entry = $entry\n" if ($debug);
				print FILE "#data:$key!$entry!".$self->{data}->{$key}->{$entry}."\n";
			}
		}
		foreach my $z (@{$self->{zones}}) {
			print FILE "!zone;". $z->GetName() .";". $z->GetWeight() .";". $z->GetNumberOfLines() .";".$z->GetTotalWords()."\n";
			my $datas = $z->GetData();
			foreach my $l (@$datas) {
				print FILE "%$l\n";
			}
		}
		close(FILE);
	}
	else {
		throw Mioga2::Exception::Simple("Search::Document WriteInCache", "Cannot create cache file $filename");
	}
}
# ============================================================================
# GetDocInCache
# ============================================================================
sub GetDocInCache
{
	my ($self) = @_;
	print STDERR "Search::Document GetDocInCache()\n" if ($debug);
	my $retval = 0;

	$self->{zones} = ();
	my $path = $self->{cache_path} . $self->{path};
	print STDERR "path = $path\n" if ($debug);
	if (open(FILE, "<:utf8", "$path")) {
		my $line;
		my $nu = 1;
		my $name;
		my $weight;
		my $mime;
		my $zone;
		my $nb_lines;
		my $size;
		my $iline;
		while ($line = <FILE>) {
			chomp($line);
			print STDERR "    line = $line  is_utf8 : " . utf8::is_utf8($line) . "\n" if ($debug);
			#print Dump($line) if ($debug);
			if (substr($line, 0, 1) eq '%') {
				if (!defined($zone)) {
					throw Mioga2::Exception::Simple("Search::Document GetDocInCache", "Bad syntax on cache file ($self->{cache_path}), line $nu (no zone)");
				}
				if ($iline > $nb_lines) {
					throw Mioga2::Exception::Simple("Search::Document GetDocInCache", "Bad syntax on cache file ($self->{cache_path}), line $nu (too much lines for zone)");
				}
				$zone->AddData(substr($line, 1));
				$iline++;
			}
			# Decode document infos
			elsif (substr($line, 0, 1) eq '#') {
				$line =~ /^#([^:]*):(.*)$/;
				if (!defined($1) or !defined($2)) {
					throw Mioga2::Exception::Simple("Search::Document GetDocInCache", "Bad syntax on cache file, line $nu (#line)");
				}
				if ($1 eq 'data') {
					my ($key, $entry, $value);
					my $arg = $2;
					$arg =~ /^([^!]*)!([^!]*)!(.*)$/;
					print STDERR "arg = $arg  1 = $1  2 = $2 3 = $3\n" if ($debug);
					if (defined($1) && defined($2) && defined($3)) {
						$self->{data}->{$1}->{$2} = $3;
					}
				}
				else {
					$self->{$1} = $2;
				}
			}
			# Decode zones
			elsif (substr($line, 0, 1) eq '!') {
				my @codes = split(';', substr($line, 1));
				if ($codes[0] ne "zone") {
					throw Mioga2::Exception::Simple("Search::Document GetDocInCache", "Bad syntax on cache file, line $nu (!line)");
				}
				$zone = Mioga2::Search::Zone->new({ name => $codes[1],
													weight => $codes[2],
												});
				$self->AddZone($zone);
				$nb_lines = $codes[3];
				$size = $codes[4];
				print STDERR "nb_lines = $nb_lines  size = $size\n" if ($debug);
				$iline = 0;
			}
			else {
				throw Mioga2::Exception::Simple("Search::Document GetDocInCache", "Bad syntax on cache file, line $nu");
			}
			$nu++;
		}
		close(FILE);
		$retval = 1;
	}
	else {
		print STDERR "    file $path not in cache\n" if ($debug);
	}
	return $retval;
}
# ============================================================================
# HTMLToText
# ============================================================================
sub HTMLToText
{
	my ($self, $html) = @_;
	print STDERR "Search::Document HTMLToText($html)\n" if ($debug);

	HTML::Entities::decode_entities($html);


=head2

	$html =~ s/<[^>]*>/ /g;

=cut
	
	my $parser = HTML::TokeParser::Simple->new(\$html);
	my $jump = 0;
	my $data;
	while ( my $token = $parser->get_token ) {
		if ($token->is_start_tag('script')) {
			$jump = 1;
		}
		if ($token->is_end_tag('script')) {
			$jump = 0;
		}
		next unless $token->is_text;
		if (! $jump) {
			$data .= $token->as_is;
		}
	}
	print STDERR "   html = $data\n" if ($debug);

	return $data;
}
# ============================================================================
# CleanLine
# ============================================================================
sub CleanLine
{
	my ($self, $line) = @_;
	print STDERR "Search::Document CleanLine()\n" if ($debug);

	$line =~ s/[\s]+/ /g;
	$line =~ s/^ //;
	$line =~ s/ $//;

	return $line;
}
# ============================================================================
# GetLangFromCode
# ============================================================================
sub GetLangFromCode
{
	my ($self, $code) = @_;
	print STDERR "Search::Document GetLangFromCode($code)\n" if ($debug);
	my $lang;
	my %langs = ( fr_FR => 'french',
	              en_US => 'english',
				  );
	if (exists($langs{$code})) {
		$lang = $langs{$code};
	}
	else {
		$lang = 'french';
	}

	return $lang;
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Search 

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


