# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Search::Document::File.pm : File document class in Mioga2::Search.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Search::Document::File;

use strict;
use utf8;
use base qw(Mioga2::Search::Document);
use File::Basename;
use HTML::TokeParser::Simple;
use HTML::Entities;
use Data::Dumper;
use XML::LibXML;
use Encode::Detect::Detector;
use Text::Iconv;
use POSIX qw(tmpnam);
use Mioga2::tools::database;
use Mioga2::Classes::Time;
use Mioga2::Louvre;
#use Devel::Peek 'Dump';

my $debug = 0;
# ============================================================================
# new
# ============================================================================
sub new
{
	my ($class, $params) = @_;
	print STDERR "Search::Document::File new($class)\n" if ($debug);

	my $self = $class->SUPER::new($params);
	bless($self, $class);
	$self->Initialize($params);

	return $self;
}
# ============================================================================
# GetDeletedDocuments
# ============================================================================
sub GetDeletedDocuments
{
	my ($class, $app_code, $dbh) = @_;
	print STDERR "Search::Document::File GetDeletedDocuments($class)\n" if ($debug);

	my $sql = "DELETE FROM search_file WHERE rowid in (SELECT search_file.rowid from search_file LEFT JOIN m_uri ON (search_file.uri_id = m_uri.rowid)"
															." WHERE m_uri.rowid IS NULL)";
	printf STDERR "sql = $sql\n" if ($debug);
	ExecSQL($dbh, $sql);
	$sql = "SELECT t2.rowid FROM (select rowid, app_id from search_doc where app_code=$app_code AND fl_deleted='f') as t2"
																." LEFT JOIN search_file ON (search_file.uri_id = t2.app_id)"
					." WHERE search_file.rowid IS NULL limit 100";
	printf STDERR "sql = $sql\n" if ($debug);
	my $result = SelectMultiple($dbh, $sql);
	return $result;
}
# ============================================================================
# GetNewDocuments
# ============================================================================
sub GetNewDocuments
{
	my ($class, $app_code, $config) = @_;
	print STDERR "Search::Document::File GetNewDocuments($class)\n" if ($debug);
	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	my $webdav_dir = $config->GetWebdavDir();
	my $base_URI = $config->GetBaseURI();

	#
	# Update list of indexable files
	#

	my $sql = "SELECT m_uri.* FROM m_uri LEFT JOIN search_file ON (m_uri.rowid = search_file.uri_id), m_group_base"
					." WHERE m_uri.group_id = m_group_base.rowid"
							." AND m_group_base.mioga_id=$mioga_id"
							. " AND search_file.rowid IS NULL limit 1000";
	printf STDERR "sql = $sql\n" if ($debug);
	my $res;
	do {
		$res = SelectMultiple($dbh, $sql);
		printf STDERR "   count = " . scalar(@$res) . "\n" if ($debug);
		foreach my $entry (@$res) {
			my $path = $entry->{uri};
			$path =~ s/^$base_URI/$webdav_dir/;
			printf STDERR "===> Process $path\n" if ($debug);
			if (-f $path) {
				my ($fl_index, $gallery_id) = $class->IsGallery($dbh, $entry);
				my @st = stat($path);
				print STDERR "path = $path\n" if ($debug);
				print STDERR "stat = " .join(',', @st) . "\n" if ($debug);
				my $mime = $entry->{mimetype};

				print STDERR "mime = <$mime>\n" if ($debug);

				if (defined($gallery_id)) {
					ExecSQL($dbh, "INSERT INTO search_file (uri_id, mioga_id, fl_index, gallery_id) VALUES ($entry->{rowid}, $mioga_id, '$fl_index', $gallery_id)");
				}
				else {
					ExecSQL($dbh, "INSERT INTO search_file (uri_id, mioga_id, fl_index) VALUES ($entry->{rowid}, $mioga_id, '$fl_index')");
				}
			}
			else {
				ExecSQL($dbh, "INSERT INTO search_file (uri_id, mioga_id, fl_index) VALUES ($entry->{rowid}, $mioga_id, 'f')");
			}
		}
	} while (scalar(@$res) > 0);

	#
	# Update search_doc
	#
	$sql =  "SELECT search_file.uri_id as app_id"
					." FROM search_file LEFT JOIN (select rowid, app_id from search_doc where app_code=$app_code) as t2"
																					." ON (search_file.uri_id = t2.app_id)"
					." WHERE search_file.mioga_id=$mioga_id"
							. " AND search_file.fl_index = 't'"
							. " AND t2.rowid IS NULL limit 1000";

	printf STDERR "sql = $sql\n" if ($debug);
	my $result = SelectMultiple($dbh, $sql);
	return $result;



	return $result;
}
# ============================================================================
# SetModifiedDocuments
# ============================================================================
sub SetModifiedDocuments
{
	my ($class, $app_code, $config, $force_flag) = @_;
	print STDERR "Search::Document::File SetModifiedDocuments($class, $app_code, $force_flag)\n" if ($debug);
	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	my $webdav_dir = $config->GetWebdavDir();
	my $base_URI = $config->GetBaseURI();
	my $sql;

	#
	# Update search_doc table
	#
	if ($force_flag) {
		$sql = "UPDATE search_doc SET modified=now() WHERE search_doc.app_code=$app_code AND search_doc.mioga_id=$mioga_id";
	}
	else {
		$sql = "UPDATE search_doc SET modified=(SELECT modified FROM m_uri WHERE rowid=app_id)"
						." FROM m_uri WHERE search_doc.app_code=$app_code AND search_doc.mioga_id=$mioga_id"
						." AND app_id=m_uri.rowid AND search_doc.modified != m_uri.modified";
	}
	printf STDERR "sql = $sql\n" if ($debug);
	ExecSQL($dbh, $sql);
}
# ============================================================================
# VerifyAuthz
# ============================================================================
sub VerifyAuthz
{
	my ($class, $app_ids, $config, $user_id) = @_;
	print STDERR "Search::Document::File VerifyAuthz($class, $user_id)\n" if ($debug);
	my $dbh = $config->GetDBH();
	my $list = join(',', @$app_ids);

	my $sql = "SELECT uri_id, (max(accesskey)%10) AS accesskey"
				." FROM ( SELECT m_uri.rowid AS uri_id, (50 + m_authorize.access) AS accesskey"
							." FROM m_authorize, m_uri, m_authorize_user"
							." WHERE  m_uri.rowid IN ($list)"
										." AND m_authorize.uri_id = m_uri.parent_uri_id"
										." AND m_authorize_user.authz_id = m_authorize.rowid"
										." AND m_authorize_user.user_id=$user_id"
						." UNION SELECT m_uri.rowid AS uri_id, (40 + m_authorize.access) AS accesskey"
							." FROM m_authorize, m_uri, m_authorize_team, m_group_group"
							." WHERE  m_uri.rowid IN ($list) AND m_authorize.uri_id = m_uri.parent_uri_id"
										." AND m_authorize_team.authz_id = m_authorize.rowid"
										." AND m_group_group.group_id = m_authorize_team.team_id"
										." AND m_group_group.invited_group_id = $user_id"
						." UNION SELECT m_uri.rowid AS uri_id, (30 + m_authorize.access) AS accesskey"
							." FROM m_authorize, m_uri, m_authorize_profile, m_profile_group"
							." WHERE  m_uri.rowid IN ($list)"
										." AND m_authorize.uri_id = m_uri.parent_uri_id"
										." AND m_authorize_profile.authz_id = m_authorize.rowid"
										." AND m_profile_group.profile_id = m_authorize_profile.profile_id"
										." AND m_profile_group.group_id = $user_id"
						." UNION SELECT m_uri.rowid AS uri_id, (20 + m_authorize.access) AS accesskey"
							." FROM m_authorize, m_uri, m_authorize_profile, m_profile_expanded_user"
							." WHERE  m_uri.rowid IN ($list)"
										." AND m_authorize.uri_id = m_uri.parent_uri_id"
										." AND m_authorize_profile.authz_id = m_authorize.rowid"
										." AND m_profile_expanded_user.profile_id = m_authorize_profile.profile_id"
										." AND m_profile_expanded_user.user_id = $user_id ) AS tmp_req"
				." GROUP BY (uri_id)";
	my $res = SelectMultiple($dbh, $sql);

	my $authz_uri;
	foreach my $r (@$res) {
		print STDERR "  uri_id = $r->{uri_id}  accesskey = $r->{accesskey}\n" if ($debug);
		$authz_uri->{$r->{uri_id}} = $r->{accesskey};
	}
	return $authz_uri;
}
# ============================================================================
# GetUriId
# ============================================================================
sub GetUriId
{
	my ($self) = @_;
	return $self->{app_id};
}
# ============================================================================
# GetDocumentURL
# ============================================================================
sub GetDocumentURL
{
	my ($self) = @_;
	return "$self->{base_uri}/$self->{path}";
}
# ============================================================================
# GetDocumentMIME
# ============================================================================
sub GetDocumentMIME
{
	my ($self) = @_;
	return $self->{mime};
}
# ================================================================
# GetMimeTypeFromExt
# ================================================================
sub GetMimeTypeFromExt {
	my ($class, $path) = @_;
	my %types = ( pdf => 'application/pdf',
			  doc => 'application/msword',
			  xls => 'application/msexcel',
			);

	my $mime = "unknown";

	$path =~ /\.([^\.]+)$/;
	my $ext = lc($1);
	print STDERR "   ext = $ext\n" if ($debug);
	if (exists($types{$ext})) {
		$mime = $types{$ext};
	}
	return $mime;
}
# ============================================================================
# Private functions
# ============================================================================
# Initialize
# ============================================================================
sub Initialize
{
	my ($self, $params) = @_;
	print STDERR "Search::Document::File Initialize()\n" if ($debug);
	#
	# if doc creation read doc in Mioga and put it to cache
	# else get doc in cache or die
	#
	if ($self->{mode_create}) {
		print STDERR "    Create mode\n" if ($debug);
		$self->{app_code} = $self->{search_doc}->{app_code};
		$self->{app_id} = $self->{search_doc}->{app_id};
		$self->{lang} = $self->{search_doc}->{lang};
		$self->{config} = $params->{create}->{config};
		#
		# Read doc and set cache file
		#
		$self->ReadDoc();
		$self->{document_infos} .= "app_code:$self->{app_code}\n";
		$self->{document_infos} .= "app_id:$self->{app_id}\n";
		$self->{document_infos} .= "path:$self->{path}\n";
		$self->{description} = basename($self->{path})." ($self->{path})";
		if ($self->{error} == 0) {
			$self->WriteInCache($self->{path});
		}
	}
	if ($self->{error} == 0) {
		$self->{doc_in_cache} = $self->GetDocInCache();
		if (! $self->{doc_in_cache}) {
			throw Mioga2::Exception::Simple("Search::Document::File Initialize", "Document for doc_id=$self->{xapian_docid} not in cache");
		}
	}
}
# ============================================================================
# ReadDoc
# ============================================================================
sub ReadDoc
{
	my ($self) = @_;
	print STDERR "Search::Document::File ReadDoc()\n" if ($debug);
	my $retval = 0;
	#
	# Reset zones
	#
	$self->DeleteAllZones();
	#
	# Get datas from MiogaDB
	#
	my $sql = "SELECT search_file.fl_index, search_file.gallery_id, m_uri.*,uri_data.description as description, uri_data.tags as tags"
	                     ." FROM search_file, m_uri left join uri_data on m_uri.rowid=uri_data.uri_id"
						." WHERE search_file.uri_id=$self->{app_id} AND m_uri.rowid=$self->{app_id}";
	print STDERR " sql = $sql\n" if ($debug);
	my $res = SelectSingle($self->{dbh}, $sql);
	if (!defined($res)) {
		throw Mioga2::Exception::Simple("Search::Document::File Read", "No record for rowid=$self->{app_id} in table m_uri");
	}
	$self->{group_id} = $res->{group_id};
	$self->{url} = $res->{uri};
	utf8::decode($self->{url});
	$self->{mime} = $res->{mimetype};
	my $date = Mioga2::Classes::Time->FromPSQL($res->{modified});
	$self->{date} = $date->epoch;
	$self->{uri_description} = $res->{description};
	$self->{tags} = $res->{tags};

	my $path = $res->{uri};
	my $base_URI = $self->{config}->GetBaseURI();
	$self->{webdav_dir} = $self->{config}->GetWebdavDir();
	print STDERR "   mime = $res->{mimetype}\n" if ($debug);
	$self->{mime} = $res->{mimetype};
	$path =~ s/^$base_URI//;
	$self->{path} = $path;
	utf8::decode($self->{path});
	print STDERR "   path  = $self->{path}\n" if ($debug);
	#Dump($self->{path}) if ($debug);
	#
	# Get gallery informations if any, and create zone for them
	#
	my ($fl_index, $gallery_id) = $self->IsGallery($self->{dbh}, $res);
	if ($gallery_id) {
		if (!defined ($res->{gallery_id})) {
			# File already in search-engine database but not as part of a gallery
			# Now file is part of a gallery, update search-engine database
			$res->{gallery_id} = $gallery_id;
			ExecSQL ($self->{dbh}, "UPDATE search_file SET gallery_id = $gallery_id WHERE uri_id = $res->{rowid}");
		}
		print STDERR "   gallery file gallery_id = $res->{gallery_id}\n" if ($debug);
		$sql = "SELECT m_uri.uri, m_louvre.rowid AS louvre_rowid FROM m_uri, m_louvre WHERE m_louvre.uri_id=m_uri.rowid AND m_uri.rowid=$res->{gallery_id}";
		print STDERR " sql = $sql\n" if ($debug);
		my $gal = SelectSingle($self->{dbh}, $sql);
		if (!defined($gal)) {
			throw Mioga2::Exception::Simple("Search::Document::File Read", "No uri record for rowid=$res->{gallery_id} in table m_uri");
		}
		if (!defined($gal->{louvre_rowid})) {
			print STDERR "   gallery = $res->{gallery_id} doesn't exists\n" if ($debug);
		}
		else {
			print STDERR "   gallery uri = $gal->{uri}\n" if ($debug);
			my $filename = $res->{uri};
			print STDERR "   filename 1 = $filename\n" if ($debug);
			$filename =~ s/^$gal->{uri}//;
			print STDERR "   filename 2 = $filename\n" if ($debug);
			my $thumbnail = $filename;
			$thumbnail =~ s#\.[^.]+$#_tn.jpg#;  # got from Louvre _thumbnail_file 
			print STDERR "   thumbnail 1 = $thumbnail\n" if ($debug);
			$thumbnail = $gal->{uri} . "/". $Mioga2::Louvre::Const::LOUVRE_DIR . $thumbnail;
			print STDERR "   thumbnail 2 = $thumbnail\n" if ($debug);
			my $val = $gal->{uri};
			utf8::decode($val);
			print STDERR "   gal uri peek \n" if ($debug);
			#Dump($val) if ($debug);
			$self->{data}->{gallery}->{gallery_uri} = $gal->{uri};
			utf8::decode($thumbnail);
			#print STDERR "   thumbnail peek = ".Dump($thumbnail)."\n" if ($debug);
			$self->{data}->{gallery}->{thumbnail_uri} = $thumbnail;
		}
	}
	else {
		# File is no-more part of a gallery
		ExecSQL ($self->{dbh}, "UPDATE search_file SET gallery_id = NULL WHERE uri_id = $res->{rowid}");
	}
	#
	# Create zone for path and filename
	#
	#TODO get weight from config
	my $p = $self->{path};
	#$p =~ s/\./ /g;
	$p =~ s/[-_.\/]/ /g;
	#my @path_elems = split('/', $p);
	my $zone = Mioga2::Search::Zone->new({ name => 'path', weight => 2, });
	#$zone->FormatDataArray(\@path_elems);
	$zone->FormatDataString($p);
	$self->AddZone($zone);
	#
	# Create zone for tags
	#
	if (defined($self->{tags})) {
		utf8::decode($self->{tags});
		print STDERR "   tags = ".$self->{tags}."  tags peek\n" if ($debug);
		#Dump($self->{tags}) if ($debug);
		my @tags_elems = split('/', $self->{tags});
		my $z_tags = Mioga2::Search::Zone->new({ name => 'tags', weight => 5, });
		$z_tags->FormatDataArray(\@tags_elems);
		$self->AddZone($z_tags);
	}
	#
	# Create zone for description
	#
	if (defined($self->{uri_description})) {
		utf8::decode($self->{uri_description});
		print STDERR "   uri_description = ".$self->{uri_description}."  uri_description peek\n" if ($debug);
		#Dump($self->{uri_description}) if ($debug);
		my $z_desc = Mioga2::Search::Zone->new({ name => 'description', weight => 4, });
		$z_desc->FormatDataString($self->CleanLine($self->{uri_description}));
		$self->AddZone($z_desc);
	}
	#
	# Get comments and create zone
	#
	$sql = "SELECT file_comment.* FROM file_comment WHERE uri_id=".$self->{app_id};
	print STDERR " sql = $sql\n" if ($debug);
	$res = SelectMultiple($self->{dbh}, $sql);
	if (!defined($res)) {
		throw Mioga2::Exception::Simple("Search::Document::File Read", "No record for rowid=$self->{app_id} in table file_comment");
	}
	my $comment = "";
	foreach my $c (@$res) {
		$comment .= $c->{comment} . " ";
	}
	if (length($comment) > 0) {
		my $z_comment = Mioga2::Search::Zone->new({ name => 'comment', weight => 3, });
		$z_comment->FormatDataString($self->CleanLine($comment));
		$self->AddZone($z_comment);
	}
	#
	# Create other zones
	#
	eval { $self->ReadAndParseFile(); };
	if ($@) {
		warn("Search::Document::File::ReadAndParseFile : $self->{path}\nerror  [$@]");
		$self->{error} = 1;
	}
}
# ============================================================================
# ReadAndParseFile
# ============================================================================
sub ReadAndParseFile
{
	my ($self) = @_;
	print STDERR "Search::Document::File ReadAndParseFile($self->{path})\n" if ($debug);
	print STDERR "   mime = $self->{mime}\n" if ($debug);
	my $path = $self->{webdav_dir} . $self->{path};
	my $real_mime = $self->{mime};
	my $tmpname;
	#
	# For MIME type with pre-processing, call preprocessor on temporary file
	# from config hash
	#
	my $mime_actions = $self->{search_config}->GetMimeActions();
	if ($self->{mime} ne 'text/plain' and $self->{mime} ne 'text/html' and $self->{mime} ne 'application/xml') {
		if (exists($mime_actions->{$self->{mime}})) {
			my $config = $mime_actions->{$self->{mime}};
			$real_mime = $config->{res_mime};
			do { $tmpname = tmpnam() } while (-e $tmpname);
			# Escape characters for shell exec in path
			$path =~ s/\\/\\\\/g;
			$path =~ s/"/\"/g;
			$path =~ s/\$/\\\$/g;
			my $cmd = sprintf ($config->{command}." > %s", $path, $tmpname);
			print STDERR "    tmpname = $tmpname   cmd = $cmd\n" if ($debug);
			system($cmd);
			$path = $tmpname;
			my @st = stat($path);
			if ($st[7] == 0) {
				warn("Search::Document::File ReadAndParseFile() result file size equals 0 for $cmd");
				warn("    path = $self->{path} mioga_id : $self->{mioga_id} app_code = $self->{app_code}  uri_id/app_id : $self->{app_id}");
				$self->{error} = 1;
				return;
			}
		}
		else {
			#warn("Search::Document::File ReadAndParseFile() Cannot process mime type $self->{mime}");
			#warn("    path = $self->{path} mioga_id : $self->{mioga_id} app_code = $self->{app_code}  uri_id/app_id : $self->{app_id}");
			#$self->{error} = 1;
			return;
		}
	}
	#
	# Try to guess encoding and create converter if needed
	#
	my $encoding = $self->GuessEncoding($path);
	my $converter;
	print STDERR "    encoding = $encoding\n" if ($debug);
	if ($encoding ne "UTF-8") {
		$converter = Text::Iconv->new($encoding, "UTF-8");
		if (!defined($converter)) {
			throw Mioga2::Exception::Simple("Search::Document::File ReadAndParseFile", "Cannot create converter for encoding $encoding");
		}
	}
	#
	# Guess language, if not given
	#
	if (!defined($self->{lang}) or (length($self->{lang}) > 0)) {
		$self->{lang} = $self->GuessLang($path);
	}
	print STDERR "    lang = $self->{lang}\n" if ($debug);
	#
	# Process text/plain files
	#
	my $content = Mioga2::Search::Zone->new({ name => 'content',
											weight => 1,
										});
	$self->AddZone($content);
	if ($real_mime eq 'text/plain') {
		if (open(FILE, "<$path")) {
			my $line;
			while ($line = <FILE>) {
				chomp($line);
				if (defined($converter)) {
					$line = $converter->convert($line);
				}
				utf8::decode($line);
				print STDERR "    line = $line\n" if ($debug);
				$content->FormatDataString($self->CleanLine($line));
			}
			close FILE;
		}
		else {
			throw Mioga2::Exception::Simple("Search::Document::File ReadAndParseFile", "Cannot open file $path");
		}
	}
	#
	# Process text/html files
	#
	elsif ($real_mime eq 'text/html') {
		my $data;

		my $parser = HTML::TokeParser::Simple->new(file => $path);
		if (defined($parser)) {
			my $token;
			my $data = "";
			my $jump = 0;
			while ( my $token = $parser->get_token ) {
				if ($token->is_start_tag('script')) {
					$jump = 1;
				}
				if ($token->is_end_tag('script')) {
					$jump = 0;
				}
				next unless $token->is_text;
				if (! $jump) {
					$data .= $token->as_is;
				}
			}
			if (defined($converter)) {
				$data = $converter->convert($data);
			}
			utf8::decode($data);
			HTML::Entities::decode_entities($data);
			$content->FormatDataString($self->CleanLine($data));
		}
		else {
			throw Mioga2::Exception::Simple("Search::Document::File ReadAndParseFile", "Cannot parse HTML file $path");
		}
	}
	elsif ($real_mime eq 'application/xml') {
		my $parser = XML::LibXML->new();
		my $doc = $parser->parse_file($path);
		my $docstring = $doc->toString(2);
		$docstring =~ s/<[^>]*>/ /g;
		print STDERR "    docstring = $docstring\n" if ($debug);
		$content->FormatDataString($self->CleanLine($docstring));
	}
	#
	# Unknown mime => error
	#
	else {
		throw Mioga2::Exception::Simple("Search::Document::File ReadAndParseFile", "Unknown real mime type : $real_mime");
	}
	#
	# Purge tmp file if any
	#
	if (defined($tmpname)) {
		unlink ($tmpname);
	}
}
# ============================================================================
# IsGallery
# ============================================================================
sub IsGallery
{
	my ($self, $dbh, $entry) = @_;
	print STDERR "Search::Document IsHidden($entry->{uri})\n" if ($debug);
	my $fl_index = 't';
	my $gallery_id;
	# if file is in LOUVRE_DIR folder
	my $res = Mioga2::Louvre::Gallery::SQL->uri_is_gallery($dbh, $entry->{uri});
	print STDERR " uri_is_gallery ".Dumper($res)."\n" if ($debug);
	if ($res->{type} eq 's') {
		my $hidden_dir = $res->{path}."/".$Mioga2::Louvre::Const::LOUVRE_DIR;
		$gallery_id = $res->{rowid};
		print STDERR " hidden_dir $hidden_dir\n" if ($debug);
		if ($entry->{uri} =~ /^$hidden_dir/) {
			$fl_index = 'f';
		}
	}

	return ($fl_index, $gallery_id);
}
# ============================================================================
# GuessLang
# ============================================================================
sub GuessLang
{
	my ($self, $path) = @_;
	print STDERR "Search::Document GuessLang($path)\n" if ($debug);
	#TODO check language for file
	return "french";
}
# ============================================================================
# GuessEncoding
# ============================================================================
sub GuessEncoding
{
	my ($self, $path) = @_;
	print STDERR "Search::Document GuessEncoding($path)\n" if ($debug);
	my $encoding;

	#
	# Try to guess encoding
	#
	my $detector = new Encode::Detect::Detector;
	my $line;
	my $length = 0;
	if (open(FILE, "<$path")) {
		while ((!defined($encoding)) and ($length < 20000) and ($line = <FILE>)) {
			$detector->handle($line);
			$encoding = $detector->getresult;
			$length += length($line);
		}
		close FILE;
	}
	else {
		throw Mioga2::Exception::Simple("Search::Document::File GuessEncoding", "Cannot open file $path");
	}

	if (!defined($encoding)) {
		$detector->eof;
		$encoding = $detector->getresult;
	}
	if (!defined($encoding)) {
		$encoding = "UTF-8";
	}
	print STDERR "encoding : $encoding\n" if ($debug);
	return $encoding;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Search 

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


