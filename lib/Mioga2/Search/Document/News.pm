# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Search::Document::News.pm : News document class in Mioga2::Search.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Search::Document::News;

use strict;
use utf8;
use base qw(Mioga2::Search::Document);
use Data::Dumper;
use HTML::TokeParser::Simple;
use Mioga2::tools::database;

my $debug = 0;
# ============================================================================
# new
# ============================================================================
sub new
{
	my ($class, $params) = @_;
	print STDERR "Search::Document::News new($class)\n" if ($debug);

	my $self = $class->SUPER::new($params);
	bless($self, $class);
	$self->Initialize($params);

	return $self;
}
# ============================================================================
# GetDeletedDocuments
# ============================================================================
sub GetDeletedDocuments
{
	my ($class, $app_code, $dbh) = @_;
	print STDERR "Search::Document::News GetDeletedDocuments($class)\n" if ($debug);

	my $sql = "SELECT t2.rowid FROM (SELECT rowid, app_id FROM search_doc WHERE app_code=$app_code AND fl_deleted='f') as t2"
									." LEFT JOIN news ON (news.rowid = t2.app_id)"
									." WHERE news.rowid IS NULL limit 100";
	printf STDERR "sql = $sql\n" if ($debug);
	my $result = SelectMultiple($dbh, $sql);
	return $result;
}
# ============================================================================
# GetNewDocuments
# ============================================================================
sub GetNewDocuments
{
	my ($class, $app_code, $config) = @_;
	print STDERR "Search::Document::News GetNewDocuments($class)\n" if ($debug);
	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();

	my $sql =  "SELECT news.rowid as app_id FROM news LEFT JOIN (select rowid, app_id from search_doc where app_code=$app_code) as t2"
																					." ON (news.rowid = t2.app_id), m_group_base"
					." WHERE news.group_id = m_group_base.rowid"
							." AND m_group_base.mioga_id=$mioga_id"
							. " AND t2.rowid IS NULL limit 1000";
	printf STDERR "sql = $sql\n" if ($debug);
	my $result = SelectMultiple($dbh, $sql);
	return $result;
}
# ============================================================================
# SetModifiedDocuments
# ============================================================================
sub SetModifiedDocuments
{
	my ($class, $app_code, $config, $force_flag) = @_;
	print STDERR "Search::Document::News SetModifiedDocuments($class, $app_code, $force_flag)\n" if ($debug);
	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	my $sql;

	if ($force_flag) {
		$sql = "UPDATE search_doc SET modified=now() WHERE search_doc.app_code=$app_code AND search_doc.mioga_id=$mioga_id";
	}
	else {
		$sql = "UPDATE search_doc SET modified=(SELECT modified FROM news WHERE rowid=app_id)"
						." FROM news WHERE search_doc.app_code=$app_code AND search_doc.mioga_id=$mioga_id"
						." AND app_id=news.rowid AND search_doc.modified != news.modified";
	}
	printf STDERR "sql = $sql\n" if ($debug);
	ExecSQL($dbh, $sql);
}
# ============================================================================
# VerifyAuthz
# ============================================================================
sub VerifyAuthz
{
	my ($class, $app_ids, $config, $user_id) = @_;
	print STDERR "Search::Document::News VerifyAuthz($class, $user_id)\n" if ($debug);
	my $dbh = $config->GetDBH();
	#
	# TODO test in database
	#
	#
	my $authz_uri = {};
	foreach my $r (@$app_ids) {
		$authz_uri->{$r} = 1;
	}
	return $authz_uri;
}
# ============================================================================
# Private functions
# ============================================================================
# Initialize
# ============================================================================
sub Initialize
{
	my ($self, $params) = @_;
	print STDERR "Search::Document::News Initialize()\n" if ($debug);
	#
	# if doc creation read doc in Mioga and put it to cache
	# else get doc in cache or die
	#
	if ($self->{mode_create}) {
		print STDERR "    Create mode\n" if ($debug);
		$self->{app_code} = $self->{search_doc}->{app_code};
		$self->{app_id} = $self->{search_doc}->{app_id};
		$self->{lang} = $self->{search_doc}->{lang};
		$self->{config} = $params->{create}->{config};
		#
		# Read doc and set cache file
		#
		$self->ReadDoc();
		$self->{document_infos} .= "app_code:$self->{app_code}\n";
		$self->{document_infos} .= "app_id:$self->{app_id}\n";
		my $file_path = "/news/$self->{group_id}/$self->{app_id}";
		$self->{document_infos} .= "path:$file_path\n";
		$self->WriteInCache($file_path);
		$self->{path} = $file_path;

	}
	#else {
		$self->{doc_in_cache} = $self->GetDocInCache();
		if (! $self->{doc_in_cache}) {
			throw Mioga2::Exception::Simple("Search::Document::News Initialize", "Document for doc_id=$self->{xapian_docid} not in cache");
		}
	#}
	$self->{description} = ""; 
}
# ============================================================================
# ReadDoc
# ============================================================================
sub ReadDoc
{
	my ($self) = @_;
	print STDERR "Search::Document::News ReadDoc()\n" if ($debug);
	my $retval = 0;
	#
	# Reset zones
	#
	$self->DeleteAllZones();
	#
	# Read datas from DB
	#
	my $sql = "SELECT news.group_id, news.title, news.text,m_lang.ident as lang, news.modified FROM news, m_group_base, m_lang"
						." WHERE news.rowid=$self->{app_id} AND news.group_id=m_group_base.rowid"
														." AND m_group_base.lang_id=m_lang.rowid";
	my $res = SelectSingle($self->{dbh}, $sql);
	if (!defined($res)) {
		throw Mioga2::Exception::Simple("Search::Document::News Read", "No record for rowid=$self->{app_id} in table news");
	}
	$self->{group_id} = $res->{group_id};
	#$self->{url} =  $self->{config}->GetBinURI()."/__MIOGA2-GROUP__/News/DisplayMain";
	$self->{url} =  "__BIN-URI__/__MIOGA2-GROUP__/News/DisplayMain";
	$self->{mime} = "application/mioga2-news";
	$self->{description} = $res->{title};
	my $date = Mioga2::Classes::Time->FromPSQL($res->{modified});
	$self->{date} = $date->epoch;


	#
	# Guess language, if not given
	#
	if (!defined($self->{lang}) or (length($self->{lang}) > 0)) {
			$self->{lang} = $self->GetLangFromCode($res->{lang});
	}
	print STDERR "    lang = $self->{lang}\n" if ($debug);
	#
	# Create zone for title and content
	#
	#TODO get weight from config
	my $title = Mioga2::Search::Zone->new({ name => 'title', weight => 5, });
	$title->FormatDataString($res->{title});
	$self->AddZone($title);
	#
	my $content = Mioga2::Search::Zone->new({ name => 'text', weight => 1, });
	$content->FormatDataString($self->HTMLToText($res->{text}));
	$self->AddZone($content);
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Search 

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


