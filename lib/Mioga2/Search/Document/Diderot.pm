# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Search::Document::Diderot.pm : Diderot document class in Mioga2::Search.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Search::Document::Diderot;

use strict;
use utf8;
use base qw(Mioga2::Search::Document);
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::Document;

my $debug = 0;
# ============================================================================
# new
# ============================================================================
sub new
{
	my ($class, $params) = @_;
	print STDERR "Search::Document::Diderot new($class)\n" if ($debug);

	my $self = $class->SUPER::new($params);
	bless($self, $class);
	$self->Initialize($params);

	return $self;
}
# ============================================================================
# GetDeletedDocuments
# ============================================================================
sub GetDeletedDocuments
{
	my ($class, $app_code, $dbh) = @_;
	print STDERR "Search::Document::Diderot GetDeletedDocuments($class)\n" if ($debug);

	my $sql = "SELECT t2.rowid FROM (SELECT rowid, app_id FROM search_doc WHERE app_code=$app_code AND fl_deleted='f') as t2"
									." LEFT JOIN notice ON (notice.rowid = t2.app_id)"
									." WHERE notice.rowid IS NULL limit 100";
	printf STDERR "sql = $sql\n" if ($debug);
	my $result = SelectMultiple($dbh, $sql);
	return $result;
}
# ============================================================================
# GetNewDocuments
# ============================================================================
sub GetNewDocuments
{
	my ($class, $app_code, $config) = @_;
	print STDERR "Search::Document::Diderot GetNewDocuments($class)\n" if ($debug);
	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();

	my $sql =  "SELECT notice.rowid as app_id FROM notice LEFT JOIN (select rowid, app_id from search_doc where app_code=$app_code) as t2"
																					." ON (notice.rowid = t2.app_id), m_group_base"
					." WHERE notice.group_id = m_group_base.rowid"
							." AND m_group_base.mioga_id=$mioga_id"
							. " AND t2.rowid IS NULL limit 1000";
	printf STDERR "sql = $sql\n" if ($debug);
	my $result = SelectMultiple($dbh, $sql);
	return $result;
}
# ============================================================================
# SetModifiedDocuments
# ============================================================================
sub SetModifiedDocuments
{
	my ($class, $app_code, $config, $force_flag) = @_;
	print STDERR "Search::Document::Diderot SetModifiedDocuments($class, $app_code, $force_flag)\n" if ($debug);
	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	my $sql;

	if ($force_flag) {
		$sql = "UPDATE search_doc SET modified=now() WHERE search_doc.app_code=$app_code AND search_doc.mioga_id=$mioga_id";
	}
	else {
		$sql = "UPDATE search_doc SET modified=(SELECT modified FROM notice WHERE rowid=app_id)"
						." FROM notice WHERE search_doc.app_code=$app_code AND search_doc.mioga_id=$mioga_id"
						." AND app_id=notice.rowid AND search_doc.modified != notice.modified";
	}
	printf STDERR "sql = $sql\n" if ($debug);
	ExecSQL($dbh, $sql);
}
# ============================================================================
# VerifyAuthz
# ============================================================================
sub VerifyAuthz
{
	my ($class, $app_ids, $config, $user_id) = @_;
	print STDERR "Search::Document::Diderot VerifyAuthz($class, $user_id)\n" if ($debug);
	my $dbh = $config->GetDBH();

	my $authz_uri = {};
	foreach my $r (@$app_ids) {
		# Get associated document
		my $doc_id = SelectSingle ($dbh, "SELECT document_id FROM notice WHERE rowid = $r;")->{document_id};
		my $document = Mioga2::Document->Get ({
			db_obj      => $config->GetDBObject (),
			rowid       => $doc_id
		});
		$authz_uri->{$r} = ($document->GetAccess ($user_id) > 0) ? 1 : 0;
	}
	return $authz_uri;
}
# ============================================================================
# Private functions
# ============================================================================
# Initialize
# ============================================================================
sub Initialize
{
	my ($self, $params) = @_;
	print STDERR "Search::Document::Diderot Initialize()\n" if ($debug);
	#
	# if doc creation read doc in Mioga and put it to cache
	# else get doc in cache or die
	#
	if ($self->{mode_create}) {
		print STDERR "    Create mode\n" if ($debug);
		$self->{app_code} = $self->{search_doc}->{app_code};
		$self->{app_id} = $self->{search_doc}->{app_id};
		$self->{lang} = $self->{search_doc}->{lang};
		$self->{config} = $params->{create}->{config};
		#
		# Read doc and set cache file
		#
		$self->ReadDoc();
		$self->{document_infos} .= "app_code:$self->{app_code}\n";
		$self->{document_infos} .= "app_id:$self->{app_id}\n";
		my $file_path = "/notice/$self->{group_id}/$self->{app_id}";
		$self->{document_infos} .= "path:$file_path\n";
		$self->WriteInCache($file_path);
		$self->{path} = $file_path;

	}
	#else {
		$self->{doc_in_cache} = $self->GetDocInCache();
		if (! $self->{doc_in_cache}) {
			throw Mioga2::Exception::Simple("Search::Document::Diderot Initialize", "Document for doc_id=$self->{xapian_docid} not in cache");
		}
	#}
	$self->{description} = ""; 
}
# ============================================================================
# ReadDoc
# ============================================================================
sub ReadDoc
{
	my ($self) = @_;
	print STDERR "Search::Document::Diderot ReadDoc()\n" if ($debug);
	my $retval = 0;
	#
	# Reset zones
	#
	$self->DeleteAllZones();
	#
	# Get title field (first field) from notice template
	#
	my $template = SelectSingle ($self->{dbh}, "SELECT notice_template.field_order FROM notice_template, notice WHERE notice.group_id = notice_template.group_id AND notice.rowid = $self->{app_id}");
	my $order = Mioga2::tools::Convert::JSONToPerl ($template->{field_order});
	my $title_field = $order->[0]->[0];
	my $res = SelectSingle ($self->{dbh}, "SELECT value FROM notice_field WHERE notice_id = $self->{app_id} AND ident = '$title_field'");
	if (!defined($res)) {
		throw Mioga2::Exception::Simple("Search::Document::Diderot Read", "No title ($title_field) for rowid=$self->{app_id} in table notice_field");
	}
	my $title = $res->{value};
	#
	# Read data from DB
	#
	my $sql = "SELECT notice.group_id, array_agg (notice_field.value) AS contents,m_lang.ident as lang, notice.modified FROM notice, notice_field, m_group_base, m_lang"
						." WHERE notice.rowid=$self->{app_id} AND notice_field.notice_id = notice.rowid AND notice.group_id=m_group_base.rowid"
														." AND m_group_base.lang_id=m_lang.rowid"
														." GROUP BY notice.group_id, m_lang.ident, notice.modified";
	$res = SelectSingle($self->{dbh}, $sql);
	if (!defined($res)) {
		throw Mioga2::Exception::Simple("Search::Document::Diderot Read", "No record for rowid=$self->{app_id} in table notice");
	}
	$self->{group_id} = $res->{group_id};
	$self->{url} =  "__BIN-URI__/__MIOGA2-GROUP__/Diderot/DisplayMain#Diderot-notice-" . $self->{app_id};
	$self->{mime} = "application/mioga2-diderot";
	$self->{description} = $title;
	my $date = Mioga2::Classes::Time->FromPSQL($res->{modified});
	$self->{date} = $date->epoch;


	#
	# Guess language, if not given
	#
	if (!defined($self->{lang}) or (length($self->{lang}) > 0)) {
			$self->{lang} = $self->GetLangFromCode($res->{lang});
	}
	print STDERR "    lang = $self->{lang}\n" if ($debug);
	#
	# Create zone for title and content
	#
	#TODO get weight from config
	my $title_zone = Mioga2::Search::Zone->new({ name => 'title', weight => 5, });
	utf8::decode ($title);
	$title_zone->FormatDataString($title);
	$self->AddZone($title_zone);

	my $contents = Mioga2::Search::Zone->new({ name => 'text', weight => 3, });
	my $concat = join (' ', @{$res->{contents}});
	utf8::decode ($concat);
	$contents->FormatDataString($concat);
	$self->AddZone($contents);
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Search 

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


