# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Config.pm : The Config class parse the config file for search objects in Mioga2::Search.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Search::Config;

use strict;
use Mioga2::Exception::Simple;
use Mioga2::XML::Simple;
use Data::Dumper;

my $debug = 0;
my %searchconf;
# ============================================================================
# new
# ============================================================================
sub new
{
	my ($class, $path) = @_;
	print STDERR "Search::Config new($class, $path)\n" if ($debug);
	# ------------------------------------------------------
	# Check if the config object $file is cached.
	# If not, create a new object.
	# ------------------------------------------------------
	if( exists $searchconf{$path}) {
		return $searchconf{$path};
	}
	# ------------------------------------------------------
	# Create object and read file
	# ------------------------------------------------------
	if(! -e $path) {
		throw Mioga2::Exception::Simple("Mioga2::Search::Config new", "$path does not exists");
	}

	my $xs = new Mioga2::XML::Simple();
	my $file = $xs->XMLin($path);
	if (!defined($file)) {
		throw Mioga2::Exception::Simple("Mioga2::MiogaConf::new", "Cannot parse config file : <$path>");
	}
	my $self = {};
	bless($self, $class);
	$self->ParseConfig($file);
	# ------------------------------------------------------
	# Put in cache
	# ------------------------------------------------------
	#print STDERR "    self = " . Dumper($self) . "\n" if ($debug);
	$searchconf{$path} = $self;

	return $self;
}
# ============================================================================
# GetXapianDBDir
# ============================================================================
sub GetXapianDBDir
{
	my ($self) = @_;
	return  "xapianDB";
}
# ============================================================================
# GetCacheDir
# ============================================================================
sub GetCacheDir
{
	my ($self) = @_;
	return  "cache";
}
# ============================================================================
# GetMimeActions
# ============================================================================
sub GetMimeActions
{
	my ($self) = @_;
	return  $self->{mime_actions};
}
# ============================================================================
# GetApplications
# ============================================================================
sub GetApplications
{
	my ($self) = @_;
	return  $self->{applications};
}
# ============================================================================
# GetWQFParams
# ============================================================================
sub GetWQFParams
{
	my ($self) = @_;
	return  $self->{WQF};
}
# ============================================================================
# GetBM25Params
# ============================================================================
sub GetBM25Params
{
	my ($self) = @_;
	return  $self->{BM25};
}
# ============================================================================
# GetApplicationsByCodes
# ============================================================================
sub GetApplicationsByCodes
{
	my ($self) = @_;
	print STDERR "Search::Config GetApplicationsByCodes\n" if ($debug);
	my %app;
	print STDERR "    applications : " .Dumper($self->{applications}) . "\n" if ($debug);
	foreach my $k (keys(%{$self->{applications}})) {
		my $new_key = $self->{applications}->{$k}->{app_code};
		print STDERR "    key = $k   newkey = $new_key\n" if ($debug);
		$app{$new_key} = {};
		$app{$new_key}->{name} = $k;
		$app{$new_key}->{package} = $self->{applications}->{$k}->{package};
	}
	return  \%app;
}
# ============================================================================
# Private functions
# ============================================================================
# ParseConfig
# ============================================================================
sub ParseConfig
{
	my ($self, $file) = @_;
	print STDERR "Search::Config ParseConfig()\n" if ($debug);

	foreach my $k (qw(bm25_b bm25_k1 bm25_k2 bm25_k3 bm25_L)) {
		$self->{BM25}->{$k} = $file->{Config}->{$k};
	}
	print STDERR "    BM25 = " . Dumper($self->{BM25}) . "\n" if ($debug);
	foreach my $k (qw(wqf wqf_E wqf_T wqf_S wqf_U)) {
		$self->{WQF}->{$k} = $file->{Config}->{$k};
	}
	print STDERR "    WQF = " . Dumper($self->{WQF}) . "\n" if ($debug);

	$self->{mime_actions} = {};
	foreach my $action (@{$file->{MimeActions}->{action}}) {
		$self->{mime_actions}->{$action->{mime}} = { res_mime =>  $action->{output_mime},
													 command => $action->{command} };
	}

	$self->{applications} = {};
	foreach my $app (@{$file->{Applications}->{app}}) {
		$self->{applications}->{$app->{name}} = { package =>  $app->{package},
												  app_code => $app->{code},
												};
	}
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Search 

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
