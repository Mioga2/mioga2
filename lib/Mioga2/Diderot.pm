#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Diderot.pm: the Mioga2 Domument Management System application.

=head1 DESCRIPTION

The Mioga2::Diderot application gives DMS possibilities to Mioga2, allowing to
classify and retreive documents.

=head1 METHODS DESCRIPTION

=cut

#
# ============================================================================

package Mioga2::Diderot;
use base qw(Mioga2::Application);
use strict;
use warnings;
use open ':encoding(utf8)';

use Data::Dumper;
use Locale::TextDomain::UTF8 'diderot';

use Error qw(:try);
use XML::Atom::SimpleFeed;

use Mioga2::Document;
use Mioga2::Search;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::Content::XSLT;
use Mioga2::Exception::Simple;

my $debug = 0;

# ===============================================================================

=head1 INTERFACE METHODS

=cut

# ===============================================================================


# ===============================================================================

=head2 DisplayMain

Main view

=head3 Generated XML

=over

	<DisplayMain>
		<miogacontext>See Mioga2::RequestContext</miogacontext>
	</DisplayMain>

=back

=cut

# ============================================================================
sub DisplayMain {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::DisplayMain]\n" if ($debug);

	my $data = { DisplayMain => $context->GetContext () };

	# Get ACL to root document
	my $root_document = $self->GetRootDocument ();
	if ($root_document) {
		$data->{DisplayMain}->{access} = $self->{db}->SelectSingle ('SELECT authzTestAccessForDocument (?, ?) AS access;', [$root_document->GetRowid (), $self->{user_id}])->{access};
	}
	else {
		$data->{DisplayMain}->{access} = 0;
	}

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "[Mioga2::Diderot::DisplayMain] XML: $xml\n" if ($debug);

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'diderot.xsl', locale_domain => 'diderot_xsl');
	$content->SetContent($xml);
	return $content;
}
# ============================================================================


# ===============================================================================

=head1 WEBSERVICE METHODS

=cut

# ===============================================================================


#===============================================================================

=head2 GetTemplate

Get notice template

=head3 Incoming Arguments

=over

=item I<rowid>: The template rowid

=back

=head3 Return value

=over

A Perl hash converted to JSON or XML:

	{
		title: '...',
		description: '...',
		fields: '...',	# JSON data
		order: '...'	# JSON data
	}

=back

=cut

#===============================================================================
sub GetTemplate {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::GetTemplate] Entering, args: " . Dumper $context->{args} if ($debug);

	my $group = $context->GetGroup ();
	my $config = $context->GetConfig ();
	my $db = $config->GetDBObject ();

	# Get data, if any
	my $data = $db->SelectSingle ('SELECT title, description, fields, field_order AS order FROM notice_template WHERE group_id = ?', [$group->Get ('rowid')]);

	# Get list of used fields
	if ($data) {
		@{$data->{used_fields}} = map { $_->{ident} } @{ac_ForceArray ($db->SelectMultiple ('SELECT notice_field.ident FROM notice, notice_field WHERE notice_field.notice_id = notice.rowid AND notice.group_id = ?', [$group->Get ('rowid')]))};
	}

	# If no data, return empty hash
	$data ||= {};

	print STDERR "[Mioga2::Diderot::GetTemplate] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetTemplate  ----------


#===============================================================================

=head2 SetTemplate

Store a notice template

=head3 Incoming Arguments

=over

=item I<rowid>: The template rowid. Optional, if missing, a new template is created.

=item I<fields>: An array of field descriptions

=item I<order>: An array of arrays describing how fields are placed in search results

=back

=head3 Return value

=over

A perl hash to be converted to XML or JSON:

	{
		success => 'true' / 'false',
		message => ...,
		errors  => [
			{
				field   => ...,
				message => ...
			}
		]
	}

=back

=cut

#===============================================================================
sub SetTemplate {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::SetTemplate] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'allow_empty', 'want_int' ],
			[ [ 'title' ], 'disallow_empty' ],
			[ [ 'description' ], 'allow_empty' ],
			[ [ 'fields', 'order' ], 'disallow_epmty' ]
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default values
	my $data = {
		success => 'false',
		message => __('Notice template store failed.'),
	};

	if (!@$errors) {
		my $group = $context->GetGroup ();
		my $config = $context->GetConfig ();
		my $db = $config->GetDBObject ();
		try {
			if ($db->SelectSingle ('SELECT * FROM notice_template WHERE group_id = ?', [$group->Get ('rowid')])) {
				# Modify existing template
				$db->ExecSQL ('UPDATE notice_template SET modified=now (), title=?, description=?, fields=?, field_order=? WHERE group_id=?', [$values->{title}, $values->{description}, $values->{fields}, $values->{order}, $group->Get ('rowid')])
			}
			else {
				# Create template
				$db->ExecSQL ("INSERT INTO notice_template (created, modified, title, description, fields, field_order, group_id) VALUES (now (), now (), ?, ?, ?, ?, ?)", [$values->{title}, $values->{description}, $values->{fields}, $values->{order}, $group->Get ('rowid')]);

				# Create root document
				my $document = $self->GetRootDocument ();
				if (!$document) {
					throw Mioga2::Exception::Application ('Mioga2::Diderot::SetTemplate', 'Can not create root-category document');
				}

				# Record document_id for template
				$db->ExecSQL ('UPDATE notice_template SET document_id = ? WHERE group_id = ?', [$document->GetRowid (), $group->Get ('rowid')]);
			}
		}
		finally {
			$data->{success} = 'true';
			$data->{message} = __('Notice template stored.');
			$data->{template} = $self->GetTemplate ($context);
		};
	}
	else {
		print STDERR '[Mioga2::Diderot::SetTemplate] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	print STDERR "[Mioga2::Diderot::SetTemplate] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetTemplate  ----------


#===============================================================================

=head2 GetNotice

Get notice

=head3 Incoming Arguments

=over

=item I<rowid>: Optional, the rowid of the notice to fetch

=back

=head3 Return value

=over

A perl hash to be converted to XML or JSON:

	{
		template => { },	# The associated notice template (see GetTemplate for details)
		<field_code> => '...',
		<field_code> => '...',
		...
	}

=back

=cut

#===============================================================================
sub GetNotice {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::GetNotice] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'allow_empty', 'want_int', 'diderot_notice_readable' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default values
	my $data;

	if (!@$errors) {
		if (st_ArgExists ($context, 'rowid')) {
			my $group = $context->GetGroup ();
			my $config = $context->GetConfig ();
			my $db = $config->GetDBObject ();

			# Get notice fields
			my $fields = $db->SelectMultiple ('SELECT notice.rowid, notice_field.ident, notice_field.value FROM notice, notice_field WHERE notice_field.notice_id = notice.rowid AND notice.rowid = ? AND notice.group_id = ?UNION SELECT notice.rowid, \'access\' AS ident, authzTestAccessForDocument (notice.document_id, ?)::char AS value FROM notice WHERE rowid = ? AND group_id = ?;', [$values->{rowid}, $group->Get ('rowid'), $self->{user_id}, $values->{rowid}, $group->Get ('rowid')]);
			%{$data} = map { $_->{ident} => $_->{value} } @$fields;

			# Get associated document
			my $document = Mioga2::Document->Get ({
				db_obj      => $db,
				rowid       => $self->GetDocumentId ('notice', $values->{rowid})
			});
			$data->{access} = $document->GetAccess ($self->{user_id});

			$data->{rowid} = $values->{rowid};

			# Get notice files
			my $file_list = $db->SelectMultiple ('SELECT user_uri_access.uri, user_uri_access.access FROM user_uri_access, notice, notice_files WHERE notice_files.uri_id = user_uri_access.uri_id  AND notice.rowid = notice_files.notice_id AND notice.rowid = ? AND notice.group_id = ? AND user_uri_access.user_id = ? ORDER BY uri;', [$values->{rowid}, $group->Get ('rowid'), $self->{user_id}]);
			$data->{files} = [ ];
			@{$data->{files}} = map { $_->{uri} } grep { $_->{access} > 0 } @$file_list;
			if (scalar (@{$data->{files}} < scalar (@$file_list))) {
				$data->{hidden_files} = 'boolean::true';
			}
			else {
				$data->{hidden_files} = 'boolean::false';
			}
		}

		# Get notice template
		$data->{template} = $self->GetTemplate ($context);
	}
	else {
		print STDERR '[Mioga2::Diderot::GetNotice] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	print STDERR "[Mioga2::Diderot::GetNotice] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetNotice  ----------


#===============================================================================

=head2 SetNotice

Create or update a notice

=head3 Incoming Arguments

=over

A list of key / value pairs where key is an internal code matching a field in template.
Value may be a plain text value (in case of single or multi-line field) or an internal code matching field value in template.

=back

=head3 Return value

=over

A perl hash to be converted to XML or JSON:

	{
		success => 'true' / 'false',
		message => ...,
		errors  => [
			{
				field   => ...,
				message => ...
			}
		]
	}

=back

=cut

#===============================================================================
sub SetNotice {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::SetNotice] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'allow_empty', 'want_int', 'diderot_notice_writable' ],
			[ [ 'category_id' ], 'allow_empty', 'want_int', 'diderot_category_writable' ],
			[ [ 'files' ], 'allow_empty', 'stripxws' ],
			[ [ 'optional_fields' ], 'allow_empty' ],
			[ [ 'mandatory_fields' ], 'allow_empty' ],
		];

	# Add rules for optional fields
	for my $field (@{ac_ForceArray ($context->{args}->{optional_fields})}) {
		push (@$params, [ [ $field ], 'stripxws', 'allow_empty' ]);
	}

	# Add rules for mandatory fields
	for my $field (@{ac_ForceArray ($context->{args}->{mandatory_fields})}) {
		push (@$params, [ [ $field ], 'stripxws', 'disallow_empty' ]);
	}

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default values
	my $data = {
		success => 'false',
		message => __('Notice store failed.'),
	};

	if (!@$errors) {
		my $group = $context->GetGroup ();
		my $config = $context->GetConfig ();
		my $db = $config->GetDBObject ();

		# Drop useless keys
		delete ($values->{optional_fields});
		delete ($values->{mandatory_fields});

		try {
			if (st_ArgExists ($context, 'rowid')) {
				# Update existing notice
				if (st_ArgExists ($context, 'category_id')) {
					$db->ExecSQL ('UPDATE notice SET modified=now(), category_id = ?, user_id = ? WHERE rowid = ? AND group_id = ?', [$values->{category_id}, $self->{user_id}, $values->{rowid}, $group->Get ('rowid')]);

					# Update parent document if needed
					my $parent_document = Mioga2::Document->Get ({
						db_obj => $self->{db},
						rowid  => $self->GetDocumentId ('category', $values->{category_id})
					});
					my $document = Mioga2::Document->Get ({
						db_obj => $self->{db},
						rowid  => $self->GetDocumentId ('notice', $values->{rowid})
					});
					$document->InheritsFrom ($parent_document->GetRowid ());
				}
				else {
					$db->ExecSQL ('UPDATE notice SET modified=now(), user_id = ? WHERE rowid = ? AND group_id = ?', [$self->{user_id}, $values->{rowid}, $group->Get ('rowid')]);
				}

				# Get already defined fields
				my @existing = map { $_->{ident} } @{$db->SelectMultiple ('SELECT notice_field.ident FROM notice, notice_field WHERE notice.rowid = notice_field.notice_id AND notice_field.notice_id = ? AND notice.group_id = ?;', [$values->{rowid}, $group->Get ('rowid')])};

				# Record notice fields
				for my $field (keys (%$values)) {
					if (grep { /^$field$/ } @existing) {
						print STDERR "[Mioga2::Diderot::SetNotice] Updating existing field $field for notice $values->{rowid}\n" if ($debug);
						$db->ExecSQL ('UPDATE notice_field SET value = ? FROM notice WHERE notice_field.notice_id = notice.rowid AND notice.rowid = ? AND notice.group_id = ? AND notice_field.ident = ?;', [$values->{$field}, $values->{rowid}, $group->Get ('rowid'), $field])
					}
					else {
						print STDERR "[Mioga2::Diderot::SetNotice] Creating non-existing field $field for notice $values->{rowid}\n" if ($debug);
						$db->ExecSQL ('INSERT INTO notice_field SELECT ? AS ident, ? AS value, notice.rowid FROM notice WHERE rowid = ? AND group_id = ?;', [$field, $values->{$field}, $values->{rowid}, $group->Get ('rowid')]);
					}
				}
			}
			else {
				# Create notice
				my $parent_document;
				if ($values->{category_id}) {
					# Get parent document
					$parent_document = Mioga2::Document->Get ({
						db_obj      => $db,
						rowid       => $self->GetDocumentId ('category', $values->{category_id})
					});
				}
				else {
					# Get parent document
					$parent_document = $self->GetRootDocument ();
				}

				# Create document for notice
				my $document = Mioga2::Document->Create ({
					db_obj         => $db,
					url            => '',
					app_id         => $self->{app_id},
					mioga_id       => $self->{mioga_id},
					group_id       => $self->{group_id},
				});
				$document->InheritsFrom ($parent_document->GetRowid ());

				# Record notice
				if (!st_ArgExists ($context, 'category_id')) {
					$db->ExecSQL ('INSERT INTO notice (created, modified, group_id, document_id, user_id) VALUES (now (), now (), ?, ?, ?);', [$group->Get ('rowid'), $document->GetRowid (), $self->{user_id}]);
				}
				else {
					$db->ExecSQL ('INSERT INTO notice (created, modified, group_id, category_id, document_id, user_id) VALUES (now (), now (), ?, ?, ?, ?);', [$group->Get ('rowid'), $values->{category_id}, $document->GetRowid (), $self->{user_id}]);
				}

				# Set document URL
				$document->SetURL ($self->GenerateDocumentURI ('notice', $db->GetLastInsertId ('notice')));

				# Get notice rowid
				$values->{rowid} = $db->GetLastInsertId ('notice');

				# Record notice fields
				for my $field (keys (%$values)) {
					$db->ExecSQL ('INSERT INTO notice_field (ident, value, notice_id) VALUES (?, ?, ?);', [$field, $values->{$field}, $values->{rowid}]);
				}

			}

			# Link files
			if (@{ac_ForceArray ($values->{files})}) {
				# Insert newly-added files
				my $args = [$values->{rowid}, $group->Get ('rowid'), $group->Get ('rowid')];
				for my $uri_id (@{ac_ForceArray ($values->{files})}) {
					push (@$args, $uri_id);
				}
				my $placeholders = join (', ', ('?') x @{ac_ForceArray ($values->{files})});
				push (@$args, $values->{rowid});
				$db->ExecSQL ('INSERT INTO notice_files SELECT notice.rowid, m_uri.rowid FROM notice, m_uri WHERE notice.rowid = ? AND notice.group_id = ? AND m_uri.group_id = ? AND m_uri.uri IN (' . $placeholders . ') AND (notice.rowid, m_uri.rowid) NOT IN (SELECT * FROM notice_files WHERE notice_id = ?);', $args);

				# Remove dropped files
				$args = [$values->{rowid}, $group->Get ('rowid')];
				for my $uri_id (@{ac_ForceArray ($values->{files})}) {
					push (@$args, $uri_id);
				}
				$db->ExecSQL ('DELETE FROM notice_files USING notice, m_uri WHERE notice_files.notice_id = notice.rowid AND notice.rowid = ? AND notice.group_id = ? AND notice_files.uri_id NOT IN (SELECT rowid FROM m_uri WHERE uri IN (' . $placeholders . '));', $args);
			}
			else {
				# No file, remove entries
				$db->ExecSQL ('DELETE FROM notice_files USING notice WHERE notice_files.notice_id = notice.rowid AND notice.rowid = ? AND notice.group_id = ?;', [$values->{rowid}, $group->Get ('rowid')]);
			}
		}
		finally {
        	$self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);

			$data->{success} = 'true';
			$data->{message} = __('Notice stored.');
			$context->{args}->{rowid} = $values->{rowid};	# Not really clean, but the only solution to get a result from GetNotice when notice was just created
			$data->{notice} = $self->GetNotice ($context);
		};
	}
	else {
		print STDERR '[Mioga2::Diderot::SetNotice] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	print STDERR "[Mioga2::Diderot::SetNotice] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetNotice  ----------


#===============================================================================

=head2 GetRecent

Get recently-modified notice summary

=head3 Incoming Arguments

=over

=item I<count>: The number of notices to return

=back

=head3 Return value

=over

A perl hash to be converted to XML or JSON:

	{
		template => { },	# The associated notice template (see GetTemplate for details)
		notices => [
			{
				rowid => ...,
				modified => ...,
				<field_code> => ...,
			},
			...
		]
	}

=back

=cut

#===============================================================================
sub GetRecent {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::GetNotice] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'count' ], 'allow_empty', 'want_int' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default values
	my $data = {
		template => $self->GetTemplate ($context)
	};

	my $group = $context->GetGroup ();
	my $config = $context->GetConfig ();
	my $db = $config->GetDBObject ();

	# Default is to return 10 notices
	$values->{count} ||= 10;

	# Extract notice fields and dates
	my $fields = $db->SelectMultiple ("SELECT * FROM (SELECT * FROM notice_field UNION ALL SELECT 'modified' AS ident, to_char (modified, 'YYYY-MM-DD HH24:MI:SS') AS value, rowid AS notice_id FROM notice UNION ALL SELECT 'created' AS ident, to_char (created, 'YYYY-MM-DD HH24:MI:SS') AS value, rowid AS notice_id FROM notice UNION SELECT 'access' AS ident, authzTestAccessForDocument ((SELECT document_id FROM notice WHERE rowid=notice_field.notice_id), ?)::char AS value, notice.rowid AS notice_id FROM notice, notice_field WHERE notice.rowid = notice_field.notice_id) AS fields WHERE fields.notice_id IN (SELECT notice.rowid FROM notice WHERE group_id = ? ORDER BY notice.modified DESC LIMIT ?);", [$self->{user_id}, $group->Get ('rowid'), $values->{count}]);
	my $notices;
	for my $field (@{ac_ForceArray ($fields)}) {
		$notices->{$field->{notice_id}}->{$field->{ident}} = $field->{value};
	}
	@{$data->{notices}} = grep { int ($_->{access}) > 0 } map { $notices->{$_}->{rowid} = $_; $notices->{$_} } keys (%$notices);

	print STDERR "[Mioga2::Diderot::GetNotice] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetRecent  ----------


#===============================================================================

=head2 GetUncategorized

Get notices that are not affected to a category

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

A perl hash to be converted to XML or JSON:

	{
		template => { },	# The associated notice template (see GetTemplate for details)
		notices => [
			{
				rowid => ...,
				modified => ...,
				<field_code> => ...,
			},
			...
		]
	}

=back

=cut

#===============================================================================
sub GetUncategorized {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::GetUncategorized] Entering.\n" if ($debug);

	# Default values
	my $data = {
		template => $self->GetTemplate ($context)
	};

	my $group = $context->GetGroup ();
	my $config = $context->GetConfig ();
	my $db = $config->GetDBObject ();

	# Extract notice fields and dates
	my $fields = $db->SelectMultiple ("SELECT * FROM (SELECT * FROM notice_field UNION ALL SELECT 'modified' AS ident, to_char (modified, 'YYYY-MM-DD HH24:MM:SS') AS value, rowid AS notice_id FROM notice UNION ALL SELECT 'created' AS ident, to_char (created, 'YYYY-MM-DD HH24:MM:SS') AS value, rowid AS notice_id FROM notice UNION SELECT 'access' AS ident, authzTestAccessForDocument ((SELECT document_id FROM notice WHERE rowid=notice_field.notice_id), ?)::char AS value, notice.rowid AS notice_id FROM notice, notice_field WHERE notice.rowid = notice_field.notice_id) AS fields WHERE fields.notice_id IN (SELECT notice.rowid FROM notice WHERE group_id = ? AND category_id IS NULL);", [$self->{user_id}, $group->Get ('rowid')]);
	my $notices;
	for my $field (@{ac_ForceArray ($fields)}) {
		$notices->{$field->{notice_id}}->{$field->{ident}} = $field->{value};
	}
	@{$data->{notices}} = grep { int ($_->{access}) > 0} map { $notices->{$_}->{rowid} = $_; $notices->{$_} } keys (%$notices);

	print STDERR "[Mioga2::Diderot::GetUncategorized] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetUncategorized  ----------


#===============================================================================

=head2 SearchNotices

Perform a search query and return matching notices

=head3 Incoming Arguments

=over

A list of key / value pairs where key is an internal code matching a field in template.
Value may be a plain text value (in case of single or multi-line field) or an internal code matching field value in template.

=back

=head3 Return value

=over

A perl hash to be converted to XML or JSON:

	{
		template => { },	# The associated notice template (see GetTemplate for details)
		notices => [
			{
				rowid => ...,
				modified => ...,
				<field_code> => ...,
			},
			...
		]
	}

=back

=cut

#===============================================================================
sub SearchNotices {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::SearchNotices] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'query', 'category_id', 'query_string', 'search_description', 'search_tags', 'search_text', 'operator' ], 'allow_empty' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default values
	my $data = {
		success => 'false',
		message => __('Search failed.')
	};

	if (!@$errors) {
		my $group = $context->GetGroup ();
		my $config = $context->GetConfig ();
		my $db = $config->GetDBObject ();

		my ($subquery, $query_args);

		if (st_ArgExists ($context, 'query')) {
			# Complex query
			my $query = Mioga2::tools::Convert::JSONToPerl ($values->{query});
			print STDERR "[Mioga2::Diderot::SearchNotices] Search query: " . Dumper $query if ($debug);

			my ($where, $tables, $args) = $self->GenerateSearchQuery ($query);

			if ($tables) {
				# Complex search-query, use GenerateSearchQuery output to initialize SQL
				$subquery = "SELECT notice.rowid FROM notice, " . join (', ', @$tables) . " WHERE $$where AND notice.group_id = ?";
			}
			else {
				# No complex search-query
				$args = [ ];
				$subquery = "SELECT notice.rowid FROM notice WHERE notice.group_id = ?";
			}
			push (@$args, $group->Get ('rowid'));

			# Query file contents if needed
			if (st_ArgExists ($context, 'query_string')) {
				# Get linking operator from 'query' argument
				my $operator = 'AND';
				if ($values->{operator}) {
					$operator = ($values->{operator} eq 'or') ? 'OR' : 'AND';
				}

				# Get matching documents from search engine
				my $search = Mioga2::Search->new ($config);
				my $res = $search->SearchWS ($context);
				my $group_ident = $group->Get ('ident');
				my @xapian_uri = map { $_->{url} } grep { ($_->{app_type} eq '1') && ($_->{group} eq $group_ident) } @{$res->{Result}};
				push (@xapian_uri, '');		# Ensure there's at least an entry in URI list, even though it is not consistent

				# Get matching notices from search engine
				my @xapian_notices = map { $_->{url} =~ s/^.*-([0-9]+)$/$1/; $_->{url} } grep { ($_->{app_type} eq '5') &&($_->{group} eq $group_ident) } @{$res->{Result}};
				push (@xapian_notices, 0);		# Ensure there's at least an entry in rowid list, even though it is not consistent

				# Initialize subquery part and arguments
				$subquery .= ' ' . $operator . ' notice.rowid IN (SELECT notice_files.notice_id FROM notice_files, m_uri WHERE notice_files.uri_id = m_uri.rowid AND m_uri.uri IN (' . join (', ', ('?') x @xapian_uri) . ')) OR notice.rowid IN (' . join (', ', ('?') x @xapian_notices) . ')';
				for my $uri (@xapian_uri) {
					push (@$args, $uri);
				}
				for my $rowid (@xapian_notices) {
					push (@$args, $rowid);
				}
			}

			$query_args = $args;
		}
		elsif (defined ($values->{query_string})) {
			# Simple (Xapian) search

			# Get matching documents from search engine
			my $search = Mioga2::Search->new ($config);
			my $res = $search->SearchWS ($context);
			my $group_ident = $group->Get ('ident');
			my @xapian_uri = map { $_->{url} } grep { ($_->{app_type} eq '1') && ($_->{group} eq $group_ident) } @{$res->{Result}};
			push (@xapian_uri, '');		# Ensure there's at least an entry in URI list, even though it is not consistent

			# Get matching notices from search engine
			my @xapian_notices = map { $_->{url} =~ s/^.*-([0-9]+)$/$1/; $_->{url} } grep { ($_->{app_type} eq '5') &&($_->{group} eq $group_ident) } @{$res->{Result}};
			push (@xapian_notices, 0);		# Ensure there's at least an entry in rowid list, even though it is not consistent

			# Initialize subquery part and arguments
			$subquery = 'SELECT notice.rowid FROM notice WHERE notice.rowid IN (SELECT notice_files.notice_id FROM notice_files, m_uri WHERE notice_files.uri_id = m_uri.rowid AND m_uri.uri IN (' . join (', ', ('?') x @xapian_uri) . ')) OR notice.rowid IN (' . join (', ', ('?') x @xapian_notices) . ')';
			for my $uri (@xapian_uri) {
				push (@$query_args, $uri);
			}
			for my $rowid (@xapian_notices) {
				push (@$query_args, $rowid);
			}
		}
		elsif (!defined ($values->{category_id})) {
			# Group-wide query
			$subquery = 'SELECT notice.rowid FROM notice WHERE group_id = ?';
			$query_args = [$group->Get ('rowid')];
		}
		else {
			# Category-based query
			$subquery = 'SELECT notice.rowid FROM notice WHERE category_id = ? AND group_id = ?';
			$query_args = [$values->{category_id}, $group->Get ('rowid')];
		}

		# Search matching notice fields
		my $sql = "SELECT * FROM (SELECT * FROM notice_field UNION ALL SELECT 'modified' AS ident, to_char (modified, 'YYYY-MM-DD HH24:MM:SS') AS value, rowid AS notice_id FROM notice UNION ALL SELECT 'created' AS ident, to_char (created, 'YYYY-MM-DD HH24:MM:SS') AS value, rowid AS notice_id FROM notice UNION SELECT 'access' AS ident, authzTestAccessForDocument ((SELECT document_id FROM notice WHERE rowid=notice_field.notice_id), ?)::char AS value, notice.rowid AS notice_id FROM notice, notice_field WHERE notice.rowid = notice_field.notice_id) AS fields WHERE fields.notice_id IN ($subquery)";
		unshift (@$query_args, $self->{user_id});
		print STDERR "[Mioga2::Diderot::SearchNotices] Search request: $sql\n" if ($debug);
		print STDERR "[Mioga2::Diderot::SearchNotices] Search args: " . Dumper $query_args if ($debug);

		# Get notices
		my $fields = $db->SelectMultiple ($sql, $query_args);
		my $notices;
		for my $field (@{ac_ForceArray ($fields)}) {
			$notices->{$field->{notice_id}}->{$field->{ident}} = $field->{value};
		}
		@{$data->{notices}} = grep { int ($_->{access}) > 0 } map { $notices->{$_}->{rowid} = $_; $notices->{$_} } keys (%$notices);

		$data->{template} = $self->GetTemplate ($context);

		$data->{message} = __x('Search gave {count} result(s).', count => scalar (@{$data->{notices}}));
		$data->{success} = 'true';
	}
	else {
		print STDERR '[Mioga2::Diderot::SearchNotices] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	print STDERR "[Mioga2::Diderot::SearchNotices] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SearchNotices  ----------


#===============================================================================

=head2 GetCategory

Get notice categories

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

A hash to be converted to XML or JSON:
	{
		rowid: ...,
		ident: '...',
		parent_id: ...,
		group_id: ...,
		created: ...,
		modified: ...,
		notices: ...	# The number of notices associated to category
	}

=back

=cut

#===============================================================================
sub GetCategory {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::GetCategory] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int', 'diderot_category_readable' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $group = $context->GetGroup ();
		my $config = $context->GetConfig ();
		my $db = $config->GetDBObject ();

		$data = $db->SelectSingle ('SELECT notice_category.*, notice_count.notices FROM notice_category, (SELECT notice_category.rowid, count(notice.rowid) AS notices FROM notice_category LEFT JOIN notice ON notice.category_id = notice_category.rowid GROUP BY notice_category.rowid) AS notice_count WHERE notice_count.rowid = notice_category.rowid AND notice_category.rowid = ? AND notice_category.group_id = ?;', [$values->{rowid}, $group->Get ('rowid')]);
	}
	else {
		print STDERR '[Mioga2::Diderot::GetCategory] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	print STDERR "[Mioga2::Diderot::GetCategory] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetCategory  ----------


#===============================================================================

=head2 GetCategories

Get notice categories

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

An array to be converted to XML or JSON:
	[
		{
			rowid: ...,
			ident: '...',
			parent_id: ...,
			group_id: ...,
			created: ...,
			modified: ...,
			notices: ...	# The number of notices associated to category
		}
	]

=back

=cut

#===============================================================================
sub GetCategories {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::GetCategories] Entering.\n" if ($debug);

	my $group = $context->GetGroup ();
	my $config = $context->GetConfig ();
	my $db = $config->GetDBObject ();

	my $data = $db->SelectMultiple ('SELECT * FROM (SELECT notice_category.*, notice_count.notices, authzTestAccessForDocument (notice_category.document_id, ?) AS access FROM notice_category, (SELECT notice_category.rowid, count(notice.rowid) AS notices FROM notice_category LEFT JOIN (SELECT * FROM notice WHERE authzTestAccessForDocument (notice.document_id, ?) > 0) AS notice ON notice.category_id = notice_category.rowid GROUP BY notice_category.rowid) AS notice_count WHERE notice_count.rowid = notice_category.rowid AND notice_category.group_id = ?) AS MAIN WHERE access > 0;', [$self->{user_id}, $self->{user_id}, $group->Get ('rowid')]);

	print STDERR "[Mioga2::Diderot::GetCategories] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetCategories  ----------


#===============================================================================

=head2 SetCategory

Store a category

=head3 Incoming Arguments

=over

=item I<rowid>: The category rowid. This argument is optional, if missing, category will be created.

=item I<ident>: The category name.

=item I<parent_id>: The parent category rowid. This argument is optional, if missing a top-level category is created.

=back

=head3 Return value

=over

A Perl Data structure to be converted to JSON or XML containing the full list of categories (see GetCategories).

=back

=cut

#===============================================================================
sub SetCategory {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::SetCategory] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid', 'parent_id' ], 'allow_empty', 'want_int', 'diderot_category_writable' ],
			[ [ 'ident' ], 'disallow_empty', 'stripxws' ]
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default values
	my $data = {
		success => 'false',
		message => __('Category store failed.')
	};

	if (!@$errors) {
		my $group = $context->GetGroup ();
		my $config = $context->GetConfig ();
		my $db = $config->GetDBObject ();

		try {
			if (st_ArgExists ($context, 'rowid')) {
				# Modify existing category
				$db->ExecSQL ('UPDATE notice_category SET ident = ?, parent_id = ? WHERE rowid = ? AND group_id = ?', [$values->{ident}, $values->{parent_id}, $values->{rowid}, $group->Get ('rowid')]);

				# Update parent document if needed
				my $parent_document;
				if ($values->{parent_id} != $values->{rowid}) {
					$parent_document = Mioga2::Document->Get ({
						db_obj => $self->{db},
						rowid  => $self->GetDocumentId ('category', $values->{parent_id})
					});
				}
				else {
					$parent_document = $self->GetRootDocument ();
				}
				my $document = Mioga2::Document->Get ({
					db_obj => $self->{db},
					rowid  => $self->GetDocumentId ('category', $values->{rowid})
				});
				$document->InheritsFrom ($parent_document->GetRowid ());

				$data->{message} = __('Category successfully modified.');
			}
			else {
				# Get parent document
				my $parent_document;
				if ($values->{parent_id}) {
					# Get parent document
					$parent_document = Mioga2::Document->Get ({
						db_obj      => $db,
						rowid       => $self->GetDocumentId ('category', $values->{parent_id})
					});
				}
				else {
					$parent_document = $self->GetRootDocument ();
				}

				# Create document for category
				my $document = Mioga2::Document->Create ({
					db_obj         => $db,
					url            => '',
					app_id         => $self->{app_id},
					mioga_id       => $self->{mioga_id},
					group_id       => $self->{group_id},
				});
				$document->InheritsFrom ($parent_document->GetRowid ());

				# Record category
				if ($values->{parent_id}) {
					$db->ExecSQL ('INSERT INTO notice_category (ident, parent_id, group_id, document_id, created, modified) VALUES (?, ?, ?, ?, now(), now())', [$values->{ident}, $values->{parent_id}, $group->Get ('rowid'), $document->GetRowid ()]);
				}
				else {
					$db->ExecSQL ('INSERT INTO notice_category (ident, parent_id, group_id, document_id, created, modified) VALUES (?, currval(\'notice_category_rowid_seq\'), ?, ?, now (), now ())', [$values->{ident}, $group->Get ('rowid'), $document->GetRowid ()]);
				}

				# Set document URL
				$document->SetURL ($self->GenerateDocumentURI ('category', $db->GetLastInsertId ('notice_category')));

				$data->{message} = __('Category successfully created.');
			}
		}
		finally {
			$data->{success} = 'true';
			$data->{categories} = $self->GetCategories ($context);
		};
	}
	else {
		print STDERR '[Mioga2::Diderot::SetCategory] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	return ($data);
}	# ----------  end of subroutine SetCategory  ----------


#===============================================================================

=head2 DeleteCategory

Delete a category

=head3 Incoming Arguments

=over

=item I<rowid>: The category rowid.

=back

=head3 Return value

=over

A Perl Data structure to be converted to JSON or XML containing the full list of categories (see GetCategories).

=back

=cut

#===============================================================================
sub DeleteCategory {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::DeleteCategory] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default values
	my $data = {
		success => 'false',
		message => __('Category deletion failed.')
	};

	if (!@$errors) {
		my $group = $context->GetGroup ();
		my $config = $context->GetConfig ();
		my $db = $config->GetDBObject ();

		try {
			my $document_id = $self->GetDocumentId ('category', $values->{rowid});
			$db->ExecSQL ('DELETE FROM notice_category WHERE rowid=? AND group_id=?;', [$values->{rowid}, $group->Get ('rowid')]);
			Mioga2::Document->Delete ({
				db_obj  => $db,
				rowid   => $document_id,
				recurse => 1
			});
		}
		finally {
			$data->{success} = 'true';
			$data->{message} = __('Category successfully deleted.');
			$data->{categories} = $self->GetCategories ($context);
		};
	}
	else {
		print STDERR '[Mioga2::Diderot::DeleteCategory] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	return ($data);
}	# ----------  end of subroutine DeleteCategory  ----------


#===============================================================================

=head2 GetRights

Get access rights for a document

=head3 Incoming Arguments

=over

=item I<$type>: The document type in a string (category or notice)

=item I<$rowid>: The document rowid

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		inherited: true / false,
		profiles: [
			{
				rowid: <profile_rowid>,
				ident: <profile_ident>,
				access: <access_right>
			},
			...
		],
		teams: [
			{
				rowid: <team_rowid>,
				ident: <team_ident>,
				access: <access_right>
			},
			...
		],
		users: [
			{
				rowid: <user_rowid>,
				ident: <user_ident>,
				firstname: <user_firstname>,
				lastname: <user_lastname>,
				email: <user_email>,
				access: <access_right>
			},
			...
		]
	}

=back

=cut

#===============================================================================
sub GetRights {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::GetRights] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'type' ], 'disallow_empty', 'stripxws', [ 'match', "\(category|notice\)" ] ],
		];

	if ($context->{args}->{type} eq 'notice') {
		# Rowid is mandatory for a notice
		push (@$params, [ [ 'rowid' ], 'disallow_empty', 'want_int' ]);
	}
	else {
		# Rowid is optional for a category (if not given, get rights on root category)
		push (@$params, [ [ 'rowid' ], 'allow_empty', 'want_int' ]);
	}

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $document_id = $self->GetDocumentId ($values->{type}, $values->{rowid});

		my $document = Mioga2::Document->Get ({
			db_obj  => $self->{db},
			rowid   => $document_id
		});

		# Document attributes
		$data->{rowid} = $values->{rowid};
		$data->{type} = $values->{type};

		# Inheritance
		$data->{inherited} = $document->InheritsRights () ? 'boolean::true' : 'boolean::false';

		# Profiles access
		$data->{profiles} = $self->{db}->SelectMultiple ('SELECT m_profile.rowid, m_profile.ident, coalesce (doc_authz_profile.access, 0) AS access FROM m_profile LEFT JOIN (SELECT * FROM doc_authz_profile WHERE doc_id = getRefDocId (?)) AS doc_authz_profile ON doc_authz_profile.profile_id = m_profile.rowid WHERE m_profile.group_id = ?;', [$document_id, $self->{group_id}]);

		# User access
		$data->{users} = $self->{db}->SelectMultiple ('SELECT m_user_base.rowid, m_user_base.ident, m_user_base.firstname, m_user_base.lastname, m_user_base.email, doc_authz_user.access FROM m_user_base, doc_authz_user WHERE m_user_base.rowid = doc_authz_user.user_id AND doc_authz_user.doc_id = getRefDocId (?);', [$document_id]);

		# Team access
		$data->{teams} = $self->{db}->SelectMultiple ('SELECT m_group.rowid, m_group.ident, doc_authz_team.access FROM m_group, doc_authz_team WHERE m_group.rowid = doc_authz_team.team_id AND doc_authz_team.doc_id = getRefDocId (?);', [$document_id]);
	}
	else {
		print STDERR '[Mioga2::Diderot::GetRights] Got errors: ' . Dumper $errors;
	}

	print STDERR "[Mioga2::Diderot::GetRights] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetRights  ----------


#===============================================================================

=head2 SetRights

Set access rights

=head3 Incoming Arguments

=over

=item I<$rowid>: The document rowid

=item I<$type>: The document type

=item I<$inherited>: A flag indicating rights are inherited or not ('true' or 'false')

=item I<$profile_<rowid>>: The access right for profile <rowid>

=item I<$team_<rowid>>: The access right for team <rowid>

=item I<$user_<rowid>>: The access right for user <rowid>

=back

=head3 Return value

=over

A Perl hash to be converted to JSON or XML:

	{
		success: 'true' | 'false',
		message: ...,
		rights: ....	# See GetRights output
	}

=back

=cut

#===============================================================================
sub SetRights {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::SetRights] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'type' ], 'disallow_empty', 'stripxws', [ 'match', "\(category|notice\)" ] ],
			[ [ 'inherited' ], 'disallow_empty', 'stripxws', [ 'match', "\(true|false\)" ] ],
		];

	if ($context->{args}->{type} eq 'notice') {
		push (@$params, [ [ 'rowid' ], 'disallow_empty', 'want_int' ]);
	}
	else {
		push (@$params, [ [ 'rowid' ], 'allow_empty', 'want_int' ]);
	}

	# Append 'profile_*', 'user_*' and 'team_*' arguments to check parameters
	my (@args, @profiles, @users, @teams);
	for my $arg (keys (%{$context->{args}})) {
		if ($arg =~ /^profile_[0-9]+/) {
			push (@profiles, $arg);
			push (@args, $arg);
		}
		elsif ($arg =~ /^team_[0-9]+/) {
			push (@teams, $arg);
			push (@args, $arg);
		}
		elsif ($arg =~ /^user_[0-9]+/) {
			push (@users, $arg);
			push (@args, $arg);
		}
	}
	push (@$params, [ [ @args ], 'disallow_empty', 'full_int', [ 'match', "\(-1|0|1|2\)" ] ]);

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = {
		success => 'false',
		message => __('Access rights update failed.')
	};

	if (!@$errors) {
		try {
			$self->{db}->BeginTransaction ();

			# Load document
			my $document_id = $self->GetDocumentId ($values->{type}, $values->{rowid});
			my $document = Mioga2::Document->Get ({
				db_obj  => $self->{db},
				rowid   => $document_id
			});

			if (($document->InheritsRights ()) && ($values->{inherited} eq 'false')) {
				# Break inheritance
				$document->SetInheritance (0);
			}
			elsif ((!$document->InheritsRights ()) && (($values->{inherited} eq 'true'))) {
				# Restore inheritance
				$document->SetInheritance (1);
			}

			# Profile rights
			for my $profile (@profiles) {
				my ($rowid) = ($profile =~ m/^profile_([0-9]+)$/);
				$document->GrantAccessToProfile ($rowid, $values->{$profile});
			}

			# Team rights
			for my $team (@teams) {
				my ($rowid) = ($team =~ m/^team_([0-9]+)$/);
				if ($values->{$team} != -1) {
					$document->GrantAccessToTeam ($rowid, $values->{$team});
				}
				else {
					$document->RemoveAccessForTeam ($rowid);
				}
			}

			# User rights
			for my $user (@users) {
				my ($rowid) = ($user =~ m/^user_([0-9]+)$/);
				if ($values->{$user} != -1) {
					$document->GrantAccessToUser ($rowid, $values->{$user});
				}
				else {
					$document->RemoveAccessForUser ($rowid);
				}
			}

			$data->{success} = 'true';
			$data->{message} = __('Access rights successfully updated.');
			$data->{rights} = $self->GetRights ($context);

			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			print STDERR "[Mioga2::Diderot::SetRights] Failed: " . $err->stringify ();
			$self->{db}->RollbackTransaction ();
		};
	}
	else {
		print STDERR "[Mioga2::Diderot::SetRights] Got errors: " . Dumper $errors;
	}

	print STDERR "[Mioga2::Diderot::SetRights] Leaving, data:" . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetRights  ----------


#===============================================================================

=head2 RemoveRight

Remove specific access right

=head3 Incoming Arguments

=over

=item I<$rowid>: The document rowid

=item I<$type>: The document type ('category' or 'notice')

=item I<$object_type>: The object to remove right type ('user' or 'team')

=item I<$object_id>: The object to remove right rowid

=back

=head3 Return value

=over

A Perl hash to be converted to JSON or XML:

	{
		success: 'true' | 'false',
		message: ...,
		rights: ....	# See GetRights output
	}

=back

=cut

#===============================================================================
sub RemoveRight {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::RemoveRight] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'object_id' ], 'disallow_empty', 'want_int' ],
			[ [ 'type' ], 'disallow_empty', 'stripxws', [ 'match', "\(category|notice\)" ] ],
			[ [ 'object_type' ], 'disallow_empty', 'stripxws', [ 'match', "\(user|team\)" ] ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if ($context->{args}->{type} eq 'notice') {
		push (@$params, [ [ 'rowid' ], 'disallow_empty', 'want_int' ]);
	}
	else {
		push (@$params, [ [ 'rowid' ], 'allow_empty', 'want_int' ]);
	}

	my $data = {
		success => 'false',
		message => __('Access right removal failed.')
	};

	if (!@$errors) {
		try {
			$self->{db}->BeginTransaction ();

			# Load document
			my $document_id = $self->GetDocumentId ($values->{type}, $values->{rowid});
			my $document = Mioga2::Document->Get ({
				db_obj  => $self->{db},
				rowid   => $document_id
			});

			if ($values->{object_type} eq 'user') {
				$document->RemoveAccessForUser ($values->{object_id});
			}
			elsif ($values->{object_type} eq 'team') {
				$document->RemoveAccessForTeam ($values->{object_id});
			}
			else {
				throw Mioga2::Exception::Application ('Mioga2::Diderot::RemoveRight', "Object type is $values->{object_type}, which should not be...")
			}

			$self->{db}->EndTransaction ();

			$data->{success} = 'true';
			$data->{message} = __('Access right successfully removed.');
			$data->{rights} = $self->GetRights ($context);
		}
		otherwise {
			$self->{db}->RollbackTransaction ();
		};
	}
	else {
		print STDERR "[Mioga2::Diderot::RemoveRight] Got errors: " . Dumper $errors;
	}

	return ($data);
}	# ----------  end of subroutine RemoveRight  ----------


#===============================================================================

=head2 GetUsers

Get list of user members of group

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	[
		{
			rowid: ...,
			ident: ...,
			firstname: ...,
			lastname: ...,
			email: ...
		},
		...
	]

=back

=cut

#===============================================================================
sub GetUsers {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::GetUsers] Entering, args: " . Dumper $context->{args} if ($debug);

	my $data;

	@$data = $context->GetGroup ()->GetInvitedUserList (short_list => 1)->GetUsers ();

	print STDERR "[Mioga2::Diderot::GetUsers] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetUsers  ----------


#===============================================================================

=head2 GetTeams

Get list of team members of group

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	[
		{
			rowid: ...,
			ident: ...
		},
		...
	]

=back

=cut

#===============================================================================
sub GetTeams {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::GetTeams] Entering, args: " . Dumper $context->{args} if ($debug);

	my $data;

	@$data = $context->GetGroup ()->GetInvitedTeamList (short_list => 1)->GetTeams ();

	print STDERR "[Mioga2::Diderot::GetTeams] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetTeams  ----------


#===============================================================================

=head2 AttachFiles

Add (link) a file to an existing notice

=head3 Incoming Arguments

=over

=item I<$rowid>: The notice rowid

=item I<$files>: An array of file URIs

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		success: 'true' or 'false',
		message: ...
	}

=back

=cut

#===============================================================================
sub AttachFiles {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Diderot::AttachFiles] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int', 'diderot_notice_writable' ],
			[ [ 'files' ], 'disallow_empty', 'stripxws' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default values
	my $data = {
		success => 'false',
		message => __('File attach failed.'),
	};

	if (!@$errors) {
		my $args = [$values->{rowid}, $self->{group_id}, $self->{group_id}];
		for my $uri (@{ac_ForceArray ($values->{files})}) {
			push (@$args, $uri);
		}
		my $placeholders = join (', ', ('?') x @{ac_ForceArray ($values->{files})});
		push (@$args, $values->{rowid});
		$self->{db}->ExecSQL ('INSERT INTO notice_files SELECT notice.rowid, m_uri.rowid FROM notice, m_uri WHERE notice.rowid = ? AND notice.group_id = ? AND m_uri.group_id = ? AND m_uri.uri IN (' . $placeholders . ') AND (notice.rowid, m_uri.rowid) NOT IN (SELECT * FROM notice_files WHERE notice_id = ?);', $args);

		$data->{success} = 'true';
		$data->{message} = __('Files successfully attached.');
	}

	print STDERR "[Mioga2::Diderot::AttachFile] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine AttachFile  ----------


#===============================================================================

=head2 DownloadFile

Download an attached file

=head3 Incoming Arguments

=over

=item I<entries>: The file path

=item I<path>: The file's directory path

=back

=head3 Return value

=over

The file to be downloaded

=back

=cut

#===============================================================================
sub DownloadFile {
	my ($self, $context) = @_;

	return (Mioga2::Magellan->DownloadFiles ($context));
}	# ----------  end of subroutine DownloadFile  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut
# ============================================================================


#===============================================================================

=head2 GenerateDocumentURI

Generate URI to be inserted into "document" table

=head3 Incoming Arguments

=over

=item I<$config>: The Mioga2::Config matching the Mioga instance

=item I<$group>: The Mioga2::GroupList matching the Mioga group

=item I<$type>: The document type ('category' or 'notice')

=item I<$rowid>: The document rowid

=back

=head3 Return value

=over

The document URI: 
	/<instance>/<group>/<type>/<rowid>

=back

=cut

#===============================================================================
sub GenerateDocumentURI {
	my ($self, $type, $rowid) = @_;

	my $uri = '/' . $self->{mioga_ident} . '/' . $self->{group_ident};

	$uri .= '/' . $type . '/' . $rowid if (defined ($type));

	return ($uri);
}	# ----------  end of subroutine GenerateDocumentURI  ----------


#===============================================================================

=head2 GetRootDocument

Get root Mioga2::Document object

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

The root Mioga2::Document.

=back

=cut

#===============================================================================
sub GetRootDocument {
	my ($self) = @_;

	my $document;
	my $template = $self->{db}->SelectSingle ('SELECT * FROM notice_template WHERE group_id = ?;', [$self->{group_id}]);

	if ($template) {
		if ($template->{document_id}) {
			# Get root document
			$document = Mioga2::Document->Get ({
				db_obj      => $self->{db},
				rowid       => $template->{document_id},
			});
		}
		else {
			# Create root document
			$document = Mioga2::Document->Create ({
				db_obj      => $self->{db},
				url         => $self->GenerateDocumentURI (),
				app_id      => $self->{app_id},
				mioga_id    => $self->{mioga_id},
				group_id       => $self->{group_id},
			});
			$document->InheritsFrom ($document->GetRowid ());

			# Grant current user profile read/write access to root
			my $group = Mioga2::GroupList->new ($self->{config}, { attributes => { rowid => $self->{group_id} } });
			my $user = Mioga2::UserList->new ($self->{config}, { attributes => { groups => $group, rowid => $self->{user_id} } });
			my $profile_id = $user->Get ('profile_id');
			$document->GrantAccessToProfile ($profile_id, 2);

			# Update template
			$self->{db}->ExecSQL ('UPDATE notice_template SET document_id = ? WHERE group_id = ?;', [$document->GetRowid (), $self->{group_id}]);
		}
	}

	return ($document);
}	# ----------  end of subroutine GetRootDocument  ----------


#===============================================================================

=head2 GetDocumentId

Get Mioga2::Document rowid for a Diderot object

=head3 Incoming Arguments

=over

=item I<$type>: The object type (category, notice)

=item I<$rowid>: The object rowid

=back

=head3 Return value

=over

The document rowid.

=back

=cut

#===============================================================================
sub GetDocumentId {
	my ($self, $type, $rowid) = @_;

	my $table;
	if ($type eq 'notice') {
		$table = 'notice';
	}
	elsif ($type eq 'category') {
		$table = 'notice_category';
	}
	else {
		throw Mioga2::Exception::Application ('Mioga2::Diderot::GetDocumentId', "Object type $type is not supported");
	}

	my $doc_id;

	if (($type eq 'category') && (!defined ($rowid))) {
		$doc_id = $self->GetRootDocument ()->GetRowid ();
	}
	else {
		$doc_id = $self->{db}->SelectSingle ("SELECT document_id FROM $table WHERE rowid = ?;", [$rowid])->{document_id};
	}

	return ($doc_id);
}	# ----------  end of subroutine GetDocumentId  ----------


#===============================================================================

=head2 CheckAccessToDocument

Check user has enough access to document

=head3 Incoming Arguments

=over

=item I<$type>: The document type ('category' or 'notice')

=item I<$rowid>: The object (category or notice) rowid

=item I<$access>: The minimum access (1 for read-only or 2 for read-write)

=back

=head3 Return value

=over

undef if user has enough access, 1 if user has not enough access.

=back

=cut

#===============================================================================
sub CheckAccessToDocument {
	my ($self, $type, $rowid, $access) = @_;

	my $res = 1;

	my $document = Mioga2::Document->Get ({
		db_obj      => $self->{db},
		rowid       => $self->GetDocumentId ($type, $rowid)
	});
	my $doc_access = $document->GetAccess ($self->{user_id});
	if (int ($doc_access) >= int ($access)) {
		$res = undef;
	}

	return ($res);
}	# ----------  end of subroutine CheckAccessToDocument  ----------


#===============================================================================

=head2 RevokeUserFromGroup

Remove user data from database when a user is revoked from a group.

=head3 Incoming Arguments

=over

=item I<$config>: The Mioga2::Config

=item I<$group_id>: The group rowid

=item I<$user_id>: The user rowid

=item I<$group_animator_id>: The group animator rowid

=back

=head3 Return value

=over

None.

=back

=cut

#===============================================================================
sub RevokeUserFromGroup {
	my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;

	$self->{db}->ExecSQL ('DELETE FROM doc_authz_user WHERE doc_id IN (SELECT getRefDocId (document_id) FROM notice_category WHERE group_id = ? UNION SELECT getRefDocId (document_id) FROM notice WHERE group_id = ?) AND user_id = ?;', [$group_id, $group_id, $user_id]);
	$self->{db}->ExecSQL ('UPDATE notice SET user_id = ? WHERE user_id = ?;', [$group_animator_id, $user_id]);
}	# ----------  end of subroutine RevokeUserFromGroup  ----------


#===============================================================================

=head2 RevokeTeamFromGroup

Remove team data from database when a team is revoked from a group.

=head3 Incoming Arguments

=over

=item I<$config>: The Mioga2::Config

=item I<$group_id>: The group rowid

=item I<$team_id>: The team rowid

=back

=head3 Return value

=over

None.

=back

=cut

#===============================================================================
sub RevokeTeamFromGroup {
	my ($self, $config, $group_id, $team_id) = @_;

	$self->{db}->ExecSQL ('DELETE FROM doc_authz_team WHERE doc_id IN (SELECT getRefDocId (document_id) FROM notice_category WHERE group_id = ? UNION SELECT getRefDocId (document_id) FROM notice WHERE group_id = ?) AND team_id = ?;', [$group_id, $group_id, $team_id])
}	# ----------  end of subroutine RevokeTeamFromGroup  ----------


#===============================================================================

=head2 DeleteGroupData

Remove group data in database when a group is deleted

=head3 Incoming Arguments

=over

=item I<$config>: A Mioga2::Config object

=item I<$group_id>: The group rowid

=back

=head3 Return value

=over

None.

=back

=cut

#===============================================================================
sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;

	$self->{db}->ExecSQL ('DELETE FROM doc_authz_profile WHERE doc_id IN (SELECT rowid FROM document WHERE rowid IN (SELECT document_id FROM notice WHERE group_id = ? UNION SELECT document_id FROM notice_category WHERE group_id = ? UNION SELECT document_id FROM notice_template WHERE group_id = ?));', [$group_id, $group_id, $group_id]);
	$self->{db}->ExecSQL ('DELETE FROM doc_authz_team WHERE doc_id IN (SELECT rowid FROM document WHERE rowid IN (SELECT document_id FROM notice WHERE group_id = ? UNION SELECT document_id FROM notice_category WHERE group_id = ? UNION SELECT document_id FROM notice_template WHERE group_id = ?));', [$group_id, $group_id, $group_id]);
	$self->{db}->ExecSQL ('DELETE FROM doc_authz_user WHERE doc_id IN (SELECT rowid FROM document WHERE rowid IN (SELECT document_id FROM notice WHERE group_id = ? UNION SELECT document_id FROM notice_category WHERE group_id = ? UNION SELECT document_id FROM notice_template WHERE group_id = ?));', [$group_id, $group_id, $group_id]);
	$self->{db}->ExecSQL ('DELETE FROM notice_files WHERE notice_id IN (SELECT rowid FROM notice WHERE group_id = ?);', [$group_id]);
	$self->{db}->ExecSQL ('DELETE FROM notice_field WHERE notice_id IN (SELECT rowid FROM notice WHERE group_id = ?);', [$group_id]);
	$self->{db}->ExecSQL ('DELETE FROM notice WHERE group_id = ?;', [$group_id]);
	$self->{db}->ExecSQL ('DELETE FROM notice_category WHERE group_id = ?;', [$group_id]);
	$self->{db}->ExecSQL ('DELETE FROM notice_template WHERE group_id = ?;', [$group_id]);

	return ();
}	# ----------  end of subroutine DeleteGroupData  ----------


#===============================================================================

=head2 InitApp

Initialize application parameters

=cut

#===============================================================================
sub InitApp {
	my ($self, $context) = @_;

	$self->{db} = $context->GetConfig ()->GetDBObject ();
	$self->{config} = $context->GetConfig ();
	$self->{app_id} = $context->GetAppDesc ()->GetRowid ();
	$self->{group_id} = $context->GetGroup ()->Get ('rowid');
	$self->{group_ident} = $context->GetGroup ()->Get ('ident');
	$self->{user_id} = $context->GetUser ()->Get ('rowid');
	$self->{mioga_id} = $context->GetConfig ()->GetMiogaId ();
	$self->{mioga_ident} = $context->GetConfig ()->GetMiogaIdent ();

	# Register String::Checker extensions
	String::Checker::register_check ('diderot_category_readable', sub {
		my $rowid = shift;
		return ($self->CheckAccessToDocument ('category', $$rowid, 1))
	});
	String::Checker::register_check ('diderot_category_writable', sub {
		my $rowid = shift;
		return ($self->CheckAccessToDocument ('category', $$rowid, 2))
	});
	String::Checker::register_check ('diderot_notice_readable', sub {
		my $rowid = shift;
		return ($self->CheckAccessToDocument ('notice', $$rowid, 1))
	});
	String::Checker::register_check ('diderot_notice_writable', sub {
		my $rowid = shift;
		return ($self->CheckAccessToDocument ('notice', $$rowid, 2))
	});
}	# ----------  end of subroutine InitApp  ----------


#===============================================================================

=head2 GenerateSearchQuery

Generate a SQL search query from WebService arguments

=head3 Incoming Arguments

=over

=item I<$list>: An array ref containing search criteria (see SearchNotices "query" argument)

=back

=head3 Return value

=over

=item A string containing the query WHERE statement

=item An array ref containing sub-queries used by query WHERE statement

=item An array ref containing query arguments matching placeholders

=back

=cut

#===============================================================================
sub GenerateSearchQuery {
	my ($self, $list, $index, $tables, $args) = @_;

	$$index ||= 1;

	my $query;
	for my $item (@$list) {
		# Query part for current item
		my $part;

		# Get linking operator
		if ($query) {
			if ($item->{operator} eq 'and') {
				$part = 'AND ';
			}
			elsif ($item->{operator} eq 'or') {
				$part = 'OR ';
			}
			else {
				throw Mioga2::Exception::Application ("Mioga2::Diderot::GenerateSearchQuery", "Operator \"$item->{operator}\" is invalid.");
			}
		}

		if ($item->{field} eq 'special-subexp') {
			# Sub-query, go deeper into processing
			my ($subquery) = $self->GenerateSearchQuery ($item->{query}, $index, $tables, $args);
			$part .= "($$subquery)";
		}
		else {
			# Get matching operator
			my $query_arg = $item->{query};
			my $operator;
			if ((!exists ($item->{constraint})) || ($item->{constraint} eq 'equals')) {
				$operator = '=';
			}
			elsif ($item->{constraint} eq 'contains_any') {
				$operator = 'SIMILAR TO';
				$query_arg = '%(' . join ('|', split (/ /, $query_arg)) . ')%';
			}
			elsif ($item->{constraint} eq 'contains_all') {
				$operator = '~';
				# Translate query_arg from "word1 word2 word3" to "^(?=.*?word1)(?=.*?word2)(?=.*?word3).*$" matching all words in any order
				$query_arg = '^' . join ('', map { "(?=.*?$_)" } split (/ /, $query_arg)) . '.*$';
			}
			elsif ($item->{constraint} eq 'contains') {
				$operator = 'LIKE';
				$query_arg = "%$query_arg%";
			}
			elsif ($item->{constraint} eq 'does_not_contain') {
				$operator = 'NOT LIKE';
				$query_arg = "%$query_arg%";
			}

			# Push query tables and arguments
			push (@$tables, "(SELECT notice_id FROM notice_field WHERE ident = ? AND lower(value) $operator lower(?)) AS T$$index");
			push (@$args, $item->{field}, $query_arg);

			# Join tables
			$part .= "notice.rowid = T$$index.notice_id";
		}

		$$index++;

		# Append query part to global query
		$query .= " $part";
	}

	return (\$query, $tables, $args);
}	# ----------  end of subroutine GenerateSearchQuery  ----------


# ============================================================================

=head2 GetRSSFeed ($context, [$feed])

  Return a feed of notices.

=cut

# ============================================================================
sub GetRSSFeed {
    my ($self, $context, $ident, $feed, $max)   = @_;

    my $config  = $context->GetConfig ();
    my $group = Mioga2::GroupList->new ($config, { attributes => { ident => $ident } });
	my $user = $context->GetUser ();
	my $db = $config->GetDBObject ();

    if (!$feed) {
        $feed   = XML::Atom::SimpleFeed->new(
            title   => __x("Notices feed of {group} group", group => $group->GetIdent),
            link    => { href => $config->GetProtocol . "://" . $config->GetServerName . $context->{uri}->GetURI, rel => 'self' },
            link    => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/Diderot/DisplayMain",
            id      => "data:,group:".$group->GetIdent.":notices",
        );
    }
    $max    = 20 unless $max;

	# Get title field (first field) from template
	my $template = $db->SelectSingle ("SELECT notice_template.field_order FROM notice_template WHERE notice_template.group_id = ?", [$group->GetRowid]);
	my $order = Mioga2::tools::Convert::JSONToPerl ($template->{field_order});
	my $title_field = $order->[0]->[0];

	my $fields = $db->SelectMultiple ("SELECT * FROM (SELECT * FROM notice_field UNION ALL SELECT 'user_id' AS ident, user_id::char AS value, rowid AS notice_id FROM notice UNION ALL SELECT 'modified' AS ident, to_char (modified, 'YYYY-MM-DD HH24:MM:SS') AS value, rowid AS notice_id FROM notice UNION ALL SELECT 'created' AS ident, to_char (created, 'YYYY-MM-DD HH24:MM:SS') AS value, rowid AS notice_id FROM notice UNION SELECT 'access' AS ident, authzTestAccessForDocument ((SELECT document_id FROM notice WHERE rowid=notice_field.notice_id), ?)::char AS value, notice.rowid AS notice_id FROM notice, notice_field WHERE notice.rowid = notice_field.notice_id) AS fields WHERE fields.notice_id IN (SELECT notice.rowid FROM notice WHERE group_id = ? ORDER BY notice.modified DESC LIMIT ?);", [$user->GetRowid, $group->GetRowid, $max]);
	my $notices;
	for my $field (@{ac_ForceArray ($fields)}) {
		$notices->{$field->{notice_id}}->{$field->{ident}} = $field->{value};
	}
	my @notices = grep { int ($_->{access}) > 0 } map { $notices->{$_}->{rowid} = $_; $notices->{$_} } keys (%$notices);
    foreach my $notice (@notices) {
		next if ($notice->{access} == 0);
		my $author = Mioga2::UserList->new ($config, { attributes => { rowid => $notice->{user_id} } });
		$feed->add_entry (
			title         => $notice->{$title_field},
			summary       => '',
			link          => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/Diderot/DisplayMain#Diderot-notice-$notice->{rowid}",
			id            => "data:,group:" . $group->GetIdent . ":notices:$notice->{rowid}",
			author        => $author->Get ('label'),
			published     => Mioga2::Classes::Time->FromPSQL ($notice->{created})->RFC3339DateTime,
			updated       => Mioga2::Classes::Time->FromPSQL ($notice->{modified})->RFC3339DateTime,
			content       => ''
		);
    }
    return $feed;
}



# ============================================================================
# GetAppDesc ()
# ============================================================================

sub GetAppDesc {
	my ($self, $context) = @_;
	my %AppDesc = (
		ident   => 'Diderot',
		name    => __('Diderot'),
		package => 'Mioga2::Diderot',
		type => 'normal',
		description => __('The Mioga2 Document Management System application'),
		api_version => '2.4',
		is_group    => 1,
		is_user     => 0,
		is_resource => 0,
		all_groups  => 1,
		all_users  => 0,
		can_be_public => 0,
		usable_by_resource => 0,

		functions   => {
			'Use' => __('Read, write and search notices, create categories and order them'),
			'Animation' => __('Create, modify and delete notice template, set access rights'),
		},

		func_methods  => {
			Use => [ 'DisplayMain', 'GetNotice', 'SetNotice', 'GetTemplate', 'GetRecent', 'SearchNotices', 'GetCategory', 'GetCategories', 'SetCategory', 'DeleteCategory', 'GetUncategorized', 'AttachFiles', 'DownloadFile' ],
			Animation => [ 'DisplayMain', 'GetNotice', 'SetNotice', 'GetTemplate', 'SetTemplate', 'GetRecent', 'SearchNotices', 'GetCategory', 'GetCategories', 'SetCategory', 'DeleteCategory', 'GetUncategorized', 'AttachFiles', 'GetRights', 'SetRights', 'RemoveRight', 'GetUsers', 'GetTeams', 'DownloadFile' ],
		},
		public_methods => [ ],
		func_mioglets => { },
		public_mioglets => [  ],
		non_sensitive_methods => [
			'DisplayMain',
			'GetNotice',
			'GetTemplate',
			'GetRecent',
			'SearchNotices',
			'GetCategory',
			'GetCategories',
			'GetUncategorized',
			'AttachFiles',
			'GetRights',
			'GetUsers',
			'GetTeams',
			'DownloadFile',
		]
    );
	return \%AppDesc;
}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2011, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
