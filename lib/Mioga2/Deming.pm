#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Deming.pm: the Mioga2 activity-reporting application

=head1 DESCRIPTION

The Mioga2::Deming application allow users to report the time passed on their projects.

=head1 METHODS DESCRIPTION

=cut

#
# ============================================================================

package Mioga2::Deming;
use base qw(Mioga2::Application);
use strict;
use warnings;
use open ':encoding(utf8)';

use Data::Dumper;
use Locale::TextDomain::UTF8 'deming';
use Error qw(:try);

use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::Content::XSLT;
use Mioga2::Exception::Simple;
use Mioga2::TaskCategory;
use Mioga2::XML::Simple;
use Mioga2::Classes::CalEvent;

my $debug = 0;


# ============================================================================
# GetAppDesc ()
# ============================================================================

sub GetAppDesc {
	my ($self, $context) = @_;
    my %AppDesc = ( 
    	ident   => 'Deming',
	    name	=> __('Deming'),
        package => 'Mioga2::Deming',
		type => 'normal',
        description => __('The Mioga2 activity-reporting application'),
        api_version => '2.4',
		is_group    => 0,
		is_user     => 1,
		is_resource => 0,
        all_groups  => 0,
        all_users  => 1,
        can_be_public => 0,
        usable_by_resource => 0,

        functions   => { 
			'Use' => __('Activity reporting'),
		},
				
        func_methods  => { 
			Use => [ 'DisplayMain', 'GetActivities', 'GetEntries', 'GetPreferences', 'SetEntry', 'DeleteEntry', 'SetPreferences', 'GetCalendarEvents' ],
		},
		public_methods => [ ],
		func_mioglets => { },
		public_mioglets => [  ],
		non_sensitive_methods => [
			'DisplayMain',
			'GetActivities',
			'GetEntries',
			'GetPreferences',
			'GetCalendarEvents'
		],
    );
	return \%AppDesc;
}
#===============================================================================

=head1 INTERFACE METHODS

=cut

#===============================================================================

=head2 DisplayMain

Display main UI

=head3 Generated XML

=over

	<DisplayMain>
		<miogacontext>See Mioga2::RequestContext</miogacontext>
	</DisplayMain>

=back

=cut

# ============================================================================
sub DisplayMain {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Deming::DisplayMain]\n" if ($debug);

	my $data = {
		DisplayMain => $context->GetContext (),
	};
	$data->{DisplayMain}->{UserPrefs} = $context->GetSession ()->{persistent}->{Deming};

	# Get external sources configuration (system-wide)
	my %external_sources_conf;
	my $confdir = $self->{config}->GetInstallPath () . '/conf';
	if (-e "$confdir/deming_conf.xml") {
		# External activities may be used
		print STDERR "[Mioga2::Deming::GetActivities] Loading external sources from $confdir/deming_conf.xml\n" if ($debug);
		try {
			my $enabled_sources = $self->{db}->SelectMultiple ('SELECT task_category.*, activity_report_source.ident AS source_ident FROM task_category, activity_report_source WHERE activity_report_source.task_category_id = task_category.rowid AND task_category.mioga_id = ?;', [$context->GetConfig ()->GetMiogaId ()]);
			%external_sources_conf = %{$self->ParseExternalConf ("$confdir/deming_conf.xml")};
			$data->{DisplayMain}->{ExternalSource} = $enabled_sources;
		}
		otherwise {
			my $err = shift;
			print STDERR "[Mioga2::Deming::GetActivities] External source configuration is invalid, please fix: " . $err->stringify ();
		};
	}

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "[Mioga2::Deming::DisplayMain] XML: $xml\n" if ($debug);

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'deming.xsl', locale_domain => 'deming_xsl');
	$content->SetContent($xml);
	return $content;
}
# ============================================================================

#===============================================================================

=head1 WEBSERVICE METHODS

=cut

#===============================================================================


#===============================================================================

=head2 GetActivities

Returns list of available activities

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

A Perl hash to be translated to JSON or XML:
	{
		success: true / false,
		message: '...',
		activities: [
			{
				rowid: ...,
				ident: '...',
				bgcolor: '...',
				fgcolor: '...'
			},
			{
				rowid: ...,
				ident: '...',
				bgcolor: '...',
				fgcolor: '...',
				children: [
					{
						rowid: ...,
						ident: '...'
					},
					...
				]
			},
			...
		]
	}

=back

=cut

#===============================================================================
sub GetActivities {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Deming::GetActivities] Entering\n" if ($debug);

	# Default return value
	my $data = {
		success => 'boolean::false',
		activities => [ ]
	};


	# Get external sources configuration (system-wide)
	my %external_sources_conf;
	my $confdir = $self->{config}->GetInstallPath () . '/conf';
	if (-e "$confdir/deming_conf.xml") {
		# External activities may be used
		print STDERR "[Mioga2::Deming::GetActivities] Loading external sources from $confdir/deming_conf.xml\n" if ($debug);
		try {
			my $enabled_sources = $self->{db}->SelectMultiple ('SELECT task_category.*, activity_report_source.ident AS source_ident FROM task_category, activity_report_source WHERE activity_report_source.task_category_id = task_category.rowid AND task_category.mioga_id = ?;', [$context->GetConfig ()->GetMiogaId ()]);
			%external_sources_conf = %{$self->ParseExternalConf ("$confdir/deming_conf.xml")};
		}
		otherwise {
			my $err = shift;
			print STDERR "[Mioga2::Deming::GetActivities] External source configuration is invalid, please fix: " . $err->stringify ();
		};
	}

	# Get activities for current instance
	my $tc = Mioga2::TaskCategory->new ($context->GetConfig ());
	for my $activity (@{$tc->GetList ()}) {
		# Check task category is linked to an external activity
		my $external_link = $self->{db}->SelectSingle ('SELECT * FROM activity_report_source WHERE task_category_id = ?;', [$activity->{rowid}]);

		if (defined ($external_link)) {
			# Is linked to an external activity, add external data
			my $source = $external_sources_conf{$external_link->{ident}};

			# Fetch source data
			my $sql = "SELECT $source->{key} AS rowid, $source->{label} AS name FROM $source->{table} WHERE $source->{filter};";
			print STDERR "[Mioga2::Deming::GetActivities] SQL for $source->{name} external source: $sql\n" if ($debug);
			my $res = $self->{db}->SelectMultiple ($sql, [$context->GetUser ()->Get ('rowid')]);
			for my $rec (@$res) {
				# Fetch source sub-items, if any
				if (defined ($source->{items})) {
					my $sql = "SELECT $source->{items}->{key} AS rowid, $source->{items}->{label} AS name FROM $source->{items}->{table} WHERE $source->{items}->{join} = ?;";
					print STDERR "[Mioga2::Deming::GetActivities] SQL for $source->{name} external source sub-elements: $sql\n" if ($debug);
					$rec->{children} = $self->{db}->SelectMultiple ($sql, [$rec->{rowid}]);
				}

				# Push external data into activity description
				push (@{$activity->{children}}, $rec);
			}
		}

		# Push activity to output list
		push (@{$data->{activities}}, $activity);
	}

	# Set request status
	$data->{success} = 'boolean::true';

	print STDERR "[Mioga2::Deming::GetActivities] Leaving, data: " . Dumper ($data) if ($debug);
	return ($data);
}	# ----------  end of subroutine GetActivities  ----------


#===============================================================================

=head2 GetEntries

Returns report entries

=head3 Incoming Arguments

=over

=item I<start>: the date range start

=item I<end>: the date range end

=back

=head3 Return value

=over

A Perl data structure to be converted to XML or JSON:
	{
		success: true/false,
		entries: [
			{
				rowid: ...,
				activity: ...,
				dtstart: ...,
				dtend: ...,
				notes: ...
			}
		]
	}

=back

=cut

#===============================================================================
sub GetEntries {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Deming::GetEntries] Entering, args: " . Dumper $context->{args} if ($debug);

	my $data = { };


	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'start', 'end' ], 'disallow_empty', 'datetime' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	try {
		# Initialize sub-request to join activity_report to external sources
		my $ext_sql;
		my %external_sources_conf;
		my $confdir = $self->{config}->GetInstallPath () . '/conf';
		if (-e "$confdir/deming_conf.xml") {
			# External activities may be used
			print STDERR "[Mioga2::Deming::GetActivities] Loading external sources from $confdir/deming_conf.xml\n" if ($debug);
			%external_sources_conf = %{$self->ParseExternalConf ("$confdir/deming_conf.xml")};
			my @views;
			for my $src (keys (%external_sources_conf)) {
				push (@views, "SELECT * FROM ${src}_activity_report");
			}

			if (@views) {
				$ext_sql = 'LEFT JOIN (' . join (' UNION ', @views) . ') AS T1 ON T1.rowid = activity_report.rowid';
			}
		}

		# Merge sub-request into main request
		my $sql;
		if (!defined ($ext_sql)) {
			$sql = 'SELECT rowid, activity_id AS activity, dtstart, dtend, notes FROM activity_report WHERE user_id = ? AND dtstart < ? AND dtend > ?;';
		}
		else {
			$sql = "SELECT activity_report.rowid, activity_report.activity_id AS activity, activity_report.dtstart, activity_report.dtend, activity_report.notes, T1.cat_id AS sub_activity, T1.subcat_id AS sub_sub_activity FROM activity_report $ext_sql WHERE activity_report.user_id = ? AND activity_report.dtstart < ? AND activity_report.dtend > ?;";
		}
		$data->{entries} = $self->{db}->SelectMultiple ($sql, [$self->{user_id}, $values->{end}, $values->{start}]);

		$data->{success} = 'boolean::true';
	}
	otherwise {
		$data->{success} = 'boolean::false';
		$data->{entries} = [ ];
	};

	print STDERR "[Mioga2::Deming::GetEntries] Leaving, data: " . Dumper ($data) if ($debug);
	return ($data);
}	# ----------  end of subroutine GetEntries  ----------


#===============================================================================

=head2 SetEntry

Create or modify a report entry.

=head3 Incoming Arguments

=over

=item I<rowid>: (optional) the entry rowid

=item I<dtstart>: Start date

=item I<dtend>: End date

=item I<notes>: Entry notes

=back

=head3 Return value

=over

A Perl data structure to be converted to XML or JSON:
	{
		success: true/false,
		message: '...',
		entry: {
			...
		}
	}

=back

=cut

#===============================================================================
sub SetEntry {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Deming::SetEntry] Entering, args: " . Dumper $context->{args} if ($debug);

	# Default return value
	my $data = {
		success => 'boolean:false',
		message => __('Entry store failed')
	};

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'allow_empty', 'want_int' ],
			[ [ 'activity' ], 'disallow_empty', 'want_int' ],	# TODO check is activity
			[ [ 'sub_activity', 'sub_sub_activity' ], 'allow_empty', 'want_int' ],	# TODO check is activity
			[ [ 'dtstart', 'dtend' ], 'disallow_empty' ],	# TODO check is date
			[ [ 'notes' ], 'allow_empty' ],
			[ [ 'import_ref' ], 'allow_empty' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (!@$errors) {
		try {
			$self->{db}->BeginTransaction ();
			if (!st_ArgExists ($context, 'rowid')) {
				# Create entry
				$self->{db}->ExecSQL ('INSERT INTO activity_report (user_id, activity_id, dtstart, dtend, notes) VALUES (?, ?, ?, ?, ?);', [$self->{user_id}, $values->{activity}, $values->{dtstart}, $values->{dtend}, $values->{notes}]);
			}
			else {
				# Update entry
				$self->{db}->ExecSQL ('UPDATE activity_report SET activity_id = ?, dtstart = ?, dtend = ?, notes = ? WHERE user_id = ? AND rowid = ?;', [$values->{activity}, $values->{dtstart}, $values->{dtend}, $values->{notes}, $self->{user_id}, $values->{rowid}]);
			}

			# Get entry rowid for further use
			my $rowid = st_ArgExists ($context, 'rowid') ? $values->{rowid} : $self->{db}->GetLastInsertId ('activity_report');

			# Handle sub-activities
			if (st_ArgExists ($context, 'sub_activity')) {
				# Get external sources configuration (system-wide)
				my %external_sources_conf;
				my $confdir = $self->{config}->GetInstallPath () . '/conf';
				if (-e "$confdir/deming_conf.xml") {
					# External activities may be used
					print STDERR "[Mioga2::Deming::GetActivities] Loading external sources from $confdir/deming_conf.xml\n" if ($debug);
					%external_sources_conf = %{$self->ParseExternalConf ("$confdir/deming_conf.xml")};
					my $source = $self->{db}->SelectSingle ('SELECT activity_report_source.* FROM activity_report_source, task_category WHERE activity_report_source.task_category_id = task_category.rowid AND task_category.rowid = ? AND task_category.mioga_id = ?;', [$values->{activity}, $context->GetConfig ()->GetMiogaId ()]);
					if (!defined ($source)) {
						throw Mioga2::Exception::Simple ('Mioga2::Deming::SetEntry', "No source configuration found for rowid $values->{activity} on mioga_id " . $context->GetConfig ()->GetMiogaId ());
					}

					# Source configuration found, link Deming entry to source element
					my $conf = $external_sources_conf{$source->{ident}};
					my ($sql, $target_rowid);
					if (st_ArgExists ($context, 'sub_sub_activity')) {
						# 2nd-level link
						if (!defined ($conf->{items})) {
							throw Mioga2::Exception::Simple ('Mioga2::Deming::SetEntry', "No 'items' key in configuration for source '$conf->{name}'");
						}

						# Initialize SQL request to get item rowid and ensure it belong to correct Mioga2 instance
						$sql = "SELECT $rowid, $conf->{items}->{table}.$conf->{items}->{key} FROM $conf->{items}->{table}, $conf->{table} WHERE $conf->{items}->{table}.$conf->{items}->{join} = $conf->{table}.$conf->{key} AND $conf->{items}->{table}.$conf->{items}->{key} = ? AND $conf->{filter}";
						$target_rowid = $values->{sub_sub_activity};
					}
					else {
						# 1st-level link
						$sql = "SELECT $rowid, $conf->{key} FROM $conf->{table} WHERE $conf->{key} = ? AND $conf->{filter}";
						$target_rowid = $values->{sub_activity};
					}

					# Drop any already-existing activity link
					# No other way here than a loop across each table
					$self->ClearExternalLink ($rowid);

					# Record external activity link
					print STDERR "[Mioga2::Deming::SetEntry] Linking entry to external source entry: INSERT INTO activity_report_to_$source->{ident} $sql\n" if ($debug);
					print STDERR "[Mioga2::Deming::SetEntry] SQL args: $target_rowid, " . $context->GetUser ()->Get ('rowid') . "\n";
					$self->{db}->ExecSQL ("INSERT INTO activity_report_to_$source->{ident} $sql", [$target_rowid, $context->GetUser ()->Get ('rowid')]);
				}
			}
			else {
				# Clear any existing link to external activity
				$self->ClearExternalLink ($rowid);
			}

			$self->{db}->EndTransaction ();

			# Load and return entry
			# Initialize sub-request to join activity_report to external sources
			my $ext_sql;
			my %external_sources_conf;
			my $confdir = $self->{config}->GetInstallPath () . '/conf';
			if (-e "$confdir/deming_conf.xml") {
				# External activities may be used
				print STDERR "[Mioga2::Deming::GetActivities] Loading external sources from $confdir/deming_conf.xml\n" if ($debug);
				%external_sources_conf = %{$self->ParseExternalConf ("$confdir/deming_conf.xml")};
				my @views;
				for my $src (keys (%external_sources_conf)) {
					push (@views, "SELECT * FROM ${src}_activity_report");
				}

				if (@views) {
					$ext_sql = 'LEFT JOIN (' . join (' UNION ', @views) . ') AS T1 ON T1.rowid = activity_report.rowid';
				}
			}

			# Merge sub-request into main request
			my $sql;
			if (!defined ($ext_sql)) {
				$sql = 'SELECT rowid, activity_id AS activity, dtstart, dtend, notes FROM activity_report WHERE rowid= ?;';
			}
			else {
				$sql = "SELECT activity_report.rowid, activity_report.activity_id AS activity, activity_report.dtstart, activity_report.dtend, activity_report.notes, T1.cat_id AS sub_activity, T1.subcat_id AS sub_sub_activity FROM activity_report $ext_sql WHERE activity_report.rowid = ?;";
			}
			$data->{entry} = $self->{db}->SelectSingle ($sql, [$rowid]);

			# Keep import references
			#if (st_ArgExists ($context, 'import_ref')) {
			#	my $session = $context->GetSession ();
			#	push (@{$session->{persistent}->{Deming}->{ImportReferences}}, $values->{import_ref});
			#}

			$data->{success} = 'boolean::true';
			$data->{message} = __('Entry successfully stored');
		}
		otherwise {
			my $err = shift;
			print STDERR '[Mioga2::Deming::GetActivities] Store failed: ' . $err->stringify ();
			$self->{db}->RollbackTransaction ();
			$data->{success} = 'boolean::false';
			$data->{message} = __('Entry store failed');
		};
	}
	else {
		print STDERR '[Mioga2::Deming::SetEntry] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	print STDERR "[Mioga2::Deming::SetEntry] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetEntry  ----------


#===============================================================================

=head2 DeleteEntry

Delete a report entry.

=head3 Incoming Arguments

=over

=item I<rowid>: the entry rowid

=back

=head3 Return value

=over

A Perl data structure to be converted to XML or JSON:
	{
		success: true/false,
		message: '...'
	}

=back

=cut

#===============================================================================
sub DeleteEntry {
	my ($self, $context) = @_;

	# Default return value
	my $data = {
		success => 'boolean:false',
		message => __('Entry deletion failed')
	};

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (!@$errors) {
		try {
			$self->{db}->BeginTransaction ();

			# Delete any link to external activity
			$self->ClearExternalLink ($values->{rowid});

			# Delete entry
			$self->{db}->ExecSQL ('DELETE FROM activity_report WHERE user_id = ? AND rowid = ?;', [$self->{user_id}, $values->{rowid}]);

			$self->{db}->EndTransaction ();

			$data->{success} = 'boolean::true';
			$data->{message} = __('Entry successfully deleted');
		}
		otherwise {
			$self->{db}->RollbackTransaction ();
			$data->{success} = 'boolean::false';
			$data->{message} = __('Entry deletion failed');
		};
	}
	else {
		print STDERR '[Mioga2::Deming::DeleteEntry] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	return ($data);
}	# ----------  end of subroutine DeleteEntry  ----------


#===============================================================================

=head2 GetPreferences

Returns user preferences

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON:
	{
		success: true / false,
		message: '...',
		preferences: {
			default_activity: ...,
			default_sub_activity: ...
		}
	}

=back

=cut

#===============================================================================
sub GetPreferences {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Deming::GetPreferences] Entering\n" if ($debug);

	my $data = {
		success => 'boolean::true',
		preferences => { }
	};

	# Get session
	my $session = $context->GetSession ();
	if (exists ($session->{persistent}->{Deming})) {
		for my $key (keys (%{$session->{persistent}->{Deming}})) {
			next if ($key eq 'ImportReferences');
			$data->{preferences}->{$key} = $session->{persistent}->{Deming}->{$key};
		}
	}

	print STDERR "[Mioga2::Deming::GetPreferences] Leaving, data: " . Dumper ($data) if ($debug);
	return ($data);
}	# ----------  end of subroutine GetPreferences  ----------


#===============================================================================

=head2 SetPreferences

Store user preferences into session

=head3 Incoming Arguments

=over

=item I<slotMinutes>: The FullCalendar slotMinutes value

=item I<date>: The FullCalendar date value

=item I<month>: The FullCalendar month value

=item I<year>: The FullCalendar year value

=item I<dayLength>: Day length for all-day events imported from Chronos

=item I<weekends>: Weekends visibility into calendar

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:
	{
		success: true/false,
		message: '...'
	}

=back

=cut

#===============================================================================
sub SetPreferences {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Deming::SetPreferences] Entering, args: " . Dumper $context->{args} if ($debug);

	# Default return value
	my $data = {
		success => 'boolean::false',
		message => __('Preferences store failed.')
	};
	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			# WARNING Keep list of possible preferences on a single array element otherwise some preferences won't be stored (see comment "Save data into session" below)
			[ [ 'slotMinutes', 'year', 'month', 'date', 'weekends', 'firstHour' ], 'allow_empty', 'want_int' ],
			[ [ 'dayLength' ], 'allow_empty', 'stripxws' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (!@$errors) {
		# Get session
		my $session = $context->GetSession ();

		# Save data into session
		my @keys = qw (slotMinutes year month date weekends firstHour dayLength weekends);
		for my $pref (@keys) {
			$session->{persistent}->{Deming}->{$pref} = $values->{$pref} if (exists ($values->{$pref}));
		}

		$data->{success} = 'boolean::true';
		$data->{message} = __('Preferences successfully stored.');
	}
	else {
		print STDERR '[Mioga2::Deming::SetPreferences] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	print STDERR "[Mioga2::Deming::SetPreferences] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetPreferences  ----------


#===============================================================================

=head2 GetCalendarEvents

Get events from Chronos calendar

=head3 Incoming Arguments

=over

=item I<start_year>: the lower date limit year

=item I<start_month>: the lower date limit month

=item I<start_day>: the lower date limit day

=item I<end_year>: the upper date limit year

=item I<end_month>: the upper date limit month

=item I<end_day>: the upper date limit day

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		"success": true,
		"calendars": [
			{
				"calendar": "My calendar",
				"type": "owner",
				"events": [
					{
						"rowid": 1,
						"subject": "Some description",
						"dtstart": "2013-03-05 09:00:00",
						"dtend": "2013-03-05 11:00:00",
						"category_id": 2
					},
					{
						"rowid": 2,
						"subject": "Another description",
						"dtstart": "2013-03-05 11:00:00",
						"dtend": "2013-03-05 12:00:00",
						"category_id": 5
					}
				]
			},
			{
				"calendar": "Group 1",
				"type": "group",
				"events": [
					{
						"rowid": 3,
						"subject": "A description",
						"dtstart": "2013-03-05 13:00:00",
						"dtend": "2013-03-05 14:00:00",
						"category_id": 1
					}
				]
			}
		]
	}

=back

=cut

#===============================================================================
sub GetCalendarEvents {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Deming::GetCalendarEvents] Entering, args: " . Dumper $context->{args} if ($debug);

	# Default return value
	my $data = {
		success => 'boolean::false',
		message => __('Preferences store failed.')
	};


	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'start_day', 'start_month', 'start_year', 'end_day', 'end_month', 'end_year' ], 'disallow_empty', 'want_int' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (!@$errors) {
		my $session = $context->GetSession ()->{persistent}->{Deming};

		my $calendars = Mioga2::Classes::CalEvent::GetEventsForUser ({
			start_day => $values->{start_day},
			start_month => $values->{start_month},
			start_year => $values->{start_year},
			end_day => $values->{end_day},
			end_month => $values->{end_month},
			end_year => $values->{end_year},
			owner_cals => 1,
			group_cals => 1,
			owner_id => $self->{user_id},
			user_id => $self->{user_id},
			db => $self->{db}
		});

		for my $cal (@{$calendars}) {
			my $out_cal = {
				type => $cal->{type},
				calendar => $cal->{calendar},
				calendar_id => $cal->{calendar_id},
				events => [ ]
			};

			# Loop through events to filter ones already imported
			for my $event (@{$cal->{events}}) {
#				if (!grep { $_ eq $cal->{calendar_id} . '-' . $event->{rowid} } @{$session->{ImportReferences}}) {
					push (@{$out_cal->{events}}, $event);
#				}
			}

			push (@{$data->{calendars}}, $out_cal);
		}

		# Add virtual calendar for project tasks
		my $project_tasks = $self->{db}->SelectMultiple ("SELECT task_category.rowid::text || '-' || project.rowid::text || '-' || user2project_task.task_id::text AS rowid, task_category.rowid AS category_id, project.rowid AS sub_category_id, user2project_task.task_id AS sub_sub_category_id, user2project_task.dstart, user2project_task.dend, TRUE AS allday FROM task_category, activity_report_source, project, project_task, user2project_task WHERE task_category.rowid = activity_report_source.task_category_id AND activity_report_source.ident = 'haussmann' AND task_category.mioga_id = ? AND project.rowid = project_task.project_id AND project_task.rowid = user2project_task.task_id AND user2project_task.user_id = ? AND (user2project_task.dstart, user2project_task.dend) OVERLAPS (?::timestamp, ?::timestamp);", [$context->GetConfig ()->GetMiogaId (), $self->{user_id}, "$values->{start_year}-$values->{start_month}-$values->{start_day}", "$values->{end_year}-$values->{end_month}-$values->{end_day}"]);
		my $cal = {
			type => 'owner',
			calendar => __('Project tasks'),
			calendar_id => 0,
			events =>  []
		};
		for my $event (@$project_tasks) {
#			if (!grep { $_ eq $cal->{calendar_id} . '-' . $event->{rowid} } @{$session->{ImportReferences}}) {
				push (@{$cal->{events}}, $event);
#			}
		}
		push (@{$data->{calendars}}, $cal);

		$data->{success} = 'boolean::true';
		delete ($data->{message});
	}
	else {
		print STDERR '[Mioga2::Deming::GetCalendarEvents] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	print STDERR "[Mioga2::Deming::GetCalendarEvents] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetCalendarEvents  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut
# ============================================================================


#===============================================================================

=head2 ClearExternalLink

Clear any existing link from entry to external activity

=head3 Incoming Arguments

=over

=item I<$entry_id>: The entry rowid

=back

=head3 Return value

=over

None.

=back

=cut

#===============================================================================
sub ClearExternalLink {
	my ($self, $entry_id) = @_;

	# Clear any existing external activity link
	my %external_sources_conf;
	my $confdir = $self->{config}->GetInstallPath () . '/conf';
	if (-e "$confdir/deming_conf.xml") {
		# External activities may be used
		print STDERR "[Mioga2::Deming::ClearExternalLink] Loading external sources from $confdir/deming_conf.xml\n" if ($debug);
		%external_sources_conf = %{$self->ParseExternalConf ("$confdir/deming_conf.xml")};
		for my $src (keys (%external_sources_conf)) {
			$self->{db}->ExecSQL ("DELETE FROM activity_report_to_$src WHERE report_id = ?", [$entry_id]);
		}
	}
}	# ----------  end of subroutine ClearExternalLink  ----------


#===============================================================================

=head2 ParseExternalConf

Parse external sources configuration (web/conf/deming_conf.xml)

=head3 Incoming Arguments

=over

=item I<$path>: Path to configuration file

=back

=head3 Return value

=over

The sources description:
	{
		<source_ident> => {
				name: '...',	# The source name (for user display)
				label: '...',	# The source element label (for user display)
				key: '...',		# The source element key (usually a rowid, for relationship between tables)
				table: '...',	# The table to get source from
				filter: '...',	# The filter SQL taking mioga_id as argument
				items: {
					label: '...',	# The sub-source element label (for user display)
					key: '...',		# The sub-source element key (usually a rowid, for relationship between tables)
					table: '...',	# The table to get sub-source from
				}
			},
			...
	}

=back

=cut

#===============================================================================
sub ParseExternalConf {
	my ($self, $path) = @_;

	# Parse XML
	my $xs = new Mioga2::XML::Simple();
	my $conf = $xs->XMLin($path);
	if (!defined($conf)) {
		throw Mioga2::Exception::Simple("Mioga2::Deming::ParseExternalConf", "Cannot parse config file : <$path>");
	}

	# Extract data
	my $sources = { };
	for my $source (@{ac_ForceArray ($conf->{Source})}) {
		$sources->{$source->{ident}} = $source;
	}

	print STDERR "[Mioga2::Deming::ParseExternalConf] Sources: " . Dumper ($sources) if ($debug);

	return ($sources);
}	# ----------  end of subroutine ParseExternalConf  ----------


#===============================================================================

=head2 InitApp

Initialize application

=cut

#===============================================================================
sub InitApp {
	my ($self, $context) = @_;

	$self->{user} = $context->GetGroup ();
	$self->{user_id} = $self->{user}->Get ('rowid');
	$self->{db} = $context->GetConfig ()->GetDBObject ();
}	# ----------  end of subroutine InitApp  ----------



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2011, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
