# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Archive.pm: Magellan management class

=head1 DESCRIPTION

This module is useful for archive management

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
package Mioga2::Magellan::Archive;
use strict;

use Locale::TextDomain::UTF8 'filemanager_archive';

use POSIX qw( getcwd );
use Mioga2::tools::APIAuthz;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;
use Mioga2::Exception::Application;
use Text::Iconv;
use Error qw(:try);
use File::Path;
use Data::Dumper;
use File::Basename;
use Archive::Zip;
use Encode::Detect::Detector;

require Mioga2::Magellan::DAV;

my $debug = 0;

my($singleton) = {};
bless($singleton);

my($executables) = {};

# ============================================================================

=head2 new()

	new Mioga2::Magellan::Archive ()

	creates object useful for archive manipulation
=cut

# ============================================================================
sub new
{
    $singleton
}

# ============================================================================

=head2 Type ($filename)

	return the type of the given file or undef

	types : zip
=cut

# ============================================================================
sub Type
{
    my($self, $filename) = @_;
    my($unzipexe);

    my $check_name = lc($filename);

    if (!($check_name =~ /zip$/))
    {
    	return undef;
    }

    local *F;

    if(open(F, $filename))
    {
        my($buf);
        my($gzipped) = 0;

        sysread(F, $buf, 4);

        #Test for Zip archive
        if ( ($buf eq "PK\003\004") || ($buf eq "PK00") )
        {
			close(F);
			return "zip";
        }

        close(F);
    }
    else
    {
        warn "Cannot open '$filename': $!";
    }

    return undef;
}

# ============================================================================

=head2 Content ($archname)

=cut

# ============================================================================
sub Content
{
    my($self, $archname) = @_;
    my($archtype) = $self->Type($archname);
    my($list) = [];
    my($nbdirs, $nbfiles);
    my($cmd, $re);
    local *PRG;

    if($archtype eq 'zip')
    {
        my($unzipexe) = $self->_LocateExec('zipinfo');

        if(not defined($unzipexe))
        {
            warn "Cannot locate unzip executable";
            return undef;
        }

        $cmd = "$unzipexe -s \"$archname\" |";
        $re = '^(.).*\d{2}:\d{2}\s(.*)$';
    }
    else
    {
        print STDERR "Archive file '$archname': unkown or unhandled archive format";
        return undef;
    }

    if(not open(PRG, $cmd))
    {
        print STDERR "Cannot list archive file '$archname': $!";
        return undef;
    }

    my $conv  = Text::Iconv->new('utf8', 'utf8');
    my %folders;
    while(my $file = <PRG>) {
      chomp($file);
      next if $file =~ /^Archive:\s*/ or $file =~ /^\d+\s*files/;
      
      if($file =~ /$re/) {
        my $type                = $1;
        my $filename            = $2;
        my $converted_filename  = $conv->convert($filename);
        utf8::encode($filename) unless $converted_filename;
        
        $type     = ($type eq 'd') ? 'directory' : 'file';
        my @arbo  = split "/", $filename;
        while (pop @arbo) {
          $folders{join('/', @arbo)}++ if @arbo;
        }
        
        push(@$list, {name => $filename, type => $type});
        ($filename =~ /\/$/) ? ++$nbdirs : ++$nbfiles;
      }
    }
    close(PRG);
    
    foreach my $key (keys %folders) {
      my $cleankey  = quotemeta($key);
      unless (grep { $_->{name} =~ /^\/?$cleankey\/?$/ } @$list) {
        unshift @$list, {name => "$key/", type => 'directory'};
      }
    }
    @$list = sort { length($a->{name}) <=> length($b->{name}) } @$list;
  
    return ($list, $nbdirs, $nbfiles) if wantarray();
    $list
}

# ============================================================================

=head2 IsA ($archname)

	return the arch type

=cut

# ============================================================================
sub IsA
{
    my($self, $archname) = @_;

    return defined($self->Type($archname));
}

# ============================================================================

=head2 Build ($archpath, $tmppath)

	build archive for directory $archpath into tmp dir $tmppath
	return the arch name

=cut

# ============================================================================
sub Build
{
    my($self, $archpath, $tmppath) = @_;
    my($tarexe, $archname, $cwd, $output);

    if(not -d $archpath)
    {
        die "Archive directory '$archpath' not found";
    }

    if(not -d $tmppath)
    {
        die "Temporary directory '$tmppath' not found";
    }

    $tarexe = $self->_LocateExec('tar');

    if(not defined($tarexe))
    {
        warn "Cannot locate tar executable";
        return undef;
    }

    $archpath =~ /\/([^\/]+)\/?$/;

    $archname =  $1 . '.tar.gz';

    $cwd = getcwd();

    chdir($archpath);

    $archname = "$tmppath/$archname";

    $output = `$tarexe czf '$archname' .`;
     if($?)
     {
         chdir($cwd);
         die "Cannot create '$archname' archive: $?\n$output";
     }

     chdir($cwd);

    $archname
}

# ============================================================================

=head2 Extract ($archname, $destpath, $delarch)

	Extract the archive $archname to dir $destpath

	Delete the archive if $delarch = 1

=cut

# ============================================================================
sub Extract
{
    my($self, $config, $archname, $destpath, $delarch, $context) = @_;
    my($archtype) = $self->Type($archname);
    my $user_id     = $context->GetUser->GetRowid;
    my $user_ident  = $context->GetUser->GetIdent;
    my $session     = $context->GetSession;
	my @errors = ();

	my $dbh = $config->GetDBH();

    if($archtype ne 'zip') {
      throw Mioga2::Exception::Application("Mioga2::Magellan::Archive::Extract", __x("Archive file '{filename}': unkown or unhandled archive format", filename => $archname));
    }

	my $tmp_dir  = $config->GetTmpDir . "/archive-" . time . "-$user_id";
	unless (mkdir($tmp_dir)) {
	  throw Mioga2::Exception::Application("Mioga2::Magellan::Archive::Extract", __x("Can't create temp dir {directory}!", directory => $tmp_dir));
	}

	my @files;
	my %folders;

	# Extract archive contents
	my $zip = Archive::Zip->new ();
	$zip->read ($archname);

	# Bug #1351 -- Guess charset used inside archive and convert filenames if non-UTF-8
	# 	If detected charset is windows-1252, then change it to cp850 as it is the one used by Windows
	# 	(both look the same for Encode::Detect::Detector which assumes cp850 to be deprecated and returns windows-1252)
	my @member_names = $zip->memberNames ();
	my $charset = detect (join ('', @member_names));
	$charset = 'cp850' if ($charset eq 'windows-1252');
	my @unicode_names;
	if ($charset eq 'UTF-8') {
		# Mass-extract as charset is OK
		$zip->extractTree ('', "$tmp_dir/");
		@unicode_names = $zip->memberNames ();
	}
	else {
		# Extract each member individually, allowing to transcode filename to UTF8
		my $conv  = Text::Iconv->new($charset, 'utf8');
		for my $member (@member_names) {
			my $converted = $conv->convert ($member);
			$zip->extractMember ($member, $tmp_dir . '/' . $converted);
			push (@unicode_names, $converted);
		}
	}

	for (@unicode_names) {
		my @arbo  = split "/", $_;
		while (pop @arbo) {
		  $folders{join('/', @arbo)}++ if @arbo;
		}
		$_ =~ s/\/$//;
		push (@files, $_);
	}
	
	foreach my $key (keys %folders) {
	  my $cleankey  = quotemeta($key);
    unless (grep { $_ =~ /^\/?$cleankey\/?$/ } @files) {
      unshift @files, "$key/";
    }
  }
	
  my $total_size  = 0;
	my @files_infos;
	foreach my $file (@files) {
	  next unless -e "$tmp_dir/$file";
	  my $size     = -s "$tmp_dir/$file";
    $total_size += $size;
    my $mime     = (-d "$tmp_dir/$file") ? 'directory' : "application/octet-stream";
    push @files_infos, {name => $file, size => $size, mimetype => $mime};
	}
	
	my @sorted_files = sort { length($a->{name}) <=> length($b->{name}) } @files_infos;
  $destpath = $config->GetDAVProtocol . "://" . $config->GetDAVHost . ':' . $config->GetDAVPort . $destpath;
  
  @{$session->{fm_files}}     = @sorted_files;
  $session->{fm_total_size}   = $total_size;
  $session->{fm_current_size} = 0;
  
  my $file_count = 0;
	foreach my $file (@sorted_files) {
	  my $filepath = "$tmp_dir/".$file->{name};
	  $session->{fm_current_file}    = $file_count;
	  $session->{fm_current_filename} = $file->{name};
	  
    if ($file->{mimetype} eq 'directory') {
      warn "creating directory $destpath/".$file->{name}."\n" if $debug;
      unless (Mioga2::Magellan->FileExists($context, "$destpath/".$file->{name})) {
        if (my $code = Mioga2::Magellan::DAV::MakeCollection($context->GetUserAuth (), "$destpath/".$file->{name}) ne 'OK') {
          warn "Can't create directory $destpath/".$file->{name}.", aborting...\n";
          warn "Mioga2::Magellan::DAV::MakeCollection error: $code\n";
		  push (@errors, $file->{name});
        }
      }
      warn "done!\n" if $debug;
    }
    else {
      my $fh;
      my $size  = -s $filepath;
      unless (open($fh, $filepath)) {
        warn "Can't open file $filepath for reading!\n";
        warn "Error was: $!\n";
		push (@errors, $file->{name});
      }
      warn "puting file $destpath/".$file->{name}."\n" if $debug;
      if (my $code = Mioga2::Magellan::DAV::PutURIForFH($context->GetUserAuth (), $fh, "$destpath/".$file->{name}, $file->{mimetype}, $file->{size})) {
        close($fh);
        warn "Can't create file $destpath/".$file->{name}.", aborting...\n";
        warn "Mioga2::Magellan::PutURIForFH error: $code\n";
		push (@errors, $file->{name});
      }
      warn "done!\n" if $debug;
      close($fh);
    }
    $file_count++;
    $session->{fm_current_size}  += $file->{size};
    $context->{raw_session}->save;
	}

  chdir('..');
  rmtree($tmp_dir);
  if ($delarch) {
    Mioga2::Magellan::DAV::DeleteURI($context->GetUserAuth (),
                                   "$destpath/".basename($archname));
  }

  return (@errors);
}
# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

sub _LocateExec
{
    my($self, $exename) = @_;
    my(@pathlist);

    if(exists($executables->{$exename}))
    {
        return $executables->{$exename};
    }

    if(exists($ENV{PATH}))
    {
        @pathlist = split(/[:;]/, $ENV{PATH});
    }
    else
    {
        @pathlist = ('/bin', '/usr/bin', '/usr/local/bin');
    }

    for my $path (@pathlist)
    {
        if(-x "$path/$exename")
        {
            $executables->{$exename} = "$path/$exename";
            return $executables->{$exename};
        }
    }
    undef
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Magellan

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
