# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Magellan/DAV.pm: Magellan part for DAV manipulation

=head1 DESCRIPTION

This module is useful for file management through DAV

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Magellan::DAV;

use strict;
use warnings;
use open ':encoding(utf8)';
use LWP::UserAgent;
use Error qw(:try);
use Mioga2::tools::date_utils;
use Mioga2::tools::string_utils;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::database;
use MIME::Base64;
use Data::Dumper;
use File::Basename;
use Mioga2::XML::Simple;
use Time::HiRes qw(gettimeofday tv_interval usleep);
use Mioga2::XML::Simple;
use Mioga2::Constants;

my $debug = 0;

my $MULTI_STATUS	= 207;	# code DAV for the Multi-status response
my $CREATED		= 201;	# code DAV for the created mkcol response
my $NO_CONTENT		= 204;	# 

# ============================================================================

=head2 ExecuteRequest ($method, $url, $content, $header)

	ExecuteRequest executes a request from the arguments 
	and returns a response object.
	Arguments :
		$method
		$url
		$content (optionnal)
		$header (optionnal)

=cut

# ============================================================================
sub ExecuteRequest
{
	my ($method, $url, $content, $header) = @_;
	my $user_agent;
	my $request;
	
	# Escape URL for request
	$url = st_URIEscape($url);

	# Make contents UTF8
	utf8::encode ($content);

	$user_agent = new LWP::UserAgent(keep_alive => 0);
	$request = new HTTP::Request($method, $url);

	if (defined($content))
	{
		$request->content_type('text/xml; charset="utf-8"');
		$request->content_length(length($content));
		$request->content($content);
		if (defined($header))
		{
			foreach my $var (keys(%$header))
			{
				$request->push_header($var => $header->{$var});
			}
		}
	}

	my $response = $user_agent->request($request);

	if($response->code == 301) {
		return ExecuteRequest($method, st_URIUnescape($response->header('Location')), $content, $header);
	}

	if ($response->code >= 500) {
		warn "Mioga2::Magellan::ExecuteRequest Failed: " . $response->content;
	}

	return  $response;
}

# ============================================================================

=head2 ExecuteRequestForFH ($method, $url, $fh, $header)

	ExecuteRequest executes a request from the arguments 
	and returns a response object.
	Arguments :
		$method
		$url
		$fh filehandle
		$header (optionnal)

=cut

# ============================================================================
sub ExecuteRequestForFH
{
	my ($method, $url, $fh, $header) = @_;
	my $user_agent;
	my $request;
	print STDERR "ExecuteRequestForFH()\n" if($debug);
	
	# Escape URL for request
	$url = st_URIEscape($url);

	$user_agent = new LWP::UserAgent(keep_alive => 0);

	#---------------------------------------------------
	# Read HTTP::Request::Common to see how to use the dynamic content for http_request
	# We use a closure to read data ifrom the filehandle gived by Apache::Request and put them
	# to the DAV URL
	#-----------------------------------------------------
	local $HTTP::Request::Common::DYNAMIC_FILE_UPLOAD = 1;
	$request = new HTTP::Request($method, $url);
	$request->content_type('text/xml; charset="utf-8"');
	$request->content(
			sub {
				my $data;
				my $l = sysread($fh, $data, 100000);
				if (defined($l)) {
					if ($l > 0) {
					  return $data;
					}
					else {
						return;
					}
				}
				else {
					print STDERR "ExecuteRequestForFH() read error on filehandle\n";
				}
			}
		);

	if (defined($header))
	{
		foreach my $var (keys(%$header))
		{
			$request->push_header($var => $header->{$var});
		}
	}

	my $response = $user_agent->request($request);

	return  $response;
}

# ============================================================================

=head2 GetResource ($user_auth, $url)

	return mime type and content or undef if something goes wrong

=cut

# ============================================================================
sub GetResource
{
	my ($user_auth, $url) = @_;
	my $response;
	my $content = "";
	my $content_type;

	my $header = { Timeout => 'Second-86400' };
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}

	$response = ExecuteRequest('GET', $url, $content, $header);

	$content_type = join(';', $response->content_type());
	if (Mioga2::DAVFS::ResponseIsSuccess ($response))
	{
		return ($content_type, $response->content());
	}
	return undef;
}

# ============================================================================

=head2 LockResource ($user_auth, $url)

	lock the given resource

=cut

# ============================================================================
sub LockResource
{
	my ($context, $user_auth, $url) = @_;

	my $response;
	my $userident = $context->GetUser ()->GetIdent ();
	my $content = '<?xml version="1.0" encoding="utf-8" ?>' .
			'<lockinfo xmlns="DAV:">' .
			"<lockscope><exclusive/></lockscope>" .
			'<locktype><write/></locktype>' .
			"<owner>$userident</owner>" .
			'</lockinfo>' . "\n";

	my $content_type;

	my $header = { Timeout => 'Second-86400' };
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}

	$response = ExecuteRequest('LOCK', $url, $content, $header);
	$content_type = join(';', $response->content_type());
	print STDERR "LockResource() response = ".Dumper($response)."\n" if ($debug);

	my $res = undef;
	if (!Mioga2::DAVFS::ResponseIsSuccess ($response)) {
		print STDERR "[Mioga2::Magellan::DAV::LockResource] Request failed: " . $response->code () . " " . $response->message () . "\n";
		$res = $response->code ();
	}
	return ($res);
}

# ============================================================================

=head2 UnLockResource ($user_auth, $url)

	lock the given resource

=cut

# ============================================================================
sub UnLockResource
{
	my ($user_auth, $url) = @_;
	my $response;
	my $content = '';

	my($lock_token) = DiscoverLock($user_auth, $url);
	print STDERR "UnLockResource() lock_token = $lock_token\n" if ($debug);

	my $header = { 'Lock-token' => "<$lock_token>",
					Timeout => 'Second-86400' };
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}

	$response = ExecuteRequest('UNLOCK', $url, $content, $header);
	print STDERR "UnLockResource() response = ".Dumper($response)."\n" if ($debug);

	my $res = undef;
	if (!Mioga2::DAVFS::ResponseIsSuccess ($response)) {
		print STDERR "[Mioga2::Magellan::DAV::UnLockResource] Request failed: " . $response->code () . " " . $response->message () . "\n";
		$res = $response->code ();
	}
	return ($res);
}

#========================================================
# _Discover DAV Lock on a resource by sending an PROPFIND
# request to mod_dav to local server
#
# input: user_auth, URI
# output : opaque-locktoken
#
# die "rc [message]" if error
#========================================================
sub DiscoverLock {
	my($user_auth, $url) = @_;
	my($rep, $content);
	my(@headers);
	my($agent) = new LWP::UserAgent();

	$content = '<?xml version="1.0" encoding="utf-8" ?>' .
	       '<propfind xmlns="DAV:"><prop>' .
	       '<lockdiscovery xmlns="DAV:"/>' .
	       '</prop></propfind>';

	my $header = { Timeout => 'Second-86400' };
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}

	$rep = ExecuteRequest('PROPFIND', $url, $content, $header);


	if(not $rep->is_success)
	{
		die $rep->code() . " [" . $rep->message . "]";
	}

	$content = $rep->content();

	if($content =~ /<((?:[^:]+:)?[^>]+)>(opaquelocktoken:[^<]+)<\/\1>/)
	{
		return $2;
	}

	return undef
}

# ============================================================================

=head2 GetCollection ($context, $user_auth, $base_url, $uri, @extra_props)

	GetCollection return a list reference of the directory content
	if something goes wrong, it returns undef

	@extra_props is a list of custom properties to retrieve.

=cut

# ============================================================================
sub GetCollection
{
	my ($context, $user_auth, $base_url, $uri, @extra_props) = @_;
	print STDERR "Magellan::DAV::GetCollection ($uri)\n" if ($debug);
	my $response;
	my $config = $context->GetConfig();
	my $user;
	$user = $context->GetUser() unless ($context->IsPublic ());
	my $group = $context->GetGroup();

	my $private_dav_uri = $config->GetBasePath().'/'.$config->GetMiogaIdent().$config->GetPrivatePath();
	my $public_dav_uri  = $config->GetBasePath().'/'.$config->GetMiogaIdent().$config->GetPublicPath();

	my $access_right;
	my $inconsistent = 'false';

	$uri =~ s/\/+$//;
	my $url = $base_url.$uri;
	print STDERR "url = $url\n" if ($debug);

	my $content = '<?xml version="1.0" encoding="utf-8" ?>' .
				  '<D:propfind xmlns:D="DAV:" xmlns:P="http://webdav.org/cadaver/custom-properties/">' .
				  '<D:prop>' .
				  '<D:getcontentlength/>' .
				  '<D:getlastmodified/>' .
				  '<D:resourcetype/>' .
				  '<D:lockdiscovery xmlns="DAV:"/>';
	$content .= "<P:$_/>" foreach (@extra_props);
	$content .=	  '</D:prop>' .
				  '</D:propfind>';

	my $header = { Timeout => 'Second-86400',
					Depth => 1};
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}

	$response = ExecuteRequest('PROPFIND', $url, $content, $header);
	if (Mioga2::DAVFS::ResponseIsSuccess ($response))
	{
		my @file_list;

		my $xml_response = $response->content();
		print STDERR "xml_response = $xml_response\n" if ($debug > 1);

		my $xml = new Mioga2::XML::Simple(forcearray => 1);
		my $hash_response  = $xml->XMLin($xml_response);

		print STDERR Dumper($hash_response) if ($debug > 1);

		foreach my $n (@{$hash_response->{"D:response"}})
		{
			# Look for properties namespace
 			my $props_ns;

			foreach my $key (keys %$n) {
				next if ref($n->{$key}) ne '';
				next if $n->{$key} ne 'DAV:';
				next if $key !~ /^lp/;				

				$props_ns = $key;
			}
			
			# Look for custom properties namespace
 			my $custom_props_ns;

			foreach my $key (keys %$n) {
				next if ref($n->{$key}) ne '';
				next if $n->{$key} ne MIOGA_DAV_PROP_NS_URL;

				$custom_props_ns = $key;
			}
			
			my $name = $n->{"D:href"}->[0];
			$name =~ s/\/$//;

			# Name is given vy DAV response and is escaped
			$name =~ s/&/%26/g;
			$name =~ s/=/%3D/g;
			# Name is escaped but has UTF-8 flag on, which is a nonsense and leads to double-encoding issues
			utf8::downgrade ($name);
			my $unesc_name = st_URIUnescape($name);

			next if (lc($unesc_name) eq lc($uri)); # removing . dir

			warn "name : $name, uri : $uri, unesc_name : $unesc_name" if ($debug > 0);
			if (defined($name))
			{
				my $result = {};

				# Set 'inconsistent' flag if HTTP code is not 200 for an URI
				my ($http_code) = ($n->{"D:propstat"}->[0]->{"D:status"}->[0] =~ m/^[^ ]* ([0-9]+) .*/);
				if ($http_code != 200) {
					$inconsistent = 'true';
				}

				foreach (keys %{$n->{"D:propstat"}->[0]->{"D:prop"}->[0]})
				{
					if ($custom_props_ns && ($_ =~ /$custom_props_ns:(.*)/))
					{
						my $prop = $1;
						$result->{$prop} = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$custom_props_ns:$prop"}->[0];
					}
				}

				$result->{name} = $unesc_name;
				$result->{name_esc} = $name;
				my $basename = basename ($unesc_name);

				# short name is the name displayed in web page.
				$result->{short_name} = $basename;

                # short name has to be allowed to be break (ie, use in table)
                #$result->{short_name} =~ s/ /&\#160;/g;
				
				$result->{length} = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:getcontentlength"}->[0];

				my $date = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:getlastmodified"}->[0];
				if(ref($date) ne '') {
					$date = $date->{content}->[0];
				}

				$result->{date} = du_EnGMTToISO($date);

				my $type;
				if(exists $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:resourcetype"}) {
					$type = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:resourcetype"}->[0];
				}
				else {
					$type = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:resourcetype"}->[0];
				}

				if (exists $type->{"D:collection"})
				{
					$result->{type} = 'collection';
					$result->{locked} = undef;
				}
				else
				{
					$result->{type} = 'file';
					if (exists($n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:lockdiscovery"}->[0]->{"D:activelock"}))
					{
						$result->{locked} = 1;
						if (exists($n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:lockdiscovery"}->[0]->{"D:activelock"}->[0]->{"ns0:owner"})) {
							my $owner = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:lockdiscovery"}->[0]->{"D:activelock"}->[0]->{"ns0:owner"}[0]->{"content"}[0];

							try {
								my $userobj = new Mioga2::UserList ($config, { attributes => { login => $owner } });
								$result->{lock_owner} = $userobj->Get ('firstname') . ' ' . $userobj->Get ('lastname');

								# Check if user has the right to unlock (he is either the lock owner or the group animator)
								my $uriobj = new Mioga2::URI($config, uri => $result->{name});
								my $groupobj = $uriobj->GetGroup;
								my $mygroup;
								if (defined ($groupobj)) {
									$mygroup = $groupobj;
								}
								else {
									$mygroup = $group;
								}
								my $anim = $group->GetAnimator ();

								if ($user && ($owner eq $user->GetIdent ())) {
									$result->{user_is_lock_owner} = 1;
								}

								if ($user && ($owner eq $user->GetIdent() || $user->GetIdent() eq $anim->GetIdent())) {
									$result->{unlockable} = 1;
								}
								else {
									$result->{unlockable} = 0;
								}
							}
							catch Mioga2::Exception::User with {
								# Resource is locked and owner could not be translated to a Mioga2::Old::User object.
								# Bug #1165: Opening a file with MS-Word from a WebFolder mount locks the file with MS-Windows session user (which is not necessary a Mioga2 user)
								$result->{unlockable} = 0;
							};
						}
					}
					else
					{
						$result->{locked} = 0;
					}
				}

				push (@file_list, $result);
			}
		}

		if ($context->IsPublic ()) {
			# In public mode, no need to check for permissions on files, no need to ensure current directory is public as WebDAV request was issued without session key and private directory index has been dismissed
			push (@file_list, $inconsistent);
			return \@file_list;
		}

		for my $file (@file_list) {
			$file->{access_right} = "rw" if ($file->{accesskey} == AUTHZ_WRITE);
			$file->{access_right} = "r" if ($file->{accesskey} == AUTHZ_READ);
			$file->{access_right} = "" if ($file->{accesskey} == AUTHZ_NONE);
		}

		push (@file_list, $inconsistent);
		return \@file_list;
	}
	else
	{
		my  $code = $response->code();
		#warn ("GetCollection() $url get $code status");
	}
	return undef;
}

# ============================================================================

=head2 GetProperties($context, $user_auth, $url) 

	GetProperties return a hashref on the file
	if something goes wrong, it returns undef

=cut

# ============================================================================
sub GetProperties
{
	my ($context, $user_auth, $url) = @_;
	my $config = $context->GetConfig();
	my $user = $context->GetUser();
	my $group = $context->GetGroup();
	my $response;
	my $content = '<?xml version="1.0" encoding="utf-8" ?>' .
					'<propfind xmlns="DAV:">' .
					'<allprop/>' .
					'</propfind>';

	my $header = { Timeout => 'Second-86400',
					Depth => 0};
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}

	$response = ExecuteRequest('PROPFIND', $url, $content, $header);

	if (Mioga2::DAVFS::ResponseIsSuccess ($response))
	{
		my $result = { };
		my $xml_response = $response->content();
		print STDERR "GetProperties xml_response = $xml_response\n" if ($debug >0);

		my $xml	= new Mioga2::XML::Simple(forcearray => 1);
		my $hash_response  = $xml->XMLin($xml_response);

		my $name = $hash_response->{"D:response"}->[0]->{"D:href"}->[0];
		if (defined($name))
		{
			$name =~ s/\/$//;
			$name = st_URIUnescape($name);

			#
			# testing access right
			#

			my $curent_uri   = new Mioga2::URI($config, uri => $name);
			my $access_right = AuthzTestRightAccessForURI($config, $curent_uri, $user, $group);
			my $db_infos     = SelectSingle($config->GetDBH, "SELECT COUNT(uri_history.*) 
			                                                   FROM uri_history, m_uri 
			                                                   WHERE uri_history.uri_id=m_uri.rowid 
			                                                   AND m_uri.uri='".st_FormatPostgreSQLString($name)."'");
      $result->{history}  = undef;
      $result->{history}  = $db_infos->{count} if $group->HasHistory && $db_infos->{count};
      
			if (defined ($access_right) && $access_right == 2)
			{
				$result->{access_right} = 'rw';
			}
			elsif (defined ($access_right) && $access_right == 1)
			{
				$result->{access_right} = 'r';
			}
			else
			{
				$result->{access_right} = '';
			}

			if (exists($hash_response->{"D:response"}->[0]->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:lockdiscovery"}->[0]->{"D:activelock"}))
			{
				$result->{locked} = 1;
				$result->{locktoken} = $hash_response->{"D:response"}->[0]->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:lockdiscovery"}->[0]->{"D:activelock"}->[0]->{"D:locktoken"}->[0]->{"D:href"};
				$result->{owner} = $hash_response->{"D:response"}->[0]->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:lockdiscovery"}->[0]->{"D:activelock"}->[0]->{"ns0:owner"}->[0]->{"content"}[0];
			}
			else
			{
				$result->{locked} = 0;
			}


			my $response = $hash_response->{"D:response"}->[0];

			# Look for properties namespace
			my $props_ns;
			foreach my $key (keys %$response) {
				next if ref($response->{$key}) ne '';
				next if $response->{$key} ne 'DAV:';
				next if $key !~ /^lp/;				

				$props_ns = $key;
			}

			$result->{name} = $name;
			
			$result->{length} = $response->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:getcontentlength"}->[0];

			my $modification_date = $response->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:getlastmodified"}->[0];
			if(ref($modification_date) ne '') {
				$modification_date = $modification_date->{content}->[0];
			}


			$result->{modification_date} = du_EnGMTToISO($modification_date);

			my $creation_date = $response->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:creationdate"}->[0];
			if(ref($creation_date) ne '') {
				$creation_date = $creation_date->{content}->[0];
			}

			$result->{creation_date} = du_CreationTimeToISO($creation_date);

			my $type;
			if(exists $response->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:resourcetype"}->[0]) {
				$type = $response->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:resourcetype"}->[0];
			}
			else {
				$type = $response->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:resourcetype"}->[0];
			}


			if (exists($type->{"D:collection"}))
			{
				$result->{type} = 'collection';
			}
			else
			{
				$result->{type} = 'file';
				$result->{mime} = $response->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:getcontenttype"}->[0];
			}
		}
		return $result;
	}
	else
	{
		my  $code = $response->code();
	}
	return undef;
}

# ============================================================================

=head2 GetLockHeader($context, $user_auth, $url) 

	return string describing lock headers for the given $url or undef if
	there is no lock

=cut

# ============================================================================
sub GetLockHeader
{
	my ($context, $user_auth, $url) = @_;

	my $properties = GetProperties($context, $user_auth, $url);
	my $string = "";

	if (defined $properties)
	{
		if ($properties->{locked} == 1)
		{
			$string = '(<';
			$string .= join ('>) (<', @{$properties->{locktoken}});
			#$string .= '>) Content-Type: text/xml; charset="utf-8"';
			$string .= '>)';
		}
		else
		{
			return undef;
		}
	}
	else
	{
		return undef;
	}
}

# ============================================================================

=head2

	MakeResource ($user_auth, $uri) create a resource on the DAV server
	if something goes wrong, it returns DAV error code else return OK
	
=cut

# ============================================================================
sub MakeResource
{
	my ($user_auth, $uri) = @_;
	my $response;
	my $content = "";

	my $header = { Timeout => 'Second-86400' };
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}

	$response = ExecuteRequest('PUT', $uri, $content, $header);
	
	if (not Mioga2::DAVFS::ResponseIsSuccess ($response)) {
		my  $code = $response->code();
		warn("MakeResource() $uri get $code status");
		return $code;
	}
	return "OK";
}

# ============================================================================

=head2

	MakeCollection ($user_auth, $uri) create a collection on the DAV server
	if something goes wrong, it returns DAV error code else return OK

=cut

# ============================================================================
sub MakeCollection
{
	my ($user_auth, $uri) = @_;
	my $response;
	my $content = "";

	my $header = { Timeout => 'Second-86400' };
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}
 
	$response = ExecuteRequest('MKCOL', $uri, $content, $header);
	
	if (not Mioga2::DAVFS::ResponseIsSuccess ($response)) {
		my  $code = $response->code();
		warn("MakeCollection() $uri get $code status");
		return $code;
	}
	return "OK";
}

# ============================================================================

=head2

	DeleteURI ($user_auth, $url) deletes the URI gived in argument.
	if something goes wrong, it returns undef

=cut

# ============================================================================
sub DeleteURI
{
	my ($user_auth, $url) = @_;
	my $response;
	my $content = "";

	my $header = { Timeout => 'Second-86400' };
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}

	$response = ExecuteRequest('DELETE', $url, $content, $header);
	if (Mioga2::DAVFS::ResponseIsSuccess ($response))
	{
		return undef
	}
	else
	{
		my $code = $response->code ();
		my $msg = $response->message ();
		if ($code == $MULTI_STATUS) {
			my $msresponse = Mioga2::DAVFS::ParseMultiStatus ($response);
			$code = $msresponse->{status}->[0]->{code};
			$msg = $msresponse->{status}->[0]->{message};
		}
		warn "DeleteURI() $url get $code $msg status";
		return $code;
	}
}

# ============================================================================

=head2

	MoveURI ($user_auth, $overwrite, $srcURL, $dstURL) moves (or rename) the src argument into the dst argument
	it returns undef or an error message
	
	$overwrite may be 'T' or 'F'

=cut

# ============================================================================
sub MoveURI
{
	my ($user_auth, $overwrite, $srcURL, $dstURL) = @_;
	my $message;
	my $response;
	my $content = "";

	# Bug #1449 Prevent a file from overwriting its parent directory
	my $escaped = quotemeta ($dstURL);
	if ($srcURL =~ /^$escaped[\/]?[^\/]+$/) {
		# Destination is direct parent of source
		return (400);
	}

	my $header = { Timeout => 'Second-86400',
					Overwrite => $overwrite,
					Destination => st_URIEscape($dstURL)};
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}

	$response = ExecuteRequest('MOVE', $srcURL, $content, $header);
	my $code = undef;
	if (!Mioga2::DAVFS::ResponseIsSuccess ($response)) {
		$code = $response->code();
		$message = "MoveURI() $srcURL to $dstURL get $code status";
		warn $message;
	}
	return $code;
}


# ============================================================================

=head2

	CopyURI ($user_auth, $overwrite, $srcURL, $dstURL) copies the src argument into the dst argument
	it returns undef or an error message

	$overwrite may be 'T' or 'F'

=cut

# ============================================================================
sub CopyURI
{
	my ($user_auth, $overwrite, $srcURL, $dstURL) = @_;
	my $message;
	my $response;
	my $content = "";

	warn ("CopyURI : $srcURL -> $dstURL") if ($debug > 0);

	my $header = { Timeout => 'Second-86400',
					Overwrite => $overwrite,
					Destination => st_URIEscape($dstURL)};
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}
	
	$response = ExecuteRequest('COPY', $srcURL, $content, $header);

    my $code = undef;
	if (not Mioga2::DAVFS::ResponseIsSuccess ($response)) {
		$code = $response->code();
		$message = "CopyURI() $srcURL to $dstURL get $code status";
	}
	return $code;
}

# ============================================================================

=head2

	PutURI ($user_auth, $filehandle, $dstURL, $contentType) upload the contents of file to the uri gived in argument
	it returns undef on success  or an error message

=cut

# ============================================================================
sub PutURI
{
	my ($user_auth, $content, $dstURL, $contentType) = @_;
	my $message;
	my $response;
	my $bytesread;
	my $buffer;

	my $header = { Timeout => 'Second-86400',
					'Content-Type' => $contentType };
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}

	$response = ExecuteRequest('PUT', $dstURL, $content, $header);

    my  $code = $response->code();
	if (not Mioga2::DAVFS::ResponseIsSuccess ($response)) {
		$message = "PutURI() $dstURL get $code status";
	}
	return $code;
}


# ============================================================================

=head2

	PutURIForFH ($user_auth, $filehandle, $dstURL, $contentType) upload the contents of file to the uri gived in argument
	it returns undef on success  or an error message

=cut

# ============================================================================
sub PutURIForFH
{
	my ($user_auth, $fh, $dstURL, $contentType, $size) = @_;
	my $message;
	my $response;
	my $bytesread;
	my $buffer;

	my $header = { Timeout => 'Second-86400',
					'Content-Type' => $contentType,
					'Content-Length' => $size };
	if (defined ($user_auth->{token})) {
		$header->{Cookie} = "mioga_session=" . $user_auth->{token};
	}
	else {
		my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
		$header->{Authorization} = "Basic $encoded";
	}

	$response = ExecuteRequestForFH('PUT', $dstURL, $fh, $header);

	my  $code = $response->code();
	if (Mioga2::DAVFS::ResponseIsSuccess ($response)) {
		return (undef);
	}
	else {
		$message = "PutURIForFH() $dstURL get $code status";
		return $code;
	}
}
# ============================================================================

=head2

	EditURI ($content, $dstURL, $contentType, $if_lock_header) upload the contents of file to the uri gived in argument
	if_lock_header is optional more info in GetLockHeader

	it returns undef on success  or an error message

=cut

# ============================================================================
sub EditURI
{
	my ($user_auth, $content, $dstURL, $contentType, $if_lock_header) = @_;
	my $message;
	my $response;
	my $bytesread;
	my $buffer;

	my $header;
	if (defined($if_lock_header))
	{
		$header = { Timeout => 'Second-86400',
						If => $if_lock_header,
						'Content-Type' => $contentType };
		if (defined ($user_auth->{token})) {
			$header->{Cookie} = "mioga_session=" . $user_auth->{token};
		}
		else {
			my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
			$header->{Authorization} = "Basic $encoded";
		}
	}
	else
	{
		$header = { Timeout => 'Second-86400',
						'Content-Type' => $contentType };
		if (defined ($user_auth->{token})) {
			$header->{Cookie} = "mioga_session=" . $user_auth->{token};
		}
		else {
			my $encoded = encode_base64("$user_auth->{ident}:$user_auth->{password}");
			$header->{Authorization} = "Basic $encoded";
		}
	}


	$response = ExecuteRequest('PUT', $dstURL, $content, $header);
	my $code = undef;
	if ((Mioga2::DAVFS::ResponseIsSuccess ($response)) && ($response->code () != $NO_CONTENT)) {
		$code = $response->code();
		$message = "EditURI() $dstURL get $code status";
	}
	return $code;
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Magellan

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
