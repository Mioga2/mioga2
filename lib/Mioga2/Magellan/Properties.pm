#===============================================================================
#
#         FILE:  Properties.pm
#
#  DESCRIPTION:  Properties-related methods for Magellan.
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  05/03/2012 14:42
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME
	
Properties.pm: Tag-related methods for Magellan.

=head1 DESCRIPTION

	This module contains methods to set / get file properties. This module is part of Magellan.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Magellan;

use Mioga2::TagList;

use Data::Dumper;

my $debug = 0;


#===============================================================================

=head2 GetTagList

Get current instance tag list

=over

=item B<arguments:>

=over

None.

=back

=item B<Generated XML>

=over

	<GetTagList>
		<tag>Tag1</tag>
		<tag>Tag2</tag>
		...
	</GetTagList>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetTagList {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Magellan::Properties::GetTagList] Entering." if ($debug);

	my $taglist = Mioga2::TagList->new ($context->GetConfig ());
	my $data = { };
	$data->{tag} = $taglist->GetTags ();

	print STDERR "[Mioga2::Magellan::Properties::GetTagList] Leaving. Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetTagList  ----------


#===============================================================================

=head2 GetProperties

Get properties (tags and description) for a file.

=over

=item B<arguments:>

=over

=item I<$uri> a string containing the URI for which to get properties.

=back

=item B<Generated XML>

=over

	<GetProperties>
		<tag>Tag1</tag>
		<tag>Tag2</tag>
		...
		<description>...</description>
	</GetProperties>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetProperties {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Magellan::Properties::GetProperties] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['uri'], 'disallow_empty', ['location', $context->GetConfig ()] ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		$data = $self->GetPropertiesForURI ($values->{uri});
	}
	else {
		@{$data->{errors}} = map { {$_->[0] => $_->[1]->[0]} } @$errors;
	}

	print STDERR "[Mioga2::Magellan::Properties::GetProperties] Leaving. Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetProperties  ----------


#===============================================================================

=head2 SetProperties

Set properties for one or more files

=over

=item B<arguments:>

=over

=item B<$uri>: The URI(s) of the file(s) to apply the tags to.

=item B<@tags>: The tags to be applied to file(s).

=item B<$description>: The description to be applied to file(s).

=item B<$mode>: A string containing the operation mode which can be: 

=over

=item I<add>: add tags to file from list contents

=item I<replace>: replace current file tags with list contents

=item I<remove>: remove tags from list contents

=back

=back

=item B<Generated XML>

=over

	<SetProperties>
		<errors>
			<error>...</error>
			...
		</errors>
	</SetProperties>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub SetProperties {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Magellan::Properties::SetProperties] Entering. Args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['uri'], 'disallow_empty', ['location', $context->GetConfig ()] ],
			[ ['tags', 'description'], 'allow_empty', 'stripwxps' ],
			[ ['mode'], 'disallow_empty', [ 'match', "\(add|replace|remove\)" ] ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $db = $self->{db};
		try {
			$db->BeginTransaction ();

			# Register free tags, if enabled
			my $taglist = Mioga2::TagList->new ($context->GetConfig ());
			$taglist->RegisterFreeTags (\@{ac_ForceArray ($values->{tags})});

			# Create missing records in table
			$db->ExecSQL ("INSERT INTO uri_data SELECT m_uri.rowid AS uri_id, '' AS tags, '' AS description FROM m_uri WHERE m_uri.rowid NOT IN (SELECT uri_id FROM uri_data) AND m_uri.uri IN (" . join (', ', ('?') x @{ac_ForceArray ($values->{uri})}) . ')', [@{ac_ForceArray ($values->{uri})}]);

			if (st_ArgExists ($context, 'tags')) {
				# Only keep tags that are registered into m_tag
				my @tags = map { $_->{tag} } @{$db->SelectMultiple ('SELECT m_tag.* FROM m_tag, m_tag_catalog, m_mioga WHERE m_tag.catalog_id = m_tag_catalog.rowid AND m_tag_catalog.mioga_id = m_mioga.rowid AND m_mioga.rowid = ? AND m_tag.tag IN (' . join (',', ('?') x @{ac_ForceArray ($values->{tags})}) . ')', [$context->GetConfig ()->GetMiogaId (), @{ac_ForceArray ($values->{tags})}])};

				# Add mode, list of tags is added to existing ones
				if ($values->{mode} eq 'add') {
					for my $tag (@tags) {
						$db->ExecSQL ("UPDATE uri_data SET tags = tags || ';' || ? FROM m_uri WHERE uri_data.tags NOT LIKE ? AND uri_data.tags NOT LIKE ? AND uri_data.uri_id = m_uri.rowid AND m_uri.uri IN (" . join (', ', ('?') x @{ac_ForceArray ($values->{uri})}) . ')', [$tag, "%$tag;%", "%$tag", @{ac_ForceArray ($values->{uri})}]);
					}
				}

				# Replace mode, list of tags is used to replace any existing one
				if ($values->{mode} eq 'replace') {
					$db->ExecSQL ('UPDATE uri_data SET tags = ? FROM m_uri WHERE uri_data.uri_id = m_uri.rowid AND m_uri.uri IN (' . join (', ', ('?') x @{ac_ForceArray ($values->{uri})}) . ')', [join (';', @tags), @{ac_ForceArray ($values->{uri})}]);
				}

				# Remove mode, list of tags is removed from existing ones
				if ($values->{mode} eq 'remove') {
					for my $tag (@tags) {
						$db->ExecSQL ("UPDATE uri_data SET tags = regexp_replace (tags, ?, ';') FROM m_uri WHERE uri_data.uri_id = m_uri.rowid AND m_uri.uri IN (" . join (', ', ('?') x @{ac_ForceArray ($values->{uri})}) . ')', ["(^|;)$tag(;|\$)", @{ac_ForceArray ($values->{uri})}]);
					}
				}

				# Remove cleanup unwanted serialization separators
				if (($values->{mode} eq 'add') || ($values->{mode} eq 'remove')) {
					$db->ExecSQL ("UPDATE uri_data SET tags = replace (tags, ';;', ';') FROM m_uri WHERE uri_data.uri_id = m_uri.rowid AND m_uri.uri IN (" . join (', ', ('?') x @{ac_ForceArray ($values->{uri})}) . ')', [@{ac_ForceArray ($values->{uri})}]);
					$db->ExecSQL ("UPDATE uri_data SET tags = regexp_replace (tags, '^;', '') FROM m_uri WHERE uri_data.uri_id = m_uri.rowid AND m_uri.uri IN (" . join (', ', ('?') x @{ac_ForceArray ($values->{uri})}) . ')', [@{ac_ForceArray ($values->{uri})}]);
					$db->ExecSQL ("UPDATE uri_data SET tags = regexp_replace (tags, ';\$', '') FROM m_uri WHERE uri_data.uri_id = m_uri.rowid AND m_uri.uri IN (" . join (', ', ('?') x @{ac_ForceArray ($values->{uri})}) . ')', [@{ac_ForceArray ($values->{uri})}]);
				}
			}

			if ($values->{description}) {
				# Insert new description
				$db->ExecSQL ('UPDATE uri_data SET description = ? FROM m_uri WHERE uri_data.uri_id = m_uri.rowid AND m_uri.uri IN (' . join (', ', ('?') x @{ac_ForceArray ($values->{uri})}) . ')', [$values->{description}, @{ac_ForceArray ($values->{uri})}]);
			}

			$db->ExecSQL ('UPDATE m_uri SET modified = now () WHERE uri in (' . join (', ', ('?') x @{ac_ForceArray ($values->{uri})}) . ')', [@{ac_ForceArray ($values->{uri})}]);

			$self->NotifyUpdate($context, type => ['SEARCH', 'Louvre'], uris => \@{ac_ForceArray ($values->{uri})});

			$db->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			$db->RollbackTransaction ();

			push (@{$data->{errors}}, { text => $err->{'-text'} });
		};
	}
	else {
		@{$data->{errors}} = map { {$_->[0] => $_->[1]->[0]} } @$errors;
	}

	print STDERR "[Mioga2::Magellan::Properties::SetProperties] Leaving. Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetProperties  ----------


#===============================================================================

=head2 GetPropertiesForURI

Get properties for an URI. This method is intented to be used internally.

=cut

#===============================================================================
sub GetPropertiesForURI {
	my ($self, $uri) = @_;

	my $db = $self->{db};
	my $data = $db->SelectSingle ('SELECT uri_data.tags, uri_data.description FROM uri_data, m_uri WHERE uri_data.uri_id = m_uri.rowid AND m_uri.uri = ?', [$uri]);
	if ($data) {
		@{$data->{tag}} = split (';', delete ($data->{tags}));
	}

	return ($data);
}	# ----------  end of subroutine GetPropertiesForURI  ----------

1;

