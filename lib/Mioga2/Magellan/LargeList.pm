# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
# Description:

=head1 NAME

Mioga2::Magellan::LargeList - package used to build simple large list 
	from directory listing

=cut

package Mioga2::Magellan::LargeList;
use strict;

use base qw(Mioga2::SimpleLargeList);

use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Data::Dumper;


# ============================================================================

=head2 new

    new Mioga2::Magellan::LargeList($context, $params, $files);

    Create a new FileList.

	$files is an array containing files to display.
	
	See SimpleLargeList for $params

=cut

# ============================================================================

sub new {
	my ($class, $context, $params, $files) = @_;

	my $self = $class->SUPER::new($context, $params);

	if(!defined $self) {
		return undef;
	}

	$self->{files} = $files;
	$self->{default_sort_field} = 'short_name';

	return $self;
}


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 GetNumberOfElems

    $self->GetNumberOfElems($context);

    Return the number of element to display. 

    This method is used to process page index.

=cut
    
# ============================================================================

sub GetNumberOfElems {
	my ($self, $context) = @_;

	my $nb_elem = @{$self->{files}};

	return $nb_elem;
}


# ============================================================================

=head2 GetElemsList 

    $self->GetElemsList($context, $offset, $sort_field_name, $asc);

    Return <nb_elem_per_page> elements begining to the <offset>th

    Elements must be sorted on the field $sort_field_name.
    If $asc = 0 elements are sorted descending, otherwise elements are sorted ascending.

    This method MUST be overloaded.
    
=cut
    
# ============================================================================
sub GetElemsList {
	my ($self, $context, $offset, $sort_field_name, $asc) = @_;
	
	if (!defined($sort_field_name))
	{
		$sort_field_name = 'name';
	}

	my $config = $context->GetConfig();
	my $file_list = $self->{files};

	my $start = $offset;
	my $stop = $offset + $self->{nb_elem_per_page} - 1;
	
	if($stop >= @$file_list) {
		$stop = @$file_list - 1;
	}


	my @sorted_list;
	if ($asc)
	{
		@sorted_list = sort {FileSort($asc, $sort_field_name)} @$file_list;
	}
	else
	{
		@sorted_list = sort {FileSort($asc, $sort_field_name)} @$file_list;
	}

	my @list = @sorted_list[$start..$stop];

	return \@list;
}

# ============================================================================

=head2 GetElem

    $self->GetElem($context, $filename);

    Return a hash containing information about element $rowid

=cut
    
# ============================================================================
sub GetElem {
	my ($self, $context, $filename) = @_;

	my @list = grep {$_->{name} eq $filename} @{$self->{files}};

	return $list[0];
}

# ============================================================================

=head2 FileSort

	function used for sorting file
	giving always Dirs on top
	
=cut

# ============================================================================
sub FileSort
{
	my ($asc, $sort_field_name) = @_;

	if ($a->{type} eq "collection" and $b->{type} eq "file")
	{
		return -1;
	}
	elsif ($a->{type} eq "file" and $b->{type} eq "collection")
	{
		return 1;
	}

	if ($asc)
	{
		 return $a->{$sort_field_name} cmp $b->{$sort_field_name};
	}
	else
	{
		 return $b->{$sort_field_name} cmp $a->{$sort_field_name};
	}
}

# ============================================================================

=head2 DeleteElem

    $self->DeleteElem($context, $filename);

    Delete the element with id $name.
    
    This method is called after the confirmation page if askConfirmForDelete 
    is set.

=cut
    
# ============================================================================
sub DeleteElem {
	my ($self, $context, $filename) = @_;

	my $session = $context->GetSession();
	my @list = grep {$_ != $filename} @{$self->{files}};
	@{$self->{files}} = @list;

	my @fields = grep {$_->[1] eq 'delete'} @{$self->{sll_params}->{fields}};
	
	my ($name, $type, $check_auth, $hook) = @{$fields[0]};

	&$hook($context, $filename);
}

# ============================================================================

=head2 GetNumberOfElemsMatching

    $self->GetNumberOfElems($context, $pattern);

    Return the number of element matching given patterns. 
    This method MUST be overloaded.

    See SearchElem for more details on $patterns

=cut
    
# ============================================================================
sub GetNumberOfElemsMatching {
	my ($self, $context, $pattern) = @_;

	$self->RunCountRequest( $context, $pattern );
}


# ============================================================================

=head2 GetElemsMatching

    $self->GetElemsMatching($context, $pattern, $offset, $sort_field_name, $asc);

    Return <nb_elem_per_page> elements begining to the <offset>th and matching given patterns.

    Elements must be sorted on the field $sort_field_name.
    If $asc = 0 elements are sorted descending, otherwise elements are sorted ascending.

    See SearchElem for description of $pattern
    
=cut
    
# ============================================================================
sub GetElemsMatching {
	my ($self, $context, $pattern, $offset, $sort_field_name, $asc) = @_;

	return $self->RunSearchRequest( $context, $offset, $sort_field_name, $asc, $pattern );
}

# ============================================================================

=head2 RunSearchRequest

    $self->RunSearchRequest($context, $offset, $sort_field_name, $asc, $pattern);

    Return a hash containing information about elements

=cut
    
# ============================================================================

sub RunSearchRequest {
	my ($self, $context, $offset, $sort_field_name, $asc, $pattern) = @_;

	my $session = $context->GetSession();
	my @list;

	if(defined $pattern and (keys %$pattern)) {

		my $start_with = $session->{LargeList}->{$self->{list_name}}->{sll_start_with};
		$pattern = $self->GetPostgresRegExp($pattern, $start_with);

		# ------------------------------------------------------
		# First, process non group/user/resource fields
		# ------------------------------------------------------

		my $regexp = $self->FormatSqlSearchRequest($context, $pattern);
		
		@list = grep {$_->{short_name} =~ /$pattern->{name}->[0]/} @{$self->{files}};
	}
	
	if (!defined($sort_field_name))
	{
		$sort_field_name = 'name';
	}

	my $config = $context->GetConfig();

	my $start = $offset;
	my $stop = $offset + $self->{nb_elem_per_page} - 1;
	
	if($stop >= @list) {
		$stop = @list - 1;
	}


	my @sorted_list;
	if ($asc)
	{
		@sorted_list = sort {FileSort($asc, $sort_field_name)} @list;
	}
	else
	{
		@sorted_list = sort {FileSort($asc, $sort_field_name)} @list;
	}

	my @return_list = @sorted_list[$start..$stop];

	return \@return_list;
}



# ============================================================================

=head2 RunCountRequest

    $self->RunCountRequest($context, $pattern);

    Return the number of elements.

=cut
    
# ============================================================================

sub RunCountRequest {
	my ($self, $context, $pattern) = @_;

	my @list;
	
	my $session = $context->GetSession();
	my $config  = $context->GetConfig();

	if(defined $pattern and (keys %$pattern)) {

		my $start_with = $session->{LargeList}->{$self->{list_name}}->{sll_start_with};
		$pattern = $self->GetPostgresRegExp($pattern, $start_with);

		# ------------------------------------------------------
		# First, process non group/user/resource fields
		# ------------------------------------------------------

		my $regexp = $self->FormatSqlSearchRequest($context, $pattern);
		
		@list = grep {$_->{short_name} =~ /$pattern->{name}->[0]/} @{$self->{files}};
	}
	
	return @list;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::LargeList Mioga2::SimpleLargeList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
