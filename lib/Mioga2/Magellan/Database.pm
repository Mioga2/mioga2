# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Magellan/Database.pm: Magellan API

=head1 DESCRIPTION

This module is useful to access database for filemanager

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
package Mioga2::Magellan;

use strict;
use Mioga2::tools::date_utils;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use POSIX qw(strftime);
use Data::Dumper;

my $debug = 0;


# ============================================================================

=head2 GetURIFromId ($dbh, $uri_id)

	GetURIFromId uri entry from uri_id

=cut

# ============================================================================
sub GetURIFromId
{
	my ($self, $dbh, $uri_id) = @_;

	my $sql = "select * from m_uri where rowid = $uri_id";
	
	my $result = SelectSingle($dbh, $sql);

	return $result;
}


# ============================================================================

=head2 GetIdFromURI ($dbh, $uri)

	GetIdFromURI return uri rowid from uri

=cut

# ============================================================================
sub GetIdFromURI
{
	my ($self, $dbh, $uri) = @_;
	
	my $result = SelectSingle($dbh, "SELECT * FROM m_uri WHERE uri = '".st_FormatPostgreSQLString($uri)."'");

	return $result->{rowid};
}


# ============================================================================

=head2 GetURI ($dbh, $uri)

	GetURI return uri parameters from uri

=cut

# ============================================================================
sub GetURI
{
	my ($self, $dbh, $uri) = @_;
	
	my $result = SelectSingle($dbh, "SELECT * FROM m_uri WHERE uri = '".st_FormatPostgreSQLString($uri)."'");

	return $result;
}


# ============================================================================

=head2 HasSpecificRights ($dbh, $uri_id)

	Return true if specific rights are defined for the URI.

=cut

# ============================================================================
sub HasSpecificRights
{
	my ($self, $dbh, $uri_id) = @_;
	
	my $result = SelectSingle($dbh, "SELECT * FROM m_uri WHERE rowid = $uri_id AND parent_uri_id = rowid");

	if(defined $result) {
		return 1;
	}
	return 0;
}


# ============================================================================

=head2 GetTreeRights ($dbh, $uri)

	GetProfileList get profile list for the requested group_id

=cut

# ============================================================================
sub GetTreeRights
{
	my ($self, $dbh, $uri) = @_;

	my $q_uri = st_FormatPostgreSQLString($uri);
	my $sql = "select * from m_uri where uri ~ '^$q_uri(/|\$)' and rowid = parent_uri_id order by uri";
	
	my $result = SelectMultiple($dbh, $sql);

	return $result;
}

# ============================================================================

=head2 GetProfileList ($dbh ,$group_id)

	GetProfileList get profile list for the requested group_id

=cut

# ============================================================================
sub GetProfileList
{
	my ($self, $dbh, $group_id) = @_;

	my $sql = "select * "
	                 ."from m_profile "
			 ."where "
			 ."group_id = $group_id "
			 ."order by ident";
	
	my $result = SelectMultiple($dbh, $sql);

	return $result;
}

# ============================================================================

=head2 GetAccessForGroupProfilesOnURI ($dbh, $uri, $group_id)

	return access rights for profile of the given groups

=cut

# ============================================================================
sub GetAccessForGroupProfilesOnURI
{
	my ($self, $dbh, $uri_id, $group_id) = @_;



	my $sql = "SELECT m_uri.rowid AS uri_id, ".
              "       m_uri.uri, ".
			  "       m_uri.stop_inheritance, ".
			  "       m_uri.parent_uri_id, ".
			  "       m_authorize.access,  ".
			  "       m_profile.rowid AS profile_id, m_profile.ident AS profile_ident ".

		      "FROM   m_authorize, m_authorize_profile, m_uri, m_profile ".

		      "WHERE  m_uri.rowid = $uri_id AND ".
			  "       m_authorize.uri_id = m_uri.parent_uri_id AND ".
			  "       m_authorize_profile.authz_id = m_authorize.rowid AND ".
			  "       m_authorize_profile.profile_id = m_profile.rowid AND ".
			  "       m_profile.group_id = $group_id ".

			  "ORDER BY profile_ident";
	
	my $cur_uri = SelectMultiple($dbh, $sql);
	
	my $prev_uri = [];
	if(@$cur_uri) {
		my $uri = $cur_uri->[0]->{uri};
		$uri =~ s!/[^/]+$!!g;

		$sql = "SELECT m_uri.rowid AS uri_id, ".
			   "       m_uri.parent_uri_id, ".
			   "       m_authorize.access,  ".
			   "       m_profile.rowid AS profile_id, m_profile.ident AS profile_ident ".

		       "FROM   m_authorize, m_authorize_profile, m_uri, m_profile ".

		       "WHERE  m_uri.uri = '$uri' AND ".
			   "       m_authorize.uri_id = m_uri.parent_uri_id AND ".
			   "       m_authorize_profile.authz_id = m_authorize.rowid AND ".
			   "       m_authorize_profile.profile_id = m_profile.rowid AND ".
			   "       m_profile.group_id = $group_id";
			
		$prev_uri = SelectMultiple($dbh, $sql);
	}

	foreach my $uri (@$cur_uri) {
		if($uri->{stop_inheritance}) {
			$uri->{inherited} = 0;
		}
		elsif($uri->{uri_id} != $uri->{parent_uri_id}) {
			$uri->{inherited} = 1;
		}
		elsif( grep {$uri->{access} == $_->{access} && $uri->{profile_id} == $_->{profile_id}} @$prev_uri) {
			$uri->{inherited} = 1;
		}
		else {
			$uri->{inherited} = 0;
		}

	}

	return $cur_uri;
}

# ============================================================================

=head2 GetUserAccessListOnURI ($dbh, $uri_id)

	GetUserAccessListOnURI

=cut

# ============================================================================
sub GetUserAccessListOnURI 
{
	my ($self, $dbh, $uri_id) = @_;

	my $sql = "SELECT m_uri.rowid AS uri_id, ".
              "       m_uri.parent_uri_id, ".
              "       m_uri.uri, ".
			  "       m_uri.stop_inheritance, ".
              "       m_authorize.access, ".
			  "       m_authorize_user.user_id ".

		      "FROM   m_authorize, m_authorize_user, m_uri ".

		      "WHERE  m_uri.rowid = $uri_id AND ".
			  "       m_authorize.uri_id = m_uri.parent_uri_id AND ".
			  "       m_authorize_user.authz_id = m_authorize.rowid";

	
	my $cur_uri = SelectMultiple($dbh, $sql);
	
	my $prev_uri = [];
	if(@$cur_uri) {
		my $uri = st_FormatPostgreSQLString ($cur_uri->[0]->{uri});
		$uri =~ s!/[^/]+$!!g;

		$sql = "SELECT m_uri.rowid AS uri_id, ".
               "       m_uri.parent_uri_id, ".
               "       m_authorize.access, ".
	 		   "       m_authorize_user.user_id ".

		       "FROM   m_authorize, m_authorize_user, m_uri ".

		       "WHERE  m_uri.uri = '$uri' AND ".
			   "       m_authorize.uri_id = m_uri.parent_uri_id AND ".
			   "       m_authorize_user.authz_id = m_authorize.rowid";
		$prev_uri = SelectMultiple($dbh, $sql);
	}

	foreach my $uri (@$cur_uri) {
		if($uri->{stop_inheritance}) {
			$uri->{inherited} = 0;
		}
		elsif($uri->{uri_id} != $uri->{parent_uri_id}) {
			$uri->{inherited} = 1;
		}
		elsif(grep {$uri->{access} == $_->{access} && $uri->{user_id} == $_->{user_id}} @$prev_uri) {
			$uri->{inherited} = 1;
		}
		else {
			$uri->{inherited} = 0;
		}

	}

	return $cur_uri;
}


# ============================================================================

=head2 GetTeamAccessListOnURI ($dbh, $uri_id)

	GetTeamAccessListOnURI

=cut

# ============================================================================
sub GetTeamAccessListOnURI 
{
	my ($self, $dbh, $uri_id) = @_;

	my $sql = "SELECT m_uri.rowid AS uri_id, ".
              "       m_uri.uri, ".
              "       m_uri.parent_uri_id, ".
 			  "       m_uri.stop_inheritance, ".
			  "       m_authorize.access, ".
			  "       m_authorize_team.team_id ".

		      "FROM   m_authorize, m_authorize_team, m_uri ".

		      "WHERE  m_uri.rowid = $uri_id AND ".
			  "       m_authorize.uri_id = m_uri.parent_uri_id AND ".
			  "       m_authorize_team.authz_id = m_authorize.rowid";
	
	my $cur_uri = SelectMultiple($dbh, $sql);
	
	my $prev_uri = [];
	if(@$cur_uri) {
		my $uri = $cur_uri->[0]->{uri};
		$uri =~ s!/[^/]+$!!g;

		$sql = "SELECT m_uri.rowid AS uri_id, ".
               "       m_uri.parent_uri_id, ".
               "       m_authorize.access, ".
	 		   "       m_authorize_team.team_id ".

		       "FROM   m_authorize, m_authorize_team, m_uri ".

		       "WHERE  m_uri.uri = '$uri' AND ".
			   "       m_authorize.uri_id = m_uri.parent_uri_id AND ".
			   "       m_authorize_team.authz_id = m_authorize.rowid";
		$prev_uri = SelectMultiple($dbh, $sql);
	}

	foreach my $uri (@$cur_uri) {
		if($uri->{stop_inheritance}) {
			$uri->{inherited} = 0;
		}
		elsif($uri->{uri_id} != $uri->{parent_uri_id}) {
			$uri->{inherited} = 1;
		}
		elsif(grep {$uri->{access} == $_->{access} && $uri->{team_id} == $_->{team_id}} @$prev_uri) {
			$uri->{inherited} = 1;
		}
		else {
			$uri->{inherited} = 0;
		}

	}
	
	return $cur_uri;
}

# ============================================================================

=head2 GetParentURIForUser ($config, $uri, $user_id)

	GetProfileList get profile list for the requested group_id

=cut

# ============================================================================
sub GetParentURIForUser
{
	my ($self, $config, $uri, $user_id) = @_;

	my $dbh = $config->GetDBH();

	my $root_uri = $self->GetRightsOnURIForUser($config, $uri, $user_id);
	my $parent_uri = $root_uri;
	my $result = $root_uri;

	while(defined $parent_uri) {
		my $newuri = $parent_uri->{uri};
		$newuri =~ s!/[^/]+$!!;

		$parent_uri = $self->GetRightsOnURIForUser($config, $newuri, $user_id);

		if(defined $parent_uri and $parent_uri->{access} == $root_uri->{access}) {
			$result = $parent_uri;
		}
	}
	
	return $result->{uri};
}

# ============================================================================

=head2 GetRightsOnURIForUser ($config, $uri, $user_id)

	Return the description of parent_uri and rights applyed on given uri 
	for the given user.

=cut

# ============================================================================
sub GetRightsOnURIForUser {
	my ($self, $config, $uri, $user_id) = @_;

	my $dbh = $config->GetDBH();

	return SelectSingle($dbh, "SELECT m_uri.*, m_authorize.access FROM m_uri, m_authorize, m_authorize_user ".
						      "WHERE m_uri.rowid = (SELECT parent_uri_id FROM m_uri ".
						      "                     WHERE uri='".st_FormatPostgreSQLString($uri)."') AND ".
					          "      m_uri.stop_inheritance IS FALSE AND ".
						      "      m_authorize.uri_id = m_uri.rowid AND ".
						      "      m_authorize_user.authz_id = m_authorize.rowid AND ".
						      "      m_authorize_user.user_id = $user_id")
}


# ============================================================================

=head2 GetParentURIForTeam ($config, $uri, $team_id)

	GetProfileList get profile list for the requested group_id

=cut

# ============================================================================
sub GetParentURIForTeam
{
	my ($self, $config, $uri, $team_id) = @_;

	my $dbh = $config->GetDBH();

	my $root_uri = $self->GetRightsOnURIForTeam($config, $uri, $team_id);
	my $parent_uri = $root_uri;
	my $result = $root_uri;

	while(defined $parent_uri) {
		my $newuri = $parent_uri->{uri};
		$newuri =~ s!/[^/]+$!!;

		$parent_uri = $self->GetRightsOnURIForTeam($config, $newuri, $team_id);

		if(defined $parent_uri and $parent_uri->{access} == $root_uri->{access}) {
			$result = $parent_uri;
		}
	}
	
	return $result->{uri};
}

# ============================================================================

=head2 GetRightsOnURIForTeam ($config, $uri, $team_id)

	Return the description of parent_uri and rights applyed on given uri 
	for the given team.

=cut

# ============================================================================
sub GetRightsOnURIForTeam {
	my ($self, $config, $uri, $team_id) = @_;

	my $dbh = $config->GetDBH();

	return SelectSingle($dbh, "SELECT m_uri.*, m_authorize.access FROM m_uri, m_authorize, m_authorize_team ".
						      "WHERE m_uri.rowid = (SELECT parent_uri_id FROM m_uri ".
						      "                     WHERE uri='".st_FormatPostgreSQLString($uri)."') AND ".
					          "      m_uri.stop_inheritance IS FALSE AND ".
						      "      m_authorize.uri_id = m_uri.rowid AND ".
						      "      m_authorize_team.authz_id = m_authorize.rowid AND ".
						      "      m_authorize_team.team_id = $team_id")
}


# ============================================================================

=head2 GetParentURIForProfile ($config, $uri, $profile_id)

	GetProfileList get profile list for the requested group_id

=cut

# ============================================================================
sub GetParentURIForProfile
{
	my ($self, $config, $uri, $profile_id) = @_;

	my $dbh = $config->GetDBH();

	my $root_uri = $self->GetRightsOnURIForProfile($config, $uri, $profile_id);
	my $parent_uri = $root_uri;
	my $result = $root_uri;

	while(defined $parent_uri) {
		my $newuri = $parent_uri->{uri};
		$newuri =~ s!/[^/]+$!!;

		$parent_uri = $self->GetRightsOnURIForProfile($config, $newuri, $profile_id);

		if(defined $parent_uri and $parent_uri->{access} == $root_uri->{access}) {
			$result = $parent_uri;
		}
	}
	
	return $result->{uri};
}

# ============================================================================

=head2 GetRightsOnURIForProfile ($config, $uri, $profile_id)

	Return the description of parent_uri and rights applyed on given uri 
	for the given profile.

=cut

# ============================================================================
sub GetRightsOnURIForProfile {
	my ($self, $config, $uri, $profile_id) = @_;

	my $dbh = $config->GetDBH();

	return SelectSingle($dbh, "SELECT m_uri.*, m_authorize.access FROM m_uri, m_authorize, m_authorize_profile ".
						      "WHERE m_uri.rowid = (SELECT parent_uri_id FROM m_uri ".
						      "                     WHERE uri='".st_FormatPostgreSQLString($uri)."') AND ".
					          "      m_uri.stop_inheritance IS FALSE AND ".
						      "      m_authorize.uri_id = m_uri.rowid AND ".
						      "      m_authorize_profile.authz_id = m_authorize.rowid AND ".
						      "      m_authorize_profile.profile_id = $profile_id")
}



# ============================================================================

=head2 RevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

	Remove user data in database when a user is revoked of a group.

	This function is called when :
	- a user is revoked of a group
	- a team is revoked of a group for each team member not namly
	  invited in group
	- a user is revoked of a team  for each group where the team is
	  member and the user not namly invited in group

=cut

# ============================================================================

sub RevokeUserFromGroup {
	my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;

	my $dbh = $config->GetDBH();

	ExecSQL ($dbh, "UPDATE file_comment SET user_id = $group_animator_id FROM m_uri WHERE file_comment.user_id = $user_id AND file_comment.uri_id = m_uri.rowid AND m_uri.group_id = $group_id;");

	ExecSQL ($dbh, "UPDATE uri_history SET user_id = $group_animator_id FROM m_uri WHERE uri_history.user_id = $user_id AND uri_history.uri_id = m_uri.rowid AND m_uri.group_id = $group_id;");

	ExecSQL ($dbh, "UPDATE m_uri SET user_id = $group_animator_id WHERE user_id = $user_id AND group_id = $group_id;");

	ExecSQL($dbh, "DELETE FROM m_authorize_user ".
                  "USING m_authorize, m_uri " .
			      "WHERE m_authorize_user.user_id = $user_id AND ".
			      "      m_authorize_user.authz_id=m_authorize.rowid AND ".
			      "      m_authorize.uri_id = m_uri.rowid AND ".
			      "      m_uri.group_id = $group_id");
	
}

# ============================================================================

=head2 RevokeTeamFromGroup ($config, $group_id, $team_id)

	Remove team data in database when a team is revoked of a group.

	This function is called when :
	- a team is revoked of a group

=cut

# ============================================================================

sub RevokeTeamFromGroup {
	my ($self, $config, $group_id, $team_id) = @_;

	my $dbh = $config->GetDBH();
	
	ExecSQL($dbh, "DELETE FROM m_authorize_team ".
                  "USING m_authorize, m_uri " .
			      "WHERE m_authorize_team.team_id = $team_id AND ".
			      "      m_authorize_team.authz_id=m_authorize.rowid AND ".
			      "      m_authorize.uri_id = m_uri.rowid AND ".
			      "      m_uri.group_id = $group_id");
	
}


# ============================================================================

=head2 DeleteGroupData ($config, $group_id)

	Remove group data in database when a group is deleted.

	This function is called when a group/user/resource is deleted.

=cut

# ============================================================================

sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;

	my $dbh = $config->GetDBH();

	my $group_dirs = SelectMultiple($dbh, "SELECT * FROM m_uri WHERE m_uri.uri ~ '^".$config->GetBaseURI()."/[^/]+/[^/]+\$' AND group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM m_authorize_profile ".
                  "USING m_profile, m_authorize, m_uri " .
			      "WHERE m_authorize_profile.profile_id = m_profile.rowid AND ".
			      "      m_profile.group_id = $group_id AND ".
			      "      m_authorize_profile.authz_id=m_authorize.rowid AND ".
			      "      m_authorize.uri_id = m_uri.rowid AND ".
			      "      m_uri.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM m_authorize_user ".
                  "USING m_authorize, m_uri " .
			      "WHERE m_authorize_user.authz_id=m_authorize.rowid AND ".
			      "      m_authorize.uri_id = m_uri.rowid AND ".
			      "      m_uri.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM m_authorize_team ".
                  "USING m_authorize, m_uri " .
			      "WHERE m_authorize_team.authz_id=m_authorize.rowid AND ".
			      "      m_authorize.uri_id = m_uri.rowid AND ".
			      "      m_uri.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM m_authorize ".
                  "USING m_uri " .
			      "WHERE m_authorize.uri_id = m_uri.rowid AND ".
			      "      m_uri.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM file_comment ".
                  "USING m_uri " .
			      "WHERE file_comment.uri_id = m_uri.rowid AND ".
			      "      m_uri.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM uri_history ".
                  "USING m_uri " .
			      "WHERE uri_history.uri_id = m_uri.rowid AND ".
			      "      m_uri.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM uri_data ".
                  "USING m_uri " .
			      "WHERE uri_data.uri_id = m_uri.rowid AND ".
			      "      m_uri.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM notice_files ".
                  "USING m_uri " .
			      "WHERE notice_files.uri_id = m_uri.rowid AND ".
			      "      m_uri.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM m_uri ".
			      "WHERE  m_uri.group_id = $group_id");


	if($group_id == $config->GetAdminId()) {
		return ;
	}

	# Archive the files and copy it in admin private space.

	my $admin_home = $config->GetPrivateDir()."/".$config->GetAdminIdent();

	my $group = Mioga2::Old::GroupBase->CreateObject($config, rowid => $group_id);

	my $orig_filename = $group->GetIdent()."_data_".strftime("%Y-%m-%d.%H-%M-%S", gmtime);

	my $i = 0;
	my $filename = $orig_filename;
	while(-e $admin_home."/".$filename) {
		$filename = $orig_filename."-$i";
		$i++;
	}

	my @dirs = map { $_->{uri} } @$group_dirs;
	my $priv_uri = $config->GetPrivateURI();
	my $pub_uri = $config->GetPublicURI();
	my $priv_dir = $config->GetPrivateDir();
	my $pub_dir = $config->GetPublicDir();
	
	@dirs = map { $_ =~ s/$priv_uri/$priv_dir/g;  $_ =~ s/$pub_uri/$pub_dir/g; $_ } @dirs;
	
	my $command = "tar czf '$admin_home/$filename.tar.gz' '".join("' '", @dirs). "' 2>/dev/null";
    my $dest    = $config->GetPrivateURI()."/".$config->GetAdminIdent()."/$filename.tar.gz";

	system($command);
	my $size    = (-s $dest) || 0;
	AuthzDeclareURI($dbh, $dest, $config->GetAdminId, 'application/octet-stream', $size);
	system("rm -rf '".join("' '", @dirs)."'");

}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Magellan

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
