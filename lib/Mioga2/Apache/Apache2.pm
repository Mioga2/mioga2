# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Apache2.pm: Load Apache2 requirements.

=cut

use ModPerl::Util ();
  
use Apache2::RequestRec ();
use Apache2::RequestIO ();
use Apache2::RequestUtil ();

use Apache2::ServerRec ();
use Apache2::ServerUtil ();
use Apache2::Connection ();
use Apache2::Log ();

use Apache2::Module;

use Apache2::Request;

if($Apache::Request::VERSION && $Apache::Request::VERSION gt "2.03-dev") {
	require Apache2::Upload;
}

use APR::Table ();
use APR::SockAddr ();

use ModPerl::Registry ();

use Apache2::Const -compile => qw(OK HTTP_UNAUTHORIZED HTTP_NOT_FOUND HTTP_FORBIDDEN 
								 DECLINED HTTP_INTERNAL_SERVER_ERROR HTTP_MOVED_TEMPORARILY  
								 HTTP_BAD_REQUEST);
use APR::Finfo ();
use APR::Const -compile => qw(FINFO_NORM);
1;
