#===============================================================================
#
#         FILE:  ProfileList.pm
#
#  DESCRIPTION:  New generation Mioga2 profile list class
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  05/10/2012 16:37
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME
	
ProfileList.pm: New generation Mioga2 profile list class

=head1 DESCRIPTION

This class handles Mioga2 profiles. It is designed to create, load, update or delete
profiles.

=head1 SYNOPSIS

 use Mioga2::ProfileList;
 use Mioga2::Config;

 my $profilelist;
 my $config = new Mioga2::Config (...);

 # Get list of profiles for group "Administrators"
 my $group = Mioga2::GroupList->new ($config, { attributes => { ident => 'Administrators' }});
 $profilelist = Mioga2::ProfileList->new ($config, { attributes => { group => $group }});

=head1 METHODS DESRIPTION

=cut

#
#===============================================================================

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::ProfileList;

use Data::Dumper;

use Mioga2::tools::args_checker;

my $debug = 0;


#===============================================================================

=head2 new

Create a new Mioga2::ProfileList

=head3 Incoming Arguments

=over

=item I<$config:> a Mioga2::Config object.

=item I<$data:> a hashref with the following keys:

=over

=item I<attributes:> a list of key / value pairs matching the profiles.

=item I<short_list:> an optional flag to limit the amount of data describing the profiles.

=back

=back

=head3 Return value

=over

=item B<return:> a Mioga2::ProfileList object matching profiles according to provided attributes.

=back

=cut

#===============================================================================
sub new {
	my ($class, $config, $data) = @_;
	print STDERR "[Mioga2::ProfileList::new] Entering, data: " . Dumper $data if ($debug);

	my $self = { };
	bless($self, $class);

	$self->{config} = $config;
	$self->{db} = $self->{config}->GetDBObject ();

	# Initialize attributes
	$self->{attributes} = $data->{attributes};

	# Initialize object state
	$self->{loaded} = 0;
	$self->{deleted} = 0;

	# DB-related keys
	$self->{fields} = [ ];
	$self->{tables} = [ ];
	$self->{leftjoin} = [ ];
	$self->{where} = [ ];

	# Initialize WHERE statement before any update is made on the list
	push (@{$self->{fields}}, qw/m_profile.rowid m_profile.ident m_profile.group_id/);
	push (@{$self->{tables}}, qw/m_profile/);
	if (exists ($self->{attributes}->{group})) {
		$self->InitializeFromGroup ();
	}
	if (exists ($self->{attributes}->{rowid})) {
		$self->InitializeFromRowid ();
	}
	if (exists ($self->{attributes}->{ident})) {
		$self->InitializeFromIdent ();
	}

	print STDERR "[Mioga2::ProfileList::new] Leaving\n" if ($debug);
	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 Load

Load profiles from database

=head3 Incoming Arguments

=over

None

=back

=head3 Return value

=over

None, initializes $self->{profiles}.

=back

=cut

#===============================================================================
sub Load {
	my ($self) = @_;
	print STDERR "[Mioga2::ProfileList::Load] Entering\n" if ($debug);

	# Get data from Database
	my $request = 'SELECT ' . join (', ', @{$self->{fields}}) . ' FROM ' . join (', ', @{$self->{tables}});
	if (@{$self->{leftjoin}}) {
		$request .= ' LEFT JOIN ';
		$request .= join (' LEFT JOIN ', @{$self->{leftjoin}});
	}
	$request .= ' WHERE ' . join (' AND ', @{$self->{where}});
	print STDERR "[Mioga2::ProfileList::Load] Request: $request\n" if ($debug);
	$self->{profiles} = $self->{db}->SelectMultiple ($request);

	$self->{loaded} = 1;

	print STDERR "[Mioga2::ProfileList::Load] Leaving, loaded " . $self->Count () . " profiles\n" if ($debug);
}	# ----------  end of subroutine Load  ----------


#===============================================================================

=head2 Count

Returns the number of profiles in the list

=head3 Incoming Arguments

=over

None

=back

=head3 Return value

=over

The profile count.

=back

=cut

#===============================================================================
sub Count {
	my ($self) = @_;

	# Load list if needed
	if (!$self->{loaded}) {
		$self->Load ();
	}

	return (scalar (@{$self->{profiles}}));
}	# ----------  end of subroutine Count  ----------


#===============================================================================

=head2 Get

Get an attribute from current list.

=head3 Incoming Arguments

=over

=item I<$attr>: The name of the attribute to get.

=back

=head3 Return value

=over

The attribute value as a string (when only on profile is matched by the list)
or an array of attribute values (when multiple profiles matched by the list).

=back

=cut

#===============================================================================
sub Get {
	my ($self, $attr) = @_;
	print STDERR "[Mioga2::ProfileList::Get] Entering for $attr\n" if ($debug);

	# Load list if needed
	if (!$self->{loaded}) {
		$self->Load ();
	}

	# Get attribute from profiles
	my @value = map { $_->{$attr} } @{ac_ForceArray ($self->{profiles})};
	print STDERR "[Mioga2::ProfileList::Get] Value: " . Dumper \@value if ($debug);

	return ((@value == 1) ? $value[0] : \@value);
}	# ----------  end of subroutine Get  ----------


#===============================================================================

=head2 Set

Set value for profile(s)

=head3 Incoming Arguments

=over

=item I<$attribute:> the attribute name to set.

=item I<$value:> the attribute value to set.

=back

=cut

#===============================================================================
sub Set {
	my ($self, $attribute, $value) = @_;

	$self->{update}->{$attribute} = $value;
}	# ----------  end of subroutine Set  ----------


#===============================================================================

=head2 Store

Store changes to database. This method has to be called to actually create or
update one or more profiles.

=cut

#===============================================================================
sub Store {
	my ($self) = @_;
	print STDERR "[Mioga2::ProfileList::Store] Entering\n" if ($debug);

	if ((!$self->Count ()) && (!$self->{deleted})) {
		#-------------------------------------------------------------------------------
		# TODO Create a profile into DB
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::ProfileList::Store] Creating new profile\n" if ($debug);
	}
	elsif (scalar (keys (%{$self->{update}}))) {
		#-------------------------------------------------------------------------------
		# Update profiles
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::ProfileList::Store] Updating existing profile, data: " . Dumper $self->{update} if ($debug);
		my $sql = "UPDATE m_profile SET ";
		for my $field (keys (%{$self->{update}})) {
			$sql .= $field . '=' . $self->{db}->Quote ($self->{update}->{$field}) . ', ';
		}
		$sql .= "modified = now ()";
		$sql .= " WHERE rowid IN (" . join (', ', $self->Get ('rowid')) . ")";

		print STDERR "[Mioga2::ProfileList::Store] SQL: $sql\n" if ($debug);
		my $status = 1;
		$self->{db}->ExecSQL ($sql);
	}
	else {
		print STDERR "[Mioga2::ProfileList::Store] Nothing has to be done\n" if ($debug);
	}

	#-------------------------------------------------------------------------------
	# Update-pending values are now stored into DB, they can be moved to the inner
	# attributes of the object
	#-------------------------------------------------------------------------------
	for my $field (keys (%{$self->{update}})) {
		$self->{attributes}->{$field} = delete ($self->{update}->{$field});
	}

	print STDERR "[Mioga2::ProfileList::Store] Leaving\n" if ($debug);
}	# ----------  end of subroutine Store  ----------


#===============================================================================

=head2 GetProfiles

Get loaded profiles

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

A hash or an array of hashes (depending on the number of profiles) containing
the profiles attributes.

=back

=cut

#===============================================================================
sub GetProfiles {
	my ($self) = @_;

	# Load list if needed
	if (!$self->{loaded}) {
		$self->Load ();
	}

	return ((@{$self->{profiles}} == 1) ? $self->{profiles}->[0] : $self->{profiles});
}	# ----------  end of subroutine GetProfiles  ----------


#===============================================================================

=head2 SetFunctions

Set list of functions the profile can access

=head3 Incoming Arguments

=over

=item I<$functions>: An array of function rowids

=back

=cut

#===============================================================================
sub SetFunctions {
	my ($self, $functions) = @_;

	if ($functions &&  @$functions) {
		# Remove unwanted functions
		my $sql = 'DELETE FROM m_profile_function WHERE profile_id IN (' . join (', ', @{ac_ForceArray ($self->Get ('rowid'))}) . ') AND function_id NOT IN (' . join (', ', @$functions) . ');';
		$self->{db}->ExecSQL ($sql);

		# Add new functions
		$sql = 'INSERT INTO m_profile_function SELECT m_profile.rowid, m_function.rowid FROM m_function, m_profile WHERE m_profile.rowid IN (' . join (', ', @{ac_ForceArray ($self->Get ('rowid'))}) . ') AND m_function.rowid IN (' . join (', ', @$functions) . ') AND (m_profile.rowid, m_function.rowid) NOT IN (SELECT * FROM m_profile_function);';
		$self->{db}->ExecSQL ($sql);
	}
	else {
		# Clear functions for profile
		my $sql = 'DELETE FROM m_profile_function WHERE profile_id IN (' . join (', ', @{ac_ForceArray ($self->Get ('rowid'))}) . ')';
		$self->{db}->ExecSQL ($sql);
	}
}	# ----------  end of subroutine SetFunctions  ----------


#===============================================================================

=head2 Delete

Delete profiles

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

None, throws exceptions.

=back

=cut

#===============================================================================
sub Delete {
	my ($self) = @_;

	$self->{db}->ExecSQL ('DELETE FROM m_profile_function WHERE profile_id IN (' . join (', ', @{ac_ForceArray ($self->Get ('rowid'))}) . ')');
	$self->{db}->ExecSQL ('DELETE FROM m_authorize_profile WHERE profile_id IN (' . join (', ', @{ac_ForceArray ($self->Get ('rowid'))}) . ')');
	$self->{db}->ExecSQL ('DELETE FROM m_profile WHERE rowid IN (' . join (', ', @{ac_ForceArray ($self->Get ('rowid'))}) . ')');
}	# ----------  end of subroutine Delete  ----------


#===============================================================================

=head1 PRIVATE METHODS DESRIPTION

=cut

#===============================================================================


#===============================================================================

=head2 InitializeFromGroup

Initialize request to match profiles from a Mioga2::GroupList

=head3 Incoming Arguments

=over

None

=back

=head3 Return value

=over

None

=back

=cut

#===============================================================================
sub InitializeFromGroup {
	my ($self) = @_;
	print STDERR "[Mioga2::ProfileList::InitializeFromGroup] Entering.\n" if ($debug);

	my $rowids = join (', ', map { $self->{db}->Quote ($_) } @{ac_ForceArray ($self->{attributes}->{group}->Get ('rowid'))});
	$rowids = 'NULL' unless $rowids;

	# Profile attributes
	push (@{$self->{fields}}, 'COALESCE (users.count, 0) AS nb_users');
	push (@{$self->{fields}}, 'COALESCE (teams.count, 0) AS nb_teams');
	push (@{$self->{where}}, 'm_profile.group_id IN (' . $rowids . ')');

	# User count
	push (@{$self->{leftjoin}}, '(SELECT m_profile_group.profile_id, count (m_user_base.rowid) FROM m_profile_group, m_profile, m_user_base WHERE m_profile_group.profile_id = m_profile.rowid AND m_profile_group.group_id = m_user_base.rowid AND m_profile_group.profile_id = m_profile.rowid AND m_profile.group_id IN (' . $rowids . ') GROUP BY m_profile_group.profile_id) AS users ON m_profile.rowid = users.profile_id');

	# Team count
	push (@{$self->{leftjoin}}, '(SELECT m_profile_group.profile_id, count (m_group.rowid) FROM m_profile_group, m_profile, m_group WHERE m_profile_group.profile_id = m_profile.rowid AND m_profile_group.group_id = m_group.rowid AND m_profile_group.profile_id = m_profile.rowid AND m_profile.group_id IN (' . $rowids . ') GROUP BY m_profile_group.profile_id) AS teams ON m_profile.rowid = teams.profile_id');

	print STDERR "[Mioga2::ProfileList::InitializeFromGroup] Fields: " . Dumper $self->{fields} if ($debug);
	print STDERR "[Mioga2::ProfileList::InitializeFromGroup] Tables: " . Dumper $self->{tables} if ($debug);
	print STDERR "[Mioga2::ProfileList::InitializeFromGroup] LeftJoin: " . Dumper $self->{leftjoin} if ($debug);
	print STDERR "[Mioga2::ProfileList::InitializeFromGroup] Where: " . Dumper $self->{where} if ($debug);

	print STDERR "[Mioga2::ProfileList::InitializeFromGroup] Leaving.\n" if ($debug);
}	# ----------  end of subroutine InitializeFromGroup  ----------


#===============================================================================

=head2 InitializeFromRowid

Initialize request parts to match profiles from rowid(s).

=head3 Incoming Arguments

=over

None

=back

=head3 Return value

=over

None

=back

=cut

#===============================================================================
sub InitializeFromRowid {
	my ($self) = @_;
	print STDERR "[Mioga2::ProfileList::InitializeFromRowid] Entering.\n" if ($debug);

	# Profile attributes
	my $rowids = join (', ', map { $self->{db}->Quote ($_) } @{ac_ForceArray ($self->{attributes}->{rowid})});
	$rowids = 'NULL' unless $rowids;
	push (@{$self->{where}}, "m_profile.rowid IN ($rowids)");

	print STDERR "[Mioga2::ProfileList::InitializeFromRowid] Fields: " . Dumper $self->{fields} if ($debug);
	print STDERR "[Mioga2::ProfileList::InitializeFromRowid] Tables: " . Dumper $self->{tables} if ($debug);
	print STDERR "[Mioga2::ProfileList::InitializeFromRowid] Where: " . Dumper $self->{where} if ($debug);

	print STDERR "[Mioga2::ProfileList::InitializeFromRowid] Leaving.\n" if ($debug);
}	# ----------  end of subroutine InitializeFromRowid  ----------


#===============================================================================

=head2 InitializeFromIdent

Initialize request parts to match profiles from ident(s)

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

None.

=back

=cut

#===============================================================================
sub InitializeFromIdent {
	my ($self) = @_;
	print STDERR "[Mioga2::ProfileList::InitializeFromIdent] Entering.\n" if ($debug);

	# Profile attributes
	my $idents = join (', ', map { $self->{db}->Quote ($_) } @{ac_ForceArray ($self->{attributes}->{ident})});
	$idents = 'NULL' unless $idents;
	push (@{$self->{where}}, "m_profile.ident IN ($idents)");

	print STDERR "[Mioga2::ProfileList::InitializeFromIdent] Fields: " . Dumper $self->{fields} if ($debug);
	print STDERR "[Mioga2::ProfileList::InitializeFromIdent] Tables: " . Dumper $self->{tables} if ($debug);
	print STDERR "[Mioga2::ProfileList::InitializeFromIdent] Where: " . Dumper $self->{where} if ($debug);

	print STDERR "[Mioga2::ProfileList::InitializeFromIdent] Leaving.\n" if ($debug);
}	# ----------  end of subroutine InitializeFromIdent  ----------


1;

