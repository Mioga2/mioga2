# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME

Bottin.pm : The Mioga2 LDAP account management application.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Bottin;
use strict;
use vars qw($VERSION);

$VERSION = "1.0.0";

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'bottin';

use Error qw(:try);
use Mioga2::Classes::LDAPView;
use Mioga2::Classes::Grid;
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::LDAP;
use Mioga2::tools::database;
use Mioga2::tools::args_checker;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Mioga2::tools::APIGroup;
use Mioga2::Exception::Simple;
use Mioga2::tools::Convert;
use MIME::Entity;
use MIME::Words qw (:all);
use Encode::Detect::Detector;
use Data::Dumper;

my $debug = 0;

# ============================================================================
# Application Description
# ============================================================================


# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc {
	my %AppDesc = (
		ident   => 'Bottin',
		package => 'Mioga2::Bottin',
		name => __('LDAP Account Manager'),
		description => __('LDAP account management application'),
		type    => 'normal',
		all_groups => 0,
		all_users  => 0,
		can_be_public => 0,
		is_user             => 1,
		is_group            => 1,
		is_resource         => 0,
		functions => { 
			'Administration' => __('Administration Functions'),
			'Write' => __('Read-Write access'),
			'Read' => __('Read-only access'),
			'Notifications' => __('Receive notifications'),
		},
		func_methods => {
			'Administration' => [
				'DisplayMain', 'DisplayUsers', 'GetUserData', 'GetUser', 'SetUser', 'DeleteUser', 'DisableUsers', 'EnableUsers', 'ImportUsers', 'ExportUsers', 'EmailUsers', 'StorePreferences', 'GetUserStatuses'
			],
			'Write' => [
				'DisplayMain', 'DisplayUsers', 'GetUserData', 'GetUser', 'SetUser', 'DisableUsers', 'EnableUsers', 'ExportUsers', 'EmailUsers', 'StorePreferences', 'GetUserStatuses'
			],
			'Read' => [
				'DisplayMain', 'DisplayUsers', 'GetUserData', 'GetUser', 'ExportUsers', 'StorePreferences', 'GetUserStatuses'
			],
			Notifications => [],
		},
		sxml_methods => { },
		xml_methods  => { },
		non_sensitive_methods => [
			'DisplayMain',
			'DisplayUsers',
			'ExportUsers',
			'GetUser',
			'GetUserData',
			'GetUserStatuses',
			'ImportUsers',
		],
	);

	return \%AppDesc;
}

# ============================================================================
# Application Initialization
# ============================================================================


#===============================================================================

=head2 Initialize

Context-independant initialization

=cut

#===============================================================================
sub Initialize {
	my ($self, $config) = @_;

	#-------------------------------------------------------------------------------
	# CSV variables
	#-------------------------------------------------------------------------------
	for (qw/email lastname firstname password ident/) {
		push (@{$self->{csvfields}}, $_);
	}

	#-------------------------------------------------------------------------------
	# Initialize LDAP connection
	#-------------------------------------------------------------------------------
	if ((ref ($config) eq 'Mioga2::Config') && ($config->UseLDAP ())) {
		try {
			$self->{ldap} = Mioga2::LDAP->new ($config);
			$self->{ldap}->BindWith ($config->GetLDAPBindDN(), $config->GetLDAPBindPwd());
		}
		otherwise {
			# Can't crash here, would lead to a generic error page (HTTP-500), would be meaningless to user
		};
	}
}	# ----------  end of subroutine Initialize  ----------

# ============================================================================
# InitApp
#
# InitApp is used to initialize application's internal values.
# It is automatically called by Dispatch if exists.
#
# ============================================================================

sub InitApp {
	my ($self, $context) = @_;
	print STDERR "Bottin::InitApp\n" if ($debug);
	
	$self->{mioga_id} =	$context->GetConfig()->GetMiogaId();
	$self->{mioga_ident} =	$context->GetConfig()->GetMiogaIdent();
	$self->{dbh} =	$context->GetConfig()->GetDBH();
	$self->{user_id} = $context->GetUser()->GetRowid();
	$self->{user_firstname} = $context->GetUser()->GetFirstname();
	$self->{user_lastname} = $context->GetUser()->GetLastname();
	$self->{user_email} = $context->GetUser()->GetEmail();
	$self->{group_id} =	$context->GetGroup()->GetRowid();

	#-------------------------------------------------------------------------------
	# Super-Admin mode flag
	#-------------------------------------------------------------------------------
	$self->InitSuperAdminMode ();

	# ----------------------------------------------------------------------------
	# Users list fields
	# ----------------------------------------------------------------------------
	$self->{users_sll_field_list} =  [ [ 'dn', 'rowid'],
								  [ 'ident', 'normal', __('Identifier')],
								  [ 'firstname', 'normal', __('Firstname')],
								  [ 'lastname', 'normal', __('Lastname')],
								  [ 'email', 'email', __('E-mail')],
								  [ 'status', 'normal', __('Status')],
								  [ 'last_connection', 'date', __('Last connection')],
								];
	if ($self->{superadmin}) {
		push (@{$self->{users_sll_field_list}}, [ 'count(instances)', 'normal', __('Instances') ]);
	}

}	# ----------  end of subroutine InitApp ----------


# ============================================================================
# Public Methods
# ============================================================================


#===============================================================================

=head2 DisplayMain

Main entry point, display a search form

=cut

#===============================================================================
sub DisplayMain {
	my ($self, $context) = @_;

	# If LDAP connection failed, report error to user
	if (!$self->{ldap}) {
		my $data = { LdapAccessError => $context->GetContext () };
		my $content = new Mioga2::Content::XSLT ($context, stylesheet    => 'bottin.xsl', locale_domain => "bottin_xsl");
		$content->SetContent (Mioga2::tools::Convert::PerlToXML ($data));
		return ($content);
	}

	my $users = $self->{ldap}->SearchUsers (use_instance_user_filter => 1);

	my $content;
	if (scalar (@$users) > 100) {
		#-------------------------------------------------------------------------------
		# Many users, display search form
		#-------------------------------------------------------------------------------
		my $data = { DisplayMain => $context->GetContext () };

		my $xml = Mioga2::tools::Convert::PerlToXML ($data);
		print STDERR "[Mioga2::Bottin::DisplayMain] XML: $xml\n" if ($debug);

		$content = new Mioga2::Content::XSLT( $context, stylesheet    => 'bottin.xsl', locale_domain => "bottin_xsl");
		$content->SetContent($xml);
	}
	else {
		#-------------------------------------------------------------------------------
		# Few users, directly display list
		#-------------------------------------------------------------------------------
		$content = Mioga2::Content::REDIRECT->new ($context, mode => 'external');
		$content->SetContent('DisplayUsers');
	}

	return ($content);
}	# ----------  end of subroutine DisplayMain  ----------

# ============================================================================
# 
# DisplayUsers
#
# ============================================================================
sub DisplayUsers {
	my ($self, $context) = @_;
	print STDERR "Bottin::DisplayUsers()\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['firstname', 'lastname', 'email', 'type', 'status', 'match' ], 'allow_empty' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);
	
	#-------------------------------------------------------------------------------
	# Create User View and Grid
	#-------------------------------------------------------------------------------
	# ----------------------------------------------------------------------------
	# Users list search fields
	# ----------------------------------------------------------------------------
	$self->{users_sll_search_field_list} = [
                                     { ident =>  'firstname', label => __('Firstname'), type => 'text', value => $values->{firstname} },
		 							 { ident =>  'lastname', label => __('Lastname'), type => 'text', value => $values->{lastname}, focus => 1 },
		  							 { ident =>  'email', label => __('E-mail'), type => 'email', value => $values->{email} },
									 { ident =>  'status', label => __('Status'), type => 'list', value => $values->{status}, url => 'GetUserStatuses.json', nodename => 'status', identifier => 'ident' },
								];


	# ----------------------------------------------------------------------------
	# "Normal" Users list description
	# ----------------------------------------------------------------------------
	%{$self->{users_sll_desc}} = ( 'name' => 'mioga_users',
						
						'ldap_request' => {
						},
                       
                       'fields' => $self->{users_sll_field_list},
  
                       'default_sort_field' => 'lastname',

                       'filter' => $self->{users_sll_search_field_list},

						'filter_match' => $values->{match},
                      
                       );
	my $view_params = { ldap => $self->{ldap},
	                    desc => $self->{users_sll_desc},
	                  };
	my $user_view = Mioga2::Classes::LDAPView->new($view_params);
	my $grid_params = { ident => 'users',
	                    view => $user_view,
	                    get_data => 'GetUserData',
						nodename => 'items',
						identifier => 'ident',
						label => __('Identifier'),
						confirm_fields => ['firstname', 'lastname', 'email'],
	                    default_action => 'DisplayUserEdit',
						select_actions => [ { label => __("Export"), function => 'ExportUsers', type => 'file' },
						                  ],
	                  };

	# Get custom sort field and column widths from session
	my $session = $context->GetSession ();
	if (exists ($session->{persistent}->{Bottin}->{sort_field})) {
		$grid_params->{sort_field} = $session->{persistent}->{Bottin}->{sort_field};
	}
	if (exists ($session->{persistent}->{Bottin}->{column_widths})) {
		$grid_params->{column_widths} = $session->{persistent}->{Bottin}->{column_widths};
	}

	# Add menu items if user has admin rights
	if ($self->GetUserRights ()->{Administration}) {
		push (@{$grid_params->{select_actions}}, { label => __("Delete"), function => 'DeleteUser', func => 'GetInstance()' });
	}

	# Add menu items if user has write rights
	if ($self->GetUserRights ()->{Administration} || $self->GetUserRights ()->{Write}) {
		push (@{$grid_params->{select_actions}}, { label => __("Disable"), function => 'DisableUsers', func => 'GetInstance()' });
		push (@{$grid_params->{select_actions}}, { label => __("Enable"), function => 'EnableUsers', func => 'GetInstance()' });
		push (@{$grid_params->{select_actions}}, { label => __("Send E-mail"), function => 'EmailUsers', func => 'GetInstance()'});
	}

	my $user_grid = Mioga2::Classes::Grid->new($grid_params);
	
	#-------------------------------------------------------------------------------
	# Generate XML
	#-------------------------------------------------------------------------------
	my $xml = '<?xml version="1.0" encoding="UTF-8"?>';
	my $canmodify = $self->{config}->CanModifyLdapPassword ();
	my $canimport = $self->{config}->CanModifyLdapInstance ();
	my $superadmin = $self->{superadmin};
	$xml .= "<DisplayUsers CanModify='$canmodify' CanImport='$canimport' SuperAdmin='$superadmin'>";
	$xml .= $context->GetXML();
	$xml .= $self->GetXmlLDAPInstances ($context) if ($superadmin);
	$xml .= $self->{config}->GetXMLPasswordPolicy ();
	$xml .= $user_grid->GetXML();
	$xml .= "</DisplayUsers>";

	print STDERR "xml = $xml\n" if($debug);
	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'bottin.xsl', locale_domain => "bottin_xsl");
    $content->SetContent($xml);

	return $content;
}	# ----------  end of subroutine DisplayUsers  ----------


# ============================================================================

=head2 GetUserData ()

Returns user list data

=cut

# ============================================================================

sub GetUserData {
	my ($self, $context) = @_;

	print STDERR "Bottin::GetUserData() Entering. " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['instance'], 'allow_empty', 'stripxws'],
			[ ['ident', 'firstname', 'lastname', 'email', 'status'], 'allow_empty', 'stripwxps' ],
			[ ['match'], 'allow_empty', [ 'match', "^\(begins|contains|ends\)\$" ] ],
			[ ['mode'], 'allow_empty', [ 'match', "^count\$" ] ]
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Bottin::GetUserData", __("Wrong argument value.") . Dumper $errors);
	}

	#-------------------------------------------------------------------------------
	# Switch to target instance (if any)
	#-------------------------------------------------------------------------------
	$self->SwitchInstance ($values->{instance});
	
	#-------------------------------------------------------------------------------
	# Create User View and Grid
	#-------------------------------------------------------------------------------
	%{$self->{users_sll_desc}} = ( 'name' => 'mioga_users',
						
						'ldap_request' => {
						},
                       
                       'fields' => $self->{users_sll_field_list},
  
                       'default_sort_field' => 'lastname',

                       'filter' => $self->{users_sll_search_field_list},
                      
                       );
	my $view_params = { ldap => $self->{ldap},
	                    desc => $self->{users_sll_desc},
	                  };
	my $user_view = Mioga2::Classes::LDAPView->new($view_params);

	#-------------------------------------------------------------------------------
	# Get user list for current instance
	#-------------------------------------------------------------------------------
	my %filter = (instances => $self->{config}->GetMiogaIdent ());
	for my $attr (qw/firstname lastname email status/) {
		if (st_ArgExists ($context, $attr) && $values->{$attr} ne '') {
			my $value = $values->{$attr};
			if ($values->{match} eq 'begins') {
				$value = "$value*";
			}
			elsif ($values->{match} eq 'contains') {
				$value = "*$value*";
			}
			else {
				$value = "*$value";
			}
			$filter{$attr} = $value;
		}
	}
	my $data = $user_view->GetData({
			mode => 'dojo_grid',
			ldap_params => $self->{ldap}->TranslateAttributes (\%filter),
			instance => $self->{config}->GetMiogaIdent (),
			as_object => 1,
		});

	# If called with mode='count', just return user count
	if ($values->{mode}) {
		my $count = $data->{items} ? scalar (@{$data->{items}}) : 0;
		$data = { count => $count };
	}

	# Filter by status, this is achieved here as user status is not a LDAP attribute
	# and can not be handled by LDAPView
	if ($values->{status}) {
		@{$data->{items}} = grep { $_->{status} eq $values->{status} } @{$data->{items}};
	}

	# canmodify and canimport flags, translating m_mioga ldap_acces:
	# 	2: canmodify=true & canimport=true
	# 	1: canmodify=false & canimport=true
	# 	0: canmodify=false & canimport=false
	$data->{canmodify} = $self->{config}->CanModifyLdapPassword () ? 'boolean::true' : 'boolean::false';
	$data->{canimport} = $self->{config}->CanModifyLdapInstance () ? 'boolean::true' : 'boolean::false';

	my $json = Mioga2::tools::Convert::PerlToJSON ($data);

	print STDERR "data = $json\n" if($debug);
	my $content = new Mioga2::Content::ASIS($context, MIME => "text/plain");
    $content->SetContent($json);

	return $content;
}	# ----------  end of subroutine GetUserData  ----------


# ============================================================================

=head2 GetUser ()

Get a user data

=cut

# ============================================================================
sub GetUser {
    my ($self, $context) = @_;
	print STDERR "Bottin::GetUser()\n" if ($debug);

	print STDERR "args = ".Dumper($context->{args})."\n" if($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
		[ ['rowid'], 'allow_empty', 'stripxws'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Bottin::GetUser", __("Wrong argument value.") . Dumper $errors);
	}

	my $dn;
	if (exists($values->{rowid})) {
		$dn = $values->{rowid};
	}

	# Generate an unused random ident
	my $random_ident = '';
	while ($random_ident eq '') {
		$random_ident = Mioga2::tools::string_utils::st_Random (10);
		try {
			Mioga2::Old::GroupBase->CreateObject($self->{config}, ident => $random_ident);
			$random_ident = '';
		}
		otherwise {
			print STDERR "Mioga2::Bottin::GetUser Using random ident $random_ident for new user." if ($debug);
		};
	}

	my $data = { };
	$data->{user} = { firstname => '',
	                  lastname => '',
					  email => '',
					  rowid => '',
					  ident => $random_ident,
					  password => '',
					  password2 => '',
					  description => ''
					};
	
	if ($dn ne '') {
		my $user = $self->{ldap}->SearchSingleUser($dn);

		$data->{user} = { firstname => $user->{firstname},
						  lastname => $user->{lastname},
						  email => $user->{email},
						  ident => $user->{ident},
					  	  rowid => $user->{dn},
						  password => '',
						  password2 => '',
						  description => $user->{description}
						};

		if ($self->{superadmin}) {
			$data->{user}->{instances} = $user->{instances};
		}
	}

	return $data;
}	# ----------  end of subroutine GetUser  ----------


# ============================================================================

=head2 SetUser ()

Set user attributes

=cut

# ============================================================================

sub SetUser {
    my ($self, $context) = @_;
	print STDERR "Bottin::SetUser()\n" if ($debug);

	print STDERR "args = ".Dumper($context->{args})."\n" if($debug);
	
	#-------------------------------------------------------------------------------
	# Check form data
	#-------------------------------------------------------------------------------
	my $values = {};
	my $errors = [];

	# First check Instance name that can be used to generate a new Mioga2::Config
	my $params = [ [ [ 'instance' ], 'allow_empty', 'stripxws' ] ];
	($values, $errors) = ac_CheckArgs($context, $params );

	#-------------------------------------------------------------------------------
	# Switch to target instance (if any)
	#-------------------------------------------------------------------------------
	$self->SwitchInstance ($values->{instance});

	my $dn;
	if (st_ArgExists ($context, 'ident')) {
		# User edition or creation
		$params = [ [ [ 'firstname', 'lastname'], 'stripxws', 'allow_empty', ['max' => 128] ],
					[ [ 'importdn' ], 'allow_empty', 'stripxws' ],
					[ [ 'instance' ], 'allow_empty', 'stripxws' ],
					[ [ 'rowid' ], 'allow_empty', 'stripxws' ],
					[ [ 'canmodify', 'canimport' ], 'allow_empty', 'stripxws' ],
					[ [ 'description' ], 'stripxws', ['max' => 4096]  ],
				];

		$dn = $context->{args}->{rowid};
		if ($dn eq '') {
			push @$params, [ [ 'password', 'password2' ], 'allow_empty', ['password_policy', $self->{config}], ['max' => 128] ];
			push @$params, [ [ 'ident' ], ['not_used_group_ident', $self->{config}], [ 'valid_ident', $self->{config} ], ['max' => 128], 'stripxws', 'disallow_empty'];
			push @$params, [ [ 'email' ], 'want_email', [ 'unused_email', $self->{config} ], 'stripxws', 'disallow_empty', ['max' => 128]  ];
		}
		else {
			push @$params, [ [ 'password', 'password2' ], 'allow_empty', ['password_policy', $self->{config}], ['max' => 128] ];
			push @$params, [ [ 'ident' ], ['max' => 128], 'stripxws', 'disallow_empty'];

			# Check if email field is to be changed and ensure new email is not already used
			my $user = Mioga2::UserList->new ($self->{config}, { attributes => { dn => $dn } });
			my $current_email = $user->Get ('email');
			if ($current_email ne $context->{args}->{email}) {
				push @$params, [ [ 'email' ], 'want_email', [ 'unused_email', $self->{config} ], 'stripxws', 'disallow_empty', ['max' => 128]  ];
			}
			else {
				push @$params, [ [ 'email' ], 'want_email', 'stripxws', 'disallow_empty', ['max' => 128]  ];
			}
		}
	}
	else {
		# User quick import
		$params = [ [ [ 'email' ], 'want_email', [ 'unused_email', $self->{config} ], 'stripxws', 'disallow_empty', ['max' => 128]  ],
					[ [ 'canmodify', 'canimport' ], 'allow_empty', 'stripxws' ],
					[ [ 'importdn' ], 'allow_empty', 'stripxws' ],
					[ [ 'instance' ], 'allow_empty', 'stripxws' ],
				];
	}

	($values, $errors) = ac_CheckArgs($context, $params );

	$values->{email} = lc ($values->{email});	# Force email to lowercase

	if ($values->{password} ne $values->{password2}) {
		push @$errors, ['', ['different_passwords'] ];
	}

	#-------------------------------------------------------------------------------
	# Remove any duplicate 'password_policy' error
	#-------------------------------------------------------------------------------
	my $id = -1;
	for (@$errors) {
		if ($_->[1][0] eq 'password_policy') {
			$id++;
		}
	}
	if ($id > 0) {
		splice (@$errors, $id, 1);
	}

	#-------------------------------------------------------------------------------
	# Get instance name
	#-------------------------------------------------------------------------------
	my $instance = $self->{config}->GetMiogaIdent ();


	#-------------------------------------------------------------------------------
	# Check if a 'email' error is because of a deleted_not_purged user
	#-------------------------------------------------------------------------------
	my $email_error_id = undef;
	my $error_index = -1;
	my $import;
	for (@$errors) {
		$error_index++;
		next if ($_->[0] ne 'email');
		if ($_->[1]->[0] eq 'unused_email') {
			my $user = new Mioga2::Old::User ($self->{config}, email => $values->{email});
			if ($user && $user->GetStatus () eq 'deleted_not_purged') { # TODO ( - 18/09/2014 10:50): Check LDAP access
				# User is "deleted_not_purged" but doesn't exist in directory
				# Fake an import but create it
				$values->{ident} = $user->GetIdent ();
				$email_error_id = $error_index;
				if (!$values->{importdn}) {
					my $user_desc = $user->GetFirstname () . ' ' . $user->GetLastname () . ' (' . $user->GetEmail () . ')';
					$import = ['import', [$user_desc, $user->GetDN (), $user_desc] ];
				}
				last;
			}
		}
	}
	if (defined ($email_error_id)) {
		delete ($errors->[$email_error_id]);
		if ($import && $self->{config}->CanModifyLdapInstance ()) {
			push (@$errors, $import);
		}
		elsif (!$self->{config}->CanModifyLdapInstance ()) {
			push @$errors, [ 'denied', [ __('This action is denied') ] ];
		}
	}

	my $data;
	if(! @$errors) {
		if(exists $values->{password}  and $values->{password} eq '') {
			delete $values->{password};
		}
		if(exists $values->{password2}) {
			delete $values->{password2};
		}

		print STDERR "values = " . Dumper($values) . "\n" if ($debug);

		try {
			if ($values->{ident}) {
				if ($dn eq '') {
					print STDERR "Bottin::SetUser New user\n" if ($debug);
					#-------------------------------------------------------------------------------
					# Create user if not existing, add user to instance if already existing
					#-------------------------------------------------------------------------------
					my $users = $self->{ldap}->SearchUsers (mail => $values->{email}, use_instance_user_filter => 0);
					if (!scalar (@$users)) {
						$users = $self->{ldap}->SearchUsers (uid => $values->{ident}, use_instance_user_filter => 0);
						if (!scalar (@$users) && ($self->{config}->CanModifyLdapPassword ())) {
							print STDERR "Bottin::SetUser Create user\n" if ($debug);

							#-------------------------------------------------------------------------------
							# First check if deleted_not_purged user exists locally
							# In such a case, user doesn't exist in LDAP and has to be created (user previously deleted)
							#-------------------------------------------------------------------------------
							my $user = Mioga2::UserList->new ($self->{config}, { attributes => { email => $values->{email} }, short_list => 1 });
							if ($user && $user->Get ('status') eq 'deleted_not_purged') {
								for my $field (qw/ident firstname lastname dn password/) {
									$values->{$field} = $user->Get ($field);
								}
							}


							#-------------------------------------------------------------------------------
							# Create user in LDAP
							#-------------------------------------------------------------------------------
							if (!$values->{ident}) {
								# Generate an unused random ident
								my $random_ident = '';
								while ($random_ident eq '') {
									$random_ident = Mioga2::tools::string_utils::st_Random (10);
									try {
										Mioga2::GroupBase->CreateObject($self->{config}, ident => $random_ident);
										$random_ident = '';
									}
									otherwise {
										print STDERR "Mioga2::Bottin::SetUser Using random ident $random_ident for new user." if ($debug);
									};
								}
								$values->{ident} = $random_ident;
							}
							push (@{$values->{instances}}, $instance);
							my $crypter = $self->{config}->LoadPasswordCrypter();
							$values->{password} = $crypter->CryptPassword($self->{config}, $values);
							$values->{dn} = "uid=$values->{ident}," . $self->{config}->GetLDAPUserBaseDN ();
							$values->{cn} = "$values->{firstname} $values->{lastname}";
							$self->{ldap}->AddUser ($self->{ldap}->TranslateAttributes ($values));
						}
						elsif (!$self->{config}->CanModifyLdapPassword ()) {
							push @$errors, [ 'denied', [ __('This action is denied') ] ];
						}
						else {
							#-------------------------------------------------------------------------------
							# Return an error as the ident is already used
							#-------------------------------------------------------------------------------
							push @$errors, ['ident', [__('Ident already exists')] ];
						}
					}
					else {
						my $user = $users->[0];
						if ($values->{importdn} eq '') {
							if ($self->{config}->CanModifyLdapInstance ()) {
								#-------------------------------------------------------------------------------
								# Prompt the admin for user update
								#-------------------------------------------------------------------------------
								push @$errors, ['import', ["$values->{firstname} $values->{lastname} ($values->{email})", "$user->{dn}", "$user->{firstname} $user->{lastname} ($user->{email})"] ];
							}
							else {
								push @$errors, [ 'denied', [ __('This action is denied') ] ];
							}
						}
						else {
							#-------------------------------------------------------------------------------
							# Import user from another instance
							#-------------------------------------------------------------------------------
							if ($values->{importdn} eq $user->{dn}) {
								print STDERR "Bottin::SetUser Import user from another instance\n" if ($debug);
								if (!grep (/^$instance$/, @{$user->{instances}})) {
									$dn = $user->{dn};
									push (@{$user->{instances}}, $instance);
									$self->{ldap}->ModifyUser (delete ($user->{dn}), $self->{ldap}->TranslateAttributes($user));
								}
							}
							else {
								push @$errors, [ 'error', [ __('Error importing user (wrong dn)') ] ];
							}
						}
					}
				}
				else {
					my $users = $self->{ldap}->SearchUsers (mail => $values->{email}, use_instance_user_filter => 0);
					if (!scalar (@$users) || ($users->[0]->{dn} eq $dn)) {
						#-------------------------------------------------------------------------------
						# Modify user information in LDAP
						#-------------------------------------------------------------------------------
						print STDERR "Bottin::SetUser Existing user\n" if ($debug);
						if ($values->{password}) {
							my $crypter = $self->{config}->LoadPasswordCrypter();
							$values->{password} = $crypter->CryptPassword($self->{config}, $values);
						}
						$values->{cn} = "$values->{firstname} $values->{lastname}";
						$self->{ldap}->ModifyUser ($dn, $self->{ldap}->TranslateAttributes ($values));
					}
					else {
						my $user = $users->[0];
						push @$errors, ['email', ['unused_email'] ];
					}
				}
			}
			else {
				# Quick user import
				my $users = $self->{ldap}->SearchUsers (mail => $values->{email}, use_instance_user_filter => 0);
				my $user = $users->[0];
				if ((defined ($user)) && ($self->{config}->CanModifyLdapInstance ())) {
					# User already exists, prompt for import (or actually import)
					if ($values->{importdn} eq '') {
						#-------------------------------------------------------------------------------
						# Prompt the admin for user update
						#-------------------------------------------------------------------------------
						push @$errors, ['import', ["$values->{firstname} $values->{lastname} ($values->{email})", "$user->{dn}", "$user->{firstname} $user->{lastname} ($user->{email})"] ];
					}
					else {
						#-------------------------------------------------------------------------------
						# Import user from another instance
						#-------------------------------------------------------------------------------
						if ($values->{importdn} eq $user->{dn}) {
							print STDERR "Bottin::SetUser Import user from another instance\n" if ($debug);
							if (!grep (/^$instance$/, @{$user->{instances}})) {
								$dn = $user->{dn};
								push (@{$user->{instances}}, $instance);
								$self->{ldap}->ModifyUser (delete ($user->{dn}), $self->{ldap}->TranslateAttributes($user));
							}
						}
						else {
							push @$errors, [ 'error', [ __('Error importing user (wrong dn)') ] ];
						}
					}
				}
				elsif ((defined ($user))) {
					push @$errors, [ 'denied', [ __('This action is denied') ] ];
				}
				elsif ($self->{config}->CanModifyLdapPassword ()) {
					# User doesn't exist, prompt for creation

					# Generate an unused random ident
					my $random_ident = '';
					while ($random_ident eq '') {
						$random_ident = Mioga2::tools::string_utils::st_Random (10);
						try {
							Mioga2::Old::GroupBase->CreateObject($self->{config}, ident => $random_ident);
							$random_ident = '';
						}
						otherwise {
							print STDERR "Mioga2::Bottin::GetUser Using random ident $random_ident for new user." if ($debug);
						};
					}

					$data = { };
					$data->{errors} = [ ['create', [ ] ] ];
					$data->{user} = { firstname => '',
									  lastname => '',
									  email => $values->{email},
									  rowid => '',
									  ident => $random_ident,
									  password => '',
									  password2 => '',
									  description => ''
									};

				}
				else {
					push @$errors, [ 'denied', [ __('This action is denied') ] ];
				}
			}
		}
		catch Mioga2::Exception::LDAP with {
			my $err = shift;
			push @$errors, [$err->as_string, ['__modify__'] ];
		};
	}

	# Handle user to be created
	if (defined ($data) && defined ($data->{errors}) && defined ($data->{errors}->[0]) && $data->{errors}->[0]->[0] eq 'create') {
		return ($data);
	}

	my $user_dn;
	if (exists ($values->{dn})) {
		$user_dn = $values->{dn};
	}
	elsif (exists ($values->{importdn})) {
		$user_dn = $values->{dn};
	}
	else {
		$user_dn = $values->{rowid};
	}
	
	# Synchronize LDAP and DB
	if(!@$errors) {
		$self->SynchronizeInstance ($context, $instance, $user_dn);
	}
	
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	$data = {};
	$data->{errors} = $errors;

	return $data;
}	# ----------  end of subroutine SetUser  ----------


#===============================================================================

=head2 DeleteUser

Delete a user of unaffect it from current instance

=cut

#===============================================================================
sub DeleteUser {
	my ($self, $context) = @_;
	
	print STDERR "Bottin::DeleteUser()\n" if ($debug);

	print STDERR "args = ".Dumper($context->{args})."\n" if($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
		[ ['rowids'], 'disallow_empty' ],
		[ ['instance'], 'allow_empty', 'stripxws'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Bottin::DeleteUser", __("Wrong argument value.") . Dumper $errors);
	}
	
	my $dnlist = ac_ForceArray($values->{rowids});

	#-------------------------------------------------------------------------------
	# Switch to target instance (if any)
	#-------------------------------------------------------------------------------
	$self->SwitchInstance ($values->{instance});
	my $instance = $self->{config}->GetMiogaIdent ();
	
	#-------------------------------------------------------------------------------
	# Process each selected user account
	#-------------------------------------------------------------------------------
	foreach (@$dnlist) {
		my $user = $self->{ldap}->SearchSingleUser ($_);
		if (((scalar (@{$user->{instances}}) > 1) && ($instance ne '*')) || !$self->{config}->CanModifyLdapPassword ()) {
			#-------------------------------------------------------------------------------
			# User is affected to other instances, revoke it
			#-------------------------------------------------------------------------------
			my @instances = ();
			foreach (@{$user->{instances}}) {
				next if ($_ eq $instance);
				push (@instances, $_);
			}
			$user->{instances} = \@instances;
			$self->{ldap}->ModifyUser (delete ($user->{dn}), $self->{ldap}->TranslateAttributes($user));
		}
		else {
			#-------------------------------------------------------------------------------
			# User is not affected to other instances, delete it
			#-------------------------------------------------------------------------------
			$self->{ldap}->DeleteUser ($_);
		}
	}
	
	# Synchronize LDAP and DB
	$instance = '' if ($instance eq '*');
	for my $dn (@$dnlist) {
		$self->SynchronizeInstance ($context, $instance, $dn);
	}

	my $content = new Mioga2::Content::ASIS($context, MIME => "text/plain");
    $content->SetContent("OK");

	return $content;
}	# ----------  end of subroutine DeleteUser  ----------


#===============================================================================

=head2 DisableUsers

Disable users for current instance

=cut

#===============================================================================
sub DisableUsers {
	my ($self, $context) = @_;
	
	print STDERR "Bottin::DisableUsers()\n" if ($debug);

	print STDERR "args = ".Dumper($context->{args})."\n" if($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
		[ ['rowids'], 'disallow_empty'],
		[ ['instance'], 'allow_empty', 'stripxws'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Bottin::DisableUsers", __("Wrong argument value.") . Dumper $errors);
	}

	my $dnlist = ac_ForceArray($values->{rowids});

	#-------------------------------------------------------------------------------
	# Switch to target instance (if any)
	#-------------------------------------------------------------------------------
	$self->SwitchInstance ($values->{instance});
	my $instance = $self->{config}->GetMiogaIdent ();

	my $mioga_id = SelectSingle ($self->{dbh}, "SELECT rowid FROM m_mioga WHERE ident='$instance'")->{rowid};

	#-------------------------------------------------------------------------------
	# Process each selected user account
	#-------------------------------------------------------------------------------
	foreach (@$dnlist) {
		my $user = new Mioga2::Old::User ($self->{config}, (dn => $_, mioga_id => $mioga_id));
		UserDisable($self->{config}, $user->GetRowid ());
	}

	my $content = new Mioga2::Content::ASIS($context, MIME => "text/plain");
    $content->SetContent("OK");

	return $content;
}	# ----------  end of subroutine DisableUsers  ----------


#===============================================================================

=head2 EnableUsers

Disable users for current instance

=cut

#===============================================================================
sub EnableUsers {
	my ($self, $context) = @_;
	
	print STDERR "Bottin::EnableUsers()\n" if ($debug);

	print STDERR "args = ".Dumper($context->{args})."\n" if($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
		[ ['rowids'], 'disallow_empty'],
		[ ['instance'], 'allow_empty', 'stripxws'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Bottin::EnableUsers", __("Wrong argument value.") . Dumper $errors);
	}

	my $dnlist = ac_ForceArray($values->{rowids});

	#-------------------------------------------------------------------------------
	# Switch to target instance (if any)
	#-------------------------------------------------------------------------------
	$self->SwitchInstance ($values->{instance});
	my $instance = $self->{config}->GetMiogaIdent ();

	my $mioga_id = SelectSingle ($self->{dbh}, "SELECT rowid FROM m_mioga WHERE ident='$instance'")->{rowid};

	#-------------------------------------------------------------------------------
	# Process each selected user account
	#-------------------------------------------------------------------------------
	foreach (@$dnlist) {
		my $user = new Mioga2::Old::User ($self->{config}, (dn => $_, mioga_id => $mioga_id));
		UserEnable($self->{config}, $user->GetRowid ());
	}

	my $content = new Mioga2::Content::ASIS($context, MIME => "text/plain");
    $content->SetContent("OK");

	return $content;
}	# ----------  end of subroutine EnableUsers  ----------


#===============================================================================

=head2 ImportUsers

Import users from a CSV file

=cut

#===============================================================================
sub ImportUsers {
	my ($self, $context) = @_;

	print STDERR "Bottin::ImportUsers\n" if ($debug);
	print STDERR "args = ".Dumper($context->{args})."\n" if($debug);

	my $xml = '<ImportUsers>';
	$xml .= $context->GetXML ();

	if (st_ArgExists ($context, 'file')) {
		# Step 1, user submitted a file
		my $params = [
				[ ['instance'], 'allow_empty', 'stripxws'],
				[ ['file'], 'disallow_empty'],
				[ ['mode', 'process_import'], 'allow_empty'],
			];
		my ($values, $errors) = ac_CheckArgs ($context, $params);

		my $upload_data = CheckUTF8($context->GetUploadData);

		if (!@$errors) {
			my $user_data = ();
			$xml .= '<Check>';

			#-------------------------------------------------------------------------------
			# Switch to target instance (if any)
			#-------------------------------------------------------------------------------
			$self->SwitchInstance ($values->{instance});
			my $instance = $self->{config}->GetMiogaIdent ();
			my $transl = $self->{config}->GetLDAPTranslationTable ();

			#-------------------------------------------------------------------------------
			# Process contents
			#-------------------------------------------------------------------------------
			my @contents = split ("\n", $upload_data);
			my $linenr = 0;
			for my $line (@contents) {
				my ($status, $match);

				$linenr++;

				# Extract fields from CSV line
				my $fields;
				my @line = split (',', $line);
				for my $name (@{$self->{csvfields}}) {
					$fields->{$name} = shift (@line);
					$fields->{$name} =~ s/^\"(.*)\"$/$1/;
				}

				$fields->{email} = lc ($fields->{email});	# Force email to lowercase
				$fields->{instance} = $instance;

				if (@{ac_CheckValue ($fields->{email}, ['want_email'])} || @{ac_CheckValue ($fields->{ident}, [[ 'valid_ident', $context->GetConfig () ]])}) {
					$fields->{skip} = 1;
					$xml .= '<WrongSyntax>';
					$xml .= '<line>' . $linenr . '</line>';
					$xml .= '<user>' . st_FormatXMLString ($fields->{firstname}) . " " . st_FormatXMLString ($fields->{lastname}) . " (" . st_FormatXMLString ($fields->{email}) . ")" . '</user>';
					$xml .= '</WrongSyntax>';
					push (@$user_data, $fields);
					next;
				}

				if ($values->{mode} eq 'add') {
					# User will be added

					# Ensure ident is valid and unused
					if (@{ac_CheckValue ($fields->{ident}, ['not_used_group_ident', $self->{config}])}) {
						$fields->{skip} = 1;
						$xml .= '<IdentInUse>';
						$xml .= '<line>' . $linenr . '</line>';
						$xml .= '<user>' . st_FormatXMLString ($fields->{firstname}) . " " . st_FormatXMLString ($fields->{lastname}) . " (" . st_FormatXMLString ($fields->{email}) . ")" . '</user>';
						$xml .= '</IdentInUse>';
						push (@$user_data, $fields);
						next;
					}

					# Ensure email is not already used as local user
					my $chk = ac_CheckValue ($fields->{email}, [ 'unused_email', $self->{config} ]);
					if (@$chk) {
						# Email is known in current instance, ensure it is not a LDAP user with deleted_not_purged
						my $founduser = Mioga2::UserList->new ($self->{config}, { attributes => { email => $fields->{email}, type => 'ldap_user', status => 'deleted_not_purged' } });
						if (!$founduser->Count ()) {
							$fields->{skip} = 1;
							$xml .= '<AlreadyExists>';
							$xml .= '<line>' . $linenr . '</line>';
							$xml .= '<user>' . st_FormatXMLString ($fields->{firstname}) . " " . st_FormatXMLString ($fields->{lastname}) . " (" . st_FormatXMLString ($fields->{email}) . ")" . '</user>';
							$xml .= '<match>' . st_FormatXMLString ($founduser->Get ('firstname')) . " " . st_FormatXMLString ($founduser->Get ('lastname')) . " (" . st_FormatXMLString ($founduser->Get ('email')) . ")" . '</match>';
							$xml .= '</AlreadyExists>';
							push (@$user_data, $fields);
							next;
						}
					}

					my $users = $self->{ldap}->SearchUsers (mail => $fields->{email}, use_instance_user_filter => 0);

					if (@$users) {
						# A user matches current line in LDAP
						if (grep (/^$instance$/, @{$users->[0]->{instances}})) {
							# User is in the same instance, will not be processed
							$fields->{skip} = 1;
							$xml .= '<AlreadyExists>';
							$xml .= '<line>' . $linenr . '</line>';
							$xml .= '<user>' . st_FormatXMLString ($fields->{firstname}) . " " . st_FormatXMLString ($fields->{lastname}) . " (" . st_FormatXMLString ($fields->{email}) . ")" . '</user>';
							$xml .= '<match>' . st_FormatXMLString ($users->[0]->{firstname}) . " " . st_FormatXMLString ($users->[0]->{lastname}) . " (" . st_FormatXMLString ($users->[0]->{email}) . ")" . '</match>';
							$xml .= '</AlreadyExists>';
						}
						else {
							# User can be imported from another instance
							$fields->{can_be_imported} = 1;
							$xml .= '<Import>';
							$xml .= '<line>' . $linenr . '</line>';
							$xml .= '<user>' . st_FormatXMLString ($fields->{firstname}) . " " . st_FormatXMLString ($fields->{lastname}) . " (" . st_FormatXMLString ($fields->{email}) . ")" . '</user>';
							$xml .= '<match>' . st_FormatXMLString ($users->[0]->{firstname}) . " " . st_FormatXMLString ($users->[0]->{lastname}) . " (" . st_FormatXMLString ($users->[0]->{email}) . ")" . '</match>';
							if ($self->{config}->GetMiogaConf ()->ShowSourceInstanceName ()) {
								$xml .= '<source_instance>' . join ('</source_instance><source_instance>', @{$users->[0]->{instances}}) . '</source_instance>';
							}
							$xml .= '</Import>';
						}
					}
					else {
						# No user matches current line in LDAP
						if ($self->{config}->CanModifyLdapPassword ()) {
							$xml .= '<Create>';
							$xml .= '<line>' . $linenr . '</line>';
							$xml .= '<user>' . st_FormatXMLString ($fields->{firstname}) . " " . st_FormatXMLString ($fields->{lastname}) . " (" . st_FormatXMLString ($fields->{email}) . ")" . '</user>';
							$xml .= '</Create>';
						}
						else {
							$xml .= '<CreationDenied>';
							$xml .= '<line>' . $linenr . '</line>';
							$xml .= '<user>' . st_FormatXMLString ($fields->{firstname}) . " " . st_FormatXMLString ($fields->{lastname}) . " (" . st_FormatXMLString ($fields->{email}) . ")" . '</user>';
							$xml .= '</CreationDenied>';
						}
					}
				}
				else {
					# User will be modified
					my $users = $self->{ldap}->SearchUsers ($transl->{email} => $fields->{email}, $transl->{inst} => $instance, use_instance_user_filter => 0);
					if (!@$users) {
						# User not found by mail, try to find user by ident
						$users = $self->{ldap}->SearchUsers ($transl->{ident} => $fields->{ident}, $transl->{inst} => $instance, use_instance_user_filter => 0);
					}
					if (!@$users) {
						# User does not exist, update is not possible
						$fields->{skip} = 1;
						$xml .= '<DoesNotExist>';
						$xml .= '<line>' . $linenr . '</line>';
						$xml .= '<user>' . st_FormatXMLString ($fields->{firstname}) . " " . st_FormatXMLString ($fields->{lastname}) . " (" . st_FormatXMLString ($fields->{email}) . ")" . '</user>';
						$xml .= '</DoesNotExist>';
					}
					else {
						# User exists, update is possible
						$xml .= '<Modify>';
						$xml .= '<line>' . $linenr . '</line>';
						$xml .= '<user>' . st_FormatXMLString ($fields->{firstname}) . " " . st_FormatXMLString ($fields->{lastname}) . " (" . st_FormatXMLString ($fields->{email}) . ")" . '</user>';
						$xml .= '</Modify>';
					}
				}

				push (@$user_data, $fields);
			}

			# Store data to session
			my $session = $context->GetSession ();
			$session->{persistent}->{Bottin}->{Import} = {};
			$session->{persistent}->{Bottin}->{Import}->{mode} = $values->{mode};
			$session->{persistent}->{Bottin}->{Import}->{data} = $user_data;
			$session->{persistent}->{Bottin}->{Import}->{instance} = $instance;

			$xml .= '</Check>';
		}
		else {
			# Back to step 0, upload form
			$xml .= "<UploadForm>";
			if ($self->{superadmin}) {
				my $params = [
						[ ['instance'], 'disallow_empty' ],
					];
				my ($values, $errors) = ac_CheckArgs ($context, $params);
				$xml .= "<Instance>" . $values->{instance} . "</Instance>";
			}
			$xml .= "</UploadForm>";
		}
	}
	elsif (st_ArgExists ($context, 'import')) {
		
		# Step 2, user checked (or not) users to import
		my $session = $context->GetSession ();
		my $mode =  $session->{persistent}->{Bottin}->{Import}->{mode};
		my $user_data = $session->{persistent}->{Bottin}->{Import}->{data};

		my @importids = grep (/^import-line\[[0-9]*\]/, keys (%{$context->{args}}));
		my $params = [
				[ ['import'], 'disallow_empty' ],
				[ \@importids, 'disallow_empty'],
			];
		my ($values, $errors) = ac_CheckArgs ($context, $params);

		if (!@$errors) {
			$xml .= '<ProcessContents>';

			#-------------------------------------------------------------------------------
			# Switch to target instance (if any)
			#-------------------------------------------------------------------------------
			$self->SwitchInstance ($session->{persistent}->{Bottin}->{Import}->{instance});

			# Flag users to import
			for my $number (@importids) {
				$number =~ s/import-line\[([0-9]*)\]/$1/;
				$user_data->[$number-1]->{import} = 1;
			}

			print STDERR "[Mioga2::Bottin::ImportUsers] Data to import: " . Dumper $user_data if ($debug);
			$xml .= $self->ProcessCSV ($self->{config}, $mode, $user_data);

			# Synchronize instance
			my $instance = $session->{persistent}->{Bottin}->{Import}->{instance};
			$self->SynchronizeInstance ($context, $instance);

			# Remove data from session
			delete ($session->{persistent}->{Bottin}->{Import});

			$xml .= '</ProcessContents>';
		}
	}
	else {
		# Step 0, display file upload form
		$xml .= "<UploadForm>";
		if ($self->{superadmin}) {
			my $params = [
					[ ['instance'], 'disallow_empty' ],
				];
			my ($values, $errors) = ac_CheckArgs ($context, $params);
			$xml .= "<Instance>" . $values->{instance} . "</Instance>";
		}
		$xml .= "</UploadForm>";
	}

	$xml .= '</ImportUsers>';

	print STDERR "[Mioga2::Bottin::ImportUsers] xml: $xml \n" if ($debug);

	my $content = new Mioga2::Content::XSLT( $context, stylesheet    => 'bottin.xsl', locale_domain => "bottin_xsl");
    $content->SetContent($xml);

	return $content;
}	# ----------  end of subroutine ImportUsers  ----------


#===============================================================================

=head2 GetUserStatuses

Get user statuses list as XML or JSON.

=head3 Incoming Arguments

=over

None.

=back

=head3 Generated XML

=over

	<GetUserStatuses>
		<status rowid="...">...</status>
		...
	</GetUserStatuses>

B<Note:> This method actually returns a Perl hash that will be automatically converted to XML or JSON by Mioga2::Application::WSWrapper.

=back

=cut

#===============================================================================
sub GetUserStatuses {
	my ($self, $context) = @_;

	my $data = { };

	my $sql = "SELECT * FROM m_user_status;";
	my $statuses = SelectMultiple ($self->{dbh}, $sql);

	for (@$statuses) {
		$_->{label} = __($_->{ident});
	}

	$data->{status} = $statuses;

	print STDERR "[Mioga2::Bottin] Data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetUserStatuses  ----------


#===============================================================================

=head2 ProcessCSV

Process CSV file contents

=cut

#===============================================================================
sub ProcessCSV {
	my ($self, $config, $mode, $data) = @_;

	my $xml = '';
	my $transl = $config->GetLDAPTranslationTable ();

	#-------------------------------------------------------------------------------
	# Process contents
	#-------------------------------------------------------------------------------
	my $linenr = 0;
	for my $fields (@$data) {
		$linenr++;

		# Forget lines that can not be processed
		if ($fields->{skip}) {
			$xml .= '<NotProcessed>';
			$xml .= '<line>' . $linenr . '</line>';
			$xml .= '<user>' . st_FormatXMLString ($fields->{firstname}) . ' ' . st_FormatXMLString ($fields->{lastname}) . ' (' . st_FormatXMLString ($fields->{email}) . ')' . '</user>';
			$xml .= '</NotProcessed>';
			next;
		}

		my ($tag, $error);
		if ($mode eq 'add') {
			#-------------------------------------------------------------------------------
			# Add users to current instance
			#-------------------------------------------------------------------------------
			if (!$fields->{can_be_imported} && $config->CanModifyLdapPassword ()) {
				# User does not exist yet, create it
				$tag = 'ProcessCreation';
				$error = $self->CreateLDAPUser ($config, $fields);
			}
			elsif ($fields->{import}) {
				# User has been selected for import
				$tag = 'ProcessImport';
				my $instance = $fields->{instance};

				# Get data from LDAP
				my $users = $self->{ldap}->SearchUsers (mail => $fields->{email}, use_instance_user_filter => 0);
				$fields = $users->[0];
				push (@{$fields->{instances}}, $instance);
				$error = $self->ModifyLDAPUser ($config, $fields);
			}
			else {
				# User has not been selected for import, nothing to be done
				$tag = 'NotProcessed';
			}
		}
		else {
			#-------------------------------------------------------------------------------
			# Modify users in current instance
			#-------------------------------------------------------------------------------
			$tag = 'ProcessUpdate';
			my $instance = $fields->{instance};
			my $users = $self->{ldap}->SearchUsers ($transl->{email} => $fields->{email}, $transl->{inst} => $instance, use_instance_user_filter => 0);
			if (!@$users) {
				# User not found by mail, try to find user by ident
				$users = $self->{ldap}->SearchUsers ($transl->{ident} => $fields->{ident}, $transl->{inst} => $instance, use_instance_user_filter => 0);
			}
			for my $name (keys(%$fields)) {
				if ($fields->{$name}) {
					$users->[0]->{$name} = $fields->{$name};
				}
			}
			try {
				$self->{ldap}->ModifyUser (delete ($users->[0]->{dn}), $self->{ldap}->TranslateAttributes($users->[0]));
			}
			otherwise {
				$error = shift;
			};
		}

		# Generate status line
		$xml .= "<$tag>";
		$xml .= '<line>' . $linenr . '</line>';
		$xml .= '<user>' . st_FormatXMLString ($fields->{firstname}) . ' ' . st_FormatXMLString ($fields->{lastname}) . ' (' . st_FormatXMLString ($fields->{email}) . ')' . '</user>';
		if ($error) {
			$xml .= '<error>' . $error . '</error>' if ($error);
		}
		else {
			$xml .= '<success/>';
		}
		$xml .= "</$tag>";
	}

	print STDERR "[Mioga2::Bottin::ProcessCSV] Leaving\n" if ($debug);
	return ($xml);
}	# ----------  end of subroutine ProcessCSV  ----------


#===============================================================================

=head2 CreateLDAPUser

Create a user in LDAP

=cut

#===============================================================================
sub CreateLDAPUser {
	my ($self, $config, $fields) = @_;
	print STDERR "[Mioga2::Bottin::CreateLDAPUser] Entering: " . Dumper $fields if ($debug);

	my $error;

	my $crypter = $config->LoadPasswordCrypter();
	$fields->{password} = $crypter->CryptPassword($config, $fields);

	if ($fields->{ident} eq '') {
		# Generate an unused random ident
		my $random_ident = '';
		while ($random_ident eq '') {
			$random_ident = Mioga2::tools::string_utils::st_Random (10);
			try {
				Mioga2::Old::GroupBase->CreateObject($config, ident => $random_ident);
				$random_ident = '';
			}
			otherwise {
				print STDERR "Mioga2::Bottin::ImportUsers Using random ident $random_ident for new user." if ($debug);
			};
		}
		$fields->{ident} = $random_ident;
	}
	else {
		try {
			Mioga2::Old::GroupBase->CreateObject($config, ident => $fields->{ident});
			print STDERR "[Mioga2::Bottin::ImportUsers] Can not process user, ident $fields->{ident} already exists in DB\n";
			$error = __('Identifier is already used');
		}
		otherwise {
		};
	}

	if (!$error) {
		# User ident does not exist in DB, can process
		$fields->{dn} = "uid=$fields->{ident}," . $config->GetLDAPUserBaseDN ();
		$fields->{cn} = "$fields->{firstname} $fields->{lastname}";
		push (@{$fields->{instances}}, delete ($fields->{instance}));
		try {
			$self->{ldap}->AddUser ($self->{ldap}->TranslateAttributes ($fields));
		}
		catch Mioga2::Exception::LDAP with {
			$error = shift;
			print STDERR "[Mioga2::Bottin::CreateLDAPUser] User with the following attributes could not be added to LDAP: " . Dumper $self->{ldap}->TranslateAttributes ($fields);
			print STDERR "[Mioga2::Bottin::CreateLDAPUser] Message is: " . $error . "\n";
		};
	}

	print STDERR "[Mioga2::Bottin::CreateLDAPUser] Leaving: \"$error\"\n" if ($debug);
	return ($error);
}	# ----------  end of subroutine CreateLDAPUser  ----------


#===============================================================================

=head2 ModifyLDAPUser

Modify an existing user in LDAP

=cut

#===============================================================================
sub ModifyLDAPUser {
	my ($self, $config, $fields) = @_;

	my $error;

	try {
		$self->{ldap}->ModifyUser (delete ($fields->{dn}), $self->{ldap}->TranslateAttributes($fields));
	}
	catch Mioga2::Exception::LDAP with {
		my $error = shift;
		print STDERR "[Mioga2::Bottin::ModifyLDAPUser] User with the following attributes could not be added to LDAP: " . Dumper $self->{ldap}->TranslateAttributes ($fields);
		print STDERR "[Mioga2::Bottin::ModifyLDAPUser] Message is: " . $error . "\n";
	};

	return ($error);
}	# ----------  end of subroutine ModifyLDAPUser  ----------


#===============================================================================

=head2 ExportUsers

Export users to CSV according to DNs.

=cut

#===============================================================================
sub ExportUsers {
	my ($self, $context) = @_;

	print STDERR "Bottin::ExportUsers\n" if ($debug);
	
	print STDERR "args = ".Dumper($context->{args})."\n" if($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
		[ ['rowids'], 'allow_empty'],
		[ ['instance'], 'allow_empty', 'stripxws'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Bottin::ExportUsers", __("Wrong argument value.") . Dumper $errors);
	}

	my $dnlist = ac_ForceArray($values->{rowids});

	#-------------------------------------------------------------------------------
	# Switch to target instance (if any)
	#-------------------------------------------------------------------------------
	$self->SwitchInstance ($values->{instance});

	my $users = ();
	my $csv = '';

	if (scalar (@$dnlist)) {
		#-------------------------------------------------------------------------------
		# Export only the specified users
		#-------------------------------------------------------------------------------
		for (@$dnlist) {
			my $user = $self->{ldap}->SearchSingleUser ($_);
			push (@$users, $user);
		}
	}
	else {
		#-------------------------------------------------------------------------------
		# Export all users
		#-------------------------------------------------------------------------------
		$users = $self->{ldap}->SearchUsers ((ou => $self->{config}->GetMiogaIdent (), use_instance_user_filter => 0));
	}

	# Parse each user
	foreach my $user (@$users) {
		my $line = '';
		for (@{$self->{csvfields}}) {
			$line .= "\"$user->{$_}\",";
		}
		$line =~ s/,$//;
		$csv .= "$line\n";
	}

	print STDERR "CSV: $csv\n" if ($debug);

    my $content = Mioga2::Content::ASIS->new($context, MIME => 'text/csv', filename => 'mioga-users.csv');
    $content->SetContent( $csv );

    return ($content);
}	# ----------  end of subroutine ExportUsers  ----------


#===============================================================================

=head2 EmailUsers

E-mail a link to users so they can access Doris to define their password.

=cut

#===============================================================================
sub EmailUsers {
	my ($self, $context) = @_;

	print STDERR "Bottin::EmailUsers\n" if ($debug);
	
	print STDERR "args = ".Dumper($context->{args})."\n" if($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
		[ ['rowids', 'instance'], 'disallow_empty'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Bottin::EmailUsers", __("Wrong argument value.") . Dumper $errors);
	}

	my $dnlist = ac_ForceArray($values->{rowids});

	#-------------------------------------------------------------------------------
	# Switch to target instance (if any)
	#-------------------------------------------------------------------------------
	$self->SwitchInstance ($values->{instance});

	$errors = ();

	my $me = $context->GetUser ();

	my $base_url = $self->{config}->GetProtocol () . "://" . $self->{config}->GetServerName () . $self->{config}->GetBaseURI ();

	my $user;
	for (@$dnlist) {
		$user = new Mioga2::Old::User ($self->{config}, (dn => $_, mioga_id => $self->{config}->GetMiogaId ()));

		my $activation_delay = $self->{config}->GetMiogaConf()->GetActivationDelay ();
		my $password_url = Mioga2::Login::GetReinitLink ($context, $user->GetRowid (), $activation_delay * 3600 * 24);

		# Build and send mail
		my $subject = __('Your Mioga2 account has been created');
		my $body = __x("You Mioga2 account has been created on {url}.\n\nYour account is not active yet, you must first define a password connecting to the followind address: {password_url}\n\nThe link above will be valid for the next {delay} days. If you don't activate your account within this period, you can request a new activation link at the following address: {request_url}", url => $base_url, password_url => $password_url, delay => $activation_delay, request_url => Mioga2::Login::GetLoginURI ($self->{config}, $self->{config}->GetBaseURI ()));
		$subject = encode_mimeword($subject, "Q", "UTF-8");
		my $entity = MIME::Entity->build(
			Charset => 'UTF8',
			Type    => "text/plain",
			From    => $me->GetFirstname()." ".$me->GetLastname()." <".$me->GetEmail().">",
			Subject => $subject, 
			Data    => $body);
		$user->SendMail ($entity);

	}

	my $data = {};
	$data->{errors} = $errors;

	my $content = new Mioga2::Content::ASIS($context, MIME => "text/plain");
    $content->SetContent("OK");

	return $content;
}	# ----------  end of subroutine EmailUsers  ----------


#===============================================================================

=head2 StorePreferences

Store user preferences (column widths, sort field).

=cut

#===============================================================================
sub StorePreferences {
	my ($self, $context) = @_;
	print STDERR "[Bottin::StorePreferences] Entering, prefs: " . Dumper $context->{args} if ($debug);

	my $data = {};
	$data->{success} = 1;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
			[ ['sort_field', 'columns', 'widths'], 'disallow_empty', 'stripxws'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (!@$errors) {
		my $prefs = {};
		$prefs->{sort_field} = $values->{sort_field};
		for my $col (@{$values->{columns}}) {
			$prefs->{column_widths}->{$col} = shift (@{$values->{widths}});
		}

		my $session = $context->GetSession ();
		$session->{persistent}->{Bottin} = $prefs;

		$data->{success} = 1;
	}
	else {
		$data->{errors} = $errors;
	}

	print STDERR "[Bottin::StorePreferences] Leaving.\n" if ($debug);
	return ($data);
}	# ----------  end of subroutine StorePreferences  ----------


#===============================================================================

=head2 RevokeUserFromInstance

Ensure LDAP does not contain inconsistent data (a 'ou' entry matching a deleted instance for example)

=cut

#===============================================================================
sub RevokeUserFromInstance {
	my ($self, $config, $dn) = @_;

	return unless ($config->UseLDAP ());

	print STDERR "[Mioga2::Bottin::RevokeUserFromInstance] Entering, dn: $dn\n" if ($debug);

	my $user = undef;
	try {
		$user = $self->{ldap}->SearchSingleUser($dn);
	}
	otherwise {
		# User not found in LDAP, already deleted (deleted_not_purged)
		$user = undef;
	};

	if ($user) {
		my $instance = $config->GetMiogaIdent ();
		@{$user->{instances}} = grep (!/^$instance$/, @{$user->{instances}});

		if (scalar (@{$user->{instances}}) || !$config->CanModifyLdapPassword ()) {
			$self->{ldap}->ModifyUser (delete ($user->{dn}), $self->{ldap}->TranslateAttributes($user));
		}
		else {
			$self->{ldap}->DeleteUser ($dn);
		}
	}

	print STDERR "[Mioga2::Bottin::RevokeUserFromInstance] Leaving\n" if ($debug);
}	# ----------  end of subroutine RevokeUserFromInstance  ----------


# ============================================================================
# Private methods
# ============================================================================

#===============================================================================

=head2 SynchronizeInstance

Synchronize LDAP / DB for a given instance

=cut

#===============================================================================
sub SynchronizeInstance {
	my ($self, $context, $instname, $dn) = @_;

	$dn = '' unless ($dn);

	my $miogaconffile = $context->GetConfig()->GetMiogaConf()->{file};
	system ("mioga2_nonlocal_synchronizer.pl $miogaconffile $instname $dn");
}	# ----------  end of subroutine SynchronizeInstance  ----------


#===============================================================================

=head2 InitSuperAdminMode

Initialize Super-Admin flag to 0

=cut

#===============================================================================
sub InitSuperAdminMode {
    my ($self, $context) = @_;
	print STDERR "Bottin::InitSuperAdminMode()\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Set flag indicating non-Super-Admin mode
	#-------------------------------------------------------------------------------
	$self->{superadmin} = 0;
}


#===============================================================================

=head2 SwitchInstance

Switch configuration to target instance (dedicated to superadmin mode)

=head3 Incoming Arguments

=over

=item I<$instance>: Target instance ident, may be undef

=back

=head3 Return value

=over

None, updates $self->{config} & $self->{ldap}

=back

=cut

#===============================================================================
sub SwitchInstance {
	my ($self, $instance) = @_;

	if ($self->{superadmin} && $instance) {
		$self->{config} = Mioga2::Config->new ($self->{config}->GetMiogaConf (), $instance);
		if ($self->{config}->UseLDAP ()) {
			$self->{ldap} = Mioga2::LDAP->new ($self->{config});
			$self->{ldap}->BindWith ($self->{config}->GetLDAPBindDN(), $self->{config}->GetLDAPBindPwd());
		}
		else {
			delete ($self->{ldap});
		}
	}
}	# ----------  end of subroutine SwitchInstance  ----------


## check if string is in UTF8 and convert it if needed.
sub CheckUTF8 {
    my ($str) = @_;
    
    my $conv    = Text::Iconv->new('utf8', 'utf8');
    my $tmp_str = $conv->convert($str);
    unless ($tmp_str) {
        my $charset = detect($str) || 'iso-8859-15'; # defaults to latin9
        $conv = Text::Iconv->new($charset, "utf8");
        $str  = $conv->convert($str);
    }
    return $str;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application Mioga2::GlobalBottin Mioga2::Bottin::UI

=head1 COPYRIGHT

Copyright (C) 2009, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
