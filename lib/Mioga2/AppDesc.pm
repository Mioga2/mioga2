# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
AppDesc.pm: Access class to the Application Description.

=head1 DESCRIPTION

This module permits to access to the Mioga application description
and to create new application object.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
package Mioga2::AppDesc;
use strict;

use Locale::TextDomain::UTF8 'appdesc';

use Error qw(:try);
use Data::Dumper;
use Mioga2::Config;
use Mioga2::URI;
use Mioga2::Old::GroupList;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::database;
use Mioga2::Exception::Application;
use Mioga2::Exception::DB;
use Mioga2::XML::Simple;

my $debug = 0;


# ============================================================================

=head2 new ($config, $params)

	Create an application description object.
	return a new Mioga2::AppDesc object.
	
	The three creation methods are :

=over 4

=item create from the application ident

	new Mioga2::AppDesc($config, ident => "AppIdent");

=item create from the application package

	new Mioga2::AppDesc($config, package => "App::Package");

=item create from the application rowid

	new Mioga2::AppDesc($config, rowid => $app_rowid);

=back	
	
=cut

# ============================================================================

sub new {
	my ($class, $config, %param) = @_;

	my $self = {};
	$self->{config} = $config;

	bless($self, $class);	

	if(exists $param{rowid}) {
		$self->InitializeFromRowid($param{rowid});
	}
	elsif(exists $param{ident}) {
		$self->InitializeFromIdent($param{ident});
	}
	elsif(exists $param{package}) {
		$self->InitializeFromPackage($param{package});
	}

	return $self;
}



# ============================================================================

=head2 CreateApplication ()

	Return and application object based on "package" value.

=cut

# ============================================================================

sub CreateApplication {
	my ($self) = @_;
	
	my $app;
	eval "
		require $self->{values}->{package};
		\$app = new $self->{values}->{package}(\$self->{config});
	";

	if($@) {
		throw Mioga2::Exception::Simple("Mioga2::AppDesc::CreateApplication",
										$@);
	}

	return $app;	
}



# ============================================================================

=head2 GetRowid ()

	return the application rowid.

=cut

# ============================================================================

sub GetRowid {
	my ($self) = @_;

	return $self->{values}->{rowid};
}



# ============================================================================

=head2 GetIdent ()

	return the application ident.

=cut

# ============================================================================

sub GetIdent {
	my ($self) = @_;

	return $self->{values}->{ident};
}



# ============================================================================

=head2 GetType ()

	return the application type.

=cut

# ============================================================================

sub GetType {
	my ($self) = @_;

	my $res = SelectSingle($self->{config}->GetDBH(), 
						   "SELECT * FROM m_application_type WHERE rowid = $self->{values}->{type_id}");

	return $res->{ident};
}



# ============================================================================

=head2 GetPackage ()

	return the application package.

=cut

# ============================================================================

sub GetPackage {
	my ($self) = @_;

	return $self->{values}->{package};
}



# ============================================================================

=head2 GetDescription ()

	return the application description.

=cut

# ============================================================================

sub GetDescription {
	my ($self) = @_;

	return $self->{values}->{description};
}



# ============================================================================

=head2 CanBePublic ()

	return true if the application can be used in Public mode.

=cut

# ============================================================================

sub CanBePublic {
	my ($self) = @_;

	return $self->{values}->{can_be_public};
}



# ============================================================================

=head2 IsAllowedForAllGroups  ()

	return true if the application is allowed for all groups.

=cut

# ============================================================================

sub IsAllowedForAllGroups {
	my ($self) = @_;

	my $res = SelectSingle($self->{config}->GetDBH(),
						   "SELECT * FROM m_application, m_instance_application WHERE m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = ".$self->{config}->GetMiogaId()." AND m_application.rowid = $self->{values}->{rowid}");

	return $res->{all_groups};
}



# ============================================================================

=head2 IsEnabledForGroup  ($group)

	return true if the application is enabled for the given group (a Mioga2::Old::Group object).

=cut

# ============================================================================

sub IsEnabledForGroup {
	my ($self, $group) = @_;

	my $group_id = $group->GetRowid();

	my $res = SelectSingle($self->{config}->GetDBH(),
						   "SELECT * FROM m_application_group_allowed WHERE group_id = $group_id AND application_id = $self->{values}->{rowid}");

	return (defined $res ? 1:0);
}


# ============================================================================

=head2 IsEnabledForUser  ($user)

	return true if the application is enabled for the given user (a Mioga2::Old::User object).

=cut

# ============================================================================

sub IsEnabledForUser {
	my ($self, $user) = @_;

	my $user_id = $user->GetRowid();

	my $res = SelectSingle($self->{config}->GetDBH(),
						   "SELECT * FROM m_application_group_allowed WHERE group_id = $user_id AND application_id = $self->{values}->{rowid}");

	return (defined $res ? 1:0);
}



# ============================================================================

=head2 IsAllowedForAllUsers  ()

	return true if the application is allowed for all users.

=cut

# ============================================================================

sub IsAllowedForAllUsers {
	my ($self) = @_;

	my $res = SelectSingle($self->{config}->GetDBH(),
						   "SELECT * FROM m_application, m_instance_application WHERE m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = ".$self->{config}->GetMiogaId()." AND m_application.rowid = $self->{values}->{rowid}");

	return $res->{all_users};
}



# ============================================================================

=head2 IsAllowedForAllResources  ()

	return true if the application is allowed for all resources.

=cut

# ============================================================================

sub IsAllowedForAllResources {
	my ($self) = @_;

	my $res = SelectSingle($self->{config}->GetDBH(),
						   "SELECT * FROM m_application, m_instance_application WHERE m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = ".$self->{config}->GetMiogaId()." AND m_application.rowid = $self->{values}->{rowid}");

	return $res->{all_resources};
}



# ============================================================================

=head2 IsPublicForCurrentGroup  ($context)

	return true if the application is public in the current group.

=cut

# ============================================================================

sub IsPublicForCurrentGroup {
	my ($self, $context, $check_method) = @_;

	$check_method = 1 unless (defined ($check_method));

	my $group = $context->GetGroup();
	my $group_id = $group->GetRowid();

	my @public_methods = $self->GetPublicMethods ();

	my $method = $check_method ? $context->GetMethod () : '.*';

	return ($self->IsPublicForGroup($context->GetConfig(), $group_id) && (grep /^$method$/, @public_methods));
}


# ============================================================================

=head2 IsPublicForGroup  ($config, $group_id)

	return true if the application is public in the given group.

=cut

# ============================================================================

sub IsPublicForGroup {
	my ($self, $config, $group_id) = @_;

	my $res = SelectSingle($self->{config}->GetDBH(),
						   "SELECT is_public FROM m_application_group_allowed WHERE application_id = $self->{values}->{rowid} AND group_id = $group_id");

	if(!defined $res) {
		return 0;
	}

	return $res->{is_public};
}


# ============================================================================

=head2 GetAllowedGroupList  ()

	return a GroupList object representing groups that can use this 
	application.

=cut

# ============================================================================

sub GetAllowedGroupList {
	my ($self) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $result = SelectMultiple($dbh, "SELECT * FROM m_application_group_allowed
	                                            WHERE app_id = $self->{values}->{rowid}");
	
	my @group_id_list = map {$_->{group_id}} @$result;
	
	my $group_list = new Mioga2::Old::GroupList(rowid_list => \@group_id_list);

	return $group_list;
}



# ============================================================================

=head2 GetFunctions ()

	return the list of functions ident in application.

=cut

# ============================================================================

sub GetFunctions {
	my ($self) = @_;
	
	my $dbh = $self->{config}->GetDBH();

	my $result = SelectMultiple($dbh, "SELECT ident FROM m_function 
	                                                WHERE application_id = $self->{values}->{rowid}");

	my @ident_list = map {$_->{ident}} @$result;

	return \@ident_list;
}


# ============================================================================

=head2 GetFunctionsIdsFromIdent ($ident)

	return the rowid of function in application.

=cut

# ============================================================================

sub GetFunctionsIdsFromIdent {
	my ($self, $ident) = @_;
	
	my $dbh = $self->{config}->GetDBH();

	my $result = SelectSingle($dbh, "SELECT rowid FROM m_function
	                                                WHERE ident='$ident' and application_id = $self->{values}->{rowid}");
	
	if(!defined($result))
	{
		throw Mioga2::Exception::Simple("GetFunctionsIdsFromIdent()", __x("Cannot get rowid for Function ident = '{ident}'", ident => $ident));
	}

	return $result->{rowid};
}


# ============================================================================

=head2 GetFunctionsIds ()

	return the list of functions rowids in application.

=cut

# ============================================================================

sub GetFunctionsIds {
	my ($self) = @_;
	
	my $dbh = $self->{config}->GetDBH();

	my $result = SelectMultiple($dbh, "SELECT rowid FROM m_function 
	                                                WHERE application_id = $self->{values}->{rowid}");

	my @rowid_list = map {$_->{rowid}} @$result;

	return \@rowid_list;
		
}


#===============================================================================

=head2 GetPublicMethods

Get list of public methods from application's GetAppDesc method output.

=cut

#===============================================================================
sub GetPublicMethods {
	my ($self) = @_;

	eval "require " . $self->GetPackage (); warn $@ if $@;

	my $apps = $self->GetPackage ()->new;
	my $appdesc = $apps->GetAppDesc();

	if (defined $appdesc->{public_methods}) {
		return (@{$appdesc->{public_methods}});
	} else {
		return;
	}		
}	# ----------  end of subroutine GetPublicMethods  ----------


# ============================================================================

=head2 CanBeSXML ($ident)

	return true if the given method can be executed in SXML mode

=cut

# ============================================================================

sub CanBeSXML {
	my ($self, $ident) = @_;
	
	my $dbh = $self->{config}->GetDBH();

	my $result = SelectSingle($dbh, "SELECT m_method.*, m_method_type.ident AS type FROM m_method, m_function, m_method_type
	                                            WHERE m_method.ident = '$ident' AND ".
								      "               m_method_type.rowid = m_method.type_id AND ".
								      "               m_method.function_id = m_function.rowid AND ".
								      "               m_function.application_id = $self->{values}->{rowid}");

	if(!defined $result) {
		return 0;
	}
	elsif($result->{type} eq 'sxml') {
		return 1;
	}

	return 0;
}



# ============================================================================

=head2 CanBeXML ($ident)

	return true if the given method can be executed in XML mode

=cut

# ============================================================================

sub CanBeXML {
	my ($self, $ident) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $result = SelectSingle($dbh, "SELECT m_method.*, m_method_type.ident AS type FROM m_method, m_function, m_method_type
	                                            WHERE m_method.ident = '$ident' AND ".
								      "               m_method_type.rowid = m_method.type_id AND ".
								      "               m_method.function_id = m_function.rowid AND ".
								      "               m_function.application_id = $self->{values}->{rowid}");

	if(!defined $result) {
		return 0;
	}
	elsif($result->{type} eq 'xml') {
		return 1;
	}

	return 0;
}


# ============================================================================

=head2 GetLocaleName ($user)

	return the application name in the given user language.

=cut

# ============================================================================

sub GetLocaleName {
	my ($self, $user) = @_;

	my $lang = $user->GetLang();

	my $appslang = $self->LoadAppsLang($lang);

	my @res = grep {$_->{ident} eq $self->{values}->{ident}} @{$appslang->{application}};


	if(! @res) {
		return $self->{values}->{ident};
	}
	else {
		return $res[0]->{name};
	}
}

# ============================================================================

=head2 GetLocaleDescription ($user)

	return the application description in the given user language.

=cut

# ============================================================================

sub GetLocaleDescription {
	my ($self, $user) = @_;

	my $lang = $user->GetLang();

	my $appslang = $self->LoadAppsLang($lang);

	my @res = grep {$_->{ident} eq $self->{values}->{ident}} @{$appslang->{application}};


	if(! @res) {
		return $self->{values}->{ident};
	}
	elsif(ref($res[0]->{description}) eq 'HASH') {
		return $res[0]->{name};
	}
	else {
		return $res[0]->{description};
	}

}

# ============================================================================

=head2 GetLocaleForFunction ($function, $user)

	return the function description in the given user language.

=cut

# ============================================================================

sub GetLocaleForFunction {
	my ($self, $function, $user) = @_;

	my $lang = $user->GetLang();

	my $appslang = $self->LoadAppsLang($lang);

	my @res = grep {$_->{ident} eq $self->{values}->{ident}} @{$appslang->{application}};


	if(! @res or !exists $res[0]->{function}) {
		return $function;
	}
	else {
		if(ref($res[0]->{function}) eq 'HASH') {
			$res[0]->{function} = [$res[0]->{function}];
		}

		@res = grep {$_->{ident} eq $function} @{$res[0]->{function}};
		
		if(! @res) {
			return $function;
		}
		else {
			return $res[0]->{name};
		}
	}
}



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 LoadAppsLang ($lang)

	Load applications translation files.

=cut

# ============================================================================

my %LocaleCache;

sub LoadAppsLang {
	my ($self, $lang) = @_;

	my $appslang;
	if(exists $LocaleCache{$lang}) {
		$appslang = $LocaleCache{$lang};
	}
	else {
		my $langdir = $self->{config}->GetLangDir();

		my $file;
		if( -e "$langdir/${lang}.xml") {
			$file = "$langdir/${lang}.xml";
		} 
		else {
			$file = "$langdir/default.xml";
		}
			
		open(F_LANGFILE, $file)
        or die __x("Can't open file {file}: {error}", file => $file, error => $!);

		my $xml;

		{
			local $/ = undef;
			$xml = <F_LANGFILE>;
		}

		close(F_LANGFILE);

		my $xs = new Mioga2::XML::Simple;
		$appslang = $LocaleCache{$lang} = $xs->XMLin($xml);
	}

    return $appslang;
}


# ============================================================================

=head2 InitializeFromRowid ($rowid)

	Initialize the AppDesc object froma application rowid.

=cut

# ============================================================================

sub InitializeFromRowid {
	my ($self, $rowid) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $result = SelectSingle($dbh, "SELECT * FROM m_application, m_instance_application 
	                                          WHERE m_application.rowid = m_instance_application.application_id AND m_application.rowid=$rowid");

	
	if(!defined $result) {
		throw Mioga2::Exception::Application("Mioga2::AppDesc::InitializeFromRowid",
        __x("Application with rowid {rowid} not found", rowid => $rowid));
	}

	$self->{values} = $result;
}



# ============================================================================

=head2 InitializeFromIdent ($ident)

	Initialize the AppDesc object from application ident.

=cut

# ============================================================================

sub InitializeFromIdent {
	my ($self, $ident) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $result = SelectSingle($dbh, "SELECT * FROM m_application, m_instance_application 
	                                          WHERE m_application.rowid = m_instance_application.application_id AND m_application.ident='$ident' AND m_instance_application.mioga_id = ".$self->{config}->GetMiogaId());

	if(!defined $result) {
		throw Mioga2::Exception::Application("Mioga2::AppDesc::InitializeFromIdent",
        __x("Application with ident {ident} not found", ident => $ident));
	}

	$self->{values} = $result;
}



# ============================================================================

=head2 InitializeFromPackage ($package)

	Initialize the AppDesc object from application package.

=cut

# ============================================================================

sub InitializeFromPackage {
	my ($self, $package) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $result = SelectSingle($dbh, "SELECT * FROM m_application, m_instance_application 
	                                          WHERE m_application.rowid = m_instance_application.application_id AND m_application.package='$package' AND m_instance_application.mioga_id = ".$self->{config}->GetMiogaId());

	if(!defined $result) {
		throw Mioga2::Exception::Application("Mioga2::AppDesc::InitializeFromPackage",
        __x("Application with package {package} not found", 'package' => $package));
	}

	$self->{values} = $result;
}




#===============================================================================

=head2 IsSensitiveMethod

Check if a method is sensitive regarding CSRF protection

=head3 Incoming Arguments

=over

None

=back

=head3 Return value

=over

True (sensitive) or false

=over

=back

=back

=cut

#===============================================================================
sub IsSensitiveMethod {
	my ($self, $method) = @_;

	my @non_sensitive_methods = [];
	eval "
		require $self->{values}->{package};
		\@non_sensitive_methods = \@{$self->{values}->{package}->GetAppDesc()->{non_sensitive_methods}};
		";

	if($@) {
		throw Mioga2::Exception::Simple("Mioga2::AppDesc::IsSensitiveMethod", $@);
	}

	return !grep(/$method/, @non_sensitive_methods);
}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application 

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
