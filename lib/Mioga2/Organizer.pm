# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Organizer.pm: The Mioga2 Organizer application

=head1 METHODS DESRIPTION

=cut
package Mioga2::Organizer;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'organizer';

use Mioga2::tools::APIAuthz;
use Mioga2::tools::string_utils;
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;

use Data::Dumper;
use Error qw(:try);
use Mioga2::Exception::Simple;
use POSIX qw(strftime);

use URI::Escape;

require Mioga2::Organizer::Database;
require Mioga2::Organizer::Month;
require Mioga2::Organizer::User;
require Mioga2::Organizer::View;
require Mioga2::Organizer::PeriodicTask;
require Mioga2::Organizer::Meeting;

my $debug = 0;

# ============================================================================
#
# InitializeUserApplication ()
#
#        This method is useful to initialize
#        application values for users
#
#        Return nothing.
#
# ============================================================================

sub InitializeUserApplication {
	my ($self, $conf, $usr, $application) = @_;
	
	$self->Org_InitializeTaskCategories($conf, $application->{DefaultTaskCategories}->[0]);

	$self->Org_InitializeUser($conf->GetDBH(), $usr);
}

# ============================================================================
#
# GetAppDesc ()
#
#	Return the current application description.
#
# ============================================================================

sub GetAppDesc
{
    my %AppDesc = ( ident   => 'Organizer',
	    	name	=> __('Organizer'),
                package => 'Mioga2::Organizer',
                description => __('The Mioga2 Organizer application '),
				type    => 'normal',
                all_groups => 1,
                all_users  => 1,
                can_be_public => 0,
				is_user             => 1,
				is_group            => 1,
				is_resource         => 1,

				functions => {
					'Write' => __('Creation, alteration and deletion functions'),
					'Read'  => __('Consultation functions'),
				},
				func_methods => {

					Write => [
							  'CreateStrictOrPeriodicTask',  # Organizer/User.pm
							  'CreateToDoTask',              # Organizer/User.pm
							  'EditStrictTask',              # Organizer/User.pm
							  'EditTodoTask',                # Organizer/User.pm
							  'EditPeriodicTask',            # Organizer/PeriodicTask.pm
							  'ChangePeriodicTask',          # Organizer/PeriodicTask.pm
							  'CreateMeeting',               # Organizer/Meeting.pm
							  'ProposeMeeting',              # Organizer/Meeting.pm
							  'EditUserList' ,               # Organizer/Meeting.pm
							  'EditResourceList' ,           # Organizer/Meeting.pm
							  'BuildDateList' ,              # Organizer/Meeting.pm
							  "TodoChangeStatus",
							  "TodoDelete",
                              "CreateSimplePeriodicTask",
							  ],
					 Read  => [
							   'DisplayMain',                 # Organizer.pm
							   'ViewDay',                     # Organizer/View.pm
							   'ViewWeek',                    # Organizer/View.pm
							   'ViewMonth',                   # Organizer/Month.pm
							   'ViewStrictTask',              # Organizer/User.pm
							   'ViewPeriodicTask',            # Organizer/PeriodicTask.pm
							   'ViewTodoTask',                # Organizer/PeriodicTask.pm
							   'ViewMeeting',                 # Organizer/Meeting.pm
							   'ManageMeetings',              # todo: Organizer/Meeting.pm
							   'ExportOrganizer',             # 
							   ],
					},
				
				sxml_methods => { Write => [ "SXMLListStringOrPeriodicTasks", "XMLCreateTaskForUser", 'XMLDeleteTask' ] },
				
				xml_methods  => { Read  => [ "XMLGetTasksModifiedSince", "XMLGetTasksRowids", "XMLGetTaskCategories", "XMLListStringOrPeriodicTasks" ],
							      Write => [ 'XMLImportTasks', 'XMLDeleteTasks']
								},				

				non_sensitive_methods => [
					'CreateStrictOrPeriodicTask',
					'CreateToDoTask',
					'EditStrictTask',
					'EditTodoTask',
					'EditPeriodicTask',
					'ChangePeriodicTask',
					'CreateMeeting',
					'ProposeMeeting',
					'EditUserList' ,
					'EditResourceList' ,
					'BuildDateList' ,
					"TodoChangeStatus",
					"TodoDelete",
					"CreateSimplePeriodicTask",
					'DisplayMain',
					'ViewDay',
					'ViewWeek',
					'ViewMonth',
					'ViewStrictTask',
					'ViewPeriodicTask',
					'ViewTodoTask',
					'ViewMeeting',
					'ManageMeetings',
					'ExportOrganizer',
				]
                );
    
	return \%AppDesc;
}

# ============================================================================

=head2 DisplayMain ()

	Main Mioga entry point.
	
=head3 Generated XML :
    

View* Functions have standard XML headers :

<ViewDay|ViewWeek|ViewMonth ...>

	<miogacontext>
	   <!-- 
	      Here must be the standard miogacontext and calendar headers.
          See Mioga2::RequestContext->GetXML for more details.
         -->
	</miogacontext>

  <!--
       The first part of the XML file is always present.
       After Calendar-next, it depends on current view mode (Day, Month, Week) and 
       contains the tasks description.

       See Organizer::View for more details on these sections.

       -->
  

  <!-- 
      Calendar is used to build the tiny calendar on top left corner of Organizer page.
       - firstDay is the number of the first day of week of the current month. 0 = monday, 6 = sunday.
       
      More than one Calendar entry can be present when the current week is on two different month.
       
       -->

  <Calendar month="05" year="2005" firstDay="06">
    <DayNames>
      <DayName dayInWeek="1"/>
      <DayName dayInWeek="2"/>
      <DayName dayInWeek="3"/>
      <DayName dayInWeek="4"/>
      <DayName dayInWeek="5"/>
      <DayName dayInWeek="6"/>
      <DayName dayInWeek="7"/>
    </DayNames>
    <Week number="17" year="2005">
      <Day/>
      <Day/>
      <Day/>
      <Day/>
      <Day/>
      <Day/>
      <Day number="01" isWorkingDay="0"/>
    </Week>
    <Week number="18" year="2005">
      <Day number="02" isWorkingDay="1"/>
      <Day number="03" isWorkingDay="1"/>
      <Day number="04" isWorkingDay="1"/>
      <Day number="05" isWorkingDay="0"/>
      <Day number="06" isWorkingDay="1"/>
      <Day number="07" isWorkingDay="0"/>
      <Day number="08" isWorkingDay="0"/>
    </Week>
    <Week number="19" year="2005">
      <Day number="09" isWorkingDay="1"/>
      <Day number="10" isWorkingDay="1"/>
      <Day number="11" isWorkingDay="1"/>
      <Day number="12" isWorkingDay="1"/>
      <Day number="13" isWorkingDay="1"/>
      <Day number="14" isWorkingDay="0"/>
      <Day number="15" isWorkingDay="0"/>
    </Week>
    <Week number="20" year="2005">
      <Day number="16" isWorkingDay="0"/>
      <Day number="17" isWorkingDay="1"/>
      <Day number="18" isWorkingDay="1"/>
      <Day number="19" isWorkingDay="1"/>
      <Day number="20" isWorkingDay="1"/>
      <Day number="21" isWorkingDay="0"/>
      <Day number="22" isWorkingDay="0"/>
    </Week>
    <Week number="21" year="2005">
      <Day number="23" isWorkingDay="1"/>
      <Day number="24" isWorkingDay="1"/>
      <Day number="25" isWorkingDay="1"/>
      <Day number="26" isWorkingDay="1"/>
      <Day number="27" isWorkingDay="1"/>
      <Day number="28" isWorkingDay="0"/>
      <Day number="29" isWorkingDay="0"/>
    </Week>
    <Week number="22" year="2005">
      <Day number="30" isWorkingDay="1"/>
      <Day number="31" isWorkingDay="1"/>
      <Day/>
      <Day/>
      <Day/>
      <Day/>
      <Day/>
    </Week>
  </Calendar>

  <!-- 
       OtherOrganizer : the list of other calendar (user group or resource) the current user can access.
       -->
  <OtherOrganizer selected="admin">
    <OtherUserList>
      <user ident="admin">Mioga Administrator</user>
    </OtherUserList>
    <OtherResourceList>
      <resource ident="myresource">My Resource</user>
    </OtherResourceList>
    <OtherGroupList>
      <group ident="mygroup">My Group</user>
    </OtherGroupList>
  </OtherOrganizer>
  
  
  <!-- 
       Calendar-previous and Calendar-next describe the previous and next organizer page.
       In this case, in Day View, it s yesterday and tomorrow
       method can be : ViewDay, ViewMonth or ViewWeek.

       These values should be used to create link to previous and next page like this :
       
       {$private_bin_uri}/Organizer/{@method}?{@attribute}={@value}
       
       -->
  <Calendar-previous method="ViewDay" attribute="day" value="2005-4-06"/>
  <Calendar-next     method="ViewDay" attribute="day" value="2005-6-06"/>



<!--
    List of todo tasks.

    -->

<ToDoTasks canWrite="1">

   <ToDoTask priority="very urgent" taskrowid="12" 
             private="0" bgcolor="#CCCCCC" fgcolor="#000000" 
             method="Organizer/EditTodoTask?taskrowid" 
             done="0">
      todo2
   </ToDoTask>

   <ToDoTask priority="urgent" taskrowid="11"
             private="0" bgcolor="#CCCCCC" fgcolor="#000000" 
             method="Organizer/EditTodoTask?taskrowid" 
             done="0">
      todo1
   </ToDoTask>

</ToDoTasks>



  <!--
    List of meeting.

    -->

  <MeetingList canWrite="1">

   <Meeting rowid="1" private="0" fgcolor="#000000" bgcolor="#CCCCCC" canWrite="1">
      Meeting 1
   </Meeting>

   <Meeting rowid="2" private="0" fgcolor="#000000" bgcolor="#CCCCCC" canWrite="1">
      Meeting 2
   </Meeting>

  </MeetingList>


  <!--
      View Day, Week, Month dependent XML 
    -->


</ViewDay|ViewWeek|ViewMonth>


=cut

# ============================================================================

sub DisplayMain
{
   my ($self, $context) = @_;
   my $viewState;

   my $session = $context->GetSession();
   my $config = $context->GetConfig();
  	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'month', 'year', 'day', 'go', 'method', 'new_organizer' ], 'stripxws', 'allow_empty'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Organizer::DisplayMain", ac_FormatErrors($errors));
	}

   if( st_ArgExists($context, 'change_organizer')) {
	   my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	
	   if($context->{args}->{new_organizer} eq '') {
		   $context->{args}->{new_organizer} = $context->GetGroup()->GetIdent();
	   }
   
	   my $uri = new Mioga2::URI($config, group       => $context->{args}->{new_organizer},
                                          public      => 0,
                                          application => 'Organizer',
								          method      => 'DisplayMain');

	   $content->SetContent($uri->GetURI());
	   return $content;
   }

   ##===========================================================================
   ## Info from the session (or from TopControl)
   ##===========================================================================
   $self->InitializeViewState($context);

   $viewState = $session->{organizer}{viewState};

   my $method = $viewState->{method};

   $self->$method( $context );
}


# ============================================================================

=head2 ExportOrganizer ()

	Export Calendar file.
	
    Return an iCalendar file describing the tasks in calendar.

=cut

# ============================================================================

sub ExportOrganizer
{
	my ($self, $context) = @_;
	
	if(st_ArgExists($context, 'export_act')) {
		
		my $start_date;
		if($context->{args}->{start_date} eq 'epoch') {
			$start_date = '1970-01-01 00:00:00';
		}
		elsif ($context->{args}->{start_date} eq 'user_date') {
			$start_date = sprintf("%04d-%02d-%02d",
								  $context->{args}->{other_date_year},
								  $context->{args}->{other_date_month}, 
								  $context->{args}->{other_date_day});
		}
		
		my $result;
		my $mime = 'text/plain';
		if($context->{args}->{format} eq 'ICAL') {
			$result = $self->Org_ExportICal($context, $context->GetGroup()->GetRowid(), $start_date);
			$mime = 'text/calendar';

		}
		
		my $content = new Mioga2::Content::ASIS($context, MIME => $mime, filename => 'mioga.ics');
		$content->SetContent($result);
		
		return $content;
	}
	
	my $xml = '<?xml version="1.0"?>';
	$xml .= "<ExportOrganizer>";
	$xml .= $context->GetXML();
	$xml .= "<referer>".st_FormatXMLString($context->GetReferer())."</referer>";
	
	$xml .= "<fields>";

	foreach my $field (qw(start_date other_date_day other_date_month other_date_year format)) {
		if(exists $context->{args}->{$field}) {
			$xml .= "<$field>".st_FormatXMLString($context->{args}->{$field})."</$field>";
		}
	}

	if(!exists $context->{args}->{start_date}) {
		$xml .= "<start_date>last_synchro</start_date>";
		
	}
	
	$xml .= "</fields>";
	
	$xml .= "<available_format>";
	$xml .= '<format ident="ICAL">iCalendar</format>';
	$xml .= "</available_format>";
	

	$xml .= "<start_date>";
	$xml .= '<date>last_synchro</date>';
	$xml .= '<date>epoch</date>';
	$xml .= '<date>user_date</date>';
	$xml .= "</start_date>";
	

	$xml .= "</ExportOrganizer>";
	
	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'organizer.xsl', locale_domain => 'organizer_xsl');
	$content->SetContent($xml);
	return $content;
}


# ============================================================================
#
# SXMLListStringOrPeriodicTasks ()
#
#	Return List of tasks for a user.
#	
# ============================================================================

sub SXMLListStringOrPeriodicTasks
{
   my ($self, $context) = @_;
   my $xml = $self->_XMLUserTasks($context);
   my $content = new Mioga2::Content::XML($context,
										  recipient_keyident => $context->GetSender());
   $content->SetContent($xml);
   return $content;
}

# ============================================================================
#
# XMLListStringOrPeriodicTasks ()
#
#	Return List of tasks for a user.
#	
# ============================================================================

sub XMLListStringOrPeriodicTasks
{
   my ($self, $context) = @_;
   my $xml = $self->_XMLUserTasks($context);
   my $content = new Mioga2::Content::XML($context);
   $content->SetContent($xml);
   return $content;
}


# ============================================================================
#
# XMLUserTasks ()
#
#	Return List of tasks for a user.
#	
# ============================================================================

sub _XMLUserTasks
{
   my ($self, $context) = @_;

   my $user = new Mioga2::Old::User($context->GetConfig(), ident => $context->{args}->{user_ident});

   my $tasks = $self->Org_ListStrictPeriodicTasks($context, $user->GetRowid(), 'GMT', $context->{args}->{start}, $context->{args}->{stop} );

   my $xml = "<Tasks>";

   foreach my $task (@$tasks) {
	   $xml .= "<task>";
	   foreach my $key (keys %$task) {
		   $xml .= "<$key>".st_FormatXMLString($task->{$key})."</$key>";
	   }
	   $xml .= "</task>";
   }

   $xml   .= "</Tasks>";
   return $xml;
}

# ============================================================================
#
# XMLCreateTaskForUser ()
#
#	Create a simple (not periodic) task for a user.
#	
# ============================================================================

sub XMLCreateTaskForUser
{
   my ($self, $context) = @_;

   my $user = new Mioga2::Old::User($context->GetConfig(), ident => $context->{args}->{user_ident});

   my $values = { group_id => $user->GetRowid(),
		  category_id => $self->Org_GetDefaultTaskCat($context->GetConfig()),
		  description => $context->{args}->{description},
		  name => $context->{args}->{name},
		  start => $context->{args}->{start},
		  duration => $context->{args}->{duration},
	      };

   my $rowid = $self->Org_CreateTask($context, 'planned', $values );

   my $xml = "<task><rowid>$rowid</rowid></task>";

   my $content = new Mioga2::Content::XML($context,
										  recipient_keyident => $context->GetSender());
   
   $content->SetContent($xml);
   return $content;
}


# ============================================================================
#
# XMLDeleteTask ()
#
#	Delete a simple (not periodic) task.
#	
# ============================================================================

sub XMLDeleteTask
{
   my ($self, $context) = @_;

   my $rowid = $self->Org_DeleteTask($context, $context->{args}->{task_id});

   my $xml = "<ok/>";

   my $content = new Mioga2::Content::XML($context,
										  recipient_keyident => $context->GetSender());
   $content->SetContent($xml);
   return $content;
}



# ============================================================================
#
# XMLGetTasksModifiedSince ()
#
#	return tasks modified or created since the given date
#	
# ============================================================================

sub XMLGetTasksModifiedSince {
	my ($self, $context) = @_;

	my $xml = $self->Org_ExportToXML($context, $context->GetGroup()->GetRowid(), $context->{args}->{date}, $context->{args}->{synchro_ident});

	print STDERR "XMLGetTasksModifiedSince xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XML($context);

	$content->SetContent($xml);
	return $content;
}





# ============================================================================
#
# XMLGetTasksRowids ()
#
#	return xml describing tasks rowids.
#	
# ============================================================================

sub XMLGetTasksRowids {
	my ($self, $context) = @_;

	my $xml = $self->Org_ExportRowidsToXML($context, $context->GetGroup()->GetRowid(), $context->{args}->{synchro_ident});

	print STDERR "XMLGetTasksRowids xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XML($context);
	
	$content->SetContent($xml);
	return $content;
}



# ============================================================================
#
# XMLGetTaskCategories ()
#
#	return xml describing tasks categories.
#	
# ============================================================================

sub XMLGetTaskCategories {
	my ($self, $context) = @_;

	my $xml = $self->Org_ExportTaskCategoryToXML($context);

	print STDERR "XMLGetTaskCategories xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XML($context);

	$content->SetContent($xml);
	return $content;
}



# ============================================================================
#
# XMLImportTasks ()
#
#	return xml describing the list of imported tasks rowid.
#	
# ============================================================================

sub XMLImportTasks {
	my ($self, $context) = @_;

	my $xml = $self->Org_ImportFromXML($context, $context->GetGroup()->GetRowid(), $context->{args}->{data}, $context->{args}->{synchro_ident});

	print STDERR "XMLImportTasks xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XML($context);
	$content->SetContent($xml);
	return $content;
}



# ============================================================================
#
# XMLDeleteTasks ()
#
#	return xml describing the list of imported tasks rowid.
#	
# ============================================================================

sub XMLDeleteTasks {
	my ($self, $context) = @_;

	my $xml = $self->Org_DeleteFromXML($context, $context->GetGroup()->GetRowid(), $context->{args}->{data});

	print STDERR "XMLDeleteTasks xml = $xml\n" if ($debug);
	my $content = new Mioga2::Content::XML($context);
	$content->SetContent($xml);

	return $content;
}


# ============================================================================
#
# DeleteGroupData ($config, $group_id)
#
#	Remove group data in database when a group is deleted.
#
#	This function is called when a group/user/resource is deleted.
#
#	--> SEE Organizer/Database.pm
#
# ============================================================================



# ============================================================================
#
# PRIVATE METHODS DESCRIPTION
#
# ============================================================================


# ============================================================================
#
# InitializeViewState ()
#
#	Initialize session from request arguments.
#	
# ============================================================================

sub InitializeViewState {
	my ($self, $context) = @_;
	
	my $session = $context->GetSession();
	
	if(! exists $context->{args}{method}) 
	{
		if(exists $session->{persistent}->{organizer}{method}) {
			$context->{args}{method} = $session->{persistent}->{organizer}{method};
		}
		else {
			$context->{args}{method} = 'ViewDay';
		}
		delete $session->{organizer}{viewState};
	}
	
	my $viewState;
	$viewState->{method} = $context->{args}{method};
	
	my ($day, $month, $year);
	
	if (defined $context->{args}{day} and defined $context->{args}{month} and defined $context->{args}{year} )
	{
		$day   = int($context->{args}{day});
		$month = int($context->{args}{month});
		$year  = int($context->{args}{year});
	}
	elsif($context->{args}{method} eq $session->{persistent}->{organizer}{method} and 
	   exists $session->{organizer}{viewState}) {
		return;
	}

	else
	{
		($year, $month, $day) = split ("-", strftime("%Y-%m-%d", localtime()));
	}

	if( $context->{args}{method} eq "ViewDay" )
	{
		$viewState->{method} = "ViewDay";
		$viewState->{parameter} = "$year-$month-$day";
	}
	elsif( $context->{args}{method} eq "ViewWeek" )
	{
		$viewState->{method} = "ViewWeek";
		$viewState->{parameter} = strftime("%Y-%V", 0, 0, 0, $day, $month-1, $year-1900);
	}
	elsif( $context->{args}{method} eq "ViewMonth" )
	{
		$viewState->{method} = "ViewMonth";
		$viewState->{parameter} = "$year-$month";
	}
	else
	{
		throw Mioga2::Exception::Simple("Mioga2::Organizer::InitializeViewState", 
        __x("No such method for arg: '{arg}'", arg => $context->{args}{method}));
	}

	$session->{organizer}{viewState} = $viewState;
	$session->{persistent}->{organizer}{method} = $viewState->{method};	
}


# ============================================================================
#
# MiogaResponse ()
#
#	Return Mioga2::Content::XSLT
#	
# ============================================================================

sub _MiogaResponse
{
   my ($self, $context, $xml, $stylesheet) = @_;

   $stylesheet = 'organizer.xsl' unless defined($stylesheet);

   my $content = new Mioga2::Content::XSLT($context, stylesheet => $stylesheet, locale_domain => 'organizer_xsl');
   $content->SetContent($xml);

   return $content;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
