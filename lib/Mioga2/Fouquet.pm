#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Fouquet.pm: the new-generation Mioga2 group animation application

=head1 DESCRIPTION

The Mioga2::Fouquet application is used for group animation.

=head1 METHODS DESCRIPTION

=cut

#
# ============================================================================

package Mioga2::Fouquet;
use base qw(Mioga2::Application);
use strict;
use warnings;
use open ':encoding(utf8)';

use Data::Dumper;

use Error qw(:try);

use Locale::TextDomain::UTF8 'fouquet';
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::Content::XSLT;
use Mioga2::Exception::Simple;
use Mioga2::UserList;
use Mioga2::TeamList;
use Mioga2::ApplicationList;
use Mioga2::ProfileList;
use Mioga2::Theme;

my $debug = 0;


#===============================================================================

=head1 INTERFACE METHODS

=cut

#===============================================================================

=head2 DisplayMain

Main entry point, display synthesis.

=head3 Generated XML

=over

	<DisplayMain>
		<miogacontext>See Mioga2::RequestContext</miogacontext>
		<group>...</group>
	</DisplayMain>

=back

=head3 Data

=over

The "group" tag contains JSON-encoded group data, obtained from GetGroup.

=back

=cut

# ============================================================================
sub DisplayMain {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Fouquet::DisplayMain]\n" if ($debug);

	my $data = { DisplayMain => $context->GetContext () };

	$data->{DisplayMain}->{group} = Mioga2::tools::Convert::PerlToJSON ($self->GetGroup ($context));

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "[Mioga2::Fouquet::DisplayMain] XML: $xml\n" if ($debug);

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'fouquet.xsl', locale_domain => 'fouquet_xsl');
	$content->SetContent($xml);
	return $content;
}
# ============================================================================


# ===============================================================================

=head1 WEBSERVICE METHODS DESCRIPTION

=cut

# ===============================================================================


#===============================================================================

=head2 GetGroup

Get group data as XML or JSON

=head3 Incoming Arguments

=over

None, works on current group.

=back

=head3 Return value

=over

A Perl hash containing group data that will be converted to XML or JSON by Mioga2::Application::WSWrapper.

=over

	{
		rowid: ...,
		ident: ...,
		disk_space_used: ...,
		default_app_id: ...,
		animator: {
			rowid: ...,
			firstname: ...,
			lastname: ...,
			email: ...
		},
		default_profile: {
			rowid: ...,
			ident: ...,
			rights: ...
		},
		lang: {
			rowid: ...,
			ident: ...,
			name: ...
		},
		theme: {
			rowid: ...,
			ident: ...,
			name: ...
		},
		teams: [
			{
				rowid: ...,
				ident: ...,
				user_count: ...,
				profile: {
					rowid: ...,
					ident: ...
				}
			},
			{
				...
			}
		],
		users: [
			{
				rowid: ...,
				ident: ...,
				firstname: ...,
				lastname: ...,
				email: ...,
				last_connection: ...,
				profile: {
					rowid: ...,
					ident: ...
				}
			},
			{
				...
			}
		],
		profiles: [
			{
				rowid: ...,
				ident: ...
			}
		],
		applications: [
			{
				ident: ...,
				label: ...,
				description: ...,
				selected: true / false,
				public: true / false
			},
			{
				...
			}
		],
		resources: [
			{
				ident: ...,
				location: ...,
				selected: true / false
			},
			{
				...
			}
		]
	}

=back

=back

=cut

#===============================================================================
sub GetGroup {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Fouquet::GetGroup] Entering for group rowid " . $context->GetGroup ()->Get ('rowid') . "\n" if ($debug);

	my $data = { };

	my $config = $context->GetConfig ();
	my $group = $context->GetGroup ();

	# Get group attributes
	for my $attr (qw/rowid ident default_app_id disk_space_used/) {
		$data->{$attr} = $group->Get ($attr);
	}

	# Get group animator
	my $animator = Mioga2::UserList->new ($config, { attributes => { rowid => $group->Get ('anim_id') } });
	$data->{animator} = { };
	for my $attr (qw/rowid firstname lastname email/) {
		$data->{animator}->{$attr} = $animator->Get ($attr);
	}

	# Default profile
	my $profile = Mioga2::ProfileList->new ($config, { attributes => { rowid => $group->Get ('default_profile_id') } });
	$data->{default_profile} = { };
	for my $attr (qw/rowid ident rights/) {
		$data->{default_profile}->{$attr} = $profile->Get ($attr);
	}

	# Languages
	$data->{lang} = $self->DBGetLang ($group->Get ('lang_id'));
	$data->{langs} = $self->DBGetLang ();

	# Themes
	$data->{theme} = {
		rowid => $group->Get ('theme_id'),
		ident => $group->Get ('theme_ident'),
		name  => $group->Get ('theme')
	};
	my $themes = Mioga2::Theme->new ($config);
	$data->{themes} = $themes->GetList ();

	# Users
	my $users = $group->GetInvitedUserList (short_list => 1);
	@{$data->{users}} = $users->GetUsers ();
	# Expand profile information
	for my $user (@{$data->{users}}) {
		my $profile = Mioga2::ProfileList->new ($config, { attributes => { rowid => $user->{profile_id} }});
		$user->{profile} = $profile->GetProfiles ();
	}
	# Append non-members to list
	$users->Revert ();
	if ($users->Count () <= 10) {
		my @users = $users->GetUsers ();
		for my $user (@users) {
			push (@{$data->{users}}, $user);
		}
	}
	else {
		$data->{non_member_users} = $users->Count ();
	}

	# Teams
	my $teams = $group->GetInvitedTeamList ();
	@{$data->{teams}} = $teams->GetTeams ();
	# Expand profile information
	for my $team (@{$data->{teams}}) {
		my $profile = Mioga2::ProfileList->new ($config, { attributes => { rowid => $team->{profile_id} }});
		$team->{profile} = $profile->GetProfiles ();
	}
	# Append non-members to list
	$teams->Revert ();
	if ($teams->Count () <= 10) {
		my @teams = $teams->GetTeams ();
		for my $team (@teams) {
			push (@{$data->{teams}}, $team);
		}
	}
	else {
		$data->{non_member_teams} = $teams->Count ();
	}

	# Profiles
	my $profiles = Mioga2::ProfileList->new ($config, { attributes => { group => $group } });
	$data->{profiles} = $profiles->GetProfiles ();

	# Applications
	my $applications = Mioga2::ApplicationList->new ($config, { attributes => { groups => $group, only_active => 1 } });
	@{$data->{applications}} = $applications->GetApplications ();
	map { $_->{selected} = 'true' } @{$data->{applications}};
	$applications->Revert ();
	my @apps = $applications->GetApplications ();
	for my $app (@apps) {
		push (@{$data->{applications}}, $app);
	}

	# Protected apps
	@{$data->{protected_apps}} = @Mioga2::GroupList::ProtectedApplications;

	# Resources
	my $resources = Mioga2::ResourceList->new ($config, { attributes => { group => $group } });
	@{$data->{resources}} = $resources->GetResources ();
	map { $_->{selected} = 'true' } @{$data->{resources}};
	$resources->Revert ();
	for my $resource ($resources->GetResources ()) {
		push (@{$data->{resources}}, $resource);
	}

	print STDERR "[Mioga2::Fouquet::GetGroup] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetGroup  ----------


#===============================================================================

=head2 SetGroup

Modify group attributes

=head3 Incoming Arguments

=over

=item I<rowid>: The group rowid

=item I<ident>: The group ident

=item I<default_profile_id>: The group's default profile

=item I<default_app_id>: The group's default application

=item I<lang_id>: The group's language

=item I<theme_id>: The group's theme

=item I<applications>: An array of application rowids that are enabled in group

=item I<resources> (I<optional>): An array of resource rowids that are enabled in group

=item I<users> (I<optional>): An array of user rowids that belong to the group

=item I<teams> (I<optional>): An array of team rowids that belong to the group

=back

=head3 Return value

=over

A perl hash to be converted to XML or JSON:

	{
		success => 'true' / 'false',
		message => ...,
		errors  => [
			{
				field   => ...,
				message => ...
			}
		]
	}

=back

=cut

#===============================================================================
sub SetGroup {
	my ($self, $context) = @_;
	print STDERR '[Mioga2::Fouquet::SetGroup] Entering, args: ' . Dumper $context->{args} if ($debug);

	my $config = $context->GetConfig ();
	my $group = $context->GetGroup ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'ident' ], 'disallow_empty', 'stripxws' ],
			[ [ 'rowid' ], 'disallow_empty', 'want_int', ['match', $group->Get ('rowid')] ],
			[ [ 'default_profile_id' ], 'disallow_empty', 'want_int', ['mioga_profile_id', $group] ],
			[ [ 'theme_id' ], 'disallow_empty', 'want_int', ['mioga_theme_id', $config] ],
			[ [ 'lang_id' ], 'disallow_empty', 'want_int', ['mioga_lang_id', $config] ],
			[ [ 'default_app_id' ], 'disallow_empty', 'want_int', ['mioga_app_id', $group] ],
			[ [ 'users', 'teams' ], 'allow_empty', 'want_int' ],
			[ [ 'applications' ], 'disallow_empty' ],
			[ [ 'resources' ], 'allow_empty' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Default values
	my $data = {
		success => 'false',
		message => __('Group modification failed')
	};

	if (!@$errors) {
		# Set group attributes
		for my $attr (keys (%$values)) {
			$group->Set ($attr, $values->{$attr});
		}

		# Store group
		$group->Store ();

		# Set user members
		my $users = Mioga2::UserList->new ($config, { attributes => { rowid => $values->{users} } });
		my %users = map { $_->{ident} => '' } $users->GetUsers ();
		$context->GetGroup ()->InviteUsers (%users);

		# Set team members
		my $teams = Mioga2::TeamList->new ($config, { attributes => { rowid => $values->{teams} } });
		my %teams = map { $_->{ident} => '' } $teams->GetTeams ();
		$context->GetGroup ()->InviteTeams (%teams);

		# Set application list
		my $apps = Mioga2::ApplicationList->new ($config, { attributes => { rowid => $values->{applications} } });
		$group->ActivateApplications (map { { ident => $_->{ident}, active => 1 } } $apps->GetApplications ());

		# Set resource list
		my $resources = Mioga2::ResourceList->new ($config, { attributes => { rowid => $values->{resources} } });
		$group->SetResources ($resources);

		$data->{success} = 'true';
		$data->{message} = __('Group successfully modified.');
		$data->{group} = $self->GetGroup ($context);
	}
	else {
		print STDERR '[Mioga2::Fouquet::SetGroup] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	print STDERR '[Mioga2::Fouquet::SetGroup] Leaving, data: ' . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetGroup  ----------


#===============================================================================

=head2 GetUser

Get user details

=head3 Incoming Arguments

=over

=item I<rowid>: The user's rowid

=back

=head3 Return value

=over

A perl hash to be converted to JSON or XML

	$data = {
		rowid: ...,
		ident: ...,
		fistname: ...,
		lastname: ...,
		email: ...,
		last_connection: ...,
		profile: {
			rowid: ...,
			ident: ...
		},
		teams: [
			{
				rowid: ...,
				ident: ...
			}
		]
		profiles: [
			{
				rowid: ...,
				ident: ...
			},
			...
		],
		applications: {
			profile: [
				{
					ident: ...,
					label: ...,
					functions: [
						{
							ident: ...,
							label: ...
						},
						...
					]
				},
				...
			],
			teams: [
				{
					rowid: ...,
					ident: ...,
					rights: [
						{
							ident: ...,
							label: ...,
							functions: [
								{
									ident: ...,
									label: ...
								},
								...
							]
						},
						...
					]
				},
				...
			]
		},
		filesystem: [
			{
				path: ...,
				user: ...,      # Access key for user
				profile: ...,   # Access key for profile
				teams: [
					{
						rowid: ...,
						access: ...  # Access key for team
					}
				]
			}
		]
	}

=back

=cut

#===============================================================================
sub GetUser {
	my ($self, $context) = @_;
	print STDERR '[Mioga2::Fouquet::GetUser] Entering, args: ' . Dumper $context->{args} if ($debug);

	my $config = $context->GetConfig ();
	my $group = $context->GetGroup ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', ['mioga_user_id', $config] ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $user = Mioga2::UserList->new ($config, { attributes => { rowid => $values->{rowid}, groups => $group } });

		if (!$user->Count ()) {
			# No user matched by previous initialization, user is not directly member of group
			$user = Mioga2::UserList->new ($config, { attributes => { rowid => $values->{rowid} } });
		}
		else {
			if ($user->Get ('rowid') == $group->Get ('anim_id')) {
				$data->{is_animator} = 'true';
			}
		}

		# User attributes
		for my $attr (qw/rowid ident firstname lastname email last_connection/) {
			$data->{$attr} = $user->Get ($attr);
		}

		# User profile
		my $profile = Mioga2::ProfileList->new ($config, { attributes => { group => $group, ident => $user->Get ('profile') } });
		if ($profile->Count ()) {
			for my $attr (qw/rowid ident/) {
				$data->{profile}->{$attr} = $profile->Get ($attr);
			}
		}

		# Profiles
		my $profiles = Mioga2::ProfileList->new ($config, { attributes => { group => $group } });
		$data->{profiles} = $profiles->GetProfiles ();

		# User teams
		my $teams = Mioga2::TeamList->new ($config, { attributes => { users => $user, groups => $group } });
		my @teams = $teams->GetTeams ();
		for my $team (@teams) {
			push (@{$data->{teams}}, {
				rowid => $team->{rowid},
				ident => $team->{ident}
			});
		}

		# Application access rights
		$data->{applications} = $self->DBGetApplicationRights ($user);

		# Filesystem access rights
		$data->{filesystem} = $self->DBGetFilesystemRights ($user);
	}
	else {
		print STDERR '[Mioga2::Fouquet::GetUser] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			});
		}
	}

	print STDERR '[Mioga2::Fouquet::GetUser] Leaving, data: ' . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetUser  ----------


#===============================================================================

=head2 SetUser

Modify user profile

=head3 Incoming Arguments

=over

=item I<rowid>: The user's rowid

=item I<profile>: The profile's rowid

=back

=head3 Return value

=over

A Perl hash to be converted to JSON or XML:

	$data = {
		success: true / false,
		message: ...
	}

=back

=cut

#===============================================================================
sub SetUser {
	my ($self, $context) = @_;
	print STDERR '[Mioga2::Fouquet::SetUser] Entering, args: ' . Dumper $context->{args} if ($debug);

	my $config = $context->GetConfig ();
	my $group = $context->GetGroup ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', ['mioga_user_id', $config] ],
			[ [ 'profile' ], 'disallow_empty', ['mioga_profile_id', $group] ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $user = Mioga2::UserList->new ($config, { attributes => { rowid => $values->{rowid} } });
		my $profile = Mioga2::ProfileList->new ($config, { attributes => { rowid => $values->{profile} } });
		$group->AffectProfileToUsers ($user, $profile->Get ('ident'));

		$data->{success} = 'true';
		$data->{message} = __x('User now has profile {profile}.', profile => $profile->Get ('ident'));
		$data->{user} = $self->GetUser ($context);
	}
	else {
		print STDERR '[Mioga2::Fouquet::SetUser] Got errors: ' . Dumper $errors;
		$data->{success} = 'false';
		$data->{message} = __('User profile change failed.');
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			});
		}
	}

	print STDERR '[Mioga2::Fouquet::SetUser] Leaving, data: ' . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetUser  ----------


#===============================================================================

=head2 GetTeam

Get team details

=head3 Incoming Arguments

=over

=item I<rowid>: The team's rowid

=back

=head3 Return value

=over

A perl hash to be converted to JSON or XML

	$data = {
		rowid: ...,
		ident: ...,
		profile: {
			rowid: ...,
			ident: ...
		},
		users: [
			{
				rowid: ...,
				ident: ...,
				firstname: ...,
				lastname: ...,
				email: ...
			}
		]
		profiles: [
			{
				rowid: ...,
				ident: ...
			},
			...
		],
		applications: {
			profile: [
				{
					ident: ...,
					label: ...,
					functions: [
						{
							ident: ...,
							label: ...
						},
						...
					]
				},
				...
			]
		},
		filesystem: [
			{
				path: ...,
				team: ...,      # Access key for team
			}
		]
	}

=back

=cut

#===============================================================================
sub GetTeam {
	my ($self, $context) = @_;
	print STDERR '[Mioga2::Fouquet::GetTeam] Entering, args: ' . Dumper $context->{args} if ($debug);

	my $config = $context->GetConfig ();
	my $group = $context->GetGroup ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', ['mioga_team_id', $config] ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $team = Mioga2::TeamList->new ($config, { attributes => { rowid => $values->{rowid}, groups => $group } });

		# Team attributes
		for my $attr (qw/rowid ident/) {
			$data->{$attr} = $team->Get ($attr);
		}

		# Team profile
		my $profile = Mioga2::ProfileList->new ($config, { attributes => { group => $group, ident => $team->Get ('profile') } });
		for my $attr (qw/rowid ident/) {
			$data->{profile}->{$attr} = $profile->Get ($attr);
		}

		# Profiles
		my $profiles = Mioga2::ProfileList->new ($config, { attributes => { group => $group } });
		$data->{profiles} = $profiles->GetProfiles ();

		# Team users
		my $users = Mioga2::UserList->new ($config, { attributes => { teams => $team } });
		my @users = $users->GetUsers ();
		for my $user (@users) {
			push (@{$data->{users}}, {
				rowid => $user->{rowid},
				ident => $user->{ident},
				firstname => $user->{firstname},
				lastname => $user->{lastname},
				email => $user->{email}
			});
		}

		# Application access rights
		$data->{applications} = $self->DBGetApplicationRights ($team);

		# Filesystem access rights
		$data->{filesystem} = $self->DBGetFilesystemRights ($team);
	}
	else {
		print STDERR '[Mioga2::Fouquet::GetTeam] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			});
		}
	}

	print STDERR '[Mioga2::Fouquet::GetTeam] Leaving, data: ' . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetTeam  ----------


#===============================================================================

=head2 SetTeam

Modify team profile

=head3 Incoming Arguments

=over

=item I<rowid>: The team's rowid

=item I<profile>: The profile's rowid

=back

=head3 Return value

=over

A Perl hash to be converted to JSON or XML:

	$data = {
		success: true / false,
		message: ...
	}

=back

=cut

#===============================================================================
sub SetTeam {
	my ($self, $context) = @_;
	print STDERR '[Mioga2::Fouquet::SetTeam] Entering, args: ' . Dumper $context->{args} if ($debug);

	my $config = $context->GetConfig ();
	my $group = $context->GetGroup ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', ['mioga_team_id', $config] ],
			[ [ 'profile' ], 'disallow_empty', ['mioga_profile_id', $group] ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		my $team = Mioga2::TeamList->new ($config, { attributes => { rowid => $values->{rowid} } });
		my $profile = Mioga2::ProfileList->new ($config, { attributes => { rowid => $values->{profile} } });
		$group->AffectProfileToTeams ($team, $profile->Get ('ident'));

		$data->{success} = 'true';
		$data->{message} = __x('Team now has profile {profile}.', profile => $profile->Get ('ident'));
		$data->{team} = $self->GetTeam ($context);
	}
	else {
		print STDERR '[Mioga2::Fouquet::SetTeam] Got errors: ' . Dumper $errors;
		$data->{success} = 'false';
		$data->{message} = __('Team profile change failed.');
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			});
		}
	}

	print STDERR '[Mioga2::Fouquet::SetTeam] Leaving, data: ' . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetTeam  ----------


#===============================================================================

=head2 GetProfile

Get profile details

=head3 Incoming Arguments

=over

=item I<rowid>: The profile rowid

=back

=head3 Return value

=over

A Perl hash to be converted to JSON or XML:

	$data = {
		rowid: ...,
		ident: ...,
		users: [
			{
				rowid: ...,
				fistname: ...,
				lastname: ...,
				email: ...,
			},
			...
		],
		teams: [
			{
				rowid: ...,
				ident: ...,
			},
			...
		],
		rights: [
			applications: ...,	# Output of DBGetApplicationRights
			filesystem: ...,	# Output of DBGetFilesystemRights
		]
	}

=back

=cut

#===============================================================================
sub GetProfile {
	my ($self, $context) = @_;
	print STDERR '[Mioga2::Fouquet::GetProfile] Entering, args: ' . Dumper $context->{args} if ($debug);

	my $config = $context->GetConfig ();
	my $group = $context->GetGroup ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'allow_empty', ['mioga_profile_id', $group] ],
			[ [ 'full' ], 'allow_empty', 'want_int' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = { };

	if (!@$errors) {
		$values->{rowid} = $context->GetGroup ()->Get ('default_profile_id') if (!defined ($values->{rowid}));
		my $profile = Mioga2::ProfileList->new ($config, { attributes => { rowid => $values->{rowid} } });

		# Profile attributes
		for my $attr (qw/rowid ident/) {
			$data->{$attr} = $profile->Get ($attr);
		}

		# Users
		# TODO This should be handled into Mioga2::UserList instead of grepping the list..
		my $users = Mioga2::UserList->new ($config, { attributes => { groups => $group } });
		my @users = $users->GetUsers ();
		@{$data->{users}} = grep { $_->{profile} eq $profile->Get ('ident') } @users;

		# Teams
		# TODO This should be handled into Mioga2::TeamList instead of grepping the list...
		my $teams = Mioga2::TeamList->new ($config, { attributes => { groups => $group } });
		my @teams = $teams->GetTeams ();
		@{$data->{teams}} = grep { $_->{profile} eq $profile->Get ('ident') } @teams;

		# Application access rights
		if ($values->{full}) {
			# Full list is required, return all rights
			$data->{rights}->{applications} = $self->DBGetApplicationRights ($profile);
		}
		else {
			# No full list is required, filter active functions
			for my $app (@{$self->DBGetApplicationRights ($profile)}) {
				my $record;
				for my $func (@{$app->{functions}}) {
					if ($func->{selected} eq 'true') {
						$record->{ident} = $app->{ident};
						$record->{label} = $app->{label};
						push (@{$record->{functions}}, $func);
					}
				}
				push (@{$data->{rights}->{applications}}, $record) if ($record);
			}
		}

		# Filesystem access rights
		$data->{rights}->{filesystem} = $self->DBGetFilesystemRights ($profile);
	}
	else {
		print STDERR '[Mioga2::Fouquet::GetProfile] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			});
		}
	}

	print STDERR '[Mioga2::Fouquet::GetProfile] Leaving, data: ' . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetProfile  ----------


#===============================================================================

=head2 SetProfile

Modify or create a profile

=head3 Incoming Arguments

=over

=item I<rowid> (I<optional>): The profile rowid

=item I<ident>: The profile ident

=item I<functions>: The function rowids the profile can access

=back

=head3 Return value

=over

A Perl hash to be converted to JSON or XML:

	$data = {
		success: true / false,
		message: ...
	}

=back

=cut

#===============================================================================
sub SetProfile {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Fouquet::SetProfile] Entering, args: " . Dumper $context->{args} if ($debug);

	my $config = $context->GetConfig ();
	my $group = $context->GetGroup ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'allow_empty', ['mioga_profile_id', $group] ],
			[ [ 'ident' ], 'stripwxps', 'disallow_empty', [ 'valid_profile_ident', [$config, $group, $context->{args}->{rowid}] ] ],
			[ [ 'functions' ], 'allow_empty' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $update_mode = 1;
	if (!@$errors) {
		if (!$values->{rowid}) {
			$update_mode = 0;
			my $db = $config->GetDBObject ();
			try {
				$db->BeginTransaction ();
				$db->ExecSQL ('INSERT INTO m_profile (created, modified, ident, group_id) VALUES (now (), now (), ?, ?)', [$values->{ident}, $group->Get ('rowid')]);
				$values->{rowid} = $db->GetLastInsertId ('m_profile');

				# Set authorization for profile
				my $private = Mioga2::URI->new ($config,
					group => $group->Get ('ident'),
					public => 0,
					path => '/'
				);
				my ($private_uri) = ($private->GetURI () =~ m~^(.*)/$~);
				my $public = Mioga2::URI->new ($config,
					group => $group->Get ('ident'),
					public => 1,
					path => '/'
				);
				my ($public_uri) = ($public->GetURI () =~ m~^(.*)/$~);

				# Create authorization to URI if not exists
				$db->ExecSQL ('INSERT INTO m_authorize (uri_id, access) SELECT m_uri.rowid AS uri_id, 0 AS access FROM m_uri WHERE m_uri.uri IN (?, ?) AND (m_uri.rowid, 0) NOT IN (SELECT uri_id, access FROM m_authorize);', [$private_uri, $public_uri]);

				# Link profile to authorization
				$db->ExecSQL ('INSERT INTO m_authorize_profile SELECT m_authorize.rowid AS authz_id, ? AS profile_id FROM m_authorize, m_uri WHERE m_authorize.access = 0 AND m_authorize.uri_id = m_uri.rowid AND m_uri.uri IN (?, ?) AND (m_authorize.rowid, ?) NOT IN (SELECT authz_id, profile_id FROM m_authorize_profile);', [$values->{rowid}, $private_uri, $public_uri, $values->{rowid}]);

				$context->{args}->{rowid} = $values->{rowid};
				$db->EndTransaction ();
			}
			otherwise {
				push (@$errors, ['ident', ['valid_profile_ident']]);
				$db->RollbackTransaction ();
			};
		}
	}

	my $data = { };

	if (!@$errors) {
		# Load profile
		my $profile = Mioga2::ProfileList->new ($config, { attributes => { rowid => $values->{rowid} } });

		# rename profile
		if ($values->{ident} ne $profile->Get ('ident')) {
			$profile->Set ('ident', $values->{ident});
			$profile->Store ();
		}

		# Set profile functions
		$profile->SetFunctions (ac_ForceArray ($values->{functions}));

		$data->{success} = 'true';
		my $message_string = $update_mode ? 'Profile {profile} successfully updated.' : 'Profile {profile} successfully created.';
		$data->{message} = __x($message_string, profile => $profile->Get ('ident'));
		$data->{profile} = $self->GetProfile ($context);
	}
	else {
		print STDERR '[Mioga2::Fouquet::SetProfile] Got errors: ' . Dumper $errors;
		$data->{success} = 'false';
		my $message_string = $update_mode ? 'Profile {profile} change failed.' : 'Profile {profile} creation failed.';
		$data->{message} = __x($message_string, profile => $values->{ident});
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			});
		}
	}

	print STDERR "[Mioga2::Fouquet::SetProfile] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetProfile  ----------


#===============================================================================

=head2 GetNonMemberUsers

Get list of users not being member of group

=head3 Incoming Arguments

=over

=item I<max>: The max user count to be returned.

=back

=head3 Return value

=over

If user count <= max:
	A Perl data structure to be converted to JSON or XML 
		[
			{
				rowid: ...,
				ident: ...,
				firstname: ...,
				lastname: ...,
				email: ...
			},
			...
		]

If user count > max, the user count.

=back

=cut

#===============================================================================
sub GetNonMemberUsers {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Fouquet::GetNonMemberUsers] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'max' ], 'allow_empty', 'want_int' ],
			[ [ 'lastname' ], 'allow_empty', 'stripxws' ],
			[ [ 'app_name' ], 'allow_empty', [ 'match', 'Fouquet' ] ]
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data;

	if (!@$errors) {
		my $config = $context->GetConfig ();

		# Get non-member users
		my $userlist = $self->{group}->GetInvitedUserList (short_list => 1);
		$userlist->Revert ();
		my @users = $userlist->GetUsers ();

		if (exists $values->{lastname}) {
			my ($lastname) = ($values->{lastname} =~ m/(.)\*/);
			if ($lastname =~ /[A-Z]/) {
				my $list = Mioga2::UserList->new ($config, { attributes => { rowid => $userlist->Get ('rowid'), lastname => $lastname }, match => 'begins' });
				my @users = $list->GetUsers ();
				$data = \@users;
			}
			else {
				my @users = $userlist->GetUsers ();
				@$data = grep { $_->{lastname} !~ /^[A-Z]/i } @users;
			}
		} elsif (defined ($values->{max}) && (scalar (@users) > $values->{max})) {
			$data = scalar (@users);
		} else {
			$data = \@users;
		}
	}

	print STDERR "[Mioga2::Fouquet::GetNonMemberUsers] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetNonMemberUsers  ----------


#===============================================================================

=head2 GetNonMemberTeams

Get list of teams not being member of group

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

	A Perl data structure to be converted to JSON or XML 
		[
			{
				rowid: ...,
				ident: ...
			},
			...
		]

=back

=cut

#===============================================================================
sub GetNonMemberTeams {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Fouquet::GetNonMemberTeams] Entering" if ($debug);

	my $data;

	# Get non-member teams
	my $teamlist = $self->{group}->GetInvitedTeamList (short_list => 1);
	$teamlist->Revert ();
	my @teams = $teamlist->GetTeams ();

	$data = \@teams;

	print STDERR "[Mioga2::Fouquet::GetNonMemberTeams] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine GetNonMemberTeams  ----------


#===============================================================================

=head2 SetApplicationPublic

Set application public or private

=head3 Incoming Arguments

=over

=item I<rowid>: The application rowid

=item I<mode>: The mode ('public' or 'private')

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:
	{
		success => 'true' / 'false',
		message => ...
	}

=back

=cut

#===============================================================================
sub SetApplicationPublic {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Fouquet::SetApplicationPublic] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int' ],
			[ [ 'mode' ], 'disallow_empty', 'stripxws', [ 'match', "^\(private|public\)\$" ] ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = {
		success => 'false',
		message => __('Application mode toggle failed.')
	};

	if (!@$errors) {
		my $config = $context->GetConfig ();
		my $db = $config->GetDBObject ();

		try {
			$db->BeginTransaction ();

			$db->ExecSQL ('UPDATE m_application_group_allowed SET is_public = ? WHERE group_id = ? AND application_id = ?;', [($values->{mode} eq 'public') ? 1 : 0, $self->{group}->Get ('rowid'), $values->{rowid}]);

			# Set output data
			$data->{success} = 'true';
			$data->{message} = __('Application mode successfully toggled.');

			$db->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			print STDERR '[Mioga2::Fouquet::SetApplicationPublic] Toggle failed: ' . $err->stringify;
			$db->RollbackTransaction ();
		};
	}
	else {
		print STDERR "[Mioga2::Fouquet::SetApplicationPublic] Got errors: " . Dumper $errors if ($debug);
	}

	print STDERR "[Mioga2::Fouquet::SetApplicationPublic] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine SetApplicationPublic  ----------


#===============================================================================

=head2 DeleteProfile

Delete a profile

=head3 Incoming Arguments

=over

=item I<rowid>: The profile rowid. Profile must exist and have no user/team associated

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:
	{
		success => 'true' / 'false',
		message => ...
	}

=back

=cut

#===============================================================================
sub DeleteProfile {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Fouquet::DeleteProfile] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int', [ 'mioga_profile_id', $context->GetGroup () ], [ 'unused_profile_id', $context->GetConfig () ] ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = {
		success => 'false',
		message => __('Profile deletion failed.'),
	};

	if (!@$errors) {
		my $config = $context->GetConfig ();
		my $db = $config->GetDBObject ();

		try {
			$db->BeginTransaction ();

			# Delete profile
			my $profile = Mioga2::ProfileList->new ($self->{config}, { attributes => { rowid => $values->{rowid} } });
			$profile->Delete ();

			# Set output data
			$data->{success} = 'true';
			$data->{message} = __('Profile successfully deleted.');

			$db->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			print STDERR '[Mioga2::Fouquet::DeleteProfile] Deletion failed: ' . $err->stringify;
			$db->RollbackTransaction ();
		};
	}
	else {
		print STDERR "[Mioga2::Fouquet::DeleteProfile] Got errors: " . Dumper $errors if ($debug);
	}

	print STDERR "[Mioga2::Fouquet::DeleteProfile] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DeleteProfile  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut
# ============================================================================


#===============================================================================

=head2 DBGetLang

Get lang from database

=head3 Incoming Arguments

=over

=item I<$rowid>: Optional, the language rowid.

=back

=head3 Return value

=over

	$data = {
		rowid => ...,
		ident => ...,
		name  => ...
	}

This method returns a single hash, as above, when called with a rowid, or an
array of hashes, as above, when called without rowid.

=back

=cut

#===============================================================================
sub DBGetLang {
	my ($self, $rowid) = @_;
	print STDERR "[Mioga2::Fouquet::DBGetLang] Entering, getting " . ((defined ($rowid)) ? 'rowid=' . $rowid : 'full list') . "\n" if ($debug);

	my $data;

	if (defined ($rowid)) {
		$data = $self->{db}->SelectSingle ('SELECT rowid, ident FROM m_lang WHERE rowid = ?;', [$rowid]);
		$data->{name} = __($data->{ident});
	}
	else {
		$data = $self->{db}->SelectMultiple ('SELECT rowid, ident FROM m_lang;');
		map { $_->{name} = __($_->{ident}) } @$data;
	}

	print STDERR "[Mioga2::Fouquet::DBGetLang] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DBGetLang  ----------


#===============================================================================

=head2 DBGetApplicationRights

Get rights to applications for a user / team / profile

=head3 Incoming Arguments

=over

=item I<$object>: A Mioga2::UserList, Mioga2::TeamList or Mioga2::ProfileList

=back

=head3 Return value

=over

A Perl hash containing access rights.

	$data = [
		{
			ident: ...,
			label: ...,
			functions: [
				{
					ident: ...,
					label: ...
				},
				...
			]
		},
		...
	]

=back

=cut

#===============================================================================
sub DBGetApplicationRights {
	my ($self, $object) = @_;
	print STDERR '[Mioga2::Fouquet::DBGetApplicationRights] Entering for ' . ref ($object) . ' idents: ' . Dumper $object->Get ('ident') if ($debug);

	my $data;
	my $appslang = $self->LoadAppsLang ();

	if (ref ($object) eq 'Mioga2::UserList') {
		my $profile = Mioga2::ProfileList->new ($self->{config}, { attributes => { rowid => $object->Get ('profile_id') } });
		my $group = $self->{group};
		my $teams = Mioga2::TeamList->new ($self->{config}, { attributes => { users => $object, groups => $group }});

		if ($profile->Count ()) {
			# Get rights for profile
			for my $app (@{$self->DBGetApplicationRights ($profile)}) {
				my $record;
				for my $func (@{$app->{functions}}) {
					if ($func->{selected} eq 'true') {
						$record->{ident} = $app->{ident};
						$record->{label} = $app->{label};
						push (@{$record->{functions}}, $func);
					}
				}
				push (@{$data->{profile}}, $record) if ($record);
			}
		}

		if ($teams->Count ()) {
			# Get rights for teams
			for my $app (@{$self->DBGetApplicationRights ($teams)}) {
				my $record;
				for my $func (@{$app->{functions}}) {
					if ($func->{selected} eq 'true') {
						$record->{ident} = $app->{ident};
						$record->{label} = $app->{label};
						push (@{$record->{functions}}, $func);
					}
				}
				push (@{$data->{teams}}, $record) if ($record);
			}
		}
	}
	elsif (ref ($object) eq 'Mioga2::ProfileList') {
		# Active functions
		my $sql = 'SELECT m_application.ident AS application_ident, m_function.rowid AS function_rowid, m_function.ident AS function_ident, m_function.description AS description, \'true\' AS selected FROM m_profile_function, m_function, m_application WHERE m_function.application_id = m_application.rowid AND m_function.rowid = m_profile_function.function_id AND profile_id IN (' . join (', ', @{ac_ForceArray ($object->Get ('rowid'))}) . ')';
		# Inactive functions
		$sql .= 'UNION SELECT m_application.ident AS application_ident, m_function.rowid AS function_rowid, m_function.ident AS function_ident, m_function.description AS description, \'false\' AS selected FROM m_application, m_function WHERE m_function.application_id = m_application.rowid AND m_application.rowid IN (SELECT application_id FROM m_application_group_allowed WHERE group_id IN (' . join (', ', @{ac_ForceArray ($object->Get ('group_id'))}) . ')) AND m_function.rowid NOT IN (SELECT function_id FROM m_profile_function WHERE profile_id IN (' . join (', ', @{ac_ForceArray ($object->Get ('rowid'))}) . '))';
		my $functions = $self->{db}->SelectMultiple ($sql);
		my %functions;
		map {
			my $function_ident = $_->{function_ident};
			my @func_transl = grep { $_->{ident} eq $function_ident } @{ac_ForceArray ($appslang->{$_->{application_ident}}->{function})};
			push (@{$functions{$_->{application_ident}}}, { rowid => $_->{function_rowid}, ident => $_->{function_ident}, label => $func_transl[0]->{name}, selected => $_->{selected} })
		} @{$functions};
		for my $app (keys (%functions)) {
			push (@$data, {
				ident => $app,
				label => $app,	# TODO Find application name translation
				functions => $functions{$app}
			});
		}
	}
	elsif(ref ($object) eq 'Mioga2::TeamList') {
		my $sql = 'SELECT m_profile_group.group_id AS team_id, m_group_base.ident AS team_ident, m_application.ident AS application_ident, m_function.ident AS function_ident, m_function.description AS description FROM m_group_base, m_profile_group, m_profile_function, m_function, m_application WHERE m_profile_group.profile_id = m_profile_function.profile_id AND m_profile_group.group_id = m_group_base.rowid AND m_function.application_id = m_application.rowid AND m_function.rowid = m_profile_function.function_id AND m_profile_group.profile_id IN (' . join (', ', @{ac_ForceArray ($object->Get ('profile_id'))}) . ') AND m_group_base.rowid IN (' . join (', ', @{ac_ForceArray ($object->Get ('rowid'))}) . ');';
		my $functions = $self->{db}->SelectMultiple ($sql);

		# Group data into a hash: 
		# 'Tous' => {
		#             'rights' => {
		#                           'AnimGroup' => [
		#                                            {
		#                                              'label' => 'Voir la liste des applications disponibles et des membres',
		#                                              'ident' => 'Users'
		#                                            } 
		#                                          ] 
		#                         },
		#             'rowid' => 1
		#           },
		my %functions;
		map {
			my $function_ident = $_->{function_ident};
			my @func_transl = grep { $_->{ident} eq $function_ident } @{ac_ForceArray ($appslang->{$_->{application_ident}}->{function})};
			$functions{$_->{team_ident}}->{rowid} = $_->{team_id}; push (@{$functions{$_->{team_ident}}->{rights}->{$_->{application_ident}}}, { ident => $_->{function_ident}, label => $func_transl[0]->{name} })
		} @{$functions};

		# Cycle through hash keys and 'rights' subkeys to produce final data
		for my $team (keys (%functions)) {
			my $entry = {
				ident => $team,
				rowid => $functions{$team}->{rowid},
			};
			for my $app (keys (%{$functions{$team}->{rights}})) {
				push (@{$entry->{rights}}, {
					ident => $app,
					label => $app,	# TODO Find application name translation
					functions => $functions{$team}->{rights}->{$app}
				});
			}
			push (@$data, $entry);
		}
	}
	else {
		throw Mioga2::Exception::Application ('Mioga2::Fouquet::DBGetApplicationRights', "Object type " . ref ($object) . " is not supported");
	}

	print STDERR '[Mioga2::Fouquet::DBGetApplicationRights] Leaving, data: ' . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DBGetApplicationRights  ----------


#===============================================================================

=head2 DBGetFilesystemRights

Get rights to applications for a user / team / profile

=head3 Incoming Arguments

=over

=item I<$object>: A Mioga2::UserList, Mioga2::TeamList or Mioga2::ProfileList

=back

=head3 Return value

=over

A Perl hash containing access rights.

=back

=head4 Given a Mioga2::UserList

=over

	$data = [
		{
			path: ...,
			user: ...,      # Access key for user
			profile: ...,   # Access key for profile
			teams: [
				{
					rowid: ...,
					team:  ...,  # Team-specific access key
					profile: ... # Team-profile access key
				}
			]
		},
		...
	]

=back

=head4 Given a Mioga2::TeamList

=over

	$data = [
		{
			uri_id: ...,    # The URI rowid
			team_id: ...,   # The team rowid
			team: ...,      # Access key for team
			profile: ...,   # Access key for profile
		},
		...
	]

=back

=head4 Given a Mioga2::ProfileList

=over

	$data = [
		{
			path: ...,
			profile: ...,   # Access key for profile
		},
		...
	]

=back

=cut

#===============================================================================
sub DBGetFilesystemRights {
	my ($self, $object) = @_;
	print STDERR '[Mioga2::Fouquet::DBGetFilesystemRights] Entering for ' . ref ($object) . ' idents: ' . Dumper $object->Get ('ident') if ($debug);
		my @data = ( );
		my $rights = { };

	if (ref ($object) eq 'Mioga2::UserList') {
		my $rights = { };

		my $profile = Mioga2::ProfileList->new ($self->{config}, { attributes => { rowid => $object->Get ('profile_id') } });
		my $group = $self->{group};
		my $teams = Mioga2::TeamList->new ($self->{config}, { attributes => { users => $object, groups => $group }});

		if ($profile->Count ()) {
			# Get specific rights
			# The request below produces the following output:
			#  rowid |                    uri                    | access
			# -------+-------------------------------------------+--------
			#      3 | /Mioga/home/Commun                        |      2
			#      4 | /Mioga/public/Commun                      |      2
			#    456 | /Mioga/home/Commun/Form2Mail              |      2
			#   1055 | /Mioga/home/Commun/fichier.txt            |      2
			#  15995 | /Mioga/home/Commun/mioga.sources.list.txt |      2
			my $sql = 'SELECT m_uri.uri, m_authorize.access FROM m_uri, m_authorize, m_authorize_user WHERE m_uri.rowid = m_authorize.uri_id AND m_authorize.rowid = m_authorize_user.authz_id AND m_authorize_user.user_id IN (' . join (', ', @{ac_ForceArray ($object->Get ('rowid'))}) . ') AND m_uri.group_id IN (' . join (', ', @{ac_ForceArray ($group->Get ('rowid'))}) . ');';

			# Data is processed to produce the following array:
			# $VAR1 = [ 
			#           { 
			#             'user' => 2,
			#             'path' => '/Mioga/home/Commun'
			#           },
			#           { 
			#             'user' => 2,
			#             'path' => '/Mioga/public/Commun'
			#           },
			#           { 
			#             'user' => 2,
			#             'path' => '/Mioga/home/Commun/Form2Mail'
			#           },
			#           { 
			#             'user' => 2,
			#             'path' => '/Mioga/home/Commun/fichier.txt'
			#           },
			#           { 
			#             'user' => 2,
			#             'path' => '/Mioga/home/Commun/mioga.sources.list.txt'
			#           }
			#         ];
			@{$rights->{user}} = map { { path => $_->{uri}, user => $_->{access} } } @{ac_ForceArray ($self->{db}->SelectMultiple ($sql))};

			# Get rights for profile
			$rights->{profile} = $self->DBGetFilesystemRights ($profile);
		}

		if ($teams->Count ()) {
			# Get rights for teams
			$rights->{teams} = $self->DBGetFilesystemRights ($teams);
		}

		# Merge data into a hash for further processing
		# 	{
		#		'<path>' => {
		#			user    => <user_access>,
		#			profile => <user_profile_access>,
		#			teams   => [
		#				{
		#					rowid   => <rowid>,
		#					team    => <team_access>,
		#					profile => <team_profile_access>
		#				},
		#				...
		#			]
		#		}
		# 	}
		my %merge;
		for my $source (qw/user profile/) {
			for my $entry (@{$rights->{$source}}) {
				$merge{$entry->{path}}->{$source} = $entry->{$source};
			}
		}
		for my $entry (@{$rights->{teams}}) {
			push (@{$merge{$entry->{path}}->{teams}}, {
				rowid => $entry->{team_id},
				team => $entry->{team},
				profile => $entry->{profile},
			})
		}

		# Flatten merged data into an array
		for my $path (keys (%merge)) {
			my $entry = {
				path => $path,
			};
			$entry->{user} = $merge{$path}->{user} if (defined ($merge{$path}->{user}));
			$entry->{profile} = $merge{$path}->{profile} if (defined ($merge{$path}->{profile}));
			for my $team (@{$merge{$path}->{teams}}) {
				my $subentry = {
					rowid   => $team->{rowid},
					profile => $team->{profile}
				};
				$subentry->{team} = $team->{team} if (defined ($team->{team}));
				push (@{$entry->{teams}}, $subentry);
			}
			push (@data, $entry);
		}
	}
	elsif (ref ($object) eq 'Mioga2::ProfileList') {
		# The request below produces the following output:
		#  rowid |                    uri                    | access
		# -------+-------------------------------------------+--------
		#      3 | /Mioga/home/Commun                        |      2
		#      4 | /Mioga/public/Commun                      |      2
		#    456 | /Mioga/home/Commun/Form2Mail              |      2
		#   1055 | /Mioga/home/Commun/fichier.txt            |      2
		#  15995 | /Mioga/home/Commun/mioga.sources.list.txt |      2
		my $sql = 'SELECT m_uri.rowid, m_uri.uri, m_authorize.access FROM m_uri, m_authorize, m_authorize_profile WHERE m_uri.rowid = m_authorize.uri_id AND m_authorize.rowid = m_authorize_profile.authz_id AND m_authorize_profile.profile_id IN (' . join (', ', @{ac_ForceArray ($object->Get ('rowid'))}) . ') AND m_uri.group_id IN (' . join (', ', @{ac_ForceArray ($object->Get ('group_id'))}) . ');';

		# Data is processed to produce the following array:
		# $VAR1 = [ 
		#           { 
		#             'profile' => 2,
		#             'path' => '/Mioga/home/Commun'
		#           },
		#           { 
		#             'profile' => 2,
		#             'path' => '/Mioga/public/Commun'
		#           },
		#           { 
		#             'profile' => 2,
		#             'path' => '/Mioga/home/Commun/Form2Mail'
		#           },
		#           { 
		#             'profile' => 2,
		#             'path' => '/Mioga/home/Commun/fichier.txt'
		#           },
		#           { 
		#             'profile' => 2,
		#             'path' => '/Mioga/home/Commun/mioga.sources.list.txt'
		#           }
		#         ];
		@data = map { { path => $_->{uri}, profile => $_->{access} } } @{ac_ForceArray ($self->{db}->SelectMultiple ($sql))};
	}
	elsif (ref ($object) eq 'Mioga2::TeamList') {
		# Load useful objects
		my $profile = Mioga2::ProfileList->new ($self->{config}, { attributes => { rowid => $object->Get ('profile_id') } });

		# The request below produces the following output:
		#                    path                    | uri_id | team_id | profile_access | team_access 
		# -------------------------------------------+--------+---------+----------------+-------------
		#  /Mioga/home/Commun/fichier.txt            |   1055 |       1 |              1 |           1
		#  /Mioga/home/Commun/Form2Mail              |    456 |       1 |              1 |           0
		#  /Mioga/public/Commun                      |      4 |       1 |              1 |            
		#  /Mioga/home/Commun/mioga.sources.list.txt |  15995 |       1 |              1 |            
		#  /Mioga/home/Commun                        |      3 |       1 |              1 |            
		my $sql = 'SELECT m_uri.uri AS path, m_uri.rowid AS uri_id, profile_authz.team_id AS team_id, profile_authz.access AS profile_access, team_authz.access AS team_access FROM m_uri LEFT JOIN (SELECT m_authorize.uri_id, m_profile_group.group_id AS team_id, m_authorize.access FROM m_authorize, m_authorize_profile, m_profile_group WHERE m_authorize.rowid = m_authorize_profile.authz_id AND m_authorize_profile.profile_id = m_profile_group.profile_id AND m_authorize_profile.profile_id IN (' . join (', ', @{ac_ForceArray ($profile->Get ('rowid'))}) . ') AND m_profile_group.group_id IN (' . join (', ', @{ac_ForceArray ($object->Get ('rowid'))}) . ')) AS profile_authz ON profile_authz.uri_id = m_uri.rowid LEFT JOIN (SELECT m_uri.rowid AS uri_id, authz.team_id, authz.access FROM m_uri LEFT JOIN (SELECT m_authorize.uri_id, m_authorize_team.team_id, m_authorize.access FROM m_authorize, m_authorize_team WHERE m_authorize.rowid = m_authorize_team.authz_id AND m_authorize_team.team_id IN (' . join (', ', @{ac_ForceArray ($object->Get ('rowid'))}) . ')) AS authz ON authz.uri_id = m_uri.rowid) AS team_authz ON team_authz.uri_id = m_uri.rowid AND team_authz.team_id = profile_authz.team_id WHERE m_uri.group_id IN (' . join (', ', @{ac_ForceArray ($profile->Get ('group_id'))}) . ') AND m_uri.parent_uri_id = m_uri.rowid';

		# The request data is processed to produce the following array:
		# $VAR1 = [
		#           {
		#             'profile' => 1,
		#             'uri_id' => 1055,
		#             'team_id' => 1,
		#             'path' => '/Mioga/home/Commun/fichier.txt',
		#             'team' => 1
		#           },
		#           {
		#             'profile' => 1,
		#             'uri_id' => 456,
		#             'team_id' => 1,
		#             'path' => '/Mioga/home/Commun/Form2Mail',
		#             'team' => 0
		#           },
		#           {
		#             'profile' => 1,
		#             'uri_id' => 4,
		#             'team_id' => 1,
		#             'path' => '/Mioga/public/Commun'
		#           },
		#           {
		#             'profile' => 1,
		#             'uri_id' => 15995,
		#             'team_id' => 1,
		#             'path' => '/Mioga/home/Commun/mioga.sources.list.txt'
		#           },
		#           {
		#             'profile' => 1,
		#             'uri_id' => 3,
		#             'team_id' => 1,
		#             'path' => '/Mioga/home/Commun'
		#           } 
		#         ];
		for my $entry (@{ac_ForceArray ($self->{db}->SelectMultiple ($sql))}) {
			my $record = {
				path    => $entry->{path},
				uri_id  => $entry->{uri_id},
				team_id => $entry->{team_id},
				profile => $entry->{profile_access}
			};
			$record->{team} = $entry->{team_access} if (defined ($entry->{team_access}));
			push (@data, $record);
		}
	}
	else {
		throw Mioga2::Exception::Application ('Mioga2::Fouquet::DBGetApplicationRights', "Object type " . ref ($object) . " is not supported");
	}

	print STDERR '[Mioga2::Fouquet::DBGetFilesystemRights] Leaving, data: ' . Dumper \@data if ($debug);
	return (\@data);
}	# ----------  end of subroutine DBGetFilesystemRights  ----------


# ============================================================================

=head2 LoadAppsLang ($lang)

	Load applications translation files.

=cut

# ============================================================================

my %LocaleCache;

sub LoadAppsLang {
	my ($self) = @_;

	my $lang = $self->{lang};

	my $appslang;
	if(exists $LocaleCache{$lang}) {
		$appslang = $LocaleCache{$lang};
	}
	else {
		my $langdir = $self->{langdir};

		my $file;
		if( -e "$langdir/${lang}.xml") {
			$file = "$langdir/${lang}.xml";
		} 
		else {
			$file = "$langdir/default.xml";
		}
			
		open(F_LANGFILE, $file)
        or die "Can't open file $file: $!";

		my $xml;

		{
			local $/ = undef;
			$xml = <F_LANGFILE>;
		}

		close(F_LANGFILE);

		my $xs = new Mioga2::XML::Simple;
		my $apps;
		for (@{$xs->XMLin($xml)->{application}}) {
			$apps->{$_->{ident}} = {
					name => $_->{name},
					description => $_->{description},
					icon => $_->{icon},
					function => $_->{function}
				};
		}
		$appslang = $LocaleCache{$lang} = $apps;
	}

    return $appslang;
}


#===============================================================================

=head2 InitApp

Initialize inner data for application. Not to be used, automatically called by
Mioga2::Application.

=cut

#===============================================================================
sub InitApp {
	my ($self, $context) = @_;

	$self->{group} = $context->GetGroup ();

	$self->{langdir} = $context->GetConfig ()->GetLangDir ();
	$self->{lang} = $context->GetConfig ()->GetDefaultLang ();
}	# ----------  end of subroutine InitApp  ----------


# ============================================================================
# GetAppDesc ()
# ============================================================================

sub GetAppDesc {
	my ($self, $context) = @_;
	my %AppDesc = ( 
		ident   => 'Fouquet',
		name	=> __('Fouquet'),
		package => 'Mioga2::Fouquet',
		type => 'normal',
		description => __('The Mioga2 group animation application'),
		api_version => '2.4',
		all_groups  => 1,
		all_users   => 0,
		is_group    => 1,
		is_user     => 0,
		is_resource => 0,
		can_be_public => 0,
		usable_by_resource => 0,

        functions   => {
			'Read' => __('Read only functions'),
			'Write' => __('Write functions'),
		},

        func_methods  => { 
			Read => [ 'DisplayMain', 'GetGroup', 'GetUser', 'GetTeam', 'GetProfile' ],
			Write => [ 'DisplayMain', 'GetGroup', 'GetUser', 'GetTeam', 'GetProfile', 'SetGroup', 'SetUser', 'SetTeam', 'SetProfile', 'GetNonMemberUsers', 'GetNonMemberTeams', 'SetApplicationPublic', 'DeleteProfile' ]
		},
		public_methods => [ ],
		func_mioglets => { },
		public_mioglets => [  ],
		non_sensitive_methods => [
			'DisplayMain',
			'GetGroup',
			'GetUser',
			'GetTeam',
			'GetProfile',
			'GetNonMemberUsers',
			'GetNonMemberTeams',
		]
    );
	return \%AppDesc;
}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2011, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
