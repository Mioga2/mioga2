# ============================================================================
# Mioga2::Portal Project (C) 2003-2007 The MiogaII Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Portal.pm: Mioga2::Portal application

=head1 DESCRIPTION

The Mioga2::Portal application is used to build HTML pages containing
various informations of Mioga2 applications.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Portal;
use strict;

use base qw(Mioga2::Application);
use Locale::Messages qw(:locale_h :libintl_h);
use Locale::TextDomain::UTF8 'portal';

use Data::Dumper;
use POSIX qw(tmpnam);
use Error qw(:try);
use Time::HiRes qw( gettimeofday tv_interval );

use Mioga2::tools::APIAuthz;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::Exception::Simple;
use Mioga2::Portal::Exception::Authorize;
use Mioga2::Portal::MiogletDesc;
use Mioga2::Constants;
use XML::LibXML;
use XML::LibXSLT;
use File::Glob ':glob';
use File::MimeInfo::Magic;

my $debug = 0;
my $dump_xml_xsl = 1;

# ============================================================================

=head2 GetAppDesc ()

	Return the current application description.

=cut

# ============================================================================

sub GetAppDesc {
	my ($self, $context) = @_;

    my %AppDesc = ( ident   => 'Portal',
	    	name	=> __('Portal'),
                package => 'Mioga2::Portal',
				type => 'portal',
                description => __('The Mioga2::Portal Application'),
                all_groups  => 1,
                all_users  => 1,
                can_be_public => 1,
				is_user             => 1,
				is_group            => 1,
				is_resource         => 0,

                functions   => { 
					'Portal' => __('Portal functions'),
				},
				
                func_methods  => { 
					Portal => [ 'DisplayMain', 'DisplayPortal' ],
				},
				public_methods => [
							'DisplayMain',
							'DisplayPortal'
						],

				non_sensitive_methods => [
					"DisplayMain",
					"DisplayPortal",
					"CreatePortal",
					"EditPortal",
				]

                );
    
	return \%AppDesc;
}

# ============================================================================

=head2 CheckUserAccessOnMethod ($context, $user, $group, $method_ident)

	Check if the user $user working in group $group can access to
	the method $method_ident in the current application .
	
=cut

# ============================================================================

sub CheckUserAccessOnMethod {
	my ($self, $context, $user, $group, $method_ident) = @_;
	
	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	
	if($method_ident eq 'DisplayPortal') {
		return AUTHZ_OK;
	}
	elsif($method_ident eq 'CreatePortal') {
		# Check write access on root of private space of current group
		my $private_uri = $config->GetPrivateURI() . "/" .$group->GetIdent();
		my $uri = new Mioga2::URI($config, uri => $private_uri);
		my $authz = AuthzTestRightAccessForURI($config, $uri, $user, $group);

		return ($authz == AUTHZ_WRITE)?AUTHZ_OK:AUTHZ_FORBIDDEN;
	}
	elsif($method_ident eq 'EditPortal') {
		
		my $uri = $context->GetURI()->GetURI();
		$uri =~ s!//!/!g;

		my $private_bin = $config->GetBinURI() . "/" . $context->GetGroup()->GetIdent();

		my $path;
		if(exists $context->{args}->{path}) {
			$path = $context->{args}->{path};
		}
		elsif($uri =~ m!^($private_bin/Portal/.*Portal)(/.+)$!) {
			$path = $2;
		}
		elsif(exists $session->{PortalEditor}->{path}) {
			$path = $session->{PortalEditor}->{path};
		}
		else {
			return AUTHZ_FORBIDDEN;
		}

		my $fullpath;
		if($context->GetURI()->IsPrivate()) {
			$fullpath = $config->GetPrivateURI();
		}
		else {
			$fullpath = $config->GetPublicURI();
		}
	
		$fullpath .= "/" . $context->GetGroup()->GetIdent() . "$path";

		my $fileuri = new Mioga2::URI($config, uri => $fullpath);
		my $authz = AuthzTestRightAccessForURI($config, $fileuri, $user, $group);

		return ($authz == AUTHZ_WRITE)?AUTHZ_OK:AUTHZ_FORBIDDEN;
	}
	else {
		return AUTHZ_OK;
	}
}

# ============================================================================

=head2 DisplayMain ()

	Main Portal applications function.

=cut

# ============================================================================

sub DisplayMain {
	my ($self, $context) = @_;

	my $session = $context->GetSession ();
	my $config = $context->GetConfig();
	my $user = $context->GetUser();
	my $group = $context->GetGroup();

	# Check write access on root of private space of current group
	my $private_uri = $config->GetPrivateURI() . "/" .$group->GetIdent();
	my $uri = new Mioga2::URI($config, uri => $private_uri);
	my $authz = AuthzTestRightAccessForURI($config, $uri, $user, $group);

	my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayMain";
	if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
	}
	$xml .= ">";
	$xml .= $context->GetXML;

	if($authz == AUTHZ_WRITE) {
		$xml .= "<canCreatePortal/>";
	}

	$xml .= $self->GetXMLPortalList($config, $user,  $group);
	$xml .= "</DisplayMain>";


	my $content = new Mioga2::Content::XSLT($context, stylesheet => "portal_main.xsl", locale_domain => 'portal_xsl');
	$content->SetContent($xml);

	return $content;
}


# ============================================================================

=head2 CreatePortal ()

	Main Portal applications function.

=cut

# ============================================================================

sub CreatePortal {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();

	my $values = {};
	my $errors = [];

	if(st_ArgExists($context, 'portal_act_create')) {
		($values, $errors) = ac_CheckArgs($context, [ [ [ 'filename','name'], 'disallow_empty' ],
													  [ [ 'description' ], 'allow_empty' ],
													  [ ['X-CSRF-Token'], 'disallow_empty', ['csrf_token', [$context->GetConfig(), $context->GetUser()]] ],
													  ]);		

		if(! @$errors) {

			if($values->{filename} !~ /\.xml$/) {
				$values->{filename} .= ".xml";
			}

			$self->CreateNewPortal($config, $context->GetGroup, $context->GetUser, $values);
			$session->{__internal}->{message}{type} = 'info';
			$session->{__internal}->{message}{text} = __('Portal successfully created.');
			my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
			$content->SetContent('DisplayMain');
			return $content;
		}

	}

	elsif(! keys %{$context->{args}})  {
		delete $session->{Portal}->{Create};
	}


	# ------------------------------------------------------
	# Initialize missing values to default values from config
	# ------------------------------------------------------
	$self->InitializeNewPortalValues($context, $values);
	
	my $xml = '<?xml version="1.0"?>';
	$xml .= "<CreatePortal>";
	$xml .= $context->GetXML();

	$xml .= "<fields>";
	$xml .= ac_XMLifyArgs(['name', 'filename', 'description'],
						  $values );

	$xml .= "</fields>";

	$xml .= ac_XMLifyErrors($errors);

	$xml .= $self->GetXMLThemeList($config);

	$xml .= "</CreatePortal>";


	my $content = new Mioga2::Content::XSLT($context, stylesheet => "portal_main.xsl", locale_domain => 'portal_xsl');
	$content->SetContent($xml);

	return $content;
}


# ============================================================================

=head2 DisplayPortal ()

	Main Portal applications function.

=cut

# ============================================================================

sub DisplayPortal {
	my ($self, $context) = @_;

	return $self->ProcessPortal($context, "display");
}


# ============================================================================

=head2 EditPortal ()

	Main Portal editor function.

=cut

# ============================================================================

sub EditPortal {
	my ($self, $context) = @_;

	return $self->ProcessPortal($context, "edit");
}

# ============================================================================

=head2 ProcessPortal ()

	Generate Portal HTML Document.

=cut

# ============================================================================

sub ProcessPortal {
	my ($self, $context, $mode) = @_;
	my $config = $context->GetConfig();
	my $session = $context->GetSession();

	print STDERR ("Portal::ProcessPortal\n") if ($debug);

	if(! exists $context->{args}->{path}) 
	{
		#
		# Path to xml file can be given in the URL (and not in path parameter)
		# This is required for Redirection of /.../portal/... URI because
		# RedirectMatch can't pass arguments to URI (?foo=bar)
		#

		my $uri = $context->GetURI()->GetURI();
		$uri =~ s!//!/!g;

		my $private_bin = $config->GetBinURI() . "/" . $context->GetGroup()->GetIdent();
		if($uri =~ m!^($private_bin/Portal/.*Portal)(/.+)$!) {

			my $content = new Mioga2::Content::REDIRECT($context, mode => "external");
			$content->SetContent("$1?path=$2");
			return $content;
		}
	}


	if( ! exists $context->{args}->{path} and 
		! ($mode eq 'edit' and exists $session->{PortalEditor}->{xmldocument})) {

		throw Mioga2::Exception::Simple("Mioga2::Portal::DisplayPortal", __"Missing path argument");
	}

	#
	# Create XML parser
	# 
	my $parser = XML::LibXML->new();
	$self->{parser} = $parser;
	
	#
	# Load description from file or from cache for portal editor
	# and update description from submited parameters.
	#
	my $doc;
	if( $mode eq 'edit' and exists $session->{PortalEditor}->{xmldocument}
						and (!exists $context->{args}->{path}
									or ($session->{PortalEditor}->{path} eq $context->{args}->{path}))
						and (!exists $context->{args}->{nocache}) ) {
		
		$doc = $parser->parse_string($session->{PortalEditor}->{xmldocument});

		if(st_ArgExists($context, 'save_portal')) {
			return $self->SavePortal($context, $doc);
		}
		
		$self->UpdateCurrentDescription($context, $doc);
	}	

	else {
		$session->{PortalEditor}->{path} = $context->{args}->{path};
		$session->{PortalEditor}->{referer} = $context->GetReferer();
		$doc = $self->GetXMLDescription($context);
	}
	print STDERR ("Portal::ProcessPortal after reading doc = " . $doc->toString() . "\n") if ($debug);

	if($mode eq 'edit' and st_ArgExists($context, 'view_cache')) {
		$mode = 'display';
	}
	#
	#
	#
	my $mioglets = $self->GetMioglets($config, $doc);

	my $mioglets_desc = $self->LaunchMioglets($context, $mioglets);

	my $xmldocument = $self->BuildXMLDocument($context, $doc, $mioglets_desc, $mode);	
	#
	# Save portal description in cache
	#
	if($mode eq 'edit') {
		$self->CachePortalDescription($context, $xmldocument);
	}
	#
	# Redirect to select_id
	#
	if(exists $context->{args}->{redirect_select_id}) {
		my $content = new Mioga2::Content::REDIRECT($context, mode => "external");
		
		my $uri = $context->GetURI()->GetURI();
		$uri =~ s/\?(.*)$//g;
		$uri .= "?select_id=$context->{args}->{redirect_select_id}";
		$content->SetContent("$uri");

		return $content;
	}
	#
	# Construct XSL stylesheet
	#
	my $xsl = new XML::LibXSLT();
	my $xsldocument = $self->BuildXSLDocument($context, $xmldocument, $mode);

	my %params = (mode => "'$mode'", select_id => "''", referer => "'$session->{PortalEditor}->{referer}'");
	if(exists $context->{args}->{select_id}) {
		$params{select_id} = "'$context->{args}->{select_id}'";
	}
	#
	# For debugging purpose
	#
	if($dump_xml_xsl) {
		my $tmpdir = $config->GetTmpDir();
		open(F_FILE, ">$tmpdir/Portal.xml") or die "Can't open $tmpdir/Portal.xml : $!";
		print F_FILE $xmldocument->serialize();
		close(F_FILE);

		open(F_FILE, ">$tmpdir/Portal.xsl") or die "Can't open $tmpdir/Portal.xsl : $!";
		print F_FILE $xsldocument;
		close(F_FILE);

		open(F_FILE, ">$tmpdir/Portal.params") or die "Can't open $tmpdir/Portal.params : $!";
		print F_FILE Dumper(\%params);
		close(F_FILE);
	}
	
	#
  # Initalize I18n
  #
  textdomain('portal_xsl');
  bindtextdomain('portal_xsl' => $config->GetLocalesPath);
  $xsl->register_function("urn:mioga", "gettext", sub { 
    my ($string, @params) = @_;
    return sprintf(gettext($string), @params);
  });
	
	#
	# Generate portal
	#
	my $style_doc = $parser->parse_string($xsldocument);
	my $stylesheet = $xsl->parse_stylesheet($style_doc);

	if(!defined $stylesheet) {
		throw Mioga2::Exception::Simple("Mioga2::Portal::DisplayPortal", __"Invalid stylesheet");
	}

	my $results = $stylesheet->transform($xmldocument, %params);
	
	my $content = new Mioga2::Content::ASIS($context, MIME => "text/html; charset=UTF-8");
	$content->SetContent($stylesheet->output_string($results));

	return $content;
}
# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 GetXMLDescription ()

	Return a XML::LibXML::Document object of Portal XML description
	or throw an exception if the file specified in the "path" argument
	does not exists or if the XML parser failes to parse the XML file.

=cut

# ============================================================================


sub GetXMLDescription {
	my ($self,  $context) = @_;

	my $config = $context->GetConfig();	
	my $uri = $context->GetURI();

	my $group = $context->GetGroup();
	my $dir;
	if($uri->IsPublic()) {
		$dir = $config->GetPublicDir();
	}
	else {
		$dir = $config->GetPrivateDir();
	}

	my $path = $dir . '/'. $group->GetIdent(). $context->{args}->{path};
	
	if(! -f $path) {
		throw Mioga2::Exception::Simple("Mioga2::Portal::GetXMLDescription",
        __x("File {filename} does not exists or is not a regular file.", filename => $path));
	}

	my $parser = $self->{parser};

	my $doc;

	try {
		$doc = $parser->parse_file($path);
	}
	otherwise {
		my $err = shift;

		throw Mioga2::Exception::Simple("Mioga2::Portal::GetXMLDescription",
        __x("Invalid XML description: {description}", description => Dumper($err)));
	};

	return $doc;
}

# ============================================================================

=head2 GetMioglets ()

	Return a XML::LibXML::NodeList object containing Mioglets declared in XML 
	description.

	Tags image, html (not inline), iframe and files are converted as Base/OtherContent mioglet entry
	and added to mioglets tree.

=cut

# ============================================================================

sub GetMioglets {
	my ($self,  $config, $doc) = @_;

	warn("Portal::GetMioglets\n") if ($debug);

	my $mioglets = $doc->findnodes("//mioglet");

	my @iframes = $doc->findnodes("//iframe");
	foreach my $node (@iframes) {
		# convert attributes in hash ref
		my $attrs = $self->GetAttributesAsHashRef($config, $node);
		$attrs->{mime} = "iframe/iframe";

		my $newnode = $self->AddMiogletNode($config, $node, "Base/OtherContent", "base_mioglet.xsl", $attrs);

		$mioglets->push($newnode);
	}

	my @images = $doc->findnodes("//image");
	foreach my $node (@images) {
		# convert attributes in hash ref
		my $attrs = $self->GetAttributesAsHashRef($config, $node);
		$attrs->{mime} = "image/image";

		my $newnode = $self->AddMiogletNode($config, $node, "Base/OtherContent", "base_mioglet.xsl", $attrs);

		$mioglets->push($newnode);
	}
	

	my @ext_html = $doc->findnodes("//html[attribute::href]");
	foreach my $node (@ext_html) {
		# convert attributes in hash ref
		my $attrs = $self->GetAttributesAsHashRef($config, $node);
		$attrs->{mime} = "text/html";

		my $newnode = $self->AddMiogletNode($config, $node, "Base/OtherContent", "base_mioglet.xsl", $attrs);

		$mioglets->push($newnode);
	}

	my @files = $doc->findnodes("//file");
	foreach my $node (@files) {
		# convert attributes in hash ref
		my $attrs = $self->GetAttributesAsHashRef($config, $node);

		my $newnode = $self->AddMiogletNode($config, $node, "Base/OtherContent", "base_mioglet.xsl", $attrs);

		$mioglets->push($newnode);
	}

	return $mioglets;
}
# ============================================================================

=head2 LaunchMioglets ()

	Return a XML::LibXML::NodeList object containing XML DOM tree generated 
	by Mioglets.
	Mioglets can return plain text XML or XML::LibXML::Node object.
	This method add namespace to XML tree generated by mioglets.
	WARNING : This method empty out the given mioglets nodelist.

=cut

# ============================================================================
sub LaunchMioglets {
	my ($self,  $context, $mioglets) = @_;

	warn("Mioga2::Portal::LaunchMioglets\n") if ($debug);

	my $config = $context->GetConfig();
	my $uri = $context->GetURI();

	my $mioglets_desc = new XML::LibXML::NodeList();

	while(my $mioglet = $mioglets->shift()) {		
		my $type = $mioglet->getAttribute("type");
		warn("Mioga2::Portal::LaunchMioglets type = $type\n") if ($debug);

		try {
			my ($ident, $method) = ($type =~ m!^([^/]+)/([^/]+)$!);
			warn("Mioga2::Portal::LaunchMioglets ident = $ident method = $method\n") if ($debug);

			# Create object
			my $mioglet_desc = new Mioga2::Portal::MiogletDesc($config, $ident);
			my $mioglet_obj = $mioglet_desc->CreateObject($mioglet);

			# Check Authorization
			my $authz = $mioglet_obj->TestAuthorize($context);
			if($authz == AUTHZ_NONE) {
				throw Mioga2::Portal::Exception::Authorize($type); 
			}
			# Generate XML content
			my $desc = $mioglet_obj->$method($context);
			warn("Mioga2::Portal::LaunchMioglets desc = $desc\n") if ($debug);

			my $realdesc;

			my $mioglet_uri = "http://www.mioga2.org/portal/mioglets/".$mioglet_desc->GetIdent();	
			my $prefix = $mioglet_desc->GetPrefix();
			warn("Mioga2::Portal::LaunchMioglets prefix = $prefix  mioglet_uri = $mioglet_uri\n") if ($debug);
			
			if(ref($desc) eq '') {
				$desc = qq|<miogletdesc xmlns:$prefix="$mioglet_uri">|."$desc</miogletdesc>";
				my $node = $self->{parser}->parse_balanced_chunk($desc);
				$realdesc = $node->getFirstChild();
				$desc = $realdesc->getFirstChild();
			}
			else {
				$realdesc = new XML::LibXML::Element("miogletdesc");
				$realdesc->setAttribute("xmlns:$prefix", $mioglet_uri);
				$realdesc->addChild($desc);
			}

			if($desc->nodeName !~ /^$prefix:/) {
				my @nodes = $desc->findnodes("//*");

				foreach my $elem (@nodes) {
					my $name = $elem->nodeName();
					$elem->setNodeName("$prefix:$name");
				}
			}	

			my $mioglet_id = $self->GenerateMiogletId($config);
			warn("Mioga2::Portal::LaunchMioglets mioglet_id = $mioglet_id\n") if ($debug);
			$realdesc->setAttribute("id", $mioglet_id);
			$mioglet->setAttribute("ref", $mioglet_id);
			$mioglet->setAttribute("ns-uri", $mioglet_uri);
		
			$mioglets_desc->push($realdesc);
		}		
		catch Mioga2::Portal::Exception::Authorize with {
			my $err = shift;
			warn ("Portal::LaunchMioglet Authorize error : " . Dumper($err)) if $debug;

			if($type eq 'Base/Error')  {
				throw Mioga2::Exception::Simple("Mioga2::Portal::LaunchMioglets",
                __x("Error in parsing XML generated by Base/Error.\nCrash to prevent infinite loop.\n\n{description}", description => Dumper($err)));
			}
			
			my $attrs = { errortype => "Not Authorized",
						  text => $err->as_string(),
					  };
			
			my $newnode = $self->AddMiogletNode($config, $mioglet, "Base/Error", "base_mioglet.xsl", $attrs);
			$mioglets->push($newnode);
		}
		otherwise {
			my $err = shift;
			warn ("Portal::LaunchMioglet error : " . Dumper($err)) if $debug;

			if($type eq 'Base/Error')  {
				throw Mioga2::Exception::Simple("Mioga2::Portal::LaunchMioglets",
                __x("Error in parsing XML generated by Base/Error.\nCrash to prevent infinite loop.\n\n{description}", description => Dumper($err)));
			}
			my $attrs = { errortype => "Invalid Mioglet XML Description",
						  text => Dumper($err),
					  };
			
			my $newnode = $self->AddMiogletNode($config, $mioglet, "Base/Error", "base_mioglet.xsl", $attrs);
			$mioglets->push($newnode);
		};
	}

	return $mioglets_desc;
}
# ============================================================================

=head2 BuildXMLDocument ()

	Concatenate main XML description and mioglet generated document.

=cut

# ============================================================================
sub BuildXMLDocument {
	my ($self, $context, $doc, $mioglets_nodelist, $mode) = @_;
	my $config = $context->GetConfig();

	my $root = $doc->documentElement();

	my $newroot = new XML::LibXML::Element("Portal");
	$newroot->addChild($root);

	my $descnode = new XML::LibXML::Element("MiogletDescs");

	while(my $mioglet = $mioglets_nodelist->shift()) {
		$descnode->addChild($mioglet);
	}
	
	$newroot->addChild($descnode);
	$newroot->appendWellBalancedChunk($context->GetXML);

	if($mode eq 'edit') {
#		$newroot->appendWellBalancedChunk(encodeToUTF8('ISO-8859-1', $self->GetXMLThemeList($config)));
		$self->IdentifyNodes($config, $root);
	}

	my $document = new XML::LibXML::Document();
	$document->setDocumentElement($newroot);
	
	return $document;
}
# ============================================================================

=head2 BuildXSLDocument ()

	Generate XSL document.

=cut

# ============================================================================
sub BuildXSLDocument {
	my ($self, $context, $doc, $mode) = @_;

	my $config = $context->GetConfig();

	my $xml = '<?xml version="1.0"?>';
	$xml .= '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"';
	my @nslist = $doc->getNamespaces();
	foreach my $ns (@nslist) {
		$xml .= " xmlns:".$ns->prefix().'="'.$ns->getNamespaceURI().'"';
	}
	$xml .= ">";

	my $group = $context->GetGroup();
	my $xsldir = $config->GetXSLDir();

	my $theme = $group->GetTheme();
	my $lang = $group->GetLang();

	$xml .= '<xsl:import href="'.$self->GetXSLFilePath($config, $theme, $lang, 'base.xsl').'"/>';
	$xml .= '<xsl:output  method="html" encoding="utf-8"/>';
	$xml .= '<xsl:include href="'.$self->GetXSLFilePath($config, $theme, $lang, 'organizer_color.xsl').'"/>';
	$xml .= '<xsl:include href="'.$self->GetXSLFilePath($config, $theme, $lang, 'simple_large_list.xsl').'"/>';
	if($mode eq 'edit') {
		$xml .= '<xsl:include href="'.$self->GetXSLFilePath($config, $theme, $lang, 'portal_editor.xsl').'"/>';
	}
	else {
		$xml .= '<xsl:include href="'.$self->GetXSLFilePath($config, $theme, $lang, 'portal.xsl').'"/>';
	}
	
	my $nodelist = $doc->findnodes("/Portal/Mioga2Portal//mioglet/stylesheet");
	my %loaded = ();
	while(my $node = $nodelist->shift()) {
	  my $file = $node->textContent;
	  utf8::encode($file);
	  
		my $filepath = $self->GetXSLFilePath($config, $theme, $lang, $file);
		
		if(!exists $loaded{$filepath}) {
			$xml .= qq|<xsl:include href="$filepath"/>|;
			$loaded{$filepath} = 1;
		}
	}
	
	$xml .= "</xsl:stylesheet>";
	return $xml;
}
# ============================================================================

=head2 GetAttributesAsHashRef ()

	Return a hash ref containing node attributes description.

=cut

# ============================================================================
sub GetAttributesAsHashRef {
	my ($self,  $config, $node) = @_;

	my $result = {};
	
	foreach my $attr ($node->attributes()) {
		$result->{$attr->getName()} = $attr->getValue();
	}

	return $result;
}
# ============================================================================

=head2 AddMiogletNode ()

	Return a new XML::LibXML::Element object describing a Mioglet and
	subsitute the old node with newly created mioglet node in DOM tree.

=cut

# ============================================================================
sub AddMiogletNode {
	my ($self, $config, $oldnode, $name, $stylesheet, $params) = @_;
	warn("Portal::AddMiogletNode\n") if ($debug);

	my $node = new XML::LibXML::Element("mioglet");
	$node->setAttribute("type", $name);

	my $ssnode = new XML::LibXML::Element("stylesheet");
	$ssnode->appendText($stylesheet);
	$node->addChild($ssnode);

	$ssnode = new XML::LibXML::Element("mode");
	$ssnode->appendText("group");
	$node->addChild($ssnode);

	foreach my $attr (keys %$params) {
		if($attr eq 'id' ) {
			$node->setAttribute("id", $params->{$attr});
		}
		else {
			my $chilnode = new XML::LibXML::Element($attr);
			$chilnode->appendText($params->{$attr});
			$node->addChild($chilnode);
		}
	}

	if(defined $oldnode) {
		my $parent = $oldnode->parentNode();
		$parent->replaceChild($node, $oldnode);
	}
	warn("Portal::AddMiogletNode node = " . $node->toString() . "\n") if ($debug);
	return $node;
}
# ============================================================================

=head2 CreateNewBox ()

	Return a new XML::LibXML::Element object describing a box containing
	a mioglet selector mioglet.

=cut

# ============================================================================
sub CreateNewBox {
	my ($self, $config) = @_;

	my $box = new XML::LibXML::Element("box");

	my $mioglet = $self->AddMiogletNode($config, undef, "Base/MiogletSelector", "base_mioglet.xsl", {});
	$box->addChild($mioglet);

	return $box;
}
# ============================================================================

=head2 GenerateMiogletId ()

	Return a unique identifier for new Mioglets.

=cut

# ============================================================================
sub GenerateMiogletId {
	my ($self, $config) = @_;

	if(! exists $self->{current_id_nb}) {
		my $name = tmpnam;
		$name =~ s!^/tmp/file!!sg;
		$self->{current_id_base} = $name;
		$self->{current_id_nb} = 0;
	}	

	my $id = "gid-".$self->{current_id_base}.$self->{current_id_nb};
	$self->{current_id_nb}++;

	return $id;
}
# ============================================================================

=head2 IdentifyNodes ()

	Add a unique identifier on node (hbox, vbox, TitledBox, html)

=cut

# ============================================================================
sub IdentifyNodes {
	my ($self, $config, $root) = @_;

	my $nodelist = $root->find('//*[local-name(.)="vbox" or local-name(.)="hbox" or '.
							       'local-name(.)="TitledBox" or local-name(.)="html" or '.
							       'local-name(.)="mioglet"]');
	
	foreach my $node ($nodelist->get_nodelist()) {
		my $node_id = $self->GenerateMiogletId($config);
		if(! defined $node->getAttribute("id")) {
			$node->setAttribute("id", $node_id);
		}
	}
	
}
# ============================================================================

=head2 Return the XSL file path  ($config, $theme, $lang, $file)

	Return a unique identifier for new Mioglets.

=cut

# ============================================================================
sub GetXSLFilePath {
	my ($self, $config, $theme, $lang, $file) = @_;

	my $xsl_dir = $config->GetXSLDir;
	
	foreach my $path ("${xsl_dir}/${theme}/${lang}/$file",
					  "${xsl_dir}/${theme}/$file",
					  "${xsl_dir}/default/$file") {
		
		if (-e $path) {
			return st_URIEscape($path);
		}
	}
	warn("Mioga2::Portal:GetXSLFilePath  theme = $theme, lan = $lang  file = $file\n") if ($debug);
	throw Mioga2::Exception::Simple("Mioga2::Portal:GetXSLFilePath", __x("No stylesheet file = '{filename}' for theme='{theme}' and lang = '{language}'", filename => $file, theme => $theme, language => $lang));
}
# ============================================================================

=head2 Store current portal description in session

=cut

# ============================================================================
sub CachePortalDescription {
	my ($self, $context, $xmldocument) = @_;
	my $session = $context->GetSession();

	my $nodes = $xmldocument->findnodes("/Portal/Mioga2Portal");
	
	$session->{PortalEditor}->{xmldocument} = $nodes->shift()->serialize;
}
# ============================================================================

=head2 Save current portal.

=cut

# ============================================================================
sub SavePortal {
	my ($self, $context, $xmldocument) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $root = $xmldocument->documentElement();

	
	# for each mioglet and inline html, remove id and ref params 
	# (just usefull Editor for internal purpose)

	my @mioglets = $root->findnodes("//mioglet");
	push @mioglets, $root->findnodes("//html");

	foreach my $mioglet (@mioglets) {
		$mioglet->removeAttribute('id');
		$mioglet->removeAttribute('ref');
	}

    my $xml = "<?xml version=\"1.0\"?>\n";
	$xml .= $root->serialize(1);
	my $path = $session->{PortalEditor}->{path};
	
	my $fullpath;
	if($context->GetURI()->IsPrivate()) {
		$fullpath = $config->GetPrivateDir();
	}
	else {
		$fullpath = $config->GetPublicDir();
	}
	
	$fullpath .= "/" . $context->GetGroup()->GetIdent() . "$path";
	
	open(F_PORTAL, ">$fullpath")
		or die "Can't open $fullpath : $!";
	
    utf8::encode($xml);
	print F_PORTAL $xml;
			
	close(F_PORTAL);
			
	$session->{__internal}->{message}{type} = 'info';
	$session->{__internal}->{message}{text} = __('Portal successfully saved.');
	my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	$content->SetContent('DisplayMain');
	return $content;
}
# ============================================================================

=head2 Update document description from http args

=cut

# ============================================================================
sub UpdateCurrentDescription {
	my ($self, $context, $xmldocument) = @_;
	warn("Portal::UpdateCurrentDescription\n") if ($debug);

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $root = $xmldocument->documentElement();

	if(exists $context->{args}->{delete_id}) {
		my $node = $self->getAncestorOfId($root, $context->{args}->{delete_id});
		$node->parentNode()->removeChild($node);
		$context->{args}->{redirect_select_id} = "";
	}
	
	elsif(exists $context->{args}->{up_id}) {
		my $node = $self->getAncestorOfId($root, $context->{args}->{up_id});
		my $next;
		for($next = $node->nextSibling(); $next->nodeType() == 3; $next = $next->nextSibling){};
		my $parent = $node->parentNode();

        $parent->removeChild($node);
		$parent->insertAfter($node, $next);

		$context->{args}->{redirect_select_id} = $context->{args}->{up_id};
	}
	
	elsif(exists $context->{args}->{down_id}) {
		my $node = $self->getAncestorOfId($root, $context->{args}->{down_id});

		my $prev;
		for($prev = $node->previousSibling(); $prev->nodeType() == 3; $prev = $prev->previousSibling){};
		my $parent = $node->parentNode();

        $parent->removeChild($node);
		$parent->insertBefore($node, $prev);

		$context->{args}->{redirect_select_id} = $context->{args}->{down_id};
	}

	elsif(exists $context->{args}->{add_before_id}) {
		my $node = $self->getAncestorOfId($root, $context->{args}->{add_before_id});

        my $newBox = $self->CreateNewBox($config);
		$self->IdentifyNodes($config, $newBox);

		my $parent = $node->parentNode();
		$parent->insertBefore($newBox, $node);

		$context->{args}->{redirect_select_id} = $context->{args}->{add_before_id};
	}

	elsif(exists $context->{args}->{add_after_id}) {
		my $node = $self->getAncestorOfId($root, $context->{args}->{add_after_id});

        my $newBox = $self->CreateNewBox($config);
		$self->IdentifyNodes($config, $newBox);

		my $parent = $node->parentNode();
		$parent->insertAfter($newBox, $node);

		$context->{args}->{redirect_select_id} = $context->{args}->{add_after_id};
 	}

	elsif(st_ArgExists($context, "set_mioglet")) {
		my $path = "//*[\@id='$context->{args}->{id}']";
		warn("Portal::UpdateCurrentDescription set_mioglet path = $path\n") if ($debug);
		my $node = $root->findnodes("//*[\@id='$context->{args}->{id}']")->shift;
		if (!defined ($node)) {
			throw Mioga2::Exception::Simple("Mioga2::Portal::UpdateCurrentDescription", __x("Cannot find node for path {path}", path => $path));
		}

		my $xml = "";
		if($context->{args}->{"mioglet_name"} eq 'inline-html') {
			$xml = "<html><content>HTML</content></html>"
		}
		else {
			my ($mioglet_ident, $method_ident);
			if($context->{args}->{"mioglet_name"} =~ /^[vh]box$/) {
				$xml = "<$context->{args}->{mioglet_name}><box>";
				($mioglet_ident, $method_ident) = ("Base", "MiogletSelector");
			}
			else {
				($mioglet_ident, $method_ident) = split(/\//, $context->{args}->{"mioglet_name"});
			}
			
			if(!defined $method_ident) {
				$context->{args}->{redirect_select_id} = $context->{args}->{id};
				return;
			}
			
			warn("Portal::UpdateCurrentDescription mioglet_ident = $mioglet_ident  method_ident = $method_ident\n") if ($debug);
			my $mioglet_desc = new Mioga2::Portal::MiogletDesc($config, $mioglet_ident);
			my $mioglet_pkg = $mioglet_desc->GetPackage();
			warn("Portal::UpdateCurrentDescription mioglet_pkg = $mioglet_pkg\n") if ($debug);
			
			eval "require $mioglet_pkg;
                 \$xml .= $mioglet_pkg->GetDefaultXML(\$mioglet_desc, '$method_ident');";
			
			if($@) {
				throw Mioga2::Exception::Simple("Mioga2::Portal->UpdateCurrentDescription", __x("Problem when initializing new mioglet: {error}", error => $@));
			}
			
			if($context->{args}->{"mioglet_name"} =~ /^[vh]box$/) {
				$xml .= "</box></$context->{args}->{mioglet_name}>";
			}
			
		}
		warn("Portal::UpdateCurrentDescription xml = $xml\n") if ($debug);

		my $newnode = $self->{parser}->parse_balanced_chunk($xml)->getFirstChild();
		$newnode->setAttribute("id", $node->getAttribute("id"));
		$node->replaceNode($newnode);
		
		$context->{args}->{redirect_select_id} = $newnode->getAttribute("id");
 	}
	
	elsif(st_ArgExists($context, 'insert_root')) {
		my $container = new XML::LibXML::Element($context->{args}->{root_container});
		my $box = new XML::LibXML::Element("box");
		$container->addChild($box);

		my $root_cont = ($root->findnodes("//portal/*[name(.) = 'vbox' or name(.) = 'hbox']"))[0];
		
		$root_cont->replaceNode($container);
		$box->addChild($root_cont);
	}

	if(!exists $context->{args}->{mioglet_id}) {
		return;
	}

	my $node = $root->findnodes("//*[\@id='$context->{args}->{mioglet_id}']")->shift;
	if(! defined $node) {
		throw Mioga2::Exception::Simple("Mioga2::Portal->UpdateCurrentDescription", __"Invalid id");
	}

	$self->RemoveTag($node, "titled");
	$self->RemoveTag($node, "boxed");

	if($node->parentNode()->nodeName() eq 'portal') {
		my $xml = "";	

		if(exists $context->{args}->{param_bgcolor}
		   and  $context->{args}->{param_bgcolor} ne '') {
			$xml .= "<background color=\"".st_FormatXMLString($context->{args}->{param_bgcolor})."\"/>";
		}

		if(exists $context->{args}->{param_bgimage}
			  and  $context->{args}->{param_bgimage} ne '') {
			$xml .= "<background image=\"".st_FormatXMLString($context->{args}->{param_bgimage})."\"/>";
		}

		if(exists $context->{args}->{param_theme}
			  and  $context->{args}->{param_theme} ne '') {
			$xml .= "<theme>".st_FormatXMLString($context->{args}->{param_theme})."</theme>";
		}

		if(exists $context->{args}->{param_name}) {
			$root->find("/Mioga2Portal")->shift->setAttribute("name", $context->{args}->{param_name});
		}

		if(exists $context->{args}->{description}) {
			$xml .= "<description>".$context->{args}->{description}."</description>";
		}
		if(exists $context->{args}->{css_file}) {
			$xml .= "<css_file>".$context->{args}->{css_file}."</css_file>";
		}

		my $global = $root->findnodes("//global")->shift;
		$global->removeChildNodes();
		$global->appendWellBalancedChunk($xml);

		delete $context->{args}->{param_bgcolor};
		delete $context->{args}->{param_bgimage};
		delete $context->{args}->{param_theme};
		delete $context->{args}->{param_name};
		delete $context->{args}->{description};
	}

	my $xml = "";	
	my @borders;

	$node->removeAttribute("href");

	foreach my $arg (keys %{$context->{args}}) {
		next if($arg eq 'path' or $arg eq 'mioglet_id' or $arg eq 'select_id');

		next if($arg =~ /^change_mioglet_param/);

		$self->RemoveTag($node, $arg);

		if($arg eq 'title' and $context->{args}->{title} ne '') {
			$xml .= "<titled/>";
		}
		elsif($arg eq 'boxed') {
			$xml .= "<boxed/>";
			next;
		}
		
		if($arg eq 'href' and $context->{args}->{href} !~ m!^/! and $context->{args}->{href} !~ /:/) {
			$context->{args}->{href} = "/$context->{args}->{href}";
		}

	
		if($arg =~ /^param_(.*)$/) {
			my $realarg = $1;
			
			if($realarg =~ /^border_(.*)$/) {
				push @borders, $1;
			}
			elsif(grep {$realarg eq $_} qw(bgcolor bgimage width)) {
				my $ancestor = $self->getAncestorOfId($root, $context->{args}->{mioglet_id});
				$ancestor->setAttribute($realarg, $context->{args}->{$arg});
			}
			else {
				$node->setAttribute($realarg, $context->{args}->{$arg});
			}
		}
		
		elsif($arg eq 'content') {
			my $html_doc = $self->{parser}->parse_html_string($context->{args}->{content});
			my $html = $html_doc->documentElement()->serialize(0);

			$html =~ s/<html(.*)?><body>//g;
			$html =~ s!</body></html>!!g;

			$xml .= "<content>$html</content>";
		}
		else {
			$xml .= "<$arg>".st_FormatXMLString($context->{args}->{$arg})."</$arg>";
		}

	}

	
	if($node->parentNode()->nodeName() ne 'portal') {
		my $ancestor = $self->getAncestorOfId($root, $context->{args}->{mioglet_id});
		if(@borders) {
			$ancestor->setAttribute('border', join(' ', @borders)); 
		}
		else {
			$ancestor->removeAttribute('border'); 
		}
	}

	if($xml ne "") {
		$node->appendWellBalancedChunk($xml);
	}
}

# ============================================================================

=head2 Remove child with tag name $name in node $node

=cut

# ============================================================================

sub RemoveTag {
	my ($self, $node, $name) = @_;

	my $oldnode = $node->getChildrenByTagName($name)->shift;
	if(defined $oldnode) {
		$node->removeChild($oldnode);
	}
}


# ============================================================================

=head2 Return the node ancestor of node with given id (be carreful of content indirection)

=cut

# ============================================================================
sub getAncestorOfId {
	my ($self, $root, $id) = @_;

	my $node = $root->findnodes("//*[\@id='$id']")->shift;
	if(! defined $node) {
		throw Mioga2::Exception::Simple("Mioga2::Portal::getAncestorOfId", __"Invalid id");
	}
	
	my $elemnode = $node->findnodes("ancestor-or-self::box[1]")->shift;
	if(! defined $elemnode) {
		my $content = $node->findnodes("ancestor-or-self::content[1]")->shift;
		my $content_id = $content->getAttribute('id');
		$elemnode = $root->findnodes("//box[\@ref='$content_id']")->shift;
	}

	if(! defined $elemnode) {
		throw Mioga2::Exception::Simple("Mioga2::Portal::getAncestorOfId", __"Invalid content id");
	}

	return $elemnode;
}
# ============================================================================

=head2 Return XML decribing portal files present in given group private 
	space root

=cut

# ============================================================================
sub GetXMLPortalList {
	my ($self, $config, $user, $group) = @_;

	my $private_space = $config->GetPrivateDir . "/" . $group->GetIdent;
	my $private_uri  = $config->GetPrivateURI . "/" . $group->GetIdent;


	my $parser = XML::LibXML->new;

	my $xml = "<PortalFiles>";

	foreach my $path (<$private_space/*.xml>) {
		my ($dir, $file) = ($path =~ m!^(.*)/([^/]+)$!);
		
		my $data;
		{
			local $/ = undef;
			if(!open(F_FILE, $path)) {
				warn "Can't open $path";
				return;
			}
			
			$data = <F_FILE>;

			close(F_FILE);
		}

		next if($data !~ /Mioga2Portal/);
		
		my $ok = 0;
		my $root;
		try {
			my $doc = $parser->parse_file($path);
			$root = $doc->documentElement();
			$ok = 1;
		};
		next unless $ok;

		my $name = $root->getAttribute("name");
        utf8::encode($name);
		$name = "noname" unless defined $name;

		my @nodes = $root->findnodes('global/description');
		my $description = "";
		if(@nodes) {
			$description = $nodes[0]->toString(0);
            utf8::encode($description);
		}
		
		my $uri = $path;
		$uri =~ s!$private_space!$private_uri!g;

		my $uriobj = new Mioga2::URI($config, uri => $uri);
		my $authz = AuthzTestRightAccessForURI($config, $uriobj, $user, $group);

		$xml .= '<portal group="' . st_FormatXMLString($group->GetIdent) .'" '.
			             'canwrite="' . ($authz == AUTHZ_WRITE) . '" '.
			             'name="' . st_FormatXMLString($name) . '" '.
                         'file="' . st_FormatXMLString($file) . '">';
		$xml .= $description;
		$xml .= '</portal>';
	}

	$xml .= "</PortalFiles>";
	return $xml;	
}
# ============================================================================

=head2 Return XML describing available mioglets (declare in DB)

=cut

# ============================================================================
sub GetXMLMiogletList {
	my ($self, $config) = @_;

	my $dbh = $config->GetDBH();
	my $mioglets = SelectMultiple($dbh, 
								  "SELECT portal_mioglet.xml_prefix, portal_mioglet_method.ident, ".
								  "       portal_mioglet.ident AS mioglet_ident ".
								  "FROM portal_mioglet, portal_mioglet_method, m_application, m_instance_application ".
								  "WHERE m_application.rowid = portal_mioglet.app_id AND ".
								  "      portal_mioglet_method.mioglet_id = portal_mioglet.rowid AND ".
								  "      portal_mioglet_method.ident NOT IN ('OtherContent', 'Error') AND ".
								  "      m_instance_application.application_id = m_application.rowid AND ".
								  "      m_instance_application.mioga_id = " . $config->GetMiogaId());
	
	my $xml = "<InstalledMioglets>";
	
	foreach my $mioglet (@$mioglets) {
		my $mioglet_uri = "http://www.mioga2.org/portal/mioglets/".$mioglet->{mioglet_ident};	

		$xml .= qq|<$mioglet->{xml_prefix}:$mioglet->{ident} xmlns:$mioglet->{xml_prefix}="$mioglet_uri"/>|;
	}

	$xml .= '<base:image xmlns:base="http://www.mioga2.org/portal/mioglets/Base"/>';
	$xml .= '<base:html  xmlns:base="http://www.mioga2.org/portal/mioglets/Base"/>';
	$xml .= '<base:none  xmlns:base="http://www.mioga2.org/portal/mioglets/Base"/>';
	$xml .= '<base:box  xmlns:base="http://www.mioga2.org/portal/mioglets/Base"/>';
	
	$xml .= "</InstalledMioglets>";
	return $xml;
}
# ============================================================================

=head2 Create a new portal file

=cut

# ============================================================================
sub CreateNewPortal {
	my ($self, $config, $group, $user, $values) = @_;

	my $private_dir = $config->GetPrivateDir() . "/" . $group->GetIdent() . "/" .$values->{filename};
	my $private_uri = $config->GetPrivateURI() . "/" . $group->GetIdent() . "/" .$values->{filename};

	my $xml = '<?xml version="1.0"?>';
	$xml .= '<Mioga2Portal name="'.st_FormatXMLString($values->{name})."\">\n";
	$xml .= "\t<global>\n";
	$xml .= "\t\t<description>\n";
	$xml .= $values->{description};
	$xml .= "\t\t</description>\n";
	$xml .= "\t</global>\n";
	$xml .= "\t<portal>\n";
	$xml .= "\t\t<mioglet type=\"Base/MiogletSelector\">\n";
	$xml .= "\t\t\t<stylesheet>base_mioglet.xsl</stylesheet>\n";
	$xml .= "\t\t\t<mode>user</mode>\n";
	$xml .= "\t\t</mioglet>\n";
	$xml .= "\t</portal>\n";
	$xml .= '</Mioga2Portal>';

	open(F_PORTAL, ">$private_dir") or die "Can't open $private_dir : $!";

	print F_PORTAL $xml;

	close(F_PORTAL);

  my $mime  = mimetype($private_dir);
  utf8::encode($mime);
  $mime  = 'directory' if $mime =~ /directory/;
  my $size = (-s $private_dir) || 0;

	AuthzDeclareURI($config->GetDBH, $private_uri, $user->GetRowid, $mime, $size);
}
# ============================================================================

=head2 Initialize portal creation session

=cut

# ============================================================================

sub InitializeNewPortalValues {
	my ($self, $context, $values) = @_;

	my $session = $context->GetSession();

	my $default = { filename => "",
					name => "",
					description => "",
				};
	
	
	foreach my $key (qw(filename name description)) {
		if(exists $values->{$key}) {
			next;
		}
		elsif(exists $context->{args}->{$key}) {
			$values->{$key} = $context->{args}->{$key};
		}
		elsif(exists $session->{Portal}->{Create}->{$key}) {
			$values->{$key} = $session->{Admin}->{User}->{$key};
		}
		elsif(exists $default->{$key}) {
			$values->{$key} = $default->{$key};
		}
	}

	$session->{Portal}->{Create} = $values;
}


# ============================================================================

=head2 GetXMLThemeList($config)
	
	Return XML theme list.

=cut

# ============================================================================

sub GetXMLThemeList {
	my ($self, $config) = @_;

	my $dbh = $config->GetDBH();
	my $res = SelectMultiple($dbh, "SELECT * FROM m_theme WHERE mioga_id = ".$config->GetMiogaId());

	my $xml = "<ThemeList>";
	$xml .= '<theme ident="portal">Portal</theme>';
	
	foreach my $theme (@$res) {
		$xml .= '<theme ident="'.st_FormatXMLString($theme->{ident}).'">'.st_FormatXMLString($theme->{name}).'</theme>';
	}

	$xml .= "</ThemeList>";

	return $xml;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
