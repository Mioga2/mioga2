# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
	
Magellan.pm : The Mioga2 files explorer.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Magellan;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'magellan';

use vars qw($VERSION $CODENAME);
$VERSION  = "1.1a";
$CODENAME = 'Victoria';

use Error qw(:try);
use Mioga2::Content::ASIS;
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::Content::SendFile;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::Classes::Time;
use Mioga2::Louvre;
use Mioga2::Mermoz;
use Mioga2::Magellan::LargeList;
use Mioga2::Magellan::Archive;
use Mioga2::tools::Convert;
use Mioga2::TagList;
use Mioga2::Constants;
use MIME::Entity;
use Data::Dumper;
use POSIX qw( getcwd );
use File::MimeInfo::Magic;
use File::Basename;
use Text::Iconv;
use XML::Atom::SimpleFeed;
use IPC::Open3;

require Mioga2::Magellan::DAV;
require Mioga2::Magellan::Database;
require Mioga2::Magellan::Properties;

my $debug = 0;

# ============================================================================
# CheckUserAccessOnMethod ($context, $user, $group, $method_ident)
#
# if the user $user working in group $group can access to
#	the method $method_ident in the current application .
#	
# ============================================================================

sub CheckUserAccessOnMethod {
	my ($self, $context, $user, $group, $method_ident) = @_;

	if($method_ident eq 'AccessRights')
	{
		my $uri;
		if (exists ($context->{args}->{uri}))
		{
			$uri = $context->{args}->{uri};
		}
		else
		{
			$uri = $context->GetSession()->{Magellan}->{AccessRights}->{uri};
		}

		my $config = $context->GetConfig();
		my $current_group_ident = $group->GetIdent();

		my $private = $config->GetPrivateURI()."/$current_group_ident";
		my $public = $config->GetPublicURI()."/$current_group_ident";

		if ($uri =~ /^($private|$public)\/?$/)
		{
			return $self->CheckUserAccessOnFunction($context, $user, $group, 'RootACL');
		}
		else {
			return $self->CheckUserAccessOnFunction($context, $user, $group, 'OtherACL');
		}
	}

	return $self->SUPER::CheckUserAccessOnMethod($context, $user, $group, $method_ident);

}

# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc {
    my $self    = shift;
    my %AppDesc = (
        ident              => 'Magellan',
	name	 	   => __('Magellan'),
        package            => 'Mioga2::Magellan',
        description        => __('Mioga2 files explorer'),
        type               => 'normal',
        all_groups         => 1,
        all_users          => 1,
        can_be_public      => 0,
		is_user             => 1,
		is_group            => 1,
		is_resource         => 0,

        functions => {
            'Read'    => __('Browse files'),
            'RootACL' => __('Access rights management for groups root directories'),
            'OtherACL' => __('Access rights management for other directories'),
			'Hidden'   => __('View hidden files and folders'),
            'History' => __('History management'),
			'Properties' => __('Manage file properties'),
        },

        func_methods => {
            'RootACL'  => [ 'AccessRights', 'ParentAccessRights' ],
            'OtherACL'  => [ 'AccessRights', 'ParentAccessRights' ],
			'Hidden' => [],
            'Read' => [
                'DisplayMain',       'GetNodes',
                'GetFullNodes',      'MimeList',
                'IncludeMime',       'DownloadFiles',
                'MonitorDecompress', 'GetHistory',
                'DownloadRevision',  'LastModified',
                'CreateResource',    'MoveResource',
                'DeleteResources',   'PasteResources',
                'UploadFile',        'LockResource',
                'UnlockResource',    'Extract',
                'GetResource',       'EditResource',
                'GetComments',       'AddComment',
				'AccessRights',      'OtherACL',
				'SetPreferences',
            ],
            'History' => ['DeleteRevision', 'EnableHistory', 'DisableHistory'],
			'Properties' => ['GetTagList', 'GetProperties', 'SetProperties']
        },

        sxml_methods => {},

        xml_methods => {},

		non_sensitive_methods => [
			'DisplayMain',
			'GetNodes',
			'GetFullNodes',
			'MimeList',
			'IncludeMime',
			'DownloadFiles',
			'MonitorDecompress',
			'GetHistory',
			'DownloadRevision',
			'LastModified',
			'GetResource',
			'GetComments',
			'AccessRights',
			'OtherACL',
			'GetTagList',
			'GetProperties',
		]

    );
    return \%AppDesc;
}

# ============================================================================
# Public Methods
# ============================================================================
# ============================================================================

=head2 DisplayMain ()

=cut

# ============================================================================

sub DisplayMain {
    my ( $self, $context ) = @_;

    my $data = { DisplayMain => $context->GetContext () };
    my $config = $context->GetConfig;
	my $session = $context->GetSession ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	[ [ 'path'], 'allow_empty', ['location', $config]],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::DisplayMain", __("Wrong argument value."))
	}

	my $path   = $values->{path};
    my $user   = $context->GetUser;
    my $group  = $context->GetGroup;
    $data->{DisplayMain}->{CanWrite} = 1
      if $self->CheckUserAccessOnFunction( $context, $user, $group, "Write" );
    my $mermoz = Mioga2::Mermoz->new($config);
    $data->{DisplayMain}->{CanSendMail} = 1
      if $mermoz->CheckUserAccessOnMethod( $context, $user, $group,
              'SendMail' );
    $data->{DisplayMain}->{Instance} = $context->GetConfig->GetMiogaIdent;
    $data->{DisplayMain}->{Infos} = {
        version  => $VERSION,
        codename => $CODENAME,
    };
	$data->{DisplayMain}->{Preferences} = $session->{magellan_prefs};
    $data->{DisplayMain}->{Path} = $path
      if $path && $path !~ /^\s*$/;

    $data->{DisplayMain}->{isUserApp} = (ref ($group) eq 'Mioga2::Old::User') ? 1 : 0;

	my $taglist = Mioga2::TagList->new ($context->GetConfig ());
	@{$data->{DisplayMain}->{tag}} = map { s/(["'])/\\$1/g; $_ } @{$taglist->GetTags ()};
	$data->{DisplayMain}->{free_tags} = $taglist->GetFreeTagsStatus ();


	$data->{DisplayMain}->{no_header} = $session->{no_header};

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "[Mioga2::Magellan::DisplayMain] XML: $xml\n" if ($debug);

    my $content = new Mioga2::Content::XSLT(
        $context,
        stylesheet    => 'magellan.xsl',
        locale_domain => "magellan_xsl"
    );
    $content->SetContent ($xml);

    return $content;
}

# ============================================================================

=head2 GetNodes ()

return informations on files & folders in node

=cut

# ============================================================================

sub GetNodes {
    my ( $self, $context ) = @_;
	print STDERR "Magellan::GetNodes()\n" if ($debug);

    my $config = $context->GetConfig;
	
	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
				[ [ 'files' ], 'allow_empty', 'want_int'],
				[ [ 'node'], 'disallow_empty', ['location', $config]],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::GetNodes", __("Wrong argument value."))
	}

	# Full refresh
	if ($values->{refresh}) {
		Mioga2::DAVFS::RefreshDirectoryInfo ($config, $values->{node});
	}

    my $files  = $context->{args}->{files};
    my $node   = $context->{args}->{node};
    my $data   = $self->{data};
    my $user   = $context->GetUser;
    my $group  = $context->GetGroup;

    my $private_dav = $config->GetPrivateURI;
    my $public_dav  = $config->GetPublicURI;
    my $root        = $config->GetBaseURI;

	my $inconsistent = 'false';

    if ( $node eq $root ) {
        $data = [
            {
                text     => "home",
                id       => $private_dav,
                mimetype => 'directory',
                size     => 0,
                locked   => 0,
                rights   => 'r',
                manage   => 'false'
            },
            {
                text     => "public",
                mimetype => 'directory',
                id       => $public_dav,
                size     => 0,
                locked   => 0,
                rights   => 'r',
                manage   => 'false'
            },
        ];
    }
    else {
        $data = [];

		my $uriobj = new Mioga2::URI($config, uri => $node);
		my $groupobj = $uriobj->GetGroup;
		my $mygroup;
		if (defined ($groupobj)) {
			$mygroup = $groupobj;
		}
		else {
			$mygroup = $group;
		}

        my $resources = Mioga2::Magellan::DAV::GetCollection(
            $context,
            $context->GetUserAuth,
            $config->GetMiogaConf ()->GetDAVProtocol () . "://" . $config->GetMiogaConf ()->GetDAVHost () . ":" . $config->GetMiogaConf ()->GetDAVPort (), $node
        );

		$inconsistent = pop (@$resources);

        foreach my $res (@$resources) {
            next if ( !$files && $res->{type} ne "collection" );
			next if ($res->{hidden} && !$res->{view_hidden});

			try {
				if ($node eq $public_dav || $node eq $private_dav) {
					# Check if user can manage access rights on target resource
					my $resuri = new Mioga2::URI ($config, uri => "$res->{name}");
					my $resgrp = $resuri->GetGroup ();
					my $resgrpcfg = $resgrp->{config};
					$res->{manage} =
					  $self->CheckUserAccessOnFunction( $context, $user, $resgrp, 'RootACL')
					  ? 1
					  : 0;
					$res->{email} = 0;          # No use to send link to group home through Mermoz
					$res->{diderot} = 0;        # No use to attach a directory to a Diderot notice
				}

				my $type =
				  $res->{type} eq "collection" ? "directory" : $res->{type};

				my $louvre;
				if ( $type eq "directory" ) {
					$louvre = Mioga2::Louvre::Gallery::SQL->uri_is_gallery($config->GetDBH (), $res->{name}); # TODO ( - 09/05/2012 17:18): Move to DAVFS
					$louvre->{perm} = $res->{louvre} ? 'true' : 'false';
				}

				my $diderot = 'false';
				if ($type ne 'directory') {
					$diderot = $res->{diderot} ? 'true' : 'false';
				}

				push @$data,
				  {
					group      => $res->{group_ident},
					size       => $res->{size},
					text       => $res->{short_name},
					id         => $res->{name},
					mimetype   => $res->{mimetype},
					locked     => $res->{locked},
					unlockable => $res->{unlockable},
					lock_owner => $res->{lock_owner},
					user_is_lock_owner => $res->{user_is_lock_owner},
					author     => $res->{author},
					ident      => $res->{ident},
					rights     => $res->{access_right},
					modified   => du_ConvertGMTISOToLocale($res->{date}, $context->GetUser),
					can_set_history    => $res->{can_set_history} ? 'true' : 'false',
					group_has_history    => $res->{group_has_history} ? 'true' : 'false',
					history_enabled    => $res->{history_enabled} ? 'true' : 'false',
					history    => $res->{history} ? 'true' : 'false',
					manage     => $res->{manage} ? 'true' : 'false',
					email      => $res->{email} ? 'true' : 'false',
					diderot    => $diderot,
					elements   => $res->{elements},
					comments   => $res->{comments},
					setprops   => $res->{setprops} ? 'true' : 'false',
					properties => $self->GetPropertiesForURI ($res->{name}),
					louvre     => $louvre # hashref
				  };
			}
			otherwise {
				$inconsistent = 'true';
			};
        }
        @$data = sort { lc( $a->{text} ) cmp lc( $b->{text} ) } @$data;
    }

	# If called directly (initializing tree view), do not report inconsistency
	if ($context->{method} ne 'GetNodes') {
		push (@$data, $inconsistent);
	}
    return $data;
}

# ============================================================================

=head2 GetFullNodes ()

return informations on files & folders in node

=cut

# ============================================================================

sub GetFullNodes {
    my ( $self, $context ) = @_;
    my $nodes = $self->GetNodes($context, 1);

	my $inconsistent = pop (@$nodes);

    my $data = { resources => $nodes, inconsistent => $inconsistent };
    return $data;
}

# ============================================================================

=head2 GetComments ()

return comments for a given node

=cut

# ============================================================================

sub GetComments {
    my ( $self, $context ) = @_;
	print STDERR "Magellan::GetComments()\n" if ($debug);
    my $config  = $context->GetConfig;
    my $dbh     = $config->GetDBH;
    
	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['node'], 'disallow_empty', ['location', $config] ],
			];
	my ($args, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::GetComments", __("Wrong argument value."))
	}

	my $node    = $args->{node};

	my $user   = $context->GetUser;
	my $uri = new Mioga2::URI($config, uri=>$node);
	my $right = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_READ);
	if (defined $right and $right) {
		my $comments    = SelectMultiple($dbh, "SELECT fc.modified, fc.comment, (ub.firstname || ' ' || ub.lastname) AS author FROM file_comment AS fc, m_user_base AS ub, m_uri AS u WHERE u.uri = '" . st_FormatPostgreSQLString($node) . "' AND fc.uri_id = u.rowid AND ub.rowid = fc.user_id");
		map { $_->{modified} =~ s/\..*$// } @$comments;
		map { utf8::decode($_->{comment})} @$comments;

		print STDERR "comments = ".Dumper($comments)."\n" if ($debug);

		my $data = { comments => $comments };
		return $data;
	}
	else {
		throw Mioga2::Exception::Application ("Mioga2::Magellan::GetComments", __"Insufficiant rights to access this method for URI: " . $node);
	}
}

# ============================================================================

=head2 AddComment ()

  add a comment to a file

=cut

# ============================================================================

sub AddComment {
    my ( $self, $context ) = @_;

    my $config  = $context->GetConfig;
    my $data    = $self->{data};
    my $error   = undef;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['node'], 'disallow_empty', ['location', $config] ],
				[ ['comment'], 'disallow_empty' ],
			];
	my ($args, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::AddComment", __("Wrong argument value."))
	}

    if ( $args->{comment} =~ /^\s*$/ ) {
        $error = __ "'comment' argument is missing!";
    }
    elsif ( $args->{node} =~ /^\s*$/ ) {
        $error = __ "'node' argument is missing!";
    }

    $data->{success}    = 'false';
    $data->{node}       = $args->{node};

    unless ($error) {
		my $user   = $context->GetUser;
		my $uri = new Mioga2::URI($config, uri=>$data->{node});
		my $right = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_WRITE);
		if (defined $right and $right) {
			try {
				my $config  = $context->GetConfig;
				my $user    = $context->GetUser;
				my $user_id = $user->GetRowid;
				my $dbh     = $config->GetDBH;
				my $uri_id  = SelectSingle($dbh, "SELECT rowid FROM m_uri WHERE uri='" . st_FormatPostgreSQLString($args->{node}) . "'");
				$uri_id     = $uri_id->{rowid};
				
				ExecSQL($dbh, "INSERT INTO file_comment(comment, user_id, uri_id) VALUES('" . st_FormatPostgreSQLString($args->{comment}) . "', $user_id, $uri_id)");
				$data->{success} = 'true';
			}
			otherwise {
				my $ex  = shift;
				$error  = $ex->as_string;
			};
		}
		else {
			throw Mioga2::Exception::Application ("Mioga2::Magellan::AddComment", __"Insufficiant rights to access this method for URI: " . $data->{node});
		}
	}
	$data->{error} = $error if $error;
	return $data;
}

# ============================================================================

=head2 DeleteResources ()

delete one or more resources

=cut

# ============================================================================

sub DeleteResources {
    my ( $self, $context ) = @_;

    my $config = $context->GetConfig;
    my $base   = $config->GetMiogaConf ()->GetDAVProtocol () . "://" . $config->GetMiogaConf ()->GetDAVHost () . ":" . $config->GetMiogaConf ()->GetDAVPort ();
    my $data   = $self->{data};
    my $error  = undef;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['entries'], 'disallow_empty', ['location', $config] ],
			];
	my ($args, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::DeleteResources", __("Wrong argument value."))
	}

    if ( !$args->{entries} ) {
        $error = __ "'entries' argument is missing!";
    }
    $data->{success} = 'false';

	my $delete_errors = [];
    unless ($error) {
		$args->{entries} = [ $args->{entries} ]
		if ( ref( $args->{entries} ) ne 'ARRAY' );

		foreach my $entry ( @{ $args->{entries} } ) {
			my $user   = $context->GetUser;
			my $uri = new Mioga2::URI($config, uri=>$entry);
			my $right = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_WRITE);
			if (defined $right and $right) {
				my $res = Mioga2::Magellan::DAV::DeleteURI(
					$context->GetUserAuth,
					$base . $entry
				);
				if ($res) {
					push (@$delete_errors, {uri => $entry, code => $res});
				}
			}
			else {
				throw Mioga2::Exception::Application ("Mioga2::Magellan::DeleteResources", __"Insufficiant rights to access this method for URI: " . $entry);
			}
		}

		if (!@$delete_errors) {
			$data->{success} = 'true';
		}
	}

	$data->{errors} = $delete_errors if scalar (@$delete_errors);
	return $data;
}

# ============================================================================

=head2 MoveResource ()

move (or rename) a resource from source arg to destination arg

=cut

# ============================================================================

sub MoveResource {
    my ( $self, $context ) = @_;

    my $config = $context->GetConfig;
    my $base   = $config->GetMiogaConf ()->GetDAVProtocol () . "://" . $config->GetMiogaConf ()->GetDAVHost () . ":" . $config->GetMiogaConf ()->GetDAVPort ();
    my $data   = $self->{data};
    my $error  = undef;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['source', 'destination'], 'disallow_empty', ['location', $config] ],
			];
	my ($args, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::MoveResource", __("Wrong argument value."))
	}

    if ( $args->{source} =~ /^\s*$/ ) {
        $error = __ "'source' argument is missing!";
    }
    elsif ( $args->{destination} =~ /^\s*$/ ) {
        $error = __ "'destination' argument is missing!";
    }

    $data->{success}     = 'false';
    $data->{source}      = $args->{source};
    $data->{destination} = $args->{destination};

    unless ($error) {
		my $user   = $context->GetUser;
		# Check read access to source
		my $uri = new Mioga2::URI($config, uri=>$data->{source});
		my $rright = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_READ);
		# Check write access to destination parent directory
		my $dest = $data->{destination};
		$dest =~ s/[^\/]*$//;
		$uri = new Mioga2::URI($config, uri=>$dest);
		my $wright = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_WRITE);
		if (defined $rright and $rright and defined $wright and $wright) {
			my $res = Mioga2::Magellan::DAV::MoveURI(
				$context->GetUserAuth,
				'T', "$base$args->{source}", "$base$args->{destination}"
			);
			unless ($res) {
				$data->{success} = 'true';
			}
			else {
				push (@{$data->{errors}}, {uri => $data->{destination}, code => $res});
			}
		}
		else {
			throw Mioga2::Exception::Application ("Mioga2::Magellan::MoveResource", __"Insufficiant rights to access this method for URI: " . "(" . $data->{source} . " => " . $data->{destination} . ")");
		}
    }
	$data->{error} = $error if $error;
	return $data;
}

# ============================================================================

=head2 LockResource ()

  lock a resource

=cut

# ============================================================================

sub LockResource {
    my ( $self, $context ) = @_;

    my $config = $context->GetConfig;
    my $base   = $config->GetMiogaConf ()->GetDAVProtocol () . "://" . $config->GetMiogaConf ()->GetDAVHost () . ":" . $config->GetMiogaConf ()->GetDAVPort ();
    my $data   = $self->{data};
    my $error  = undef;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['resource'], 'disallow_empty', ['location', $config] ],
			];
	my ($args, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::LockResource", __("Wrong argument value."))
	}

    if ( $args->{resource} =~ /^\s*$/ ) {
        $error = __ "'resource' argument is missing!";
    }

    $data->{success}  = 'false';
    $data->{resource} = $args->{resource};

    unless ($error) {
		my $user   = $context->GetUser;
		my $uri = new Mioga2::URI($config, uri=>$data->{resource});
		my $right = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_WRITE);
		if (defined $right and $right) {
			my $res = Mioga2::Magellan::DAV::LockResource(
				$context,
				$context->GetUserAuth,
				"$base$args->{resource}"
			);
			if (!$res) {
				$data->{success} = 'true';
			}
			else {
				$data->{success} = 'false';
				push (@{$data->{errors}}, {uri => $args->{resource}, code => $res});
			}
		}
		else {
			throw Mioga2::Exception::Application ("Mioga2::Magellan::LockResource", __"Insufficiant rights to access this method for URI: " . $data->{resource});
		}
    }
	$data->{error} = $error if $error;
    return $data;
}

# ============================================================================

=head2 UnlockResource ()

  lock a resource

=cut

# ============================================================================

sub UnlockResource {
    my ( $self, $context ) = @_;

    my $config = $context->GetConfig;
    my $base   = $config->GetMiogaConf ()->GetDAVProtocol () . "://" . $config->GetMiogaConf ()->GetDAVHost () . ":" . $config->GetMiogaConf ()->GetDAVPort ();
    my $data   = $self->{data};
    my $error  = undef;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['resource'], 'disallow_empty', ['location', $config] ],
			];
	my ($args, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::UnlockResource", __("Wrong argument value."))
	}

    if ( $args->{resource} =~ /^\s*$/ ) {
        $error = __ "'resource' argument is missing!";
    }

    if ( $args->{resource} =~ /^\s*$/ ) {
        $error = __ "'resource' argument is missing!";
    }

    $data->{success}  = 'false';
    $data->{resource} = $args->{resource};

    unless ($error) {
		my $user   = $context->GetUser;
		my $uri = new Mioga2::URI($config, uri=>$data->{resource});
		my $right = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_WRITE);
		if (defined $right and $right) {
			my $res = Mioga2::Magellan::DAV::UnLockResource(
				$context->GetUserAuth,
				"$base$args->{resource}"
			);
			if (!$res) {
				$data->{success} = 'true';
			}
			else {
				$data->{success} = 'false';
				push (@{$data->{errors}}, {uri => $args->{resource}, code => $res});
			}
		}
		else {
			throw Mioga2::Exception::Application ("Mioga2::Magellan::UnlockResource", __"Insufficiant rights to access this method for URI: " . $data->{resource});
		}
    }
	$data->{error} = $error if $error;
    return $data;
}

# ============================================================================

=head2 GetResource ()

  get content of a text resource

=cut

# ============================================================================

sub GetResource {
    my ( $self, $context ) = @_;
	print STDERR "Magellan::GetResource()\n" if ($debug);

    my $config = $context->GetConfig;
    my $base   = $config->GetMiogaConf ()->GetDAVProtocol () . "://" . $config->GetMiogaConf ()->GetDAVHost () . ":" . $config->GetMiogaConf ()->GetDAVPort ();
    my $data   = $self->{data};
    my $error  = undef;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['resource'], 'disallow_empty', ['location', $config] ],
			];
	my ($args, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::GetResource", __("Wrong argument value."))
	}

    if ( $args->{resource} =~ /^\s*$/ ) {
        $error = __ "'resource' argument is missing!";
    }

    $data->{success}  = 'false';
    $data->{resource} = $args->{resource};

    unless ($error) {
		my $user   = $context->GetUser;
		my $uri = new Mioga2::URI($config, uri=>$data->{resource});
		my $right = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_READ);
		if (defined $right and $right) {
			my ( $mime, $content ) = Mioga2::Magellan::DAV::GetResource(
				$context->GetUserAuth,
				$base . $args->{resource}
			);
			print STDERR "mime = $mime\n content= $content\n" if ($debug);
			if ($mime) {
				#my $conv = Text::Iconv->new( 'utf8', 'utf8' );
				#my $encoding = $conv->convert($content);
				#if ($encoding) {
				if ($mime =~ /(application\/xml)|(text\/)|(application\/xsl)/)
				{
					utf8::decode($content) if (length($content) > 0);
					$data->{data} = { content => $content };
					$data->{success} = 'true';
				}
				else {
					$data->{success} = 'false';
					$error = __ "This file can't be edited.";
					utf8::decode($error);
				}
			}
		}
		else {
			throw Mioga2::Exception::Application ("Mioga2::Magellan::GetResource", __"Insufficiant rights to access this method for URI: " . $data->{resource});
		}
    }
	$data->{error} = $error if $error;
    return $data;
}

# ============================================================================

=head2 EditResource ()

  edit a resource

=cut

# ============================================================================

sub EditResource {
    my ( $self, $context ) = @_;

    my $config = $context->GetConfig;
    my $base   = $config->GetMiogaConf ()->GetDAVProtocol () . "://" . $config->GetMiogaConf ()->GetDAVHost () . ":" . $config->GetMiogaConf ()->GetDAVPort ();
    my $data   = $self->{data};
    my $error  = undef;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['resource'], 'disallow_empty', ['location', $config] ],
				[ ['content'], 'allow_empty' ],
			];
	my ($args, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::EditResource", __("Wrong argument value."))
	}

    if ( $args->{resource} =~ /^\s*$/ ) {
        $error = __ "'resource' argument is missing!";
    }
    elsif ( $args->{content} =~ /^\s*$/ ) {
        $error = __ "'content' argument is missing!";
    }

    $data->{success}  = 'false';
    $data->{resource} = $args->{resource};

    unless ($error) {
		my $user   = $context->GetUser;
		my $uri = new Mioga2::URI($config, uri=>$data->{resource});
		my $right = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_WRITE);
		if (defined $right and $right) {
			my $lock_header = Mioga2::Magellan::DAV::GetLockHeader(
				$context,
				$context->GetUserAuth,
				$base . $args->{resource}
			);
			my $result = Mioga2::Magellan::DAV::EditURI(
				$context->GetUserAuth,
				$args->{content},            $base . $args->{resource},
				"unknown/unknown",           $lock_header
			);

			if ($result) {
				push (@{$data->{errors}}, {uri => $data->{resource}, code => $result});
			}
			else {
				$data->{success} = 'true';
			}
		}
		else {
			throw Mioga2::Exception::Application ("Mioga2::Magellan::EditResource", __"Insufficiant rights to access this method for URI: " . $data->{resource});
		}
    }
	$data->{error} = $error if $error;
    return $data;
}

# ============================================================================

=head2 PasteResources ()

  copy or move resources

=cut

# ============================================================================

sub PasteResources {
    my ( $self, $context ) = @_;

    my $config = $context->GetConfig;
    my $error = undef;
    my $data  = $self->{data};

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['destination', 'entries'], 'disallow_empty', ['location', $config] ],
				[ ['clipboard'], 'allow_empty', ['match', "\(true|false\)\?"] ],
				[ ['mode'], 'disallow_empty', ['match', "\(copy|cut\)"] ],
			];
	my ($args, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::DeleteResources", __("Wrong argument value.") . Dumper \@$errors);
	}

    if ( !$args->{entries} ) {
        $error = __ "'entries' argument is missing!";
    }
    elsif ( $args->{mode} =~ /^\s*$/ ) {
        $error = __ "'mode' argument is missing!";
    }
    elsif ( $args->{destination} =~ /^\s*$/ ) {
        $error = __ "'destination' argument is missing!";
    }

    $data->{success}     = 'false';
    $data->{destination} = $args->{destination};

	my $delete_errors = [];
    unless ($error) {
		my $user   = $context->GetUser;
		my $uri = new Mioga2::URI($config, uri=>$data->{destination});
		my $right = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_WRITE);
		if (defined $right and $right) {
			my $config        = $context->GetConfig;
			my $base          = $config->GetMiogaConf ()->GetDAVProtocol () . "://" . $config->GetMiogaConf ()->GetDAVHost () . ":" . $config->GetMiogaConf ()->GetDAVPort ();
			my $method        = ( $args->{mode} eq 'copy' ) ? 'CopyURI' : 'MoveURI';
			$args->{entries} = [ $args->{entries} ]
			if ( ref( $args->{entries} ) ne 'ARRAY' );

			foreach my $entry ( @{ $args->{entries} } ) {
				my $uri = new Mioga2::URI($config, uri=>$entry);
				my $right = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_READ);
				if (defined $right and $right) {
					my $res;
					my @parts = split /\//, $entry;
					eval(
							'$res = Mioga2::Magellan::DAV::'
						. $method
						. '($context->GetUserAuth,
												"T",
												"$base$entry",
												"$base$args->{destination}/$parts[-1]")'
					);
					if ($res) {
						push (@$delete_errors, {uri => $entry, code => $res});
					}
				}
				else {
					throw Mioga2::Exception::Application ("Mioga2::Magellan::PasteResources", __"Insufficiant rights to access this method for URI: " . $entry);
				}
			}

			if (!@$delete_errors) {
				$data->{success} = 'true';
			}
		}
		else {
			throw Mioga2::Exception::Application ("Mioga2::Magellan::PasteResources", __"Insufficiant rights to access this method for URI: " . $data->{destination});
		}
    }
	$data->{errors} = $delete_errors if (scalar (@$delete_errors));
    return $data;
}

# ============================================================================

=head2 CreateResource ()

creates empty file or folder

=cut

# ============================================================================

sub CreateResource {
    my ( $self, $context ) = @_;

    my $config = $context->GetConfig;
    my $base   = $config->GetMiogaConf ()->GetDAVProtocol () . "://" . $config->GetMiogaConf ()->GetDAVHost () . ":" . $config->GetMiogaConf ()->GetDAVPort ();
    my $data   = $self->{data};
    my $error  = undef;
    my $res;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['name'], 'disallow_empty', 'filename' ],
				[ ['path'], 'disallow_empty', ['location', $config] ],
				[ ['type'], 'disallow_empty' ],
			];
	my ($args, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		$error = __('Can not create an element with that name!');
	}

    $data->{success} = 'false';
    if ( $args->{name} =~ /^\s*$/ ) {
        $error = __ "'name' argument is missing!";
    }
    elsif ( $args->{path} =~ /^\s*$/ ) {
        $error = __ "'path' argument is missing!";
    }
    elsif ( $args->{type} =~ /^\s*$/ ) {
        $error = __ "'type' argument is missing!";
    }

    unless ($error) {
		my $user   = $context->GetUser;
		my $uri = new Mioga2::URI($config, uri=>$args->{path});
		my $right = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_WRITE);
		if (defined $right and $right) {
			if ( $args->{type} eq 'folder' ) {
				$res =
				Mioga2::Magellan->CreateDir( $context, "$base$args->{path}",
					$args->{name} );
			}
			elsif ( $args->{type} eq 'file' ) {
				$res =
				Mioga2::Magellan->CreateFile( $context, "$base$args->{path}",
					$args->{name} );
			}

			if ( $res eq 'OK' ) {
				$data->{success} = 'true';
			}
			else {
				push (@{$data->{errors}}, {uri => "$args->{path}/$args->{name}", code => $res})
			}
		}
		else {
			throw Mioga2::Exception::Application ("Mioga2::Magellan::CreateResource", __"Insufficiant rights to access this method for URI: " . $data->{path});
		}
    }

	$data->{error} = $error if $error;
    return $data;
}

# ============================================================================

=head2 UploadFile ()

  upload file to a directory

=cut

# ============================================================================

sub UploadFile {
    my ( $self, $context ) = @_;
	
	print STDERR "Magellan::UploadFile\n" if ($debug);

    $self->{mimetype} = "text/html; charset=UTF-8";    # needed for ExtJS upload
    my $config = $context->GetConfig;
    my $base   = $config->GetMiogaConf ()->GetDAVProtocol () . "://" . $config->GetMiogaConf ()->GetDAVHost () . ":" . $config->GetMiogaConf ()->GetDAVPort ();
    my $data   = $self->{data};
    my $args   = $context->{args};
    my $error  = undef;

    $data->{success} = 'false';

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	[ [ 'destination'], ['location', $config], 'disallow_empty'],
					[ [ 'file'], 'filename', 'disallow_empty'],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if ( !$args->{file} ) {
        $error = __ "'file' argument is missing!";
    }
    elsif ( $args->{destination} =~ /^\s*$/ ) {
        $error = __ "'destination' argument is missing!";
    }

    unless (@$errors) {
		my $user   = $context->GetUser;
		my $uri = new Mioga2::URI($config, uri=>$args->{destination});
		my $right = AuthzTestAccessForURI ($config, $uri, $user, undef, AUTHZ_WRITE);
		if (defined $right and $right) {
			my $filename = $args->{file};
			$filename =~ s/.*?([^\/\\]+)$/$1/;
			my $destination = $args->{destination};
	print STDERR "destination = $destination\n" if ($debug);

			my $downloaddata =
			Mioga2::Magellan->DownloadFile( $context, $base . $destination, $filename );
			if (!$downloaddata->{code}) {
				$data->{success}   = 'true';
				$data->{path}      = $destination;
				$data->{filename}  = $filename;
				$data->{disk_path} = $downloaddata->{archive_path};
			}
			else {
				$data->{success} = 'false';
				push (@{$data->{errors}}, {uri => $destination, code => $downloaddata->{code}});
			}
		}
		else {
			throw Mioga2::Exception::Application ("Mioga2::Magellan::UploadFile", __"Insufficiant rights to access this method for URI: " . $data->{destination});
		}
    }

	$data->{error} = $error if $error;
    return $data;
}

# ============================================================================

=head2 DownloadFiles ()

  download a set of files and directories as an archive

=cut

# ============================================================================

sub DownloadFiles {
    my ( $self, $context ) = @_;

    my $config = $context->GetConfig;
    my $base   = $config->GetMiogaConf ()->GetDAVProtocol () . "://" . $config->GetMiogaConf ()->GetDAVHost () . ":" . $config->GetMiogaConf ()->GetDAVPort ();
	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['path', 'entries'], 'disallow_empty', ['location', $config]],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::DownloadFiles", __("Wrong argument value."))
	}

    my $args   = $context->{args};
    my $error;

    if ( !$args->{entries} ) {
        $error = __ "'entries' argument is missing!";
    }
    elsif ( $args->{path} =~ /^\s*$/ ) {
        $error = __ "'path' argument is missing!";
    }

    $args->{entries} = [ $args->{entries} ]
      if ( ref( $args->{entries} ) ne 'ARRAY' );

    my $tmp_dir    = $config->GetTmpDir();
    my $webdav_dir = $config->GetWebdavDir();
    my $base_uri   = $config->GetBaseURI();
    my $path = $args->{path};
    map { s/^\Q$path\E\///; } @{ $args->{entries} };
    $path =~ s/^$base_uri/$webdav_dir/;

    my $archname;
    if ( @{ $args->{entries} } == 1 && !-d $path . '/' . $args->{entries}->[0] )
    {
        $archname = $path . '/' . $args->{entries}->[0];
    }
    else {
        $path =~ /\/([^\/]+)\/?$/;
        $archname = (
            @{ $args->{entries} } == 1
            ? basename( $args->{entries}->[0] )
            : $1
        ) . '.zip';

		my $zip = Archive::Zip->new ();
		$args->{path} =~ s/\/$//;
		foreach (@{ $args->{entries} }) {
			my $user   = $context->GetUser;
			# List all URIs the user can access below the specified URI
			my @uris = AuthzListAccessibleURIs ($config, $context->GetUser (), ("$args->{path}/$_"));
			if (scalar (@uris)) {
				for (@uris) {
					# Remove base path as we are working in the correct directory
					$_ =~ s!^\Q$args->{path}\E/?!!;
					if (-f ("$path/$_")) {
						my $zipmember = $zip->addFile ("$path/$_", $_);
					}
					elsif (-d "$path/$_") {
						my $zipmember = $zip->addDirectory ("$path/$_", $_);
					}
					else {
						print STDERR "[Mioga2::Magellan::DownloadFiles] Unhandled type for path $path/$_\n";
					}
				}
			}
			else {
				my $msg = __"Insufficiant rights to access this method for URI: ";
				throw Mioga2::Exception::Application ("Mioga2::Magellan::DownloadFiles", $msg . "$args->{path}/$_");
			}
		}
        $archname = "$tmp_dir/$archname";
		$zip->writeToFileNamed ($archname);
    }
    my $filename = $archname;
    $filename =~ s/.*\/(.*)$/$1/;
    my $content = Mioga2::Content::SendFile->new( $context, path => $archname );
    return $content;
}

# ============================================================================
# MonitorDecompress ()
#
#    Monitor decompression phase. Polling is intended to be done with AJAX call
#
# ============================================================================
sub MonitorDecompress {
    my ( $self, $context ) = @_;

    my $session = $context->GetSession;
    my $data    = $self->{data};

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['_dc'], 'allow_empty', 'want_int'],	# This (a timestamp) is automatically added by extjs and doesn't seem to be used by Mioga
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::MonitorDecompress", __("Wrong argument value."))
	}

    if ( $session->{fm_files} ) {
        my $total_files = scalar @{ $session->{fm_files} };
        $data->{progress_infos} = {
            current_file     => $session->{fm_current_file} + 1,
            current_filename => $session->{fm_current_filename},
            total_files      => $total_files,
            current_size     => $session->{fm_current_size},
            total_size       => $session->{fm_total_size},
            percent_size     => int(
                $session->{fm_current_size} * 100 / $session->{fm_total_size}
            ),
            percent_files =>
              int( ( $session->{fm_current_file} + 1 ) * 100 / $total_files ),
            error => $session->{fm_error} ? 'true' : 'false',
        };
    }

    return $data;
}

# ============================================================================
# Extract ()
#
#    Extract files from an archive. Intended for AJAX calls and to work with
#    MonitorDecompress().
#
# ============================================================================
sub Extract {
    my ( $self, $context ) = @_;
    my $config = $context->GetConfig;
    my ( $arch_dir, $arch_path, $error );
    my $data    = $self->{data};
    my $session = $context->GetSession;

    delete $session->{fm_files};
    delete $session->{fm_total_size};
    delete $session->{fm_current_size};
    delete $session->{fm_current_file};
    delete $session->{fm_current_filename};
    delete $session->{fm_error};
    $context->{raw_session}->save;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	[ [ 'arch_dir'], ['location', $config], 'disallow_empty'],
					[ [ 'arch_path'], ['path', $config], 'disallow_empty'],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

    $arch_dir  = $context->{args}->{arch_dir};
    $arch_path = $context->{args}->{arch_path};

    $arch_dir =~ s/&amp;/&/g;
    $arch_path =~ s/&amp;/&/g;

    $data->{success} = 'false';

	my @errors;
    if ( defined($arch_dir) && defined($arch_path) ) {
        my $arch = new Mioga2::Magellan::Archive();
        try {
            @errors = $arch->Extract( $config, $arch_path, $arch_dir, 1, $context );
        }
        otherwise {
            $error = shift;
            warn Dumper($error);
        };
    }

    $data->{infos} = { arch_dir => $arch_dir };

    if (@errors) {
        $data->{error} = __("Your permissions don't allow you to extract all the contents of the archive, some files could not be extracted.");
        $data->{success} = 'false';
		$data->{files} = \@errors;
        $context->GetSession->{fm_error} = 1;
        $context->{raw_session}->save;
    }
    else {
        $data->{success} = 'true';
    }

    return $data;
}

# ============================================================================
# GetHistory ()
#
#    get files history for a given uri
#
# ============================================================================
sub GetHistory {
    my ( $self, $context ) = @_;
    my $config = $context->GetConfig;
    my $dbh    = $config->GetDBH;
    my $user   = $context->GetUser;
    my $tz     = $user->GetTimezone;
    my $data   = $self->{data};

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['resource'], 'disallow_empty', ['location', $config] ],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::GetHistory", __("Wrong argument value."))
	}

    my $uri    = $values->{resource};

	$data->{history} = [];
	my $uriobj = new Mioga2::URI($config, uri=>$uri);
	my $right = AuthzTestAccessForURI ($config, $uriobj, $user, undef, AUTHZ_READ);
	if (defined $right and $right) {
		my $can_manage =
		$self->CheckUserAccessOnFunction( $context, $user, $context->GetGroup,
			"History" ) ? 'true' : 'false';

		my $results = SelectMultiple(
			$dbh,
	"SELECT uri_history.rowid, uri_history.modified, m_user_base.lastname, m_user_base.firstname, m_user_base.ident, uri_history.old_name 
										FROM uri_history, m_uri, m_user_base
										WHERE m_uri.uri='"
			. st_FormatPostgreSQLString($uri) . "'
										AND uri_history.uri_id = m_uri.rowid
										AND m_user_base.rowid = uri_history.user_id
										ORDER BY modified DESC"
		);

		foreach my $res (@$results) {
			my $modified = Mioga2::Classes::Time->FromPSQL( $res->{modified} );

			push @{ $data->{history} },
			{
				datetime => $modified->strftimetz( "%F %T", $tz ),
				rowid    => $res->{rowid},
				old_name => $res->{old_name},
				type => $res->{old_name} ? 'rename' : 'edit',
				username   => "$res->{firstname} $res->{lastname}",
				ident      => $res->{ident},
				can_manage => $can_manage,
			};
		}
	}
	else {
		throw Mioga2::Exception::Application ("Mioga2::Magellan::GetHistory", __"Insufficiant rights to access this method for URI: " . $uri);
	}

    return $data;
}

# ============================================================================
# DeleteRevision ()
#
#    Delete a revision in history
#
# ============================================================================
sub DeleteRevision {
    my ( $self, $context ) = @_;

    my $config = $context->GetConfig;
    my $dbh    = $context->GetConfig->GetDBH;
    my $data   = $self->{data};

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['resource'], 'disallow_empty', ['location', $config] ],
				[ ['revision'], 'disallow_empty', 'want_int' ],
			];
	my ($args, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::DeleteRevision", __("Wrong argument value."))
	}

	my $uri    = $args->{resource};
    my $rev    = $args->{revision};

    $data->{success}  = 'false';
    $data->{resource} = $uri;
    $data->{revision} = $rev;

	my $user   = $context->GetUser;
	my $uriobj = new Mioga2::URI($config, uri=>$uri);
	my $right = AuthzTestAccessForURI ($config, $uriobj, $user, undef, AUTHZ_WRITE);
	if (defined $right and $right) {
		if ( $uri && $rev ) {
			my $base_dir = $config->GetMiogaFilesDir . "/history";
			my ( $name, @segments ) = Mioga2::Classes::History::GetHistoryPath(
				Mioga2::Classes::URI->new( uri => $uri ),
				$uriobj->GetGroupIdent ());
			$uri = st_FormatPostgreSQLString($uri);
			ExecSQL(
				$dbh, "DELETE FROM uri_history
					USING m_uri
					WHERE uri_history.rowid=$rev
						AND m_uri.uri='$uri'
						AND uri_history.uri_id=m_uri.rowid"
			);
			unlink( "$base_dir/" . join( "/", @segments ) . "/$name-$rev" );
			$data->{success} = 'true';
		}
	}
	else {
		throw Mioga2::Exception::Application ("Mioga2::Magellan::DeleteRevision", __"Insufficiant rights to access this method for URI: " . $uri);
	}

    return $data;
}

# ============================================================================
# DownloadRevision ()
#
#    Download a file at a specified revision
#
# ============================================================================
sub DownloadRevision {
    my ( $self, $context ) = @_;

    my $config = $context->GetConfig;
    my $dbh    = $context->GetConfig->GetDBH;
    my $content;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['resource'], 'disallow_empty', ['location', $config] ],
				[ ['revision'], 'disallow_empty', 'want_int' ],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::DownloadRevision", __("Wrong argument value."))
	}

    my $uri    = $values->{resource};
    my $rev    = $values->{revision};

	my $user   = $context->GetUser;
	my $uriobj = new Mioga2::URI($config, uri=>$uri);
	my $right = AuthzTestAccessForURI ($config, $uriobj, $user, undef, AUTHZ_READ);
	if (defined $right and $right) {
		if ( $uri && $rev ) {
			my $base_dir = $config->GetMiogaFilesDir . "/history";
			my ( $name, @segments ) = Mioga2::Classes::History::GetHistoryPath(
				Mioga2::Classes::URI->new( uri => $uri ),
				$uriobj->GetGroupIdent ());
			$content = new Mioga2::Content::FILE( $context, filename => $name );
			$content->SetContent(
				"$base_dir/" . join( "/", @segments ) . "/$name-$rev" );
		}
	}
	else {
		throw Mioga2::Exception::Application ("Mioga2::Magellan::DownloadRevision", __"Insufficiant rights to access this method for URI: " . $uri);
	}

    return $content;
}

# ============================================================================

=head2 LastModified ()

  Show last modified files in current instance.

=cut

# ============================================================================

sub LastModified {
    my ( $self, $context ) = @_;

    my $config   = $context->GetConfig;
    my $dbh      = $config->GetDBH;
    my $user     = $context->GetUser;
    my $group    = $context->GetGroup;
    my $limit    = $context->{args}->{limit};
    my $instance = $config->GetMiogaId;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['limit'], 'allow_empty', 'want_int' ],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::LastModified", __("Wrong argument value."))
	}

    $limit = 10 if ( $limit !~ /^\d+$/ || ref($limit) eq "HASH" );

    my $offset      = 0;
    my $offset_step = 50;

    my @rowids;
    my $total = SelectSingle(
        $dbh, "SELECT COUNT(m_uri.rowid) FROM m_uri, m_group_base WHERE 
            m_uri.group_id = m_group_base.rowid AND 
            m_group_base.mioga_id = $instance AND m_uri.mimetype <> 'directory'"
    );
    $total = $total->{count};
    while ( $total > 0 && @rowids < $limit ) {
        push @rowids, AuthzTestAccessForRequest(
            $config, $user, "SELECT m_uri.rowid FROM m_uri, m_group_base WHERE 
            m_uri.group_id = m_group_base.rowid AND 
            m_group_base.mioga_id = $instance AND m_uri.mimetype <> 'directory' ORDER BY m_uri.modified DESC LIMIT $offset_step OFFSET $offset"
        );
        $offset += $offset_step;
        $total -= $offset_step;
    }

    my $ids = "(" . join( ',', @rowids ) . ")";
    my $res = SelectMultiple(
        $dbh, "SELECT m_uri.*, m_user_base.firstname, m_user_base.lastname 
                FROM m_uri, m_user_base 
                WHERE m_uri.rowid IN $ids AND m_uri.user_id = m_user_base.rowid
                ORDER BY modified DESC"
    );
    splice @$res, $limit;

    my $data = $self->{data};
    $data->{file} = [];

    foreach my $result (@$res) {
        my $uri = Mioga2::URI->new( $config, uri => $result->{uri} );
        next if ( -d $uri->GetRealPath );

        my $time = Mioga2::Classes::Time->FromPSQL( $result->{modified} );
        my @parts = split "/", $uri->GetURI;
        push @{ $data->{file} },
          {
            uri => {
                cn   => $parts[-1],
                '$t' => "" . $uri->GetURI . "",
            },
            modified => $time->strftimetz( "%F %T", $user->GetTimezone ),
            user     => {
                rowid => $result->{rowid},
                '$t'  => $result->{firstname} . " " . $result->{lastname},
            },
            mimetype => $result->{mimetype},
          };
    }

    return $data;
}

# ============================================================================

=head2 MimeList ()

  mimetypes list

=cut

# ============================================================================

sub MimeList {
    my ( $self, $context ) = @_;

    my $data = $self->{data};

    $data->{mimetypes} = {
        directory => {
            image       => "folder",
            description => __("Directory"),
        },
        file => {
            image       => "unknown",
            description => __("File"),
        },
        #'application/andrew-inset' => {
        #    image       => 'file',
        #    description => __("ATK inset"),
        #},
        #'application/illustrator' => {
        #    image       => 'file',
        #    description => __("Adobe Illustrator document"),
        #},

        #'application/mac-binhex40' => {
        #    image       => 'file',
        #    description => __("Macintosh BinHex-encoded file"),
        #},

        #'application/mathematica' => {
        #    image       => 'file',
        #    description => __("Mathematica Notebook"),
        #},

        #'application/mbox' => {
        #    image       => 'file',
        #    description => __("Mailbox file"),
        #},

        #'application/octet-stream' => {
        #    image       => 'binary',
        #    description => __("unknown"),
        #},

        #'application/oda' => {
        #    image       => 'file',
        #    description => __("ODA document"),
        #},

        'application/pdf' => {
            image       => 'application-pdf',
            description => __("PDF document"),
        },

        #'application/xspf+xml' => {
        #    image       => 'file',
        #    description => __("XSPF playlist"),
        #},

        #'application/pgp-encrypted' => {
        #    image       => 'encrypted',
        #    description => __("PGP/MIME-encrypted message header"),
        #},

        #'application/pgp-keys' => {
        #    image       => 'file',
        #    description => __("PGP keys"),
        #},

        #'application/pgp-signature' => {
        #    image       => 'file',
        #    description => __("detached OpenPGP signature"),
        #},

        'application/pkcs7-mime' => {
            image       => 'application-pkcs7-mime',
            description => __("S/MIME file"),
        },

        #'application/pkcs7-signature' => {
        #    image       => 'application-pkcs7-signature',
        #    description => __("detached S/MIME signature"),
        #},

        'application/postscript' => {
            image       => 'application-postscript',
            description => __("PS document"),
        },

        'application/rtf' => {
            image       => 'application-rtf',
            description => __("RTF document"),
        },

        #'application/sieve' => {
        #    image       => 'file',
        #    description => __("Sieve mail filter script"),
        #},

        #'application/smil' => {
        #    image       => 'file',
        #    description => __("SMIL document"),
        #},

        #'application/x-sqlite2' => {
        #    image       => 'file',
        #    description => __("SQLite2 database"),
        #},

        #'application/x-sqlite3' => {
        #    image       => 'file',
        #    description => __("SQLite3 database"),
        #},

        #'application/stuffit' => {
        #    image       => 'file',
        #    description => __("StuffIt archive"),
        #},

        #'application/x-gedcom' => {
        #    image       => 'file',
        #    description => __("GEDCOM family history"),
        #},

        #'application/x-flash-video' => {
        #    image       => 'file',
        #    description => __("Flash video"),
        #},

        #'application/x-go-sgf' => {
        #    image       => 'file',
        #    description => __("SGF record"),
        #},

        #'application/vnd.corel-draw' => {
        #    image       => 'file',
        #    description => __("Corel Draw drawing"),
        #},

        #'application/vnd.hp-hpgl' => {
        #    image       => 'file',
        #    description => __("HPGL file"),
        #},

        #'application/vnd.hp-pcl' => {
        #    image       => 'file',
        #    description => __("PCL file"),
        #},

        #'application/vnd.lotus-1-2-3' => {
        #    image       => 'file',
        #    description => __("Lotus 1-2-3 spreadsheet"),
        #},

        #'application/vnd.mozilla.xul+xml' => {
        #    image       => 'file',
        #    description => __("XUL document"),
        #},

        'application/vnd.ms-access' => {
            image       => 'application-vnd.ms-access',
            description => __("JET database"),
        },

        'application/vnd.ms-excel' => {
            image       => 'application-vnd.ms-excel',
            description => __("Excel spreadsheet"),
        },

        'application/vnd.ms-powerpoint' => {
            image       => 'application-vnd.ms-powerpoint',
            description => __("PowerPoint presentation"),
        },

        'application/msword' => {
            image       => 'application-msword',
            description => __("Word document"),
        },

        #'application/vnd.palm' => {
        #    image       => 'file',
        #    description => __("Palmpilot database/document"),
        #},

        #'application/vnd.stardivision.calc' => {
        #    image       => 'file',
        #    description => __("StarCalc spreadsheet"),
        #},

        #'application/vnd.stardivision.chart' => {
        #    image       => 'file',
        #    description => __("StarChart chart"),
        #},

        #'application/vnd.stardivision.draw' => {
        #    image       => 'file',
        #    description => __("StarDraw drawing"),
        #},

        #'application/vnd.stardivision.impress' => {
        #    image       => 'file',
        #    description => __("StarImpress presentation"),
        #},

        #'application/vnd.stardivision.mail' => {
        #    image       => 'file',
        #    description => __("StarMail email"),
        #},

        #'application/vnd.stardivision.math' => {
        #    image       => 'file',
        #    description => __("StarMath formula"),
        #},

        #'application/vnd.stardivision.writer' => {
        #    image       => 'file',
        #    description => __("StarWriter document"),
        #},

        'application/vnd.sun.xml.calc' => {
            image       => 'application-vnd.oasis.opendocument.spreadsheet',
            description => __("OpenOffice Calc"),
        },

        'application/vnd.sun.xml.calc.template' => {
            image       => 'application-vnd.oasis.opendocument.spreadsheet-template',
            description => __("OpenOffice Calc template"),
        },

        'application/vnd.sun.xml.draw' => {
            image       => 'application-vnd.oasis.opendocument.graphics',
            description => __("OpenOffice Draw"),
        },

        #'application/vnd.sun.xml.draw.template' => {
        #    image       => 'file',
        #    description => __("OpenOffice Draw template"),
        #},

        'application/vnd.sun.xml.impress' => {
            image       => 'application-vnd.oasis.opendocument.presentation',
            description => __("OpenOffice Impress"),
        },

        'application/vnd.sun.xml.impress.template' => {
            image       => 'application-vnd.oasis.opendocument.presentation-template',
            description => __("OpenOffice Impress template"),
        },

        #'application/vnd.sun.xml.math' => {
        #    image       => 'file',
        #    description => __("OpenOffice Math"),
        #},

        'application/vnd.sun.xml.writer' => {
            image       => 'application-vnd.oasis.opendocument.text',
            description => __("OpenOffice Writer"),
        },

        #'application/vnd.sun.xml.writer.global' => {
        #    image       => 'file',
        #    description => __("OpenOffice Writer global"),
        #},

        #'application/vnd.sun.xml.writer.template' => {
        #    image       => 'file',
        #    description => __("OpenOffice Writer template"),
        #},

        'application/vnd.oasis.opendocument.text' => {
            image       => 'application-vnd.oasis.opendocument.text',
            description => __("ODT document"),
        },

        #'application/vnd.oasis.opendocument.text-template' => {
        #    image       => 'document',
        #    description => __("ODT template"),
        #},

        #'application/vnd.oasis.opendocument.text-web' => {
        #    image       => 'document',
        #    description => __("OTH template"),
        #},

        #'application/vnd.oasis.opendocument.text-master' => {
        #    image       => 'document',
        #    description => __("ODM document"),
        #},

        'application/vnd.oasis.opendocument.graphics' => {
            image       => 'application-vnd.oasis.opendocument.graphics',
            description => __("ODG drawing"),
        },

        #'application/vnd.oasis.opendocument.graphics-template' => {
        #    image       => 'file',
        #    description => __("ODG template"),
        #},

        'application/vnd.oasis.opendocument.presentation' => {
            image       => 'application-vnd.oasis.opendocument.presentation',
            description => __("ODP presentation"),
        },

        'application/vnd.oasis.opendocument.presentation-template' => {
            image       => 'application-vnd.oasis.opendocument.presentation-template',
            description => __("ODP template"),
        },

        'application/vnd.oasis.opendocument.spreadsheet' => {
            image       => 'application-vnd.oasis.opendocument.spreadsheet',
            description => __("ODS spreadsheet"),
        },

        'application/vnd.oasis.opendocument.spreadsheet-template' => {
            image       => 'application-vnd.oasis.opendocument.spreadsheet-template',
            description => __("ODS template"),
        },

        'application/vnd.oasis.opendocument.chart' => {
            image       => 'application-vnd.oasis.opendocument.chart',
            description => __("ODC chart"),
        },

        'application/vnd.oasis.opendocument.formula' => {
            image       => 'application-vnd.oasis.opendocument.formula',
            description => __("ODF formula"),
        },

        'application/vnd.oasis.opendocument.database' => {
            image       => 'application-vnd.oasis.opendocument.database',
            description => __("ODB database"),
        },

        'application/vnd.oasis.opendocument.image' => {
            image       => 'application-vnd.oasis.opendocument.image',
            description => __("ODI image"),
        },

        #'application/vnd.wordperfect' => {
        #    image       => 'file',
        #    description => __("WordPerfect document"),
        #},

        #'application/x-xbel' => {
        #    image       => 'file',
        #    description => __("XBEL bookmarks"),
        #},

        'application/x-7z-compressed' => {
            image       => 'application-x-7z-compressed',
            description => __("7-zip archive"),
        },

        #'application/x-abiword' => {
        #    image       => 'file',
        #    description => __("AbiWord document"),
        #},

        #'application/x-cue' => {
        #    image       => 'file',
        #    description => __("CD image cuesheet"),
        #},

        #'application/x-amipro' => {
        #    image       => 'file',
        #    description => __("Lotus AmiPro document"),
        #},

        #'application/x-applix-spreadsheet' => {
        #    image       => 'file',
        #    description => __("Applix Spreadsheets spreadsheet"),
        #},

        #'application/x-applix-word' => {
        #    image       => 'file',
        #    description => __("Applix Words document"),
        #},

        #'application/x-arc' => {
        #    image       => 'file',
        #    description => "",
        #},

        #'application/x-archive' => {
        #    image       => 'file',
        #    description => __("AR archive"),
        #},

        #'application/x-arj' => {
        #    image       => 'zip',
        #    description => __("ARJ archive"),
        #},

        #'application/x-asp' => {
        #    image       => 'file',
        #    description => __("ASP page"),
        #},

        #'application/x-awk' => {
        #    image       => 'file',
        #    description => __("AWK script"),
        #},

        #'application/x-bcpio' => {
        #    image       => 'file',
        #    description => __("BCPIO document"),
        #},

        #'application/x-bittorrent' => {
        #    image       => 'file',
        #    description => __("BitTorrent seed file"),
        #},

        #'application/x-blender' => {
        #    image       => 'file',
        #    description => __("Blender scene"),
        #},

        'application/x-bzip' => {
            image       => 'application-x-bzip.png',
            description => __("bzip archive"),
        },

        'application/x-bzip-compressed-tar' => {
            image       => 'application-x-bzip-compressed-tar.png',
            description => __("tar archive (bzip-compressed)"),
        },

        #'application/x-cbr' => {
        #    image       => 'file',
        #    description => __("comic book archive"),
        #},

        #'application/x-cbz' => {
        #    image       => 'file',
        #    description => __("comic book archive"),
        #},

        'application/x-cd-image' => {
            image       => 'application-x-cd-image',
            description => __("raw CD image"),
        },

        #'application/x-cgi' => {
        #    image       => 'file',
        #    description => __("CGI script"),
        #},

        #'application/x-chess-pgn' => {
        #    image       => 'file',
        #    description => __("PGN chess game"),
        #},

        #'application/x-chm' => {
        #    image       => 'file',
        #    description => __("CHM document"),
        #},

        #'application/x-class-file' => {
        #    image       => 'file',
        #    description => __("Java byte code"),
        #},

        'application/x-compress' => {
            image       => 'application-x-compress',
            description => __("UNIX-compressed file"),
        },

        'application/x-compressed-tar' => {
            image       => 'application-x-compressed-tar',
            description => __("tar archive (gzip-compressed)"),
        },

        #'application/x-core' => {
        #    image       => 'file',
        #    description => __("program crash data"),
        #},

        #'application/x-cpio' => {
        #    image       => 'file',
        #    description => __("CPIO archive"),
        #},

        #'application/x-cpio-compressed' => {
        #    image       => 'file',
        #    description => __("CPIO archive (gzip-compressed)"),
        #},

        #'application/x-csh' => {
        #    image       => 'file',
        #    description => __("C shell script"),
        #},

        #'application/x-dbf' => {
        #    image       => 'file',
        #    description => __("Xbase document"),
        #},

        #'application/ecmascript' => {
        #    image       => 'file',
        #    description => __("ECMAScript program"),
        #},

        #'application/x-dbm' => {
        #    image       => 'file',
        #    description => "",
        #},

        #'application/x-dc-rom' => {
        #    image       => 'file',
        #    description => __("Dreamcast ROM"),
        #},

        #'application/x-deb' => {
        #    image       => 'deb',
        #    description => __("Debian package"),
        #},

        #'application/x-designer' => {
        #    image       => 'file',
        #    description => __("Qt Designer file"),
        #},

        #'application/x-desktop' => {
        #    image       => 'file',
        #    description => __("desktop configuration file"),
        #},

        #'application/x-dia-diagram' => {
        #    image       => 'file',
        #    description => __("Dia diagram"),
        #},

        #'application/x-dvi' => {
        #    image       => 'dvi',
        #    description => __("TeX DVI document"),
        #},

        #'application/x-e-theme' => {
        #    image       => 'file',
        #    description => __("Enlightenment theme"),
        #},

        #'application/x-egon' => {
        #    image       => 'file',
        #    description => __("Egon Animator animation"),
        #},

        #'application/x-executable' => {
        #    image       => 'binary',
        #    description => __("executable"),
        #},

        #'application/x-font-type1' => {
        #    image       => 'font_type1',
        #    description => __("font"),
        #},

        #'application/x-font-afm' => {
        #    image       => 'file',
        #    description => __("Adobe font metrics"),
        #},

        #'application/x-font-bdf' => {
        #    image       => 'file',
        #    description => __("BDF font"),
        #},

        #'application/x-font-dos' => {
        #    image       => 'file',
        #    description => __("DOS font"),
        #},

        #'application/x-font-framemaker' => {
        #    image       => 'file',
        #    description => __("Adobe FrameMaker font"),
        #},

        #'application/x-font-libgrx' => {
        #    image       => 'file',
        #    description => __("LIBGRX font"),
        #},

        #'application/x-font-linux-psf' => {
        #    image       => 'file',
        #    description => __("Linux PSF console font"),
        #},

        #'application/x-font-pcf' => {
        #    image       => 'file',
        #    description => __("PCF font"),
        #},

        #'application/x-font-otf' => {
        #    image       => 'file',
        #    description => __("OpenType font"),
        #},

        #'application/x-font-speedo' => {
        #    image       => 'file',
        #    description => __("Speedo font"),
        #},

        #'application/x-font-sunos-news' => {
        #    image       => 'file',
        #    description => __("SunOS News font"),
        #},

        #'application/x-font-tex' => {
        #    image       => 'file',
        #    description => __("TeX font"),
        #},

        #'application/x-font-tex-tfm' => {
        #    image       => 'file',
        #    description => __("TeX font metrics"),
        #},

        #'application/x-font-ttf' => {
        #    image       => 'font_truetype',
        #    description => __("TrueType font"),
        #},

        #'application/x-font-vfont' => {
        #    image       => 'file',
        #    description => __("V font"),
        #},

        #'application/x-frame' => {
        #    image       => 'file',
        #    description => "",
        #},

        #'application/x-gameboy-rom' => {
        #    image       => 'file',
        #    description => __("Game Boy ROM"),
        #},

        #'application/x-gba-rom' => {
        #    image       => 'file',
        #    description => __("Game Boy Advance ROM"),
        #},

        #'application/x-gdbm' => {
        #    image       => 'file',
        #    description => __("GDBM database"),
        #},

        #'application/x-genesis-rom' => {
        #    image       => 'file',
        #    description => __("Genesis ROM"),
        #},

        #'application/x-gettext-translation' => {
        #    image       => 'gettext',
        #    description => __("translated messages (machine-readable)"),
        #},

        #'application/x-glade' => {
        #    image       => 'file',
        #    description => __("Glade project"),
        #},

        #'application/x-gmc-link' => {
        #    image       => 'file',
        #    description => __("GMC link"),
        #},

        #'application/x-gnucash' => {
        #    image       => 'file',
        #    description => __("GnuCash spreadsheet"),
        #},

        #'application/x-gnumeric' => {
        #    image       => 'file',
        #    description => __("Gnumeric spreadsheet"),
        #},

        #'application/x-gnuplot' => {
        #    image       => 'file',
        #    description => __("Gnuplot document"),
        #},

        #'application/x-graphite' => {
        #    image       => 'file',
        #    description => __("Graphite scientific graph"),
        #},

        #'application/x-gtar' => {
        #    image       => 'file',
        #    description => __("gtar archive"),
        #},

        #'application/x-gtktalog' => {
        #    image       => 'file',
        #    description => __("GTKtalog catalog"),
        #},

        'application/x-gzip' => {
            image       => 'application-x-gzip',
            description => __("gzip archive"),
        },

        #'application/x-gzpostscript' => {
        #    image       => 'postscript',
        #    description => __("PostScript document (gzip-compressed)"),
        #},

        #'application/x-hdf' => {
        #    image       => 'file',
        #    description => __("HDF document"),
        #},

        #'application/x-ipod-firmware' => {
        #    image       => 'file',
        #    description => __("iPod firmware"),
        #},

        #'application/x-java-archive' => {
        #    image       => 'zip',
        #    description => __("Java archive"),
        #},

        #'application/x-java' => {
        #    image       => 'java_src',
        #    description => __("Java class"),
        #},

        #'application/x-java-jnlp-file' => {
        #    image       => 'file',
        #    description => __("JNLP file"),
        #},

        #'application/javascript' => {
        #    image       => 'source',
        #    description => __("JavaScript program"),
        #},

        #'application/x-jbuilder-project' => {
        #    image       => 'file',
        #    description => __("JBuilder project"),
        #},

        #'application/x-karbon' => {
        #    image       => 'file',
        #    description => __("Karbon14 drawing"),
        #},

        #'application/x-kchart' => {
        #    image       => 'file',
        #    description => __("KChart chart"),
        #},

        #'application/x-kformula' => {
        #    image       => 'file',
        #    description => __("KFormula formula"),
        #},

        #'application/x-killustrator' => {
        #    image       => 'file',
        #    description => __("KIllustrator drawing"),
        #},

        #'application/x-kivio' => {
        #    image       => 'file',
        #    description => __("Kivio flowchart"),
        #},

        #'application/x-kontour' => {
        #    image       => 'file',
        #    description => __("Kontour drawing"),
        #},

        #'application/x-kpovmodeler' => {
        #    image       => 'file',
        #    description => __("KPovModeler scene"),
        #},

        #'application/x-kpresenter' => {
        #    image       => 'file',
        #    description => __("KPresenter presentation"),
        #},

        #'application/x-krita' => {
        #    image       => 'file',
        #    description => __("Krita document"),
        #},

        #'application/x-kspread' => {
        #    image       => 'file',
        #    description => __("KSpread spreadsheet"),
        #},

        #'application/x-kspread-crypt' => {
        #    image       => 'file',
        #    description => __("KSpread spreadsheet (encrypted)"),
        #},

        #'application/x-ksysv-package' => {
        #    image       => 'file',
        #    description => "",
        #},

        #'application/x-kugar' => {
        #    image       => 'file',
        #    description => __("Kugar document"),
        #},

        #'application/x-kword' => {
        #    image       => 'file',
        #    description => __("KWord document"),
        #},

        #'application/x-kword-crypt' => {
        #    image       => 'file',
        #    description => __("KWord document (encrypted)"),
        #},

        #'application/x-lha' => {
        #    image       => 'zip',
        #    description => __("LHA archive"),
        #},

        #'application/x-lhz' => {
        #    image       => 'zip',
        #    description => __("LHZ archive"),
        #},

        #'application/x-linguist' => {
        #    image       => 'file',
        #    description => __("message catalog"),
        #},

        #'application/x-lyx' => {
        #    image       => 'file',
        #    description => __("LyX document"),
        #},

        #'application/x-lzop' => {
        #    image       => 'file',
        #    description => __("LZO archive"),
        #},

        #'application/x-magicpoint' => {
        #    image       => 'file',
        #    description => __("MagicPoint presentation"),
        #},

        #'application/x-macbinary' => {
        #    image       => 'file',
        #    description => __("Macintosh MacBinary file"),
        #},

        #'application/x-matroska' => {
        #    image       => 'video',
        #    description => __("Matroska video"),
        #},

        #'application/x-mif' => {
        #    image       => 'file',
        #    description => __("FrameMaker MIF document"),
        #},

        #'application/x-mozilla-bookmarks' => {
        #    image       => 'file',
        #    description => __("Mozilla bookmarks"),
        #},

        #'application/x-ms-dos-executable' => {
        #    image       => 'exec_wine',
        #    description => __("DOS/Windows executable"),
        #},

        #'application/x-mswinurl' => {
        #    image       => 'file',
        #    description => "",
        #},

        #'application/x-mswrite' => {
        #    image       => 'file',
        #    description => __("WRI document"),
        #},

        #'application/x-msx-rom' => {
        #    image       => 'file',
        #    description => __("MSX ROM"),
        #},

        #'application/x-m4' => {
        #    image       => 'file',
        #    description => __("M4 macro"),
        #},

        #'application/x-n64-rom' => {
        #    image       => 'file',
        #    description => __("Nintendo64 ROM"),
        #},

        #'application/x-nautilus-link' => {
        #    image       => 'file',
        #    description => __("Nautilus link"),
        #},

        #'application/x-nes-rom' => {
        #    image       => 'file',
        #    description => __("NES ROM"),
        #},

        #'application/x-netcdf' => {
        #    image       => 'file',
        #    description => __("Unidata NetCDF document"),
        #},

        #'application/x-netscape-bookmarks' => {
        #    image       => 'file',
        #    description => __("Netscape bookmarks"),
        #},

        #'application/x-object' => {
        #    image       => 'source_o',
        #    description => __("object code"),
        #},

        #'application/ogg' => {
        #    image       => 'sound',
        #    description => __("Ogg multimedia"),
        #},

        #'audio/x-vorbis+ogg' => {
        #    image       => 'sound',
        #    description => __("Ogg Vorbis audio"),
        #},

        #'audio/x-flac+ogg' => {
        #    image       => 'sound',
        #    description => __("Ogg FLAC audio"),
        #},

        #'audio/x-speex+ogg' => {
        #    image       => 'sound',
        #    description => __("Ogg Speex audio"),
        #},

        #'video/x-theora+ogg' => {
        #    image       => 'video',
        #    description => __("Ogg Theora video"),
        #},

        #'video/x-ogm+ogg' => {
        #    image       => 'video',
        #    description => __("OGM video"),
        #},

        #'application/x-ole-storage' => {
        #    image       => 'file',
        #    description => __("OLE2 compound document storage"),
        #},

        #'application/x-oleo' => {
        #    image       => 'file',
        #    description => __("GNU Oleo spreadsheet"),
        #},

        #'application/x-palm-database' => {
        #    image       => 'file',
        #    description => __("Palm OS database"),
        #},

        #'application/x-par2' => {
        #    image       => 'file',
        #    description => __("PAR2 Parity File"),
        #},

        #'application/x-pef-executable' => {
        #    image       => 'file',
        #    description => __("PEF executable"),
        #},

        #'application/x-perl' => {
        #    image       => 'source_pl',
        #    description => __("Perl script"),
        #},

        #'application/x-php' => {
        #    image       => 'php',
        #    description => __("PHP script"),
        #},

        #'application/x-pkcs12' => {
        #    image       => 'file',
        #    description => __("PKCS#12 certificate bundle"),
        #},

        #'application/x-planperfect' => {
        #    image       => 'file',
        #    description => __("PlanPerfect spreadsheet"),
        #},

        #'application/x-profile' => {
        #    image       => 'file',
        #    description => __("profiler results"),
        #},

        #'application/x-pw' => {
        #    image       => 'file',
        #    description => __("Pathetic Writer document"),
        #},

        #'application/x-python-bytecode' => {
        #    image       => 'source_py',
        #    description => __("Python bytecode"),
        #},

        #'application/x-quattropro' => {
        #    image       => 'file',
        #    description => __("Quattro Pro spreadsheet"),
        #},

        #'application/x-quicktime-media-link' => {
        #    image       => 'file',
        #    description => __("QuickTime metalink playlist"),
        #},

        #'application/x-qw' => {
        #    image       => 'file',
        #    description => __("Quicken document"),
        #},

        #'application/x-rar' => {
        #    image       => 'zip',
        #    description => __("RAR archive"),
        #},

        #'application/x-reject' => {
        #    image       => 'file',
        #    description => __("rejected patch"),
        #},

        #'application/x-rpm' => {
        #    image       => 'file',
        #    description => __("RPM package"),
        #},

        #'application/x-ruby' => {
        #    image       => 'file',
        #    description => __("Ruby script"),
        #},

        #'application/x-sc' => {
        #    image       => 'file',
        #    description => __("SC/Xspread spreadsheet"),
        #},

        #'application/x-shar' => {
        #    image       => 'file',
        #    description => __("shell archive"),
        #},

        #'application/x-shared-library-la' => {
        #    image       => 'file',
        #    description => __("shared library (la)"),
        #},

        #'application/x-sharedlib' => {
        #    image       => 'file',
        #    description => __("shared library"),
        #},

        #'application/x-srt' => {
        #    image       => 'file',
        #    description => __("subtitle file"),
        #},

        #'application/x-shellscript' => {
        #    image       => 'shellscript',
        #    description => __("shell script"),
        #},

        #'application/x-shockwave-flash' => {
        #    image       => 'swf',
        #    description => __("Shockwave Flash file"),
        #},

        #'application/x-shorten' => {
        #    image       => 'file',
        #    description => __("Shorten audio"),
        #},

        #'application/x-siag' => {
        #    image       => 'file',
        #    description => __("Siag spreadsheet"),
        #},

        #'application/x-slp' => {
        #    image       => 'file',
        #    description => __("Stampede package"),
        #},

        #'application/x-sms-rom' => {
        #    image       => 'file',
        #    description => __("SMS/Game Gear ROM"),
        #},

        #'application/x-snes-rom' => {
        #    image       => 'file',
        #    description => __("Super Nintendo ROM"),
        #},

        #'application/x-stuffit' => {
        #    image       => 'zip',
        #    description => __("Macintosh StuffIt archive"),
        #},

        #'application/x-sv4cpio' => {
        #    image       => 'file',
        #    description => __("SV4 CPIO archive"),
        #},

        #'application/x-sv4crc' => {
        #    image       => 'file',
        #    description => __("SV4 CPIP archive (with CRC)"),
        #},

        #'application/x-tar' => {
        #    image       => 'tar',
        #    description => __("tar archive"),
        #},

        #'application/x-tarz' => {
        #    image       => 'zip',
        #    description => __("tar archive (compressed)"),
        #},

        #'application/x-tex-gf' => {
        #    image       => 'file',
        #    description => __("generic font file"),
        #},

        #'application/x-tex-pk' => {
        #    image       => 'file',
        #    description => __("packed font file"),
        #},

        #'application/x-tgif' => {
        #    image       => 'file',
        #    description => __("TGIF document"),
        #},

        #'application/x-theme' => {
        #    image       => 'file',
        #    description => __("theme"),
        #},

        #'application/x-toutdoux' => {
        #    image       => 'file',
        #    description => __("ToutDoux document"),
        #},

        #'application/x-trash' => {
        #    image       => 'file',
        #    description => __("backup file"),
        #},

        #'text/troff' => {
        #    image       => 'file',
        #    description => __("Troff document"),
        #},

        #'application/x-troff-man' => {
        #    image       => 'file',
        #    description => __("Troff document (with manpage macros)"),
        #},

        #'application/x-troff-man-compressed' => {
        #    image       => 'file',
        #    description => __("manual page (compressed)"),
        #},

        #'application/x-tzo' => {
        #    image       => 'file',
        #    description => __("tar archive (LZO-compressed)"),
        #},

        #'application/x-ustar' => {
        #    image       => 'file',
        #    description => __("ustar archive"),
        #},

        #'application/x-wais-source' => {
        #    image       => 'file',
        #    description => __("WAIS source code"),
        #},

        #'application/x-wpg' => {
        #    image       => 'file',
        #    description => __("WordPerfect/Drawperfect image"),
        #},

        #'application/x-x509-ca-cert' => {
        #    image       => 'file',
        #    description => __("DER/PEM/Netscape-encoded X.509 certificate"),
        #},

        #'application/x-zerosize' => {
        #    image       => 'file',
        #    description => __("empty document"),
        #},

        #'application/x-zoo' => {
        #    image       => 'file',
        #    description => __("zoo archive"),
        #},

        #'application/xhtml+xml' => {
        #    image       => 'html',
        #    description => __("XHTML page"),
        #},

        'application/zip' => {
           image       => 'application-zip',
           description => __("ZIP archive"),
        },

        #'audio/ac3' => {
        #    image       => 'sound',
        #    description => __("Dolby Digital audio"),
        #},

        #'audio/AMR' => {
        #    image       => 'sound',
        #    description => __("AMR audio"),
        #},

        #'audio/AMR-WB' => {
        #    image       => 'sound',
        #    description => __("AMR-WB audio"),
        #},

        #'audio/basic' => {
        #    image       => 'sound',
        #    description => __("ULAW (Sun) audio"),
        #},

        #'audio/prs.sid' => {
        #    image       => 'sound',
        #    description => __("Commodore 64 audio"),
        #},

        #'audio/x-adpcm' => {
        #    image       => 'sound',
        #    description => __("PCM audio"),
        #},

        #'audio/x-aifc' => {
        #    image       => 'sound',
        #    description => __("AIFC audio"),
        #},

        #'audio/x-aiff' => {
        #    image       => 'sound',
        #    description => __("AIFF/Amiga/Mac audio"),
        #},

        #'audio/x-aiffc' => {
        #    image       => 'sound',
        #    description => __("AIFF audio"),
        #},

        #'audio/x-ape' => {
        #    image       => 'sound',
        #    description => __("Monkey's audio"),
        #},

        #'audio/x-it' => {
        #    image       => 'sound',
        #    description => __("Impulse Tracker audio"),
        #},

        #'audio/x-ape' => {
        #    image       => 'sound',
        #    description => __("Monkey's Audio"),
        #},

        'audio/x-flac' => {
            image       => 'audio-x-flac',
            description => __("FLAC audio"),
        },

        #'audio/x-wavpack' => {
        #    image       => 'sound',
        #    description => __("WavPack audio"),
        #},

        #'audio/x-wavpack-correction' => {
        #    image       => 'sound',
        #    description => __("WavPack audio correction file"),
        #},

        'audio/midi' => {
            image       => 'audio-midi',
            description => __("MIDI audio"),
        },

        #'audio/mp4' => {
        #    image       => 'sound',
        #    description => __("MPEG-4 audio"),
        #},

        'video/mp4' => {
            image       => 'video-x-generic',
            description => __("MPEG-4 video"),
        },

        #'video/3gpp' => {
        #    image       => 'video',
        #    description => __("3GPP multimedia"),
        #},

        #'audio/x-mod' => {
        #    image       => 'sound',
        #    description => __("Amiga SoundTracker audio"),
        #},

        'audio/mpeg' => {
            image       => 'audio-x-generic',
            description => __("MP3 audio"),
        },

        #'audio/x-mp3-playlist' => {
        #    image       => 'file',
        #    description => __("MP3 playlist"),
        #},

        'audio/x-mpeg' => {
            image       => 'audio-x-generic',
            description => __("MP3 audio"),
        },

        #'audio/x-mpegurl' => {
        #    image       => 'sound',
        #    description => __("MP3 audio (streamed)"),
        #},

        #'audio/x-ms-asx' => {
        #    image       => 'file',
        #    description => __("Playlist"),
        #},

        'audio/x-ms-wma' => {
            image       => 'audio-x-generic',
            description => __("WMA audio"),
        },

        #'audio/x-musepack' => {
        #    image       => 'sound',
        #    description => __("Musepack audio"),
        #},

        #'audio/x-musepack' => {
        #    image       => 'sound',
        #    description => __("Musepack audio data"),
        #},

        #'audio/vnd.rn-realaudio' => {
        #    image       => 'real',
        #    description => __("RealAudio document"),
        #},

        #'audio/vnd.rn-realvideo' => {
        #    image       => 'real',
        #    description => __("RealVideo document"),
        #},

        #'application/vnd.rn-realmedia' => {
        #    image       => 'real',
        #    description => __("RealMedia document"),
        #},

        #'image/vnd.rn-realpix' => {
        #    image       => 'real',
        #    description => __("RealPix document"),
        #},

        #'text/vnd.rn-realtext' => {
        #    image       => 'real',
        #    description => __("RealText document"),
        #},

        #'audio/x-riff' => {
        #    image       => 'sound',
        #    description => __("RIFF audio"),
        #},

        #'audio/x-s3m' => {
        #    image       => 'sound',
        #    description => __("Scream Tracker 3 audio"),
        #},

        #'audio/x-scpls' => {
        #    image       => 'file',
        #    description => __("MP3 ShoutCast playlist"),
        #},

        #'audio/x-stm' => {
        #    image       => 'sound',
        #    description => __("Scream Tracker audio"),
        #},

        #'audio/x-voc' => {
        #    image       => 'sound',
        #    description => __("VOC audio"),
        #},

        'audio/x-wav' => {
            image       => 'audio-x-wav',
            description => __("WAV audio"),
        },

        #'audio/x-xi' => {
        #    image       => 'file',
        #    description => __("Scream Tracker instrument"),
        #},

        #'audio/x-xm' => {
        #    image       => 'sound',
        #    description => __("FastTracker II audio"),
        #},

        'image/bmp' => {
            image       => 'image-x-generic',
            description => __("Windows BMP image"),
        },

        'image/cgm' => {
            image       => 'image-x-generic',
            description => __("Computer Graphics Metafile"),
        },

        #'image/fax-g3' => {
        #    image       => 'image-x-generic',
        #    description => __("CCITT G3 fax"),
        #},

        #'image/g3fax' => {
        #    image       => 'image-x-generic',
        #    description => __("G3 fax image"),
        #},

        'image/gif' => {
            image       => 'image-x-generic',
            description => __("GIF image"),
        },

        #'image/ief' => {
        #    image       => 'image-x-generic',
        #    description => __("IEF image"),
        #},

        'image/jpeg' => {
            image       => 'image-x-generic',
            description => __("JPEG image"),
        },

        'image/jpeg2000' => {
            image       => 'image-x-generic',
            description => __("JPEG-2000 image"),
        },

        'image/x-pict' => {
            image       => 'image-x-generic',
            description => __("Macintosh Quickdraw/PICT drawing"),
        },

        #'image/x-minolta-mrw' => {
        #    image       => 'image',
        #    description => __("Minolta raw image"),
        #},

        #'image/x-canon-crw' => {
        #    image       => 'image',
        #    description => __("Canon raw image"),
        #},

        #'image/x-nikon-nef' => {
        #    image       => 'image',
        #    description => __("Nikon raw image"),
        #},

        #'image/x-kodak-dcr' => {
        #    image       => 'image',
        #    description => __("Kodak raw image"),
        #},

        #'image/x-kodak-kdc' => {
        #    image       => 'image',
        #    description => __("Kodak raw image"),
        #},

        #'image/x-olympus-orf' => {
        #    image       => 'image',
        #    description => __("Olympus raw image"),
        #},

        #'image/x-fuji-raf' => {
        #    image       => 'image',
        #    description => __("Fuji raw image"),
        #},

        'image/png' => {
            image       => 'image-x-generic',
            description => __("PNG image"),
        },

        #'image/rle' => {
        #    image       => 'image',
        #    description => __("Run Length Encoded bitmap"),
        #},

        'image/svg+xml' => {
            image       => 'image-svg+xml',
            description => __("SVG image"),
        },

        'image/tiff' => {
            image       => 'image-x-generic',
            description => __("TIFF image"),
        },

        'image/vnd.dwg' => {
            image       => 'image-x-generic',
            description => __("AutoCAD image"),
        },

        'image/vnd.dxf' => {
            image       => 'image-x-generic',
            description => __("DXF vector image"),
        },

        'image/x-3ds' => {
            image       => 'image-x-generic',
            description => __("3D Studio image"),
        },

        #'image/x-applix-graphics' => {
        #    image       => 'image',
        #    description => __("Applix Graphics image"),
        #},

        #'image/x-cmu-raster' => {
        #    image       => 'image',
        #    description => __("CMU raster image"),
        #},

        'image/x-compressed-xcf' => {
            image       => 'image-x-generic',
            description => __("GIMP image (compressed)"),
        },

        #'application/dicom' => {
        #    image       => 'image',
        #    description => __("DICOM image"),
        #},

        #'application/docbook+xml' => {
        #    image       => 'file',
        #    description => __("DocBook document"),
        #},

        #'image/x-dib' => {
        #    image       => 'image',
        #    description => __("DIB image"),
        #},

        #'image/vnd.djvu' => {
        #    image       => 'image',
        #    description => __("DjVu image"),
        #},

        #'image/dpx' => {
        #    image       => 'image',
        #    description => __("DPX image"),
        #},

        #'image/x-eps' => {
        #    image       => 'image',
        #    description => __("EPS image"),
        #},

        #'image/x-fits' => {
        #    image       => 'file',
        #    description => __("FITS document"),
        #},

        #'image/x-fpx' => {
        #    image       => 'image',
        #    description => __("FPX image"),
        #},

        #'image/x-ico' => {
        #    image       => 'image',
        #    description => __("ICO icon"),
        #},

        #'image/x-iff' => {
        #    image       => 'image',
        #    description => __("IFF image"),
        #},

        #'image/x-ilbm' => {
        #    image       => 'image',
        #    description => __("ILBM image"),
        #},

        #'image/x-jng' => {
        #    image       => 'image',
        #    description => __("JNG image"),
        #},

        #'image/x-lwo' => {
        #    image       => 'file',
        #    description => __("LightWave object"),
        #},

        #'image/x-lws' => {
        #    image       => 'file',
        #    description => __("LightWave scene"),
        #},

        #'image/x-msod' => {
        #    image       => 'file',
        #    description => __("Office drawing"),
        #},

        #'image/x-niff' => {
        #    image       => 'file',
        #    description => "",
        #},

        #'image/x-pcx' => {
        #    image       => 'image',
        #    description => __("PCX image"),
        #},

        #'image/x-photo-cd' => {
        #    image       => 'image',
        #    description => __("PCD image"),
        #},

        'image/x-portable-anymap' => {
            image       => 'image-x-generic',
            description => __("PNM image"),
        },

        'image/x-portable-bitmap' => {
            image       => 'image-x-generic',
            description => __("PBM image"),
        },

        'image/x-portable-graymap' => {
            image       => 'image-x-generic',
            description => __("PGM image"),
        },

        'image/x-portable-pixmap' => {
            image       => 'image-x-generic',
            description => __("PPM image"),
        },

        #'image/x-psd' => {
        #    image       => 'image',
        #    description => __("PSD image"),
        #},

        #'image/x-rgb' => {
        #    image       => 'image',
        #    description => __("RGB image"),
        #},

        #'image/x-sgi' => {
        #    image       => 'image',
        #    description => __("SGI image"),
        #},

        #'image/x-sun-raster' => {
        #    image       => 'image',
        #    description => __("SUN Rasterfile image"),
        #},

        #'image/x-tga' => {
        #    image       => 'image',
        #    description => __("TGA image"),
        #},

        #'image/x-win-bitmap' => {
        #    image       => 'file',
        #    description => __("Windows cursor"),
        #},

        #'image/x-wmf' => {
        #    image       => 'file',
        #    description => __("WMF document"),
        #},

        'image/x-xbitmap' => {
            image       => 'image-x-generic',
            description => __("XBM image"),
        },

        'image/x-xcf' => {
            image       => 'image-x-generic',
            description => __("GIMP image"),
        },

        'image/x-xfig' => {
            image       => 'image-x-generic',
            description => __("XFig image"),
        },

        'image/x-xpixmap' => {
            image       => 'image-x-generic',
            description => __("XPM image"),
        },

        #'image/x-xwindowdump' => {
        #    image       => 'image',
        #    description => __("X window image"),
        #},

        #'inode/blockdevice' => {
        #    image       => 'file',
        #    description => __("block device"),
        #},

        #'inode/chardevice' => {
        #    image       => 'file',
        #    description => __("character device"),
        #},

        #'inode/directory' => {
        #    image       => 'folder',
        #    description => __("folder"),
        #},

        #'inode/fifo' => {
        #    image       => 'file',
        #    description => __("pipe"),
        #},

        #'inode/mount-point' => {
        #    image       => 'file',
        #    description => __("mount point"),
        #},

        #'inode/socket' => {
        #    image       => 'file',
        #    description => __("socket"),
        #},

        #'inode/symlink' => {
        #    image       => 'file',
        #    description => __("symbolic link"),
        #},

        #'message/delivery-status' => {
        #    image       => 'file',
        #    description => __("mail delivery report"),
        #},

        #'message/disposition-notification' => {
        #    image       => 'file',
        #    description => __("mail disposition report"),
        #},

        #'message/external-body' => {
        #    image       => 'file',
        #    description => __("reference to remote file"),
        #},

        #'message/news' => {
        #    image       => 'file',
        #    description => __("Usenet news message"),
        #},

        #'message/partial' => {
        #    image       => 'file',
        #    description => __("partial email message"),
        #},

        #'message/rfc822' => {
        #    image       => 'file',
        #    description => __("email message"),
        #},

        #'message/x-gnu-rmail' => {
        #    image       => 'file',
        #    description => __("GNU mail message"),
        #},

        #'model/vrml' => {
        #    image       => 'file',
        #    description => __("VRML document"),
        #},

        #'multipart/alternative' => {
        #    image       => 'file',
        #    description => __("message in several formats"),
        #},

        #'multipart/appledouble' => {
        #    image       => 'file',
        #    description => __("Macintosh AppleDouble-encoded file"),
        #},

        #'multipart/digest' => {
        #    image       => 'file',
        #    description => __("message digest"),
        #},

        #'multipart/encrypted' => {
        #    image       => 'file',
        #    description => __("encrypted message"),
        #},

        #'multipart/mixed' => {
        #    image       => 'file',
        #    description => __("compound documents"),
        #},

        #'multipart/related' => {
        #    image       => 'file',
        #    description => __("compound document"),
        #},

        #'multipart/report' => {
        #    image       => 'file',
        #    description => __("mail system report"),
        #},

        #'multipart/signed' => {
        #    image       => 'file',
        #    description => __("signed message"),
        #},

        #'multipart/x-mixed-replace' => {
        #    image       => 'file',
        #    description => __("stream of data (server push)"),
        #},

        'text/calendar' => {
            image       => 'text-calendar',
            description => __("VCS/ICS calendar"),
        },

        'text/css' => {
            image       => 'text-css',
            description => __("CSS stylesheet"),
        },

        #'text/directory' => {
        #    image       => 'file',
        #    description => __("Electronic Business Card"),
        #},

        #'text/x-txt2tags' => {
        #    image       => 'file',
        #    description => __("txt2tags document"),
        #},

        #'text/x-vhdl' => {
        #    image       => 'file',
        #    description => __("VHDL document"),
        #},

        #'text/enriched' => {
        #    image       => 'file',
        #    description => __("enriched text document"),
        #},

        #'text/htmlh' => {
        #    image       => 'file',
        #    description => __("help page"),
        #},

        #'text/mathml' => {
        #    image       => 'file',
        #    description => __("MathML document"),
        #},

        'text/plain' => {
            image       => 'text-plain',
            description => __("plain text document"),
        },

        'text/rdf' => {
            image       => 'text-rdf',
            description => __("RDF file"),
        },

        #'text/rfc822-headers' => {
        #    image       => 'file',
        #    description => __("email headers"),
        #},

        'text/richtext' => {
            image       => 'text-rtf',
            description => __("rich text document"),
        },

        'application/rss+xml' => {
            image       => 'text-rdf',
            description => __("RSS summary"),
        },

        #'text/sgml' => {
        #    image       => 'test-sgml',
        #    description => __("SGML document"),
        #},

        'text/spreadsheet' => {
            image       => 'text-csv',
            description => __("Spreadsheet interchange document"),
        },

        'text/tab-separated-values' => {
            image       => 'text-csv',
            description => __("TSV document"),
        },

        #'text/vnd.wap.wml' => {
        #    image       => 'file',
        #    description => __("WML document"),
        #},

        #'text/x-adasrc' => {
        #    image       => 'file',
        #    description => __("Ada source code"),
        #},

        #'text/x-authors' => {
        #    image       => 'file',
        #    description => __("author list"),
        #},

        #'text/x-bibtex' => {
        #    image       => 'file',
        #    description => __("BibTeX document"),
        #},

        #'text/x-c++hdr' => {
        #    image       => 'file',
        #    description => __("C++ header"),
        #},

        #'text/x-c++src' => {
        #    image       => 'file',
        #    description => __("C++ source code"),
        #},

        #'text/x-changelog' => {
        #    image       => 'file',
        #    description => __("ChangeLog document"),
        #},

        #'text/x-chdr' => {
        #    image       => 'file',
        #    description => __("C header"),
        #},

        'text/csv' => {
            image       => 'text-csv',
            description => __("CSV document"),
        },

        #'text/x-copying' => {
        #    image       => 'file',
        #    description => __("license terms"),
        #},

        #'text/x-credits' => {
        #    image       => 'file',
        #    description => __("author credits"),
        #},

        #'text/x-csrc' => {
        #    image       => 'file',
        #    description => __("C source code"),
        #},

        #'text/x-csharp' => {
        #    image       => 'file',
        #    description => __("C# source code"),
        #},

        #'text/x-dcl' => {
        #    image       => 'file',
        #    description => __("DCL script"),
        #},

        #'text/x-dsl' => {
        #    image       => 'file',
        #    description => __("DSSSL document"),
        #},

        #'text/x-dsrc' => {
        #    image       => 'file',
        #    description => __("D source code"),
        #},

        'text/x-dtd' => {
            image       => 'text-x-dtd',
            description => __("DTD file"),
        },

        #'text/x-emacs-lisp' => {
        #    image       => 'file',
        #    description => __("Emacs Lisp source code"),
        #},

        #'text/x-fortran' => {
        #    image       => 'file',
        #    description => __("Fortran source code"),
        #},

        #'text/x-gettext-translation' => {
        #    image       => 'file',
        #    description => __("translation"),
        #},

        #'text/x-gettext-translation-template' => {
        #    image       => 'file',
        #    description => __("translation template"),
        #},

        'text/html' => {
            image       => 'text-html',
            description => __("HTML document"),
        },

        #'text/x-gtkrc' => {
        #    image       => 'file',
        #    description => __("GTK+ configuration"),
        #},

        #'text/x-haskell' => {
        #    image       => 'file',
        #    description => __("Haskell source code"),
        #},

        #'text/x-idl' => {
        #    image       => 'file',
        #    description => __("IDL document"),
        #},

        #'text/x-install' => {
        #    image       => 'file',
        #    description => __("installation instructions"),
        #},

        #'text/x-java' => {
        #    image       => 'file',
        #    description => __("Java source code"),
        #},

        #'text/x-ksysv-log' => {
        #    image       => 'file',
        #    description => "",
        #},

        #'text/x-literate-haskell' => {
        #    image       => 'file',
        #    description => __("LHS source code"),
        #},

        #'text/x-log' => {
        #    image       => 'file',
        #    description => __("application log"),
        #},

        #'text/x-makefile' => {
        #    image       => 'make',
        #    description => __("Makefile"),
        #},

        #'text/x-moc' => {
        #    image       => 'file',
        #    description => __("Qt MOC file"),
        #},

        #'text/x-mup' => {
        #    image       => 'file',
        #    description => __("Mup publication"),
        #},

        #'text/x-objcsrc' => {
        #    image       => 'file',
        #    description => __("Objective-C source code"),
        #},

        #'text/x-ocaml' => {
        #    image       => 'file',
        #    description => __("OCaml source code"),
        #},

        #'text/x-matlab' => {
        #    image       => 'file',
        #    description => __("MATLAB script/function"),
        #},

        #'text/x-pascal' => {
        #    image       => 'file',
        #    description => __("Pascal source code"),
        #},

        #'text/x-patch' => {
        #    image       => 'file',
        #    description => __("differences between files"),
        #},

        #'text/x-python' => {
        #    image       => 'file',
        #    description => __("Python script"),
        #},

        #'text/x-readme' => {
        #    image       => 'readme',
        #    description => __("README document"),
        #},

        #'text/x-scheme' => {
        #    image       => 'file',
        #    description => __("Scheme source code"),
        #},

        #'text/x-setext' => {
        #    image       => 'file',
        #    description => __("Setext document"),
        #},

        #'text/x-speech' => {
        #    image       => 'file',
        #    description => __("Speech document"),
        #},

        #'text/x-sql' => {
        #    image       => 'file',
        #    description => __("SQL code"),
        #},

        #'text/x-tcl' => {
        #    image       => 'file',
        #    description => __("Tcl script"),
        #},

        #'text/x-tex' => {
        #    image       => 'file',
        #    description => __("TeX document"),
        #},

        #'text/x-texinfo' => {
        #    image       => 'file',
        #    description => __("TeXInfo document"),
        #},

        #'text/x-troff-me' => {
        #    image       => 'file',
        #    description => __("Troff ME input document"),
        #},

        #'text/x-troff-mm' => {
        #    image       => 'file',
        #    description => __("Troff MM input document"),
        #},

        #'text/x-troff-ms' => {
        #    image       => 'file',
        #    description => __("Troff MS input document"),
        #},

        #'text/x-uil' => {
        #    image       => 'file',
        #    description => __("X-Motif UIL table"),
        #},

        #'text/x-uri' => {
        #    image       => 'file',
        #    description => __("resource location"),
        #},

        #'text/x-xmi' => {
        #    image       => 'file',
        #    description => __("XMI file"),
        #},

        #'text/x-xslfo' => {
        #    image       => 'file',
        #    description => __("XSL FO file"),
        #},

        'application/xslt+xml' => {
            image       => 'text-xml',
            description => __("XSLT stylesheet"),
        },

        #'text/xmcd' => {
        #    image       => 'file',
        #    description => "",
        #},

        'application/xml' => {
            image       => 'text-xml',
            description => __("XML document"),
        },

        #'video/dv' => {
        #    image       => 'video',
        #    description => __("DV video"),
        #},

        #'video/isivideo' => {
        #    image       => 'video',
        #    description => __("ISI video"),
        #},

        'video/mpeg' => {
            image       => 'video-x-generic',
            description => __("MPEG video"),
        },

        'video/quicktime' => {
            image       => 'video-x-generic',
            description => __("QT video"),
        },

        #'video/vivo' => {
        #    image       => 'video',
        #    description => __("Vivo video"),
        #},

        #'video/wavelet' => {
        #    image       => 'video',
        #    description => __("Wavelet video"),
        #},

        #'video/x-anim' => {
        #    image       => 'video',
        #    description => __("ANIM animation"),
        #},

        #'video/x-flic' => {
        #    image       => 'video',
        #    description => __("FLIC animation"),
        #},

        #'video/x-mng' => {
        #    image       => 'video',
        #    description => __("MNG animation"),
        #},

        #'video/x-ms-asf' => {
        #    image       => 'video',
        #    description => __("ASF video"),
        #},

        'video/x-ms-wmv' => {
            image       => 'video-x-generic',
            description => __("WMV video"),
        },

        'video/x-msvideo' => {
            image       => 'video-x-generic',
            description => __("AVI video"),
        },

        #'video/x-nsv' => {
        #    image       => 'video',
        #    description => __("NSV video"),
        #},

        #'video/x-sgi-movie' => {
        #    image       => 'video',
        #    description => __("SGI video"),
        #},

        #'application/x-crw' => {
        #    image       => 'file',
        #    description => __("Canon RAW File"),
        #},

        #'application/x-mps' => {
        #    image       => 'file',
        #    description => __("Linear and integer program expression format"),
        #},
        "application/x-pdf" => {
            image       => 'application-pdf',
            description => __("PDF document"),
        },
        "application/x-bzip2" => {
            image       => 'application-x-bzip',
            description => __("bzip archive"),
        },
        #"application/x-debian-package" => {
        #    image       => 'deb',
        #    description => __("Debian package"),
        #},
        "application/x-zip-compressed" => {
            image       => 'application-zip',
            description => __("ZIP archive"),
        },
        "audio/x-midi" => {
            image       => 'audio-midi',
            description => __("MIDI audio"),
        },
        "audio/x-mp3" => {
            image       => 'audio-x-generic',
            description => __("MP3 audio"),
        },
        "audio/wav" => {
            image       => 'audio-x-wav',
            description => __("WAV audio"),
        },
        "text/x-vcalendar" => {
            image       => 'text-calendar',
            description => __("VCS/ICS calendar"),
        },
        "text/x-vcard" => {
            image       => 'text-x-vcard',
            description => __("VCard"),
        },
        "text/xml" => {
            image       => 'text-xml',
            description => __("XML document"),
        },
        "video/x-avi" => {
            image       => 'video-x-generic',
            description => __("AVI video"),
        },
        "video/vnd.divx" => {
            image       => 'video-x-generic',
            description => __("DivX video"),
        },
        'application/vnd.openxmlformats-officedocument.presentationml.presentation' => {
            image       => 'application-vnd.ms-powerpoint',
            description => __("Office Open XML Presentation"),
        },
        'application/vnd.openxmlformats-officedocument.presentationml.slideshow' => {
            image       => 'application-vnd.ms-powerpoint',
            description => __("Office Open XML Presentation Slideshow"),
        },
        'application/vnd.ms-powerpoint.slide.macroEnabled.12' => {
            image       => 'application-vnd.ms-powerpoint',
            description => __("Office Open XML Presentation Macro-Enabled Slide"),
        },
        'application/vnd.openxmlformats-officedocument.presentationml.slide' => {
            image       => 'application-vnd.ms-powerpoint',
            description => __("Office Open XML Presentation Slide"),
        },
        'application/vnd.ms-powerpoint.addin.macroEnabled.12' => {
            image       => 'application-vnd.ms-powerpoint',
            description => __("Microsoft Powerpoint Addin"),
        },
        'application/vnd.ms-powerpoint.template.macroEnabled.12' => {
            image       => 'application-vnd.ms-powerpoint',
            description => __("Office Open XML Macro-Enabled Presentation Template"),
        },
        'application/vnd.ms-powerpoint.slideshow.macroEnabled.12' => {
            image       => 'application-vnd.ms-powerpoint',
            description => __("Office Open XML Macro-Enabled Presentation Slideshow"),
        },
        'application/vnd.ms-powerpoint.presentation.macroEnabled.12' => {
            image       => 'application-vnd.ms-powerpoint',
            description => __("Office Open XML Macro-Enabled Presentation"),
        },
        'application/vnd.openxmlformats-officedocument.presentationml.template' => {
            image       => 'application-vnd.ms-powerpoint',
            description => __("Office Open XML Presentation Template"),
        },
        'application/vnd.ms-excel.addin.macroEnabled.12' => {
            image       => 'application-vnd.ms-excel',
            description => __("Microsoft Excel Addin"),
        },
        'application/vnd.ms-excel.sheet.binary.macroEnabled.12' => {
            image       => 'application-vnd.ms-excel',
            description => __("Office Open XML Binary Spreadsheet"),
        },
        'application/vnd.ms-excel.template.macroEnabled.12' => {
            image       => 'application-vnd.ms-excel',
            description => __("Office Open XML Macro-Enabled Spreadsheet Template"),
        },
        'application/vnd.ms-excel.sheet.macroEnabled.12' => {
            image       => 'application-vnd.ms-excel',
            description => __("Office Open XML Macro-Enabled Spreadsheet"),
        },
        'application/vnd.openxmlformats-officedocument.spreadsheetml.template' => {
            image       => 'application-vnd.ms-excel',
            description => __("Office Open XML Spreadsheet Template"),
        },
        'application/vnd.ms-word.template.macroEnabled.12' => {
            image       => 'application-msword',
            description => __("Office Open XML Macro-Enabled Document Template"),
        },
        'application/vnd.ms-word.document.macroEnabled.12' => {
            image       => 'application-msword',
            description => __("Office Open XML Macro-Enabled Document"),
        },
        'application/vnd.openxmlformats-officedocument.wordprocessingml.template' => {
            image       => 'application-msword',
            description => __("Office Open XML Document Template"),
        },
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => {
            image       => 'application-msword',
            description => __("Office Open XML Document"),
        },
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => {
            image       => 'application-vnd.ms-excel',
            description => __("Office Open XML Spreadsheet"),
        },
    };

    return $data;
}

# ============================================================================

=head2 IncludeMime ()

  format mimetypes list to be included (css or javascript)

=cut

# ============================================================================

sub IncludeMime {
    my ( $self, $context ) = @_;

	my $config = $context->GetConfig ();
	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['css', 'js'], 'allow_empty'],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Magellan::IncludeMime", __("Wrong argument value."))
	}

    my $mimetypes = $self->MimeList;
    my $xsl_sheet = exists $context->{args}->{css} ? 'css' : 'js';

    my $data = { IncludeMime => $context->GetContext () };
    $data->{IncludeMime}->{mimetype} = [];
    foreach my $mime ( keys %{ $mimetypes->{mimetypes} } ) {
        push @{ $data->{IncludeMime}->{mimetype} },
          {
            type        => $mime,
            class       => $mimetypes->{mimetypes}->{$mime}->{image},
            description => $mimetypes->{mimetypes}->{$mime}->{description}
          };
    }

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "[Mioga2::Magellan::IncludeMime] XML: $xml\n" if ($debug);

    my $content = new Mioga2::Content::XSLT(
        $context,
        stylesheet    => "mime_$xsl_sheet.xsl",
        locale_domain => "mime_xsl"
    );
    $content->SetContent ($xml);

    return $content;
}

# ============================================================================
# Private Methods
# ============================================================================

sub get_infos_for_directory {
    my ( $self, $context, $node ) = @_;

    my @infos = ( 0, 0, 0 );
    my $dbh = $context->GetConfig->GetDBH;
    
    my $result = SelectSingle( $dbh,
"SELECT COUNT(*) AS elements, SUM(size) AS size FROM m_uri WHERE uri SIMILAR TO '"
          . st_FormatPostgreSQLRegExpString($node) . "(/%)*'" );
    if ($result) {
        $infos[0] = $result->{elements};
        $infos[1] = $result->{size};
        $infos[2] = Mioga2::Louvre::Gallery::SQL->uri_is_gallery($dbh, $node);
    }
    return @infos;
}

# ============================================================================
# GetRSSFeed
#   Retrieve feed of files
# ============================================================================

sub GetRSSFeed {
    my ($self, $context, $ident, $feed, $max)   = @_;
    
    my $config  = $context->GetConfig;
    my $group;
    try {
        $group = Mioga2::Old::Group->new($config, ident => $ident);
    }
    otherwise {
        $group = Mioga2::Old::User->new($config, ident => $ident);
    };

    if (!$feed) {
        $feed   = XML::Atom::SimpleFeed->new(
            title   => __x("Files feed of {group} group", group => $group->GetIdent),
            link    => { href => $config->GetProtocol . "://" . $config->GetServerName . $context->{uri}->GetURI, rel => 'self' },
            link    => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/Magellan/DisplayMain",
            id      => "data:,group:".$group->GetIdent.":files",
        );
    }
    
    $max            = 20 unless $max;
    my $files       = $self->last_modified($context, max => $max, group => $group);
    my $group_ident = $group->GetIdent;

    foreach my $file (@$files) {
        my $basename    = basename($file->{uri});
        my $link        = $config->GetProtocol . "://" . $config->GetServerName . st_URIEscape ($file->{uri});
        $feed->add_entry(
            title       => __x("[{group}] File: {file}", group => $group_ident, file => $basename),
            link        => $link,
            id          => "data:,group:".$group->GetIdent.":files:$file->{rowid}",
            author      => "$file->{firstname} $file->{lastname}",
            published   => Mioga2::Classes::Time->FromPSQL($file->{modified})->RFC3339DateTime,
            updated     => Mioga2::Classes::Time->FromPSQL($file->{modified})->RFC3339DateTime,
            content     => "<p><a href=\"$link\">$link</a></p>",
        );
    }
    return $feed;
}

# ============================================================================

=head2 last_modified ()

  Return last modified files for given group.

=cut

# ============================================================================

sub last_modified {
  my ($self, $context, %options) = @_;
  
  my $default_options   = {max => 20, group => $context->GetGroup};
  %options      = (%$default_options, %options);
  
  my $config    = $context->GetConfig;
  my $dbh       = $config->GetDBH;
  my $user      = $context->GetUser;
  my $group     = $options{group};
  my $limit     = $options{max};
  my $instance  = $config->GetMiogaId;
  my $group_id  = $group->GetRowid;
  
  my $offset      = 0;
  my $offset_step = 50;
            
  my @rowids;
  my $total = SelectSingle($dbh, "SELECT COUNT(m_uri.rowid) FROM m_uri, m_group_base WHERE 
            m_uri.group_id = m_group_base.rowid AND 
            m_group_base.mioga_id = $instance AND m_group_base.rowid = $group_id AND m_uri.mimetype <> 'directory'");
  $total    = $total->{count};
  while ($total > 0 && @rowids < $limit) {
    push @rowids, AuthzTestAccessForRequest($config, $user, "SELECT m_uri.rowid FROM m_uri, m_group_base WHERE 
            m_uri.group_id = m_group_base.rowid AND 
            m_group_base.mioga_id = $instance AND m_group_base.rowid = $group_id AND m_uri.mimetype <> 'directory' ORDER BY m_uri.modified DESC LIMIT $offset_step OFFSET $offset");
    $offset += $offset_step;
    $total -= $offset_step;
  }
  
  my $ids     = "(" . join(',', @rowids) . ")";
  my $res     = SelectMultiple($dbh, "SELECT m_uri.*, m_user_base.firstname, m_user_base.lastname 
                FROM m_uri, m_user_base 
                WHERE m_uri.rowid IN $ids AND m_uri.user_id = m_user_base.rowid
                ORDER BY modified DESC");
  splice @$res, $limit;
  
  return $res;
}


# ============================================================================

=head2 AccessRights ()

	acces to ACL management

=head3 Generated XML

	<AccessRights>

       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <referer>URL of calling application</referer>
	
	   <inherite>Inherite or not from paren,t URI (0|1)</inherite>
       <uri>Current file escaped URI</uri>
	   <dispuri>Current file URI ot escaped</dispuri>


       <!-- AccessRight description -->

       <AccessRights>
	       <Profile rowid="ROWID" inherited="0|1" access="RIGHT">NAME</Profile>
	       <Profile ...

	       <User rowid="ROWID" inherited="0|1" access="RIGHT">NAME</Profile>
	       <User ...

           <Team rowid="ROWID" inherited="0|1" access="RIGHT">NAME</Profile>
	       <Team ...
       </AccessRights>
	
	  <!--
	        Errors description. See Mioga2::tools::args_checker->ac_XMLifyErrors
        -->
    	
    </AccessRights>     

=cut

# ============================================================================

sub AccessRights {
	my ($self, $context) = @_;
	my $config = $context->GetConfig();
	my $session = $context->GetSession();

	my $dbh = $config->GetDBH();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['mode', 'no_header'], 'allow_empty'],
				[ ['uri'], 'disallow_empty'],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# getting URI args
	# ----------------
	
	if(exists $context->{args}->{uri}) {
		delete $session->{Magellan}->{AccessRights};

		my $uri = $self->GetURI($dbh, $context->{args}->{uri});

		if(defined $uri) {
			$session->{Magellan}->{AccessRights} = 
			{ uri    => $context->{args}->{uri},
			  uri_id => $uri->{rowid},
			  has_spec_rights => $self->HasSpecificRights($dbh, $uri->{rowid}),
			  inherite => ($uri->{stop_inheritance}==0)?1:0,
			  referer => $context->GetReferer(),
		  };
		}
			
		if(exists $context->{args}->{referer}) {
			$session->{Magellan}->{AccessRights}->{referer} = $context->{args}->{referer};
		}

		$session->{Magellan}->{AccessRights}->{referer} =~ s/\?.*$//g;
	}
	
	# change inheritance mode
	# ----------------------
	elsif(st_ArgExists($context, 'act_change_inheritance')) {
		if($context->{args}->{inheritance} eq 'stop') {
			AuthzDisableURIInheritance($dbh, $session->{Magellan}->{AccessRights}->{uri_id});
			$session->{Magellan}->{AccessRights}->{inherite} = 0;
		}

		elsif($context->{args}->{inheritance} eq 'inherite') {
			AuthzEnableURIInheritance($dbh, $session->{Magellan}->{AccessRights}->{uri_id});
			$session->{Magellan}->{AccessRights}->{inherite} = 1;
		}
	}
	
	# add new user
	# ----------------------
	elsif(st_ArgExists($context, 'act_add_user')) {
		return $self->LaunchSelectMultipleUser($context);
	}
	
	# add new user callback
	# ----------------------
	elsif(st_ArgExists($context, 'back_add_user')) {
		if(st_ArgExists($context, 'rowids')) {
			if( ! $session->{Magellan}->{AccessRights}->{has_spec_rights}) {
				AuthzDuplicateParentAuthz($dbh, $session->{Magellan}->{AccessRights}->{uri_id});
			}

			foreach my $rowid (split (',', $context->{args}->{rowids})) {
				AuthzAddAuthzForUser($dbh, $session->{Magellan}->{AccessRights}->{uri_id}, $rowid, 0);
			}
		}
	}
	
	# add new team
	# ----------------------
	elsif(st_ArgExists($context, 'act_add_team')) {
		return $self->LaunchSelectMultipleTeam($context);
	}
		

	# add new team callback
	# ----------------------
	elsif(st_ArgExists($context, 'back_add_team')) {
		if(st_ArgExists($context, 'rowids')) {
			if( ! $session->{Magellan}->{AccessRights}->{has_spec_rights}) {
				AuthzDuplicateParentAuthz($dbh, $session->{Magellan}->{AccessRights}->{uri_id});
			}

			foreach my $rowid (split (',', $context->{args}->{rowids})) {
				AuthzAddAuthzForTeam($dbh, $session->{Magellan}->{AccessRights}->{uri_id}, $rowid, 0);
			}
		}
	}
		
	else {
		my ($values, $errors) = ({}, []);
		$values->{uri_id}          = $session->{Magellan}->{AccessRights}->{uri_id};
		$values->{has_spec_rights} = $session->{Magellan}->{AccessRights}->{has_spec_rights};
		

		# changing access rights
		# ----------------------
		if(! @$errors) {
			
			if( ! $values->{has_spec_rights} and 
				(
				 ( st_ArgExists($context, 'act_change_user')) or
				 ( st_ArgExists($context, 'act_change_team')) or
				 ( st_ArgExists($context, 'act_change_profile'))
				 )
				) {
				AuthzDuplicateParentAuthz($dbh, $values->{uri_id});
			}

			warn Dumper($context->{args}) if ($debug);
			foreach my $key (keys %{$context->{args}}) {

				if(st_ArgExists($context, 'act_change_user') and $key =~ /^user_(\d+)/) {
					my $user_id = $1;
					AuthzUpdateAuthzForUser($dbh, $values->{uri_id}, $user_id, $context->{args}->{$key});
				}

				elsif(st_ArgExists($context, 'act_change_team') and $key =~ /^team_(\d+)/) {
					my $team_id = $1;
					AuthzUpdateAuthzForTeam($dbh, $values->{uri_id}, $team_id, $context->{args}->{$key});
				}

				elsif(st_ArgExists($context, 'act_change_profile') and $key =~ /^profile_(\d+)/) {
					my $profile_id = $1;
					AuthzUpdateAuthzForProfile($dbh, $values->{uri_id}, $profile_id, $context->{args}->{$key});
				}

				elsif(st_ArgExists($context, 'act_delete_user') and $key =~ /^delete_user_(\d+)/) {
					my $user_id = $1;
					AuthzDeleteAuthzForUser($dbh, $values->{uri_id}, $user_id);
				}

				elsif(st_ArgExists($context, 'act_delete_team') and $key =~ /^delete_team_(\d+)/) {
					my $team_id = $1;
					AuthzDeleteAuthzForTeam($dbh, $values->{uri_id}, $team_id);
				}

				elsif(st_ArgExists($context, 'act_delete_profile') and $key =~ /^delete_profile_(\d+)/) {
					my $profile_id = $1;
					AuthzDeleteAuthzForProfile($dbh, $values->{uri_id}, $profile_id);
				}
			}
		}
	}

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'magellan.xsl', locale_domain => 'magellan_xsl');

	my $dispuri = $session->{Magellan}->{AccessRights}->{uri};

	my $xml = '<?xml version="1.0"?>';
	$xml .= "<AccessRights>";
	$xml .= $context->GetXML();
	$xml .= "<referer>".st_FormatXMLString($session->{Magellan}->{AccessRights}->{referer})."</referer>";
	$xml .= "<inherite>$session->{Magellan}->{AccessRights}->{inherite}</inherite>";
	$xml .= "<uri>".st_FormatXMLString(st_URIEscape($session->{Magellan}->{AccessRights}->{uri}))."</uri>";
	$xml .= "<dispuri>".st_FormatXMLString($dispuri)."</dispuri>";
	$xml .= $self->GenerateXMLAccessRights($context, $session->{Magellan}->{AccessRights}->{uri_id});
	
	$xml .= ac_XMLifyErrors($errors);
	
	$xml .= "</AccessRights>";
	print STDERR "[Mioga2::Magellan::AccessRights] XML: $xml\n" if ($debug);

	$content->SetContent($xml);

	return $content;
}


# ============================================================================
# GenerateXMLAccessRights ($context, $uri_id)
#
#    generate xml description of rights
#
#    <AccessRights>
#       <Profile rowid="ROWID" inherited="0|1" access="RIGHT">NAME</Profile>
#       <Profile ...
#       <User rowid="ROWID" inherited="0|1" access="RIGHT">NAME</Profile>
#       <User ...
#       <Team rowid="ROWID" inherited="0|1" access="RIGHT">NAME</Profile>
#       <Team ...
#    </AccessRights>
#
#    RIGHT : 0 : none, 1 : read, 2 : read/write
#    ROWID : profile rowid
#    NAME  : Profile|Group|Team name
#
# ============================================================================
sub GenerateXMLAccessRights {
	my ($self, $context, $uri_id) = @_;
	
	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $uri = new Mioga2::URI ($config, rowid => $uri_id);
	my $group = $uri->GetGroup ();
	my $group_id = $group->GetRowid();
	
	my $dbh = $config->GetDBH();

	# Getting profile list for current group
	# --------------------------------------
	my $xml = "\n";

	my $access = $self->GetAccessForGroupProfilesOnURI($dbh, $uri_id, $group_id);

 	foreach my $profile (@$access)
 	{
 		$xml .= '<Profile rowid="'.st_FormatXMLString($profile->{profile_id}).'" '.
			    '         inherited="'.st_FormatXMLString($profile->{inherited}).'"'.
				'         access="'.st_FormatXMLString($profile->{access}).'">'.
				st_FormatXMLString($profile->{profile_ident}).
				'</Profile>';
 	}

	$xml .= "\n";

	
	# Getting user list for current group
	# --------------------------------------
	my $users = $self->GetUserAccessListOnURI($dbh, $uri_id);
	foreach my $user (@$users)
	{
		my $user_obj = new Mioga2::Old::User($config, rowid => $user->{user_id});

		$xml .= qq|<User rowid="$user->{user_id}" 
			             inherited="$user->{inherited}" 
						 access="$user->{access}" 
						 ident="|.st_FormatXMLString($user_obj->GetIdent()).qq|">|;

		$xml .= st_FormatXMLString($user_obj->GetName());

		$xml .= '</User>';
	}
	
	$xml .= "\n";
	
	# Getting team list for current group
	# --------------------------------------
	my $teams = $self->GetTeamAccessListOnURI($dbh, $uri_id);
	foreach my $team (@$teams)
	{
		my $team_obj = new Mioga2::Old::Team($config, rowid => $team->{team_id});

		$xml .= qq|<Team rowid="$team->{team_id}" 
			             inherited="$team->{inherited}" 
						 access="$team->{access}" 
						 ident="|.st_FormatXMLString($team_obj->GetIdent()).qq|">|;

		$xml .= st_FormatXMLString($team_obj->GetIdent());
		
		$xml .= '</Team>';
	}
	

	return $xml;
}


# ============================================================================
# LaunchSelectMultipleUser ()
#
#    Launch the select multiple user list.
#
#    Store current form values into session,
#    Build Select application URI
#    And return a REDIRECT content.
#
# ============================================================================

sub LaunchSelectMultipleUser {
	my($self, $context) = @_;
	
	my $config  = $context->GetConfig();
	my $session = $context->GetSession();
	
	my $dbh     = $config->GetDBH();

	my $group   = $context->GetGroup();
	my $cur_uri = $context->GetURI();

	my $users   = $self->GetUserAccessListOnURI($dbh, $session->{Magellan}->{AccessRights}->{uri_id});
	
    my $uri = new Mioga2::URI($config, group  => $group->GetIdent(),
	                                   public => 0,
	                                   application => 'Select',
	                                   method => 'SelectMultipleUsersMemberOfGroup',
	                                   args   => { action  => 'back_add_user',
	                                               referer => $cur_uri->GetURI(),
												   rowids  => join(',', map {$_->{user_id}} @$users),
											     },
	                                   );
	
	my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	$content->SetContent($uri->GetURI());

	return $content;
}


# ============================================================================
# LaunchSelectMultipleTeam ()
#
#    Launch the select multiple team list.
#
#    Store current form values into session,
#    Build Select application URI
#    And return a REDIRECT content.
#
# ============================================================================

sub LaunchSelectMultipleTeam {
	my($self, $context) = @_;
	
	my $config  = $context->GetConfig();
	my $session = $context->GetSession();
	
	my $dbh     = $config->GetDBH();

	my $group   = $context->GetGroup();
	my $cur_uri = $context->GetURI();

	my $teams   = $self->GetTeamAccessListOnURI($dbh, $session->{Magellan}->{AccessRights}->{uri_id});
	
	
  my $uri = new Mioga2::URI($config, group  => $group->GetIdent(),
	                                   public => 0,
	                                   application => 'Select',
	                                   method => 'SelectMultipleTeamsMemberOfGroup',
	                                   args   => { action  => 'back_add_team',
	                                               referer => $cur_uri->GetURI(),
												   rowids  => join(',', map {$_->{team_id}} @$teams),
											     },
	                                   );
	
	my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	$content->SetContent($uri->GetURI());

	return $content;
}


# ============================================================================

=head2 ParentAccessRights ()

	Redirect to access right of parent uri.

=cut

# ============================================================================

sub ParentAccessRights {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $referer;

	if(exists $context->{args}->{referer}) {
		$referer =  $context->{args}->{referer};
	}
	else {
		$referer =  $context->GetReferer();
	}


	my $root_uri;
	if(exists $context->{args}->{user_id}) {
		$root_uri = $self->GetParentURIForUser($config, $context->{args}->{uri}, $context->{args}->{user_id});
	} 
	elsif(exists $context->{args}->{team_id}) {
		$root_uri = $self->GetParentURIForTeam($config, $context->{args}->{uri}, $context->{args}->{team_id});
	} 
	elsif(exists $context->{args}->{profile_id}) {
		$root_uri = $self->GetParentURIForProfile($config, $context->{args}->{uri}, $context->{args}->{profile_id});
	} 


	my $method;
	if($context->GetReferer() =~ /ViewAccessRights/) {
		$method = "ViewAccessRights";
	}
	else {
		$method = "AccessRights";
	}

	my $uri = new Mioga2::URI($config, group => $context->GetGroup()->GetIdent(),
							           public => 0,
							           application => 'Magellan',
							           method => $method, 
							           args => { 'uri' => $root_uri,
											     'referer' => $referer },
							  );

	my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	$content->SetContent($uri->GetURI());
	
	return $content;
}


# ============================================================================
# CreateFile ($context, $url, $filename)
#
#    Create a new empty file
#
# ============================================================================

sub CreateFile {
	my ($self, $context, $url, $filename) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $group = $context->GetGroup();
	my $user_id = $user->GetRowid();
	my $group_ident = $group->GetIdent();
	my $dbh = $config->GetDBH();

	my $session_token = $context->GetUserAuth ();

	my $result = Mioga2::Magellan::DAV::MakeResource ($session_token, $url."/".$filename);

	return $result;
}


# ============================================================================
# CreateDir ($context, $url, $dirname)
#
#    Create a new directory
#
# ============================================================================

sub CreateDir {
	my ($self, $context, $url, $dirname) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $group = $context->GetGroup();
	my $user_id = $user->GetRowid();
	my $group_ident = $group->GetIdent();
	my $dbh = $config->GetDBH();

	my $session_token = $context->GetUserAuth ();

	my $result = Mioga2::Magellan::DAV::MakeCollection ($session_token, $url."/".$dirname);
	return $result;
}


# ============================================================================
# DownloadFile ($context, $url, $filename)
#
# submited file to the $url directory
#	if something goes wrong, return undef
#
# ============================================================================

sub DownloadFile {
	my ($self, $context, $url, $filename) = @_;
	print STDERR "DownloadFile url = $url\n" if ($debug);

	my $fh = $context->GetUploadFH();
	my $size = $context->GetUploadSize;
	my $session_token = $context->GetUserAuth ();

	print STDERR "filename = $filename\n" if ($debug);

	my $data = {};

	if (not (my $result = Mioga2::Magellan::DAV::PutURIForFH ($session_token, $fh, $url."/".$filename, "unknown/unknown", $size))) {
		# Everything went OK, generate filepath
		my $config = $context->GetConfig();
		my $base_url = $config->GetDAVProtocol()."://".$config->GetDAVHost().":" . $config->GetDAVPort .$config->GetBaseURI();
		print STDERR "   base_url = $base_url\n" if ($debug);
		my $file_path = $url."/".$filename;
		print STDERR "   file_path 1 = $file_path\n" if ($debug);
		$file_path =~ s/$base_url//;
		print STDERR "   file_path 2 = $file_path\n" if ($debug);
		$file_path = $config->GetWebdavDir().$file_path;
		print STDERR "   file_path 3 = $file_path\n" if ($debug);

		my $arch = new Mioga2::Magellan::Archive ();
		my $arch_type = $arch->Type($file_path);

		print STDERR "arch_type = $arch_type\n" if ($debug);
		if (defined($arch_type)) {
			$data->{archive_path} = $file_path;
		}
	}
	else {
		# Something wrong happened, report it
		$data->{code} = $result;
	}

	return ($data);
}


#===============================================================================

=head2 SetPreferences

Set application preferences

=cut

#===============================================================================
sub SetPreferences {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Magellan::SetPreferences] Entering\n" if ($debug);

    my $session = $context->GetSession ();
	
	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
				[ ['view_mode' ], 'allow_empty', [ 'match', "^\(icon|list\)\$" ] ]
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		throw Mioga2::Exception::Application ("Mioga2::Magellan::SetPreferences", __("Wrong argument value."))
	}

	for (qw/view_mode/) {
		if ($values->{$_}) {
			$session->{magellan_prefs}->{$_} = $values->{$_};
		}
	}
    $context->{raw_session}->save;

	return ("OK");
}	# ----------  end of subroutine SetPreferences  ----------


#===============================================================================

=head2 EnableHistory

Enable history for a resource

=head3 Incoming Arguments

=over

=item I<resource>: The path to URI to enable history for

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		success: 'true' / 'false',
		message: ...
	}

=back

=cut

#===============================================================================
sub EnableHistory {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Magellan::EnableHistory] Entering, args: " . Dumper $context->{args} if ($debug);

	my $config = $context->GetConfig ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['resource'], 'disallow_empty', ['location', $config] ],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = {
		success => 'false',
		message => __('History enable failed')
	};

	if (!@$errors) {
		my $db = $config->GetDBObject ();
		try {
			$db->BeginTransaction ();

			# Ensure group is allowed to use history
			my $uri = Mioga2::URI->new ($config, uri => $values->{resource});
			my $group = Mioga2::GroupList->new ($config, { attributes => { ident => $uri->GetGroupIdent () } });
			if (!$group->Get ('history')) {
				throw Mioga2::Exception::Application ('Mioga2::Magellan::EnableHistory', 'Group is not allowed to use history');
			}

			$db->ExecSQL ("UPDATE m_uri SET history = 't' WHERE rowid IN (WITH RECURSIVE uri (rowid, uri, directory_id) AS (SELECT rowid, uri, directory_id FROM m_uri WHERE uri = ? UNION SELECT m_uri.rowid, m_uri.uri, m_uri.directory_id FROM uri, m_uri where m_uri.directory_id = uri.rowid) SELECT rowid FROM uri);", [$values->{resource}]);

			$db->EndTransaction ();

			$data->{success} = 'true';
			$data->{message} = __('History successfully enabled.');
		}
		otherwise {
			$db->RollbackTransaction ();
		};
	}

	print STDERR "[Mioga2::Magellan::EnableHistory] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine EnableHistory  ----------


#===============================================================================

=head2 DisableHistory

Disable history for a resource

=head3 Incoming Arguments

=over

=item I<resource>: The path to URI to disable history for

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	{
		success: 'true' / 'false',
		message: ...
	}

=back

=cut

#===============================================================================
sub DisableHistory {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Magellan::EnableHistory] Entering, args: " . Dumper $context->{args} if ($debug);

	my $config = $context->GetConfig ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [	
				[ ['resource'], 'disallow_empty', ['location', $config] ],
			];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $data = {
		success => 'false',
		message => __('History disable failed')
	};

	if (!@$errors) {
		my $db = $config->GetDBObject ();
		try {
			$db->BeginTransaction ();

			my $uris = $db->SelectMultiple ('WITH RECURSIVE uri (rowid, uri, directory_id) AS (SELECT rowid, uri, directory_id FROM m_uri WHERE uri = ? UNION SELECT m_uri.rowid, m_uri.uri, m_uri.directory_id FROM uri, m_uri where m_uri.directory_id = uri.rowid) SELECT rowid, uri FROM uri;', [$values->{resource}]);
			$db->ExecSQL ("UPDATE m_uri SET history = 'f' WHERE rowid IN (" . join (', ', map { $_->{rowid} } @$uris) . ");");
			for my $uri (@$uris) {
				Mioga2::DAVFS::historyDelete ({}, $config, $uri->{uri});
			}

			$db->EndTransaction ();

			$data->{success} = 'true';
			$data->{message} = __('History successfully disabled.');
		}
		otherwise {
			$db->RollbackTransaction ();
		};
	}

	print STDERR "[Mioga2::Magellan::EnableHistory] Leaving, data: " . Dumper $data if ($debug);
	return ($data);
}	# ----------  end of subroutine DisableHistory  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================
# FileExists ($context, $url)
#
#    return 1 if file/directory $uri exists
#    else return 0 
#
# ============================================================================
sub FileExists
{
	my ($self, $context, $url) = @_;
	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $user = $context->GetUser();
	my $user_id = $user->GetRowid();
	my $session_token = $context->GetUserAuth();
	my $private_tmp_dir = $config->GetTmpDir();

	my $error = 'ok';

	my $properties = Mioga2::Magellan::DAV::GetProperties($context, $session_token, $url);

	if (defined($properties))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__

