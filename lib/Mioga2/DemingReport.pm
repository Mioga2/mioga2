#===============================================================================
#
#         FILE:  DemingReport.pm
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  21/05/2013 13:49
#===============================================================================
#
#  Copyright (c) year 2013, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME
	
DemingReport.pm

=head1 DESCRIPTION

DemingReport.pm: the Mioga2 activity-reporting application, reporting module.

=head1 METHODS DESRIPTION

=cut

#
#===============================================================================

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::DemingReport;
use base qw(Mioga2::Deming);
use strict;
use warnings;
use open ':encoding(utf8)';

use Data::Dumper;
use Locale::TextDomain::UTF8 'deming';
use Error qw(:try);

use Mioga2::GroupList;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::Content::XSLT;
use Mioga2::Exception::Simple;
use Mioga2::TaskCategory;
use Mioga2::XML::Simple;
use Mioga2::CSV;

my $debug = 0;


#===============================================================================

=head1 INTERFACE METHODS

=cut

#===============================================================================

=head2 DisplayMain

Display main UI

=head3 Generated XML

=over

	<DisplayReport>
		<miogacontext>See Mioga2::RequestContext</miogacontext>
	</DisplayReport>

=back

=cut

# ============================================================================
sub DisplayMain {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Deming::DisplayMain]\n" if ($debug);

	my $data = {
		DisplayReport => $context->GetContext (),
	};

	# Get instance groups
	my $grouplist = Mioga2::GroupList->new ($context->GetConfig ());
	my @groups = $grouplist->GetGroups ();
	$data->{DisplayReport}->{Groups} = \@groups;

	# Get activities
	$data->{DisplayReport}->{Activities} = $self->GetActivities ($context)->{activities};

	# Get external sources configuration (system-wide)
	my %external_sources_conf;
	my $confdir = $self->{config}->GetInstallPath () . '/conf';
	if (-e "$confdir/deming_conf.xml") {
		# External activities may be used
		print STDERR "[Mioga2::Deming::GetActivities] Loading external sources from $confdir/deming_conf.xml\n" if ($debug);
		try {
			my $enabled_sources = $self->{db}->SelectMultiple ('SELECT task_category.*, activity_report_source.ident AS source_ident FROM task_category, activity_report_source WHERE activity_report_source.task_category_id = task_category.rowid AND task_category.mioga_id = ?;', [$context->GetConfig ()->GetMiogaId ()]);
			%external_sources_conf = %{$self->ParseExternalConf ("$confdir/deming_conf.xml")};
			$data->{DisplayReport}->{ExternalSource} = $enabled_sources;
		}
		otherwise {
			my $err = shift;
			print STDERR "[Mioga2::Deming::GetActivities] External source configuration is invalid, please fix: " . $err->stringify ();
		};
	}

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "[Mioga2::Deming::DisplayMain] XML: $xml\n" if ($debug);

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'deming.xsl', locale_domain => 'deming_xsl');
	$content->SetContent($xml);
	return $content;
}


#===============================================================================

=head2 GetEntries

Returns report entries

=head3 Incoming Arguments

=over

=item I<start>: the date range start

=item I<end>: the date range end

=back

=head3 Return value

=over

A Perl data structure to be converted to XML or JSON:
	{
		success: true/false,
		entries: [
			{
				rowid: ...,
				activity: ...,
				dtstart: ...,
				dtend: ...,
				notes: ...
			}
		]
	}

=back

=cut

#===============================================================================
sub GetEntries {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Deming::GetEntries] Entering, args: " . Dumper $context->{args} if ($debug);

	# Default return value
	my $data = {
		success => 'boolean::false',
		message => __('Failed to fetch entries'),
	};


	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'activities' ], 'allow_empty' ],
			[ [ 'grouping' ], 'allow_empty', ['match', "\(user|activity|sub_activity|sub_sub_activity\)"] ],
			[ [ 'start', 'end' ], 'disallow_empty', 'datetime' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (!@$errors) {
		try {
			# Initialize sub-request to join activity_report to external sources
			my $ext_sql;
			my %external_sources_conf;
			my $confdir = $self->{config}->GetInstallPath () . '/conf';
			if (-e "$confdir/deming_conf.xml") {
				# External activities may be used
				print STDERR "[Mioga2::Deming::GetActivities] Loading external sources from $confdir/deming_conf.xml\n" if ($debug);
				%external_sources_conf = %{$self->ParseExternalConf ("$confdir/deming_conf.xml")};
				my @views;
				for my $src (keys (%external_sources_conf)) {
					push (@views, "SELECT * FROM ${src}_activity_report");
				}

				if (@views) {
					$ext_sql = 'LEFT JOIN (' . join (' UNION ', @views) . ') AS T1 ON T1.rowid = activity_report.rowid';
				}
			}

			# Merge sub-request into main request
			my $sql;
			my @args;
			my @parts;
			my @group;
			my $raw = (defined ($values->{raw}) && ($values->{raw} == 1)) ? 1 : 0;
			if (!defined ($ext_sql)) {
				$sql = 'SELECT user_id, activity_id AS activity, ' . ($raw ? 'dtstart, dtend' : 'sum (dtend - dtstart) AS duration') . ' FROM activity_report;';
	#			unless ($raw) {
	#				if ($values->{grouping} eq 'activity') {
	#					push (@group, 'activity_report.user_id', 'activity');
	#				}
	#				elsif ($values->{grouping} eq 'sub_activity') {
	#					push (@group, 'activity_report.user_id', 'activity', 'sub_activity');
	#				}
	#				elsif ($values->{grouping} eq 'sub_activity') {
	#					push (@group, 'activity_report.user_id', 'activity', 'sub_activity', 'sub_sub_activity');
	#				}
	#				else {
	#					push (@group, 'activity_report.user_id');
	#				}
	#			}
			}
			else {
				#if ($raw) {
					$sql = "SELECT activity_report.user_id, activity_report.activity_id AS activity, T1.cat_id AS sub_activity, T1.subcat_id AS sub_sub_activity, activity_report.dtstart, activity_report.dtend, (activity_report.dtend-activity_report.dtstart) as duration, activity_report.notes FROM activity_report $ext_sql";
				#}
				#else {

=head2

					if ($values->{grouping} eq 'activity') {
						$sql = "SELECT activity_report.user_id, activity_report.activity_id AS activity, sum (activity_report.dtend - activity_report.dtstart) AS duration FROM activity_report $ext_sql";
						push (@group, 'activity_report.user_id', 'activity');
					}
					elsif ($values->{grouping} eq 'sub_activity') {
						$sql = "SELECT activity_report.user_id, activity_report.activity_id AS activity, T1.cat_id AS sub_activity, sum (activity_report.dtend - activity_report.dtstart) AS duration FROM activity_report $ext_sql";
						push (@group, 'activity_report.user_id', 'activity', 'sub_activity');
					}
					elsif ($values->{grouping} eq 'sub_sub_activity') {
						$sql = "SELECT activity_report.user_id, activity_report.activity_id AS activity, T1.cat_id AS sub_activity, T1.subcat_id AS sub_sub_activity, sum (activity_report.dtend - activity_report.dtstart) AS duration FROM activity_report $ext_sql";
						push (@group, 'activity_report.user_id', 'activity', 'sub_activity', 'sub_sub_activity');
					}
					else {
						$sql = "SELECT activity_report.user_id, sum (activity_report.dtend - activity_report.dtstart) AS duration FROM activity_report $ext_sql";
						push (@group, 'activity_report.user_id');
					}

=cut

				#}
			}
				my $userlist = $context->GetGroup()->GetExpandedUserList();
				my @users = @{ac_ForceArray ($userlist->Get ('rowid'))};
				push (@parts, 'activity_report.user_id IN (' . join (', ', ('?') x @users) . ')');
				push (@args, @users);
			if (st_ArgExists ($context, 'activities') && defined ($ext_sql)) {
				my @subparts;
				for my $activity (@{ac_ForceArray ($values->{activities})}) {
					my @activity_details = split ('-', $activity);
					my $sub_sql = '';
					if (defined ($activity_details[0])) {
						$sub_sql .= 'activity_report.activity_id = ?';
						push (@args, $activity_details[0]);
					}
					if (defined ($activity_details[1])) {
						$sub_sql .= ' AND T1.cat_id = ?';
						push (@args, $activity_details[1]);
					}
					if (defined ($activity_details[2])) {
						$sub_sql .= ' AND T1.subcat_id = ?';
						push (@args, $activity_details[2]);
					}
					push (@subparts, $sub_sql);
				}
				push (@parts, '(' . join (' OR ', @subparts) . ')');
			}
			if (st_ArgExists ($context, 'start') && st_ArgExists ($context, 'end')) {
				push (@parts, 'activity_report.dtstart < ? AND activity_report.dtend > ?');
				push (@args, $values->{end}, $values->{start});
			}
			# Merge all parts into request
			$sql .= ' WHERE ' . join (' AND ', @parts) . " ORDER BY activity_report.dtstart";
	#		if (@group) {
	#			$sql .= ' GROUP BY ' . join (', ', @group);
	#		}

			print STDERR "[Mioga2::Deming::GetEntries] SQL: $sql\n" if ($debug);
			print STDERR "[Mioga2::Deming::GetEntries] Args: " . Dumper \@args if ($debug);

			$data->{entries} = $self->{db}->SelectMultiple ($sql, \@args);

			delete ($data->{message});
			$data->{success} = 'boolean::true';
		}
		otherwise {
			my $err = shift;
			print STDERR "[Mioga2::Deming::GetEntries] Got errors: " . $err->stringify ();

			$data->{success} = 'boolean::false';
			$data->{entries} = [ ];
		};
	}
	else {
		print STDERR '[Mioga2::DemingReport::GetEntries] Got errors: ' . Dumper $errors;
		$data->{errors} = [ ];
		for my $error (@$errors) {
			push (@{$data->{errors}}, {
				field => $error->[0],
				message => __($error->[1]->[0])
			})
		}
	}

	print STDERR "[Mioga2::Deming::GetEntries] Leaving, data: " . Dumper ($data) if ($debug);
	return ($data);
}	# ----------  end of subroutine GetEntries  ----------


#===============================================================================

=head2 ExportEntries

Export entries to CSV

=head3 Incoming Arguments

=over

See GetEntries

=back

=head3 Return value

=over

A CSV table

=back

=cut

#===============================================================================
sub ExportEntries {
	my ($self, $context) = @_;

	# Get entries
	my $data = $self->GetEntries ($context);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'raw' ], 'disallow_empty', 'want_int' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	# Format data
	my $columns = [];
	my @users = $self->{group}->GetExpandedUserList ()->GetUsers ();
	my @activities = @{$self->GetActivities ($context)->{activities}};
	my $entries = [ ];
	for my $entry (@{$data->{entries}}) {
		my @columns;

		# Change user rowid to user label
		my @matching = grep { $_->{rowid} == $entry->{user_id} } @users;
		my $user = $matching[0];
		next unless ($user);	# Should not happen
		$entry->{user} = $user->{firstname} . ' ' . $user->{lastname};
		delete ($entry->{user_id});
		push (@columns, 'user');

		# Change activity ids to labels
		@matching = grep { $_->{rowid} == $entry->{activity} } @activities;
		my $activity = $matching[0];
		next unless ($activity);	# Should not happen
		$entry->{activity} = $activity->{name};
		push (@columns, 'activity');

		# Change sub-activity ids to labels
		if (defined ($entry->{sub_activity})) {
			my @matching = grep { $_->{rowid} == $entry->{sub_activity} } @{$activity->{children}};
			my $sub_activity = $matching[0];
			next unless ($sub_activity);	# Should not happen
			$entry->{sub_activity} = $sub_activity->{name};
			push (@columns, 'sub_activity');

			# Change sub-sub-activity ids to labels
			if (defined ($entry->{sub_sub_activity})) {
				my @matching = grep { $_->{rowid} == $entry->{sub_sub_activity} } @{$sub_activity->{children}};
				my $sub_sub_activity = $matching[0];
				next unless ($sub_sub_activity);	# Should not happen
				$entry->{sub_sub_activity} = $sub_sub_activity->{name};
				push (@columns, 'sub_sub_activity');
			}
		}

		if ((defined ($values->{raw})) && ($values->{raw} == 1)) {
			push (@columns, 'dtstart', 'dtend', 'duration');
		}
		else {
			# Format duration in days
			my @parts = ($entry->{duration} =~ m/(\d+):(\d+):(\d+)/);
			$entry->{duration} = ($parts[0]*3600 + $parts[1]*60 + $parts[2]) / ($values->{day_duration}*3600);
			push (@columns, 'duration');
		}

		# Set column list if more columns for current entry (activity having sub-activities) than previous ones (activities having no sub-activity)
		$columns = \@columns unless (@$columns > @columns);

		push (@$entries, $entry);
	}
	print STDERR "[Mioga2::DemingReport::ExportEntries] Entries: " . Dumper ($entries) if ($debug);


	# Convert entries to CSV
	my $converter = Mioga2::CSV->new (
		columns   => $columns,
		separator => ';',
		quote     => '"'
	);
	my $csv = $converter->Convert ($entries);
	print STDERR "[Mioga2::DemingReport::ExportEntries] CSV: $csv\n" if ($debug);

	# Set response contents
	my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/csv', filename => 'deming-report.csv');
	$content->SetContent ($csv);

	return ($content);
}	# ----------  end of subroutine ExportEntries  ----------


#===============================================================================

=head2 GetUsers

Get instance users

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:

	[
		{
			rowid: ...,
			ident: ...,
			firstname: ...,
			lastname: ...,
			email: ...
		},
		...
	]

=back

=cut

#===============================================================================
sub GetUsers {
	my ($self, $context) = @_;

	my $data = {
		success => 'boolean::true'
	};

	#my $userlist = Mioga2::UserList->new ($context->GetConfig (), { short_list => 1 });
	my $userlist = $context->GetGroup()->GetExpandedUserList();
	@{$data->{users}} = $userlist->GetUsers ();

	return ($data);
}	# ----------  end of subroutine GetUsers  ----------


# ============================================================================


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut
# ============================================================================


#===============================================================================

=head2 InitApp

Initialize application

=cut

#===============================================================================
sub InitApp {
	my ($self, $context) = @_;

	$self->{group} = $context->GetGroup ();
	$self->{db} = $context->GetConfig ()->GetDBObject ();
}	# ----------  end of subroutine InitApp  ----------

# ============================================================================
# GetAppDesc ()
# ============================================================================

sub GetAppDesc {
	my ($self, $context) = @_;
    my %AppDesc = ( 
    	ident   => 'DemingReport',
	    name	=> __('Deming reporting'),
        package => 'Mioga2::DemingReport',
		type => 'normal',
        description => __('The Mioga2 activity-reporting application, reporting module'),
        api_version => '2.4',
		is_group    => 1,
		is_user     => 0,
		is_resource => 0,
        all_groups  => 0,
        all_users  => 0,
        can_be_public => 0,
        usable_by_resource => 0,

        functions   => { 
			'Use' => __('View activity reports'),
		},
				
        func_methods  => { 
			Use => [ 'DisplayMain', 'GetEntries', 'GetActivities', 'ExportEntries', 'GetUsers' ],
		},
		public_methods => [ ],
		func_mioglets => { },
		public_mioglets => [  ],
		non_sensitive_methods => [
			'DisplayMain',
			'GetEntries',
			'GetActivities',
			'ExportEntries',
			'GetUsers'
		],
    );
	return \%AppDesc;
}





# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2011, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;

