# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
URI.pm: Access class to the Mioga2 URIs.

=head1 DESCRIPTION

This module permit simple check and access to Mioga URIs.

A Mioga2 application URI is build like this :

/<BASE>/<MIOGA_IDENT>/<DOMAIN>/<GROUP>/<APPIDENT>/<METHOD>?<ARGS>

A Mioga2 WebDAV URI is build like this :

/<BASE>/<MIOGA_IDENT>/<DOMAIN>/<GROUP>/<PATH_TO_FILE>


<BASE> is the value of the "base_uri" configuration parameter.

<DOMAIN> is the value of  "private_uri", "public_uri", "bin_uri" or "pubbin_uri"
configuration parameters. These parameters are used to identify public/private 
space and application/webdav requests.

=head1 METHODS DESRIPTION

=cut

# ============================================================================

package Mioga2::URI;
use strict;

use Locale::TextDomain::UTF8 'uri';

use Error qw(:try);
use Data::Dumper;
use URI::Escape;

use Mioga2::MiogaConf;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;
use Mioga2::Old::Group;
use Mioga2::Old::User;

my $debug = 0;


# ============================================================================

=head2 new ($config, $param)

	Create the object and initialize from config and parameters.

	return a new Mioga2::URI object.
	
	
	The three creation modes are :
	
=over 4

=item create from an existing uri.
	
	new Mioga2::URI ($config, uri => "/Mioga2/Mioga/...")

=item create a webdav uri from parameters

	new Mioga2::URI ($config, group  => $group_ident,
	                          public => (1|0),
	                          path   => $file_path)

	All parameters are required 

=item create an application uri from parameters

	new Mioga2::URI ($config, group       => $group_ident,
	                          public      => (1|0),
	                          application => $app_ident,
	                          method      => $method_ident,
	                          args        => $args)

	All parameters are required except args.

	args can by a string or an hashref like
	{
		arg_name1 => arg_value1,
		arg_name2 => [arg2_val1, arg2_val2, ...],
		...
	}
	
=back

=cut

# ============================================================================

sub new {
	my ($class, $config, %param) = @_;

	my $self = {};
	$self->{miogaconf} = $config;

	bless($self, $class);
	
	if(exists $param{uri}) {
		$param{uri} =~ s!/$!!;
		$param{uri} =~ s!//!/!g;

		$self->{uri} = $param{uri};

		# Domain may not be present in some cases (when authenticating before a redirect)
		if (exists ($param{missing_domain})) {
			$self->{missing_domain} = $param{missing_domain};
		}
		else {
			$self->{missing_domain} = 0;
		}

		$self->CheckURI();
	}
	elsif (exists $param{rowid}) {
		$self->{uri} = SelectSingle ($config->GetDBH(), "SELECT uri FROM m_uri WHERE rowid=$param{rowid}")->{uri};
		$self->CheckURI ();
	}
	elsif(exists $param{path}) {
		$self->BuildWebDavURI(\%param);
	}
	elsif(exists $param{application}) {
		$self->BuildApplicationURI(\%param);
	}
	else {
		throw Mioga2::Exception::Simple("Mioga2::URI::new",
										__"Missing parameters uri, webdav or application.");
	}

	return $self;
}
# ============================================================================

=head2 GetURI ()

	return the uri.
	
=cut

# ============================================================================
sub GetURI {
	my ($self) = @_;
	
	return $self->{uri};
}

# ============================================================================

=head2 GetRealPath ()

	return the file path if it is a file URI.
	
=cut

# ============================================================================
sub GetRealPath {
	my ($self) = @_;
	if ($self->{is_private_webdav} or $self->{is_public_webdav}) {
		return $self->{real_path};
	}
	return undef;
}

# ============================================================================

=head2 GetData ($config)

  return a data hash for current URI.
  
=cut

# ============================================================================

sub GetData {
  my ($self, $config) = @_;
  
  my $base_uri    = $config->GetBaseURI();
  my $bin_uri     = $config->GetBinURI();
  my $pubbin_uri  = $config->GetPubBinURI();
  my $private_uri = $config->GetPrivateURI();
  my $public_uri  = $config->GetPublicURI();
  my $sxml_uri    = $config->GetXMLURI();
  my $xml_uri     = $config->GetPubXMLURI();
  my $pubxml_uri  = $config->GetSXMLURI();
  my $image_uri   = $config->GetImageURI();
  my $help_uri    = $config->GetHelpURI();
  my $cdn_uri     = $config->GetCDNURI();

  my $data  = {
    base            => $base_uri,
    image           => $image_uri,
    help            => $help_uri,
	cdn_uri         => $cdn_uri,
    bin_uri         => $bin_uri,
    pubbin_uri      => $pubbin_uri,
    xml_uri         => $xml_uri,
    sxml_uri        => $sxml_uri,
    pubxml_uri      => $pubxml_uri,
    public_dav_uri  => $public_uri,
    private_dav_uri => $private_uri,
    public_dav      => "$public_uri/$self->{group_ident}",
    private_dav     => "$private_uri/$self->{group_ident}",
    private_bin     => "$bin_uri/$self->{group_ident}",
    public_bin      => "$pubbin_uri/$self->{group_ident}",
    sxml            => "$sxml_uri/$self->{group_ident}",
    xml             => "$xml_uri/$self->{group_ident}",
    pubxml          => "$pubxml_uri/$self->{group_ident}",
  };

  return $data;
}

# ============================================================================

=head2 GetXML ($config)

	return an XML representation of the current URI.
	
=cut

# ============================================================================

sub GetXML {
	my ($self, $config) = @_;
	
	my $base_uri = $config->GetBaseURI();
	my $bin_uri = $config->GetBinURI();
	my $pubbin_uri = $config->GetPubBinURI();
	my $private_uri = $config->GetPrivateURI();
	my $public_uri  = $config->GetPublicURI();
	my $sxml_uri  = $config->GetXMLURI();
	my $xml_uri  = $config->GetPubXMLURI();
	my $pubxml_uri  = $config->GetSXMLURI();
	my $image_uri  = $config->GetImageURI();
	my $help_uri  = $config->GetHelpURI();
	my $logout_uri  = Mioga2::Login::GetLogoutURI ($config->GetMiogaConf ());

    my $xml = "<uri";
	$xml   .= qq| base="$base_uri"|;
	#$xml   .= qq| image="$image_uri/| . st_FormatXMLString($self->{group_ident}) . '"'; #';
	$xml   .= qq| image="$image_uri"|;
	#$xml   .= qq| image="$image_uri"|;
	$xml   .= qq| help="$help_uri"|;

	$xml   .= qq| bin_uri="$bin_uri"|;
	$xml   .= qq| pubbin_uri="$pubbin_uri"|;
	$xml   .= qq| xml_uri="$xml_uri"|;
	$xml   .= qq| sxml_uri="$sxml_uri"|;
	$xml   .= qq| pubxml_uri="$pubxml_uri"|;
	$xml   .= qq| public_dav_uri="$public_uri"|; 
	$xml   .= qq| private_dav_uri="$private_uri"|;
	
	$xml   .= qq| public_dav="$public_uri/|    . st_FormatXMLString($self->{group_ident}) . '"'; #'
	$xml   .= qq| private_dav="$private_uri/|  . st_FormatXMLString($self->{group_ident}) . '"'; #'
	$xml   .= qq| private_bin="$bin_uri/| . st_FormatXMLString($self->{group_ident}) . '"'; #'
	$xml   .= qq| public_bin="$pubbin_uri/|   . st_FormatXMLString($self->{group_ident}) . '"'; #'
	$xml   .= qq| sxml="$sxml_uri/|   . st_FormatXMLString($self->{group_ident}) . '"'; #'
	$xml   .= qq| xml="$xml_uri/|   . st_FormatXMLString($self->{group_ident}) . '"'; #'
	$xml   .= qq| pubxml="$pubxml_uri/|   . st_FormatXMLString($self->{group_ident}) . '"'; #'
	$xml   .= qq| logout="| . st_FormatXMLString ($logout_uri) . '"';
	
	$xml .= "/>";

	return $xml;
}



# ============================================================================

=head2 IsPublic ()

	return true if the uri is public.
	
=cut

# ============================================================================
sub IsPublic {
	my ($self) = @_;

	if($self->{is_public_webdav} or $self->{is_public_application}) {
		return 1;
	}

	return 0;
}



# ============================================================================

=head2 IsPrivate ()

	return true if the uri is private.
	
=cut

# ============================================================================
sub IsPrivate {
	my ($self) = @_;

	if($self->{is_private_webdav} or $self->{is_private_application}) {
		return 1;
	}

	return 0;
}



# ============================================================================

=head2 IsRootWebDav ()

	return true if the uri is for webdav.
	
=cut

# ============================================================================
sub IsRootWebDav {
	my ($self) = @_;

	if($self->{is_root_webdav}) {
		return 1;
	}

	return 0;
}


# ============================================================================

=head2 IsWebDav ()

	return true if the uri is for webdav.
	
=cut

# ============================================================================
sub IsWebDav {
	my ($self) = @_;

	if($self->{is_public_webdav} or $self->{is_private_webdav}) {
		return 1;
	}

	return 0;
}



# ============================================================================

=head2 IsApplication ()

	return true if the uri is for applications.
	
=cut

# ============================================================================
sub IsApplication {
	my ($self) = @_;

	if( $self->{is_public_application} or $self->{is_private_application}) {
		return 1;
	}

	return 0;
}



# ============================================================================

=head2 IsXML ()

	return true if the uri is for XML transaction.
	
=cut

# ============================================================================
sub IsXML {
	my ($self) = @_;

	if( $self->{is_public_xml} or $self->{is_private_xml}) {
		return 1;
	}

	return 0;
}



# ============================================================================

=head2 IsSXML ()

	return true if the uri is for secure XML transaction.
	
=cut

# ============================================================================
sub IsSXML {
	my ($self) = @_;

	return $self->{is_secure_xml};
}


#===============================================================================

=head2 IsLogin

Return true if URI is a HTML login handler

=cut

#===============================================================================
sub IsLogin {
	my ($self) = @_;

	return ($self->{is_login});
}	# ----------  end of subroutine IsLogin  ----------



# ============================================================================

=head2 GetMiogaIdent ()

	return the MiogaIdent.
	
=cut

# ============================================================================
sub GetMiogaIdent {
	my ($self) = @_;

	return $self->{mioga_ident};
}

# ============================================================================

=head2 GetMiogaId ()

    return the Mioga id.
    
=cut

# ============================================================================
sub GetMiogaId {
    my ($self) = @_;

    my $id  = SelectSingle($self->{miogaconf}->GetDBH, "SELECT rowid FROM m_mioga WHERE ident = '" . st_FormatPostgreSQLString($self->GetMiogaIdent) . "'")->{rowid};
    return $id;
}

# ============================================================================

=head2 GetBase ()

	return the base file path (/BASE/MIOGA/DOMAIN/GROUPIDENT).
	
=cut

# ============================================================================
sub GetBase {
	my ($self) = @_;

	return $self->{base};
}

# ============================================================================
=head2 GetParent ()

	Returns the parent URI as a string.
	If the parent URI would be beyond GetBase(), returns undef.
	
=cut

sub GetParent {
	my ($self) = @_;
	(my $parent = $self->{uri}) =~ s#^(.*)/[^/]*$#$1#;
	if (length($parent) < length($self->{base})) {
		return undef;
	} else {
		return $parent;
	}
}

# ============================================================================

=head2 GetWebDavPath ()

	return the webdav file path.
	
=cut

# ============================================================================
sub GetWebDavPath {
	my ($self) = @_;

	if(! $self->IsWebDav()) {
		throw Mioga2::Exception::Simple("Mioga2::URI::GetWebDavPath", __"Not a Webdav URL.");
	}

	return $self->{webdav_path};
}



# ============================================================================

=head2 GetGroupIdent ()

	return true if the group ident.
	
=cut

# ============================================================================
sub GetGroupIdent {
	my ($self) = @_;

	return $self->{group_ident};
}

# ============================================================================

=head2 GetGroup ()

    return a group object.
    
=cut

# ============================================================================
sub GetGroup {
    my ($self) = @_;


    if (exists($self->{group_ident}) && defined($self->{group_ident}) && !$self->{group}) {
    	if (ref($self->{miogaconf}) eq 'Mioga2::Config') {
	        $self->{config} = Mioga2::Config->new($self->{miogaconf}->GetMiogaConf, $self->GetMiogaIdent) if !$self->{config};
    	}
    	else {
  	      $self->{config} = Mioga2::Config->new($self->{miogaconf}, $self->GetMiogaIdent) if !$self->{config};
    	}
        try {
            $self->{group}  = Mioga2::Old::Group->new($self->{config}, ident => $self->GetGroupIdent);
        }
        otherwise {
            $self->{group}  = Mioga2::Old::User->new($self->{config}, ident => $self->GetGroupIdent);
        };
    }
    return $self->{group};
}

# ============================================================================

=head2 GetAppIdent ()

	return true if the application ident.
	
=cut

# ============================================================================
sub GetAppIdent {
	my ($self) = @_;

	if(! $self->IsApplication()) {
		throw Mioga2::Exception::Simple("Mioga2::URI::GetAppIdent", __"Not an application URI.");
	}

	return $self->{app_ident};
}



# ============================================================================

=head2 GetMethod ()

	return true if the application method.
	
=cut

# ============================================================================
sub GetMethod {
	my ($self) = @_;

	if(! $self->IsApplication() && ! $self->IsLogin ()) {
		throw Mioga2::Exception::Simple("Mioga2::URI::GetMethod", __"Not an application URI.");
	}

	return $self->{method};
}



# ============================================================================

=head2 GetArgs ()

	return true if the application method.
	
=cut

# ============================================================================
sub GetArgs {
	my ($self) = @_;

	if(exists $self->{args}) {
		return $self->{args};
	}

	return "";
}



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 BuildWebDavURI ($params)
	
	create a webdav URI.
	
	$params is a hashref like :
	{
		group  => $group_ident,
		public => (1|0),
		path   => $file_path,
	}

    All parameters are required.
	
=cut

# ============================================================================
sub BuildWebDavURI {
	my ($self, $param) = @_;

	if(!exists $param->{group}) {
		throw Mioga2::Exception::Simple("Mioga2::URI::BuildWebDavURI", 
										__"Missing group parameter");
	}

	if(!exists $param->{public}) {
		throw Mioga2::Exception::Simple("Mioga2::URI::BuildWebDavURI", 
										__"Missing public parameter");
	}

	if(!exists $param->{path}) {
		throw Mioga2::Exception::Simple("Mioga2::URI::BuildWebDavURI", 
										__"Missing path parameter");
	}

	my $uri;

	if($param->{public}) {
		$uri = $self->{miogaconf}->GetPublicURI;
	}
	else {
		$uri = $self->{miogaconf}->GetPrivateURI;
	}

	$uri .= "/".$param->{group};

	$uri .= $param->{path};

	$self->{uri} = $uri;
	$self->CheckURI();
}



# ============================================================================

=head2 BuildApplicationURI ($params)
	
	create a webdav URI.
	
	$params is a hashref like :
	{
		group  => $group_ident,
		public => (1|0),
		application => $app_ident,
		method      => $method_ident,
		args        => $args,
	}

    All parameters are required except args.

	args can by a string or an hashref like
	{
		arg_name1 => arg_value1,
		arg_name2 => [arg2_val1, arg2_val2, ...],
		...
	}
	
=cut

# ============================================================================
sub BuildApplicationURI {
	my ($self, $param) = @_;

	if(!exists $param->{public}) {
		throw Mioga2::Exception::Simple("Mioga2::URI::BuildApplicationURI",
										__"Missing public parameter");
	}

	if(!exists $param->{group} ) {
		throw Mioga2::Exception::Simple("Mioga2::URI::BuildApplicationURI", 
										__"Missing group parameter");
	}

	if(!exists $param->{application}) {
		throw Mioga2::Exception::Simple("Mioga2::URI::BuildApplicationURI", 
										__"Missing application parameter");
	}

	if(!exists $param->{method}) {
		throw Mioga2::Exception::Simple("Mioga2::URI::BuildApplicationURI",
										__"Missing method parameter");
	}


	my $uri;

	if($param->{public}) {
		$uri = $self->{miogaconf}->GetPubBinURI;
	}
	else {
		$uri = $self->{miogaconf}->GetBinURI;
	}
	
	$uri .= "/".$param->{group};
	$uri .= "/".$param->{application};
	$uri .= "/".$param->{method};

	my @args;
	if(exists $param->{args} and ref($param->{args}) eq 'HASH') {

		foreach my $key (keys %{$param->{args}}) {

			if(ref($param->{args}->{$key}) eq 'ARRAY') {

				foreach my $val (@{$param->{args}->{$key}}) {
					push @args, "$key=".uri_escape($val);
				}
			}
			else {
				push @args, "$key=".uri_escape($param->{args}->{$key});
			}
		}
		
		$self->{args} = join('&' , @args);
		if($self->{args} ne '') {
			$uri .= "?$self->{args}";
		}
	}
	elsif(exists $param->{args}) {
		$self->{args} = $param->{args};
		
		if($self->{args} ne '') {
			$uri .= "?$self->{args}";
		}
	}

	

	$self->{uri} = $uri;
	$self->CheckURI();
}



# ============================================================================

=head2 CheckURI ()

	Initialize URI object.
	Used by new.
	
=cut

# ============================================================================
sub CheckURI {
	my ($self) = @_;

	my $uri = $self->{uri};

	my $base_uri = $self->{miogaconf}->GetBasePath();

	# Check base URI
	if($base_uri ne '' and ! $uri =~ /^$base_uri([$|\/])/g ) {
		throw Mioga2::Exception::Simple("Mioga2::URI::CheckURI", 
        __x("URI does not begin with {pattern}.", pattern => $base_uri));
	}

	$uri =~ s/^$base_uri([$|\/])//;

	# Retrieve Mioga Ident
	my ($mioga_ident) = split(/[\/\?]/, $uri);

	if(! defined $mioga_ident) {
		throw Mioga2::Exception::Simple("Mioga2::URI::CheckURI", 
										__"Missing mioga ident in url");
	}

	$uri =~ s/^$mioga_ident($|\/)//;
	$self->{mioga_ident} = $mioga_ident;

	# Check domain URI
	my ($domain) = split(/\//, $uri);

	# Check if mioga_ident is the login handler
	if ('/' . $self->{mioga_ident} eq $self->{miogaconf}->GetLoginURI ()) {
		print STDERR "URI $self->{mioga_ident} is login handler\n" if ($debug);
		$domain = delete ($self->{mioga_ident});
		$uri =~ s/^$domain\?/\?/;
	}

	# Check if $uri is the user handler
	if ('/' . $uri eq $self->{miogaconf}->GetUserURI ()) {
		print STDERR "URI $uri is user handler\n" if ($debug);
		$domain = $domain;
	}

	if(!$self->{missing_domain} && ! defined $domain) {
		throw Mioga2::Exception::Simple("Mioga2::URI::CheckURI", 
										__"Missing domain (bin, pubbin, ...) in url");
	}

	$uri =~ s/^$domain($|\/)//;
	$domain = "/$domain";
	my ($group_ident) = split(/\//, $uri);

	#
	# TODO : MUST modify MiogaConf and Config to suppress one
	#
	my $private_dir;
	my $public_dir;
	if ($self->{miogaconf}->isa("Mioga2::Config")) {
		warn("URI config is Config\n") if ($debug);
		$private_dir = $self->{miogaconf}->GetPrivateDir();
		$public_dir = $self->{miogaconf}->GetPublicDir();
	}
	elsif ($self->{miogaconf}->isa("Mioga2::MiogaConf")) {
		warn("URI config is MiogaConf\n") if ($debug);
		$private_dir = $self->{miogaconf}->GetInstallDir() . "/$mioga_ident" . $self->{miogaconf}->GetBaseDir() . $self->{miogaconf}->GetPrivateDir();
		$public_dir = $self->{miogaconf}->GetInstallDir() . "/$mioga_ident" . $self->{miogaconf}->GetBaseDir() . $self->{miogaconf}->GetPublicDir();
	}

	$self->{is_public_webdav}  = 0;
	$self->{is_private_webdav} = 0;
	$self->{is_root_webdav} = 0;
	$self->{is_private_application} = 0;
	$self->{is_public_application}  = 0;
	$self->{is_private_xml} = 0;
	$self->{is_public_xml}  = 0;
	$self->{is_secure_xml}  = 0;
	$self->{is_webservice}  = 0;
	$self->{format}         = undef;

	if($domain eq $self->{miogaconf}->GetPubBinPath()) {
		$self->{is_public_application} = 1;
	}

	elsif($domain eq $self->{miogaconf}->GetBinPath()) {
		$self->{is_private_application} = 1;
	}

	elsif($domain eq $self->{miogaconf}->GetPrivatePath()) {
		$self->{is_private_webdav} = 1;
		$self->{real_path} = $private_dir. "/$uri";
		if(! defined $group_ident) {
			$self->{is_root_webdav} = 1;
		}
	}

	elsif($domain eq $self->{miogaconf}->GetPublicPath()) {
		$self->{is_public_webdav} = 1;
		$self->{real_path} = $public_dir . "/$uri";
		if(! defined $group_ident) {
			$self->{is_root_webdav} = 1;
		}
	}

	elsif($domain eq $self->{miogaconf}->GetPubXMLPath()) {
		$self->{is_public_xml} = 1;
		return;
	}

	elsif($domain eq $self->{miogaconf}->GetXMLPath()) {
		$self->{is_private_xml} = 1;
		return;
	}

	elsif($domain eq $self->{miogaconf}->GetSXMLPath()) {
		$self->{is_secure_xml} = 1;
		return;
	}

	elsif ($domain eq $self->{miogaconf}->GetLoginURI ()) {
		$self->{is_login} = 1;
		($self->{method}, $self->{args}) = split(/\/|\?/, $uri);
		my @method = split /\./, $self->{method};
		$self->{method} = $method[0];
		if (@method == 2) {
			$self->{is_webservice}  = 1;
			$self->{format}         = lc($method[1]);
		}
		return;
	}
	elsif ($domain eq $self->{miogaconf}->GetUserURI ()) {
		return;
	}

	else {
		if ($self->{missing_domain}) {
			return;
		}
		else {
			throw Mioga2::Exception::Simple("Mioga2::URI::CheckURI", 
			__x("Unknown domain (bin, pubbin, ...): {domain}", domain => $domain));
		}
	}

	# Check group ident

	if(! defined $group_ident) {
		return;
	}

	$uri =~ s/^$group_ident($|\/)//;
	$self->{group_ident} = $group_ident;

	$self->{base} = "$base_uri/$mioga_ident$domain/$group_ident";

	# for webdav, store path
	if(	$self->{is_public_webdav} or $self->{is_private_webdav} ) {
		$self->{webdav_path} = "/";
		
		$self->{webdav_path} .= $uri;
	}

	# for application, check app. ident, method and args
	else {
		
		my ($application, $method, $args) = split(/\/|\?/, $uri);
		
		if(! defined $application) {
			throw Mioga2::Exception::Simple("Mioga2::URI::CheckURI", 
											__"Missing application ident.");
		}

		$self->{app_ident} = $application;
	
		if(! defined $method) {
			throw Mioga2::Exception::Simple("Mioga2::URI::CheckURI", 
											__"Missing application method.");
		}

    my @method = split /\./, $method;
		$self->{method} = $method[0];
		if (@method == 2) {
		  $self->{is_webservice}  = 1;
		  $self->{format}         = lc($method[1]);
		}
		
		$uri =~ s/^$application\/$method(\/|\?)?//;

		if( defined $args) {
			$self->{args} = $args;
		}
		
	}
}

# ============================================================================

=head2 IsWS ()

  return true if the uri is a webservice call.
  
=cut

# ============================================================================
sub IsWS {
  my ($self) = @_;

  return $self->{is_webservice};
}

# ============================================================================

=head2 GetFormat ()

  return which format to use for a webservice.
  
=cut

# ============================================================================
sub GetFormat {
  my ($self) = @_;

  return $self->{format};
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga2 Mioga2::MiogaConf Mioga2::URI Mioga2::Application Mioga2::AppDesc
Mioga2::Old::User Mioga2::Old::Group Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
