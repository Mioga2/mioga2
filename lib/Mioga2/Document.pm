# ============================================================================
# Mioga2 Project (C) 2003-2012 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Document.pm: Access class to the Mioga2 documents.

=head1 DESCRIPTION

This module permits to deal with Mioga2 document, create, modify and destroy them.

=head1 METHODS DESRIPTION

=cut

# ============================================================================

package Mioga2::Document;
use strict;

use Locale::TextDomain::UTF8 'document';

use Error qw(:try);
use Data::Dumper;


my $debug = 0;


# ============================================================================

=head2 Create

	Create the document in database and initialize attributes and return a new Mioga2::Document object.

	Mioga2::Document::Create ({ db_obj  => $self->{db},
	                            url     => $url
	                            app_id  => $app_id
	                            mioga_id  => $mioga_id,
								group_id  => $group_id	# Optional, limit document access to group members
							 });
	All parameters are required unless explicitly indicated
	
=cut

# ============================================================================

sub Create {
	my ($class, $params) = @_;

	my $self = {};

	bless($self, $class);
	#
	# mandatory parameters
	#
	if( ! exists $params->{db_obj}) {
		throw Mioga2::Exception::Simple("Mioga2::Document::Create", __"Missing parameter db_obj.");
	}
	if( ! exists $params->{mioga_id}) {
		throw Mioga2::Exception::Simple("Mioga2::Document::Create", __"Missing parameter mioga_id.");
	}
	if( ! exists $params->{app_id}) {
		throw Mioga2::Exception::Simple("Mioga2::Document::Create", __"Missing parameter app_id.");
	}
	if( ! exists $params->{url}) {
		throw Mioga2::Exception::Simple("Mioga2::Document::Create", __"Missing parameter url.");
	}
	$self->{db} = $params->{db_obj};
	$self->{url} = $params->{url};
	$self->{app_id} = $params->{app_id};
	$self->{mioga_id} = $params->{mioga_id};
	#
	# optional parameters
	#
	$self->{group_id} = undef;
	if(exists $params->{group_id}) {
		$self->{group_id} = $params->{group_id};
	}

	$self->DBCreate();

	return $self;
}


# ============================================================================

=head2 Get

	Get the document from database and return a new Mioga2::Document object.

	Mioga2::Document::Get ({ db_obj  => $self->{db},
	                         rowid   => $rowid,
							 });
	All parameters are required 
	
=cut

# ============================================================================

sub Get {
	my ($class, $params) = @_;

	my $self = {};

	bless($self, $class);
	#
	# mandatory parameters
	#
	if( ! exists $params->{db_obj}) {
		throw Mioga2::Exception::Simple("Mioga2::Document::Create", __"Missing parameter db_obj.");
	}
	if( ! exists $params->{rowid}) {
		throw Mioga2::Exception::Simple("Mioga2::Document::Create", __"Missing parameter rowid.");
	}
	$self->{db} = $params->{db_obj};
	$self->{rowid} = $params->{rowid};

	$self->DBGet();

	return $self;
}
#===============================================================================

=head2 Delete

Delete a document and its associated access rights

	Mioga2::Document::Delete ({ db_obj  => $self->{db},
	                            rowid   => $rowid
							 	recurse => (0|1)	# [optional], defaults to 0
							 });
	All parameters are required unless explicitly indicated

=cut

#===============================================================================
sub Delete {
	my ($class, $params) = @_;

	my $self = {};

	bless($self, $class);
	#
	# mandatory parameters
	#
	if( ! exists $params->{db_obj}) {
		throw Mioga2::Exception::Simple("Mioga2::Document::Create", __"Missing parameter db_obj.");
	}
	if( ! exists $params->{rowid}) {
		throw Mioga2::Exception::Simple("Mioga2::Document::Create", __"Missing parameter rowid.");
	}

	# Recursive deletion (in case of document tree)
	my $recurse = 0;
	if (exists $params->{recurse}) {
		$recurse = 1;
	}

	$self->{db} = $params->{db_obj};
	$self->{rowid} = $params->{rowid};

	$self->DBDelete ($recurse);

	return undef;
}	# ----------  end of subroutine Delete  ----------
#===============================================================================

=head2 ClearRights

Clear a document's associated access rights. This does not delete the document.

	$document->ClearRights ($recurse);

=head3 Incoming Arguments

=over

=item I<$recurse>: Recurse through document tree (optional, defaults to 0)

=back

=cut

#===============================================================================
sub ClearRights {
	my ($self, $recurse) = @_;

	$self->DBClearRights ($recurse);

	return undef;
}	# ----------  end of subroutine Delete  ----------
# ============================================================================

=head2 GetRowid ()

	return the rowid if exists or throw an exception.
	
=cut

# ============================================================================
sub GetRowid {
	my ($self) = @_;
	
	if (exists($self->{rowid}) && defined($self->{rowid}) ) {
		return $self->{rowid};
	}
	else {
		throw Mioga2::Exception::Simple("Mioga2::Document::GetRowid", __"rowid not defined");
	}
}
# ============================================================================

=head2 InheritsRights ()

return the access rights inheritance status for a document
	
=cut

# ============================================================================
sub InheritsRights {
	my ($self) = @_;
	
	if (exists($self->{stop_inheritance}) && defined($self->{stop_inheritance}) ) {
		return (($self->{stop_inheritance} == 0) ? 1 : 0);
	}
	else {
		throw Mioga2::Exception::Simple("Mioga2::Document::InheritsRights", __"stop_inheritance not defined");
	}
}
# ============================================================================

=head2 SetInheritance ()

Set the "stop_inheritance" value

=head3 Incoming Arguments

=over

=item I<$inheritance>: The "stop_inheritance" value (0 for false, 1 for true)

=back

=cut

# ============================================================================
sub SetInheritance {
	my ($self, $inheritance) = @_;
	
	$self->{db}->ExecSQL ('UPDATE document_tree SET stop_inheritance=? WHERE document_id=?;', [($inheritance == 0) ? 't' : 'f', $self->{rowid}]);
}
# ============================================================================

=head2 InheritsFrom ()

Set or query current document in document_tree.
If argument $doc_id is given, the current document is attached to $doc_id in document_tree.
If argument $doc_id is omitted, the "parent" document in document_tree is returned.

=head3 Incoming Arguments

=over

=item I<$doc_id>: The document_id to inherit from (optional, see above)

=back

=cut

# ============================================================================
sub InheritsFrom {
	my ($self, $doc_id) = @_;
	
	if (defined ($doc_id)) {
		# $doc_id is defined, update tree
		my $current = $self->{db}->SelectSingle ('SELECT * FROM document_tree WHERE document_id = ?;', [$self->{rowid}]);
		my $parent  = $self->{db}->SelectSingle ('SELECT * FROM document_tree WHERE document_id = ?;', [$doc_id]);
		if (!defined ($current) && (!defined ($parent)) && ($self->{rowid} != $doc_id)) {
			# None of the documents exist into document_tree, record them
			# We assume here that parent document ($doc_id) doesn't inherit from any other document
			$self->{db}->ExecSQL ('INSERT INTO document_tree SELECT ?::int AS document_id, ?::int AS inherits_from, TRUE AS stop_inheritance UNION SELECT ?::int AS document_id, ?::int AS inherits_from, FALSE AS stop_inheritance;', [$doc_id, $doc_id, $self->{rowid}, $doc_id]);
		}
		if (!defined ($current) && (!defined ($parent)) && ($self->{rowid} == $doc_id)) {
			# None of the documents exist into document_tree BUT BOTH ARE THE SAME, record single
			# We assume here that document ($self->{rowid}) doesn't inherit from any other document
			$self->{db}->ExecSQL ('INSERT INTO document_tree SELECT ?::int AS document_id, ?::int AS inherits_from, FALSE AS stop_inheritance;', [$self->{rowid}, $self->{rowid}]);
		}
		elsif (defined ($parent) && (!defined ($current))) {
			# Parent exists, child doesn't, just create child
			$self->{db}->ExecSQL ('INSERT INTO document_tree SELECT ?::int AS document_id, ?::int AS inherits_from, FALSE AS stop_inheritance;', [$self->{rowid}, $doc_id]);
		}
		elsif (!defined ($parent) && defined ($current)) {
			# Parent doesn't exist but child does, create parent and link child to it
			# We assume here that parent document ($doc_id) doesn't inherit from any other document
			$self->{db}->ExecSQL ('INSERT INTO document_tree SELECT ?::int AS document_id, ?::int AS inherits_from, TRUE AS stop_inheritance;', [$doc_id, $doc_id]);
		}
		else {
			# Both parent and child exist, update record, do not change "stop_inheritance" value
			$self->{db}->ExecSQL ('UPDATE document_tree SET inherits_from = ? WHERE document_id = ?;', [$doc_id, $self->{rowid}]);
		}
	}
	else {
		# $doc_id is not defined, return rowid of "parent" document
		my $res = $self->{db}->SelectSingle ('SELECT inherits_from FROM document_tree WHERE document_id = ?;', [$self->{rowid}]);
		if (defined ($res)) {
			# Document inherits from another, return rowid
			return ($res->{inherits_from});
		}
		else {
			# Document inherits from no other, should inherit from itself (root-node)
			return ($self->{rowid});
		}
	}
}
#===============================================================================

=head2 SetURL

Set URL for an existing document

=head3 Incoming Arguments

=over

=item I<$url>: The document URL

=back

=cut

#===============================================================================
sub SetURL {
	my ($self, $url) = @_;

	$self->{db}->ExecSQL ('UPDATE document SET url=? WHERE rowid=?;', [$url, $self->{rowid}]);
}	# ----------  end of subroutine SetURL  ----------
# ============================================================================

=head2 GrantAccessToUser ($user_id, $access)

	Set the access right for given user_id.
	access = 0 for no access, 1 for read and 2 for read/write.
	
=cut

# ============================================================================
sub GrantAccessToUser {
	my ($self, $user_id, $access) = @_;
	
	$self->DBSetAccessForObject('user', $user_id, $access);
}
# ============================================================================

=head2 GrantAccessToTeam ($team_id, $access)

	Set the access right for given team_id.
	access = 0 for no access, 1 for read and 2 for read/write.
	
=cut

# ============================================================================
sub GrantAccessToTeam {
	my ($self, $team_id, $access) = @_;
	
	$self->DBSetAccessForObject('team', $team_id, $access);
}
# ============================================================================

=head2 GrantAccessToProfile ($profile_id, $access)

	Set the access right for given profile_id.
	access = 0 for no access, 1 for read and 2 for read/write.

=cut

# ============================================================================
sub GrantAccessToProfile {
	my ($self, $profile_id, $access) = @_;
	
	$self->DBSetAccessForObject('profile', $profile_id, $access);
}
# ============================================================================

=head2 RemoveAccessForUser ($user_id)

	Remove specific access right for given user_id.
	
=cut

# ============================================================================
sub RemoveAccessForUser {
	my ($self, $user_id) = @_;
	
	$self->DBRemoveAccessForObject('user', $user_id);
}
# ============================================================================

=head2 RemoveAccessForTeam ($team_id)

	Remove specific access right for given team_id.
	
=cut

# ============================================================================
sub RemoveAccessForTeam {
	my ($self, $team_id) = @_;
	
	$self->DBRemoveAccessForObject('team', $team_id);
}
# ============================================================================

=head2 RemoveAccessForProfile ($profile_id)

	Remove specific access right for given profile_id.

=cut

# ============================================================================
sub RemoveAccessForProfile {
	my ($self, $profile_id) = @_;
	
	$self->DBRemoveAccessForObject('profile', $profile_id);
}
# ============================================================================

=head2 GetAccess ($user_id)

	Get access right for given user_id.
	access = 0 for no access, 1 for read and 2 for read/write.
	
=cut

# ============================================================================
sub GetAccess {
	my ($self, $user_id) = @_;

	# Cleanup inconsistent entries
	$self->DBCleanup ();

	my $access = $self->{db}->SelectSingle ('SELECT authzTestAccessForDocument (?, ?) AS access;', [$self->{rowid}, $user_id])->{access};

	return ($access);
}

# ============================================================================

=head2 GetUserIds ()

	Get list of user rowids having a right set to document
	
=cut

# ============================================================================
sub GetUserIds {
	my ($self) = @_;

	# Cleanup inconsistent entries
	$self->DBCleanup ();

	my $res = $self->{db}->SelectMultiple ('SELECT user_id FROM doc_authz_user WHERE doc_id = ?;', [$self->{rowid}]);
	my @users = map { $_->{user_id} } @$res;

	return (\@users);
}

# ============================================================================

=head2 GetTeamIds ()

	Get list of team rowids having a right set to document
	
=cut

# ============================================================================
sub GetTeamIds {
	my ($self) = @_;

	# Cleanup inconsistent entries
	$self->DBCleanup ();

	my $res = $self->{db}->SelectMultiple ('SELECT team_id FROM doc_authz_team WHERE doc_id = ?;', [$self->{rowid}]);
	my @teams = map { $_->{team_id} } @$res;

	return (\@teams);
}
# ============================================================================

=head2 GetUserAccess ()

	Get list of user access with user attributes.
	Return array of hash with access content plus user parameters
	
=cut

# ============================================================================
sub GetUserAccess {
	my ($self) = @_;

	my $sql = 'SELECT doc_authz_user.*, m_user_base.firstname, m_user_base.lastname, m_user_base.email'
				.' FROM doc_authz_user, m_user_base WHERE m_user_base.rowid = doc_authz_user.user_id AND doc_id = ?';
	my $res = $self->{db}->SelectMultiple ($sql, [$self->{rowid}]);

	return $res;
}
# ============================================================================

=head2 GetTeamIds ()

	Get list of team access having a right set to document
	Return array of hash with access content plus team parameters
	
=cut

# ============================================================================
sub GetTeamAccess {
	my ($self) = @_;

	my $sql = 'SELECT doc_authz_team.*, m_group.ident FROM doc_authz_team, m_group WHERE m_group.rowid = doc_authz_team.team_id AND doc_id = ?';
	my $res = $self->{db}->SelectMultiple ($sql, [$self->{rowid}]);

	return $res;
}
# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 DBCreate
	
	create a document in database.
	
=cut

# ============================================================================
sub DBCreate {
	my ($self) = @_;

	my $result = $self->{db}->SelectSingle("SELECT nextval('document_rowid_seq') as rowid");
	my $rowid  = $result->{rowid};

	my $sql = "INSERT INTO document (rowid, created, modified, url, mioga_id, app_id) VALUES (?, now(), now(), ?, ?, ?)";
	$self->{db}->ExecSQL($sql, [$rowid, $self->{url}, $self->{mioga_id}, $self->{app_id}]);

	if (defined ($self->{group_id})) {
		$self->{db}->ExecSQL ('INSERT INTO document_group (document_id, group_id) VALUES (?, ?);', [$rowid, $self->{group_id}]);
	}

	$self->{rowid} = $rowid;
}
# ============================================================================

=head2 DBGet
	
	Get a document in database.
	
=cut

# ============================================================================
sub DBGet {
	my ($self) = @_;

	my $result = $self->{db}->SelectSingle("SELECT document.*, document_tree.inherits_from, document_tree.stop_inheritance FROM document LEFT JOIN document_tree ON document.rowid = document_tree.document_id WHERE rowid=?;", [$self->{rowid}]);
	if ($result) {
		$self->{rowid} = $result->{rowid};
		$self->{url} = $result->{url};
		$self->{app_id} = $result->{app_id};
		$self->{mioga_id} = $result->{mioga_id};
		$self->{stop_inheritance} = $result->{stop_inheritance};

		$result = $self->{db}->SelectSingle ('SELECT group_id FROM document_group WHERE document_id = ?;', [$self->{rowid}]);
		if ($result) {
			$self->{group_id} = $result->{group_id};
		}
	}
	else {
		throw Mioga2::Exception::Simple("Mioga2::Document::DBGet", __"No such document");
	}
}
# ============================================================================

=head2 DBDelete
	
	Delete a document (and matching access rights) from database.
	
=cut

# ============================================================================
sub DBDelete {
	my ($self, $recurse) = @_;

	$self->ClearRights ($recurse);

	if ($recurse) {
		$self->{db}->ExecSQL ('DELETE FROM document_group WHERE document_id IN (SELECT getChildDocIds (?));', [$self->{rowid}]);
		$self->{db}->ExecSQL ('DELETE FROM document_tree WHERE document_id IN (SELECT getChildDocIds (?));', [$self->{rowid}]);
		$self->{db}->ExecSQL ('DELETE FROM document WHERE rowid IN (SELECT getChildDocIds (?));', [$self->{rowid}]);
	}
	else {
		$self->{db}->ExecSQL ('DELETE FROM document_group WHERE document_id = ?;', [$self->{rowid}]);
		$self->{db}->ExecSQL ('DELETE FROM document_tree WHERE document_id = ?;', [$self->{rowid}]);
		$self->{db}->ExecSQL ('DELETE FROM document WHERE rowid=?;', [$self->{rowid}]);
	}
}
# ============================================================================

=head2 DBClearRights
	
	Clear a document's associated rights
	
=cut

# ============================================================================
sub DBClearRights {
	my ($self, $recurse) = @_;

	if ($recurse) {
		$self->{db}->ExecSQL ('DELETE FROM doc_authz_user WHERE doc_id IN (SELECT getChildDocIds (?));', [$self->{rowid}]);
		$self->{db}->ExecSQL ('DELETE FROM doc_authz_team WHERE doc_id IN (SELECT getChildDocIds (?));', [$self->{rowid}]);
		$self->{db}->ExecSQL ('DELETE FROM doc_authz_profile WHERE doc_id IN (SELECT getChildDocIds (?));', [$self->{rowid}]);
	}
	else {
		$self->{db}->ExecSQL ('DELETE FROM doc_authz_user WHERE doc_id=?;', [$self->{rowid}]);
		$self->{db}->ExecSQL ('DELETE FROM doc_authz_team WHERE doc_id=?;', [$self->{rowid}]);
		$self->{db}->ExecSQL ('DELETE FROM doc_authz_profile WHERE doc_id=?;', [$self->{rowid}]);
	}
}
# ============================================================================

=head2 DBSetAccessForObject ($type_obj, $obj_id, $access)
	
	$type_obj = 'profile', 'user' or 'team'
	
=cut

# ============================================================================
sub DBSetAccessForObject {
	my ($self, $obj_type, $obj_id, $access) = @_;
	print STDERR "Mioga2::Document::DBSetAccesForObject for document = ".$self->{rowid}."\n" if ($debug);
	print STDERR "obj_type = $obj_type  obj_id = $obj_id  access = $access\n" if ($debug);
	my $table;
	my $obj_id_name;
	my $sql;
	my $res;

	if ($obj_type eq 'user') {
		$table = 'doc_authz_user';
		$obj_id_name = 'user_id';
		$sql = "SELECT rowid FROM m_user_base WHERE rowid = ? AND mioga_id = ?";
		$res = $self->{db}->SelectSingle($sql, [$obj_id, $self->{mioga_id}]);
	}
	elsif ($obj_type eq 'team') {
		$table = 'doc_authz_team';
		$obj_id_name = 'team_id';
		$sql = "SELECT m_group_base.rowid FROM m_group_base, m_group_type WHERE m_group_base.rowid = ? AND m_group_base.mioga_id = ? AND m_group_base.type_id = m_group_type.rowid AND m_group_type.ident = 'team'";
		$res = $self->{db}->SelectSingle($sql, [$obj_id, $self->{mioga_id}]);
	}
	elsif ($obj_type eq 'profile') {
		$table = 'doc_authz_profile';
		$obj_id_name = 'profile_id';
		$sql = "SELECT m_profile.rowid FROM m_profile, m_group_base WHERE m_profile.rowid = ? AND m_group_base.mioga_id = ? AND m_profile.group_id = m_group_base.rowid";
		$res = $self->{db}->SelectSingle($sql, [$obj_id, $self->{mioga_id}]);
	}
	else {
		throw Mioga2::Exception::Simple("Mioga2::Document::DBSetAccessForObject", __"Invalid obj_type");
	}

	print STDERR "  after test, res = ".Dumper($res)."\n" if ($debug);
	if (!defined($res)) {
		throw Mioga2::Exception::Simple("Mioga2::Document::DBSetAccessForObject", __"Object not defined in current instance");
	}

	$sql = "SELECT * FROM $table WHERE $obj_id_name = ? AND doc_id = ?";
	print STDERR "sql = $sql\n" if ($debug);
	my $authz = $self->{db}->SelectSingle($sql, [$obj_id, $self->{rowid}]);
	if (! defined($authz)) {
		print STDERR "   no authz create it\n" if ($debug);
		$sql = "INSERT INTO $table (doc_id, $obj_id_name, access) VALUES (?, ?, ?)";
		$self->{db}->ExecSQL($sql, [$self->{rowid}, $obj_id, $access]);
	}
	else {
		print STDERR "   use authz ".$authz->{rowid}."\n" if ($debug);
		$sql = "UPDATE $table SET access = ? WHERE $obj_id_name = ? AND doc_id IN (SELECT getChildDocIds (?)) AND access = (SELECT access FROM $table WHERE $obj_id_name = ? AND doc_id = ?);";
		$self->{db}->ExecSQL($sql, [$access, $obj_id, $self->{rowid}, $obj_id, $self->{rowid}]);
	}
}
# ============================================================================

=head2 DBRemoveAccessForObject ($type_obj, $obj_id)
	
	$type_obj = 'profile', 'user' or 'team'
	
=cut

# ============================================================================
sub DBRemoveAccessForObject {
	my ($self, $obj_type, $obj_id) = @_;
	print STDERR "Mioga2::Document::DBRemoveAccessForObject for document = ".$self->{rowid}."\n" if ($debug);
	print STDERR "obj_type = $obj_type  obj_id = $obj_id\n" if ($debug);
	my $table;
	my $obj_id_name;

	if ($obj_type eq 'user') {
		$table = 'doc_authz_user';
		$obj_id_name = 'user_id';
	}
	elsif ($obj_type eq 'team') {
		$table = 'doc_authz_team';
		$obj_id_name = 'team_id';
	}
	elsif ($obj_type eq 'profile') {
		$table = 'doc_authz_profile';
		$obj_id_name = 'profile_id';
	}
	else {
		throw Mioga2::Exception::Simple("Mioga2::Document::DBRemoveAccessForObject", __"Invalid obj_type");
	}

	$self->{db}->ExecSQL ("DELETE FROM $table WHERE $obj_id_name = ? AND doc_id = ?", [$obj_id, $self->{rowid}]);
}


#===============================================================================

=head2 DBCleanup

Cleanup inconsistent DB entries (access rights to users not being member of group, etc.)

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

=over

None.

=back

=cut

#===============================================================================
sub DBCleanup {
	my ($self) = @_;

	if ($self->{group_id}) {
		$self->{db}->ExecSQL ('DELETE FROM doc_authz_user WHERE doc_id = ? AND user_id NOT IN (SELECT invited_group_id FROM m_group_group WHERE group_id = ?);', [$self->{rowid}, $self->{group_id}]);
		$self->{db}->ExecSQL ('DELETE FROM doc_authz_team WHERE doc_id = ? AND team_id NOT IN (SELECT invited_group_id FROM m_group_group WHERE group_id = ?);', [$self->{rowid}, $self->{group_id}]);
		$self->{db}->ExecSQL ('DELETE FROM doc_authz_profile WHERE doc_id = ? AND profile_id NOT IN (SELECT rowid FROM m_profile WHERE group_id = ?);', [$self->{rowid}, $self->{group_id}]);
	}
}	# ----------  end of subroutine DBCleanup  ----------

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga2 Mioga2::MiogaConf Mioga2::URI Mioga2::Application Mioga2::AppDesc
Mioga2::Old::User Mioga2::Old::Group Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
