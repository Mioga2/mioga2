#===============================================================================
#
#         FILE:  LogHandler.pm
#
#  DESCRIPTION:  Handler to log request informations in DB
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  02/02/2012 15:01
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME
	
LogHandler.pm: Handler to log request informations in DB

=head1 DESCRIPTION

This handler records information from context in database
http://perl.apache.org/docs/2.0/user/handlers/http.html#PerlLogHandler

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::LogHandler;

use Apache2::Const;
use Apache2::RequestUtil;
use Mioga2::Config;
use Mioga2::Database;
use Mioga2::tools::Convert;
use Mioga2::tools::string_utils;
use Data::Dumper;

my $debug = 0;

sub handler : method {
	my($class, $r) = @_;
	print STDERR "[Mioga2::LogHandler::handler -- $$] ---------------------------------------------------------\n" if ($debug);

	my $config = $r->pnotes("config");
	if (!defined($config)) {
		print STDERR "[Mioga2::LogHandler::handler -- $$] no config defined cannot log\n";
		return Apache2::Const::OK;
	}

	my $mioga_id = $config->GetMiogaIdent();

	my $uri_obj = $r->pnotes("uri_obj");
	my $uri = $uri_obj->GetURI();

	my $user_id;
	my $user = $r->pnotes("user");
	if (defined($user)) {
		$user_id = $user->GetIdent();
	}

	my $args = $r->pnotes("args");
	my $st_args = "";
	if (defined($args)) {
		$st_args = Mioga2::tools::Convert::PerlToJSON($args);
	}

	# Cleanup GET args as they are in $st_args above
	$uri =~ s/\?.*$//;

	my $method = $r->method();

	print STDERR "  mioga_id = $mioga_id user_id = $user_id uri = $uri st_args = $st_args method = $method\n" if ($debug );

	my $db = $config->GetDBObject();
	my $sql = "INSERT INTO m_log (date, args, method, status, instance, uri, user_ident) VALUES(now(), ?, ?, '".$r->status()."', ";

	if (defined($mioga_id)) {
		$sql .= "'$mioga_id', ";
	}
	else {
		$sql .= "null, ";
	}
	$sql .= $db->Quote ($uri) . ",";
	if (defined($user_id)) {
		$sql .= "'$user_id' ";
	}
	else {
		$sql .= "null ";
	}
	$sql .= ")";
	print STDERR "   sql = $sql\n" if ($debug);
	#$db->ExecSQL($sql, [$mioga_id, $uri, $ident]);
	$db->ExecSQL($sql, [$st_args, $method]);

	return Apache2::Const::OK;
}

1;

