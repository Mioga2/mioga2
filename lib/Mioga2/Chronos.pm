#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Chronos.pm: the Mioga2 calendar application

=head1 DESCRIPTION

The Mioga2::Chronos offers access to resource, group and user calendar.

Chronos can be used as a user, group or resource application. Function of this context,
methods behave differently (look individual methods description).

There is only one calendar for groups and resources, and access rights are given
by profiles for Chronos application.

A user can be owner of many calendars and set access rights for other users and teams 
to each of them.

In user mode, Chronos give access to multiple calendars function of the rights the user have on theme.

=head1 METHODS DESCRIPTION

=cut

#
# ============================================================================

package Mioga2::Chronos;
use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'chronos';

use strict;
use warnings;
use open ':encoding(utf8)';

use Error qw(:try);
use DateTime::Event::ICal;
use Data::ICal;
use Data::ICal::Entry::Event;
use DateTime::Format::ICal;
use Mioga2::Document;
use Data::Dumper;
use Mioga2::tools::string_utils;
use Mioga2::tools::Convert;
use Mioga2::tools::args_checker;
use Mioga2::Content::XSLT;
use Mioga2::Exception::Simple;
use Mioga2::Exception::HTTPError;
use Mioga2::TaskCategory;
use Mioga2::Classes::Time;
use Mioga2::Classes::CalEvent;

my $debug = 0;

# ============================================================================
# GetAppDesc ()
# ============================================================================

sub GetAppDesc {
	my ($self, $context) = @_;
    my %AppDesc = ( 
    	ident   => 'Chronos',
	    name	=> __('Chronos'),
        package => 'Mioga2::Chronos',
		type => 'normal',
        description => __('The Mioga2 chronos application'),
        api_version => '2.4',
        all_groups  => 1,
        all_users  => 1,
        all_resources  => 1,
        is_user  => 1,
        is_group  => 1,
        is_resource  => 1,
        can_be_public => 0,
        usable_by_resource => 1,

        functions   => { 
			'Read' => __('Read only functions'),
			'Write' => __('Write functions'),
			'Anim' => __('Animation functions'),
		},
				
        func_methods  => { 
			Read => [ 'DisplayMain', 'GetEvents', 'GetConfiguration', 'GetProjectTasks', 'GetICSCalendar'  ],
			Write => [ 'CreateEvent', 'UpdateEvent', 'SuppressEvent',
			           'CreateUserCalendar', 'DestroyUserCalendar', 'SetCalendar',
					   'SetVisibility', 'SearchUserFromMail', 'GetTeamList', 'GetUserList',
					   'ChangeAccessToCalendar', 'SuppressAccessToCalendar',
					   'ConnectCalendar', 'DetachFromCalendar', 'SetProjectTask' ],
			Anim => [ 'SetPreferences' ],
		},
		public_methods => [
			'DisplayMain'
		],
		func_mioglets => { },
		public_mioglets => [  ],
		non_sensitive_methods => [
			'DisplayMain',
			'GetEvents',
			'GetConfiguration',
			'GetProjectTasks',
			'GetICSCalendar',
			'SearchUserFromMail',
			'GetTeamList',
			'GetUserList',
		]
    );
	return \%AppDesc;
}

#===============================================================================

=head1 INTERFACE METHODS

=cut

#===============================================================================

=head2 DisplayMain

Display main window for Chronos.

=head3 Generated XML

=over

	<DisplayMain>
		<miogacontext>See Mioga2::RequestContext</miogacontext>
	</DisplayMain>

=back

=cut

# ============================================================================
sub DisplayMain {
	my ($self, $context) = @_;
	print STDERR "Chronos::DisplayMain\n" if ($debug);
	my $xml;
	$xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= '<DisplayMain>';
	$xml .= $context->GetXML();
	#TODO read configuration to limit server requests
	$xml .= '</DisplayMain>';
	print STDERR "xml = $xml\n" if $debug;
	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'chronos.xsl', locale_domain => 'chronos_xsl');
	$content->SetContent($xml);
	return $content;
}
#===============================================================================

=head2 GetICSCalendar

	Return given calendar in ICS format

=cut

# ============================================================================
sub GetICSCalendar {
	my ($self, $context) = @_;
	print STDERR "Chronos::GetICSCalendar\n" if ($debug);
	my $params = [	
			[ [ 'calendar_id' ], 'disallow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	if (scalar(@$errors) == 0) {
		my $user_id = $context->GetUser()->Get('rowid');
		#my $doc_id = $self->DBVerifyOwnerOfCalendar($user_id, $values->{calendar_id});
		my $access = $self->CheckCalendarAccess($user_id, $values->{calendar_id});
		# acces read asked
		if ($access >= 1) {
			my $result = $self->DBExportICAL($user_id, $values->{calendar_id});
			my $mime = 'text/calendar';
		
			my $content = new Mioga2::Content::ASIS($context, MIME => $mime, filename => 'mioga.ics');
			$content->SetContent($result);

			return $content;
		}
		else {
			throw Mioga2::Exception::HTTPError("Chronos::GetICSCalendar", __("No access to calendar !"), "HTTP_AUTHZ");
		}
	}
	throw Mioga2::Exception("Chronos::GetICSCalendar", __("Bad parameters !"));
}
#===============================================================================

=head1 WEBSERVICE METHODS

=cut

#===============================================================================

=head2 GetConfiguration

	Get the calendar configuration function of mode (user, group or resource).

	In group or resource mode, calendar is created if it doesn't exist.
	In user mode, we get the list of calendars the user can access and organise the response
	function 

=cut

# ============================================================================
sub GetConfiguration {
	my ($self, $context) = @_;
	print STDERR "Chronos::GetConfiguration\n" if ($debug);

	my $data = {};

	my $config;
	$config->{calendars} = [];

	#print STDERR Dumper($context->GetUser()) if ($debug);
	#
	# Look for context, group or user mode.
	#
	my $group_id = $context->GetGroup()->Get('rowid');
	my $user_id = $context->GetUser()->Get('rowid');
	print STDERR "group_id = $group_id   user_id = $user_id\n" if $debug;
	my $prefs = $self->DBReadPreferences($user_id, $group_id);
	print STDERR "prefs = ".Dumper($prefs)."\n" if $debug;
	if (defined($prefs)) {
		$config->{currentCalId} = $prefs->{currentcalid};
		$config->{currentView}     = $prefs->{currentview};
		$config->{firsthour}       = $prefs->{firsthour};
		$config->{colorscheme}     = $prefs->{colorscheme};
		$config->{showweekend}     = $prefs->{showweekend};
		# user mode
	}
	else {
		$config->{currentCalId} = 0;
		$config->{currentView} = "agendaWeek";
		$config->{firsthour} = "07h00";
		$config->{colorscheme} = 1;
		$config->{showweekend}     = 1;
	}
	$config->{anim_right} = 0;
	$config->{anim_right} = 1 if ($self->GetUserRights()->{Anim});
	#
	#  Access in user mode
	#
	if ($group_id == $user_id) {
		print STDERR "mode user\n" if ($debug);
		$config->{mode} = "user";
		# Get user's calendar list
		my $cals = $self->DBGetUserCalendars($user_id);
		if (!defined($cals) || scalar(@$cals) == 0) {
			my $params = { ident => __("My calendar")
						  , color => "#000000"
						  , bgcolor => "#ffffff" };
			my $calendar = $self->DBCreateUserCalendar($user_id, $params);
			$cals = $self->DBGetUserCalendars($user_id);
			if (!defined($cals) || scalar(@$cals) == 0) {
				throw Mioga2::Exception("Chronos::GetConfiguration", __("Cannot create and get default user calendar !"));
			}
		}
		# Get group calendar with access for user
		# and create an hash with calendar_id as key
		my $groups = $self->DBGetGroupsWithCalendar($user_id);
		my $gcals = {};
		foreach my $g (keys(%$groups)) {
			$gcals->{$groups->{$g}->{calendar_id}} = $g;
		}
		print STDERR "gcals ".Dumper($gcals)."\n" if ($debug);
		# Get other users calendar
		my $others = $self->DBGetOtherCalendars($user_id);
		print STDERR "others ".Dumper($others)."\n" if ($debug);
		my $ocals = {};
		for (my $i = 0; $i < scalar(@$others); $i++) {
			$ocals->{$others->[$i]->{rowid}} = $i;
		}
		print STDERR "ocals ".Dumper($ocals)."\n" if ($debug);
		# prepare calendar list
		foreach my $cal (@$cals) {
			print STDERR Dumper($cal) if ($debug);
			my $c = {
						ident => $cal->{ident},
						bgcolor => $cal->{bgcolor},
						color => $cal->{color},
						owner_type => $cal->{owner_type},
						owner_id => $cal->{user_id},
						type => $cal->{type},
						visible => $cal->{visible},
						rowid => $cal->{rowid},
					 };
			# For user's calendars, get access right
			if ($cal->{type} == 0) {
				$c->{access_rights} =  $self->DBGetAccessRightForDocument($cal->{document_id});
				$c->{access} =  2;  # user have write access on its calendar
			}
			# For other's calendars, ??????
			elsif ($cal->{type} == 1) {
				if (exists($ocals->{$cal->{rowid}})) {
					$c->{access} =  $others->[$ocals->{$cal->{rowid}}]->{access};
					delete $ocals->{$cal->{rowid}};
				}
				else {
					#user hasn't access to calendar flush user2cal record
					$self->DBDetachCalendar($user_id, $cal->{rowid});
					print STDERR "user cannot access other calendar : ".$cal->{rowid}."\n" if ($debug);
					$c = undef;
				}
			}
			# For used group's calendars
			elsif ($cal->{type} == 2) {
				# if user has really access on calendar
				if (exists($gcals->{$cal->{rowid}})) {
					$c->{access} =  $groups->{$gcals->{$cal->{rowid}}}->{access};
					delete $gcals->{$cal->{rowid}};
				}
				# else we don't push the calendar
				else {
					print STDERR "user cannot access group calendar : ".$cal->{rowid}."\n" if ($debug);
					$self->DBDetachCalendar($user_id, $cal->{rowid});
					$c = undef;
				}
			}
			if (defined($c)) {
				push @{$config->{calendars}}, $c;
			}
		}
		# prepare authorized group calendar list
		foreach my $cal_id (keys(%$gcals)) {
			push @{$config->{groupCals}}, { ident => $groups->{$gcals->{$cal_id}}->{ident},
										  rowid => $cal_id,
										};
		}
		# prepare authorized others calendar list
		foreach my $cal_id (keys(%$ocals)) {
			push @{$config->{otherCals}}, { ident => $others->[$ocals->{$cal_id}]->{email} ." (".  $others->[$ocals->{$cal_id}]->{ident} . ")",
										  rowid => $cal_id,
										};
		}
	}
	#
	#  Access from a group
	#
	else {
		print STDERR "mode group\n" if ($debug);
		$config->{mode} = "group";
		# TODO defined acces rights on calendar for user
		my $cal = $self->DBGetGroupCalendar($group_id);
		if (!defined($cal)) {
			$self->DBCreateGroupCalendar($group_id, $context->GetGroup()->Get('ident'));
			$cal = $self->DBGetGroupCalendar($group_id);
			if (!defined($cal)) {
				throw Mioga2::Exception("Chronos::GetConfiguration", __("Cannot create and get group calendar for group !"));
			}
		}
		my $access = 0;
		$access = 1 if ($self->GetUserRights()->{Read});
		$access = 2 if ($self->GetUserRights()->{Write});

		push @{$config->{calendars}}, {type => 0,
		                             owner_type => $cal->{owner_type},
		                             ident => $cal->{ident},
									 bgcolor => $cal->{bgcolor},
									 color => $cal->{color},
									 access => $access,
									 rowid => $cal->{rowid},
									 };
	}

	#
	# Task categories defined
	#
	my $taskcat = new Mioga2::TaskCategory ($context->GetConfig ());
	my $cat = $taskcat->GetList ();
	print STDERR "cat = ".Dumper($cat)."\n" if ($debug);
	$config->{categories} = [];
	foreach my $c (@$cat) {
		print STDERR "c = ".Dumper($c)."\n" if ($debug);
		push @{$config->{categories}}, { name => $c->{name}, category_id => $c->{rowid},  bgcolor => $c->{bgcolor}, fgcolor => $c->{fgcolor} };
	}

	$data->{status} = "OK";
	$data->{config} = $config;
	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 GetProjectTasks

	Records calendar preferences for user or group

=cut

# ============================================================================
sub GetProjectTasks {
	my ($self, $context) = @_;
	print STDERR "Chronos::GetProjectTasks\n" if ($debug);
	my $data = {};
	# Project tasks
	my $user_id = $context->GetUser()->Get('rowid');
	$data->{projectTasks} = $self->DBGetProjectTasksForUser($user_id);
	return $data;
}
#===============================================================================

=head2 SetPreferences

	Records calendar preferences for user or group

=cut

# ============================================================================
sub SetPreferences {
	my ($self, $context) = @_;
	print STDERR "Chronos::SetPreferences\n" if ($debug);

	my $params = [	
			[ [ 'currentCalId' ], 'disallow_empty', 'wantint' ],
			[ [ 'currentView' ], 'disallow_empty', 'stripxws' ],
			[ [ 'colorscheme' ], 'disallow_empty', 'wantint' ],
			[ [ 'firsthour' ], 'disallow_empty', 'stripxws' ],
			[ [ 'showweekend'], 'disallow_empty', 'stripxws', 'wantint' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $group_id = $context->GetGroup()->Get('rowid');
		my $user_id = $context->GetUser()->Get('rowid');
		$self->DBWritePreferences($user_id, $group_id, $values);
		$data->{status} = "OK";
	}

	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 SetVisibility

	Set the visible flag for calendar in user Chronos.

=cut

# ============================================================================
sub SetVisibility {
	my ($self, $context) = @_;
	print STDERR "Chronos::SetVisibility\n" if ($debug);

	my $params = [	
			[ [ 'calendar_id' ], 'disallow_empty', 'wantint' ],
			[ [ 'visible' ], 'disallow_empty', 'boolean' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	# TODO verify access to calendar
	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		$self->DBSetVisibility($user_id, $values);
	}

	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 SearchUserFromMail

	Search a user in Mioga instance database from email address.

	Return lastname, firstname, email and rowid

=cut

# ============================================================================
sub SearchUserFromMail {
	my ($self, $context) = @_;
	print STDERR "Chronos::SearchUserFromMail\n" if ($debug);

	my $params = [	
			[ [ 'email' ], 'disallow_empty', 'stripxws' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $res = $self->DBSearchUserFromMail($values);
		if (defined($res)) {
			$data->{status} = "OK";
			$data->{user} = $res;
		}
		else {
			$data->{status} = "OK";
			$data->{user} = undef;
		}
	}

	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 GetUserList

	Get the user list for instance not affected to given calendar. The users must be invited in
	groups where caller is invited too.

	Return an array of hashes with rowid and ident informations

=cut

# ============================================================================
sub GetUserList {
	my ($self, $context) = @_;
	print STDERR "Chronos::GetUserList\n" if ($debug);

	my $params = [	
			[ [ 'calendar_id' ], 'disallow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		my $res = $self->DBGetUserList($user_id, $values->{calendar_id});
		if (defined($res)) {
			$data->{status} = "OK";
			$data->{users} = $res;
		}
		else {
			$data->{status} = "KO";
		}
	}

	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 GetTeamList

	Get the team list for instance not affected to given calendar

	Return an array of hashes with rowid and ident

=cut

# ============================================================================
sub GetTeamList {
	my ($self, $context) = @_;
	print STDERR "Chronos::GetTeamList\n" if ($debug);

	my $params = [	
			[ [ 'calendar_id' ], 'disallow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		my $res = $self->DBGetTeamList($user_id, $values->{calendar_id});
		if (defined($res)) {
			$data->{status} = "OK";
			$data->{teams} = $res;
		}
		else {
			$data->{status} = "KO";
		}
	}

	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 ChangeAccessToCalendar

	Grant access right to given calendar for given object 

	Params :
	   object_type : 1 for user, 2 for team
	   action      : 1 for add, 2 for change
	   object_id : rowid for given object
	   calendar_id : rowid of calendar
	   access : 1 for read, 2 for write

	Return complete access list for given calendar

=cut

# ============================================================================
sub ChangeAccessToCalendar {
	my ($self, $context) = @_;
	print STDERR "Chronos::ChangeAccessToCalendar\n" if ($debug);
	#
	# argument checking
	#
	my $params = [	
			[ [ 'object_type', 'calendar_id', 'object_id', 'access', 'action' ], 'disallow_empty', 'wantint' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) if ($debug);
	print STDERR "errors = " . Dumper($errors) if ($debug);
	if ($values->{access} != 1 && $values->{access} != 2) {
		push @$errors, [ 'access', [ 'bad_value' ] ];
	}
	if ($values->{action} != 1 && $values->{action} != 2) {
		push @$errors, [ 'action', [ 'bad_value' ] ];
	}
	#
	# Processing
	#
	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		# User access
		if ($values->{object_type} == 2) {
			print STDERR "  user access\n" if ($debug);
			$data->{access_list} = $self->DBGrantAccessToUser($user_id, $values->{object_id}, $values->{calendar_id}, $values->{access});
			$data->{status} = "OK";
		}
		# Team access
		elsif ($values->{object_type} == 1) {
			print STDERR "  team access\n" if ($debug);
			$data->{access_list} = $self->DBGrantAccessToTeam($user_id, $values->{object_id}, $values->{calendar_id}, $values->{access});
			$data->{status} = "OK";
		}
		else {
			print STDERR "  unknown access\n" if ($debug);
			$data->{status} = "KO";
		}
	}

	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 SuppressAccessToCalendar

	Suppress access right to given calendar for given object 

	Params :
	   object_type : 1 for user, 2 for team
	   object_id : rowid for given object
	   access_id : rowid of access
	   calendar_id : rowid of calendar

	Return complete access list for given calendar

=cut

# ============================================================================
sub SuppressAccessToCalendar {
	my ($self, $context) = @_;
	print STDERR "Chronos::SuppressAccessToCalendar\n" if ($debug);

	my $params = [	
			[ [ 'object_type', 'access_id', 'object_id', 'calendar_id' ], 'disallow_empty', 'wantint' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) if ($debug);
	print STDERR "errors = " . Dumper($errors) if ($debug);

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		print STDERR "  got error, KO\n" if ($debug);
		$data->{status} = "KO";
	}
	else {
		print STDERR "  no error, continue\n" if ($debug);
		my $user_id = $context->GetUser()->Get('rowid');
		$data->{access_list} = $self->DBSuppressAccessForObject($user_id, $values->{object_id}, $values->{object_type}, $values->{access_id}, $values->{calendar_id});
		$data->{status} = "OK";
	}

	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 ConnectCalendar

	Connect to an existing calendar.

=cut

# ============================================================================
sub ConnectCalendar {
	my ($self, $context) = @_;
	print STDERR "Chronos::ConnectCalendar\n" if ($debug);

	my $params = [	
			[ [ 'color', 'bgcolor', 'ident' ], 'disallow_empty', 'stripxws' ],
			[ [ 'type', 'calendar_id' ], 'disallow_empty', 'wantint' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		my $access = $self->CheckCalendarAccess($user_id, $values->{calendar_id});
		print STDERR "access = $access\n" if ($debug);
		if ($access > 0) {
			my $calendar = $self->DBConnectCalendar($user_id, $values, $access);
			if (defined($calendar)) {
				$data->{status} = "OK";
				$data->{calendar} = $calendar;
			}
			else {
				$data->{status} = "KO";
			}
		}
		else {
			$data->{status} = "KO";
		}
	}
	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 CreateUserCalendar

	Create a new calendar for user

=cut

# ============================================================================
sub CreateUserCalendar {
	my ($self, $context) = @_;
	print STDERR "Chronos::CreateUserCalendar\n" if ($debug);
	my $params = [	
			[ [ 'ident', 'color', 'bgcolor' ], 'disallow_empty', 'stripxws' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		my $calendar = $self->DBCreateUserCalendar($user_id, $values);
		if (defined($calendar)) {
			$data->{status} = "OK";
			$data->{calendar} = $calendar;
		}
		else {
			$data->{status} = "KO";
		}
	}
	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 DestroyUserCalendar

	Destroy user calendar, with all events attached and access rights

=cut

# ============================================================================
sub DestroyUserCalendar {
	my ($self, $context) = @_;
	print STDERR "Chronos::DestroyUserCalendar\n" if ($debug);
	my $params = [	
			[ [ 'calendar_id' ], 'disallow_empty', 'wantint' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		if ($self->DBDestroyUserCalendar($user_id, $values->{calendar_id})) {
			$data->{status} = "OK";
		}
		else {
			$data->{status} = "KO";
		}
	}
	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 DetachFromCalendar

	Detach user from given calendar. Suppress user2cal record.

=cut

# ============================================================================
sub DetachFromCalendar {
	my ($self, $context) = @_;
	print STDERR "Chronos::DetachFromCalendar\n" if ($debug);
	my $params = [	
			[ [ 'calendar_id' ], 'disallow_empty', 'wantint' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		if ($self->DBDetachCalendar($user_id, $values->{calendar_id})) {
			$data = $self->GetConfiguration($context);
		}
		else {
			$data->{status} = "KO";
		}
	}
	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 SetCalendar

	Change parameters for given calendar

=cut

# ============================================================================
sub SetCalendar {
	my ($self, $context) = @_;
	print STDERR "Chronos::SetCalendar\n" if ($debug);

	my $params = [	
			[ [ 'rowid' ], 'disallow_empty', 'stripxws', 'wantint' ],
			[ [ 'ident', 'color', 'bgcolor' ], 'disallow_empty', 'stripxws' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);


	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		my $group_id = $context->GetGroup()->Get('rowid');
		$self->DBSetCalendar($user_id, $group_id, $values);
	}

	print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 GetEvents

	Return tasks for given period

=cut

# ============================================================================
sub GetEvents {
	my ($self, $context) = @_;
	print STDERR "Chronos::GetEvents\n" if ($debug);

	my $params = [	
			[ [ 'start_year', 'start_month', 'start_day' ], 'disallow_empty', 'stripxws', 'wantint' ],
			[ [ 'end_year', 'end_month', 'end_day' ], 'disallow_empty', 'stripxws', 'wantint' ],
			[ [ 'calendar_list' ], 'disallow_empty', 'stripxws' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);
	#
	# get calendar list and verify access right
	#
	$values->{calendar_list} =~ s/\s+//g;
	$values->{calendar_list} =~ s/^\[//g;
	$values->{calendar_list} =~ s/\]$//g;
	print STDERR "calendar_list = " . $values->{calendar_list} . "\n" if ($debug);
	my @cal_ids = split(",", $values->{calendar_list});
	print STDERR "cal_ids = " . Dumper(\@cal_ids) . "\n" if ($debug);
	my $user_id = $context->GetUser()->Get('rowid');
	my $group_id = $context->GetGroup()->Get('rowid');
	$values->{db} = $self->{db};
	$values->{user_id} = $user_id;
	$values->{cal_ids} = \@cal_ids;

	my $data;
	$data->{tasks} = Mioga2::Classes::CalEvent::GetEventsForCals($values);

	#print STDERR "data = ".Dumper($data)."\n" if ($debug);
	return $data;
}
#===============================================================================

=head2 UpdateEvent

	Modify task parameters given a task rowid. It can be a move between two calendars

=cut

# ============================================================================
sub UpdateEvent {
	my ($self, $context) = @_;
	print STDERR "Chronos::UpdateEvent\n" if ($debug);

	my $params = [
			[ [ 'event' ], 'disallow_empty', 'stripxws' ],
			[ [ 'rowid', 'calendar_id' ], 'disallow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		my $access = $self->CheckCalendarAccess($user_id, $values->{calendar_id});
		print STDERR "access = $access\n" if ($debug);
		if ($access > 1) {
			$self->DBWriteEvent($user_id, 0, $values);
			$data->{status} = "OK";
		}
		else {
			$data->{status} = "KO";
		}
	}

	return $data;
}
#===============================================================================

=head2 CreateEvent

	Create a new event

=cut

# ============================================================================
sub CreateEvent {
	my ($self, $context) = @_;
	print STDERR "Chronos::CreateEvent\n" if ($debug);

	my $params = [
			[ [ 'event' ], 'disallow_empty', 'stripxws' ],
			[ [ 'calendar_id' ], 'disallow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);


	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		my $access = $self->CheckCalendarAccess($user_id, $values->{calendar_id});
		print STDERR "access = $access\n" if ($debug);
		if ($access > 1) {
			$self->DBWriteEvent($user_id, 1, $values);
			$data->{status} = "OK";
		}
		else {
			$data->{status} = "KO";
		}
	}

	return $data;
}
#===============================================================================

=head2 SuppressEvent

	Suppress an existing event.

=cut

# ============================================================================
sub SuppressEvent {
	my ($self, $context) = @_;
	print STDERR "Chronos::SuppressEvent\n" if ($debug);

	my $params = [	
			[ [ 'rowid' ], 'disallow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $user_id = $context->GetUser()->Get('rowid');

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $calendar_id = $self->DBGetCalendarOfEvent($values->{rowid});
		my $access = $self->CheckCalendarAccess($user_id, $calendar_id);
		print STDERR "access = $access\n" if ($debug);
		if ($access > 1) {
			$self->DBDeleteEvent($values->{rowid});
			$data->{status} = "OK";
		}
		else {
			$data->{status} = "KO";
		}
	}

	return $data;
}
#===============================================================================

=head2 SetProjectTask

	Change parameters for given project task

=cut

# ============================================================================
sub SetProjectTask {
	my ($self, $context) = @_;
	print STDERR "Chronos::SetProjectTask\n" if ($debug);

	my $params = [	
			[ [ 'rowid' ], 'disallow_empty', 'want_int' ],
			[ [ 'dstart', 'dend' ], 'allow_empty', 'stripxws' ],
			[ [ 'show_event', 'type'  ], 'disallow_empty', 'stripxws' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "values = " . Dumper($values) . "\n" if ($debug);
	print STDERR "errors = " . Dumper($errors) . "\n" if ($debug);

	my $data = {};
	$data->{errors} = $errors;
	if (scalar(@$errors) > 0) {
		$data->{status} = "KO";
	}
	else {
		my $user_id = $context->GetUser()->Get('rowid');
		my $task = $self->DBSetProjectTask($user_id, $values->{rowid}, $values);
		if (defined($task)) {
			$data->{status} = "OK";
			$data->{task} = $task;
		}
		else {
			$data->{status} = "KO";
		}
	}

	return $data;
}
# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

=head2 InitApp

	Initialize application parameters

=cut

# ============================================================================
sub InitApp {
	my ($self, $context) = @_;
	print STDERR "Chronos::InitApp\n" if ($debug);

	$self->{mioga_id} = $context->GetConfig()->GetMiogaId();
	$self->{app_id} = $context->GetAppDesc()->GetRowid();
	print STDERR "mioga_id = ".$self->{mioga_id}."   app_id = ".$self->{app_id}."\n" if ($debug);
}
# ============================================================================

=head2 DBSetVisibility

	Record in DB the visible flag for calendar

=cut

# ============================================================================
sub DBSetVisibility {
	my ($self, $user_id, $params) = @_;
	print STDERR "Chronos::DBSetVisibility\n" if ($debug);

	my $visible = 't';
	$visible = 'f' if ($params->{visible} eq 'false');

	my $sql = "UPDATE user2cal set visible = ? WHERE user_id = ? AND calendar_id = ?";
	$self->{db}->ExecSQL($sql, [$visible, $user_id, $params->{calendar_id}]);
}
# ============================================================================

=head2 DBWritePreferences

	Write preferences to database function of group or user access.

=cut

# ============================================================================
sub DBWritePreferences {
	my ($self, $user_id, $group_id, $params) = @_;
	print STDERR "Chronos::DBWritePreferences\n" if ($debug);

	my $sql;
	my $res;

	my $showwe = 't';
	if ($params->{showweekend} == 0) {
		$showwe = 'f';
	}

	# User preferences
	if ($user_id == $group_id) {
		$sql = "UPDATE chronos_user_pref set currentCalId=?, currentView=?, showweekend = ?, firsthour=?, colorscheme=?  WHERE user_id = $user_id";
		$self->{db}->ExecSQL($sql, [$params->{currentCalId}, $params->{currentView}, $showwe, $params->{firsthour}, $params->{colorscheme}]);
	}
	# Group preferences
	else {
		$sql = "UPDATE chronos_group_pref set currentCalId=?, currentView=?, showweekend = ?, firsthour=?, colorscheme=? WHERE group_id = $group_id";
		$self->{db}->ExecSQL($sql, [$params->{currentCalId}, $params->{currentView}, $showwe, $params->{firsthour}, $params->{colorscheme}]);
	}
}
# ============================================================================

=head2 DBReadPreferences

	Read preferences from database function of group or user access.

=cut

# ============================================================================
sub DBReadPreferences {
	my ($self, $user_id, $group_id) = @_;
	print STDERR "Chronos::DBReadPreferences\n" if ($debug);

	my $sql;
	my $res;

	# User preferences
	if ($user_id == $group_id) {
		$sql = "SELECT * FROM chronos_user_pref WHERE user_id = ?";
		$res = $self->{db}->SelectSingle($sql, [$user_id]);
		if (! defined($res)) {
			$sql = "INSERT INTO chronos_user_pref (user_id) VALUES ($user_id)";
			$self->{db}->ExecSQL($sql);
			$sql = "SELECT * FROM chronos_user_pref WHERE user_id = ?";
			$res = $self->{db}->SelectSingle($sql, [$user_id]);
		}
	}
	# Group preferences
	else {
		$sql = "SELECT * FROM chronos_group_pref WHERE group_id = ?";
		$res = $self->{db}->SelectSingle($sql, [$group_id]);
		if (! defined($res)) {
			$sql = "INSERT INTO chronos_group_pref (group_id) VALUES ($group_id)";
			$self->{db}->ExecSQL($sql);
			$sql = "SELECT * FROM chronos_group_pref WHERE group_id = ?";
			$res = $self->{db}->SelectSingle($sql, [$group_id]);
		}
	}

	return $res;
}
# ============================================================================

=head2 DBGetGroupCalendar

	Read group calendar parameters.

=cut

# ============================================================================
sub DBGetGroupCalendar {
	my ($self, $group_id) = @_;
	print STDERR "Chronos::DBGetGroupCalendar\n" if ($debug);

	my $sql = "SELECT calendar.*, group2cal.* FROM calendar, group2cal WHERE group2cal.calendar_id=calendar.rowid AND  group2cal.group_id = ?";
	my $cal = $self->{db}->SelectSingle($sql, [$group_id]);

	return $cal;
}
# ============================================================================

=head2 DBGetUserCalendars

	Read user calendar list

=cut

# ============================================================================
sub DBGetUserCalendars {
	my ($self, $user_id) = @_;
	print STDERR "Chronos::DBGetUserCalendars\n" if ($debug);

	# Get calendars accessed by user
	my $sql = "SELECT calendar.*, user2cal.* FROM calendar, user2cal WHERE user2cal.calendar_id=calendar.rowid AND user2cal.user_id = ? ORDER BY user2cal.ident";
	my $cals = $self->{db}->SelectMultiple($sql, [$user_id]);
}
# ============================================================================

=head2 DBGetOtherCalendars

	Read other users calendar list

=cut

# ============================================================================
sub DBGetOtherCalendars {
	my ($self, $user_id) = @_;
	print STDERR "Chronos::DBGetOtherCalendars\n" if ($debug);

	# Get calendars with access rights for user and not owned by user
	my $sql = "SELECT calendar.*, authzTestAccessForDocument(calendar.document_id, m_user_base.rowid) AS access"
						.", owner.email, owner.firstname, owner.lastname, user2cal.ident"
				." FROM calendar, m_user_base, m_user_base AS owner, user2cal"
				." WHERE authzTestAccessForDocument(calendar.document_id, m_user_base.rowid) > 0"
						." AND m_user_base.rowid = ?"
						." AND owner.rowid != ?"
						." AND calendar.rowid=user2cal.calendar_id"
						." AND user2cal.user_id = owner.rowid"
						." AND calendar.owner_type = 1"
				." ORDER BY owner.email";
	print STDERR "sql = $sql\n" if ($debug);
	my $res = $self->{db}->SelectMultiple($sql, [$user_id, $user_id]);
	return $res;
}
# ============================================================================

=head2 DBGetGroupsWithCalendar

	Get the list of groups with Chronos activated and with acces for user on it
	Returns a hash with group_id as key. Each element contains :
	- calendar_id
	- ident of group
	- access level (1 or 2)

=cut

# ============================================================================
sub DBGetGroupsWithCalendar {
	my ($self, $user_id) = @_;
	print STDERR "Chronos::DBGetGroupsWithCalendar\n" if ($debug);

	my $sql = "SELECT group2cal.calendar_id as calendar_id, t1.group_id, t1.function_ident, m_group.ident FROM (SELECT * FROM m_profile_user_function WHERE app_ident='Chronos' AND user_id=? UNION SELECT * FROM m_profile_expanded_user_functio WHERE app_ident='Chronos' AND user_id=?) AS t1  LEFT JOIN group2cal ON group2cal.group_id = t1.group_id, m_group WHERE t1.group_id = m_group.rowid";
	print STDERR "sql = $sql\n" if ($debug);
	my $res = $self->{db}->SelectMultiple($sql, [$user_id, $user_id]);
	print STDERR Dumper($res) if ($debug);
	my $groups = {};
	foreach my $r (@$res) {
		my $access;
		if ($r->{function_ident} eq 'Write') {
			$access = 2;
		}
		else {
			$access = 1;
		}
		if (exists($groups->{$r->{group_id}}->{access})) {
			if ($groups->{$r->{group_id}}->{access} < $access) {
				$groups->{$r->{group_id}}->{access} = $access;
			}
		}
		else {
			$groups->{$r->{group_id}}->{access} = $access;
		}
		$groups->{$r->{group_id}}->{calendar_id} = $r->{calendar_id};
		$groups->{$r->{group_id}}->{ident} = $r->{ident};
	}
	foreach my $g_id (keys(%$groups)) {
		# if group calendar doesn't exists, create it
		if (! defined($groups->{$g_id}->{calendar_id})) {
			$self->DBCreateGroupCalendar($g_id, $groups->{$g_id}->{ident});
			my $cal = $self->DBGetGroupCalendar($g_id);
			if (!defined($cal)) {
				throw Mioga2::Exception("Chronos::DBGetGroupsWithCalendar", __("Cannot create and get group calendar for group !"));
			}
			$groups->{$g_id}->{calendar_id} = $cal->{rowid};
		}
	}
	print STDERR Dumper($groups) if ($debug);
	return $groups;
}
# ============================================================================

=head2 DBCreateUserCalendar

	Create a user calendar

=cut

# ============================================================================
sub DBCreateUserCalendar {
	my ($self, $user_id, $params) = @_;
	print STDERR "Chronos::DBCreateUserCalendar\n" if ($debug);
	my $calendar;

	$self->{db}->BeginTransaction();
    try {
		# Create a document and grant access to owner in read/write mode
		#
		my $document_url = "/user/Chronos/".$user_id;
		my $document = Mioga2::Document->Create({ db_obj => $self->{db},
		                                           url    => $document_url,
												   app_id => $self->{app_id},
												   mioga_id => $self->{mioga_id}
												});
		my $document_id = $document->GetRowid();
		print STDERR "document_id = $document_id\n" if ($debug);
		$document->GrantAccessToUser($user_id, 2);
		#
		# Then, create the calendar and set option for user
		#
		my $sql = "INSERT INTO calendar (created, modified, owner_type, document_id) VALUES (now(), now(), 1, ?)";
		$self->{db}->ExecSQL($sql, [$document_id]);
		my $cal_id = $self->{db}->GetLastInsertId('calendar');
		$sql = "INSERT INTO user2cal (user_id, calendar_id, type, ident, color, bgcolor) VALUES (?, ?, 0, ?, ?, ?)";
		$self->{db}->ExecSQL($sql, [$user_id, $cal_id, $params->{ident}, $params->{color}, $params->{bgcolor}]);
		#
		# Finally, read calendar data for returning to caller
		#
		$sql = "SELECT calendar.*, user2cal.* FROM calendar, user2cal WHERE user2cal.calendar_id=calendar.rowid AND calendar.rowid = ?";
		my $res = $self->{db}->SelectSingle($sql, [$cal_id]);
		if (defined ($res)) {
			$calendar->{ident} = $res->{ident};
			$calendar->{bgcolor} = $res->{bgcolor};
			$calendar->{color} = $res->{color};
			$calendar->{owner_type} = $res->{owner_type};
			$calendar->{type} = $res->{type};
			$calendar->{rowid} = $cal_id;
			$calendar->{access} =  2;  # user have write access on its calendar
		}
		else {
			throw Mioga2::Exception("Chronos::DBCreateUserCalendar", __("Error in getting parameters for newly created calendar"));
		}
	}
	otherwise {
		my $ex = shift;
		warn Dumper($ex);

		$self->{db}->RollbackTransaction();
		#$ex->throw();
		return undef;
	};
	$self->{db}->EndTransaction();
	return $calendar;
}
# ============================================================================

=head2 DBDestroyUserCalendar

	Create a user calendar

=cut

# ============================================================================
sub DBDestroyUserCalendar {
	my ($self, $user_id, $calendar_id) = @_;
	print STDERR "Chronos::DBDestroyUserCalendar\n" if ($debug);
	my $calendar;

	$self->{db}->BeginTransaction();
    try {
		# Verify user acces on given calendar
		my $sql = "SELECT calendar.rowid, calendar.document_id FROM calendar, user2cal"
					." WHERE calendar.owner_type = 1"    # verify owner_type is user
							." AND user2cal.type = 0"  # verify user is owner
							." AND user2cal.user_id = ?" # $owner_id
							." AND user2cal.calendar_id = calendar.rowid"
							." AND calendar.rowid = ?"; # $calendar_id
		print STDERR "sql = $sql\n" if ($debug);
		my $res = $self->{db}->SelectSingle($sql, [$user_id, $calendar_id]);
		if (! defined($res)) {
			throw Mioga2::Exception("Chronos::DBDestroyUserCalendar", __("Given calendar_id not owned by user_id"));
		}

		# Delete all events
		$sql = "DELETE FROM cal_event WHERE calendar_id = ?";
		$self->{db}->ExecSQL($sql, [$calendar_id]);
		# delete accestocal
		$sql = "DELETE FROM user2cal WHERE calendar_id = ?";
		$self->{db}->ExecSQL($sql, [$calendar_id]);
		#delete calendar
		$sql = "DELETE FROM calendar WHERE rowid = ?";
		$self->{db}->ExecSQL($sql, [$calendar_id]);
		# delete document and access rights
		Mioga2::Document->Delete({ db_obj => $self->{db}, rowid => $res->{document_id} });
	}
	otherwise {
		my $ex = shift;
		warn Dumper($ex);

		$self->{db}->RollbackTransaction();
		#$ex->throw();
		return undef;
	};
	$self->{db}->EndTransaction();
	return 1;
}
# ============================================================================

=head2 DBCreateGroupCalendar

	Create a group calendar

=cut

# ============================================================================
sub DBCreateGroupCalendar {
	my ($self, $group_id, $ident) = @_;
	print STDERR "Chronos::DBCreateGroupCalendar  group_id = $group_id  ident = $ident\n" if ($debug);

	$self->{db}->BeginTransaction();
    try {
		my $document_url = "/group/Chronos/".$group_id;
		my $document = Mioga2::Document->Create({ db_obj => $self->{db},
		                                           url    => $document_url,
												   app_id => $self->{app_id},
												   mioga_id => $self->{mioga_id}
												});
		my $document_id = $document->GetRowid();
		print STDERR "document_id = $document_id\n" if ($debug);
		# TODO Set access for group profiles ?

		my $sql = "INSERT INTO calendar (created, modified, owner_type, document_id) VALUES (now(), now(), 2, ?)";
		print STDERR "sql = $sql\n" if ($debug);
		$self->{db}->ExecSQL($sql, [$document_id]);
		my $cal_id = $self->{db}->GetLastInsertId('calendar');
		$sql = "INSERT INTO group2cal (group_id, calendar_id, ident) VALUES (?, ?, ?)";
		print STDERR "sql = $sql\n" if ($debug);
		$self->{db}->ExecSQL($sql, [$group_id, $cal_id, $ident]);
	}
	otherwise {
		my $ex = shift;
		warn Dumper($ex);

		$self->{db}->RollbackTransaction();
		$ex->throw();
	};
	$self->{db}->EndTransaction();
}
# ============================================================================

=head2 DBConnectCalendar

	Connect an existing calendar to user

=cut

# ============================================================================
sub DBConnectCalendar {
	my ($self, $user_id, $params, $access) = @_;
	print STDERR "Chronos::DBConnectCalendar\n" if ($debug);

	my $calendar;
	$self->{db}->BeginTransaction();
    try {
		my $sql = "INSERT INTO user2cal (user_id, calendar_id, type, ident, color, bgcolor) VALUES (?, ?, ?, ?, ?, ?)";
		$self->{db}->ExecSQL($sql, [$user_id, $params->{calendar_id}, $params->{type}, $params->{ident}, $params->{color}, $params->{bgcolor}]);

		$sql = "SELECT calendar.*, user2cal.* FROM calendar, user2cal WHERE user2cal.calendar_id=calendar.rowid AND calendar.rowid = ? AND user2cal.user_id = ?";
		my $res = $self->{db}->SelectSingle($sql, [$params->{calendar_id}, $user_id]);
		if (defined ($res)) {
			$calendar->{ident} = $res->{ident};
			$calendar->{bgcolor} = $res->{bgcolor};
			$calendar->{color} = $res->{color};
			$calendar->{owner_type} = $res->{owner_type};
			$calendar->{type} = $res->{type};
			$calendar->{rowid} = $res->{rowid};
			$calendar->{access} = $access;
		}
		else {
			throw Mioga2::Exception("Chronos::DBConnectCalendar", __("Error in getting parameters for newly created calendar"));
		}
	}
	otherwise {
		my $ex = shift;
		warn Dumper($ex);

		$self->{db}->RollbackTransaction();
		$ex->throw();
	};
	$self->{db}->EndTransaction();
	return $calendar;
}
# ============================================================================

=head2 DBDetachCalendar

	Detach the user from calendar.

=cut

# ============================================================================
sub DBDetachCalendar {
	my ($self, $user_id, $calendar_id) = @_;
	print STDERR "Chronos::DBDetachCalendar\n" if ($debug);

	my $sql = "DELETE FROM user2cal WHERE user_id = ? AND calendar_id = ?";
	$self->{db}->ExecSQL($sql, [$user_id, $calendar_id]);
}
# ============================================================================

=head2 DBSetCalendar

	Set calendar options

=cut

# ============================================================================
sub DBSetCalendar {
	my ($self, $user_id, $group_id, $params) = @_;
	print STDERR "Chronos::DBSetCalendar\n" if ($debug);

	$self->{db}->BeginTransaction();
    try {
		my $sql;
		if ($user_id == $group_id) {
			$sql = "UPDATE user2cal set ident = ?, color=?, bgcolor=? WHERE user_id = ? AND calendar_id = ?";
			$self->{db}->ExecSQL($sql, [$params->{ident}, $params->{color}, $params->{bgcolor}, $user_id, $params->{rowid}]);
		}
		else {
			$sql = "UPDATE group2cal set ident = ?, color=?, bgcolor=? WHERE group_id = ?";
			$self->{db}->ExecSQL($sql, [$params->{ident}, $params->{color}, $params->{bgcolor}, $group_id]);
		}
	}
	otherwise {
		my $ex = shift;
		warn Dumper($ex);

		$self->{db}->RollbackTransaction();
		$ex->throw();
	};
	$self->{db}->EndTransaction();
}
# ============================================================================

=head2 DBWriteEvent

	Create or update an event

=cut

# ============================================================================
sub DBWriteEvent {
	my ($self, $user_id, $fl_create, $values) = @_;
	print STDERR "Chronos::DBWriteEvent\n" if ($debug);

	my $event = Mioga2::tools::Convert::JSONToPerl($values->{event});
	print STDERR "event = ".Dumper($event) ."\n" if ($debug);

	$self->{db}->BeginTransaction();
    try {
		my $start = sprintf("%.4d-%.2d-%.2d", $event->{start_year}, $event->{start_month}, $event->{start_day});
		my $end   = sprintf("%.4d-%.2d-%.2d", $event->{end_year}, $event->{end_month}, $event->{end_day});
		my $all_day;
		my $fl_recur;
		if ($event->{all_day}) {
			$start .= " 00:00:00";
			$end .= " 00:00:00";
			$all_day = 't';
		}
		else {
			$start .= sprintf(" %.2d:%.2d:%.2d", $event->{start_hours}, $event->{start_minutes}, $event->{start_seconds});
			$end .= sprintf(" %.2d:%.2d:%.2d", $event->{end_hours}, $event->{end_minutes}, $event->{end_seconds});
			$all_day = 'f';
		}
		if ($event->{fl_recur}) {
			$fl_recur = 't';
		}
		else {
			$fl_recur = 'f';
		}
		my $rrule =  Mioga2::tools::Convert::PerlToJSON($event->{rrule});
		print STDERR "rrule = $rrule\n" if ($debug);

		if ($fl_create) {
			my $sql = "INSERT INTO cal_event (created, modified, user_id, calendar_id, subject, description, dstart, dend, allDay, fl_recur, rrule, category_id)"
							." VALUES (now(), now(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			print STDERR "sql = $sql\n" if ($debug);
			$self->{db}->ExecSQL($sql, [$user_id, $values->{calendar_id}, $event->{subject}, $event->{desc}, $start, $end, $all_day, $fl_recur, $rrule, $event->{category_id}]);
		}
		else {
			my $sql = "UPDATE cal_event set modified=now(), user_id=?, calendar_id=?, subject=?, description=?, dstart=?, dend=?, allDay=?, fl_recur=?, rrule=?, category_id=? WHERE rowid=?";
			print STDERR "sql = $sql\n" if ($debug);
			$self->{db}->ExecSQL($sql, [$user_id, $values->{calendar_id}, $event->{subject}, $event->{desc}, $start, $end, $all_day, $fl_recur, $rrule, $event->{category_id}, $values->{rowid}]);
		}
	}
	otherwise {
		my $ex = shift;
		warn Dumper($ex);

		$self->{db}->RollbackTransaction();
		$ex->throw();
	};
	$self->{db}->EndTransaction();
}
# ============================================================================

=head2 DBDeleteEvent

	Delete an event

=cut

# ============================================================================
sub DBDeleteEvent {
	my ($self, $event_id) = @_;
	print STDERR "Chronos::DBDeleteEvent\n" if ($debug);
	my $sql = "DELETE FROM cal_event WHERE rowid = ?";
	print STDERR "sql = $sql\n" if ($debug);
	$self->{db}->ExecSQL($sql, [$event_id]);
}
# ============================================================================

=head2 DBGetCalendarOfEvent

	Delete an event

=cut

# ============================================================================
sub DBGetCalendarOfEvent {
	my ($self, $event_id) = @_;
	print STDERR "Chronos::DBGetCalendarOfEvent\n" if ($debug);
	my $sql = "SELECT calendar_id FROM cal_event WHERE rowid = ?";
	print STDERR "sql = $sql\n" if ($debug);
	my $res = $self->{db}->SelectSingle($sql, [$event_id]);
	return $res->{calendar_id};
}
# ============================================================================

=head2 DBSearchUserFromMail

	Search and get user data from email address in current instance

=cut

# ============================================================================
sub DBSearchUserFromMail {
	my ($self, $values) = @_;
	print STDERR "Chronos::DBSearchUserFromMail\n" if ($debug);

	my $sql = "SELECT rowid, lastname, firstname, email FROM m_user_base WHERE email = ? AND mioga_id = ?";
	print STDERR "sql = $sql\n" if ($debug);
	my $res = $self->{db}->SelectSingle($sql, [$values->{email}, $self->{mioga_id}]);

	return $res;
}
# ============================================================================

=head2 DBGetTeamList

	Get the team list not affected to given calendar for user

=cut

# ============================================================================
sub DBGetTeamList {
	my ($self, $user_id, $calendar_id) = @_;
	print STDERR "Chronos::DBGetTeamList\n" if ($debug);
	my $res;

	my $doc_id = $self->DBVerifyOwnerOfCalendar($user_id, $calendar_id);
	if (defined($doc_id)) {
		my $sql = "SELECT m_group_base.rowid, m_group_base.ident"
					." FROM m_group_base LEFT JOIN (SELECT team_id FROM doc_authz_team, document, calendar WHERE doc_authz_team.doc_id = document.rowid AND calendar.document_id= document.rowid AND calendar.rowid = ?) AS T2 ON T2.team_id = m_group_base.rowid, m_group_type"
					." WHERE m_group_base.type_id = m_group_type.rowid"
							 ." AND m_group_type.ident = 'team'"
							 ." AND T2.team_id is NULL"
							 ." AND m_group_base.mioga_id = ?";
		print STDERR "sql = $sql\n" if ($debug);
		$res = $self->{db}->SelectMultiple($sql, [$calendar_id, $self->{mioga_id}]);
	}
	else {
		throw Mioga2::Exception("Chronos::DBGetTeamList", __("User not owner of calendar"));
	}
	return $res;
}
# ============================================================================

=head2 DBGetUserList

	Get the user list not affected to given calendar for user

=cut

# ============================================================================
sub DBGetUserList {
	my ($self, $user_id, $calendar_id) = @_;
	print STDERR "Chronos::DBGetTeamList\n" if ($debug);
	my $res;
	my $doc_id = $self->DBVerifyOwnerOfCalendar($user_id, $calendar_id);
	if (defined($doc_id)) {
		my $sql = "SELECT DISTINCT m_user_base.rowid, m_user_base.firstname, m_user_base.lastname, m_user_base.email"
					." FROM m_user_base LEFT JOIN (SELECT user_id FROM doc_authz_user, document, calendar WHERE doc_authz_user.doc_id = document.rowid AND calendar.document_id= document.rowid AND calendar.rowid = ?) AS T2 ON T2.user_id = m_user_base.rowid, m_group_group as GG1, m_group_group as GG2, m_group_base, m_group_type"
					." WHERE m_user_base.rowid = GG1.invited_group_id"
					       ." AND GG1.group_id = GG2.group_id"
						   ." AND GG2.group_id = m_group_base.rowid"
						   ." AND m_group_type.rowid = m_group_base.type_id"
						   ." AND m_group_type.ident = 'group'"
						   ." AND T2.user_id is NULL"
						   ." AND GG2.invited_group_id = ?";
		print STDERR "sql = $sql\n" if ($debug);
		$res = $self->{db}->SelectMultiple($sql, [$calendar_id, $user_id]);
	}
	else {
		throw Mioga2::Exception("Chronos::DBGetUserList", __("User not owner of calendar"));
	}
	return $res;
}
# ============================================================================

=head2 DBVerifyOwnerOfCalendar

	Verify if current user is owner of given calendar

	Return document_id for calendar.

=cut

# ============================================================================
sub DBVerifyOwnerOfCalendar {
	my ($self, $owner_id, $calendar_id) = @_;
	print STDERR "Chronos::DBVerifyOwnerOfCalendar owner_id = $owner_id  calendar_id = $calendar_id\n" if ($debug);
	my $doc_id = undef;
	# Verify if user is owner of calendar
	# To verify IDs, the best is to get the calendar with all parameters and table joint
	# and get the document rowid in return
	my $sql = "SELECT document.rowid FROM document, calendar, user2cal"
				." WHERE calendar.owner_type = 1"    # verify owner_type is user
						." AND user2cal.type = 0"  # verify user is owner
						." AND user2cal.user_id = ?" # $owner_id
						." AND user2cal.calendar_id = calendar.rowid"
						." AND calendar.document_id = document.rowid"
						." AND calendar.rowid = ?"; # $calendar_id
	print STDERR "sql = $sql\n" if ($debug);
	my $res = $self->{db}->SelectSingle($sql, [$owner_id, $calendar_id]);
	print STDERR "res = ".Dumper($res)."\n" if ($debug);
	$doc_id = $res->{rowid} if (defined($res));
		
	return $doc_id;
}
# ============================================================================

=head2 DBGrantAccessToUser

	Grant access to calendar but verify if the user is really owner and if all ids exists

	Return access entry with user infos

=cut

# ============================================================================
sub DBGrantAccessToUser {
	my ($self, $owner_id, $guest_id, $calendar_id, $access) = @_;
	print STDERR "Chronos::DBGrantAccessToUser owner_id = $owner_id  guest_id =  $guest_id  calendar_id = $calendar_id  access = $access\n" if ($debug);
	my $access_list;

	my $doc_id = $self->DBVerifyOwnerOfCalendar($owner_id, $calendar_id);
	if (defined($doc_id)) {
		print STDERR "Got a document with rowid = $doc_id\n" if ($debug);
		my $document = Mioga2::Document->Get( { db_obj => $self->{db},
		                    				   rowid => $doc_id } );
		$document->GrantAccessToUser($guest_id, $access);
		$access_list->{users} = $document->GetUserAccess();
		$access_list->{teams} = $document->GetTeamAccess();
	}
	else {
		throw Mioga2::Exception("Chronos::DBGrantAccessToUser", __("User not owner of calendar"));
	}

	return $access_list;
}
# ============================================================================

=head2 DBGrantAccessToTeam

	Grant access to calendar but verify if the user is really owner and if all ids exists

	Return access entry with team infos

=cut

# ============================================================================
sub DBGrantAccessToTeam {
	my ($self, $owner_id, $team_id, $calendar_id, $access) = @_;
	print STDERR "Chronos::DBGrantAccessToTeam owner_id = $owner_id  team_id =  $team_id  calendar_id = $calendar_id  access = $access\n" if ($debug);
	my $access_list;

	my $doc_id = $self->DBVerifyOwnerOfCalendar($owner_id, $calendar_id);
	if (defined($doc_id)) {
		print STDERR "Got a document with rowid = $doc_id\n" if ($debug);
		my $document = Mioga2::Document->Get( { db_obj => $self->{db},
		                    				   rowid => $doc_id } );
		$document->GrantAccessToTeam($team_id, $access);
		$access_list->{users} = $document->GetUserAccess();
		$access_list->{teams} = $document->GetTeamAccess();
	}
	else {
		throw Mioga2::Exception("Chronos::DBGrantAccessToTeam", __("User not owner of calendar"));
	}

	return $access_list;
}
# ============================================================================

=head2 DBSuppressAccessForObject

	Grant access to calendar but verify if the user is really owner and if all ids exists

	Return access entry with team infos

=cut

# ============================================================================
sub DBSuppressAccessForObject {
	my ($self, $owner_id, $object_id, $object_type, $access_id, $calendar_id) = @_;
	print STDERR "Chronos::DBSuppressAccessForObject owner_id = $owner_id  object_type =  $object_type  object_id = $object_id  access_id = $access_id  calendar_id = $calendar_id\n" if ($debug);
	my $access_list;

	my $doc_id = $self->DBVerifyOwnerOfCalendar($owner_id, $calendar_id);
	if (defined($doc_id)) {
		print STDERR "Got a document with rowid = $doc_id\n" if ($debug);
		my $document = Mioga2::Document->Get( { db_obj => $self->{db},
		                    				   rowid => $doc_id } );
		if ($object_type == 2) {
			$document->RemoveAccessForUser($object_id);
		}
		else {
			$document->RemoveAccessForTeam($object_id);
		}
		$access_list->{users} = $document->GetUserAccess();
		$access_list->{teams} = $document->GetTeamAccess();
	}
	else {
		throw Mioga2::Exception("Chronos::DBSuppressAccessForObject", __("User not owner of calendar"));
	}

	return $access_list;
}
# ============================================================================

=head2 DBGetAccessRightForDocument

	Return an hash with list of users and teams having access to the document

=cut

# ============================================================================
sub DBGetAccessRightForDocument {
	my ($self, $document_id) = @_;
	print STDERR "Chronos::DBGetAccessRightForDocument document_id = $document_id\n" if ($debug);
	my $access_list = {};

	my $document = Mioga2::Document->Get( { db_obj => $self->{db},
		                    				   rowid => $document_id } );
	$access_list->{users} = $document->GetUserAccess();
	$access_list->{teams} = $document->GetTeamAccess();

	return $access_list;
}
# ============================================================================

=head2 DBSetProjectTask

	Write project task attributes for user

=cut

# ============================================================================
sub DBSetProjectTask {
	my ($self, $user_id, $task_id, $values) = @_;
	print STDERR "Chronos::DBSetProjectTask user_id = $user_id\n" if ($debug);
	my $sql;
	if ($values->{type} eq 'r') {
		if ($values->{dstart} eq "") {
			$sql = "UPDATE user2project_task set show_event = ?, dstart = null ,dend = null WHERE user_id = ? AND task_id = ?";
			$self->{db}->ExecSQL($sql, [$values->{show_event}, $user_id, $task_id ]);
		}
		else {
			$sql = "UPDATE user2project_task set show_event = ?, dstart = ?, dend = ? WHERE user_id = ? AND task_id = ?";
			$self->{db}->ExecSQL($sql, [$values->{show_event}, $values->{dstart}, $values->{dend}, $user_id, $task_id ]);
		}
	}
	else {
		$sql = "UPDATE attendee2project_task set show_event = ? WHERE user_id = ? AND task_id = ?";
		$self->{db}->ExecSQL($sql, [$values->{show_event}, $user_id, $task_id ]);
	}
	$sql = "SELECT * FROM user_project_task_view WHERE user_id = ? AND rowid = ?";
	my $res = $self->{db}->SelectSingle($sql, [$user_id, $task_id]);
	return $res;
}
# ============================================================================

=head2 DBGetProjectTasksForUser

	Return an array with list of project tasks affected to user.

=cut

# ============================================================================
sub DBGetProjectTasksForUser {
	my ($self, $user_id) = @_;
	print STDERR "Chronos::DBGetProjectTasksForUser user_id = $user_id\n" if ($debug);

	my $sql = "SELECT * FROM user_project_task_view WHERE user_id = ?";
	my $res = $self->{db}->SelectMultiple($sql, [$user_id]);

	return $res;
}
# ============================================================================

=head2 DBExportICAL

	Return an array with list of project tasks affected to user.

=cut

# ============================================================================
sub DBExportICAL {
	my ($self, $user_id, $calendar_id) = @_;
	print STDERR "Chronos::DBExportICAL user_id = $user_id  calendar_id = $calendar_id\n" if ($debug);

	my $ical = Data::ICal->new();

	my $sql = "SELECT cal_event.*, task_category.name as category FROM cal_event, task_category WHERE calendar_id = ? AND task_category.rowid = cal_event.category_id";
	my $res = $self->{db}->SelectMultiple($sql, [$calendar_id]);
	foreach my $e (@$res) {
		print STDERR "start = ".$e->{dstart}."  end = ".$e->{dend}."\n" if ($debug);
		my $vevent = Data::ICal::Entry::Event->new();

		my $dtcreated;
		my $dtmodified;
		my $dtmp;
		$dtmp = Mioga2::Classes::Time->FromPSQL($e->{created});
		$dtcreated = DateTime->new(year => $dtmp->year, month  => $dtmp->mon, day => $dtmp->mday,
								hour => $dtmp->hour, minute  => $dtmp->min, second => $dtmp->sec, time_zone => 'UTC');
		$dtmp = Mioga2::Classes::Time->FromPSQL($e->{modified});
		$dtmodified = DateTime->new(year => $dtmp->year, month  => $dtmp->mon, day => $dtmp->mday,
								hour => $dtmp->hour, minute  => $dtmp->min, second => $dtmp->sec, time_zone => 'UTC');


		$vevent->add_properties(
			summary         => $e->{subject},
			description     => $e->{description},
			created         => DateTime::Format::ICal->format_datetime($dtcreated),
			'last-modified' => DateTime::Format::ICal->format_datetime($dtmodified),
			categories      => $e->{category},
		);

		# dtstart & dtend
		my $dstart = Mioga2::Classes::Time->FromPSQL($e->{dstart});
		my $dend = Mioga2::Classes::Time->FromPSQL($e->{dend});
		my $dtstart;
		my $dtend;
		my $ical_start;
		my $ical_end;
		if ($e->{allday}) {
			$dtstart = DateTime->new(year => $dstart->year, month  => $dstart->mon, day => $dstart->mday);
			$dtend = DateTime->new(year => $dend->year, month  => $dend->mon, day => $dend->mday);
			# dtend is set on 00:00:00 on the last day of period so we must set it one day later
			# to be displayed correctly
			my $dur = DateTime::Duration->new(days => 1);
			$dtend = $dtend + $dur;
			$ical_start = substr DateTime::Format::ICal->format_datetime($dtstart),0, 8;
			$ical_end = substr DateTime::Format::ICal->format_datetime($dtend),0, 8;
			$vevent->add_properties(
				dtstart         => [ $ical_start, { VALUE => 'DATE' } ],
				dtend           => [ $ical_end, { VALUE => 'DATE' } ]
			);
		}
		else {
			$dtstart = DateTime->new(year => $dstart->year, month  => $dstart->mon, day => $dstart->mday,
								hour => $dstart->hour, minute  => $dstart->min, second => $dstart->sec, time_zone => 'UTC');
			$dtend = DateTime->new(year => $dend->year, month  => $dend->mon, day => $dend->mday,
								hour => $dend->hour, minute  => $dend->min, second => $dend->sec, time_zone => 'UTC');
			$ical_start = DateTime::Format::ICal->format_datetime($dtstart);
			$ical_end = DateTime::Format::ICal->format_datetime($dtend);
			$vevent->add_properties(
				dtstart         => $ical_start,
				dtend           => $ical_end,
			);
		}

    	$ical->add_entry($vevent);
	}

	print STDERR "ICAL : ".$ical->as_string."\n" if ($debug);
	return $ical->as_string;
}
# ============================================================================

=head2 CheckCalendarAccess ($id, $user_id, $group_id)

	Get the access right for given calendar :
	0 : none
	1 : read
	2 : write

=cut

# ============================================================================
sub CheckCalendarAccess {
	my ($self, $user_id, $calendar_id) = @_;
	print STDERR "Chronos::CheckCalendar access\n" if ($debug);
	my $sql = "SELECT testaccessoncalendar(?,?) as access";
	my $res = $self->{db}->SelectSingle($sql, [$calendar_id, $user_id]);
	print STDERR "access res= ".Dumper($res) if ($debug);
	if (defined($res)) {
		return $res->{access};
	}
	return 0; # default to no access
}

# ============================================================================

=head2 RevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

	Remove user data in database when a user is revoked of a group.

	This function is called when :
	- a user is revoked of a group
	- a team is revoked of a group for each team member not namly
	  invited in group
	- a user is revoked of a team  for each group where the team is
	  member and the user not namly invited in group

=cut

# ============================================================================

sub RevokeUserFromGroup {
	my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;

	print STDERR "Chronos::RevokeUserFromGroup\n" if ($debug);
	my $sql;
	# TODO verify

	$sql = "DELETE FROM user2cal USING group2cal WHERE user2cal.calendar_id=group2cal.calendar_id AND group2cal.group_id = ? AND user2cal.user_id = ?";
	$self->{db}->ExecSQL($sql, [$group_id, $user_id]);
	# the events of revoked user from calendar group must be updated. They must belong to the group animator
	$self->{db}->ExecSQL("UPDATE cal_event SET user_id = ? WHERE rowid IN ( SELECT rowid FROM cal_event WHERE user_id=? AND calendar_id IN (SELECT calendar_id FROM group2cal WHERE group_id=?))",[$group_animator_id, $user_id, $group_id]);
}


# ============================================================================

=head2 RevokeTeamFromGroup ($config, $group_id, $team_id)

	Remove team data in database when a team is revoked of a group.

	This function is called when :
	- a team is revoked of a group

=cut

# ============================================================================

sub RevokeTeamFromGroup {
	my ($self, $config, $group_id, $team_id) = @_;
	print STDERR "Chronos::RevokeTeamFromGroup nothing to do \n" if ($debug);

	#Nothing to do
}


# ============================================================================

=head2 DeleteGroupData ($config, $group_id)

	Remove group data in database when a group is deleted.

	This function is called when a group/user/resource is deleted.

=cut

# ============================================================================

sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;
	print STDERR "Chronos::DeleteGroupData for group_id $group_id\n" if ($debug);
	my $sql;
	my $res;
	my $mode = 0;
	my @calendars;
	my @documents;
	# TODO verify

	# Get calendar list and set mode
	#    mode = 0 => nothing to do
	#    mode = 1 => calendars in user mode exists
	#    mode = 2 => calendars in group mode exists
	#
	$sql = "SELECT rowid, document_id FROM calendar,user2cal WHERE calendar.rowid = user2cal.calendar_id AND user2cal.user_id = ?";
	$res = $self->{db}->SelectMultiple($sql, [$group_id]);
	if (scalar(@$res) > 0) {
		$mode = 1;
		map {push @calendars, $_->{rowid}; push @documents, $_->{document_id};} @$res;
	}
	if ($mode == 0) {
		$sql = "SELECT rowid, document_id FROM calendar,group2cal WHERE calendar.rowid = group2cal.calendar_id AND group2cal.group_id = ?";
		$res = $self->{db}->SelectMultiple($sql, [$group_id]);
		if (scalar(@$res) > 0) {
			$mode = 2;
			map {push @calendars, $_->{rowid}; push @documents, $_->{document_id};} @$res;
		}
	}
	print STDERR " mode $mode\n" if ($debug);
	print STDERR " calendars = ".Dumper(\@calendars)."\n" if ($debug);
	print STDERR " documents = ".Dumper(\@documents)."\n" if ($debug);

	if ($mode == 0) {
		print STDERR "Nothing to do\n" if ($debug);
		return;
	}

	my $cal_list = join(",", @calendars);
	print STDERR " cal_list $cal_list\n" if ($debug);

	# delete preferences and relations
	if ($mode == 1) {
		$sql = "DELETE FROM user2cal WHERE  calendar_id in ($cal_list)";
		$res = $self->{db}->ExecSQL($sql);
		$sql = "DELETE FROM chronos_user_pref WHERE  user_id = ?";
		$res = $self->{db}->ExecSQL($sql, [$group_id]);
	}
	elsif ($mode == 2) {
		$sql = "DELETE FROM group2cal WHERE  calendar_id in ($cal_list)";
		$res = $self->{db}->ExecSQL($sql);
		$sql = "DELETE FROM chronos_group_pref WHERE  group_id = ?";
		$res = $self->{db}->ExecSQL($sql, [$group_id]);
	}
	else {
		throw Mioga2::Exception("Chronos::DeleteGroupData", __("mode has an incorrect value."));
	}

	# Delete events and calendars
	$sql = "DELETE FROM cal_event WHERE calendar_id in ($cal_list)";
	$res = $self->{db}->ExecSQL($sql);
	$sql = "DELETE FROM calendar WHERE rowid in ($cal_list)";
	$res = $self->{db}->ExecSQL($sql);

	# delete documents
	foreach my $doc_id (@documents) {
		Mioga2::Document->Delete( { db_obj => $self->{db}, rowid => $doc_id } );
	}

	# delete events and calendars
	$sql = "DELETE FROM cal_event WHERE  calendar_id in ($cal_list)";
	$res = $self->{db}->ExecSQL($sql);
	$sql = "DELETE FROM calendar WHERE  rowid in ($cal_list)";
	$res = $self->{db}->ExecSQL($sql);
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2011, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
