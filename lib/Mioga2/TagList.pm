#===============================================================================
#
#         FILE:  TagList.pm
#
#  DESCRIPTION:  Mioga2 tags management class
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  05/03/2012 16:27
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME
	
TagList.pm: Mioga2 tags management class.

=head1 DESCRIPTION

	This class handles Mioga2 tags that can be applied to Mioga2 objects.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::TagList;

use Error qw/:try/;
use Data::Dumper;

my $debug = 0;


#===============================================================================

=head2 new

Create a Mioga2::TagList object.

=over

=item B<arguments:>

=over

=item I<$config:> a Mioga2::Config object matching the instance to work into.

=back

=item B<return:> a Mioga2::TagList object corresponding to the specified instance.

=back

=cut

#===============================================================================
sub new {
	my ($class, $config) = @_;
	print STDERR "[Mioga2::TagList::new] Entering\n" if ($debug);

	my $self = { };
	bless($self, $class);

	$self->{config} = $config;

	print STDERR "[Mioga2::TagList::new] Leaving\n" if ($debug);
	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 GetTagLists

Get tag lists for an instance

=cut

#===============================================================================
sub GetTagLists {
	my ($self) = @_;
	print STDERR "[Mioga2::TagList::GetTagLists] Entering." if ($debug);

	my $config = $self->{config};
	my $db = $config->GetDBObject ();

	my $list = $db->SelectMultiple ('SELECT m_tag_catalog.rowid, m_tag_catalog.ident, count (tag) AS tags FROM m_tag_catalog LEFT JOIN m_tag ON m_tag.catalog_id = m_tag_catalog.rowid WHERE m_tag_catalog.mioga_id = ? GROUP BY m_tag_catalog.rowid, m_tag_catalog.ident', [$config->GetMiogaId ()]);

	print STDERR "[Mioga2::TagList::GetTagLists] Leaving. Data: " . Dumper $list if ($debug);
	return ($list);
}	# ----------  end of subroutine GetTagLists  ----------


#===============================================================================

=head2 GetTags

Get tags from list

=over

=item B<arguments:>

=over

=item I<$catalog_id:> the catalog to get tags from (optional).

=back

=item B<return:> an array of tags.

=back

=cut

#===============================================================================
sub GetTags {
	my ($self, $catalog_id) = @_;
	print STDERR "[Mioga2::TagList::GetTags] Entering." if  ($debug);

	my $config = $self->{config};
	my $db = $config->GetDBObject ();

	my $sql = 'SELECT m_tag.tag FROM m_tag, m_tag_catalog WHERE m_tag.catalog_id = m_tag_catalog.rowid AND m_tag_catalog.mioga_id = ?';
	my $args = [$config->GetMiogaId ()];
	if ($catalog_id) {
		$sql .= ' AND m_tag_catalog.rowid = ?';
		push (@$args, $catalog_id);
	}
	$sql .= ' ORDER BY m_tag.tag';

	my @list = map { $_->{tag} } @{$db->SelectMultiple ($sql, $args)};

	print STDERR "[Mioga2::TagList::GetTags] Leaving. Tags: " . Dumper \@list if  ($debug);
	return (\@list);
}	# ----------  end of subroutine GetTags  ----------


#===============================================================================

=head2 SetTags

Set tag list, overwriting existing list

=over

=item B<arguments:>

=over

=item I<$file> the catalog file name.

=item I<@list> a list of tags.

=back

=item B<return:>

=over

=item I<@errors> a list of errors.

=back

=back

=cut

#===============================================================================
sub SetTags {
	my ($self, $file, $list) = @_;
	print STDERR "[Mioga2::TagList::SetTags] Entering. File: $file, list: " . Dumper $list if  ($debug);

	my $config = $self->{config};
	my $db = $config->GetDBObject ();

	# Remove unwanted characters from tags
	# Need to decode / upgrade because perl < 5.14 doesn't handle correctly regex on unicode data (newer Perl provides a "/u" modifier, see perlre)
	map { utf8::decode ($_); s/[^\x20-\x7f\xa0-\x{2FFFF}]//g; utf8::upgrade ($_); } @$list;

	my @errors = ( );

	# Register catalog
	try {
		$db->ExecSQL ('DELETE FROM m_tag USING m_tag_catalog WHERE m_tag.catalog_id = m_tag_catalog.rowid AND m_tag_catalog.ident = ? AND m_tag_catalog.mioga_id = ?', [$file, $config->GetMiogaId ()]);
		$db->ExecSQL ('DELETE FROM m_tag_catalog WHERE ident = ? AND mioga_id = ?', [$file, $config->GetMiogaId ()]);
		$db->ExecSQL ('INSERT INTO m_tag_catalog (ident, mioga_id, created, modified) VALUES (?, ?, now (), now ());', [$file, $config->GetMiogaId ()]);
	}
	otherwise {
		my $err = shift;
		push (@errors, {error => $err->{'-text'}});
	};

	if (!@errors) {
		my $catalog_id = $db->GetLastInsertId ('m_tag_catalog');

		my $line = 1;
		for my $tag (@$list) {
			try {
				$db->ExecSQL ('INSERT INTO m_tag (tag, catalog_id) VALUES (?, ?);', [$tag, $catalog_id]);
			}
			otherwise {
				my $err = shift;
				push (@errors, {line => $line, tag => $tag, error => $err->{'-text'}});
			};
			$line++;
		}
	}

	print STDERR "[Mioga2::TagList::SetTags] Leaving. Errors: " . Dumper \@errors if  ($debug);
	return (\@errors);
}	# ----------  end of subroutine SetTags  ----------


#===============================================================================

=head2 DeleteCatalog

Delete a tags catalog. This does not delete tags from uri_properties, only the
tags calalog and its associated tags from m_tag.

=cut

#===============================================================================
sub DeleteCatalog {
	my ($self, $rowid) = @_;
	print STDERR "[Mioga2::TagList::DeleteCatalog] Entering, rowid: $rowid\n" if  ($debug);

	my $config = $self->{config};
	my $db = $config->GetDBObject ();

	$db->ExecSQL ('DELETE FROM m_tag WHERE catalog_id = ?', [$rowid]);
	$db->ExecSQL ('DELETE FROM m_tag_catalog WHERE rowid = ?', [$rowid]);

	print STDERR "[Mioga2::TagList::DeleteCatalog] Leaving\n" if  ($debug);
}	# ----------  end of subroutine DeleteCatalog  ----------


#===============================================================================

=head2 GetFreeTagsStatus

Returns a flag indicating wether free tags can be used or not.

=cut

#===============================================================================
sub GetFreeTagsStatus {
	my ($self) = @_;
	print STDERR "[Mioga2::TagList::GetFreeTagsStatus] Entering.\n" if  ($debug);

	my $config = $self->{config};
	my $db = $config->GetDBObject ();

	my $list = $db->SelectMultiple ("SELECT m_tag_catalog.rowid, m_tag_catalog.ident FROM m_tag_catalog WHERE m_tag_catalog.ident = '' AND m_tag_catalog.mioga_id = ?;", [$config->GetMiogaId ()]);

	my $status = scalar (@$list);

	print STDERR "[Mioga2::TagList::GetFreeTagsStatus] Leaving, status: $status.\n" if  ($debug);
	return ($status);
}	# ----------  end of subroutine GetFreeTagsStatus  ----------


#===============================================================================

=head2 EnableFreeTags

Enable free tags for instance

=cut

#===============================================================================
sub EnableFreeTags {
	my ($self) = @_;

	my $config = $self->{config};
	my $db = $config->GetDBObject ();

	$self->SetTags ('', undef);
}	# ----------  end of subroutine EnableFreeTags  ----------


#===============================================================================

=head2 DisableFreeTags

Disable free tags for instance

=cut

#===============================================================================
sub DisableFreeTags {
	my ($self) = @_;

	my $config = $self->{config};
	my $db = $config->GetDBObject ();

	for my $catalog (@{$self->GetTagLists}) {
		next unless ($catalog->{ident} eq '');

		$self->DeleteCatalog ($catalog->{rowid});
		last;
	}
}	# ----------  end of subroutine DisableFreeTags  ----------


#===============================================================================

=head2 RegisterFreeTags

Register unknown tags into virtual free-tags catalog

=cut

#===============================================================================
sub RegisterFreeTags {
	my ($self, $list) = @_;

	if ($self->GetFreeTagsStatus () && @$list) {
		my $config = $self->{config};
		my $db = $config->GetDBObject ();

		my $sql = "INSERT INTO m_tag SELECT nextval('m_tag_rowid_seq'), tags.tag AS tag, m_tag_catalog.rowid AS catalog_id FROM (SELECT " . join ('::text AS tag UNION SELECT ', ('?') x @$list) . "::text AS tag) AS tags, m_tag_catalog WHERE m_tag_catalog.mioga_id = ? AND m_tag_catalog.ident = '' AND tags.tag NOT IN (SELECT m_tag.tag FROM m_tag, m_tag_catalog where m_tag.catalog_id = m_tag_catalog.rowid AND m_tag_catalog.mioga_id = ?);";

		$db->ExecSQL ($sql, [@$list, $config->GetMiogaId (), $config->GetMiogaId ()]);
	}
}	# ----------  end of subroutine RegisterFreeTags  ----------


1;

