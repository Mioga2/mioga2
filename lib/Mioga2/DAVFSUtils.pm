# ============================================================================
# Mioga2 Project (C) 2003-2010 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME

DAVFSUtils.pm: File access for Mioga2 applications (via DAVFS)

=head1 DESCRIPTION

Mioga2::DAVFS is the only point of file access, and it speaks WebDAV.
This module provides convenience functions for Mioga2 applications
that are easier to use than direct WebDAV access.

=head1 EXAMPLE

use Mioga2::DAVFSUtils;
use Mioga2::Old::User;

my $user = Mioga2::Old::User->new($config, login => 'admin');
my $davfsutils = Mioga2::DAVFSUtils->new($config, $user);

if ($davfsutils->IsCollection('/Mioga2/Mioga/home/admin', $user)) {
	print "OK!";
}

=cut

# ============================================================================

package Mioga2::DAVFSUtils;
use strict;
use warnings;
use utf8;
use 5.008;

use APR::Pool ();
use APR::Table ();
use Data::Dumper;
use Error qw(:try);
use File::Basename;
use HTTP::Status;
use Mioga2::DAVFS;
use Mioga2::Exception::Application;
use Mioga2::tools::date_utils;
use Mioga2::tools::string_utils;
use Mioga2::URI;
use Mioga2::XML::Simple;

my $debug = 0;

=head2 new ($config, $user, %opts)

	Create a new Mioga2::DAVFSUtils object. Takes a Mioga2::Config as a parameter,
	and a Mioga2::Old::User object. File operations will be done on behalf of
	that user and with her authorisation rights.
	
	When you don't want notifications to be triggered, give the option 'notify => 0'.

=cut

sub new {
	my ( $class, $config, $user, %opts ) = @_;
	
	my %davfs_opts = (); # 'protocol' => 'http'
	$davfs_opts{notify} = $opts{notify} if exists $opts{notify};
	  
	my $self = { 
		config   => $config,
		davfs    => Mioga2::DAVFS->new($config, %davfs_opts),
		user     => $user,
	};
	bless $self, $class;
	return $self;
}

# Lazy loading of an APR::Pool
sub _apr_pool {
	my ($self) = @_;
	unless (exists $self->{apr_pool}) {
		$self->{apr_pool} = APR::Pool->new();
	}
	return $self->{apr_pool};
}

# Lazy loading of the XML parser
sub _xml {
	my ($self) = @_;
	unless (exists $self->{xml}) {
		$self->{xml} = Mioga2::XML::Simple->new(forcearray => 1);
	}
	return $self->{xml};
}


# If the argument is a scalarref or a scalar, turn it into an IO::File.
sub EnsureHandle {
	my ($clob, $arg) = @_;
	my $result = undef;
	my $reftype = ref($arg);
	
	if ($reftype eq 'SCALAR') {
		my $fh = undef;
		open($fh, '<', $arg);
		$result = IO::File->new_from_fd($fh, 'r');
		
	} elsif ($reftype eq '') {
		unless (-r $arg) {
			throw Mioga2::Exception::Application("Mioga2::DAVFSUtils::ensure_handle",
					"File $arg is not readable");
		}
		$result = IO::File->new($arg);
	}
		
	if ((not defined $result) or not $result->isa('IO::Handle')) {
		throw Mioga2::Exception::Application("Mioga2::DAVFSUtils::ensure_handle",
				"Cannot produce an IO::Handle" . ($reftype eq '' ? " for $arg" : ''));
	}
	return $result;
}


sub _propfind {
	my ($self, $uri, %opts) = @_;
	my $config = $self->{config};
	print STDERR "[Mioga2::DAVFSUtils::_propfind] called for URI $uri\n" if $debug;
	my ($body, $content_type, $status) = ('','','');
	my ($headers_out, $cb);
	my $depth   = $opts{depth} || 0;
	my $body_in = (defined $opts{body_in} ? $self->EnsureHandle($opts{body_in}) : undef);
	
	if (exists $opts{just_the_status}) {
		$cb = Mioga2::DAVFS::Callbacks->new($uri,
				headers_in       => { Depth  => $depth },
				body_in          => $body_in,
				set_status       => \$status);
	} else {
		$headers_out = APR::Table::make($self->_apr_pool, 1);
		
		$cb = Mioga2::DAVFS::Callbacks->new($uri,
			headers_in       => { Depth  => $depth }, 
			headers_out      => $headers_out,
			body_in          => $body_in,
			body_out         => \$body,
			set_content_type => \$content_type,
			set_status       => \$status );
	}

	$self->{davfs}->DAVRequest('PROPFIND', 
			Mioga2::URI->new($config, uri => $uri), $cb, $self->{user});
    print STDERR "[Mioga2::DAVFSUtils::_propfind] returns status $status\n" if $debug > 1;
    return (exists $opts{just_the_status}) ? $status : ($status, $headers_out, \$body);
}


sub _get {
	my ($self, $uri) = @_;
	my ($body, $content_type, $status) = ('','','');
	my $cb = Mioga2::DAVFS::Callbacks->new($uri,
			headers_in       => { Depth  => 0 }, 
			body_out		 => \$body,
			set_content_type => \$content_type,
			set_status       => \$status	);
	
	my $ret = $self->{davfs}->DAVRequest('GET',
			Mioga2::URI->new($self->{config}, uri => $uri),
			$cb, $self->{user});
	return ($status, \$body, $content_type);
}


=head2 IsCollection($uri)

Returns 1 if the given URI is a collection (directory), 0 otherwise.
$uri should be a simple string.

=cut 

# This information could also be looked up in the m_uri table.
sub IsCollection {
	my ($self, $uri) = @_;
	$uri .= '/' unless $uri =~ m#/$#;
	my ($status, undef, $bodyref) = $self->_propfind($uri);
	if (Mioga2::DAVFS::ResponseIsSuccess($status)) {
		my $xml = $self->_xml->XMLin( $$bodyref );
		if (exists $xml->{'D:response'}[0]{'D:propstat'}[0]{'D:prop'}[0]
				{'lp1:resourcetype'}[0]{'D:collection'}) {
			return 1;
		}
	} else {
		print STDERR "[Mioga2::DAVFSUtils::IsCollection] no success (HTTP $status) for URI $uri\n"
				if $debug;
	}
	return 0;
}

=head2 Exists($uri)

Returns 1 if the given URI exists, 0 otherwise.
$uri should be a simple string.

=cut

# This information could also be looked up in the m_uri table.
sub Exists {
	my ($self, $uri) = @_;
	if ( Mioga2::DAVFS::ResponseIsSuccess($self->_propfind($uri, 'just_the_status' => 1)) ) {
		return 1;
	} else {
		return 0;
	}
}


#===============================================================================

=head2 SetHidden

Set hidden property for an URI.

=head3 Incoming Arguments

=over

=item B<$uri> the URI to work on.

=item B<$hidden> a flag, 1 to set the URI as hidden, 0 to set it as visible

=back

=cut

#===============================================================================
sub SetHidden {
	my ($self, $uri, $hidden) = @_;

	my $status = '';
	my $body   = qq~<?xml version="1.0" encoding="utf-8" ?>
		<D:propertyupdate xmlns:D="DAV:"><D:set><D:prop><hidden xmlns="http://webdav.org/cadaver/custom-properties/">$hidden</hidden></D:prop></D:set></D:propertyupdate>~;
	my $cb = Mioga2::DAVFS::Callbacks->new($uri,
			headers_in    => {
				'Content-Length' => length ($body)
			},
			body_in     => \$body,
			set_status  => \$status );
	my $uri_obj = Mioga2::URI->new($self->{config}, uri => $uri);
	$self->{davfs}->DAVRequest('PROPPATCH', $uri_obj, $cb, $self->{user});

	return (Mioga2::DAVFS::ResponseIsSuccess($status));
}	# ----------  end of subroutine SetHidden  ----------


=head2 MakeCollection($uri)

Attempts to create a directory with the given URI.

With option "robust", create intermediary directories if necessary,
and don't report a failure when the directory already exists.

Returns 1 on success, undef on failure.

=cut

sub MakeCollection {
	my ($self, $uri, %opts) = @_;
	my $status = '';
	my $cb = Mioga2::DAVFS::Callbacks->new($uri,
			set_status       => \$status );
	my $uri_obj = Mioga2::URI->new($self->{config}, uri => $uri); 
	$self->{davfs}->DAVRequest('MKCOL', $uri_obj, $cb, $self->{user});

	if (Mioga2::DAVFS::ResponseIsSuccess($status)) {
		print STDERR "[Mioga2::DAVFSUtils::MakeCollection] MKCOL $uri succeeded\n"
				if $debug > 1;
		return 1;
		
	} elsif ($opts{robust} and $status == 404) {
		my $parent = $uri_obj->GetParent();
		if (defined $parent) {
			print STDERR "[Mioga2::DAVFSUtils::MakeCollection] Trying to create parent dir$parent...\n"
					if $debug > 1;
			if ($self->MakeCollection($parent, 'robust' => 1)) {
				return $self->MakeCollection($uri);
			}
		}
		print STDERR "[Mioga2::DAVFSUtils::MakeCollection] Cannot create parent dir for $uri\n";
		return undef;
	
	} elsif ($opts{robust} and $status == 405) {
		print STDERR "[Mioga2::DAVFSUtils::MakeCollection] Ignoring existing directory $uri\n"
				if $debug > 1;
		return 1;
		
	} else {
		print STDERR "[Mioga2::DAVFSUtils::MakeCollection] HTTP $status for MKCOL $uri\n";
		return undef;
	}
}

=head2 GetFile($uri)

Returns the content of $uri in a scalarref, or the HTTP status (as a scalar)
in case of errors.

=cut

sub GetFile {
	my ($self, $uri) = @_;
	my ($status, $bodyref, undef) = $self->_get($uri);
	if (Mioga2::DAVFS::ResponseIsSuccess($status)) {
		return $bodyref;
	} else {
		return $status;
	}
}


=head2 PutFile($uri, $fh, $size)

Create a file under the specified URI.
Its content will be read from $fh, which may be a
IO::Handle, a reference to a scalar with the _content_,
or a scalar with a filename.

A size must only be specified if $fh is an IO::Handle.

Returns 1 on success, undef on failure.

=cut

sub PutFile {
	my ($self, $uri, $body, $size) = @_;
	my ($status) = ('');
	print STDERR "[Mioga2::DAVFSUtils::PutFile] invoked PUT $uri\n"
			if $debug > 1;
	my $body_handle = $self->EnsureHandle($body);
	unless (defined $size) {
		my $reftype = ref($body);
		if ($reftype eq 'SCALAR') {
			$size = length($$body);
		} elsif ($reftype eq '') {
			$size = -s $body;
		} else {
			throw Mioga2::Exception::Application("[Mioga2::DAVFSUtils::PutFile]",
					"Size must be specified!\n");
		}
	}
	
	my $cb = Mioga2::DAVFS::Callbacks->new($uri,
			body_in          => $body_handle,
			headers_in       => { 'Content-Length' => $size }, 
			set_status       => \$status,
			);
	$self->{davfs}->DAVRequest('PUT',
			Mioga2::URI->new($self->{config}, uri => $uri),
			$cb, $self->{user});

	if (Mioga2::DAVFS::ResponseIsSuccess($status)) {
		return 1;
	} else {
		print STDERR "[Mioga2::DAVFSUtils::PutFile] HTTP $status for PUT $uri\n";
		return undef;
	}			
}


=head2 PutCollection($uri, $rootfolder)

Put a whole arborescence of files under $uri.

Returns 1 on full success, undef on (partial) failure (details in the logs).

=cut

sub PutCollection {
	my ($self, $uri, $root) = @_;
	my $success = 1;
	print STDERR "[Mioga2::DAVFSUtils::PutCollection] invoked for $uri.\n"
			if $debug > 1;
	
	unless (-d $root and -r $root) {
		throw Mioga2::Exception::Application("Mioga2::DAVFSUtils::PutCollection",
				"Logic error: $root is not a readable directory\n");
	}
	
	$success = $self->MakeCollection($uri) && $success unless $self->IsCollection($uri);

	my $entries = IO::Dir->new($root);
	while (my $entry = $entries->read()) {
		next if $entry eq '.' or $entry eq '..';
		my $full_entry = "$root/$entry";
		if (-d $full_entry) {
			$success = $self->PutCollection("$uri/$entry", $full_entry) && $success;
		}
		elsif ( -f $full_entry) {
			$success = $self->PutFile("$uri/$entry", $full_entry) && $success;
		}
		else {
			print STDERR "[Mioga2::DAVFSUtils::PutCollection] $full_entry is neither file nor directory\n"
					if $debug;
			$success = 0;
		}
	}
	return $success;
}


=head2 ReadCollection($uri, %opts)

Returns the content of the given directory, or a HTTP status scalar on failure.
If the $uri does point to an existing file, "FILE" is returned.

The entries are returned as a listref of hashrefs. The hashrefs have
at least the fields "name" and "type" (which is "collection" or "file"),
and more if %opts are specified:

* lastmodified => 1 : the field "mod" contains the date of last modification,
in the format 'yyyy-mm-dd hh:mm:ss'.

* short_name => 1 : the field "short_name" contains the file name part.

=cut

# This is a stripped down version of Mioga2::Magellan::DAV::GetCollection.
# TODO: some of the commented code might be necessary or useful to have.
sub ReadCollection
{
	my ($self, $uri, %opts) = @_;
	#my ($context, $userident, $passwd, $base_url, $uri, @extra_props) = @_;
	print STDERR "[Mioga2::DAVFSUtils::ReadCollection] ($uri)\n" if ($debug);
	my $response;
	my $config = $self->{config};
	my $user = $self->{user};
	# my $group = $context->GetGroup();

	# my $private_dav_uri = $config->GetBasePath().'/'.$config->GetMiogaIdent().$config->GetPrivatePath();
	# my $public_dav_uri  = $config->GetBasePath().'/'.$config->GetMiogaIdent().$config->GetPublicPath();

	# my $access_right;

	# $uri =~ s/\/+$//;
	# my $url = $base_url.$uri;
	# print STDERR "uri = $uri\n" if ($debug);

#	my $content = '';
#	$content = '<?xml version="1.0" encoding="utf-8" ?>' .
#				  '<D:propfind xmlns:D="DAV:" xmlns:P="http://webdav.org/cadaver/custom-properties/">' .
#				  '<D:prop>' .
#				  '<D:getcontentlength/>' .
#				  '<D:getlastmodified/>' .
#				  '<D:resourcetype/>' .
#				  '<D:lockdiscovery xmlns="DAV:"/>';
#	# $content .= "<P:$_/>" foreach (@extra_props);
#	$content .=	  '</D:prop>' .
#				  '</D:propfind>';

	# my $encoded = encode_base64("$userident:$passwd");

	my $header = { Depth => 1 };
		#		   Authorization => "Basic $encoded" };
	$uri .= '/' unless $uri =~ m#/$#;
	my ($status, undef, $body) = $self->_propfind($uri, depth => 1); # body_in => \$content);
	
	# $response = DAVRequest('PROPFIND', $uri, $content, $header);
	if (Mioga2::DAVFS::ResponseIsSuccess($status))
	{
		my @file_list;

		print STDERR "xml_response = $$body\n" if ($debug > 2);

		my $xml = new Mioga2::XML::Simple(forcearray => 1);
		my $hash_response  = $xml->XMLin($$body);

		print STDERR Dumper($hash_response) if ($debug > 2);

		DAV_ENTRY: foreach my $n (@{$hash_response->{"D:response"}})
		{
			# Look for properties namespace
 			my $props_ns;

			foreach my $key (keys %$n) {
				next if ref($n->{$key}) ne '';
				next if $n->{$key} ne 'DAV:';
				next if $key !~ /^lp/;				

				$props_ns = $key;
			}
			
			# Look for custom properties namespace
# 			my $custom_props_ns;
#
#			foreach my $key (keys %$n) {
#				next if ref($n->{$key}) ne '';
#				next if $n->{$key} ne 'http://webdav.org/cadaver/custom-properties/';			
#
#				$custom_props_ns = $key;
#			}
			
			my $name = $n->{"D:href"}->[0];
			$name =~ s/\/$//;

			# Name is given by DAV response and is escaped
			$name =~ s/&/%26/g;
			$name =~ s/=/%3D/g;
			my $unesc_name = st_URIUnescape($name);
			
			print STDERR "[Mioga2::DAVFSUtils::ReadCollection] name : $name, uri : $uri, unesc_name : $unesc_name\n" 
					if $debug > 1;
			if (defined($name))
			{
				my $result = {};

				# foreach (keys %{$n->{"D:propstat"}->[0]->{"D:prop"}->[0]})
				# {
				#	if ($_ =~ /$custom_props_ns:(.*)/)
				#	{
				#		my $prop = $1;
				#		$result->{$prop} = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$custom_props_ns:$prop"}->[0];
				#	}
				# }

				$result->{name} = $unesc_name;
				# $result->{name_esc} = $name;
				
				if ($opts{short_name}) {
					my $basename = basename ($unesc_name);
					# short name is the name displayed in web page.
					$result->{short_name} = $basename;
				}
                # short name has to be allowed to be break (ie, use in table)
                #$result->{short_name} =~ s/ /&\#160;/g;
				
				# $result->{length} = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:getcontentlength"}->[0];

				if ($opts{lastmodified}) {
					my $date = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]
							->{"$props_ns:getlastmodified"}->[0];
					if(ref($date) ne '') {
						$date = $date->{content}->[0];
					}
					$result->{mod} = du_EnGMTToISO($date);
				}

				my $type;
				if(exists $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:resourcetype"}) {
					$type = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:resourcetype"}->[0];
				} else {
					$type = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:resourcetype"}->[0];
				}

				if (exists $type->{"D:collection"})
				{
					$result->{type} = 'collection';
					# $result->{locked} = undef;
				}
				else
				{
					$result->{type} = 'file';
#					if (exists($n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:lockdiscovery"}->[0]->{"D:activelock"}))
#					{
#						$result->{locked} = 1;
#						if (exists($n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:lockdiscovery"}->[0]->{"D:activelock"}->[0]->{"ns0:owner"})) {
#							my $owner = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:lockdiscovery"}->[0]->{"D:activelock"}->[0]->{"ns0:owner"}[0]->{"content"}[0];
#
#							my $userobj = new Mioga2::Old::User($config, ident => $owner);
#							$result->{lock_owner} = $userobj->GetName();
#
#							# Check if user has the right to unlock (he is either the lock owner or the group animator)
#							my $uriobj = new Mioga2::URI($config, uri => $result->{name});
#							my $groupobj = $uriobj->GetGroup;
#							my $mygroup;
#							if (defined ($groupobj)) {
#								$mygroup = $groupobj;
#							}
#							else {
#								$mygroup = $group;
#							}
#							my $anim = $group->GetAnimator ();
#
#							if ($owner eq $user->GetIdent ()) {
#								$result->{user_is_lock_owner} = 1;
#							}
#
#							if ($owner eq $user->GetIdent() || $user->GetIdent() eq $anim->GetIdent()) {
#								$result->{unlockable} = 1;
#							}
#							else {
#								$result->{unlockable} = 0;
#							}
#						}
#					}
#					else
#					{
#						$result->{locked} = 0;
#					}
				}
			
				# detect the DAV equivalent of a "." entry
				my $lc_unesc_name = lc($unesc_name);
				my $lc_uri        = lc($uri);
				if ($lc_unesc_name eq $lc_uri or $lc_unesc_name.'/' eq $lc_uri) {
					# break out if "." is no directory
					if ($result->{type} eq 'file') {
						return 'FILE';
					}	
				} else {
					push (@file_list, $result);
				}
			}
		}
#		my $user_id = $user->GetRowid();
#		my ($sql, $rowids, %uris);
#
#		#making sql query string
#		$uri = st_FormatPostgreSQLRegExpString($uri);
#		$sql = "uri SIMILAR TO '$uri/[^/]*\$'";
#		print "sql = $sql\n" if ($debug);
#		
#		my $history_sql = ", (SELECT COUNT(uri_history.rowid) FROM uri_history WHERE uri_id=m_uri.rowid) as history";
#		return \@file_list if (not defined $sql or $sql eq "");
#		my $results = SelectMultiple($config->GetDBH, "SELECT (SELECT COUNT(*) FROM file_comment WHERE uri_id = m_uri.rowid) AS comments, m_uri.rowid, m_uri.uri, m_uri.mimetype, m_user_base.lastname, m_user_base.firstname, m_user_base.ident$history_sql 
#		                                               FROM m_uri, m_user_base 
#		                                               WHERE m_user_base.rowid=m_uri.user_id 
#		                                               AND $sql");
#	
#        my $infos;
#		foreach my $fn (@file_list)
#		{
#			$fn->{name} =~ s/\/$//;
#			$uris{$fn->{name}} = 0;
#		}
#		foreach my $res (@{$results})
#		{
#			if (exists($uris{$res->{uri}})) {
#				$rowids .= "$res->{rowid},";
#				$uris{$res->{rowid}} = $res->{uri};
#				$uris{$res->{uri}} = $res->{rowid};
#				$infos->{$res->{rowid}} = $res;
#			}
#		}
#		$rowids =~ s/,$//;
#		print STDERR "rowids = $rowids\n" if($debug);
#		return \@file_list if ($rowids eq "");
#		
#		#deleting possibly non existing entries
#		warn ("file_liste before = " . Dumper(\@file_list) . "\n") if($debug);
#		foreach (0..$#file_list)
#		{
#			delete $file_list[$_] if (not exists $uris{$file_list[$_]->{name}});
#		}
#		warn ("file_liste after = " . Dumper(\@file_list) . "\n") if($debug);
#
#		#checking permissions
#		my $request = "SELECT uri_id, (max(accesskey)%10) AS accesskey  FROM ( SELECT m_uri.rowid AS uri_id, (50 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_user WHERE  m_uri.rowid IN ($rowids) AND m_authorize.uri_id = m_uri.parent_uri_id AND m_authorize_user.authz_id = m_authorize.rowid AND m_authorize_user.user_id = $user_id UNION SELECT m_uri.rowid AS uri_id, (40 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_team, m_group_group WHERE  m_uri.rowid IN ($rowids) AND m_authorize.uri_id = m_uri.parent_uri_id AND m_authorize_team.authz_id = m_authorize.rowid AND m_group_group.group_id = m_authorize_team.team_id AND m_group_group.invited_group_id = $user_id UNION SELECT m_uri.rowid AS uri_id, (30 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_profile, m_profile_group WHERE  m_uri.rowid IN ($rowids) AND m_authorize.uri_id = m_uri.parent_uri_id AND m_authorize_profile.authz_id = m_authorize.rowid AND m_profile_group.profile_id = m_authorize_profile.profile_id AND m_profile_group.group_id = $user_id UNION SELECT m_uri.rowid AS uri_id, (20 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_profile, m_profile_expanded_user WHERE  m_uri.rowid IN ($rowids) AND m_authorize.uri_id = m_uri.parent_uri_id AND m_authorize_profile.authz_id = m_authorize.rowid AND m_profile_expanded_user.profile_id = m_authorize_profile.profile_id AND m_profile_expanded_user.user_id = $user_id ) AS tmp_req GROUP BY (uri_id)";
#		$results = SelectMultiple($config->GetDBH(), $request);
#	
#		#setting access rights for dav files
#		foreach my $res (@{$results})
#		{
#			foreach my $file (@file_list)
#			{
#				if ($file->{name} eq $uris{$res->{uri_id}})
#				{
#				  my @mime              = split /;/, $infos->{$res->{uri_id}}->{mimetype};
#				  $file->{mimetype}     = $mime[0];
#				  $file->{author}       = $infos->{$res->{uri_id}}->{firstname}." ".$infos->{$res->{uri_id}}->{lastname};
#				  $file->{ident}        = $infos->{$res->{uri_id}}->{ident};
#                  $file->{access_right} = "rw" if ($res->{accesskey} == AUTHZ_WRITE);
#                  $file->{access_right} = "r" if ($res->{accesskey} == AUTHZ_READ);
#                  $file->{access_right} = "" if ($res->{accesskey} == AUTHZ_NONE);
#                  $file->{history}      = $infos->{$res->{uri_id}}->{history} if $infos->{$res->{uri_id}}->{history};
#                  $file->{comments}     = $infos->{$res->{uri_id}}->{comments};
#					last;
#				}
#			}
#		}
		return \@file_list;
	}
	else
	{
		print STDERR "[Mioga2::DAVFSUtils::ReadCollection] HTTP $status for reading $uri\n"
				if $debug;
		return $status;
	}
}


=head2 Delete($uri)

Deletes a file or a whole collection. Returns 1 on success, undef on failure.

=cut

sub Delete {
	my ($self, $uri) = @_;
	my ($status) = ('');
	my $cb = Mioga2::DAVFS::Callbacks->new($uri,
			set_status       => \$status	);
	
	my $ret = $self->{davfs}->DAVRequest('DELETE',
			Mioga2::URI->new($self->{config}, uri => $uri),
			$cb, $self->{user});
	
	if (Mioga2::DAVFS::ResponseIsSuccess($status)) {
		return 1;
	} else {
		print STDERR "[Mioga2::DAVFSUtils::Delete] cannot delete $uri (HTTP $status)\n"
				if $debug;
		return undef;
	}
}


1;
