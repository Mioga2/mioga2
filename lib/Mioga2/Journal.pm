#===============================================================================
#
#         FILE:  Journal.pm
#
#  DESCRIPTION:  Journal of operations for Mioga2.
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  26/03/2012 11:13
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Journal.pm: Journal of operations for Mioga2.

=head1 DESCRIPTION

This module provides facilities for tracking the operations and their
status. It contains, for example, the different steps involved in instance
creation and their status, giving the user an indication of what's going
wrong, if something went wrong.

=head1 SYNOPSIS

=head2 Feeding the journal

 my $journal;
 tie @$journal, 'Mioga2::Journal';
 push (@$journal, {step => 'Step 1', status => 1});
 push (@$journal, {step => 'Step 2', status => 0});

=head2 Adding metadata

 my $o = tied @$journal
 # Add metadata
 $o->SetMetadata ({ key1 => 'value1' });
 # Clear metadata
 $o->SetMetadata ({});

=head2 Exporting the journal

 my $o = tied @$journal;
 print $o->ToJSON ();
 print $o->ToXML ();

=head2 Using callbacks

 my $journal;
 my $o = tie @$journal, 'Mioga2::Journal', { callbacks => { errors => sub { grep { $_->{status} == 0 } @$journal } } };
 push (@$journal, {step => 'Step 1', status => 1});
 push (@$journal, {step => 'Step 2', status => 0});
 print Dumper $o->{errors}->();

=head1 PUBLIC METHODS

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Journal;

require Tie::Array;

use Data::Dumper;

use Mioga2::tools::Convert;

my $debug = 0;

sub TIEARRAY {
	my ($class, $data) = @_;
	print STDERR "[Mioga2::Journal::TIEARRAY] Entering, data: " . Dumper $data if ($debug);

	my $self = {
		ARRAY => []
	};

	# Create shortcut to callbacks
	for my $callback (keys (%{$data->{callbacks}})) {
		$self->{$callback} = $data->{callbacks}->{$callback};
	}

	bless ($self, $class);

	print STDERR "[Mioga2::Journal::TIEARRAY] Leaving\n" if ($debug);
	return ($self);
}

sub FETCHSIZE {
	my ($self) = @_;
	return (scalar (@{$self->{ARRAY}}));
}

sub STORE {
	my ($self, $index, $value) = @_;
	print STDERR "[Mioga2::Journal::STORE] Entering, index: $index, value: " . Dumper $value if ($debug);

	if ((ref ($value) eq 'HASH') && (defined ($self->{metadata}))) {
		for my $key (keys (%{$self->{metadata}})) {
			$value->{$key} = $self->{metadata}->{$key};
		}
	}

	$self->EXTEND ($index) if ($index > $self->FETCHSIZE);
	$self->{ARRAY}->[$index] = $value;

	print STDERR "[Mioga2::Journal::STORE] Leaving, ARRAY: " . Dumper $self->{ARRAY} if ($debug);
}

sub EXTEND {
	my ($self, $count) = @_;
	$self->STORESIZE ($count);
}

sub FETCH {
	my ($self, $index) = @_;
	return ($self->{ARRAY}->[$index]);
}

sub PUSH {
	my ($self, @list) = @_;

	my $last = $self->FETCHSIZE ();
	$self->STORE ($last + $_, $list[$_]) foreach 0..$#list;
}


#===============================================================================

=head2 SetMetadata

Set data that will be automatically appended to data that is added to the journal

=cut

#===============================================================================
sub SetMetadata {
	my ($self, $data) = @_;

	$self->{metadata} = $data;
}	# ----------  end of subroutine SetMetadata  ----------


#===============================================================================

=head2 ToJSON

Translate journal data to JSON

=cut

#===============================================================================
sub ToJSON {
	my ($self) = @_;
	print STDERR "[Mioga2::Journal::ToJSON] Entering\n" if ($debug);

	my $json = Mioga2::tools::Convert::PerlToJSON ($self->{ARRAY});

	print STDERR "[Mioga2::Journal::ToJSON] JSON: $json\n" if ($debug);
	return ($json);
}	# ----------  end of subroutine ToJSON  ----------


#===============================================================================

=head2 ToXML

Translate journal data to XML

=cut

#===============================================================================
sub ToXML {
	my ($self) = @_;
	print STDERR "[Mioga2::Journal::ToXML] Entering\n" if ($debug);

	my $xml = Mioga2::tools::Convert::PerlToXML ($self->{ARRAY});

	print STDERR "[Mioga2::Journal::ToXML] XML: $xml\n" if ($debug);
	return ($xml);
}	# ----------  end of subroutine ToXML  ----------


#===============================================================================

=head2 ToArray

Translate journal data to an array, which is no translation as the journal itself is an array...

=cut

#===============================================================================
sub ToArray {
	my ($self) = @_;

	print STDERR "[Mioga2::Journal::ToArray] Array: " . Dumper $self->{ARRAY} if ($debug);
	return ($self->{ARRAY});
}	# ----------  end of subroutine ToArray  ----------

1;

