#===============================================================================
#
#         FILE:  UserList.pm
#
#  DESCRIPTION:  New generation Mioga2 userlist class
#
# REQUIREMENTS:  ---
#        NOTES:  TODO Rewrite the Store and Delete methods so this module is
#				 	  independant from APIGroup
#				 TODO Handle parent directories creation in CreateDirectory
#				      and CreateFile methods
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  16/04/2010 11:24
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

UserList.pm: New generation Mioga2 userlist class

=head1 DESCRIPTION

This class handles Mioga2 users. It is designed to create, load, update or delete
users.

=head1 SYNOPSIS

 use Mioga2::UserList;
 use Mioga2::Config;
 
 my $userlist;
 my $config = new Mioga2::Config (...);
 
 # Set all users whose firstname is John as autonomous
 $userlist = Mioga2::UserList->new ($config, { attributes => { firstname => 'John' } });
 $userlist->Set ('autonomous', 1);
 $userlist->Store ();
 
 # Delete user whose ident is jdoe
 $userlist = Mioga2::UserList->new ($config, { attributes => { ident => 'jdoe' } });
 $userlist->Delete ();
 
 # Create a new user from attributes and skeleton
 $userlist = Mioga2::UserList->new ($config, {
		attributes => {
 			email => 'jdoe@noname.org',
 			ident => 'jdoe',
 			firstname => 'John',
 			lastname => 'DOE',
 			password => 'secret',
 			skeleton => '/path/to/std_user.xml'
 		}
 	});
 $userlist->Store ();

 # Get users that can run Colbert from their personal space
 $applist = Mioga2::ApplicationList->new ($config, { attributes => { ident => 'Colbert' } });
 $userlist = Mioga2::UserList->new ($config, { attributes => { applications => $apps } });

 # Get users that can run Colbert from group 'Admins'
 $applist = Mioga2::ApplicationList->new ($config, { attributes => { ident => 'Colbert' }  });
 $grouplist = Mioga2::GroupList->new ($config, { attributes => { ident => 'Admins' } });
 $userlist = Mioga2::UserList->new ($config, { attributes => { groups => $grouplist, applications => $apps } });

 # Get users that can run function 'UsersCreate' of application Colbert from any group
 $applist = Mioga2::ApplicationList->new ($config, { attributes => { ident => 'Colbert' } });
 $grouplist = Mioga2::GroupList->new ($config);
 $userlist = Mioga2::UserList->new ($config, { attributes => { groups => $grouplist, applications => $apps, functions => ['UsersCreate'] } });

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::UserList;
use base qw(Mioga2::Old::Compatibility);
use Locale::TextDomain::UTF8 'userlist';

use Data::Dumper;

use File::Copy;
use File::MimeInfo::Magic;

use Locale::TextDomain::UTF8 'userlist';

use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIApplication;
use Mioga2::tools::APIAuthz;
use Error qw(:try);
use Mioga2::Exception::User;
use Mioga2::GroupList;
use Mioga2::TeamList;
use Mioga2::ApplicationList;
use Mioga2::CSV;
use Mioga2::tools::mail_utils;
use Mioga2::tools::args_checker;
use Mioga2::Journal;
use Mioga2::Constants;


my $debug = 0;

my @text_fields = qw/ident firstname lastname email/;

@Mioga2::UserList::ProtectedApplications = qw/Workspace Narkissos/;

#===============================================================================

=head2 new

Create a new Mioga2::UserList object.

=over

=item B<arguments:>

=over

=item I<$config:> a Mioga2::Config object matching the instance to work into.

=item I<$data:> a hashref with the following keys:

=over

=item I<attributes:> a list of key / value pairs matching the users. Key names correspond to m_user_base table field names. Other keys are supported:

=over

=item skeleton: the path to a skeleton file to apply to user(s).

=back

=item I<short_list:> an optional flag to limit the amount of data describing the users.

=item I<fields:> a list of fields describing the users (this overrides the behavior of the flag above).

=item I<match:> a string indicating the match type for users (may be 'begins', 'contains', 'ends' or may be empty for exact match).

=item I<journal:> a Mioga2::Journal-tied-array to record operations.

=back

=back

=item B<return:> a Mioga2::UserList object matching users according to provided attributes.

=back

=cut

#===============================================================================
sub new {
	my ($class, $config, $data) = @_;
	print STDERR "[Mioga2::UserList::new] Entering, data: " . Dumper $data if ($debug);

	my $self = { };
	bless($self, $class);

	$self->{config} = $config;

	# Initialize attributes
	$self->{attributes} = $data->{attributes};

	# Initialize object state
	$self->{loaded} = 0;
	$self->{deleted} = 0;

	# Force e-mail address to lowercase
	$self->{attributes}->{email} = lc ($self->{attributes}->{email}) if ($self->{attributes}->{email});

	# Fields obtained from DB can be expressed in a fuzzy way (short_list) initializing a pre-defined field-list
	if (exists ($data->{short_list}) && $data->{short_list}) {
		# Restricted list of fields
		$self->{fields} = "m_user_base.rowid, m_user_base.ident, m_user_base.firstname, m_user_base.lastname, m_user_base.email, m_user_base.firstname || \' \' || m_user_base.lastname || \' (\' || m_user_base.email || \')\' AS label, m_group_type.ident AS type, m_user_status.ident AS status";
		$self->{short_list} = delete ($data->{short_list});
	}
	else {
		# Full list of fields
		$self->{fields} = 'm_user_base.*, m_user_base.firstname || \' \' || m_user_base.lastname || \' (\' || m_user_base.email || \')\' AS label, m_group_type.ident AS type, m_lang.ident AS lang, m_theme.ident AS theme_ident, m_theme.name AS theme, m_timezone.ident AS timezone, m_user_status.ident AS status';
		$self->{short_list} = delete ($data->{short_list});
	}

	# Match type can be 'begins', 'contains' or 'ends' to find user from a part of their attributes
	if (defined ($data->{match})) {
		my $match = $data->{match};
		if (grep (/^$match$/, qw/begins contains ends/)) {
			$self->{match} = $data->{match};
		}
		delete ($data->{match});
	}

	# Fields obtained from DB can be chosen by caller
	if (defined ($data->{fields})) {
		$self->{fields} = delete ($data->{fields});
	}

	# Journal of operations
	if (defined ($data->{journal})) {
		$self->{journal} = $data->{journal};
	}
	else {
		tie (@{$self->{journal}}, 'Mioga2::Journal');
	}

	# List of protected applications that can not be disabled for a user
	$self->{protected_applications} = ();
	for (@Mioga2::UserList::ProtectedApplications) {
		push (@{$self->{protected_applications}}, $_);
	}

	# Initialize WHERE statement before any update is made on the list
	$self->SetWhereStatement (keys (%{$self->{attributes}}));

	# Process skeleton
	if (exists ($self->{attributes}->{skeleton})) {
		my $status = 1;
		try {
			$self->LoadSkeleton ();
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, {module => 'Mioga2::UserList', step => __('Skeleton loading'), status => $status});
		};
	}

	print STDERR "[Mioga2::UserList::new] Leaving\n" if ($debug);
	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 Set

Set an attribute value.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to set.

=item I<$value:> the attribute value to set.

=back

=back

=cut

#===============================================================================
sub Set {
	my ($self, $attribute, $value) = @_;

	$self->{update}->{$attribute} = $value;
}	# ----------  end of subroutine Set  ----------


#===============================================================================

=head2 Get

Get an attribute value.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get.

=back

=item B<return:> the attribute value.

=back

=cut

#===============================================================================
sub Get {
	my ($self, $attribute) = @_;
	print STDERR "[Mioga2::UserList::Get] Entering, attribute name is $attribute\n" if ($debug);

	my $value = undef;

	#-------------------------------------------------------------------------------
	# Load the object from DB if attribute is not known already
	#-------------------------------------------------------------------------------
	if ((!grep /^$attribute$/, keys (%{$self->{attributes}})) && (!grep /^$attribute$/, keys (%{$self->{update}})) && !$self->{loaded}) {
		# The requested attribute is not set into object yet, get it from DB
		$self->Load ();
	}

	#-------------------------------------------------------------------------------
	# Retreive attribute value
	#-------------------------------------------------------------------------------
	if (exists ($self->{update}->{$attribute})) {
		# Attribute is in update list, get its value
		$value = $self->{update}->{$attribute};
	}
	elsif (exists ($self->{attributes}->{$attribute})) {
		# Attribute is in initial list, get its value
		$value = $self->{attributes}->{$attribute};
	}
	else {
		# Attribute is not in initial list, nor in update list, pick it from loaded users
		if (scalar (@{$self->{users}}) > 1) {
			# Multiple users in the list, return an array of attribute values.
			$value = ();
			for (@{$self->{users}}) {
				push (@$value, $_->{$attribute});
			}
		}
		else {
			# Only one user in the list, return attribute value
			$value = $self->{users}->[0]->{$attribute};
		}
	}

	# Add '%' signs according to match attribute
	if (exists ($self->{match}) && (ref ($value) eq '') && grep (/^$attribute$/, @text_fields)) {
		if ($self->{match} eq 'begins') {
			$value = "$value%";
		}
		elsif ($self->{match} eq 'contains') {
			$value = "%$value%";
		}
		elsif ($self->{match} eq 'ends') {
			$value = "%$value";
		}
		else {
			throw Mioga2::Exception::User ('Mioga2::UserList::RequestForAttribute', "Unknown match type: $self->{match}");
		}
	}

	if (ref ($value) eq 'ARRAY') {
		print STDERR "[Mioga2::UserList::Get] Leaving, attribute value is " . Dumper $value if ($debug);
	}
	elsif (ref ($value) eq 'Mioga2::GroupList') {
		my @rowids = $value->GetRowIds ();
		print STDERR "[Mioga2::UserList::Get] Leaving, attribute value is truncated, Mioga2::GroupList rowids " . Dumper @rowids if ($debug);
	}
	elsif (ref ($value) eq 'Mioga2::TeamList') {
		my @rowids = $value->GetRowIds ();
		print STDERR "[Mioga2::UserList::Get] Leaving, attribute value is truncated, Mioga2::TeamList rowids " . Dumper @rowids if ($debug);
	}
	elsif (ref ($value) eq 'Mioga2::ApplicationList') {
		my @rowids = $value->GetRowIds ();
		print STDERR "[Mioga2::GroupList::Get] Leaving, attribute value is truncated, Mioga2::ApplicationList rowids " . Dumper \@rowids if ($debug);
	}
	else {
		print STDERR "[Mioga2::UserList::Get] Leaving, attribute value is $value\n" if ($debug);
	}
	return ($value);
}	# ----------  end of subroutine Get  ----------


#===============================================================================

=head2 Store

Store changes to database. This method has to be called to actually create or
update one or more users. To delete users, see the "Delete" method.

=cut

#===============================================================================
sub Store {
	my ($self) = @_;
	print STDERR "[Mioga2::UserList::Store] Entering\n" if ($debug);

	if ((!$self->Count ()) && (!$self->{deleted})) {
		#-------------------------------------------------------------------------------
		# Create a user into DB
		# TODO this part has to be rewritten to be completely autonomous from APIGroup
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::UserList::Store] Creating new user\n" if ($debug);

		if (!exists ($self->{attributes}->{skeleton})) {
			throw Mioga2::Exception::Simple ('Mioga2::UserList::Store', 'Missing skeleton');
		}

		# Generate random ident if none specified
		if ((!$self->Has ('ident')) || ($self->Get ('ident') eq '')) {
			$self->Set ('ident', Mioga2::tools::string_utils::st_Random (10));
		}
		$self->SetWhereStatement (qw/ident/);

		# Apply skeleton to attributes
		$self->ApplySkeleton (qw/attributes/);

		# Create user
		my $user_attrs = $self->GetDatabaseValues ();
		my $user_type = delete ($user_attrs->{type});
		$user_type =~ s/_user//;

		my $status = 1;
		try {
			CreateUser($self->{config}, $user_attrs, $user_type, 1); # TODO (SNI - 21/04/2010 08:54): Handle this inside the module
		}
		otherwise {
			my $err = shift;
			$status = 0;
			push (@{$self->{journal}}, { module => 'Mioga2::UserList', step => $err->{"-text"}, status => $status});
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, {module => 'Mioga2::UserList', step => __x('User "{email}" creation', email => $self->Get ('email')), status => $status});
		};

		# Apply the rest of the skeleton
		$self->ApplySkeleton (qw/applications profiles teams groups/);
	}
	elsif (scalar (keys (%{$self->{update}}))) {
		#-------------------------------------------------------------------------------
		# Update users
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::UserList::Store] Updating existing user\n" if ($debug);
		my $sql = "UPDATE m_user_base SET ";
		for (keys (%{$self->{update}})) {
			my $request = $self->RequestForAttribute ($_);
			$sql .= "$request, " if ($request ne '');
		}
		$sql .= "modified = now ()";
		$sql .= " WHERE " . $self->GetWhereStatement (qw/m_user_base/);

		print STDERR "[Mioga2::UserList::Store] SQL: $sql\n" if ($debug);
		my $status = 1;
		try {
			ExecSQL ($self->{config}->GetDBH (), $sql);
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, {module => 'Mioga2::UserList', step => __x('User "{email}" update', email => $self->Get ('email')), status => $status});
		};
	}
	else {
		print STDERR "[Mioga2::UserList::Store] Nothing has to be done\n" if ($debug);
	}

	#-------------------------------------------------------------------------------
	# Update WHERE statement, as some modifications were made, users can not be
	# matched anymore by the same WHERE statement, matching according to rowids
	#-------------------------------------------------------------------------------
	$self->SetWhereStatement (qw/rowid/);

	#-------------------------------------------------------------------------------
	# Update-pending values are now stored into DB, they can be moved to the inner
	# attributes of the object
	#-------------------------------------------------------------------------------
	for (keys (%{$self->{update}})) {
		$self->{attributes}->{$_} = delete ($self->{update}->{$_});
	}

	print STDERR "[Mioga2::UserList::Store] Leaving\n" if ($debug);
}	# ----------  end of subroutine Store  ----------


#===============================================================================

=head2 Count

Count the users in the list. This method loads the user list from DB if
required.

=over

=item B<return:> the number of users in the list.

=back

=cut

#===============================================================================
sub Count {
	my ($self) = @_;
	print STDERR "[Mioga2::UserList::Count] Entering\n" if ($debug);

	my $count = undef;

	if ($self->{loaded}) {
		# Get count from loaded items
		$count = scalar (@{$self->{users}});
	}
	else {
		# Get count from dedicated request
		my $sql = "SELECT count(*) FROM m_user_base, " . $self->GetAdditionnalTables () . " WHERE " . $self->GetWhereStatement ();
		$count = SelectSingle ($self->{config}->GetDBH (), $sql)->{count};
	}

	print STDERR "[Mioga2::UserList::Count] Leaving: $count users\n" if ($debug);
	return ($count);
}	# ----------  end of subroutine Count  ----------


#===============================================================================

=head2 GetRowIds

Get RowIds for users in the list.

=over

=item B<return:> an array of rowids.

=back

=cut

#===============================================================================
sub GetRowIds {
	my ($self) = @_;

	my @list = ();

	$self->Load () unless $self->{loaded};

	for (@{$self->{users}}) {
		push (@list, $_->{rowid});
	}

	return (@list);
}	# ----------  end of subroutine GetRowIds  ----------


#===============================================================================

=head2 GetUsers

Get list of users in the list.

=over

=item B<return:> an array of hashes describing the users in the list. Each hash
of this array can be used to create a new Mioga2::UserList that
only matches this user.

=back

=cut

#===============================================================================
sub GetUsers {
	my ($self) = @_;
	print STDERR "[Mioga2::UserList::GetUsers] Entering\n" if ($debug);

	$self->Load () unless $self->{loaded};

	for my $user (@{$self->{users}}) {
		for (keys (%{$self->{update}})) {
			$user->{$_} = $self->{update}->{$_};
		}
	}

	# Compact list (if a user is invited through different ways - direct, or teams), ensure he is returned only once
	# Different profiles are placed into an array
	if ($self->Has ('expanded_groups')) {
		my %users;
		for my $u (@{$self->{users}}) {
			if (!exists ($users{$u->{rowid}})) {
				$users{$u->{rowid}} = $u;
			}
			else {
				if (ref ($users{$u->{rowid}}->{profile}) eq 'ARRAY') {
					push (@{$users{$u->{rowid}}->{profile}}, $u->{profile});
				}
				else {
					my $profile = $users{$u->{rowid}}->{profile};
					$users{$u->{rowid}}->{profile} = [ ];
					push (@{$users{$u->{rowid}}->{profile}}, $profile, $u->{profile});
				}
			}
		}
		@{$self->{users}} = sort { $a->{lastname} . $a->{label} cmp $b->{lastname} . $b->{label} } map { $users{$_} } keys (%users);
	}

	print STDERR "[Mioga2::UserList::GetUsers] Users: " . Dumper $self->{users} if ($debug);
	return (@{$self->{users}});
}	# ----------  end of subroutine GetUsers  ----------


#===============================================================================

=head2 Delete

Delete users.

=cut

#===============================================================================
sub Delete {
	my ($self) = @_;
	print STDERR "[Mioga2::UserList::Delete] Entering\n" if ($debug);

	if ($self->Count ()) {
		for my $user ($self->GetUsers ()) {
			if ($self->{config}->GetAdminId () && ($user->{rowid} == $self->{config}->GetAdminId ())) {
				# Check if the admin is the last user in the instance, otherwise don't delete it
				my $instance_users = Mioga2::UserList->new ($self->{config});
				next if (($self->Count () > 1) && ($instance_users->Count () > 1));
				print STDERR "[Mioga2::UserList::Delete] Deleting instance '" . $self->{config}->GetMiogaIdent () . "' admin account\n" if ($debug);
			}
			print STDERR "[Mioga2::UserList::Delete] Deleting user rowid $user->{rowid}, mioga_id $user->{mioga_id} (ident '$user->{ident}')\n" if ($debug);

			# Delete user from LDAP, if applicable
			if (($user->{type} eq 'ldap_user') && ($self->{config}->CanModifyLdapPassword () || $self->{config}->CanModifyLdapInstance ())) {
				print STDERR "[Mioga2::UserList::Delete] User is LDAP, cleanup directory\n" if ($debug);
				require Mioga2::Bottin;
				my $bottin = Mioga2::Bottin->new ($self->{config});
				$bottin->RevokeUserFromInstance ($self->{config}, $user->{dn});
			}

			# Delete user from DB
			my $status = 1;
			try {
				UserDelete ($self->{config}, $user->{rowid}, 1); # TODO (SNI - 21/04/2010 08:54): Handle this inside the module
			}
			otherwise {
				my $err = shift;
				$status = 0;
				throw $err;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::UserList', step => __x('User "{email}" deletion', email => $user->{email}), status => $status });
			};
		}

		$self->{deleted} = 1;
	}

	print STDERR "[Mioga2::UserList::Delete] Leaving\n" if ($debug);
}	# ----------  end of subroutine Delete  ----------


#===============================================================================

=head2 JoinTeams

Invite users into teams.

=over

=item B<attributes:>

=over

=item I<@teams:> an array of team idents.

=back

=back

=cut

#===============================================================================
sub JoinTeams {
	my ($self, @teams) = @_;
	print STDERR "[Mioga2::UserList::JoinTeams] Entering\n" if ($debug);

	my $where = $self->GetWhereStatement ();
	my $mioga_id = $self->{config}->GetMiogaId ();
	my $teams_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_) } @teams);

	# Revoke users from not specified teams
	my $sql = "DELETE FROM m_group_group USING m_group, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_group_group.group_id = m_group.rowid AND m_group_group.invited_group_id = m_user_base.rowid" . (($teams_idents eq '') ? '' : " AND m_group.ident NOT IN ($teams_idents)") . " AND m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'team') AND $where;";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	if ($teams_idents ne '') {
		# Generate and run SQL query
		$sql = "INSERT INTO m_group_group SELECT m_group_base.rowid AS group_id, m_user_base.rowid AS invited_group_id FROM m_group_base, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_group_base.ident IN ($teams_idents) AND m_group_base.mioga_id = $mioga_id AND $where AND m_user_base.rowid NOT IN (SELECT invited_group_id FROM m_group_group WHERE group_id = m_group_base.rowid);";
		ExecSQL ($self->{config}->GetDBH (), $sql);
	}

	print STDERR "[Mioga2::UserList::JoinTeams] Leaving\n" if ($debug);
}	# ----------  end of subroutine JoinTeams  ----------


#===============================================================================

=head2 JoinGroups

Invite users into groups.

=over

=item B<arguments:>

=over

=item I<%groups:> a hash whose keys are group idents and values are associated profile ident.

=back

=back

=cut

#===============================================================================
sub JoinGroups {
	my ($self, %groups) = @_;
	print STDERR "[Mioga2::UserList::JoinGroups] Entering\n" if ($debug);

	my $where = $self->GetWhereStatement ();
	my $mioga_id = $self->{config}->GetMiogaId ();
	my $groups_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_) } keys (%groups));

	# Revoke users from all groups they were member of and are not anymore
	for my $group ($self->GetInvitedGroupList ()->GetGroups ()) {
		my $group_ident = $group->{ident};
		if (!grep (/^$group_ident$/, keys (%groups))) {
			for my $user_id (@{ac_ForceArray ($self->Get ('rowid'))}) {
				LaunchMethodInApps($self->{config}, "RevokeUserFromGroup", $group->{rowid}, $user_id, $group->{anim_id});
			}
		}
	}

	# Revoke users from not specified groups
	my $sql = "DELETE FROM m_profile_group USING m_profile, m_group, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_profile_group.group_id = m_user_base.rowid AND m_profile_group.profile_id = m_profile.rowid AND m_profile.group_id = m_group.rowid" . (($groups_idents eq '') ? '' : " AND m_group.ident NOT IN ($groups_idents)") . " AND m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'group') AND m_group.anim_id != m_user_base.rowid AND $where;";
	ExecSQL ($self->{config}->GetDBH (), $sql);
	$sql = "DELETE FROM m_group_group USING m_group, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_group_group.group_id = m_group.rowid AND m_group_group.invited_group_id = m_user_base.rowid" . (($groups_idents eq '') ? '' : " AND m_group.ident NOT IN ($groups_idents)") . " AND m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'group') AND m_group.anim_id != m_user_base.rowid AND $where;";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	if ($groups_idents ne '') {
		# Invite users into group
		$sql = "INSERT INTO m_group_group SELECT m_group_base.rowid AS group_id, m_user_base.rowid AS invited_group_id FROM m_group_base, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_group_base.ident IN ($groups_idents) AND m_group_base.mioga_id = $mioga_id AND $where AND m_user_base.rowid NOT IN (SELECT invited_group_id FROM m_group_group WHERE group_id = m_group_base.rowid);";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Set last connection to null
		$sql = "INSERT INTO m_group_user_last_conn SELECT m_group_base.rowid AS group_id, m_user_base.rowid AS user_id, NULL AS last_conection FROM m_group_base, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_group_base.ident IN ($groups_idents) AND m_group_base.mioga_id = $mioga_id AND $where AND m_user_base.rowid NOT IN (SELECT user_id FROM m_group_user_last_conn WHERE group_id = m_group_base.rowid);";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Affect profiles to users
		for (keys (%groups)) {
			my $group_ident = $_;
			my $profile_ident = ($groups{$_} ne '') ? $self->{config}->GetDBH()->quote ($groups{$_}) : $self->{config}->GetDBH ()->quote (SelectSingle ($self->{config}->GetDBH (), "SELECT m_profile.ident FROM m_profile, m_profile_group, m_user_base, m_group, " . $self->GetAdditionnalTables () . " WHERE m_profile_group.group_id = m_user_base.rowid AND m_profile.group_id = m_group.rowid AND m_profile_group.profile_id = m_profile.rowid AND $where AND m_group.mioga_id = $mioga_id AND m_group.ident = " . $self->{config}->GetDBH ()->quote ($group_ident) . " AND m_group.mioga_id = $mioga_id UNION SELECT m_profile.ident FROM m_profile, m_group WHERE m_profile.rowid = m_group.default_profile_id AND m_group.ident = " . $self->{config}->GetDBH ()->quote ($group_ident) . " AND m_group.mioga_id = $mioga_id LIMIT 1")->{ident});

			# Create new profile affectation
			$sql = "INSERT INTO m_profile_group SELECT m_profile.rowid AS profile_id, m_user_base.rowid AS group_id FROM m_profile, m_group_base, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_profile.group_id = m_group_base.rowid AND m_group_base.ident = " . $self->{config}->GetDBH()->quote ($group_ident) . " AND m_group_base.mioga_id = $mioga_id AND $where AND m_profile.ident = $profile_ident AND m_user_base.rowid NOT IN (SELECT group_id FROM m_profile_group WHERE profile_id IN (SELECT m_profile.rowid FROM m_profile, m_group_base WHERE m_group_base.ident = " . $self->{config}->GetDBH()->quote ($group_ident) . " AND m_group_base.mioga_id = $mioga_id AND m_profile.group_id = m_group_base.rowid));";
			ExecSQL ($self->{config}->GetDBH (), $sql);
		}
	}

	print STDERR "[Mioga2::UserList::JoinGroups] Leaving\n" if ($debug);
}	# ----------  end of subroutine JoinGroups  ----------


#===============================================================================

=head2 EnableApplications

Enable applications in user's space so that the user can activate them to
actually use them.

=over

=item B<arguments:>

=over

=item I<@applications:> an array of application idents.

=back

=back

=cut

#===============================================================================
sub EnableApplications {
	my ($self, @applications) = @_;
	print STDERR "[Mioga2::UserList::EnableApplications] Entering. Applications: " . Dumper \@applications if ($debug);

	my $where = $self->GetWhereStatement ();
	my $mioga_id = $self->{config}->GetMiogaId ();
	my $apps_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_->{ident}) } @applications);

	my $sql;

	# Delete application methods / profiles association for applications that are not in the list
	$sql = "DELETE FROM m_profile_function USING m_function, m_application, m_instance_application, m_profile, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_function.application_id = m_application.rowid AND m_profile.group_id = m_user_base.rowid AND m_profile_function.profile_id = m_profile.rowid AND m_profile_function.function_id = m_function.rowid AND $where AND m_application.ident NOT IN ($apps_idents) AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_user_base.mioga_id = $mioga_id;";
	ExecSQL ($self->{config}->GetDBH (), $sql);
	# Disable applications that are not in the list
	$sql = "DELETE FROM m_application_group_allowed WHERE (application_id, group_id) IN (SELECT m_application.rowid AS application_id, m_user_base.rowid AS group_id FROM m_application, m_instance_application, m_user_base, " . $self->GetAdditionnalTables () . " WHERE $where AND m_application.ident NOT IN ($apps_idents) AND m_user_base.mioga_id = $mioga_id AND m_instance_application.application_id = m_application.rowid AND m_instance_application.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE all_users = 't' AND mioga_id = $mioga_id) AND m_application.type_id IN (SELECT rowid FROM m_application_type WHERE ident IN ('normal', 'shell', 'portal')));";
	ExecSQL ($self->{config}->GetDBH (), $sql);
	# Disallow applications that are not in the list
	$sql = "DELETE FROM m_application_group WHERE (application_id, group_id) IN (SELECT m_application.rowid AS application_id, m_user_base.rowid AS group_id FROM m_application, m_instance_application, m_user_base, " . $self->GetAdditionnalTables () . " WHERE $where AND m_application.ident NOT IN ($apps_idents) AND m_user_base.mioga_id = $mioga_id AND m_instance_application.application_id = m_application.rowid AND m_instance_application.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE all_users = 't' AND mioga_id = $mioga_id) AND m_application.type_id IN (SELECT rowid FROM m_application_type WHERE ident IN ('normal', 'shell', 'portal')));";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	# Process all_users applications
	my $reverted_users = Mioga2::UserList->new ($self->{config}, { attributes => { rowid => $self->Get ('rowid') } });
	$reverted_users->Revert ();
	my $reverted_where = $reverted_users->GetWhereStatement ();
	$sql = "INSERT INTO m_application_group SELECT m_application.rowid AS application_id, m_user_base.rowid AS group_id FROM m_application, m_instance_application, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_application.ident NOT IN ($apps_idents) AND m_application.type_id IN (SELECT rowid FROM m_application_type WHERE ident IN ('normal', 'shell', 'portal')) AND m_instance_application.application_id = m_application.rowid AND m_instance_application.all_users = 't' AND m_instance_application.mioga_id = $mioga_id AND $reverted_where AND (m_application.rowid, m_user_base.rowid) NOT IN (SELECT * FROM m_application_group);";
	ExecSQL ($self->{config}->GetDBH (), $sql);
	$sql = "UPDATE m_instance_application SET all_users = 'f' FROM m_application WHERE m_instance_application.application_id = m_application.rowid AND m_application.ident NOT IN ($apps_idents) AND m_application.type_id IN (SELECT rowid FROM m_application_type WHERE ident IN ('normal', 'shell', 'portal')) AND m_instance_application.mioga_id = $mioga_id;";
	ExecSQL ($self->{config}->GetDBH (), $sql);
	
	# Enable applications that are in the list
	$sql = "INSERT INTO m_application_group (application_id, group_id) SELECT m_application.rowid AS application_id, m_user_base.rowid AS group_id FROM m_application, m_instance_application, m_user_base, " . $self->GetAdditionnalTables () . " WHERE $where AND m_application.ident IN ($apps_idents) AND m_user_base.mioga_id = $mioga_id AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_application_group WHERE group_id = m_user_base.rowid) AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE mioga_id = $mioga_id AND all_users = 't');";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	print STDERR "[Mioga2::UserList::EnableApplications] Leaving\n" if ($debug);
}	# ----------  end of subroutine EnableApplications  ----------


#===============================================================================

=head2 ActivateApplications

Activate applications that have been previously enabled via
Mioga2::UserList::EnableApplications.

=over

=item B<arguments:>

=over

=item I<@applications:> an array of application idents.

=back

=back

=cut

#===============================================================================
sub ActivateApplications {
	my ($self, @applications) = @_;
	print STDERR "[Mioga2::UserList::ActivateApplications] Entering. Applications: " . Dumper \@applications if ($debug);

	my $where = $self->GetWhereStatement ();
	my $mioga_id = $self->{config}->GetMiogaId ();

	# Only select applications that have active="1" set in their attributes
	my @active_apps = map { if ((defined $_->{active}) && ($_->{active} == 1)) { $_->{ident} } else { } } @applications;
	my $apps_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_) } @active_apps);

	my $already_active = SelectMultiple ($self->{config}->GetDBH (), "SELECT m_application.ident FROM m_application, m_instance_application, m_user_base, " . $self->GetAdditionnalTables () . " WHERE $where AND m_application.ident IN ($apps_idents) AND m_user_base.mioga_id = $mioga_id AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_application.rowid IN (SELECT application_id FROM m_application_group_allowed WHERE group_id = m_user_base.rowid)");
	my @already_active = map { $_->{ident} } @$already_active;
	
	# Activate applications
	my $sql = "INSERT INTO m_application_group_allowed (created, modified, application_id, group_id, is_public) SELECT now() AS created, now() AS modified, m_application.rowid AS application_id, m_user_base.rowid AS group_id, 'f' AS is_public FROM m_application, m_instance_application, m_user_base, " . $self->GetAdditionnalTables () . " WHERE $where AND m_application.ident IN ($apps_idents) AND m_user_base.mioga_id = $mioga_id AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_application.rowid NOT IN (SELECT application_id FROM m_application_group_allowed WHERE group_id = m_user_base.rowid);";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	# Make expected applications public
	my @public_apps = map { if ((defined $_->{public}) && ($_->{public} == 1)) { $_->{ident} } else { } } @applications;
	my $public_apps_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_) } @public_apps);
	if (@public_apps) {
		$sql = "UPDATE m_application_group_allowed SET is_public = 't' WHERE application_id IN (SELECT rowid FROM m_application WHERE ident IN ($public_apps_idents)) AND group_id IN (SELECT m_user_base.rowid FROM m_user_base, " . $self->GetAdditionnalTables () . " WHERE $where);";
		ExecSQL ($self->{config}->GetDBH (), $sql);
	}

	for my $user ($self->GetUsers ()) {
		for (@applications) {
			my $appident = $_->{ident};
			next if (grep (/^$appident$/, @already_active));
			my $app_desc = new Mioga2::AppDesc ($self->{config}, ident => $_->{ident});
			my $app = $app_desc->CreateApplication();
			$app->InitializeUserApplication($self->{config}, $user, $_);
		}
	}

	# Enable all functions for animator profile
	$sql = "INSERT INTO m_profile_function SELECT m_profile.rowid AS profile_id, m_function.rowid AS function_id FROM m_profile, m_user_base, " . $self->GetAdditionnalTables () . ", m_profile_user, m_application, m_instance_application, m_function WHERE $where AND m_profile.group_id = m_user_base.rowid AND m_profile_user.profile_id = m_profile.rowid AND m_profile_user.group_id = m_user_base.rowid AND m_function.application_id = m_application.rowid AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_application.ident IN ($apps_idents) AND (m_profile.rowid, m_function.rowid) NOT IN (SELECT * FROM m_profile_function WHERE profile_id = m_profile.rowid);";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	print STDERR "[Mioga2::UserList::ActivateApplications] Leaving\n" if ($debug);
}	# ----------  end of subroutine ActivateApplications  ----------


#===============================================================================

=head2 CreateProfile

Create a profile.

=over

=item B<arguments:>

=over

=item I<$profile:> a hash with the following keys:

=over

=item ident: the profile ident.

=item applications: a hash whose keys are application idents and values are a hash describing access rights to functions.

=over

=item function: an array of function idents to grant access to.

=item all_functions: if this key is defined, access to all functions is granted.

=back

=back

=back

=back

=cut

#===============================================================================
sub CreateProfile {
	my ($self, $profile) = @_;
	print STDERR "[Mioga2::UserList::CreateProfile] Entering\n" if ($debug);

	my $where = $self->GetWhereStatement ();
	my $mioga_id = $self->{config}->GetMiogaId ();
	my $profile_ident = $profile->{ident};

	#-------------------------------------------------------------------------------
	# Create profile
	#-------------------------------------------------------------------------------
	my $sql = "INSERT INTO m_profile (created, modified, ident, group_id) SELECT DISTINCT now() AS created, now() AS modified, " . $self->{config}->GetDBH()->quote ($profile_ident) . " AS ident, m_user_base.rowid AS group_id FROM m_user_base, " . $self->GetAdditionnalTables () . " WHERE $where AND m_user_base.rowid NOT IN (SELECT group_id FROM m_profile WHERE ident = " . $self->{config}->GetDBH()->quote ($profile_ident) . ");";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	#-------------------------------------------------------------------------------
	# Associate functions to profile
	#-------------------------------------------------------------------------------
	for (@{$profile->{application}}) {
		# Initialize functions idents list
		my $funcs_idents;
		if (exists ($_->{all_functions})) {
			$funcs_idents = "SELECT m_function.ident FROM m_function, m_application, m_instance_application WHERE m_function.application_id = m_application.rowid AND m_application.rowid = m_instance_application.application_id AND m_application.ident = " . $self->{config}->GetDBH()->quote ($_->{ident}) . " AND m_instance_application.mioga_id = $mioga_id";
		}
		else {
			$funcs_idents = join (', ', map { $self->{config}->GetDBH()->quote ($_) } @{$_->{function}});
		}
		$sql = "INSERT INTO m_profile_function SELECT m_profile.rowid AS profile_id, m_function.rowid AS function_id FROM m_profile, m_function, m_application, m_instance_application, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_function.application_id = m_application.rowid AND m_profile.ident = " . $self->{config}->GetDBH()->quote ($profile_ident) . " AND m_profile.group_id = m_user_base.rowid AND $where AND m_application.ident = " . $self->{config}->GetDBH()->quote ($_->{ident}) . " AND m_function.ident IN ($funcs_idents) AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id AND m_function.rowid NOT IN (SELECT function_id FROM m_profile_function WHERE profile_id = m_profile.rowid);";
		ExecSQL ($self->{config}->GetDBH (), $sql);
	}

	print STDERR "[Mioga2::UserList::CreateProfile] Leaving\n" if ($debug);
}	# ----------  end of subroutine CreateProfile  ----------


#===============================================================================

=head2 AffectProfileToUsers

This affects a profile to a Mioga2::UserList users into another Mioga2::UserList users personal space.

=over

=item B<arguments:>

=over

=item I<$users:> a Mioga2::UserList object to affect the profile to.

=item I<$profile_ident:> the profile ident to affect to users.

=back

=back

=cut

#===============================================================================
sub AffectProfileToUsers {
	my ($self, $users, $profile_ident) = @_;
	print STDERR "[Mioga2::UserList::AffectProfileToUsers] Entering\n" if ($debug);

	my $users_where = $users->GetWhereStatement ();
	my $where = $self->GetWhereStatement ();

	# Delete any existing profile affectation
	my $sql = "DELETE FROM m_profile_group WHERE profile_id IN (SELECT m_profile.rowid FROM m_profile, m_profile_group, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_profile.group_id = m_user_base.rowid AND m_profile_group.profile_id = m_profile.rowid AND $where AND m_profile_group.group_id IN (SELECT m_user_base.rowid FROM m_user_base, " . $users->GetAdditionnalTables () . " WHERE $users_where));";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	# Create new profile affectation
	$sql = "INSERT INTO m_profile_group SELECT m_profile.rowid AS profile_id, m_user_base.rowid AS group_id FROM m_profile, m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_profile.group_id = m_user_base.rowid AND $where AND $users_where AND m_profile.ident = " . $self->{config}->GetDBH()->quote ($profile_ident) . ";";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	print STDERR "[Mioga2::UserList::AffectProfileToUsers] Leaving\n" if ($debug);
}	# ----------  end of subroutine AffectProfileToUsers  ----------


#===============================================================================

=head2 SetDefaultProfile

Set default profile for users' personal space.

=over

=item B<arguments>

=over

=item I<$profile_ident:> the default profile ident.

=back

=back

=cut

#===============================================================================
sub SetDefaultProfile {
	my ($self, $profile_ident) = @_;
	print STDERR "[Mioga2::UserList::SetDefaultProfile] Entering\n" if ($debug);

	my $where = $self->GetWhereStatement ();

	my $sql = "UPDATE m_user_base SET default_profile_id = m_profile.rowid FROM m_profile, " . $self->GetAdditionnalTables () . " WHERE m_profile.group_id = m_user_base.rowid AND m_profile.ident = " . $self->{config}->GetDBH()->quote ($profile_ident) . " AND $where;";
	ExecSQL ($self->{config}->GetDBH (), $sql);

	print STDERR "[Mioga2::UserList::SetDefaultProfile] Leaving\n" if ($debug);
}	# ----------  end of subroutine SetDefaultProfile  ----------


#===============================================================================

=head2 GetExpandedGroupList

Get list of groups the users in the list are invited into, either directly or
via teams.

=over

=item B<arguments:>

=over

=item I<%attributes:> attributes to initialize the resulting Mioga2::GroupList.
This is mainly intented to pass short_list flag to Mioga2::GroupList. It could
lead to inconsistent Mioga2::GroupList contents if other keys (such as 'ident')
are passed.

=back

=back

=over

=item B<return:> a Mioga2::GroupList object.

=back

=cut

#===============================================================================
sub GetExpandedGroupList {
	my ($self, %attributes) = @_;

	my $short_list = (exists ($attributes{short_list})) ? $attributes{short_list} : $self->{short_list};

	my $groups = Mioga2::GroupList->new ($self->{config}, { attributes => { 'expanded_users' => $self }, short_list => $short_list, journal => $self->{journal} });

	return ($groups);
}	# ----------  end of subroutine GetExpandedGroupList  ----------


#===============================================================================

=head2 GetInvitedGroupList

Get list of groups the users in the list are directly invited into (ie not via teams).

=over

=item B<arguments:>

=over

=item I<%attributes:> attributes to initialize the resulting Mioga2::GroupList.
This is mainly intented to pass short_list flag to Mioga2::GroupList. It could
lead to inconsistent Mioga2::GroupList contents if other keys (such as 'ident')
are passed.

=back

=back

=over

=item B<return:> a Mioga2::GroupList object.

=back

=cut

#===============================================================================
sub GetInvitedGroupList {
	my ($self, %attributes) = @_;

	my $short_list = (exists ($attributes{short_list})) ? $attributes{short_list} : $self->{short_list};

	my $groups = Mioga2::GroupList->new ($self->{config}, { attributes => { 'users' => $self }, short_list => $short_list, journal => $self->{journal} });

	return ($groups);
}	# ----------  end of subroutine GetInvitedGroupList  ----------


#===============================================================================

=head2 GetInvitedTeamList

Get list of teams the users in the list are invited into.

=over

=item B<arguments:>

=over

=item I<%attributes:> attributes to initialize the resulting Mioga2::TeamList.
This is mainly intented to pass short_list flag to Mioga2::TeamList. It could
lead to inconsistent Mioga2::TeamList contents if other keys (such as 'ident')
are passed.

=back

=back

=over

=item B<return:> a Mioga2::TeamList object.

=back

=cut

#===============================================================================
sub GetInvitedTeamList {
	my ($self, %attributes) = @_;

	my $short_list = (exists ($attributes{short_list})) ? $attributes{short_list} : $self->{short_list};

	my $teams = Mioga2::TeamList->new ($self->{config}, { attributes => { 'users' => $self }, short_list => $short_list, journal => $self->{journal} });

	return ($teams);
}	# ----------  end of subroutine GetInvitedTeamList  ----------


#===============================================================================

=head2 GetApplicationList

Get list of applications the users of the list can use.

=over

=item B<return:> a Mioga2::ApplicationList object.

=back

=cut

#===============================================================================
sub GetApplicationList {
	my ($self) = @_;

	my $applications = Mioga2::ApplicationList->new ($self->{config}, { attributes => { 'users' => $self }, journal => $self->{journal} });

	return ($applications);
}	# ----------  end of subroutine GetApplicationList  ----------


#===============================================================================

=head2 GetActiveApplicationList

Get list of applications enabled for the users of the list.

=over

=item B<return:> a Mioga2::ApplicationList object.

=back

=cut

#===============================================================================
sub GetActiveApplicationList {
	my ($self) = @_;

	my $applications = Mioga2::ApplicationList->new ($self->{config}, { attributes => { 'users' => $self, only_active => 1 }, journal => $self->{journal} });

	return ($applications);
}	# ----------  end of subroutine GetActiveApplicationList  ----------


#===============================================================================

=head2 Revert

Revert list.

=cut

#===============================================================================
sub Revert {
	my ($self) = @_;

	$self->{where}->{m_user_base} = 'm_user_base.rowid NOT IN (SELECT m_user_base.rowid FROM m_user_base WHERE ' . $self->GetWhereStatement (qw/m_user_base/) . ') AND m_user_base.mioga_id = ' . $self->{config}->GetMiogaId ();

	$self->{attributes} = { };
	$self->{update} = { };
	$self->{loaded} = 0;
	$self->{deleted} = 0;

	print STDERR "[Mioga2::UserList::Revert] New rules: " . Dumper $self->{where} if ($debug);
}	# ----------  end of subroutine Revert  ----------


#===============================================================================

=head2 GetProtectedApplications

Get list of applications idents that can not be disabled for a user.

=cut

#===============================================================================
sub GetProtectedApplications {
	my ($self) = @_;

	return (@{$self->{protected_applications}});
}	# ----------  end of subroutine GetProtectedApplications  ----------


# ============================================================================

=head2 SendMail (MIME::Entity)

Send the message passed in parameters to the users.

=cut

# ============================================================================

sub SendMail {
	my ($self, $entity) = @_;

	for ($self->GetUsers ()) {
		if($entity->head ()->count ("To") > 0) { 
			$entity->replace ("To" => $_->{email});
		}
		else {
			$entity->add ("To" => $_->{email});
		}
		
		mu_SendMail ($self->{config}, $entity);
	}
}


#===============================================================================

=head2 SetSecretQuestion

Set secret question and answer for a user

=cut

#===============================================================================
sub SetSecretQuestion {
	my ($self, $question, $answer) = @_;
	print STDERR "[Mioga2::UserList::SetSecretQuestion] Entering\n" if ($debug);

	my $config = $self->{config};

	$self->Load () if (!$self->{loaded});

	# Escape strings
	$question = st_FormatPostgreSQLString ($question);
	$answer = st_FormatPostgreSQLString ($answer);

	# Get different user IDs
	my $rowids = ac_ForceArray ($self->Get ('rowid'));
	for (@$rowids) {
		my $req = "SELECT DISTINCT question_id FROM m_user2question WHERE user_id = $_";
		my $id = SelectSingle ($config->GetDBH(), $req);

		if (!$id) {
			# No question set yet, define it
			ExecSQL ($config->GetDBH(), "INSERT INTO m_secret_question (question, answer) VALUES ('$question', '$answer')");
			my $question_id = GetLastInsertId ($config->GetDBH(), 'm_secret_question');
			ExecSQL ($config->GetDBH(), "INSERT INTO m_user2question (user_id, question_id) VALUES ($_, $question_id)");
		}
		else {
			# Question already set, update it
			ExecSQL ($config->GetDBH(), "UPDATE m_secret_question SET question='$question', answer='$answer' WHERE rowid=$id->{question_id}");
		}
	}

	print STDERR "[Mioga2::UserList::SetSecretQuestion] Leaving\n" if ($debug);
}	# ----------  end of subroutine SetSecretQuestion  ----------


#===============================================================================

=head2 GetSecretQuestion

Get secret question and answer for a user

=cut

#===============================================================================
sub GetSecretQuestion {
	my ($self) = @_;
	print STDERR "[Mioga2::UserList::GetSecretQuestion] Entering\n" if ($debug);

	my $config = $self->{config};

	if ($self->Count () > 1) {
		throw Mioga2::Exception::User ('Mioga2::UserList::GetSecretQuestion', 'More than one user matched by WHERE statement, this method should never apply to more than one user.');
	}

	# Get different user IDs
	my $rowids = ac_ForceArray ($self->Get ('rowid'));
	my $qa = SelectSingle ($config->GetDBH(), "SELECT question, answer FROM m_secret_question WHERE rowid=(SELECT DISTINCT question_id FROM m_user2question WHERE user_id IN (" . join (',', @$rowids) . "));");

	print STDERR "[Mioga2::UserList::GetSecretQuestion] Leaving\n" if ($debug);
	return ($qa);
}	# ----------  end of subroutine GetSecretQuestion  ----------


#===============================================================================

=head2 CheckSecretQuestion

Check answer for secret question

=cut

#===============================================================================
sub CheckSecretQuestion {
	my ($self, $answer) = @_;
	print STDERR "[Mioga2::UserList::CheckSecretQuestion] Entering\n" if ($debug);

	my $config = $self->{config};

	if ($self->Count () > 1) {
		throw Mioga2::Exception::User ('Mioga2::UserList::CheckSecretQuestion', 'More than one user matched by WHERE statement, this method should never apply to more than one user.');
	}

	my $ret = 0;

	# Escape strings
	$answer = st_FormatPostgreSQLString ($answer);

	# Get different user IDs in case user is LDAP and in several instances
	my $rowid = $self->Get ('rowid');
	my $qa = SelectSingle ($config->GetDBH(), "SELECT question, answer FROM m_secret_question WHERE rowid=(SELECT DISTINCT question_id FROM m_user2question WHERE user_id = $rowid) AND answer~*'^$answer\$';");

	if ($qa) {
		$ret = 1;
	}

	print STDERR "[Mioga2::UserList::CheckSecretQuestion] Leaving\n" if ($debug);
	return ($ret);
}	# ----------  end of subroutine CheckSecretQuestion  ----------


#===============================================================================

=head2 DeleteSecretQuestion

Reset user's secret question

=cut

#===============================================================================
sub DeleteSecretQuestion {
	my ($self) = @_;
	print STDERR "[Mioga2::UserList::DeleteSecretQuestion] Entering\n" if ($debug);

	my $config = $self->{config};

	# Get different user IDs in case user is LDAP and in several instances
	my $rowids = ac_ForceArray ($self->Get ('rowid'));
	ExecSQL ($config->GetDBH (), "DELETE FROM m_secret_question WHERE rowid IN (SELECT question_id FROM m_user2question WHERE user_id IN (" . join (', ', @$rowids) . "));");
	ExecSQL ($config->GetDBH (), "DELETE FROM m_user2question WHERE user_id IN (" . join (', ', @$rowids) . ");");

	print STDERR "[Mioga2::UserList::DeleteSecretQuestion] Leaving\n" if ($debug);
}	# ----------  end of subroutine DeleteSecretQuestion  ----------


#===============================================================================

=head2 CheckPassword

Check user password

=over

=item B<return:> 1 if password is OK, 0 otherwise.

=back

=cut

#===============================================================================
sub CheckPassword {
	my ($self, $password) = @_;

	my $status = 0;

	my ($crypt_method, $crypted_password) = ($self->Get ('password') =~ m/^\{([^\}]+)\}(.*)$/);
	print STDERR "[Mioga2::UserList::CheckPassword] Password crypt method: $crypt_method\n" if ($debug);

	my $crypter = $self->{config}->LoadPasswordCrypter ($crypt_method);
	if (($self->Get ('type') eq 'ldap_user') && !$self->{config}->GetMiogaConf ()->GetConfParameter ('store_ldap_password')) {
		my $ldap = Mioga2::LDAP->new ($self->{config});
		try {
			$ldap->BindWith ($self->Get ('dn'), $password);
			$status = 1;
		}
		otherwise {
			print STDERR "[Mioga2::UserList::CheckPassword] LDAP bind error for " . $self->Get ('dn') . "\n";
		};
	}
	else {
		if($crypter->CheckPassword($password, $crypted_password)) {
			$status = 1;
		}
		elsif ($self->Get ('type') ne 'local_user') {
			my $new_password;
			if ($self->Get ('type') eq 'ldap_user') {
				$new_password = $self->UpdateLDAPPassword ();
			}
			elsif ($self->Get ('type') eq 'external_user') {
				# $new_password = $self->UpdateExternalPassword (); # TODO ( - 30/11/2010 16:06): TODO
			}

			if ($new_password) {
				# Check if given password matches updated password
				($crypt_method, $new_password) = ($new_password =~ m/^\{([^\}]+)\}(.*)$/);
				print STDERR "[Mioga2::UserList::CheckPassword] Password crypt method: $crypt_method\n" if ($debug);

				my $crypter = $self->{config}->LoadPasswordCrypter ($crypt_method);
				if($crypter->CheckPassword($password, $new_password)) {
					$status = 1;
				}
			}
		}
	}

	print STDERR "[Mioga2::UserList::CheckPassword] Leaving (status = $status)\n" if ($debug);
	return ($status);
}	# ----------  end of subroutine CheckPassword  ----------


#===============================================================================

=head2 GetJournal

Get journal of operations as an object

=cut

#===============================================================================
sub GetJournal {
	my ($self) = @_;

	my $journal = tied (@{$self->{journal}});

	return ($journal);
}	# ----------  end of subroutine GetJournal  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


#===============================================================================

=head2 LoadSkeleton

Load provided skeleton file and store its contents as inner attributes of the object.

=cut

#===============================================================================
sub LoadSkeleton {
	my ($self) = @_;
	print STDERR "[Mioga2::UserList::LoadSkeleton] Entering\n" if ($debug);

	my $data = '';

	my $skel_file = delete $self->{attributes}->{skeleton};
	$self->{attributes}->{skeleton}->{file} = $skel_file;

	#-------------------------------------------------------------------------------
	# Read file
	#-------------------------------------------------------------------------------
	open(FILE, "< $self->{attributes}->{skeleton}->{file}") or throw Mioga2::Exception::User ('Mioga2::UserList::LoadSkeleton', "Can't open skeleton file '$self->{attributes}->{skeleton}->{file}': $!.");
	while (<FILE>) { $data .= $_ };
	close (FILE);

	#-------------------------------------------------------------------------------
	# Parse XML
	#-------------------------------------------------------------------------------
	$data =~ s/<--!.*?-->//gsm; # remove comments

	my $xml		  = new Mioga2::XML::Simple(forcearray => 1);
	my $xmltree   = $xml->XMLin($data);
	my $skel_name = (exists($xmltree->{'name'}))? $xmltree->{'name'} : "";
	my $skel_type = (exists($xmltree->{'type'}))? $xmltree->{'type'} : "";

	if (lc ($skel_type) ne 'user') {
		throw Mioga2::Exception::User ('Mioga2::UserList::LoadSkeleton', "Skeleton type '$skel_type' can not be applied to a user");
	}

	#-------------------------------------------------------------------------------
	# Translate XML tree
	#-------------------------------------------------------------------------------
	my %contents = ();

	# Attributes
	for (keys (%{$xmltree->{attributes}->[0]})) {
		$contents{attributes}{$_} = $xmltree->{attributes}->[0]->{$_}->[0];
	}

	# Applications
	for (@{$xmltree->{applications}->[0]->{application}}) {
		push (@{$contents{applications}}, $_);
	}

	# Profiles
	$contents{profiles} = $xmltree->{profiles}->[0];

	# Groups
	for (@{$xmltree->{groups}->[0]->{group}}) {
		$contents{groups}->{$_->{ident}} = $_->{profile}->[0];
	}

	# Teams
	for (@{$xmltree->{teams}->[0]->{team}}) {
		push (@{$contents{teams}}, $_->{ident});
	}

	#-------------------------------------------------------------------------------
	# Store skeleton definition into object
	#-------------------------------------------------------------------------------
	$self->{attributes}->{skeleton}->{name} = $skel_name;
	$self->{attributes}->{skeleton}->{contents} = \%contents;

	print STDERR "[Mioga2::UserList::LoadSkeleton] Leaving\n" if ($debug);
}	# ----------  end of subroutine LoadSkeleton  ----------


#===============================================================================

=head2 Load

Load object attributes from DB. This method is automatically called when trying to access an attribute whose value has not already been loaded from DB.

=cut

#===============================================================================
sub Load {
	my ($self) = @_;
	print STDERR "[Mioga2::UserList::Load] Entering\n" if ($debug);

	my $mioga_id = $self->{config}->GetMiogaId ();

	#-------------------------------------------------------------------------------
	# Initialize and run SQL query
	#-------------------------------------------------------------------------------
	my $sql = 'SELECT ' . $self->{fields} . ', coalesce (T1.group_count, 0) AS nb_groups, coalesce (T2.group_count, 0) AS nb_teams, (SELECT count(m_group.*) FROM m_group WHERE anim_id = m_user_base.rowid) AS is_anim_of, m_group_user_last_conn.last_connection FROM m_user_base LEFT JOIN (SELECT count(m_group_group.*) AS group_count, m_group_group.invited_group_id as user_id FROM m_group_group, m_group, m_group_type WHERE m_group.mioga_id = ' . $mioga_id . ' AND m_group.rowid = m_group_group.group_id AND m_group.type_id = m_group_type.rowid AND m_group_type.ident=\'group\' GROUP BY  m_group_group.invited_group_id) AS T1 ON m_user_base.rowid = T1.user_id LEFT JOIN (SELECT count(m_group_group.*) AS group_count, m_group_group.invited_group_id AS user_id FROM m_group_group, m_group, m_group_type WHERE m_group.mioga_id = ' . $mioga_id . ' AND m_group.rowid = m_group_group.group_id AND m_group.type_id = m_group_type.rowid AND m_group_type.ident=\'team\' GROUP BY m_group_group.invited_group_id) AS T2 ON m_user_base.rowid = T2.user_id, (SELECT user_id, max(m_group_user_last_conn.last_connection) AS last_connection FROM m_group_user_last_conn GROUP BY user_id) AS m_group_user_last_conn, ' . $self->GetAdditionnalTables () . ' WHERE ' . $self->GetWhereStatement () . ' AND m_group_user_last_conn.user_id = m_user_base.rowid';

	# Quite complex request in case we want a GroupList from a UserList, overwrite the $sql variable
	if ($self->Has ('groups') || $self->Has ('expanded_groups')) {
		my $groups_where = $self->Has ('groups') ? $self->Get ('groups')->GetWhereStatement () : $self->Get ('expanded_groups')->GetWhereStatement ();
		my $groups_tables = $self->Has ('groups') ? $self->Get ('groups')->GetAdditionnalTables () : $self->Get ('expanded_groups')->GetAdditionnalTables ();

		$sql = 'SELECT ' . $self->{fields} . ', m_profile.ident AS profile, m_profile.rowid AS profile_id, m_group_user_last_conn.last_connection FROM m_user_base, ' . $self->GetAdditionnalTables () . ', m_profile_user, m_profile, m_group_user_last_conn WHERE ' . $self->GetWhereStatement () . " AND m_lang.rowid = m_user_base.lang_id AND m_theme.rowid = m_user_base.theme_id AND m_user_base.mioga_id = " . $self->{config}->GetMiogaId () . " AND m_profile_user.group_id = m_user_base.rowid AND m_profile.rowid = m_profile_user.profile_id AND m_profile.group_id IN (SELECT m_group.rowid FROM m_group, $groups_tables WHERE $groups_where) AND m_group_user_last_conn.group_id = m_profile.group_id AND m_group_user_last_conn.user_id = m_user_base.rowid";

		if ($self->Has ('expanded_groups')) {
			my $users_where = $self->Get ('expanded_groups')->GetWhereStatement ();
			my $users_tables = $self->Get ('expanded_groups')->GetAdditionnalTables ();

			$sql .= " UNION (SELECT " . $self->{fields} . ", m_profile.ident AS profile, m_profile.rowid AS profile_id, m_group_user_last_conn.last_connection FROM m_user_base, " . $self->GetAdditionnalTables () . ", m_profile_expanded_user, m_profile, (SELECT group_id, user_id, max(m_group_user_last_conn.last_connection) AS last_connection FROM m_group_user_last_conn GROUP BY group_id, user_id) AS m_group_user_last_conn WHERE " . $self->GetWhereStatement () . " AND m_lang.rowid = m_user_base.lang_id AND m_theme.rowid = m_user_base.theme_id AND m_user_base.mioga_id = " . $self->{config}->GetMiogaId () . " AND m_profile_expanded_user.user_id = m_user_base.rowid AND m_profile.rowid = m_profile_expanded_user.profile_id AND m_profile.group_id IN (SELECT m_group.rowid FROM m_group, $groups_tables WHERE $groups_where) AND m_group_user_last_conn.group_id = m_profile.group_id AND m_group_user_last_conn.user_id = m_user_base.rowid)";
		}
	}

	$sql .= ' ORDER BY lastname, firstname, email';

	print STDERR "[Mioga2::UserList::Load] SQL: $sql\n" if ($debug);

	my $res = SelectMultiple ($self->{config}->GetDBH (), $sql);

	#-------------------------------------------------------------------------------
	# Drop undefined values
	#-------------------------------------------------------------------------------
	for my $user (@$res) {
		for (keys (%$user)) {
			delete ($user->{$_}) if (!defined ($user->{$_}));
		}
	}

	#-------------------------------------------------------------------------------
	# Update object contents
	#-------------------------------------------------------------------------------
	$self->{users} = $res;

	# Raise "loaded" flag if query returned something
	$self->{loaded} = 1 if (scalar (@{$self->{users}}));

	print STDERR "[Mioga2::UserList::Load] Leaving\n" if ($debug);
}	# ----------  end of subroutine Load  ----------


#===============================================================================

=head2 RequestForAttribute

Generate SQL statement that matches attribute into m_user_base.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get associated SQL request.

=back

=item B<return:> an array containing an attribute name and the statement to
match this attribute in m_user_base table. If the attribute is stored as-is
into table, this method returns the attribute and its value. If the attribute
is stores as the ID of a record in another table, this method returns the
attribute name and a SQL subquery picking the attribute ID from this other
table. If the attribute has to be ignored, this method returns empty strings.

=back

=cut

#===============================================================================
sub RequestForAttribute {
	my ($self, $attribute) = @_;

	my $request = '';
	my $value = '';
	my $operator = '=';
	my $attrval = '';

	# Change operator if a match type is specified
	if (exists ($self->{match}) && grep (/^$attribute$/, @text_fields)) {
		$operator = 'ILIKE';
	}

	#-------------------------------------------------------------------------------
	# Format string so that attribute value can be a scalar or an array
	#-------------------------------------------------------------------------------
	if (ref ($self->Get ($attribute)) eq 'ARRAY') {
		$operator = 'IN';
		for (@{$self->Get ($attribute)}) {
			$attrval = "(" . join (', ', map { $self->{config}->GetDBH()->quote ($_) } @{$self->Get ($attribute)}) . ")";
		}
		if ($attrval eq '') {
			$attrval = '(NULL)';
		}
	}
	elsif (ref ($self->Get ($attribute)) eq 'Mioga2::GroupList') {
		$operator = 'IN';
		my @rowids = $self->Get ($attribute)->GetRowIds ();
		$attrval = "(SELECT invited_group_id FROM m_group_invited_user WHERE group_id IN (" . join (', ', @rowids) . ")";
		if ($attribute eq 'expanded_groups') {
			$attrval .= " UNION SELECT invited_group_id FROM m_group_expanded_user WHERE group_id IN (" . join (', ', @rowids) . ")";
		}
		$attrval .= ")";
	}
	elsif (ref ($self->Get ($attribute)) eq 'Mioga2::TeamList') {
		$operator = 'IN';
		my @rowids = $self->Get ($attribute)->GetRowIds ();
		$attrval = "(SELECT invited_group_id FROM m_group_group WHERE group_id IN (" . join (', ', @rowids) . "))";
	}
	elsif (ref ($self->Get ($attribute)) eq 'Mioga2::ApplicationList') {
		#-------------------------------------------------------------------------------
		# Select applications
		#-------------------------------------------------------------------------------
		$operator = 'IN';
		my @rowids = $self->Get ($attribute)->GetRowIds ();
		if ($self->Has ('groups') || $self->Has ('expanded_groups')) {
			# Select functions, if required
			my $functions_where = '';
			if ($self->Has ('functions')) {
				my @idents = map { $self->{config}->GetDBH()->quote ($_) } @{$self->Get ('functions')};
				$functions_where = 'AND m_function.ident IN (' . join (', ', @idents) . ')';
			}

			# Retreive the list of users that can use the application in the groups
			my $grpattr = ($self->Has ('groups') ? 'groups' : 'expanded_groups');
			$attrval = "(SELECT m_profile_group.group_id FROM m_profile_function, m_profile_group, m_function, m_profile, m_group, m_application, m_user_base WHERE m_profile_function.profile_id = m_profile.rowid AND m_profile_function.function_id = m_function.rowid AND m_function.application_id = m_application.rowid AND m_profile_group.profile_id = m_profile.rowid AND m_profile.group_id = m_group.rowid AND m_group.rowid IN (" . join (', ', $self->Get ($grpattr)->GetRowIds ()) . ") AND m_application.rowid IN (" . join (', ', @rowids) . ") $functions_where";
			if ($grpattr eq 'expanded_groups') {
				$attrval .= "UNION SELECT m_profile_expanded_user.user_id FROM m_profile_function, m_profile_expanded_user, m_function, m_profile, m_group, m_application, m_user_base WHERE m_profile_function.profile_id = m_profile.rowid AND m_profile_function.function_id = m_function.rowid AND m_function.application_id = m_application.rowid AND m_profile_expanded_user.profile_id = m_profile.rowid AND m_profile.group_id = m_group.rowid AND m_group.rowid IN (" . join (', ', $self->Get ($grpattr)->GetRowIds ()) . ") AND m_application.rowid IN (" . join (', ', @rowids) . ") $functions_where"
			}
			$attrval .= ')';
		}
		else {
			# Retreive list of users that can use the applications
			$attrval = "(SELECT group_id FROM m_application_group WHERE application_id IN (" . join (', ', @rowids) . ") UNION SELECT m_user_base.rowid FROM m_user_base, m_application, m_instance_application WHERE m_user_base.mioga_id = m_instance_application.mioga_id AND m_application.rowid = m_instance_application.application_id AND m_application.rowid in (" . join (', ', @rowids) . ") AND m_instance_application.all_users = 't')";
		}
	}
	else {
		if ($self->Has ($attribute) && ($self->Get ($attribute) eq '')) {
			$operator = 'IS';
			$attrval = 'NULL';
		}
		else {
			$attrval = $self->{config}->GetDBH()->quote ($self->Get ($attribute));
		}
	}


	#-------------------------------------------------------------------------------
	# Perform some checks about mandatory selectors
	#-------------------------------------------------------------------------------
	if ($attribute eq 'functions' && !$self->Has ('applications')) {
		# Throw an exception if 'functions' is used without 'applications'
		throw Mioga2::Exception::User ('Mioga2::UserList::RequestForAttribute', "Current list has a 'functions' selector, but no 'applications' one...");
	}


	#-------------------------------------------------------------------------------
	# Process attribute name
	#-------------------------------------------------------------------------------
	if (grep (/^$attribute$/, qw/skeleton profile profile_id last_connection functions/)) {
		# Skeleton attribute is ignored as it doesn't match anything in DB
		# Password attribute is also ignored as it is stored encrypted and is not really significant to load user(s)
		$attribute = '';
		$value = '';
	}
	elsif (grep (/^$attribute$/, qw/timezone lang/)) {
		# Some attributes need their ID to be picked from another table
		$value = "(SELECT rowid FROM m_$attribute WHERE ident $operator $attrval)";
		$attribute .= '_id';
	}
	elsif (grep (/^$attribute$/, qw/status/)) {
		# Some attributes need their ID to be picked from another table which name is not m_$attribute
		$value = "(SELECT rowid FROM m_user_$attribute WHERE ident $operator $attrval)";
		$attribute .= '_id';
	}
	elsif (grep (/^$attribute$/, qw/type/)) {
		# Some attributes need their ID to be picked from another table which name is not m_$attribute
		$value = "(SELECT rowid FROM m_group_$attribute WHERE ident $operator $attrval)";
		$attribute .= '_id';
	}
	elsif (grep (/^$attribute$/, qw/theme/)) {
		# Some attributes need their ID to be picked from another table, matching is done through "name" field and Mioga ID
		$value = "(SELECT rowid FROM m_$attribute WHERE ident $operator $attrval AND mioga_id = " . $self->{config}->GetMiogaId () . ")";
		$attribute .= '_id';
	}
	elsif (grep (/^$attribute$/, qw/groups expanded_groups teams applications/)) {
		$value = $attrval;
		$attribute = 'rowid';
	}
	elsif (grep (/^$attribute$/, qw/password/)) {
		# Crypt password according to configured crypt method
		my $crypter = $self->{config}->LoadPasswordCrypter();
		$value = $self->{config}->GetDBH()->quote ($crypter->CryptPassword($self->{config}, { password => $self->Get ('password')}));
	}
	elsif ($attribute eq 'login') {
		$attribute = ['ident', 'email'];
		$value = $attrval;
	}
	else {
		# Other attributes are simply associated to their text value
		$value = $attrval if ($self->Has ($attribute));
	}

	my $prefix = ($self->{loading} ? 'm_user_base.' : '');
	if (ref ($attribute) eq 'ARRAY') {
		$request = '(' . join (' OR ', map { $_ eq 'email' ? "lower($prefix$_) $operator " . lc($value) : "$prefix$_ $operator $value" } @$attribute) . ')';
	}
	else {
		$request = "$prefix$attribute $operator $value" if ($attribute ne '' && $value ne '');
	}

	return ($request);
}	# ----------  end of subroutine RequestForAttribute  ----------


#===============================================================================

=head2 Has

Check if a user list has a given attribute set.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to check.

=back

=item B<return:> 1 or 0 whether the object has or not a given attribute in its
current state.

=back

B<Warning:> This method doesn't load the list from DB so it is possible that
the object does not have an attribute that the users of the list will have once
they have been loaded.

=cut

#===============================================================================
sub Has {
	my ($self, $attribute) = @_;

	return (exists ($self->{update}->{$attribute}) || exists ($self->{attributes}->{$attribute}) || ($attribute eq 'rowid'));
}	# ----------  end of subroutine Has  ----------


#===============================================================================

=head2 ApplySkeleton

Apply skeleton to list of users. This method is automatically called by the
"Store" method.

=cut

#===============================================================================
sub ApplySkeleton {
	my ($self, @parts) = @_;
	print STDERR "[Mioga2::UserList::ApplySkeleton] Entering\n" if ($debug);

	my $mioga_id = $self->{config}->GetMiogaId ();
	my $where = $self->GetWhereStatement ();

	#-------------------------------------------------------------------------------
	# Check what has to be applied
	#-------------------------------------------------------------------------------
	my @all_parts = qw/attributes applications profiles teams groups/;

	if (!scalar (@parts)) {
		print STDERR "[Mioga2::UserList::ApplySkeleton] List of parts to apply is empty, applying all parts\n" if ($debug);
		@parts = @all_parts;
	}

	my %apply = ();
	for (@parts) {
		$apply{$_} = 1;
	}

	#-------------------------------------------------------------------------------
	# Apply attributes
	#-------------------------------------------------------------------------------
	if ($apply{attributes}) {
		print STDERR "[Mioga2::UserList::ApplySkeleton] Applying attributes\n" if ($debug);
		for (keys (%{$self->{attributes}->{skeleton}->{contents}->{attributes}})) {
			$self->{update}->{$_} = $self->{attributes}->{skeleton}->{contents}->{attributes}->{$_};
		}
	}

	#-------------------------------------------------------------------------------
	# Apply applications
	#-------------------------------------------------------------------------------
	if ($apply{applications}) {
		print STDERR "[Mioga2::UserList::ApplySkeleton] Applying applications\n" if ($debug);
		my $status = 1;
		try {
			$self->EnableApplications (@{$self->{attributes}->{skeleton}->{contents}->{applications}});
			$self->ActivateApplications (@{$self->{attributes}->{skeleton}->{contents}->{applications}});
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, {module => 'Mioga2::UserList', step => __('Applications activation'), status => $status});
		};
	}

	#-------------------------------------------------------------------------------
	# Apply profiles
	#-------------------------------------------------------------------------------
	if ($apply{profiles}) {
		print STDERR "[Mioga2::UserList::ApplySkeleton] Applying profiles\n" if ($debug);
		# Create profiles
		my $status = 1;
		try {
			for my $profile (@{$self->{attributes}->{skeleton}->{contents}->{profiles}->{profile}}) {
				$self->CreateProfile ($profile);
			}
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, {module => 'Mioga2::UserList', step => __('Profiles creation'), status => $status});
		};

		# Add user to its profile
		$status = 1;
		try {
			my $user = Mioga2::UserList->new ($self->{config}, { attributes => { ident => $self->Get ('ident') }, journal => $self->{journal} });
			$self->AffectProfileToUsers ($user, $self->{attributes}->{skeleton}->{contents}->{profiles}->{animator});
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, {module => 'Mioga2::UserList', step => __('User to profile affectation'), status => $status});
		};

		# Set default profile
		$status = 1;
		try {
			$self->SetDefaultProfile ($self->{attributes}->{skeleton}->{contents}->{profiles}->{default});
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, {module => 'Mioga2::UserList', step => __('Default profile selection'), status => $status});
		};
	}

	#-------------------------------------------------------------------------------
	# Apply teams
	#-------------------------------------------------------------------------------
	if ($apply{teams}) {
		print STDERR "[Mioga2::UserList::ApplySkeleton] Applying teams\n" if ($debug);
		my $status = 1;
		try {
			$self->JoinTeams (@{$self->{attributes}->{skeleton}->{contents}->{teams}});
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, {module => 'Mioga2::UserList', step => __('Teams affectation'), status => $status});
		};
	}

	#-------------------------------------------------------------------------------
	# Apply groups
	#-------------------------------------------------------------------------------
	if ($apply{groups}) {
		print STDERR "[Mioga2::UserList::ApplySkeleton] Applying groups\n" if ($debug);
		my $status = 1;
		try {
			$self->JoinGroups (%{$self->{attributes}->{skeleton}->{contents}->{groups}});
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, {module => 'Mioga2::UserList', step => __('Groups affectation'), status => $status});
		};
	}

	print STDERR "[Mioga2::UserList::ApplySkeleton] Leaving\n" if ($debug);
}	# ----------  end of subroutine ApplySkeleton  ----------


#===============================================================================

=head2 GetDatabaseValues

Get a hash of keys / values that fit into database table. This method
translates the attributes that can not fit as-is into DB.

=cut

#===============================================================================
sub GetDatabaseValues {
	my ($self) = @_;
	print STDERR "[Mioga2::UserList::GetDatabaseValues] Entering\n" if ($debug);

	my %values = ();

	for (qw/ident autonomous firstname lastname password email time_per_day working_days monday_first external_mioga_id dn external_ident/) {
		$values{$_} = $self->Get ($_);
	}

	# Delete some fields if not set
	for (qw/external_mioga_id/) {
		delete $values{$_} if (!$self->Has ($_));
	}

	# Get rowids from text attributes
	$values{status_id} = SelectSingle ($self->{config}->GetDBH (), "SELECT rowid FROM m_user_status WHERE ident = " . ($self->Has ('status') ? $self->{config}->GetDBH()->quote ($self->Get ('status')) : "'active'"))->{rowid};
	$values{timezone_id} = SelectSingle ($self->{config}->GetDBH (), "SELECT rowid FROM m_timezone WHERE ident = " . ($self->Has ('timezone') ? $self->{config}->GetDBH()->quote ($self->Get ('timezone')) : "'Europe/Paris'"))->{rowid};
	$values{theme_id} = SelectSingle ($self->{config}->GetDBH (), "SELECT rowid FROM m_theme WHERE name = " . $self->{config}->GetDBH()->quote ($self->Get ('theme')) . " AND mioga_id = " . $self->{config}->GetMiogaId ())->{rowid};
	$values{lang_id} = SelectSingle ($self->{config}->GetDBH (), "SELECT rowid FROM m_lang WHERE locale = " . $self->{config}->GetDBH()->quote ($self->Get ('lang')))->{rowid};

	# Set default user type to local
	if (!$self->Has ('type')) {
		$self->Set ('type', 'local');
	}
	$values{type} = $self->Get ('type');

	print STDERR "[Mioga2::UserList::GetDatabaseValues] Values: " . Dumper \%values if ($debug);

	print STDERR "[Mioga2::UserList::GetDatabaseValues] Leaving\n" if ($debug);
	return (\%values);
}	# ----------  end of subroutine GetDatabaseValues  ----------


#===============================================================================
 
=head2 CreateDirectory

Create a directory in user's personal space.

=over

=item B<Note:> This method is not private, but is not intended to be used for
another reason than creating the instance admin private directory to backup
deleted users data.

=item B<arguments:>

=over

=item I<$space:> a string containing "private" or "public" to select the space the directory has to be created in.

=item I<$info:> a hash with the following keys:

=over

=item name: the directory name

=item acls: a hash describing ACLs to apply to the directory

=over

=item profile: the profile to which the ACL is defined

=item acl: the value for the ACL (see Mioga2::Constants for values)

=back

=back

=back

=back

=cut

#===============================================================================
sub CreateDirectory { # TODO (SNI - 20/04/2010 16:54): Handle creation of parent directories if not exist
	my ($self, $space, $info) = @_;
	print STDERR "[Mioga2::UserList::CreateDirectory] Entering ($space / " . ($info->{name} ? $info->{name} : '---') . ")\n" if ($debug);

	my ($dir, $uri);

	my $path = $info->{name};

	for ($self->GetUsers ()) {
		my $user = $_;

		#-------------------------------------------------------------------------------
		# Initialize directory and URI base
		#-------------------------------------------------------------------------------
		for ($space) {
			if (/private/) {
				$dir = $self->{config}->GetPrivateDir().'/'.$user->{ident};
				$uri = $self->{config}->GetPrivateURI().'/'.$user->{ident};
			}
			elsif (/public/) {
				$dir = $self->{config}->GetPublicDir().'/'.$user->{ident};
				$uri = $self->{config}->GetPublicURI().'/'.$user->{ident};
			}
			else {
				throw Mioga2::Exception::User ('Mioga2::UserList::CreateDirectory', "Incorrect space specification: $space");
			}
		}

		#-------------------------------------------------------------------------------
		# Append relative path
		#-------------------------------------------------------------------------------
		if ($path) {
			$path =~ s/^[\/]?(.*)[\/]?$/\/$1/;
			$dir .= $path;
			$uri .= $path;
		}

		#-------------------------------------------------------------------------------
		# Get parent URI
		#-------------------------------------------------------------------------------
		my $parent_sql;
		if ($path && !$info->{acls}) {
			my @uri_parts = split '/', $uri;
			pop @uri_parts;
			my @uris;
			foreach my $part (@uri_parts) {
				next unless $part;
				if (defined($uris[-1])) {
					print STDERR "[Mioga2::UserList::CreateDirectory] uris[-1] = $uris[-1]  part = $part\n" if ($debug);
					push @uris, $uris[-1]."/$part";
				}
				else {
					push @uris, "/$part";
				}
			}
			@uris = map {$self->{config}->GetDBH()->quote ($_)} @uris;
	  
			$parent_sql = "(SELECT rowid FROM m_uri WHERE (uri = " . join(" OR uri = ", @uris) . ") AND rowid = parent_uri_id ORDER BY length(uri) DESC LIMIT 1)";
		}
		else {
			$parent_sql = "currval('m_uri_rowid_seq')";
		}

		#-------------------------------------------------------------------------------
		# Create directory
		#-------------------------------------------------------------------------------
		if (! (-d $dir)) {
			mkdir ($dir, 0770)
				or throw Mioga2::Exception::User ("Mioga2::UserList::CreateDirectory", 
				__x("Cannot create dir: {directory}", directory => $dir));
		}

		#-------------------------------------------------------------------------------
		# Declare URI
		#-------------------------------------------------------------------------------
		my $size = (-s $dir) || 0;
		my $sql = "INSERT INTO m_uri SELECT nextval('m_uri_rowid_seq') AS rowid, m_user_base.rowid AS group_id, " . $self->{config}->GetDBH()->quote ($uri) . " AS uri, currval('m_uri_rowid_seq') AS parent_uri_id, 'f' AS stop_inheritance, now() AS modified, m_user_base.rowid AS user_id, '' AS lang, 'directory' AS mimetype, $size AS size FROM m_user_base WHERE m_user_base.rowid = $user->{rowid} AND m_user_base.rowid NOT IN (SELECT user_id FROM m_uri WHERE uri = " . $self->{config}->GetDBH()->quote ($uri) . ");";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		my $access = AUTHZ_WRITE;

		# Declare specified access to URI
		$sql = "INSERT INTO m_authorize (uri_id, access) SELECT m_uri.rowid AS uri_id, $access AS access FROM m_uri WHERE m_uri.uri = " . $self->{config}->GetDBH()->quote ($uri) . " AND $access NOT IN (SELECT access FROM m_authorize WHERE uri_id = m_uri.rowid);";
		ExecSQL ($self->{config}->GetDBH (), $sql);

		# Attach profile to specified access
		$sql = "INSERT INTO m_authorize_profile SELECT m_authorize.rowid AS authz_id, m_profile.rowid AS profile_id FROM m_authorize, m_profile, m_uri WHERE m_profile.group_id = $user->{rowid} AND m_profile.rowid = (SELECT profile_id from m_profile_group where profile_id in (SELECT rowid FROM m_profile WHERE group_id = $user->{rowid}) AND group_id = $user->{rowid}) AND m_authorize.uri_id = m_uri.rowid AND m_uri.uri = " . $self->{config}->GetDBH()->quote ($uri) . " AND m_authorize.access = $access;";
		ExecSQL ($self->{config}->GetDBH (), $sql);
	}
	
	print STDERR "[Mioga2::UserList::CreateDirectory] Leaving\n" if ($debug);
}	# ----------  end of subroutine CreateDirectory  ----------


#===============================================================================

=head2 SetWhereStatement

Initialize a WHERE statement that matches user list in m_user_base according to
provided attributes.

=over

=item B<arguments:>

=over

=item I<@attributes:> a list of inner attributes to base the statement on.

=back

=item B<return:> a string to paste as-is into a request, after the WHERE keyword.

=back

=cut

#===============================================================================
sub SetWhereStatement {
	my ($self, @attributes) = @_;

	# Fetch attributes, if any
	for my $attr (@attributes) {
		$self->Get ($attr);
	}

	$self->{loading} = 1;
	$self->{where} = {};

	# Set rules to m_user_base
	my @rules;
	for my $attrname (@attributes) {
		next if (grep /^$attrname$/, qw/skeleton password label nb_groups nb_teams/);
		my $request = $self->RequestForAttribute ($attrname);
		push (@rules, $request) unless ($request eq '');
	}
	push (@rules, 'm_user_base.mioga_id = ' . $self->{config}->GetMiogaId ());
	$self->{where}->{m_user_base} = join (' AND ', @rules);

	# Set rules to other tables
	$self->{where}->{m_group_type} = "m_user_base.type_id = m_group_type.rowid AND m_group_type.ident in ('local_user', 'external_user', 'ldap_user')";
	$self->{where}->{m_lang} = 'm_user_base.lang_id = m_lang.rowid';
	$self->{where}->{m_theme} = 'm_user_base.theme_id = m_theme.rowid';
	$self->{where}->{m_timezone} = 'm_user_base.timezone_id = m_timezone.rowid';
	$self->{where}->{m_user_status} = 'm_user_base.status_id = m_user_status.rowid';

	print STDERR "[Mioga2::UserList::SetWhereStatement] Rules: " . Dumper $self->{where} if ($debug);

	$self->{loading} = 0;
	delete ($self->{match});

	return ($self->{where});
}	# ----------  end of subroutine SetWhereStatement  ----------


#===============================================================================

=head2 GetWhereStatement

Generate a WHERE statement that matches user list in m_user_base.

=over

=item B<return:> a string to paste as-is into a request, after the WHERE keyword.

=back

=cut

#===============================================================================
sub GetWhereStatement {
	my ($self, @tables) = @_;
	print STDERR "[Mioga2::UserList::GetWhereStatement] Entering\n" if ($debug);

	$self->SetWhereStatement () if (!$self->{where});

	@tables = qw/m_user_base m_group_type m_lang m_theme m_timezone m_user_status m_group_type/ unless (@tables);

	my @parts;
	for my $table (@tables) {
		push (@parts, $self->{where}->{$table});
	}

	my $where = join (' AND ', @parts);

	print STDERR "[Mioga2::UserList::GetWhereStatement] Leaving: $where\n" if ($debug);
	return ($where);
}	# ----------  end of subroutine GetWhereStatement  ----------


#===============================================================================

=head2 GetAdditionnalTables

Return list of additionnal tables to include into the FROM statement.

=cut

#===============================================================================
sub GetAdditionnalTables {
	my ($self) = @_;

	if (!$self->{additionnal_tables}) {
		$self->{additionnal_tables} = 'm_lang, m_theme, m_timezone, m_user_status, m_group_type';
	}

	return ($self->{additionnal_tables});
}	# ----------  end of subroutine GetAdditionnalTables  ----------


#===============================================================================

=head2 ToCSV

Export UserList to CSV.

=cut

#===============================================================================
sub ToCSV {
	my ($self) = @_;

	my $csv = '';

	my $conv = new Mioga2::CSV (
			columns => [qw/lastname firstname email ident/]
		);

	$self->Load () if (!$self->{loaded});
	$csv = $conv->Convert ($self->{users});

	print STDERR "[Mioga2::UserList::ToCSV] $csv\n" if ($debug);

	return ($csv);
}	# ----------  end of subroutine ToCSV  ----------


#===============================================================================

=head2 RecordIntoLDAP

Record an user into LDAP directory.

=over

=item B<return:> The user's DN.

=back

=cut

#===============================================================================
sub RecordIntoLDAP {
	my ($self) = @_;

	my $values = undef;
	my $dn = undef;

	my $instance = $self->{config}->GetMiogaIdent ();

	#-------------------------------------------------------------------------------
	# Initialize LDAP connection
	#-------------------------------------------------------------------------------
	my $ldap = new Mioga2::LDAP ($self->{config});
	$ldap->BindWith ($self->{config}->GetLDAPBindDN(), $self->{config}->GetLDAPBindPwd());

	my $users = $ldap->SearchUsers (mail => $self->Get ('email'), use_instance_user_filter => 0);
	if (!scalar (@$users)) {
		#-------------------------------------------------------------------------------
		# Create user in LDAP
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::UserList::RecordIntoLDAP] Create user\n" if ($debug);
		for (qw/ident email password firstname lastname/) {
			$values->{$_} = $self->Get ($_);
		}
		push (@{$values->{instances}}, $instance);
		my $crypter = $self->{config}->LoadPasswordCrypter ();
		$values->{password} = $crypter->CryptPassword ($self->{config}, $values);
		$values->{dn} = "uid=$values->{ident}," . $self->{config}->GetLDAPUserBaseDN ();
		$values->{cn} = "$values->{firstname} $values->{lastname}";
		$ldap->AddUser ($ldap->TranslateAttributes ($values));
		$dn = $values->{dn};
	}
	else {
		#-------------------------------------------------------------------------------
		# Link existing user to instance
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::UserList::RecordIntoLDAP] Link existing user to instance\n" if ($debug);
		my $user = $users->[0];
		if (!grep (/^$instance$/, @{$user->{instances}})) {
			$dn = $user->{dn};
			$self->Set ('ident', $user->{ident});
			push (@{$user->{instances}}, $instance);
			$ldap->ModifyUser (delete ($user->{dn}), $ldap->TranslateAttributes($user));
		}
	}

	return ($dn);
}	# ----------  end of subroutine RecordIntoLDAP  ----------


#===============================================================================

=head2 UpdateLDAPPassword

Update local DB password according to LDAP contents.

=cut

#===============================================================================
sub UpdateLDAPPassword {
	my ($self) = @_;

	my $new_password;

	#-------------------------------------------------------------------------------
	# Initialize LDAP connection
	#-------------------------------------------------------------------------------
	my $ldap = new Mioga2::LDAP ($self->{config});
	$ldap->BindWith ($self->{config}->GetLDAPBindDN(), $self->{config}->GetLDAPBindPwd());

	my $user = $ldap->SearchSingleUser($self->Get ('dn'));
	if ($user) {
		$new_password = $user->{password};

		# Update user
		if ($self->{config}->GetMiogaConf ()->GetConfParameter ('store_ldap_password')) {
			$self->Set ('password', $new_password);
			$self->Store ();
		}
	}

	return ($new_password);
}	# ----------  end of subroutine UpdateLDAPPassword  ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project

=head1 SEE ALSO

Mioga2 Mioga2::GroupList Mioga2::TeamList Mioga2::ApplicationList Mioga2::InstanceList

=head1 COPYRIGHT

Copyright (C) 2011, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;
