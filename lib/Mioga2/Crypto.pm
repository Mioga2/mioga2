# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Crypto.pm: Interface on Gnu PGP (gpg) to encrypt, decrypt and authenticate
	        messages.

=head1 DESCRIPTION

This module permits to access to encryption, descryption and signing 
fonctionnalities of gpg.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Crypto;
use strict;

use Locale::TextDomain::UTF8 'crypto';

use Error qw(:try);
use Data::Dumper;
use Fcntl ':flock';
use POSIX qw(tmpnam);

use Mioga2::XML::Simple;
use Mioga2::Old::Instance;
use Mioga2::tools::database;
use Mioga2::Exception::Simple;
use Mioga2::Exception::Crypto;

my $debug = 0;

# ============================================================================

=head2 new ($config)

	Create the Mioga2::Crypto object from Mioga2::Config.
	Initialize the key ring. (extract private and publics keys from database)

=cut

# ============================================================================

sub new {
	my ($class, $config) = @_;

	my $self = {config => $config, 
				gpghome => $config->GetTmpDir()."/gpghome",
			   };
	$self->{gpgopts} = "--armor --yes --batch --no-tty --home $self->{gpghome} --status-fd 1";


	bless($self, $class);
	
	$self->InitializeKeyRing();

	return $self;
}




# ============================================================================

=head2 CreateKeyPair ()

	Create the gnupg key pair and update keys in database for current instance.

=cut

# ============================================================================

sub CreateKeyPair {
	my ($self) = @_;
	
	my $config = $self->{config};

	my $dir = tmpnam;
	mkdir($dir, 0700);

	my $server;
	my $server_name = $config->GetServerName();
	my $protocol = $config->GetProtocol();

	my $base_uri = $config->GetBaseURI();
	if(!defined $base_uri or $base_uri eq '') {
		$server = "$protocol://$server_name/".$config->GetMiogaIdent;
	}
	else {
		$server = "$protocol://$server_name$base_uri";
	}
	open(F_CMD, ">$dir-cmd")
		or die "Can't create $dir-cmd : $!";
	
	my $admin = new Mioga2::Old::User($config, rowid => $config->GetAdminId());

	print F_CMD "Key-Type: DSA\n".
	            "Key-Length: 256\n".
	            "Subkey-Type: RSA\n".
				"Subkey-Length: 256\n".
				"Name-Real: ".$admin->GetFirstname()." ".$admin->GetLastname()."\n".
				"Name-Comment: $server\n".
				"Name-Email: ".$admin->GetEmail()."\n".
				"Expire-Date: 0\n".
				'%commit'."\n";

	close(F_CMD);

	open(F_GPG, "gpg --armor --batch --no-tty --home $dir --status-fd 1 --gen-key $dir-cmd 2>&1 |")
		or die "Can't run gpg : $!";


	my $ok = 0;
	while(my $line = <F_GPG>) {
		if($line =~ /^\[GNUPG:\] KEY_CREATED/) {
			$ok = 1;
		}
	}

	if(! $ok) {
		die "Key pair creation failed !";
	}

	close(F_GPG);

	# Import Public Key 
	system("gpg --armor --batch --no-tty --home $dir --status-fd 1 --export-secret-key > $dir-secret_export");
	system("gpg --armor --batch --no-tty --home $dir --status-fd 1 --export > $dir-public_export");

	my $dbh = $config->GetDBH();

	try {
		BeginTransaction($dbh);

		my $prev = SelectSingle($dbh, "SELECT * FROM m_mioga WHERE rowid = ".$config->GetMiogaId());
		if(defined $prev->{private_key}) {
			$dbh->func($prev->{private_key}, 'lo_unlink');
		}
		if(defined $prev->{public_key}) {
			$dbh->func($prev->{public_key}, 'lo_unlink');
		}
		
		my $values;
		$values->{private_key} = $dbh->func("$dir-secret_export", 'lo_import');
		$values->{public_key} = $dbh->func("$dir-public_export", 'lo_import');

		$values->{modified} = "now()";

		my $sql = BuildUpdateRequest( $values, table  => "m_mioga",
									           other  => ['modified', 'private_key', 'public_key']						  
									  );

		$sql .= " WHERE rowid = " . $config->GetMiogaId();

		ExecSQL($dbh, $sql);

		EndTransaction($dbh);
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);

		system("rm -rf $dir-*");
		system("rm -rf $dir");

		$err->throw;
	};

	system("rm -rf $dir-*");
	system("rm -rf $dir");
}


# ============================================================================

=head2 GetMyPublicKey ()

	Return the public key of the current Mioga instance.
	
=cut

# ============================================================================

sub GetMyPublicKey {
	my ($self) = @_;

	my $dbh = $self->{config}->GetDBH();

	my $sql = "SELECT pg_largeobject.data FROM pg_largeobject, m_mioga ".
	          "WHERE  pg_largeobject.loid = m_mioga.public_key AND ".
			  "       m_mioga.rowid = ".$self->{config}->GetMiogaId();
	

	my $res = SelectSingle($dbh, $sql);
	return $res->{data};
}



# ============================================================================

=head2 CryptAndSignDataForServer ($server_name, $data)

	Crypt and sign the data for the server $server_name
	
=cut

# ============================================================================

sub CryptAndSignDataForServer {
	my ($self, $server, $data) = @_;

	my $tmpfile = tmpnam;
	my $outtmpfile = tmpnam;

	open(F_TMPFILE, ">$tmpfile");
	print F_TMPFILE $data;
	close(F_TMPFILE);

	# lock the key ring.
	open(F_LOCK, "$self->{gpghome}/read_lock");
	flock(F_LOCK, LOCK_SH);

	my $cmd = "gpg ".$self->{gpgopts}." --always-trust --output $outtmpfile -r '($server)' --sign --encrypt $tmpfile";
	print STDERR "[Mioga2::Crypto::CryptAndSignDataForServer] Command: $cmd\n" if ($debug);
	
	open(F_GPG, "$cmd 2>/dev/null|")
		or die "Can't run gpg : $!";

	my $ok = 0;
	while(my $line = <F_GPG>) {
		print STDERR "[Mioga2::Crypto::CryptAndSignDataForServer] Command output: $line" if ($debug);
		next unless ($line =~ /^\[GNUPG:\]/);
		
		if($line =~ /END_ENCRYPTION/) {
			$ok = 1;
		}
	}
	
	close(F_GPG);
	
	flock(F_LOCK, LOCK_UN);
	close(F_LOCK);

	if(!$ok) {
		throw Mioga2::Exception::Crypto("Mioga2::Crypto::CryptAndSignDataForServer", __"Encryption failed");
	}

	open(F_OUT, "$outtmpfile")
		or die "Can't open $outtmpfile : $!";

	my $crypted_data;
	{
		local $/ = undef;
		$crypted_data = <F_OUT>;
	}
	
	close(F_OUT);
	
	unlink($outtmpfile);
	unlink($tmpfile);

	return $crypted_data;
}


# ============================================================================

=head2 VerifyDecryptData ($crypted_data)

	Verify the signature and return the data and remote server name.
	
=cut

# ============================================================================

sub VerifyDecryptData {
	my ($self, $data) = @_;

	my $tmpfile = tmpnam;
	my $outtmpfile = tmpnam;

	open(F_TMPFILE, ">$tmpfile");
	print F_TMPFILE $data;
	close(F_TMPFILE);

	# lock the key ring.
	open(F_LOCK, "$self->{gpghome}/read_lock");
	flock(F_LOCK, LOCK_SH);

	my $cmd = "gpg ".$self->{gpgopts}." --always-trust --output $outtmpfile --decrypt $tmpfile";
	print STDERR "[Mioga2::Crypto::VerifyDecryptData] Command: $cmd\n" if ($debug);

	open(F_GPG, "$cmd 2>/dev/null|")
		or die "Can't run gpg : $!";

	my $sender;
	my $ok = 0;
	try {
		
		while(my $line = <F_GPG>) {
			print STDERR "[Mioga2::Crypto::VerifyDecryptData] Command output: $line" if ($debug);
			next unless ($line =~ /^\[GNUPG:\]/);	
			
			if($line =~ /GOODSIG [^\s]+ (.*)/) {
				$sender = $1;
			}
			
			elsif($line =~ /DECRYPTION_OKAY/) {
				$ok = 1;
			}
			
			elsif($line =~ /BADSIG/) {
				throw Mioga2::Exception::Crypto("Mioga2::Crypto::VerifyDecryptData", __"Bad signature");
			}
		}
		
		close(F_GPG);
		
		if(!$ok) {
			throw Mioga2::Exception::Crypto("Mioga2::Crypto::VerifyDecryptData", __"Decryption failed");
		}
	}

	otherwise {
		my $err = shift;

		flock(F_LOCK, LOCK_UN);
		close(F_LOCK);

		$err->throw;
	};
	
	flock(F_LOCK, LOCK_UN);
	close(F_LOCK);


	open(F_OUT, "$outtmpfile")
		or die "Can't open $outtmpfile : $!";

	my $decrypted_data;
	{
		local $/ = undef;
		$decrypted_data = <F_OUT>;
	}
	
	close(F_OUT);

	unlink($outtmpfile);
	unlink($tmpfile);
	
	return ($decrypted_data, $sender);
}



# ============================================================================

=head2 ImportPublicKey ($pub_key_string)

	Import a public key
	
=cut

# ============================================================================

sub ImportPublicKey {
	my ($self, $data) = @_;

	# lock the key ring.
	open(F_LOCK, "$self->{gpghome}/read_lock");
	flock(F_LOCK, LOCK_EX);

	my $datafile = "$self->{gpghome}/data_to_import";

	open(F_DATA, ">$datafile")
		or die "Can't open $datafile : $!";

	print F_DATA $data;
	close(F_DATA);

	my $cmd = "gpg ".$self->{gpgopts}." --import $datafile";

	open(F_GPG, "$cmd 2>/dev/null |")
		or die "Can't run gpg : $!";

	my $imported = 0;
	my $keyname;
	my $reason;
	while(my $line = <F_GPG>) {
		next unless ($line =~ /^\[GNUPG:\]/);
		
		if($line =~ /IMPORTED [^\s]+ (.*)/) {
			$imported = 1;
			$keyname = $1;
		}

		elsif($line =~ /IMPORT_OK (\d)+ /) {
			$reason = $1;
		}
	}

	close(F_GPG);

	unlink($datafile);

	flock(F_LOCK, LOCK_UN);
	close(F_LOCK);

	if(!$imported) {
		if($reason == 0) {
			throw Mioga2::Exception::Crypto("Mioga2::Crypto::ImportPublicKey",
			                                __"Key is already imported");
		}
		elsif($reason | 16) { # contains private key
			unlink("$self->{gpghome}/secring.gpg");
			
			$self->InitializeKeyRing();

			throw Mioga2::Exception::Simple("Mioga2::Crypto::ImportPublicKey",
			                                __"Contains Private Key !!");
		}

		throw Mioga2::Exception::Simple("Mioga2::Crypto::ImportPublicKey",
		                                __"Bad Key.");
	}

	return $keyname;
}



# ============================================================================

=head2 DeletePublicKey ($keyname)

	Delete a public key
	
=cut

# ============================================================================

sub DeletePublicKey {
	my ($self, $keyname) = @_;

	# lock the key ring.
	open(F_LOCK, "$self->{gpghome}/read_lock");
	flock(F_LOCK, LOCK_EX);

	my $dir = $self->{config}->GetTmpDir()."/gpghome";
	my $cmd = "rm -rf $dir";

	system("$cmd");

	flock(F_LOCK, LOCK_UN);
	close(F_LOCK);
}



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 InitializeKeyRing ()

	Initialize the public key ring and extract private key from database.
	
=cut

# ============================================================================

sub InitializeKeyRing {
	my ($self) = @_;
	
	if(! -e $self->{gpghome}) {
		mkdir($self->{gpghome}, 0700);
	}

	# create lock file
	open(F_LOCK, "+>$self->{gpghome}/read_lock");
	close(F_LOCK);
	
	# lock the key ring.
	open(F_LOCK, "$self->{gpghome}/read_lock");
	flock(F_LOCK, LOCK_EX);


	try {
		# Import my private keys

		my $tmpfile = $self->{gpghome}."/tmpfile";
		
		my $dbh = $self->{config}->GetDBH();

		my $sql = "SELECT pg_largeobject.data FROM pg_largeobject, m_mioga ".
	              "WHERE pg_largeobject.loid = m_mioga.private_key AND m_mioga.private_key IS NOT NULL AND ".
				  "      m_mioga.rowid = ".$self->{config}->GetMiogaId();
		

		my $res = SelectSingle($dbh, $sql);

		if(!defined $res) {
			$self->CreateKeyPair();
			$self->InitializeKeyRing();
			return;
		}

		open(F_DATA, ">$tmpfile")
			or die "Can't open $tmpfile : $!";
		print F_DATA $res->{data};
		close(F_DATA);

		$self->ImportKey($tmpfile);
		
		unlink($tmpfile);
		

		# Import my public keys
		
		$tmpfile = $self->{gpghome}."/tmpfile";
		
		$dbh = $self->{config}->GetDBH();

		$sql = "SELECT pg_largeobject.data FROM pg_largeobject, m_mioga ".
			   "WHERE pg_largeobject.loid = m_mioga.public_key AND ".
			   "      m_mioga.rowid = ".$self->{config}->GetMiogaId();
		

		$res = SelectSingle($dbh, $sql);
		open(F_DATA, ">$tmpfile")
			or die "Can't open $tmpfile : $!";
		print F_DATA $res->{data};
		close(F_DATA);

		$self->ImportKey($tmpfile);
		
		unlink($tmpfile);
		
		
		# Import all public keys
		
		$sql = "SELECT * FROM m_external_mioga ".
	           "WHERE mioga_id = ".$self->{config}->GetMiogaId();

		$res = SelectMultiple($dbh, $sql);
		
		foreach my $mioga (@$res) {
			# Extract the public key.

			$sql = "SELECT data FROM pg_largeobject ".
				   "WHERE loid = $mioga->{public_key}";
		

			my $res = SelectSingle($dbh, $sql);
			open(F_DATA, ">$tmpfile")
				or die "Can't open $tmpfile : $!";
			print F_DATA $res->{data};
			close(F_DATA);

			$self->ImportKey($tmpfile);
			
			unlink($tmpfile);
		}
	}
	otherwise {
		my $err = shift;

		flock(F_LOCK, LOCK_UN);
		close(F_LOCK);

		$err->throw;
	};

	flock(F_LOCK, LOCK_UN);
	close(F_LOCK);
}



# ============================================================================

=head2 ImportKey ($filename)

	Import the secret key in the key ring.
	
=cut

# ============================================================================

sub ImportKey {
	my ($self, $filename) = @_;

	my $cmd = "gpg ".$self->{gpgopts}." --import $filename";

	open(F_GPG, "$cmd 2>/dev/null |")
		or die "Can't run gpg : $!";

	my $ok = 0;
	while(my $line = <F_GPG>) {
		next unless ($line =~ /^\[GNUPG:\]/);
		
		if($line =~ /IMPORTED /) {
			$ok = 1;
		}

		elsif($line =~ /IMPORT_OK /) {
			$ok = 1;
		}
	}

	close(F_GPG);

	if(!$ok) {
		throw Mioga2::Exception::Simple("Mioga2::Crypto::ImportKey", 
										__"Failed to import private key in key ring");
	}

}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga2 Mioga2::Config Mioga2::URI Mioga2::Application Mioga2::AppDesc
Mioga2::Old::User Mioga2::Old::Group Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
