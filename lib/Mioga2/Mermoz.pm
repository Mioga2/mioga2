# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
	
Mermoz.pm : The Mioga2 mail sender.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Mermoz;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'mermoz';

use vars qw($VERSION $CODENAME);
$VERSION  = "1.0";
$CODENAME = 'Breguet';

use Error qw(:try);
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::mail_utils;
use MIME::Entity;
use MIME::Words qw (:all);
use Data::Dumper;

my $debug = 0;

my $max_filter = 500;
# Max recipient count allowing addresses to be CC-ed
my $max_recipients = 50;

# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc {
    my $self    = shift;
    my %AppDesc = (
        ident              => 'Mermoz',
	name		   => __('Mermoz'),
        package            => 'Mioga2::Mermoz',
        description        => __('Send mails to your contacts'),
        type               => 'normal',
        all_groups         => 1,
        all_users          => 1,
        can_be_public      => 0,
		is_user             => 1,
		is_group            => 1,
		is_resource         => 0,

        functions => {
            'Base'     => __('Send mail to group'),
            'Standard' => __('Send mail to invited team in group'),
            'Anim'     => __("Send mail to anyone from a list of choices"),
        },

        func_methods => {
            'Base'     => [ 'DisplayMain', 'SendMail', ],
            'Standard' => [ 'DisplayMain', 'SendMail', ],
            'Anim'     => [ 'DisplayMain', 'SendMail', ],
        },

        sxml_methods => {},

        xml_methods => {},

		non_sensitive_methods => [
			'DisplayMain'
		]
    );
    return \%AppDesc;
}

# ============================================================================
# Public Methods
# ============================================================================
# ============================================================================

=head2 DisplayMain ()

=cut

# ============================================================================

sub DisplayMain {
    my ( $self, $context ) = @_;
	print STDERR "Mermoz::DisplayMain()\n" if ($debug);
    my $data = { DisplayMain => $context->GetContext, };
    my $config = $context->GetConfig;
    my $group_obj = Mioga2::GroupList->new ($self->{config});
    my $user_obj = Mioga2::UserList->new ($self->{config});
    my $count_elem = $group_obj->Count() * $user_obj->Count();
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'no_header', 'from_magellan', 'success'], 'allow_empty', 'bool'],
													 [ [ 'path'], 'allow_empty', ['location', $config ]],
													 [ [ 'entries'], 'allow_empty', ['location', $config ] ], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Mermoz::DisplayMain", ac_FormatErrors($errors));
	}

    $data->{DisplayMain}->{Instance} = $context->GetConfig->GetMiogaIdent;
    $data->{DisplayMain}->{Infos} = {
        version  => $VERSION,
        codename => $CODENAME,
    };
	$data->{DisplayMain}->{MaxRecipients} = $max_recipients;

    my $user  = $context->GetUser;
    my $group = $context->GetGroup;
    $data->{DisplayMain}->{ContactList} = $self->GetContactList($context, $count_elem);
    $data->{DisplayMain}->{CurrentUser} = {
        name  => $user->GetName,
        ident => $user->GetIdent,
        rowid => $user->GetRowid,
        email => $user->GetEmail
    };
	if (ref ($group) ne 'Mioga2::Old::User') {
		# Not in user context, detail current group attributes
		my $group_obj = Mioga2::GroupList->new ($config, { attributes => { rowid => $group->GetRowid () } });
		my $count = $group_obj->GetExpandedUserList ()->Count ();
		$data->{DisplayMain}->{CurrentGroup} = {
			ident => $group->GetIdent,
			rowid => $group->GetRowid,
			count => $count
		};
	}
    $data->{DisplayMain}->{Signature} = {
        enabled => $self->SignatureEnabled($context),
        'contents'    => $self->GetSignature($context)
    };

    if ( $values->{from_magellan} == '1' ) {
	print STDERR "from_magellan".Dumper($context).")\n" if ($debug);
        if ( ref( $values->{entries} ) ne 'ARRAY' ) {
            $values->{entries} = [ $values->{entries} ];
        }

		# __n from Locale::TextDomain does not seem to work correctly, bypassing
		my $subject;
		if (scalar @{$values->{entries}} == 1) {
			$subject = __("Link to file");
		}
		else {
			$subject = __("Links to files");
		}
        $data->{DisplayMain}->{Subject} = $subject;

		my $desturi = new Mioga2::URI ($config, uri => $values->{path});
		my $path = $config->GetProtocol () . '://' . $config->GetServerName () . $config->GetBinURI () . '/' . $desturi->GetGroup()->GetIdent () . '/Magellan/DisplayMain?path=' . $values->{path};
        my $message = __x(
"Link to directory in Magellan: <a href=\"{parent_folder}\">{parent_folder}</a><br/>\n",
            parent_folder => $path
        );
        foreach my $entry ( @{ $values->{entries} } ) {
            $message .= __x(
                "File: <a href=\"{file}\">{file}</a><br/>",
                file => $config->GetProtocol() . "://"
                  . $config->GetServerName()
                  . $entry
            );
        }
        $data->{DisplayMain}->{Message} = $message;
    }
    $data->{DisplayMain}->{Success} = {} if $values->{success} == '1';
	print STDERR "data = ".Mioga2::tools::Convert::PerlToXML($data)."\n" if ($debug);

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'mermoz.xsl', locale_domain => "mermoz_xsl");
    $content->SetContent(Mioga2::tools::Convert::PerlToXML ($data));

    return $content;
}

# ============================================================================

=head2 SendMail ()

=cut

# ============================================================================

sub SendMail {
    my ( $self, $context ) = @_;

    my $data = { SendMail => $context->GetContext, };
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'toggle-signature', 'detail-recipients' ], 'allow_empty', 'bool'],
													 [ [ 'subject', 'contents', 'signature'], 'allow_empty', 'stripxws'],
													 [ [ 'recipients'], 'allow_empty'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Mermoz::DisplayMain", ac_FormatErrors($errors));
	}


    $data->{SendMail}->{Instance} = $context->GetConfig->GetMiogaIdent;
    $data->{SendMail}->{Infos} = {
        version  => $VERSION,
        codename => $CODENAME,
    };
    my $config = $context->GetConfig;

    my $recipients = $values->{recipients};
    my @recipients = split /;/, $recipients;

	my @emails = ();

    my @group_ids = grep /group-/, @recipients;
    map { $_ =~ s/group-// } @group_ids;
    my $group_ids = \@group_ids;

	if (@group_ids) {
		my $groups = Mioga2::GroupList->new( $config, { attributes => { rowid => \@group_ids } });
		my $groupusers = Mioga2::UserList->new( $config, { attributes => { groups => $groups, status => 'active' } });
		push(@emails, map { $_->{email} } $groupusers->GetUsers);
	}

	my @team_ids = grep /team-/, @recipients;
	map { $_ =~ s/team-// } @team_ids;

	if (@team_ids) {
		my $teams = Mioga2::TeamList->new( $config, { attributes => { rowid => \@team_ids } });
		my $teamusers = Mioga2::UserList->new( $config, { attributes => { teams => $teams, status => 'active' } });
		push(@emails, map { $_->{email} } $teamusers->GetUsers);
	}

	my @user_ids = grep /user-/, @recipients;
	map { $_ =~ s/user-// } @user_ids;

	if (@user_ids) {
		my $users = Mioga2::UserList->new( $config, { attributes => { rowid => \@user_ids, status => 'active' } });
		push(@emails, map { $_->{email} } $users->GetUsers);
	}

    my %saw;
    my @uniq_mails = grep( !$saw{$_}++, @emails );

    $self->SaveSignature($context);
    $self->CreateMails( $config, $context->GetUser, \@uniq_mails, $context->{args} );

    my $content = new Mioga2::Content::REDIRECT( $context, mode => 'external' );
    $content->SetContent('DisplayMain?success=1');

    return $content;
}

# ============================================================================
# Private Methods
# ============================================================================

# ============================================================================

=head2 SaveSignature ()

=cut

# ============================================================================

sub SaveSignature {
    my ( $self, $context ) = @_;

    my $config  = $context->GetConfig;
    my $dbh     = $config->GetDBH;
    my $user    = $context->GetUser;
    my $user_id = $user->GetRowid;
    $self->GetSignature($context);

    my $enabled   = 'false';
    my $signature = "";
    if ( $context->{args}->{'toggle-signature'} == '1' ) {
        $enabled   = 'true';
        $signature = ", signature = '"
          . st_FormatPostgreSQLString( $context->{args}->{signature} ) . "'";
    }

    ExecSQL( $dbh,
"UPDATE mermoz_signature SET enabled = '$enabled'$signature WHERE user_id = $user_id"
    );
}

# ============================================================================

=head2 GetSignature ()

=cut

# ============================================================================

sub GetSignature {
    my ( $self, $context ) = @_;

    my $config  = $context->GetConfig;
    my $dbh     = $config->GetDBH;
    my $user    = $context->GetUser;
    my $user_id = $user->GetRowid;

    my $signature = SelectSingle( $dbh,
        "SELECT signature FROM mermoz_signature WHERE user_id = $user_id" );
    if ( !$signature ) {
        ExecSQL( $dbh,
"INSERT INTO mermoz_signature(user_id, signature) VALUES($user_id, '')"
        );
        $signature = { signature => "" };
    }

    return $signature->{signature};
}

# ============================================================================

=head2 SignatureEnabled ()

=cut

# ============================================================================

sub SignatureEnabled {
    my ( $self, $context ) = @_;

    my $config  = $context->GetConfig;
    my $dbh     = $config->GetDBH;
    my $user    = $context->GetUser;
    my $user_id = $user->GetRowid;

    my $signature = SelectSingle( $dbh,
        "SELECT enabled FROM mermoz_signature WHERE user_id = $user_id" );
    if ( !$signature ) {
        ExecSQL( $dbh,
"INSERT INTO mermoz_signature(user_id, signature) VALUES($user_id, '')"
        );
        $signature = { enabled => 'f' };
    }

    return $signature->{enabled};
}

# ============================================================================

=head2 CreateMails ()

=cut

# ============================================================================

sub CreateMails {
    my ( $self, $config, $user, $mail_list, $args ) = @_;
	print STDERR "Mermoz::CreateMails\n" if ($debug);
	print STDERR "contents = ".$args->{contents}."\n" if ($debug);

    my $subject           = encode_mimeword($args->{subject}, "Q", "UTF-8");
	print STDERR "subject encoded : $subject\n" if ($debug);
    my $signature         = $args->{signature};
    my $enabled_signature = $args->{'toggle-signature'};
    my $contents          = $args->{contents}." ";
    my $from              = $user->GetEmail;


	$signature = st_FilterHTMLTags(\$signature);
	$signature =~ s#\n#<br/>#g;
    $contents .= "<p>--<br/>\n$signature</p>" if ( $enabled_signature == '1' );
    $contents = "<html>\n<head></head>\n<body>$contents</body>\n</html>";

    my $entity = MIME::Entity->build(
        Charset => 'UTF8',
        Data    => $contents,
        Type    => "text/html",
        From    => $from,
        Subject => $subject
    );

    my $head = $entity->head;
	if (($args->{'detail-recipients'}) && (scalar (@$mail_list) < $max_recipients)) {
		my $to = join (', ', @$mail_list);
		if ( $head->count("To") > 0 ) {
			$head->replace( "To" => $to );
		}
		else {
			$head->add( "To" => $to );
		}
		mu_SendMail( $config, $entity );
	}
	else {
		foreach my $email (@$mail_list) {
			if ( $head->count("To") > 0 ) {
				$head->replace( "To" => $email );
			}
			else {
				$head->add( "To" => $email );
			}

			mu_SendMail( $config, $entity );
		}
	}
}

# ============================================================================

=head2 GetContactList ()

=cut

# ============================================================================

sub GetContactList {
    my ( $self, $context, $count_elem ) = @_;
    my $user   = $context->GetUser;
    my $group  = $context->GetGroup;
    my $config = $context->GetConfig;

    my $base =
      $self->CheckUserAccessOnFunction( $context, $user, $group, "Base" );
    my $std =
      $self->CheckUserAccessOnFunction( $context, $user, $group, "Standard" );
    my $anim =
      $self->CheckUserAccessOnFunction( $context, $user, $group, "Anim" );

    my $list = {};
    if ( $base || $std || $anim ) {
        $list->{'Group'} = [];
        my $groups = $user->GetExpandedGroups->GetIdents;

        if (
            $self->CheckUserAccessOnMethod(
                $context, $user, $user, 'SendMail'
            )
          )
        {
            push @{ $list->{'Group'} },
              $self->AddInfosToContactList( $user->GetIdent,
                $user, $base, $std, $anim, $count_elem );

        }
        foreach my $ident (@$groups) {
            my $group = Mioga2::Old::Group->new( $config, ident => $ident );
			$base = $self->CheckUserAccessOnFunction( $context, $user, $group, "Base" );
			$std = $self->CheckUserAccessOnFunction( $context, $user, $group, "Standard" );
			$anim = $self->CheckUserAccessOnFunction( $context, $user, $group, "Anim" );

			if ($base || $std || $anim) {
				push @{ $list->{'Group'} },
				  $self->AddInfosToContactList( $ident, $group, $base, $std,
					$anim, $count_elem );
			}
        }
    }
    return $list;
}

sub AddInfosToContactList {
    my ( $self, $ident, $group, $base, $std, $anim, $count_elem ) = @_;

    my $infos = { ident => $ident, rowid => $group->GetRowid };
    my $user_list = $group->GetInvitedUsers->GetRowids;
	my $count_user_in_team = 0;
	my $count_user_in_group = 0;
    if ($std) {
        my $team_idents = $group->GetInvitedTeams->GetIdents;
        my $team_ids    = $group->GetInvitedTeams->GetRowids;
        my $teams       = [];
        for ( my $idx = 0 ; $idx < scalar @$team_idents ; $idx++ ) {
			$count_user_in_team += $self->{teams}->{counts}->{$team_idents->[$idx]};
            push @$teams,
              {
                rowid => $team_ids->[$idx],
                ident => $team_idents->[$idx],
				count => $self->{teams}->{counts}->{$team_idents->[$idx]}
              };
        }
        $infos->{Team} = $teams;
    }

	my $user_idents = $group->GetExpandedUsers->GetIdents;
	$count_user_in_group = scalar @$user_idents;
	if ($anim) {
		my $users       = [];
		if ( $count_elem < $max_filter ) {
			my $firstnames  = $group->GetExpandedUsers->GetFirstnames;
			my $lastnames   = $group->GetExpandedUsers->GetLastnames;
			my $user_ids    = $group->GetExpandedUsers->GetRowids;
			for ( my $idx = 0 ; $idx < $count_user_in_group ; $idx++ ) {
				push @$users,
				  {
					rowid => $user_ids->[$idx],
					ident => $user_idents->[$idx],
					name  => $lastnames->[$idx] . " " . $firstnames->[$idx],
				  };
			}
		}
		$infos->{User} = $users;
	}

	my $total = $count_user_in_group + $count_user_in_team;
	$infos->{count} = $total;
    return $infos;
}

# ============================================================================
# RevokeUserFromGroup ($self, $config, $group_id, $user_id, $group_animator_id)
#
#    Remove user data in database when a user is revoked of a group.
#
#    This function is called when :
#    - a user is revoked of a group
#    - a team is revoked of a group for each team member not namly
#      invited in group
#    - a user is revoked of a team  for each group where the team is
#      member and the user not namly invited in group
#
# ============================================================================
sub RevokeUserFromGroup {
    my ( $self, $config, $group_id, $user_id, $anim_id ) = @_;

	my $dbh = $config->GetMiogaConf()->GetDBH();

	# Remove user signature if any
	my $sign = SelectSingle ($dbh, "SELECT rowid FROM mermoz_signature WHERE user_id=$user_id");
	if (defined ($sign)) {
		try {
			ExecSQL ($dbh, "DELETE FROM mermoz_signature WHERE user_id=$user_id");
		}
		otherwise {
			my $err = shift;		
			RollbackTransaction($dbh);
			$err->throw;
		};
	}
}

# ============================================================================
# DeleteGroupData ($self, $config, $group_id)
#
#    Remove group data in database when a group is deleted.
#
#    This function is called when a group/user/resource is deleted.
#
# ============================================================================
sub DeleteGroupData {
    my ( $self, $config, $group_id ) = @_;
}


#===============================================================================

=head2 InitApp

Application-specific initialization.

=cut

#===============================================================================
sub InitApp {
	my ($self, $context) = @_;

	$self->{config} = $context->GetConfig ();

	# Initialize user-count for each team of instance
	# For further use in "AddInfosToContactList"
	my $teams = Mioga2::TeamList->new ($self->{config});
	my @list = $teams->GetTeams ();
	%{$self->{teams}->{counts}} = map { $_->{ident} => $_->{nb_users} } @list;
}	# ----------  end of subroutine InitApp  ----------

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2009, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__

