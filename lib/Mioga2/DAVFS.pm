# ============================================================================
# Mioga2 Project (C) 2003-2010 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME

DAVFS.pm: Single point of file access.

=head1 DESCRIPTION

This module provides the _only_ way to read, modify, or delete files on the
server's file system which are administrated by Mioga. It is used by WebDAV
access (Magellan, third-party WebDAV clients) via DAVProxy, by interactive
Mioga applications like Louvre, and by Mioga daemons like the Notifier.

The job of Mioga2::DAVFS is to synchronise file system access with the corresponding
database manipulations.

This class is not thread-safe (although it would not be difficult to change this).
Every thread or process must have its own DAVFS instance.

=head1 SCHEME

 o
/|\ WebDAV
/ \ access
 .--------.
 | Apache |--.
 '--------'  |                   o
      handler|                  /|\
             |                  / \
             v            .-------------.        .--------.
       .----------.       |    Mioga    |        | Mioga  |
       | DAVProxy |       | application |        | daemon |
       '----------'       '-------------'        '--------'
             |                   |                    |
             '----------------.  |  .-----------------'
                              v  v  v
                         *****************
                         * Mioga2::DAVFS *
                         *****************
                               |   |            _.-----._
             .-------------.   |   |          .-         -.
             |  mod_dav    |   |   |          |-_       _-|
       .-----| Apache loc. |<--'   '--------->|  ~-----~  |
       |     '-------------'                  |           |
       v                                      `._       _.'
  __________                                     "-----"
 [_|||||||_°]                                    database
 [_|||||||_°]
 [_|||||||_°]

 file system


=head1 IMPLEMENTATION

Mioga2::DAVFS is based on former code in the two modules L<Mioga2::Magellan::DAV>
and L<Mioga2::DAVProxy>.

For economical reasons, we keep the same architecture as before: WebDAV access
is proxified to mod_dav of an "internal" Apache location. In the long run,
one might think of an extension of mod_dav (written in C) instead.

=head1 API

The functions mirror the WebDAV methods. As a consequence, clients of Mioga2::DAVFS
speak WebDAV with it. Comfort methods are added as appropriate into the 
Mioga2::DAVFSUtil class, which is supposed to be used by the majority of Mioga applications.

=head1 ACCESS CONTROL

When access comes via DAVProxy, Apache's autorisation handler has already run.
Otherwise we have to do the necessary checks ourselves! The caller of this
module has to be responsible and not disguise himself as DAVProxy when he is not.

=head1 ERROR HANDLING

File system and database must stay consistent. DAV errors are handled if necessary,
files being PUT are replaced with their original versions. However, database errors
may result in an inconsistent state. For the moment, their handling is outside
the scope of DAVFS.

=head1 EFFICIENCY OF FILE TRANSFER

Wherever possible, file handles are used as parameters and returned references,
to avoid unnecessary local copies.

=head1 CONCURRENCY

This module communicates with an internal Apache server (mod_dav) and with a
PostgreSQL database. Each of those provide means to ensure consistency for
concurrent access. The missing weak point is the combination of these two.
However, we consider a case of a truly concurrent modification of the same file
to be very rare; the problems with elaborate file-locking schemes may well be
greater.

=cut

#
# ============================================================================

package Mioga2::DAVFS;
use strict;
use warnings;
use open ':encoding(utf8)';
use utf8;

use Data::Dumper;
use Error qw(:try);
use File::Copy;
use File::MimeInfo::Magic;
use File::Spec;
use File::Temp;
use LWP::UserAgent;
use HTTP::Request;
use HTTP::Status;
use IO::Dir;
use Mioga2::Apache;
use Mioga2::Authz;
use Mioga2::Classes::History;
use Mioga2::Classes::URI;
use Mioga2::Exception::DB;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::Convert;
use XML::LibXML ();
use Mioga2::Constants;

my $debug = 0;

my $MULTI_STATUS	= 207;	# code DAV for the Multi-status response

=head2 new ($config)

	Create a new Mioga2::DAVFS object. Takes a Mioga2::Config as a parameter.
	Further optional parameters:
	
	* protocol => 'http'|'https'
	If not specified, the dav_protocol variable is used.
	
	* host => 'localhost'|'...'
	If not specified, the dav_host variable is used.
	
	* port => '80'|'...'
	If not specified, the dav_port variable is used.
	
	* notify => 1 | 0
	Whether DAV actions should trigger notifications. Default is 1.

=cut

sub new {
	my ( $class, $config, %opts ) = @_;
	my $self = { 
		config   => $config,
		tempdir  => '/tmp',
		protocol => (exists $opts{protocol} ? $opts{protocol} : $config->GetDAVProtocol),
		host => (exists $opts{host} ? $opts{host} : $config->GetDAVHost),
		port => (exists $opts{port} ? $opts{port} : $config->GetDAVPort),
		notify   => (exists $opts{notify}   ? $opts{notify}   : 1 )
	};
	bless $self, $class;
	return $self;
}

=head2 DAVRequest ($method, $uri_obj, $callbacks, $user)

	This is the most important method to be called from clients of this module.
	
	Runs authorisation checks and executes the DAV request.
	
	$uri_obj is a Mioga2::URI object. $callbacks is a Mioga2::Callbacks object
	that handles different means of communicating to and from mod_dav.
	
	$user can be a Mioga2::Old::User or undef. In the latter case, one is assumed to be
    in the public area.

=cut

sub DAVRequest {
	my ($self, $method, $uri_obj, $callbacks, $user) = @_;
	my $config = $self->{config};
	
	print STDERR "[Mioga2::DAVFS::DAVRequest] HTTP $method for ".$uri_obj->GetURI
			.", user ".$user->GetIdent."\n" if $debug;
	
	my $permission = Mioga2::Authz::CheckWebDavAuthz(
			Mioga2::DAVFS::PseudoRequest->new("CheckWebDavAuthz", $method),
			$config, $uri_obj, $user);
		
	if ($permission ne Mioga2::Apache::HTTP_OK) {
		print STDERR "[Mioga2::DAVFS::DAVRequest] Skipping request,"
				. " Mioga2::Authz::CheckWebDavAuthz already returned HTTP status $permission\n"
				if $debug;
		$callbacks->{status}->($permission); # might be 403 or 404
		return $permission;
	}
	
	return $self->DAVRequestAfterClearance($method, $uri_obj, $callbacks, $user);
}
	

=head2 DAVRequestAfterClearance ($method, $uri_obj, $callbacks, $user)

    B<Only DAVProxy is allowed to call this method. Everyone else must use either DAVRequest()
    or something from Mioga2::DAVFSUtils.>

    Executes a DAV request on behalf of DAVProxy. Assumes that authorisation checks have already
    taken place. $uri_obj is a Mioga2::URI object. $callbacks has to be a L<DAVFS::Callbacks>
    object.

    $user can be a Mioga2::Old::User or undef. In the latter case, one is assumed to be
    in the public area.

=cut

sub DAVRequestAfterClearance {
	my ( $self, $method, $uri_obj, $callbacks, $user ) = @_;
	my $config = $self->{config};
	my $dbh    = $config->GetDBH;

	print STDERR "[Mioga2::DAVFS::DAVRequestAfterClearance] Invoked with method = $method, uri = " . $uri_obj->GetURI . ", user = " . ( defined($user) ? $user->GetName : "[undef]" ) . "\n" if $debug;

	$self->prepareCodeForRollback( $method, $uri_obj, $user, $callbacks->{uri}->() );

	my $dbres;
	my $content_ref;
	{
		my $string = '';
		$content_ref = \$string;
	}
	my $res = $self->make_request( $method, $callbacks, $content_ref );
	if ($res->code >= 500) {
		throw Mioga2::Exception::Simple ('Mioga2::DAVProxy::proxy_handler', $res->status_line);
	}
	warn "Received response from server:\n" . Dumper($res) if ( $debug > 2 );
	print STDERR "code = " . $res->code . " status_line = " . $res->status_line . "\n" if ($debug);

	if ( $method !~ /GET/i || ( $res->code >= 300 && $res->code <= 307 ) ) {

		if ( ResponseIsSuccess( $res ) ) {
			print STDERR "is_success 0K \n" if ($debug);
			BeginTransaction($dbh);
			try {
				$self->updateMiogaDB( $res, $uri_obj, $user );
				EndTransaction($dbh);
				$dbres = Mioga2::Apache::HTTP_OK;
			}
			catch Mioga2::Exception::DB with {
				my $err = shift;
				RollbackTransaction($dbh);
				$dbres = Mioga2::Apache::HTTP_SERVER_ERROR;

				# Throw exception to level above (Mioga2::DAVProxy)
				$err->throw ();
			};
		}
		else {
			print STDERR "is_success KO \n" if ($debug);
			$dbres = $self->{rollback_files}->() || $dbres;
		}
		print STDERR "dbres = $dbres\n" if $debug and $dbres;

		my $ct = $res->header('Content-type');
		$callbacks->{content_type}->($ct) if defined($ct) and length($ct) > 0;
		$callbacks->{status}->( $res->code );
		$callbacks->{status_line}->( $res->status_line );
		$content_ref = $res->content_ref if $res->content_ref;
	}

	if ( defined($dbres) and $dbres ne Mioga2::Apache::HTTP_OK ) {
		$self->forget($callbacks);
		return $dbres;
	}

	my $dav_uri   = $config->GetDAVBasePath;
	my $mioga_uri = $config->GetBasePath;

	print STDERR "continue processing...\n" if ($debug);
	if ($$content_ref) {
		print STDERR "content exists\n" if ($debug);
		print STDERR "content = " . $$content_ref if $debug;
		if (
			( $method !~ /GET/i )
			|| ( ( $res->headers->{'title'} =~ /index of/i )
				&& !( $res->code >= 300 && $res->code <= 307 ) )
		  )
		{
			$$content_ref =~ s/$dav_uri/$mioga_uri/g;
		}
		if ($user) {
			my $orig_uri =
			  Mioga2::Classes::URI->new( uri => $callbacks->{uri}->() );
			if ( $method =~ /PROPFIND/i && ResponseIsSuccess( $res ) ) {
				$content_ref = $self->retrieveDAVAuthFiles( $config, $orig_uri, $content_ref, $user, $callbacks->{remote_host}->() );
			}
			elsif ($method =~ /GET/i && ResponseIsSuccess( $res ) && $res->headers->{'title'} =~ /index of/i )
			{
				$callbacks->{content_type}->('text/html');
				$content_ref =
				  $self->retrieveGETAuthFiles( $config, $orig_uri, $content_ref, $user );
			}
		}

		# Translate URL protocol in redirects when remote host is really remote
		if (($res->code == 301) && ($callbacks->{remote_host}->() ne 'localhost')) {
			my $dav_protocol = $self->{protocol};
			my $protocol = $self->{config}->GetProtocol ();
			$res->headers->{location} =~ s/^$dav_protocol/$protocol/;
		}
	}
	else {
		# Empty contents Apache 2.4 will not send Content-Type in such a case, force it
		my $ct = ($res->headers->{'content-type'} && ($res->headers->{'content-type'} ne '')) ? $res->headers->{'content-type'} : getMimeType( $uri_obj->GetRealPath );
		$callbacks->{content_type}->($ct);
	}

	if ($method !~ /GET/i or $$content_ref) {
		print STDERR "process headers\n" if ($debug);
		my $table = $callbacks->{headers_out}->();
		if (defined $table) {
			$table->clear;
		foreach my $header ( $res->headers->header_field_names ) {
				if ($header =~ /content-length/i) {
					$res->headers->header($header => length($$content_ref));
				}
				if ($header =~ /location/i && ( $res->code >= 300 && $res->code <= 307 )) {
					my $uri = $res->headers->header($header);
					$uri =~ s/$dav_uri/$mioga_uri/;
					$res->headers->header ($header => $uri);
				}
				$table->add( $header, $res->headers->header($header) );
			}
		}

		print STDERR "content = " . $$content_ref if $debug;
		$callbacks->{content_write}->($$content_ref);
	}

	$self->forget($callbacks);

	return Mioga2::Apache::HTTP_OK;
}

=head2 prepareCodeForRollback()

	Sets up a code block for rolling back changes. Saved files are
	restored when the DAV request fails; DAV changes are rolled back
	when the database request fails. This may save copies, link existing files,
	create inverse WebDAV requests - whatever is necessary.
		
=cut

sub prepareCodeForRollback() {
	my ( $self, $method, $uri_obj, $user, $uri ) = @_;

	# Roll back FILES

	if ( $method =~ /PUT/i ) {
		my $path = $uri_obj->GetRealPath;    # a single file
		print STDERR
"[Mioga2::DAVFS::prepareCodeForRollback] PUT preserve_file path = $path"
		  if $debug;
		if ( -f $path ) {
			print STDERR "  exists" if ($debug);
			$self->{preserved_file_for_PUT} = $self->preserveFile($uri_obj);
		}
		print STDERR "\n" if ($debug);
	}

	$self->{rollback_files} = sub {
		my $dbres = undef;
		if ( $method eq 'PUT' || $method eq 'DELETE' ) {
			my $new_uri = $uri_obj->GetURI;
			if ( $method eq 'PUT' and $self->{preserved_file_for_PUT})	{
				# in case of DELETE, preserved_file is undef
				$self->{preserved_file_for_PUT} =~ /(~[0-9]+)$/;
				$new_uri .= $1;
			}
			my $realpath = $uri_obj->GetRealPath;
			if ( $method eq 'PUT' or -e $realpath ) {    # do nothing for DELETE 404 and similar
				my $config  = $self->{config};
				my $dbh     = $config->GetDBH;
				my $user_id = $user->GetRowid;
				BeginTransaction($dbh);
				try {
					my $mime = getMimeType( $uri_obj->GetRealPath );
					createURI( $config, $new_uri, $user_id, $mime,
						getSize( $uri_obj->GetRealPath ) );
					EndTransaction($dbh);
					$dbres = Mioga2::Apache::HTTP_OK;
				}
				otherwise {
					my $err = shift;
					print STDERR "[Mioga2::DAVFS::rollback_files] Exception = "
					  . Dumper($err) . "\n";
					RollbackTransaction($dbh);
					$dbres = Mioga2::Apache::HTTP_SERVER_ERROR;
				};
			}
		}
		return $dbres;
	};

	# Rolling back DAV requests is no longer in the scope of DAVFS.
	# If you want some code for this functionality, look into SVN r3525 as compared to r3528.
}

=head2 forget

	Forgets about "prepared code".
	
=cut

sub forget {
	my ($self, $callbacks) = @_;
	if (exists $self->{preserved_file_for_PUT}) {
		$callbacks->{register_pnotes}->($self->{preserved_file_for_PUT});
		delete $self->{preserved_file_for_PUT};
	}
	
	delete $self->{rollback_files};
}

# ============================================================================

=head2 retrieveDAVAuthFiles ($config, $request, $orig_uri, $body)

    Cleans the body of PROPFIND to delete entries not available to current user.

    return the new body.

=cut

# ============================================================================

sub retrieveDAVAuthFiles {
	my ( $self, $config, $orig_uri, $body_ref, $user, $remote_host ) = @_;

	my $parser = XML::LibXML->new;
	$parser->keep_blanks(0);

	my $detailed_list;

	# If requested by Magellan
	if ($remote_host && ($remote_host eq 'localhost')) {
		$detailed_list = getURIList( $config, $user->GetRowid, $orig_uri->as_trimed_string );
	} else {
		$detailed_list = getURIList( $config, $user->GetRowid, $orig_uri->as_trimed_string, 1 );
	}

	my $uri_list;
	@$uri_list = map { $_->{uri} } @$detailed_list;
	push @$uri_list, $orig_uri->as_trimed_string;

	my $doc   = $parser->parse_string($$body_ref);
	my $root  = $doc->documentElement;
	my @nodes = $root->childNodes;
	my @davns = grep( $_->value eq 'DAV:', $root->getNamespaces );
	my $ns    = $davns[0]->getLocalName;
	foreach my $node (@nodes) {
		$node->setAttribute ('xmlns:' . MIOGA_DAV_PROP_NS, MIOGA_DAV_PROP_NS_URL);
		my @href = $node->getElementsByTagName( $ns . ':href' );
		my $link = $href[0]->textContent;
		utf8::decode($link);
		my $node_uri =
		  Mioga2::Classes::URI->new( uri => $link )->as_trimed_string;


		# Add custom properties from Mioga database
		my @uris = grep { ref ($_) eq 'HASH' && $_->{uri} eq $node_uri } @$detailed_list;
		if (scalar (@uris) == 1) {
			my $node = $node->getChildrenByTagName ('D:propstat')->[0]->getChildrenByTagName ('D:prop')->[0];
			for my $prop (qw/elements size accesskey mimetype author comments can_set_history group_has_history history_enabled history email louvre diderot manage group_ident setprops hidden view_hidden/) {
				my $subnode = new XML::LibXML::Element (MIOGA_DAV_PROP_NS . ":$prop");
				$subnode->appendText ($uris[0]->{$prop}) if (defined ($uris[0]->{$prop}));
				$node->addChild ($subnode);
			}
		}

		unless ( grep( $_ eq $node_uri, @$uri_list ) ) {
			my $remove_node = 1;
			if (!SelectSingle ($config->GetDBH (), "SELECT rowid FROM m_uri WHERE uri = '" . st_FormatPostgreSQLString( $node_uri ) . "'")) {
				try {
					# Object is present on disk but not declared into database, report it
					print STDERR "[Mioga2::DAVProxy::RetrieveDAVAuthFiles] Found file on disk that is not declared into database: $node_uri\n";

					# Re-integrate object
					my $uri_obj = Mioga2::URI->new ($config, uri => $node_uri);
					my $mime = getMimeType( $uri_obj->GetRealPath );
					createURI( $config, $uri_obj->GetURI, $uri_obj->GetGroup->GetAnimId, $mime,
						getSize( $uri_obj->GetRealPath ) );

					if ($remote_host && ($remote_host eq 'localhost')) {
						# Request coming from Magellan, leave node and set its status so that error is reported to user
						$remove_node = 0;
						my $subnode = $node->getChildrenByTagName ('D:propstat')->[0]->getChildrenByTagName ('D:status')->[0];
						bless ($subnode, 'XML::LibXML::Text');
						$subnode->setData ('HTTP/1.1 500 INTERNAL SERVER ERROR');
					}
				}
				catch Mioga2::Exception::User with {
					my $err = shift;
					print STDERR "[Mioga2::DAVProxy::RetrieveDAVAuthFiles] Owner not found for URI $link\n";
					print STDERR $err->as_string ();
				};
			}

			# Remove node if needed
			if ($remove_node) {
				$root->removeChild($node);
			}
		}
	}
	my $result = $doc->serialize(1);
	return \$result;
}

# ============================================================================

=head2 retrieveGETAuthFiles ($config, $request, $orig_uri, $body)

    Cleans the body of a GET (from mod_index) to delete entries not available to current user.

    return the new body.

=cut

# ============================================================================

sub retrieveGETAuthFiles {
	my ( $self, $config, $orig_uri, $body_ref, $user ) = @_;
	print STDERR "Mioga2::DAVFS::retrieveGETAuthFiles  body_ref = $body_ref\n" if ($debug);

	my $parser = XML::LibXML->new();
	$parser->keep_blanks(0);
	$$body_ref =~ s/&/&amp;/g;

	my $detailed_list = getURIList( $config, $user->GetRowid, $orig_uri->as_trimed_string );

	my $uri_list;
	@$uri_list = map { $_->{uri} } @$detailed_list;
	push @$uri_list, $orig_uri->as_trimed_string;

	my $doc   = $parser->parse_html_string($$body_ref);
	my $root  = $doc->documentElement;
	my $table = ${ $root->findnodes('body/table') }[0];
	my @nodes = $root->getElementsByTagName('tr');
	foreach my $node (@nodes) {
		my @href = $node->findnodes('td/a');
		next if !@href;

		my $link = $href[0]->getAttribute('href');
		utf8::decode($link);
		next if ( $orig_uri->as_trimed_string =~ "\Q$link\E" );
		$link =~ s/^\.\///;

		my $node_uri =
		  Mioga2::Classes::URI->new(
			uri => $orig_uri->as_trimed_string . '/' . $link )
		  ->as_trimed_string;
		unless ( grep( $_ eq $node_uri, @$uri_list ) ) {
			$table->removeChild($node);
		}
	}
	local $XML::LibXML::skipXMLDeclaration = 1;
	my $body = $doc->serialize(1);
	$body =~ s/&amp;/&/g;
	utf8::decode($body);
	return \$body;
}

# ============================================================================

=head2 getMimeType ($path)

    Search Mime type with various methods

    return a string for Mime

=cut

# ============================================================================

sub getMimeType {
	my ($path) = @_;
	my $mime;
	eval { $mime = mimetype($path) };
	if ($@) {
		print STDERR "DAVProxy::getMimeType error : $@\n";
		$mime = "application/octet-stream" if ( !defined($mime) );
	}
	elsif ($mime) {
		utf8::encode($mime);
		$mime = 'directory' if $mime =~ /directory/;
		$mime = '' if $mime =~ /x-system\/x-error/;
	}
	return $mime;
}

# ============================================================================

=head2 getSize ($path)

    Get size for $path

    return size as an integer

=cut

# ============================================================================

sub getSize {
	my ($path) = @_;
	my $size = ( -s $path ) || 0;
	return $size;
}

# ============================================================================

=head2 updateMiogaDB ($preserve_file, $response, $uri_obj, $user)

    Performs the necessary steps to update Mioga database accordingly to DAV response.
    In an error case, exceptions are thrown.
    The call to updateMiogaDB can be wrapped into a transaction.

=cut

# ============================================================================

sub updateMiogaDB {
	my ( $self, $res, $uri_obj, $user ) = @_;
	my $config    = $self->{config};
	my $method    = $res->request->method;
	my $dav_uri   = $config->GetDAVBasePath;
	my $mioga_uri = $config->GetBasePath;
	my $user_id   = (defined($user) ? $user->GetRowid : undef);
	my $dbh       = $config->GetDBH;
	my $uri_str   = $uri_obj->GetURI; # this can be a string or an URI::_generic

	print STDERR "[Mioga2::DAVFS::updateMiogaDB] code = " . $res->code . ", method = " . $method . "\n"
	  if ($debug);
	my $group_ident = $uri_obj->GetGroupIdent;
	my $history = (defined $group_ident) 
			? $self->uri_has_history( $config, $uri_obj->GetURI ())
			: undef;

	if ( $method =~ /MKCOL/i ) {
		createURI( $config, $uri_str, $user_id, 'directory',
			getSize( $uri_obj->GetRealPath ) );
		$self->notifyUpdate(
			$config, $uri_obj,
			user_id => $user_id,
			type    => [ 'RSS', 'SEARCH', 'filesystem' ]
		) if $self->{notify};
	}
	elsif ( $method =~ /MOVE/i ) {
		my $srcdir = Mioga2::Classes::URI->new( uri => $uri_str );
		my $desturi =
		  Mioga2::Classes::URI->new(
			uri => $res->request->headers->{destination} )->as_string;
		$desturi =~ s/$dav_uri/$mioga_uri/;
		$desturi =~ s/http(?:s?):\/\/[^\/]+//;

		if ($history) {
			$self->historyRename( $config, $srcdir, $desturi, $user_id );
		}
		my $srcdir_str = ''.$srcdir->as_string;
		renameURI( $config, $srcdir_str, $desturi );
		$self->notifyUpdate(
			$config, $uri_obj,
			user_id => $user_id,
			type    => [ 'RSS', 'Louvre', 'SEARCH', 'filesystem' ],
			uris    => [$srcdir_str, $desturi]
		) if $self->{notify};
	}
	elsif ( $method =~ /COPY/i ) {
		my $desturi =
		  Mioga2::Classes::URI->new(
			uri => $res->request->headers->{destination} )->as_string;
		$desturi =~ s/$dav_uri/$mioga_uri/;
		$desturi =~ s/http(?:s?):\/\/[^\/]+//;

		copyURI( $config, $uri_obj, $desturi, $user_id );
		$self->notifyUpdate(
			$config, $uri_obj,
			user_id => $user_id,
			type    => [ 'RSS', 'Louvre', 'SEARCH', 'filesystem' ],
			uris    => [$desturi]
		) if $self->{notify};
	}
	elsif ( $method =~ /PUT/i ) {
		if ( $history && -f $uri_obj->GetRealPath ) {
			my $uri = Mioga2::Classes::URI->new( uri => $uri_str );
			$self->historyReplace( $config, $uri,
				$self->{preserved_file_for_PUT}, $user_id );
		}
		my $mime = getMimeType( $uri_obj->GetRealPath );
		createURI( $config, $uri_obj->GetURI, $user_id, $mime,
			getSize( $uri_obj->GetRealPath ) );
		$self->notifyUpdate(
			$config, $uri_obj,
			user_id => $user_id,
			type    => [ 'RSS', 'Louvre', 'SEARCH', 'filesystem' ],
			uris    => [(ref($uri_str) eq '' ? $uri_str : ''.$uri_str->as_string)]
		) if $self->{notify};
	}
	elsif ( $method =~ /DELETE/i ) {
		if ($history) {
			$self->historyDelete( $config, $uri_str );
		}
		deleteURI( $config, $uri_obj->GetURI );
		$self->notifyUpdate($config, $uri_obj, user_id => $user_id, type => ['Louvre', 'SEARCH', 'filesystem'],
            	uris => [(ref($uri_str) eq '' ? $uri_str : ''.$uri_str->as_string)])
            	if $self->{notify};	
	}
	elsif ($method =~ /PROPPATCH/i) {
		my $parser = XML::LibXML->new;
		$parser->keep_blanks(0);

		my $doc   = $parser->parse_string($res->request ()->content);
		my $root  = $doc->documentElement;
		my @davns = grep( $_->value eq 'DAV:', $root->getNamespaces );
		my $ns    = $davns[0]->getLocalName;
		my $node  = $root->getElementsByTagName ($ns . ':set')->[0]->getElementsByTagName ($ns . ':prop')->[0]->firstChild ();
		my $propname = $node->nodeName ();
		my $propvalue = int ($node->textContent ());

		print STDERR "[Mioga2::DAVFS::updateMiogaDB] Got PROPPATCH request for " . $uri_obj->GetURI () . " $propname => $propvalue\n" if ($debug);

		if ($propname eq 'hidden') {
			ExecSQL ($dbh, 'UPDATE m_uri SET hidden = ? WHERE uri = ?', [$propvalue ? 'true' : 'false', $uri_obj->GetURI ()]);
		}
	}
}

# ============================================================================

=head2 make_request ($method, $callbacks, $content_ref)

    Make request to local DAV location.

    return the request from DAV.

=cut

# ============================================================================

sub make_request {
	my ( $self, $method, $callbacks, $content_ref ) = @_;
	my $config = $self->{config};
	my $protocol = $self->{protocol};
	my $host = $self->{host};
	my $port = $self->{port};

	my $dav_uri   = $config->GetDAVBasePath;
	my $mioga_uri = $config->GetBasePath;

	my $orig_uri = Mioga2::Classes::URI->new( uri => $callbacks->{uri}->() );
	my $uri      = $orig_uri->as_string;
	$uri =~ s/^$mioga_uri/$dav_uri/;
	$uri = Mioga2::Classes::URI->new( uri => "$protocol://$host:$port$uri" )
	  ->as_string;
	print STDERR "[Mioga2::DAVFS::make_request] uri = $uri\n" if $debug;

	$uri = $self->escapeSpecialChars($uri);
	my $request = HTTP::Request->new( $method, $uri );
	my $headers = $callbacks->{headers_in}->();

	while ( my ( $header, $value ) = each %$headers ) {

		# process headers
		if ( $header =~ /^destination/i && $value !~ $dav_uri ) {
			$value =~ s!^http[s]?://[^/]+$mioga_uri!$protocol://$host$dav_uri!;
			$value = Mioga2::Classes::URI->new( uri => $value )->as_string;
			$value = $self->escapeSpecialChars($value);
		}
		elsif ( $header =~ /^accept-encoding/i ) {
			$value = '';
		}
		elsif (($header =~ /^Transfer-Encoding/i) && ($value eq 'Chunked')) {
			# Bug #1394: Ignore Transfer-Encoding Chunked headers
			next;
		}
		$request->header( $header, $value );
	}

	if (( $headers->{'Content-Length'} ) || ($headers->{'Transfer-Encoding'} && ($headers->{'Transfer-Encoding'} eq 'Chunked'))) {
		if ((($method =~ /PROPPATCH/i) && (ref ($callbacks->{content_read}) ne ''))) {
			# Copy contents when method is PROPPATCH because request contents will be sent to mod_dav, then processed by $self->updateMiogaDB
			my $content = '';
			while ((my $buffer = $callbacks->{content_read}->()) ne '' )  {
				$content .= $buffer;
			}
			$request->content( $content );
		}
		else {
			$request->content( $callbacks->{content_read} );
		}
	}

	warn "Request to send to server:\n" . Dumper($request) if $debug;

	my $ua = LWP::UserAgent->new (parse_head => 0);
	$ua->max_size(undef);

	if ( $method =~ /GET/i ) {
		my $set_headers = 0;
		my $index_mode  = 0;
		return $ua->simple_request(
			$request,
			sub {
				my ( $chunk, $response, $protocol ) = @_;
				if ( !$set_headers ) {
					my $ct = $response->header('Content-type');
					$ct = $ct->[0] if ( ref($ct) eq 'ARRAY' );
					if (defined ($ct)) {
						($ct) = split( ',', $ct );
						$callbacks->{content_type}->($ct);
					}

					my @headers = $response->header_field_names;
					my $table   = $callbacks->{headers_out}->();
					if ($table) {	
						$table->clear;
						foreach my $header (@headers) {
							my $value = $response->header($header);
							$value = join( ', ', @$value )
						    		if ( ref($value) eq 'ARRAY' );
							$table->add( $header, $value );
						}
					}
					$callbacks->{status}->( $response->code );
					$callbacks->{status_line}->( $response->status_line );

					$set_headers = 1;
				}
				my $response_title = $response->header('title');
				if (( defined($response_title) and $response_title =~ /index of/i ) || ($chunk =~ /index of/i) || $index_mode)
				{
					$index_mode = 1;
					$$content_ref .= $chunk;
				}
				else {
					$callbacks->{content_write}->($chunk);
				}
			}
		);
	}
	else {
		my $response = $ua->simple_request($request);

		# Bug #1344 -- Process 'DAV' header being an array and not correctly translated to string
		my @dav_header = $response->header ('DAV');
		$response->header ('DAV' => join (',', @dav_header));

		return ($response);
	}
}

# ============================================================================

=head2 escapeSpecialChars ($uri)

  Escape extra special characters such as '#'. This is very specific for
  file transfer and allows certains

=cut

# ============================================================================

sub escapeSpecialChars {
	my ( $self, $uri ) = @_;

    $uri =~ s/%/%25/g;
    $uri =~ s/\?/%3F/g;
    $uri =~ s/&/%26/g;
	$uri =~ s/#/%23/g;
	return $uri;
}

# ============================================================================

=head2 preserveFile($path)

    Copy the given file to a backup location.

=cut

# ============================================================================
sub preserveFile {
	my ( $self, $uri ) = @_;

	print STDERR ("Mioga2::DAVFS::preserveFile\n") if ( $debug > 0 );

	my $real_path = $uri->GetRealPath();
	my $copy_path = $self->getBackupName($real_path);
	print STDERR ("copy_path = $copy_path\n") if ( $debug > 0 );

	#
	# copy file
	#
	my $buffer;
	my $res;
	open( INPUT, "<$real_path" )
	  or die "Mioga2::DAVFS::preserveFile cannot open source $real_path\n";
	open( OUTPUT, ">$copy_path" )
	  or die "Mioga2::DAVFS::preserveFile cannot open copy $copy_path\n";
	binmode(INPUT);
	binmode(OUTPUT);
	do {
		$res = read( INPUT, $buffer, 262144 );
		if ( !defined($res) ) {
			die
"Mioga2::DAVFS::preserveFile cannot read source file $real_path\n";
		}
		print OUTPUT $buffer
		  or die
		  "Mioga2::DAVFS::preserveFile cannot write copy file $copy_path\n";
	} while ($res);
	close(INPUT);
	close(OUTPUT);

	return $copy_path;
}

# ============================================================================

=head2 getBackupName($real_path)

    Returns a not-existing name for $real_path, suited for copying or linking
    a backup there. $real_path must exist.

=cut

# ============================================================================
sub getBackupName {
	my ( $self, $full_path ) = @_;
	my $num = 1;
	while ( -e "$full_path~$num" ) {
		$num++;
	}
	return "$full_path~$num";
}

# ============================================================================

=head2 DavDBOutFilter::copyURI ($config, $dest, $uri, $user)

    Execute a recursive copy in the database.

=cut

# ============================================================================
sub copyURI {
	my ( $config, $src, $dest, $user_id ) = @_;
	print STDERR ( "Mioga2::DavProxy::copyURI(" . $src->GetURI . ", $dest)\n" )
	  if ( $debug > 0 );

	my $dbh  = $config->GetDBH;
	my $mime = getMimeType( $src->GetRealPath );
	my $size = getSize( $src->GetRealPath );
	return AuthzCopyURI( $dbh, $src->GetURI, $dest, $user_id, $mime, $size );
}

# ============================================================================

=head2 DavDBOutFilter::createURI ($config, $uri, $user_id, $mime, $size)

    Add the URI in the database.

=cut

# ============================================================================
sub createURI {
	my ( $config, $uri, $user_id, $mime, $size ) = @_;
	print STDERR ("Mioga2::DavProxy::createURI uri = $uri\n") if ( $debug > 0 );

	my $dbh = $config->GetDBH;
	return AuthzDeclareURI( $dbh, $uri, $user_id, $mime, $size );
}

# ============================================================================

=head2 DavDBOutFilter::deleteURI ($config, $uri)

    Add the URI in the database.

=cut

# ============================================================================
sub deleteURI {
	my ( $config, $uri ) = @_;
	print STDERR ("Mioga2::DavProxy::deleteURI uri = $uri\n") if ( $debug > 0 );

	my $dbh = $config->GetDBH();

	AuthzDeleteURI( $dbh, $uri );
}

# ============================================================================

=head2 DavDBOutFilter::renameURI ($config, $dest, $uri)

    Execute a recursive rename in the database.

=cut

# ============================================================================
sub renameURI {
	my ( $config, $src, $dest ) = @_;

	my $dbh = $config->GetDBH();

	print STDERR "Mioga2::DavProxy::renameURI($src, $dest)\n" if ( $debug > 0 );
	return AuthzRenameURI( $dbh, $src, $dest );
}

# ============================================================================

=head2 historyRename ($dbh, $uri, $user_id)

  Add renamed file in history.

=cut

# ============================================================================

sub historyRename {
	my ( $self, $config, $uri, $desturi, $user_id ) = @_;

	my $dbh = $config->GetDBH;
	my $mioga_dest = Mioga2::URI->new( $config, uri => $desturi );

	# try to create destination directory
	my ( $destname, @destsegs ) = Mioga2::Classes::History::GetHistoryPath(
		Mioga2::Classes::URI->new( uri => $desturi ),
		$mioga_dest->GetGroupIdent );
	my $base_dir = $config->GetMiogaFilesDir . "/history";
	my $history_dir = "$base_dir/" . join( "/", @destsegs );

	while ( !-d $history_dir ) {
		mkdir($base_dir) unless ( -d $base_dir );
		$base_dir .= "/" . shift(@destsegs);
	}
	$base_dir = $config->GetMiogaFilesDir . "/history";

	my $uri_str = (ref($uri) eq '') ? $uri : ''.$uri->as_string;
	my $mioga_src = Mioga2::URI->new( $config, uri => $uri_str );
	my ( $name, @segments ) = Mioga2::Classes::History::GetHistoryPath( $uri,
		$mioga_src->GetGroupIdent );

	if ( -d $mioga_dest->GetRealPath ) {

		# move on FS only, no need to update DB
		move( "$base_dir/" . join( '/', @segments ) . "/$name",
			"$history_dir/$destname" );
	}
	elsif ( -f $mioga_dest->GetRealPath ) {
		ExecSQL(
			$dbh, "INSERT INTO uri_history(uri_id, modified, user_id, old_name)
      VALUES ((SELECT rowid FROM m_uri WHERE uri = '"
			  . st_FormatPostgreSQLString( $uri_str )
			  . "'),now(),$user_id,'"
			  . st_FormatPostgreSQLString($name) . "')"
		);
		my $files = SelectMultiple( $dbh,
			"SELECT uri_history.* from uri_history, m_uri where m_uri.uri = '"
			  . st_FormatPostgreSQLString( $uri_str )
			  . "' and uri_history.uri_id = m_uri.rowid" );
		return 0 unless defined $files;

		foreach my $file (@$files) {
			my $old_file = "$name-" . $file->{rowid};
			move(
				"$base_dir/" . join( '/', @segments ) . "/$old_file",
				"$history_dir/$destname-" . $file->{rowid}
			);
		}
	}

	return 1;
}

# ============================================================================

=head2 uri_has_history ($config, $uri)

  Returns 1 if $uri (string) has history enabled, 0 otherwise

=cut

# ============================================================================

sub uri_has_history {
	my ( $self, $config, $uri ) = @_;

	my $db      = $config->GetDBObject ();
	my $res = $db->SelectSingle ('SELECT history FROM m_uri WHERE uri = ?;', [$uri]);
	my $ret = ($res->{history}) ? 1 : 0;
	return ($ret)
}

# ============================================================================

=head2 historyReplace ($config, $uri, $preserve_file, $user_id)

  Add replaced file in history.

=cut

# ============================================================================

sub historyReplace {
	my ( $self, $config, $uri, $pfile, $user_id ) = @_;

	my $dbh = $config->GetDBH;

	my $uri_str = (ref($uri) eq '') ? $uri : ''.$uri->as_string;
	my $me = SelectSingle( $dbh,
		    "SELECT * FROM m_uri WHERE uri = '"
		  . st_FormatPostgreSQLString( $uri_str )
		  . "'" );
	return 0 unless defined $me;

	my $mioga_uri = Mioga2::URI->new( $config, uri => $uri_str );
	my $group_ident = $mioga_uri->GetGroupIdent;

	my ( $name, @segments ) =
	  Mioga2::Classes::History::GetHistoryPath( $uri, $group_ident );

	ExecSQL( $dbh,
		    "INSERT INTO uri_history(uri_id, modified, user_id) VALUES ("
		  . $me->{rowid}
		  . ",now(),$user_id)" );
	my $rowid = GetLastInsertId( $dbh, "uri_history" );

	my $base_dir = $config->GetMiogaFilesDir . "/history";
	my $history_dir = "$base_dir/" . join( "/", @segments );
	while ( !-d $history_dir ) {
        mkdir($base_dir) or throw Mioga2::Exception::Simple ('Mioga2::DAVProxy::HistoryReplace', "Directory $base_dir creation failed: $!") unless ( -d $base_dir );
		$base_dir .= "/" . shift(@segments);
	}

    move( $pfile, "$history_dir/" . st_PathEscape ($name) . "-$rowid" ) or throw Mioga2::Exception::Simple ('Mioga2::DAVProxy::HistoryReplace', "History file $history_dir/$name-$rowid creation failed: $!");
}

# ============================================================================

=head2 historyDelete ($config, $uri)

  Delete files in history.

=cut

# ============================================================================

sub historyDelete {
	my ( $self, $config, $uri ) = @_;

	my $dbh   = $config->GetDBH;
	my $files = SelectMultiple( $dbh,
"SELECT m_uri.uri, uri_history.rowid FROM m_uri, uri_history WHERE uri_history.uri_id = m_uri.rowid AND m_uri.uri ~ '"
		  . st_FormatPostgreSQLRegExpString($uri)
		  . "(/.*)?\$'" );
	return 0 unless scalar @$files;

	my $base_dir = $config->GetMiogaFilesDir . "/history";

	my @history_files;
	foreach my $file (@$files) {
		my $mioga_uri = Mioga2::URI->new( $config, uri => $file->{uri} );
		my $group_ident = $mioga_uri->GetGroupIdent;
		my ( $name, @segments ) = Mioga2::Classes::History::GetHistoryPath(
			Mioga2::Classes::URI->new( uri => $file->{uri} ), $group_ident );

        push @history_files, "$base_dir/" . join( "/", @segments ) . "/" . st_PathEscape ($name) . "-" . $file->{rowid};
	}

	unlink(@history_files) if (@history_files);

	my $rc = ExecSQL( $dbh,
"DELETE FROM uri_history USING m_uri WHERE uri_history.uri_id = m_uri.rowid AND m_uri.uri ~ '"
		  . st_FormatPostgreSQLRegExpString($uri)
		  . "(/.*)?\$'" );
	if ($rc eq '0E0') {
		throw Mioga2::Exception::DB ('Mioga2::DAVFS::AuthzDeleteURI', "Deleting URI $uri from history failed, no records matched by SQL DELETE.");
	}
}

# ============================================================================

=head2 getURIList ($config)

    Return uri_ids of root uris.

=cut

# ============================================================================

sub getURIList {
	my ( $config, $user_id, $uri, $no_property ) = @_;

	my $dbh = $config->GetDBH;
	my $res;
	try {
		my $uri_obj = Mioga2::URI->new ($config, uri => $uri);
		my $listid = '(SELECT rowid FROM m_uri WHERE directory_id = (SELECT rowid FROM m_uri WHERE uri = ?))';
		my $all_with_no_righ = '';

		# On WebDAV root, elements have no directory_id
		if ($uri_obj->IsRootWebDav ()) {
			$listid = '(SELECT rowid FROM m_uri WHERE directory_id IS NULL)';
		}

		# In public WebDAV, include all items with accesskey set to 0 (no right) so they appear when user is authenticated
		if ($uri_obj->IsPublic ()) {
			$all_with_no_righ = 'UNION SELECT m_uri.rowid AS uri_id, 0 AS accesskey FROM m_uri WHERE m_uri.rowid IN ' . $listid;
		}

		# Get as many information as possible and send it to client. Custom properties (size, mimetype, elements, etc.) will only be supported by Magellan.
		# To add a custom property, the request below should be modified and the list in method retrieveDAVAuthFiles around line 482 (at the time this comment is written, SVN r5494)
		my $request = "SELECT uri_access.uri_id, m_uri.uri, uri_access.accesskey, m_uri.elements, m_uri.size, m_uri.mimetype, m_uri.hidden, m_uri.history AS history_enabled, (m_user_base.firstname || ' ' || m_user_base.lastname) AS author, m_group_base.ident AS group_ident, m_group_base.history AS group_has_history ";
		
		if (!$no_property) {
			$request .= ", count(can_set_history.profile_id) AS can_set_history, count(file_comment.rowid) AS comments, count(uri_history.rowid) AS history, count(email.profile_id) AS email, count(diderot.profile_id) AS diderot, count(louvre.profile_id) AS louvre, count(manage.profile_id) AS manage, count(setprops.profile_id) AS setprops, count(view_hidden.profile_id) AS view_hidden";
		}
		
		$request .= " FROM m_user_base, m_group_base,
		( SELECT tmp_req.uri_id, (max(tmp_req.accesskey)%10) AS accesskey FROM
        ( SELECT m_uri.rowid AS uri_id, (50 + m_authorize.access) AS accesskey
          FROM m_authorize, m_uri, m_authorize_user
          WHERE  m_uri.rowid IN $listid AND
                 m_authorize.uri_id = m_uri.parent_uri_id AND
                 m_authorize_user.authz_id = m_authorize.rowid AND m_authorize_user.user_id = ?

          UNION

          SELECT m_uri.rowid AS uri_id, (40 + m_authorize.access) AS accesskey
          FROM m_authorize, m_uri, m_authorize_team, m_group_group
          WHERE m_uri.rowid IN $listid AND
                m_authorize.uri_id = m_uri.parent_uri_id AND
                m_authorize_team.authz_id = m_authorize.rowid AND
                m_group_group.group_id = m_authorize_team.team_id AND
                m_group_group.invited_group_id=?

          UNION

          SELECT m_uri.rowid AS uri_id, (30 + m_authorize.access) AS accesskey
          FROM m_authorize, m_uri, m_authorize_team, m_group_expanded_user
          WHERE m_uri.rowid IN $listid AND
                m_authorize.uri_id = m_uri.parent_uri_id AND
                m_authorize_team.authz_id = m_authorize.rowid AND
                m_group_expanded_user.group_id = m_authorize_team.team_id AND
                m_group_expanded_user.invited_group_id=?

          UNION

          SELECT m_uri.rowid AS uri_id, (20 + m_authorize.access) AS accesskey
          FROM m_authorize, m_uri, m_authorize_profile, m_profile_group
          WHERE m_uri.rowid IN $listid AND
                m_authorize.uri_id = m_uri.parent_uri_id AND
                m_authorize_profile.authz_id = m_authorize.rowid AND
                m_profile_group.profile_id = m_authorize_profile.profile_id AND
                m_profile_group.group_id=?

          UNION

          SELECT m_uri.rowid AS uri_id, (10 + m_authorize.access) AS accesskey
          FROM m_authorize, m_uri, m_authorize_profile, m_profile_expanded_user
          WHERE m_uri.rowid IN $listid AND
                m_authorize.uri_id = m_uri.parent_uri_id AND
                m_authorize_profile.authz_id = m_authorize.rowid AND
                m_profile_expanded_user.profile_id = m_authorize_profile.profile_id AND
                m_profile_expanded_user.user_id=?

          $all_with_no_righ

     ) AS tmp_req GROUP BY tmp_req.uri_id
	 ) AS uri_access

	 LEFT JOIN m_uri ON m_uri.rowid = uri_access.uri_id ";

	if (!$no_property) {
		 $request .= "LEFT JOIN file_comment ON file_comment.uri_id = m_uri.rowid
		 LEFT JOIN uri_history ON uri_history.uri_id = m_uri.rowid

		 LEFT JOIN (
			SELECT user_id, group_id, profile_id FROM m_profile_user_function WHERE app_ident = 'Louvre' AND function_ident IN ('Anim') AND user_id = ?
			UNION
			SELECT user_id, group_id, profile_id FROM m_profile_expanded_user_functio WHERE app_ident = 'Louvre' AND function_ident IN ('Anim') AND user_id = ?
			LIMIT 1
		 ) AS louvre
		 ON louvre.group_id = m_uri.group_id

		 LEFT JOIN (
			SELECT user_id, group_id, profile_id FROM m_profile_user_function WHERE app_ident = 'Mermoz' AND function_ident IN ('Base','Standard','Anim') AND user_id = ?
			UNION
			SELECT user_id, group_id, profile_id FROM m_profile_expanded_user_functio WHERE app_ident = 'Mermoz' AND function_ident IN ('Base','Standard','Anim') AND user_id = ?
			LIMIT 1
		 ) AS email
		 ON email.group_id = m_uri.group_id

		 LEFT JOIN (
			SELECT user_id, group_id, profile_id FROM m_profile_user_function WHERE app_ident = 'Diderot' AND function_ident IN ('Use', 'Animation') AND user_id = ?
			UNION
			SELECT user_id, group_id, profile_id FROM m_profile_expanded_user_functio WHERE app_ident = 'Diderot' AND function_ident IN ('Use', 'Animation') AND user_id = ?
			LIMIT 1
		 ) AS diderot
		 ON diderot.group_id = m_uri.group_id

		 LEFT JOIN (
			SELECT user_id, group_id, profile_id FROM m_profile_user_function WHERE app_ident = 'Magellan' AND function_ident IN ('OtherACL') AND user_id = ?
			UNION
			SELECT user_id, group_id, profile_id FROM m_profile_expanded_user_functio WHERE app_ident = 'Magellan' AND function_ident IN ('OtherACL') AND user_id = ?
			LIMIT 1
		 ) AS manage
		 ON manage.group_id = m_uri.group_id

		 LEFT JOIN (
			SELECT user_id, group_id, profile_id FROM m_profile_user_function WHERE app_ident = 'Magellan' AND function_ident IN ('Properties') AND user_id = ?
			UNION
			SELECT user_id, group_id, profile_id FROM m_profile_expanded_user_functio WHERE app_ident = 'Magellan' AND function_ident IN ('Properties') AND user_id = ?
			LIMIT 1
		 ) AS setprops
		 ON setprops.group_id = m_uri.group_id

		 LEFT JOIN (
			SELECT user_id, group_id, profile_id FROM m_profile_user_function WHERE app_ident = 'Magellan' AND function_ident IN ('Hidden') AND user_id = ?
			UNION
			SELECT user_id, group_id, profile_id FROM m_profile_expanded_user_functio WHERE app_ident = 'Magellan' AND function_ident IN ('Hidden') AND user_id = ?
			LIMIT 1
		 ) AS view_hidden
		 ON view_hidden.group_id = m_uri.group_id

		 LEFT JOIN (
			SELECT user_id, group_id, profile_id FROM m_profile_user_function WHERE app_ident = 'Magellan' AND function_ident IN ('History') AND user_id = ?
			UNION
			SELECT user_id, group_id, profile_id FROM m_profile_expanded_user_functio WHERE app_ident = 'Magellan' AND function_ident IN ('History') AND user_id = ?
			LIMIT 1
		 ) AS can_set_history
		 ON can_set_history.group_id = m_uri.group_id ";
	 }

	 $request .= "WHERE m_uri.user_id = m_user_base.rowid AND m_uri.group_id = m_group_base.rowid

     GROUP BY uri_access.uri_id, uri_access.accesskey, m_uri.uri, m_uri.elements, m_uri.size, m_uri.mimetype, m_uri.hidden, m_uri.history, author, group_ident, m_group_base.history ORDER BY m_uri.uri";

		# Initialize args array
		my $args;
		if ($uri_obj->IsRootWebDav ()) {
			push (@$args, ($user_id) x 5);
		}
		else {
			push (@$args, ($uri, $user_id) x 5);
		}

		# Append URI for 'all_with_no_righ' part of the request
		if ($uri_obj->IsPublic () && !$uri_obj->IsRootWebDav ()) {
			push (@$args, $uri);
		}

		# Append user_id for enhanced properties (view_hidden, setprops, etc.)
		if (!$no_property) {
			push (@$args, ($user_id) x 14);
		}

		$res = SelectMultiple ($dbh, $request, $args);

		if ( !@$res ) {
			return;
		}

		# Filter forbidden URIs
		if ($uri_obj->IsPrivate ()) {
			@$res = grep { $_->{accesskey} != 0 } @$res;
		}
	}
	otherwise {
		my $err = shift;
		warn Dumper($err);
		$err->throw;
	};

	if ( !@$res ) {
		return [];
	}

	return $res;
}

# ============================================================================

=head2 notifyUpdate ()

  Send a message to the notify daemon to update resources (RSS, search engine cache, etc.)

=cut

# ============================================================================

# TODO: merge this duplicated code with the method from Mioga2::Application

sub notifyUpdate {
	my ( $self, $config, $uri, %options ) = @_;

	my $group_id        = $uri->GetGroup->GetRowid;
	my $mioga_id        = $config->GetMiogaId;
	my $default_options = {
		mioga_id    => $mioga_id,
		group_id    => $group_id,
		application => __PACKAGE__
	};
	%options = ( %$default_options, %options );

	$config->WriteNotifierFIFO( Mioga2::tools::Convert::PerlToJSON ( \%options ) . "\n" );
}

#===============================================================================

=head2 ResponseIsSuccess

Determines if a response is success. is_success ($response) is not sufficient
because it doesn't handle multi-status responses.

This is a class method that can be used directly without instanciating a
Mioga2::DAVFS object.

=cut

#===============================================================================
sub ResponseIsSuccess {
	my ($response) = @_;

	my $is_success = 1;

	if ((ref ($response) eq '') && ($response =~ /^[0-9]+$/)) {
		# $response may be a HTTP status code
		if ($response >= 400) {
			$is_success = 0;
		}
	}
	else {
		# or $response may be a HTTP::Response object
		if ($response->code == $MULTI_STATUS) {
			my $msresponse = ParseMultiStatus ($response);
			$is_success = $msresponse->{is_success};
		}
		elsif (!$response->is_success ()) {
			$is_success = 0;
		}
	}

	return ($is_success);
}	# ----------  end of subroutine ResponseIsSuccess  ----------


#===============================================================================

=head2 ParseMultiStatus

Parse a multi-status response and returns it as a hash.

=over

=item B<Arguments:>

=over

=item I<$response:> a HTML::Response object.

=back

=item B<Return:> a hash containing the following keys.

=over

=item I<is_success:> a flag indicating if request succeeded or not.

=item I<status:> an array of hashes containing the different statuses:

=over

=item I<code:> the HTTP code.

=item I<message:> the HTTP message.

=back

=back

=back

This is a class method that can be used directly without instanciating a
Mioga2::DAVFS object.

=cut

#===============================================================================
sub ParseMultiStatus {
	my ($response) = @_;

	my $msresponse = {is_success => 1};

	my $content = $response->content;
	# $content =~ s/<!DOCTYPE.*>//gsm;

	my $xml = new Mioga2::XML::Simple (forcearray => 1);
	my $xmltree = $xml->XMLin ($content);
	for (@{$xmltree->{'D:response'}}) {
		# Parse status line
		my $status = $_->{'D:status'}->[0];
		next if (!$status); # Just ignore D:propstat :: D:status
		my ($code, $msg) = ($status =~ m/^[^ ]* ([0-9]+) (.*)$/);
		if ($code >= 400) {
			$msresponse->{is_success} = 0;
		}
		push (@{$msresponse->{status}}, {entry => $_->{'D:href'}->[0], code => $code, message => $msg});
	}

	return ($msresponse);
}	# ----------  end of subroutine ParseMultiStatus  ----------


#===============================================================================

=head2 RefreshDirectoryInfo

Refresh directory information such as size and number of elements according to
modification time of URI sub-elements.

=head3 Incoming Arguments

=over

=item B$config: a Mioga2::Config

=item B$uri: a string containing the directory URI

=back

=head3 Return value

None

=back

=cut

#===============================================================================
sub RefreshDirectoryInfo {
	my ($config, $uri) = @_;
	print STDERR "[Mioga2::DAVFS::RefreshDirectoryInfo] Entering, URI: $uri\n" if ($debug);

	my $db = $config->GetDBObject ();
	$db->Execute ("UPDATE m_uri SET elements = m_uri.elements + changes.elements, size = m_uri.size + changes.size, modified = now() FROM (SELECT m_uri.rowid, count(modified.elements) AS elements, sum(modified.size) AS size FROM m_uri LEFT JOIN (SELECT rowid, uri, elements, size, directory_id FROM m_uri WHERE uri LIKE ? AND modified > (SELECT modified FROM m_uri WHERE uri = ?)) AS modified ON modified.uri LIKE m_uri.uri || '/%' WHERE modified.uri IS NOT NULL GROUP BY m_uri.rowid) AS changes WHERE m_uri.rowid = changes.rowid;", ["$uri/%", $uri]);

	print STDERR "[Mioga2::DAVFS::RefreshDirectoryInfo] Leaving\n" if ($debug);
}	# ----------  end of subroutine RefreshDirectoryInfo  ----------

=head1 NAME

DAVFS::Callbacks: Callback object necessary to call DAVRequest methods of DAVFS.

=head1 DESCRIPTION

This class encapsulates a bunch of callbacks necessary for calls to DAVRequest* methods
of DAVFS. An object has to provide the following keys:

=over 2

=item *

B<uri()> returns the originally requested URI, as a string.

=item *

B<headers_in()> returns the incoming headers, as a APR::Table.

=item *

B<headers_out()> returns the outgoing headers, as a APR::Table.

=item *

B<content_type($type)> is a function setting the content type.

=item *

B<status($status)> is a function setting the status.

=item *

B<status_line($status_line)> is a function setting the status line.

=item *

B<content_read()> is a function returning a number of bytes read from the
incoming content body or file.

=item *
B<content_write($x)> is a function taking a number of bytes to write into
the outgoing content body or file.

=back

=cut

package Mioga2::DAVFS::Callbacks;
use strict;
use warnings;
use open ':encoding(utf8)';

use Error qw(:try);
use Mioga2::Exception::Application;

=head2 CreateCallbacksFromApacheRequest($r)

Set up a DAVFS::Callbacks object using the values and methods provided
by this Apache2::RequestRec object.

=cut

sub CreateCallbacksFromApacheRequest {
	my ( $class, $r ) = @_;
	my $cb = {
		uri             => sub { return $r->uri(); },
		headers_in      => sub { return $r->headers_in(); },
		headers_out     => sub { return $r->headers_out(); },
		content_type    => sub { return $r->content_type(@_); },
		status          => sub { return $r->status(@_); },
		status_line     => sub { return $r->status_line(@_); },
		remote_host     => sub { return $r->hostname (); },
		register_pnotes => sub { $r->pnotes('preserved_file_for_PUT', @_); },
		content_read    => $class->_createSubForReadingFH($r),
		content_write   => $class->_createSubForWritingFH($r)
	};
	bless $cb, $class;
	return $cb;
}

=head2 new($uri, %opts) 

Set up a DAVFS::Callbacks object. The $uri as a scalar is mandatory.
Following options can be given. If an option is not specified,
input will be empty and output will be thrown away.

* headers_in => $ref
If specified, $ref must point to an APR::Table or an hashref.

* body_in => $ref
If specified, $ref must be a filehandle or a SCALAR ref.

* headers_out => $ref
If specified, $ref must be an APR::Table (no hashref).

* body_out => $ref
If specified, $ref must be a filehandle or a scalarref
(that will be extended with new content).

* set_content_type => $ref, set_status => $ref, set_status_line => ref
If any of these are specified, they must be scalarrefs
(that will be overwritten with the new content, which is typically one single line).  

Examples:
A DAVFS::Callbacks with only an URI is already enough for an MKCOL.

A DAVFS::Callbacks object with an URI, a body_in reference and
a line 'Content-Length' in headers_in is sufficient for a PUT request.

=cut

sub new {
	my ( $class, $uri, %opts ) = @_;
	if (not defined $uri) {
		throw Mioga2::Exception::Application("[Mioga2::Callbacks::new]",
				"No URI given!");
	}
	if (defined($opts{headers_in}) and not
			(ref($opts{headers_in}) eq 'HASH' or $opts{headers_in}->isa('APR::Table'))) {
		throw Mioga2::Exception::Application("[Mioga2::Callbacks::new]",
				"'headers_in' is neither hashref nor APR::Table");				
	}
	if (defined($opts{body_in}) and ((ref ($opts{body_in}) ne 'SCALAR') and (not $opts{body_in}->isa('IO::Handle')))) {
		throw Mioga2::Exception::Application("[Mioga2::Callbacks::new]",
				"'body_in' is not an IO::Handle nor a SCALAR ref");
	}
	if (defined($opts{headers_out}) and not $opts{headers_out}->isa('APR::Table')) {
		throw Mioga2::Exception::Application("[Mioga2::Callbacks::new]",
				"'headers_out' is not an APR::Table");
	}
	if (defined($opts{body_out}) and not 
			(ref($opts{body_out}) eq 'SCALAR' or $opts{body_out}->isa('IO::Handle'))) {
		throw Mioga2::Exception::Application("[Mioga2::Callbacks::new]",
				"'body_out' is neither IO::Handle nor scalarref");
	}
	my %oneliners = ( 
		'content_type' => 'set_content_type',
		'status'       => 'set_status',
		'status_line'  => 'set_status_line'
	);
	while (my ($funcname, $optname) = each %oneliners) {
		if (defined($opts{$optname}) and not ref($opts{$optname}) eq 'SCALAR') {
			throw Mioga2::Exception::Application("[Mioga2::Callbacks::new]",
					"'$optname' is not a scalarref");
		}
	}
		
	my $cb = { uri => sub { return $uri; }, remote_host => sub { return undef; } };
	
	$cb->{headers_in} = (defined $opts{headers_in})
			? sub { return $opts{headers_in} }
			: sub { };
			
	if (ref ($opts{body_in}) eq 'SCALAR') {
		$cb->{content_has_been_read} = 0;
		$cb->{content_read} = sub {
			if ($cb->{content_has_been_read}) {
				return ('');
			}
			else {
				$cb->{content_has_been_read} = 1;
				return (${$opts{body_in}});
			}
		};
	}
	else {
		$cb->{content_read} = (defined $opts{body_in})
				? $class->_createSubForReadingFH($opts{body_in})
				: sub { };
	}
		
	$cb->{headers_out} = (defined $opts{headers_out}) 
			? sub { return $opts{headers_out} }
			: sub { };
	if (not defined $opts{body_out}) {
		$cb->{content_write} = sub { }; # black hole
	} elsif (ref($opts{body_out}) eq 'SCALAR') {
		$cb->{content_write} = $class->_createSubForWritingStringref($opts{body_out});
	} elsif ($opts{body_out}->isa("IO::Handle")) {
		$cb->{content_write} = $class->_createSubForWritingFH($opts{body_out});
	}
	
	# Set up the three 'one-line' output functions
	while (my ($funcname, $optname) = each %oneliners) {
		$cb->{$funcname} = (defined $opts{$optname})
				? $class->_createSubForWritingStringref($opts{$optname}, 'overwrite')
				: sub { };
	}

	bless $cb, $class;
	return $cb;
}

sub _createSubForReadingFH {
	my ($clob, $fh) = @_;
	return sub {
		my $buffer;
		my $bytes_read = $fh->read( $buffer, 102400 );
		return ( $bytes_read ? $buffer : '' );
	};
}

sub _createSubForWritingFH {
	my ($clob, $fh) = @_;
	return sub { $fh->print(@_); };
}

sub _createSubForWritingStringref {
	my ($clob, $ref, $overwrite) = @_;
	if ($overwrite) {
		return sub {
			$$ref = '';
			foreach my $arg (@_) {
				$$ref .= $arg;
			}
		};
	} else {
		return sub {
			foreach my $arg (@_) {
				$$ref .= $arg;
			}
		};
	}
}




package Mioga2::DAVFS::PseudoRequest;

sub new {
	my ( $class, $usage, $method ) = @_;
	my $pr = {
		method	=> $method,
		usage   => $usage
	};
	bless $pr, $class;
	return $pr;	
}

sub method {
	my ($self) = @_;
	return $self->{method};
}

sub warn {
	my ($self, $msg) = @_;
	print STDERR "[Mioga2::DAVFS::PseudoRequest for $self->{usage}] $msg\n"
			if $debug;
}

1;
