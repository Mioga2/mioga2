####################################################################
#
#    This file was generated using Parse::Yapp version 1.05.
#
#        Don't edit this file, use source file instead.
#
#             ANY CHANGE MADE HERE WILL BE LOST !
#
####################################################################
package Mioga2::Contact::vCard;
use vars qw ( @ISA );
use strict;

@ISA= qw ( Parse::Yapp::Driver );
use Parse::Yapp::Driver;

#line 27 "lib/Mioga2/Contact/vCard.yp"

    use Error;
    use Mioga2::Exception::Simple;
    my %groups;
    my $value_encoding;


sub new {
        my($class)=shift;
        ref($class)
    and $class=ref($class);

    my($self)=$class->SUPER::new( yyversion => '1.05',
                                  yystates =>
[
	{#State 0
		ACTIONS => {
			'SPACE' => 1,
			'HTAB' => 5,
			'CRLF' => 4
		},
		DEFAULT => -20,
		GOTOS => {
			'vcard_file' => 2,
			'wsls1' => 3,
			'eowsls' => 7,
			'wsls' => 6
		}
	},
	{#State 1
		DEFAULT => -7
	},
	{#State 2
		ACTIONS => {
			'' => 8
		}
	},
	{#State 3
		ACTIONS => {
			'SPACE' => 1,
			'HTAB' => 5,
			'CRLF' => 4
		},
		DEFAULT => -10,
		GOTOS => {
			'wsls1' => 3,
			'wsls' => 9
		}
	},
	{#State 4
		DEFAULT => -9
	},
	{#State 5
		DEFAULT => -8
	},
	{#State 6
		DEFAULT => -21
	},
	{#State 7
		ACTIONS => {
			"BEGIN" => 11
		},
		GOTOS => {
			'vcard' => 10
		}
	},
	{#State 8
		DEFAULT => 0
	},
	{#State 9
		DEFAULT => -11
	},
	{#State 10
		ACTIONS => {
			'SPACE' => 1,
			'HTAB' => 5,
			'CRLF' => 4
		},
		DEFAULT => -20,
		GOTOS => {
			'wsls1' => 3,
			'eowsls' => 12,
			'wsls' => 6
		}
	},
	{#State 11
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 17,
			'ws1' => 16
		}
	},
	{#State 12
		DEFAULT => -1
	},
	{#State 13
		DEFAULT => -3
	},
	{#State 14
		DEFAULT => -19
	},
	{#State 15
		DEFAULT => -4
	},
	{#State 16
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -5,
		GOTOS => {
			'ws' => 18,
			'ws1' => 16
		}
	},
	{#State 17
		ACTIONS => {
			":" => 19
		}
	},
	{#State 18
		DEFAULT => -6
	},
	{#State 19
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 20,
			'ws1' => 16
		}
	},
	{#State 20
		ACTIONS => {
			"VCARD" => 21
		}
	},
	{#State 21
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 22,
			'ws1' => 16
		}
	},
	{#State 22
		ACTIONS => {
			'CRLF' => 23
		},
		GOTOS => {
			'oomcrlf' => 24
		}
	},
	{#State 23
		ACTIONS => {
			'CRLF' => 23
		},
		DEFAULT => -16,
		GOTOS => {
			'oomcrlf' => 25
		}
	},
	{#State 24
		ACTIONS => {
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 32,
			"LOGO" => 34,
			"VALUE" => 35,
			"TEL" => 37,
			"ROLE" => 36,
			"ADR" => 39,
			"CID" => 38,
			"PHOTO" => 40,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 52,
			"AGENT" => 51,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			"UID" => 56,
			"KEY" => 57,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 64,
			"CIS" => 63,
			"ORG" => 62,
			"INLINE" => 61,
			"X-" => 65,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 69,
			"FN" => 68,
			"LABEL" => 70,
			"APPLELINK" => 71,
			"TZ" => 72,
			"GEO" => 73,
			"TITLE" => 75,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"JPEG" => 82,
			"PREF" => 83,
			"SOUND" => 84,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"ATTMAIL" => 99,
			"EWORLD" => 98,
			"REV" => 97,
			"PDF" => 96,
			"TIFF" => 102,
			"GIF" => 103,
			"X400" => 104,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 107,
			"MSG" => 109,
			"VOICE" => 108,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 112,
			"AVI" => 113,
			"URL" => 115,
			"TLX" => 114,
			"PCM" => 116,
			"WMF" => 118,
			"VCARD" => 119,
			"N" => 120,
			"POSTAL" => 121
		},
		GOTOS => {
			'word' => 100,
			'groups' => 87,
			'item' => 80,
			'item1' => 101,
			'items' => 122,
			'ignoredname' => 117
		}
	},
	{#State 25
		DEFAULT => -17
	},
	{#State 26
		DEFAULT => -109
	},
	{#State 27
		DEFAULT => -75
	},
	{#State 28
		DEFAULT => -31
	},
	{#State 29
		DEFAULT => -96
	},
	{#State 30
		DEFAULT => -95
	},
	{#State 31
		DEFAULT => -45
	},
	{#State 32
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -108,
		GOTOS => {
			'params' => 123
		}
	},
	{#State 33
		DEFAULT => -87
	},
	{#State 34
		ACTIONS => {
			"." => -65
		},
		DEFAULT => -143
	},
	{#State 35
		DEFAULT => -66
	},
	{#State 36
		ACTIONS => {
			"." => -97
		},
		DEFAULT => -147
	},
	{#State 37
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -86,
		GOTOS => {
			'params' => 125
		}
	},
	{#State 38
		DEFAULT => -83
	},
	{#State 39
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -64,
		GOTOS => {
			'params' => 126
		}
	},
	{#State 40
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -99,
		GOTOS => {
			'params' => 127
		}
	},
	{#State 41
		DEFAULT => -78
	},
	{#State 42
		DEFAULT => -24
	},
	{#State 43
		DEFAULT => -100
	},
	{#State 44
		DEFAULT => -90
	},
	{#State 45
		DEFAULT => -81
	},
	{#State 46
		DEFAULT => -56
	},
	{#State 47
		DEFAULT => -27
	},
	{#State 48
		DEFAULT => -50
	},
	{#State 49
		DEFAULT => -29
	},
	{#State 50
		DEFAULT => -110
	},
	{#State 51
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -91,
		GOTOS => {
			'params' => 128
		}
	},
	{#State 52
		ACTIONS => {
			"." => -38
		},
		DEFAULT => -148
	},
	{#State 53
		DEFAULT => -54
	},
	{#State 54
		DEFAULT => -94
	},
	{#State 55
		DEFAULT => -39
	},
	{#State 56
		ACTIONS => {
			"." => -32
		},
		DEFAULT => -149
	},
	{#State 57
		ACTIONS => {
			"." => -59
		},
		DEFAULT => -150
	},
	{#State 58
		DEFAULT => -61
	},
	{#State 59
		DEFAULT => -73
	},
	{#State 60
		DEFAULT => -93
	},
	{#State 61
		DEFAULT => -92
	},
	{#State 62
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -89,
		GOTOS => {
			'params' => 129
		}
	},
	{#State 63
		DEFAULT => -79
	},
	{#State 64
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -33,
		GOTOS => {
			'params' => 130
		}
	},
	{#State 65
		ACTIONS => {
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"X400" => 104,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"WMF" => 118,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -37,
		GOTOS => {
			'word' => 152
		}
	},
	{#State 66
		DEFAULT => -36
	},
	{#State 67
		DEFAULT => -42
	},
	{#State 68
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -104,
		GOTOS => {
			'params' => 157
		}
	},
	{#State 69
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -101,
		GOTOS => {
			'params' => 158
		}
	},
	{#State 70
		ACTIONS => {
			"." => -82
		},
		DEFAULT => -152
	},
	{#State 71
		DEFAULT => -48
	},
	{#State 72
		ACTIONS => {
			"." => -106
		},
		DEFAULT => -145
	},
	{#State 73
		ACTIONS => {
			"." => -77
		},
		DEFAULT => -146
	},
	{#State 74
		DEFAULT => -111
	},
	{#State 75
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -68,
		GOTOS => {
			'params' => 159
		}
	},
	{#State 76
		DEFAULT => -84
	},
	{#State 77
		DEFAULT => -112
	},
	{#State 78
		DEFAULT => -88
	},
	{#State 79
		DEFAULT => -70
	},
	{#State 80
		DEFAULT => -115
	},
	{#State 81
		DEFAULT => -22
	},
	{#State 82
		DEFAULT => -34
	},
	{#State 83
		DEFAULT => -40
	},
	{#State 84
		ACTIONS => {
			"." => -85
		},
		DEFAULT => -144
	},
	{#State 85
		DEFAULT => -74
	},
	{#State 86
		DEFAULT => -30
	},
	{#State 87
		ACTIONS => {
			"." => 160
		}
	},
	{#State 88
		DEFAULT => -105
	},
	{#State 89
		DEFAULT => -62
	},
	{#State 90
		DEFAULT => -53
	},
	{#State 91
		DEFAULT => -41
	},
	{#State 92
		DEFAULT => -107
	},
	{#State 93
		DEFAULT => -103
	},
	{#State 94
		DEFAULT => -25
	},
	{#State 95
		DEFAULT => -80
	},
	{#State 96
		DEFAULT => -35
	},
	{#State 97
		ACTIONS => {
			"." => -46
		},
		DEFAULT => -151
	},
	{#State 98
		DEFAULT => -51
	},
	{#State 99
		DEFAULT => -76
	},
	{#State 100
		DEFAULT => -114
	},
	{#State 101
		DEFAULT => -117
	},
	{#State 102
		DEFAULT => -60
	},
	{#State 103
		DEFAULT => -44
	},
	{#State 104
		DEFAULT => -72
	},
	{#State 105
		DEFAULT => -52
	},
	{#State 106
		DEFAULT => -58
	},
	{#State 107
		ACTIONS => {
			"." => -26
		},
		DEFAULT => -153
	},
	{#State 108
		DEFAULT => -69
	},
	{#State 109
		DEFAULT => -71
	},
	{#State 110
		DEFAULT => -67
	},
	{#State 111
		DEFAULT => -57
	},
	{#State 112
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -63,
		GOTOS => {
			'params' => 161
		}
	},
	{#State 113
		DEFAULT => -102
	},
	{#State 114
		DEFAULT => -49
	},
	{#State 115
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -47,
		GOTOS => {
			'params' => 162
		}
	},
	{#State 116
		DEFAULT => -43
	},
	{#State 117
		ACTIONS => {
			";" => 124
		},
		DEFAULT => -159,
		GOTOS => {
			'params' => 163
		}
	},
	{#State 118
		DEFAULT => -98
	},
	{#State 119
		DEFAULT => -23
	},
	{#State 120
		ACTIONS => {
			":" => -159,
			";" => 124
		},
		DEFAULT => -55,
		GOTOS => {
			'params' => 164
		}
	},
	{#State 121
		DEFAULT => -28
	},
	{#State 122
		ACTIONS => {
			'CRLF' => 166
		},
		DEFAULT => -14,
		GOTOS => {
			'soncrlf' => 165
		}
	},
	{#State 123
		ACTIONS => {
			":" => 167
		}
	},
	{#State 124
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 168,
			'ws1' => 16
		}
	},
	{#State 125
		ACTIONS => {
			":" => 169
		}
	},
	{#State 126
		ACTIONS => {
			":" => 170
		}
	},
	{#State 127
		ACTIONS => {
			":" => 171
		}
	},
	{#State 128
		ACTIONS => {
			":" => 172
		}
	},
	{#State 129
		ACTIONS => {
			":" => 173
		}
	},
	{#State 130
		ACTIONS => {
			":" => 174
		}
	},
	{#State 131
		DEFAULT => -108
	},
	{#State 132
		DEFAULT => -65
	},
	{#State 133
		DEFAULT => -97
	},
	{#State 134
		DEFAULT => -86
	},
	{#State 135
		DEFAULT => -64
	},
	{#State 136
		DEFAULT => -99
	},
	{#State 137
		DEFAULT => -91
	},
	{#State 138
		DEFAULT => -38
	},
	{#State 139
		DEFAULT => -32
	},
	{#State 140
		DEFAULT => -59
	},
	{#State 141
		DEFAULT => -89
	},
	{#State 142
		DEFAULT => -33
	},
	{#State 143
		DEFAULT => -37
	},
	{#State 144
		DEFAULT => -104
	},
	{#State 145
		DEFAULT => -101
	},
	{#State 146
		DEFAULT => -82
	},
	{#State 147
		DEFAULT => -106
	},
	{#State 148
		DEFAULT => -77
	},
	{#State 149
		DEFAULT => -68
	},
	{#State 150
		DEFAULT => -85
	},
	{#State 151
		DEFAULT => -46
	},
	{#State 152
		DEFAULT => -154
	},
	{#State 153
		DEFAULT => -26
	},
	{#State 154
		DEFAULT => -63
	},
	{#State 155
		DEFAULT => -47
	},
	{#State 156
		DEFAULT => -55
	},
	{#State 157
		ACTIONS => {
			":" => 175
		}
	},
	{#State 158
		ACTIONS => {
			":" => 176
		}
	},
	{#State 159
		ACTIONS => {
			":" => 177
		}
	},
	{#State 160
		ACTIONS => {
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 32,
			"LOGO" => 34,
			"VALUE" => 35,
			"TEL" => 37,
			"ROLE" => 36,
			"ADR" => 39,
			"CID" => 38,
			"PHOTO" => 40,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 52,
			"AGENT" => 51,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			"UID" => 56,
			"KEY" => 57,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 64,
			"CIS" => 63,
			"ORG" => 62,
			"INLINE" => 61,
			"X-" => 65,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 69,
			"FN" => 68,
			"LABEL" => 70,
			"APPLELINK" => 71,
			"TZ" => 72,
			"GEO" => 73,
			"TITLE" => 75,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 84,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 97,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"X400" => 104,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 107,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 112,
			"AVI" => 113,
			"URL" => 115,
			"TLX" => 114,
			"PCM" => 116,
			"WMF" => 118,
			"VCARD" => 119,
			"N" => 120,
			"POSTAL" => 121
		},
		GOTOS => {
			'word' => 178,
			'item1' => 179,
			'ignoredname' => 117
		}
	},
	{#State 161
		ACTIONS => {
			":" => 180
		}
	},
	{#State 162
		ACTIONS => {
			":" => 181
		}
	},
	{#State 163
		ACTIONS => {
			":" => 182
		}
	},
	{#State 164
		ACTIONS => {
			":" => 183
		}
	},
	{#State 165
		ACTIONS => {
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 32,
			"LOGO" => 34,
			"VALUE" => 35,
			"TEL" => 37,
			"ROLE" => 36,
			"ADR" => 39,
			"CID" => 38,
			"PHOTO" => 40,
			"8BIT" => 41,
			"END" => 184,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 52,
			"AGENT" => 51,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			"UID" => 56,
			"KEY" => 57,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 64,
			"CIS" => 63,
			"ORG" => 62,
			"INLINE" => 61,
			"X-" => 65,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 69,
			"FN" => 68,
			"LABEL" => 70,
			"APPLELINK" => 71,
			"TZ" => 72,
			"GEO" => 73,
			"TITLE" => 75,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 84,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 97,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"X400" => 104,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 107,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 112,
			"AVI" => 113,
			"URL" => 115,
			"TLX" => 114,
			"PCM" => 116,
			"WMF" => 118,
			"VCARD" => 119,
			"N" => 120,
			"POSTAL" => 121
		},
		GOTOS => {
			'word' => 100,
			'groups' => 87,
			'item' => 185,
			'item1' => 101,
			'ignoredname' => 117
		}
	},
	{#State 166
		ACTIONS => {
			'CRLF' => 166
		},
		DEFAULT => -14,
		GOTOS => {
			'soncrlf' => 186
		}
	},
	{#State 167
		DEFAULT => -131,
		GOTOS => {
			'@7-3' => 187
		}
	},
	{#State 168
		ACTIONS => {
			"MPEG2" => 218,
			"ENCODING" => 189,
			"INTL" => 188,
			"CGM" => 219,
			"DOM" => 190,
			"WAVE" => 191,
			"INTERNET" => 192,
			"LANGUAGE" => 193,
			"VALUE" => 194,
			"PREF" => 221,
			"JPEG" => 220,
			"PGP" => 222,
			"PAGER" => 224,
			"MCIMAIL" => 223,
			"PRODIGY" => 226,
			"QTIME" => 196,
			"VIDEO" => 197,
			"IBMMAIL" => 228,
			"PS" => 227,
			"AIFF" => 198,
			"DIB" => 199,
			"ISDN" => 200,
			"CAR" => 201,
			"MODEM" => 202,
			"PMB" => 203,
			"PICT" => 229,
			"ATTMAIL" => 232,
			"EWORLD" => 231,
			"PDF" => 230,
			"AOL" => 204,
			"TIFF" => 233,
			"GIF" => 234,
			"MPEG" => 205,
			"POWERSHARE" => 206,
			"X400" => 235,
			"CELL" => 236,
			"TYPE" => 237,
			"BMP" => 208,
			"PARCEL" => 209,
			"VOICE" => 239,
			"MSG" => 238,
			"WORK" => 210,
			"X509" => 240,
			"FAX" => 241,
			"CIS" => 211,
			"X-" => 212,
			"AVI" => 242,
			"CHARSET" => 213,
			"TLX" => 243,
			"HOME" => 214,
			"PCM" => 244,
			"APPLELINK" => 215,
			"WMF" => 245,
			"MET" => 216,
			"BBS" => 217,
			"POSTAL" => 246
		},
		GOTOS => {
			'paramlist' => 207,
			'knowntype' => 195,
			'param' => 225
		}
	},
	{#State 169
		DEFAULT => -129,
		GOTOS => {
			'@6-3' => 247
		}
	},
	{#State 170
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 249,
			'nonsemi' => 250,
			'addressparts' => 255,
			'ws1' => 252
		}
	},
	{#State 171
		DEFAULT => -121,
		GOTOS => {
			'@2-3' => 258
		}
	},
	{#State 172
		ACTIONS => {
			'CRLF' => 260
		},
		DEFAULT => -12,
		GOTOS => {
			'eocrlf' => 259
		}
	},
	{#State 173
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 261,
			'orgparts' => 262,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 174
		DEFAULT => -141,
		GOTOS => {
			'@10-3' => 263
		}
	},
	{#State 175
		DEFAULT => -123,
		GOTOS => {
			'@3-3' => 264
		}
	},
	{#State 176
		DEFAULT => -133,
		GOTOS => {
			'@8-3' => 265
		}
	},
	{#State 177
		DEFAULT => -125,
		GOTOS => {
			'@4-3' => 266
		}
	},
	{#State 178
		DEFAULT => -113
	},
	{#State 179
		DEFAULT => -118
	},
	{#State 180
		DEFAULT => -127,
		GOTOS => {
			'@5-3' => 267
		}
	},
	{#State 181
		DEFAULT => -135,
		GOTOS => {
			'@9-3' => 268
		}
	},
	{#State 182
		DEFAULT => -119,
		GOTOS => {
			'@1-3' => 269
		}
	},
	{#State 183
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'nameparts' => 271,
			'strnosemi' => 270,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 184
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15,
			"." => -24
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 272,
			'ws1' => 16
		}
	},
	{#State 185
		DEFAULT => -116
	},
	{#State 186
		DEFAULT => -15
	},
	{#State 187
		ACTIONS => {
			"7BIT_VAL" => 275,
			"QUOTED-PRINTABLE_VAL" => 273,
			"BASE64_VAL" => 276,
			"8BIT_VAL" => 274
		},
		GOTOS => {
			'value' => 277
		}
	},
	{#State 188
		DEFAULT => -211
	},
	{#State 189
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 278,
			'ws1' => 16
		}
	},
	{#State 190
		DEFAULT => -210
	},
	{#State 191
		DEFAULT => -255
	},
	{#State 192
		DEFAULT => -232
	},
	{#State 193
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 279,
			'ws1' => 16
		}
	},
	{#State 194
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 280,
			'ws1' => 16
		}
	},
	{#State 195
		DEFAULT => -169
	},
	{#State 196
		DEFAULT => -251
	},
	{#State 197
		DEFAULT => -226
	},
	{#State 198
		DEFAULT => -256
	},
	{#State 199
		DEFAULT => -245
	},
	{#State 200
		DEFAULT => -225
	},
	{#State 201
		DEFAULT => -224
	},
	{#State 202
		DEFAULT => -223
	},
	{#State 203
		DEFAULT => -244
	},
	{#State 204
		DEFAULT => -227
	},
	{#State 205
		DEFAULT => -252
	},
	{#State 206
		DEFAULT => -235
	},
	{#State 207
		ACTIONS => {
			":" => -160,
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 281,
			'ws1' => 16
		}
	},
	{#State 208
		DEFAULT => -242
	},
	{#State 209
		DEFAULT => -213
	},
	{#State 210
		DEFAULT => -215
	},
	{#State 211
		DEFAULT => -230
	},
	{#State 212
		ACTIONS => {
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"X400" => 104,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"WMF" => 118,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		GOTOS => {
			'word' => 282
		}
	},
	{#State 213
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 283,
			'ws1' => 16
		}
	},
	{#State 214
		DEFAULT => -214
	},
	{#State 215
		DEFAULT => -228
	},
	{#State 216
		DEFAULT => -243
	},
	{#State 217
		DEFAULT => -222
	},
	{#State 218
		DEFAULT => -253
	},
	{#State 219
		DEFAULT => -240
	},
	{#State 220
		DEFAULT => -250
	},
	{#State 221
		DEFAULT => -216
	},
	{#State 222
		DEFAULT => -259
	},
	{#State 223
		DEFAULT => -234
	},
	{#State 224
		DEFAULT => -221
	},
	{#State 225
		DEFAULT => -161
	},
	{#State 226
		DEFAULT => -236
	},
	{#State 227
		DEFAULT => -249
	},
	{#State 228
		DEFAULT => -233
	},
	{#State 229
		DEFAULT => -246
	},
	{#State 230
		DEFAULT => -248
	},
	{#State 231
		DEFAULT => -231
	},
	{#State 232
		DEFAULT => -229
	},
	{#State 233
		DEFAULT => -247
	},
	{#State 234
		DEFAULT => -239
	},
	{#State 235
		DEFAULT => -238
	},
	{#State 236
		DEFAULT => -220
	},
	{#State 237
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 284,
			'ws1' => 16
		}
	},
	{#State 238
		DEFAULT => -219
	},
	{#State 239
		DEFAULT => -217
	},
	{#State 240
		DEFAULT => -258
	},
	{#State 241
		DEFAULT => -218
	},
	{#State 242
		DEFAULT => -254
	},
	{#State 243
		DEFAULT => -237
	},
	{#State 244
		DEFAULT => -257
	},
	{#State 245
		DEFAULT => -241
	},
	{#State 246
		DEFAULT => -212
	},
	{#State 247
		ACTIONS => {
			"7BIT_VAL" => 275,
			"QUOTED-PRINTABLE_VAL" => 273,
			"BASE64_VAL" => 276,
			"8BIT_VAL" => 274
		},
		GOTOS => {
			'value' => 285
		}
	},
	{#State 248
		DEFAULT => -205
	},
	{#State 249
		ACTIONS => {
			";" => 286
		},
		DEFAULT => -184
	},
	{#State 250
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			'ESCAPECRLF' => 287,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			'EQUALCRLF' => 289,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"MCIMAIL" => 89,
			"PAGER" => 88,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			'ESCAPEDSC' => 290,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 288,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 251
		DEFAULT => -208
	},
	{#State 252
		DEFAULT => -203
	},
	{#State 253
		DEFAULT => -204
	},
	{#State 254
		DEFAULT => -207
	},
	{#State 255
		ACTIONS => {
			'CRLF' => 291
		}
	},
	{#State 256
		DEFAULT => -206
	},
	{#State 257
		DEFAULT => -209
	},
	{#State 258
		ACTIONS => {
			"7BIT_VAL" => 275,
			"QUOTED-PRINTABLE_VAL" => 273,
			"BASE64_VAL" => 276,
			"8BIT_VAL" => 274
		},
		GOTOS => {
			'value' => 292
		}
	},
	{#State 259
		ACTIONS => {
			"BEGIN" => 11
		},
		GOTOS => {
			'vcard' => 293
		}
	},
	{#State 260
		DEFAULT => -13
	},
	{#State 261
		ACTIONS => {
			";" => 294
		},
		DEFAULT => -191
	},
	{#State 262
		ACTIONS => {
			'CRLF' => 295
		}
	},
	{#State 263
		ACTIONS => {
			"7BIT_VAL" => 275,
			"QUOTED-PRINTABLE_VAL" => 273,
			"BASE64_VAL" => 276,
			"8BIT_VAL" => 274
		},
		GOTOS => {
			'value' => 296
		}
	},
	{#State 264
		ACTIONS => {
			"7BIT_VAL" => 275,
			"QUOTED-PRINTABLE_VAL" => 273,
			"BASE64_VAL" => 276,
			"8BIT_VAL" => 274
		},
		GOTOS => {
			'value' => 297
		}
	},
	{#State 265
		ACTIONS => {
			"7BIT_VAL" => 275,
			"QUOTED-PRINTABLE_VAL" => 273,
			"BASE64_VAL" => 276,
			"8BIT_VAL" => 274
		},
		GOTOS => {
			'value' => 298
		}
	},
	{#State 266
		ACTIONS => {
			"7BIT_VAL" => 275,
			"QUOTED-PRINTABLE_VAL" => 273,
			"BASE64_VAL" => 276,
			"8BIT_VAL" => 274
		},
		GOTOS => {
			'value' => 299
		}
	},
	{#State 267
		ACTIONS => {
			"7BIT_VAL" => 275,
			"QUOTED-PRINTABLE_VAL" => 273,
			"BASE64_VAL" => 276,
			"8BIT_VAL" => 274
		},
		GOTOS => {
			'value' => 300
		}
	},
	{#State 268
		ACTIONS => {
			"7BIT_VAL" => 275,
			"QUOTED-PRINTABLE_VAL" => 273,
			"BASE64_VAL" => 276,
			"8BIT_VAL" => 274
		},
		GOTOS => {
			'value' => 301
		}
	},
	{#State 269
		ACTIONS => {
			"7BIT_VAL" => 275,
			"QUOTED-PRINTABLE_VAL" => 273,
			"BASE64_VAL" => 276,
			"8BIT_VAL" => 274
		},
		GOTOS => {
			'value' => 302
		}
	},
	{#State 270
		ACTIONS => {
			";" => 303
		},
		DEFAULT => -193
	},
	{#State 271
		ACTIONS => {
			'CRLF' => 304
		}
	},
	{#State 272
		ACTIONS => {
			":" => 305
		}
	},
	{#State 273
		DEFAULT => -158
	},
	{#State 274
		DEFAULT => -156
	},
	{#State 275
		DEFAULT => -155
	},
	{#State 276
		DEFAULT => -157
	},
	{#State 277
		ACTIONS => {
			'CRLF' => 306
		}
	},
	{#State 278
		ACTIONS => {
			"=" => 307
		}
	},
	{#State 279
		ACTIONS => {
			"=" => 308
		}
	},
	{#State 280
		ACTIONS => {
			"=" => 309
		}
	},
	{#State 281
		ACTIONS => {
			";" => 310
		}
	},
	{#State 282
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 311,
			'ws1' => 16
		}
	},
	{#State 283
		ACTIONS => {
			"=" => 312
		}
	},
	{#State 284
		ACTIONS => {
			"=" => 313
		}
	},
	{#State 285
		ACTIONS => {
			'CRLF' => 314
		}
	},
	{#State 286
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 315,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 287
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 316,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 288
		DEFAULT => -199
	},
	{#State 289
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 317,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 290
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 318,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 291
		DEFAULT => -137
	},
	{#State 292
		ACTIONS => {
			'CRLF' => 319
		}
	},
	{#State 293
		ACTIONS => {
			'CRLF' => 320
		}
	},
	{#State 294
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 261,
			'orgparts' => 321,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 295
		DEFAULT => -138
	},
	{#State 296
		ACTIONS => {
			'CRLF' => 322
		}
	},
	{#State 297
		ACTIONS => {
			'CRLF' => 323
		}
	},
	{#State 298
		ACTIONS => {
			'CRLF' => 324
		}
	},
	{#State 299
		ACTIONS => {
			'CRLF' => 325
		}
	},
	{#State 300
		ACTIONS => {
			'CRLF' => 326
		}
	},
	{#State 301
		ACTIONS => {
			'CRLF' => 327
		}
	},
	{#State 302
		ACTIONS => {
			'CRLF' => 328
		}
	},
	{#State 303
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 329,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 304
		DEFAULT => -139
	},
	{#State 305
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 330,
			'ws1' => 16
		}
	},
	{#State 306
		DEFAULT => -132
	},
	{#State 307
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 331,
			'ws1' => 16
		}
	},
	{#State 308
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 332,
			'ws1' => 16
		}
	},
	{#State 309
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 333,
			'ws1' => 16
		}
	},
	{#State 310
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 334,
			'ws1' => 16
		}
	},
	{#State 311
		ACTIONS => {
			"=" => 335
		}
	},
	{#State 312
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 336,
			'ws1' => 16
		}
	},
	{#State 313
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 337,
			'ws1' => 16
		}
	},
	{#State 314
		DEFAULT => -130
	},
	{#State 315
		ACTIONS => {
			";" => 338
		},
		DEFAULT => -185
	},
	{#State 316
		DEFAULT => -201
	},
	{#State 317
		DEFAULT => -202
	},
	{#State 318
		DEFAULT => -200
	},
	{#State 319
		DEFAULT => -122
	},
	{#State 320
		DEFAULT => -140
	},
	{#State 321
		DEFAULT => -192
	},
	{#State 322
		DEFAULT => -142
	},
	{#State 323
		DEFAULT => -124
	},
	{#State 324
		DEFAULT => -134
	},
	{#State 325
		DEFAULT => -126
	},
	{#State 326
		DEFAULT => -128
	},
	{#State 327
		DEFAULT => -136
	},
	{#State 328
		DEFAULT => -120
	},
	{#State 329
		ACTIONS => {
			";" => 339
		},
		DEFAULT => -194
	},
	{#State 330
		ACTIONS => {
			"VCARD" => 340
		}
	},
	{#State 331
		ACTIONS => {
			"7BIT" => 346,
			"QUOTED-PRINTABLE" => 345,
			"X-" => 343,
			"BASE64" => 341,
			"8BIT" => 342
		},
		GOTOS => {
			'pencodingval' => 344
		}
	},
	{#State 332
		ACTIONS => {
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"X400" => 104,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"WMF" => 118,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		GOTOS => {
			'word' => 348,
			'langval' => 347
		}
	},
	{#State 333
		ACTIONS => {
			"URL" => 354,
			"INLINE" => 350,
			"X-" => 351,
			"CID" => 349,
			"CONTENT-ID" => 353
		},
		GOTOS => {
			'pvalueval' => 352
		}
	},
	{#State 334
		ACTIONS => {
			"MPEG2" => 218,
			"ENCODING" => 189,
			"INTL" => 188,
			"CGM" => 219,
			"DOM" => 190,
			"WAVE" => 191,
			"INTERNET" => 192,
			"LANGUAGE" => 193,
			"VALUE" => 194,
			"PREF" => 221,
			"JPEG" => 220,
			"PGP" => 222,
			"PAGER" => 224,
			"MCIMAIL" => 223,
			"PRODIGY" => 226,
			"QTIME" => 196,
			"VIDEO" => 197,
			"IBMMAIL" => 228,
			"PS" => 227,
			"AIFF" => 198,
			"DIB" => 199,
			"ISDN" => 200,
			"CAR" => 201,
			"MODEM" => 202,
			"PMB" => 203,
			"PICT" => 229,
			"ATTMAIL" => 232,
			"EWORLD" => 231,
			"PDF" => 230,
			"AOL" => 204,
			"TIFF" => 233,
			"GIF" => 234,
			"MPEG" => 205,
			"POWERSHARE" => 206,
			"X400" => 235,
			"CELL" => 236,
			"TYPE" => 237,
			"BMP" => 208,
			"PARCEL" => 209,
			"VOICE" => 239,
			"MSG" => 238,
			"WORK" => 210,
			"X509" => 240,
			"FAX" => 241,
			"CIS" => 211,
			"X-" => 212,
			"AVI" => 242,
			"CHARSET" => 213,
			"TLX" => 243,
			"HOME" => 214,
			"PCM" => 244,
			"APPLELINK" => 215,
			"WMF" => 245,
			"MET" => 216,
			"BBS" => 217,
			"POSTAL" => 246
		},
		GOTOS => {
			'knowntype' => 195,
			'param' => 355
		}
	},
	{#State 335
		ACTIONS => {
			'SPACE' => 13,
			'HTAB' => 15
		},
		DEFAULT => -18,
		GOTOS => {
			'ws' => 14,
			'eows' => 356,
			'ws1' => 16
		}
	},
	{#State 336
		ACTIONS => {
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"X400" => 104,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"WMF" => 118,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		GOTOS => {
			'charsetval' => 358,
			'word' => 357
		}
	},
	{#State 337
		ACTIONS => {
			"MPEG2" => 218,
			"INTL" => 188,
			"CGM" => 219,
			"DOM" => 190,
			"WAVE" => 191,
			"INTERNET" => 192,
			"PREF" => 221,
			"JPEG" => 220,
			"PGP" => 222,
			"PAGER" => 224,
			"MCIMAIL" => 223,
			"PRODIGY" => 226,
			"QTIME" => 196,
			"VIDEO" => 197,
			"IBMMAIL" => 228,
			"PS" => 227,
			"AIFF" => 198,
			"DIB" => 199,
			"ISDN" => 200,
			"CAR" => 201,
			"MODEM" => 202,
			"PMB" => 203,
			"PICT" => 229,
			"ATTMAIL" => 232,
			"EWORLD" => 231,
			"PDF" => 230,
			"AOL" => 204,
			"TIFF" => 233,
			"GIF" => 234,
			"MPEG" => 205,
			"POWERSHARE" => 206,
			"X400" => 235,
			"CELL" => 236,
			"BMP" => 208,
			"PARCEL" => 209,
			"VOICE" => 239,
			"MSG" => 238,
			"WORK" => 210,
			"X509" => 240,
			"FAX" => 241,
			"CIS" => 211,
			"X-" => 361,
			"AVI" => 242,
			"TLX" => 243,
			"HOME" => 214,
			"PCM" => 244,
			"APPLELINK" => 215,
			"WMF" => 245,
			"MET" => 216,
			"BBS" => 217,
			"POSTAL" => 246
		},
		GOTOS => {
			'knowntype' => 359,
			'ptypeval' => 360
		}
	},
	{#State 338
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 362,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 339
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 363,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 340
		DEFAULT => -2
	},
	{#State 341
		DEFAULT => -180
	},
	{#State 342
		DEFAULT => -178
	},
	{#State 343
		ACTIONS => {
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"X400" => 104,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"WMF" => 118,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		GOTOS => {
			'word' => 364
		}
	},
	{#State 344
		DEFAULT => -165
	},
	{#State 345
		DEFAULT => -179
	},
	{#State 346
		DEFAULT => -177
	},
	{#State 347
		DEFAULT => -167
	},
	{#State 348
		DEFAULT => -183
	},
	{#State 349
		DEFAULT => -175
	},
	{#State 350
		DEFAULT => -172
	},
	{#State 351
		ACTIONS => {
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"X400" => 104,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"WMF" => 118,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		GOTOS => {
			'word' => 365
		}
	},
	{#State 352
		DEFAULT => -164
	},
	{#State 353
		DEFAULT => -174
	},
	{#State 354
		DEFAULT => -173
	},
	{#State 355
		DEFAULT => -162
	},
	{#State 356
		ACTIONS => {
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"X400" => 104,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"WMF" => 118,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		GOTOS => {
			'word' => 366
		}
	},
	{#State 357
		DEFAULT => -182
	},
	{#State 358
		DEFAULT => -166
	},
	{#State 359
		DEFAULT => -170
	},
	{#State 360
		DEFAULT => -163
	},
	{#State 361
		ACTIONS => {
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"X400" => 104,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"WMF" => 118,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		GOTOS => {
			'word' => 367
		}
	},
	{#State 362
		ACTIONS => {
			";" => 368
		},
		DEFAULT => -186
	},
	{#State 363
		ACTIONS => {
			";" => 369
		},
		DEFAULT => -195
	},
	{#State 364
		DEFAULT => -181
	},
	{#State 365
		DEFAULT => -176
	},
	{#State 366
		DEFAULT => -168
	},
	{#State 367
		DEFAULT => -171
	},
	{#State 368
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 370,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 369
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 371,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 370
		ACTIONS => {
			";" => 372
		},
		DEFAULT => -187
	},
	{#State 371
		ACTIONS => {
			";" => 373
		},
		DEFAULT => -196
	},
	{#State 372
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 374,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 373
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 375,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 374
		ACTIONS => {
			";" => 376
		},
		DEFAULT => -188
	},
	{#State 375
		DEFAULT => -197
	},
	{#State 376
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 377,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 377
		ACTIONS => {
			";" => 378
		},
		DEFAULT => -189
	},
	{#State 378
		ACTIONS => {
			":" => 248,
			"INTL" => 27,
			"ENCODING" => 26,
			"DOM" => 28,
			"WAVE" => 29,
			"INTERNET" => 31,
			"BASE64" => 30,
			"LANGUAGE" => 33,
			"EMAIL" => 131,
			"LOGO" => 132,
			"VALUE" => 35,
			"TEL" => 134,
			"ROLE" => 133,
			"ADR" => 135,
			"CID" => 38,
			"PHOTO" => 136,
			"8BIT" => 41,
			"END" => 42,
			"VIDEO" => 44,
			"QTIME" => 43,
			"AIFF" => 47,
			"ISDN" => 46,
			"DIB" => 45,
			"CAR" => 49,
			"MODEM" => 48,
			"PMB" => 50,
			"BDAY" => 138,
			"AGENT" => 137,
			"AOL" => 53,
			"POWERSHARE" => 55,
			"MPEG" => 54,
			'NCNSASCII' => 251,
			"UID" => 139,
			"KEY" => 140,
			"BMP" => 58,
			"PARCEL" => 59,
			"WORK" => 60,
			"NICKNAME" => 142,
			"CIS" => 63,
			"ORG" => 141,
			"INLINE" => 61,
			"X-" => 143,
			"CHARSET" => 66,
			"HOME" => 67,
			"NOTE" => 145,
			"FN" => 144,
			"LABEL" => 146,
			"APPLELINK" => 71,
			"TZ" => 147,
			"GEO" => 148,
			"TITLE" => 149,
			"MET" => 74,
			"BBS" => 76,
			'WORD' => 77,
			"MPEG2" => 78,
			"CGM" => 79,
			"BEGIN" => 81,
			"PREF" => 83,
			"JPEG" => 82,
			"SOUND" => 150,
			"QUOTED-PRINTABLE" => 85,
			"PGP" => 86,
			"PAGER" => 88,
			"MCIMAIL" => 89,
			"CONTENT-ID" => 90,
			"PRODIGY" => 91,
			"7BIT" => 92,
			"PS" => 94,
			"IBMMAIL" => 93,
			"PICT" => 95,
			"PDF" => 96,
			"REV" => 151,
			"EWORLD" => 98,
			"ATTMAIL" => 99,
			"TIFF" => 102,
			"GIF" => 103,
			"." => 254,
			"X400" => 104,
			'SPACE' => 13,
			"CELL" => 105,
			"TYPE" => 106,
			"MAILER" => 153,
			"VOICE" => 108,
			"MSG" => 109,
			"X509" => 110,
			"FAX" => 111,
			"VERSION" => 154,
			"AVI" => 113,
			"URL" => 155,
			"TLX" => 114,
			"PCM" => 116,
			"=" => 256,
			"WMF" => 118,
			'ASCII8' => 257,
			'HTAB' => 15,
			"VCARD" => 119,
			"N" => 156,
			"POSTAL" => 121
		},
		DEFAULT => -198,
		GOTOS => {
			'word' => 253,
			'strnosemi' => 379,
			'nonsemi' => 250,
			'ws1' => 252
		}
	},
	{#State 379
		DEFAULT => -190
	}
],
                                  yyrules  =>
[
	[#Rule 0
		 '$start', 2, undef
	],
	[#Rule 1
		 'vcard_file', 3,
sub
#line 38 "lib/Mioga2/Contact/vCard.yp"
{ \%groups }
	],
	[#Rule 2
		 'vcard', 14, undef
	],
	[#Rule 3
		 'ws1', 1, undef
	],
	[#Rule 4
		 'ws1', 1, undef
	],
	[#Rule 5
		 'ws', 1, undef
	],
	[#Rule 6
		 'ws', 2, undef
	],
	[#Rule 7
		 'wsls1', 1, undef
	],
	[#Rule 8
		 'wsls1', 1, undef
	],
	[#Rule 9
		 'wsls1', 1, undef
	],
	[#Rule 10
		 'wsls', 1, undef
	],
	[#Rule 11
		 'wsls', 2, undef
	],
	[#Rule 12
		 'eocrlf', 0, undef
	],
	[#Rule 13
		 'eocrlf', 1, undef
	],
	[#Rule 14
		 'soncrlf', 0, undef
	],
	[#Rule 15
		 'soncrlf', 2, undef
	],
	[#Rule 16
		 'oomcrlf', 1, undef
	],
	[#Rule 17
		 'oomcrlf', 2, undef
	],
	[#Rule 18
		 'eows', 0, undef
	],
	[#Rule 19
		 'eows', 1, undef
	],
	[#Rule 20
		 'eowsls', 0, undef
	],
	[#Rule 21
		 'eowsls', 1, undef
	],
	[#Rule 22
		 'word', 1, undef
	],
	[#Rule 23
		 'word', 1, undef
	],
	[#Rule 24
		 'word', 1, undef
	],
	[#Rule 25
		 'word', 1, undef
	],
	[#Rule 26
		 'word', 1, undef
	],
	[#Rule 27
		 'word', 1, undef
	],
	[#Rule 28
		 'word', 1, undef
	],
	[#Rule 29
		 'word', 1, undef
	],
	[#Rule 30
		 'word', 1, undef
	],
	[#Rule 31
		 'word', 1, undef
	],
	[#Rule 32
		 'word', 1, undef
	],
	[#Rule 33
		 'word', 1, undef
	],
	[#Rule 34
		 'word', 1, undef
	],
	[#Rule 35
		 'word', 1, undef
	],
	[#Rule 36
		 'word', 1, undef
	],
	[#Rule 37
		 'word', 1, undef
	],
	[#Rule 38
		 'word', 1, undef
	],
	[#Rule 39
		 'word', 1, undef
	],
	[#Rule 40
		 'word', 1, undef
	],
	[#Rule 41
		 'word', 1, undef
	],
	[#Rule 42
		 'word', 1, undef
	],
	[#Rule 43
		 'word', 1, undef
	],
	[#Rule 44
		 'word', 1, undef
	],
	[#Rule 45
		 'word', 1, undef
	],
	[#Rule 46
		 'word', 1, undef
	],
	[#Rule 47
		 'word', 1, undef
	],
	[#Rule 48
		 'word', 1, undef
	],
	[#Rule 49
		 'word', 1, undef
	],
	[#Rule 50
		 'word', 1, undef
	],
	[#Rule 51
		 'word', 1, undef
	],
	[#Rule 52
		 'word', 1, undef
	],
	[#Rule 53
		 'word', 1, undef
	],
	[#Rule 54
		 'word', 1, undef
	],
	[#Rule 55
		 'word', 1, undef
	],
	[#Rule 56
		 'word', 1, undef
	],
	[#Rule 57
		 'word', 1, undef
	],
	[#Rule 58
		 'word', 1, undef
	],
	[#Rule 59
		 'word', 1, undef
	],
	[#Rule 60
		 'word', 1, undef
	],
	[#Rule 61
		 'word', 1, undef
	],
	[#Rule 62
		 'word', 1, undef
	],
	[#Rule 63
		 'word', 1, undef
	],
	[#Rule 64
		 'word', 1, undef
	],
	[#Rule 65
		 'word', 1, undef
	],
	[#Rule 66
		 'word', 1, undef
	],
	[#Rule 67
		 'word', 1, undef
	],
	[#Rule 68
		 'word', 1, undef
	],
	[#Rule 69
		 'word', 1, undef
	],
	[#Rule 70
		 'word', 1, undef
	],
	[#Rule 71
		 'word', 1, undef
	],
	[#Rule 72
		 'word', 1, undef
	],
	[#Rule 73
		 'word', 1, undef
	],
	[#Rule 74
		 'word', 1, undef
	],
	[#Rule 75
		 'word', 1, undef
	],
	[#Rule 76
		 'word', 1, undef
	],
	[#Rule 77
		 'word', 1, undef
	],
	[#Rule 78
		 'word', 1, undef
	],
	[#Rule 79
		 'word', 1, undef
	],
	[#Rule 80
		 'word', 1, undef
	],
	[#Rule 81
		 'word', 1, undef
	],
	[#Rule 82
		 'word', 1, undef
	],
	[#Rule 83
		 'word', 1, undef
	],
	[#Rule 84
		 'word', 1, undef
	],
	[#Rule 85
		 'word', 1, undef
	],
	[#Rule 86
		 'word', 1, undef
	],
	[#Rule 87
		 'word', 1, undef
	],
	[#Rule 88
		 'word', 1, undef
	],
	[#Rule 89
		 'word', 1, undef
	],
	[#Rule 90
		 'word', 1, undef
	],
	[#Rule 91
		 'word', 1, undef
	],
	[#Rule 92
		 'word', 1, undef
	],
	[#Rule 93
		 'word', 1, undef
	],
	[#Rule 94
		 'word', 1, undef
	],
	[#Rule 95
		 'word', 1, undef
	],
	[#Rule 96
		 'word', 1, undef
	],
	[#Rule 97
		 'word', 1, undef
	],
	[#Rule 98
		 'word', 1, undef
	],
	[#Rule 99
		 'word', 1, undef
	],
	[#Rule 100
		 'word', 1, undef
	],
	[#Rule 101
		 'word', 1, undef
	],
	[#Rule 102
		 'word', 1, undef
	],
	[#Rule 103
		 'word', 1, undef
	],
	[#Rule 104
		 'word', 1, undef
	],
	[#Rule 105
		 'word', 1, undef
	],
	[#Rule 106
		 'word', 1, undef
	],
	[#Rule 107
		 'word', 1, undef
	],
	[#Rule 108
		 'word', 1, undef
	],
	[#Rule 109
		 'word', 1, undef
	],
	[#Rule 110
		 'word', 1, undef
	],
	[#Rule 111
		 'word', 1, undef
	],
	[#Rule 112
		 'word', 1, undef
	],
	[#Rule 113
		 'groups', 3, undef
	],
	[#Rule 114
		 'groups', 1, undef
	],
	[#Rule 115
		 'items', 1, undef
	],
	[#Rule 116
		 'items', 3, undef
	],
	[#Rule 117
		 'item', 1,
sub
#line 93 "lib/Mioga2/Contact/vCard.yp"
{  if($_[1]) {
                                               # Append new value to the hash
                                               my ($k,$v);
					       while (  ($k,$v) = each(%{$_[1]}) ) {
						   if($k eq "email_") {
						       if(defined $groups{'_default_'}->{"email_work"}) {
							   $k = "email_home";
						       }
						       else {
							   $k = "email_work";
						       }
						   }
						   $groups{'_default_'}->{$k} = $v;
					       }
					   }
				       }
	],
	[#Rule 118
		 'item', 3,
sub
#line 110 "lib/Mioga2/Contact/vCard.yp"
{ if($_[3]) {
                                               # Append new value to the hash
                                               my ($k,$v);
					       while ( ($k,$v) = each(%{$_[3]}) ) {
						   if($k eq "email_") {
						       if(defined $groups{$_[1]}->{"email_work"}) {
							   $k = "email_home";
						       }
						       else {
							   $k = "email_work";
						       }
						   }
						   
						   $groups{$_[1]}->{$k} = $v;
					       }					       
		                           }  
		                         }
	],
	[#Rule 119
		 '@1-3', 0,
sub
#line 129 "lib/Mioga2/Contact/vCard.yp"
{ parse_value($_[2]->{ENCODING}); }
	],
	[#Rule 120
		 'item1', 6,
sub
#line 129 "lib/Mioga2/Contact/vCard.yp"
{ undef }
	],
	[#Rule 121
		 '@2-3', 0,
sub
#line 130 "lib/Mioga2/Contact/vCard.yp"
{ parse_value($_[2]->{ENCODING}); }
	],
	[#Rule 122
		 'item1', 6,
sub
#line 130 "lib/Mioga2/Contact/vCard.yp"
{ photo ($_[2], $_[5]);  }
	],
	[#Rule 123
		 '@3-3', 0,
sub
#line 131 "lib/Mioga2/Contact/vCard.yp"
{ parse_value($_[2]->{ENCODING}); }
	],
	[#Rule 124
		 'item1', 6,
sub
#line 131 "lib/Mioga2/Contact/vCard.yp"
{ undef }
	],
	[#Rule 125
		 '@4-3', 0,
sub
#line 132 "lib/Mioga2/Contact/vCard.yp"
{ parse_value($_[2]->{ENCODING}); }
	],
	[#Rule 126
		 'item1', 6,
sub
#line 132 "lib/Mioga2/Contact/vCard.yp"
{ title ($_[2], $_[5]); }
	],
	[#Rule 127
		 '@5-3', 0,
sub
#line 133 "lib/Mioga2/Contact/vCard.yp"
{ parse_value($_[2]->{ENCODING}); }
	],
	[#Rule 128
		 'item1', 6,
sub
#line 133 "lib/Mioga2/Contact/vCard.yp"
{ check_version($_[5]); }
	],
	[#Rule 129
		 '@6-3', 0,
sub
#line 134 "lib/Mioga2/Contact/vCard.yp"
{ parse_value($_[2]->{ENCODING}); }
	],
	[#Rule 130
		 'item1', 6,
sub
#line 134 "lib/Mioga2/Contact/vCard.yp"
{ telephone($_[2], $_[5]); }
	],
	[#Rule 131
		 '@7-3', 0,
sub
#line 135 "lib/Mioga2/Contact/vCard.yp"
{ parse_value($_[2]->{ENCODING}); }
	],
	[#Rule 132
		 'item1', 6,
sub
#line 135 "lib/Mioga2/Contact/vCard.yp"
{ email($_[2], $_[5]); }
	],
	[#Rule 133
		 '@8-3', 0,
sub
#line 136 "lib/Mioga2/Contact/vCard.yp"
{ parse_value($_[2]->{ENCODING}); }
	],
	[#Rule 134
		 'item1', 6,
sub
#line 136 "lib/Mioga2/Contact/vCard.yp"
{ note($_[2], $_[5]); }
	],
	[#Rule 135
		 '@9-3', 0,
sub
#line 137 "lib/Mioga2/Contact/vCard.yp"
{ parse_value($_[2]->{ENCODING}); }
	],
	[#Rule 136
		 'item1', 6,
sub
#line 137 "lib/Mioga2/Contact/vCard.yp"
{ url($_[2], $_[5]); }
	],
	[#Rule 137
		 'item1', 5,
sub
#line 138 "lib/Mioga2/Contact/vCard.yp"
{ address($_[2], $_[4]); }
	],
	[#Rule 138
		 'item1', 5,
sub
#line 139 "lib/Mioga2/Contact/vCard.yp"
{ organization($_[2], $_[4]); }
	],
	[#Rule 139
		 'item1', 5,
sub
#line 140 "lib/Mioga2/Contact/vCard.yp"
{ name($_[2], $_[4]); }
	],
	[#Rule 140
		 'item1', 6,
sub
#line 141 "lib/Mioga2/Contact/vCard.yp"
{ undef }
	],
	[#Rule 141
		 '@10-3', 0,
sub
#line 142 "lib/Mioga2/Contact/vCard.yp"
{ parse_value($_[2]->{ENCODING}); }
	],
	[#Rule 142
		 'item1', 6,
sub
#line 142 "lib/Mioga2/Contact/vCard.yp"
{ undef }
	],
	[#Rule 143
		 'ignoredname', 1, undef
	],
	[#Rule 144
		 'ignoredname', 1, undef
	],
	[#Rule 145
		 'ignoredname', 1, undef
	],
	[#Rule 146
		 'ignoredname', 1, undef
	],
	[#Rule 147
		 'ignoredname', 1, undef
	],
	[#Rule 148
		 'ignoredname', 1, undef
	],
	[#Rule 149
		 'ignoredname', 1, undef
	],
	[#Rule 150
		 'ignoredname', 1, undef
	],
	[#Rule 151
		 'ignoredname', 1, undef
	],
	[#Rule 152
		 'ignoredname', 1, undef
	],
	[#Rule 153
		 'ignoredname', 1, undef
	],
	[#Rule 154
		 'ignoredname', 2, undef
	],
	[#Rule 155
		 'value', 1, undef
	],
	[#Rule 156
		 'value', 1, undef
	],
	[#Rule 157
		 'value', 1, undef
	],
	[#Rule 158
		 'value', 1, undef
	],
	[#Rule 159
		 'params', 0,
sub
#line 153 "lib/Mioga2/Contact/vCard.yp"
{ {} }
	],
	[#Rule 160
		 'params', 3,
sub
#line 154 "lib/Mioga2/Contact/vCard.yp"
{  my %hash;
	                       for(my $i=0; $i < @{$_[3]}; $i+=2 ) {
				   push @{$hash{$_[3]->[$i]}}, $_[3]->[$i+1];
			       }
			       \%hash; 
			    }
	],
	[#Rule 161
		 'paramlist', 1,
sub
#line 162 "lib/Mioga2/Contact/vCard.yp"
{ $_[1] }
	],
	[#Rule 162
		 'paramlist', 5,
sub
#line 163 "lib/Mioga2/Contact/vCard.yp"
{ unshift (@{$_[1]},  @{$_[5]}) if defined $_[5];  $_[1]}
	],
	[#Rule 163
		 'param', 5,
sub
#line 166 "lib/Mioga2/Contact/vCard.yp"
{ [uc($_[1]), uc($_[5])]  }
	],
	[#Rule 164
		 'param', 5,
sub
#line 167 "lib/Mioga2/Contact/vCard.yp"
{ [uc($_[1]), uc($_[5])]  }
	],
	[#Rule 165
		 'param', 5,
sub
#line 168 "lib/Mioga2/Contact/vCard.yp"
{ [uc($_[1]), uc($_[5])]  }
	],
	[#Rule 166
		 'param', 5,
sub
#line 169 "lib/Mioga2/Contact/vCard.yp"
{ [uc($_[1]), uc($_[5])]  }
	],
	[#Rule 167
		 'param', 5,
sub
#line 170 "lib/Mioga2/Contact/vCard.yp"
{ [uc($_[1]), uc($_[5])]  }
	],
	[#Rule 168
		 'param', 6,
sub
#line 171 "lib/Mioga2/Contact/vCard.yp"
{ undef }
	],
	[#Rule 169
		 'param', 1,
sub
#line 172 "lib/Mioga2/Contact/vCard.yp"
{ ["TYPE", $_[1]] }
	],
	[#Rule 170
		 'ptypeval', 1, undef
	],
	[#Rule 171
		 'ptypeval', 2, undef
	],
	[#Rule 172
		 'pvalueval', 1, undef
	],
	[#Rule 173
		 'pvalueval', 1, undef
	],
	[#Rule 174
		 'pvalueval', 1, undef
	],
	[#Rule 175
		 'pvalueval', 1, undef
	],
	[#Rule 176
		 'pvalueval', 2, undef
	],
	[#Rule 177
		 'pencodingval', 1, undef
	],
	[#Rule 178
		 'pencodingval', 1, undef
	],
	[#Rule 179
		 'pencodingval', 1, undef
	],
	[#Rule 180
		 'pencodingval', 1, undef
	],
	[#Rule 181
		 'pencodingval', 2, undef
	],
	[#Rule 182
		 'charsetval', 1, undef
	],
	[#Rule 183
		 'langval', 1, undef
	],
	[#Rule 184
		 'addressparts', 1,
sub
#line 191 "lib/Mioga2/Contact/vCard.yp"
{ {"address", $_[1]} }
	],
	[#Rule 185
		 'addressparts', 3,
sub
#line 192 "lib/Mioga2/Contact/vCard.yp"
{ {"address", $_[1], "address2", $_[3] } }
	],
	[#Rule 186
		 'addressparts', 5,
sub
#line 193 "lib/Mioga2/Contact/vCard.yp"
{ {"address", $_[1], "address2", $_[3], "street", $_[5] } }
	],
	[#Rule 187
		 'addressparts', 7,
sub
#line 194 "lib/Mioga2/Contact/vCard.yp"
{ {"address", $_[1], "address2", $_[3], "street", $_[5], "city", $_[7] } }
	],
	[#Rule 188
		 'addressparts', 9,
sub
#line 195 "lib/Mioga2/Contact/vCard.yp"
{ {"address", $_[1], "address2", $_[3], "street", $_[5], 
											      "city", $_[7], "state", $_[9] } }
	],
	[#Rule 189
		 'addressparts', 11,
sub
#line 197 "lib/Mioga2/Contact/vCard.yp"
{ {"address", $_[1], "address2", $_[3], "street", $_[5], 
											      "city", $_[7], "state", $_[9], "postalcode", $_[11] } }
	],
	[#Rule 190
		 'addressparts', 13,
sub
#line 199 "lib/Mioga2/Contact/vCard.yp"
{ {"address", $_[1], "address2", $_[3], "street", $_[5], 
													    "city", $_[7], "state", $_[9], "postalcode", $_[11], "country", $_[13] } }
	],
	[#Rule 191
		 'orgparts', 1,
sub
#line 203 "lib/Mioga2/Contact/vCard.yp"
{[$_[1]]}
	],
	[#Rule 192
		 'orgparts', 3,
sub
#line 204 "lib/Mioga2/Contact/vCard.yp"
{ unshift (@{$_[3]}, $_[1]); $_[3] }
	],
	[#Rule 193
		 'nameparts', 1,
sub
#line 207 "lib/Mioga2/Contact/vCard.yp"
{ {"family", $_[1]} }
	],
	[#Rule 194
		 'nameparts', 3,
sub
#line 208 "lib/Mioga2/Contact/vCard.yp"
{ {"family", $_[1], "first", $_[3]} }
	],
	[#Rule 195
		 'nameparts', 5,
sub
#line 209 "lib/Mioga2/Contact/vCard.yp"
{ {"family", $_[1], "first", $_[3], "middle", $_[5]}  }
	],
	[#Rule 196
		 'nameparts', 7,
sub
#line 210 "lib/Mioga2/Contact/vCard.yp"
{ {"family", $_[1], "first", $_[3], "middle", $_[5], "prefix", $_[7]}}
	],
	[#Rule 197
		 'nameparts', 9,
sub
#line 211 "lib/Mioga2/Contact/vCard.yp"
{ {"family", $_[1], "first", $_[3], "middle", $_[5], "prefix", $_[7], "suffix", $_[9]} }
	],
	[#Rule 198
		 'strnosemi', 0,
sub
#line 214 "lib/Mioga2/Contact/vCard.yp"
{ '' }
	],
	[#Rule 199
		 'strnosemi', 2,
sub
#line 215 "lib/Mioga2/Contact/vCard.yp"
{ $_[1].$_[2] }
	],
	[#Rule 200
		 'strnosemi', 3,
sub
#line 216 "lib/Mioga2/Contact/vCard.yp"
{ $_[1].$_[2].$_[3] }
	],
	[#Rule 201
		 'strnosemi', 3,
sub
#line 217 "lib/Mioga2/Contact/vCard.yp"
{ $_[1].$_[2].$_[3] }
	],
	[#Rule 202
		 'strnosemi', 3,
sub
#line 218 "lib/Mioga2/Contact/vCard.yp"
{ $_[1].$_[3] }
	],
	[#Rule 203
		 'nonsemi', 1, undef
	],
	[#Rule 204
		 'nonsemi', 1, undef
	],
	[#Rule 205
		 'nonsemi', 1, undef
	],
	[#Rule 206
		 'nonsemi', 1, undef
	],
	[#Rule 207
		 'nonsemi', 1, undef
	],
	[#Rule 208
		 'nonsemi', 1, undef
	],
	[#Rule 209
		 'nonsemi', 1, undef
	],
	[#Rule 210
		 'knowntype', 1, undef
	],
	[#Rule 211
		 'knowntype', 1, undef
	],
	[#Rule 212
		 'knowntype', 1, undef
	],
	[#Rule 213
		 'knowntype', 1, undef
	],
	[#Rule 214
		 'knowntype', 1, undef
	],
	[#Rule 215
		 'knowntype', 1, undef
	],
	[#Rule 216
		 'knowntype', 1, undef
	],
	[#Rule 217
		 'knowntype', 1, undef
	],
	[#Rule 218
		 'knowntype', 1, undef
	],
	[#Rule 219
		 'knowntype', 1, undef
	],
	[#Rule 220
		 'knowntype', 1, undef
	],
	[#Rule 221
		 'knowntype', 1, undef
	],
	[#Rule 222
		 'knowntype', 1, undef
	],
	[#Rule 223
		 'knowntype', 1, undef
	],
	[#Rule 224
		 'knowntype', 1, undef
	],
	[#Rule 225
		 'knowntype', 1, undef
	],
	[#Rule 226
		 'knowntype', 1, undef
	],
	[#Rule 227
		 'knowntype', 1, undef
	],
	[#Rule 228
		 'knowntype', 1, undef
	],
	[#Rule 229
		 'knowntype', 1, undef
	],
	[#Rule 230
		 'knowntype', 1, undef
	],
	[#Rule 231
		 'knowntype', 1, undef
	],
	[#Rule 232
		 'knowntype', 1, undef
	],
	[#Rule 233
		 'knowntype', 1, undef
	],
	[#Rule 234
		 'knowntype', 1, undef
	],
	[#Rule 235
		 'knowntype', 1, undef
	],
	[#Rule 236
		 'knowntype', 1, undef
	],
	[#Rule 237
		 'knowntype', 1, undef
	],
	[#Rule 238
		 'knowntype', 1, undef
	],
	[#Rule 239
		 'knowntype', 1, undef
	],
	[#Rule 240
		 'knowntype', 1, undef
	],
	[#Rule 241
		 'knowntype', 1, undef
	],
	[#Rule 242
		 'knowntype', 1, undef
	],
	[#Rule 243
		 'knowntype', 1, undef
	],
	[#Rule 244
		 'knowntype', 1, undef
	],
	[#Rule 245
		 'knowntype', 1, undef
	],
	[#Rule 246
		 'knowntype', 1, undef
	],
	[#Rule 247
		 'knowntype', 1, undef
	],
	[#Rule 248
		 'knowntype', 1, undef
	],
	[#Rule 249
		 'knowntype', 1, undef
	],
	[#Rule 250
		 'knowntype', 1, undef
	],
	[#Rule 251
		 'knowntype', 1, undef
	],
	[#Rule 252
		 'knowntype', 1, undef
	],
	[#Rule 253
		 'knowntype', 1, undef
	],
	[#Rule 254
		 'knowntype', 1, undef
	],
	[#Rule 255
		 'knowntype', 1, undef
	],
	[#Rule 256
		 'knowntype', 1, undef
	],
	[#Rule 257
		 'knowntype', 1, undef
	],
	[#Rule 258
		 'knowntype', 1, undef
	],
	[#Rule 259
		 'knowntype', 1, undef
	]
],
                                  @_);
    bless($self,$class);
}

#line 239 "lib/Mioga2/Contact/vCard.yp"



# ----------------------------------------------------------------------------
# parse_value 
# ----------------------------------------------------------------------------
sub parse_value {
    my $encoding = shift;

    # In vcard specification, default is 7BIT. But Outlook print 8BIT  
    # data without specifying ENCODING=8BIT. So, we lokk for 8BIT data by 
    # default to be compatible with outlook vCard.

    $value_encoding = (defined $encoding->[0] ? $encoding->[0] : "8BIT");
}


# ----------------------------------------------------------------------------
# _Error is the function run by yapp when an error occures
# ----------------------------------------------------------------------------
sub _Error {
    my $parser = shift;

    my $errormsg = "";
    if(exists $parser->YYData->{ERRMSG}) {
	 $errormsg = $parser->YYData->{ERRMSG};
    }

    warn $parser->YYData->{INPUT};

    throw Mioga2::Exception::Simple("vCard parser", "Syntax error. $errormsg");
}


# ----------------------------------------------------------------------------
# _Lexer is the function run by yapp when a new token is needed
# ----------------------------------------------------------------------------
sub _Lexer {
    my($parser)=shift;

        $parser->YYData->{INPUT} ne ""
    or  return('',undef);

    my @terminal_list= ('BEGIN', 'VCARD', 'END', 'PS', 'MAILER', 'AIFF', 'POSTAL', 'CAR', 'PGP', 'DOM', 'UID', 'NICKNAME',
			'JPEG', 'PDF', 'CHARSET', 'X-', 'BDAY', 'POWERSHARE', 'PREF', 'PRODIGY', 'HOME', 'PCM',
			'GIF', 'INTERNET', 'REV', 'URL', 'APPLELINK', 'TLX', 'MODEM', 'EWORLD', 'CELL', 'CONTENT-ID',
			'AOL', 'N', 'ISDN', 'FAX', 'TYPE', 'KEY', 'TIFF', 'BMP', 'MCIMAIL', 'VERSION', 'ADR', 'LOGO', 
			'VALUE', 'X509', 'TITLE', 'VOICE', 'CGM', 'MSG', 'X400', 'PARCEL', 'QUOTED-PRINTABLE', 'INTL', 
			'ATTMAIL', 'GEO', '8BIT', 'CIS', 'PICT', 'DIB', 'LABEL', 'CID', 'BBS', 'SOUND', 'TEL', 'LANGUAGE',
			'MPEG2', 'ORG', 'VIDEO', 'AGENT', 'INLINE', 'WORK', 'MPEG', 'BASE64', 'WAVE', 'ROLE', 
			'WMF', 'PHOTO', 'QTIME', 'NOTE', 'AVI', 'IBMMAIL', 'FN', 'PAGER', 'TZ', '7BIT', 'EMAIL', 
			'ENCODING', 'PMB', 'MET' );


    if(defined $value_encoding) {
	my $val_type = uc($value_encoding)."_VAL";
	my $value;
	
	if($value_encoding eq "7BIT") {
	    $parser->YYData->{INPUT} =~ s/^([\x09\x20-\x7E]+)//;
	    $value = $1;
	}
	elsif($value_encoding eq "8BIT") {
	    $parser->YYData->{INPUT} =~ s/^([\x09\x20-\x7E\x80-\xFF]+)//;
	    $value = $1;
	}
	elsif($value_encoding eq "BASE64") {
	    $parser->YYData->{INPUT} =~ s/^((((\x0D)?\x0A)?(\x20)+)?([A-Za-z0-9\+\/\=]+((\x0D)?\x0A(\x20)+)?)*)//;
	    $value = $1;
	}
	elsif($value_encoding eq "QUOTED-PRINTABLE") {
	    $parser->YYData->{INPUT} =~ s/^((([\x21-\x3C\x3E-\x7E\x20\x09]*(\=[0-9A-F][0-9A-F])*)*(\=(\x0D)?\x0A)?)+)//;
	    $value = $1;
	}

	undef $value_encoding;
	return ($val_type, $value);
    }


    # look for separator
    if($parser->YYData->{INPUT}=~s/^\\\;//){
	return ('ESCAPEDSC', $1);
    }
    if($parser->YYData->{INPUT}=~s/^\\(\x0D\x0A)//){
	return ('ESCAPECRLF', $1);
    }
    if($parser->YYData->{INPUT}=~s/^\\(\x0A)//){
	return ('ESCAPECRLF', $1);
    }
    if($parser->YYData->{INPUT}=~s/^=(\x0D\x0A)//){
	return ('EQUALCRLF', $1);
    }
    if($parser->YYData->{INPUT}=~s/^=(\x0A)//){
	return ('EQUALCRLF', $1);
    }
    if($parser->YYData->{INPUT}=~s/^(\x0D\x0A)//){
	return ('CRLF', $1);
    }
    if($parser->YYData->{INPUT}=~s/^(\x0A)//){
	return ('CRLF', $1);
    }
    elsif($parser->YYData->{INPUT}=~s/^(\x20)//){
	return ('SPACE', $1);
    }
    elsif($parser->YYData->{INPUT}=~s/^(\x09)//){
	return ('HTAB', $1);
    }
    
    # look for special characters
    if($parser->YYData->{INPUT}=~s/^([;:=\.])//) {
	return ($1, $1);
    }

    #look for a 'key word'
    $parser->YYData->{INPUT}=~/^([A-Za-z0-9][A-Za-z0-9\-]*)/;
    my $word = $1;
    if(defined $word) {
	my $ucword = uc($word);
	foreach my $term (@terminal_list) {
	    if($term eq $ucword){
		$parser->YYData->{INPUT}=~s/^$word//;
		return ($ucword, $word);
	    }
	    elsif($ucword =~ /^X\-.*/) {
		$parser->YYData->{INPUT}=~s/X\-//i;
		return ("X-", "X-");
	    }
	}
    }    


    # look for word
    if ($parser->YYData->{INPUT}=~s/^([\x21-\x2B\x2D\x2F-\x39\x3C\x3E-\x5A\x5C\x5E-\x7E]+)//) {
	return ('WORD', $1);
    }
    
    if($parser->YYData->{INPUT} =~ s/^([^\x00-\x20\x3B\x80-\xFF]+)//) {
	return ('NCNSASCII', $1);	
    }

    if($parser->YYData->{INPUT}=~s/^([^\x00-\x20\x3B]+)//) {
	return ('ASCII8', $1);	
    }
    
    return ('ERROR','ERROR');
}

# ----------------------------------------------------------------------------
# Run : parse vcard data 
# return : a hash ref representing the vCard
# ----------------------------------------------------------------------------
sub Run {
    my($self, $vcard_data) = @_;

    ## Do little subsitute in data to correct non-standard vCard
    $vcard_data =~ s/\;quoted\-printable([:\;])/;ENCODING=QUOTED-PRINTABLE$1/gi;
    $vcard_data =~ s/\;base64([:\;])/;ENCODING=BASE64$1/gi;

    my $old_dolbs = $/;
    undef $/;
    
    $self->YYData->{INPUT} = $vcard_data;
    my $res = $self->YYParse( yylex => \&_Lexer, yyerror => \&_Error );

    $/=$old_dolbs;

    return $res;
}


########## PRIVATE METHODS ##############

use MIME::Base64;
use MIME::QuotedPrint;
use POSIX qw(tmpnam);
use Unicode::String qw(utf8 latin1);

# ----------------------------------------------------------------------------
# name is run when the N property is parsed in the vCard file
# return : ("name", <contact_name>, "surname", <contact_surname(s)>) 
# ----------------------------------------------------------------------------
sub check_version {
    my ($version)=@_;

    if($version > 2.1) {  
	$_[0]->YYData->{ERRMSG} =  
	    "The vcard version is $_[4].".
		"Versions > 2.1 is not supported" ;
	$_[0]->YYError; 
    }
}

# ----------------------------------------------------------------------------
# name is run when the N property is parsed in the vCard file
# return : ("name", <contact_name>, "surname", <contact_surname(s)>) 
# ----------------------------------------------------------------------------
sub name {
    my ($params_hash, $name_hash)=@_;

    my $name = decode_string($name_hash->{family}, $params_hash);
    
    my $surname =  decode_string($name_hash->{first}, $params_hash);
    if(defined $name_hash->{middle}) { 
        $surname .= " ".decode_string($name_hash->{middle}, $params_hash);
    }

    return {"name", $name, "surname", $surname};
}

# ----------------------------------------------------------------------------
# url is run when the URL property is parsed in the vCard file
# return : ("url", <url>) 
# ----------------------------------------------------------------------------
sub url {
    my ($params_hash, $url) = @_;

    return {"url", decode_string($url, $params_hash)};
}

# ----------------------------------------------------------------------------
# note is run when the NOTE property is parsed in the vCard file
# return : ("comment", <comments>) 
# ----------------------------------------------------------------------------
sub note {
    my ($params_hash, $note) = @_;

    return {"comment", decode_string($note, $params_hash)};
}


# ----------------------------------------------------------------------------
# title is run when the TITLE property is parsed in the vCard file
# return : ("title", <title>) 
# ----------------------------------------------------------------------------
sub title {
    my ( $params_hash, $title) = @_;
    
    return {"title", decode_string($title, $params_hash)};
}

# ----------------------------------------------------------------------------
# email is run when the EMAIL property is parsed in the vCard file
# return : ("email_<loc>", <email>) 
#          with <loc> = work or home 
# ----------------------------------------------------------------------------
sub email {
    my ($params_hash, $email_data)=@_;

    my $type_list = $params_hash->{"TYPE"};

    my $type="";
    foreach my $item (@$type_list) {
        $item = uc($item);

        if($item eq "WORK") {
            $type = "work";
        }
        elsif($item eq "HOME") {
            $type = "home";
        }
    }

    return {"email_$type", decode_string($email_data, $params_hash)};
}

# ----------------------------------------------------------------------------
# telephone is run when the TEL property is parsed in the vCard file
# return : ("tel_<type>", <tel nbr>) 
#          with <type> = work, home, mobile or fax (others telephone types are ignored) 
# ----------------------------------------------------------------------------
sub telephone {
    my ($params_hash, $telephone_data)=@_;

    my $type_list = $params_hash->{"TYPE"};

    my $type="";
    foreach my $item (@$type_list) {
        $item = uc($item);

        if($item eq "WORK") {
            $type = "work";
        }
        elsif($item eq "HOME") {
            $type = "home";
        }
        elsif($item eq "CELL") {
            $type = "mobile";
        }
        elsif($item eq "FAX") {
            $type = "fax";
        }
    }

    if($type eq "") {
        $type = "work";
    }

    return {"tel_$type", decode_string($telephone_data, $params_hash)};
}

# ----------------------------------------------------------------------------
# address is run when the ADR property is parsed in the vCard file
# return : ("addr_<loc>", <address>, "city_<loc>", <city>, 
#           "postal_code_<loc>", <postal code>, "state_<loc>", <state>,
#           "country_<loc>", <country>) 
#          with <loc> = work or home (others address types are ignored) 
# ----------------------------------------------------------------------------
sub address {
    my ($params_hash, $addr_data_hash)=@_;
    my %res;

    foreach my $key (keys %$addr_data_hash) {
        $addr_data_hash->{$key} = decode_string($addr_data_hash->{$key}, $params_hash);
    }

    my $type_list = $params_hash->{"TYPE"};
    my $loc = "";
    foreach my $item (@$type_list) {
        $item = uc($item);

        if($item eq "WORK") {
            $loc = "work";
        }
        elsif($item eq "HOME") {
            $loc = "home";
        }
    }

    $loc = "work" if $loc eq "";
    
    if(defined $addr_data_hash->{"address"}) {
        $res{"addr_$loc"} = $addr_data_hash->{"address"};
    }

    if(defined $addr_data_hash->{"address2"}) {
        $res{"addr_$loc"} .=" \n".$addr_data_hash->{"address2"};
    }
    
    if(defined $addr_data_hash->{"street"}) {
        $res{"addr_$loc"} .= " ".$addr_data_hash->{"street"};
    }
    
    if(defined $addr_data_hash->{"city"}) {
        $res{"city_$loc"} = $addr_data_hash->{"city"};
    }
    
    if(defined $addr_data_hash->{"state"}) {
        $res{"state_$loc"} = $addr_data_hash->{"state"};
    }

    if(defined $addr_data_hash->{"postalcode"}) {
        $res{"postal_code_$loc"} = $addr_data_hash->{"postalcode"};
    }
    
    if(defined $addr_data_hash->{"country"}) {
        $res{"country_$loc"} = $addr_data_hash->{"country"};
    }
    
    return \%res;

}

# ----------------------------------------------------------------------------
# organization is run when the N property is parsed in the vCard file
# return : ("name", <contact_name>, "surname", <contact_surname(s)>) 
# ----------------------------------------------------------------------------
sub organization {
    my ($params_hash, $org_data_list)=@_;

    my $data = "";
    foreach my $ou (@$org_data_list) {
        $data .= " ; " if $data ne "";
        $data .= decode_string($ou, $params_hash);
    }

    return {"orgname", $data};
}


# ----------------------------------------------------------------------------
# photo is run when the PHOTO property is parsed in the vCard file
# return : ("photo_data", <jpeg_temp_file_name>), ("photo_url", <url>) or undef
# ----------------------------------------------------------------------------
sub photo {
    my ($params_hash, $photo_data) =@_;

    my $value;
    my $encoding = "7BIT";
    
    if(defined $params_hash->{"ENCODING"}) {
        $encoding = $params_hash->{"ENCODING"}->[0];
    }
    
    if(!defined $params_hash->{"VALUE"}) {
        $value = "INLINE";
    }
    else {
        $value = $params_hash->{"VALUE"}->[0];
    }

    if($value eq "URL") {
        decode_string($photo_data, $params_hash);
        return {"photo_url", $photo_data};
    }
    elsif($value =~ /X\-.*/) {
        return undef;
    }
    elsif($value eq "CID" 
          or $value eq "CONTENT-ID" ) {
        warn "Separated MIME entity is not yet supported. PHOTO is ignored";
        return undef;
    }
    
    if($encoding ne "BASE64") {
        warn "Encoding type \"$encoding\" is not supported for inline photo";
        return undef;
    }

    ## Decode base64 data, store it in a temporary file and eventualy convert 
    ## the picture in JPG (using "convert")
    my $type;
    if(!defined $params_hash->{"TYPE"}) {
        $type = "JPG";
    }
    else {
        $type = $params_hash->{"TYPE"}->[0];
    }
    
    if($type eq "JPG" or
       $type eq "JPEG" or
       $type eq "GIF" or
       $type eq "PNG") {
	    # look for the "convert" binary path
	    my $decoded = decode_base64($photo_data);
    
	    my $filename = tmpnam;
	    
	    open(PIC_FILE, ">$filename")
		or throw Mioga2::Exception::Simple("Mioga2::Contact::vCard", "Error can not open $filename for writing");
	    print PIC_FILE $decoded;
	    close(PIC_FILE);

	    return {"photo_data", $filename};
    }
    
    return undef;
}


# ----------------------------------------------------------------------------
# decode_string decode quoted-printable and base64 and convert strings to latin1 
# ----------------------------------------------------------------------------
sub decode_string {
    my ($data, $params_hash) = @_;

    my $encoding;
    my $charset;

    if(!exists $params_hash->{"ENCODING"}) {
        $encoding = "7BIT";
    }
    else {
        $encoding = $params_hash->{"ENCODING"}->[0];
    }

    if(!defined $params_hash->{"CHARSET"}) {
        $charset = "us-ascii";
    }
    else {
        $charset = $params_hash->{"CHARSET"}->[0];    
    }
    

    $charset = lc($charset);

    if($encoding eq "BASE64"){
        $data = decode_base64($data);
    }
    elsif($encoding eq "QUOTED-PRINTABLE") {
        $data = decode_qp($data);
    }
    
    # Convert unicode to latin1
    if($charset eq "utf-8") {
        #$data = utf8($data)->latin1;
    }
    elsif($charset !~ /^us-ascii$/ && $charset !~ /^iso-8859-1(5)?$/) {
        warn "Unsupported charset $charset, convert 8bit characters to _";
        $data =~ s/[\x80-\xFF]/_/g;
    }
    
    return $data;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Contact

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__

1;
