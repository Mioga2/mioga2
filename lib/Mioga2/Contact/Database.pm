# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
# Description:

=head1 NAME

Mioga2::Contact - The Mioga2 address book application

=head1 DESCRIPTION

=cut

package Mioga2::Contact;
use strict;

use vars qw(@ISA);

use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Mioga2::Contact::vCard;
use Mioga2::Synchro;
use MIME::QuotedPrint;
use MIME::Base64;
use Error qw(:try);
use POSIX qw(tmpnam);


# ----------------------------------------------------------------------------

=head2 ContactDBGetContact

ContactDBGetContact return information for contact $ctc_id

The return value is a hashref like ContactDBAdd $params attributes

=cut

# ----------------------------------------------------------------------------
sub ContactDBGetContact {
    my ($self, $config, $ctc_id) = @_;

	my $dbh = $config->GetDBH();

    return SelectSingle($dbh, "SELECT * FROM contacts WHERE rowid=$ctc_id");
}



# ----------------------------------------------------------------------------

=head2 ContactDBAdd

ContactDBAdd create a new contact for group $grp_id.

Contact parameters are passed in the $params hash ref :

Available fields are :
    name          : last name (required)
    surname       : first name
    photo_data    : JPEG data of contact's photo
    photo_url     : the photo's URL
    title         : title
    orgname       : the organisation name

    email_work    : professional email
    email_home    : personal email

    tel_work      : professional phone number
    tel_home      : personal phone number
    tel_mobile    : GSM phone number
    tel_fax       : fax number

    addr_home     : personal address
    city_home     : personal city
    postal_code_home : personal postal code
    state_home    : personal state
    country_home  : personal country

    addr_work     : professional address
    city_work     : professional city   
    postal_code_work : professional postal code :
    state_work    : professional state  
    country_work  : professional country

    url           : organisation web site URL

    comment       : comments

=cut

# ----------------------------------------------------------------------------
sub ContactDBAdd
{
    my ($self, $config, $grp_id, $params) = @_;

	my $dbh = $config->GetDBH();
    
    my (@varlist, @valuelist);
	push @varlist, "created";

    if(defined $params->{created}) {
        push @valuelist, "'$params->{created}'";
    }
    else {
        push @valuelist, "now()";
    }

    push @varlist, "modified";
    if(defined $params->{modified}) {
        push @valuelist, "'$params->{modified}'";
    }
    else {
        push @valuelist, "now()";
    }

    push @varlist, "group_id";
    push @valuelist, "$grp_id";



 	my $rowid;
    try {
		BeginTransaction($dbh);

		$rowid = SelectSingle($dbh, "SELECT nextval('contacts_rowid_seq') AS rowid");
		
		push @varlist, 'rowid';
		push @valuelist, $rowid->{rowid};


		foreach my $var ("name", "surname", "photo_data", "photo_url", "title",
						 "orgname", "email_work", "email_home", "tel_work",
						 "tel_home", "tel_mobile", "tel_fax", "addr_home",
						 "city_home", "postal_code_home", "state_home", 
						 "country_home", "addr_work", "city_work", "postal_code_work",
						 "state_work", "country_work", "url", "comment") {        
			
			if(defined $params->{$var} and $params->{$var} ne "") {            
				
				push @varlist, $var;
				
				if($var eq "photo_data" and -f $params->{$var}) {
					# Stupid and not usefull request but required my lo_import
					ExecSQL($dbh, "SELECT * FROM contacts WHERE rowid = $rowid->{rowid}");
					
					my $oid = $dbh->func($params->{$var}, 'lo_import');

					if(!defined $oid) {
						throw Mioga2::Exception::Simple("Mioga2::Contact->ContactDBAdd",
                        __x("Can't import {var} into database", var => $params->{$var}));
					}
					
					push @valuelist, $oid;
				}
				else {
					$params->{$var} = st_FormatPostgreSQLString($params->{$var});
					push @valuelist, "'".$params->{$var}."'";
				}
			}
		}
		
		
		my $sql = "INSERT INTO contacts".
                  "(".join(", ", @varlist).   ") ".
				  "VALUES (".join(",", @valuelist).")";

        ExecSQL($dbh, $sql);

		EndTransaction($dbh);
    }
    otherwise {
		my $err = shift;

        RollbackTransaction($dbh);

		$err->throw;
    };

    
    return $rowid->{rowid};
}



# ----------------------------------------------------------------------------

=head2 ContactDBModify

ContactDBModify modify the contact $rowid

Contact parameters are passed in the $params hash ref : 
see ContactDBAdd $params fields' list.

=cut

# ---------------------------------------------------------------------------'
sub ContactDBModify
{
    my ($self, $config, $rowid, $params) = @_;

	my $dbh = $config->GetDBH();
      
    my @updtlist;
    if(defined $params->{modified}) {
        push @updtlist, "modified = '".$params->{modified}."'";
    }
    else {
        push @updtlist, "modified = now()";
    }


	try {
		BeginTransaction($dbh);

		if(exists $params->{"photo_data"} or exists $params->{"photo_delete"}) {
			my $curctc = SelectSingle($dbh, "SELECT * FROM contacts WHERE rowid=$rowid");
			if(defined $curctc->{photo_data}) {
				$dbh->func($curctc->{photo_data}, "lo_unlink");
			}
		}

		foreach my $var ("name", "surname", "photo_data", "photo_url", "photo_delete", "title",
						 "orgname", "email_work", "email_home", "tel_work",
						 "tel_home", "tel_mobile", "tel_fax", "addr_home",
						 "city_home", "postal_code_home", "state_home", 
						 "country_home", "addr_work", "city_work", "postal_code_work",
						 "state_work", "country_work", "url", "comment") {        
			
			if(defined $params->{$var}) {
				if($var eq "photo_data") {
					my $oid = $dbh->func($params->{$var}, "lo_import");
					
					if(!defined $oid) {
						throw Mioga2::XML::Simple("Mioga2::Contact->ContactDBModify",
                        __x("Can't import {var} into database", var => $params-{$var}));
					}
					
					push @updtlist, "$var = $oid";
				}
				elsif($var eq "photo_delete") {
					push @updtlist, "photo_data = NULL";
				}
				else {
					$params->{$var} = st_FormatPostgreSQLString($params->{$var});
					push @updtlist, "$var = "."'".$params->{$var}."'";
				}
			}
		}

		my $sql = "UPDATE contacts SET ".join(",", @updtlist)." WHERE rowid = $rowid";
		ExecSQL($dbh, $sql);

		EndTransaction($dbh);
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};
		
    return $rowid;
}



# ----------------------------------------------------------------------------

=head2 ContactDBRemove

ContactDBRemove remove the contact $rowid from database.

=cut

# ---------------------------------------------------------------------------'

sub ContactDBRemove {
    my ($self, $config, $rowid) = @_;

	my $dbh = $config->GetDBH();
           
    ExecSQL($dbh, "DELETE FROM contacts WHERE rowid = $rowid");
}



# ----------------------------------------------------------------------------

=head2 ContactDBGetPhoto

ContactDBGetPhoto export the phto with oid $oid in $filename 

If $filename is not specified a new file will be created in /tmp.

Return the file name

=cut

# ---------------------------------------------------------------------------'
sub ContactDBGetPhoto 
{
    my ($self, $config, $oid, $filename) = @_;

	my $dbh = $config->GetDBH();

    if(!defined $filename) {
        $filename = tmpnam;
    }

	try {
		BeginTransaction($dbh);
		
		# Another stupid request for lo_*
		ExecSQL($dbh, "SELECT * FROM contacts WHERE photo_data = $oid");
		
		$dbh->func($oid, $filename, "lo_export");

		EndTransaction($dbh);
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw; 
	};

    return $filename;
}



# ----------------------------------------------------------------------------

=head2 ContactDBGetNubmerForGroup

ContactDBGetNumberForGroup return the number of contact for group $grp_id

=cut

# ---------------------------------------------------------------------------'
sub ContactDBGetNumberForGroup {
    my ($self, $config, $grp_id) = @_;

	my $dbh = $config->GetDBH();

    my $res;

    my $sql = "SELECT count(*) FROM contacts WHERE group_id=$grp_id";
    $res = SelectSingle($dbh, $sql);

    return $res->{count};
}



# ----------------------------------------------------------------------------

=head2 ContactDBGetNumberMatchingForGroup

ContactDBGetNumberMatchingForGroup return the number of contact matching regexps
for group $grp_id

$params is an hashref with ContactDBAdd keys and regexp's list as values

=cut

# ---------------------------------------------------------------------------'
sub ContactDBGetNumberMatchingForGroup {
    my ($self, $config, $grp_id, $params) = @_;

	my $dbh = $config->GetDBH();

    my @regexp_list = ("group_id = $grp_id");

    foreach my $var ("name", "surname", "photo_url", "title",
		     "orgname", "email_work", "email_home", "tel_work",
		     "tel_home", "tel_mobile", "tel_fax", "addr_home",
		     "city_home", "postal_code_home", "state_home", 
		     "country_home", "addr_work", "city_work", "postal_code_work",
		     "state_work", "country_work", "url", "comment") {
            
            if(defined $params->{$var}) {
		    my @var_reg_exp_list;
		    
		    foreach my $regexp (@{$params->{$var}}) {
                push @var_reg_exp_list, st_NoAccent($var)." ~* '$regexp'";
		    }            
		    
		    
		    push @regexp_list, "(".join(" OR ", @var_reg_exp_list).")";
		    
            }
        
        }
        
    my $sql = "SELECT count(*) FROM contacts WHERE ".join(" AND ", @regexp_list);        
    my $res = SelectSingle($dbh, $sql);

    return $res->{count};
}


# ----------------------------------------------------------------------------

=head2 ContactDBGetNumberMatching

ContactDBGetNumberMatching return the number of contact matching regexps
for groups $grp_id_list

$params is an hashref with ContactDBAdd keys and regexp's list as values

=cut

# ---------------------------------------------------------------------------'
sub ContactDBGetNumberMatching {
    my ($self, $config, $grp_id_list, $params) = @_;

	my $dbh = $config->GetDBH();

    my @regexp_list = ("group_id IN (".join(", ", @$grp_id_list).")");

    foreach my $var ("name", "surname", "photo_url", "title",
		     "orgname", "email_work", "email_home", "tel_work",
		     "tel_home", "tel_mobile", "tel_fax", "addr_home",
		     "city_home", "postal_code_home", "state_home", 
		     "country_home", "addr_work", "city_work", "postal_code_work",
		     "state_work", "country_work", "url", "comment") {
            
            if(defined $params->{$var}) {
		    my @var_reg_exp_list;
		    
		    foreach my $regexp (@{$params->{$var}}) {
                push @var_reg_exp_list, st_NoAccent($var)." ~* '$regexp'";
		    }            
		    
		    
		    push @regexp_list, "(".join(" OR ", @var_reg_exp_list).")";
		    
            }
        
        }
        
    my $sql = "SELECT count(*) FROM contacts WHERE ".join(" AND ", @regexp_list);        
    
    my $res = SelectSingle($dbh, $sql);

    return $res->{count};
}



# ----------------------------------------------------------------------------

=head2 ContactDBGetMatchingForGroupWithOffset

ContactDBGetMatchingForGroupWithOffset return the list of contact for group $grp_id
with offset and limit

=cut

# ---------------------------------------------------------------------------'
sub ContactDBGetMatchingForGroupWithOffset {
    my ($self, $config, $grp_id, $offset, $limit, $sort_field_name, $asc, $params) = @_;

	my $dbh = $config->GetDBH();

    my @regexp_list = ("group_id = $grp_id");

    foreach my $var ("name", "surname", "photo_url", "title",
                     "orgname", "email_work", "email_home", "tel_work",
                     "tel_home", "tel_mobile", "tel_fax", "addr_home",
                     "city_home", "postal_code_home", "state_home", 
                     "country_home", "addr_work", "city_work", "postal_code_work",
                     "state_work", "country_work", "url", "comment") {
        
        if(defined $params->{$var}) {
            my @var_reg_exp_list;
            
            foreach my $regexp (@{$params->{$var}}) {
                push @var_reg_exp_list, st_NoAccent($var)." ~* '$regexp'";
            }            
            
            
            push @regexp_list, "(".join(" OR ", @var_reg_exp_list).")";
            
        }
        
    }
    
    my $sql = "SELECT * FROM contacts WHERE ".join(" AND ", @regexp_list);
    $sql .=   " ORDER BY $sort_field_name ".($asc ? "ASC":"DESC");
    $sql .=   " OFFSET $offset LIMIT $limit";

    return SelectMultiple($dbh, $sql);
}


# ----------------------------------------------------------------------------

=head2 ContactDBGetMatchingWithOffset

ContactDBGetMatching return the list of contact matching regexps
for groups in $grp_id_list list with offset and limit

$params is an hashref with ContactDBAdd keys and regexp's list as values

return a list of ContactDBGetContact

=cut

# ---------------------------------------------------------------------------'
sub ContactDBGetMatchingWithOffset {
    my ($self, $config, $grp_id_list, $offset, $limit, $sort_field_name, $asc, $params) = @_;

	my $dbh = $config->GetDBH();

    my @regexp_list = ("group_id IN (".join(", ", @$grp_id_list).")");


    foreach my $var ("name", "surname", "photo_url", "title",
                     "orgname", "email_work", "email_home", "tel_work",
                     "tel_home", "tel_mobile", "tel_fax", "addr_home",
                     "city_home", "postal_code_home", "state_home", 
                     "country_home", "addr_work", "city_work", "postal_code_work",
                     "state_work", "country_work", "url", "comment") {
        
        if(defined $params->{$var}) {
            my @var_reg_exp_list;
            
            foreach my $regexp (@{$params->{$var}}) {
                push @var_reg_exp_list, st_NoAccent($var)." ~* '$regexp'";
            }            
            
            
            push @regexp_list, "(".join(" OR ", @var_reg_exp_list).")";
            
        }
        
    }
    
    my $sql = "SELECT * FROM contacts WHERE ".join(" AND ", @regexp_list);
    $sql .=   " ORDER BY $sort_field_name ".($asc ? "ASC":"DESC");
    $sql .=   " OFFSET $offset LIMIT $limit" ;
    
    return SelectMultiple($dbh, $sql);
}

# ----------------------------------------------------------------------------

=head2 ContactDBExportVcard

ContactDBExportVcard convert the contact $rowid in vCard data.

=cut

# ---------------------------------------------------------------------------'
sub ContactDBExportVcard {
    my ($self, $config, $rowid) = @_;

	my $dbh = $config->GetDBH();

    my $contact = SelectSingle($dbh, "SELECT * FROM contacts WHERE rowid = $rowid");

    my $vcard = "BEGIN:VCARD\n";
    $vcard .= "VERSION:2.1\n";
    
    my $formatted_name;
    if(defined $contact->{surname}) {
        $formatted_name = $contact->{surname}." ".$contact->{name};
        addEntryToVcard(\$vcard, "N", $contact->{name}, $contact->{surname});
    }
    else {
        $formatted_name = $contact->{name};
    }

    ## Add the contact name and organization info.
    addEntryToVcard(\$vcard, "FN", $formatted_name);
    
    addEntryToVcard(\$vcard, "TITLE", $contact->{title}) if defined $contact->{title};
    
    if(defined $contact->{orgname} and ref($contact->{orgname}) eq "ARRAY") {
	addEntryToVcard(\$vcard, "ORG", split(';', @{$contact->{orgname}}));
    }
    elsif(defined $contact->{orgname}) {
	addEntryToVcard(\$vcard, "ORG", $contact->{orgname});
    }

    addEntryToVcard(\$vcard, "URL", $contact->{url}) if defined $contact->{url};
    addEntryToVcard(\$vcard, "NOTE", $contact->{comment}) if defined $contact->{comment};

    ## Add email and telephone
    addEntryToVcard(\$vcard, "EMAIL;INTERNET", $contact->{email_work}) if defined $contact->{email_work};
    addEntryToVcard(\$vcard, "EMAIL;INTERNET", $contact->{email_home}) if defined $contact->{email_home};
    
    addEntryToVcard(\$vcard, "TEL;WORK", $contact->{tel_work}) if defined $contact->{tel_work};
    addEntryToVcard(\$vcard, "TEL;HOME", $contact->{tel_home}) if defined $contact->{tel_home};
    addEntryToVcard(\$vcard, "TEL;CELL", $contact->{tel_mobile}) if defined $contact->{tel_mobile};
    addEntryToVcard(\$vcard, "TEL;FAX", $contact->{tel_fax}) if defined $contact->{tel_fax};
    
    
    ## Add the contact work address 
    if(defined $contact->{"addr_work"}        or 
       defined $contact->{"city_work"}        or 
       defined $contact->{"postal_code_work"} or 
       defined $contact->{"state_work"}       or 
       defined $contact->{"country_work"}) {
        
        addEntryToVcard(\$vcard, "ADR;WORK",   '', '', 
                        defined $contact->{"addr_work"} ? $contact->{"addr_work"} : '',
                      
                        defined $contact->{"city_work"} ? $contact->{"city_work"} : '',
                        defined $contact->{"state_work"} ? $contact->{"state_work"} : '',
                        defined $contact->{"postal_code_work"} ? $contact->{"postal_code_work"} : '',
                        defined $contact->{"country_work"} ? $contact->{"country_work"} : '');
    }


    ## Add the contact home address 
    if(defined $contact->{"addr_home"}        or 
       defined $contact->{"city_home"}        or 
       defined $contact->{"postal_code_home"} or 
       defined $contact->{"state_home"}       or 
       defined $contact->{"country_home"}) {
        
        addEntryToVcard(\$vcard, "ADR;HOME", '', '', 
                        defined $contact->{"addr_home"} ? $contact->{"addr_home"} : '',
                        
                        defined $contact->{"city_home"} ? $contact->{"city_home"} : '',
                        defined $contact->{"state_home"} ? $contact->{"state_home"} : '',
                        defined $contact->{"postal_code_home"} ? $contact->{"postal_code_home"} : '',
                        defined $contact->{"country_home"} ? $contact->{"country_home"} : '');
    }

    
    ## Add photo
    if(defined $contact->{"photo_url"}) {
        addEntryToVcard(\$vcard, "PHOTO;VALUE=URL", $contact->{photo_url});
    }
    elsif(defined $contact->{"photo_data"}) {
        my $tempfile = tmpnam.".jpg";
        my $oid = SelectSingle($dbh, "SELECT photo_data FROM contacts WHERE rowid = $rowid");

		$self->ContactDBGetPhoto($config, $oid->{photo_data}, $tempfile);

        $vcard .= "PHOTO;VALUE=INLINE;ENCODING=BASE64;TYPE=JPEG:\n    ";

        my $old_dolbs = $/;
        undef $/;
        
        open(F_PIC, $tempfile)
        or throw Mioga2::Exception::Simple("ContactDBExportVcard()", __x("Cannot open file {filename}: {error}", filename => $contact->{photo_data}, error => $!));
        
        my $picture = <F_PIC>;
        close (F_PIC);

        my $base64_pic = encode_base64($picture);
        
        $base64_pic =~ s/\n$//;
        $base64_pic =~ s/\n/\n    /g;
        
        $vcard .= $base64_pic."\n";

        $/ = $old_dolbs;            
        
        unlink($tempfile);
    }

    $vcard .= "END:VCARD\n";
    
    $vcard =~ s/\n/\x0D\x0A/g;
    
    return $vcard;
}




# ----------------------------------------------------------------------------

=head2 ContactDBImportVcard

ContactDBImportVcard parse a vCard and add a contact for group grp_id. 

=cut

# ---------------------------------------------------------------------------'
sub ContactDBImportVcard {
    my ($self, $config, $grp_id, $vcard_data) = @_;

	my $dbh = $config->GetDBH();
    
    my $vcard = new Mioga2::Contact::vCard;
    my $res = $vcard->Run($vcard_data);

    foreach my $group (keys %$res) {
        $self->ContactDBAdd($config, $grp_id, $res->{$group});

        if(defined $res->{$group}->{photo_data}) {
            unlink($res->{$group}->{photo_data});
        }
    }
}



# ============================================================================

=head2 ContactDBDeleteGroupData ($config, $group_id)

	Remove group data in database when a group is deleted.

	This function is called when a group/user/resource is deleted.

=cut

# ============================================================================

# ----------------------------------------------------------------------------
# ContactDBDeleteGroup
# ----------------------------------------------------------------------------
sub ContactDBDeleteGroupData
{
	my ($self, $config, $group_id) = @_;

	my $dbh = $config->GetDBH();
	my $sql = "DELETE FROM contacts WHERE group_id=$group_id";
	ExecSQL($dbh,$sql);
}



# ----------------------------------------------------------------------------

=head2 ContactDBExportToXML

ContactDBExportToXML export $groupId's contacts to XML

Returne a xml string like :

<ContactList>
    <Contact>
        <rowid>1</rowid>
        <created>2002-04-04 09:00:39+00</created>
        <modified>2002-04-04 09:00:39+00</modified>
        <name>Toto</name>
                
        and all available fields (see ContactDBAdd) except photo_data.

    </Contact>
    <Contact>
       .
       .
       .
    </Contact>
</ContactList>

=cut

# ---------------------------------------------------------------------------'
sub ContactDBExportToXML {
    my ($self, $config, $group_id, $modif_date, $synchro_ident) = @_;

	my $dbh = $config->GetDBH();

	if(!defined $modif_date) {
		$modif_date = '01-01-1970 00:00:00';
	}
  
    my $ctclist = SelectMultiple($dbh, "SELECT * FROM contacts WHERE group_id = $group_id AND modified >= '$modif_date'");


	my @rowids = map {$_->{rowid}} @$ctclist;
	
	my $sync = new Mioga2::Synchro();
	my $remoteids = $sync->GetRemoteRowids($config, $synchro_ident, 'Contact', \@rowids);


    my $xml = "<ContactList>\n";

    foreach my $contact (@$ctclist) {
        $xml .= "\t<Contact>\n";

        foreach my $field ("rowid", "created", "modified", "name",
                           "surname", "photo_url", "title", "orgname",
                           "email_work", "email_home", "tel_work",
                           "tel_home", "tel_mobile", "tel_fax",
                           "addr_home", "city_home",
                           "postal_code_home", "state_home",
                           "country_home", "addr_work", "city_work",
                           "postal_code_work", "state_work",
                           "country_work", "url", "comment") {

            next unless defined $contact->{$field} and $contact->{$field} ne "";

            $xml .= "\t\t<$field>".st_FormatXMLString($contact->{$field})."</$field>\n";
        }

		if(exists $remoteids->{$contact->{rowid}}) {
			$xml .= "\t\t<remote_id>".st_FormatXMLString($remoteids->{$contact->{rowid}})."</remote_id>\n";
		}

        $xml .= "\t</Contact>\n";        
    }

    $xml .= "</ContactList>\n";

    return $xml;
}


# ----------------------------------------------------------------------------

=head2 ContactDBExportRowids

ContactDBExportRowids export $groupId's contact rowids to XML

Returne a xml string like :

<RowidList>
    <rowid>1</rowid>
    <rowid>3</rowid>
    ...
</RowidList>

=cut

# ---------------------------------------------------------------------------'
sub ContactDBExportRowids {
    my ($self, $config, $group_id, $synchro_ident) = @_;

	my $dbh = $config->GetDBH();

    my $ctclist = SelectMultiple($dbh, "SELECT rowid FROM contacts WHERE group_id = $group_id");

	my @rowids = map {$_->{rowid}} @$ctclist;
	my $sync = new Mioga2::Synchro();
	my $remoteids = $sync->GetRemoteRowids($config, $synchro_ident, 'Contact', \@rowids);

    my $xml = "<RowidList>\n";

	map {  if(exists $remoteids->{$_->{rowid}}) {
			   $xml .= "<rowid remote_id=\"$remoteids->{$_->{rowid}}\">$_->{rowid}</rowid>";
	       }
		   else {
			   $xml .= "<rowid>$_->{rowid}</rowid>";
		   }
	    } @$ctclist;

    $xml .= "</RowidList>\n";

    return $xml;
}


# ---------------------------------------------------------------------------'
=head2 ContactDBDeleteFromXML

ContactDBDeleteFromXML delete contacts list described in XML data

The data xml string looks like :

<RowidList>
    <rowid>1</rowid>
    <rowid>3</rowid>
    ...
</RowidList>

=cut

# ---------------------------------------------------------------------------'
sub ContactDBDeleteFromXML {
    my ($self, $config, $group_id, $data) = @_;

	if(! exists $data->{rowid}) {
		return;
	}

	my $dbh = $config->GetDBH();

	my $sql = "DELETE FROM contacts WHERE rowid IN (".join(',', @{$data->{rowid}}).") AND group_id = $group_id";

	my $sync = new Mioga2::Synchro();
	my $remoteids = $sync->DeleteRowids($config, 'Contact', $data->{rowid});

	ExecSQL($dbh, $sql);
}


# ---------------------------------------------------------------------------'
=head2 ContactDBImportFromXML

ContactDBImportFromXML import contacts list described in XML data

The data xml string looks like :

<ContactList>
    <Contact>
        <rowid>1</rowid>
        <created>2002-04-04 09:00:39+00</created>
        <modified>2002-04-04 09:00:39+00</modified>
        <name>Toto</name>
                
        and all available fields (see ContactDBAdd) except photo_data.

    </Contact>
    <Contact>
       .
       .
       .
    </Contact>
</ContactList>

=cut

# ---------------------------------------------------------------------------'
sub ContactDBImportFromXML {
    my ($self, $config, $group_id, $data, $synchro_ident) = @_;

	my $dbh = $config->GetDBH();
	
	my @givenrowids = map {$_->{rowid}->[0]} grep {exists $_->{rowid} } @{$data->{Contact}};

	my $sync = new Mioga2::Synchro();
	my $remote_ids = $sync->GetRemoteRowids($config, $synchro_ident, 'Contact', \@givenrowids);

	my @id_list;

	my $newremoteids;

    foreach my $contact (@{$data->{Contact}}) {
        my $ctc;

        foreach my $field ("rowid",  "name",
                           "surname", "photo_url", "title", "orgname",
                           "email_work", "email_home", "tel_work",
                           "tel_home", "tel_mobile", "tel_fax",
                           "addr_home", "city_home",
                           "postal_code_home", "state_home",
                           "country_home", "addr_work", "city_work",
                           "postal_code_work", "state_work",
                           "country_work", "url", "comment", "remote_id") {

            next unless exists $contact->{$field};

            if(ref($contact->{$field}->[0]) ne 'HASH') {
                $ctc->{$field} = $contact->{$field}->[0];
            }
            else {
                $ctc->{$field} = "";
            }
        }


		foreach my $arg ("name", "surname", "title", "orgname",
						 "email_work", "email_home", "tel_work",
						 "tel_home", "tel_mobile", "tel_fax",
						 "addr_home", "city_home",
						 "postal_code_home", "state_home",
						 "country_home", "addr_work", "city_work",
						 "postal_code_work", "state_work",
						 "country_work") {
		   if(exists $ctc->{$arg} and length($ctc->{$arg}) > 128) {
			   $ctc->{comment} = '' unless exists $ctc->{comment};
			   $ctc->{comment} .= "\n$ctc->{$arg}\n";
			   $ctc->{$arg} = substr($ctc->{$arg}, 0, 128);
		   }
	   }


        if(exists $ctc->{rowid}) {
			my $ctcid = $self->ContactDBModify($config, $ctc->{rowid}, $ctc);
			push @id_list, $ctcid;			
			
			if(!exists $remote_ids->{$ctcid}) {
				$newremoteids->{$ctcid} = $ctc->{remote_id};
			}
		}
        else {
			my $ctcid = $self->ContactDBAdd($config, $group_id, $ctc);
			
			$newremoteids->{$ctcid} = $ctc->{remote_id};

            push @id_list, $ctcid;			
        }
    }

	$sync->AddRemoteRowids($config, $synchro_ident, 'Contact', $newremoteids);

	$remote_ids = $sync->GetRemoteRowids($config, $synchro_ident, 'Contact', \@id_list);

	my $xml = "<RowidList>";
	map {$xml .= "<rowid remote_id=\"$remote_ids->{$_}\">$_</rowid>"} @id_list;
	$xml .= "</RowidList>";

    return $xml;
}


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ----------------------------------------------------------------------------
# Add an entry to the vcard with required params and encoding  
# ----------------------------------------------------------------------------
sub addEntryToVcard {
    my ($vcard_ref, $property, @data_list) = @_;
    
    $$vcard_ref .= $property;

	local $/ = undef;
	
	my $quoted_printable = 0;
	my $ascii8 = 0;
	foreach my $data (@data_list) {
		$data =~ s/^\s+//;
		$data =~ s/\s+$//;
		
		if($data =~ /[\x00-\x1f\x3d\x7f-\xff]/) {
			$quoted_printable = 1;
		}
		
		if($data =~ /[\x80-\xff]/) {
			$ascii8 = 1;
		}
	}
	
	if($quoted_printable) {
		$$vcard_ref .= ";ENCODING=QUOTED-PRINTABLE";
		
		for(my $i=0; $i<@data_list; $i++) {
			$data_list[$i] = encode_qp($data_list[$i]);
		}
	}
	
	if($ascii8) {
		$$vcard_ref .= ";CHARSET=UTF-8";
	}
	
	$$vcard_ref .= ":".join(";", @data_list)."\n";
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


