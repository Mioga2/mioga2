#===============================================================================
#
#  Copyright (c) year 1999-2013, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

HaussmannReport.pm: the Mioga2 Project report application

=head1 DESCRIPTION

The Mioga2::HaussmannReport application is used to consult projects according to user context.

=head1 METHODS DESCRIPTION

=cut

#

package Mioga2::HaussmannReport;
use base qw(Mioga2::Haussmann);
use vars qw($VERSION);

$VERSION = "1.0.0";

use Locale::TextDomain::UTF8 'haussmann';
use Mioga2::UserList;
use Mioga2::GroupList;
use Mioga2::ApplicationList;
use Mioga2::Config;

my $debug = 0;

#===============================================================================

=head2 InitApp

Initialize inner data for application. Not to be used, automatically called by
Mioga2::Application.

=cut

#===============================================================================
sub InitApp {
	my ($self, $context) = @_;

	$self->{current_group_id} = [];
	my $config = $context->GetConfig ();
	my $user = $context->GetUser();
	my $app = Mioga2::ApplicationList->new ($config, { attributes => { ident => 'Haussmann' } });
#	liste des groupes pouvant lancer l'application Haussmann, dont l'utilisateur est invité et dont l'utilisateur à les droits d'accès à l'application Haussmann
#	Group list can launch tha Haussmann application, that user is invited and that the user access rights to the Haussmann application
	my $g = Mioga2::GroupList->new ($config, { attributes => { expanded_users => $user, applications => $app} });
	my @tab = $g->GetGroups();
	for my $grp (@tab) {
		push (@{$self->{current_group_id}}, $grp->{rowid});
	}
}	# ----------  end of subroutine InitApp  ----------
# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc {
	my %AppDesc = (
		ident   => 'HaussmannReport',
		name => __('Haussmann reporting'),
		package => 'Mioga2::HaussmannReport',
		description => __('The Mioga2 Project Report Application'),
		type    => 'normal',
		api_version => '2.4',
		is_group            => 0,
		is_user             => 1,
		is_resource         => 0,
		all_groups => 0,
		all_users  => 1,
		can_be_public => 0,
		usable_by_resource => 0,
        functions   => { 
			'Read'                 => __('Read functions')
		},
        func_methods  => { 
			Read =>                ['DisplayMain', 'GetLangList', 'GetTagList', 'GetModelList', 'GetProjectList', 'GetProject', 'GetProjectByTask', 'GetProjectByResult', 'GetTaskList', 'GetTask', 'GetTaskNoteList', 'GetResultList', 'GetResultNoteList']
		},
		public_methods => [
			'DisplayMain'
		],
		non_sensitive_methods => [
			'DisplayMain',
			'GetLangList',
			'GetTagList',
			'GetModelList',
			'GetProjectList',
			'GetProject',
			'GetProjectByTask',
			'GetProjectByResult',
			'GetTaskList',
			'GetTask',
			'GetTaskNoteList',
			'GetResultList',
			'GetResultNoteList'
		]
    );

	return \%AppDesc;
}

1;
