# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
local.pm: Modify password for local user

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================


package Mioga2::Password::Modifier::local;

use strict;
use Error qw(:try);
use Mioga2::tools::string_utils;

# ============================================================================

=head2 new ()

	Create the Mioga2::Password::Modifier::local object.

=cut

# ============================================================================

sub new {
	my ($class) = @_;
	
	my $self = {};
	bless($self, $class);

	return $self;
}


# ============================================================================

=head2 ModifyPassword ($config, $values)
	
	Modify local user password.
	Does nothing, nothing else needed for local user.

=cut

# ============================================================================

sub ModifyPassword {
	my ($self, $config, $values) = @_;

	return;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Config

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
