# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
ldap.pm: Modify password for ldap user

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================


package Mioga2::Password::Modifier::ldap;
use strict;

use Locale::TextDomain::UTF8 'password_modifier_ldap';

use Error qw(:try);
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Mioga2::LDAP;

# ============================================================================

=head2 new ()

	Create the Mioga2::Password::Modifier::ldap object.

=cut

# ============================================================================

sub new {
	my ($class) = @_;
	
	my $self = {};
	bless($self, $class);

	return $self;
}


# ============================================================================

=head2 ModifyPassword ($config, $values)
	
	Modify ldap user password.
	Does nothing, nothing else needed for ldap user.

=cut

# ============================================================================

sub ModifyPassword {
	my ($self, $config, $values) = @_;

	my $mldap = new Mioga2::LDAP($config);
	# TODO put modify method in LDAP object
	my $ldap = $mldap->{ldap};
	my $dn = $values->{dn};

	#
	# Rebind as current user
	# 

	#my $res = $ldap->bind($dn, password => $values->{old_password});
	#$res->code && throw Mioga2::Exception::Simple("Mioga2::Password::Modifier::ldap->ModifyPassword", 
	#											 "Can't bind to ldap server : ".$res->error);

	my $res = $ldap->modify($dn, replace => { userPassword => $values->{password} });
	$res->code && throw Mioga2::Exception::Simple("Mioga2::Password::Modifier::ldap->ModifyPassword", 
    __x("Can't modify password: {error}", error => $res->error));

	return;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Config

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
