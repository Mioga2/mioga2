# ============================================================================
# Mioga2 Project (C) 2003 The MiogaII Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
sha1.pm: Implement sha1 method for password

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================


package Mioga2::Password::Crypter::sha;

use strict;
use Error qw(:try);
use Mioga2::tools::string_utils;
use Digest::SHA  qw(sha1_base64);

# ============================================================================

=head2 new ()

	Create the Mioga2::Password::Crypter::sha1 object.

=cut

# ============================================================================

sub new {
	my ($class) = @_;
	
	my $self = {};
	bless($self, $class);

	return $self;
}


# ============================================================================

=head2 CryptPassword ($config, $values)

	Crypt the password with "sha" method

=cut

# ============================================================================

sub CryptPassword {
	my ($self, $config, $values) = @_;
	
	my $sha1 = $self->GetSHA1($values->{password});
	return "{sha}".$sha1;
}




# ============================================================================

=head2 CheckPassword ($clearpasswd, $cryptedpasswd)

	Check password crypted with "sha1" method

=cut

# ============================================================================

sub CheckPassword {
	my ($self, $clearpasswd, $cryptedpasswd) = @_;

	my $sha1 = $self->GetSHA1($clearpasswd);
	return ( $sha1 eq $cryptedpasswd );
}



# ============================================================================

=head2 GetSHA1 ($password)

	Encode given password into sha1.
	Return a base64 encoded string

=cut

# ============================================================================

sub GetSHA1 {
	my ($self, $password) = @_;

	my $sha1 = 	sha1_base64($password);

	# Append (size % 4) = sign to base64 string
	for(my $i=0; $i < (length($sha1) % 4); $i++) {
		$sha1 .= "=";
	}
	
	return $sha1;
}
		

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Config

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
