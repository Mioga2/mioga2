# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#   Description:

=head1 NAME
	
Organizer.pm: The Mioga2 Organizer application

=head1 METHODS DESRIPTION

=cut

package Mioga2::Organizer;

use strict;
use Mioga2::Exception::Simple;
use Mioga2::Contact;
use Mioga2::tools::APIAuthz;
use Date::Manip;
use Data::Dumper;
use POSIX qw(tzset);

##******************************************************************************

=head2 CreateStrictOrPeriodicTask ()

	Create a Planned or Periodic task.

=head3 Generated XML :
    
   <CreateStrictOrPeriodicTask 
        <!-- Is Planned = 0 when task is not planned (without time) -->
        is_planned="1"

        <!-- Task Rowid (-1 for new tasks) -->
        taskrowid="-1"

        <!-- Task name -->
        name=""

        <!-- Start Date -->
        startHour="11"
        startMin="0"
	    day="20"
        month="5"
        year="2005"

        <!-- Stop hour -->
	    stopHour="12"
        stopMin="0"

        <!-- Stop date for periodic tasks -->
	    endMonth="0"
        endDay="0"
        endYear="0"

	    <!-- Contact Related informations -->
        first_name=""
        last_name=""
        email=""
        telephone=""
        outside="0"
        fax=""

        <!-- Periodic tasks settings -->

	    <!-- Task period settings type 
               - planned : a planned task
               - day : a daily tasks
               - week : a weekly tasks
               - month : a monthly task
               - year : a yearly task
           -->
  	    periodType="planned"

        <!-- for daily tasks, the frequency. Every "dayFreq" day -->
        dayFreq="1"

        <!-- for weekly tasks, the frequency and week of day. -->
  	    weekWeekDay="1"
        weekFreq="1"

        <!-- for monthly tasks, there is two type of monthly task : 
            - per day of month : the Nth day of the month
            - per week and week day : the tuesday of the third week of the month.
              -->
        monthPeriodType="day"

        <!-- Frequency. Valid for the two monthly period type -->
        monthFreq="1"

        <!-- For per day of month tasks -->
  	    monthDay="1"

        <!-- The week number and day of week for per week and week day tasks -->
        monthWeek="1"
        monthWeekDay="1"

        <!-- For yearly tasks, the day of year. -->
	    yearFreq="1"
        yearMonth="1"
        yearDay="1"

        <!-- The calling application URI -->
        referer="https://galadriel/Mioga2/Mioga/bin/admin/Organizer/DisplayMain"
    >

        <description>
        Task description
        </description>

        <task_category rowid="category rowid" fgcolor="text color" bgcolor="category bg color">
           category name 
        </task_category>     
    </CreateStrictOrPeriodicTask>

=head3 Form actions and fields :
    
	- create : Try to create the task.

      Arguments :
         name                type                  description
       ---------------------------------------------------------------------
       name        :  not empty string      : the task name
       is_planned  :  boolean               : is the task planned (with an hour) ?
       outside     :  boolean               : is the task outside ?
       startHour   :  string                : the task start hour
       startMin    :  string                : the task start minute
       day         :  string                : the task day of month number
       month       :  string                : the task month number
       year        :  string                : the task year number
       firstname   :  string                : the task contact first name
       lastname    :  string                : the task contact last name
       email       :  email                 : the task contact email
       telephone   :  string                : the task contact phone number
       fax         :  string                : the task contact fax number
  	   periodType  :  string                : the periodic task type
       dayFreq     :  string                : see xml description
  	   weekWeekDay :  string                : see xml description
       weekFreq    :  string                : see xml description
       monthPeriodType : string             : see xml description
       monthFreq   :  string                : see xml description
  	   monthDay    :  string                : see xml description
       monthWeek   :  string                : see xml description
       monthWeekDay:  string                : see xml description
	   yearFreq    :  string                : see xml description
       yearMonth   :  string                : see xml description
       yearDay     :  string                : see xml description
	   endMonth    :  string                : see xml description
       endDay      :  string                : see xml description
       endYear     :  string                : see xml description

    - cancel_periodicity : Back from task periodicity setting cancelation

	
	- contact : Launch contact selection


	- periodicity : Launch periodicity selection

=cut

##******************************************************************************
sub CreateStrictOrPeriodicTask {
    my ($self, $context) = @_;

    my $userTimeZone = $self->GetTimezone($context);

    ##===========================================================================
    ## Session management
    ##===========================================================================
    my $session = $context->GetSession();

	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'stopHour', 'stopMin', 'month', 'day', 'year', 'startMin', 'startHour', 'category_id'], 'allow_empty', 'want_int'],
													 [ [ 'taskrowid'], 'allow_empty', 'full_int'],
													 [ [ 'mode', 'name', 'description', 'telephone', 'first_name', 'last_name', 'fax', 'create'], 'allow_empty', 'stripxws'],
													 [ [ 'email'], 'allow_empty', 'stripxws'],
													 [ [ 'outside', 'periodic'], 'allow_empty', 'bool'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Organizer::CreateStrictOrPeriodicTask", ac_FormatErrors($errors));
	}
    ## Initial form parameters
    if (exists($context->{args}{mode})
        && $context->{args}{mode} eq "initial")
    {
        $session->{organizer}{data} = {};
        $session = $session->{organizer}{data};
        foreach my $parameter (qw( taskrowid day month year )) {
            return $self->_Error($context, __x("{parameter} not defined", parameter => $parameter))
              if (!defined($context->{args}{$parameter}));
        }
        foreach my $parameter (qw( startHour startMin stopHour stopMin  )) {
            $session->{$parameter} = '' if (!defined($session->{$parameter}));
        }
        $session->{referer} = $context->GetReferer();
    }
    ## We are not in the initial form
    else {
        throw Mioga2::Exception::Simple("Mioga2::Organizer->CreateStrictOrPeriodicTask", __("session->organizer not defined"))
          if (!exists($session->{organizer}{data}));
        $session = $session->{organizer}{data};
    }

    ##===========================================================================
    ## Initial form / next form  parameters
    ##===========================================================================
    if (!st_ArgExists($context, "cancel_periodicity")) {
        foreach my $parameter (
            qw(
            day month year startHour startMin stopHour stopMin
            taskrowid name description category_id private bgcolor fgcolor outside first_name last_name
            telephone fax email periodType endDay endMonth endYear dayFreq weekFreq weekWeekDay
            monthFreq monthPeriodType monthDay monthWeek monthWeekDay yearFreq yearDay yearMonth
            )
          )
        {
            $session->{$parameter} = $context->{args}{$parameter} if (exists($context->{args}{$parameter}));
        }

        foreach my $parameter (qw( referer taskrowid day month year  )) {
            return $self->_Error($context, __x("{parameter} not defined", parameter => $parameter))
              if (!defined($session->{$parameter}));
        }

        foreach my $parameter (qw( startHour startMin stopHour stopMin  )) {
            $session->{$parameter} = '' if (!defined($session->{$parameter}));

        }
    }

    ##===========================================================================
    ## Default values
    ##===========================================================================
    $self->_AddingContact($context, $session);

    try {
        my $appdesc     = new Mioga2::AppDesc($context->GetConfig(), ident => 'Contact');
        my $application = $appdesc->CreateApplication();
        my $test        = $application->CheckUserAccessOnMethod($context, $context->GetUser(), $context->GetGroup(), 'SearchEngine');

        $session->{display_icon} = ($test == AUTHZ_OK);
    }

    otherwise {
        $session->{display_icon} = 0;
    };

    $session->{name}         = "" if (!exists($session->{name}));
    $session->{description}  = "" if (!exists($session->{description}));
    $session->{outside}      = 0  if (!exists($session->{outside}));
    $session->{first_name}   = "" if (!exists($session->{first_name}));
    $session->{last_name}    = "" if (!exists($session->{last_name}));
    $session->{telephone}    = "" if (!exists($session->{telephone}));
    $session->{fax}          = "" if (!exists($session->{fax}));
    $session->{email}        = "" if (!exists($session->{email}));
    $session->{waiting_book} = 0  if (!exists($session->{waiting_book}));

    if (   !exists($session->{endDay})
        or $session->{endDay} == 0
        or !exists($session->{endMonth})
        or $session->{endMonth} == 0
        or !exists($session->{endYear})
        or $session->{endYear} == 0)
    {
        $session->{endDay}   = 0;
        $session->{endMonth} = 0;
        $session->{endYear}  = 0;
    }
    $session->{periodType}      = 'planned' if (!exists($session->{periodType}));
    $session->{monthPeriodType} = 'day'     if (!exists($session->{monthPeriodType}));
    $session->{dayFreq}         = 1         if (!exists($session->{dayFreq}));
    $session->{weekFreq}        = 1         if (!exists($session->{weekFreq}));
    $session->{monthFreq}       = 1         if (!exists($session->{monthFreq}));
    $session->{yearFreq}        = 1         if (!exists($session->{yearFreq}));
    $session->{weekWeekDay}     = 1         if (!exists($session->{weekWeekDay}));
    $session->{monthDay}        = 1         if (!exists($session->{monthDay}));
    $session->{monthWeek}       = 1         if (!exists($session->{monthWeek}));
    $session->{monthWeekDay}    = 1         if (!exists($session->{monthWeekDay}));
    $session->{yearDay}         = 1         if (!exists($session->{yearDay}));
    $session->{yearMonth}       = 1         if (!exists($session->{yearMonth}));

    foreach my $parameter (qw( startHour startMin stopHour stopMin  )) {
        if ($session->{$parameter} eq '') {
            $session->{is_planned} = 0;
        }
    }

    if (!exists $session->{is_planned}) {
        $session->{is_planned} = 1;
    }

    ##===========================================================================
    ## If contact : save parameters to session and call contact
    ##===========================================================================
    if (st_ArgExists($context, "contact")) {
        my $uri = new Mioga2::URI(
            $context->GetConfig(),
            group       => $context->GetGroup()->GetIdent(),
            public      => 0,
            application => 'Contact',
            method      => 'SearchEngine',
            args        => {
                action  => 'Contact',
                referer => $context->GetURI()->GetURI()
            }
        );

        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent($uri->GetURI());
        return $content;
    }

    ##===========================================================================
    ## If periodicity : let's go changing the periodicity
    ##===========================================================================
    if (st_ArgExists($context, "periodicity")) {
        return $self->_CreateStrictOrPeriodicTask_Form($context, $session, "OK", "Periodicity");
    }

    ##===========================================================================
    ## Gererate the inital form
    ##===========================================================================
    if (!st_ArgExists($context, "create")) {
        return $self->_CreateStrictOrPeriodicTask_Form($context, $session, "OK", "");
    }

    ##===========================================================================
    ## Create Strict task
    ##===========================================================================
    if ($session->{periodType} eq "planned") {
        return $self->_CreateStrictOrPeriodicTask_Form($context, $session, "NAME_EMPTY", "")
          if ($session->{name} =~ /^\s*$/);

        $session->{month} = sprintf("%02d", $session->{month});
        $session->{day}   = sprintf("%02d", $session->{day});

        my $startDate;
        my $duration_value;
        if ($session->{is_planned}) {
            $session->{startHour} = sprintf("%02d", $session->{startHour});
            $session->{startMin}  = sprintf("%02d", $session->{startMin});
            $session->{stopHour}  = sprintf("%02d", $session->{stopHour});
            $session->{stopMin}   = sprintf("%02d", $session->{stopMin});

            ##------------------------------------------------------------------------
            my $duration = du_GetMonthDuration($session->{month}, $session->{year});
            if ($session->{day} > $duration) {
                $session->{day} = $duration;
                return $self->_CreateStrictOrPeriodicTask_Form($context, $session, "BAD_DAY_FOR_MONTH", "");
            }

            ##------------------------------------------------------------------------
            my ($startSec, $stopDate, $stopSec);
            {
                local $ENV{TZ} = $userTimeZone;
                tzset();
                $startSec = strftime('%s', 0, $session->{startMin}, $session->{startHour}, $session->{day}, $session->{month} - 1, $session->{year} - 1900);
                local $ENV{TZ} = 'GMT';
                tzset();
                $startDate = strftime("%Y-%m-%d %H:%M:%S.00 GMT", gmtime($startSec));

                local $ENV{TZ} = $userTimeZone;
                tzset();
                $stopSec = strftime('%s', 0, $session->{stopMin}, $session->{stopHour}, $session->{day}, $session->{month} - 1, $session->{year} - 1900);
                local $ENV{TZ} = 'GMT';
                tzset();
                $stopDate = strftime("%Y-%m-%d %H:%M:%S.00 GMT", gmtime($stopSec));
            }
            tzset();

            ##------------------------------------------------------------------------
            $duration_value = $stopSec - $startSec;
            return $self->_CreateStrictOrPeriodicTask_Form($context, $session, "START_TIME_AFTER", "")
              if ($duration_value <= 0);
        }
        else {
            $startDate      = "$session->{year}-$session->{month}-$session->{day} 00:00:00";
            $duration_value = 0;
        }

        ##------------------------------------------------------------------------

        my %fields = (
            "group_id" => int($session->{resource_id}) ? $session->{resource_id} : $context->GetGroup()->GetRowid(),
            "user_id"  => $context->GetUser()->GetRowid(),
            "name"     => $session->{name},
            "description"  => $session->{description},
            "category_id"  => $session->{category_id},
            "start"        => $startDate,
            "duration"     => $duration_value,
            "outside"      => $session->{outside} ? 't' : 'f',
            "first_name"   => $session->{first_name},
            "last_name"    => $session->{last_name},
            "telephone"    => $session->{telephone},
            "fax"          => $session->{fax},
            "email"        => $session->{email},
            "is_planned"   => $session->{is_planned},
            "waiting_book" => $session->{waiting_book} ? 't' : 'f',
        );

        ##------------------------------------------------------------------------
        my $r = $self->Org_CreateTask($context, "planned", \%fields);
        if (!$r) {
            return $self->_CreateStrictOrPeriodicTask_Form($context, $session, "CREATION_ERROR", "");
        }

        ##------------------------------------------------------------------------
        return $self->_TaskResult_Page($context, "create");

    }
    ##===========================================================================
    ## Create Periodic task
    ##===========================================================================
    else {
        return $self->_CreatePeriodicTask($context);
    }

}

##******************************************************************************

=head2 CreateSimplePeriodicTask ()

	Create simple periodic task by just selecting a beginning date and an ending date.

=cut

##******************************************************************************

sub CreateSimplePeriodicTask {
    my ($self, $context) = @_;

	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'taskrowid', 'stopHour', 'stopMin', 'month', 'day', 'year', 'startMin', 'startHour', 'category_id'], 'allow_empty', 'full_int'],
													 [ [ 'name', 'description', 'telephone', 'first_name', 'last_name', 'fax', 'create'], 'allow_empty', 'stripxws'],
													 [ [ 'email'], 'allow_empty', 'stripxws', 'want_email'],
													 [ [ 'outside', 'periodic'], 'allow_empty', 'bool'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Organizer::CreateSimplePeriodicTask", ac_FormatErrors($errors));
	}

    ##===========================================================================
    ## Session management
    ##===========================================================================
    my $session = $context->GetSession();

    if (not st_ArgExists($context, 'create')) {
        $session->{organizer}{data} = {};
        $session = $session->{organizer}{data};

        $session->{name}         = "";
        $session->{description}  = "";
        $session->{outside}      = 0;
        $session->{first_name}   = "";
        $session->{last_name}    = "";
        $session->{telephone}    = "";
        $session->{fax}          = "";
        $session->{email}        = "";
        $session->{waiting_book} = ($context->{args}{waiting_book}) ? 1 : 0;
        $session->{taskrowid}    = -1;
        $session->{referer}      = $context->{args}{referer} || $context->GetReferer();

        my @current_time = localtime;
        $session->{day}   = $context->{args}{day}   || strftime("%d", @current_time);
        $session->{month} = $context->{args}{month} || strftime("%m", @current_time);
        $session->{year}  = $context->{args}{year}  || strftime("%Y", @current_time);

        $current_time[3] += 1;

        $session->{endDay}   = $context->{args}{endDay}   || strftime("%d", @current_time);
        $session->{endMonth} = $context->{args}{endMonth} || strftime("%m", @current_time);
        $session->{endYear}  = $context->{args}{endYear}  || strftime("%Y", @current_time);

        if ($context->{args}->{mode} eq "booking") {
            $session->{resource_id}  = $context->{args}->{res_id};
            $session->{mode}         = $context->{args}->{mode};
            $session->{name}         = __("Request in progress");
            $session->{waiting_book} = 1;
        }

        return $self->_Task_GenericForm($context, $session, "OK", "CreateSimplePeriodicTask");
    }

    $session = $session->{organizer}{data};

    foreach my $key (keys %{ $context->{args} }) {
        $session->{$key} = $context->{args}{$key};
    }

    $session->{periodType} = 'day';
    $session->{dayFreq}    = 1;
    return $self->_CreateSimplePeriodicTask($context);
}

##******************************************************************************

=head2 EditSimplePeriodicTask ()

	Edit simple periodic task.

=cut

##******************************************************************************

sub EditSimplePeriodicTask {
    my ($self, $context) = @_;
    my $session = $context->GetSession();

    if (not st_ArgExists($context, "change")) {
        my ($row, $errmsg);
        $session->{organizer}{data} = {};
        $session = $session->{organizer}{data};

        $session->{taskrowid}   = int($context->{args}{taskrowid});
        $session->{resource_id} = $context->GetGroup()->GetRowid();
        $session->{referer}     = $context->{args}{referer} || $context->GetReferer();

        if ($context->{args}{mode} eq "booking") {
            $session->{resource_id} = $context->{args}{resource_id};
            $session->{mode}        = $context->{args}{mode};
        }

        try {
            $row = $self->Org_GetTask($context, $session->{taskrowid}, 'periodic', $session->{resource_id});
        }
        catch Mioga2::Exception::Simple with {

            # error getting task
        };

        if (!defined($row)) {
            return $self->_TaskResult_Page($context, "unknown_task");
        }

        $row->{is_planned} = 1;
        if ($errmsg = $self->_PeriodicTaskRowToSession($row, $session, $context)) {
            return $self->_Error($context, $errmsg);
        }
        $session->{is_planned} = 0;

        return $self->_Task_GenericForm($context, $session, "OK", "EditSimplePeriodicTask");
    }

    $session = $session->{organizer}{data};

    foreach my $key (keys %{ $context->{args} }) {
        $session->{$key} = $context->{args}{$key};
    }
    $session->{outside} = $context->{args}{outside} ? "t" : "f";
    return $self->_EditSimplePeriodicTask($context);
}

##******************************************************************************

=head2 TodoChangeStatus ()

	Change the status of a todo task.
	Redirect to referer

=cut

##******************************************************************************

sub TodoChangeStatus {
    my ($self, $context) = @_;

    my $taskrowid = $context->{args}->{taskrowid};

    my $task = $self->Org_GetTask($context, $taskrowid, 'todo', $context->GetGroup()->GetRowid());

    my %fields;
    if ($task->{done}) {
        $fields{done} = 'f';
    }
    else {
        $fields{done} = 't';
    }

    my $r = $self->Org_ChangeTask($context, $taskrowid, 'todo', $context->GetGroup()->GetRowid(), \%fields);

    my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
    $content->SetContent($context->GetReferer());
    return $content;

}

##******************************************************************************

=head2 TodoDelete ()

	Delete a todotask
	Return a success message page.
	
=cut

##******************************************************************************
sub TodoDelete {
    my ($self, $context) = @_;

    my $session   = $context->GetSession();
    my $taskrowid = $context->{args}->{taskrowid};

    if (exists $context->{args}->{referer}) {
        $session->{organizer}->{data}->{referer} = $context->{args}->{referer};
    }
    else {
        $session->{organizer}->{data}->{referer} = $context->GetReferer();
    }

    my $referer = $session->{organizer}->{data}->{referer};

    if (!$context->GetConfig()->AskConfirmForDelete()
        or exists($context->{args}->{confirm}))
    {
        $self->Org_DeleteTask($context, $taskrowid, $context->GetGroup()->GetRowid());

        return $self->_TaskResult_Page($context, "delete");
    }

    return $self->_ConfirmDeletion_Page($context, "TodoDelete?taskrowid=$context->{args}{taskrowid}&amp;confirm=1&amp;referer=" . st_FormatXMLString($referer), 'todo');
}

##******************************************************************************

=head2 CreateToDoTask ()

	Create a Todo task.

=head3 Generated XML :
    
<CreateToDoTask
   <!-- Todo task priority (very urgent, urgent, normal, not urgent) -->
   priority="normal"

   <!-- Name and outside parameter of todo tasks -->
   name=""
   outside="0"

   <!-- Task contact -->
   first_name=""
   last_name=""
   email=""
   telephone=""
   fax=""

   referer="Calling application URL"
  >

    <description>
      Task description
    </description>

    <task_category rowid="category rowid" fgcolor="text color" bgcolor="category bg color">
        category name 
    </task_category>

</CreateToDoTask>

=head3 Form actions and fields :
    
	- create : Try to create the task.

      Arguments :
         name                type                  description
       ---------------------------------------------------------------------
       name        :  not empty string      : the task name
       priority    :  not empty string      : the task priority
       is_planned  :  boolean               : is the task planned (with an hour) ?
       outside     :  boolean               : is the task outside ?
       firstname   :  string                : the task contact first name
       lastname    :  string                : the task contact last name
       email       :  email                 : the task contact email
       telephone   :  string                : the task contact phone number
       fax         :  string                : the task contact fax number

	- contact : Launch contact selection

=cut

##******************************************************************************
sub CreateToDoTask {
    my ($self, $context) = @_;

	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'taskrowid', 'category_id'], 'allow_empty', 'full_int'],
													 [ [ 'name', 'description', 'telephone', 'first_name', 'last_name', 'fax', 'create'], 'allow_empty', 'stripxws'],
													 [ [ 'email'], 'allow_empty', 'stripxws', 'want_email'],
													 [ [ 'priority' ], 'allow_empty' ],
												   ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Organizer::CreateToDoTask", ac_FormatErrors($errors));
	}
    ##===========================================================================
    ## Initial form  parameters
    ##===========================================================================
    my $session = $context->GetSession();

    if (exists($context->{args}{mode})
        && $context->{args}{mode} eq "initial")
    {
        $session->{organizer}{data} = {};
        $session                    = $session->{organizer}{data};
        $session->{referer}         = $context->GetReferer();
    }
    ##===========================================================================
    ## We are not in the initial form
    ##===========================================================================
    else {
        throw Mioga2::Exception::Simple("Mioga2::Organizer->CreateToDoTask", __("session->organizer not defined"))
          if (!exists($session->{organizer}{data}));
        $session = $session->{organizer}{data};
    }

    ##===========================================================================
    ## Initial form / next form  parameters
    ##===========================================================================
    foreach my $parameter (
        qw(
        name description outside first_name
        last_name telephone fax email priority
        category_id
        )
      )
    {
        $session->{$parameter} = $context->{args}{$parameter}
          if (exists($context->{args}{$parameter}));
    }

    $self->_AddingContact($context, $session);

    $session->{name}        = ""       if (!exists($session->{name}));
    $session->{description} = ""       if (!exists($session->{description}));
    $session->{outside}     = "0"      if (!exists($session->{outside}));
    $session->{first_name}  = ""       if (!exists($session->{first_name}));
    $session->{last_name}   = ""       if (!exists($session->{last_name}));
    $session->{telephone}   = ""       if (!exists($session->{telephone}));
    $session->{fax}         = ""       if (!exists($session->{fax}));
    $session->{email}       = ""       if (!exists($session->{email}));
    $session->{priority}    = "normal" if (!exists($session->{priority}));

    try {
        my $appdesc     = new Mioga2::AppDesc($context->GetConfig(), ident => 'Contact');
        my $application = $appdesc->CreateApplication();
        my $test        = $application->CheckUserAccessOnMethod($context, $context->GetUser(), $context->GetGroup(), 'SearchEngine');

        $session->{display_icon} = ($test == AUTHZ_OK);
    }

    otherwise {
        $session->{display_icon} = 0;
    };

    ##===========================================================================
    ## If contact : save parameters to session and call contact
    ##===========================================================================
    if (st_ArgExists($context, "contact")) {
        my $uri = new Mioga2::URI(
            $context->GetConfig(),
            group       => $context->GetGroup()->GetIdent(),
            public      => 0,
            application => 'Contact',
            method      => 'SearchEngine',
            args        => {
                action  => 'Contact',
                referer => $context->GetURI()->GetURI()
            }
        );

        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent($uri->GetURI());
        return $content;
    }

    ##===========================================================================
    ## Gererate the inital form
    ##===========================================================================
    if (!st_ArgExists($context, "create")) {
        return $self->_CreateToDoTask_Form($context, $session, "OK");
    }

    ##===========================================================================
    ## Create task
    ##===========================================================================
    return $self->_CreateToDoTask_Form($context, $session, "NAME_EMPTY")
      if ($session->{name} =~ /^\s*$/);

    my %fields = (
        "group_id"    => $context->GetGroup()->GetRowid(),
        "name"        => $session->{name},
        "category_id" => $session->{category_id},
        "description" => $session->{description},
        "outside"     => $session->{outside} ? 't' : 'f',
        "first_name"  => $session->{first_name},
        "last_name"   => $session->{last_name},
        "telephone"   => $session->{telephone},
        "fax"         => $session->{fax},
        "email"       => $session->{email},
        "priority"    => $session->{priority}
    );

    my $r = $self->Org_CreateTask($context, "todo", \%fields);
    if (!$r) {
        return $self->_CreateToDoTask_Form($context, $session, "CREATION_ERROR");
    }

    ##===========================================================================
    return $self->_TaskResult_Page($context, "create");

}

##******************************************************************************

=head2 EditStrictTask ()

	Edit a strict task

=head3 Generated XML :

   <EditStrictTask 
        <!-- Is Planned = 0 when task is not planned (without time) -->
        is_planned="1"

        <!-- Task Rowid (-1 for new tasks) -->
        taskrowid="-1"

        <!-- Task name -->
        name=""

        <!-- Start Date -->
        startHour="11"
        startMin="0"
	    day="20"
        month="5"
        year="2005"

        <!-- Stop hour -->
	    stopHour="12"
        stopMin="0"

	    <!-- Contact Related informations -->
        first_name=""
        last_name=""
        email=""
        telephone=""
        outside="0"
        fax=""

        <!-- The calling application URI -->
        referer="https://galadriel/Mioga2/Mioga/bin/admin/Organizer/DisplayMain"
    >

        <description>
        Task description
        </description>

        <task_category rowid="category rowid" fgcolor="text color" bgcolor="category bg color">
           category name 
        </task_category>     


   </EditStrictTask>


=head3 Form actions and fields :
    
	- change : Try to modify the task.

      Arguments :
         name                type                  description
       ---------------------------------------------------------------------
       taskrowid   :  not empty string      : the task rowid
       name        :  not empty string      : the task name
       is_planned  :  boolean               : is the task planned (with an hour) ?
       outside     :  boolean               : is the task outside ?
       startHour   :  string                : the task start hour
       startMin    :  string                : the task start minute
       day         :  string                : the task day of month number
       month       :  string                : the task month number
       year        :  string                : the task year number
       firstname   :  string                : the task contact first name
       lastname    :  string                : the task contact last name
       email       :  email                 : the task contact email
       telephone   :  string                : the task contact phone number
       fax         :  string                : the task contact fax number

	- contact : Launch contact selection


	- delete : Try to delete the task


=cut

##******************************************************************************
sub EditStrictTask {
    my ($self, $context) = @_;

    my $userTimeZone = $self->GetTimezone($context);

    ##===========================================================================
    ## Session management
    ##===========================================================================
    my $session = $context->GetSession();

    ## Initial form parameters
    if (exists($context->{args}{mode})
        && $context->{args}{mode} eq "initial")
    {
        $session->{organizer}{data} = {};
        $session = $session->{organizer}{data};
        foreach my $parameter (qw( taskrowid )) {
            return $self->_Error($context, __x("{parameter} not defined", parameter => $parameter))
              if (!defined($context->{args}{$parameter}));
        }

        if (exists $context->{args}->{referer}) {
            $session->{referer} = $context->{args}->{referer};
        }
        else {
            $session->{referer} = $context->GetReferer();
        }
        $session->{taskrowid} = $context->{args}{taskrowid};
        $session->{group_id} = int($context->{args}{resource_id}) || $context->GetGroup()->GetRowid();

        my $task;
        try {
            $task = $self->Org_GetTask($context, $session->{taskrowid}, 'planned', $session->{group_id});
        }
        catch Mioga2::Exception::Simple with {

            # error getting task
        };

        if (!defined($task)) {
            return $self->_TaskResult_Page($context, "unknown_task");
        }

        if ($task->{private} == 1 and $context->GetUser()->GetRowid() != $context->GetGroup()->GetAnimId()) {
            throw Mioga2::Exception::Simple("Mioga2::Organizer->EditStrictTask", __("can't edit a private task"));
        }

        ##------------------------------------------------------------------------
        ## Getting dates
        ##------------------------------------------------------------------------
        printf STDERR "Organizer::EditStrictTask is_planned = $task->{is_planned}\n";
        if ($task->{is_planned}) {
            if (    defined($task->{start})
                and defined($task->{duration}))
            {
                my ($day, $month, $year, $startHour, $startMin, $stopHour, $stopMin);
                {
                    local $ENV{TZ} = 'GMT';
                    tzset();
                    my ($taskYear, $taskMonth, $taskDay, $taskHour, $taskMin) = ($task->{start} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/);

                    my $startSec = strftime('%s', 0, $taskMin, $taskHour, $taskDay, $taskMonth - 1, $taskYear - 1900);
                    my $stopSec = $startSec + $task->{duration};
                    local $ENV{TZ} = $userTimeZone;
                    tzset();
                    ($day, $month, $year, $startHour, $startMin) = split(/:/, strftime("%d:%m:%Y:%H:%M", localtime($startSec)));
                    ($stopHour, $stopMin) = split(/:/, strftime("%H:%M", localtime($stopSec)));
                }
                tzset();

                $session->{day}       = $day;
                $session->{month}     = $month;
                $session->{year}      = $year;
                $session->{startHour} = $startHour;
                $session->{startMin}  = $startMin;
                $session->{stopHour}  = $stopHour;
                $session->{stopMin}   = $stopMin;
            }
            $session->{is_planned} = 1;
        }

        else {
            my ($taskYear, $taskMonth, $taskDay) = ($task->{start} =~ /^(\d+)-(\d+)-(\d+)/);
            $session->{is_planned} = 0;
            $session->{day}        = $taskDay;
            $session->{month}      = $taskMonth;
            $session->{year}       = $taskYear;
            $session->{startHour}  = $session->{startMin} = $session->{stopHour} = $session->{stopMin} = '';
        }

        ##------------------------------------------------------------------------
        $session->{name}         = $task->{name};
        $session->{description}  = $task->{description};
        $session->{category_id}  = $task->{category_id};
        $session->{type}         = $task->{type};
        $session->{outside}      = $task->{outside};
        $session->{first_name}   = $task->{first_name} || "";
        $session->{last_name}    = $task->{last_name} || "";
        $session->{telephone}    = $task->{telephone} || "";
        $session->{fax}          = $task->{fax} || "";
        $session->{email}        = $task->{email} || "";
        $session->{priority}     = $task->{priority} || "normal";
        $session->{waiting_book} = $task->{waiting_book} || 0;
    }
    ## We are not in the initial form
    else {
        throw Mioga2::Exception::Simple("Mioga2::Organizer->EditStrictTask", __("session->organizer not defined"))
          if (!exists($session->{organizer}{data}));

        $session = $session->{organizer}{data};

        my $task = $self->Org_GetTask($context, $session->{taskrowid}, 'planned', $session->{group_id});
        if ($task->{private} == 1 and $context->GetUser()->GetRowid() != $context->GetGroup()->GetAnimId()) {
            throw Mioga2::Exception::Simple("Mioga2::Organizer->EditStrictTask", __("can't edit a private task"));
        }

    }

    ##===========================================================================
    ## Initial form / next form  parameters
    ##===========================================================================
    foreach my $parameter (
        qw(
        day category_id month year startHour startMin stopHour stopMin
        taskrowid name description type first_name
        last_name telephone fax email priority
        )
      )
    {
        $session->{$parameter} = $context->{args}{$parameter}
          if (exists($context->{args}{$parameter}));

    }

    if (    exists $context->{args}->{name}
        and exists $context->{args}->{outside})
    {
        $session->{outside} = 1;
    }
    elsif (exists $context->{args}->{name}) {
        $session->{outside} = 0;
    }

    ##===========================================================================
    $self->_AddingContact($context, $session);

    if (   !defined($session->{day})
        or !defined($session->{month})
        or !defined($session->{year}))
    {
        local $ENV{TZ} = $userTimeZone;
        tzset();
        my ($min, $hour, $day, $month, $year) = split(/,/, strftime("%M,%H,%d,%m,%Y", localtime(time() + 5 * 60)));
        $min                  = 5 * int($min / 5);
        $session->{day}       = $day;
        $session->{month}     = $month;
        $session->{year}      = $year;
        $session->{startHour} = $hour;
        $session->{startMin}  = $min;

        ($min, $hour) = split(/,/, strftime("%M,%H", localtime(time() + 65 * 60)));
        $min                 = 5 * int($min / 5);
        $session->{stopHour} = $hour;
        $session->{stopMin}  = $min;
    }
    tzset();

    if (   !defined($session->{startHour})
        or !defined($session->{startMin})
        or !defined($session->{stopHour})
        or !defined($session->{stopMin}))
    {
        $session->{startHour} = $session->{startMin} = $session->{stopHour} = $session->{stopMin} = '';
        $session->{is_planned} = 0;
    }
    elsif ( ($session->{startHour} eq '')
        and ($session->{startMin} eq '')
        and ($session->{stopHour} eq '')
        and ($session->{stopMin}  eq ''))
    {
        $session->{is_planned} = 0;
    }
    else {
        $session->{is_planned} = 1;
    }

    $session->{first_name} = ""       if (!exists($session->{first_name}));
    $session->{last_name}  = ""       if (!exists($session->{last_name}));
    $session->{telephone}  = ""       if (!exists($session->{telephone}));
    $session->{fax}        = ""       if (!exists($session->{fax}));
    $session->{email}      = ""       if (!exists($session->{email}));
    $session->{priority}   = 'normal' if (!exists($session->{priority}));

    try {
        my $appdesc     = new Mioga2::AppDesc($context->GetConfig(), ident => 'Contact');
        my $application = $appdesc->CreateApplication();
        my $test        = $application->CheckUserAccessOnMethod($context, $context->GetUser(), $context->GetGroup(), 'SearchEngine');

        $session->{display_icon} = ($test == AUTHZ_OK);
    }

    otherwise {
        $session->{display_icon} = 0;
    };

    ##===========================================================================
    ## Deleting
    ##===========================================================================
    if (st_ArgExists($context, "delete")) {

        if ($context->GetConfig()->AskConfirmForDelete()
            and !exists($session->{confirm}))
        {

            $session->{confirm} = 1;
            return $self->_ConfirmDeletion_Page($context, "EditStrictTask?delete.x=0&amp;taskrowid=$context->{args}{taskrowid}", $session->{type}, 'EditStrictTask');
        }

        my $r = $self->Org_DeleteTask($context, $session->{taskrowid}, $context->GetGroup()->GetRowid());
        if (!$r) {
            return $self->_EditStrictTask_Form($context, $session, "DELETION_ERROR");
        }

        return $self->_TaskResult_Page($context, "delete");
    }
    ##===========================================================================
    ## If contact : save parameters to session and call contact
    ##===========================================================================
    elsif (st_ArgExists($context, "contact")) {
        my $uri = new Mioga2::URI(
            $context->GetConfig(),
            group       => $context->GetGroup()->GetIdent(),
            public      => 0,
            application => 'Contact',
            method      => 'SearchEngine',
            args        => {
                action  => 'Contact',
                referer => $context->GetURI()->GetURI()
            }
        );

        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent($uri->GetURI());
        return $content;
    }
    ##===========================================================================
    ## Gererate the inital form
    ##===========================================================================
    elsif (!st_ArgExists($context, "change") and !st_ArgExists($context, "transform_todo")) {
        return $self->_EditStrictTask_Form($context, $session, "OK");
    }

    ##===========================================================================
    ## Modification
    ##===========================================================================
    return $self->_EditStrictTask_Form($context, $session, "NAME_EMPTY")
      if ($session->{name} =~ /^\s*$/);

    my %fields = (
        "group_id"    => $session->{group_id},
        "name"        => $session->{name},
        "category_id" => $session->{category_id},
        "description" => $session->{description},
        "outside"     => $session->{outside} ? 't' : 'f',
        "first_name"  => $session->{first_name},
        "last_name"   => $session->{last_name},
        "telephone"   => $session->{telephone},
        "fax"         => $session->{fax},
        "email"       => $session->{email},
    );

    ##---------------------------------------------------------------------------
    ## Modification of a planned task
    ##---------------------------------------------------------------------------
    my $r;

    if (st_ArgExists($context, "change")) {
        foreach my $p (qw(month day startHour startMin stopHour stopMin)) {
            $session->{$p} = sprintf("%02d", $session->{$p}) if ($session->{$p} ne '');
        }

        my $duration_value;
        my $startDate;
        if ($session->{is_planned}) {
            ##------------------------------------------------------------------------
            my $duration = du_GetMonthDuration($session->{month}, $session->{year});
            if ($session->{day} > $duration) {
                $session->{day} = $duration;
                return $self->_EditStrictTask_Form($context, $session, "BAD_DAY_FOR_MONTH");
            }

            ##------------------------------------------------------------------------
            my ($startSec, $stopDate, $stopSec);
            {
                local $ENV{TZ} = $userTimeZone;
                tzset();
                $startSec = strftime('%s', 0, $session->{startMin}, $session->{startHour}, $session->{day}, $session->{month} - 1, $session->{year} - 1900);
                local $ENV{TZ} = 'GMT';
                tzset();
                $startDate = strftime("%Y-%m-%d %H:%M:%S.00 GMT", gmtime($startSec));

                local $ENV{TZ} = $userTimeZone;
                tzset();
                $stopSec = strftime('%s', 0, $session->{stopMin}, $session->{stopHour}, $session->{day}, $session->{month} - 1, $session->{year} - 1900);
                local $ENV{TZ} = 'GMT';
                tzset();
                $stopDate = strftime("%Y-%m-%d %H:%M:%S.00 GMT", gmtime($stopSec));
            }
            tzset();

            ##------------------------------------------------------------------------
            $duration_value = ($stopSec - $startSec);
            return $self->_EditStrictTask_Form($context, $session, "START_TIME_AFTER")
              if ($duration_value <= 0);
        }
        else {

            $startDate = "$session->{year}-$session->{month}-$session->{day}";

            $duration_value = 0;
        }

        ##------------------------------------------------------------------------
        $fields{start}      = $startDate;
        $fields{duration}   = $duration_value;
        $fields{is_planned} = $session->{is_planned};

        $r = $self->Org_ChangeTask($context, $session->{taskrowid}, 'planned', $session->{group_id}, \%fields);
    }
    ##---------------------------------------------------------------------------
    ## unplannify a todo task
    ##---------------------------------------------------------------------------
    elsif (st_ArgExists($context, "transform_todo")) {
        $fields{priority} = $session->{priority};

        $r = $self->Org_ChangeTask_NewType($context, $session->{taskrowid}, 'todo', $session->{group_id}, \%fields);
    }
    ##------------------------------------------------------------------------
    ## Calling the modification method
    ##---------------------------------------------------------------------------

    if (!$r) {
        return $self->_EditStrictTask_Form($context, $session, "CHANGING_ERROR");
    }
    return $self->_TaskResult_Page($context, "modify");

}

##******************************************************************************

=head2 EditTodoTask ()

	Edit a Todo task.

=head3 Generated XML :
    
<EditTodoTask
   <!-- Task Rowid (-1 for new tasks) -->
   taskrowid="-1"

   <!-- Todo task priority (very urgent, urgent, normal, not urgent) -->
   priority="normal"

   <!-- Name and outside parameter of todo tasks -->
   name=""
   outside="0"

   <!-- Task contact -->
   first_name=""
   last_name=""
   email=""
   telephone=""
   fax=""

   referer="Calling application URL"
  >

    <description>
      Task description
    </description>

    <task_category rowid="category rowid" fgcolor="text color" bgcolor="category bg color">
        category name 
    </task_category>

</EditTodoTask>

    
	- change : Try to modify the task.

      Arguments :
         name                type                  description
       ---------------------------------------------------------------------
       taskrowid   :  not empty string      : the task rowid
       name        :  not empty string      : the task name
       priority    :  not empty string      : the task priority
       is_planned  :  boolean               : is the task planned (with an hour) ?
       outside     :  boolean               : is the task outside ?
       firstname   :  string                : the task contact first name
       lastname    :  string                : the task contact last name
       email       :  email                 : the task contact email
       telephone   :  string                : the task contact phone number
       fax         :  string                : the task contact fax number


	- transform_strict : Transform the todo task to planned task

      Arguments :
         name                type                  description
       ---------------------------------------------------------------------
       taskrowid   :  not empty string      : the task rowid
       startHour   :  string                : the task start hour
       startMin    :  string                : the task start minute
       day         :  string                : the task day of month number
       month       :  string                : the task month number
       year        :  string                : the task year number


	- contact : Launch contact selection


    - delete : Try to delete the task

=cut

##******************************************************************************
sub EditTodoTask {
    my ($self, $context) = @_;

    my $userTimeZone = $self->GetTimezone($context);

    ##===========================================================================
    ## Session management
    ##===========================================================================
    my $session = $context->GetSession();

    ## Initial form parameters
    if (exists($context->{args}{mode})
        && $context->{args}{mode} eq "initial")
    {
        $session->{organizer}{data} = {};
        $session = $session->{organizer}{data};
        foreach my $parameter (qw( taskrowid )) {
            return $self->_Error($context, "$parameter not defined")
              if (!defined($context->{args}{$parameter}));
        }
        $session->{referer}   = $context->GetReferer;
        $session->{taskrowid} = $context->{args}{taskrowid};

        my $task;
        try {
            $task = $self->Org_GetTask($context, $session->{taskrowid}, 'todo', $context->GetGroup()->GetRowid());
        }
        catch Mioga2::Exception::Simple with {

            # error getting task
        };

        if (!defined($task)) {
            return $self->_TaskResult_Page($context, "unknown_task");
        }

        if ($task->{private} == 1 and $context->GetUser()->GetRowid() != $context->GetGroup()->GetAnimId()) {
            throw Mioga2::Exception::Simple("Mioga2::Organizer->EditTodoTask", __("can't edit a private task"));
        }

        {
            local $ENV{TZ} = $userTimeZone;
            tzset();

            ($session->{day}, $session->{month}, $session->{year}, $session->{startHour}, $session->{startMin}) = split(':', strftime("%d:%m:%Y:%H:%M", localtime(time())));

            $session->{startMin} = int($session->{startMin} / 5) * 5;

            $session->{stopHour} = $session->{startHour} + 1;
            $session->{stopMin}  = $session->{startMin};
        }
        tzset();

        ##------------------------------------------------------------------------
        $session->{name}        = $task->{name};
        $session->{description} = $task->{description};
        $session->{category_id} = $task->{category_id};
        $session->{type}        = $task->{type};
        $session->{outside}     = $task->{outside};
        $session->{first_name}  = $task->{first_name} || "";
        $session->{last_name}   = $task->{last_name} || "";
        $session->{telephone}   = $task->{telephone} || "";
        $session->{fax}         = $task->{fax} || "";
        $session->{email}       = $task->{email} || "";
        $session->{priority}    = $task->{priority} || "normal";
    }
    else {
        throw Mioga2::Exception::Simple("Mioga2::Organizer->EditTodoTask", __("session->organizer not defined"))
          if (!exists($session->{organizer}{data}));

        $session = $session->{organizer}{data};
    }

    ##===========================================================================
    ## Initial form / next form  parameters
    ##===========================================================================
    foreach my $parameter (
        qw(
        category_id
        taskrowid name description type first_name
        last_name telephone fax email priority day month year startMin startHour stopMin stopHour
        )
      )
    {
        $session->{$parameter} = $context->{args}{$parameter}
          if (exists($context->{args}{$parameter}));
    }

    if (    exists $context->{args}->{name}
        and exists $context->{args}->{outside})
    {
        $session->{outside} = 1;
    }
    elsif (exists $context->{args}->{name}) {
        $session->{outside} = 0;
    }

    ##===========================================================================
    $self->_AddingContact($context, $session);

    $session->{first_name} = ""       if (!exists($session->{first_name}));
    $session->{last_name}  = ""       if (!exists($session->{last_name}));
    $session->{telephone}  = ""       if (!exists($session->{telephone}));
    $session->{fax}        = ""       if (!exists($session->{fax}));
    $session->{email}      = ""       if (!exists($session->{email}));
    $session->{priority}   = 'normal' if (!exists($session->{priority}));

    try {
        my $appdesc     = new Mioga2::AppDesc($context->GetConfig(), ident => 'Contact');
        my $application = $appdesc->CreateApplication();
        my $test        = $application->CheckUserAccessOnMethod($context, $context->GetUser(), $context->GetGroup(), 'SearchEngine');

        $session->{display_icon} = ($test == AUTHZ_OK);
    }

    otherwise {
        $session->{display_icon} = 0;
    };

    ##===========================================================================
    ## If contact : save parameters to session and call contact
    ##===========================================================================
    if (st_ArgExists($context, "contact")) {
        my $uri = new Mioga2::URI(
            $context->GetConfig(),
            group       => $context->GetGroup()->GetIdent(),
            public      => 0,
            application => 'Contact',
            method      => 'SearchEngine',
            args        => {
                action  => 'Contact',
                referer => $context->GetURI()->GetURI()
            }
        );

        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent($uri->GetURI());
        return $content;
    }
    ##===========================================================================
    ## Gererate the inital form
    ##===========================================================================
    elsif (!st_ArgExists($context, "change") and !st_ArgExists($context, "delete") and !st_ArgExists($context, "transform_strict")) {
        return $self->_EditTodoTask_Form($context, $session, "OK");
    }

    ##===========================================================================
    ## Modification
    ##===========================================================================
    return $self->_EditTodoTask_Form($context, $session, "NAME_EMPTY")
      if ($session->{name} =~ /^\s*$/);

    my %fields = (
        "group_id"    => $context->GetGroup()->GetRowid(),
        "name"        => $session->{name},
        "category_id" => $session->{category_id},
        "description" => $session->{description},
        "outside"     => $session->{outside} ? 't' : 'f',
        "priority"    => $session->{priority},
        "first_name"  => $session->{first_name},
        "last_name"   => $session->{last_name},
        "telephone"   => $session->{telephone},
        "fax"         => $session->{fax},
        "email"       => $session->{email},
    );

    ##---------------------------------------------------------------------------
    ## Modification of a todo task
    ##---------------------------------------------------------------------------
    my $r;

    if (st_ArgExists($context, "change")) {
        $r = $self->Org_ChangeTask($context, $session->{taskrowid}, 'todo', $context->GetGroup()->GetRowid(), \%fields);
    }
    ##===========================================================================
    ## Deleting
    ##===========================================================================
    elsif (st_ArgExists($context, "delete")) {

        if ($context->GetConfig()->AskConfirmForDelete()
            and !exists($session->{confirm}))
        {

            $session->{confirm} = 1;
            return $self->_ConfirmDeletion_Page($context, "EditTodoTask?delete.x=0&amp;taskrowid=$context->{args}{taskrowid}", $session->{type}, 'EditTodoTask');
        }

        my $r = $self->Org_DeleteTask($context, $session->{taskrowid}, $context->GetGroup()->GetRowid());
        if (!$r) {
            return $self->_EditTodoTask_Form($context, $session, "DELETION_ERROR");
        }

        return $self->_TaskResult_Page($context, "delete");
    }
    ##---------------------------------------------------------------------------
    ## unplannify a todo task
    ##---------------------------------------------------------------------------
    elsif (st_ArgExists($context, "transform_strict")) {
        delete $fields{priority};

        ##------------------------------------------------------------------------
        my ($startSec, $startDate, $stopDate, $stopSec);
        {
            local $ENV{TZ} = $userTimeZone;
            tzset();
            $startSec = strftime('%s', 0, $session->{startMin}, $session->{startHour}, $session->{day}, $session->{month} - 1, $session->{year} - 1900);
            local $ENV{TZ} = 'GMT';
            tzset();
            $startDate = strftime("%Y-%m-%d %H:%M:%S.00 GMT", gmtime($startSec));

            local $ENV{TZ} = $userTimeZone;
            tzset();
            $stopSec = strftime('%s', 0, $session->{stopMin}, $session->{stopHour}, $session->{day}, $session->{month} - 1, $session->{year} - 1900);
            local $ENV{TZ} = 'GMT';
            tzset();
            $stopDate = strftime("%Y-%m-%d %H:%M:%S.00 GMT", gmtime($stopSec));
        }
        tzset();

        ##------------------------------------------------------------------------
        my $duration_value = ($stopSec - $startSec);
        return $self->_EditTodoTask_Form($context, $session, "START_TIME_AFTER")
          if ($duration_value <= 0);

        ##------------------------------------------------------------------------
        $fields{start}    = $startDate;
        $fields{duration} = $duration_value;

        $r = $self->Org_ChangeTask_NewType($context, $session->{taskrowid}, 'planned', $context->GetGroup()->GetRowid(), \%fields);
    }
    ##------------------------------------------------------------------------
    ## Calling the modification method
    ##---------------------------------------------------------------------------

    if (!$r) {
        return $self->_EditTodoTask_Form($context, $session, "CHANGING_ERROR");
    }
    return $self->_TaskResult_Page($context, "modify");

}

##******************************************************************************

=head2 ViewTodoTask ()

	Edit a Todo task.

=head3 Generated XML :
    
<ViewTodoTask
   <!-- Task Rowid (-1 for new tasks) -->
   taskrowid="-1"

   <!-- Todo task priority (very urgent, urgent, normal, not urgent) -->
   priority="normal"

   <!-- Name and outside parameter of todo tasks -->
   name=""
   outside="0"

   <!-- Task contact -->
   first_name=""
   last_name=""
   email=""
   telephone=""
   fax=""

   <!-- Category description -->
   fgcolor="#XXXXXX"
   bgcolor="#XXXXXX"
   private="0|1"

   referer="Calling application URL"
  >

    <description>
      Task description
    </description>

</ViewTodoTask>

=cut

##******************************************************************************
sub ViewTodoTask {
    my ($self, $context) = @_;

    my $userTimeZone = $context->GetUser()->GetRowid();
    my $session      = $context->GetSession();

    $session->{organizer}{data} = {};

    return $self->_Error($context, __("taskrowid not defined"))
      if (!defined($context->{args}{taskrowid}));
    my $taskrowid = $context->{args}{taskrowid};

    my $task;
    try {
        $task = $self->Org_GetTask($context, $taskrowid, 'todo', $context->GetGroup()->GetRowid());
    }
    catch Mioga2::Exception::Simple with {

        # error getting task
    };

    if (!defined($task)) {
        return $self->_TaskResult_Page($context, "unknown_task");
    }

    $task->{referer} = $context->GetReferer();

    if ($task->{private} == 1 and $context->GetUser()->GetRowid() != $context->GetGroup()->GetAnimId()) {
        throw Mioga2::Exception::Simple("Mioga2::Organizer->ViewStrictTask", __("can't edit a private task"));
    }

    delete($task->{modified});
    delete($task->{created});

    return $self->_ViewTodoTask_Form($context, $task, "OK");

}

##******************************************************************************

=head2 ViewStrictTask ()

	Show a Planned task.

=head3 Generated XML :
    
   <ViewStrictTask 
        <!-- Is Planned = 0 when task is not planned (without time) -->
        is_planned="1"

        <!-- Task Rowid (-1 for new tasks) -->
        taskrowid="-1"

        <!-- Task name -->
        name=""

        <!-- Start Date -->
        startHour="11"
        startMin="0"
	    day="20"
        month="5"
        year="2005"

        <!-- Stop date for periodic tasks -->
	    endMonth="0"
        endDay="0"
        endYear="0"

	    <!-- Contact Related informations -->
        first_name=""
        last_name=""
        email=""
        telephone=""
        outside="0"
        fax=""


        <!-- Category description -->
        fgcolor="#XXXXXX"
        bgcolor="#XXXXXX"
        private="0|1"

        <!-- The calling application URI -->
        referer="https://galadriel/Mioga2/Mioga/bin/admin/Organizer/DisplayMain"
    >

        <description>
        Task description
        </description>
    </ViewStrictTask>

=cut

##******************************************************************************
sub ViewStrictTask {
    my ($self, $context) = @_;

    my $userTimeZone = $context->GetUser()->GetTimezone();
    my $session      = $context->GetSession();

    $session->{organizer}{data} = {};

    return $self->_Error($context, __("taskrowid not defined"))
      if (!defined($context->{args}{taskrowid}));
    my $taskrowid = $context->{args}{taskrowid};

    my $task;
    try {
        $task = $self->Org_GetTask($context, $taskrowid, 'planned', $context->GetGroup()->GetRowid());
    }
    catch Mioga2::Exception::Simple with {

        # error getting task
    };

    if (!defined($task)) {
        return $self->_TaskResult_Page($context, "unknown_task");
    }

    $task->{referer}    = $context->GetReferer();
    $task->{escreferer} = st_URIEscape($task->{referer});

    if ($task->{private} == 1 and $context->GetUser()->GetRowid() != $context->GetGroup()->GetAnimId()) {
        throw Mioga2::Exception::Simple("Mioga2::Organizer->ViewStrictTask", __("can't edit a private task"));
    }

    ##------------------------------------------------------------------------
    ## Getting dates
    ##------------------------------------------------------------------------
    if (    defined($task->{start})
        and defined($task->{duration}))
    {
        my ($day, $month, $year, $startHour, $startMin, $stopHour, $stopMin);
        {
            local $ENV{TZ} = 'GMT';
            tzset();
            my ($taskYear, $taskMonth, $taskDay, $taskHour, $taskMin) = ($task->{start} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/);
            my $startSec = strftime('%s', 0, $taskMin, $taskHour, $taskDay, $taskMonth - 1, $taskYear - 1900);
            my $stopSec = $startSec + $task->{duration};
            local $ENV{TZ} = $userTimeZone;
            tzset();
            ($day, $month, $year, $startHour, $startMin) = split(/:/, strftime("%d:%m:%Y:%H:%M", localtime($startSec)));
            ($stopHour, $stopMin) = split(/:/, strftime("%H:%M", localtime($stopSec)));
        }
        tzset();

        $task->{day}       = $day;
        $task->{month}     = $month;
        $task->{year}      = $year;
        $task->{startHour} = $startHour;
        $task->{startMin}  = $startMin;
        $task->{stopHour}  = $stopHour;
        $task->{stopMin}   = $stopMin;
        delete($task->{start});
        delete($task->{duration});
    }

    delete($task->{modified});
    delete($task->{created});

    return $self->_ViewStrictTask_Form($context, $task, "OK");

}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

##******************************************************************************
## Method _YearInterval  private
##******************************************************************************
sub _YearInterval {
    my ($self, $context, $sessionYear) = @_;

    my $actualYear;
    {
        local $ENV{'TZ'} = $self->GetTimezone($context);
        tzset();
        (undef, undef, undef, undef, undef, $actualYear, undef, undef) = localtime(time() + 5 * 60);
    }
    tzset();

    $actualYear += 1900;
    my $startYear = $actualYear;
    my $stopYear  = $actualYear;
    $startYear = $sessionYear if ($sessionYear < $startYear);
    $stopYear  = $sessionYear if ($sessionYear > $stopYear);
    $startYear -= 1;
    $stopYear += 3;

    return ($startYear, $stopYear);

}

##******************************************************************************
## Method _Task_GenericForm  private
##******************************************************************************
sub _Task_GenericForm {
    my ($self, $context, $session, $message, $tag, $stylesheet) = @_;

    my $dbh = $context->GetConfig()->GetDBH();

    $stylesheet = 'organizer.xsl' if (!defined($stylesheet));

    my $xml = "<?xml version=\"1.0\"?>\n";
    $xml .= "<$tag\n";

    while (my ($k, $v) = each(%$session)) {
        next if ($k eq "description");
        $xml .= "   $k=\"" . st_FormatXMLString($v) . "\"\n";
    }

    my ($startYear, $stopYear) = $self->_YearInterval($context, $session->{year});
    $xml .= "   startYear=\"$startYear\"\n" . "   stopYear=\"$stopYear\"\n" . "   message=\"$message\"" . ">\n";

    my $task_cat_list = $self->Org_TaskCatGetList($context);

    foreach my $cat (@$task_cat_list) {
        $xml .= "<task_category rowid=\"$cat->{rowid}\" fgcolor=\"$cat->{fgcolor}\" bgcolor=\"$cat->{bgcolor}\">" . st_FormatXMLString($cat->{name}) . "</task_category>\n";
    }

    $xml .= $context->GetXML();
    $xml .= "<description>" . st_FormatXMLString($session->{description}) . "</description>\n";
    $xml .= "</$tag>";

    if ($message ne "OK") {
        print STDERR "Mioga2::Organizer::User->_Task_GenericForm: User error: '$message'\n";
    }

    return $self->_MiogaResponse($context, $xml, $stylesheet);
}

##******************************************************************************
## Method _EditStrictTask_Form  private
##******************************************************************************
sub _EditStrictTask_Form {
    my ($self, $context, $session, $message) = @_;

    for my $attr (qw( month day startHour startMin stopHour stopMin )) {
        $session->{$attr} = int($session->{$attr}) if ($session->{$attr} ne '');
    }

    return $self->_Task_GenericForm($context, $session, $message, "EditStrictTask");
}

##******************************************************************************
## Method _EditTodoTask_Form  private
##******************************************************************************
sub _EditTodoTask_Form {
    my ($self, $context, $session, $message) = @_;

    return $self->_Task_GenericForm($context, $session, $message, "EditTodoTask");
}

##******************************************************************************
## Method _CreateStrictOrPeriodicTask_Form  private
##******************************************************************************
sub _CreateStrictOrPeriodicTask_Form {
    my ($self, $context, $session, $message, $adding) = @_;

    for my $attr (
        qw( month day startHour startMin stopHour stopMin
        endDay endMonth endYear dayFreq weekFreq monthFreq yearFreq
        weekWeekDay monthDay monthWeek monthWeekDay yearDay yearMonth)
      )
    {
        if ($session->{$attr} ne '') {
            $session->{$attr} = int($session->{$attr});
        }
    }

    return $self->_Task_GenericForm($context, $session, $message, "CreateStrictOrPeriodicTask$adding");
}

##******************************************************************************
## Method _CreateToDoTask_Form  private
##******************************************************************************
sub _CreateToDoTask_Form {
    my ($self, $context, $session, $message) = @_;
    return $self->_Task_GenericForm($context, $session, $message, "CreateToDoTask");
}

##******************************************************************************
## Method _ViewTodoTask_Form  private
##******************************************************************************
sub _ViewTodoTask_Form {
    my ($self, $context, $session, $message) = @_;

    return $self->_Task_GenericForm($context, $session, $message, "ViewTodoTask");
}

##******************************************************************************
## Method _ViewStrictTask_Form  private
##******************************************************************************
sub _ViewStrictTask_Form {
    my ($self, $context, $session, $message) = @_;

    for my $attr (qw( month day startHour stopHour )) {
        $session->{$attr} = int($session->{$attr});
    }

    if ($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), 'Write')) {
        $session->{canwrite} = 1;
    }

    return $self->_Task_GenericForm($context, $session, $message, "ViewStrictTask");
}

##******************************************************************************
## Method _TaskResult_Page  private
##******************************************************************************
sub _TaskResult_Page {
    my ($self, $context, $action) = @_;

    my $session = $context->GetSession();

    my $referer = $session->{organizer}{data}->{referer};
    delete($session->{organizer}{data});

    for ($action) {
        if (/create/) {
            $session->{__internal}->{message}{type} = 'info';
            $session->{__internal}->{message}{text} = __('Task successfully created.');
        }
        elsif (/delete/) {
            $session->{__internal}->{message}{type} = 'info';
            $session->{__internal}->{message}{text} = __('Task successfully deleted.');
        }
        elsif (/modify/) {
            $session->{__internal}->{message}{type} = 'info';
            $session->{__internal}->{message}{text} = __('Task successfully modified.');
        }
        elsif (/unknown_task/) {
            $session->{__internal}->{message}{type} = 'error';
            $session->{__internal}->{message}{text} = __('Unknown task (or newly deleted).');
        }
    }

    my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
    $content->SetContent($referer);

    return $content;
}

##******************************************************************************
## Method _ConfirmDeletion_Page  private
##******************************************************************************
sub _ConfirmDeletion_Page {
    my ($self, $context, $method, $label, $referer) = @_;

    my $session = $context->GetSession();

    if (!defined $referer) {
        $referer = st_FormatXMLString($session->{organizer}{data}->{referer});
    }

    my $xml = "<?xml version=\"1.0\"?>\n";
    $xml .= "<ConfirmDeletion link=\"$referer\" method=\"$method\" label=\"$label\">\n";
    $xml .= $context->GetXML();
    $xml .= "</ConfirmDeletion>\n";

    return $self->_MiogaResponse($context, $xml);
}

##******************************************************************************
## Method _AddingContact  private
##******************************************************************************
sub _AddingContact {
    my ($self, $context, $session) = @_;

    if (    exists($context->{args}{action})
        and $context->{args}{action} eq "Contact"
        and exists($context->{args}{id}))
    {
        my $ctcapp = new Mioga2::Contact;
        my $contact = $ctcapp->ContactGetContact($context, $context->{args}{id});
        $session->{first_name} = $contact->{surname}    || "";
        $session->{last_name}  = $contact->{name}       || "";
        $session->{telephone}  = $contact->{tel_work}   || $contact->{tel_mobile} || $contact->{tel_home} || "";
        $session->{fax}        = $contact->{tel_fax}    || "";
        $session->{email}      = $contact->{email_work} || $contact->{email_home} || "";
    }

}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
