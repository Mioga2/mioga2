# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Organizer.pm: The Mioga2 Organizer application

=head1 METHODS DESRIPTION

=cut

package Mioga2::Organizer;

use strict;
use Mioga2::AppDesc;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::date_utils;
use Error qw(:try);
use Data::Dumper;
use POSIX qw(tzset);


# ============================================================================

=head2 ViewDay ()

	Main function for the Day view in organizer.

=head3 Generated XML
	
  <!-- The Root element parameters describe :
       day : the current day of month
       dayNumber : the current day of week (1=monday, 7=sunday)

       isWorkingDay : set to 1 is the currently selected day is worked.
       canWrite : set to 1 if the current user can write in the current Organizer.


<ViewDay day="06" dayNumber="5" 
         now_day="06" now_month="05" now_year="2005" now_hour="11" now_minute="19" 
         isWorkingDay="1" canWrite="1">


<!-- 

  Standard View* Headers. See Mioga2::Organizer::DisplayMain 

-->



<!--
     Description of tasks

     flexibleTasks parameters contains the number of flexible tasks to display.
     -->

<DayTasks flexibleTasks="1">
  
  <!-- 
       For each time interval (depends of user settings. By default it s 1 hour),
       a DayTask entry is present.

       Parameters of DayTask always present :
       printTime : if = 1, print just start hour of time interval. If set to 2, print start and stop hour of time interval.
       planned : 1 for "normal" time interval, 0 for "not planned task" entry.

       rowspan : if a task take more than one time interval, rowspan indicate 
                 the number of time interval used by the task.
                 Ex : if time interval is 1 hours, a task of 2 hours have rowspan=2

       method : method to call when the cells is clicked. For empty cells, it's CreateStrictOrPeriodicTask
                for cells containing tasks it's EditStrictTask or EditPeriodicTask.

       conflict : set to 1 if the time interval have task conflict.

       startHour, startMin : start hour and minutes of time interval.
       stopHour, stopMin : stop hour and minutes of time interval.
       
       taskrowid: rowid of task contained in time interval (-1 when no task is present)

       private : set to 1 is the time interval contains a private task.
       bgcolor : background color of task category (empty when no task is present)
       fgcolor : text color of task category (empty when no task is present)

       first_name, last_name  : first and last name of task contact. Empty if no contact is supplied or no task is present.

       The content of DayTask is the task name.

       -->

  <DayTask  printTime="0" planned="0" private="0" rowspan="1" method="CreateStrictOrPeriodicTask">
  </DayTask>

  <DayTask conflict="0" printTime="1" planned="1" 
           startHour="08" startMin="00" stopHour="09" stopMin="00" 
           taskrowid="-1" method="CreateStrictOrPeriodicTask" rowspan="1" 
           private="0" bgcolor="" fgcolor="" 
           first_name="" last_name="">
  </DayTask>

  <DayTask conflict="0" printTime="1" planned="1" 
           startHour="09" startMin="00" stopHour="10" stopMin="00" 
           taskrowid="-1" method="CreateStrictOrPeriodicTask" rowspan="1" 
           private="0" bgcolor="" fgcolor="" 
           first_name="" last_name="">
  </DayTask>
  
  <DayTask conflict="0" printTime="2" planned="1" 
           startHour="10" startMin="00" stopHour="11" stopMin="00" 
           taskrowid="1" method="EditStrictTask" rowspan="1" 
           private="0" bgcolor="#CCCCCC" fgcolor="#000000" 
           first_name="Foo" last_name="Bar">
      task 1
  </DayTask>

  <DayTask conflict="0" printTime="1" planned="1" 
           startHour="11" startMin="00" stopHour="12" stopMin="00"
           taskrowid="-1" method="CreateStrictOrPeriodicTask" rowspan="1" 
           private="0" bgcolor="" fgcolor=""
           first_name="" last_name="">
  </DayTask>

  <DayTask conflict="0" printTime="1" planned="1" 
           startHour="12" startMin="00" stopHour="13" stopMin="00"
           taskrowid="-1" method="CreateStrictOrPeriodicTask" rowspan="1"
           private="0" bgcolor="" fgcolor=""
           first_name="" last_name="">
  </DayTask>
  
  <DayTask conflict="0" printTime="1" planned="1" 
           startHour="13" startMin="00" stopHour="14" stopMin="00"
           taskrowid="-1" method="CreateStrictOrPeriodicTask" rowspan="1"
           private="0" bgcolor="" fgcolor=""
           first_name="" last_name="">
  </DayTask>

  <DayTask conflict="1" printTime="2" planned="1" 
           startHour="14" startMin="00" stopHour="16" stopMin="00"
           taskrowid="2" method="EditStrictTask" rowspan="1"
           private="0" bgcolor="#CCCCCC" fgcolor="#000000"
           first_name="" last_name="">
      task 2
  </DayTask>

  <DayTask conflict="1" printTime="2" planned="1" 
           startHour="15" startMin="00" stopHour="17" stopMin="00"
           taskrowid="6" method="EditStrictTask" rowspan="1"
           private="0" bgcolor="#CCCCCC" fgcolor="#000000"
           first_name="" last_name="">
      Conflicting task
  </DayTask>

  <DayTask conflict="0" printTime="1" planned="1" 
           startHour="17" startMin="00" stopHour="18" stopMin="00"
           taskrowid="-1" method="CreateStrictOrPeriodicTask" rowspan="1"
           private="0" bgcolor="" fgcolor="" 
           first_name="" last_name=""></DayTask>

  <DayTask conflict="0" printTime="2" planned="1"
           startHour="18" startMin="00" stopHour="19" stopMin="00"
           taskrowid="4" method="EditPeriodicTask" rowspan="1"
           private="0" bgcolor="#CCCCCC" fgcolor="#000000" 
           first_name="" last_name="">
      Periodic Task
  </DayTask>
  
  <DayTask conflict="0" printTime="1" planned="1" 
           startHour="19" startMin="00" stopHour="20" stopMin="00"
           taskrowid="-1" method="CreateStrictOrPeriodicTask" rowspan="1"
           private="0" bgcolor="" fgcolor=""
           first_name="" last_name="">
  </DayTask>


  <!-- 
       Flexible tasks description 
       -->

  <FlexibleTasks>
      <FlexibleTask private="0" taskrowid="7" type="flexible" bgcolor="#CCCCCC" fgcolor="#000000" name="Flexible Task 1" />
      <FlexibleTask private="0" taskrowid="8" type="flexible" bgcolor="#CCCCCC" fgcolor="#000000" name="Flexible Task 2" />
  </FlexibleTasks>

</DayTasks>

</ViewDay>

=cut

# ============================================================================

sub ViewDay
{
   my ($self, $context) = @_;

   $context->{args}{method} = 'ViewDay';
   $self->InitializeViewState($context);

   my $session = $context->GetSession();
   my $config = $context->GetConfig();

   my $userTimeZone = $self->GetTimezone($context);

   my $test = $self->CheckUserAccessOnMethod($context, $context->GetUser(), 
											 $context->GetGroup(), 'EditStrictTask');

   my $canWrite = ($test == AUTHZ_OK);

   ##===========================================================================
   ## Arguments
   ##===========================================================================
   my ($day,$month,$year);
   my $format = "";
   if( defined($context->{args}{day}) )
   {
		my ($values, $errors);

      if( !defined( $context->{args}{month} ) )
      {
		($values, $errors) = ac_CheckArgs($context, [ [ [ 'day' ], 'stripxws', 'disallow_empty'], ]);
         ($year,$month,$day) = ( $context->{args}{day} =~ m/^([0-9]+)-([0-9]+)-([0-9]+)$/ );
         $format = "day=$context->{args}{day}";
      }
      else
      {
		($values, $errors) = ac_CheckArgs($context, [ [ [ 'day', 'month', 'year' ], 'stripxws', 'disallow_empty', 'want_int'], ]);
         $day   = $context->{args}{day};
         $month = $context->{args}{month};
         $year  = $context->{args}{year};
         $format = "day=$context->{args}{day},month=$context->{args}{month},".
                   "year=$context->{args}{year}";
      }
	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Organizer::ViewDAy", ac_FormatErrors($errors));
	}

   }
   elsif( exists( $session->{organizer}{viewState} ) )
   {
      ($year,$month,$day) = ( $session->{organizer}{viewState}{parameter} =~
                              m/^([0-9]+)-([0-9]+)-([0-9]+)$/ );
      $format = "viewState=$session->{organizer}{viewState}{parameter}";
   }
   else
   {
      return $self->_Error($context, __("no day!"));
   }

   ##===========================================================================
   if( !defined( $day ) or !defined( $month ) or !defined( $year ) or
       $day   <    1 or $day   >   31 or
       $month <    1 or $month >   12 )
   {
       return $self->_Error($context, __x("bad format for day: {format}", format => $format));
   }

   my $monthDuration = du_GetMonthDuration($month, $year);
   $day = $monthDuration if( $day > $monthDuration );

   $month = sprintf( "%02d", $month );
   $day = sprintf( "%02d", $day );

   ##===========================================================================
   ## Saving the current view-state
   ##===========================================================================
   my $viewState = $session->{organizer}{viewState} = {};
   $viewState->{method} = "ViewDay";
   $viewState->{parameter} = "$year-$month-$day";

   ##===========================================================================
   ## Getting tasks
   ##===========================================================================
   my ($local00Sec,$local23Sec);
   my ($local0000,$local2359);
   {
      local $ENV{TZ} = $userTimeZone;
	  tzset();
      $local00Sec = strftime('%s',  0,  0,  0, $day, $month-1, $year-1900 );
      $local23Sec = strftime('%s', 59, 59, 23, $day, $month-1, $year-1900 )+1;
      local $ENV{TZ} = 'GMT';
	  tzset();
      $local0000 = strftime("%Y-%m-%d %H:%M:%S GMT", gmtime($local00Sec));
      $local2359 = strftime("%Y-%m-%d %H:%M:%S GMT", gmtime($local23Sec));
   }
   tzset();

   my $taskList = $self->Org_ListStrictPeriodicTasks( $context, $context->GetGroup()->GetRowid(),
													  $userTimeZone, $local0000, $local2359 );

   my $notplannedtasks = $self->Org_ListNotPlannedTasks( $context, $context->GetGroup()->GetRowid(),
														 "$year-$month-$day", "$year-$month-$day");


   ##===========================================================================
   ## Tasks pre-treatement : creation of {start|stop}MinuteDay fields
   ##===========================================================================
   my $previvousTask = undef;
   foreach my $task (@$taskList)
   {
      my ($startYear,$startMonth,$startDay,$startHour,$startMin) = ( $task->{start} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
      my ($stopHour,$stopMin);
      {
         local $ENV{TZ} = 'GMT';
		 tzset();
         my $startSec = strftime('%s', 0, $startMin, $startHour, $startDay, $startMonth-1, $startYear-1900);
         my $stopSec = $startSec + $task->{duration};

         local $ENV{TZ} = $userTimeZone;
		 tzset();
         ($startHour,$startMin) = split( /:/, strftime("%H:%M", localtime($startSec)) );
         ($stopHour,$stopMin) = split( /:/, strftime("%H:%M", localtime($stopSec)) );
      }
	  tzset();


      if($task->{private} == 1 and $context->GetUser()->GetRowid() == $context->GetGroup()->GetAnimId()) {
		  $task->{private} = 0;
      }
   
      $task->{startMinuteDay} = $startMin + 60 * $startHour;
      $task->{startMinute} = $startMin;
      $task->{startHour} = $startHour;

      $task->{stopMinuteDay} = $stopMin + 60 * $stopHour;
      $task->{stopMinute} = $stopMin;
      $task->{stopHour} = $stopHour;

      ##------------------------------------------------------------------------
      ## conflicts detection
      ##------------------------------------------------------------------------
      if( defined( $previvousTask ) and
          $task->{startMinuteDay} < $previvousTask->{stopMinuteDay} )
      {
         $previvousTask->{conflict} = 1;
         $task->{conflict} = 1;
      }
      else
      {
         $task->{conflict} = 0;
      }
      $previvousTask = $task;
   }

   ##===========================================================================
   ## Starting with the tasks
   ##===========================================================================
   my $userPreferences = $self->Org_GetUserPreferences($context);
   my $lowerLimitMinuteDay = $userPreferences->{lowerLimitMinuteDay};
   my $upperLimitMinuteDay = $userPreferences->{upperLimitMinuteDay};
   my $timeIncrement       = $userPreferences->{timeIncrement};

   if( $#$taskList > -1 )
   {
      $lowerLimitMinuteDay = $taskList->[0]->{startMinuteDay}
	  if( $taskList->[0]->{startMinuteDay} < $lowerLimitMinuteDay );
      $upperLimitMinuteDay = $taskList->[$#$taskList]->{stopMinuteDay}
	  if( $taskList->[$#$taskList]->{stopMinuteDay} > $upperLimitMinuteDay );
   }

   ## sentinel :
   push @$taskList, { "startMinuteDay" => $upperLimitMinuteDay+8000 };

   my $startMinuteDay = $lowerLimitMinuteDay;
   my $taskIndex = 0;
   my $restartMinuteDay = $startMinuteDay;  # the moment where to restart after treating conflict tasks
   my $treatedTask = [];

   ##===========================================================================
   ## For each period of time
   ##===========================================================================
   while( $startMinuteDay < $upperLimitMinuteDay and
          $taskIndex <= $#$taskList )
   {
      my $stopMinuteDay = $startMinuteDay + $timeIncrement;
      $stopMinuteDay = $timeIncrement * int( $stopMinuteDay / $timeIncrement );

      ##------------------------------------------------------------------------
      ## The first task begin in the period of time
      ##------------------------------------------------------------------------
      if( $taskList->[$taskIndex]->{startMinuteDay} > $startMinuteDay and
          $taskList->[$taskIndex]->{startMinuteDay} < $stopMinuteDay )
      {
         $startMinuteDay = $restartMinuteDay;

         my $method;
         if( $canWrite ) { $method = "CreateStrictOrPeriodicTask"; }
         else            { $method = "none"; }

         my $startHour = sprintf( "%02d", int( $startMinuteDay / 60 ) );
         my $startMinute = sprintf( "%02d", $startMinuteDay - $startHour*60 );
         my $task = {
                      'conflict'       => 0,
                      'printTime'      => 2,
                      'startHour'      => $startHour,
                      'startMin'       => $startMinute,
                      'stopHour'       => $taskList->[$taskIndex]->{startHour},
                      'stopMin'        => $taskList->[$taskIndex]->{startMinute},
                      'private'        => $taskList->[$taskIndex]->{private},
                      'taskrowid'      => -1,
                      'method'         => "$method",
                      'rowspan'        => 1,
                      'name'           => "",
                      'startMinuteDay' => $startMinuteDay,
                      'stopMinuteDay'  => $taskList->[$taskIndex]->{startMinuteDay},
                      'flexibles'      => [],
					  'first_name'     => "",
					  'last_name'      => "",
                    };

		 push @$treatedTask, $task;
         $startMinuteDay = $taskList->[$taskIndex]->{startMinuteDay};
         ## the next test will be true ...

         $restartMinuteDay = $startMinuteDay;
      }

      ##------------------------------------------------------------------------
      ## The first task begin at the exact begining of the period of time
      ##------------------------------------------------------------------------
      if( $taskList->[$taskIndex]->{startMinuteDay} == $startMinuteDay )
      {
         my $method;
         my $tasktype =  $taskList->[$taskIndex]->{type};

         if( $canWrite )
         {
            if(    $tasktype eq "periodic" ) { $method = "EditPeriodicTask"; }
            else                             { $method = "EditStrictTask"; }
         }
         else
         {
            if(    $tasktype eq "periodic" ) { $method = "ViewPeriodicTask"; }
            else                             { $method = "ViewStrictTask"; }
         }


         my $task = {
                      'conflict'       => $taskList->[$taskIndex]->{conflict},
                      'printTime'      => 2,
                      'startHour'      => $taskList->[$taskIndex]->{startHour},
                      'startMin'       => $taskList->[$taskIndex]->{startMinute},
                      'stopHour'       => $taskList->[$taskIndex]->{stopHour},
                      'stopMin'        => $taskList->[$taskIndex]->{stopMinute},
                      'private'        => $taskList->[$taskIndex]->{private},
                      'taskrowid'      => $taskList->[$taskIndex]->{rowid},
                      'method'         => $method,
                      'rowspan'        => 1,
                      'name'           => $taskList->[$taskIndex]->{name},
                      'startMinuteDay' => $taskList->[$taskIndex]->{startMinuteDay},
                      'stopMinuteDay'  => $taskList->[$taskIndex]->{stopMinuteDay},
                      'bgcolor'          => $taskList->[$taskIndex]->{bgcolor},
                      'fgcolor'          => $taskList->[$taskIndex]->{fgcolor},
                      'flexibles'      => [],
					  'first_name'     => $taskList->[$taskIndex]->{first_name},
					  'last_name'      => $taskList->[$taskIndex]->{last_name},
                    };

   

		 push @$treatedTask, $task;
         $startMinuteDay = $taskList->[$taskIndex]->{stopMinuteDay};
         if( $taskList->[$taskIndex+1]->{startMinuteDay} < $startMinuteDay )
         {
            $startMinuteDay = $taskList->[$taskIndex+1]->{startMinuteDay};
         }

         if( $taskList->[$taskIndex]->{stopMinuteDay} > $restartMinuteDay )
         {
            $restartMinuteDay = $taskList->[$taskIndex]->{stopMinuteDay};
         }

         $taskIndex++;
      }
      ##------------------------------------------------------------------------
      ## There is no task in the period of time
      ##------------------------------------------------------------------------
      else
      {
         $startMinuteDay = $restartMinuteDay;

         my $method;
         if( $canWrite ) { $method = "CreateStrictOrPeriodicTask"; }
         else            { $method = "none"; }

         my $startHour = sprintf( "%02d", int( $startMinuteDay / 60 ) );
         my $startMinute = sprintf( "%02d", $startMinuteDay - $startHour*60 );
         my $stopHour = sprintf( "%02d", int( $stopMinuteDay / 60 ) );
         my $stopMinute = sprintf( "%02d", $stopMinuteDay - $stopHour*60 );
         my $task = {
                      'conflict'       => 0,
                      'printTime'      => 1,
                      'startHour'      => $startHour,
                      'startMin'       => $startMinute,
                      'stopHour'       => $stopHour,
                      'stopMin'        => $stopMinute,
                      'taskrowid'      => -1,
                      'method'         => "$method",
                      'rowspan'        => 1,
                      'name'           => "",
                      'startMinuteDay' => $startMinuteDay,
                      'stopMinuteDay'  => $stopMinuteDay,
                      'flexibles'      => [],
					  'private'        => 0,
                    };
         push @$treatedTask, $task;
         $startMinuteDay = $timeIncrement * int( $startMinuteDay / $timeIncrement );
         $startMinuteDay += $timeIncrement;

         $restartMinuteDay = $startMinuteDay;
      }

      ##------------------------------------------------------------------------
   }

   $taskList = undef;

   ##===========================================================================
   ## Flexible tasks : pre-treatement
   ##===========================================================================
   my $flexibleList = ();

   my $workingDaysHash = {};
   my $calendarXml = $self->_MonthCalendar($context, $month, $year, $day, $day, $workingDaysHash);
   my $workingDay = $workingDaysHash->{"$year-$month-$day"};

   if( $workingDay )
   {
      my ($start, $stop);
	  {
		  local $ENV{TZ} = $userTimeZone;
		  tzset();

		  my $startsec = du_ISOToSecond("$year-$month-$day 00:00:00");
		  my $stopsec  = du_ISOToSecond("$year-$month-$day 23:59:59");

		  $start = strftime("%Y-%m-%d %H:%M:%S", gmtime($startsec));
		  $stop = strftime("%Y-%m-%d %H:%M:%S", gmtime($stopsec));
	  }
	  tzset();
	  
	  $flexibleList = $self->Org_ListFlexibleTasks( $context, $context->GetGroup()->GetRowid(), $start, $stop);

      ##===========================================================================
      ## Flexible tasks : pre-treatement
      ##===========================================================================
      foreach my $task (@$flexibleList)
      {
		  my ($startYear,$startMonth,$startDay,$startHour,$startMin) = ( $task->{start} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
         {
            local $ENV{TZ} = 'GMT';
			tzset();
            my $startSec = strftime('%s', 0, $startMin, $startHour, $startDay, $startMonth-1, $startYear-1900);
            local $ENV{TZ} = $userTimeZone;
			tzset();
            ($startHour,$startMin) = split( /:/, strftime("%H:%M", localtime($startSec)) );
         }
         my ($stopYear,$stopMonth,$stopDay,$stopHour,$stopMin) = ( $task->{stop} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
         {
            local $ENV{TZ} = 'GMT';
			tzset();
            my $stopSec = strftime('%s', 0, $stopMin, $stopHour, $stopDay, $stopMonth-1, $stopYear-1900);
            local $ENV{TZ} = $userTimeZone;
			tzset();
            ($stopHour,$stopMin) = split( /:/, strftime("%H:%M", localtime($stopSec)) );
         }
		 tzset();

         $task->{startMinuteDay} = $startMin + 60 * $startHour;
         $task->{stopMinuteDay} = $stopMin + 60 * $stopHour;
         $task->{stopMinuteDay} = 24*60 if( $task->{stopMinuteDay} == 0 );

      }

      ##===========================================================================
      ## Flexible tasks : adding
      ##===========================================================================
      foreach my $task (@$flexibleList)
      {
         ##------------------------------------------------------------------------
         ## Seaching the time intervals
         ##------------------------------------------------------------------------
         my $startPeriod = undef;
         my $stopPeriod = undef;
         for( my $i=0; $i<=$#$treatedTask; $i++ )
         {
            if( !defined( $startPeriod ) and
                $task->{startMinuteDay} < $treatedTask->[$i]->{stopMinuteDay} )
            {
               $startPeriod = $i;
            }
            if( $task->{stopMinuteDay} <= $treatedTask->[$i]->{stopMinuteDay} )
            {
               $stopPeriod = $i;
               last;
            }
         }
         $startPeriod = $#$treatedTask if (!defined( $startPeriod ) );
         $stopPeriod  = $#$treatedTask if (!defined( $stopPeriod ) );
         $task->{startPeriod} = $startPeriod;
         $task->{stopPeriod}  = $stopPeriod;


		 if($task->{private} == 1 and $context->GetUser()->GetRowid() == $context->GetGroup()->GetAnimId()) {
			 $task->{private} = 0;
		 }

         ##------------------------------------------------------------------------
         ## Before flexible task
         ##------------------------------------------------------------------------
         if( $task->{startPeriod} != 0 )
         {
            my $flexible = {
                             'empty'    => 1,
                             'rowspan'  => $task->{startPeriod},
							 'bgcolor'    => $task->{bgcolor},
							 'fgcolor'    => $task->{fgcolor},
							 'private'  => $task->{private},
                           };
            push @{$treatedTask->[0]->{flexibles}}, $flexible;
         }

		 ##------------------------------------------------------------------------
         ## During flexible task
         ##------------------------------------------------------------------------
         my $flexible = {
                          'empty'    => 0,
                          'rowspan'  => $task->{stopPeriod} - $task->{startPeriod} + 1,
                          'conflict' => $task->{conflict},
						  'bgcolor'    => $task->{bgcolor},
						  'fgcolor'    => $task->{fgcolor},
						  'private'  => $task->{private},
                        };
         push @{$treatedTask->[ $task->{startPeriod} ]->{flexibles}}, $flexible;

         ##------------------------------------------------------------------------
         ## After flexible task
         ##------------------------------------------------------------------------
         if( $task->{stopPeriod} != $#$treatedTask )
         {
            my $flexible = {
                             'empty'    => 1,
                             'rowspan'  => $#$treatedTask - $task->{stopPeriod},
							 'bgcolor'    => $task->{bgcolor},
							 'fgcolor'    => $task->{fgcolor},
							 'private'  => $task->{private},
						 };
            push @{$treatedTask->[ $task->{stopPeriod}+1 ]->{flexibles}}, $flexible;
		}
	 }
  }
   
   
   ##===========================================================================
   ## XML generation
   ##===========================================================================
   my $dayNumber;
   {
      local $ENV{TZ} = $userTimeZone;
	  tzset();
      $dayNumber = strftime("%w", 0, 0, 0, $day, $month-1, $year-1900);
      $dayNumber = 7 if( $dayNumber == 0 );
   }
   tzset();

   my ($startYear,$stopYear) = $self->_YearInterval($context,$year);

   my $xml = "<?xml version=\"1.0\"?>\n";
   $xml .= "<ViewDay".
           " day=\"$day\"".
           " dayNumber=\"$dayNumber\"";
   {
      local $ENV{TZ} = $userTimeZone;
	  tzset();
      $xml .= " now_day=\"".strftime("%d", localtime())."\"".
              " now_month=\"".strftime("%m", localtime())."\"".
              " now_year=\"".strftime("%Y", localtime())."\"".
              " now_hour=\"".strftime("%H", localtime())."\"".
              " now_minute=\"".strftime("%M", localtime())."\"";

      $xml .= " prev_day=\"".strftime("%d", localtime($local00Sec-86400))."\"".
              " prev_month=\"".strftime("%m", localtime($local00Sec-86400))."\"".
              " prev_year=\"".strftime("%Y", localtime($local00Sec-86400))."\"";

      $xml .= " next_day=\"".strftime("%d", localtime($local00Sec+86400))."\"".
              " next_month=\"".strftime("%m", localtime($local00Sec+86400))."\"".
              " next_year=\"".strftime("%Y", localtime($local00Sec+86400))."\"";


   }
   tzset();

   if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
   }
   $xml .= " startYear=\"$startYear\" stopYear=\"$stopYear\"".
           " isWorkingDay=\"$workingDay\"".
           " canWrite=\"$canWrite\"".
           ">\n";

   $xml .= $context->GetXML()."\n";
   $xml .= "$calendarXml\n";

   $xml .= $self->_GetOtherUserListXML($context);

   ##---------------------------------------------------------------------------
   ## Previous and next button in calendar
   ##---------------------------------------------------------------------------
   my ($otherDay,$otherMonth,$otherYear);
   #my $monthDuration;

   $otherMonth = $month - 1;
   $otherYear = $year;
   if( $otherMonth == 0 )
   {
      $otherMonth = 12;
      $otherYear--;
   }
   $otherDay = $day;
   $monthDuration = du_GetMonthDuration( $otherMonth, $otherYear );

   $otherDay = $monthDuration if( $otherDay > $monthDuration );
   $xml .= "<Calendar-previous method=\"ViewDay\" attribute=\"day\" ".
           "value=\"$otherYear-$otherMonth-$otherDay\"/>\n";

   $otherMonth = $month + 1;
   $otherYear = $year;
   if( $otherMonth == 13 )
   {
      $otherMonth = 1;
      $otherYear++;
   }
   $otherDay = $day;
   $monthDuration = du_GetMonthDuration( $otherMonth, $otherYear );
   $otherDay = $monthDuration if( $otherDay > $monthDuration );
   $xml .= "<Calendar-next     method=\"ViewDay\" attribute=\"day\" ".
           "value=\"$otherYear-$otherMonth-$otherDay\"/>\n";

   ##---------------------------------------------------------------------------
   ## Tasks
   ##---------------------------------------------------------------------------
   $xml .= "<DayTasks flexibleTasks=\"".($#$flexibleList+1)."\">\n";

   if( $canWrite ) {
	   $xml .= "<DayTask ".
	          " printTime=\"0\"".
		      " planned=\"0\"".
			  " private=\"0\"".
              " rowspan=\"1\"".
              " method=\"CreateStrictOrPeriodicTask\"".
              ">\n";
	   if(@$flexibleList) {
         $xml .= "<FlexibleTask empty=\"1\" fgcolor=\"\" bgcolor=\"\" private=\"\"".
                 " rowspan=\"".(@$notplannedtasks+1)."\"";
         $xml .= "/>\n";
	   }

	   $xml .= "</DayTask>\n";
   }

   ##---------------------------------------------------------------------------
   ## Not planned Tasks
   ##---------------------------------------------------------------------------
   elsif(@$notplannedtasks and @$flexibleList) {
	   $notplannedtasks->[0]->{flexibles} = [{ 'empty'    => 1,
																'rowspan'  => @$notplannedtasks,
																'bgcolor'  => "",
																'fgcolor'  => "",
																'private'  => "" }];
   }


   foreach my $task (@$notplannedtasks) {
	   my $method;
	   if( $canWrite )
	   {
		   if(    $task->{type} eq "periodic" ) { $method = "EditPeriodicTask"; }
		   else                             { $method = "EditStrictTask"; }
	   }
	   else
	   {
		   if(    $task->{type} eq "periodic" ) { $method = "ViewPeriodicTask"; }
		   else                             { $method = "ViewStrictTask"; }
	   }

     if($task->{private} == 1 and $context->GetUser()->GetRowid() == $context->GetGroup()->GetAnimId()) {
		 $task->{private} = 0;
	 }

      $xml .= "<DayTask ".
		      " printTime=\"1\"".
		      " planned=\"0\"".
			  " private=\"$task->{private}\"".
              " taskrowid=\"$task->{rowid}\"".
              " rowspan=\"1\"".
              " bgcolor=\"$task->{bgcolor}\"".
              " fgcolor=\"$task->{fgcolor}\"".
              " first_name=\"".st_FormatXMLString($task->{first_name})."\"".
              " last_name=\"".st_FormatXMLString($task->{last_name})."\"".
              " method=\"".st_FormatXMLString($method)."\"".
              ">\n";	   

      foreach my $flexible (@{$task->{flexibles}})
      {
         $xml .= "<FlexibleTask empty=\"$flexible->{empty}\" fgcolor=\"$flexible->{fgcolor}\" bgcolor=\"$flexible->{bgcolor}\" private=\"$flexible->{private}\"".
                 " rowspan=\"$flexible->{rowspan}\"";
         $xml .= " conflict=\"$flexible->{conflict}\"" if( exists( $flexible->{conflict} ) );
         $xml .= "/>\n";
      }
	  $xml .= st_FormatXMLString($task->{name})."</DayTask>\n";
   }


   foreach my $task (@$treatedTask)
   {

      $xml .= "<DayTask conflict=\"$task->{conflict}\"".
              " printTime=\"$task->{printTime}\"".
		      " planned=\"1\"".
              " startHour=\"$task->{startHour}\"".
              " startMin=\"$task->{startMin}\"".
              " stopHour=\"$task->{stopHour}\"".
              " stopMin=\"$task->{stopMin}\"".
              " private=\"$task->{private}\"".
              " taskrowid=\"$task->{taskrowid}\"".
              " method=\"$task->{method}\"".
              " rowspan=\"$task->{rowspan}\"".
              " bgcolor=\"$task->{bgcolor}\"".
              " fgcolor=\"$task->{fgcolor}\"".
              " first_name=\"".st_FormatXMLString($task->{first_name})."\"".
              " last_name=\"".st_FormatXMLString($task->{last_name})."\"".
              ">\n";
	  
      foreach my $flexible (@{$task->{flexibles}})
      {
         $xml .= "<FlexibleTask empty=\"$flexible->{empty}\" fgcolor=\"$flexible->{fgcolor}\" bgcolor=\"$flexible->{bgcolor}\" private=\"$flexible->{private}\"".
                 " rowspan=\"$flexible->{rowspan}\"";
         $xml .= " conflict=\"$flexible->{conflict}\"" if( exists( $flexible->{conflict} ) );
         $xml .= "/>\n";
      }
      $xml .= st_FormatXMLString($task->{name})."</DayTask>\n";
   }

   ##---------------------------------------------------------------------------
   ## Flexible tasks
   ##---------------------------------------------------------------------------
   $xml .= "<FlexibleTasks>\n";
   my $canGlobal  = undef;
   my $canProject = undef;

   for( my $index=0; $index<=$#$flexibleList; $index++ )
   {
	   my $id = $flexibleList->[$index]->{rowid};
	   my $type = $flexibleList->[$index]->{type};

      if( !defined($canGlobal) and $type eq 'flexible' )
      {
		  try {
			  my $appdesc = new Mioga2::AppDesc($config, ident => 'Tasks');
			  my $application = $appdesc->CreateApplication();
			  my $test = $application->CheckUserAccessOnFunction($context, $context->GetUser(), 
																 $context->GetGroup(), 'Write');

			  $canGlobal = ($test == AUTHZ_OK);
		  }

		  otherwise {
			  $canGlobal = 0;			  
		  };
      }
      elsif( !defined($canProject) and $type eq 'project' )
      {
		  try {
			  my $appdesc = new Mioga2::AppDesc($config, ident => 'Project');
			  my $application = $appdesc->CreateApplication();
			  my $test = $application->CheckUserAccessOnMethod($context, $context->GetUser(), 
															   $context->GetGroup(), 'ProjectDisplayTask');

			  $canProject = ($test == AUTHZ_OK);
		  }

		  otherwise {
			  $canProject = 0;			  
		  };
      }

      $type = 'none' unless( ( $type eq 'flexible' and $canGlobal  ) or
                             ( $type eq 'project'   and $canProject ) );
      
      $xml .= "   <FlexibleTask id=\"$index\" private=\"$flexibleList->[$index]->{private}\" taskrowid=\"$id\" type=\"$type\" bgcolor=\"$flexibleList->[$index]->{bgcolor}\" fgcolor=\"$flexibleList->[$index]->{fgcolor}\"".
                  " name=\"".st_FormatXMLString($flexibleList->[$index]->{name})."\" />\n";
   }
   $xml .= "</FlexibleTasks>\n";
   $xml .= "</DayTasks>\n";

   $xml .= $self->_GetTodoTasksXML($context, $canWrite);

   if($context->GetGroup()->GetType() ne 'resource') {
	   $xml .= $self->_GetMeetingXML($context, $canWrite);
   }

   $xml .= "</ViewDay>\n";

   ##---------------------------------------------------------------------------
   return $self->_MiogaResponse($context,$xml);
}

# ============================================================================

=head2 ViewWeek ()

	Main function for the Week view in organizer.

=head3 Generated XML
  <!-- The Root element parameters describe :
       week : the displayed week of year
       year : the displayed year

       isWorkingDay : set to 1 is the currently selected day is worked.
       canWrite : set to 1 if the current user can write in the current Organizer.

<ViewWeek week="18" year="2005" now_day="06">


<!-- 
   Here must be the standard miogacontext and calendar headers.
   See Organizer-DisplayMain-Basic.xml for more details.
  -->

<!--
     Description of tasks 

     WeekTasks attributes :
     
     previous*, next* : previous and last week of displayed week.
     
     -->


<WeekTasks previousWeek="17" previousYear="2005" nextWeek="19" nextYear="2005">

  <!--

       Day of Week displayed : isWorkingDay = 1 => not worked day.

     -->

   <WeekDays>
      <WeekDay index="1" day="02" month="05" year="2005" isWorkingDay="1"/>
      <WeekDay index="2" day="03" month="05" year="2005" isWorkingDay="1"/>
      <WeekDay index="3" day="04" month="05" year="2005" isWorkingDay="1"/>
      <WeekDay index="4" day="05" month="05" year="2005" isWorkingDay="0"/>
      <WeekDay index="5" day="06" month="05" year="2005" isWorkingDay="1"/>
      <WeekDay index="6" day="07" month="05" year="2005" isWorkingDay="0"/>
      <WeekDay index="7" day="08" month="05" year="2005" isWorkingDay="0"/>
   </WeekDays>

  <!--

       There are a WeekTime for each time interval.
       Inside WeekTime there is a WeekTimePeriod describing tasks 
       for each WeekDay.

       Tasks are described like this :

       8h00 => 9h00 
          Monday
          Tuesday
          Wednesday
          ...
   
       9h00 => 10h00
          Monday
          Tuesday
          Wednesday
          ...


       ...

       Inside each WeekTimePeriod, there are a list of WeekTask, 
       the list of tasks to display in the time period.
       

       WeekTime attributes are :
       
       is_planned : 0 for not planned tasks (tasks without hour)

       startHour, startMin : start hour and minute of the time period
       stopHour, stopMin : stop hour and minute of the time period
       printTime : if = 1, print just start hour of time interval. If set to 2, print start and stop hour of time interval.



       WeekTimePeriod attributes are :
       working : set to 1 if the time period is worked.

       canWrite : set to 1 if the current user can write in the current Organizer.
       
       empty : set to 1 if there is no tasks in the time period.



       WeekTask attributes are :
       
       startHour, startMin : the start hour and minutes of the task
       stopHour, stopMin :  the stop hour and minutes of the task

       private : set to 1 is the time interval contains a private task.
       bgcolor : background color of task category (empty when no task is present)
       fgcolor : text color of task category (empty when no task is present)


       method : method to call when the cells is clicked. For empty cells, it's CreateStrictOrPeriodicTask
                for cells containing tasks it's EditStrictTask or EditPeriodicTask.

       conflict : set to 1 if the time interval have task conflict.

       taskrowid: rowid of task

     -->



   <WeekTime is_planned="0">
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
   </WeekTime>
   <WeekTime startHour="08" startMin="00" stopHour="09"  stopMin="00"  is_planned="1" printTime="1">
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
   </WeekTime>
   <WeekTime startHour="09" startMin="00" stopHour="10"  stopMin="00"  is_planned="1" printTime="1">
      <WeekTimePeriod working="1" canWrite="1" empty="0">

         <WeekTask startHour="09" startMin="00" stopHour="10" stopMin="00" 
                   private="0" bgcolor="#CCCCCC" fgcolor="#000000" 
                   method="EditStrictTask" conflict="0" taskrowid="10">
           Little task
         </WeekTask>

      </WeekTimePeriod>

      <WeekTimePeriod working="1" canWrite="1" empty="0">

         <WeekTask startHour="09" startMin="00" stopHour="16" stopMin="00"
                   private="0" bgcolor="#CCCCCC" fgcolor="#000000" 
                   method="EditStrictTask" conflict="0" taskrowid="9">
           Big task
         </WeekTask>

      </WeekTimePeriod>

      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>

      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>

      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>

      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>

      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>

   </WeekTime>

   <WeekTime startHour="10" startMin="00" stopHour="11"  stopMin="00"  is_planned="1" printTime="1">
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">
         <WeekTask startHour="09" startMin="00" stopHour="16" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditStrictTask" conflict="0" taskrowid="9">Big task</WeekTask>
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">

        <WeekTask startHour="09" startMin="00" stopHour="16" stopMin="00"
                  private="0" bgcolor="#CCCCCC" fgcolor="#000000" 
                  method="EditStrictTask" conflict="0" taskrowid="9">
          Big task
        </WeekTask>

      </WeekTimePeriod>

      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>

      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>

   </WeekTime>

   <WeekTime startHour="11" startMin="00" stopHour="12"  stopMin="00"  is_planned="1" printTime="1">
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">
         <WeekTask startHour="09" startMin="00" stopHour="16" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditStrictTask" conflict="0" taskrowid="9">Big task</WeekTask>
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
   </WeekTime>

   <WeekTime startHour="12" startMin="00" stopHour="13"  stopMin="00"  is_planned="1" printTime="1">
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">
         <WeekTask startHour="09" startMin="00" stopHour="16" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditStrictTask" conflict="0" taskrowid="9">Big task</WeekTask>
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
   </WeekTime>

   <WeekTime startHour="13" startMin="00" stopHour="14"  stopMin="00"  is_planned="1" printTime="1">
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">
         <WeekTask startHour="09" startMin="00" stopHour="16" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditStrictTask" conflict="0" taskrowid="9">Big task</WeekTask>
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
   </WeekTime>

   <WeekTime startHour="14" startMin="00" stopHour="15"  stopMin="00"  is_planned="1" printTime="1">
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">
         <WeekTask startHour="09" startMin="00" stopHour="16" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditStrictTask" conflict="0" taskrowid="9">Big task</WeekTask>
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">
         <WeekTask startHour="14" startMin="00" stopHour="16" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditStrictTask" conflict="1" taskrowid="2">task 2</WeekTask>
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
   </WeekTime>

   <WeekTime startHour="15" startMin="00" stopHour="16"  stopMin="00"  is_planned="1" printTime="1">
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">
         <WeekTask startHour="09" startMin="00" stopHour="16" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditStrictTask" conflict="0" taskrowid="9">Big task</WeekTask>
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">
         <WeekTask startHour="14" startMin="00" stopHour="16" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditStrictTask" conflict="1" taskrowid="2">task 2</WeekTask>
         <WeekTask startHour="15" startMin="00" stopHour="17" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditStrictTask" conflict="1" taskrowid="6">Conflicting task</WeekTask>
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
   </WeekTime>

   <WeekTime startHour="16" startMin="00" stopHour="17"  stopMin="00"  is_planned="1" printTime="1">
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">
         <WeekTask startHour="15" startMin="00" stopHour="17" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditStrictTask" conflict="1" taskrowid="6">Conflicting task</WeekTask>
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
   </WeekTime>

   <WeekTime startHour="17" startMin="00" stopHour="18"  stopMin="00"  is_planned="1" printTime="1">
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
   </WeekTime>

   <WeekTime startHour="18" startMin="00" stopHour="19"  stopMin="00"  is_planned="1" printTime="1">
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">
         <WeekTask startHour="18" startMin="00" stopHour="19" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditPeriodicTask" conflict="0" taskrowid="4">Periodic Task</WeekTask>
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">
         <WeekTask startHour="18" startMin="00" stopHour="19" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditPeriodicTask" conflict="0" taskrowid="4">Periodic Task</WeekTask>
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="0">
         <WeekTask startHour="18" startMin="00" stopHour="19" stopMin="00" private="0" bgcolor="#CCCCCC" fgcolor="#000000" method="EditPeriodicTask" conflict="0" taskrowid="4">Periodic Task</WeekTask>
      </WeekTimePeriod>
   </WeekTime>

   <WeekTime startHour="19" startMin="00" stopHour="20"  stopMin="00"  is_planned="1" printTime="1">
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
      <WeekTimePeriod working="1" canWrite="1" empty="1">
      </WeekTimePeriod>
   </WeekTime>


   <!--
        Flexible task description

        There is a FlexiblePeriod for each day of week.
        empty = 1 when the task is present thid day.
        -->

   <FlexibleTasks>

      <FlexibleTask id="0" taskrowid="7" type="flexible" fgcolor="#000000" bgcolor="#CCCCCC" private="0" name="Flexible Task 1">
         <FlexiblePeriod empty="1"/> <!--colspan="4"/>-->
         <FlexiblePeriod empty="1"/> <!--colspan="4"/>-->
         <FlexiblePeriod empty="1"/> <!--colspan="4"/>-->
         <FlexiblePeriod empty="1"/> <!--colspan="4"/>-->
         <FlexiblePeriod empty="0" conflict=""/> <!--colspan="4" conflict=""/>-->
         <FlexiblePeriod empty="1"/> <!--colspan="4"/>-->
         <FlexiblePeriod empty="1"/> <!--colspan="4"/>-->
      </FlexibleTask>

      <FlexibleTask id="1" taskrowid="8" type="flexible" fgcolor="#000000" bgcolor="#CCCCCC" private="0" name="Flexible Task 2">
         <FlexiblePeriod empty="1"/> <!--colspan="4"/>-->
         <FlexiblePeriod empty="1"/> <!--colspan="4"/>-->
         <FlexiblePeriod empty="0" conflict=""/> <!--colspan="4" conflict=""/>-->
         <FlexiblePeriod empty="1"/> <!--colspan="4"/>-->
         <FlexiblePeriod empty="0" conflict=""/> <!--colspan="4" conflict=""/>-->
         <FlexiblePeriod empty="1"/> <!--colspan="4"/>-->
         <FlexiblePeriod empty="1"/> <!--colspan="4"/>-->
      </FlexibleTask>

   </FlexibleTasks>
</WeekTasks>

</ViewWeek>
	

=cut

# ============================================================================

sub ViewWeek
{
   my ($self, $context) = @_;

   $context->{args}{method} = 'ViewWeek';
   $self->InitializeViewState($context);

   my $session = $context->GetSession();
   my $config = $context->GetConfig();

   my $userTimeZone = $self->GetTimezone($context);

   my $test = $self->CheckUserAccessOnMethod( $context, $context->GetUser(),
											  $context->GetGroup(), "EditStrictTask" );
   my $canWrite = ($test == AUTHZ_OK);

   ##===========================================================================
   ## Arguments
   ##===========================================================================
   my ($year,$week);
   my $format;
   if( defined($context->{args}{week}) )
   {
		my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'week' ], 'stripxws', 'disallow_empty'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Organizer::ViewWeek", ac_FormatErrors($errors));
	}

      ($year,$week) = ( $context->{args}{week} =~ m/^([0-9]+)-([0-9]+)$/ );
      $format = "week=$context->{args}{week}";
   }
   elsif( exists( $session->{organizer}{viewState} ) )
   {
      ($year,$week) = ( $session->{organizer}{viewState}{parameter} =~
                        m/^([0-9]+)-([0-9]+)$/ );
      $format = "viewState=$context->{args}{week}";
   }
   else
   {
      return $self->_Error($context, __("no week!"));
   }

   ##===========================================================================
   if( !defined( $week ) or
       $year  < 1970 or $year  > 2036 or
       $week  <    1 or $week  >   53 )
   {
       return $self->_Error($context, __x("bad format for week: {error}", error => $context->{args}{week}));
   }

   $week = sprintf( "%02d", $week );

   ##===========================================================================
   ## Saving the current view-state
   ##===========================================================================
   my $viewState = $session->{organizer}{viewState} = {};
   $viewState->{method} = "ViewWeek";
   $viewState->{parameter} = "$year-$week";

   ##===========================================================================
   ## Starting XML
   ##===========================================================================
   my ($startYear,$stopYear) = $self->_YearInterval($context,$year);
   my $xml = "<?xml version=\"1.0\"?>\n";
   $xml .= "<ViewWeek".
           " week=\"$week\"".
           " year=\"$year\"";
   {
      local $ENV{TZ} = $userTimeZone;
	  tzset();
      $xml .= " now_day=\"".strftime("%d", localtime())."\"".
              " now_month=\"".strftime("%m", localtime())."\"".
              " now_year=\"".strftime("%Y", localtime())."\"".
              " now_hour=\"".strftime("%H", localtime())."\"".
              " now_minute=\"".strftime("%M", localtime())."\"";
   }
   tzset();

   if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
   }
   $xml .= " startYear=\"$startYear\" stopYear=\"$stopYear\"".
           ">\n";

   $xml .= $context->GetXML();

   ##===========================================================================
   ## Calendar(s)
   ##===========================================================================
   my $workingDaysHash = {};

   my ($weekDays1,$weekDays2,$wednesday) = $self->_GetDaysForAWeek( $week, $year );

   my $firstDay = $weekDays1->[0];
   my $lastDay = $weekDays1->[$#$weekDays1];
   $xml .= $self->_MonthCalendar( $context, $firstDay->{month}, $firstDay->{year},
                                  $firstDay->{day}, $lastDay->{day}, $workingDaysHash)."\n";
   if( defined( $weekDays2->[0] ) )
   {
      $lastDay = $weekDays2->[$#$weekDays2];
      $xml .= $self->_MonthCalendar( $context, $weekDays2->[0]->{month}, $weekDays2->[0]->{year},
                                     $weekDays2->[0]->{day}, $lastDay->{day}, $workingDaysHash)."\n";
   }

   ##===========================================================================
   ## Previous and next button in calendar
   ##===========================================================================
   my ($otherWeek,$otherYear,$otherMonth);

   $otherMonth = $wednesday->{month}-1;
   $otherYear = $wednesday->{year};
   if( $otherMonth == 0 )
   {
      $otherMonth = 12;
      $otherYear--;
   }
   {
      local $ENV{TZ} = $userTimeZone;
	  tzset();
      $otherWeek = strftime("%V", 0, 0, 0, $wednesday->{day}, $otherMonth-1, $otherYear-1900);
   }
   tzset();


   $xml .= "<Calendar-previous method=\"ViewWeek\" attribute=\"week\" ".
           "value=\"$otherYear-$otherWeek\"/>\n";

   $otherMonth = $wednesday->{month}+1;
   $otherYear = $wednesday->{year};
   if( $otherMonth == 13 )
   {
      $otherMonth = 1;
      $otherYear++;
   }
   {
      local $ENV{TZ} = $userTimeZone;
	  tzset();
      $otherWeek = strftime("%V", 0, 0, 0, $wednesday->{day}, $otherMonth-1, $otherYear-1900);
	  $otherYear-- if ($otherWeek >= 52);
   }
   tzset();


   $xml .= $self->_GetOtherUserListXML($context);
   $xml .= "<Calendar-next     method=\"ViewWeek\" attribute=\"week\" ".
           "value=\"$otherYear-$otherWeek\"/>\n";

   ##===========================================================================
   ## Previous and next button by week
   ##===========================================================================
   my $sec1 = du_ISOToSecond("$firstDay->{year}-$firstDay->{month}-$firstDay->{day} 00:00:00");
   my ($previousWeek,$previousYear) = split(/,/, strftime("%V,%G", gmtime($sec1 - (7*86400))));
   my ($nextWeek,$nextYear) = split(/,/, strftime("%V,%G", gmtime($sec1 + (7*86400))));

#    if( $previousWeek == 0 )
#    {
#       $previousWeek = 52;
#       $previousYear--;
#    }

#    my ($nextWeek,$nextYear) = ($week+1,$year);
#    if( $nextWeek == 53 )
#    {
#       $nextWeek = 1;
#       $nextYear++;
#    }

   ##===========================================================================
   ## 
   ##===========================================================================
   $xml .= "<WeekTasks previousWeek=\"$previousWeek\" previousYear=\"$previousYear\" ".
                       "nextWeek=\"$nextWeek\" nextYear=\"$nextYear\">\n";
   $xml .= "   <WeekDays>\n";

   my $index = 1;
   foreach my $weekDay (@$weekDays1,@$weekDays2)
   {
      my $workingDay = $workingDaysHash->{"$weekDay->{year}-$weekDay->{month}-$weekDay->{day}"};

      $xml .= "      <WeekDay index=\"$index\" day=\"$weekDay->{day}\" ".
              "month=\"$weekDay->{month}\" year=\"$weekDay->{year}\" isWorkingDay=\"$workingDay\"/>\n";

      $index++;
   }
   $xml .= "   </WeekDays>\n";

   ##===========================================================================
   ## User preferences
   ##===========================================================================
   my $userPreferences = $self->Org_GetUserPreferences($context);
   my $lowerLimitMinuteDay = $userPreferences->{lowerLimitMinuteDay};
   my $upperLimitMinuteDay = $userPreferences->{upperLimitMinuteDay};
   my $timeIncrement       = $userPreferences->{timeIncrement};

   ##===========================================================================
   ## tasks will contains all the tasks, the key will be <dayNumber>,<minutesFrom0h>
   ## and values a hash-table with start-minute, colspan, an ordered-list of task for such day and time
   ##===========================================================================
   my %tasks = ();
   my %notplannedtasks = ();
   my %dayFlexible = ();
   my %flexibleTasks = ();

   ##===========================================================================
   ## For each day
   ##===========================================================================
   my $indexWeekDay = 0;
   foreach my $weekDay (@$weekDays1,@$weekDays2)
   {
      $indexWeekDay ++;

      ##===========================================================================
      ## Getting strict tasks
      ##===========================================================================
      my ($local0000,$local2359);
      my ($local00Sec,$local23Sec);
      {
         local $ENV{TZ} = $userTimeZone;
		 tzset();
         $local00Sec = strftime('%s',  0,  0,  0, $weekDay->{day}, $weekDay->{month}-1, $weekDay->{year}-1900 );
         $local23Sec = strftime('%s', 59, 59, 23, $weekDay->{day}, $weekDay->{month}-1, $weekDay->{year}-1900 ) + 1;
         local $ENV{TZ} = 'GMT';
		 tzset();
         $local0000 = strftime("%Y-%m-%d %H:%M:%S GMT", gmtime($local00Sec));
         $local2359 = strftime("%Y-%m-%d %H:%M:%S GMT", gmtime($local23Sec));
      }
	  tzset();

      my $taskList = $self->Org_ListStrictPeriodicTasks( $context, $context->GetGroup()->GetRowid(), $userTimeZone, $local0000, $local2359 );
	  my $notplannedtaskslist = $self->Org_ListNotPlannedTasks( $context, $context->GetGroup()->GetRowid(),
															"$weekDay->{year}-$weekDay->{month}-$weekDay->{day}",
															"$weekDay->{year}-$weekDay->{month}-$weekDay->{day}");


	  my $key = $weekDay->{day};
	  foreach my $task (@$notplannedtaskslist) {
		  if( !exists( $notplannedtasks{$key} ) ) {
			  $notplannedtasks{$key} = [$task];
		  }
		  else {
			  push @{$notplannedtasks{$key}}, $task;
		  }
	  }

      ##===========================================================================
      ## Tasks pre-treatement : creation of {start|stop}MinuteDay fields
      ##===========================================================================
      my $previvousTask = undef;
      foreach my $task (@$taskList)
      {
		  my ($startYear,$startMonth,$startDay,$startHour,$startMin) = ( $task->{start} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
		  my ($stopHour,$stopMin);
         {
            local $ENV{TZ} = 'GMT';
			tzset();
            my $startSec = strftime('%s', 0, $startMin, $startHour, $startDay, $startMonth-1, $startYear-1900);
            my $stopSec = $startSec + $task->{duration};
            local $ENV{TZ} = $userTimeZone;
			tzset();
            ($startHour,$startMin) = split( /:/, strftime("%H:%M", localtime($startSec)) );
            ($stopHour,$stopMin) = split( /:/, strftime("%H:%M", localtime($stopSec)) );
         }
		 tzset();

		  if($task->{private} == 1 and $context->GetUser()->GetRowid() == $context->GetGroup()->GetAnimId()) {
			  $task->{private} = 0;
		  }

         $task->{startMinuteDay} = $startMin + 60 * $startHour;
         $task->{startMinute} = $startMin;
         $task->{startHour} = $startHour;
         my $roundedStartMinuteDay = $timeIncrement * int( ($task->{startMinuteDay} - $lowerLimitMinuteDay) / $timeIncrement ) + $lowerLimitMinuteDay;

         ##------------------------------------------------------------------------
         my $key = "$weekDay->{day},$roundedStartMinuteDay";
		  if( !exists( $tasks{$key} ) )
         {
            $tasks{$key} = { 'list' => [],
                   'startMinuteDay' => $roundedStartMinuteDay,
                    'stopMinuteDay' => $roundedStartMinuteDay + $timeIncrement };
         }

         push( @{$tasks{$key}->{list}}, $task );

         ##------------------------------------------------------------------------
         ## Computing colspan
         ##------------------------------------------------------------------------
         $task->{stopMinuteDay} = $stopMin + 60 * $stopHour;
         $task->{stopMinute} = $stopMin;
         $task->{stopHour} = $stopHour;

         if( $task->{stopMinuteDay} > $tasks{$key}->{stopMinuteDay} )
         {
			 for( my $time = $tasks{$key}->{startMinuteDay} + $timeIncrement;
				  $time < $task->{stopMinuteDay};
				  $time += $timeIncrement )
			 {
				 my $nextKey = "$weekDay->{day},$time";

				 if( !exists( $tasks{$nextKey} ) )
				 {
					 $tasks{$nextKey} = { 'list' => [],
										  'startMinuteDay' => $time,
										  'stopMinuteDay' => $time + $timeIncrement };
				 }

				 push( @{$tasks{$nextKey}->{list}}, $task );
			 }
		 }
		  
         ##------------------------------------------------------------------------
         $lowerLimitMinuteDay = $tasks{$key}->{startMinuteDay}
            if( $tasks{$key}->{startMinuteDay} < $lowerLimitMinuteDay );
         $upperLimitMinuteDay = $tasks{$key}->{stopMinuteDay}
            if( $tasks{$key}->{stopMinuteDay} > $upperLimitMinuteDay );

         ##------------------------------------------------------------------------
         ## conflicts detection
         ##------------------------------------------------------------------------
         if( defined( $previvousTask ) and
             $task->{startMinuteDay} < $previvousTask->{stopMinuteDay} )
         {
            $previvousTask->{conflict} = 1;
            $task->{conflict} = 1;
         }
         else
         {
            $task->{conflict} = 0;
         }
         $previvousTask = $task;
      }


      ##===========================================================================
      ## Getting flexible tasks
      ##===========================================================================
      my $workingDay = $workingDaysHash->{"$weekDay->{year}-$weekDay->{month}-$weekDay->{day}"};
      if( $workingDay )
      {
		  my ($start, $stop);
		  {
			  local $ENV{TZ} = $userTimeZone;
			  tzset();
			  
			  my $startsec = du_ISOToSecond("$weekDay->{year}-$weekDay->{month}-$weekDay->{day} 00:00:00");
			  my $stopsec  = du_ISOToSecond("$weekDay->{year}-$weekDay->{month}-$weekDay->{day} 23:59:59");
			  
			  $start = strftime("%Y-%m-%d %H:%M:%S", gmtime($startsec));
			  $stop = strftime("%Y-%m-%d %H:%M:%S", gmtime($stopsec));
		  }
		  tzset();
		  
         my $r = $self->Org_ListFlexibleTasks($context, $context->GetGroup()->GetRowid(), $start, $stop);
         foreach my $task (@$r)
         {
            $dayFlexible{"$task->{rowid},$indexWeekDay"} = 1;
            $flexibleTasks{$task->{rowid}} = $task;
         }

      }
   }

   ##===========================================================================
   ## 
   ##===========================================================================

   $xml .= "   <WeekTime is_planned=\"0\">\n";


   foreach my $weekDay (@$weekDays1,@$weekDays2)
   {
	   
	   my $empty = 1;
	   my $contentXml = "";
	   foreach my $task (@{$notplannedtasks{$weekDay->{day}}}) {
		   my $method;
		   my $tasktype = $task->{type};
		   if( $canWrite )
		   {
			   if(    $tasktype eq "periodic" ) { $method = "EditPeriodicTask"; }
			   else                             { $method = "EditStrictTask"; }
		   }
		   else
		   {
			   if(    $tasktype eq "periodic" ) { $method = "ViewPeriodicTask"; }
			   else                             { $method = "ViewStrictTask"; }
		   }
		   if($task->{private} == 1 and $context->GetUser()->GetRowid() == $context->GetGroup()->GetAnimId()) {
        $task->{private} = 0;
      }
		   
		   $contentXml .= "         <WeekTask".
			   " private=\"$task->{private}\"".
			   " bgcolor=\"$task->{bgcolor}\"".
			   " fgcolor=\"$task->{fgcolor}\"".
			   " method=\"$method\"".
			   " conflict=\"0\"".
			   " taskrowid=\"$task->{rowid}\">".st_FormatXMLString($task->{name})."</WeekTask>\n";
		   
		   $empty = 0;
	   }
	   
	   $xml .= "      <WeekTimePeriod".
		   " working=\"1\"".
		   " rowspan=\"1\"".
		   " canWrite=\"$canWrite\"".
		   " empty=\"$empty\">\n";
	   $xml .= $contentXml;
	   $xml .= "      </WeekTimePeriod>\n";
   }


   $xml .= "   </WeekTime>\n";
   ##===========================================================================
   ## 
   ##===========================================================================

   my $startMinuteDay = $lowerLimitMinuteDay;
   while( $startMinuteDay < $upperLimitMinuteDay )
   {
      my $stopMinuteDay = $startMinuteDay + $timeIncrement;

      my $startHour = sprintf( "%02d", int( $startMinuteDay / 60 ) );
      my $startMinute = sprintf( "%02d", $startMinuteDay - $startHour*60 );
      my $stopHour = sprintf( "%02d", int( $stopMinuteDay / 60 ) );
      my $stopMinute = sprintf( "%02d", $stopMinuteDay - $stopHour*60 );

      $xml .= "   <WeekTime".
              " startHour=\"$startHour\"".
              " startMin=\"$startMinute\"".
              " stopHour=\"$stopHour\" ".
              " stopMin=\"$stopMinute\" ".
			  " is_planned=\"1\"".
              " printTime=\"1\">\n";

      foreach my $weekDay (@$weekDays1,@$weekDays2)
      {
         my $key = "$weekDay->{day},$startMinuteDay";
         my $contentXml = "";
         my $empty = 1;
         my $rowspan = 1;

         if( exists( $tasks{$key} ) )
         {
            if( !exists( $tasks{$key}->{done} ) )
            {
               $tasks{$key}->{done} = 1;
               #$rowspan = $tasks{$key}->{rowspan};
               foreach my $task ( @{$tasks{$key}->{list}} )
               {

                  my $method;
                  my $tasktype = $task->{type};
                  if( $canWrite )
                  {
                     if(    $tasktype eq "periodic" ) { $method = "EditPeriodicTask"; }
                     else                             { $method = "EditStrictTask"; }
                  }
                  else
                  {
                     if(    $tasktype eq "periodic" ) { $method = "ViewPeriodicTask"; }
                     else                             { $method = "ViewStrictTask"; }
                  }

                  $contentXml .= "         <WeekTask".
                                 " startHour=\"$task->{startHour}\"".
                                 " startMin=\"$task->{startMinute}\"".
                                 " stopHour=\"$task->{stopHour}\"".
                                 " stopMin=\"$task->{stopMinute}\"".
                                 " private=\"$task->{private}\"".
                                 " bgcolor=\"$task->{bgcolor}\"".
                                 " fgcolor=\"$task->{fgcolor}\"".
                                 " method=\"$method\"".
								 " conflict=\"$task->{conflict}\"".
                                 " taskrowid=\"$task->{rowid}\">".st_FormatXMLString($task->{name})."</WeekTask>\n";
                  $empty = 0;
               }
            }
            else
            {
               $rowspan = 0;
            }
         }

         $xml .= "      <WeekTimePeriod".
                 " working=\"1\"".
                 " rowspan=\"$rowspan\"".
                 " canWrite=\"$canWrite\"".
                 " empty=\"$empty\">\n";
         $xml .= $contentXml;
         $xml .= "      </WeekTimePeriod>\n";
      }
      $xml .= "   </WeekTime>\n";

      $startMinuteDay += $timeIncrement;
   }


   ##===========================================================================
   ## Flexible tasks : treatement
   ##===========================================================================
   $xml .= "   <FlexibleTasks>\n";
   my $canGlobal  = undef;
   my $canProject = undef;
   $index=0;
   foreach my $key (sort keys %flexibleTasks)
   {
      my $task = $flexibleTasks{$key};
      my $id = $task->{rowid};
	  my $type = $task->{type};

      if( !defined($canGlobal) and $type eq 'flexible' )
      {
		  try {
			  my $appdesc = new Mioga2::AppDesc($config, ident => 'Tasks');
			  my $application = $appdesc->CreateApplication();
			  my $test = $application->CheckUserAccessOnFunction($context, $context->GetUser(), 
																 $context->GetGroup(), 'Write');

			  $canGlobal = ($test == AUTHZ_OK);
		  }

		  otherwise {
			  $canGlobal = 0;			  
		  };
      }
      elsif( !defined($canProject) and $type eq 'project' )
      {
		  try {
			  my $appdesc = new Mioga2::AppDesc($config, ident => 'Project');
			  my $application = $appdesc->CreateApplication();
			  my $test = $application->CheckUserAccessOnMethod($context, $context->GetUser(), 
															   $context->GetGroup(), 'ProjectDisplayTask');

			  $canProject = ($test == AUTHZ_OK);
		  }

		  otherwise {
			  $canProject = 0;			  
		  };
      }

      $type = 'none' unless( ( $type eq 'flexible' and $canGlobal  ) or
                             ( $type eq 'project'   and $canProject ) );

	 if($task->{private} == 1 and $context->GetUser()->GetRowid() == $context->GetGroup()->GetAnimId()) {
	     $task->{private} = 0;
	 }

      $xml .= "      <FlexibleTask id=\"$index\" taskrowid=\"$id\" type=\"$type\"".
   	      " fgcolor=\"$task->{fgcolor}\" bgcolor=\"$task->{bgcolor}\" private=\"$task->{private}\"".
              " name=\"".st_FormatXMLString($task->{name})."\">\n";
      foreach $indexWeekDay (1..7)
      {
         if( defined( $dayFlexible{"$task->{rowid},$indexWeekDay"} ) )
         {
            $xml .= "         <FlexiblePeriod empty=\"0\" colspan=\"1\" conflict=\"$task->{conflict}\"/>\n";
         }
         else
         {
            $xml .= "         <FlexiblePeriod empty=\"1\" colspan=\"1\"/>\n";
         }
      }
      $xml .= "      </FlexibleTask>\n";
      $index++;
   }
   $xml .= "   </FlexibleTasks>\n";

   ##===========================================================================
   $xml .= "</WeekTasks>\n";
   if($context->GetGroup()->GetType() ne 'resource') {
	   $xml .= $self->_GetMeetingXML($context, $canWrite);
   }
   $xml .= $self->_GetTodoTasksXML($context, $canWrite);

   ##---------------------------------------------------------------------------
   $xml .= "</ViewWeek>\n";

   return $self->_MiogaResponse($context,$xml);
}



# ============================================================================
#
# PRIVATE METHODS DESCRIPTION
#
# ============================================================================


# ============================================================================
#
# _Error ()
#
#	Throw a Mioga2 Simple Error
#	
# ============================================================================

sub _Error
{
   my ($self,$context,$message) = @_;

   throw Mioga2::Exception::Simple("Mioga2::Organizer", $message);
}



# ============================================================================
#
# _GetDaysForAWeek ()
#
#	Return the days of the given week of the given year
#	
# ============================================================================

sub _GetDaysForAWeek
{
   my ($self, $argWeek, $argYear) = @_;
   my $l1 = [];
   my $l2 = [];
   my $l = \$l1;
   my $wednesday;
   my $currentMonth = -1;
   my @years = ();
   if( $argWeek < 2 ) {
      $years[0] = { 'year'=>$argYear-1, 'startMonth' => 12, 'stopMonth'  => 12 };
      $years[1] = { 'year'=>$argYear,   'startMonth' => 1,  'stopMonth'  => 1  };
   } elsif( $argWeek > 51 ) {
      $years[0] = { 'year'=>$argYear,   'startMonth' => 12, 'stopMonth'  => 12 };
      $years[1] = { 'year'=>$argYear+1, 'startMonth' => 1,  'stopMonth'  => 1  };
   } else {
      $years[0] = { 'year'=>$argYear,   'startMonth' => 1,  'stopMonth'  => 12 };
   }

   YEAR: foreach my $year (@years)
   {
      MONTH: for( my $month=$year->{startMonth}; $month<=$year->{stopMonth}; $month++ )
      {
         DAY: for( my $day=1; $day<=31; $day++ )
         {
            my ($week,$dayWeek,$sDay,$sMonth,$sYear) =
               split(/:/,strftime("%V:%u:%d:%m:%Y", 0, 0, 0, $day, $month-1, $year->{year}-1900));
            last DAY if( $sMonth != $month );
            if( $week  == $argWeek )
            {
               my %h = ( 'day' => $sDay, 'month' => $sMonth, 'year' => $sYear );
               if( $currentMonth == -1 )
               { $currentMonth = $h{month} }
               elsif( $currentMonth != $h{month} )
               { $l = \$l2; }
               push @$$l, \%h;
               $wednesday = \%h if( $dayWeek == 3 );
            }
         }
      }
   }
   return( $l1, $l2, $wednesday );
}

# ============================================================================
#
# _GetTodoTasksXML ()
#
#	Return XML Describing Todo tasks
#	
# ============================================================================

sub _GetTodoTasksXML
{
   my ($self, $context, $canWrite) = @_;
   my $xml = "";
   $xml .= "<ToDoTasks canWrite=\"$canWrite\">\n";

   my $config = $context->GetConfig();
   my $dbh = $config->GetDBH();

   my $taskList = $self->Org_ListTodoTasks($context, $context->GetGroup()->GetRowid() );

   my $canGlobal = undef;
   foreach my $task (@$taskList)
   {       
      my $method;
	  
      if( $task->{type} eq "todo" )
      {
         if($canWrite) { $method = "Organizer/EditTodoTask?taskrowid"; }
         else          { $method = "Organizer/ViewTodoTask?taskrowid"; }
      }
      else  # $task->{type} eq "flexible"
      {
         if( ! defined($canGlobal) )
         {
			 try {
				 my $appdesc = new Mioga2::AppDesc($config, ident => 'Tasks');
				 my $application = $appdesc->CreateApplication();
				 my $test = $application->CheckUserAccessOnFunction($context, $context->GetUser(), 
																	$context->GetGroup(), 'Write');

				 if($test == AUTHZ_OK) {
					 $canGlobal = 1;
				 }
				 else {
					 $canGlobal = 0;
				 }
			 }

			 otherwise {
				 $canGlobal = 0;
			 };
		 }

         if ($canGlobal) { $method = "Tasks/EditTask?rowid"; }
         else      { $method = "none"; }
      }

      if($task->{private} == 1 and $context->GetUser()->GetRowid() == $context->GetGroup()->GetAnimId()) {
		  $task->{private} = 0;
      }

      $xml .= "   <ToDoTask priority=\"$task->{priority}\" taskrowid=\"$task->{rowid}\"".
	      "             private=\"$task->{private}\" bgcolor=\"$task->{bgcolor}\" fgcolor=\"$task->{fgcolor}\"".
                          " method=\"$method\" done=\"$task->{done}\">\n";
      $xml .= "      ".st_FormatXMLString($task->{name})."\n";
      $xml .= "   </ToDoTask>\n";
   }
   $xml .= "</ToDoTasks>\n";

}


# ============================================================================
#
# _GetOtherUserListXML ()
#
#	Return XML Describing other user that the current user can view the 
#	organizer.
#	
# ============================================================================

sub _GetOtherUserListXML
{
   my ($self, $context) = @_;

   my $xml = "<OtherOrganizer selected=\"".$context->GetGroup()->GetIdent()."\">";

   #
   # User
   #

   my $userlist = $context->GetUser->GetUsersWithFunction("Organizer", "Read");
  
   my $idents = $userlist->GetIdents;
   my $names = $userlist->GetNames;
   
   my @users;
   for (my $i = 0; $i < @$names; $i++) {
     push @users, { ident => $idents->[$i], name => $names->[$i] };
   }

   my $nb_user = @$idents;

   if ($nb_user) {
     my $rowids = join(",", @{$userlist->GetRowids});
     my $teams  = SelectMultiple($context->GetConfig->GetDBH, "SELECT m_group_base.ident AS team, m_user_base.firstname, m_user_base.lastname, m_user_base.ident as user_ident 
                  FROM m_group_group, m_group_base, m_user_base, m_group_type
                  WHERE invited_group_id IN ($rowids) AND m_group_base.rowid = m_group_group.group_id 
                  AND m_group_base.type_id=m_group_type.rowid AND m_user_base.rowid=invited_group_id 
                  AND m_group_type.ident = 'team'
                  ORDER BY team ASC, m_user_base.lastname ASC");
     
     if (@$teams) {
       $xml .= "<OtherTeamList>";
       my $cur_team;
       for (my $i = 0; $i < @$teams; $i++) {
         if ($cur_team ne $$teams[$i]->{team}) {
           $cur_team = $$teams[$i]->{team};
           $xml .= "</Team>" if $i > 0;
           $xml .= "<Team ident=\"".st_FormatXMLString($$teams[$i]->{team})."\">";
         }
         $xml .= "<User ident=\"".st_FormatXMLString($$teams[$i]->{user_ident})."\">".
         st_FormatXMLString($$teams[$i]->{lastname})." ".st_FormatXMLString($$teams[$i]->{firstname})."</User>";
       }
       $xml .= "</Team>";
       $xml .= "</OtherTeamList>";
     }
     @users = grep { my $ident = $_->{ident}; ! grep { $ident eq $_->{user_ident} } @$teams } @users;
   }
   
   $xml .= "<OtherUserList>";
   
   foreach my $user (@users) {
	   $xml .= "<user ident=\"".st_FormatXMLString($user->{ident})."\">".st_FormatXMLString($user->{name})."</user>" 	   
   }

   $xml .= "</OtherUserList>";


   #
   # Resource
   #

   my $reslist = $context->GetUser()->GetResourcesWithFunction("Organizer", "Read");
   $idents = $reslist->GetIdents();

   my $nb_res = @$idents;

   $xml .= "<OtherResourceList>";
 
   for(my $i=0; $i<$nb_res; $i++) {
	   $xml .= "<resource ident=\"$idents->[$i]\">".st_FormatXMLString($idents->[$i])."</resource>";
   }

   $xml .= "</OtherResourceList>";


   #
   # Groups
   #

   my $grouplist = $context->GetUser()->GetGroupsWithFunction("Organizer", "Read");
   $idents = $grouplist->GetIdents();

   my $nb_group = @$idents;

   $xml .= "<OtherGroupList>";
 
   for(my $i=0; $i<$nb_group; $i++) {
	   $xml .= "<group ident=\"$idents->[$i]\">".st_FormatXMLString($idents->[$i])."</group>";
   }

   $xml .= "</OtherGroupList>";
   $xml .= "</OtherOrganizer>";

   return $xml;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application 

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
