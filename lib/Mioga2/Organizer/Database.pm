# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Organizer.pm: The Mioga2 Organizer application.

=head1 METHODS DESRIPTION

=cut

package Mioga2::Organizer;
use strict;

use Mioga2::Exception::Simple;
use Mioga2::tools::database;
use Mioga2::tools::date_utils;
use Mioga2::tools::string_utils;
use Mioga2::Synchro;
use Mioga2::XML::Simple;
use MIME::QuotedPrint;
use Data::Dumper;
use Error qw(:try);
use POSIX qw( strftime tzset );

# ============================================================================

=head1 NAME

Mioga2::Organizer::Database -  Set of function for accessing tasks tables in DB

=head1 DESCRIPTION

This is the description of some methods.

=cut


# ============================================================================

=head2 Org_InitializeUser ()

	Initilize user preferences
	
=cut

# ============================================================================

sub Org_InitializeUser
{
	my ($self, $dbh, $user) = @_;
	
	ExecSQL($dbh, "INSERT INTO org_user_pref (created, modified, user_id) (SELECT now() AS created, now() AS modified, $user->{rowid} AS user_id WHERE $user->{rowid} NOT IN (SELECT user_id FROM org_user_pref));");
}



# ============================================================================

=head2 Org_InitializeTaskCategories ()

	Initilize task categories in database (add the default categorie if the table is empty)
	
=cut

# ============================================================================

sub Org_InitializeTaskCategories
{
	my ($self, $config, $values) = @_;
	
	my $dbh = $config->GetDBH();

	my $res = SelectSingle($dbh, "SELECT count(*) FROM task_category WHERE mioga_id = ".$config->GetMiogaId());
	
	if($res->{count} == 0 and defined $values) {
		$values->{created} = 'now()';
		$values->{modified} = 'now()';
		$values->{mioga_id} = $config->GetMiogaId();

		my $sql = BuildInsertRequest($values, table   => 'task_category',
									          string  => ['bgcolor', 'fgcolor', 'name'],
									          bool    => ['private'],
									          other   => ['mioga_id', 'created', 'modified'] );

		ExecSQL($dbh, $sql);
	}
}



# ============================================================================

=head2 Org_CreateTaskCategory ()

	Add a task category in the database.
	
=cut

# ============================================================================

sub Org_CreateTaskCategory
{
	my ($self, $config, $values) = @_;
	
	my $dbh = $config->GetDBH();

	$values->{created} = 'now()';
	$values->{modified} = 'now()';
	$values->{mioga_id} = $config->GetMiogaId();
	
	my $sql = BuildInsertRequest($values, table   => 'task_category',
									      string  => ['bgcolor', 'fgcolor', 'name'],
									      bool => ['private'],
									      other   => ['mioga_id', 'created', 'modified'] );

	ExecSQL($dbh, $sql);
}

# ============================================================================

=head2 Org_ModifyTaskCategory ()

	Add a task category in the database.
	
=cut

# ============================================================================

sub Org_ModifyTaskCategory
{
	my ($self, $config, $rowid, $values) = @_;
	
	my $dbh = $config->GetDBH();

	$values->{modified} = 'now()';
	
	my $sql = BuildUpdateRequest($values, table   => 'task_category',
									      string  => ['bgcolor', 'fgcolor', 'name'],
									      bool => ['private'],
									      other   => ['modified'] );

	$sql .= " WHERE rowid = $rowid AND mioga_id = ".$config->GetMiogaId();

	ExecSQL($dbh, $sql);
}


##******************************************************************************
## Method Org_GetTaskCategory  public
##  Description  : return the task category
##******************************************************************************
sub Org_GetTaskCategory
{
	my ($self, $config, $rowid) = @_;

	my $dbh = $config->GetDBH();

    my $sql = "SELECT * FROM task_category WHERE mioga_id = ".$config->GetMiogaId()." AND rowid = $rowid";

    return SelectSingle($dbh, $sql);
}

##******************************************************************************
## Method Org_GetDefaultTaskCat  public
##  Description  : return the task category
##******************************************************************************
sub Org_GetDefaultTaskCat
{
	my ($self, $config) = @_;

	my $dbh = $config->GetDBH();

    my $sql = "SELECT * FROM task_category WHERE mioga_id = ".$config->GetMiogaId()." AND name = 'default'";
	my $res =  SelectSingle($dbh, $sql);
	return $res->{rowid};
}


# ============================================================================

=head2 GetTimezone ()

	Return the timezone to use.
	
=cut

# ============================================================================

sub GetTimezone
{
	my ($self, $context) = @_;
	
	my $group = $context->GetGroup();
	my $user = $context->GetUser();

	if($group->GetType() eq 'resource') {
		return $group->GetTimezone();
	}
	else {
		return $user->GetTimezone();		
	}
}

# ============================================================================

=head2 Org_ChangeUserPrefs ()

	Modify Organizer user preferences
	
=cut

# ============================================================================

sub Org_ChangeUserPrefs
{
   my ($self,$config, $user, $fields) = @_;
   my $dbh =  $config->GetDBH();

   my $rowid = $user->GetRowid();

   $fields->{modified} = "now()";
   
   my $sql = BuildUpdateRequest($fields, table => 'org_user_pref',
								         string => ['start_time', 'end_time'],
								         other  => ['interval', 'modified']);

   ExecSQL($dbh, $sql." WHERE user_id=$rowid");

}

# ============================================================================

=head2 Org_GetUserPrefs ()

	Return Organizer user preferences
	
=cut

# ============================================================================

sub Org_GetUserPrefs
{
   my ($self, $config, $user) = @_;
   my $dbh =  $config->GetDBH();

   my $rowid = $user->GetRowid();

   $self->Org_InitializeUser ($dbh, {rowid => $rowid});
   
   my $res = SelectSingle($dbh, "SELECT * FROM org_user_pref WHERE user_id = $rowid");
   
   ($res->{start_time_hour}, $res->{start_time_min}) = split( /:/, $res->{start_time} );
   ($res->{end_time_hour}, $res->{end_time_min}) = split( /:/, $res->{end_time} );
   
   
   return $res;
   
}

# ============================================================================

=head2 Org_GetUserPreferences ()
	
	Return Organizer user preferences
	
=cut
	
# ============================================================================
	
sub Org_GetUserPreferences
{
	my ($self,$context) = @_;
	my $dbh =  $context->GetConfig()->GetDBH();
	
	my $rowid;
	my $group = $context->GetGroup();
	my $user = $context->GetUser();
	
	$rowid = $user->GetRowid();

   $self->Org_InitializeUser ($dbh, {rowid => $rowid});
	
	my $res = SelectSingle($dbh, "SELECT * FROM org_user_pref WHERE user_id = $rowid");
	
	my ($lowerHour,$lowerMin)  = split( /:/, $res->{start_time} );
	my ($upperHour,$upperMin)  = split( /:/, $res->{end_time} );
	
	return { "lowerLimitMinuteDay" => $lowerHour * 60 + $lowerMin,
			 "upperLimitMinuteDay" => $upperHour * 60 + $upperMin,
			 "timeIncrement"       => $res->{interval} };

}


##******************************************************************************
## Method Org_TaskCatGetList  public
##  Description  : return the task category list
##******************************************************************************
sub Org_TaskCatGetList
{
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	my $dbh = $config->GetDBH();
		
    my $sql = "SELECT * FROM task_category WHERE mioga_id = ".$config->GetMiogaId();

	if($context->GetGroup()->GetType() !~ /_user$/) {
		$sql .= " AND private IS FALSE ";
	}

	$sql .= " ORDER BY name";

    return SelectMultiple($dbh, $sql);
}

##******************************************************************************
## Method Org_DeleteGroupTasks  public
##  Description  : deletes tasks for a group
##******************************************************************************
sub Org_DeleteGroupTasks
{
   my ($self, $context, $groupRowid) = @_;

   my $dbh = $context->GetConfig()->GetDBH();

   my $sql = "DELETE from org_flexible_task WHERE group_id = $groupRowid";
   my $r = $dbh->do( $sql );

   $sql = "DELETE from org_todo_task WHERE group_id = $groupRowid";
   $r = $dbh->do( $sql );

   $sql = "DELETE from org_panned_task WHERE group_id = $groupRowid";
   $r = $dbh->do( $sql );

   $sql = "DELETE from org_periodic_task WHERE group_id = $groupRowid";
   $r = $dbh->do( $sql );

   return 1;
}


##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
##
##                          CancelledPeriodic tasks
##
##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

##******************************************************************************
## Method Org_CreateCancelledPeriodicTask  public
##  Description  : creates a cancelled periodic task
##******************************************************************************
sub Org_CreateCancelledPeriodicTask
{
   my ($self, $context, $fields,$created) = @_;
   # $created  is optional

   my $dbh = $context->GetConfig()->GetDBH();

   $created = "now()" if( !defined $created );

   delete $fields->{created};
   delete $fields->{modified};

   my $sql = "INSERT INTO org_cancelled_periodic_task(created,modified,";
   $sql .= join(',', keys(%$fields));
   $sql .= ") VALUES ($created, now(),";
   $sql .= join(',', map  { '?' } keys(%$fields));
   $sql .= ")";

   if(!$dbh->do($sql, undef, values(%$fields)))
   {
      throw Mioga2::Exception::DB("Mioga2::Organizer::Database->Org_CreateCancelledPeriodicTask", $DBI::err,  $DBI::errstr, $sql);
   }

   # Change modified date of parent tasks
   ExecSQL($dbh, "UPDATE org_periodic_task SET modified=now() WHERE rowid = $fields->{task_id}");

   return 1;
}

##******************************************************************************
## Method Org_DeleteCancelledPeriodicTask  public
## Description  : deletes a cancelled periodic task
##******************************************************************************
sub Org_DeleteCancelledPeriodicTask
{
   my ($self, $context, $fields) = @_;
   my $dbh = $context->GetConfig()->GetDBH();

   my $sql = "DELETE FROM org_cancelled_periodic_task WHERE ";
   $sql .= join(' AND ', map { "$_=?" } keys(%$fields));

   if(!$dbh->do($sql, undef, values(%$fields)))
   {
      throw Mioga2::Exception::DB("Mioga2::Organizer::Database->Org_DeleteCancelledPeriodicTask", $DBI::err,  $DBI::errstr, $sql);
   }

   # Change modified date of parent tasks
   ExecSQL($dbh, "UPDATE org_periodic_task SET modified=now() WHERE rowid = $fields->{task_id}");

   return 1;
}

##******************************************************************************
## Method Org_ChangeCancelledPeriodicTask  public
## Description  : change a cancelled periodic task for new time
##******************************************************************************
sub Org_ChangeCancelledPeriodicTask
{
   my ($self, $context, $taskrowid, $timeDelta) = @_;
   my $dbh = $context->GetConfig()->GetDBH();

   my $sql = "UPDATE org_cancelled_periodic_task SET date = date+'$timeDelta' WHERE task_id=$taskrowid";
   if( !$dbh->do($sql) )
   {
      throw Mioga2::Exception::DB("Mioga2::Organizer::Database->Org_ChangeCancelledPeriodicTask", $DBI::err,  $DBI::errstr, $sql);
   }

   # Change modified date of parent tasks
   ExecSQL($dbh, "UPDATE org_periodic_task SET modified=now() WHERE rowid = $taskrowid");

   return 1;
}

##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
##
##                          Common methods
##
##    The parameters named  taskrowid  are made of 2 parts:
## . the database rowid (of table org_task)
## . the type of the task (strict, periodic, todo, flexible)
## separated by an _
##    The taskrowid '3_strict' is the strict task of rowid 3.
## Methods which need taskrowid, require such format.
## Methods which return tasks lists, must give such format for taskrowid.
##
##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

##******************************************************************************
## Method Org_CreateTask  private
##  Description  : creates a task
##******************************************************************************
sub Org_CreateTask
{
   my ($self, $context, $tasktype, $fields, $creationTime, $rowid) = @_;
   # $creationTime, $rowid are optionals


   my $dbh = $context->GetConfig()->GetDBH();

   delete $fields->{rowid};

   if( !defined( $rowid ) )
   {
      $rowid = SelectSingle($dbh, "SELECT nextval('org_task_rowid_seq') as rowid")
         if( !defined( $rowid ) );

      $rowid = $rowid->{rowid};
   }

   $fields->{rowid} = $rowid;

   my $res = SelectSingle($dbh, "SELECT * FROM org_task_type WHERE ident = '$tasktype'");

   $fields->{type_id} = $res->{rowid};
   $creationTime = $creationTime || "now()";
   $creationTime = "now()" if( $creationTime eq "''" );

   delete( $fields->{taskrowid} ) if( exists( $fields->{taskrowid} ) );
   delete( $fields->{created} )   if( exists( $fields->{created} ) );
   delete( $fields->{modified} )  if( exists( $fields->{modified} ) );

   if(exists $fields->{priority}) {
	   my $res = SelectSingle($dbh, "SELECT * FROM org_todo_task_priority WHERE ident = '$fields->{priority}'");

	   $fields->{priority_id} = $res->{rowid};
	   delete $fields->{priority};
   }

   if(exists $fields->{is_planned} and $fields->{is_planned} == 0) {
	   $fields->{is_planned} = "f";
   }
   elsif ($tasktype eq 'todo') {
   	# nothing to do
   }
   else {
	   $fields->{is_planned} = "t";
   }

   my $sql = "INSERT INTO org_${tasktype}_task(created,modified,";
   $sql .= join(',', keys(%$fields));
   $sql .= ") VALUES ($creationTime,CURRENT_TIMESTAMP,";
   $sql .= join(',', map  { '?' } keys(%$fields));
   $sql .= ")";

   if(!$dbh->do($sql, undef, values(%$fields)))
   {
      throw Mioga2::Exception::DB("Mioga2::Organizer::Database->Org_CreateTask", $DBI::err,  $DBI::errstr, $sql);
   }

   return $rowid;
}


##******************************************************************************
## Method Org_CreateTaskForExternalUser  private
##  Description  : creates a task for an external user
##******************************************************************************
sub Org_CreateTaskForExternalUser
{
   my ($self, $context, $tasktype, $fields) = @_;
   my $dbh = $context->GetConfig()->GetDBH();
   
   my $ext_mioga = new Mioga2::ExternalMioga($context->GetConfig(), user_rowid => $fields->{group_id});
   my $user = new Mioga2::Old::User($context->GetConfig(), rowid => $fields->{group_id});


   my $response = $ext_mioga->SendRequest(application => 'Organizer', method => 'XMLCreateTaskForUser',
											   args => {
												        user_ident => $user->GetRemoteIdent(),
														category_id => $fields->{category_id},
														description => $fields->{description},
														name => $fields->{name},
														start => $fields->{start},
														duration => $fields->{duration},
													   }
											   );
		
   my $xs = new Mioga2::XML::Simple();
   my $xmltree = $xs->XMLin($response);

   return $xmltree->{task}->{rowid};
}


##******************************************************************************
## Method Org_ChangeTask  public
##  Description  : changes a task
##******************************************************************************
sub Org_ChangeTask
{
   my ($self, $context, $taskrowid, $tasktype, $groupRowid, $fields, $SQLchangeCondition) = @_;
   # $SQLchangeCondition is optional

    my $dbh = $context->GetConfig()->GetDBH();
   
   delete( $fields->{taskrowid} ) if( exists( $fields->{taskrowid} ) );
   delete( $fields->{created} )   if( exists( $fields->{created} ) );
   delete( $fields->{modified} )  if( exists( $fields->{modified} ) );

   if(exists $fields->{priority}) {

	   my $res = SelectSingle($dbh, "SELECT * FROM org_todo_task_priority WHERE ident = '$fields->{priority}'");

	   $fields->{priority_id} = $res->{rowid};
	   delete $fields->{priority};
   }


   if(exists $fields->{is_planned} and $fields->{is_planned} == 0) {
	   $fields->{is_planned} = "f";
   }
   elsif(!exists $fields->{is_planned}) {
	   # nothing todo
   }
   else {
	   $fields->{is_planned} = "t";
   }
   
   my $sql = "UPDATE org_${tasktype}_task SET modified = now()";
   foreach my $k ( keys(%$fields) )
   {
      $sql .= ", $k = ?";
   }
   $sql .= " WHERE rowid=$taskrowid";
   $sql .= " AND group_id = $groupRowid";
   $sql .= " AND $SQLchangeCondition"
      if( defined( $SQLchangeCondition ) );

   if(!$dbh->do($sql, undef, values(%$fields)))
   {
      throw Mioga2::Exception::DB("Mioga2::Organizer::Database->Org_ChangeTask", $DBI::err,  $DBI::errstr, $sql);
   }

   return 1;
}

##******************************************************************************
## Method Org_ChangeTask_NewType  public
##  Description  : Create a new task
##******************************************************************************
sub Org_ChangeTask_NewType
{
   my ($self, $context, $taskrowid, $tasktype, $groupRowid, $fields) = @_;

   my $dbh = $context->GetConfig()->GetDBH();
   
   ##===========================================================================
   my $sql = "SELECT created FROM org_task WHERE rowid=$taskrowid";
   $sql .= " and group_id = $groupRowid";

   my $r = SelectSingle( $dbh, $sql );

   my $creationTime = "'$r->{created}'";

   ##===========================================================================
   $dbh->{AutoCommit} = 0;
   try
   {
	   $r = $self->Org_DeleteTask($context, $taskrowid, $groupRowid ) &&
		   $self->Org_CreateTask($context, $tasktype, $fields, $creationTime );
   }
   otherwise # For all exceptions
   {
      my $err = shift;
      $dbh->rollback();
      $dbh->{AutoCommit} = 1;
      throw $err;
   };

   ##===========================================================================
   if( ! $r ) { $dbh->rollback(); }
   else       { $dbh->commit();   }
   $dbh->{AutoCommit} = 1;

   return $r;
}

##******************************************************************************
## Method Org_DeleteTask  public
##  Description  : deletes a task
##******************************************************************************
sub Org_DeleteTask
{
   my ($self, $context, $taskrowid, $groupRowid) = @_;

   my $dbh = $context->GetConfig()->GetDBH();
   
   my $sql = "DELETE from org_task WHERE rowid = $taskrowid";

   my $r = $dbh->do( $sql );
   if( $r eq "0E0" )
   {
      # $self->{dbh}->{AutoCommit} = 1;
      throw Mioga2::Exception::DB("Mioga2::Organizer::Database->Org_DeleteTask", $DBI::err, $DBI::errstr, $sql);
   }
   if( ! $r )
   {
      throw Mioga2::Exception::DB("Mioga2::Organizer::Database->Org_DeleteTask", $DBI::err, $DBI::errstr, $sql);
   }

   return 1;
}

##******************************************************************************
## Method Org_GetTask  public
##  Description  : gives information about a task
##******************************************************************************
sub Org_GetTask
{
   my ($self, $context, $taskrowid, $tasktype, $groupRowid) = @_;

   my $dbh = $context->GetConfig()->GetDBH();
   
   my($sql) = "SELECT org_${tasktype}_task.*, task_category.private, task_category.bgcolor, task_category.fgcolor from org_${tasktype}_task, task_category " .
              "WHERE org_${tasktype}_task.rowid='$taskrowid' AND org_${tasktype}_task.group_id='$groupRowid' AND task_category.rowid = org_${tasktype}_task.category_id";
   my $r = SelectSingle( $dbh, $sql );

   if (!defined($r))
   {
   	  throw Mioga2::Exception::Simple("Mioga2::Organizer::Database->Org_GetTask",
      __x("Cannot get task for (rowid, goupid) = ('{task_id}', '{group_id}')", task_id => $taskrowid, group_id => $groupRowid));
   	  return undef;
   }

   $r->{rowid} = "$r->{rowid}";
   $r->{type}  = $tasktype;

   if($tasktype eq 'todo') {
	   my $res = SelectSingle($dbh, "SELECT * FROM org_todo_task_priority WHERE rowid = $r->{priority_id}");
	   $r->{priority} = $res->{ident};
	   delete $r->{priority_id};
   }

   return $r;
}

##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
##
##                          Listing tasks
##
##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

##******************************************************************************
## Method _Org_ListPeriodicTasks  private
##  Description  : gives a list of periodic tasks between 2 dates
##******************************************************************************
sub _Org_ListPeriodicTasks
{
    my($self, $context, $groupRowid, $userTimeZone, $startDate, $endDate) = @_;
	my $dbh = $context->GetConfig()->GetDBH();
   
	# Convert stardDate and endDate to GMT  as they are stored as-is in DB
	local $ENV{TZ} = $userTimeZone;
	tzset();
	my ($year,$month,$day,$hour,$min) = ( $startDate =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
	my $sec = strftime('%s', 0, $min, $hour, $day, $month-1, $year-1900);
	$startDate = strftime( "%Y-%m-%d %H:%M:%S", gmtime($sec) );
	($year,$month,$day,$hour,$min) = ( $endDate =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
	$sec = strftime('%s', 0, $min, $hour, $day, $month-1, $year-1900);
	$endDate = strftime( "%Y-%m-%d %H:%M:%S", gmtime($sec) );

    my($sql) = "SELECT org_periodic_task.rowid, 'periodic' AS type, org_periodic_task.category_id, org_periodic_task.name, org_periodic_task.is_planned, task_category.bgcolor, task_category.fgcolor, 
                       task_category.private, " .
	           "       org_periodic_task.first_name, org_periodic_task.last_name, ".
                      "org_periodic_task.duration, org_periodic_task.repetition, org_periodic_task.start AS base, " .
                      "CASE WHEN org_periodic_task.start >= '$startDate' THEN org_periodic_task.start ELSE '$startDate' END AS start, " .
                      "CASE WHEN COALESCE(org_periodic_task.stop, '29991231') <= '$endDate' THEN org_periodic_task.stop ELSE '$endDate' END AS stop ".
               "FROM org_periodic_task, task_category " .
               "WHERE org_periodic_task.group_id = $groupRowid " .
			   "AND task_category.rowid = org_periodic_task.category_id " .
               "AND   org_periodic_task.start <=  '$endDate' " .
               "AND   org_periodic_task.is_planned =  't' " .
               "AND   COALESCE(org_periodic_task.stop, '29991231')   >= '$startDate'";

    my($defs) = SelectMultiple( $dbh, $sql);
    my($ptasks) = [];

    for my $def (@$defs)
    {
        my ($baseYear,$baseMonth,$baseDay,$baseHour,$baseMin) = ( $def->{base} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
        my ($startYear,$startMonth,$startDay,$startHour,$startMin) = ( $def->{start} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
        my ($stopYear,$stopMonth,$stopDay,$stopHour,$stopMin) = ( $def->{stop} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
        my ($baseLocal, $startLocal, $stopLocal);

        {
            local $ENV{TZ} = 'GMT';
			tzset();
            my($baseSec) = strftime('%s', 0, $baseMin, $baseHour, $baseDay, $baseMonth-1, $baseYear-1900);
            my($startSec) = strftime('%s', 0, $startMin, $startHour, $startDay, $startMonth-1, $startYear-1900);
            my($stopSec) = strftime('%s', 0, $stopMin, $stopHour, $stopDay, $stopMonth-1, $stopYear-1900);

            local $ENV{TZ} = $userTimeZone;
			tzset();
            $baseLocal = strftime("%Y-%m-%d %H:%M:%S", localtime($baseSec));
            $startLocal = strftime("%Y-%m-%d %H:%M:%S", localtime($startSec));
            $stopLocal = strftime("%Y-%m-%d %H:%M:%S", localtime($stopSec));
        }
		tzset();

        for my $date (ParseRecur($def->{repetition}, $baseLocal, $startLocal, $stopLocal))
        {
            my($pgdate) = UnixDate($date, '%Y-%m-%d %H:%M:%S');
            my($r) = SelectSingle($dbh,
                         "SELECT 1 FROM org_cancelled_periodic_task WHERE task_id='$def->{rowid}' AND date='$pgdate'");

            if(!$r)
            {
                local $ENV{TZ} = $userTimeZone;
				tzset();
                my ($year,$month,$day,$hour,$min) = split(/,/, UnixDate($date, '%Y,%m,%d,%H,%M'));
                my($sec) = strftime('%s', 0, $min, $hour, $day, $month-1, $year-1900);

                local $ENV{TZ} = 'GMT';
				tzset();
                $date = strftime("%Y-%m-%d %H:%M:%S", localtime($sec));

                push(@$ptasks, {
                                rowid =>          "$def->{rowid}",
                                name =>           $def->{name},
								type =>          $def->{type},
								category_id =>    $def->{category_id},
								bgcolor =>          $def->{bgcolor},
								fgcolor =>          $def->{fgcolor},
                                private =>        $def->{private},
								first_name =>          $def->{first_name},
                                last_name =>        $def->{last_name},
                                start =>          $date,
                                duration => $def->{duration},
                                is_planned => $def->{is_planned},
                               });
            }
			tzset();
        }
    }

    return $ptasks;
}


##******************************************************************************
## Method _Org_ListAllPeriodicTasks  private
##  Description  : gives a list of periodic tasks between 2 dates
##******************************************************************************
sub _Org_ListAllPeriodicTasks
{
    my($self, $context, $groupRowid, $userTimeZone, $startDate, $endDate) = @_;
	my $dbh = $context->GetConfig()->GetDBH();
   
	# Convert stardDate and endDate to GMT  as they are stored as-is in DB
	local $ENV{TZ} = $userTimeZone;
	tzset();
	my ($year,$month,$day,$hour,$min) = ( $startDate =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
	my $sec = strftime('%s', 0, $min, $hour, $day, $month-1, $year-1900);
	$startDate = strftime( "%Y-%m-%d %H:%M:%S", gmtime($sec) );
	($year,$month,$day,$hour,$min) = ( $endDate =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
	$sec = strftime('%s', 0, $min, $hour, $day, $month-1, $year-1900);
	$endDate = strftime( "%Y-%m-%d %H:%M:%S", gmtime($sec) );
   
    my($sql) = "SELECT org_periodic_task.rowid, 'periodic' AS type, org_periodic_task.category_id, org_periodic_task.name, org_periodic_task.is_planned, task_category.bgcolor, task_category.fgcolor, 
                       task_category.private, " .
	           "       org_periodic_task.first_name, org_periodic_task.last_name, ".
                      "org_periodic_task.duration, org_periodic_task.repetition, org_periodic_task.outside, org_periodic_task.start AS base, " .
                      "CASE WHEN org_periodic_task.start >= '$startDate' THEN org_periodic_task.start ELSE '$startDate' END AS start, " .
                      "CASE WHEN COALESCE(org_periodic_task.stop, '29991231') <= '$endDate' THEN org_periodic_task.stop ELSE '$endDate' END AS stop ".
               "FROM org_periodic_task, task_category " .
               "WHERE org_periodic_task.group_id = $groupRowid " .
			   "AND task_category.rowid = org_periodic_task.category_id " .
               "AND   org_periodic_task.start <=  '$endDate' " .
               "AND   COALESCE(org_periodic_task.stop, '29991231')   >= '$startDate'";

    my($defs) = SelectMultiple( $dbh, $sql);
    my($ptasks) = [];

    for my $def (@$defs)
    {
        my ($baseYear,$baseMonth,$baseDay,$baseHour,$baseMin) = ( $def->{base} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
        my ($startYear,$startMonth,$startDay,$startHour,$startMin) = ( $def->{start} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
        my ($stopYear,$stopMonth,$stopDay,$stopHour,$stopMin) = ( $def->{stop} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
        my ($baseLocal, $startLocal, $stopLocal);

        {
			local $ENV{TZ} = 'GMT';
			tzset();
            my($baseSec) = strftime('%s', 0, $baseMin, $baseHour, $baseDay, $baseMonth-1, $baseYear-1900);
            my($startSec) = strftime('%s', 0, $startMin, $startHour, $startDay, $startMonth-1, $startYear-1900);
            my($stopSec) = strftime('%s', 0, $stopMin, $stopHour, $stopDay, $stopMonth-1, $stopYear-1900);

			local $ENV{TZ} = $userTimeZone;
			tzset();
            $baseLocal = strftime("%Y-%m-%d %H:%M:%S", localtime($baseSec));
            $startLocal = strftime("%Y-%m-%d %H:%M:%S", gmtime($startSec));
            $stopLocal = strftime("%Y-%m-%d %H:%M:%S", localtime($stopSec));
        }
		tzset();

        for my $date (ParseRecur($def->{repetition}, $baseLocal, $startLocal, $stopLocal))
        {
            my($pgdate) = UnixDate($date, '%Y-%m-%d %H:%M:%S');
            my($r) = SelectSingle($dbh,
                         "SELECT 1 FROM org_cancelled_periodic_task WHERE task_id='$def->{rowid}' AND date='$pgdate'");

            if(!$r)
            {
				local $ENV{TZ} = $userTimeZone;
				tzset();
                my ($year,$month,$day,$hour,$min) = split(/,/, UnixDate($date, '%Y,%m,%d,%H,%M'));
                my($sec) = strftime('%s', 0, $min, $hour, $day, $month-1, $year-1900);

				local $ENV{TZ} = 'GMT';
				tzset();
                $date = strftime("%Y-%m-%d %H:%M:%S", localtime($sec));

                push(@$ptasks, {
                                rowid =>          "$def->{rowid}",
                                name =>           $def->{name},
								type =>          $def->{type},
								category_id =>    $def->{category_id},
								bgcolor =>          $def->{bgcolor},
								fgcolor =>          $def->{fgcolor},
                                private =>        $def->{private},
								first_name =>          $def->{first_name},
                                last_name =>        $def->{last_name},
                                start =>          $date,
                                duration => $def->{duration},
                                is_planned => $def->{is_planned},
								outside => $def->{outside},
                               });
            }
			tzset();
        }
    }

    return $ptasks;
}

##******************************************************************************
## Method _Org_ListNotPlannedPeriodicTasks  private
##  Description  : gives a list of periodic tasks between 2 dates
##******************************************************************************
sub _Org_ListNotPlannedPeriodicTasks
{
    my($self, $context, $groupRowid, $startDate, $endDate) = @_;

	my $dbh = $context->GetConfig()->GetDBH();
   
    my($sql) = "SELECT org_periodic_task.rowid, 'periodic' AS type, org_periodic_task.category_id, org_periodic_task.name, task_category.bgcolor, org_periodic_task.is_planned,  task_category.fgcolor, 
                       task_category.private, " .
	           "       org_periodic_task.first_name, org_periodic_task.last_name, ".
                      "org_periodic_task.duration, org_periodic_task.repetition, org_periodic_task.start AS base, " .
                      "CASE WHEN org_periodic_task.start > '$startDate' THEN org_periodic_task.start ELSE '$startDate 00:00:00' END AS start, " .
                      "CASE WHEN COALESCE(org_periodic_task.stop, '29991231') < '$endDate' THEN org_periodic_task.stop ELSE '$endDate 23:59:59' END AS stop ".
               "FROM org_periodic_task, task_category " .
               "WHERE org_periodic_task.group_id = $groupRowid " .
			   "AND task_category.rowid = org_periodic_task.category_id " .
               "AND   org_periodic_task.start <  '$endDate 23:59:59' " .
               "AND   org_periodic_task.is_planned =  'f' " .
               "AND   COALESCE(org_periodic_task.stop, '29991231')   >= '$startDate 00:00:00'";

    my($defs) = SelectMultiple( $dbh, $sql);

    my($ptasks) = [];

    for my $def (@$defs)
    {

        for my $date (ParseRecur($def->{repetition}, $def->{base}, "$startDate 00:00:00", "$endDate 23:59:59"))
        {
            my($pgdate) = UnixDate($date, '%Y-%m-%d %H:%M:%S');
            my($r) = SelectSingle($dbh,
                         "SELECT 1 FROM org_cancelled_periodic_task WHERE task_id='$def->{rowid}' AND date='$pgdate'");

            if(!$r)
            {
                my ($year,$month,$day,$hour,$min) = split(/,/, UnixDate($date, '%Y,%m,%d,%H,%M'));
                my($sec) = strftime('%s', 0, $min, $hour, $day, $month-1, $year-1900);

                $date = strftime("%Y-%m-%d %H:%M:%S", localtime($sec));

                push(@$ptasks, {
                                rowid =>          "$def->{rowid}",
                                name =>           $def->{name},
								type =>          $def->{type},
								category_id =>    $def->{category_id},
								bgcolor =>          $def->{bgcolor},
								fgcolor =>          $def->{fgcolor},
                                private =>        $def->{private},
								first_name =>          $def->{first_name},
                                last_name =>        $def->{last_name},
                                start =>          $date,
                                duration => $def->{duration},
                               });
            }
        }
    }

    return $ptasks;
}

##******************************************************************************
## Method Org_ListNotPlannedTasks  public
##  Description  : gives a list not planned tasks between $startDate and $stopDate.
##******************************************************************************
sub Org_ListNotPlannedTasks
{
   my ($self, $context, $groupRowid, $startDate, $stopDate) = @_;

   my $dbh = $context->GetConfig()->GetDBH();

   my $sql = "SELECT org_planned_task.rowid AS rowid, 'planned' AS type, org_planned_task.name, org_planned_task.category_id, org_planned_task.start, org_planned_task.duration, org_planned_task.is_planned, ".
	         "       org_planned_task.first_name, org_planned_task.last_name, ".
	         "       task_category.fgcolor, task_category.bgcolor, task_category.private ".

             "FROM org_planned_task, task_category, org_task_type ".

			 "WHERE org_planned_task.group_id = $groupRowid AND ".
			 "      task_category.rowid = category_id AND ".
			 "      org_planned_task.start  >= '$startDate 00:00:00' AND ".
			 "      org_planned_task.start < '$stopDate 23:59:59' AND ".
			 "      org_planned_task.is_planned = 'f' AND ".
			 "      org_task_type.rowid = org_planned_task.type_id AND ".
			 "      org_task_type.ident = 'planned' ".

			 "ORDER BY org_planned_task.start, org_planned_task.duration ASC"; 

   
   my $r = SelectMultiple( $dbh, $sql );
   
   my($ptasks) = $self->_Org_ListNotPlannedPeriodicTasks($context, $groupRowid, $startDate, $stopDate);

   if(@$ptasks)
   {
       my(@sorted) = sort { $a->{start} cmp $b->{start} || $a->{duration} <=> $b->{duration} }
                     (@$r, @$ptasks);

       $r = [ @sorted ];
   }

   return $r;
}

##******************************************************************************
## Method Org_ListStrictPeriodicTasks  public
##  Description  : gives a list of strict or periodic tasks between 2 dates
##******************************************************************************
sub Org_ListStrictPeriodicTasks
{
   my ($self, $context, $groupRowid, $userTimeZone, $startDate, $stopDate) = @_;
   my $dbh = $context->GetConfig()->GetDBH();
   

   my $sql = "SELECT org_planned_task.rowid AS rowid, 'planned' AS type, org_planned_task.name, org_planned_task.description, org_planned_task.category_id, org_planned_task.start, org_planned_task.duration, org_planned_task.is_planned, ".
	         "       org_planned_task.first_name, org_planned_task.last_name, ".
	         "       task_category.fgcolor, task_category.bgcolor, task_category.private ".

             "FROM org_planned_task, task_category, org_task_type ".

			 "WHERE org_planned_task.group_id = $groupRowid AND ".
			 "      task_category.rowid = category_id AND ".
			 "      org_planned_task.start  >= '$startDate' AND ".
			 "      org_planned_task.start < '$stopDate' AND ".
			 "      org_planned_task.is_planned = 't' AND ".
			 "      org_task_type.rowid = org_planned_task.type_id AND ".
			 "      org_task_type.ident = 'planned' ".

			 "ORDER BY org_planned_task.start, org_planned_task.duration ASC";

#
   ### duration_value  is not enouth for a 2nd sort-criteria, we need to have duration_value*duration_unit but ...

   my $r = SelectMultiple( $dbh, $sql );

   my($ptasks) = $self->_Org_ListPeriodicTasks($context, $groupRowid, $userTimeZone, $startDate, $stopDate);

   if(@$ptasks)
   {
       my(@sorted) = sort { $a->{start} cmp $b->{start} || $a->{duration} <=> $b->{duration} }
                     (@$r, @$ptasks);

       $r = [ @sorted ];
   }


   return $r;
}

##******************************************************************************
## Method Org_ListAllStrictPeriodicTasks  public
##  Description  : gives a list of all strict or periodic tasks between 2 dates
##******************************************************************************
sub Org_ListAllStrictPeriodicTasks
{
   my ($self, $context, $groupRowid, $userTimeZone, $startDate, $stopDate) = @_;
   my $dbh = $context->GetConfig()->GetDBH();
   

   my $sql = "SELECT org_planned_task.rowid AS rowid, 'planned' AS type, org_planned_task.name, org_planned_task.description, org_planned_task.category_id, org_planned_task.start, org_planned_task.duration, org_planned_task.is_planned, ".
	         "       org_planned_task.first_name, org_planned_task.last_name, org_planned_task.outside, ".
	         "       task_category.fgcolor, task_category.bgcolor, task_category.private ".

             "FROM org_planned_task, task_category, org_task_type ".

			 "WHERE org_planned_task.group_id = $groupRowid AND ".
			 "      task_category.rowid = category_id AND ".
			 "      org_planned_task.start  >= '$startDate' AND ".
			 "      org_planned_task.start < '$stopDate' AND ".
			 "      org_task_type.rowid = org_planned_task.type_id AND ".
			 "      org_task_type.ident = 'planned' ".

			 "ORDER BY org_planned_task.start, org_planned_task.duration ASC";

#
   ### duration_value  is not enouth for a 2nd sort-criteria, we need to have duration_value*duration_unit but ...

   my $r = SelectMultiple( $dbh, $sql );

   my($ptasks) = $self->_Org_ListAllPeriodicTasks($context, $groupRowid, $userTimeZone, $startDate, $stopDate);

   if(@$ptasks)
   {
       my(@sorted) = sort { $a->{start} cmp $b->{start} || $a->{duration} <=> $b->{duration} }
                     (@$r, @$ptasks);

       $r = [ @sorted ];
   }


   return $r;
}

##******************************************************************************
## Method Org_ListStrictPeriodicTasksForExternalUser  public
##  Description  : gives a list of strict or periodic tasks between 2 dates
##******************************************************************************
sub Org_ListStrictPeriodicTasksForExternalUser
{
   my ($self, $context, $user_id, $userTimeZone, $startDate, $stopDate) = @_;
   my $dbh = $context->GetConfig()->GetDBH();
   
   my $ext_mioga = new Mioga2::ExternalMioga($context->GetConfig(), user_rowid => $user_id);

   my $user = new Mioga2::Old::User($context->GetConfig(), rowid => $user_id);

   my $response = $ext_mioga->SendRequest(application => 'Organizer', method => 'SXMLListStringOrPeriodicTasks',
											   args => {user_ident => $user->GetRemoteIdent(),
														start      => $startDate,
														stop       => $stopDate
													   }
											   );
		
   my $xs = new Mioga2::XML::Simple();
   my $xmltree = $xs->XMLin($response);

   my $tasks = $xmltree->{Tasks}->{task};
   if(ref($tasks) ne 'ARRAY') {
	   $tasks = [$tasks];
   }
   
   foreach my $task (@$tasks) {
	   foreach my $key (keys %$task) {
		   if(ref ($task->{$key}) eq 'HASH') {
			   $task->{$key} = '';
		   }
	   }
   }

   return $tasks;
}

##******************************************************************************
## Method Org_ListTodoTasks  public
##  Description  : gives the list of todo tasks for a group
##******************************************************************************
sub Org_ListTodoTasks
{
   my ($self, $context, $groupRowid) = @_;
	my $dbh = $context->GetConfig()->GetDBH();
   

   my $sql = "SELECT org_todo_task.rowid  AS rowid, 'todo' AS type, org_todo_task.done,  org_todo_task.name, org_todo_task.category_id, task_category.bgcolor, task_category.fgcolor, task_category.private, org_todo_task_priority.ident AS priority ".
             "FROM org_todo_task, task_category, org_todo_task_priority WHERE org_todo_task.group_id = $groupRowid AND task_category.rowid = category_id AND org_todo_task_priority.rowid = org_todo_task.priority_id";
   #$sql .= " ORDER BY org_todo_task_priority.ident, org_todo_task.created ASC";
   $sql .= " ORDER BY org_todo_task.priority_id, org_todo_task.created ASC";

   my $r1 = SelectMultiple( $dbh, $sql );

   my $r2 = [];
   my $userRowId = $context->GetGroup()->GetAnimId();
   if( defined( $userRowId ) )
   {
	   #$r2 = Mioga2::tools::APIGlobalTask::GTaskListTodoTasks( $self->{dbh}, $userRowId );
   }

   return [ @$r1, @$r2 ];
}


##******************************************************************************
## Method Org_ListFlexibleTasks  public
##  Description  : gives a list of flexible tasks between 2 dates
##******************************************************************************
sub Org_ListFlexibleTasks
{
   my ($self, $context, $userRowid, $minDate, $maxDate) = @_;

   my $dbh = $context->GetConfig()->GetDBH();

#    ##===========================================================================
#    my $s = Mioga2::tools::APIProject::ProjectListProjectTasks( $self->{dbh}, $userRowId,
#                                           $startDate, $stopDate );


   my $tasks = SelectMultiple($dbh, "SELECT org_flexible_task.*, 'flexible' AS type, task_category.bgcolor, task_category.fgcolor, task_category.private ".
							        "FROM org_flexible_task, task_category ".
							        "WHERE org_flexible_task.delegate_id = $userRowid AND ".
							        "      org_flexible_task.stop >= '$minDate' AND org_flexible_task.start <= '$maxDate' AND ".
							        "      task_category.rowid = org_flexible_task.category_id");

   ##===========================================================================
   my ($tmpYear,$tmpMonth,$tmpDay,$tmpHour,$tmpMin);
   
   ($tmpYear,$tmpMonth,$tmpDay,$tmpHour,$tmpMin) = ( $minDate =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
   my $minSec = strftime('%s', 0, $tmpMin, $tmpHour, $tmpDay, $tmpMonth-1, $tmpYear-1900);
   
   ($tmpYear,$tmpMonth,$tmpDay,$tmpHour,$tmpMin) = ( $maxDate =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
   my $maxSec = strftime('%s', 0, $tmpMin, $tmpHour, $tmpDay, $tmpMonth-1, $tmpYear-1900);
   
    ##===========================================================================
   foreach my $task (@$tasks)
   {
       my $tmpSec;
	   
       ($tmpYear,$tmpMonth,$tmpDay,$tmpHour,$tmpMin) = ( $task->{start} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
       $tmpSec = strftime('%s', 0, $tmpMin, $tmpHour, $tmpDay, $tmpMonth-1, $tmpYear-1900);
	   
       $tmpSec = $minSec  if( $minSec > $tmpSec );
       $task->{start} = strftime("%Y-%m-%d %H:%M:%S", gmtime($tmpSec));
	   
	   
       ($tmpYear,$tmpMonth,$tmpDay,$tmpHour,$tmpMin) = ( $task->{stop} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
       $tmpSec = strftime('%s', 0, $tmpMin, $tmpHour, $tmpDay, $tmpMonth-1, $tmpYear-1900);
	   
       $tmpSec = $maxSec  if( $maxSec < $tmpSec );
       $task->{stop} = strftime("%Y-%m-%d %H:%M:%S", gmtime($tmpSec));
    }
   
    ##===========================================================================
   my @sorted_tasks = sort { $a->{start} cmp $b->{start} } @$tasks;
   
   return \@sorted_tasks;
}

##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
##
##                            Meetings
##
##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

##******************************************************************************
## Method Org_GetMeetingList  public
##  Description  : gives list of meeting definitions for a given resource
##******************************************************************************
sub Org_GetMeetingList
{
   my ($self, $context, $group_id) = @_;

   my $dbh = $context->GetConfig()->GetDBH();

   my($sql) = "SELECT org_meeting.*, task_category.fgcolor, task_category.bgcolor, task_category.private,
                      (SELECT count(*) FROM org_meeting_resource WHERE meeting_id = org_meeting.rowid AND task_id IS NOT NULL) AS nb_task
               FROM org_meeting, task_category
               WHERE org_meeting.group_id=$group_id AND task_category.rowid = org_meeting.category_id ".
              "ORDER BY org_meeting.created, org_meeting.rowid";
   my $r = SelectMultiple( $dbh, $sql );

   return $r;
}

##******************************************************************************
## Method Org_CreateMeeting  public
##  Description  : creates a meeting definition
##******************************************************************************
sub Org_CreateMeeting
{
   my ($self, $context) = @_;
   
   my $dbh = $context->GetConfig->GetDBH();
   my $session = $context->GetSession();

   my $fields = $session->{organizer}->{meeting}->{values};

   BeginTransaction($dbh);

   my $rowid;
   try {
	   $rowid = SelectSingle($dbh, "SELECT nextval('org_meeting_rowid_seq') as rowid");
	   
	   $fields->{rowid} = $rowid->{rowid};
	   $fields->{created} = 'now()';
	   $fields->{modified} = 'now()';
	   $fields->{group_id} = $context->GetGroup()->GetRowid();
	   $fields->{duration} = $fields->{duration_hour}*3600 + $fields->{duration_min}*60;
	   
	   my $sql = BuildInsertRequest($fields, table  => "org_meeting",
									         string => [qw(description name)],
								             other  => [qw(rowid created modified category_id group_id duration)]);
	   
	   
	   ExecSQL($dbh, $sql);
	   
	   
	   foreach my $date (@{$fields->{dates}}) {
		   my $isodate = du_SecondToISO($date->{date});
		   
		   ExecSQL($dbh, "INSERT INTO org_meeting_date (meeting_id, date) VALUES($rowid->{rowid}, '$isodate')");

		   $date->{rowid} = $date->{date};
	   }
	   
	   
	   foreach my $user (@{$fields->{users}}, @{$fields->{resources}}) {
		   my $values = { meeting_id => $rowid->{rowid},
						  resource_id => $user,
						  };
		   
		   my $sql = BuildInsertRequest($values, table  => "org_meeting_resource");
		   ExecSQL($dbh, $sql);
	   }
   }

   otherwise {
	   my $err = shift;

	   RollbackTransaction($dbh);

	   $err->throw();
   };

   EndTransaction($dbh);

   return $rowid->{rowid};
}

##******************************************************************************
## Method Org_UpdateMeeting  public
##  Description  : update a meeting definition
##******************************************************************************
sub Org_UpdateMeeting
{
   my ($self, $context) = @_;
   
   my $dbh = $context->GetConfig->GetDBH();
   my $session = $context->GetSession();

   my $fields = $session->{organizer}->{meeting}->{values};

   BeginTransaction($dbh);

   try {
	   $fields->{modified} = 'now()';
	   $fields->{duration} = $fields->{duration_hour}*3600 + $fields->{duration_min}*60;
	   
	   my $sql = BuildUpdateRequest($fields, table  => "org_meeting",
									         string => [qw(description name)],
								             other  => [qw(modified category_id duration)]);
	   
	   
	   ExecSQL($dbh, $sql." WHERE rowid = $fields->{rowid}");
	   
	   #***************
	   # Modify Dates
	   #**************
	   
	   # Remove deleted dates 
	   #
	   ExecSQL($dbh, "DELETE FROM org_meeting_date ".
 			         "WHERE meeting_id = $fields->{rowid}");

	   #
	   # Add new dates
	   #
	   foreach my $date (@{$fields->{dates}}) {
		   my $isodate = du_SecondToISO($date->{date});
		   
		   ExecSQL($dbh, "INSERT INTO org_meeting_date (meeting_id, date) VALUES($fields->{rowid}, '$isodate')");

		   $date->{rowid} = $date->{date};
	   }


	   #***************
	   # Modify Users/Resources
	   #**************	   

	   #
	   # Remove deleted users/resources
	   #

	   if(@{$fields->{users}} or @{$fields->{resources}}) {
		   ExecSQL($dbh, "DELETE FROM org_meeting_resource ".
				         "WHERE meeting_id = $fields->{rowid} AND ".
				         "      resource_id NOT IN (".join(',', @{$fields->{users}}, @{$fields->{resources}}).")");
	   }
	   

	   #
	   # Add new users
	   #
	   
	   my $res = SelectMultiple($dbh, "SELECT * FROM org_meeting_resource WHERE meeting_id = $fields->{rowid}");
	   my @cur_users = map {$_->{resource_id}} @$res;

	   foreach my $user (@{$fields->{users}}, @{$fields->{resources}}) {
		   next if(grep {$_ == $user} @cur_users);

		   my $values = { meeting_id => $fields->{rowid},
						  resource_id => $user,
						  };
		   
		   my $sql = BuildInsertRequest($values, table  => "org_meeting_resource");
		   ExecSQL($dbh, $sql);
	   }
   }

   otherwise {
	   my $err = shift;

	   RollbackTransaction($dbh);

	   $err->throw();
   };

   EndTransaction($dbh);
}

##******************************************************************************
## Method Org_GetMeeting  public
##  Description  : update a meeting definition
##******************************************************************************
sub Org_GetMeeting
{
   my ($self, $context, $rowid) = @_;
   
   my $dbh = $context->GetConfig->GetDBH();

   my $fields = SelectSingle($dbh, "SELECT *, (duration/3600) AS duration_hour, (duration%3600) AS duration_min ".
							       "FROM org_meeting WHERE rowid = $rowid");
   

   my $res = SelectMultiple($dbh, "SELECT org_meeting_resource.* FROM org_meeting_resource, m_user_base ".
							      "WHERE meeting_id = $rowid AND m_user_base.rowid = resource_id");

   @{$fields->{users}} = map {$_->{resource_id}} @$res;


   $res = SelectMultiple($dbh, "SELECT org_meeting_resource.* FROM org_meeting_resource, m_resource ".
							      "WHERE meeting_id = $rowid AND m_resource.rowid = resource_id");

   @{$fields->{resources}} = map {$_->{resource_id}} @$res;



   $res = SelectMultiple($dbh, "SELECT * FROM org_meeting_date ".
							      "WHERE meeting_id = $rowid");
   
   @{$fields->{dates}} = map { { rowid => $_->{date}, date => du_ISOToSecond($_->{date}) } } @$res;

   return $fields;
}


##******************************************************************************
## Method Org_DeleteMeeting  public
##  Description  : update a meeting definition
##******************************************************************************
sub Org_DeleteMeeting
{
   my ($self, $context, $rowid) = @_;
   
   $self->Org_MeetingDeleteTasks($context, $rowid);

   my $dbh = $context->GetConfig->GetDBH();

   ExecSQL($dbh, "DELETE FROM org_meeting_date WHERE meeting_id = $rowid");
   ExecSQL($dbh, "DELETE FROM org_meeting_resource WHERE meeting_id = $rowid");
   ExecSQL($dbh, "DELETE FROM org_meeting WHERE rowid = $rowid");
}



##******************************************************************************
## Method Org_DeleteMeetingKeepTask  public
##  Description  : update a meeting definition
##******************************************************************************
sub Org_DeleteMeetingKeepTask
{
   my ($self, $context, $rowid) = @_;
   
   my $dbh = $context->GetConfig->GetDBH();

   ExecSQL($dbh, "DELETE FROM org_meeting_date WHERE meeting_id = $rowid");
   ExecSQL($dbh, "DELETE FROM org_meeting_resource WHERE meeting_id = $rowid");
   ExecSQL($dbh, "DELETE FROM org_meeting WHERE rowid = $rowid");
}

##******************************************************************************
## Method Org_UpdateMeetingResource  public
##  Description  : creates a meeting definition
##******************************************************************************
sub Org_UpdateMeetingResource
{
   my ($self, $context, $meeting_id, $resource_id, $task_id) = @_;
   
   my $sql = "UPDATE org_meeting_resource SET task_id=$task_id ";
   $sql .=   "WHERE  meeting_id=$meeting_id AND resource_id=$resource_id";
   
   ExecSQL($context->GetConfig()->GetDBH(), $sql);
}



##******************************************************************************
## Method Org_MeetingGetNbTask  public
## Description  : get number of tasks of a given meeting
##******************************************************************************
sub Org_MeetingGetNbTask
{
   my ($self, $context, $meeting_id) = @_;
   

   my $res = SelectSingle($context->GetConfig()->GetDBH(), 
						  "SELECT count(*) FROM org_meeting_resource WHERE meeting_id = $meeting_id AND task_id IS NOT NULL");
   

   return $res->{count};
}



##******************************************************************************
## Method Org_MeetingDeleteTasks  public
## Description  : delete tasks associated to the meeting
##******************************************************************************
sub Org_MeetingDeleteTasks
{
   my ($self, $context, $meeting_id) = @_;
   
   my $dbh = $context->GetConfig()->GetDBH();

   my $res = SelectMultiple($dbh, "SELECT org_meeting_resource.* FROM org_meeting_resource, m_user_base ".
							      "WHERE org_meeting_resource.meeting_id = $meeting_id AND org_meeting_resource.task_id IS NOT NULL AND ".
							      "      m_user_base.rowid = org_meeting_resource.resource_id");
   
   if(@$res) {

	   foreach my $user (@$res) {
		   $self->Org_MeetingDeleteTaskForExternalUser($context, $user->{resource_id}, $user->{task_id});
	   }
	   
	   ExecSQL($dbh, "UPDATE org_meeting_resource SET task_id = NULL WHERE meeting_id = $meeting_id AND resource_id IN (".
		             join(',', map {$_->{resource_id}} @$res).")");

   }


   $res = SelectMultiple($dbh, "SELECT * FROM org_meeting_resource WHERE meeting_id = $meeting_id AND task_id IS NOT NULL");

   return if(! @$res);

   my $tasks = join(",", map {$_->{task_id}} @$res);

   ExecSQL($dbh, "DELETE FROM org_planned_task WHERE rowid IN ($tasks)");
}


##******************************************************************************
## Method Org_MeetingDeleteTaskForExternalUser  public
## Description  : delete tasks associated to the meeting
##******************************************************************************
sub Org_MeetingDeleteTaskForExternalUser
{
   my ($self, $context, $resource_id, $task_id) = @_;

   my $ext_mioga = new Mioga2::ExternalMioga($context->GetConfig(), user_rowid => $resource_id);

   my $response = $ext_mioga->SendRequest(application => 'Organizer', method => 'XMLDeleteTask',
										  args => { task_id => $task_id }
										  );
}
   

##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
##
##                            XML import / export
##
##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

##******************************************************************************
## Method Org_ExportToXML public
## Description  : export $groupId's tasks to XML
##******************************************************************************

=head1 Org_ExportToXML

Org_ExportToXML export $groupRowid's tasks to XML

Return a xml string like :

   <TaskList>
       <PlannedTask    .... attributes are the database fields .... >description</PlannedTask>
       <PeriodicTask ......
       <ToDoTask ......
   </TaskList>

This is the DTD :

   <!ELEMENT TaskList ( PlannedTask*, PeriodicTask*, ToDoTask*, Remove* ) >

   <!ELEMENT PlannedTask ( #PCDATA ) >
   <!ATTLIST PlannedTask rowid CDATA #REQUIRED >
   <!ATTLIST PlannedTask created CDATA #REQUIRED >
   <!ATTLIST PlannedTask modified CDATA #REQUIRED >
   <!ATTLIST PlannedTask name CDATA #REQUIRED >
   <!ATTLIST PlannedTask group_id CDATA #REQUIRED >
   <!ATTLIST PlannedTask start CDATA #REQUIRED >
   <!ATTLIST PlannedTask duration CDATA #REQUIRED >
   <!ATTLIST PlannedTask outside CDATA #REQUIRED >
   <!ATTLIST PlannedTask telephone CDATA #REQUIRED >
   <!ATTLIST PlannedTask first_name CDATA #REQUIRED >
   <!ATTLIST PlannedTask last_name CDATA #REQUIRED >
   <!ATTLIST PlannedTask email CDATA #REQUIRED >
   <!ATTLIST PlannedTask fax CDATA #REQUIRED >


   <!ELEMENT PeriodicTask ( #PCDATA, Cancelled* ) >
   <!ATTLIST PeriodicTask rowid CDATA #REQUIRED >
   <!ATTLIST PeriodicTask created CDATA #REQUIRED >
   <!ATTLIST PeriodicTask modified CDATA #REQUIRED >
   <!ATTLIST PeriodicTask name CDATA #REQUIRED >
   <!ATTLIST PeriodicTask group_id CDATA #REQUIRED >
   <!ATTLIST PeriodicTask start CDATA #REQUIRED >
   <!ATTLIST PeriodicTask duration CDATA #REQUIRED >
   <!ATTLIST PeriodicTask repetition CDATA #REQUIRED >
   <!ATTLIST PeriodicTask stop CDATA #REQUIRED >
   <!ATTLIST PeriodicTask outside CDATA #REQUIRED >
   <!ATTLIST PeriodicTask telephone CDATA #REQUIRED >
   <!ATTLIST PeriodicTask first_name CDATA #REQUIRED >
   <!ATTLIST PeriodicTask last_name CDATA #REQUIRED >
   <!ATTLIST PeriodicTask email CDATA #REQUIRED >
   <!ATTLIST PeriodicTask fax CDATA #REQUIRED >

   <!ELEMENT Cancelled EMPTY >
   <!ATTLIST Cancelled created CDATA #REQUIRED >
   <!ATTLIST Cancelled modified CDATA #REQUIRED >
   <!ATTLIST Cancelled date CDATA #REQUIRED >

   <!ELEMENT TodoTask ( #PCDATA ) >
   <!ATTLIST TodoTask rowid CDATA #REQUIRED >
   <!ATTLIST TodoTask created CDATA #REQUIRED >
   <!ATTLIST TodoTask modified CDATA #REQUIRED >
   <!ATTLIST TodoTask name CDATA #REQUIRED >
   <!ATTLIST TodoTask group_id CDATA #REQUIRED >
   <!ATTLIST TodoTask outside CDATA #REQUIRED >
   <!ATTLIST TodoTask priority CDATA #REQUIRED >
   <!ATTLIST TodoTask first_name CDATA #REQUIRED >
   <!ATTLIST TodoTask last_name CDATA #REQUIRED >
   <!ATTLIST TodoTask telephone CDATA #REQUIRED >
   <!ATTLIST TodoTask email CDATA #REQUIRED >
   <!ATTLIST TodoTask fax CDATA #REQUIRED >


   #PCDATA are the task descriptions

=cut

sub Org_ExportToXML
{
   my ($self, $context, $groupRowid, $modif_start_date, $synchro_ident) = @_;


   my $dbh = $context->GetConfig()->GetDBH();
   my $user = $context->GetUser();
   
   my $xml = "<TaskList>\n";
   $xml .= $self->_Org_ExportToXML_Task($context->GetConfig(), $user, $groupRowid, "org_planned_task", "planned", "PlannedTask", $modif_start_date,   $synchro_ident);
   $xml .= $self->_Org_ExportToXML_Task($context->GetConfig(), $user, $groupRowid, "org_periodic_task", "periodic", "PeriodicTask", $modif_start_date, $synchro_ident);
   $xml .= $self->_Org_ExportToXML_TodoTask($context->GetConfig(), $groupRowid, $modif_start_date, $synchro_ident );
   $xml .= "</TaskList>\n";
   return $xml;
}

sub _Org_ExportToXML_Task
{
   my ($self, $config, $user, $groupRowid, $table, $type, $tag, $modif_start_date, $synchro_ident) = @_;

   my $dbh = $config->GetDBH();
   my $mioga_id = $config->GetMiogaId();

   my $xml = "";
   my $list = SelectMultiple($dbh, "SELECT $table.* ".
							       "FROM $table, org_task_type ".
							       "WHERE group_id = $groupRowid AND ".
							       "      (modified >= '$modif_start_date' OR created >= '$modif_start_date') AND ".
							       "      org_task_type.rowid = $table.type_id AND ".
							       "      org_task_type.ident = '$type'");

   
   my $remoteids = $self->_Org_GetRemoteRowids($config, $synchro_ident, $list);

   foreach my $task (@$list)
   {
      $xml .= "   <$tag\n";
      while( my ($k,$v) = each( %$task ) )
      {
         next if( $k eq "description" );

         if( $table eq "org_periodic_task" and $k eq 'repetition') {
			 next;
		 }

         if( $k eq 'start' and $table eq "org_periodic_task") {
			 my $date = $v;
			 if($task->{is_planned}) {
				 $date = du_ConvertGMTISOToLocale($v, $user);
			 }

			 $date = du_FirstRecurDate ($config, $date, $task->{repetition});
			 $xml .= "      $k=\"".st_FormatXMLString($date)."\"\n";

			 next;
		 }

         elsif($k eq 'start') {
			 my $date = $v;
			 if($task->{is_planned}) {
				 $date = du_ConvertGMTISOToLocale($v, $user);
			 }
			 
			 $xml .= "      $k=\"".st_FormatXMLString($date)."\"\n";

			 next;
		 }
         elsif($k eq 'stop' and defined $v) {
			 my $date = $v;
			 $date = du_ConvertGMTISOToLocale($v, $user);	
			 
			 $xml .= "      $k=\"".st_FormatXMLString($date)."\"\n";

			 next;
		 }
		 elsif(defined $v) {
			 $xml .= "      $k=\"".st_FormatXMLString($v)."\"\n";
		 }
      }

	  if(exists $remoteids->{$task->{rowid}}) {
		  $xml .= "      remote_id=\"$remoteids->{$task->{rowid}}\"\n";
	  }

      $xml .= "   >".st_FormatXMLString($task->{description});

	  if( $type eq "periodic" ) {
		  $xml .= $self->_Org_ExportRepetition($task->{repetition});


		  $xml .= $self->_Org_ExportToXML_CancelledPeriodicTask($dbh, $task->{rowid} );
	  }

      $xml .= "</$tag>\n"
   }
   return $xml;
}


sub _Org_ExportRepetition {
	my ($self, $value) = @_;

	my $xml = "";

	my ($Y, $M, $W, $D, $H, $MN, $S) = split(/:/, $value);

	if( $value =~ /^([1-9]\d*)\*(\d+):0:(\d+):(\d+):(\d+):0/) {
		$xml .= qq|<year freq="$1" month="$2" day="$3" hour="$4" minute="$5"/>|;
	}

	elsif($value =~ /^0:([1-9]\d*)\*0:(\d+):(\d+):(\d+):0/) {
		$xml .= qq|<monthdate freq="$1" day="$2" hour="$3" minute="$4"/>|;
	}

	elsif($value =~ /^0:([1-9]\d*)\*(\d+):(\d+):(\d+):(\d+):0/) {
		my $dow = $3 % 7;

		$xml .= qq|<monthday freq="$1" week="$2" dow="$dow" hour="$4" minute="$5"/>|;
	}

	elsif($value =~ /^0:0:([1-9]\d*)\*(\d+):(\d+):(\d+):0/) {
		my $dow = $2 % 7;

		$xml .= qq|<week freq="$1" dow="$dow" hour="$3" minute="$4"/>|;
	}

	elsif($value =~ /^0:0:0:([1-9]\d*)\*(\d+):(\d+):0/) {
		$xml .= qq|<day freq="$1" hour="$2" minute="$3"/>|;
	}

	return $xml;
}


sub _Org_ExportToXML_TodoTask
{
   my ($self, $config, $groupRowid, $modif_start_date, $synchro_ident) = @_;

   my $dbh = $config->GetDBH();

   my $xml = "";
   my $list = SelectMultiple($dbh, "SELECT org_todo_task.*, org_todo_task_priority.ident AS priority FROM org_todo_task, org_todo_task_priority WHERE org_todo_task.group_id = $groupRowid AND (org_todo_task.modified >= '$modif_start_date' OR org_todo_task.created >= '$modif_start_date') AND org_todo_task_priority.rowid = org_todo_task.priority_id");

   my $remoteids = $self->_Org_GetRemoteRowids($config, $synchro_ident, $list);

   foreach my $task (@$list)
   {
	   $xml .= "   <TodoTask\n";
      while( my ($k,$v) = each( %$task ) )
      {
         next if( $k eq "description" );
         $xml .= "      $k=\"".st_FormatXMLString($v)."\"\n";
      }

 	  $xml .= "      remote_id=\"$remoteids->{$task->{rowid}}\"\n";

     $xml .= "   >".st_FormatXMLString($task->{description});
      $xml .= "</TodoTask>\n"
   }
   return $xml;
}

##******************************************************************************
sub _Org_ExportToXML_CancelledPeriodicTask
{
   my ($self, $dbh, $taskRowid) = @_;

   my $xml = "";
   my $list = SelectMultiple($dbh, "SELECT * FROM org_cancelled_periodic_task WHERE task_id = $taskRowid");
   foreach my $task (@$list)
   {
      $xml .= "<Cancelled";
      while( my ($k,$v) = each( %$task ) )
      {
         next if( $k eq "task_id" );
         $xml .= " $k=\"".st_FormatXMLString($v)."\"";
      }
      $xml .= "/>";
   }

   return $xml;
}

##******************************************************************************
## Method Org_ExportRowidsToXML public
## Description  : export all tasks rowids to xml
##******************************************************************************

sub Org_ExportRowidsToXML
{
   my ($self, $context, $groupRowid, $modif_start_date, $synchro_ident) = @_;

   my $config = $context->GetConfig();
   my $dbh = $config->GetDBH();

   my $xml = "<RowidList>\n";
   $xml .= $self->_Org_ExportRowidsToXML_Task($config, $groupRowid, "org_planned_task", "planned", "PlannedTask", $modif_start_date );
   $xml .= $self->_Org_ExportRowidsToXML_Task($config, $groupRowid, "org_periodic_task", "periodic", "PeriodicTask", $modif_start_date );
   $xml .= $self->_Org_ExportRowidsToXML_Task($config, $groupRowid, "org_todo_task", "todo", "TodoTask", $modif_start_date );
   $xml .= "</RowidList>\n";
   return $xml;
}

sub _Org_ExportRowidsToXML_Task
{
   my ($self, $config, $groupRowid, $table, $type, $tag, $modif_start_date, $synchro_ident) = @_;

   my $dbh = $config->GetDBH();

   my $xml = "";
   my $list = SelectMultiple($dbh, "SELECT $table.* FROM $table, org_task_type WHERE group_id = $groupRowid AND org_task_type.rowid = $table.type_id AND org_task_type.ident = '$type'");

   my $remoteids = $self->_Org_GetRemoteRowids($config, $synchro_ident, $list);

   $xml .= "<$tag>";
   foreach my $task (@$list)
   {
	   if(exists $remoteids->{$task->{rowid}}) {
		   $xml .= "   <rowid remote_id=\"$remoteids->{$task->{rowid}}\">$task->{rowid}</rowid>\n";
	   }
	   else {
		   $xml .= "   <rowid>$task->{rowid}</rowid>\n";
	   }
   }
   $xml .= "</$tag>";
   return $xml;
}


##******************************************************************************
## Method Org_ExportTaskCategoryToXML public
## Description  : export all tasks categories to xml
##******************************************************************************

sub Org_ExportTaskCategoryToXML
{
   my ($self, $context, $groupRowid, $modif_start_date) = @_;

   my $dbh = $context->GetConfig()->GetDBH();

   my $xml = "<TaskCategories>\n";
   
   my $taskcats = $self->Org_TaskCatGetList($context);

   foreach my $cat (@$taskcats) {
	   $xml .= "<Category ";
	   
	   foreach my $key (keys %$cat) {
		   $xml .= "$key='".st_FormatXMLString($cat->{$key})."' ";
	   }

	   $xml .= "/>";
   }

   $xml .= "</TaskCategories>\n";
   return $xml;
}

##******************************************************************************
## Method Org_DeleteFromXML public
## Description  : delete tasjs described in data xmltree.
##******************************************************************************

sub Org_DeleteFromXML
{
   my ($self, $context, $groupRowid, $data) = @_;

   foreach my $key (qw(planned periodic todo)) {
	   my $tag = ucfirst($key)."Task";

	   next unless exists $data->{$tag};

	   ExecSQL($context->GetConfig()->GetDBH(), "DELETE FROM org_${key}_task WHERE rowid IN (".join(',', @{$data->{$tag}->[0]->{rowid}}).") AND group_id=$groupRowid");

	   my $sync = new Mioga2::Synchro;
	   $sync->DeleteRowids($context->GetConfig(), 'Organizer', $data->{$tag}->[0]->{rowid});
   }
}


# ============================================================================

=head2 DeleteGroupData ($config, $group_id)

	Remove group data in database when a group is deleted.

	This function is called when a group/user/resource is deleted.

	--> SEE Organizer/Database.pm

=cut

# ============================================================================

sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;

	my $dbh = $config->GetDBH();

	ExecSQL($dbh, "DELETE FROM org_meeting_date ".
                  "USING org_meeting " .
			      "WHERE org_meeting_date.meeting_id = org_meeting.rowid AND ".
			      "      org_meeting.group_id = $group_id");
	
	ExecSQL($dbh, "DELETE FROM org_meeting_resource ".
                  "USING org_meeting " .
			      "WHERE org_meeting_resource.meeting_id = org_meeting.rowid AND ".
			      "      org_meeting.group_id = $group_id");
	
	ExecSQL($dbh, "DELETE FROM org_meeting_resource ".
			      "WHERE org_meeting_resource.resource_id = $group_id");
	
	ExecSQL($dbh, "DELETE FROM org_meeting ".
			      "WHERE org_meeting.group_id = $group_id");
	

	ExecSQL($dbh, "DELETE FROM org_task ".
			      "WHERE group_id = $group_id");
	
	ExecSQL($dbh, "DELETE FROM org_user_pref ".
			      "WHERE user_id = $group_id");
	

}

# ============================================================================

=head2 RevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

	Remove user data in database when a user is revoked of a group.

	This function is called when :
	- a user is revoked of a group
	- a team is revoked of a group for each team member not namly
	  invited in group
	- a user is revoked of a team  for each group where the team is
	  member and the user not namly invited in group

=cut

# ============================================================================

sub RevokeUserFromGroup {
	my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;

	my $dbh = $config->GetDBH();

	ExecSQL($dbh, "UPDATE org_task SET user_id = $group_animator_id WHERE user_id = $user_id");
}

##******************************************************************************
## Method Org_ImportFromXML public
## Description  : import $groupRowid's tasks from XML
##******************************************************************************

=head1 Org_ImportFromXML

Org_ImportFromXML import $groupRowid's tasks from XML

Takes an XML string like Org_ExportToXML returns.

For the import mode (see below), some 'Remove' tags can be added :

   <!ELEMENT TaskList ( StrictTask*, PeriodicTask*, ToDoTask* ) >
 
   . If rowid in the file
       . If such rowid in the database
           . If modified date  file > database
               -> change the task in the database
           . else
               -> do nothing
       . else
           -> do nothing
   . else
      -> creation of a new task

=cut

sub Org_ImportFromXML
{
   my ($self, $context, $groupRowid, $data, $synchro_ident) = @_;

   my $xml = "<RowidList>";
   $xml .= $self->_Org_ImportFromXML_Task( $context, $groupRowid, $data->{PlannedTask}, "planned", $synchro_ident );
   $xml .= $self->_Org_ImportFromXML_Task( $context, $groupRowid, $data->{PeriodicTask}, "periodic", $synchro_ident );
   $xml .= $self->_Org_ImportFromXML_Task( $context, $groupRowid, $data->{TodoTask}, "todo", $synchro_ident );
   $xml .= "</RowidList>";

   return $xml;
}

##******************************************************************************
sub _Org_ImportFromXML_Task
{
   my ($self, $context, $groupRowid, $taglist, $type, $synchro_ident ) = @_;

   my $config = $context->GetConfig();
   my $dbh = $config->GetDBH();

   my @rowids = ();

   my @givenrowids = map {$_->{rowid}} grep {exists $_->{rowid}} @$taglist;

   my $sync = new Mioga2::Synchro();
   my $remoteids = $sync->GetRemoteRowids($context->GetConfig(), $synchro_ident, 'Organizer', \@givenrowids);

   my $newremoteids;

   foreach my $t (@$taglist)
   {

       if( exists( $t->{content} ) )
	   {
		   $t->{description} = $t->{content}->[0];
		   delete( $t->{content} );
	   }
	   else
	   {
		   #$t->{description} = "";
	   }
	   
	   my %params = ( string  => ['name', 'description', 'first_name', 'last_name', 'telephone', 'fax', 'email'],
					  bool    => ['outside'] ,
					  other   => ['modified', 'type_id', 'group_id', 'category_id'] );
	   
	   
	   if($type eq 'todo') {
		   if(exists $t->{priority}) {
			   my $res = SelectSingle($dbh, "SELECT * FROM org_todo_task_priority WHERE ident='$t->{priority}'");
			   
			   if(defined $res) {
				   push @{$params{other}}, 'priority_id';
					   $t->{priority_id} = $res->{rowid};
			   }
		   }
		   
		   push @{$params{bool}}, 'done';
	   }
	   
	   elsif($type eq 'planned') {
		   push @{$params{other}}, 'duration' if (exists ($t->{duration}));
		   push @{$params{string}}, 'start' if (exists ($t->{start}));
		   push @{$params{bool}}, 'is_planned' if (exists ($t->{is_planned}));

		   if(! exists $t->{is_planned} or $t->{is_planned}) {
			   if (exists $t->{start}) {
				   my $user = $context->GetUser();
				   my $new_start = du_ConvertLocaleISOToGMT($t->{start}, $user);
				   $t->{start} = $new_start;
			   }
		   }
	   }
	   
	   elsif($type eq 'periodic') {
		   push @{$params{other}}, 'duration' if (exists ($t->{duration}));
		   push @{$params{string}}, 'start' if (exists ($t->{start}));
		   push @{$params{string}}, 'stop';
		   push @{$params{bool}}, 'is_planned' if (exists ($t->{is_planned}));

		   if(! exists $t->{is_planned} or $t->{is_planned}) {
			   my $user = $context->GetUser();

			   if (exists $t->{start}) {
				   my $new_start = du_ConvertLocaleISOToGMT($t->{start}, $user);
				   $t->{start} = $new_start;
			   }
			   
			   if (exists $t->{stop}) {
				   my $new_stop = du_ConvertLocaleISOToGMT($t->{stop}, $user);
				   $t->{stop} = $new_stop;
			   }
		   }

		   $self->Org_ImportRepetition($t, \%params);
	   }
	   

	   my $res = SelectSingle($dbh, "SELECT * FROM org_task_type WHERE ident = '$type'");
	   $t->{'type_id'} = $res->{rowid};

	   foreach my $arg (@{$params{string}}) {
		   next if $arg eq 'description';

		   if(exists $t->{$arg} and length($t->{$arg}) > 128) {
			   $t->{description} = '' unless exists $t->{description};
			   $t->{description} .= "\n$t->{$arg}\n";
			   $t->{$arg} = substr($t->{$arg}, 0, 128);
		   }
	   }

	   ##---------------------------------------------------------------------
	   if( exists( $t->{rowid} ) )
	   {
		   my $sql = BuildUpdateRequest($t, table => "org_${type}_task", %params);
		   
		   ExecSQL($dbh, $sql." WHERE rowid = $t->{rowid} AND group_id = $groupRowid");
		   
		   if(!exists $remoteids->{$t->{rowid}}) {
			   $newremoteids->{$t->{rowid}} = $t->{remote_id};
		   }
	   }
	   
	   else {
		   $t->{created} = 'now()';
		   $t->{modified} = 'now()';
		   $t->{group_id} = $groupRowid;
		   $t->{category_id} = $self->Org_GetDefaultTaskCat($config);
 
		   push @{$params{other}}, 'created', 'groupRowid';

		   my $res = SelectSingle($dbh, "SELECT nextval('org_task_rowid_seq') as rowid");
		   $t->{rowid} = $res->{rowid};

		   push @{$params{other}}, 'rowid';


		   my $sql = BuildInsertRequest($t, table => "org_${type}_task", %params);
		   ExecSQL($dbh, $sql);  
		   $newremoteids->{$t->{rowid}} = $t->{remote_id};
	   }

	   push @rowids, $t->{rowid};

	   
	   if($type eq 'periodic' and exists $t->{start}) {
		   ExecSQL($dbh, "DELETE FROM org_cancelled_periodic_task WHERE task_id = $t->{rowid}");
		   
		   foreach my $canceled (@{$t->{Cancelled}}) {
			   ExecSQL($dbh, "INSERT INTO org_cancelled_periodic_task (task_id, date, created, modified) VALUES($t->{rowid}, '$canceled->{date}', now(), now())");
		   }
	   }
   }

   $self->_Org_AddRemoteRowids($context->GetConfig(), $synchro_ident, $newremoteids);
   
   $remoteids = $sync->GetRemoteRowids($context->GetConfig(), $synchro_ident, 'Organizer', \@rowids);
   
   my $xml = "<".ucfirst($type)."Task>";
   map {$xml .= "<rowid remote_id=\"$remoteids->{$_}\">".$_."</rowid>"} @rowids;
   $xml .= "</".ucfirst($type)."Task>";

   return $xml;
}


##******************************************************************************
sub Org_ImportRepetition
{
   my ($self, $task, $params) = @_;
   
   my $repetition;

   if(exists $task->{year}) {
	   my $year = $task->{year}->[0];

	   $repetition = $year->{freq}."*".$year->{month}.":0:".$year->{day}.":".$year->{hour}.":".$year->{minute}.":0";
   }

   elsif(exists $task->{monthdate}) {
	   my $monthdate = $task->{monthdate}->[0];
	   $repetition = "0:".$monthdate->{freq}."*0:".$monthdate->{day}.":".$monthdate->{hour}.":".$monthdate->{minute}.":0";
   }

   elsif(exists $task->{monthday}) {
	   my $monthday = $task->{monthday}->[0];
	   $repetition = "0:".$monthday->{freq}."*".$monthday->{week}.":".$monthday->{dow}.":".$monthday->{hour}.":".$monthday->{minute}.":0";
   }

   elsif(exists $task->{week}) {
	   my $week = $task->{week}->[0];
	   $repetition = "0:0:".$week->{freq}."*".$week->{dow}.":".$week->{hour}.":".$week->{minute}.":0";
   }

   elsif(exists $task->{day}) {
	   my $day = $task->{day}->[0];
	   $repetition = "0:0:0:".$day->{freq}."*".$day->{hour}.":".$day->{minute}.":0";
   }

   else {
          return
   }

   $task->{repetition} = $repetition;

   push @{$params->{string}}, 'repetition';
}


##******************************************************************************
sub _Org_GetRemoteRowids 
{
	my ($self, $config, $synchro_ident, $list) = @_;

	my @rowids = map {$_->{rowid}} @$list;

	my $sync = new Mioga2::Synchro();
	return $sync->GetRemoteRowids($config, $synchro_ident, 'Organizer', \@rowids);
}


##******************************************************************************
sub _Org_AddRemoteRowids 
{
	my ($self, $config, $synchro_ident, $remoteids) = @_;

	my $sync = new Mioga2::Synchro();
	return $sync->AddRemoteRowids($config, $synchro_ident, 'Organizer', $remoteids);
}


##******************************************************************************
sub _Org_UpdateLastSynchroDate 
{
	my ($self, $config, $synchro_ident) = @_;

	my $sync = new Mioga2::Synchro();
	return $sync->SetLastSynchroDate($config, $synchro_ident);
}

##******************************************************************************
sub _Org_GetLastSynchroDate 
{
	my ($self, $config, $synchro_ident) = @_;

	my $sync = new Mioga2::Synchro();
	return $sync->GetLastSynchroDate($config, $synchro_ident);
}




##******************************************************************************
## Method Org_ExportCalendar public
## Description  : export $groupId's tasks and update remote 
## Ids table
##******************************************************************************

=head1 Org_ExportCalendar

Org_ExportCalendar export $groupRowid's tasks and update remote Ids table

Return a hash with task descriptions.

=cut

sub Org_ExportCalendar
{
   my ($self, $context, $groupRowid, $modif_start_date, $synchro_ident) = @_;

   if(!defined $modif_start_date) {
	   $modif_start_date = $self->_Org_GetLastSynchroDate($context->GetConfig(), $synchro_ident);
   } 
   
   my $dbh = $context->GetConfig()->GetDBH();
   my $user = $context->GetUser();
   
   my $list = $self->_Org_ExportCalendar_Task($context->GetConfig(), $user, $groupRowid,
											  "org_planned_task", "planned", "PlannedTask",
											  $modif_start_date,   $synchro_ident);

   my $per_tasks = $self->_Org_ExportCalendar_Task($context->GetConfig(), $user, $groupRowid,
												   "org_periodic_task", "periodic", "PeriodicTask", 
												   $modif_start_date, $synchro_ident);

   push @$list, @$per_tasks;

   return $list;
}

##******************************************************************************
## Method Org_ExportTodo public
## Description  : export $groupId's ToDo and update remote 
## Ids table
##******************************************************************************

=head1 Org_ExportTodo

Org_ExportTodo export $groupRowid's tasks and update remote Ids table

Return a hash with task descriptions.

=cut

sub Org_ExportTodo
{
   my ($self, $context, $groupRowid, $modif_start_date, $synchro_ident) = @_;

   my $config = $context->GetConfig();
   my $dbh = $config->GetDBH();

   if(!defined $modif_start_date) {
	   $modif_start_date = $self->_Org_GetLastSynchroDate($context->GetConfig(), $synchro_ident);
   } 
   
   my $list = SelectMultiple($dbh,
							 "SELECT org_todo_task.*, org_todo_task_priority.ident AS priority, ".
							 "       task_category.private AS private, task_category.name AS category ".
							 "FROM org_todo_task, org_todo_task_priority, task_category ".
							 "WHERE org_todo_task.group_id = $groupRowid AND ".
							 "      org_todo_task.created >= '$modif_start_date' AND ".
							 "      org_todo_task_priority.rowid = org_todo_task.priority_id AND ".
							 "      task_category.rowid = org_todo_task.category_id");


   my $remoteids = $self->_Org_GetRemoteRowids($config, $synchro_ident, $list);

   $self->_Org_UpdateLastSynchroDate($config, $synchro_ident);

   return $list;
}


sub _Org_ExportCalendar_Task
{
   my ($self, $config, $user, $groupRowid, $table, $type, $tag, $modif_start_date, $synchro_ident) = @_;

   my $dbh = $config->GetDBH();
   my $mioga_id = $config->GetMiogaId();

   my $list = SelectMultiple($dbh, "SELECT $table.*, task_category.private AS private, task_category.name AS category ".

							       "FROM $table, org_task_type, task_category ".

							       "WHERE $table.group_id = $groupRowid AND ".
							       "      $table.created >= '$modif_start_date' AND ".
							       "      org_task_type.rowid = $table.type_id AND ".
							       "      org_task_type.ident = '$type' AND ".
							       "      task_category.rowid = $table.category_id");

   
   my $remoteids = $self->_Org_GetRemoteRowids($config, $synchro_ident, $list);
   
   my $result = [];
   foreach my $task (@$list)
   {
	   my $hash;
	   
	   if(exists $remoteids->{$task->{rowid}}) {
		   $hash->{remote_id} = $remoteids->{$task->{rowid}};
	   }
	   
	   while( my ($k,$v) = each( %$task ) )
	   {
		   next unless defined $v;
		   
		   if( $table eq "org_periodic_task" and $k eq 'repetition') {
			   $hash->{start} = du_ConvertGMTISOToLocale($task->{start}, $user);

			   $hash->{repetition} = $self->_Org_ExportCalendarRepetition($config, $task->{rowid}, $v);
			   $hash->{start} =~ s/ 00:00:00$//g;
			   $hash->{start} .= sprintf(" %02d:%02s:00", $hash->{repetition}->{hour}, $hash->{repetition}->{minute});
		   }
		   
		   elsif( $k eq 'start' and $table eq "org_planned_task"){
			   $hash->{$k} = du_ConvertGMTISOToLocale($v, $user);
		   }
		   
		   elsif($k eq 'stop') {
			   $hash->{$k} = du_ConvertGMTISOToLocale($v, $user);
		   }
		   elsif($k eq 'created' or $k eq 'modified') {
			   $hash->{$k} = $v;
		   }
		   
		   elsif($k ne 'start') {
			   $hash->{$k} = $v;
		   }
	   }
	   
	   push @$result, $hash;
   }
   
   
   $self->_Org_UpdateLastSynchroDate($config, $synchro_ident);

   return $result;
}


sub _Org_ExportCalendarRepetition {
	my ($self, $config, $task_id, $rep) = @_;

	my ($Y, $M, $W, $D, $H, $MN, $S) = split(/:/, $rep);

	my $hash = {};
	if( $rep =~ /^([1-9]\d*)\*(\d+):0:(\d+):(\d+):(\d+):0/) {
		$hash->{freq} = $1;
		$hash->{year}->{month}=$2;
		$hash->{year}->{dayofmonth}=$3;
		
		$hash->{hour} = $4;
		$hash->{minute} = $5;
	}

	elsif($rep =~ /^0:([1-9]\d*)\*0:(\d+):(\d+):(\d+):0/) {
		$hash->{freq} = $1;
		$hash->{month}->{dayofmonth} = $2;
		
		$hash->{hour} = $3;
		$hash->{minute} = $4;
	}

	elsif($rep =~ /^0:([1-9]\d*)\*(\d+):(\d+):(\d+):(\d+):0/) {
		my $dow = $3 % 7;

		$hash->{freq} = $1;
		$hash->{month}->{weekofmonth} = $2;
		$hash->{month}->{dayofweek} = $3;
		
		$hash->{hour} = $4;
		$hash->{minute} = $5;
	}

	elsif($rep =~ /^0:0:([1-9]\d*)\*(\d+):(\d+):(\d+):0/) {
		my $dow = $2 % 7;

		$hash->{freq} = $1;
		$hash->{week}->{dayofweek} = $2;
		
		$hash->{hour} = $3;
		$hash->{minute} = $4;
	}

	elsif($rep =~ /^0:0:0:([1-9]\d*)\*(\d+):(\d+):0/) {
		$hash->{freq} = $1;
		$hash->{daily} = 1;
		
		$hash->{hour} = $2;
		$hash->{minute} = $3;
	}


	$hash->{exceptions} = [];
	my $exceptions = SelectMultiple($config->GetDBH(), "SELECT * FROM org_cancelled_periodic_task WHERE task_id = $task_id");
	
	foreach my $task (@$exceptions) {
		push @{$hash->{exceptions}}, $task->{date};
	}
	   

	return $hash;
}

##******************************************************************************
## Method Org_ExportICal public
## Description  : export $groupId's tasks to iCalendar
##******************************************************************************

=head1 Org_ExportICal

Org_ExportICal export $groupRowid's tasks in iCalendar

=cut

sub Org_ExportICal
{
   my ($self, $context, $groupRowid, $modif_start_date) = @_;

   my $events = $self->Org_ExportCalendar($context, $groupRowid, $modif_start_date, 'ical_vevent_synchro');
   my $todos = $self->Org_ExportTodo($context, $groupRowid, $modif_start_date, 'ical_vtodo_synchro');

   my $ical = "BEGIN:VCALENDAR\n";
   $ical .= "PRODID:-//Mioga2 Projext//iCalendar Synchronizer v0.1//EN\n";
   $ical .= "VERSION:2.0\n";
   $ical .= "METHOD:PUBLISH\n";
   
   $ical .= $self->_Org_ExportICalendarTasks($context, 'VEVENT', $events);
   $ical .= $self->_Org_ExportICalendarTasks($context, 'VTODO', $todos);

   $ical .= "END:VCALENDAR\n";

   return $ical;
}  

sub _Org_ExportICalendarTasks {
	my ($self, $context, $type, $list) = @_;

	my $user = $context->GetUser();

	my $ical = "";
	foreach my $event (@$list) {
		
		$ical .= "BEGIN:".uc($type)."\n";
		$ical .= add_ical_field("UID", du_ISOToICal($event->{created})."-$event->{rowid}\@mioga2.org");
		
		if(exists $event->{name}) {
			$ical .= add_ical_field("SUMMARY",$event->{name});
		}
		
		if(exists $event->{description}) {
			$ical .= add_ical_field("DESCRIPTION", $event->{description});
		}
		
		if(exists $event->{private} and $event->{private}) {
			$ical .= "CLASS:PRIVATE\n";
		}
		else {
			$ical .= "CLASS:PUBLIC\n";
		}
		
		$ical .= add_ical_field("CREATED", du_ISOLocalToICalGMT($event->{created}, $user));
		$ical .= add_ical_field("LAST-MODIFIED", du_ISOLocalToICalGMT($event->{modified}, $user));
		$ical .= add_ical_field("DTSTAMP", du_ISOLocalToICalGMT(du_GetNowISO(), $user));
		
		if(exists $event->{start}) {
			$ical .= add_ical_field("DTSTART", du_ISOLocalToICalGMT($event->{start}, $user));
			$ical .= add_ical_field("DTEND", du_SecondToICal(du_ISOToSecond(du_ConvertLocaleISOToGMT($event->{start}, $user)) + $event->{duration})."Z");
		}
		
		if(exists $event->{email} and $event->{email} ne '') {
			my $params;
			if(exists $event->{first_name} and $event->{first_name} ne '' and
			   exists $event->{last_name}  and $event->{last_name} ne '' ) {
			   $params->{CN} = $event->{first_name}." ".$event->{last_name};
		   }
			
			$params->{MAILTO} = $event->{email};
			
			$ical .= add_ical_field("ATTENDEE", undef, $params);
			
	   }
		
		if(exists $event->{category}) {
			$ical .= add_ical_field("CATEGORY",$event->{category});
		}

		if(exists $event->{priority_id}) {
			my $priority = { "very urgent" => 1, "urgent" => 2, "normal" => 3, "not urgent" => 4};
			$ical .= "PRIORITY:".$priority->{$event->{priority}}."\n";
		}
		
		if(exists $event->{repetition}) {
			my $rep = $event->{repetition};
			
			my $value = ""; 
			if(exists $rep->{daily}) {
				$value .= "FREQ=DAILY;";
			}
			elsif(exists $rep->{week}) {
				$value .= "FREQ=WEEKLY;BYDAY=".du_dayNumberToICal($rep->{week}->{dayofweek}).";WKST=MO;";
			}
			
			elsif(exists $rep->{month} and exists $rep->{month}->{dayofmonth} ) {
				$value .= "FREQ=MONTHLY;BYMONTHDAY=$rep->{month}->{dayofmonth};";
			}
			
			elsif(exists $rep->{month}) {
				$value .= "FREQ=MONTHLY;BYDAY=".du_dayNumberToICal($rep->{month}->{dayofweek}).";BYSETPOS=$rep->{month}->{weekofmonth};";
			}
			elsif(exists $rep->{year}) {
				$value .= "FREQ=YEARLY;BYMONTHDAY=$rep->{year}->{dayofmonth};BYMONTH=$rep->{year}->{month};";
			}
			
			$value .= "INTERVAL=$rep->{freq};";
			
			if(exists $event->{stop}) {
				$value .= "UNTIL=".du_SecondToICal(du_ISOToSecondInUserLocale($event->{stop}, $context->GetUser()))."Z;";
			}
			
			
			$ical .= add_ical_field("RRULE",$value);
			
			if(@{$rep->{exceptions}}) {
				$ical .= add_ical_field("EXDATE",join(',', map {du_ISOToICal($_)} @{$rep->{exceptions}}));
			}	   
		} 
		
		$ical .= "END:".uc($type)."\n",
	}
	
	return $ical;

} 

sub add_ical_field { my ($name, $value, $params) = @_;

	my $field = "$name";
	if(defined $params) {
		foreach my $param (keys %$params) {
			$field .= ";$param:$params->{$param}";
		}
	}
	
	if(defined $value) {
		$field .= ":$value";
	}

	$field =~ s/(\n|(\x0D\x0A))/\\n/gsm;
	$field =~ s/(.{75})/$1\n /gm;
	$field =~ s/^\n $//gm;

	return $field."\n";
}
   



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
