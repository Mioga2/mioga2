# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Organizer.pm: The Mioga2 Organizer application

=head1 METHODS DESRIPTION

=cut
package Mioga2::Organizer;

use strict;
use Mioga2::Exception::Simple;
use Mioga2::tools::APIAuthz;
use Data::Dumper;
use Date::Parse;
use POSIX qw(tzset);

##******************************************************************************

=head2 EditPeriodicTask ()

	Edit Periodic task.

=head3 Generated XML :
    
   <EditPeriodicTask
        <!-- Is Planned = 0 when task is not planned (without time) -->
        is_planned="1"

        <!-- Task Rowid (-1 for new tasks) -->
        taskrowid="-1"

        <!-- Task name and outside parameter-->
        name=""
      	outside="0|"

        <!-- Start Date -->
        startHour="11"
        startMin="0"
	    day="20"
        month="5"
        year="2005"

        <!-- Stop hour -->
	    stopHour="12"
        stopMin="0"

        <!-- Stop date for periodic tasks -->
	    endMonth="0"
        endDay="0"
        endYear="0"

	    <!-- Contact Related informations -->
        first_name=""
        last_name=""
        email=""
        telephone=""
        outside="0"
        fax=""

        <!-- Periodic tasks settings -->

	    <!-- Task period settings type 
               - planned : a planned task
               - day : a daily tasks
               - week : a weekly tasks
               - month : a monthly task
               - year : a yearly task
           -->
  	    periodType="planned"

        <!-- for daily tasks, the frequency. Every "dayFreq" day -->
        dayFreq="1"

        <!-- for weekly tasks, the frequency and week of day. -->
  	    weekWeekDay="1"
        weekFreq="1"

        <!-- for monthly tasks, there is two type of monthly task : 
            - per day of month : the Nth day of the month
            - per week and week day : the tuesday of the third week of the month.
              -->
        monthPeriodType="day"

        <!-- Frequency. Valid for the two monthly period type -->
        monthFreq="1"

        <!-- For per day of month tasks -->
  	    monthDay="1"

        <!-- The week number and day of week for per week and week day tasks -->
        monthWeek="1"
        monthWeekDay="1"

        <!-- For yearly tasks, the day of year. -->
	    yearFreq="1"
        yearMonth="1"
        yearDay="1"

        <!-- Selected Category description -->
        fgcolor="#XXXXXX"
        bgcolor="#XXXXXX"
        private="0|1"

       <!-- The calling application URI -->
        referer="https://galadriel/Mioga2/Mioga/bin/admin/Organizer/DisplayMain"
    >

        <description>
        Task description
        </description>

        <task_category rowid="category rowid" fgcolor="text color" bgcolor="category bg color">
           category name 
        </task_category>     
    </EditPeriodicTask>


=head3 Form actions and fields :
    
	- change : Try to modify the task.

      Arguments :
         name                type                  description
       ---------------------------------------------------------------------
       name        :  not empty string      : the task name
       is_planned  :  boolean               : is the task planned (with an hour) ?
       outside     :  boolean               : is the task outside ?
       startHour   :  string                : the task start hour
       startMin    :  string                : the task start minute
       day         :  string                : the task day of month number
       month       :  string                : the task month number
       year        :  string                : the task year number
       firstname   :  string                : the task contact first name
       lastname    :  string                : the task contact last name
       email       :  email                 : the task contact email
       telephone   :  string                : the task contact phone number
       fax         :  string                : the task contact fax number
  	   periodType  :  string                : the periodic task type
       dayFreq     :  string                : see xml description
  	   weekWeekDay :  string                : see xml description
       weekFreq    :  string                : see xml description
       monthPeriodType : string             : see xml description
       monthFreq   :  string                : see xml description
  	   monthDay    :  string                : see xml description
       monthWeek   :  string                : see xml description
       monthWeekDay:  string                : see xml description
	   yearFreq    :  string                : see xml description
       yearMonth   :  string                : see xml description
       yearDay     :  string                : see xml description
	   endMonth    :  string                : see xml description
       endDay      :  string                : see xml description
       endYear     :  string                : see xml description
	

	- delete : Try to delete the task


=cut

##******************************************************************************
sub EditPeriodicTask
{
    my ($self, $context) = @_;
    my($errmsg);
    my $session = $context->GetSession();

    if($context->{args}{mode} eq 'initial')
    {
        my($row);

        $session->{mode}        = '';

        $session->{organizer}{data} = {};
        $session = $session->{organizer}{data};

		if(exists $context->{args}->{referer}) {
			$session->{referer}  = $context->{args}->{referer};
		}
		else {
			$session->{referer}  = $context->GetReferer();
		}

        $session->{taskrowid} = $context->{args}{taskrowid};

      try
      {
        $row = $self->Org_GetTask($context, $session->{taskrowid}, 'periodic', $context->GetGroup()->GetRowid());
      }
      catch Mioga2::Exception::Simple with
      {
      	# error getting task
      };

      if (!defined($row))
      {
         return $self->_TaskResult_Page( $context, "unknown_task" );
      }

		if($row->{private} == 1 and $context->GetUser()->GetRowid() != $context->GetGroup()->GetAnimId() ) {
			throw Mioga2::Exception::Simple("Mioga2::Organizer->EditPeriodicTask", __("can't edit a private task"));
		}   
		
        $session->{year}        = int($context->{args}{year});  # OK for 2 times
        $session->{month}       = int($context->{args}{month});
        $session->{day}         = int($context->{args}{day});

        if($errmsg = $self->_PeriodicTaskRowToSession($row, $session, $context))
        {
            return $self->_Error($context, $errmsg);
        }

        $session->{year}        = int($context->{args}{year});  # OK for 2 times
        $session->{month}       = int($context->{args}{month});
        $session->{day}         = int($context->{args}{day});
    }
    else
    {
        throw Mioga2::Exception::Simple("Mioga2::Organizer->EditPeriodicTask", __("session->organizer not defined"))
			if( !exists($session->{organizer}{data}) );
		
        my($row) = $self->Org_GetTask($context, $context->{args}{taskrowid}, 'periodic', $context->GetGroup()->GetRowid());
		if($row->{private} == 1 and $context->GetUser()->GetRowid() != $context->GetGroup()->GetAnimId() ) {
			throw Mioga2::Exception::Simple("Mioga2::Organizer->EditPeriodicTask", __("can't edit a private task"));
		}

        $session = $session->{organizer}{data};

        if(st_ArgExists($context, 'delete'))
        {
            if(  $context->GetConfig()->AskConfirmForDelete() and
				 !exists( $session->{confirm} ) )
            {
				$session->{confirm} = 1;
				return $self->_ConfirmDeletion_Page( $context, "EditPeriodicTask?delete.x=0&amp;taskrowid=$context->{args}{taskrowid}",
													 "occurrence" );
            }
			

            local $ENV{TZ} = $self->GetTimezone($context);
			tzset();
            my($rowid) = $session->{taskrowid} =~ /^(\d+)/;
            my($sec) = strftime("%s", 0, $session->{startMin},
                                         $session->{startHour},
                                         $session->{day},
                                         $session->{month} - 1,
                                         $session->{year} - 1900);

			local $ENV{TZ} = 'GMT';
			tzset();

            my($pgdate);

            {
                local $ENV{TZ} = $self->GetTimezone($context);
				tzset();
                $pgdate = strftime("%Y-%m-%d %H:%M:00 GMT", localtime($sec));
            }
			tzset();

            my($fields) = {
                task_id => $rowid,
                date    => $pgdate
            };

            $self->Org_CreateCancelledPeriodicTask($context, $fields );

            $self->Org_ChangeTask($context, "$session->{taskrowid}", "periodic",
								  $context->GetGroup()->GetRowid(),
								  {});

            return $self->_TaskResult_Page( $context, 'delete');
        }
        elsif(st_ArgExists($context, 'change'))
        {
            delete($context->{args}{'change'});
            delete($context->{args}{'change.x'});
            ++$context->{args}{'chain'};
            return $self->ChangePeriodicTask( $context );
        }
        else
        {
            return $self->_Error($context, __("Missing action !!!!!"));
        }

    }

    return $self->_EditPeriodicTask_Form($context, $session, "OK");

}

##******************************************************************************
## Method ChangePeriodicTask  public
##******************************************************************************
sub ChangePeriodicTask
{
    my ($self, $context) = @_;
    my($errmsg);
    my $session = $context->GetSession();

    throw Mioga2::Exception::Simple("Mioga2::Organizer->ChangePeriodicTask", __("session->organizer not defined"))
		if( !exists($session->{organizer}{data}) );

    $session = $session->{organizer}{data};

    if(st_ArgExists($context, 'change')) {
	my $task = $self->Org_GetTask($context, $context->{args}{taskrowid}, 'periodic', $context->GetGroup()->GetRowid() );
	if($task->{private} == 1 and $context->GetUser()->GetRowid() != $context->GetGroup()->GetAnimId() ) {
	    throw Mioga2::Exception::Simple("Mioga2::Organizer->ChangePeriodicTask", __("can't edit a private task"));
	}
    }
    else  {
	my $task = $self->Org_GetTask($context, $session->{taskrowid},'periodic',   $context->GetGroup()->GetRowid() );
	if($task->{private} == 1 and $context->GetUser()->GetRowid() != $context->GetGroup()->GetAnimId() ) {
	    throw Mioga2::Exception::Simple("Mioga2::Organizer->ChangePeriodicTask", __("can't edit a private task"));
	}
    }
   

	$self->_AddingContact( $context, $session );
	
	try {
		my $appdesc = new Mioga2::AppDesc($context->GetConfig(), ident => 'Contact');
		my $application = $appdesc->CreateApplication();
		my $test = $application->CheckUserAccessOnMethod($context, $context->GetUser(), 
														 $context->GetGroup(), 'SearchEngine');
		
		$session->{display_icon} = ($test == AUTHZ_OK);
	}
	
	otherwise {
		$session->{display_icon} = 0;			  
	};

	##==========================================================================
	my($fields, $r);
	if(st_ArgExists($context, 'change') or st_ArgExists($context, 'contact'))
    {

        $session->{day}             = int($context->{args}{day});
        $session->{month}           = int($context->{args}{month});
        $session->{year}            = int($context->{args}{year});

		foreach my $key (qw(startHour startMin stopHour stopMin)) {
			if($context->{args}{$key} ne '') {
				$session->{$key} = int($context->{args}{$key});
			}
			else {
				$session->{$key} =  '';
				$session->{is_planned} =  0;
			}
		}

		if(!exists $session->{is_planned}) {
			$session->{is_planned} = 1;
		}

        $session->{taskrowid}       = $context->{args}{taskrowid};

        $session->{name}            = $context->{args}{name};
        $session->{description}     = $context->{args}{description};
        $session->{category_id}     = $context->{args}{category_id};
        $session->{outside}         = $context->{args}{outside};

        $session->{endDay}          = int($context->{args}{endDay});
        $session->{endMonth}        = int($context->{args}{endMonth});
        $session->{endYear}         = int($context->{args}{endYear});

        $session->{periodType}      = $context->{args}{periodType};
        $session->{monthPeriodType} = $context->{args}{monthPeriodType};
        $session->{yearFreq}        = int($context->{args}{yearFreq});
        $session->{yearDay}         = int($context->{args}{yearDay});
        $session->{yearMonth}       = int($context->{args}{yearMonth});
        $session->{monthFreq}       = int($context->{args}{monthFreq});
        $session->{monthPeriodType} = $context->{args}{monthPeriodType};
        $session->{monthDay}        = int($context->{args}{monthDay});
        $session->{monthWeek}       = int($context->{args}{monthWeek});
        $session->{monthWeekDay}    = int($context->{args}{monthWeekDay});
        $session->{weekFreq}        = int($context->{args}{weekFreq});
        $session->{weekWeekDay}     = int($context->{args}{weekWeekDay});
        $session->{dayFreq}         = int($context->{args}{dayFreq});
		$session->{first_name}  = $context->{args}->{first_name};
		$session->{last_name}   = $context->{args}->{last_name};
		$session->{telephone}   = $context->{args}->{telephone},
		$session->{fax}         = $context->{args}->{fax};
		$session->{email}       = $context->{args}->{email};
	}
	
	##----------------------------------------------------------------------
   if( st_ArgExists($context, "contact") )
   {
	   my $uri = new Mioga2::URI($context->GetConfig(),
							           group  => $context->GetGroup()->GetIdent(),
	                                   public => 0,
	                                   application => 'Contact',
	                                   method => 'SearchEngine',
	                                   args   => { action  => 'Contact',
	                                               referer => $context->GetURI()->GetURI()
	                                               }
	                                   );

	   my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	   $content->SetContent($uri->GetURI());
	   return $content;
   }
	
    elsif(st_ArgExists($context, 'change'))
    {

		if($session->{is_planned}) {

			if( $session->{endDay}   != 0 and
				$session->{endMonth} != 0 and
				$session->{endYear}  != 0 )
			{
				my ($startSec,$startDate,$stopDate,$stopSec);
				local $ENV{TZ} = 'GMT';
				tzset();
				$startSec = strftime( '%s', 0, 0, 0, $session->{day},
									  $session->{month}-1, $session->{year}-1900);
				$stopSec  = strftime( '%s', 59, 59, 23, $session->{endDay},
									  $session->{endMonth}-1, $session->{endYear}-1900);

				return $self->_ChangePeriodicTask_Form($context,$session, "START_DATE_AFTER")
					if( $stopSec - $startSec <= 0 );
			}
		}

		##----------------------------------------------------------------------
        eval { $fields = $self->_PeriodicTaskSessionToFields($context, $session) };

		if($@)
        {
            my $error = $@;
            chomp $error;
			
            return $self->_ChangePeriodicTask_Form($context, $session, $error);
        }

        ##----------------------------------------------------------------------
		if($session->{is_planned}) {
			
			my $newSec = ( 60*$session->{startHour}         + $session->{startMin}         ) * 60;
			my $oldSec = ( 60*$session->{originalStartHour} + $session->{originalStartMin} ) * 60;
			my $delta = $newSec - $oldSec;
			if( $delta != 0 )
			{
				my $sign = "+";
				if( $delta < 0 ) { $sign="-"; $delta=-$delta; }
				my $second = $delta % 60;
				$delta = int($delta/60);
				my $minute = $delta % 60;
				my $hour = int($delta/60);
				$delta = sprintf( "%s%02d:%02d:%02d", $sign, $hour, $minute, $second );
				$r = $self->Org_ChangeCancelledPeriodicTask($context, $session->{taskrowid},$delta);
				if( ! $r )
				{
					return $self->_ChangePeriodicTask_Form($context, $session, "CHANGING_ERROR");
				}
			}
		}

        ##----------------------------------------------------------------------
		$r = $self->Org_ChangeTask($context, $session->{taskrowid}, 'periodic',
                                          $context->GetGroup()->GetRowid(),
                                          $fields);
        if( ! $r )
        {
            return $self->_ChangePeriodicTask_Form($context, $session, "CHANGING_ERROR");
        }

        return $self->_TaskResult_Page( $context, 'modify');
    }
    ##==========================================================================
    elsif(st_ArgExists($context, 'delete'))
    {

        if(  $context->GetConfig->AskConfirmForDelete()  and
            !exists( $session->{confirm} ) )
        {
           $session->{confirm} = 1;
           return $self->_ConfirmDeletion_Page( $context, "ChangePeriodicTask?delete.x=0&amp;taskrowid=$session->{taskrowid}",
                                                "periodic" );
        }


        my($rowid) = $session->{taskrowid} =~ /^(\d+)/;
        my($fields) = {
            task_id => $rowid
        };

        $self->Org_DeleteCancelledPeriodicTask($context, $fields );
        $self->Org_DeleteTask($context, $session->{taskrowid}, $context->GetGroup()->GetRowid());
        return $self->_TaskResult_Page( $context, 'delete');
    }
    ##==========================================================================
    elsif(exists $context->{args}{'chain'})
    {
        my $row = $self->Org_GetTask($context, $session->{taskrowid}, 'periodic', $context->GetGroup()->GetRowid());

        if($errmsg = $self->_PeriodicTaskRowToSession($row, $session, $context))
        {
            return $self->_Error($context, $errmsg);
        }

        $session->{originalStartHour} = $session->{startHour};
        $session->{originalStartMin}  = $session->{startMin};

        return $self->_ChangePeriodicTask_Form($context, $session, "OK");
    }
    ##==========================================================================
    else
    {
        return $self->_Error($context, __("Missing action !!!!!"));
    }
}

##******************************************************************************
## Method _CreatePeriodicTask  private
##******************************************************************************
sub _CreatePeriodicTask
{
    my ($self, $context) = @_;
    my $session = $context->GetSession();

	$session = $session->{organizer}{data};



    ##==========================================================================
    if( $session->{endDay}   != 0 and
        $session->{endMonth} != 0 and
        $session->{endYear}  != 0 )
    {
        my ($startSec,$startDate,$stopDate,$stopSec);
        local $ENV{TZ} = 'GMT';
		tzset();
        $startSec = strftime( '%s', 0, 0, 0, $session->{day},
                              $session->{month}-1, $session->{year}-1900);
        $stopSec  = strftime( '%s', 59, 59, 23, $session->{endDay},
                              $session->{endMonth}-1, $session->{endYear}-1900);
        return $self->_CreateStrictOrPeriodicTask_Form($context,$session, "START_DATE_AFTER")
            if( $stopSec - $startSec <= 0 );
    }

    ##==========================================================================
    my($r, $fields);

    eval { $fields = $self->_PeriodicTaskSessionToFields($context, $session) };

    if($@)
    {
       my $error = $@;
       chomp $error;
       return $self->_CreateStrictOrPeriodicTask_Form($context, $session, $error );
    }

    $r = $self->Org_CreateTask($context, 'periodic', $fields );

    if( !$r )
    {
       return $self->_CreateStrictOrPeriodicTask_Form($context,$session,"CREATION_ERROR");
    }

    return $self->_TaskResult_Page( $context, 'create');

}

##******************************************************************************
## Method _CreateSimplePeriodicTask  private
##******************************************************************************
sub _CreateSimplePeriodicTask
{
    my ($self, $context) = @_;
    my $session = $context->GetSession();

	$session = $session->{organizer}{data};



    ##==========================================================================
    if( $session->{endDay}   != 0 and
        $session->{endMonth} != 0 and
        $session->{endYear}  != 0 )
    {
        my ($startSec,$startDate,$stopDate,$stopSec);
        local $ENV{TZ} = 'GMT';
		tzset();
        $startSec = strftime( '%s', 0, 0, 0, $session->{day},
                              $session->{month}-1, $session->{year}-1900);
        $stopSec  = strftime( '%s', 59, 59, 23, $session->{endDay},
                              $session->{endMonth}-1, $session->{endYear}-1900);
        return $self->_Task_GenericForm($context, $session, "START_DATE_AFTER", "CreateSimplePeriodicTask")
            if( $stopSec - $startSec <= 0 );
    }

    ##==========================================================================
    my($r, $fields);

    eval { $fields = $self->_PeriodicTaskSessionToFields($context, $session) };

    if($@)
    {
       my $error = $@;
       chomp $error;
       return $self->_Task_GenericForm($context, $session, $error, "CreateSimplePeriodicTask");
    }
    
    if ($session->{mode} eq 'booking') {
    	$fields->{group_id} = $session->{resource_id};
    }

    $r = $self->Org_CreateTask($context, 'periodic', $fields );

    if( !$r )
    {
       return $self->_Task_GenericForm($context, $session, "CREATION_ERROR", "CreateSimplePeriodicTask");
    }

    return $self->_TaskResult_Page( $context, 'create');

}

##******************************************************************************
## Method _EditSimplePeriodicTask  private
##******************************************************************************
sub _EditSimplePeriodicTask
{
    my ($self, $context) = @_;
    my $session = $context->GetSession();
	$session = $session->{organizer}{data};

    ##==========================================================================
    if( $session->{endDay}   != 0 and
        $session->{endMonth} != 0 and
        $session->{endYear}  != 0 )
    {
        my ($startSec,$startDate,$stopDate,$stopSec);
        local $ENV{TZ} = 'GMT';
		tzset();
        $startSec = strftime( '%s', 0, 0, 0, $session->{day},
                              $session->{month}-1, $session->{year}-1900);
        $stopSec  = strftime( '%s', 59, 59, 23, $session->{endDay},
                              $session->{endMonth}-1, $session->{endYear}-1900);
        return $self->_Task_GenericForm($context, $session, "START_DATE_AFTER", "EditSimplePeriodicTask")
            if( $stopSec - $startSec <= 0 );
    }

    ##==========================================================================
    my($r, $fields);

    eval { $fields = $self->_PeriodicTaskSessionToFields($context, $session) };
    
    if ($session->{mode} eq 'booking') {
        $fields->{group_id} = $session->{resource_id};
    }

    if($@)
    {
       my $error = $@;
       chomp $error;
       return $self->_Task_GenericForm($context, $session, $error, "EditSimplePeriodicTask");
    }

    $r = $self->Org_ChangeTask($context, $session->{taskrowid}, 'periodic', $session->{resource_id}, $fields);
    
    if( ! $r )
    {
        return $self->_Task_GenericForm($context, $session, "CREATION_ERROR", "EditSimplePeriodicTask");
    }

    return $self->_TaskResult_Page( $context, 'modify');
}

##******************************************************************************

=head2 ViewPeriodicTask ()

	Display a Periodic task.

=head3 Generated XML :
    
   <ViewPeriodicTask
        <!-- Is Planned = 0 when task is not planned (without time) -->
        is_planned="1"

        <!-- Task Rowid (-1 for new tasks) -->
        taskrowid="-1"

        <!-- Task name and outside parameter-->
        name=""
      	outside="0|"

        <!-- Start Date -->
        startHour="11"
        startMin="0"
	    day="20"
        month="5"
        year="2005"

        <!-- Stop hour -->
	    stopHour="12"
        stopMin="0"

        <!-- Stop date for periodic tasks -->
	    endMonth="0"
        endDay="0"
        endYear="0"

	    <!-- Contact Related informations -->
        first_name=""
        last_name=""
        email=""
        telephone=""
        outside="0"
        fax=""

        <!-- Periodic tasks settings -->

	    <!-- Task period settings type 
               - planned : a planned task
               - day : a daily tasks
               - week : a weekly tasks
               - month : a monthly task
               - year : a yearly task
           -->
  	    periodType="planned"

        <!-- for daily tasks, the frequency. Every "dayFreq" day -->
        dayFreq="1"

        <!-- for weekly tasks, the frequency and week of day. -->
  	    weekWeekDay="1"
        weekFreq="1"

        <!-- for monthly tasks, there is two type of monthly task : 
            - per day of month : the Nth day of the month
            - per week and week day : the tuesday of the third week of the month.
              -->
        monthPeriodType="day"

        <!-- Frequency. Valid for the two monthly period type -->
        monthFreq="1"

        <!-- For per day of month tasks -->
  	    monthDay="1"

        <!-- The week number and day of week for per week and week day tasks -->
        monthWeek="1"
        monthWeekDay="1"

        <!-- For yearly tasks, the day of year. -->
	    yearFreq="1"
        yearMonth="1"
        yearDay="1"

        <!-- Selected Category description -->
        fgcolor="#XXXXXX"
        bgcolor="#XXXXXX"
        private="0|1"

       <!-- The calling application URI -->
        referer="https://galadriel/Mioga2/Mioga/bin/admin/Organizer/DisplayMain"
    >

        <description>
        Task description
        </description>

        <task_category rowid="category rowid" fgcolor="text color" bgcolor="category bg color">
           category name 
        </task_category>     
    </ViewPeriodicTask>

=cut

##******************************************************************************
sub ViewPeriodicTask
{
   my ($self, $context) = @_;
   my $userTimeZone = $self->GetTimezone($context);

   my $session = $context->GetSession();
   $session->{organizer}{data} = {};

   foreach my $parameter ( qw( taskrowid day month year ) )
   {
       return $self->_Error($context, __x("{parameter} not defined", parameter => $parameter))
         if( !defined($context->{args}{$parameter}) );
   }
   my $taskrowid = $context->{args}{taskrowid};

   my $row;
   try
   {
      $row = $self->Org_GetTask($context, $taskrowid, 'periodic', $context->GetGroup()->GetRowid());
   }
   catch Mioga2::Exception::Simple with
   {
   	# error getting task
   };

   if (!defined($row))
   {
      return $self->_TaskResult_Page( $context, "unknown_task" );
   }

   if($row->{private} == 1 and $context->GetUser()->GetRowid() != $context->GetGroup()->GetAnimId() ) {
       throw Mioga2::Exception::Simple("Mioga2::Organizer->ViewPeriodicTask", __("can't edit a private task"));
   }
   
   my $task = {};
   if(exists $context->{args}->{referer}) {
	   $task->{referer}  = $context->{args}->{referer};
   }
   else {
	   $task->{referer}  = $context->GetReferer();
   }
   $task->{escreferer}  = st_URIEscape($task->{referer});
   $task->{year}     = int($context->{args}{year});  # OK for 2 times
   $task->{month}    = int($context->{args}{month});
   $task->{day}      = int($context->{args}{day});
   $task->{rowid}      = int($context->{args}{taskrowid});
   if(my $errmsg = $self->_PeriodicTaskRowToSession($row, $task, $context))
   {
      return $self->_Error($context, $errmsg);
   }
   $task->{year}     = int($context->{args}{year});  # OK for 2 times
   $task->{month}    = int($context->{args}{month});
   $task->{day}      = int($context->{args}{day});

   $task->{startMin} = sprintf("%02d",$task->{startMin});
   $task->{stopMin}  = sprintf("%02d",$task->{stopMin});

   return $self->_ViewPeriodicTask_Form($context,$task,'OK');

}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

##******************************************************************************
## Method _PeriodicTaskSessionToFields private
##******************************************************************************
sub _PeriodicTaskSessionToFields
{
    my($self, $context, $session) = @_;
    my($startDate, $endDate);
    my($startTime, $endTime);
    my($def, $fields);
    my($tz) = $self->GetTimezone($context);

    if(not $session->{name})
    {
         die "NAME_EMPTY\n";
    }
    if(not $startDate = ParseDate("$session->{year}-$session->{month}-$session->{day}"))
    {
         die "START_DATE_INCORRECT\n";
    }
    if($session->{endYear} or $session->{endMonth} or $session->{endDay})
    {
        if(not $endDate = ParseDate("$session->{endYear}-$session->{endMonth}-$session->{endDay}"))
        {
            die "STOP_DATE_INCORRECT\n";
        }
    }

	if($session->{is_planned}) {
		$startTime = ($session->{startHour} * 60 + $session->{startMin}) * 60;
		$endTime = ($session->{stopHour} * 60 + $session->{stopMin}) * 60;

		if($endTime <= $startTime)
		{
			die "START_TIME_AFTER\n";
		}
	}

	if($session->{periodType} eq 'day')
	{
		$def = sprintf("0:0:0:%d*", $session->{dayFreq});
	}
	elsif($session->{periodType} eq 'week')
    {
        $def=sprintf("0:0:%d*%d:", $session->{weekFreq}, $session->{weekWeekDay});
    }
    elsif($session->{periodType} eq 'month')
    {
        if($session->{monthPeriodType} eq 'day')
        {
            $def = sprintf("0:%d*0:%d:", $session->{monthFreq}, $session->{monthDay});
        }
        elsif($session->{monthPeriodType} eq 'week')
        {
            $def = sprintf("0:%d*%d:%d:", $session->{monthFreq}, $session->{monthWeek}, $session->{monthWeekDay});
        }
        else
        {
            print STDERR "Mioga2::Organizer::PeriodicTask->_PeriodicTaskSessionToFields: Month period '$session->{monthPeriodType}' unkown (BUG)\n";
            die "Period '$session->{monthPeriodType}' unkown (BUG)\n";
        }
    }
    elsif($session->{periodType} eq 'year')
    {
        $def = sprintf("%d*%d:0:%d:", $session->{yearFreq}, $session->{yearMonth}, $session->{yearDay});
    }
    else
    {
        print STDERR "Mioga2::Organizer::PeriodicTask->_PeriodicTaskSessionToFields: Period '$session->{periodType}' unkown (BUG)\n";
        die "Period '$session->{periodType}' unkown (BUG)\n";
    }

	if($session->{is_planned}) {
		$def .= sprintf("%d:%d:0", $session->{startHour}, $session->{startMin});
	}
	else {
		$def .= "0:0:0";
	}
	
	if($session->{is_planned}) {
		{
			local $ENV{TZ} = $tz;
			tzset();
			my($sec) = strftime('%s', 0, 0, 0, $session->{day}, $session->{month}-1, $session->{year}-1900);
			
			$ENV{TZ} = 'GMT';
			tzset();
			$startDate = strftime( "%Y-%m-%d %H:%M:%S.00 GMT", gmtime($sec) );
		}
		tzset();

	}
	else {
		$startDate = "$session->{year}-$session->{month}-$session->{day}";
	}
	
    $fields = {
            group_id        => $context->GetGroup()->GetRowid(),
            user_id         => $context->GetUser()->GetRowid(),
            name            => $session->{name},
            description     => $session->{description},
            outside         => $session->{outside}?"t":"f",
            waiting_book    => $session->{waiting_book}?"t":"f",
            category_id     => $session->{category_id},
			start           => $startDate,
            duration        => $endTime - $startTime,
            repetition      => $def,
            is_planned      => $session->{is_planned},
	};

	foreach my $key (qw(first_name last_name telephone fax email)) {
		$fields->{$key} = $session->{$key} if(defined $session->{$key});
	}


    if(defined($endDate))
    {
        local $ENV{TZ} = $tz;
		tzset();
        my($sec) = strftime('%s', 59, 59, 23, $session->{endDay}, $session->{endMonth}-1, $session->{endYear}-1900);

        $ENV{TZ} = 'GMT';
		tzset();
        $endDate = strftime( "%Y-%m-%d %H:%M:%S.00 GMT", gmtime($sec + 1) );
        $fields->{stop} = $endDate;
    }
    else
    {
        $fields->{stop} = undef;
    }
	tzset();

    return $fields;
}

##******************************************************************************
## Method _PeriodicTaskRowToSession private
##******************************************************************************
sub _PeriodicTaskRowToSession
{
    my($self, $row, $session, $context) = @_;
    my($period);
    my(@def) = split(/:|\*/, $row->{repetition});

    for ($period = 0;$period < 5; ++$period) {
        $def[$period] and last;
    }

	$session->{is_planned} = $row->{is_planned};

    ##===========================================================================
    ## Getting time
    ##===========================================================================
	if($session->{is_planned}) {
		my ($startHour,$startMin,$stopHour,$stopMin);
		{
			local $ENV{TZ} = $self->GetTimezone($context);
			tzset();
			my $startSec = strftime('%s', 0, $def[5], $def[4], $session->{day}, $session->{month}-1, $session->{year}-1900);
			my $stopSec = $startSec + $row->{duration};
			local $ENV{TZ} = $self->GetTimezone($context);
			tzset();
			($startHour,$startMin) = split( /:/, strftime("%H:%M", localtime($startSec)) );
			($stopHour,$stopMin) = split( /:/, strftime("%H:%M", localtime($stopSec)) );
		}
		tzset();

		$session->{startHour}   = int($startHour);
		$session->{startMin}    = int($startMin);
		$session->{stopHour}    = int($stopHour);
		$session->{stopMin}     = int($stopMin);
	}

    ##===========================================================================

    $session->{monthPeriodType} = 'day';
    $session->{yearFreq} = 1;
    $session->{yearDay} = 1;
    $session->{yearMonth} = 1;
    $session->{monthFreq} = 1;
    $session->{monthPeriodType} = 'day';
    $session->{monthDay} = 1;
    $session->{monthWeek} = 1;
    $session->{monthWeekDay} = 1;
    $session->{weekFreq} = 1;
    $session->{weekWeekDay} = 1;
    $session->{dayFreq} = 1;

    if($period == 0)
    {
        $session->{periodType} = 'year';
        $session->{yearFreq} = $def[$period];
        $session->{yearDay} = $def[3];
        $session->{yearMonth} = $def[1];
    }
    elsif($period == 1)
    {
        $session->{periodType} = 'month';
        $session->{monthFreq} = $def[$period];
        if($def[2])
        {
            $session->{monthPeriodType} = 'week';
            $session->{monthWeek} = $def[2];
            $session->{monthWeekDay} = $def[3];
        }
        else
        {
            $session->{monthDay} = $def[3];
        }
    }
    elsif($period == 2)
    {
        $session->{periodType} = 'week';
        $session->{weekFreq} = $def[$period];
        $session->{weekWeekDay} = $def[3];
    }
    elsif($period == 3)
    {
        $session->{periodType} = 'day';
        $session->{dayFreq} = $def[$period];
    }
    else
    {
        return "Invalid repetition definition (BUG)";
    }

	if($row->{stop})
	{
		local $ENV{TZ} = 'GMT';
		tzset();
		my ($year,$month,$day,$hour,$min,$sec) =
			( $row->{stop} =~ /(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/ );
		my $second = strftime("%s", $sec, $min, $hour, $day, $month-1, $year-1900 );
		local $ENV{TZ} = $self->GetTimezone($context);
		tzset();
		my ($y,$m,$d) = split(/:/, strftime("%Y:%m:%d", localtime($second - 1 ) ) );
		($session->{endDay}, $session->{endMonth}, $session->{endYear}) = (int($d), int($m), int($y));
	}
	else
	{
		$session->{endDay} = 0;
		$session->{endMonth} = 0;
		$session->{endYear} = 0;
	}
	tzset();
		
	if($session->{is_planned}) {
		if($row->{start})
		{
			local $ENV{TZ} = 'GMT';
			tzset();
			my ($year,$month,$day, $hour,$min,$sec) = ( $row->{start} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/);
			my $second = strftime("%s", $sec, $min, $hour, $day, $month-1, $year-1900 );
			
			local $ENV{TZ} = $self->GetTimezone($context);
			tzset();
			my ($y,$m,$d) = split(/:/, strftime("%Y:%m:%d", localtime($second) ) );
			
			($session->{day}, $session->{month}, $session->{year}) = (int($d), int($m), int($y));
		}
		tzset();

	}

    $session->{waiting_book}    = $row->{waiting_book};
    $session->{user_id}         = $row->{user_id};
    $session->{name}        = $row->{name};
    $session->{description} = $row->{description};
    $session->{outside}     = $row->{outside};
    $session->{category_id} = $row->{category_id};
    $session->{bgcolor}       = $row->{bgcolor};
    $session->{fgcolor}       = $row->{fgcolor};
    $session->{private}     = $row->{private};
    $session->{tz}          = $self->GetTimezone($context);
	$session->{first_name}  = $row->{first_name},
	$session->{last_name}   = $row->{last_name},
	$session->{telephone}   = $row->{telephone},
	$session->{fax}         = $row->{fax},
	$session->{email}       = $row->{email},

    return undef;

}

##******************************************************************************
## Method _EditPeriodicTask_Form private
##******************************************************************************
sub _EditPeriodicTask_Form
{

   my ($self,$context,$session,$message) = @_;

   return $self->_Task_GenericForm($context,$session,$message,
                                   "EditPeriodicTask",
                                   'organizer.xsl');
}

##******************************************************************************
## Method _ChangePeriodicTask_Form private
##******************************************************************************
sub _ChangePeriodicTask_Form
{
   my ($self,$context,$session,$message) = @_;

   return $self->_Task_GenericForm($context,$session,$message,
                                   "ChangePeriodicTask",
                                   'organizer.xsl');
}

##******************************************************************************
## Method _ViewPeriodicTask_Form private
##******************************************************************************
sub _ViewPeriodicTask_Form
{
   my ($self,$context,$session,$message) = @_;

   if($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), 'Write')) {
	   $session->{canwrite} = 1;
   }

   return $self->_Task_GenericForm($context,$session,$message,
                                   "ViewPeriodicTask",
                                   'organizer.xsl');
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
