# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Organizer.pm: The Mioga2 Organizer application

=head1 METHODS DESRIPTION

=cut

package Mioga2::Organizer;

use strict;
use Mioga2::Exception::Simple;
use Mioga2::tools::args_checker;
use Mioga2::tools::date_utils;
use Mioga2::LargeList::RowidList;
use Data::Dumper;
use MIME::Entity;
use MIME::Words qw (:all);
use POSIX qw(tzset);



# ============================================================================
# Invited users tiny list.
# ============================================================================

my %tiny_inv_users_sll_desc = ( 'name' => 'tiny_meeting_users_sll',
																
								'fields' => [ [ 'rowid', 'rowid'],
											  [ 'user_firstname', 'user_firstname', 'rowid' ],
											  [ 'user_lastname', 'user_lastname', 'rowid' ],
											  [ 'delete', 'delete'],
											],

								'default_sort_field' => 'user_lastname',
								
								'nb_lines_per_page' => 5,
							  );

my %ro_tiny_inv_users_sll_desc = ( 'name' => 'tiny_meeting_users_sll',
																
								'fields' => [ [ 'rowid', 'rowid'],
											  [ 'user_firstname', 'user_firstname', 'rowid' ],
											  [ 'user_lastname', 'user_lastname', 'rowid' ],
											],

								'default_sort_field' => 'user_lastname',
								
								'nb_lines_per_page' => 5,
							  );

# ============================================================================
# Invited users list.
# ============================================================================

my %inv_users_sll_desc = ( 'name' => 'meeting_users_sll',
																
						   'fields' => [ [ 'rowid', 'rowid'],
										 [ 'user_ident', 'user_ident', 'rowid' ],
										 [ 'user_firstname', 'user_firstname', 'rowid' ],
										 [ 'user_lastname', 'user_lastname', 'rowid' ],
										 [ 'user_email', 'user_email', 'rowid' ],
										 [ 'delete', 'delete', undef ],
										 ],

						   'default_sort_field' => 'user_lastname',
								
						   'search' => ['user_ident', 'user_firstname', 'user_lastname'],
							  );


# ============================================================================
# Invited resource tiny list.
# ============================================================================

my %tiny_inv_resources_sll_desc = ( 'name' => 'tiny_meeting_resources_sll',
																
									'fields' => [ [ 'rowid', 'rowid'],
												  [ 'resource_ident', 'resource_ident', 'rowid' ],
												  [ 'resource_location', 'resource_location', 'rowid' ],
												  [ 'delete', 'delete'],
											    ],                      
									'nb_lines_per_page' => 5,
								  );



my %ro_tiny_inv_resources_sll_desc = ( 'name' => 'tiny_meeting_resources_sll',
																
									'fields' => [ [ 'rowid', 'rowid'],
												  [ 'resource_ident', 'resource_ident', 'rowid' ],
												  [ 'resource_location', 'resource_location', 'rowid' ],
											    ],                      
									'nb_lines_per_page' => 5,
								  );



# ============================================================================
# Invited resources list.
# ============================================================================

my %inv_resources_sll_desc = ( 'name' => 'meeting_resources_sll',
																
						   'fields' => [ [ 'rowid', 'rowid'],
										 [ 'resource_ident', 'resource_ident', 'rowid' ],
										 [ 'resource_location', 'resource_location', 'rowid' ],
										 [ 'delete', 'delete', undef ],
										 ],

						   'search' => ['resource_ident', 'resource_location'],
							  );


# ============================================================================

=head2 CreateMeeting ()

	Meeting creation method.

=head3 Generated XML :

    <CreateMeeting>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <referer>URL of calling application</referer>


	   <task_category rowid="category rowid" fgcolor="category text color"
	                 bgcolor="category background color">
	        Category Name
	   </task_category>


	   <fields>

          <!-- 
             Fields value description. See Mioga2::tools::args_checker->ac_XMLifyArgs
             -->

       </fields>


       <!--
            Errors description. See Mioga2::tools::args_checker->ac_XMLifyErrors
         -->
    

       <UserList>

       <!--
           List of user invited in meeting LargeList description.
	       See Mioga2::LargeList::RowidList
         -->

       </UserList>

       <ResourceList>

       <!--
           List of resource invited in meeting LargeList description.
	       See Mioga2::LargeList::RowidList
         -->

       </ResourceList>

    </CreateMeeting>

=cut

# ============================================================================

sub CreateMeeting {
   my ($self, $context) = @_;

   my $session = $context->GetSession();
   my $config  = $context->GetConfig();
   
   my $values = {};
   my $errors = [];
	($values, $errors) = ac_CheckArgs($context, [ [ [ 'rowids' ], 'stripxws', 'allow_empty'],
													 [ [ 'select_users_back', 'select_resources_back', 'search_date', 'edit_user_list', 'search_date_list_back' ], 'stripxws', 'allow_empty'],
													 [ [ 'sortby_tiny_meeting_users_sll', 'pagenb_tiny_meeting_users_sll', 'nb_elem_per_page_tiny_meeting_users_sll', ], 'stripxws', 'allow_empty'],
												   ]);

   if( ! keys %{$context->{args}} or st_ArgExists($context, 'rowid')) {
	   delete $session->{organizer}->{meeting};
	   $session->{organizer}->{meeting}->{referer} = $context->GetReferer();
	   $errors = [];
	   $values = {};
   }
   # Meeting duplication
   elsif(st_ArgExists($context, 'duplicate_act')) {
	   delete $session->{organizer}->{meeting}->{values}->{rowid};
   }
   # Back arguments.
   elsif(! st_ArgExists($context, 'name')) {
	   $errors = [];
	   $values = {};
   }
   else  {
	   ($values, $errors) = ac_CheckArgs($context, [ [ [ 'name', 'category_id'], 'stripxws', 'disallow_empty'],
													 [ [ 'duration_hour', 'duration_min'], 'want_int', 'disallow_empty'],
													 [ [ 'description' ], 'stripxws', 'allow_empty'],
												   ]);
   }

   $self->InitializeMeetingValues($context, $values);
   #
   # Save the meeting in database (OK Button)
   # 
   if( st_ArgExists($context, 'meeting_ok_act')) {
	   if( ! @$errors) {
		   $self->SaveMeeting($context, $values, $errors);
		   
		   if(!@$errors) {
				$session->{__internal}->{message}{type} = 'info';
				$session->{__internal}->{message}{text} = __('Appointment successfully created.');
				my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
				$content->SetContent($session->{organizer}->{meeting}->{referer});
				return $content;
		   }
	   }
   }

   #
   # Destroy the meeting
   # 

   elsif(st_ArgExists($context, "confirm_delete_meeting") or (st_ArgExists($context, "delete_meeting") and ! $config->AskConfirmForDelete())) {
	   $self->Org_DeleteMeeting($context, $values->{rowid});
	   
		$session->{__internal}->{message}{type} = 'info';
		$session->{__internal}->{message}{text} = __('Appointment successfully deleted.');
		my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
		$content->SetContent($session->{organizer}->{meeting}->{referer});
		return $content;
   }

   elsif(st_ArgExists($context, "confirm_delete_meeting_keep_task") or (st_ArgExists($context, "delete_meeting_keep_task") and ! $config->AskConfirmForDelete())) {
	   $self->Org_DeleteMeetingKeepTask($context, $values->{rowid});
	   
				$session->{__internal}->{message}{type} = 'info';
				$session->{__internal}->{message}{text} = __('Appointment successfully deleted.');
				my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
				$content->SetContent($session->{organizer}->{meeting}->{referer});
				return $content;
   }

   elsif(st_ArgExists($context, "delete_meeting_keep_task")) {
	   my $content = new Mioga2::Content::XSLT($context, stylesheet => 'organizer_meeting.xsl', locale_domain => 'organizer_xsl');
	   
	   my $xml = '<?xml version="1.0"?>';
	   $xml .= "<ConfirmMeetingDelete>";
	   $xml .= $context->GetXML();
	   $xml .= "<type>confirm_delete_meeting_keep_task</type>";
	   $xml .= "</ConfirmMeetingDelete>";
	   $content->SetContent($xml);
	   return $content;
   }

   elsif(st_ArgExists($context, "delete_meeting")) {
	   my $content = new Mioga2::Content::XSLT($context, stylesheet => 'organizer_meeting.xsl', locale_domain => 'organizer_xsl');
	   
	   my $xml = '<?xml version="1.0"?>';
	   $xml .= "<ConfirmMeetingDelete>";
	   $xml .= $context->GetXML();
	   $xml .= "<type>confirm_delete_meeting</type>";
	   $xml .= "</ConfirmMeetingDelete>";
	   $content->SetContent($xml);
	   return $content;
   }

   #
   # Redirect to BuildDateList
   #
   elsif(st_ArgExists($context, 'search_date')) {
	   my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	   $content->SetContent('BuildDateList');
	   return $content;
   }

   #
   # Redirect to EditUserList
   #
   elsif (st_ArgExists($context, 'view_user_list')) {
	   my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	   $content->SetContent('EditUserList');
	   return $content;
   }

   #
   # Redirect to EditResourceList
   #
   elsif (st_ArgExists($context, 'view_resource_list')) {
	   my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	   $content->SetContent('EditResourceList');
	   return $content;
   }


   #
   # Launch select multiple user
   #
   elsif (st_ArgExists($context, 'edit_user_list')) {
	   return $self->LaunchSelectMultipleUsers($context, $values);
   }

   #
   # Launch select multiple resource
   #
   elsif (st_ArgExists($context, 'edit_resource_list')) {
	   return $self->LaunchSelectMultipleResources($context, $values);
   }

   #
   # Redirect to ProposeMeeting
   #
   elsif( st_ArgExists($context, 'meeting_propose_act')) {
	   if( ! @$errors) {
		   $self->SaveMeeting($context, $values, $errors);

		   if(!@$errors) {	   
			   my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
			   $content->SetContent('ProposeMeeting');
			   return $content;
		   }
	   }
   }

   #
   # Add or update tasks in invited users organizer.
   # Redirect to ValidateMeeting
   #
   elsif( st_ArgExists($context, 'meeting_validate_act')) {
	   if( ! @$errors) {
		   $self->SaveMeeting($context, $values, $errors);
		   
		   if(!@$errors) {
			   return $self->ValidateMeeting($context);
		   }
	   }
   }

   else {
	   $errors = [];
   }

   #
   # Generate XML
   #

   my $content = new Mioga2::Content::XSLT($context, stylesheet => 'organizer_meeting.xsl', locale_domain => 'organizer_xsl');
   
   my $xml = '<?xml version="1.0"?>';
   my $creation = 1;

   if(exists $values->{rowid}) {
	   $creation = 0;
   }


   $xml .= qq|<CreateMeeting creation="$creation"|;
	if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
	}
	$xml .= ">";

   $xml .= $self->GetXMLMeeting($context, $values, $errors, 1);
 
   $xml .= "</CreateMeeting>";

   
   $content->SetContent($xml);
   return $content;
}


# ============================================================================

=head2 ViewMeeting ()

	Meeting view method.

=head3 Generated XML :

   $xml .= "<ViewMeeting>";

       <miogacontext>See Mioga2::RequestContext</miogacontext>

	   <!-- List of user emails -->
       <users>
	       <email>email address</email>
	       ...
       </users>
   
       <!-- If user have write access -->
       <canWrite/>
	
	   	   <task_category rowid="category rowid" fgcolor="category text color"
	                 bgcolor="category background color">
	        Category Name
	   </task_category>


	   <fields>

          <!-- 
             Fields value description. See Mioga2::tools::args_checker->ac_XMLifyArgs
             -->

       </fields>


       <!--
            Errors description. See Mioga2::tools::args_checker->ac_XMLifyErrors
         -->
    

       <UserList>

       <!--
           List of user invited in meeting LargeList description.
	       See Mioga2::LargeList::RowidList
         -->

       </UserList>

       <ResourceList>

       <!--
           List of resource invited in meeting LargeList description.
	       See Mioga2::LargeList::RowidList
         -->

       </ResourceList>


    </ViewMeeting>

=cut

# ============================================================================

sub ViewMeeting {
   my ($self, $context) = @_;

   my $session = $context->GetSession();
   my $config  = $context->GetConfig();
   

   if(exists $context->{args}->{rowid}) {
	   $session->{organizer}->{meeting}->{values} = 
	       $self->Org_GetMeeting($context, $context->{args}->{rowid});
	   $session->{organizer}->{meeting}->{referer} = $context->GetReferer();
   }

   my $values = $session->{organizer}->{meeting}->{values};

   #
   # Generate XML
   #
   my $content = new Mioga2::Content::XSLT($context, stylesheet => 'organizer_meeting.xsl', locale_domain => 'organizer_xsl');
   
   my $xml = '<?xml version="1.0"?>';
   $xml .= "<ViewMeeting";
	if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
	}
	$xml .= ">";

   my $user_list = new Mioga2::Old::UserList($config, rowid_list => $values->{users});
   my $emails = $user_list->GetEmails();

   $xml .= "<users>";
   foreach my $email (@$emails) {
	   $xml .= "<email>".st_FormatXMLString($email)."</email>";
   }
   $xml .= "</users>";
   

   if($self->CheckUserAccessOnMethod($context, $context->GetUser(), $context->GetGroup(), 'CreateMeeting')) {
	   $xml .= "<canWrite/>";
   }

   $xml .= $self->GetXMLMeeting($context, $values, [], 0);
   $xml .= "</ViewMeeting>";
   
   $content->SetContent($xml);
   return $content;
}


# ============================================================================

=head2 BuildDateList ()

	Date list search (planning like view) method.

=head3 Generated XML :

    <BuildDateList>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <!-- List of date -->
	   <fields>

	       <day>Date description. See Mioga2::tools::date_utils->du_GetXMLDate</day>

       </fields>
	
       <!-- See Calendar description in Mioga2::Organizer->DisplayMain  -->


       <!-- User List -->
	   <user>
    	   <rowid>user rowid</rowid>
	       <firstname>user first name</firstname>
	       <lastname>user last name</lastname>
	   
           <!-- List of user tasks -->
           <task diff="number of empty cells since last task" size="current task number cell"/>
           ...

	   </user>


       <!-- Resource List -->
	   <resource>
    	   <rowid>resource rowid</rowid>
	       <ident>resource ident</ident>
	   
           <!-- List of user tasks -->
           <task diff="number of empty cells since last task" size="current task number cell"/>
           ...

	   </resource>

    </BuildDateList>

=cut

# ============================================================================

sub BuildDateList {
   my ($self, $context) = @_;

   my $session = $context->GetSession();
   my $config  = $context->GetConfig();
   my $dates   = $session->{organizer}->{meeting}->{values}->{dates};

   my $users  = $session->{organizer}->{meeting}->{values}->{users};
   my $resources  = $session->{organizer}->{meeting}->{values}->{resources};

   my $values = {};
   my ($chkvalues, $errors) = ac_CheckArgs($context, [ [ [ 'month', 'year', 'day', ], 'allow_empty', 'want_int'],
													 [ [ 'add_date', 'zone' ], 'allow_empty', 'stripxws'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Organizer::BuildDateList", ac_FormatErrors($errors));
	}


   if(st_ArgExists($context, 'add_date')) {
	   $self->AddDateToList($context, $dates);
   }

   $values = {};
   $self->InitializeBuildDateValues($context, $values);

   my $xml = '<?xml version="1.0"?>';
   $xml .= "<BuildDateList>";
   $xml .= $context->GetXML();

   $xml .= $self->GetXMLDayList($context, $values);

   $xml .= "<fields>";
   $xml .= $self->GetXMLDates($context, $session->{organizer}->{meeting}->{values});
   $xml .= "</fields>";

   my ($year, $month, $day) = ($values->{startDate} =~ /^(\d+)-(\d+)-(\d+)/);

   $xml .= $self->_MonthCalendar( $context, $month, $year,
								  $day, $day, {});


   #
   # Add user planning
   #
   my $user_list = new Mioga2::Old::UserList($config, rowid_list => $users);
   my $fns = $user_list->GetFirstnames();
   my $lns = $user_list->GetLastnames();
   my $types = $user_list->GetTypes();
   my $nb_user = @$users;
   
   for(my $i=0; $i < $nb_user; $i++) {
	   my $user = $users->[$i];
	   my $fn = $fns->[$i];
	   my $ln = $lns->[$i];

	   my $start = du_ConvertLocaleISOToGMT($values->{startDate}, $context->GetUser());
	   my $stop  = du_ConvertLocaleISOToGMT($values->{stopDate}, $context->GetUser());

	   my $tasks;
	   if($types->[$i] eq 'external_user') {
		   $tasks = $self->Org_ListStrictPeriodicTasksForExternalUser($context, $user, $self->GetTimezone($context), $start, $stop);
	   }
	   else {
		   $tasks = $self->Org_ListStrictPeriodicTasks($context, $user, $self->GetTimezone($context), $start, $stop);
	   }

	   $xml .= "<user>";
	   $xml .= "<rowid>$user</rowid>";
	   $xml .= "<firstname>".st_FormatXMLString($fn)."</firstname>";
	   $xml .= "<lastname>".st_FormatXMLString($ln)."</lastname>";
	   
	   $xml .= $self->GetXMLTaskList($context, $values, $tasks);

	   $xml .= "</user>";
   }


   #
   # Add resource planning
   #
   
   my $resource_list = new Mioga2::Old::ResourceList($config, rowid_list => $resources);
   my $idents = $resource_list->GetIdents();
   my $nb_resource = @$resources;
   for(my $i=0; $i < $nb_resource; $i++) {

	   my $resource = $resources->[$i];

	   my $ident = $idents->[$i];
	   
	   my $start = du_ConvertLocaleISOToGMT($values->{startDate}, $context->GetUser());
	   my $stop  = du_ConvertLocaleISOToGMT($values->{stopDate}, $context->GetUser());

	   if(! du_IsWorkingDayForUser($config, $start, $context->GetUser())) {
		   next;
	   }	   

	   my $tasks = $self->Org_ListStrictPeriodicTasks($context, $resource, $self->GetTimezone($context), $start, $stop);
	   
	   $xml .= "<resource>";
	   $xml .= "<rowid>$resource</rowid>";
	   $xml .= "<ident>".st_FormatXMLString($ident)."</ident>";
	   

	   $xml .= $self->GetXMLTaskList($context, $values, $tasks);

	   $xml .= "</resource>";
   }


   $xml .= "</BuildDateList>";

   my $content = new Mioga2::Content::XSLT($context, stylesheet => 'organizer_meeting.xsl', locale_domain => 'organizer_xsl');
   $content->SetContent($xml);
   return $content;   
}


# ============================================================================

=head2 ProposeMeeting ()

	Send an email to invited Meeting form.

=head3 Generated XML :

   <ProposeMeeting>
 
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <!-- Current user identity -->
       <me>
          <firstname>Firstname</firstname>
          <lastname>Lastname</lastname>
       </me>

       <!-- Invited Users -->
       <invited>
          <user>
             <firstname>First name</firstname>
             <lastname>Last name</lastname>
          </user>
       
          ...  
       </invited>
   
       <!-- selected date list -->   
       <date rowid="date rowid">|;
	       Date description. See Mioga2::tools::date_utils->du_GetXMLDate
       </date>
       ....


       <name>Meeting name</name>
       <description>Meeting description</description>

       <duration>
          <hour>duration hour</hour>
	      <min>duration minute</min>
      </duration>

   </ProposeMeeting>

=cut

# ============================================================================

sub ProposeMeeting {
   my ($self, $context) = @_;

   my $session = $context->GetSession();

   my $values  = $session->{organizer}->{meeting}->{values};
   my $users   = new Mioga2::Old::UserList($context->GetConfig(), rowid_list => $session->{organizer}->{meeting}->{values}->{users});

   if(st_ArgExists($context, 'proposal_send_act')) {
	   my $me = $context->GetUser();

		my $subject = encode_mimeword($context->{args}->{subject}, "Q", "UTF-8");
	   my $entity = MIME::Entity->build(
				Charset => 'UTF8',
				Type    => "text/plain",
				From    => $me->GetFirstname()." ".$me->GetLastname()." <".$me->GetEmail().">",
				Subject => $subject, 
				Data    => $context->{args}->{body}
			);

	   $users->SendMail($entity);

		$session->{__internal}->{message}{type} = 'info';
		$session->{__internal}->{message}{text} = __('Message sent to invitees.');
		my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
		$content->SetContent('CreateMeeting?propose_meeting_back');
		return $content;
   }

   my $xml = '<?xml version="1.0"?>';
   $xml .= "<ProposeMeeting";
	if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
	}
	$xml .= ">";
   $xml .= $context->GetXML();

   $xml .= "<me>";
   $xml .= "<firstname>".st_FormatXMLString($context->GetUser()->GetFirstname())."</firstname>";
   $xml .= "<lastname>".st_FormatXMLString($context->GetUser()->GetLastname())."</lastname>";
   $xml .= "</me>";
   $xml .= "<invited>";
   
   my $fns = $users->GetFirstnames();
   my $lns = $users->GetLastnames();

   my $nb_users = @$fns;
   for(my $i = 0; $i<$nb_users; $i++) {
	   $xml .= "<user>";
	   $xml .= "<firstname>".st_FormatXMLString($fns->[$i])."</firstname>";
	   $xml .= "<lastname>".st_FormatXMLString($lns->[$i])."</lastname>";
	   $xml .= "</user>";
   }
   
   $xml .= "</invited>";  
   
   $xml .= $self->GetXMLDates($context, $values);

   $xml .= "<name>".st_FormatXMLString($values->{name})."</name>";
   $xml .= "<description>".st_FormatXMLString($values->{description})."</description>";

   $xml .= "<duration>";
   $xml .= "<hour>".st_FormatXMLString($values->{duration_hour})."</hour>";
   $xml .= "<min>".st_FormatXMLString($values->{duration_min})."</min>";
   $xml .= "</duration>";

   $xml .= "</ProposeMeeting>";

   my $content = new Mioga2::Content::XSLT($context, stylesheet => 'organizer_meeting.xsl', locale_domain => 'organizer_xsl');
   $content->SetContent($xml);
   return $content;   
}


# ============================================================================

=head2 ValidateMeeting ()

	Add the meeting into the invited users and resources organizer.
	Redirection to referer with InlineMessage.

=cut

# ============================================================================

sub ValidateMeeting {
   my ($self, $context) = @_;

   my $session = $context->GetSession();
   my $values  = $session->{organizer}->{meeting}->{values};

   my $userlist = new Mioga2::Old::UserList($context->GetConfig(), rowid_list => $values->{users});
   my $types = $userlist->GetTypes();

   my $nb_user = @$types;

   for(my $i=0; $i < $nb_user; $i++) {

	   my $user =  $values->{users}->[$i];
	   my $type = $types->[$i];

	   my $fields = { group_id => $user, 
					  category_id => $values->{category_id},
					  description => $values->{description},
					  name => $values->{name},
					  start => du_SecondToISO($values->{dates}->[0]->{date}),
					  duration => $values->{duration_min}*60 + $values->{duration_hour}*3600,
				  };
	   

	   my $rowid;
	   if($type eq 'external_user') {
		   $rowid = $self->Org_CreateTaskForExternalUser($context, 'planned', $fields);
	   }
	   else {
		   $rowid = $self->Org_CreateTask($context, 'planned', $fields);
	   }

	   $self->Org_UpdateMeetingResource($context, $values->{rowid}, $user, $rowid);
   }

   
   foreach my $user (@{$values->{resources}}) {
	   my $fields = { group_id => $user, 
					  category_id => $values->{category_id},
					  description => $values->{description},
					  name => $values->{name},
					  start => du_SecondToISO($values->{dates}->[0]->{date}),
					  duration => $values->{duration_min}*60 + $values->{duration_hour}*3600,
				  };
	   
	   my $rowid = $self->Org_CreateTask($context, 'planned', $fields);

	   $self->Org_UpdateMeetingResource($context, $values->{rowid}, $user, $rowid);
   }


	$session->{__internal}->{message}{type} = 'info';
	$session->{__internal}->{message}{text} = __('Message sent to invitees.');
	my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	$content->SetContent('ViewMeeting?validate_meeting_back');
	return $content;
}



# ============================================================================
#
# EditUserList ()
#
#	User list edition method.
#
# ============================================================================

sub EditUserList {
   my ($self, $context) = @_;

   my $session = $context->GetSession();
   my $rowids  = $session->{organizer}->{meeting}->{values}->{users};
   
   if(st_ArgExists($context, 'add_user')) {
	   return $self->LaunchSelectMultipleUsers($context, $session->{organizer}->{meeting}->{values});
   }

   elsif(st_ArgExists($context, 'select_users_back')) {
	   if(exists $context->{args}->{rowids}) {
		   foreach my $rowid (split(/,/, $context->{args}->{rowids})) {
			   if(!grep {$rowid == $_} @$rowids) {
				   push @$rowids, $rowid;
			   }
		   }
	   }
   }

   my $xml = '<?xml version="1.0"?>';
   $xml .= "<EditUserList>";
   $xml .= $context->GetXML();

   my $user_sll = new Mioga2::LargeList::RowidList($context, \%inv_users_sll_desc, $rowids);
   $xml .= $user_sll->Run($context);

   $xml .= "</EditUserList>";

   my $content = new Mioga2::Content::XSLT($context, stylesheet => 'organizer_meeting.xsl', locale_domain => 'organizer_xsl');
   $content->SetContent($xml);
   return $content;   
}


# ============================================================================
#
# GetXMLMeeting ()
#
#	Return xml describing current meeting.
#
# ============================================================================

sub GetXMLMeeting {
	my ($self, $context, $values, $errors, $write) = @_;

	my $session = $context->GetSession();

	my $xml = "<referer>".
	          st_FormatXMLString($session->{organizer}->{meeting}->{referer}).
		      "</referer>";
   
	$xml .= $context->GetXML();
	
	$xml .= $self->GetXMLTaskCat($context);
	
	$xml .= "<fields>";
	$xml .= ac_XMLifyArgs(['name', 'duration_hour', 'duration_min', 'category_id', 'description' ], 
						  $values );
	
	$xml .= $self->GetXMLDates($context, $values);
	
	$xml .= "</fields>";
	
	$xml .= ac_XMLifyErrors($errors);
	

	my $user_list = ($write ? \%tiny_inv_users_sll_desc : \%ro_tiny_inv_users_sll_desc);
	$xml .= "<UserList>";
	my $user_sll = new Mioga2::LargeList::RowidList($context, $user_list, $values->{users});
	$xml .= $user_sll->Run($context);
	$xml .= "</UserList>";
	
	my $resource_list = ($write ? \%tiny_inv_resources_sll_desc : \%ro_tiny_inv_resources_sll_desc);
	$xml .= "<ResourceList>";
	my $resource_sll = new Mioga2::LargeList::RowidList($context, $resource_list, $values->{resources});
	$xml .= $resource_sll->Run($context);
	$xml .= "</ResourceList>";
	
	return $xml;
}

# ============================================================================
#
# EditResourceList ()
#
#	Resource list edition method.
#
# ============================================================================

sub EditResourceList {
   my ($self, $context) = @_;

   my $session = $context->GetSession();
   my $rowids  = $session->{organizer}->{meeting}->{values}->{resources};
   
   if(st_ArgExists($context, 'add_resource')) {
	   return $self->LaunchSelectMultipleResources($context, $session->{organizer}->{meeting}->{values});
   }

   elsif(st_ArgExists($context, 'select_resources_back')) {
	   if(exists $context->{args}->{rowids}) {
		   foreach my $rowid (split(/,/, $context->{args}->{rowids})) {
			   if(!grep {$rowid == $_} @$rowids) {
				   push @$rowids, $rowid;
			   }
		   }
	   }
   }

   my $xml = '<?xml version="1.0"?>';
   $xml .= "<EditResourceList>";
   $xml .= $context->GetXML();

   my $resource_sll = new Mioga2::LargeList::RowidList($context, \%inv_resources_sll_desc, $rowids);
   $xml .= $resource_sll->Run($context);

   $xml .= "</EditResourceList>";

   my $content = new Mioga2::Content::XSLT($context, stylesheet => 'organizer_meeting.xsl', locale_domain => 'organizer_xsl');
   $content->SetContent($xml);
   return $content;   
}



# ============================================================================
#
# PRIVATE METHODS DESCRIPTION
#
# ============================================================================

# ============================================================================
#
# SaveMeeting ()
#
#	Add (or update) the meeting into the database.
#
# ============================================================================

sub SaveMeeting {
	my($self, $context, $values, $errors) = @_;

	if(!exists $values->{rowid}) {
		try {
			$values->{rowid} = $self->Org_CreateMeeting($context);
		}
		
		otherwise {
			my $err = shift;
			push @$errors, [$err->as_string, ["__create__"]];
		};
	}

	else {
		try {
			$self->Org_UpdateMeeting($context);
		}

		otherwise {
			my $err = shift;
			push @$errors, [$err->as_string, ["__modify__"]];
		};
		
	}

}


# ============================================================================
#
# LaunchSelectMultipleUsers ()
#
#	Launch the select multiple user list.
#
#	Store current form values into session,
#	Build Select application URI
#	And return a REDIRECT content.
#
# ============================================================================

sub LaunchSelectMultipleUsers {
	my($self, $context, $values) = @_;
	
	my $config  = $context->GetConfig();
	my $group   = $context->GetGroup();
	my $cur_uri = $context->GetURI();
	
    my $uri = new Mioga2::URI($config, group  => $group->GetIdent(),
	                                   public => 0,
	                                   application => 'Select',
	                                   method => 'SelectMultipleUsers',
	                                   args   => { action  => 'select_users_back',
	                                               referer => $cur_uri->GetURI(),
												   rowids  => join(',', @{$values->{users}})
											   }
	                                   );
	
	my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	$content->SetContent($uri->GetURI());

	return $content;
}


# ============================================================================
#
# LaunchSelectMultipleResources ()
#
#	Launch the select multiple resource list.
#
#	Store current form values into session,
#	Build Select application URI
#	And return a REDIRECT content.
#
# ============================================================================

sub LaunchSelectMultipleResources {
	my($self, $context, $values) = @_;
	
	my $config  = $context->GetConfig();
	my $group   = $context->GetGroup();
	my $cur_uri = $context->GetURI();
	
    my $uri = new Mioga2::URI($config, group  => $group->GetIdent(),
	                                   public => 0,
	                                   application => 'Select',
	                                   method => 'SelectMultipleResourcesAllowedInGroup',
	                                   args   => { action  => 'select_resources_back',
	                                               referer => $cur_uri->GetURI(),
												   rowids  => join(',', @{$values->{resources}})
											   }
	                                   );
	
	my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	$content->SetContent($uri->GetURI());

	return $content;
}


# ============================================================================
#
# InitializeBuildDateValues ()
#
#	Initialize start/stop dates with default values or session values.
#	Store values in session.
#
# ============================================================================

sub InitializeBuildDateValues {
	my($self, $context, $values) = @_;

	my $session = $context->GetSession();

	my @start = du_GetDateInUserLocale(time,         $context->GetUser());
	my @stop  = du_GetDateInUserLocale(time+86400*7, $context->GetUser());


	my $default;
		
	$default = { startDate => strftime("%Y-%m-%d 00:00:00", 0, 0, 0, @start[3,4,5]),
				 stopDate  => strftime("%Y-%m-%d 23:59:59", 0, 0, 0, @stop[3,4,5]),
  			   };


	foreach my $key (qw(startDate stopDate)) {
		if(! exists $values->{$key} and 
		   exists $session->{organizer}->{'meeting-bd'}->{values}->{$key}) {
			$values->{$key} = $session->{organizer}->{'meeting-bd'}->{values}->{$key};
		}
	}
	
	foreach my $key (qw(startDate stopDate)) {
		if(! exists $values->{$key}) {
			$values->{$key} = $default->{$key};
		}
	}



	if(st_ArgExists($context, 'prev_week')) {
		my $newdate = du_ISOToSecond($values->{startDate}) - 7*86400;
		$values->{startDate} = strftime("%Y-%m-%d 00:00:00", localtime($newdate)),

		my $newlastdate = du_ISOToSecond($values->{stopDate}) - 7*86400;
		$values->{stopDate} = strftime("%Y-%m-%d 23:59:59", localtime($newlastdate)),
	}


	if(st_ArgExists($context, 'next_week')) {
		my $newdate = du_ISOToSecond($values->{startDate}) + 7*86400;
		$values->{startDate} = strftime("%Y-%m-%d 00:00:00", localtime($newdate)),

		my $newlastdate = du_ISOToSecond($values->{stopDate}) + 7*86400;
		$values->{stopDate} = strftime("%Y-%m-%d 23:59:59", localtime($newlastdate)),
	}


	$self->CheckDeleteDates($context, $session->{organizer}->{meeting}->{values});
	$self->UpdateDatesValues($context, $session->{organizer}->{meeting}->{values});

	$session->{organizer}->{'meeting-bd'}->{values} = $values;
	
}


# ============================================================================
#
# InitializeMeetingValues ()
#
#	Initialize Values with default values.
#	Treat date add/remove action.
#	Store values in session.
#
# ============================================================================

sub InitializeMeetingValues {
	my($self, $context, $values) = @_;

	my $session = $context->GetSession();

	my $default = { duration_hour => 1,
					duration_min  => 0,
					dates         => [],
					users         => [$context->GetUser()->GetRowid()],
					resources     => [],
					name          => '',
					description   => ''
				  };


	if(st_ArgExists($context, 'rowid')) {
		$default = $self->Org_GetMeeting($context, $context->{args}->{rowid});
		$values->{rowid} = $context->{args}->{rowid};
	}

	foreach my $key (qw(name description category_id duration_hour duration_min dates users resources rowid)) {
		if(! exists $values->{$key} and 
		   exists $session->{organizer}->{meeting}->{values}->{$key}) {
			$values->{$key} = $session->{organizer}->{meeting}->{values}->{$key};
		}
	}

	foreach my $key (qw(name description category_id duration_hour duration_min dates users resources)) {
		if(! exists $values->{$key}) {
			$values->{$key} = $default->{$key};
		}
	}


	if(st_ArgExists($context, 'select_users_back')) {
	   if(exists $context->{args}->{rowids}) {
		   foreach my $rowid (split(/,/, $context->{args}->{rowids})) {
		   		$rowid = int($rowid);
			   if(!grep {$rowid == $_} @{$values->{users}}) {
				   push @{$values->{users}}, $rowid;
			   }
		   }
	   }
	}

	if(st_ArgExists($context, 'select_resources_back')) {
	   if(exists $context->{args}->{rowids}) {
		   foreach my $rowid (split(/,/, $context->{args}->{rowids})) {
			   if(!grep {$rowid == $_} @{$values->{resources}}) {
				   push @{$values->{resources}}, $rowid;
			   }
		   }
	   }
	}


	my $dates = $values->{dates};

	if(st_ArgExists($context, 'add_date')) {
		my @date = localtime();
		$date[1] = int($date[1]/5)*5;
		push @$dates, { rowid => 'new_'.time, date => strftime("%s", @date) };
	}
	

	
	$self->CheckDeleteDates($context, $values);

	$self->UpdateDatesValues($context, $values);

	$session->{organizer}->{meeting}->{values} = $values;
}



# ============================================================================
#
# UpdateDatesValues ()
#
#	Update dates values.
#
# ============================================================================

sub UpdateDatesValues {
	my($self, $context, $values) = @_;

	foreach my $key (keys %{$context->{args}}) {
		if($key =~ /^day_(.*)/) {	
			my $rowid = $1;
			
			my @date = grep { $_->{rowid} eq $rowid} @{$values->{dates}};

			{
				local $ENV{TZ} = $self->GetTimezone($context);
				tzset();
				$date[0]->{date} = strftime ("%s", 0, $context->{args}->{"min_$rowid"}, $context->{args}->{"hour_$rowid"}, 
										              $context->{args}->{"day_$rowid"}, $context->{args}->{"month_$rowid"}-1,
										              $context->{args}->{"year_$rowid"}-1900 );
			}
		}
	}

	tzset();
}

# ============================================================================
#
# CheckDeleteDates ()
#
#	Check date deletion request in URL args.
#
# ============================================================================

sub CheckDeleteDates {
	my($self, $context, $values) = @_;

	foreach my $key (keys %{$context->{args}}) {
		if($key =~ /^del_date_([^\.]*)/) {	
			my $rowid = $1;

			@{$values->{dates}} = grep { $_->{rowid} ne $rowid} @{$values->{dates}};
		}
	}
}


# ============================================================================
#
# GetXMLTaskCat ()
#
#	Return xml describing task categories
#
# ============================================================================

sub GetXMLTaskCat {
	my($self, $context) = @_;

	my $task_cat_list = $self->Org_TaskCatGetList($context);
	my $xml = "";
	
	foreach my $cat (@$task_cat_list) {
		$xml .= qq|<task_category rowid="$cat->{rowid}" fgcolor="$cat->{fgcolor}" bgcolor="$cat->{bgcolor}">|;
		$xml .= st_FormatXMLString($cat->{name});
		$xml .= qq|</task_category>|;
	}
	
	return $xml;
}



# ============================================================================
#
# GetXMLDayList ()
#
#	Return xml describing days list.
#
# ============================================================================

sub GetXMLDayList {
	my($self, $context, $values) = @_;

	my $start = du_ISOToSecond($values->{startDate});
	my $stop  = du_ISOToSecond($values->{stopDate});

	my $xml = "";
	while($start <= $stop) {
		if(du_IsWorkingDayForUser($context->GetConfig(), du_SecondToISO($start), $context->GetUser())) {
			$xml .= "<day>".du_GetDateXML($start)."</day>";
		}
		$start += 86400;
	}


	return $xml;
}

# ============================================================================
#
# GetXMLTaskList ()
#
#	Return xml describing tasks list.
#
# ============================================================================

sub GetXMLTaskList {
	my($self, $context, $values, $tasks) = @_;

	my $xml = "";

	my $lastsec = du_ConvertLocaleISOToGMT($values->{startDate}, $context->GetUser());
	$lastsec = du_ISOToSecond($lastsec);

	my ($lastday) = ($values->{startDate} =~ /^(\d+-\d+-\d+)/);
	my $lastzone=0;

	foreach my $task (@$tasks) {
		my @date = du_GetDateInUserLocale(du_ISOToSecond($task->{start}), $context->GetUser());
		my @stop = du_GetDateInUserLocale(du_ISOToSecond($task->{start}) + $task->{duration}-1, $context->GetUser());
	
 		my $day = strftime("%Y-%m-%d", @date);
		my $sec = strftime("%s", 0, 0, 0, @date[3,4,5]);

		my $zone = $self->GetZone(\@date);
		my $endzone = $self->GetZone(\@stop);

		my $nb_not_worked = du_GetNumberOfNotWorkingDayForUserBetween($context->GetConfig(), $lastday, $day, $context->GetUser());
	
		my $diff = 4*int(($sec - $lastsec) / 86400) + $zone - $lastzone - 1 - $nb_not_worked*4;
		my $size = $endzone - $zone + 1;

		if($diff < 0) {
			$size += $diff;
			$diff = 0;
		}

		if($size <= 0) {
			next;
		}

		$xml .= qq|<task diff="$diff" size="$size"/>|;

		$lastzone = $endzone;
		$lastsec  = $sec;
		$lastday  = $day;
	}

	return $xml;
}


sub GetZone {
	my ($self, $date) = @_;

	my $zone;
	if( $date->[2] < 10) {
		$zone = 1;
	}
	elsif($date->[2] >= 10 and $date->[2] < 13) {
		$zone = 2;
	}
	elsif($date->[2] >= 13 and $date->[2] < 16) {
		$zone = 3;
	}
	elsif($date->[2] >= 16) {
		$zone = 4;
	}

	return $zone;
}


# ============================================================================
#
# GetXMLDates ()
#
#	Return xml describing meeting dates
#
# ============================================================================

sub GetXMLDates {
	my($self, $context, $values) = @_;

	my $xml = "";

	foreach my $date (@{$values->{dates}}) {
		$xml .= qq|<date rowid="$date->{rowid}">|;
		$xml .= du_GetDateXMLInUserLocale($date->{date}, $context->GetUser());
		$xml .= qq|</date>|;
	}
	
	return $xml;
}


# ============================================================================
#
# AddDateToList ()
#
#	Add a date to the list.
#
# ============================================================================

sub AddDateToList {
	my($self, $context, $dates) = @_;

	my $hour = (8, 10, 13, 16)[$context->{args}->{zone}-1];

	my $newdate;

	{
		local $ENV{TZ} = $self->GetTimezone($context);
		tzset();

		$newdate =  strftime ("%s", 0, 0, $hour, 
									      $context->{args}->{day},
									      $context->{args}->{month}-1,
									      $context->{args}->{year}-1900 );
	}
	tzset();

	push @$dates, { rowid => "new_".time, date => $newdate};
}


##******************************************************************************
## Method _GetMeetingXML  private
##******************************************************************************
sub _GetMeetingXML
{
   my ($self, $context, $canWrite) = @_;
   my $xml = "";

   $xml .= "<MeetingList canWrite=\"$canWrite\">\n";

   my $list = $self->Org_GetMeetingList( $context, $context->GetGroup()->GetRowid());
   foreach my $meeting (@$list)
   {
       if($meeting->{private} == 1 and $context->{user}->{rowid} == $context->{group}->{id_admin}) {
           $meeting->{private} = 0;
       }
	   
	   my $access = $canWrite && $meeting->{nb_task} == 0;

	   $xml .= "   <Meeting rowid=\"$meeting->{rowid}\" private=\"$meeting->{private}\" fgcolor=\"$meeting->{fgcolor}\" bgcolor=\"$meeting->{bgcolor}\" canWrite=\"$access\">\n";
	   $xml .= "      ".st_FormatXMLString($meeting->{name})."\n";
	   $xml .= "   </Meeting>\n";
   }
   $xml .= "</MeetingList>\n";

   return $xml;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application 

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
