# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Organizer.pm: The Mioga2 Organizer application.

=head1 METHODS DESRIPTION

=cut
package Mioga2::Organizer;

use strict;
use POSIX qw(strftime setlocale LC_TIME tzset);
use Mioga2::tools::date_utils;
use Mioga2::tools::APIAuthz;
use locale;


##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
## Public Methods
##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



# ============================================================================

=head2 ViewMonth ()

	Main function for the Month view in organizer.

=head3 Generated XML
	

  <!-- The Root element parameters describe :
       month : the displayed week of month
       year : the displayed year

       isWorkingDay : set to 1 is the currently selected day is worked.
       canWrite : set to 1 if the current user can write in the current Organizer.

<ViewMonth month="05" year="2005">


<!-- 
   Here must be the standard miogacontext and calendar headers.
   See Organizer-DisplayMain-Basic.xml for more details.
  -->

<!--
     Description of tasks 

     WeekTasks attributes :
     
     month and year : month number and year of displayed month.
     
     -->


<MonthTasks month="05" year="2005">

  <!-- 
       DayNames : one entry per displayed day of week
       -->
   <DayNames>
      <DayName dayInWeek="1"/>
      <DayName dayInWeek="2"/>
      <DayName dayInWeek="3"/>
      <DayName dayInWeek="4"/>
      <DayName dayInWeek="5"/>
      <DayName dayInWeek="6"/>
      <DayName dayInWeek="7"/>
   </DayNames>

  <!-- 
       Week : one entry per displayed week of month

       number : number of the week in the current year.
       year : year of the displayed month.


       Inside Week, there is a MonthDay entry per day of week.
       If MonthDay is empty (no attributes) it s just an empty cells.


       MonthDay parameters :

       day, month, year : date of the cell.
       isWorkingDay : set to 1 if the day is worked.



       Inside MonthDay, there is a list of MonthTask.
       There is a MonthTask entry per task.

       MonthTask atributes :
       like DayTask or WeekTask.


       Inside MonthDay, there is a FlexibleTask per FlexibleTask.

       -->

   <Week number="17" year="2005">
      <MonthDay >
      </MonthDay>
      <MonthDay >
      </MonthDay>
      <MonthDay >
      </MonthDay>
      <MonthDay >
      </MonthDay>
      <MonthDay >
      </MonthDay>
      <MonthDay >
      </MonthDay>
      <MonthDay day="01" month="05" year="2005" isWorkingDay="0" >
      </MonthDay>
   </Week>
   <Week number="18" year="2005">
      <MonthDay day="02" month="05" year="2005" isWorkingDay="1" >
         <MonthTask startMin="00" startHour="09" conflict="0" stopMin="00" stopHour="10" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditStrictTask" taskrowid="10" is_planned="1">
            Little task
         </MonthTask>
         <FlexibleTask empty="1"/>
         <FlexibleTask empty="1"/>
      </MonthDay>
      <MonthDay day="03" month="05" year="2005" isWorkingDay="1" >
         <MonthTask startMin="00" startHour="09" conflict="0" stopMin="00" stopHour="16" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditStrictTask" taskrowid="9" is_planned="1">
            Big task
         </MonthTask>
         <FlexibleTask empty="1"/>
         <FlexibleTask empty="1"/>
      </MonthDay>
      <MonthDay day="04" month="05" year="2005" isWorkingDay="1" >
         <FlexibleTask empty="0"  name="Flexible Task 2" bgcolor="#CCCCCC" fgcolor="#000000" private="0" taskrowid="8" type="flexible"/>
         <FlexibleTask empty="1"/>
      </MonthDay>
      <MonthDay day="05" month="05" year="2005" isWorkingDay="0" >
         <FlexibleTask empty="1"/>
         <FlexibleTask empty="1"/>
      </MonthDay>
      <MonthDay day="06" month="05" year="2005" isWorkingDay="1" >
         <MonthTask startMin="00" startHour="10" conflict="0" stopMin="00" stopHour="11" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditStrictTask" taskrowid="1" is_planned="1">
            task 1
         </MonthTask>
         <MonthTask startMin="00" startHour="14" conflict="1" stopMin="00" stopHour="16" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditStrictTask" taskrowid="2" is_planned="1">
            task 2
         </MonthTask>
         <MonthTask startMin="00" startHour="15" conflict="1" stopMin="00" stopHour="17" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditStrictTask" taskrowid="6" is_planned="1">
            Conflicting task
         </MonthTask>
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
         <FlexibleTask empty="0"  name="Flexible Task 2" bgcolor="#CCCCCC" fgcolor="#000000" private="0" taskrowid="8" type="flexible"/>
         <FlexibleTask empty="0"  name="Flexible Task 1" bgcolor="#CCCCCC" fgcolor="#000000" private="0" taskrowid="7" type="flexible"/>
      </MonthDay>
      <MonthDay day="07" month="05" year="2005" isWorkingDay="0" >
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
         <FlexibleTask empty="1"/>
         <FlexibleTask empty="1"/>
      </MonthDay>
      <MonthDay day="08" month="05" year="2005" isWorkingDay="0" >
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
         <FlexibleTask empty="1"/>
         <FlexibleTask empty="1"/>
      </MonthDay>
   </Week>
   <Week number="19" year="2005">
      <MonthDay day="09" month="05" year="2005" isWorkingDay="1" >
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
         <FlexibleTask empty="0"  name="Flexible Task 2" bgcolor="#CCCCCC" fgcolor="#000000" private="0" taskrowid="8" type="flexible"/>
      </MonthDay>
      <MonthDay day="10" month="05" year="2005" isWorkingDay="1" >
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
         <FlexibleTask empty="1"/>
      </MonthDay>
      <MonthDay day="11" month="05" year="2005" isWorkingDay="1" >
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
         <FlexibleTask empty="1"/>
      </MonthDay>
      <MonthDay day="12" month="05" year="2005" isWorkingDay="1" >
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
         <FlexibleTask empty="1"/>
      </MonthDay>
      <MonthDay day="13" month="05" year="2005" isWorkingDay="1" >
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
         <FlexibleTask empty="1"/>
      </MonthDay>
      <MonthDay day="14" month="05" year="2005" isWorkingDay="0" >
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
         <FlexibleTask empty="1"/>
      </MonthDay>
      <MonthDay day="15" month="05" year="2005" isWorkingDay="0" >
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
         <FlexibleTask empty="1"/>
      </MonthDay>
   </Week>
   <Week number="20" year="2005">
      <MonthDay day="16" month="05" year="2005" isWorkingDay="0" >
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
      </MonthDay>
      <MonthDay day="17" month="05" year="2005" isWorkingDay="1" >
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
      </MonthDay>
      <MonthDay day="18" month="05" year="2005" isWorkingDay="1" >
         <MonthTask startMin="00" startHour="18" conflict="0" stopMin="00" stopHour="19" bgcolor="#CCCCCC" fgcolor="#000000" private="0" method="EditPeriodicTask" taskrowid="4" is_planned="1">
            Periodic Task
         </MonthTask>
      </MonthDay>
      <MonthDay day="19" month="05" year="2005" isWorkingDay="1" >
      </MonthDay>
      <MonthDay day="20" month="05" year="2005" isWorkingDay="1" >
      </MonthDay>
      <MonthDay day="21" month="05" year="2005" isWorkingDay="0" >
      </MonthDay>
      <MonthDay day="22" month="05" year="2005" isWorkingDay="0" >
      </MonthDay>
   </Week>
   <Week number="21" year="2005">
      <MonthDay day="23" month="05" year="2005" isWorkingDay="1" >
      </MonthDay>
      <MonthDay day="24" month="05" year="2005" isWorkingDay="1" >
      </MonthDay>
      <MonthDay day="25" month="05" year="2005" isWorkingDay="1" >
      </MonthDay>
      <MonthDay day="26" month="05" year="2005" isWorkingDay="1" >
      </MonthDay>
      <MonthDay day="27" month="05" year="2005" isWorkingDay="1" >
      </MonthDay>
      <MonthDay day="28" month="05" year="2005" isWorkingDay="0" >
      </MonthDay>
      <MonthDay day="29" month="05" year="2005" isWorkingDay="0" >
      </MonthDay>
   </Week>
   <Week number="22" year="2005">
      <MonthDay day="30" month="05" year="2005" isWorkingDay="1" >
      </MonthDay>
      <MonthDay day="31" month="05" year="2005" isWorkingDay="1" >
      </MonthDay>
      <MonthDay >
      </MonthDay>
      <MonthDay >
      </MonthDay>
      <MonthDay >
      </MonthDay>
      <MonthDay >
      </MonthDay>
      <MonthDay >
      </MonthDay>
   </Week>
</MonthTasks>

</ViewMonth>

=cut

# ============================================================================

sub ViewMonth
{
   my ($self, $context, %message) = @_;
   my $userTimeZone = $self->GetTimezone($context);

   $context->{args}{method} = 'ViewMonth';
   $self->InitializeViewState($context);

   my $test = $self->CheckUserAccessOnMethod($context, $context->GetUser(), 
											 $context->GetGroup(), 'EditStrictTask');

   my $canWrite = ($test == AUTHZ_OK);

   my $session = $context->GetSession();

   ##===========================================================================
   ## Arguments
   ##===========================================================================
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'month', 'year' ], 'stripxws', 'allow_empty'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Organizer::ViewMonth", ac_FormatErrors($errors));
	}

   my ($year,$month);
   my $format;
   if( defined($context->{args}{month}) and
      !defined($context->{args}{year}) )
   {
      ($year,$month) = ( $context->{args}{month} =~ m/^([0-9]+)-([0-9]+)$/ );
      $format = "month=$context->{args}{month}";
   }
   elsif( exists( $session->{organizer}{viewState} ) )
   {
      ($year,$month) = ( $session->{organizer}{viewState}{parameter} =~
                        m/^([0-9]+)-([0-9]+)$/ );
      $format = "viewState=$context->{args}{month}";
   }
   else
   {
      return $self->_Error($context,"no month!");
   }

   $month = sprintf( "%02d", $month );

   ##===========================================================================
   if( !defined( $month ) or !defined( $year ) or
       $year  < 1970 or $year  > 2036 or
       $month <    1 or $month >   12 )
   {
      return $self->_Error($context,"bad format for day : $format");
   }

   ##===========================================================================
   ## Saving the current view-state
   ##===========================================================================
   my $viewState = $session->{organizer}{viewState} = {};
   $viewState->{method} = "ViewMonth";
   $viewState->{parameter} = "$year-$month";

   ##===========================================================================
   ## Starting XML
   ##===========================================================================
   my ($startYear,$stopYear) = $self->_YearInterval($context,$year);

   my $xml = "<?xml version=\"1.0\"?>\n";
   $xml .= "<ViewMonth".
           " month=\"$month\"".
           " year=\"$year\"";
   {
      local $ENV{TZ} = $userTimeZone;
	  tzset();
      $xml .= " now_day=\"".strftime("%d", localtime())."\"".
              " now_month=\"".strftime("%m", localtime())."\"".
              " now_year=\"".strftime("%Y", localtime())."\"".
              " now_hour=\"".strftime("%H", localtime())."\"".
              " now_minute=\"".strftime("%M", localtime())."\"";
   }
   tzset();
   
   if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
   }
   $xml .= " startYear=\"$startYear\" stopYear=\"$stopYear\">".
           ">\n";
   $xml .= "   ".$context->GetXML()."\n";
   my $monthDuration = du_GetMonthDuration( $month, $year ); 
   my $workingDaysHash = {};
   $xml .= $self->_MonthCalendar( $context, $month, $year, undef, undef, $workingDaysHash );

   $xml .= $self->_GetOtherUserListXML($context);

   ##===========================================================================
   ## Previous and next button in calendar
   ##===========================================================================
   my ($otherYear,$otherMonth);

   $otherMonth = $month - 1;
   $otherYear = $year;
   if( $otherMonth == 0 )
   {
      $otherMonth = 12;
      $otherYear--;
   }
   $xml .= "<Calendar-previous method=\"ViewMonth\" attribute=\"month\" ".
           "value=\"$otherYear-$otherMonth\"/>\n";

   $otherMonth = $month + 1;
   $otherYear = $year;
   if( $otherMonth == 13 )
   {
      $otherMonth = 1;
      $otherYear++;
   }
   $xml .= "<Calendar-next     method=\"ViewMonth\" attribute=\"month\" ".
           "value=\"$otherYear-$otherMonth\"/>\n";

   ##===========================================================================
   $xml .= "<MonthTasks month=\"$month\" year=\"$year\">\n";
   $xml .= $self->_DayNames();

   ##===========================================================================
   ## For each week
   ##===========================================================================
   my $weekList = $self->_GetMonthRepresentation( $month, $year );
   foreach my $weekHash (@$weekList)
   {
      $xml .= "   <Week number=\"$weekHash->{week}\" year=\"$weekHash->{year}\">\n";

      my %weekData = ( "task_nbr" => 0 );
      ##========================================================================
      ## For each day 1/2
      ##========================================================================
      foreach my $day (@{$weekHash->{days}})
      {
         next if( $day eq '' );

         $weekData{"day$day"} = {};

         ##---------------------------------------------------------------------
         my ($local0000,$local2359);
         {
            local $ENV{TZ} = $userTimeZone;  
			tzset();
            my $local00Sec = strftime('%s',  0,  0,  0, $day, $month-1, $year-1900 );
            my $local23Sec = strftime('%s', 59, 59, 23, $day, $month-1, $year-1900 )+1;
            local $ENV{TZ} = 'GMT';
			tzset();
            $local0000 = strftime("%Y-%m-%d %H:%M:%S GMT", gmtime($local00Sec));
            $local2359 = strftime("%Y-%m-%d %H:%M:%S GMT", gmtime($local23Sec));
         }
		 tzset();


         $weekData{"day$day"}->{local0000} = $local0000;
         $weekData{"day$day"}->{local2359} = $local2359;
		 $weekData{"day$day"}->{day} = "$year-$month-$day";
         next if( !$workingDaysHash->{"$year-$month-$day"} );
 
         ##---------------------------------------------------------------------
		  my ($start, $stop);
		  {
			  local $ENV{TZ} = $userTimeZone;
			  tzset();
			  
			  my $startsec = du_ISOToSecond("$year-$month-$day 00:00:00");
			  my $stopsec  = du_ISOToSecond("$year-$month-$day 23:59:59");
			  
			  $start = strftime("%Y-%m-%d %H:%M:%S", gmtime($startsec));
			  $stop = strftime("%Y-%m-%d %H:%M:%S", gmtime($stopsec));
		  }
		 tzset();
		  
         $weekData{"day$day"}->{flexibleList} =
			 $self->Org_ListFlexibleTasks( $context, $context->GetGroup()->GetRowid(), $start, $stop);

         foreach my $flexible (@{ $weekData{"day$day"}->{flexibleList} })
         {
            if( !exists( $weekData{"task_$flexible->{rowid}"} ) )
            {
               $weekData{"task_$flexible->{rowid}"} = $weekData{"task_nbr"};
               $weekData{"task_nbr"}++;
            }
            $flexible->{view_rank} = $weekData{"task_$flexible->{rowid}"};
         }

         @{ $weekData{"day$day"}->{flexibleList} } =
            sort( { $a->{view_rank} <=> $b->{view_rank} }
                  @{ $weekData{"day$day"}->{flexibleList} } );

      }

      ##========================================================================
      ## For each day 2/2
      ##========================================================================
      foreach my $day (@{$weekHash->{days}})
      {
         my $contentXml = "";

         ##---------------------------------------------------------------------------
         if( $day ne '' )
         {
            my $taskList = $self->Org_ListStrictPeriodicTasks( $context,  $context->GetGroup()->GetRowid(), $userTimeZone,
															   $weekData{"day$day"}->{local0000},
															   $weekData{"day$day"}->{local2359} );


			my $notplannedtasks = $self->Org_ListNotPlannedTasks( $context, $context->GetGroup()->GetRowid(),
																  $weekData{"day$day"}->{day},
																  $weekData{"day$day"}->{day});

			##------------------------------------------------------------------------
            ## Tasks pre-treatement : creation of {start|stop}MinuteDay fields
            ##------------------------------------------------------------------------
			foreach my $task (@$notplannedtasks) {
				my $method;
				my $tasktype = $task->{type};
				if( $canWrite )
				{
					if(    $tasktype eq "periodic" ) { $method = "EditPeriodicTask"; }
					else                             { $method = "EditStrictTask"; }
				}
				else
				{
					if(    $tasktype eq "periodic" ) { $method = "ViewPeriodicTask"; }
					else                             { $method = "ViewStrictTask"; }
				}
				if($task->{private} == 1 and $context->GetUser()->GetRowid() == $context->GetGroup()->GetAnimId()) {
           $task->{private} = 0;
         }

				$contentXml .= "         <MonthTask bgcolor=\"$task->{bgcolor}\" fgcolor=\"$task->{fgcolor}\" private=\"$task->{private}\" ".
                                                  "method=\"$method\" taskrowid=\"$task->{rowid}\" is_planned=\"0\">\n";
				$contentXml .= "            ".st_FormatXMLString($task->{name})."\n";
				$contentXml .= "         </MonthTask>\n";

		
			}




            my $previvousTask = undef;
            for(my $i=0; $i < @$taskList; $i++) {
		my $task = $taskList->[$i];
            
		my $conflict = 0;
		my ($startYear,$startMonth,$startDay,$startHour,$startMin) =
                                  ( $task->{start} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
		my ($stopHour,$stopMin);
		{
			local $ENV{TZ} = 'GMT';
			tzset();
			my $startSec = strftime('%s', 0, $startMin, $startHour, $startDay, $startMonth-1, $startYear-1900);
			my $stopSec = $startSec + $task->{duration};
			local $ENV{TZ} = $userTimeZone;
			tzset();
			($startHour,$startMin) = split( /:/, strftime("%H:%M", localtime($startSec)) );
			($stopHour,$stopMin) = split( /:/, strftime("%H:%M", localtime($stopSec)) );
		}
		tzset();

	       if($task->{private} == 1 and $context->GetUser()->GetRowid() == $context->GetGroup()->GetAnimId()) {
			   $task->{private} = 0;
	       }

               $task->{startMinuteDay} = $startMin + 60 * $startHour;
               $task->{stopMinuteDay} = $stopMin + 60 * $stopHour;

               my $method;
               my $tasktype = $task->{type};
               if( $canWrite )
               {
                  if(    $tasktype eq "periodic" ) { $method = "EditPeriodicTask"; }
                  else                             { $method = "EditStrictTask"; }
               }
               else
               {
                  if(    $tasktype eq "periodic" ) { $method = "ViewPeriodicTask"; }
                  else                             { $method = "ViewStrictTask"; }
               }

	       ##---------------------------------------------------------------------
               ## conflicts detection
               ##---------------------------------------------------------------------
               if( defined( $previvousTask ) and
                   $task->{startMinuteDay} < $previvousTask->{stopMinuteDay} )
               {
                  $conflict = 1;
               }

		if($i+1 < @$taskList) {
		    my ($nextStopHour,$nextStopMin);
		    {
			my $nextTask = $taskList->[$i+1];
			my ($nextStartYear,$nextStartMonth,$nextStartDay,$nextStartHour,$nextStartMin) = ( $nextTask->{start} =~ /^(\d+)-(\d+)-(\d+) (\d+):(\d+):/ );
			local $ENV{TZ} = 'GMT';
			tzset();
			my $nextStartSec = strftime('%s', 0, $nextStartMin, $nextStartHour, $nextStartDay, $nextStartMonth-1, $nextStartYear-1900);
			local $ENV{TZ} = $userTimeZone;
			tzset();
			($nextStartHour,$nextStartMin) = split( /:/, strftime("%H:%M", localtime($nextStartSec)) );

			if( $nextStartMin + 60 * $nextStartHour < $task->{stopMinuteDay} ) {
			    $conflict = 1;
			}

		    }
		    

		}
		tzset();
	       $contentXml .= "         <MonthTask startMin=\"$startMin\" startHour=\"$startHour\" conflict=\"$conflict\" ".
                                                  "stopMin=\"$stopMin\" stopHour=\"$stopHour\" ".
                                                  "bgcolor=\"$task->{bgcolor}\" fgcolor=\"$task->{fgcolor}\" private=\"$task->{private}\" ".
                                                  "method=\"$method\" taskrowid=\"$task->{rowid}\" is_planned=\"1\">\n";
               $contentXml .= "            ".st_FormatXMLString($task->{name})."\n";
               $contentXml .= "         </MonthTask>\n";

               $previvousTask = $task;
            }

            ##------------------------------------------------------------------------
            ## Flexible Tasks
            ##------------------------------------------------------------------------
            my $actualFlexible = 0;
            my $canGlobal  = undef;
            my $canProject = undef;

            for( my $rank=0; $rank<$weekData{task_nbr}; $rank++ )
            {
               my $flexible = $weekData{"day$day"}->{flexibleList}->[$actualFlexible];
               if( defined( $flexible ) and
                   $flexible->{view_rank} == $rank )
               {
                  my ($id) = $flexible->{rowid};
				  my $type = $flexible->{type};

		  if($flexible->{private} == 1 and  $context->GetUser()->GetRowid() == $context->GetGroup()->GetAnimId()) {
		      $flexible->{private} = 0;
		  }


				  $canGlobal = 1;
				  $canProject = 1;

                  $type = 'none' unless( ( $type eq 'flexible' and $canGlobal  ) or
                                         ( $type eq 'project'   and $canProject ) );

                  $contentXml .= "         <FlexibleTask empty=\"0\" ".
                                 " name=\"".st_FormatXMLString($flexible->{name})."\"".
                                 " bgcolor=\"$flexible->{bgcolor}\" fgcolor=\"$flexible->{fgcolor}\" private=\"$flexible->{private}\"".
                                 " taskrowid=\"$id\" type=\"$type\"/>\n";
                  $actualFlexible++;
               }
               else
               {
                  $contentXml .= "         <FlexibleTask empty=\"1\"/>\n";
               }
            }
         }

         ##---------------------------------------------------------------------------
         $xml .= "      <MonthDay";
         my $longDay = sprintf( "%02d", $day );
         my $longMonth = sprintf( "%02d", $month );
         if( $day ne '' )
         {
            $xml .= " day=\"$longDay\"";
            $xml .= " month=\"$longMonth\"";
            $xml .= " year=\"$year\"";
            $xml .= " isWorkingDay=\"".$workingDaysHash->{"$year-$longMonth-$longDay"}."\"";
         }
         $xml .= " >\n";
         $xml .= $contentXml;
         $xml .= "      </MonthDay>\n";

      }
      $xml .= "   </Week>\n";
   }

   $xml .= "</MonthTasks>\n";

   ##===========================================================================
   if($context->GetGroup()->GetType() ne 'resource') {
	   $xml .= $self->_GetMeetingXML($context, $canWrite);
   }
   $xml .= $self->_GetTodoTasksXML($context, $canWrite);
   $xml .= "</ViewMonth>\n";

   return $self->_MiogaResponse($context,$xml);

}


# ============================================================================
#
# PRIVATE METHODS DESCRIPTION
#
# ============================================================================

##******************************************************************************
## Method _GetMonthRepresentation  private
##  Description  : builds a month
##******************************************************************************
sub _GetMonthRepresentation
{
    my($self, $month, $year) = @_;

    my $weekList = [];
    my $actualWeek = {};
    my $dayweeknbr = 0; # number of day in the week for the month
    my $weekNumber = undef;
    my $lastWeek = -1;
    my $weekYear=$year;
    $month -= 1;
    $year -= 1900;

    for(my $day = 1; ; ++$day)
    {
        my ($week, $dayNumber, $monthNumber, $weekYear) = split(/,/, strftime("%V,%u,%m,%G", 0, 0, 0, $day, $month, $year) );

        if( $lastWeek == -1 )      { $lastWeek = $week; }

        $monthNumber --;
        $week --;

        last if($monthNumber != $month);

        if(not defined($weekNumber) or $weekNumber != $week)
        {
            if(defined($weekNumber))
            {
               push @$weekList, $actualWeek;
            }
            $actualWeek = {'week'=>sprintf("%02d", $week + 1),'year'=>$weekYear,'days'=>[]};
            $weekNumber = $week;
            for(my $i = 1; $i < $dayNumber; ++$i)
            {
                push @{$actualWeek->{days}}, "";
            }
            $dayweeknbr = 0;
        }
        push @{$actualWeek->{days}}, sprintf("%02d",$day);
        ++$dayweeknbr;
    }

    for( ; $dayweeknbr<7; $dayweeknbr++)
    {
       push @{$actualWeek->{days}}, "";
    }

    push @$weekList, $actualWeek;

    return $weekList;
}

##******************************************************************************
## Method _DayNames  private
##******************************************************************************
sub _DayNames
{
   my $xml .= qq!   <DayNames>\n!;
   for( my $i=1; $i<=7; $i++ )
   {
      $xml .= "      <DayName dayInWeek=\"".$i."\"/>\n";
   }
   $xml .= qq!   </DayNames>\n!;
   return $xml;
}

##******************************************************************************
## Method _MonthCalendar  private
##  Description  : builds the XML for a month
##******************************************************************************
sub _MonthCalendar
{
   my($self, $context, $month, $year, $firstDay, $lastDay, $workingDaysHash) = @_;
   my $xml = "";
   $xml .= "<Calendar month=\"$month\" year=\"$year\"";
   $xml .= " firstDay=\"$firstDay\"" if( defined($firstDay) );
   $xml .= " lastDay=\"$lastDay\"" if( defined($lastDay) );
   $xml .= ">\n";
   $xml .= $self->_DayNames();

   my $weekList = $self->_GetMonthRepresentation( $month, $year );
   foreach my $weekHash (@$weekList)
   {
      $xml .= "   <Week number=\"$weekHash->{week}\" year=\"$weekHash->{year}\">\n";

      foreach my $day (@{$weekHash->{days}})
      {
         $xml .= "      <Day";
         if( $day ne "" )
         {
            $xml .= " number=\"" . sprintf("%02d", $day) . "\"";

			my $workingDay;
			if($context->GetGroup()->GetType() eq 'resource') {
				$workingDay = du_IsWorkingDayForResource($context->GetConfig(), "$year-$month-$day 00:00:00", $context->GetGroup());
			}
			else {
				$workingDay = du_IsWorkingDayForUser($context->GetConfig(), "$year-$month-$day 00:00:00", $context->GetUser());
			}


            $workingDaysHash->{"$year-$month-$day"} = $workingDay
               if( defined($workingDaysHash) );
            $xml .= " isWorkingDay=\"$workingDay\"";
         }
         $xml .= "/>\n";

      }
      $xml .= "   </Week>\n";
   }

   $xml .= "</Calendar>\n";

   return $xml;
}




# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
