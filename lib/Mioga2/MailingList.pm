# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
MailingList.pm: The Mioga II Mailing list animation and archive viewer 
	             application.

=head1 METHODS DESRIPTION

=cut

package Mioga2::MailingList;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'mailinglist';

use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Error qw(:try);
use Mioga2::Exception::Simple;
use Mioga2::Application;
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::Content::FILE;
use Data::Dumper;
use Date::Manip;
use MIME::Parser;
use MIME::Entity;
use Text::Iconv;
use Encode::Detect::Detector;
use POSIX qw(tmpnam);

my $debug = 0;

# ============================================================================
# Public Methods
# ============================================================================

sub GetAppDesc
{
	my $self = shift;
    
    my %AppDesc = ( ident   => 'MailingList',
	    	name	=> __('Mailing-list'),
                package => 'Mioga2::MailingList',
                description => __('The Mioga2 MailingList archive viewer application'),
				type    => 'normal',
                all_groups => 1,
                all_users  => 0,
                can_be_public => 0,
				is_user             => 0,
				is_group            => 1,
				is_resource         => 0,

        		functions 	=> { Anim	=> __('Animation functions'),
		                         User 	=> __('User functions'),
								 },
				func_methods 	=> {
					             Anim   => [ 'DeleteMessage'],
								 User 	=> [ 'DisplayMain', 'DisplayMessage', 'DisplayYearMonthMsg', 'DownloadJoined' ],
								 },

				sxml_methods => {},

				xml_methods  => {},

				non_sensitive_methods => [
					'DisplayMain',
					'DisplayMessage',
					'DisplayYearMonthMsg',
					'DownloadJoined',
					'DeleteMessage',
				]

                );
    
	return \%AppDesc;
}


# ============================================================================

=head2 DisplayMain ()

	Main Mioga entry point.

=head3 Generated XML :

	<DisplayMain>

	    <miogacontext>See Mioga2::RequestContext</miogacontext>

        <error id="$error" />

	    <!-- For each year/month having archived mail -->
	    <YearMonthCount year="year" month="month" count="number of mail" />
        ...

   </DisplayMain>

=cut

# ============================================================================

sub DisplayMain
{
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	my $group  = $context->GetGroup();

	my $error = 'ok';
	
	# display directly msg (useful for crawler)
	# -----------------------------------------
	if (defined($context->{args}->{rowid}))
	{

		my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');

		my $uri = new Mioga2::URI($config, group       => $group,
                                           public      => 0,
                                           application => 'MailingList',
                                           method      => 'DisplayMessage',
								           args        => { rowid => $context->{args}->{rowid} });
		
		$content->SetContent($uri->GetURI());
		
		return $content;
	}

	my $xml = '<?xml version="1.0"?>';
	# -----------------------------
	# Generate XML
	# -----------------------------
	$xml .= "<DisplayMain>";
	$xml .= $context->GetXML();
	$xml .= qq|<error id="$error" />|;

	my $year_month = $self->MailGetYearMonth($config, $group->GetRowid());

	warn Dumper($year_month) if $debug;

	foreach my $line (@{$year_month })
	{
		$xml .= qq|<YearMonthCount year="$line->{year}" month="$line->{month}" count="$line->{count}" />\n|;
	}
	$xml .= "</DisplayMain>";

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'mailinglist.xsl', locale_domain => 'mailinglist_xsl');
	$content->SetContent($xml);
	return $content;
}

# ============================================================================

=head2 DisplayYearMonthMsg ()

	Display archive for the given month.
	
	
=head3 Generated XML :
    
	<DisplayYearMonthMsg>

	    <miogacontext>See Mioga2::RequestContext</miogacontext>

        <referer>URL of calling application</referer>
 
        <!-- If the curernt user is animator -->
        <can_anim/>

	    <!-- List of messages -->
        <messages>
             <message rowid="message rowid">
                 <date>Message expedition date</date>
                 <subject>Message subject</subject>
                 <from>Message sender</from>
             </message>
             ...
        </messages>
 
   </DisplayYearMonthMsg>

=cut

# ============================================================================

sub DisplayYearMonthMsg
{
	my ($self, $context) = @_;

	my $session = $context->GetSession ();
	my $config = $context->GetConfig();
	my $referer = $context->GetReferer();
	my $group = $context->GetGroup();

	my $error = 'ok';
	
	my $xml = '<?xml version="1.0"?>';
	# -----------------------------
	# Generate XML
	# -----------------------------

	my $messages = $self->MailGetMsgForGroup ($config, 
											  $group->GetRowid(),
											  $context->{args}->{year},
											  $context->{args}->{month} );
	
	$xml .= "<DisplayYearMonthMsg";
	if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
	}
	$xml .= ">";
	$xml .= $context->GetXML();
	$xml .= qq|<error id="$error" />|;
	$xml .= "<referer>" . st_FormatXMLString($referer) . "</referer>";

	if($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), 'Anim')) {
		$xml .= "<can_anim/>";
	}

	$xml .= "<messages>\n";

	foreach my $message (@{$messages})
	{
		$xml .= "<message rowid=\"$message->{rowid}\">\n";
		$xml .= "   <date>" . st_FormatXMLString($message->{date}) . "</date>\n";
		$xml .= "   <subject>" . st_FormatXMLString($message->{subject}) . "</subject>\n";
		$xml .= "   <from>" . st_FormatXMLString($message->{mail_from}) . "</from>\n";
		$xml .= "</message>\n";
	}
	$xml .= "</messages>\n";
	$xml .= "</DisplayYearMonthMsg>";

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'mailinglist.xsl', locale_domain => 'mailinglist_xsl');
	$content->SetContent($xml);
	return $content;
}

# ============================================================================

=head2 DisplayMessage ()

	Display the given message.
	
=head3 Generated XML :
    
	<DisplayMessage>

	    <miogacontext>See Mioga2::RequestContext</miogacontext>

        <referer>URL of calling application</referer>
 
        <!-- message description -->
        <Mail>
              <Subject>Message subject</Subject>
              <From>Message sender address</From>
              <To>Message recipient address</To>
              <Cc>Message "Carbon Copy" recipient addresses</Cc>
 

              <date>
                 <!-- Meesage expedition date. See Mioga2::tools::date_utils->du_ISOToXML -->
              </date>

              <!-- For each message part -->

              <!-- For printable part (text, html, ...) -->
              <printable_part>Printable Content</printable_part>

              <!-- For image -->
              <image_part>Image URI</image_part>


              <!-- For RFC822 message (another joined message -->
              <Mail> 
                  ... a message description ...
              </Mail>

              <!-- For another unknown message type (joined binary file, etc)    
              <unknown_part>
                 <type>Mime Type</type>
                 <size>File Size</size>
                 <unit>File size unit (K M G)</unit>
                 <name>Part name</name>
              </unknown_part>

       </Mail>
    </DisplayMessage>

=cut

# ============================================================================

sub DisplayMessage
{
	my ($self, $context) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $referer = $context->GetReferer();
	my $group = $context->GetGroup();

	my $error = 'ok';
	
	my $xml = '<?xml version="1.0"?>';
	# -----------------------------
	# Generate XML
	# -----------------------------

	$xml .= "<DisplayMessage>";
	$xml .= $context->GetXML();
	$xml .= qq|<error id="$error" />|;
	$xml .= "<referer>" . st_FormatXMLString($referer) . "</referer>";

	$session->{MailingList}->{DisplayMessage}->{rowid} = $context->{args}->{rowid};

	$xml .= $self->MessageToXML($context, $context->{args}->{rowid});


	$xml .= "</DisplayMessage>";

	my $content = new Mioga2::Content::XSLT($context, stylesheet => 'mailinglist.xsl', locale_domain => 'mailinglist_xsl');
	$content->SetContent($xml);
	return $content;
}


# ============================================================================

=head2 DeleteMessage ()

	Delete the given message and return a success message

	
=cut

# ============================================================================

sub DeleteMessage
{
	my ($self, $context) = @_;

	my $session = $context->GetSession ();
	my $config = $context->GetConfig();

	$self->MailDeleteMsg ($config, $context->{args}->{rowid});

	$session->{__internal}->{message}{type} = 'info';
	$session->{__internal}->{message}{text} = __('Message successfully deleted.');
	my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	$content->SetContent($context->GetReferer);
	return $content;
}


# ============================================================================

=head2 DownloadJoined ()

	Return Joined files of a mail .

    Return joined file. Take an argument : name=part_name.
	
=cut

# ============================================================================

sub DownloadJoined {
    my ($self, $context) = @_;
    
	my $config = $context->GetConfig();
	my $session = $context->GetSession();
	my $tmpdir = $config->GetTmpDir();

	my $message_id = $session->{MailingList}->{DisplayMessage}->{rowid};
    my $name = $context->{args}->{name};

	my $message = $self->MailGetMessage($config, $message_id);

    my $parser = new MIME::Parser;
    $parser->extract_nested_messages(0); 
    $parser->decode_headers(1);  

    my $output_dir = "$tmpdir/mail_output_${message_id}";

	system("rm -rf $output_dir");
    mkdir ($output_dir, 0700);

    $parser->output_dir($output_dir);

	my $entity = $parser->parse_data($message->{content});
	
	my @parts;
    if($entity->is_multipart) {
        @parts = $entity->parts;
    }
    else {
        $parts[0] = $entity;
    }

    foreach my $part (@parts) {
        my $mime_type = $part->mime_type;

		if($mime_type eq 'message/rfc822') {
            my $content = $part->bodyhandle->as_string;
			$parser->parse_data($content);
        }
	}

	my $content = new Mioga2::Content::FILE($context, filename => "$name");
    $content->SetContent("$output_dir/$name");	

    return $content;
}



# ============================================================================
#
# PRIVATE METHODS DESCRIPTION
#
# ============================================================================

# ============================================================================
#
# MessageToXML ()
#
#	Return XML describing the message
#	
# ============================================================================

sub MessageToXML {
    my ($self, $context, $message_id) = @_;
	
	my $config = $context->GetConfig();
	my $tmpdir = $config->GetTmpDir();

    my $parser = new MIME::Parser;
    $parser->extract_nested_messages(0); 
    $parser->decode_headers(1);  

    my $output_dir = "$tmpdir/mail_output_${message_id}";

	system("rm -rf $output_dir");
    mkdir ($output_dir, 0700);
 
	$parser->output_dir($output_dir);

	my $message = $self->MailGetMessage($config, $message_id);

	my $res = $self->EntityToXML($context, $parser, $message->{content});


	system("rm -rf $output_dir");

	return $res;
}



# ============================================================================
#
# EntityToXML ()
#
#	Return XML describing the MIME::Entity.
#	
# ============================================================================

sub EntityToXML {
	my ($self, $context, $parser, $content) = @_;

    my $entity = $parser->parse_data($content);


    my $xml;
    $xml .= "<Mail>";

    my $head = $entity->head;

    foreach my $header ("Subject", "From", "To", "Cc") {
		my $h = $head->get($header, 0);
		$xml .= "<$header>".st_FormatXMLString(CheckUTF8($h))."</$header>\n";
	}
   
	my $date = $head->get("Date", 0);
	if(defined $date) {
	   	$date = ParseDate($head->get("Date", 0));
		$date = UnixDate($date, "%s");
		
		$xml.= "<date>".du_ISOToXMLInUserLocale(du_SecondToISO($date), $context->GetUser())."</date>";
	}
    
    my @parts;

    if($entity->is_multipart) {
        @parts = $entity->parts;
    }
    else {
        $parts[0] = $entity;
    }

    foreach my $part (@parts) {
        my $mime_type = $part->mime_type;

        if($mime_type =~ /^text\//) {
            my $content = CheckUTF8($part->bodyhandle->as_string);
          
            if($mime_type eq 'text/plain') {
                $content =~ s/\r?\n/<br\/>/g;
            }

            $xml .= "<printable_part>".st_FormatXMLString($content)."</printable_part>";
        }
        elsif ($mime_type =~ /^image\//) {
            my $name = $part->bodyhandle->path;
            $name =~ s|^.*/([^/]+)$|$1|;
            $xml .= "<image_part>".st_FormatXMLString($name)."</image_part>";
        }
        elsif($mime_type eq 'message/rfc822') {
            my $content = CheckUTF8($part->bodyhandle->as_string);
          
            $xml .= $self->EntityToXML($context, $parser, $content);
        }
        else {
            my $name = $part->bodyhandle->path;

            my @stat = stat($name);
            my $size = $stat[7];
			my $unit = __"B";

            if($size > 1024*1024) {
                $size = sprintf("%.1f", $size/(1024*1024));
				$unit = __"MB";
            }
            elsif($size > 1024) {
                $size = sprintf("%.1f", $size/1024);
				$unit = __"KB";
            }

            $name =~ s|^.*/([^/]+)$|$1|;

            $xml .= "<unknown_part>";
            $xml .= "<type>".CheckUTF8($mime_type)."</type>";
            $xml .= "<size>".$size."</size>";
            $xml .= "<unit>".$unit."</unit>";
            $xml .= "<name>".st_FormatXMLString(CheckUTF8($name))."</name>";
            $xml .= "</unknown_part>";
        }
    }

	$xml .= "</Mail>";

    return $xml;
}


# ============================================================================
#
# DATA ACCESS METHODS DESCRIPTION
#
# ============================================================================


# ============================================================================
#
# DeleteGroupData ($config, $group_id)
#
#	Remove group data in database when a group is deleted.
#
#	This function is called when a group/user/resource is deleted.
#
# ============================================================================

sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;

	my $dbh = $config->GetDBH();

	ExecSQL($dbh, "DELETE FROM mailinglist WHERE group_id = $group_id");

}

# ============================================================================
#
# MailGetYearMonth ()
#
#	Return availables year/month for the given group
#	
# ============================================================================

sub MailGetYearMonth
{
	my ($self, $config, $group_id) = @_;

	my $dbh = $config->GetDBH();

	my $sql;
	my $result;

	$sql = "SELECT count(*), date_part('year', date) as year, date_part('month', date) as month FROM mailinglist WHERE group_id = $group_id GROUP BY year, month";

	$result = SelectMultiple ($dbh, $sql);

	return $result;
}



# ============================================================================
#
# MailGetMsgForGroup ()
#
#	Return availables messages for the given group in the given month/year
#	
# ============================================================================

sub MailGetMsgForGroup
{
	my ($self, $config, $group_id, $year, $month) = @_;

	my $dbh = $config->GetDBH();

	my $next_year = $year;
	my $next_month = $month;

	if ($month == 12)
	{
		$next_month = 1;
		$next_year += 1;
	}
	else
	{
		$next_month += 1;
	}

	my $sql = "SELECT * FROM mailinglist ".
		      "WHERE group_id = $group_id AND ".
		      "      date >= '$year-$month-01 00:00:00' AND ".
		      "      date < '$next_year-$next_month-01 00:00:00'";

	my $result = SelectMultiple ($dbh, $sql);

	return $result;
}


# ============================================================================
#
# MailGetMessage ()
#
#	Return the given message.
#	
# ============================================================================

sub MailGetMessage
{
	my ($self, $config, $message_id) = @_;

	my $tmpfile = tmpnam;

	my $dbh = $config->GetDBH();

	my $result;
	try {
		BeginTransaction($dbh);

		my $sql = "SELECT * FROM mailinglist ".
			      "WHERE rowid = $message_id";
		
		$result = SelectSingle ($dbh, $sql);
		$dbh->func($result->{content}, $tmpfile, "lo_export");
		
		open(F_FILE, "$tmpfile")
			or die "Can't open $tmpfile : $!";
		
		{
			local $/ = undef;
			$result->{content} = <F_FILE>;
		}
		
		close(F_FILE);
		unlink($tmpfile);

		EndTransaction($dbh);
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};

	return $result;
}



# ============================================================================
#
# MailDeleteMsg ()
#
#	Delete a message.
#	
# ============================================================================

sub MailDeleteMsg
{
	my ($self, $config, $message_id) = @_;

	my $dbh = $config->GetDBH();

	my $sql = "SELECT content FROM mailinglist ".
		      "WHERE rowid = $message_id";

	try {
		BeginTransaction($dbh);		

		my $res = SelectSingle ($dbh, $sql);
		$dbh->func($res->{content}, "lo_unlink");
		
		ExecSQL ($dbh, "DELETE FROM mailinglist WHERE rowid = $message_id");

		EndTransaction($dbh);
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};
}


## check if string is in UTF8 and convert it if needed.
sub CheckUTF8 {
    my ($str) = @_;
    
    my $conv    = Text::Iconv->new('utf8', 'utf8');
    my $tmp_str = $conv->convert($str);
    unless ($tmp_str) {
        my $charset = detect($str) || 'iso-8859-15'; # defaults to latin9
        $conv = Text::Iconv->new($charset, "utf8");
        $str  = $conv->convert($str);
    }
    return $str;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
