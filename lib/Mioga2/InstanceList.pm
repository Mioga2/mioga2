#===============================================================================
#
#         FILE:  InstanceList.pm
#
#  DESCRIPTION:  New generation Mioga2 instance class
#
# REQUIREMENTS:  ---
#        NOTES:  TODO Handle system and user skeletons, merging of both, etc.
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  22/04/2010 16:40
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

InstanceList.pm: New generation Mioga2 instance class

=head1 DESCRIPTION

This class handles Mioga2 instances. It is designed to create, load, update or delete
instances.

=head1 SYNOPSIS

 use Mioga2::InstanceList;
 use Mioga2::MiogaConf;

 my $instlist;
 my $miogaconf = new Mioga2::MiogaConf (...);

 # Create a new instance from a skeleton
 $instlist = Mioga2::InstanceList->new ($miogaconf, { attributes => { ident => "MyInstance", skeleton => '/path/to/std_inst.xml' } });
 $instlist->Store ();

 # Enable LDAP for instances that do not have LDAP enabled
 $instlist = Mioga2::InstanceList->new ($miogaconf, { attributes => { use_ldap => 'f' } });
 $instlist->Set ('use_ldap', 't');
 $instlist->Store ();

 # Delete instance whose ident is MyInstance
 $instlist = Mioga2::InstanceList->new ($miogaconf, { attributes => { ident => "MyInstance" } });
 $instlist->Delete ();

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::InstanceList;

use Locale::TextDomain::UTF8 'instancelist';

use Data::Dumper;
use File::Path;
use File::Copy::Recursive qw/dircopy/;

use Mioga2::tools::database;
use Mioga2::tools::args_checker;
use Mioga2::tools::string_utils;
use Mioga2::Config;
use Mioga2::Constants;
use Mioga2::UserList;
use Mioga2::GroupList;
use Mioga2::ResourceList;
use Mioga2::Old::Instance;
use Error qw(:try);
use Mioga2::Exception::Simple;
use Mioga2::Journal;

my $debug = 0;

# Numeric fields can not be updated with " = ''", but with " = NULL"
my @numeric_fields = qw/default_group_id pwd_min_digit pwd_min_special referent_id auth_fail_timeout pwd_min_chcase pwd_min_letter pwd_min_length quota_soft quota_hard rowid max_allowed_group ldap_access/;
# Text fields can not be updated with " = NULL", but with " = ''"
my @text_fields = qw/referent_comment comment ident domain_name password_crypt_method default_lang lang holidays ldap_host ldap_port ldap_bind_dn ldap_bind_pwd ldap_user_base_dn ldap_user_filter ldap_user_scope ldap_user_ident_attr ldap_user_firstname_attr ldap_user_lastname_attr ldap_user_email_attr ldap_user_pwd_attr ldap_user_desc_attr ldap_user_inst_attr homepage target_url/;
my @boolean_fields = qw/use_secret_question doris_can_set_secret_question use_ldap/;


#===============================================================================

=head2 new

Create a new Mioga2::InstanceList object.

=over

=item B<arguments:>

=over

=item I<$config:> a Mioga2::MiogaConf object matching Mioga2 installation parameters.

=item I<$data:> a hashref with the following keys:

=over

=item I<attributes:> a list of key / value pairs matching the instances. Key names correspond to m_mioga table field names. Other keys are supported:

=over

=item skeleton: the path to a skeleton file to apply to instance(s).

=back

=item I<journal:> a Mioga2::Journal-tied-array to record operations.

=back

=back

=item B<return:> a Mioga2::InstanceList object matching instances according to provided attributes.

=back

B<Warning:> $config argument is a Mioga2::MiogaConf object.

=cut

#===============================================================================
sub new {
	my ($class, $config, $data) = @_;
	print STDERR "[Mioga2::InstanceList::new] Entering. Data: " . Dumper $data if ($debug);

	my $self = { };
	bless($self, $class);

	$self->{config} = $config;

	# Admin account macros
	for my $attr (qw/admin_ident admin_email admin_password/) {
		$self->{admin_macros}->{uc ($attr)} = delete ($data->{attributes}->{$attr}) if (exists ($data->{attributes}->{$attr}));
	}

	# Initialize attributes
	$self->{attributes} = $data->{attributes};
	if ($self->{attributes}->{ldap_user_filter}) {
		# Process $$INSTNAME$$ macro
		my $ident = $self->Get ('ident');
		$self->{attributes}->{ldap_user_filter} =~ s/\$\$INSTNAME\$\$/$ident/g;
	}

	# Match type can be 'begins', 'contains' or 'ends' to find user from a part of their attributes
	if (defined ($data->{attributes}->{match})) {
		my $match = $data->{attributes}->{match};
		if (grep (/^$match$/, qw/begins contains ends/)) {
			$self->{match} = $data->{attributes}->{match};
		}
		delete ($data->{attributes}->{match});
	}

	# Journal of operations
	if (defined ($data->{journal})) {
		$self->{journal} = $data->{journal};
	}
	else {
		tie (@{$self->{journal}}, 'Mioga2::Journal');
	}

	# Initialize object state
	$self->{loaded} = 0;

	# Initialize WHERE statement before any update is made on the list
	$self->SetWhereStatement (keys (%{$self->{attributes}}));

	# Process skeleton
	if (exists ($self->{attributes}->{skeleton})) {
		my $status = 1;
		try {
			$self->LoadSkeleton ();
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, {module => 'Mioga2::InstanceList', step => __('Skeleton loading'), status => $status});
		};
	}

	print STDERR "[Mioga2::InstanceList::new] Leaving\n" if ($debug);
	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 Set

Set an attribute value.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to set.

=item I<$value:> the attribute value to set.

=back

=back

=cut

#===============================================================================
sub Set {
	my ($self, $attribute, $value) = @_;

	# Initialize target_url from default redirection data
	if (($attribute eq 'homepage') && ($value ne '')) {
		# Ensure internal config is a Mioga2::MiogaConf
		if (ref ($self->{config}) eq 'Mioga2::Config') {
			$self->{config} = $self->{config}->GetMiogaConf ();
		}
		$self->Set ('target_url', $value);
	}
	elsif (($attribute eq 'default_group_id') && ($value ne '')) {
		# Ensure internal config is a Mioga2::MiogaConf
		if (ref ($self->{config}) eq 'Mioga2::Config') {
			$self->{config} = $self->{config}->GetMiogaConf ();
		}
		my $config = Mioga2::Config->new ($self->{config}, $self->Get ('ident'));
		my $group = Mioga2::GroupList->new ($config, { attributes => { rowid => $value } });
		my $app = Mioga2::ApplicationList->new ($config, { attributes => { rowid => $group->Get ('default_app_id') } });
		$self->Set ('target_url', '/bin/' . $group->Get ('ident') . '/' . $app->Get ('ident') . '/DisplayMain');
	}

	$self->{update}->{$attribute} = $value;
}

#===============================================================================

=head2 Get

Get an attribute value.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get.

=back

=item B<return:> the attribute value.

=back

=cut

#===============================================================================
sub Get {
	my ($self, $attribute) = @_;
	print STDERR "[Mioga2::InstanceList::Get] Entering, attribute name is $attribute\n" if ($debug);

	my $value = undef;

	#-------------------------------------------------------------------------------
	# Load the object from DB if attribute is not known already
	#-------------------------------------------------------------------------------
	if ((!grep /^$attribute$/, keys (%{$self->{attributes}})) && (!grep /^$attribute$/, keys (%{$self->{update}})) && !$self->{loaded}) {
		# The requested attribute is not set into object yet, get it from DB
		$self->Load ();
	}

	#-------------------------------------------------------------------------------
	# Retreive attribute value
	#-------------------------------------------------------------------------------
	if (exists ($self->{update}->{$attribute})) {
		# Attribute is in update list, get its value
		$value = $self->{update}->{$attribute};
	}
	elsif (exists ($self->{attributes}->{$attribute})) {
		# Attribute is in initial list, get its value
		$value = $self->{attributes}->{$attribute};
	}
	else {
		# Attribute is not in initial list, nor in update list, pick it from loaded instances
		if (scalar (@{$self->{instances}}) > 1) {
			# Multiple instances in the list, return an array of attribute values.
			$value = ();
			for (@{$self->{instances}}) {
				push (@$value, $_->{$attribute});
			}
		}
		else {
			# Only one instance in the list, return attribute value
			$value = $self->{instances}->[0]->{$attribute};
		}
	}

	# Add '%' signs according to match attribute
	if (exists ($self->{match}) && (ref ($value) eq '')) {
		if ($self->{match} eq 'begins') {
			$value = "$value%";
		}
		elsif ($self->{match} eq 'contains') {
			$value = "%$value%";
		}
		elsif ($self->{match} eq 'ends') {
			$value = "%$value";
		}
		else {
			throw Mioga2::Exception::Simple ('Mioga2::InstanceList::RequestForAttribute', "Unknown match type: $self->{match}");
		}
	}

	if (ref ($value) eq 'ARRAY') {
		print STDERR "[Mioga2::InstanceList::Get] Leaving, attribute value is " . Dumper $value if ($debug);
	}
	elsif (ref ($value) eq 'Mioga2::ApplicationList') {
		my @rowids = $value->GetRowIds ();
		print STDERR "[Mioga2::InstanceList::Get] Leaving, attribute value is truncated, Mioga2::ApplicationList rowids " . Dumper \@rowids if ($debug);
	}
	else {
		print STDERR "[Mioga2::InstanceList::Get] Leaving, attribute value is " . ($value ? $value : 'undef') . "\n" if ($debug);
	}
	return ($value);
}	# ----------  end of subroutine Set  ----------


#===============================================================================

=head2 Store

Store changes to database. This method has to be called to actually create or
update one or more instances. To delete instances, see the "Delete" method.

=cut

#===============================================================================
sub Store {
	my ($self) = @_;
	print STDERR "[Mioga2::InstanceList::Store] Entering\n" if ($debug);

	my $where = $self->GetWhereStatement ();

	if (!$self->Count ()) {
		#-------------------------------------------------------------------------------
		# Create an instance into DB
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::InstanceList::Store] Creating new instance\n" if ($debug);

		my $inst_count = SelectSingle ($self->{config}->GetDBH (), 'SELECT count(*) FROM m_mioga;')->{count};

		$self->ApplySkeleton (qw/attributes/);

		# Build SQL INSERT request according to what we know
		my $db_values = $self->GetDatabaseValues ();
		my (@keys, @values);
		map { my $str = (defined ($db_values->{$_})) ? "'$db_values->{$_}'" : 'NULL'; push (@values, "$str") } keys (%{$db_values});
		my $fields = join (', ', keys (%{$db_values}));
		my $values = join (', ', @values);
		my $sql = "INSERT INTO m_mioga (created, modified, $fields) VALUES (now(), now(), $values);";
		my $status = 1;
		try {
			ExecSQL ($self->{config}->GetDBH (), $sql);
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw ($err) if ($err);
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => __x('Instance "{ident}" creation', ident => $self->Get ('ident')), status => $status });
		};
		
		# Get newly-created instance config
		if (ref ($self->{config}) ne 'Mioga2::Config') {
			$self->{config} = new Mioga2::Config ($self->{config}, $self->Get ('ident'));
		}

		ExecSQL ($self->{config}->GetDBH (), "INSERT INTO m_ldap_lock_table VALUES(?, '1970-01-01')", [$self->{config}->GetMiogaId ()]);

		# Load Mioga2 config
		my $conf = new MiogaConf(config => $self->{config}->{miogaconf}->{install_dir}.'/conf/Config.xml');

		# Create applications as first instance is created
		if (!$inst_count) {
			$conf->UpgradeAppDesc($self->{config});
		}

		# Handle applications
		try {
			$self->ApplySkeleton (qw/applications/);
			if ($self->Has ('applications')) {
				$self->EnableApplications ($self->Get ('applications'));
			}
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => __('Applications activation'), status => $status });
		};

		# Apply what's remaining in skeleton
		$self->ApplySkeleton (qw/users teams resources groups filesystem themes/);

		# Special handling for default_theme_id
		$self->Set ('default_theme_id', SelectSingle ($self->{config}->GetDBH (), "SELECT m_theme.rowid FROM m_theme, m_mioga, " . $self->GetAdditionnalTables () . " WHERE m_theme.mioga_id = m_mioga.rowid AND m_theme.name = '$self->{attributes}->{skeleton}->{contents}->{themes}->{default}' AND $where;")->{rowid});
		$self->Store ();

		$conf->RunHooks($self->{config}->{miogaconf});

		# Run crawl.sh to initialize search engine database
		my $crawlcmd = $self->{config}->{miogaconf}->GetMiogaPrefix () . "/bin/mioga2_index.pl --conf=" . $self->{config}->GetMiogaConfPath () . ' --search_conf=' . $self->{config}->GetInstallPath . "/conf/search_conf.xml" . ' ' . $self->Get ('ident');
		system ("$crawlcmd");
	}
	elsif (scalar (keys (%{$self->{update}}))) {
		#-------------------------------------------------------------------------------
		# Update instance
		#-------------------------------------------------------------------------------
		print STDERR "[Mioga2::InstanceList::Store] Updating existing instance." . Dumper $self->{update} if ($debug);
		my $sql = "UPDATE m_mioga SET ";
		for (keys (%{$self->{update}})) {
			next if (/^(admin_password|applications)$/);
			my $request = $self->StringRequestForAttribute ($_);
			$request =~ s/IS NULL$/ = NULL/;
			$sql .= "$request, " if ($request ne '');
		}
		$sql .= "modified = now ()";
		$sql .= " FROM " . $self->GetAdditionnalTables ();
		$sql .= " WHERE $where";
		print STDERR "[Mioga2::InstanceList::Store] Update SQL request: $sql\n" if ($debug);

		my $status = 1;
		try {
			ExecSQL ($self->{config}->GetDBH (), $sql);
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw ($err);
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => __x('Instance "{ident}" parameters definition', ident => $self->Get ('ident')), status => $status });
		};

		# Handle applications
		if ($self->Has ('applications')) {
			$self->EnableApplications ($self->Get ('applications'));
		}

		# Get current instance config
		if (ref ($self->{config}) ne 'Mioga2::Config') {
			$self->{config} = new Mioga2::Config ($self->{config}, $self->Get ('ident'));
		}

		#-------------------------------------------------------------------------------
		# Update admin password if required
		#-------------------------------------------------------------------------------
		if ($self->Has ('admin_password')) {
			my $admin = Mioga2::UserList->new ($self->{config}, { attributes => { rowid => $self->Get ('admin_id') } });
			$admin->Set ('password', $self->Get ('admin_password'));
			$admin->Store ();
		}
	}
	else {
		print STDERR "[Mioga2::InstanceList::Store] Nothing has to be done\n" if ($debug);
	}

	#-------------------------------------------------------------------------------
	# Update-pending values are now stored into DB, they can be moved to the inner
	# attributes of the object
	#-------------------------------------------------------------------------------
	for (keys (%{$self->{update}})) {
		$self->{attributes}->{$_} = delete ($self->{update}->{$_});
	}

	print STDERR "[Mioga2::InstanceList::Store] Leaving\n" if ($debug);
}	# ----------  end of subroutine Store  ----------


#===============================================================================

=head2 Count

Count the instances in the list. This method loads the instance list from DB if
required.

=over

=item B<return:> the number of instances in the list.

=back

=cut

#===============================================================================
sub Count {
	my ($self) = @_;
	print STDERR "[Mioga2::InstanceList::Count] Entering\n" if ($debug);

	$self->Load () unless $self->{loaded};

	my $count = scalar (@{$self->{instances}});

	print STDERR "[Mioga2::InstanceList::Count] Leaving: $count instances\n" if ($debug);
	return ($count);
}	# ----------  end of subroutine Count  ----------


#===============================================================================

=head2 GetInstances

Get list of instances in the list.

=over

=item B<return:> an array of hashes describing the instances in the list. Each hash
of this array can be used to create a new Mioga2::InstanceList that
only matches this instance.

=back

=cut

#===============================================================================
sub GetInstances {
	my ($self) = @_;
	print STDERR "[Mioga2::InstanceList::GetInstances] Entering\n" if ($debug);

	$self->Load () unless $self->{loaded};

	for my $instance (@{$self->{instances}}) {
		for (keys (%{$self->{update}})) {
			$instance->{$_} = $self->{update}->{$_};
		}
	}

	print STDERR "[Mioga2::InstanceList::GetInstances] Instances: " . Dumper $self->{instances} if ($debug);
	return (@{$self->{instances}});
}	# ----------  end of subroutine GetInstances  ----------


#===============================================================================

=head2 Delete

Delete instances.

=cut

#===============================================================================
sub Delete {
	my ($self) = @_;
	print STDERR "[Mioga2::InstanceList::Delete] Entering\n" if ($debug);

	my ($step, $status);

	if ($self->Count ()) {
		for ($self->GetInstances ()) {
			# Get Mioga2::Config object for current instance
			if (ref ($self->{config}) eq 'Mioga2::Config') {
				$self->{config} = $self->{config}->GetMiogaConf ();
			}
			my $config = new Mioga2::Config ($self->{config}, $_->{'ident'});

			$status = 1;
			$step = __('Resources deletion');
			try {
				# Delete resources
				my $resources = Mioga2::ResourceList->new ($config, { journal => $self->{journal} });
				$resources->Delete ();
			}
			otherwise {
				$status = 0;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => $step, status => $status });
			};

			# Delete groups
			$status = 1;
			$step = __('Groups deletion');
			try {
				ExecSQL($self->{config}->GetDBH (), "UPDATE m_mioga SET default_group_id = NULL WHERE rowid = " . $_->{rowid});
				my $groups = Mioga2::GroupList->new ($config, { journal => $self->{journal} });
				$groups->Delete ();
			}
			otherwise {
				$status = 0;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => $step, status => $status });
			};

			# Delete teams
			$status = 1;
			$step = __('Teams deletion');
			try {
				my $teams = Mioga2::TeamList->new ($config, { journal => $self->{journal} });
				$teams->Delete ();
			}
			otherwise {
				$status = 0;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => $step, status => $status });
			};

			# Delete non-admin users
			$status = 1;
			$step = __('Users deletion');
			try {
				ExecSQL($self->{config}->GetDBH (), "UPDATE m_mioga SET referent_id = NULL WHERE rowid = " . $_->{rowid});
				my $users = Mioga2::UserList->new ($config, { attributes => { rowid => $_->{admin_id} }, journal => $self->{journal} });
				$users->Revert ();
				$users->Delete ();
			}
			otherwise {
				$status = 0;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => $step, status => $status });
			};

			# Delete admin account
			$status = 1;
			$step = __('Admin user deletion');
			try {
				ExecSQL($self->{config}->GetDBH (), "UPDATE m_mioga SET admin_id = NULL WHERE rowid = " . $_->{rowid});
				my $users = Mioga2::UserList->new ($config, { journal => $self->{journal} });
				$users->Delete ();
			}
			otherwise {
				$status = 0;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => $step, status => $status });
			};

			# Misc deletions
			$status = 1;
			$step = __('Themes, external Mioga and task categories deletion');
			try {
				# Delete themes
				ExecSQL ($self->{config}->GetDBH (), "UPDATE m_mioga SET default_theme_id = NULL WHERE rowid = " . $_->{rowid});
				ExecSQL ($self->{config}->GetDBH (), "DELETE FROM m_theme WHERE mioga_id = " . $_->{rowid});

				# Delete associated entry into m_ldap_lock_table
				ExecSQL ($self->{config}->GetDBH (), "DELETE FROM m_ldap_lock_table WHERE mioga_id = $_->{rowid};");

				# Delete external Miogas
				ExecSQL ($self->{config}->GetDBH (), "DELETE FROM m_external_mioga WHERE mioga_id = $_->{rowid};");

				# Delete task categories
				ExecSQL ($self->{config}->GetDBH (), "DELETE FROM task_category WHERE mioga_id = $_->{rowid};");

				# Delete tags
				ExecSQL ($self->{config}->GetDBH (), "DELETE FROM m_tag USING m_tag_catalog WHERE m_tag.catalog_id = m_tag_catalog.rowid AND m_tag_catalog.mioga_id = $_->{rowid};");
				ExecSQL ($self->{config}->GetDBH (), "DELETE FROM m_tag_catalog WHERE mioga_id = $_->{rowid};");
			}
			otherwise {
				$status = 0;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => $step, status => $status });
			};

			# Remove applications from instance
			$status = 1;
			$step = __('Applications removal');
			try {
				ExecSQL ($self->{config}->GetDBH (), "DELETE FROM m_instance_application WHERE mioga_id = $_->{rowid};");
			}
			otherwise {
				$status = 0;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => $step, status => $status });
			};

			# Delete instance from m_mioga
			$status = 1;
			$step = __x('Instance "{ident}" deletion', ident => $_->{ident});
			try {
				ExecSQL ($self->{config}->GetDBH (), "DELETE FROM m_mioga WHERE rowid = $_->{rowid};");
			}
			otherwise {
				$status = 0;
			}
			finally {
				push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => $step, status => $status });
			};

			# Delete instance data
			$status = 1;
			if (!rmtree ($config->GetInstallDir ())) {
				# throw Mioga2::Exception::Simple ("Mioga2::InstanceList::Delete", "Can't remove instance data.");
				print STDERR "[Mioga2::InstanceList::Delete] Can't remove instance $_->{ident} data.\n";
				$status = 0;
			}
			push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => __x('Instance "{ident}" data removal', ident => $_->{ident}), status => $status });
		}
	}

	print STDERR "[Mioga2::InstanceList::Delete] Leaving\n" if ($debug);
}	# ----------  end of subroutine Delete  ----------


#===============================================================================

=head2 SetAdmin

Set administrator for instance.

=over

=item B<arguments:>

=over

=item I<$ident:> the ident of the user to set as instance admin.

=back

=back

=cut

#===============================================================================
sub SetAdmin {
	my ($self, $ident) = @_;

	my $where = $self->GetWhereStatement ();
	ExecSQL ($self->{config}->GetDBH (), "UPDATE m_mioga SET admin_id = m_user_base.rowid FROM m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_user_base.mioga_id = m_mioga.rowid AND m_user_base.ident = '$ident' AND $where;");
}	# ----------  end of subroutine SetAdmin  ----------


#===============================================================================

=head2 SetReferent

Set referent for instance.

=over

=item B<arguments:>

=over

=item I<$ident:> the ident of the user to set as instance referent.

=back

=back

=cut

#===============================================================================
sub SetReferent {
	my ($self, $ident) = @_;

	my $where = $self->GetWhereStatement ();
	ExecSQL ($self->{config}->GetDBH (), "UPDATE m_mioga SET referent_id = m_user_base.rowid FROM m_user_base, " . $self->GetAdditionnalTables () . " WHERE m_user_base.mioga_id = m_mioga.rowid AND m_user_base.ident = '$ident' AND $where;");
}	# ----------  end of subroutine SetReferent  ----------


#===============================================================================

=head2 GetApplicationList

Get list of applications the instances of the list can use.

=over

=item B<return:> a Mioga2::ApplicationList object.

=back

=cut

#===============================================================================
sub GetApplicationList {
	my ($self) = @_;

	my $applications = Mioga2::ApplicationList->new ($self->{config}, { attributes => { 'instance' => $self->Get ('ident') } });

	return ($applications);
}	# ----------  end of subroutine GetApplicationList  ----------


#===============================================================================

=head2 EnableApplications

Add records to m_instance_application so that instance can use applications

=cut

#===============================================================================
sub EnableApplications {
	my ($self, $applications) = @_;
	print STDERR "[Mioga2::InstanceList::EnableApplications] Entering. Applications: " . Dumper @$applications if ($debug);

	# Disable applications to remove for each user and group of the instances
	my @app_idents = map { $_->{ident} } @$applications;
	for my $instname ($self->Get ('ident')) {
		# Ensure internal config is a Mioga2::MiogaConf}
		my $config = $self->{config};
		if (ref ($config) eq 'Mioga2::Config') {
			$config = $config->GetMiogaConf ();
		}

		# Create instance Mioga2::Config
		my $instcfg = Mioga2::Config->new ($config, $instname);

		# Load list of applications to disable
		my $apps = Mioga2::ApplicationList->new ($instcfg, { attributes => { ident => \@app_idents } });
		$apps->Revert (1);

		# Set applications to an empty list of groups (ie disable applications for all groups of the instance)
		my $groups = Mioga2::GroupList->new ($instcfg, { attributes => { ident => '' } });
		$apps->SetGroups ($groups);

		# Set applications to an empty list of users (ie disable applications for all users of the instance)
		my $users = Mioga2::UserList->new ($instcfg, { attributes => { ident => '' } });
		$apps->SetUsers ($users);
	}

	my $sql;

	# Delete applications that are not in the list
	$sql = "DELETE FROM m_instance_application USING m_mioga, m_application, " . $self->GetAdditionnalTables () . " WHERE m_instance_application.mioga_id = m_mioga.rowid AND m_instance_application.application_id = m_application.rowid AND m_application.ident NOT IN (" . join (', ', map { $self->{config}->GetDBH ()->quote ($_->{ident}) } @$applications) . ") AND " . $self->GetWhereStatement ();
	ExecSQL ($self->{config}->GetDBH (), $sql);

	# Record applications from list
	for my $app (@$applications) {
		$sql = "INSERT INTO m_instance_application SELECT m_mioga.rowid AS mioga_id, m_application.rowid AS application_id, ";
		for (qw/all_groups all_users all_resources/) {
			my $val = (exists ($app->{$_})) ? 't' : 'f';
			$sql .= "'" . $val . "' AS $_, ";
		}
		$sql =~ s/, $/ /;
		$sql .= " FROM m_mioga, m_application, " . $self->GetAdditionnalTables () . " WHERE m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE mioga_id = m_mioga.rowid) AND m_application.ident = '" . st_FormatPostgreSQLString ($app->{ident}) . "' AND " . $self->GetWhereStatement ();
		ExecSQL ($self->{config}->GetDBH (), $sql);
	}

	# Ensure libraries can be accessed
	ExecSQL ($self->{config}->GetDBH (), "INSERT INTO m_instance_application SELECT m_mioga.rowid AS mioga_id, m_application.rowid AS application_id, 't' AS all_users, 't' AS all_groups, 't' AS all_resources FROM m_application, m_application_type, m_mioga WHERE m_application.type_id = m_application_type.rowid AND m_application_type.ident = 'library' AND m_application.rowid NOT IN (SELECT application_id FROM m_instance_application WHERE mioga_id = m_mioga.rowid);");

	print STDERR "[Mioga2::InstanceList::EnableApplications] Leaving.\n" if ($debug);
}	# ----------  end of subroutine EnableApplications  ----------


#===============================================================================

=head2 GetJournal

Get journal of operations as an object

=cut

#===============================================================================
sub GetJournal {
	my ($self) = @_;

	my $journal = tied (@{$self->{journal}});

	return ($journal);
}	# ----------  end of subroutine GetJournal  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


#===============================================================================

=head2 LoadSkeleton

Load provided skeleton file and store its contents as inner attributes of the object.

=cut

#===============================================================================
sub LoadSkeleton {
	my ($self) = @_;
	print STDERR "[Mioga2::InstanceList::LoadSkeleton] Entering\n" if ($debug);

	my $data = '';

	my $skel_file = delete $self->{attributes}->{skeleton};
	$self->{attributes}->{skeleton}->{file} = $skel_file;

	#-------------------------------------------------------------------------------
	# Read file
	#-------------------------------------------------------------------------------
	open(FILE, "< $self->{attributes}->{skeleton}->{file}") or throw Mioga2::Exception::User ('Mioga2::InstanceList::LoadSkeleton', "Can't open skeleton file '$self->{attributes}->{skeleton}->{file}': $!.");
	while (<FILE>) { $data .= $_ };
	close (FILE);

	#-------------------------------------------------------------------------------
	# Replace macros by corresponding value 
	#-------------------------------------------------------------------------------
	$data =~ s/\$\$INSTNAME\$\$/$self->{attributes}->{ident}/gsm;
	my $confdir = $self->{config}->GetInstallDir () . '/conf';
	$data =~ s/\$\$CONFDIR\$\$/$confdir/gsm;

	# Replace admin account macros
	my $admin_macros = $self->GetAdminMacros ();
	for my $attr (keys (%$admin_macros)) {
		$data =~ s/\$\$$attr\$\$/$admin_macros->{$attr}/gsm;
	}

	#-------------------------------------------------------------------------------
	# Parse XML
	#-------------------------------------------------------------------------------
	$data =~ s/<--!.*?-->//gsm; # remove comments

	my $xml		    = new Mioga2::XML::Simple(forcearray => 1);
	my $xmltree     = $xml->XMLin($data);
	my $skel_name   = (exists($xmltree->{'name'}))? $xmltree->{'name'} : "";
	my $skel_type   = (exists($xmltree->{'type'}))? $xmltree->{'type'} : "";
	my $admin_ident = (exists($xmltree->{'admin'}))? $xmltree->{'admin'} : "";

	if (lc ($skel_type) ne 'instance') {
		throw Mioga2::Exception::User ('Mioga2::InstanceList::LoadSkeleton', "Skeleton type '$skel_type' can not be applied to an instance");
	}

	#-------------------------------------------------------------------------------
	# Translate XML tree
	#-------------------------------------------------------------------------------
	my %contents = ();

	# Attributes
	for my $attr (qw/general limits system security ldap i18n/) {
		for (keys (%{$xmltree->{attributes}->[0]->{$attr}->[0]})) {
			if ((ref ($xmltree->{attributes}->[0]->{$attr}->[0]->{$_}->[0]) eq 'HASH') && !scalar (keys (%{$xmltree->{attributes}->[0]->{$attr}->[0]->{$_}->[0]}))) {
				$contents{attributes}{$attr}{$_} = '';
			}
			else {
				$contents{attributes}{$attr}{$_} = $xmltree->{attributes}->[0]->{$attr}->[0]->{$_}->[0];
			}
		}
	}

	# Applications
	$contents{applications} = $xmltree->{applications}->[0]->{application};
	for my $field (qw/all_groups all_users all_resources/) {
		map {$_->{$field} = 1 if (exists ($_->{$field}))} @{$contents{applications}};
	}

	# Teams
	for (@{$xmltree->{teams}->[0]->{team}}) {
		push (@{$contents{teams}}, { ident => $_->{ident}, description => $_->{description}->[0]});
	}

	# Groups
	$contents{groups} = $xmltree->{groups}->[0];
	for my $field (qw/skeleton default_url animator/) {
		map {$_->{$field} = $_->{$field}->[0] if (exists ($_->{$field}))} @{$contents{groups}{group}};
	}

	# Users
	$contents{users} = $xmltree->{users}->[0];
	for my $field (qw/firstname lastname email password type skeleton/) {
		map {$_->{$field} = $_->{$field}->[0] if (exists ($_->{$field}))} @{$contents{users}{user}};
	}

	# Resources
	$contents{resources} = $xmltree->{resources}->[0];
	for my $field (qw/location animator skeleton/) {
		map {$_->{$field} = $_->{$field}->[0] if (exists ($_->{$field}))} @{$contents{resources}{resource}};
	}

	# Filesystem
	$contents{filesystem} = $xmltree->{filesystem}->[0];

	# Themes
	$contents{themes} = $xmltree->{themes}->[0];

	#-------------------------------------------------------------------------------
	# Store admin ident
	#-------------------------------------------------------------------------------
	$self->{attributes}->{skeleton}->{admin} = $admin_ident;

	#-------------------------------------------------------------------------------
	# Store skeleton definition into object
	#-------------------------------------------------------------------------------
	$self->{attributes}->{skeleton}->{name} = $skel_name;
	$self->{attributes}->{skeleton}->{contents} = \%contents;

	print STDERR "[Mioga2::InstanceList::LoadSkeleton] Leaving\n" if ($debug);
}	# ----------  end of subroutine LoadSkeleton  ----------


#===============================================================================

=head2 Load

Load object attributes from DB. This method is automatically called when trying
to access an attribute whose value has not already been loaded from DB.

=cut

#===============================================================================
sub Load {
	my ($self) = @_;
	print STDERR "[Mioga2::InstanceList::Load] Entering\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Initialize and run SQL query
	#-------------------------------------------------------------------------------
	my $sql = 'SELECT m_mioga.*, m_lang.ident AS lang FROM m_mioga, ' . $self->GetAdditionnalTables () . ' WHERE ' . $self->GetWhereStatement ();
	my $res = SelectMultiple ($self->{config}->GetDBH (), $sql);

	#-------------------------------------------------------------------------------
	# Drop undefined values
	#-------------------------------------------------------------------------------
	for my $instance (@$res) {
		for (keys (%$instance)) {
			delete ($instance->{$_}) if (!defined ($instance->{$_}));
		}
	}

	#-------------------------------------------------------------------------------
	# Update object contents
	#-------------------------------------------------------------------------------
	$self->{instances} = $res;

	# Raise "loaded" flag if query returned something
	$self->{loaded} = 1 if (scalar (@{$self->{instances}}));

	print STDERR "[Mioga2::InstanceList::Load] Leaving\n" if ($debug);
}	# ----------  end of subroutine Load  ----------


#===============================================================================

=head2 StringRequestForAttribute

Format RequestForAttribute returned array as a string to paste as-is into a
select or update request.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get associated SQL request.

=back

=item B<return:> a string to update attribute value or to select from attribute
value.

=back

=cut

#===============================================================================
sub StringRequestForAttribute {
	my ($self, $argument) = @_;

	my $request = '';

	my $ret = $self->RequestForAttribute ($argument);

	unless (defined ($ret->[2])) {
		$ret->[1] = 'IS';
		$ret->[2] = 'NULL';
	}

	$request = "$ret->[0] $ret->[1] $ret->[2]" if ($ret->[0] ne '');

	return ($request);
}	# ----------  end of subroutine StringRequestForAttribute  ----------


#===============================================================================

=head2 RequestForAttribute

Generate SQL statement that matches attribute into m_mioga.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to get associated SQL request.

=back

=item B<return:> an array containing an attribute name and the statement to
match this attribute in m_mioga table. If the attribute is stored as-is into
table, this method returns the attribute and its value. If the attribute is
stores as the ID of a record in another table, this method returns the
attribute name and a SQL subquery picking the attribute ID from this other
table. If the attribute has to be ignored, this method returns empty strings.

=back

=cut

#===============================================================================
sub RequestForAttribute {
	my ($self, $attribute) = @_;

	my $value = '';
	my $operator = '=';
	my $attrval = '';

	# Change operator if a match type is specified
	if (exists ($self->{match})) {
		$operator = 'ILIKE';
	}

	#-------------------------------------------------------------------------------
	# Format string so that attribute value can be a scalar or an array
	#-------------------------------------------------------------------------------
	if (ref ($self->Get ($attribute)) eq 'ARRAY') {
		$operator = 'IN';
		for (@{$self->Get ($attribute)}) {
			if (grep (/^$attribute$/, @text_fields)) {
				$attrval = "(" . join (', ', map { $self->{config}->GetDBH()->quote ($_) } @{$self->Get ($attribute)}) . ")";
			}
			else {
				$attrval = "(" . join (', ', @{$self->Get ($attribute)}) . ")";
			}
		}
		if ($attrval eq '') {
			$attrval = '(NULL)';
		}
	}
	elsif (ref ($self->Get ($attribute)) eq 'Mioga2::ApplicationList') {
		$operator = 'IN';
		$attrval = "(SELECT m_instance_application.mioga_id FROM m_application, m_instance_application WHERE m_application.rowid = m_instance_application.application_id AND m_application.ident IN (" . join (', ', map { $self->{config}->GetDBH()->quote ($_->{ident}) } $self->Get ($attribute)->GetApplications ()) . "))";
	}
	else {
		if (grep (/^$attribute$/, @text_fields)) {
			$attrval = $self->{config}->GetDBH ()->quote ($self->Get ($attribute));
		}
		elsif (grep (/^$attribute$/, @boolean_fields)) {
			$attrval = ($self->Get ($attribute)) ? "'t'" : "'f'";
		}
		elsif (grep (/^$attribute$/, @numeric_fields)) {
			if (defined ($self->Get ($attribute)) && $self->Get ($attribute) ne '') {
				$attrval = $self->Get ($attribute);
			}
			else {
				$attrval = 'NULL';
			}
		}
		else {
			if ($self->Get ($attribute)) {
				$attrval = $self->Get ($attribute);
			}
			else {
				$attrval = 'NULL';
			}
		}
	}

	# Change operator to 'IS' to match 'NULL'
	if ($attrval eq 'NULL') {
		$operator = 'IS';
	}

	#-------------------------------------------------------------------------------
	# Process attribute name
	#-------------------------------------------------------------------------------
	if (grep (/^$attribute$/, qw/skeleton default_lang/)) {
		# Skeleton attribute is ignored as it doesn't match anything in DB
		$attribute = '';
		$value = '';
	}
	elsif ($attribute eq 'applications') {
		$attribute = 'rowid';
		$value = $attrval;
	}
	elsif (grep (/^$attribute$/, qw/lang/)) {
		# Some attributes need their ID to be picked from another table
		$value = "(SELECT rowid FROM m_$attribute WHERE ident $operator $attrval)";
		$attribute = 'default_lang_id';
	}
	elsif ($attribute eq 'referent') {
		$value = "(SELECT rowid FROM m_user_base WHERE firstname " . $operator . " " . $self->{config}->GetDBH()->quote ($self->Get ($attribute)) . " OR lastname " . $operator . " " . $self->{config}->GetDBH()->quote ($self->Get ($attribute)) . ")";
		$operator = 'IN';
		$attribute = 'referent_id';
	}
	else {
		# Other attributes are simply associated to their text value
		$value = $attrval if ($self->Has ($attribute));
	}

	my @ret = ($attribute, $operator, $value);

	return (\@ret);
}	# ----------  end of subroutine RequestForAttribute  ----------


#===============================================================================

=head2 Has

Check if an instance list has a given attribute set.

=over

=item B<arguments:>

=over

=item I<$attribute:> the attribute name to check.

=back

=item B<return:> 1 or 0 whether the object has or not a given attribute in its
current state.

=back

B<Warning:> This method doesn't load the list from DB so it is possible that
the object does not have an attribute that the instances of the list will have
once they have been loaded.

=cut

#===============================================================================
sub Has {
	my ($self, $attribute) = @_;

	return (exists ($self->{update}->{$attribute}) || exists ($self->{attributes}->{$attribute}));
}	# ----------  end of subroutine Has  ----------


#===============================================================================

=head2 ApplySkeleton

Apply skeleton to list of instances. This method is automatically called by the
"Store" method.

=cut

#===============================================================================
sub ApplySkeleton {
	my ($self, @parts) = @_;
	print STDERR "[Mioga2::InstanceList::ApplySkeleton] Entering\n" if ($debug);

	my $where = $self->GetWhereStatement ();

	#-------------------------------------------------------------------------------
	# Check what has to be applied
	#-------------------------------------------------------------------------------
	my @all_parts = qw/attributes applications users teams resources groups filesystem themes/;

	if (!scalar (@parts)) {
		print STDERR "[Mioga2::InstanceList::ApplySkeleton] List of parts to apply is empty, applying all parts\n" if ($debug);
		@parts = @all_parts;
	}

	my %apply = ();
	for (@parts) {
		$apply{$_} = 1;
	}

	#-------------------------------------------------------------------------------
	# Apply attributes
	#-------------------------------------------------------------------------------
	if ($apply{attributes}) {
		print STDERR "[Mioga2::InstanceList::ApplySkeleton] Applying attributes\n" if ($debug);
		for my $node (keys (%{$self->{attributes}->{skeleton}->{contents}->{attributes}})) {
			for my $key (keys (%{$self->{attributes}->{skeleton}->{contents}->{attributes}->{$node}})) {
				for ($node) {
					if (/ldap/) {
						# LDAP attributes are prefixed with 'ldap_' in DB
						$self->Set ("ldap_$key", $self->{attributes}->{skeleton}->{contents}->{attributes}->{$node}->{$key}) unless (defined ($self->{attributes}->{"ldap_$key"}) || (defined ($self->{attributes}->{use_ldap}) && $self->{attributes}->{use_ldap} == 0));
					}
					else {
						# Default behaviour is to store node value associated to node name
						$self->Set ($key, $self->{attributes}->{skeleton}->{contents}->{attributes}->{$node}->{$key}) unless (defined ($self->{attributes}->{$key}));
					}
				}
			}
		}

		# Default lang
		my $lang_id = SelectSingle ($self->{config}->GetDBH (), "SELECT rowid FROM m_lang WHERE locale = '$self->{attributes}->{skeleton}->{contents}->{attributes}->{i18n}->{default_lang}'")->{rowid};
		$self->Set ('default_lang_id', $lang_id);
	}

	#-------------------------------------------------------------------------------
	# Apply applications
	#-------------------------------------------------------------------------------
	if ($apply{applications}) {
		print STDERR "[Mioga2::InstanceList::ApplySkeleton] Applying applications\n" if ($debug);
		my $applications;
		for my $skelapp (@{$self->{attributes}->{skeleton}->{contents}->{applications}}) {
			push (@$applications, $skelapp);
		}
		$self->EnableApplications ($applications);
	}

	#-------------------------------------------------------------------------------
	# Apply filesystem
	#-------------------------------------------------------------------------------
	if ($apply{filesystem}) {
		print STDERR "[Mioga2::InstanceList::ApplySkeleton] Applying filesystem\n" if ($debug);
		my $status = 1;
		try {
			for ($self->GetInstances ()) {
				my $basedir = $self->{config}->GetInstallPath () . '/' . $_->{ident};
				#-------------------------------------------------------------------------------
				# Create directory
				#-------------------------------------------------------------------------------
				my $private = "$basedir/web/htdocs/home/";
				my $public = "$basedir/web/htdocs/public/";
				if ((! -d $private) || (! -d $public)) {
					if (!mkpath ($private, $public, { mode => 0770 })) {
						throw Mioga2::Exception::Simple("Mioga2::InstanceList::ApplySkeleton", __x("Cannot create dir: {directory}", directory => $private));
					}
				}

				#-------------------------------------------------------------------------------
				# Create system directories
				#-------------------------------------------------------------------------------
				my $confdir = $self->{config}->GetInstallPath () . '/conf';
				symlink ("$confdir/lang", "$basedir/" . LANGDIR) or throw Mioga2::Exception::Simple ("Mioga2::InstanceList::ApplySkeleton", "Can't create LANG directory: $!.\n");
				mkdir ("$basedir/" . THEMESDIR) or throw Mioga2::Exception::Simple ("Mioga2::InstanceList::ApplySkeleton", "Can't create themes directory: $!.");
				symlink ("$confdir/themes/default", "$basedir/" . THEMESDIR . '/default') or throw Mioga2::Exception::Simple ("Mioga2::InstanceList::ApplySkeleton", "Can't create default theme symlink: $!.\n");
				mkdir ("$basedir/" . XSLDIR);
				symlink ("$confdir/xsl", "$basedir/" . XSLDIR . '/default') or throw Mioga2::Exception::Simple ("Mioga2::InstanceList::ApplySkeleton", "Can't symlink XSL directory: $!.\n");
				# TODO Bug #1465, if group or user skeleton file is in original instance MiogaFiles/skel but not in $confdir/skel, instance creation fails.
				dircopy ("$confdir/skel", "$basedir/MiogaFiles/skel") or throw Mioga2::Exception::Simple ("Mioga2::InstanceList::ApplySkeleton", "Can't create skel directory: $!.\n");

				for my $type (qw/dir file link/) {
					for ((@{$self->{attributes}->{skeleton}->{contents}->{filesystem}->{$type}})) {
						my $name = "$basedir/$_->{name}";
						my $src = $_->{src};
						for ($type) {
							if (/dir/) {
								dircopy ($src, $name) or throw Mioga2::Exception::Simple ("[Mioga2::InstanceList::ApplySkeleton]", " Can't copy directory $src to $name: $!");
							}
							elsif (/file/) {
								print STDERR "[Mioga2::InstanceList::ApplySkeleton] File creation is not implemented yet.\n";
							}
							elsif (/link/) {
								symlink ($src, $name) or throw ("[Mioga2::InstanceList::ApplySkeleton]", "Can't symlink $name to $src: $!");
							}
							else {
								print STDERR "[Mioga2::InstanceList::ApplySkeleton] Unhandled filesystem type $type\n";
							}
						}
					}
				}
			}
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => __('Instance disk space creation'), status => $status });
		};
	}

	#-------------------------------------------------------------------------------
	# Apply themes
	#-------------------------------------------------------------------------------
	if ($apply{themes}) {
		print STDERR "[Mioga2::InstanceList::ApplySkeleton] Applying themes\n" if ($debug);
		my $status = 1;
		try {
			for (@{$self->{attributes}->{skeleton}->{contents}->{themes}->{theme}}) {
				my $sql = "INSERT INTO m_theme (created, modified, name, ident, mioga_id) SELECT now() AS created, now() AS modified, '$_->{name}' AS name, '$_->{ident}' AS ident, m_mioga.rowid AS mioga_id FROM m_mioga WHERE m_mioga.ident = '" . $self->Get ('ident') . "' AND m_mioga.rowid NOT IN (SELECT mioga_id FROM m_theme WHERE name = '$_->{name}' AND ident = '$_->{ident}');";
				ExecSQL ($self->{config}->GetDBH (), $sql);
			}
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => __('Themes creation'), status => $status });
		};
	}

	#-------------------------------------------------------------------------------
	# Apply teams
	#-------------------------------------------------------------------------------
	if ($apply{teams}) {
		print STDERR "[Mioga2::InstanceList::ApplySkeleton] Applying teams\n" if ($debug);
		my $status = 1;
		try {
			for (@{$self->{attributes}->{skeleton}->{contents}->{teams}}) {
				my $sql = "INSERT INTO m_group (created, modified, ident, mioga_id, type_id, theme_id, lang_id, description, status_id) SELECT now() AS created, now() AS modified, '$_->{ident}' AS ident, m_mioga.rowid AS mioga_id, m_group_type.rowid AS type_id, m_theme.rowid AS theme_id, " . $self->Get ('default_lang_id') . " AS lang_id, '$_->{description}' AS description, m_group_status.rowid AS status_id FROM m_mioga, " . $self->GetAdditionnalTables () . ", m_group_type, m_group_status, m_theme WHERE m_group_type.ident = 'team' AND m_group_status.ident = 'active' AND $where AND '$_->{ident}' NOT IN (SELECT m_group.ident FROM m_group, m_mioga, " . $self->GetAdditionnalTables () . " WHERE m_group.mioga_id = m_mioga.rowid AND $where) AND m_theme.name = '$self->{attributes}->{skeleton}->{contents}->{themes}->{default}' AND m_theme.mioga_id = m_mioga.rowid;";
				ExecSQL ($self->{config}->GetDBH (), $sql);
			}
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => __('Teams creation'), status => $status });
		};
	}

	#-------------------------------------------------------------------------------
	# Apply users
	#-------------------------------------------------------------------------------
	if ($apply{users}) {
		print STDERR "[Mioga2::InstanceList::ApplySkeleton] Applying users\n" if ($debug);
		my $status = 1;
		try {
			for ($self->GetInstances ()) {
				my $basedir = $self->{config}->GetInstallPath () . '/' . $_->{ident};
				my $lang = $_->{default_lang};
				#-------------------------------------------------------------------------------
				# Create users
				#-------------------------------------------------------------------------------
				for (@{$self->{attributes}->{skeleton}->{contents}->{users}->{user}}) {
					$_->{skeleton} = $self->{attributes}->{skeleton}->{contents}->{users}->{default_skel} unless (exists ($_->{skeleton}));
					$_->{skeleton} = $self->{config}->GetSkeletonDir () . "/$lang/$_->{skeleton}";
					my $user = Mioga2::UserList->new ($self->{config}, { attributes => $_, journal => $self->{journal} });
					$user->Store ();
				}

				#-------------------------------------------------------------------------------
				# Declare instance admin
				#-------------------------------------------------------------------------------
				$self->SetAdmin ($self->{attributes}->{skeleton}->{admin});

				#-------------------------------------------------------------------------------
				# Create admin private space to backup deleted users data
				#-------------------------------------------------------------------------------
				# Reload instance attributes from DB to get admin_id value
				$self->Load ();

				# Load admin user
				my $admin = Mioga2::UserList->new ($self->{config}, { attributes => { rowid => $self->Get ('admin_id') }, journal => $self->{journal} });

				# Create private directory
				$admin->CreateDirectory ('private');

				#-------------------------------------------------------------------------------
				# Update admin password if required
				#-------------------------------------------------------------------------------
				if ($self->Has ('admin_password')) {
					$admin->Set ('password', $self->Get ('admin_password'));
					$admin->Store ();
				}
			}
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => __('Users creation'), status => $status });
		};
	}

	#-------------------------------------------------------------------------------
	# Apply groups
	#-------------------------------------------------------------------------------
	if ($apply{groups}) {
		print STDERR "[Mioga2::InstanceList::ApplySkeleton] Applying groups\n" if ($debug);
		my $status = 1;
		try {
			for ($self->GetInstances ()) {
				my $basedir = $self->{config}->GetInstallPath () . '/' . $_->{ident};
				my $lang = $_->{default_lang};
				my $instcfg = new Mioga2::Config ($self->{config}->{miogaconf}, $_->{ident});
				#-------------------------------------------------------------------------------
				# Create groups
				#-------------------------------------------------------------------------------
				for my $attrs (@{$self->{attributes}->{skeleton}->{contents}->{groups}->{group}}) {
					$attrs->{skeleton} = $self->{attributes}->{skeleton}->{contents}->{groups}->{default_skel} unless (exists ($attrs->{skeleton}));
					$attrs->{skeleton} = $self->{config}->GetSkeletonDir () . "/$lang/$attrs->{skeleton}";
					my $group = Mioga2::GroupList->new ($instcfg, { attributes => $attrs, journal => $self->{journal} });
					$group->Store ();
					if ($self->{attributes}->{skeleton}->{contents}->{groups}->{default} && $self->{attributes}->{skeleton}->{contents}->{groups}->{default} eq $attrs->{ident}) {
						$self->Set ('default_group_id', $group->Get ('rowid'));
					}
				}
			}
		}
		otherwise {
			my $err = shift;
			$status = 0;
			throw $err;
		}
		finally {
			push (@{$self->{journal}}, { module => 'Mioga2::InstanceList', step => __('Groups creation'), status => $status });
		};
	}

	print STDERR "[Mioga2::InstanceList::ApplySkeleton] Leaving\n" if ($debug);
}	# ----------  end of subroutine ApplySkeleton  ----------


#===============================================================================

=head2 GetDatabaseValues

Get a hash of keys / values that fit into database table. This method
translated the attributes that can not fit as-is into DB.

=cut

#===============================================================================
sub GetDatabaseValues { # TODO (SNI - 23/04/2010 10:51): handle default_theme_id
	my ($self) = @_;
	print STDERR "[Mioga2::InstanceList::GetDatabaseValues] Entering\n" if ($debug);

	my %values = ();

	for (qw/ident admin_id holidays domain_name ldap_access ldap_host ldap_port ldap_bind_dn ldap_bind_pwd ldap_user_base_dn ldap_user_filter ldap_user_scope ldap_user_ident_attr ldap_user_firstname_attr ldap_user_lastname_attr ldap_user_email_attr ldap_user_pwd_attr ldap_user_desc_attr ldap_user_inst_attr max_allowed_group quota_soft quota_hard password_crypt_method auth_fail_timeout pwd_min_length pwd_min_letter pwd_min_digit pwd_min_special pwd_min_chcase use_secret_question doris_can_set_secret_question default_lang_id referent_id referent_comment comment default_group_id homepage target_url/) {
		next if (/^ldap_/ && !$self->Has ('ldap_host'));
		next if (/applications/);
		$values{$_} = $self->Get ($_);
		my $attribute = $_;
		if (exists ($values{$attribute}) && (defined ($values{$attribute})) && ($values{$attribute} eq '')) {
			if (grep (/^$attribute$/, @numeric_fields)) {
				$values{$attribute} = undef;
			}
			elsif (grep (/^$attribute$/, @boolean_fields)) {
				$values{$attribute} = 'f';
			}
		}
	}

	# Initialize some LDAP values
	$values{use_ldap} = $self->Has ('ldap_host') ? 1 : 0;

	print STDERR "[Mioga2::InstanceList::GetDatabaseValues] Values: " . Dumper \%values if ($debug);

	print STDERR "[Mioga2::InstanceList::GetDatabaseValues] Leaving\n" if ($debug);
	return (\%values);
}	# ----------  end of subroutine GetDatabaseValues  ----------


#===============================================================================

=head2 SetWhereStatement

Initialize a WHERE statement that matches team list in m_mioga according to
provided attributes.

=over

=item B<arguments:>

=over

=item I<@attributes:> a list of inner attributes to base the statement on.

=item B<return:> a string to paste as-is into a request, after the WHERE keyword.

=back

=back

=cut

#===============================================================================
sub SetWhereStatement {
	my ($self, @attributes) = @_;

	# Fetch attributes, if any
	for my $attr (@attributes) {
		$self->Get ($attr);
	}

	my $where = '';

	for my $attrname (@attributes) {
		next if (grep /^$attrname$/, qw/skeleton admin_password applications/);
		my $request = $self->StringRequestForAttribute ($attrname);
		$where .= "m_mioga.$request AND " unless ($request eq '');
	}

	$where .= "m_lang.rowid = m_mioga.default_lang_id";

	$self->{where} = ($where ne '') ? $where : '1=1';
	delete ($self->{match});

	print STDERR "[Mioga2::InstanceList::SetWhereStatement] $where\n" if ($debug);

	return ($self->{where});
}	# ----------  end of subroutine SetWhereStatement  ----------


#===============================================================================

=head2 GetWhereStatement

Generate a WHERE statement that matches instance list in m_mioga.

=over

=item B<return:> a string to paste as-is into a request, after the WHERE keyword.

=back

=cut

#===============================================================================
sub GetWhereStatement {
	my ($self) = @_;

	$self->SetWhereStatement () if (!$self->{where});

	return ($self->{where});
}	# ----------  end of subroutine GetWhereStatement  ----------


#===============================================================================

=head2 GetAdditionnalTables

Return list of additionnal tables to include into the FROM statement.

=cut

#===============================================================================
sub GetAdditionnalTables {
	my ($self) = @_;

	if (!$self->{additionnal_tables}) {
		$self->{additionnal_tables} = 'm_lang';
	}

	return ($self->{additionnal_tables});
}	# ----------  end of subroutine GetAdditionnalTables  ----------


#===============================================================================

=head2 GetAdminMacros

Return admin account macro translation table. The table is the result of a
merge between internal values (passed to constructor) and system values (from
Mioga2::MiogaConf).

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

A hash containing tranlations for ADMIN_IDENT, ADMIN_EMAIL, ADMIN_PASSWORD.

=back

=cut

#===============================================================================
sub GetAdminMacros {
	my ($self) = @_;

	# Get internal values
	my $macros = $self->{admin_macros} ? $self->{admin_macros} : { };

	# Merge with system values
	my $system = $self->{config}->GetAdminMacros ();
	for my $attr (keys (%$system)) {
		$macros->{$attr} = $system->{$attr} unless (defined ($macros->{$attr}));
	}

	return ($macros);
}	# ----------  end of subroutine GetAdminMacros  ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::UserList Mioga2::GroupList

=head1 COPYRIGHT

Copyright (C) 2003-2010, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;
