# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Authen.pm: Authentication Apache Handler

=head1 DESCRIPTION

This module is the Mioga Apache/mod_perl authentication handler. It : 

=over 4

=item 1 

Create Mioga2::Config object (parse Mioga.conf file)
	
=item 2

Create current user object from ident (login) given by the user

=item 3

Check the password

=item 4

Store Config and User object in pnote.

=back

=head1 APACHE CONFIGURATION

 <Location /foo>
    SetHandler  perl-script
    AuthType Basic
    AuthName Mioga2
    require valid-user
    PerlSetVar MiogaConfig /path/to/Mioga.conf
    PerlAuthenHandler Mioga2::Authen
    PerlAuthzHandler Mioga2::Authz
    PerlHandler Mioga2::Dispatch
 </Location>

The variable MiogaConfig must be set to the path of the Mioga.conf file.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Authen;
use strict;

use Mioga2::Apache;
use Error qw(:try);
use Data::Dumper;
use Mioga2::Old::User;
use Mioga2::URI;
use Mioga2::Config;
use Mioga2::MiogaConf;
use Mioga2::Exception::Crypto;
use Mioga2::Classes::URI;
use Benchmark::Timer;
use Encode::Detect::Detector;
use Mioga2::tools::database;
use Mioga2::GroupList;
use Mioga2::InstanceList;

my $debug = 0;

# ============================================================================

=head2 Authen::handler ($request)

	Apache/mod_perl handler main function. $request is a the Apache request 
	object.

	return an Apache::Constant constant like OK or AUTH_REQUIRED.
	
=cut

# ============================================================================

sub handler : method {
    my($class, $r) = @_;

	warn("======================================================================\n") if ($debug > 0);
	warn("==> Mioga2::Authen::handler uri = " . $r->uri . "\n") if ($debug > 0);

    my $t_uri  = Mioga2::Classes::URI->new(uri => $r->uri)->as_string;
	
	#-----------------------------------------------------------
	# Load Mioga Config
	#-----------------------------------------------------------
	my $config_file = $r->dir_config("MiogaConfig");

	if(!defined $config_file) {
		$r->log_error("==> Mioga2::Authen::handler : Can't find MiogaConfig parameter");
		$r->log_error("==> Mioga2::Authen::handler : Add \"PerlSetVar MiogaConfig /path/to/Mioga.conf\" in your apache configuration file");
		return Mioga2::Apache::HTTP_SERVER_ERROR;
	}

	my $miogaconf;
	my $config;
	my $uri_obj;
	my $auth_return = Mioga2::Apache::HTTP_OK;
	my $real_uri = $t_uri;
	$real_uri =~ s!/$!!;

	my $args = $r->args;

	try {
		$miogaconf = new Mioga2::MiogaConf($config_file);
		if ($r->pnotes ('redirect') == 1) {
			# We're about to redirect from instance or group, URI is not full (has no domain, application), so make Mioga2::URI more tolerant
			$uri_obj    = new Mioga2::URI($miogaconf, uri => $real_uri, missing_domain => 1);
		}
		else {
			$uri_obj    = new Mioga2::URI($miogaconf, uri => $real_uri);
		}
		$config = new Mioga2::Config($miogaconf, $uri_obj->GetMiogaIdent());
	}
	catch Mioga2::Exception::Simple with {
		my $err = shift;
		$r->log_error("==> Mioga2::Authen::handler : Exception Simple = " . $err->as_string());
		$auth_return = Mioga2::Apache::HTTP_NOT_FOUND;
	}
	otherwise {
		my $err = shift;
		$r->log_error("==> Mioga2::Authen::handler : Other exception ");
		$r->log_error(Dumper($err));
		$auth_return = Mioga2::Apache::HTTP_SERVER_ERROR;
	};

	if($auth_return != Mioga2::Apache::HTTP_OK) {
		return $auth_return;
	}

	my $authen_web = $r->pnotes->{authen_web};
	warn("==> Mioga2::Authen::handler AuthenWeb: $authen_web\n") if ($debug > 0);

	# The authenticated user object
	my $user;
	# Check for cookie even if not authen web in case of Webdav access and user already authenticated
	Mioga2::Login::Initialize ($config);
	my $login_uri;
	my %cookies = CGI::Cookie->fetch($r);
	my $cookie = $cookies{mioga_session};
	my $token = $cookie->value () if ($cookie);
	# if no token given, try to regenerate it from specific part. For example by CAS ticket
	if (!Mioga2::Login::IsValidToken($config, $token) ) {
		warn ("==> Token not valid  check for Token") if ($debug);
		my %args = map { /^([^=]*)=([^=]*)$/; $1 => $2 } split ('&', $args);
		$token = Mioga2::Login::CheckForToken($config, $uri_obj, \%args);
	}
	if (defined($token)) {
		warn ("==> Session token provided, trying to authenticate user against $token") if ($debug);
		$user = Mioga2::Login::GetUser ($config, $uri_obj, $token);
	if ($user) {
			Mioga2::Login::UpdateToken($miogaconf, $token);
		}
		else {
			$token = undef;
		}

	}







	# Variables for HTTP authentication
	my ($login, $password, $res);




	#-------------------------------------------------------------------------------
	# Check for login-special request
	#-------------------------------------------------------------------------------
	Mioga2::Login::CheckRequest ($config, $r);

	#-------------------------------------------------------------------------------
	# First check if a session token exists and if it is valid
	#-------------------------------------------------------------------------------
	#if (!$user) {
		#my %args = map { /^([^=]*)=([^=]*)$/; $1 => $2 } split ('&', $args);
		#warn ("==> Session token provided, trying to authenticate user against $token") if ($debug);
		#($user, $token) = Mioga2::Login::GetUser ($config, $uri_obj, $token, %args);
		#$token = Mioga2::Login::GetCookie ($miogaconf, $user)->value () if ($user);
		#Mioga2::Login::UpdateToken ($miogaconf, $token) if ($user);
	#}

	#-------------------------------------------------------------------------------
	# If no valid token was provided, check for HTTP-Basic authentication
	#-------------------------------------------------------------------------------
	if (!$user && $r->get_basic_auth_pw) {
		warn ("==> HTTP-Basic authentication provided, trying to authenticate user") if ($debug);
		# Extract the login
		$login = $r->user;

		# Decode the password
		($res, $password) = $r->get_basic_auth_pw;
		my $charset = detect($password) || 'iso-8859-15'; # defaults to latin9
		utf8::encode($password) unless ($charset eq 'UTF-8');

		# Load the user object
		try {
			$user = new Mioga2::Old::User($config, login => $login);
		}
		catch Mioga2::Exception::User with {
			# Nothing to be done here, will be processed later
		}
		catch Mioga2::Exception::Simple with {
			my $err = shift;
			$r->log_error("==> Mioga2::Authen::handler : Exception Simple = " . $err->as_string());
			$auth_return = Mioga2::Apache::HTTP_NOT_FOUND;
		}
		otherwise {
			my $err = shift;
			$r->log_error("==> Mioga2::Authen::handler : Other exception ");
			$r->log_error(Dumper($err));
			$auth_return = Mioga2::Apache::HTTP_SERVER_ERROR;
		};

		# Check password
		if ($user && !$user->CheckPassword($password)) {
			$user->RegisterPasswordFail ();
			$user = undef;
		}
		else {
			$r->pnotes("plain_password" => $password);
		}
	}

	#-------------------------------------------------------------------------------
	# Check if the user has been authenticated
	#-------------------------------------------------------------------------------
	if ($user) {
		# Update $login to match user ident (user can have authenticated with email which can be different from ident)
		warn ("==> User correctly authenticated") if ($debug);
		$login = $user->GetIdent ();
		$r->user ($user->GetEmail ());
		$auth_return = Mioga2::Apache::HTTP_OK;
	}
	else {
		warn ("==> User not authenticated") if ($debug);
		if (!$authen_web) {
			$r->note_basic_auth_failure;
			$auth_return = Mioga2::Apache::HTTP_AUTH_REQUIRED;
		}
		else {
			$login_uri = Mioga2::Login::GetLoginURI ($config->GetMiogaConf (), $uri_obj);
			warn (" login_uri = $login_uri") if ($debug);
			$r->headers_out->{Location} = $login_uri;
			$auth_return = Mioga2::Apache::HTTP_REDIRECT;
		}
	}

	#-------------------------------------------------------------------------------
	# Return if authentication failed
	#-------------------------------------------------------------------------------
	if($auth_return != Mioga2::Apache::HTTP_OK) {
		#-----------------------------------------------------------
		# Store config and user in pnotes
		#-----------------------------------------------------------
		$r->pnotes("config" => $config);
		$r->pnotes("uri_obj"  => $uri_obj);
		#$r->pnotes("user" => $user);
		if ($authen_web) {
			$r->pnotes ('authen_web' => 1);
			#$r->pnotes ('session_token' => $token);
		}
		return $auth_return;
	}

	#-------------------------------------------------------------------------------
	# Check if disabled_attacked user can be re-enabled
	#-------------------------------------------------------------------------------
	if (($user->GetStatus() eq 'disabled_attacked') && (($user->GetLastAuthFail() + $config->GetAuthFailTimeout ()) < time ())) {
		$r->warn("==> Mioga2::Authen::handler: User $login has been disabled on " . $user->GetLastAuthFail() . " because of possible brute-force attacks and can now be re-enabled.") if $debug > 0;
		$user->ResetPasswordFail ();
		Mioga2::tools::APIGroup::UserEnable ($config, $user->GetRowid ());
	}
	else {
		#-----------------------------------------------------------
		# Check if the user is active
		#-----------------------------------------------------------
		if($user->GetStatus() ne 'active') {
			$r->warn("==> Mioga2::Authen::handler: User $login was not active.") if $debug > 0;
			if (!$authen_web) {
				$r->note_basic_auth_failure;
				$auth_return = Mioga2::Apache::HTTP_AUTH_REQUIRED;
			}
			else {
				$r->headers_out->{Location} = $login_uri;
				$auth_return = Mioga2::Apache::HTTP_REDIRECT;
			}
		}
	}

	#-------------------------------------------------------------------------------
	# Reset password-fail count as user successfully logged-in
	#-------------------------------------------------------------------------------
	$user->ResetPasswordFail ();

	#-----------------------------------------------------------
	# Check and redirect for __MIOGA-USER__
	#-----------------------------------------------------------
	my $redir_uri = $t_uri;
	if($redir_uri =~ /__MIOGA-USER__/) {
		$r->warn("redirect") if $debug > 0;
		my $args = $r->args;

		$redir_uri =~ s/__MIOGA-USER__/$login/;
		$r->warn("redirect to $redir_uri") if $debug > 0;
		if($args) {
			$redir_uri .= "?".$args;
		}

		if ($authen_web) {
			$r->err_headers_out->add('Set-Cookie' => Mioga2::Login::GetCookie ($miogaconf, $user, $token));
		}
		$r->headers_out->{Location} = $redir_uri;
		$auth_return = Mioga2::Apache::HTTP_REDIRECT;
	}

	#-----------------------------------------------------------
	# Store config and user in pnotes
	#-----------------------------------------------------------
	$r->pnotes("config" => $config);
	$r->pnotes("uri_obj"  => $uri_obj);
	$r->pnotes("user" => $user);
	if ($authen_web) {
		$r->pnotes ('authen_web' => 1);
		$r->pnotes ('session_token' => $token);
	}

	return $auth_return;
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga.conf Mioga2

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
