#===============================================================================
#
#         FILE:  Database.pm
#
#  DESCRIPTION:  Object-oriented database handler.
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  22/11/2011 17:09
#===============================================================================
#
#  Copyright (c) year 2011, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME
	
Database.pm: Object-oriented database handler.

=head1 DESCRIPTION

	This object provides an object-oriented access to Mioga2 database.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Database;

use Data::Dumper;
use Error qw(:try);
use DBI;

my $debug = 0;


#===============================================================================

=head2 new

Connect to database

=cut

#===============================================================================
sub new {
	my ($class, %attributes) = @_;
	print STDERR "[Mioga2::Database] Entering, attributes: " . Dumper \%attributes if ($debug);

	my $self = { };
	bless($self, $class);

	for my $attr (qw/DBIlogin DBIpasswd DBIdriver DBname/) {
		if (!defined ($attributes{$attr})) {
			throw Mioga2::Exception::DB ("[Mioga2::Database::new]", "Cannot connect to database: " . $DBI::errstr);
		}
		$self->{database}->{$attr} = $attributes{$attr};
	}

	my $dbiUser = $self->{database}->{DBIlogin};
	my $dbiPassword = $self->{database}->{DBIpasswd};
	my $dbDriver = $self->{database}->{DBIdriver};
	my $dbName = $self->{database}->{DBname};

	my $datasource = "dbi:$dbDriver:dbname=$dbName";

	$self->{dbh} = DBI->connect($datasource, $dbiUser, $dbiPassword);

	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 GetDBH

Return database handler

=cut

#===============================================================================
sub GetDBH {
	my ($self) = @_;

	return ($self->{dbh});
}	# ----------  end of subroutine GetDBH  ----------


#===============================================================================

=head2 SelectSingle

Run an SQL request that returns just one entry.

=cut

#===============================================================================
sub SelectSingle {
	my ($self, $sql, $args) = @_;

	my $res = Mioga2::tools::database::SelectSingle ($self->{dbh}, $sql, $args);

	return ($res);
}	# ----------  end of subroutine SelectSingle  ----------


#===============================================================================

=head2 SelectMultiple

Run an SQL request that returns multiple entries.

=cut

#===============================================================================
sub SelectMultiple {
	my ($self, $sql, $args) = @_;

	my $res = Mioga2::tools::database::SelectMultiple ($self->{dbh}, $sql, $args);

	return ($res);
}	# ----------  end of subroutine SelectMultiple  ----------


#===============================================================================

=head2 ExecSQL

Run an SQL request that return no entry (INSERT, DELETE, ...).

=cut

#===============================================================================
sub ExecSQL {
	my ($self, $sql, $args) = @_;

	my $res = Mioga2::tools::database::ExecSQL ($self->{dbh}, $sql, $args);

	return ($res);
}	# ----------  end of subroutine ExecSQL  ----------


#===============================================================================

=head2 GetLastInsertId

Read the last insert id for given table and return it. Must be used on a
transaction, after an insert order, on table with a serial field.

=cut

#===============================================================================
sub GetLastInsertId {
	my ($self, $table) = @_;

	my $res = Mioga2::tools::database::GetLastInsertId ($self->{dbh}, $table);

	return ($res);
}	# ----------  end of subroutine GetLastInsertId  ----------


#===============================================================================

=head2 BeginTransaction

Start a SQL transaction.✗
MUST be ended by a EndTransaction (that commit the modification to database)
or a RollbackTransaction (that roll back modification since BeginTransaction).

=cut

#===============================================================================
sub BeginTransaction {
	my ($self) = @_;

	Mioga2::tools::database::BeginTransaction ($self->{dbh});
}	# ----------  end of subroutine BeginTransaction  ----------


#===============================================================================

=head2 EndTransaction

End a SQL transaction with a COMMIT.

=cut

#===============================================================================
sub EndTransaction {
	my ($self) = @_;

	Mioga2::tools::database::EndTransaction ($self->{dbh});
}	# ----------  end of subroutine EndTransaction  ----------


#===============================================================================

=head2 RollbackTransaction

End a SQL transaction with a ROLLBACK.

=cut

#===============================================================================
sub RollbackTransaction {
	my ($self) = @_;

	Mioga2::tools::database::RollbackTransaction ($self->{dbh});
}	# ----------  end of subroutine RollbackTransaction  ----------


#===============================================================================

=head2 Execute

Exectute a request but no output is returned. To be used with Fetch and FetchAll.

=cut

#===============================================================================
sub Execute {
	my ($self, $sql, $args) = @_;

	my $res;
	
	if(!defined $args) {
		$args = [];
	}

	# Check request for possible injection
	Mioga2::tools::database::CheckRequest ($sql);

	print STDERR "[Mioga2::Database::Execute] Request: $sql\n" if ($debug);
	print STDERR "[Mioga2::Database::Execute] Args: " . Dumper $args if ($debug);

	$self->{sth} = $self->{dbh}->prepare($sql);
	if (defined ($self->{sth})) {
		$res = $self->{sth}->execute(@$args);
		if (!defined ($res)) {
			throw Mioga2::Exception::DB ("[Mioga2::Database::Execute]", $DBI::err,  $DBI::errstr, $sql, $args);
		}
	}
	else {
		throw Mioga2::Exception::DB ("[Mioga2::Database::Execute]", $DBI::err,  $DBI::errstr, $sql, $args);
	}

	return ($res);
}	# ----------  end of subroutine Execute  ----------


#===============================================================================

=head2 Fetch

Fetch a single row of results

=cut

#===============================================================================
sub Fetch {
	my ($self) = @_;

	# Ensure request manager exists
	if (!$self->{sth}) {
		throw Mioga2::Exception::DB ("[Mioga2::Database::Fetch]", "Request manager is undefined");
	}

	# Fetch row
	my $row = $self->{sth}->fetchrow_hashref ();

	# Delete request manager if no more rows available
	if (!defined ($row)) {
		delete ($self->{sth});
	}

	return ($row);
}	# ----------  end of subroutine Fetch  ----------


#===============================================================================

=head2 Quote

Quote a string

=cut

#===============================================================================
sub Quote {
	my ($self, $str) = @_;

	return ($self->{dbh}->quote ($str));
}	# ----------  end of subroutine Quote  ----------


1;

