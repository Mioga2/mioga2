# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
    
RSS.pm: The Mioga2 RSS application.

=head1 METHODS DESCRIPTION

=cut

# ============================================================================

package Mioga2::RSS;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'rss';

use Error qw(:try);
use Mioga2::Content::XSLT;
use Mioga2::Content::ASIS;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::Classes::Time;
use XML::Atom::SimpleFeed;
use MIME::Entity;
use Data::Dumper;
use POSIX;
use File::Path;


my $debug = 0;

# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc
{
    my $self = shift;
    
    my %AppDesc = ( ident   => 'RSS',
                package => 'Mioga2::RSS',
                description => __('The Mioga2 RSS application'),
                type    => 'normal',
                all_groups => 0,
                all_users  => 1,
                can_be_public => 0,
				is_user             => 1,
				is_group            => 0,
				is_resource         => 0,

                functions => {
                    'RSS'   => __('Access to the RSS application'),
                },

                func_methods => {
                    'RSS'  => [ 'DisplayMain', 'Feed'],
                    },
                
                sxml_methods => { },
                
                xml_methods  => { },                

				non_sensitive_methods => [
					'DisplayMain',
					'Feed',
				]
                
                );
    
    return \%AppDesc;
}
            
# ============================================================================
# Public Methods
# ============================================================================

# ============================================================================

=head2 DisplayMain ()

=cut

# ============================================================================

sub DisplayMain {
    my ($self, $context)    = @_;
    my $config      = $context->GetConfig;
    my $dbh         = $config->GetDBH;
    my $user        = $context->GetUser;
    my $data        = { DisplayMain => {} };
    my $user_id     = $user->GetRowid;
    my $mioga_id    = $config->GetMiogaId;
    
    if ($context->GetAccessMethod =~ /post/i) {
        my @groups  = grep { $_ =~ /^group-\w+/ } keys %{$context->{args}};
        map { $_ =~ s/^group-// } @groups;
        my $files   = ref($context->{args}->{files}) eq 'ARRAY' ? $context->{args}->{files} : [$context->{args}->{files}];
        @$files     = grep { $_ ne undef } @$files;
        
        my $options         = SelectSingle($dbh, "SELECT * FROM rss_option WHERE user_id = $user_id");
        my $feed_per_group  = $context->{args}->{feed_per_group} == 1 ? 'true' : 'false';    
        if ($options) {
            ExecSQL($dbh, "UPDATE rss_option SET feed_per_group = '$feed_per_group' WHERE user_id = $user_id");
        }
        else {
            ExecSQL($dbh, "INSERT INTO rss_option(user_id,feed_per_group) VALUES($user_id,'$feed_per_group')");
        }
        
        ExecSQL($dbh, "DELETE FROM rss_subscription WHERE user_id = $user_id");
        if (@groups) {
            foreach my $group (@groups) {
                $context->{args}->{"group-$group"} = [$context->{args}->{"group-$group"}] if ref($context->{args}->{"group-$group"}) ne 'ARRAY';
                foreach my $app (@{$context->{args}->{"group-$group"}}) {
                    next if int($app) <= 0 || $group =~ /^\s*$/;
                    ExecSQL($dbh, "INSERT INTO rss_subscription(user_id,group_id,application_id) VALUES($user_id,(SELECT rowid FROM m_group_base WHERE ident = '" . st_FormatPostgreSQLString($group) . "' AND mioga_id = $mioga_id),$app)");
                }
            }
        }
        if (@$files) {
            my $res = SelectSingle($dbh, "SELECT m_application.rowid FROM m_application, m_instance_application WHERE m_application.rowid = m_instance_application.application_id AND m_application.ident = 'Magellan' AND m_instance_application.mioga_id=$mioga_id");
            my $magellan_id = $res->{rowid};
            foreach my $group (@$files) {
                ExecSQL($dbh, "INSERT INTO rss_subscription(user_id,group_id,application_id) VALUES($user_id,(SELECT rowid FROM m_group_base WHERE ident = '" . st_FormatPostgreSQLString($group) . "' AND mioga_id = $mioga_id),$magellan_id)");
            }
        }
        $data->{DisplayMain}->{Success} = 1;
        $self->NotifyUpdate($context, type => ['RSS'], group_id => 0);
    }
    %{$data->{DisplayMain}} = (%{$data->{DisplayMain}}, %{$context->GetContext});
    
    my $options         = SelectSingle($dbh, "SELECT * FROM rss_option WHERE user_id = $user_id");
    my $applications    = SelectMultiple($dbh, "SELECT m_application.ident, m_application.package, m_application.rowid FROM m_application, m_instance_application WHERE m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id");
    @$applications      = grep { eval("require $_->{package}; $_->{package}->HasRSS") } @$applications;
    
    my $group_ids   = $user->GetExpandedGroups->GetRowids;
    push @$group_ids, $user->GetRowid;
    my @app_ids     = map { $_->{rowid} } @$applications;
    my $results     = SelectMultiple($dbh, "SELECT a.package, a.rowid AS app_id, g.ident, g.rowid, (SELECT count(*) FROM rss_subscription WHERE user_id = $user_id AND group_id = g.rowid AND application_id = a.rowid) AS selected FROM m_application_group_allowed AS ag, m_application AS a, m_group_base AS g WHERE ag.group_id IN (" . join(',', @$group_ids) . ") AND ag.application_id IN (" . join(',', @app_ids) . ") AND g.rowid = ag.group_id AND a.rowid = ag.application_id");
    
    $data->{DisplayMain}->{FeedPerGroup} = 1 if $options->{feed_per_group} == 1;
    my $groups      = {};
    $data->{DisplayMain}->{File} = [];
    foreach my $result (@$results) {
        if ($result->{package}->GetAppDesc->{ident} eq 'Magellan') {
            push @{$data->{DisplayMain}->{File}}, {group => $result->{ident}, selected => $result->{selected}};
            next;
        }
        $groups->{$result->{ident}} ||= [];
        push @{$groups->{$result->{ident}}}, { ident => $result->{package}->GetAppDesc->{ident}, id => $result->{app_id}, name => $result->{package}->GetAppDesc->{name}, selected => $result->{selected} };
    }
    foreach my $key (keys %$groups) {
        $data->{DisplayMain}->{Group} ||= [];
        push @{$data->{DisplayMain}->{Group}}, { ident => $key, Application => $groups->{$key} };
    }
    
    my $content = Mioga2::Content::XSLT->new(
        $context,
        stylesheet    => 'rss.xsl',
        locale_domain => "rss_xsl"
    );
    $content->SetContent(Mioga2::tools::Convert::PerlToXML ($data));
    return $content;
}

# ============================================================================

=head2 Feed ()

  Return a feed for current user.

=cut

# ============================================================================

sub Feed {
    my ($self, $context)  = @_;
    
    my $config  = $context->GetConfig;
    my $user    = $context->GetUser;
    my $group   = undef;
    
    if (int($context->{args}->{group}) != 0) {
        try {
            $group = Mioga2::Old::Group->new($config, rowid => $context->{args}->{group});
        }
        otherwise {
            $group = Mioga2::Old::User->new($config, rowid => $context->{args}->{group});
        };
    }
    my $cache_path  = $config->GetMiogaFilesDir . "/feeds/user-" . $user->GetRowid;
    my $cache_file  = $group ? "group-" . $group->GetRowid . ".atom" : "global.atom";
    $self->GenerateCache($context, $config, $user, $group, $cache_path, $cache_file) unless -f "$cache_path/$cache_file";
    
    my $content = Mioga2::Content::SendFile->new($context, disposition => 'inline', path => "$cache_path/$cache_file");
    return $content;
}

# ============================================================================

=head2 GenerateCache ()

  Generate cache for a feed.

=cut

# ============================================================================

sub GenerateCache {
    my ($self, $context, $config, $user, $group, $cache_path, $cache_file) = @_;
    my $dbh     = $config->GetDBH;
    my $user_id = $user->GetRowid;
    my $feed;
    
    if ($group) {
        $feed       = XML::Atom::SimpleFeed->new(
            title   => __x("{group} group feed", group => $group->GetIdent),
            link    => { href => $config->GetProtocol . "://" . $config->GetServerName . $context->{uri}->GetURI, rel => 'self' },
            link    => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $user->GetIdent . "/Workspace/DisplayMain",
            id      => "data:,rss:user:".$user->GetIdent.":group:".$group->GetIdent,
        );
    }
    else {
        $feed   = XML::Atom::SimpleFeed->new(
            title   => __("Global feed"),
            link    => { href => $config->GetProtocol . "://" . $config->GetServerName . $context->{uri}->GetURI, rel => 'self' },
            link    => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $user->GetIdent . "/Workspace/DisplayMain",
            id      => "data:,rss:user:".$user->GetIdent.":global",
        );
    }
    my $group_sql   = "AND r.group_id = " . $group->GetRowid if $group;
    my $apps        = SelectMultiple($dbh, "SELECT g.ident, a.package FROM rss_subscription AS r, m_application AS a, m_group_base AS g WHERE r.user_id = $user_id AND g.rowid = r.group_id AND a.rowid = r.application_id $group_sql ORDER BY g.ident, a.package");
    foreach my $app (@$apps) {
        eval "require $app->{package}; $app->{package}->GetRSSFeed(\$context, '$app->{ident}', \$feed)";
    }
    mkpath $cache_path;
    my $file;
    open $file, "+>$cache_path/$cache_file";
    $feed->print($file);
}

# ============================================================================

=head2 RevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

    Remove user data in database when a user is revoked of a group.

    This function is called when :
    - a user is revoked of a group
    - a team is revoked of a group for each team member not namly
      invited in group
    - a user is revoked of a team  for each group where the team is
      member and the user not namly invited in group

=cut

# ============================================================================

sub RevokeUserFromGroup {
    my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;
    
    my $dbh	= $config->GetDBH;
    ExecSQL($dbh, "DELETE FROM rss_subscription WHERE user_id=$user_id AND group_id=$group_id");
    ExecSQL($dbh, "DELETE FROM rss_subscription WHERE user_id=$user_id") if $user_id == $group_id;
}

# ============================================================================

=head2 DeleteGroupData ($config, $group_id)

    Remove group data in database when a group is deleted.

    This function is called when a group/user/resource is deleted.

=cut

# ============================================================================

sub DeleteGroupData {
    my ($self, $config, $group_id) = @_;
    
    my $dbh = $config->GetDBH();

    ExecSQL($dbh, "DELETE FROM rss_option WHERE user_id=$group_id");
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2009, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
