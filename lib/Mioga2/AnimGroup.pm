# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
	
AnimGroup.pm: The Mioga2 Group animation application.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
package Mioga2::AnimGroup;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'animgroup';

use Error qw(:try);
use Data::Dumper;
use Mioga2::Application;
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::LargeList::ApplicationList;
use Mioga2::SimpleLargeList;
use Mioga2::AppDescList;
use Mioga2::tools::database;
use Mioga2::Old::Group;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIProfile;
use Mioga2::tools::APIApplication;
use Mioga2::Exception::Simple;
use Mioga2::tools::APIAuthz;
use Mioga2::GroupList;
use Mioga2::tools::Convert;
use Mioga2::Constants;

my $debug = 0;

# ============================================================================
# Users list description
# ============================================================================

my %users_sll_desc = (
    'name' => 'anim_users_sll',

    'sql_request' => {
        'select' => "m_user_base.*, m_user_status.ident as status, m_profile.ident AS profile, " . " COALESCE((SELECT last_connection FROM m_group_user_last_conn " . "  WHERE group_id = m_group_invited_user.group_id AND " . "        user_id = m_user_base.rowid), '1970-01-01 00:00:00') AS last_connection",

        "select_count" => "count(*)",

        "from" => "m_user_base, m_user_status, m_group_invited_user, m_profile, m_profile_group",

        "where" => "m_group_invited_user.group_id = ? AND " . "m_user_base.rowid = m_group_invited_user.invited_group_id AND " . "m_profile.group_id = m_group_invited_user.group_id AND " . "m_profile_group.profile_id = m_profile.rowid AND " . "m_profile_group.group_id = m_user_base.rowid AND " . "m_user_status.rowid = m_user_base.status_id",
    },

    'fields' => [ [ 'rowid', 'rowid' ], [ 'ident', 'link', 'RightsForUser' ], [ 'firstname', 'normal' ], [ 'lastname', 'normal' ], [ 'email', 'email' ], [ 'status', 'normal' ], [ 'profile', 'normal' ], [ 'change_profile', 'checkboxes', '$context->GetGroup()->GetAnimId() != $_->{rowid}' ], [ 'last_connection', 'date' ], [ 'delete', 'delete', '$context->GetGroup()->GetAnimId() != $_->{rowid}', \&Mioga2::AnimGroup::DisinviteUserHook ], ],

    'default_sort_field' => 'lastname',

    'search' => [ 'ident', 'firstname', 'lastname', 'profile', 'status' ],

);

# ============================================================================
# Groups list description
# ============================================================================

my %teams_sll_desc = (
    'name' => 'anim_teams_sll',

    'sql_request' => {
        'select' => "m_group.*, m_profile.ident AS profile",

        "select_count" => "count(*)",

        "from" => "m_group, m_group_invited_group, m_group_type, m_profile, m_profile_group",

        "where" => "m_group_invited_group.group_id = ? AND " . "m_group_type.rowid = m_group.type_id AND " . "m_group_type.ident = 'team' AND " . "m_group.rowid = m_group_invited_group.invited_group_id AND " . "m_profile.group_id = m_group_invited_group.group_id AND " . "m_profile_group.profile_id = m_profile.rowid AND " . "m_profile_group.group_id = m_group.rowid",

    },

    'fields' => [ [ 'rowid', 'rowid' ], [ 'ident', 'normal' ], [ 'profile', 'normal' ], [ 'change_profile', 'checkboxes' ], [ 'delete', 'delete', undef, \&Mioga2::AnimGroup::DisinviteTeamHook ], ],

    'search' => [ 'ident', 'profile' ],

);

# ============================================================================
# Resources list description
# ============================================================================

my %resources_sll_desc = (
    'name' => 'anim_resources_sll',

    'sql_request' => {
        'select' => "m_resource.*, m_resource_status.ident AS status",

        "select_count" => "count(*)",

        "from" => "m_resource, m_resource_status, m_group_group",

        "where" => "m_resource_status.rowid = m_resource.status_id AND " . "m_resource.rowid = m_group_group.group_id AND " . "m_group_group.invited_group_id = ?",

    },

    'fields' => [ [ 'rowid', 'rowid' ], [ 'ident', 'normal' ], [ 'location', 'normal' ], [ 'anim_ident', 'user_ident', 'm_resource.anim_id' ], [ 'status', 'normal' ], [ 'delete', 'delete', '$_->{status} ne ' . "'deleted'", \&Mioga2::AnimGroup::DeleteResourceHook ], ],

    'search' => [ 'ident', 'location', 'anim_ident', 'status' ],

);

# ============================================================================
# Applications list description
# ============================================================================

my %applications_sll_desc = (
    'name' => 'anim_applications_sll',

    'sql_request' => {
        'select' => "m_application_group_status.*",

        "select_count" => "count(*)",

        "from" => "m_application_group_status, m_application_type",

        "where" => "m_application_group_status.mioga_id = ? AND m_application_group_status.group_id = ? AND " . "m_application_type.rowid = m_application_group_status.type_id AND " . "m_application_type.ident != 'resource'",
    },

    'fields' => [ [ 'rowid', 'rowid' ], [ 'ident', 'normal' ], [ 'realident', 'hidden' ], [ 'change_app_status', 'checkboxes', '(! ($context->GetGroup()->GetRowid() == $config->GetAdminId() and $_->{realident} eq "Admin")) and $_->{realident} !~ /^Anim/' ], [ 'is_public', 'link', 'ChangeApplicationPublic', '$_->{can_be_public} and $_->{status} == 1' ], ],

);

# ============================================================================
# Profiles list description
# ============================================================================

my %profiles_sll_desc = (
    'name' => 'anim_profiles_sll',

    'sql_request' => {
        'select' => "m_profile.*, (SELECT count(*) FROM m_profile_group WHERE profile_id = m_profile.rowid) AS nb_member",

        "select_count" => "count(*)",

        "from" => "m_profile",

        "where" => "group_id = ?",
    },

    'fields' => [ [ 'rowid', 'rowid' ], [ 'ident', 'link', 'ModifyProfile' ], [ 'nb_member', 'link', 'DisplayProfileUsers' ], [ 'delete', 'delete', '$_->{nb_member} == 0', \&Mioga2::AnimGroup::DeleteProfileHook ], ],

);

# ============================================================================
# Profiles user member list description
# ============================================================================

my %profiles_users_sll_desc = (
    'name' => 'anim_profiles_users_sll',

    'sql_request' => {
        'select' => "m_user_base.*",

        "select_count" => "count(m_user_base.rowid)",

        "from" => "m_profile_group, m_user_base",

        "where" => "m_user_base.rowid = m_profile_group.group_id AND " . "m_profile_group.profile_id = ?",
    },

    'fields' => [ [ 'rowid', 'rowid' ], [ 'ident', 'normal' ], [ 'firstname', 'normal' ], [ 'lastname', 'normal' ], [ 'delete', 'delete', '$_->{rowid} != $context->GetGroup()->GetAnimId()', \&Mioga2::AnimGroup::DeleteProfileGroupHook ], ],

    'default_sort_field' => 'lastname',

    'search' => [ 'ident', 'firstname', 'lastname' ],
);

my %profiles_users_tiny_sll_desc = (
    'name' => 'anim_profiles_users_tiny_sll',

    'fields' => [ [ 'rowid', 'rowid' ], [ 'user_name', 'user_name', 'rowid' ], [ 'delete', 'delete', '$_->{rowid} != $context->GetGroup()->GetAnimId()' ], ],
);

# ============================================================================
# Profiles team member list description
# ============================================================================

my %profiles_teams_sll_desc = (
    'name' => 'anim_profiles_teams_sll',

    'sql_request' => {
        'select' => "m_group.*",

        "select_count" => "count(m_group.rowid)",

        "from" => "m_profile_group, m_group",

        "where" => "m_group.rowid = m_profile_group.group_id AND " . "m_profile_group.profile_id = ?",
    },

    'fields' => [ [ 'rowid', 'rowid' ], [ 'ident', 'normal' ], [ 'anim_ident', 'user_ident', 'm_group.anim_id' ], [ 'delete', 'delete', undef, \&Mioga2::AnimGroup::DeleteProfileGroupHook ], ],

    'search' => ['ident'],
);

my %profiles_teams_tiny_sll_desc = (
    'name' => 'anim_profiles_teams_tiny_sll',

    'fields' => [ [ 'rowid', 'rowid' ], [ 'ident', 'normal' ], [ 'delete', 'delete' ], ],
);

# ============================================================================
# Members where the user is invited.
# ============================================================================

my %member_list_sll_desc = (
    'name' => 'member_group_list_sll',

    'sql_request' => {
        'select' => "DISTINCT users.* AS rowid, (users.firstname || ' ' || users.lastname) AS name ",

        "select_count" => "count(*)",

        "from" => " ( SELECT DISTINCT m_user_base.* FROM m_group_group, m_user_base " . "    WHERE  m_group_group.group_id = ? AND " . "           m_user_base.rowid = m_group_group.invited_group_id " .

          " UNION " .

          "  SELECT DISTINCT m_user_base.* FROM m_group_expanded_user, m_user_base " . "   WHERE m_group_expanded_user.group_id = ? AND " . "         m_user_base.rowid = m_group_expanded_user.invited_group_id " . " ) AS users ",

        "where" => "1=1",

    },

    'fields' => [ [ 'rowid', 'rowid' ], [ 'ident', 'normal' ], [ 'name', 'normal' ], ],

    'search' => ['ident'],

);

# ============================================================================
#
# Initialize ()
#
#	Method that can be overloaded by Application implementation to initialize
#	variable, resource, etc, at the object creation time.
#
#	Return nothing.
#
# ============================================================================

sub Initialize {
    my ($self) = @_;

    $self->{stylesheet}    = "anim_group.xsl";
    $self->{appname}       = "AnimGroup";
    $self->{defaultfunc}   = "DisplayUsers";
    $self->{locale_domain} = 'animgroup_xsl';

    return;
}

# ============================================================================

=head2 DisplayMain ()

	Main Mioga entry point.
	
	Redirect to DisplayUsers
	
=cut

# ============================================================================

sub DisplayMain {
    my ($self, $context) = @_;

    my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
    $content->SetContent("DisplayUsers");

    return $content;
}

# ============================================================================
#
# GetAppDesc ()
#
#	Return the current application description.
#
# ============================================================================

sub GetAppDesc {
    my ($self, $context) = @_;

    my %AppDesc = (
        ident              => 'AnimGroup',
        name               => __("Animation"),
        package            => 'Mioga2::AnimGroup',
        description        => __('The Mioga2 Group Animation Application '),
        type               => 'normal',
        all_groups         => 1,
        all_users          => 0,
        can_be_public      => 0,
        is_user            => 0,
        is_group           => 1,
        is_resource        => 0,

        functions => {
            'Animation' => __('Animation Functions'),
            'Users'     => __('User Functions')
        },

        func_methods => {
            'Animation' => [ 'DisplayMain', 'DisplayUsers', 'DisplayTeams', 'DisplayResources', 'DisplayApplications', 'DisplayThemes', 'DisplayProfiles', 'InviteUsers', 'InviteTeams', 'AddResources', 'ChangeApplicationPublic', 'CreateProfile', 'ModifyProfile', 'DisplayProfileUsers', 'DisplayProfileTeams', 'InviteProfileUsers', 'InviteProfileTeams', 'GetGroups', 'GetAppList', 'RightsForUser' ],
            'Users'     => [ 'AppList',     'MemberList' ],
        },

		non_sensitive_methods => [ 'DisplayMain', 'DisplayUsers', 'DisplayTeams', 'DisplayResources', 'DisplayApplications', 'DisplayThemes', 'DisplayProfiles', 'InviteUsers', 'InviteTeams', 'AddResources', 'ChangeApplicationPublic', 'CreateProfile', 'ModifyProfile', 'DisplayProfileUsers', 'DisplayProfileTeams', 'InviteProfileUsers', 'InviteProfileTeams', 'GetGroups', 'GetAppList', 'RightsForUser', 'AppList', 'MemberList'  ]

    );
    return \%AppDesc;
}

sub RightsForUser {
    my ($self, $context) = @_;
    my $data = { RightsForUser => $context->GetContext, };

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['rowid'], 'disallow_empty', 'want_int' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::RightsForUser", __("Wrong argument value.") . Dumper $errors);
    }

    my $config   = $context->GetConfig;
    my $dbh      = $config->GetDBH;
    my $mioga_id = $config->GetMiogaId;
    my $user_id  = $context->{args}->{rowid};
    my $user     = Mioga2::Old::User->new($config, rowid => $user_id);
    my $group    = $context->GetGroup;
    my $group_id = $group->GetRowid;

    my $profile = SelectSingle($dbh, "SELECT m_profile.rowid FROM m_profile, m_profile_group, m_user_base WHERE m_user_base.rowid = $user_id AND m_profile_group.group_id = m_user_base.rowid AND m_profile.group_id = $group_id AND m_profile_group.profile_id = m_profile.rowid");

    my $paths = SelectMultiple($dbh, "SELECT * FROM m_uri WHERE rowid = parent_uri_id AND group_id = $group_id");
    my $rowids  = join(',', map({ $_->{rowid} } @$paths));
    my $request = "SELECT uri_id, (max(accesskey)%10) AS accesskey  FROM ( SELECT m_uri.rowid AS uri_id, (50 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_user WHERE  m_uri.rowid IN ($rowids) AND m_authorize.uri_id = m_uri.parent_uri_id AND m_authorize_user.authz_id = m_authorize.rowid AND m_authorize_user.user_id = $user_id UNION SELECT m_uri.rowid AS uri_id, (40 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_team, m_group_group WHERE  m_uri.rowid IN ($rowids) AND m_authorize.uri_id = m_uri.parent_uri_id AND m_authorize_team.authz_id = m_authorize.rowid AND m_group_group.group_id = m_authorize_team.team_id AND m_group_group.invited_group_id = $user_id UNION SELECT m_uri.rowid AS uri_id, (30 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_profile, m_profile_group WHERE  m_uri.rowid IN ($rowids) AND m_authorize.uri_id = m_uri.parent_uri_id AND m_authorize_profile.authz_id = m_authorize.rowid AND m_profile_group.profile_id = m_authorize_profile.profile_id AND m_profile_group.group_id = $user_id UNION SELECT m_uri.rowid AS uri_id, (20 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_profile, m_profile_expanded_user WHERE  m_uri.rowid IN ($rowids) AND m_authorize.uri_id = m_uri.parent_uri_id AND m_authorize_profile.authz_id = m_authorize.rowid AND m_profile_expanded_user.profile_id = m_authorize_profile.profile_id AND m_profile_expanded_user.user_id = $user_id ) AS tmp_req GROUP BY (uri_id)";
    my $results = SelectMultiple($dbh, $request);

    foreach my $res (@{$results}) {
        foreach my $path (@$paths) {
            if ($path->{rowid} == $res->{uri_id}) {
                $path->{access_right} = "rw" if ($res->{accesskey} == AUTHZ_WRITE);
                $path->{access_right} = "r"  if ($res->{accesskey} == AUTHZ_READ);
                $path->{access_right} = ""   if ($res->{accesskey} == AUTHZ_NONE);
                last;
            }
        }
    }

	$data->{RightsForUser}->{Profile} = $profile->{rowid};
	$data->{RightsForUser}->{CurrentUser} = { ident => $user->GetIdent (), name  => $user->GetName (), rowid => $user->GetRowid (), };
	$data->{RightsForUser}->{Paths} = { Path => $paths };

	my $xml = Mioga2::tools::Convert::PerlToXML ($data);
	print STDERR "[Mioga2::AnimGroup::RightsForUser] XML: $xml\n" if ($debug);

    my $content = Mioga2::Content::XSLT->new(
        $context,
        stylesheet    => 'anim_group.xsl',
        locale_domain => "animgroup_xsl"
    );
    $content->SetContent($xml);

    return $content;
}

# ============================================================================

=head2 DisplayUsers ()

    Display the list of invited users.

=head3 Generated XML :
    
    <DisplayUsers>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <!-- 
            List of possible user status declared in database
         -->
       <UserStatus>
          <status>active</status>
          <status>disabled</status>
          ...
       </UserStatus>

       <!-- 
            List of profiles.
         -->

       <ProfileList>
    
          <profile rowid="profile rowid">profile ident</profile>
          <profile rowid="profile rowid">profile ident</profile>
          ...

       </ProfileList>

       <!--
            SimpleLargeList description. See Mioga2::SimpleLargeList
         -->
    
    </DisplayUsers>

=head3 List fields :

    anim_users_sll : List users invited in current group.
    
    Available fields :
        name             type       description
    ----------------------------------------------------------------
      rowid           : rowid       : the user rowid 
      ident           : normal      : the user ident
      firstname       : normal      : the user first name
      lastname        : normal      : the user last name
      email           : email       : the user email
      status          : normal      : the user status (active|disabled|...)
      profile         : normal      : the user profile ident
      change_profile  : checkboxes  : permit to change user profile
      last_connection : date        : the last connection date of the user in current group

=head3 Form actions and fields

    SimpleLargeList actions. See Mioga2::SimpleLargeList.

=cut

# ============================================================================

sub DisplayUsers {
    my ($self, $context) = @_;

    my $session = $context->GetSession();
    my $config  = $context->GetConfig();
    my $dbh     = $config->GetDBH();
    my $group   = $context->GetGroup();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my @selids = grep (/^select_change_profile_[0-9]*/, keys(%{ $context->{args} }));
    my $params = [ [ ['profile_id'], 'allow_empty', 'want_int' ], [ ['change_profile_act'], 'allow_empty' ], [ \@selids, 'allow_empty', 'want_int' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::DisplayUsers", __("Wrong argument value."));
    }

    if (st_ArgExists($context, 'change_profile_act')
        and $context->{args}->{profile_id} ne '')
    {

        $self->ChangeGroupProfile($context);
    }

    # ------------------------------------------------------
    # Generate the XML
    # ------------------------------------------------------

    my $sll = new Mioga2::SimpleLargeList($context, \%users_sll_desc);
    my $tmpxml = $sll->Run($context, [ $group->GetRowid() ]);
    if (!defined($session->{__internal}->{message}) && !defined($tmpxml)) {

        # $sll->Run returned nothing (display success message)
        if (st_ArgExists($context, 'sll_delete_act')) {
            $session->{__internal}->{message}{text} = __('Users successfully revoked.');
            $session->{__internal}->{message}{type} = 'info';
        }
    }

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<DisplayUsers";
    if (defined($session->{__internal}->{message})) {
        $xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
        delete $session->{__internal}->{message};
    }
    $xml .= ">";

    # ------------------------------------------------------
    # Add the list of user status
    # ------------------------------------------------------
    $xml .= $self->GetXMLUserStatus($context);

    # ------------------------------------------------------
    # Add the list of profiles
    # ------------------------------------------------------
    $xml .= $self->GetXMLGroupProfiles($context);

    $xml .= $context->GetXML();

    if (defined($tmpxml)) {

        # $sll->Run returned something (confirmation page)
        $xml .= $tmpxml;
    }
    else {

        # $sll->Run returned nothing (display collection)
        $xml .= $sll->DisplayList($context);
    }

    $xml .= "</DisplayUsers>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});
    $content->SetContent($xml);

    return $content;
}

# ============================================================================

=head2 DisplayTeams ()

    Display the Mioga2 Team List.

=head3 Generated XML :
    
    <DisplayTeams>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <!-- 
            List of profiles.
         -->

       <ProfileList>
    
          <profile rowid="profile rowid">profile ident</profile>
          <profile rowid="profile rowid">profile ident</profile>
          ...

       </ProfileList>

       <!--
            SimpleLargeList description. See Mioga2::SimpleLargeList
         -->
    
    </DisplayTeams>

=head3 List fields :

    anim_teams_sll : List teams invited in current group.
    
    Available fields :
        name             type       description
    ----------------------------------------------------------------
      rowid           : rowid       : the team rowid 
      ident           : normal      : the team ident
      profile         : normal      : the team profile ident
      change_profile  : checkboxes  : permit to change team profile

=head3 Form actions and fields

    SimpleLargeList actions. See Mioga2::SimpleLargeList.

=cut

# ============================================================================

sub DisplayTeams {
    my ($self, $context) = @_;
    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});

    my $session = $context->GetSession();
    my $config  = $context->GetConfig();

    my $group = $context->GetGroup();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['change_profile_act'], 'allow_empty' ], [ ['profile_id'], 'allow_empty', 'want_int' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::DisplayTeams", __("Wrong argument value."));
    }

    if (st_ArgExists($context, 'change_profile_act')
        and $context->{args}->{profile_id} ne '')
    {

        $self->ChangeGroupProfile($context);
    }

    # ------------------------------------------------------
    # Generate the XML
    # ------------------------------------------------------

    my $sll = new Mioga2::SimpleLargeList($context, \%teams_sll_desc);
    my $tmpxml = $sll->Run($context, [ $group->GetRowid() ]);
    if (!defined($session->{__internal}->{message}) && !defined($tmpxml)) {

        # $sll->Run returned nothing (display success message)
        if (st_ArgExists($context, 'sll_delete_act')) {
            $session->{__internal}->{message}{text} = __('Teams successfully revoked.');
            $session->{__internal}->{message}{type} = 'info';
        }
    }

    my $xml = '<?xml version="1.0" ?>';
    $xml .= "<DisplayTeams";
    if (defined($session->{__internal}->{message})) {
        $xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
        delete $session->{__internal}->{message};
    }
    $xml .= ">";

    # ------------------------------------------------------
    # Add the list of profiles
    # ------------------------------------------------------
    $xml .= $self->GetXMLGroupProfiles($context);

    $xml .= $context->GetXML();

    if (defined($tmpxml)) {

        # $sll->Run returned something (confirmation page)
        $xml .= $tmpxml;
    }
    else {

        # $sll->Run returned nothing (display collection)
        $xml .= $sll->DisplayList($context);
    }

    $xml .= "</DisplayTeams>";

    $content->SetContent($xml);
    return $content;
}

# ============================================================================

=head2 DisplayResources ()

    Display the Mioga2 Resource List.

=head3 Generated XML :
    
    <DisplayResources>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <!-- 
            List of possible resource status declared in database
         -->

       <ResourceStatus>
          <status>active</status>
          <status>disabled</status>
          ...
       </ResourceStatus>

       <!-- 
            List of profiles.
         -->

       <ProfileList>
    
          <profile rowid="profile rowid">profile ident</profile>
          <profile rowid="profile rowid">profile ident</profile>
          ...

       </ProfileList>

       <!--
            SimpleLargeList description. See Mioga2::SimpleLargeList
         -->
    
    </DisplayResources>

=head3 List fields :

    anim_resources_sll : List resources invited in current group.
    
    Available fields :
        name             type       description
    ----------------------------------------------------------------
      rowid           : rowid       : the resource rowid 
      ident           : normal      : the resource ident
      location        : normal      : the resource location
      status          : normal      : the resource status (active|disabled|...)
      anim_ident      : normal      : the resource animator ident

=head3 Form actions and fields

    SimpleLargeList actions. See Mioga2::SimpleLargeList.

=cut

# ============================================================================

sub DisplayResources {
    my ($self, $context) = @_;
    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});

    my $session = $context->GetSession();
    my $config  = $context->GetConfig();

    my $group = $context->GetGroup();

    # ------------------------------------------------------
    # Generate the XML
    # ------------------------------------------------------

    my $sll = new Mioga2::SimpleLargeList($context, \%resources_sll_desc);
    my $tmpxml = $sll->Run($context, [ $group->GetRowid() ]);
    if (!defined($session->{__internal}->{message}) && !defined($tmpxml)) {

        # $sll->Run returned nothing (display success message)
        if (st_ArgExists($context, 'sll_delete_act')) {
            $session->{__internal}->{message}{text} = __('Resources successfully revoked.');
            $session->{__internal}->{message}{type} = 'info';
        }
    }

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<DisplayResources";
    if (defined($session->{__internal}->{message})) {
        $xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
        delete $session->{__internal}->{message};
    }
    $xml .= ">";

    $xml .= $context->GetXML();

    # ------------------------------------------------------
    # Add resource status description
    # ------------------------------------------------------
    $xml .= $self->GetXMLResourceStatus($context);

    if (defined($tmpxml)) {

        # $sll->Run returned something (confirmation page)
        $xml .= $tmpxml;
    }
    else {

        # $sll->Run returned nothing (display collection)
        $xml .= $sll->DisplayList($context);
    }

    $xml .= "</DisplayResources>";

    $content->SetContent($xml);
    return $content;
}

# ============================================================================

=head2 DisplayApplications ()

	Display the Mioga2 Application List.

=head3 Generated XML :
    
    <DisplayApplications>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <!--
            SimpleLargeList description. See Mioga2::SimpleLargeList
         -->
    
    </DisplayApplications>

=head3 List fields :

    anim_applications_sll : List applications.
    
    Available fields :
        name             type       description
    ----------------------------------------------------------------
      rowid             : rowid       : the resource rowid 
      ident             : normal      : the localized application ident
      realident         : hidden      : the application ident
      change_app_status : checkboxes  : permit to change application status (active|disabled)
      is_public         : link        : is the application public ? 
                                        When the application can be public, the link point to ChangeApplicationPublic.
=head3 Form actions and fields

    SimpleLargeList actions. See Mioga2::SimpleLargeList.

=cut

# ============================================================================

sub DisplayApplications {
    my ($self, $context, $sll_desc) = @_;
    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});

    my $config = $context->GetConfig();
    my $dbh    = $config->GetDBH();

    my $group = $context->GetGroup();
    my $user  = $context->GetUser();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['change_application_act'], 'allow_empty' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::DisplayTeams", __("Wrong argument value."));
    }

    # ------------------------------------------------------
    # Generate the XML
    # ------------------------------------------------------

    if (!defined $sll_desc) {
        $sll_desc = \%applications_sll_desc;
    }

    my $sll = new Mioga2::LargeList::ApplicationList($context, $sll_desc);

    my $session  = $context->GetSession();
    my $mioga_id = $config->GetMiogaId();
    my $group_id = $group->GetRowid();
    my $user_id  = $user->GetRowid();
	my $grouplist = Mioga2::GroupList->new ($config, { attributes => { rowid => $group_id } });

    my $selected_apps = $self->GetSelectedApps($config, $group_id);

    if (st_ArgExists($context, 'change_application_act')) {
        my %available;
		my $some_apps_not_disabled = 0;
        foreach my $app_rowid (@{ $session->{LargeList}->{anim_applications_sll}->{cur_page_rowids} }) {
            my $appdesc = new Mioga2::AppDesc($config, rowid => $app_rowid);
            my $app_name = $appdesc->GetIdent();

            if (    !($group_id == $config->GetAdminId() and $app_name eq 'Admin')
                and !($app_name =~ /^Anim/) and !(grep (/^$app_name$/, $grouplist->GetProtectedApplications ())))
            {
                if (exists $context->{args}->{"select_change_app_status_$app_rowid"}) {
                    if (!grep { $_->{rowid} == $app_rowid } @$selected_apps) {
                        $available{$app_rowid} = 1;
                        ApplicationEnableInGroup($dbh, $app_rowid, $group_id);

                        my $animator = $group->GetAnimId();

                        my $res = SelectSingle($dbh, "SELECT m_profile.rowid FROM m_profile, m_profile_group " . "WHERE m_profile.group_id = $group_id AND " . "      m_profile_group.profile_id = m_profile.rowid AND " . "      m_profile_group.group_id = $animator");

                        if (defined $res) {
                            my $functions = $appdesc->GetFunctionsIds();

                            foreach my $function (@$functions) {
                                ProfileCreateFunction($dbh, $res->{rowid}, $function);
                            }
                        }
                    }
                }
                else {
                    ApplicationDisableInGroup($dbh, $app_rowid, $group_id);
                    $available{$app_rowid} = 0;
                }
            }
			elsif (!exists $context->{args}->{"select_change_app_status_$app_rowid"}) {
				$some_apps_not_disabled = 1 unless (($app_name eq 'AnimGroup') || ($app_name eq 'AnimUser'));	# Do not raise flag for AnimGroup or AnimUser as it is not (un)selectabllectablee
			}
        }

		if (!$some_apps_not_disabled) {
			$session->{__internal}->{message}{type} = 'info';
			$session->{__internal}->{message}{text} = __('Application status successfully changed.');
		}
		else {
			$session->{__internal}->{message}{type} = 'error';
			$session->{__internal}->{message}{text} = __("Group's default application can not be disabled.");
		}
        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent('DisplayApplications');
        return $content;
    }

    my @selected = ();

    foreach my $app (@$selected_apps) {
        push(@selected, $app->{rowid});
    }
    $sll = new Mioga2::LargeList::ApplicationList($context, $sll_desc);
    my $xml = '<?xml version="1.0"?>';
    $xml .= "<DisplayApplications";
    if (defined($session->{__internal}->{message})) {
        $xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
        delete $session->{__internal}->{message};
    }
    $xml .= ">";

    $xml .= $context->GetXML();

    if ($group_id == $config->GetAdminId()) {
        $xml .= "<is_admin/>";
    }

    $xml .= $sll->InitSelectedCheckboxes($context, 'change_app_status', \@selected);

    $xml .= $sll->Run($context, [ $config->GetMiogaId(), $group->GetRowid() ]);

    $xml .= "</DisplayApplications>";

    $content->SetContent($xml);
    return $content;
}

# ============================================================================

=head2 DisplayProfiles ()

	Display the Mioga2 Profile List.

=head3 Generated XML :
    
    <DisplayProfiles>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <!-- 
            List of profiles.
         -->

	   <ProfileList>
	
          <profile rowid="profile rowid">profile ident</profile>
	      <profile rowid="profile rowid">profile ident</profile>
	      ...

       </ProfileList>

	   <default>default profile rowid</default>

       <!--
            SimpleLargeList description. See Mioga2::SimpleLargeList
         -->
    
    </DisplayProfiles>

=head3 List fields :

    anim_profiles_sll : List profiles of current group.
    
    Available fields :
        name             type       description
    ----------------------------------------------------------------
      rowid           : rowid       : the profile rowid 
      ident           : link        : the profile ident. The link point to ModifyProfile
      nb_member       : normal      : number of member in the profile

=head3 Form actions and fields

    SimpleLargeList actions. See Mioga2::SimpleLargeList.

=cut

# ============================================================================

sub DisplayProfiles {
    my ($self, $context, $sll_desc, $childxml) = @_;
    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});

    my $session = $context->GetSession();
    my $config  = $context->GetConfig();

    my $group = $context->GetGroup();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['act_change_profile'], 'allow_empty' ], [ ['default_profile'], 'allow_empty', 'want_int' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::DisplayProfiles", __("Wrong argument value."));
    }

    if (st_ArgExists($context, 'act_change_profile') and st_ArgExists($context, 'default_profile')) {
        $self->ModifyDefaultProfile($config, $group, $context->{args}->{'default_profile'});
        $group->ReloadValues();
    }

    # ------------------------------------------------------
    # Generate the XML
    # ------------------------------------------------------

    if (!defined $sll_desc) {
        $sll_desc = \%profiles_sll_desc;
    }

    my $sll = new Mioga2::SimpleLargeList($context, $sll_desc);
    my $tmpxml = $sll->Run($context, [ $group->GetRowid() ]);
    if (!defined($session->{__internal}->{message}) && !defined($tmpxml)) {

        # $sll->Run returned nothing (display success message)
        if (st_ArgExists($context, 'sll_delete_act')) {
            $session->{__internal}->{message}{text} = __('Profile successfully deleted.');
            $session->{__internal}->{message}{type} = 'info';
        }
    }

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<DisplayProfiles";
    if (defined($session->{__internal}->{message})) {
        $xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
        delete $session->{__internal}->{message};
    }
    $xml .= ">";

    $xml .= $context->GetXML();

    if (defined($tmpxml)) {

        # $sll->Run returned something (confirmation page)
        $xml .= $tmpxml;
    }
    else {

        # $sll->Run returned nothing (display collection)
        $xml .= $sll->DisplayList($context);
    }

    $xml .= $self->GetXMLGroupProfiles($context);

    $xml .= "<default>" . $group->GetDefaultProfileId() . "</default>";

    if (defined $childxml) {
        $xml .= $childxml;
    }

    $xml .= "</DisplayProfiles>";

    $content->SetContent($xml);

    return $content;
}

# ============================================================================

=head2 DisplayThemes ()

    Display the Themes/Lang List.


=head3 Generated XML :
    
    <DisplayThemes>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <fields>
          <theme_ident>Current theme ident</theme_ident>
          <lang_ident>Current theme ident</lang_ident>
       </fields>


        <!--
            List of installed themes and languages
          -->
			see GetXMLThemeLang
    </DisplayThemes>

=head3 Form actions and fields

    - select_theme_act    : Validate theme modification

      Arguments :
        name          type                description
      ---------------------------------------------------------------------
      - rowid      : not empty string : the current group rowid
      - theme_id   : not empty string : the theme rowid
      - lang_id    : not empty string : the language rowid

=cut

# ============================================================================

sub DisplayThemes {
    my ($self, $context) = @_;
    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();
    my $group   = $context->GetGroup();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['select_theme_act'], 'allow_empty' ], [ [ 'lang_id', 'theme_id' ], 'allow_empty', 'want_int' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::DisplayThemes", __("Wrong argument value."));
    }

    if (st_ArgExists($context, 'select_theme_act')) {
        my $values = {
            rowid    => $group->GetRowid(),
            theme_id => $context->{args}->{theme_id},
            lang_id  => $context->{args}->{lang_id}
        };

        $self->ModifyThemeAndLang($config, $values);
        $session->{__internal}->{message}{type} = 'info';
        $session->{__internal}->{message}{text} = __('Theme successfully modified');
        my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
        $content->SetContent('DisplayThemes');
        return $content;
    }

    # ------------------------------------------------------
    # Generate the XML
    # ------------------------------------------------------

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<DisplayThemes";
    if (defined($session->{__internal}->{message})) {
        $xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
        delete $session->{__internal}->{message};
    }
    $xml .= ">";

    $xml .= $context->GetXML();

    $xml .= "<fields>";
    $xml .= "<theme_ident>" . $group->GetTheme() . "</theme_ident>";
    $xml .= "<lang_ident>" . $group->GetLang() . "</lang_ident>";
    $xml .= "</fields>";

    $xml .= $self->GetXMLThemeLang($context);

    $xml .= "</DisplayThemes>";

    $content->SetContent($xml);
    return $content;
}

# ============================================================================

=head2 DisplayProfileUsers ()

	Display profiles user members list

=head3 Generated XML :
    
    <DisplayProfileUsers>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <referer>URL of calling application</referer>

       <!--
            SimpleLargeList description. See Mioga2::SimpleLargeList
         -->
    
    </DisplayProfileUsers>

=head3 List fields :

    anim_profile_users_sll : List users of the selected profile
    
    Available fields :

        name             type       description
    ----------------------------------------------------------------
      rowid           : rowid    : the user rowid 
      ident           : normal   : the user ident
      firstname       : normal   : the user first name
      lastname        : normal   : the user last name

=head3 Form actions and fields

    SimpleLargeList actions. See Mioga2::SimpleLargeList.

=cut

# ============================================================================

sub DisplayProfileUsers {
    my ($self, $context) = @_;
    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['rowid'], 'allow_empty', 'want_int' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::DisplayProfileUsers", __("Wrong argument value.") . Dumper $errors);
    }

    my $session = $context->GetSession();
    my $config  = $context->GetConfig();

    if (exists $context->{args}->{rowid}) {
        $session->{AnimGroup}->{DisplayProfileUsers}->{rowid}   = $context->{args}->{rowid};
        $session->{AnimGroup}->{DisplayProfileUsers}->{referer} = $context->GetReferer();
        $session->{AnimGroup}->{DisplayProfileUsers}->{referer} =~ s/\?.*//g;
    }

    my $rowid   = $session->{AnimGroup}->{DisplayProfileUsers}->{rowid};
    my $referer = $session->{AnimGroup}->{DisplayProfileUsers}->{referer};

    # ------------------------------------------------------
    # Generate the XML
    # ------------------------------------------------------

    my $sll = new Mioga2::SimpleLargeList($context, \%profiles_users_sll_desc);

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<DisplayProfileUsers";
    if (defined($session->{__internal}->{message})) {
        $xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
        delete $session->{__internal}->{message};
    }
    $xml .= ">";

    $xml .= $context->GetXML();

    $xml .= "<referer>" . st_FormatXMLString($referer) . "</referer>";

    $xml .= $sll->Run($context, [$rowid]);

    $xml .= "</DisplayProfileUsers>";

    $content->SetContent($xml);
    return $content;
}

# ============================================================================

=head2 DisplayProfileTeams ()

    Display profiles team members list

=head3 Generated XML :
    
    <DisplayProfileTeams>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <referer>URL of calling application</referer>

       <!--
            SimpleLargeList description. See Mioga2::SimpleLargeList
         -->
    
    </DisplayProfileTeams>

=head3 List fields :

    anim_profile_teams_sll : List teams of the selected profile
    
    Available fields :

        name             type       description
    ----------------------------------------------------------------
      rowid           : rowid    : the user rowid 
      ident           : normal   : the user ident
      anim_ident      : normal   : the team anmator identifier

=head3 Form actions and fields

    SimpleLargeList actions. See Mioga2::SimpleLargeList.

=cut

# ============================================================================

sub DisplayProfileTeams {
    my ($self, $context) = @_;
    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['rowid'], 'allow_empty', 'want_int' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::DisplayProfileTeams", __("Wrong argument value.") . Dumper $errors);
    }

    my $session = $context->GetSession();
    my $config  = $context->GetConfig();

    if (exists $context->{args}->{rowid}) {
        $session->{AnimGroup}->{DisplayProfileTeams}->{rowid}   = $context->{args}->{rowid};
        $session->{AnimGroup}->{DisplayProfileTeams}->{referer} = $context->GetReferer();
        $session->{AnimGroup}->{DisplayProfileTeams}->{referer} =~ s/\?(.*)$//g;
    }

    my $rowid   = $session->{AnimGroup}->{DisplayProfileTeams}->{rowid};
    my $referer = $session->{AnimGroup}->{DisplayProfileTeams}->{referer};

    # ------------------------------------------------------
    # Generate the XML
    # ------------------------------------------------------

    my $sll = new Mioga2::SimpleLargeList($context, \%profiles_teams_sll_desc);

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<DisplayProfileTeams";
    if (defined($session->{__internal}->{message})) {
        $xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
        delete $session->{__internal}->{message};
    }
    $xml .= ">";

    $xml .= $context->GetXML();

    $xml .= "<referer>" . st_FormatXMLString($referer) . "</referer>";

    $xml .= $sll->Run($context, [$rowid]);

    $xml .= "</DisplayProfileTeams>";

    $content->SetContent($xml);
    return $content;
}

# ============================================================================

=head2 InviteUsers ()

	User invitation function.

	Redirect to referer with InlineMessage or redirect to Mioga2::Select->SelectMultipleUsersNotInGroup

=head3 Form actions and fields :
    
    - select_users_back : Return from user selection

      Arguments :
         name                type                   description
       -------------------------------------------------------------------
       rowids       :  not empty valid ident :  a comma separated list of selected users rowid

=cut

# ============================================================================

sub InviteUsers {
    my ($self, $context) = @_;

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['select_users_back'], 'allow_empty' ], [ ['rowids'], 'allow_empty', [ 'mioga_user_ids', $config ] ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::InviteUsers", __("Wrong argument value."));
    }

    # ------------------------------------------------------
    # Check submited values
    # ------------------------------------------------------
    if (st_ArgExists($context, 'select_users_back')) {

        # Invite Users
        if (exists $context->{args}->{rowids}) {
            my $group = $context->GetGroup();
            my $rowid = $group->GetRowid();

            my @rowids = split(',', $context->{args}->{rowids});

            my $dbh = $config->GetDBH();

            foreach my $grp_id (@rowids) {
                try {
                    GroupInviteGroup($dbh, $rowid, $grp_id);
                }

                catch Mioga2::Exception::DB with {

                    # Ignore already invited users.
                };
            }

            $session->{__internal}->{message}{type} = 'info';
            $session->{__internal}->{message}{text} = __('Users successfully invited.');
            my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{InviteUsers}->{referer});
            return $content;
        }

        else {
            my $content = new Mioga2::Content::REDIRECT($config, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{InviteUsers}->{referer});
            return $content;
        }

    }

    $session->{AnimGroup}->{InviteUsers}->{referer} = $context->GetReferer();

    return $self->LaunchSelectMultipleUsers($context);
}

# ============================================================================

=head2 InviteProfileUsers ()

    Invite users member of group in a profile.

    Redirect to referer with InlineMessage or redirect to Mioga2::Select->SelectMultipleUsersMemberOfGroup

=head3 Form actions and fields :
    
    - select_users_back : Return from user selection

      Arguments :
         name                type                   description
       -------------------------------------------------------------------
       rowids       :  not empty valid ident :  a comma separated list of selected users rowid

=cut

# ============================================================================

sub InviteProfileUsers {
    my ($self, $context) = @_;

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['select_users_back'], 'allow_empty' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::InviteProfileUsers", __("Wrong argument value.") . Dumper $errors);
    }

    # ------------------------------------------------------
    # Check submited values
    # ------------------------------------------------------
    if (st_ArgExists($context, 'select_users_back')) {

        # Invite Users
        if (exists $context->{args}->{rowids}) {
            my $dbh     = $config->GetDBH();
            my $group   = $context->GetGroup();
            my $rowid   = $group->GetRowid();
            my $prof_id = $session->{AnimGroup}->{InviteProfileUsers}->{rowid};

            my @rowids = split(',', $context->{args}->{rowids});

            foreach my $grp_id (@rowids) {
                next if ($grp_id == $group->GetAnimId());

                ProfileAddGroup($dbh, $prof_id, $rowid, $grp_id);
            }

            $session->{__internal}->{message}{type} = 'info';
            $session->{__internal}->{message}{text} = __('Users successfully invited.');
            my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{InviteProfileUsers}->{referer});
            return $content;
        }

        else {
            my $content = new Mioga2::Content::REDIRECT($config, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{InviteProfileUsers}->{referer});
            return $content;
        }

    }

    if (exists $context->{args}->{rowid}) {
        $session->{AnimGroup}->{InviteProfileUsers}->{rowid} = $context->{args}->{rowid};
    }
    else {
        $session->{AnimGroup}->{InviteProfileUsers}->{rowid} = $session->{AnimGroup}->{DisplayProfileUsers}->{rowid};
    }

    $session->{AnimGroup}->{InviteProfileUsers}->{referer} = $context->GetReferer();
    $session->{AnimGroup}->{InviteProfileUsers}->{referer} =~ s/\?(.*)$//;

    return $self->LaunchSelectMultipleUsersMemberOfGroup($context);
}

# ============================================================================

=head2 InviteProfileTeams ()

    Invite teams member of team in a profile.

    Redirect to referer with InlineMessage or redirect to Mioga2::Select->SelectMultipleTeamsMemberOfGroup

=head3 Form actions and fields :
    
    - select_teams_back : Return from team selection

      Arguments :
         name                type                   description
       -------------------------------------------------------------------
       rowids       :  not empty valid ident :  a comma separated list of selected teams rowid

=cut

# ============================================================================

sub InviteProfileTeams {
    my ($self, $context) = @_;

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['select_teams_back'], 'allow_empty' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::InviteProfileUsers", __("Wrong argument value.") . Dumper $errors);
    }

    # ------------------------------------------------------
    # Check submited values
    # ------------------------------------------------------
    if (st_ArgExists($context, 'select_teams_back')) {

        # Invite Teams
        if (exists $context->{args}->{rowids}) {
            my $dbh     = $config->GetDBH();
            my $rowid   = $context->GetGroup()->GetRowid();
            my $prof_id = $session->{AnimGroup}->{InviteProfileTeams}->{rowid};

            my @rowids = split(',', $context->{args}->{rowids});

            foreach my $grp_id (@rowids) {
                ProfileAddGroup($dbh, $prof_id, $rowid, $grp_id);
            }

            $session->{__internal}->{message}{type} = 'info';
            $session->{__internal}->{message}{text} = __('Teams successfully invited.');
            my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{InviteProfileTeams}->{referer});
            return $content;
        }

        else {
            my $content = new Mioga2::Content::REDIRECT($config, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{InviteProfileTeams}->{referer});
            return $content;
        }

    }

    if (exists $context->{args}->{rowid}) {
        $session->{AnimGroup}->{InviteProfileTeams}->{rowid} = $context->{args}->{rowid};
    }
    else {
        $session->{AnimGroup}->{InviteProfileTeams}->{rowid} = $session->{AnimGroup}->{DisplayProfileTeams}->{rowid};
    }

    $session->{AnimGroup}->{InviteProfileTeams}->{referer} = $context->GetReferer();
    $session->{AnimGroup}->{InviteProfileTeams}->{referer} =~ s/\?(.*)$//;

    return $self->LaunchSelectMultipleTeamsMemberOfGroup($context);
}

# ============================================================================

=head2 InviteTeams ()

	Team invitation function.

	Redirect to referer with InlineMessage or redirect to Mioga2::Select->SelectMultipleTeamsNotInGroup

=head3 Form actions and fields :
    
    - select_teams_back : Return from team selection

      Arguments :
         name                type                   description
       -------------------------------------------------------------------
       rowids       :  not empty valid ident :  a comma separated list of selected teams rowid

=cut

# ============================================================================

sub InviteTeams {
    my ($self, $context) = @_;

    my $config = $context->GetConfig();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['select_teams_back'], 'allow_empty' ], [ ['rowids'], 'allow_empty', [ 'mioga_team_ids', $config ] ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::InviteTeams", __("Wrong argument value."));
    }

    my $session = $context->GetSession();

    # ------------------------------------------------------
    # Check submited values
    # ------------------------------------------------------
    if (st_ArgExists($context, 'select_teams_back')) {

        # Invite Teams
        if (exists $context->{args}->{rowids}) {
            my $group = $context->GetGroup();
            my $rowid = $group->GetRowid();

            my @rowids = split(',', $context->{args}->{rowids});

            my $dbh = $config->GetDBH();

            foreach my $grp_id (@rowids) {

                next if ($grp_id == $rowid);

                try {
                    GroupInviteGroup($dbh, $rowid, $grp_id);
                }

                catch Mioga2::Exception::DB with {

                    # Ignore already invited teams.
                };
            }

            $session->{__internal}->{message}{type} = 'info';
            $session->{__internal}->{message}{text} = __('Teams successfully invited.');
            my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{InviteTeams}->{referer});
            return $content;
        }

        else {
            my $content = new Mioga2::Content::REDIRECT($config, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{InviteTeams}->{referer});
            return $content;
        }

    }

    $session->{AnimGroup}->{InviteTeams}->{referer} = $context->GetReferer();

    return $self->LaunchSelectMultipleTeams($context);
}

# ============================================================================

=head2 AddResources ()

	Resource invitation function.

	Redirect to referer with InlineMessage or redirect to Mioga2::Select->SelectMultipleResourcesNotInGroup

=head3 Form actions and fields :
    
    - select_resources_back : Return from resource selection

      Arguments :
         name                type                   description
       -------------------------------------------------------------------
       rowids       :  not empty valid ident :  a comma separated list of selected resources rowid

=cut

# ============================================================================

sub AddResources {
    my ($self, $context) = @_;

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['select_resources_back'], 'allow_empty' ], [ ['rowids'], 'allow_empty', [ 'mioga_resource_ids', $config ] ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::AddResources", __("Wrong argument value."));
    }

    # ------------------------------------------------------
    # Check submited values
    # ------------------------------------------------------
    if (st_ArgExists($context, 'select_resources_back')) {

        # Invite Groups
        if (exists $context->{args}->{rowids}) {
            my $group = $context->GetGroup();
            my $rowid = $group->GetRowid();

            my @rowids = split(',', $context->{args}->{rowids});

            my $dbh = $config->GetDBH();

            foreach my $res_id (@rowids) {

                try {
                    GroupInviteGroup($dbh, $res_id, $rowid);
                }

                catch Mioga2::Exception::DB with {

                    # Ignore already invited groups.
                };
            }

            $session->{__internal}->{message}{type} = 'info';
            $session->{__internal}->{message}{text} = __('Resources successfully invited.');
            my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{AddResources}->{referer});
            return $content;
        }

        else {
            my $content = new Mioga2::Content::REDIRECT($config, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{AddResources}->{referer});
            return $content;
        }

    }

    $session->{AnimGroup}->{AddResources}->{referer} = $context->GetReferer();

    return $self->LaunchSelectMultipleResources($context);
}

# ============================================================================

=head2 ChangeApplicationPublic ()

    Change application public state.

	Redirect to referer with InlineMessage

=cut

# ============================================================================

sub ChangeApplicationPublic {
    my ($self, $context) = @_;

    my $config  = $context->GetConfig();
    my $dbh     = $config->GetDBH();
    my $session = $context->GetSession();

    my $group = $context->GetGroup();

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ ['rowid'], 'disallow_empty', 'want_int' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::ChangeApplicationPublic", __("Wrong argument value."));
    }

    my $appdesc = new Mioga2::AppDesc($config, rowid => $context->{args}->{rowid});
    if ($appdesc->IsPublicForCurrentGroup($context, 0)) {
        ApplicationPrivateInGroup($dbh, $appdesc->GetRowid(), $group->GetRowid());
    }
    else {

        ApplicationPublicInGroup($dbh, $appdesc->GetRowid(), $group->GetRowid());
    }

    $session->{__internal}->{message}{type} = 'info';
    $session->{__internal}->{message}{text} = __('Application status successfully modified.');
    my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
    $content->SetContent($context->GetReferer());
    return $content;
}

# ============================================================================

=head2 CreateProfile ()

    Profile creation method.

=head3 Generated XML :
    
    <CreateProfile>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <referer>URL of calling application</referer>

	   <!-- 
           List of applications functions
          -->
		<Applications>

            <app rowid="app rowid" ident="app ident">
                <func>Function ident</func>;
                <func>Function ident</func>;
                ...
            </app>

            ...

 		</Applications>

       <fields>
          <!-- 
             Fields value description. See Mioga2::tools::args_checker->ac_XMLifyArgs
             -->

          <!-- 
           List of selected applications functions
             -->
          <Applications>

               <app rowid="Application rowid" ident="Application Ident"  
                    description="Application description"
		            name="Localized application name">
			
 	              <func ident="Function identifier"></func>
 	              <func ident="Function identifier"></func>
                  ...

		       </app>

          <Applications>

       </fields>

       <!--
            Errors description. See Mioga2::tools::args_checker->ac_XMLifyErrors
         -->
    

       <TeamsList>

       <!--
           List of team invited is this group LargeList description. See Mioga2::LargeList::RowidList
         -->

       </TeamsList>

       <UsersList>

       <!--
           List of user invited in this group LargeList description. See Mioga2::LargeList::RowidList
         -->

       </UsersList>

    </CreateProfile>

=head3 Form actions and fields
    
    - profile_create_act : Try to create a profile

      Arguments :
         name                type                  description
       ---------------------------------------------------------------------
       ident       :  not empry string          :  the profile rowid
       select_function_<app_ident>_<func_ident> :  the selected applications and functions

    - act_add_user     : Run invited users selection


    - act_add_team     : Run invited teams selection


    - select_users_back : Return From Invited users list selection.

      Arguments :
         name                 type                  description
       ---------------------------------------------------------------------
       rowids      :  not empty valid ident  :  a comma separated list of selected user rowid


    - select_teams_back : Return Invited teams list selected.

      Arguments :
         name                 type                  description
       ---------------------------------------------------------------------
       rowids      :  not empty valid ident  :  a comma separated list of selected team rowid

=cut

# ============================================================================

sub CreateProfile {
    my ($self, $context) = @_;

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

	my $values = {};
    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ [ 'select_users_back', 'select_teams_back', 'act_add_user', 'act_add_team', 'ident' ], 'allow_empty' ], [ ['rowids'], 'allow_empty' ], ];
    my ($chkvalues, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::CreateProfile", __("Wrong argument value.") . Dumper $errors);
    }

    if (st_ArgExists($context, 'profile_create_act')) {
		#-------------------------------------------------------------------------------
		# Check incoming arguments
		#-------------------------------------------------------------------------------
		my $params = [ [ [ 'select_users_back', 'select_teams_back', 'act_add_user', 'act_add_team' ], 'allow_empty' ], [ ['rowids'], 'allow_empty' ], [ ['ident'], 'disallow_empty'], ];
		my ($chkvalues, $errors) = ac_CheckArgs($context, $params);

		if (@$errors) {
			throw Mioga2::Exception::Application("AnimGroup::CreateProfile", __("Wrong argument value.") . Dumper $errors);
		}
        else {
            my $group = $context->GetGroup();
            my $dbh   = $config->GetDBH();

            try {
                $self->InitializeProfileValues($context, $values);

                my $profile_id = ProfileCreate($dbh, $values->{ident}, $group->GetRowid());

                foreach my $apps (keys %{ $values->{apps} }) {
                    foreach my $func (@{ $values->{apps}->{$apps} }) {

                        my $appdesc = new Mioga2::AppDesc($config, rowid => $apps);
                        my $func_id = $appdesc->GetFunctionsIdsFromIdent($func);

                        ProfileCreateFunction($dbh, $profile_id, $func_id);
                    }
                }

                foreach my $grp (@{ $values->{users} }, @{ $values->{teams} }) {
                    ProfileAddGroup($dbh, $profile_id, $group->GetRowid(), $grp);
                }
            }

            catch Mioga2::Exception::Simple with {
                my $err = shift;
                push @$errors, [ $err->as_string, ['__create__'] ];
            };

            if (!@$errors) {
                $session->{__internal}->{message}{type} = 'info';
                $session->{__internal}->{message}{text} = __('Profile successfully created.');
                my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
                $content->SetContent($session->{AnimGroup}->{Profile}->{referer});
                return $content;
            }
        }
    }

    elsif (st_ArgExists($context, 'select_users_back')) {
        $values = $session->{AnimGroup}->{Profile};

        if (exists $context->{args}->{rowids}) {
            foreach my $rowid (split(/,/, $context->{args}->{rowids})) {
                if (!grep { $rowid == $_ } @{ $values->{users} }) {
                    push @{ $values->{users} }, $rowid;
                }
            }
        }

    }

    elsif (st_ArgExists($context, 'select_teams_back')) {
        $values = $session->{AnimGroup}->{Profile};

        if (exists $context->{args}->{rowids}) {
            foreach my $rowid (split(/,/, $context->{args}->{rowids})) {
                if (!grep { $rowid == $_ } @{ $values->{teams} }) {
                    push @{ $values->{teams} }, $rowid;
                }
            }
        }

    }

    elsif (st_ArgExists($context, 'act_add_user')) {
        $self->InitializeProfileValues($context, $values);
        return $self->LaunchSelectMultipleUserNotInList($context, $session->{AnimGroup}->{Profile}->{users});
    }

    elsif (st_ArgExists($context, 'act_add_team')) {
        $self->InitializeProfileValues($context, $values);
        return $self->LaunchSelectMultipleTeamNotInList($context, $session->{AnimGroup}->{Profile}->{teams});
    }

    else {
        delete $session->{AnimGroup}->{Profile};
        $values = {
            referer => $context->GetReferer(),
            users   => [],
            teams   => [],
        };
    }

    $self->InitializeProfileValues($context, $values);

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<CreateProfile>";

    $xml .= "<referer>" . st_FormatXMLString($session->{AnimGroup}->{Profile}->{referer}) . "</referer>";

    $xml .= $context->GetXML();

    $xml .= $self->GetXMLProfileApplications($context, 0, 0);

    $xml .= "<fields>";
    $xml .= ac_XMLifyArgs([ 'public_part', 'anim_id', 'ident', 'description' ], $values);

    $xml .= $self->GetXMLSelectedFunc($context);

    $xml .= "</fields>";

    $xml .= ac_XMLifyErrors($errors);

    $xml .= "<UsersList>";
    my $user_sll = new Mioga2::LargeList::RowidList($context, \%profiles_users_tiny_sll_desc, $values->{users});
    $xml .= $user_sll->Run($context);
    $xml .= "</UsersList>";

    $xml .= "<TeamsList>";
    my $team_sll = new Mioga2::LargeList::RowidList($context, \%profiles_teams_tiny_sll_desc, $values->{teams});
    $xml .= $team_sll->Run($context);
    $xml .= "</TeamsList>";

    $xml .= "</CreateProfile>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});
    $content->SetContent($xml);

    return $content;
}

# ============================================================================

=head2 ModifyProfile ()

    Profile modification method.

=head3 Generated XML :
    
    <ModifyProfile>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <referer>URL of calling application</referer>

       <!-- 
           List of applications functions
          -->
        <Applications>

            <app rowid="app rowid" ident="app ident">
                <func>Function ident</func>;
                <func>Function ident</func>;
                ...
            </app>

            ...

        </Applications>

       <fields>
          <!-- 
             Fields value description. See Mioga2::tools::args_checker->ac_XMLifyArgs
             -->

          <!-- 
           List of selected applications functions
             -->
          <Applications>

               <app rowid="Application rowid" ident="Application Ident"  
                    description="Application description"
                    name="Localized application name">
            
                  <func ident="Function identifier"></func>
                  <func ident="Function identifier"></func>
                  ...

               </app>

          <Applications>

       </fields>

       <!--
            Errors description. See Mioga2::tools::args_checker->ac_XMLifyErrors
         -->
    

       <TeamsList>

       <!--
           List of team invited is this group LargeList description. See Mioga2::LargeList::RowidList
         -->

       </TeamsList>

       <UsersList>

       <!--
           List of user invited in this group LargeList description. See Mioga2::LargeList::RowidList
         -->

       </UsersList>

    </ModifyProfile>

=head3 Form actions and fields
    
    - profile_create_act : Try to create a profile

      Arguments :
         name                type                  description
       ---------------------------------------------------------------------
       ident       :  not empry string          :  the profile rowid
       select_function_<app_ident>_<func_ident> :  the selected applications and functions

    - act_add_user     : Run invited users selection


    - act_add_team     : Run invited teams selection


    - select_users_back : Return From Invited users list selection.

      Arguments :
         name                 type                  description
       ---------------------------------------------------------------------
       rowids      :  not empty valid ident  :  a comma separated list of selected user rowid


    - select_teams_back : Return Invited teams list selected.

      Arguments :
         name                 type                  description
       ---------------------------------------------------------------------
       rowids      :  not empty valid ident  :  a comma separated list of selected team rowid

=cut

# ============================================================================

sub ModifyProfile {
    my ($self, $context) = @_;

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

    my $group = $context->GetGroup();
    my $dbh   = $config->GetDBH();
    my $user  = $context->GetUser();

	my $values = {};
    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my $params = [ [ [ 'select_users_back', 'select_teams_back', 'profile_modify_act', 'act_add_user', 'act_add_team' ], 'allow_empty' ], [ ['rowid'], 'allow_empty', 'want_int' ], [ ['rowids'], 'allow_empty' ], ];
    my ($chkvalues, $errors) = ac_CheckArgs($context, $params);

    if (@$errors) {
        throw Mioga2::Exception::Application("AnimGroup::CreateProfile", __("Wrong argument value.") . Dumper $errors);
    }

    if (st_ArgExists($context, 'profile_modify_act')) {

        Mioga2::LargeList::CheckArgs($context);

        try {

            $self->InitializeProfileValues($context, $values),

              BeginTransaction($dbh);

            try {

                $self->DeleteAllProfileFunctions($config, $values->{rowid}, $group, $user);

                my $anim_prof = $self->GetAnimationProfileId($config, $group->GetRowid());

                foreach my $apps (keys %{ $values->{apps} }) {

                    my $appdesc = new Mioga2::AppDesc($config, rowid => $apps);

                    if (    $group->GetRowid() eq $config->GetAdminId()
                        and $values->{rowid} == $anim_prof
                        and $appdesc->GetIdent() eq 'Admin')
                    {
                        next;
                    }

                    foreach my $func (@{ $values->{apps}->{$apps} }) {

                        if (    $values->{rowid} == $anim_prof
                            and $appdesc->GetIdent() =~ /^Anim/
                            and $func eq 'Animation')
                        {
                            next;
                        }

                        my $func_id = $appdesc->GetFunctionsIdsFromIdent($func);

                        ProfileCreateFunction($dbh, $values->{rowid}, $func_id);
                    }
                }

                ProfileClearGroups($dbh, $values->{rowid}, $group->GetDefaultProfileId());

                foreach my $grp (@{ $values->{users} }, @{ $values->{teams} }) {
                    ProfileAddGroup($dbh, $values->{rowid}, $group->GetRowid(), $grp);
                }
            }
            otherwise {
                my $err = shift;

                RollbackTransaction($dbh);

                $err->throw;
            };

            EndTransaction($dbh);
        }

        catch Mioga2::Exception::Simple with {
            my $err = shift;
            push @$errors, [ $err->as_string, ['__modify__'] ];
        };

        if (!@$errors) {
            $session->{__internal}->{message}{type} = 'info';
            $session->{__internal}->{message}{text} = __('Profile successfully modified.');
            my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
            $content->SetContent($session->{AnimGroup}->{Profile}->{referer});
            return $content;
        }
    }

    elsif (st_ArgExists($context, 'select_users_back')) {
        $values = $session->{AnimGroup}->{Profile};

        if (exists $context->{args}->{rowids}) {
            foreach my $rowid (split(/,/, $context->{args}->{rowids})) {
                if (!grep { $rowid == $_ } @{ $values->{users} }) {
                    push @{ $values->{users} }, $rowid;
                }
            }
        }

    }

    elsif (st_ArgExists($context, 'select_teams_back')) {
        $values = $session->{AnimGroup}->{Profile};

        if (exists $context->{args}->{rowids}) {
            foreach my $rowid (split(/,/, $context->{args}->{rowids})) {
                if (!grep { $rowid == $_ } @{ $values->{teams} }) {
                    push @{ $values->{teams} }, $rowid;
                }
            }
        }

    }

    elsif (st_ArgExists($context, 'act_add_user')) {
        $self->InitializeProfileValues($context, $values);
        return $self->LaunchSelectMultipleUserNotInList($context, $session->{AnimGroup}->{Profile}->{users});
    }

    elsif (st_ArgExists($context, 'act_add_team')) {
        $self->InitializeProfileValues($context, $values);
        return $self->LaunchSelectMultipleTeamNotInList($context, $session->{AnimGroup}->{Profile}->{teams});
    }

    elsif (exists $context->{args}->{rowid}) {
        delete $session->{AnimGroup}->{Profile};

        $values = {
            referer => $context->GetReferer(),
            rowid   => $context->{args}->{rowid},
            teams   => $self->GetTeamsForProfile($config, $context->{args}->{rowid}),
            users   => $self->GetUsersForProfile($config, $context->{args}->{rowid}),
        };

        $session->{AnimGroup}->{Profile}->{referer} =~ s/\?(.*)$//g;
    }

    $self->InitializeProfileValues($context, $values);

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<ModifyProfile>";

    $xml .= "<referer>" . st_FormatXMLString($session->{AnimGroup}->{Profile}->{referer}) . "</referer>";

    $xml .= $context->GetXML();

    my $anim_prof = $self->GetAnimationProfileId($config, $group->GetRowid());

    my $is_admin = 0;
    my $is_anim  = 0;

    if ($values->{rowid} == $anim_prof) {
        $is_anim = 1;

        if ($group->GetRowid() == $config->GetAdminId()) {
            $is_admin = 1;
        }
    }

    $xml .= $self->GetXMLProfileApplications($context, $is_anim, $is_admin);

    $xml .= "<fields>";

    $xml .= $self->GetXMLSelectedFunc($context);

    $xml .= "</fields>";

    $xml .= ac_XMLifyErrors($errors);

    $xml .= "<UsersList>";
    my $user_sll = new Mioga2::LargeList::RowidList($context, \%profiles_users_tiny_sll_desc, $values->{users});
    $xml .= $user_sll->Run($context);
    $xml .= "</UsersList>";

    $xml .= "<TeamsList>";
    my $team_sll = new Mioga2::LargeList::RowidList($context, \%profiles_teams_tiny_sll_desc, $values->{teams});
    $xml .= $team_sll->Run($context);
    $xml .= "</TeamsList>";

    $xml .= "</ModifyProfile>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});
    $content->SetContent($xml);

    return $content;
}

# ============================================================================

=head2 AppList ()

    Application available for current user in current group.


=head3 Generated XML :
    
    <AppList>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <referer>URL of calling application</referer>

	   <!-- 
           List of applications
	     -->
       <app ident="application ident" name="application name"/>
       <app ident="application ident" name="application name"/>
       ...


    </AppList>

=cut

# ============================================================================

sub AppList {
    my ($self, $context) = @_;

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

    my $user  = $context->GetUser();
    my $group = $context->GetGroup();

    my $apps = new Mioga2::AppDescList(
        $config,
        user_id  => $user->GetRowid(),
        group_id => $group->GetRowid(),
        app_type => "normal"
    );

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<AppList>";

    $xml .= $context->GetXML();

    my $idents = $apps->GetIdents();
    my $names  = $apps->GetLocaleNames($user);

    my $nb_app = @$idents;

    for (my $i = 0 ; $i < $nb_app ; $i++) {
        $xml .= '<app ident="' . st_FormatXMLString($idents->[$i]) . '" name="' . st_FormatXMLString($names->[$i]) . '"/>';
    }

    $xml .= "<referer>" . st_FormatXMLString($context->GetReferer()) . "</referer>";

    $xml .= "</AppList>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});
    $content->SetContent($xml);

    return $content;
}

# ============================================================================

=head2 MemberList ()

    Display the list of current group members.

=head3 Generated XML :
    
    <MemberList>
    
       <miogacontext>See Mioga2::RequestContext</miogacontext>

       <!--
            SimpleLargeList description. See Mioga2::SimpleLargeList
         -->
    
    </MemberList>

=head3 List fields :

    member_group_list_sll : List of user member of current group
    
    Available fields :
        name             type       description
    ----------------------------------------------------------------
      rowid           : rowid       : the user rowid 
      ident           : normal      : the user ident
      name            : normal      : the user name

=head3 Form actions and fields

    SimpleLargeList actions. See Mioga2::SimpleLargeList.

=cut

# ============================================================================

sub MemberList {
    my ($self, $context) = @_;

    my $group = $context->GetGroup();

    # ------------------------------------------------------
    # Generate the XML
    # ------------------------------------------------------

    my $sll = new Mioga2::SimpleLargeList($context, \%member_list_sll_desc);

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<MemberList>";

    $xml .= $context->GetXML();

    $xml .= $sll->Run($context, [ $group->GetRowid(), $group->GetRowid() ]);

    $xml .= "</MemberList>";

    my $content = new Mioga2::Content::XSLT($context, stylesheet => $self->{stylesheet}, locale_domain => $self->{locale_domain});
    $content->SetContent($xml);

    return $content;
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 GetXMLUserStatus ()

	Return XML listing available User status

       <UserStatus>
          <status>active</status>
          <status>disabled</status>
          ...
       </UserStatus>

=cut

# ============================================================================

sub GetXMLUserStatus {
    my ($self, $context) = @_;

    my $xml = "<UserStatus>";

    $xml .= "<status>active</status>";
    $xml .= "<status>disabled</status>";

    $xml .= "</UserStatus>";
    return $xml;
}

# ============================================================================

=head2 GetXMLGroupProfiles ()

	Return XML listing available group profiles status

	   <ProfileList>
	
          <profile rowid="profile rowid">profile ident</profile>
	      <profile rowid="profile rowid">profile ident</profile>
	      ...

       </ProfileList>

=cut

# ============================================================================

sub GetXMLGroupProfiles {
    my ($self, $context) = @_;

    my $config = $context->GetConfig();

    my $res = SelectMultiple($config->GetDBH(), "SELECT * FROM m_profile WHERE group_id = " . $context->GetGroup()->GetRowid());

    my $xml = "<ProfileList>";

    foreach my $prof (@$res) {
        $xml .= qq|<profile rowid="$prof->{rowid}">| . st_FormatXMLString($prof->{ident}) . qq|</profile>|;
    }

    $xml .= "</ProfileList>";
    return $xml;
}

# ============================================================================

=head2 GetXMLResourceStatus ()

	Return XML listing available Resource status

       <ResourceStatus>
          <status>active</status>
          <status>disabled</status>
          ...
       </ResourceStatus>

=cut

# ============================================================================

sub GetXMLResourceStatus {
    my ($self, $context) = @_;

    my $xml = "<ResourceStatus>";

    $xml .= "<status>active</status>";
    $xml .= "<status>disabled</status>";

    $xml .= "</ResourceStatus>";

    return $xml;
}

# ============================================================================

=head2 GetXMLProfileApplications ()

	Return XML listing available applications and functions for current group

		<Applications>

            <app rowid="app rowid" ident="app ident">
                <func>Function ident</func>;
                <func>Function ident</func>;
                ...
            </app>

            ...

 		</Applications>

=cut

# ============================================================================

sub GetXMLProfileApplications {
    my ($self, $context, $is_anim, $is_admin) = @_;

    my $config   = $context->GetConfig();
    my $group    = $context->GetGroup();
    my $group_id = $group->GetRowid();

    my $xml = "<Applications>";

    my $appdesc_list = new Mioga2::AppDescList($config, group_id => $group_id);
    my $appidents = $appdesc_list->GetIdents();

    foreach my $app (@$appidents) {
        next if ($app eq 'Admin' and $is_anim and $is_admin);

        my $appdesc = new Mioga2::AppDesc($config, ident => $app);

        my $rowid = $appdesc->GetRowid();

        $xml .= qq|<app rowid="$rowid" ident="$app" |;
        $xml .= "description=\"" . st_FormatXMLString($appdesc->GetLocaleDescription($group)) . "\" ";
        $xml .= "name=\"" . st_FormatXMLString($appdesc->GetLocaleName($group)) . "\">";

        my $funcs = $appdesc->GetFunctions();

        foreach my $function (@$funcs) {
            next if ($app =~ /^Anim/ and $is_anim and $function eq 'Animation');
            $xml .= "<func ident=\"$function\">" . st_FormatXMLString($appdesc->GetLocaleForFunction($function, $group)) . "</func>";
        }

        $xml .= "</app>";
    }

    $xml .= "</Applications>";

    return $xml;
}

# ============================================================================

=head2 GetXMLSelectedFunc ()

	Return XML currently selected functions foreach applications

          <Applications>

               <app rowid="Application rowid" ident="Application Ident"  
                    description="Application description"
		            name="Localized application name">
			
 	              <func ident="Function identifier"></func>
 	              <func ident="Function identifier"></func>
                  ...

		       </app>

          <Applications>

=cut

# ============================================================================

sub GetXMLSelectedFunc {
    my ($self, $context) = @_;

    my $session  = $context->GetSession();
    my $config   = $context->GetConfig();
    my $group_id = $context->GetGroup()->GetRowid();

    my $xml = "";

    # Initialize from current arguments

    if (exists $context->{args}->{profile_create_act}) {
        my $curval;

        foreach my $key (keys %{ $context->{args} }) {
            if ($key =~ /^select_function_(\d+)_(.*)$/) {
                my ($app_id, $func_ident) = ($1, $2);

                push @{ $curval->{$app_id} }, $func_ident;
            }
        }

        $xml .= "<Applications>";

        foreach my $key (keys %$curval) {

            my $appdesc = new Mioga2::AppDesc($config, rowid => $key);
            my $app_ident = $appdesc->GetIdent();

            $xml .= qq|<app rowid="$key" ident="$app_ident">|;

            foreach my $func (@{ $curval->{$key} }) {
                $xml .= "<func>" . st_FormatXMLString($func) . "</func>";
            }

            $xml .= "</app>";

        }

        $xml .= "</Applications>";

    }

    elsif (exists $session->{AnimGroup}->{Profile}->{rowid}) {
        my $dbh     = $config->GetDBH();
        my $prof_id = $session->{AnimGroup}->{Profile}->{rowid};

        my $profile = SelectSingle($dbh, "SELECT * FROM m_profile WHERE rowid = $prof_id");
        $xml .= "<ident>" . st_FormatXMLString($profile->{ident}) . "</ident>";
        $xml .= "<rowid>$prof_id</rowid>";

        my $sql = "SELECT m_function.ident AS function, m_profile_function.* " . "FROM m_profile_function, m_function " . "WHERE m_profile_function.profile_id = $prof_id AND " . "      m_function.rowid = m_profile_function.function_id AND " . "      m_function.application_id = ?";

        my $appdesc_list = new Mioga2::AppDescList($config, group_id => $group_id);
        my $appidents = $appdesc_list->GetIdents();

        $xml .= "<Applications>";

        foreach my $app (@$appidents) {
            my $appdesc = new Mioga2::AppDesc($config, ident => $app);
            my $rowid = $appdesc->GetRowid();

            $xml .= qq|<app rowid="$rowid" ident="$app">|;

            my $res = SelectMultiple($dbh, $sql, [$rowid]);

            foreach my $function (@$res) {
                $xml .= "<func>" . st_FormatXMLString($function->{function}) . "</func>";
            }

            $xml .= "</app>";
        }

        $xml .= "</Applications>";

    }

    return $xml;
}

# ============================================================================

=head2 GetXMLThemeLang ()

	Return XML listing themes/lang

	    <Themes>
	       <theme rowid="theme rowid" ident="theme ident" name="theme name" />
              ...
		</Themes>
		<Languages>
	          <lang rowid="lang rowid" ident="lang ident" locale="lang locale"/>
              ...
		</Languages>

=cut

# ============================================================================

sub GetXMLThemeLang {
    my ($self, $context) = @_;

    my $config   = $context->GetConfig();
    my $dbh      = $config->GetDBH();
    my $mioga_id = $config->GetMiogaId();

    my $xml = "<Themes>";
    my $res = SelectMultiple($dbh, "SELECT * FROM m_theme WHERE mioga_id = $mioga_id");
    foreach my $theme (@$res) {
        $xml .= qq|<theme rowid="$theme->{rowid}" ident="$theme->{ident}" name="$theme->{name}" />|;
    }
    $xml .= "</Themes>";

    $xml .= "<Languages>";
    $res = SelectMultiple($dbh, "SELECT * FROM m_lang");
    foreach my $lang (@$res) {
        $xml .= qq|<lang rowid="$lang->{rowid}" ident="$lang->{ident}" locale="$lang->{locale}" />|;
    }
    $xml .= "</Languages>";

    return $xml;
}

# ============================================================================
#
# LaunchSelectMultipleUsersMemberOfGroup ()
#
#	Launch the select multiple users member of the current group list
#
#	Build Select application URI
#	And return a REDIRECT content.
#
# ============================================================================

sub LaunchSelectMultipleUsersMemberOfGroup {
    my ($self, $context) = @_;

    my $config = $context->GetConfig();

    my $group   = $context->GetGroup();
    my $cur_uri = $context->GetURI();

    my $uri = new Mioga2::URI(
        $config,
        group       => $group->GetIdent(),
        public      => 0,
        application => 'Select',
        method      => 'SelectMultipleUsersMemberOfGroup',
        args        => {
            action  => 'select_users_back',
            referer => $cur_uri->GetURI()
        }
    );

    my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
    $content->SetContent($uri->GetURI());

    return $content;
}

# ============================================================================
#
# LaunchSelectMultipleGroupsMemberOfGroup ()
#
#	Launch the select multiple groups member of the current group list
#
#	Build Select application URI
#	And return a REDIRECT content.
#
# ============================================================================

sub LaunchSelectMultipleTeamsMemberOfGroup {
    my ($self, $context) = @_;

    my $config = $context->GetConfig();

    my $group   = $context->GetGroup();
    my $cur_uri = $context->GetURI();

    my $uri = new Mioga2::URI(
        $config,
        group       => $group->GetIdent(),
        public      => 0,
        application => 'Select',
        method      => 'SelectMultipleTeamsMemberOfGroup',
        args        => {
            action  => 'select_teams_back',
            referer => $cur_uri->GetURI()
        }
    );

    my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
    $content->SetContent($uri->GetURI());

    return $content;
}

# ============================================================================
#
# LaunchSelectMultipleUsers ()
#
#	Launch the select multiple user list.
#
#	Store current form values into session,
#	Build Select application URI
#	And return a REDIRECT content.
#
# ============================================================================

sub LaunchSelectMultipleUsers {
    my ($self, $context) = @_;

    my $config = $context->GetConfig();

    my $group   = $context->GetGroup();
    my $cur_uri = $context->GetURI();

    my $uri = new Mioga2::URI(
        $config,
        group       => $group->GetIdent(),
        public      => 0,
        application => 'Select',
        method      => 'SelectMultipleUsersNotInGroup',
        args        => {
            action  => 'select_users_back',
            referer => $cur_uri->GetURI()
        }
    );

    my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
    $content->SetContent($uri->GetURI());

    return $content;
}

# ============================================================================
#
# LaunchSelectMultipleTeams ()
#
#	Launch the select multiple team list.
#
#	Store current form values into session,
#	Build Select application URI
#	And return a REDIRECT content.
#
# ============================================================================

sub LaunchSelectMultipleTeams {
    my ($self, $context) = @_;

    my $config = $context->GetConfig();

    my $group   = $context->GetGroup();
    my $cur_uri = $context->GetURI();

    my $uri = new Mioga2::URI(
        $config,
        group       => $group->GetIdent(),
        public      => 0,
        application => 'Select',
        method      => 'SelectMultipleTeamsNotInGroup',
        args        => {
            action  => 'select_teams_back',
            referer => $cur_uri->GetURI(),
        }
    );

    my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
    $content->SetContent($uri->GetURI());

    return $content;
}

# ============================================================================
#
# LaunchSelectMultipleResources ()
#
#	Launch the select multiple resource list.
#
#	Store current form values into session,
#	Build Select application URI
#	And return a REDIRECT content.
#
# ============================================================================

sub LaunchSelectMultipleResources {
    my ($self, $context) = @_;

    my $config = $context->GetConfig();

    my $group   = $context->GetGroup();
    my $cur_uri = $context->GetURI();

    my $uri = new Mioga2::URI(
        $config,
        group       => $group->GetIdent(),
        public      => 0,
        application => 'Select',
        method      => 'SelectMultipleResourcesNotInGroup',
        args        => {
            action  => 'select_resources_back',
            referer => $cur_uri->GetURI()
        }
    );

    my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
    $content->SetContent($uri->GetURI());

    return $content;
}

# ============================================================================
#
# LaunchSelectMultipleTeamNotInList ()
#
#	Launch the select multiple team list not in the given list.
#
#	Store current form values into session,
#	Build Select application URI
#	And return a REDIRECT content.
#
# ============================================================================

sub LaunchSelectMultipleTeamNotInList {
    my ($self, $context, $team_ids, $my_id) = @_;

    Mioga2::LargeList::CheckArgs($context);

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

    my $group   = $context->GetGroup();
    my $cur_uri = $context->GetURI();

    my $referer = $cur_uri->GetURI();
    $referer =~ s/\?.*$//;

    my @list = @$team_ids;
    if (defined $my_id) {
        push @list, $my_id;
    }

    my $uri = new Mioga2::URI(
        $config,
        group       => $group->GetIdent(),
        public      => 0,
        application => 'Select',
        method      => 'SelectMultipleTeamsMemberOfGroup',
        args        => {
            action  => 'select_teams_back',
            referer => $referer,
            rowids  => join(',', @list),
        },
    );

    my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
    $content->SetContent($uri->GetURI());

    return $content;
}

# ============================================================================
#
# LaunchSelectMultipleUserNotInList ()
#
#	Launch the select multiple user list not in the given list.
#
#	Store current form values into session,
#	Build Select application URI
#	And return a REDIRECT content.
#
# ============================================================================

sub LaunchSelectMultipleUserNotInList {
    my ($self, $context, $user_ids) = @_;

    Mioga2::LargeList::CheckArgs($context);

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

    my $group   = $context->GetGroup();
    my $cur_uri = $context->GetURI();

    my $referer = $cur_uri->GetURI();
    $referer =~ s/\?.*$//;

    my @not_selectable;

    push @not_selectable, (@$user_ids, $group->GetAnimId());

    my $uri = new Mioga2::URI(
        $config,
        group       => $group->GetIdent(),
        public      => 0,
        application => 'Select',
        method      => 'SelectMultipleUsersMemberOfGroup',
        args        => {
            action  => 'select_users_back',
            referer => $referer,
            rowids  => join(',', @not_selectable),
        },
    );

    my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
    $content->SetContent($uri->GetURI());

    return $content;
}

# ============================================================================
#
# DisinviteUserHook ()
#
#	Disinvite User hook
#
# ============================================================================

sub DisinviteUserHook {
    my ($context, $rowid) = @_;

    my $config = $context->GetConfig();
    my $group  = $context->GetGroup();

    if ($group->GetAnimId() == $rowid) {
        return;
    }

    GroupRevokeUser($config, $group->GetRowid(), $rowid);
}

# ============================================================================
#
# DisinviteTeamHook ()
#
#	Disinvite Team hook
#
# ============================================================================

sub DisinviteTeamHook {
    my ($context, $rowid) = @_;

    my $config = $context->GetConfig();
    my $group  = $context->GetGroup();

    if ($group->GetAnimId() == $rowid) {
        return;
    }

    GroupRevokeTeam($config, $group->GetRowid(), $rowid);
}

# ============================================================================
#
# DeleteResourceHook ()
#
#	Delete resource Group hook
#
# ============================================================================

sub DeleteResourceHook {
    my ($context, $rowid) = @_;

    my $config = $context->GetConfig();
    my $group  = $context->GetGroup();

    if ($group->GetAnimId() == $rowid) {
        return;
    }

    ResourceRevokeGroup($config, $rowid, $group->GetRowid());
}

# ============================================================================
#
# DeleteProfileHook ()
#
#	Delete profile hook
#
# ============================================================================

sub DeleteProfileHook {
    my ($context, $rowid) = @_;

    my $config = $context->GetConfig();
    my $group  = $context->GetGroup();

    ProfileDelete($config->GetDBH(), $rowid, $group->GetRowid());
}

# ============================================================================
#
# DeleteProfileGroupHook ()
#
#	Delete profile hook
#
# ============================================================================

sub DeleteProfileGroupHook {
    my ($context, $rowid) = @_;

    my $config = $context->GetConfig();
    my $group  = $context->GetGroup();

    if ($group->GetType() !~ /_user$/) {
        GroupRevokeUser($config, $group->GetRowid(), $rowid);
    }
    else {
        GroupRevokeTeam($config, $group->GetRowid(), $rowid);
    }
}

# ============================================================================
#
# ChangeGroupProfile ()
#
#	treat user/group profile modification
#
# ============================================================================

sub ChangeGroupProfile {
    my ($self, $context) = @_;
    my @rowids;

    my $config = $context->GetConfig();
    my $dbh    = $config->GetDBH();
    my $group  = $context->GetGroup();

    foreach my $key (keys %{ $context->{args} }) {
        if ($key =~ /^select_change_profile_(\d+)$/) {
            push @rowids, $1;
        }
    }

    foreach my $grp_id (@rowids) {
        ProfileAddGroup($dbh, $context->{args}->{profile_id}, $group->GetRowid(), $grp_id);
    }
}

# ============================================================================
#
# InitializeProfileValues ()
#
#	Initialize values for profile
#
# ============================================================================

sub InitializeProfileValues {
    my ($self, $context, $values) = @_;

    my $config  = $context->GetConfig();
    my $session = $context->GetSession();

    foreach my $key (qw(users teams rowid referer ident)) {
        if (exists $values->{$key}) {
            next;
        }
        elsif (exists $context->{args}->{$key}) {
            $values->{$key} = $context->{args}->{$key};
        }
        elsif (exists $session->{AnimGroup}->{Profile}->{$key}) {
            $values->{$key} = $session->{AnimGroup}->{Profile}->{$key};
        }
    }

    my $found = 0;
    my $apps;
    foreach my $key (keys %{ $context->{args} }) {
        if ($key =~ /^select_function_(\d+)_(.*)$/) {
            $found = 1;

            my ($app_id, $func_ident) = ($1, $2);

            if (!exists $apps->{$app_id}) {
                $values->{apps}->{$app_id} = [];
            }

            push @{ $apps->{$app_id} }, $func_ident;
        }
    }

    if ($found) {
        $values->{apps} = $apps;
    }
    else {
        $values->{apps} = $session->{AnimGroup}->{Profile}->{apps};
    }

    $session->{AnimGroup}->{Profile} = $values;
}

# ============================================================================
#
# ModifyThemeAndLang ()
#
#	commit themes and lang selection in database.
#
# ============================================================================

sub ModifyThemeAndLang {
    my ($self, $config, $values) = @_;

    GroupModify($config, $values);
}

# ============================================================================
#
# ModifyDefaultProfile ()
#
#	commit default profile modification in database.
#
# ============================================================================

sub ModifyDefaultProfile {
    my ($self, $config, $group, $profile_id) = @_;

    GroupModify($config, { rowid => $group->GetRowid(), default_profile_id => $profile_id });
}

# ============================================================================
#
# GetUsersForProfile ()
#
#	Return the list of user_id member of the given profile
#
# ============================================================================

sub GetUsersForProfile {
    my ($self, $config, $profile_id) = @_;

    my $res = SelectMultiple($config->GetDBH(), "SELECT m_profile_group.* FROM m_profile_group, m_user_base WHERE m_profile_group.profile_id = $profile_id AND m_user_base.rowid = m_profile_group.group_id");

    if (!defined $res) {
        return [];
    }

    my @users = map { $_->{group_id} } @$res;
    return \@users;
}

# ============================================================================
#
# GetTeamsForProfile ()
#
#	Return the list of team_id member of the given profile
#
# ============================================================================

sub GetTeamsForProfile {
    my ($self, $config, $profile_id) = @_;

    my $res = SelectMultiple($config->GetDBH(), "SELECT m_profile_group.* FROM m_profile_group, m_group WHERE m_profile_group.profile_id = $profile_id AND m_group.rowid = m_profile_group.group_id");

    if (!defined $res) {
        return [];
    }

    my @teams = map { $_->{group_id} } @$res;
    return \@teams;
}

# ============================================================================
#
# GetSelectedApps ()
#
#	Return the list of selected apps for group
#
# ============================================================================

sub GetSelectedApps {
    my ($self, $config, $group_id) = @_;

    my $mioga_id = $config->GetMiogaId();
    my $dbh      = $config->GetDBH();

    return SelectMultiple($dbh, "select rowid from m_application_group_status where mioga_id = $mioga_id AND group_id = $group_id and status > 0");
}

# ============================================================================
#
# GetAnimationProfileId ()
#
#	Return the animation profile rowid
#
# ============================================================================

sub GetAnimationProfileId {
    my ($self, $config, $group_id) = @_;

    my $mioga_id = $config->GetMiogaId();
    my $dbh      = $config->GetDBH();

    my $res = SelectSingle($dbh, "select m_profile.rowid from m_profile, m_profile_group, m_group_base " . "where m_profile.group_id = $group_id AND " . "      m_group_base.rowid = $group_id AND " . "      m_profile_group.profile_id = m_profile.rowid AND " . "      m_profile_group.group_id = m_group_base.anim_id");

    return $res->{rowid};
}

# ============================================================================
#
# DeleteAllProfileFunctions ()
#
#	Delete All Functions from profile except anim* and admin if current group
#	is admin or current user is animotor in the current group.
#
# ============================================================================

sub DeleteAllProfileFunctions {
    my ($self, $config, $prof_id, $group, $user) = @_;

    my $anim_prof = $self->GetAnimationProfileId($config, $group->GetRowid);

    my $sql = "DELETE FROM m_profile_function USING m_application, m_function WHERE profile_id = $prof_id";

    if ($anim_prof == $prof_id) {
        if ($group->GetRowid() == $config->GetAdminId()) {
            $sql .= " AND m_application.ident != 'Admin' ";
        }

        $sql .= " AND  (m_application.ident !~ '^Anim' or m_function.ident != 'Animation') " . " AND m_function.application_id = m_application.rowid" . " AND m_profile_function.function_id = m_function.rowid";
    }

    my $mioga_id = $config->GetMiogaId();
    my $dbh      = $config->GetDBH();

    ExecSQL($config->GetDBH(), $sql);
}

##############################################################################
#
# Web Services
#
##############################################################################

# ============================================================================
#
# GetGroups ()
#
# get all groups current user is in.
#
# ============================================================================

sub GetGroups {
    my ($self, $context) = @_;

    my $data    = $self->{data};
    my $config  = $context->GetConfig;
    my $user    = $context->GetUser;
    my $groups  = $user->GetExpandedGroups;
    my $user_id = $user->GetRowid;
    my $idents  = $groups->GetIdents;
    my $rowids  = $groups->GetRowids;

    my $extra_infos = $context->{args}->{extra_infos};

    $data->{group} = [];
    for (my $i = 0 ; $i < @$rowids ; $i++) {
        my $group_infos = {
            ident => $idents->[$i],
            rowid => $rowids->[$i],
        };

        if ($extra_infos) {
            my $apps = Mioga2::AppDescList->new(
                $config,
                user_id  => $user_id,
                group_id => $rowids->[$i],
                app_type => "normal"
            );

            my $app_idents = $apps->GetIdents;
            my $app_names  = $apps->GetLocaleNames($user);
            my $nb_app     = @$idents;
            my $app_infos  = [];

            for (my $j = 0 ; $j < $nb_app ; $j++) {
                push @{$app_infos},
                  {
                    ident => $app_idents->[$j],
                    name  => $app_names->[$j],
                  };
            }
            $group_infos->{application} = $app_infos;
        }

        push @{ $data->{group} }, $group_infos;
    }

    return $data;
}

# ============================================================================
#
# GetAppList ()
#
# retrieve application list for group
#
# ============================================================================

sub GetAppList {
    my ($self, $context) = @_;

    my $data   = $self->{data};
    my $config = $context->GetConfig;
    my $user   = $context->GetUser;
    my $args   = $context->{args};
    my $group_id;

    if ($args->{group_id} && $args->{group_id} =~ /^\d+$/) {
        $group_id = $args->{group_id};
    }
    else {
        $group_id = $context->GetGroup->GetRowid;
    }

    my $apps = Mioga2::AppDescList->new(
        $config,
        user_id  => $user->GetRowid,
        group_id => $group_id,
        app_type => "normal"
    );

    my $idents = $apps->GetIdents;
    my $names  = $apps->GetLocaleNames($user);
    my $nb_app = @$idents;

    $data->{application} = [];
    for (my $i = 0 ; $i < $nb_app ; $i++) {
        push @{ $data->{application} },
          {
            ident => $idents->[$i],
            name  => $names->[$i],
          };
    }

    return $data;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


