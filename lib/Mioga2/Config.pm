# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Config.pm: Access class to the current Mioga instance configuration.

=head1 DESCRIPTION

This module permits to access to the current Mioga instance
configuration parameters.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Config;
use strict;

use Locale::TextDomain::UTF8 'config';

use Error qw(:try);
use Data::Dumper;
use DBI;
use Mioga2::XML::Simple;
use Mioga2::Exception::Simple;
use Mioga2::MiogaConf;
use Mioga2::Old::User;
use Mioga2::tools::database;

my $debug = 0;

# ============================================================================

=head2 new ($miogaconf, $mioga_ident)

	Create the Mioga2::Config object from Mioga2::MiogaConf and Mioga instance indent..
	return a new Mioga2::Config object.

=cut

# ============================================================================

sub new {
	my ($class, $miogaconf, $mioga_ident) = @_;

	my $self = {};

	bless($self, $class);

	$self->{miogaconf} = $miogaconf;
	$self->{db} = $miogaconf->{db} if (exists ($miogaconf->{db}));

	$self->LoadMiogaParams($mioga_ident);

	return $self;
}
# ============================================================================

=head2 GetDBH ()

	return database connection object (DBH)
	
=cut

# ============================================================================

sub GetDBH {
	my ($self) = @_;

	# ------------------------------------------------------
	# Check if the connection is active.
	# If not, reconnect to the database.
	# ------------------------------------------------------
	if(!defined $self->{miogaconf}->{dbh}) {
		$self->{miogaconf}->ConnectDBI();
	}
	elsif(! $self->{miogaconf}->{dbh}->ping) {
		$self->{miogaconf}->{dbh}->disconnect;
		$self->{miogaconf}->ConnectDBI();
	}

	$self->{db} = $self->{miogaconf}->{db} if (exists ($self->{miogaconf}->{db}));

	return $self->{miogaconf}->{dbh};
}


#===============================================================================

=head2 GetDBObject

Get Mioga2::Database object

=cut

#===============================================================================
sub GetDBObject {
	my ($self) = @_;

	return ($self->{db});
}	# ----------  end of subroutine GetDBObject  ----------



# ============================================================================

=head2 GetMiogaConfPath ()

	return the path to the Mioga.conf file. 
	
=cut

# ============================================================================

sub GetMiogaConfPath {
	my ($self) = @_;

	return $self->{miogaconf}->GetMiogaConfPath();
}


# ============================================================================

=head2 GetMiogaConf ()

	return the Mioga2::MiogaConf object. 
	
=cut

# ============================================================================

sub GetMiogaConf {
	my ($self) = @_;
	
	return $self->{miogaconf};
}

# ============================================================================

=head2 GetTmpDir ()

	return Mioga temporary folder path.
	
=cut

# ============================================================================

sub GetTmpDir {
	my ($self) = @_;

	return $self->{miogaconf}->GetTmpDir()."/$self->{ident}";
}



# ============================================================================

=head2 GetBinariesDir ()

	return the binaries dir.
	
=cut

# ============================================================================

sub GetBinariesDir {
	my ($self) = @_;

	return $self->{miogaconf}->GetBinariesDir();
}


# ============================================================================

=head2 GetSearchTmpDir ()

	return Mioga search temporary folder path.
	
=cut

# ============================================================================

sub GetSearchTmpDir {
	my ($self) = @_;

	return $self->{miogaconf}->GetSearchTmpDir();
}


# ============================================================================

=head2 GetLargeListDefaultLPP ()

	return the default number of line per page of large lists.
	
=cut

# ============================================================================

sub GetLargeListDefaultLPP {
	my ($self) = @_;

	return $self->{miogaconf}->GetLargeListDefaultLPP();
}

# ============================================================================

=head2 GetDomainName ()

	return domain name of the Mioga server.
	
=cut

# ============================================================================

sub GetDomainName {
	my ($self) = @_;

	return $self->{domain_name};
}

# ============================================================================

=head2 GetMailingListDescription ()

	return mailing list description of the Mioga server.
	
=cut

# ============================================================================

sub GetMailingListDescription {
	my ($self) = @_;

	return $self->{miogaconf}->{mailing_list_description};
}

# ============================================================================

=head2 GetServerName ()

	return Mioga server name.
	
=cut

# ============================================================================

sub GetServerName {
	my ($self) = @_;

	return $self->{miogaconf}->GetServerName();
}


# ============================================================================

=head2 GetSearchPort ()

	return the port of the search daemon.
	
=cut

# ============================================================================

sub GetSearchPort {
	my ($self) = @_;

	return $self->{miogaconf}->{search_port_number};
}




# ============================================================================

=head2 GetProtocol ()

	return Mioga server protocol (http or https).
	
=cut

# ============================================================================

sub GetProtocol {
	my ($self) = @_;

	return $self->{miogaconf}->GetProtocol();
}


# ============================================================================

=head2 AskConfirmForDelete ()

	return true if the config parameter AskConfirmForDelete is set.
	
=cut

# ============================================================================

sub AskConfirmForDelete {
	my ($self) = @_;

	if(exists $self->{askconfirmfordelete}) {
		return 1;
	}
	else {
		return 0;
	}
}



# ============================================================================

=head2 RightToAnimGroupForCreator ()

	return true if the config parameter RightToAnimGroupForCreator is set.
	
=cut

# ============================================================================

sub RightToAnimGroupForCreator {
	my ($self) = @_;

	if(exists $self->{righttoanimgroupforcreator}) {
		return 1;
	}
	else {
		return 0;
	}
}



# ============================================================================

=head2 GetSmtpServer ()

	return 1 if the SMTP Server used by Mioga.
	
=cut

# ============================================================================

sub GetSmtpServer {
	my ($self) = @_;

	return $self->{miogaconf}->GetSmtpServer();
}



# ============================================================================

=head2 GetHolidays ()

	return a hash with two entry :
	
	fixed  : contains the list of fixed holidays. Each day is represented
	         by a hash with two entry : day and month.

	moving : contains the list of moving holidays. Each day is represented
	         by a hash with two entry : base (a string like EASTER) and the offset.
	
=cut

# ============================================================================

sub GetHolidays {
	my ($self) = @_;

	if(exists $self->{hdays}) {
		return $self->{hdays};
	}

	my %hdays = (fixed => [], moving => []);
	foreach my $date (split(/,(\s*)/, $self->{holidays})) {
		if($date =~ /^(\d+)\/(\d+)$/) {
			push @{$hdays{fixed}}, {day => $1, month => $2};
		}
		elsif($date =~ /^([^\+]+)\+(\d+)$/) {
			push @{$hdays{moving}}, {base => $1, offset => $2};
		}
	}
	
	$self->{hdays} = \%hdays;
	return \%hdays;
}



# ============================================================================

=head2 GetAdminId ()

	return the mioga administrator id.
	
=cut

# ============================================================================

sub GetAdminId {
	my ($self) = @_;

	return $self->{admin_id};
}



# ============================================================================

=head2 GetMiogaId ()

	return the mioga id.
	
=cut

# ============================================================================

sub GetMiogaId {
	my ($self) = @_;

	return $self->{rowid};
}



# ============================================================================

=head2 GetMiogaIdent ()

	return the mioga ident.
	
=cut

# ============================================================================

sub GetMiogaIdent {
	my ($self) = @_;

	return $self->{ident};
}



# ============================================================================

=head2 GetMaxAllowedGroup ()

	return the max_allowed_group fields. undef = no limit.
	
=cut

# ============================================================================

sub GetMaxAllowedGroup {
	my ($self) = @_;

	return $self->{max_allowed_group};
}


# ============================================================================

=head2 GetFilenameEncoding ()

	return the default encoding for filenames
	
=cut

# ============================================================================

sub GetFilenameEncoding {
	my ($self) = @_;

	return $self->{miogaconf}->GetFilenameEncoding();
}

# ============================================================================

=head2 GetBaseURI ()

	return the base uri.
	
=cut

# ============================================================================

sub GetBaseURI {
	my ($self) = @_;

	return $self->{miogaconf}->GetBasePath()."/$self->{ident}";
}



# ============================================================================

=head2 GetImageURI ()

	return the image uri.
	
=cut

# ============================================================================

sub GetImageURI {
	my ($self) = @_;

	return  $self->{miogaconf}->{image_uri};
}


# ============================================================================

=head2 GetHelpURI ()

	return the help uri.
	
=cut

# ============================================================================

sub GetHelpURI {
	my ($self) = @_;

	return  $self->{miogaconf}->GetBasePath()."/$self->{ident}".$self->{miogaconf}->{help_uri};
}



# ============================================================================

=head2 GetBinURI ()

	return the private application uri.
	
=cut

# ============================================================================

sub GetBinURI {
	my ($self) = @_;

	return $self->{miogaconf}->GetBasePath()."/$self->{ident}".$self->{miogaconf}->{bin_uri};
}



# ============================================================================

=head2 GetPubBinURI ()

	return the public application uri.
	
=cut

# ============================================================================

sub GetPubBinURI {
	my ($self) = @_;

	return $self->{miogaconf}->GetBasePath()."/$self->{ident}".$self->{miogaconf}->{pubbin_uri};
}




# ============================================================================

=head2 GetSXMLURI ()

	return the secured xml transaction URI.
	
=cut

# ============================================================================

sub GetSXMLURI {
	my ($self) = @_;

	return $self->{miogaconf}->GetBasePath()."/$self->{ident}".$self->{miogaconf}->{sxml_uri};
}



# ============================================================================

=head2 GetXMLURI ()

	return the authenticated xml transaction URI.
	
=cut

# ============================================================================

sub GetXMLURI {
	my ($self) = @_;

	return $self->{miogaconf}->GetBasePath()."/$self->{ident}".$self->{miogaconf}->{xml_uri};
}



# ============================================================================

=head2 GetPubXMLURI ()

	return the public xml transaction URI.
	
=cut

# ============================================================================

sub GetPubXMLURI {
	my ($self) = @_;

	return $self->{miogaconf}->GetBasePath()."/$self->{ident}".$self->{miogaconf}->{pubxml_uri};
}


# ============================================================================

=head2 GetPrivateURI ()

	return the private webdav uri.
	
=cut

# ============================================================================

sub GetPrivateURI {
	my ($self) = @_;

	return $self->{miogaconf}->GetBasePath()."/$self->{ident}".$self->{miogaconf}->{private_uri};
}



# ============================================================================

=head2 GetPublicURI ()

	return the public webdav uri.
	
=cut

# ============================================================================

sub GetPublicURI {
	my ($self) = @_;

	return $self->{miogaconf}->GetBasePath()."/$self->{ident}".$self->{miogaconf}->{public_uri};
}


# ============================================================================

=head2 GetInstallPath ()

	return the mioga2 install path.
	
=cut

# ============================================================================

sub GetInstallPath {
	my ($self) = @_;

	return $self->{miogaconf}->GetInstallDir();
}



# ============================================================================

=head2 GetBasePath ()

	return the private application path.
	
=cut

# ============================================================================

sub GetBasePath {
	my ($self) = @_;

	return $self->{miogaconf}->GetBasePath();
}

# ============================================================================

=head2 GetDAVBasePath ()

	return the DAV base path.
	
=cut

# ============================================================================

sub GetDAVBasePath {
  my $self = shift;
  
  return $self->{miogaconf}->GetDAVBasePath;
}


# ============================================================================

=head2 GetBinPath ()

	return the private application path.
	
=cut

# ============================================================================

sub GetBinPath {
	my ($self) = @_;

	return $self->{miogaconf}->{bin_uri};
}



# ============================================================================

=head2 GetPubBinPath ()

	return the public application path.
	
=cut

# ============================================================================

sub GetPubBinPath {
	my ($self) = @_;

	return $self->{miogaconf}->{pubbin_uri};
}



# ============================================================================

=head2 GetSXMLPath ()

	return the secured xml transaction path.
	
=cut

# ============================================================================

sub GetSXMLPath {
	my ($self) = @_;

	return $self->{miogaconf}->{sxml_uri};
}



# ============================================================================

=head2 GetXMLPath ()

	return the authenticated xml transaction path.
	
=cut

# ============================================================================

sub GetXMLPath {
	my ($self) = @_;

	return $self->{miogaconf}->{xml_uri};
}



# ============================================================================

=head2 GetPubXMLPath ()

	return the public xml transaction path.
	
=cut

# ============================================================================

sub GetPubXMLPath {
	my ($self) = @_;

	return $self->{miogaconf}->{pubxml_uri};
}



# ============================================================================

=head2 GetPrivatePath ()

	return the private webdav path.
	
=cut

# ============================================================================

sub GetPrivatePath {
	my ($self) = @_;

	return $self->{miogaconf}->{private_uri};
}



# ============================================================================

=head2 GetPublicPath ()

	return the public webdav path.
	
=cut

# ============================================================================

sub GetPublicPath {
	my ($self) = @_;

	return $self->{miogaconf}->{public_uri};
}



# ============================================================================

=head2 GetGroupDataPath ()

	return Mioga group data path.
	
=cut

# ============================================================================

sub GetGroupDataPath {
	my ($self) = @_;

	return $self->{miogaconf}->{group_data};
}


# ============================================================================

=head2 GetMiogaFilesPath ()

	return Mioga the mioga files path.
	
=cut

# ============================================================================

sub GetMiogaFilesPath {
	my ($self) = @_;

	return $self->{miogaconf}->{mioga_files};
}

# ============================================================================

=head2 GetGroupDataDir ()

	return Mioga group data directory.
	
=cut

# ============================================================================

sub GetGroupDataDir {
	my ($self) = @_;

	return $self->{miogaconf}->GetPrivatePath()."/$self->{ident}/".$self->{miogaconf}->{group_data};
}


# ============================================================================

=head2 GetInstallDir ()

	return the install dir uri.
	
=cut

# ============================================================================

sub GetInstallDir {
	my ($self) = @_;

	return $self->{miogaconf}->{install_dir}."/$self->{ident}";
}
# ============================================================================

=head2 GetThemesDir ()

	return the themes dir on web space.
	
=cut

# ============================================================================

sub GetThemesDir {
	my ($self) = @_;

	return $self->{miogaconf}->{install_dir}."/$self->{ident}".$self->{miogaconf}->{base_dir}.$self->{miogaconf}->{themes_dir};
}
# ============================================================================

=head2 GetJslibDir ()

	return the folder for third-party JavaScript libraries on web space.
	
=cut

# ============================================================================

sub GetJslibDir {
	my ($self) = @_;

	return ($self->GetCDNPath () . "/jslib")
}
# ============================================================================

=head2 GetImageDir ()

	return the image dir uri.
	
=cut

# ============================================================================

sub GetImageDir {
	my ($self) = @_;

	return $self->{miogaconf}->{install_dir}."/$self->{ident}".$self->{miogaconf}->{base_dir}.$self->{miogaconf}->{image_dir};
}


# ============================================================================

=head2 GetHelpDir ()

	return the help dir uri.
	
=cut

# ============================================================================

sub GetHelpDir {
	my ($self) = @_;

	return ($self->GetCDNPath () . "/help")
}



# ============================================================================

=head2 GetXSLDir ()

	return the xsl dir.
	
=cut

# ============================================================================

sub GetXSLDir {
	my ($self) = @_;

	return $self->{miogaconf}->{install_dir}."/$self->{ident}".$self->{miogaconf}->{xsl_dir};
}



# ============================================================================

=head2 GetLangDir ()

	return the lang dir.
	
=cut

# ============================================================================

sub GetLangDir {
	my ($self) = @_;

	return $self->{miogaconf}->{install_dir}."/$self->{ident}".$self->{miogaconf}->{lang_dir};
}

# ============================================================================

=head2 GetLocalesPath ()

  return path where locales are installed.
  
=cut

# ============================================================================

sub GetLocalesPath {
  my ($self)  = @_;
  
  return $self->{miogaconf}->GetLocalesPath;
}


# ============================================================================

=head2 GetPrivateDir ()

	return the private dir.
	
=cut

# ============================================================================

sub GetPrivateDir {
	my ($self) = @_;

	return $self->{miogaconf}->{install_dir}."/$self->{ident}".$self->{miogaconf}->{base_dir}.$self->{miogaconf}->{private_dir};
}



# ============================================================================

=head2 GetPublicDir ()

	return the public dir.
	
=cut

# ============================================================================

sub GetPublicDir {
	my ($self) = @_;

	return $self->{miogaconf}->{install_dir}."/$self->{ident}".$self->{miogaconf}->{base_dir}.$self->{miogaconf}->{public_dir};
}

# ============================================================================

=head2 GetWebdavDir ()

	return the webdav dir base.
	
=cut

# ============================================================================

sub GetWebdavDir {
	my ($self) = @_;

	return $self->{miogaconf}->{install_dir}."/$self->{ident}".$self->{miogaconf}->{base_dir};
}


# ============================================================================

=head2 GetAdminIdent ()

	return the admin ident.
	
=cut

# ============================================================================

sub GetAdminIdent {
	my ($self) = @_;

	if(! defined $self->{admin_id}) {
		throw Mioga2::Exception::Simple("Mioga2::Config::GetAdminIdent", __"Admin not defined");
	}

	if(!exists $self->{admin_ident}) {
		my $admin = new Mioga2::Old::User($self, rowid => $self->{admin_id});
		$self->{admin_ident} = $admin->GetIdent();
	}

	return $self->{admin_ident};
}



# ============================================================================

=head2 GetSkeletonDir ()

	return the skeleton dir.
	
=cut

# ============================================================================

sub GetSkeletonDir {
	my ($self) = @_;

	return $self->GetMiogaConf ()->GetInstallDir () . '/' . $self->GetMiogaIdent () . $self->GetMiogaFilesPath () . '/skel';
}


# ============================================================================

=head2 GetMiogaFilesDir ()

	return the mioga files dir.
	
=cut

# ============================================================================

sub GetMiogaFilesDir {
	my ($self) = @_;

	return $self->{miogaconf}->{install_dir}."/$self->{ident}".$self->{miogaconf}->GetMiogaFilesDir();
}


# ============================================================================

=head2 GetDefaultLang ()

	return the default language.
	
=cut

# ============================================================================

sub GetDefaultLang {
	my ($self) = @_;

	my $res = $self->{miogaconf}->{default_values}->{default_lang};
	if(ref($res) eq 'HASH') {
		return '';
	}
	
	return $res;
}



# ============================================================================

=head2 GetDefaultTheme ()

	return the default theme.
	
=cut

# ============================================================================

sub GetDefaultTheme {
	my ($self) = @_;

	my $res = $self->{miogaconf}->{default_values}->{default_theme};
	if(ref($res) eq 'HASH') {
		return '';
	}
	
	return $res;
}


# ============================================================================

=head2 GetDefaultLangId ()

	return the default lang_id for current instance.
	
=cut

# ============================================================================

sub GetDefaultLangId {
	my ($self) = @_;

	return $self->{default_lang_id};
}

# ============================================================================

=head2 GetDefaultThemeId ()

	return the default theme_id for current instance.
	
=cut

# ============================================================================

sub GetDefaultThemeId {
	my ($self) = @_;

	return $self->{default_theme_id};
}

# ============================================================================

=head2 GetDefaultTimezone ()

	return the default timezone.
	
=cut

# ============================================================================

sub GetDefaultTimezone {
	my ($self) = @_;

	return $self->{miogaconf}->{default_values}->{default_timezone};
}



# ============================================================================

=head2 GetDefaultWorkingDays ()

	return the default working days.
	
=cut

# ============================================================================

sub GetDefaultWorkingDays {
	my ($self) = @_;

	return $self->{miogaconf}->{default_values}->{working_days};
}



# ============================================================================

=head2 GetDefaultTimePerDay ()

	return the default working time per day.
	
=cut

# ============================================================================

sub GetDefaultTimePerDay {
	my ($self) = @_;

	return $self->{miogaconf}->{default_values}->{time_per_day} * 60;
}



# ============================================================================

=head2 IsDefaultAutonomous ()

	return true if groups are autonomous by default.
	
=cut

# ============================================================================

sub IsDefaultAutonomous {
	my ($self) = @_;

	return exists $self->{miogaconf}->{default_values}->{autonomous};
}



# ============================================================================

=head2 IsDefaultPublicPart ()

	return true if groups have public parts by default.
	
=cut

# ============================================================================

sub IsDefaultPublicPart {
	my ($self) = @_;

	return exists $self->{miogaconf}->{default_values}->{public_part};
}



# ============================================================================

=head2 IsDefaultMondayFirst ()

	return true if monday is first by default.
	
=cut

# ============================================================================

sub IsDefaultMondayFirst {
	my ($self) = @_;

	return exists $self->{miogaconf}->{default_values}->{monday_first};
}



# ============================================================================

=head2 GetApacheUser ()

	return the apache user.
	
=cut

# ============================================================================

sub GetApacheUser {
	my ($self) = @_;

	return $self->{miogaconf}->GetApacheUser();
}

# ============================================================================

=head2 GetApacheGroup ()

	return the apache group.
	
=cut

# ============================================================================

sub GetApacheGroup {
	my ($self) = @_;

	return $self->{miogaconf}->GetApacheGroup();
}

# ============================================================================

=head2 DumpXML ()

	return true if dump xml is set.
	
=cut

# ============================================================================

sub DumpXML {
	my ($self) = @_;

	return $self->{miogaconf}->DumpXML();
}


# ============================================================================

=head2 IsLocked ()

	return true if instance is locked.
	
=cut

# ============================================================================

sub IsLocked {
	my ($self) = @_;

	if(defined $self->{locked} and $self->{locked}) {
		return 1;
	}

	return 0;
}

# ============================================================================

=head2 GetQuotaSoft ()

	return the soft quota.
	
=cut

# ============================================================================

sub GetQuotaSoft {
	my ($self) = @_;

	return $self->{quota_soft};
}


# ============================================================================

=head2 GetQuotaHard ()

	return the hard quota.
	
=cut

# ============================================================================

sub GetQuotaHard {
	my ($self) = @_;

	return $self->{quota_hard};
}

# ============================================================================

=head2 GetWarningDate ()

	return the warning date.
	
=cut

# ============================================================================

sub GetWarningDate {
	my ($self) = @_;

	return $self->{quota_warning_date};
}

# ============================================================================

=head2 GetDiskUsage ()

	return the disk usage.
	
=cut

# ============================================================================

sub GetDiskUsage {
	my ($self) = @_;

	return $self->{disk_usage};
}



# ============================================================================

=head2 GetPasswordCryptMethod ()

	return the password crypt method.
	
=cut

# ============================================================================

sub GetPasswordCryptMethod {
	my ($self) = @_;

	return $self->{password_crypt_method};
}


# ============================================================================

=head2 LoadPasswordCrypter ()

	return the password crypter according to current password_crypt_method.
	
=cut

# ============================================================================

sub LoadPasswordCrypter {
	my ($self, $cryptage) = @_;

	if(!defined $cryptage) {
		$cryptage = $self->{password_crypt_method};
	}

    $cryptage = lc($cryptage);
	my $crypter;
	eval "
		require Mioga2::Password::Crypter::$cryptage;
		\$crypter = new Mioga2::Password::Crypter::$cryptage;
	";

	if($@) {
		throw Mioga2::Exception::Simple("Mioga2::Config->LoadPasswordCrypter",
        __x("Unsupported password encryption method {method}", method => $cryptage));
	}
	
	return $crypter;
}

# ============================================================================

=head2 UseLDAP ()

	return true if instance is configured to use LDAP
	
=cut

# ============================================================================
sub UseLDAP {
	my ($self) = @_;

	return $self->{use_ldap};
}

# ============================================================================

=head2 CanModifyLdapInstance ()

	return true if user can modify user instance affectation via Mioga2.
	This is equivalent to ldap_access = 1 in m_mioga
	
=cut

# ============================================================================

sub CanModifyLdapInstance {
	my ($self) = @_;

	return (($self->{ldap_access} >= 1) ? 1 : 0);
}

# ============================================================================

=head2 CanModifyLdapPassword ()

	return true if user can modify user password via Mioga2.
	This is equivalent to ldap_access = 2 in m_mioga
	
=cut

# ============================================================================
sub CanModifyLdapPassword {
	my ($self) = @_;

	return (($self->{ldap_access} == 2) ? 1 : 0);
}
# ============================================================================

=head2 GetLDAPHost ()

	return the ldap_host parameter.
	
=cut

# ============================================================================
sub GetLDAPHost {
	my ($self) = @_;

	return $self->{ldap_host};
}
# ============================================================================

=head2 GetLDAPPort ()

	return the ldap_port parameter.
	
=cut

# ============================================================================
sub GetLDAPPort {
	my ($self) = @_;

	return $self->{ldap_port};
}
# ============================================================================

=head2 GetLDAPBindPwd ()

	return the ldap_bind_pwd parameter.
	
=cut

# ============================================================================
sub GetLDAPBindPwd {
	my ($self) = @_;

	return $self->{ldap_bind_pwd};
}
# ============================================================================

=head2 GetLDAPBindDN ()

	return the ldap_bind_dn parameter.
	
=cut

# ============================================================================
sub GetLDAPBindDN {
	my ($self) = @_;

	return $self->{ldap_bind_dn};
}
# ============================================================================

=head2 GetLDAPUserBaseDN ()

	return the ldap_user_base_dn parameter.
	
=cut

# ============================================================================
sub GetLDAPUserBaseDN {
	my ($self) = @_;

	return $self->{ldap_user_base_dn};
}


#===============================================================================

=head2 GetLDAPTranslationTable

Get translation table from Mioga attributes to LDAP attributes

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

A hashref associating Mioga2 attributes to their LDAP equivalent.

=back

=cut

#===============================================================================
sub GetLDAPTranslationTable {
	my ($self) = @_;

	my $table = {};

	for my $attr (qw/ident firstname lastname email pwd desc inst/) {
		$table->{$attr} = $self->{'ldap_user_' . $attr . '_attr'};
	}

	return ($table);
}	# ----------  end of subroutine GetLDAPTranslationTable  ----------


#===============================================================================

=head2 GetAuthFailTimeout

Get timeout value before a user account can be re-enabled after brute-force attacks

=cut

#===============================================================================
sub GetAuthFailTimeout {
	my ($self) = @_;

	return ($self->{auth_fail_timeout});
}	# ----------  end of subroutine GetAuthFailTimeout  ----------


#===============================================================================

=head2 GetPasswordPolicy

Get instance password policy

=cut

#===============================================================================
sub GetPasswordPolicy {
	my ($self) = @_;

	my $data = { };

	for (qw/pwd_min_length pwd_min_letter pwd_min_digit pwd_min_special pwd_min_chcase use_secret_question/) {
		$data->{$_} = $self->{$_};
	}

	return ($data);
}	# ----------  end of subroutine GetPasswordPolicy  ----------


#===============================================================================

=head2 GetXMLPasswordPolicy

Return a XML node containing the instance's password policy

=cut

#===============================================================================
sub GetXMLPasswordPolicy {
	my ($self) = @_;

	my $xml = '<PasswordPolicy>';
	$xml .= "<Length>$self->{pwd_min_length}</Length>";
	$xml .= "<Letters>$self->{pwd_min_letter}</Letters>";
	$xml .= "<Digits>$self->{pwd_min_digit}</Digits>";
	$xml .= "<Special>$self->{pwd_min_special}</Special>";
	$xml .= "<ChCase>$self->{pwd_min_chcase}</ChCase>";
	$xml .= '</PasswordPolicy>';

	return ($xml);
}	# ----------  end of subroutine GetXMLPasswordPolicy  ----------


#===============================================================================

=head2 UseSecretQuestion

Returns true or false wether the instance uses or not secret question protection

=cut

#===============================================================================
sub UseSecretQuestion {
	my ($self) = @_;

	return ($self->{use_secret_question});
}	# ----------  end of subroutine UseSecretQuestion  ----------


#===============================================================================

=head2 DorisCanSetSecretQuestion

Returns true or false wether setting secret question through Doris is allowed or not

=cut

#===============================================================================
sub DorisCanSetSecretQuestion {
	my ($self) = @_;

	return ($self->{doris_can_set_secret_question});
}	# ----------  end of subroutine DorisCanSetSecretQuestion  ----------


#===============================================================================

=head2 GetDefaultGroupId

Get rowid of instance default group

=cut

#===============================================================================
sub GetDefaultGroupId {
	my ($self) = @_;

	return ($self->{default_group_id});
}	# ----------  end of subroutine GetDefaultGroupId  ----------


#===============================================================================

=head2 GetDAVProtocol

Get mod_dav access protocol

=cut

#===============================================================================
sub GetDAVProtocol {
	my ($self) = @_;

	return ($self->{miogaconf}->{dav_protocol});
}	# ----------  end of subroutine GetDAVProtocol  ----------


#===============================================================================

=head2 GetDAVHost

Get mod_dav access host

=cut

#===============================================================================
sub GetDAVHost {
	my ($self) = @_;

	return ($self->{miogaconf}->{dav_host});
}	# ----------  end of subroutine GetDAVHost  ----------


#===============================================================================

=head2 GetDAVPort

Get mod_dav access port

=cut

#===============================================================================
sub GetDAVPort {
	my ($self) = @_;

	return ($self->{miogaconf}->{dav_port});
}	# ----------  end of subroutine GetDAVPort  ----------


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 LoadMiogaParams ($mioga_ident)

	Load Mioga instance parameters from database.
	
=cut

# ============================================================================

sub LoadMiogaParams {
	my ($self, $mioga_ident) = @_;

	my $dbh = $self->GetDBH();
	
	my $res = SelectSingle($dbh, "SELECT m_mioga.* ".
						         "FROM   m_mioga ".
						         "WHERE  m_mioga.ident = '$mioga_ident'");

	if(!defined $res) {
		throw Mioga2::Exception::Simple("Mioga2::Config::LoadMiogaParams", 
        __x("Can not find a mioga instance named {instance}", instance => $mioga_ident));
	}

	foreach my $key (keys %$res) {
		$self->{$key} = $res->{$key};
	}

	if(! -d $self->GetTmpDir()) {
		mkdir($self->GetTmpDir());
		system("chown -R ".$self->{miogaconf}->GetApacheUser().":".$self->{miogaconf}->GetApacheGroup()." ".$self->GetTmpDir());
	}
}


# ============================================================================

=head2 GetLocalesDir ()

=over

=item Retrieve the locales directory to use with gettext.

=item B<parameters :> I<none>.

=item B<return :> the directory to locales as a string.

=back

=cut

# ============================================================================

sub GetLocalesDir {
    my ($self) = @_;

	return $self->{miogaconf}->{install_dir}.$self->{miogaconf}->{locales_dir};
}


sub GetMiogaVersion {
  return $Mioga2::VERSION;
}


#===============================================================================

=head2 GetLoginURI

Get login handler URI

=cut

#===============================================================================
sub GetLoginURI {
	my ($self) = @_;

	return ($self->{miogaconf}->{login_uri});
}	# ----------  end of subroutine GetLoginURI  ----------


#===============================================================================

=head2 GetUserURI

Get user handler URI

=cut

#===============================================================================
sub GetUserURI {
	my ($self) = @_;

	return ($self->{miogaconf}->GetUserURI ());
}	# ----------  end of subroutine GetUserURI  ----------


#===============================================================================

=head2 GetCDNURI

Get CDN URI, if configured

=cut

#===============================================================================
sub GetCDNURI {
	my ($self) = @_;

	my $uri;
	if ((!$self->{miogaconf}->{cdn_server}) || (ref ($self->{miogaconf}->{cdn_server}) eq 'HASH') || ($self->{miogaconf}->{cdn_server} eq '')) {
		# CDN is local
		$uri = $self->{miogaconf}->GetBasePath () . $self->{miogaconf}->{cdn_uri};
	}
	else {
		$uri = $self->{miogaconf}->{cdn_server} . $self->{miogaconf}->{cdn_uri};
	}

	return ($uri);
}	# ----------  end of subroutine GetCDNURI  ----------


#===============================================================================

=head2 GetCDNPath

Get path to CDN files

=cut

#===============================================================================
sub GetCDNPath {
	my ($self) = @_;

	my $uri = $self->{miogaconf}->GetInstallDir () . $self->{miogaconf}->{cdn_uri} . '/' . $Mioga2::VERSION;

	return ($uri);
}	# ----------  end of subroutine GetCDNPath  ----------


# ============================================================================

=head2 WriteNotifierFIFO ()

    Write to notifier fifo.
    
=cut

# ============================================================================

sub WriteNotifierFIFO {
    my ($self, $data) = @_;

    $self->{miogaconf}->WriteNotifierFIFO($data);
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga2 Mioga2::Config Mioga2::URI Mioga2::Application Mioga2::AppDesc
Mioga2::Old::User Mioga2::Old::Group Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
