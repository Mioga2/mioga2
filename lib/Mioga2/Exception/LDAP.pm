# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
LDAP.pm: used for LDAP exceptions

=head1 DESCRIPTION

	LDAP execption module

=head1 METHODS DESRIPTION

=cut

package Mioga2::Exception::LDAP;

my $debug=0;

use strict;
use vars qw(@ISA);
use Mioga2::Exception;
use Data::Dumper;

@ISA = qw(Mioga2::Exception);

# ----------------------------------------------------------------------------

=head2 new ($function, $text)

=cut

# ----------------------------------------------------------------------------
sub new {
    my($class, $function, $text, $filter) = @_;
	my $self = $class->SUPER::new(-text => $text);
	$self->{function} = $function;
	$self->{filter} = $filter;
	return $self;
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

	Mioga2::Exception

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
