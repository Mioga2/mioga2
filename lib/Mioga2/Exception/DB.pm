# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
DB.pm: used for database exceptions

=head1 DESCRIPTION

	Database execption module

=head1 METHODS DESRIPTION

=cut

package Mioga2::Exception::DB;

my $debug=0;

use strict;
use vars qw(@ISA);
use Mioga2::Exception;
use Data::Dumper;

@ISA = qw(Mioga2::Exception);

# ----------------------------------------------------------------------------

=head2 new ($function, $errDB, $errStr, $sql)

=cut

# ----------------------------------------------------------------------------
sub new {
    my($class, $function, $errDB, $errStr, $sql, $args) = @_;
	my $self = $class->SUPER::new(-text => "$errStr : $errDB");
	$self->{errDB} = $errDB;
	$self->{sql} = $sql;
	$self->{args} = $args;
	$self->{function} = $function;
	return $self;
}

# ----------------------------------------------------------------------------

=head2 as_string ()

=cut 

# ----------------------------------------------------------------------------
sub as_string
{
	my $self = shift;
	my $string = "Function = $self->{function}\n" .
				"errDB = $self->{errDB}\n" .
				"errStr = $self->{-text}\n" .
				"sql = $self->{sql}\n" .
				"args = " . Dumper $self->{args};

	return $string;
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

	Mioga2::Exception

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
