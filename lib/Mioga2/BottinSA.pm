#===============================================================================
#
#         FILE:  BottinSA.pm
#
#  DESCRIPTION:  Bottin Super-Admin Application
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  15/12/2009 14:07
#===============================================================================
#
#  Copyright (c) year 2009, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================

=head1 NAME

BottinSA.pm : The Mioga2 LDAP account management application, cross-instance mode.

=head1 METHODS DESCRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::BottinSA;
use vars qw($VERSION);

$VERSION = "1.0.0";

use base qw(Mioga2::Bottin);
use Locale::TextDomain::UTF8 'bottin';

use Mioga2::tools::database;
use Mioga2::tools::args_checker;
use Mioga2::Classes::LDAPView;
use Mioga2::Content::ASIS;
use Data::Dumper;

my $debug = 0;

# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================

sub GetAppDesc {
	my %AppDesc = (
		ident   => 'BottinSA',
		package => 'Mioga2::BottinSA',
		name => __('LDAP Account Manager'),
		description => __('LDAP account management application'),
		type    => 'normal',
		all_groups => 0,
		all_users  => 0,
		can_be_public => 0,
		is_user             => 1,
		is_group            => 1,
		is_resource         => 0,
		functions => { 
			'Administration' => __('Administration Functions'),
			'Write' => __('Read-write access'),
			'Read' => __('Read-only access'),
		},
		func_methods => {
			'Administration' => [
				'DisplayMain', 'DisplayUsers', 'GetUserData', 'GetUser', 'SetUser', 'DeleteUser', 'DisableUsers', 'EnableUsers', 'ImportUsers', 'ExportUsers', 'EmailUsers', 'SearchThroughDirectory', 'GetUserStatuses'
			],
			'Write' => [
				'DisplayMain', 'DisplayUsers', 'GetUserData', 'GetUser', 'SetUser', 'DisableUsers', 'EnableUsers', 'ImportUsers', 'ExportUsers', 'EmailUsers', 'SearchThroughDirectory', 'GetUserStatuses'
			],
			'Read' => [
				'DisplayMain', 'DisplayUsers', 'GetUserData', 'GetUser', 'ExportUsers', 'SearchThroughDirectory', 'GetUserStatuses'
			],
		},
		non_sensitive_methods => [
			'DisplayMain',
			'DisplayUsers',
			'ExportUsers',
			'GetUser',
			'GetUserData',
			'GetUserStatuses',
			'ImportUsers',
		],
		sxml_methods => { },
		xml_methods  => { },
	);

	return \%AppDesc;
}


#===============================================================================

=head2 GetXmlLDAPInstances

Get LDAP-enabled-instances list as XML

=cut

#===============================================================================
sub GetXmlLDAPInstances {
	my ($self, $context) = @_;
	
	my $xml = '';

	my $instances = SelectMultiple ($self->{dbh}, "SELECT ident, ldap_access FROM m_mioga WHERE use_ldap='t' ORDER BY ident");
	for (@$instances) {
		$xml .= "<Instance";
		$xml .= ' selected="1"' if ($_->{ident} eq $context->GetConfig()->GetMiogaIdent());
		$xml .= ' access="' . $_->{ldap_access} . '"';
		$xml .= ">$_->{ident}</Instance>";
	}

	return ($xml);
}	# ----------  end of subroutine GetXmlLDAPInstances  ----------


#===============================================================================

=head2 SearchThroughDirectory

Search through the whole directory

=cut

#===============================================================================
sub SearchThroughDirectory {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Bottin::SearchThroughDirectory] Entering. Args: " . Dumper $context->{args} if ($debug);

	# Check incoming arguments
	my $params = [ [ [ 'firstname', 'lastname', 'email'], 'stripxws', 'allow_empty' ] ];
	my ($values, $errors) = ac_CheckArgs($context, $params );

	for (keys (%$values)) {
		if ($values->{$_} eq '') {
			delete $values->{$_};
		}
		else {
			$values->{$_} = "*$values->{$_}*";
		}
	}
	
	#-------------------------------------------------------------------------------
	# Create User View and Grid
	#-------------------------------------------------------------------------------
	$self->{users_sll_desc}->{fields} = $self->{users_sll_field_list};
	my $view_params = { ldap => $self->{ldap},
	                    desc => $self->{users_sll_desc},
	                  };
	my $user_view = Mioga2::Classes::LDAPView->new($view_params);

	#-------------------------------------------------------------------------------
	# Get user list for current instance
	#-------------------------------------------------------------------------------
	my $data = $user_view->GetData({ mode => 'dojo_grid', as_object => 1,
	                                   ldap_params => $self->{ldap}->TranslateAttributes ($values)});

	print STDERR "data = " . Dumper $data if($debug);

	return ($data);
}	# ----------  end of subroutine SearchThroughDirectory  ----------

# ============================================================================
# Private methods
# ============================================================================

#===============================================================================

=head2 InitSuperAdminMode

Initialize Super-Admin flag to 1

=cut

#===============================================================================
sub InitSuperAdminMode {
    my ($self, $context) = @_;
	print STDERR "BottinSA::InitSuperAdminMode()\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Set flag indicating Super-Admin mode
	#-------------------------------------------------------------------------------
	$self->{superadmin} = 1;
}

1;

