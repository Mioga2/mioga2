# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Image.pm: used for gestion of images

=head1 DESCRIPTION

	Check Image URI and the return an image. Useful for image gestion and and with themes.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Image;
use strict;

use Mioga2::Apache;
use Error qw(:try);
use Mioga2::MiogaConf;
use Mioga2::Exception::Simple;
use Mioga2::tools::date_utils;
use File::MimeInfo::Magic;

use Data::Dumper;


my $debug=0;


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ----------------------------------------------------------------------------
=head 2 handler()

	handler() is called by mod_perl function of Apache configuration

=cut

# ----------------------------------------------------------------------------
sub handler : method
{
	my ($class, $r) = @_;
	my $result;
	my $config;
	
	try {
		my $fileConfig = $r->dir_config("MiogaConfig");
		my $miogaconf = new Mioga2::MiogaConf($fileConfig);

		my $base_uri = $miogaconf->GetBasePath();
		my $image_uri = $miogaconf->GetImagePath();

		my $install_dir = $miogaconf->GetInstallDir();
		my $base_dir = $miogaconf->GetBaseDir();
		my $image_dir = $miogaconf->GetImageDir();		

		#cut uri
		my $uri = $r->uri;
		my ($mioga_ident, $group_ident, $image) = ( $uri =~ m|^$base_uri/([^/]+)$image_uri(?:/([^/]+))?(.*)| );

		if(!defined $image or $image eq '') {
			$image = $group_ident;
			$group_ident = undef;
		}
		
		my ($config, $group);
		my $context = $r->pnotes('context');

		if(!defined $context and defined $group_ident) {
			$config = new Mioga2::Config($miogaconf, $mioga_ident);
			$group = Mioga2::Old::GroupBase->CreateObject($config, ident => $group_ident);
		}
		elsif(!defined $context) {
			$config = new Mioga2::Config($miogaconf, $mioga_ident);
		}
		else {
			$config = $context->GetConfig();
			$group = $context->GetGroup();
		}

		my $theme;
		my $lang;

		if(defined $group) {
			$theme = $group->GetTheme();
			$lang = $group->GetLang();
		}
		else {
			$theme = $config->GetDefaultTheme();
			$lang  = $config->GetDefaultLang();
		}

		if ($debug > 0)
		{
			print STDERR "--------------------------------\n"; 
			print STDERR "group : $group_ident\n" if defined $group_ident;
			print STDERR "image : $image\n";
			print STDERR "theme : $theme\n";
			print STDERR "lang : $lang\n";
			print STDERR "--------------------------------\n";
		}

		#check uri content
		my $path1 = "$install_dir/$mioga_ident$base_dir$image_dir/$theme/$lang/$image";
		my $path2 = "$install_dir/$mioga_ident$base_dir$image_dir/$theme/$image";
		my $path3 = "$install_dir/$mioga_ident$base_dir$image_dir/$image";

		my $validPath;

		if ($debug > 0)
		{
			warn("$path1");
			warn("$path2");
			warn("$path3");
		}
		#lang and theme defined
		if ((defined($lang)) && (defined($theme)) && (defined($image)) && (-e $path1))
		{
			$validPath = $path1;
		}
		#theme defined
		elsif ((defined($theme)) && (defined($image)) && (-e $path2))
		{
			$validPath = $path2;
		}
		#only image defined
		elsif ((defined($image)) && (-e $path3))
		{
			$validPath = $path3;
		}
		else
		{
			$result = Mioga2::Apache::HTTP_NOT_FOUND;
		}

		if (defined($validPath))
		{
			warn("image : $validPath") if ($debug > 0);
			$result = Mioga2::Apache::HTTP_OK;
			my @stat_values = stat($validPath);
			
			#$r->set_etag;
			#$r->set_content_length(-s $validPath);
			$r->set_content_length($stat_values[7]);

			my $type =  mimetype($validPath);

            $r->content_type($type);
            #my $etag = $r->make_etag(0);
            #$r->set_etag();
            my $etag = $stat_values[9];
            my $last_modified = du_SecondToISO($stat_values[9]);
            $r->headers_out->set("ETag" => $etag);
            $r->headers_out->set("Last-Modified" => $last_modified);
            my $status = $r->meets_conditions();
        warn("Mioga2::Image handler status = $status") if ($debug);
            if ($status != Mioga2::Apache::HTTP_OK) {
        warn("Mioga2::Image handler no send file") if ($debug);
                $result = $status;
            }
            else {
                $r->sendfile($validPath);
            }
		}
	}
	catch Mioga2::Exception::DB with {
		my $err = shift;
		$r->warn("Exception DB " . $err->as_string());
		$result = Mioga2::Apache::HTTP_SERVER_ERROR;
	}
	catch Mioga2::Exception::Simple with {
		my $err = shift;
		$r->warn("Exception Simple " . $err->as_string());
		$result = Mioga2::Apache::HTTP_SERVER_ERROR;
	}
	otherwise {
		my $err = shift;
		$r->warn("Other execption ");
		$r->warn(Dumper($err));
		$result = Mioga2::Apache::HTTP_SERVER_ERROR;
	};

	return $result;
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
