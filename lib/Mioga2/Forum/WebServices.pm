# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME

Mioga2::Forum::WebServices - The Mioga2 Forum application WebServices

=cut

# ============================================================================

# TODO
# 	- moderation WebServices

package Mioga2::Forum;
use strict;

my $debug = 0;

# ============================================================================

=head1 Mioga2::Forum WEBSERVICES

=cut

# ============================================================================



#===============================================================================

=head2 GetCategories

Get available categories

=head3 Incoming Arguments

=over

None, works on selected group

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		categories: [
			{
				rowid: ...
				forum_id: ...
				label: ...
				created: ...
				modified: ...
			}
		]
	}

=over

=back

=back

=cut

#===============================================================================
sub GetCategories {
	my ($self, $context) = @_;

	return ({categories => [$self->DBGetCategories($context->GetConfig()->GetDBH())]});
}



#===============================================================================

=head2 GetCategory

Get category details

=head3 Incoming Arguments

=over

=item B<category>: the category rowid

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		category: {
			rowid: ...
			forum_id: ...
			label: ...
			created: ...
			modified: ...
		}
	}

=over

=back

=back

=cut

#===============================================================================
sub GetCategory {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid'], 'disallow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = { };

	if (!@$errors) {
		$data->{category} = $self->DBGetCategory($context->GetConfig()->GetDBH(), $values->{rowid});
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}


#===============================================================================

=head2 SetCategory

Create or update a category

=head3 Incoming Arguments

=over

=item B<rowid>: (optional) the category rowid

=item B<label>: the category name

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		category: {
			rowid: ...
			forum_id: ...
			label: ...
			created: ...
			modified: ...
		}
	}

=over

=back

=back

=cut

#===============================================================================
sub SetCategory {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'label' ], 'disallow_empty', 'noscript'],
			[ [ 'rowid' ], 'allow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = { };

	if (!@$errors) {
		if ($values->{rowid}) {
			# Update existing
			$self->DBUpdateCategory($context->GetConfig()->GetDBH(), $values->{rowid}, $values->{label});
		}
		else {
			# Create new
			$values->{rowid} = $self->DBAddCategory($context->GetConfig()->GetDBH(), $values->{label});
		}

		$data->{category} = $self->DBGetCategory($context->GetConfig()->GetDBH(), $values->{rowid});
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}


#===============================================================================

=head2 DeleteCategory

Delete a category

=head3 Incoming Arguments

=over

=item B<rowid>: the category's rowid

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		success: true|false,
		errors: [
			...
		]
	}

=over

=back

=back

=cut

#===============================================================================
sub DeleteCategory {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = {
		success => 'boolean::false'
	};

	if (!@$errors) {
		$self->DBDeleteCategory($context->GetConfig()->GetDBH(), $values->{rowid});
		$data->{success} = 'boolean::true';
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}


#===============================================================================

=head2 GetThematics

Get thematics for a category.

=head3 Incoming Arguments

=over

=item B<category>: the category rowid

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		thematics: [
			{
				rowid: ...
				label: ...
				description: ...
				subject_count: ...
				message_count: ...
				lastpost: ...
				category_id: ...
				author_id: ...
				message_id: ...
				firstname: ...
				lastname: ...
				created: ...
				modified: ...
			}
		]
	}

=over

=back

=back

=cut

#===============================================================================
sub GetThematics {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'category'], 'disallow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = { };

	if (!@$errors) {
		$data->{thematics} = $self->DBGetThematics($context->GetConfig()->GetDBH(), $values->{category});
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}


#===============================================================================

=head2 GetThematic

Get thematic details

=head3 Incoming Arguments

=over

=item B<rowid>: the thematic rowid

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		thematic: {
			rowid: ...
			label: ...
			description: ...
			subject_count: ...
			message_count: ...
			lastpost: ...
			category_id: ...
			author_id: ...
			message_id: ...
			firstname: ...
			lastname: ...
			created: ...
			modified: ...
		}
	}

=over

=back

=back

=cut

#===============================================================================
sub GetThematic {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid'], 'disallow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = { };

	if (!@$errors) {
		$data->{category} = $self->DBGetThematic($context->GetConfig()->GetDBH(), $values->{rowid});
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}


#===============================================================================

=head2 SetThematic

Create or update a thematic

=head3 Incoming Arguments

=over

=item B<rowid>: (optional) the thematic rowid

=item B<category>: the category rowid

=item B<label>: the thematic name

=item B<description>: the thematic description

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		thematic: {
			rowid: ...
			label: ...
			description: ...
			subject_count: ...
			message_count: ...
			lastpost: ...
			category_id: ...
			author_id: ...
			message_id: ...
			firstname: ...
			lastname: ...
			created: ...
			modified: ...
		}
	}

=over

=back

=back

=cut

#===============================================================================
sub SetThematic {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'category' ], 'disallow_empty', 'want_int'],
			[ [ 'label' ], 'disallow_empty', 'noscript'],
			[ [ 'description' ], 'allow_empty', 'noscript'],
			[ [ 'rowid' ], 'allow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = { };

	if (!@$errors) {
		if ($values->{rowid}) {
			# Update existing
			$self->DBUpdateThematic($context->GetConfig()->GetDBH(), $values->{rowid}, {
				category_id => $values->{category},
				label => $values->{label},
				description => $values->{description} ? $values->{description} : ""
			});
		}
		else {
			# Create new
			$values->{rowid} = $self->DBAddThematic($context->GetConfig()->GetDBH(), $values->{category}, $values->{label}, $values->{description});
		}

		$data->{thematic} = $self->DBGetThematic($context->GetConfig()->GetDBH(), $values->{rowid});
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}


#===============================================================================

=head2 DeleteThematic

Delete a thematic

=head3 Incoming Arguments

=over

=item B<rowid>: the thematic's rowid

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		success: true|false,
		errors: [
			...
		]
	}

=over

=back

=back

=cut

#===============================================================================
sub DeleteThematic {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = {
		success => 'boolean::false'
	};

	if (!@$errors) {
		$self->DBDeleteThematic($context->GetConfig()->GetDBH(), $values->{rowid});
		$data->{success} = 'boolean::true';
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}


#===============================================================================

=head2 GetSubjects

Get subjects for a thematic.

=head3 Incoming Arguments

=over

=item B<thematic>: the thematic rowid

=item B<count>: the number of subjects to fetch

=item B<offset>: offset in messages list

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		subjects: [
			{
				rowid: ...
				subject: ...
				message_count: ...
				post_it: ...
				open: ...
				lastpost: ...
				last_visit: ...
				thematic_id: ...
				author_id: ...
				message_id: ...
				firstname: ...
				lastname: ...
				created: ...
				modified: ...
			}
		]
	}

=over

=back

=back

=cut

#===============================================================================
sub GetSubjects {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'thematic', 'count', 'offset'], 'disallow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = { };

	if (!@$errors) {
		$data->{subjects} = $self->DBGetSubjectList($context->GetConfig()->GetDBH(), $values->{thematic}, $context->GetUser()->GetRowid(), $values->{offset}, $values->{count});
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}



#===============================================================================

=head2 GetSubject

Get subject details

=head3 Incoming Arguments

=over

=item B<rowid>: the subject rowid

=back

=head3 Return value

=over

A perl hash to be converted to XML or JSON according to URL:

	{
		subject: {
			rowid: ...
			subject: ...
			message_count: ...
			post_it: ...
			open: ...
			lastpost: ...
			last_visit: ...
			thematic_id: ...
			author_id: ...
			message_id: ...
			firstname: ...
			lastname: ...
			created: ...
			modified: ...
		}
	}

=over

=back

=back

=cut

#===============================================================================
sub GetSubject {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = { };

	if (!@$errors) {
		$data->{subject} = $self->DBGetSubject($context->GetConfig()->GetDBH(), $values->{rowid});
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}



#===============================================================================

=head2 SetSubject

Create or update a subject

=head3 Incoming Arguments

=over

=item B<rowid>: (optional) the subject rowid

=item B<subject>: the subject name

=item B<message>: the subject's message (in case of creation)

=item B<thematic>: the subject’s thematic rowid

=item B<open>: (0|1) is the subject open?

=item B<postit>: (0|1) is the subject pinned?

=back

=head3 Return value

=over

A perl hash to be converted to XML or JSON according to URL:

	{
		subject: {
			rowid: ...
			subject: ...
			message_count: ...
			post_it: ...
			open: ...
			lastpost: ...
			last_visit: ...
			thematic_id: ...
			author_id: ...
			message_id: ...
			firstname: ...
			lastname: ...
			created: ...
			modified: ...
		}
	}

=over

=back

=back

=cut

#===============================================================================
sub SetSubject {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'thematic', 'open', 'postit' ], 'disallow_empty', 'want_int'],
			[ [ 'subject' ], 'disallow_empty', 'noscript'],
			[ [ 'message' ], 'allow_empty', 'noscript'],
			[ [ 'rowid' ], 'allow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = { };

	if (!@$errors) {
		if ($values->{rowid}) {
			# Update existing
			$self->DBUpdateSubject($context->GetConfig()->GetDBH(), $values->{rowid}, {
				modified => time,
				modifier_id => $context->GetUser()->GetRowid(),
				thematic_id => $values->{thematic},
				open => $values->{open},
				post_it => $values->{postit},
				subject => $values->{subject}
			});
		}
		else {
			# Create new
			$values->{rowid} = $self->DBCreateSubject($context->GetConfig()->GetDBH(), {
				created => time,
				modified => time,
				author_id => $context->GetUser()->GetRowid(),
				modifier_id => $context->GetUser()->GetRowid(),
				thematic_id => $values->{thematic},
				open => $values->{open},
				post_it => $values->{postit},
				subject => $values->{subject},
				message => $values->{message}
			});
		}

		$data->{subject} = $self->DBGetSubject($context->GetConfig()->GetDBH(), $values->{rowid});

		# TODO notify update, send e-mails (see Mioga2::Forum::UpdateMessage)
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}



#===============================================================================

=head2 DeleteSubject

Delete a subject

=head3 Incoming Arguments

=over

=item B<rowid>: the subject rowid

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		success: true|false,
		errors: [
			...
		]
	}

=over

=back

=back

=cut

#===============================================================================
sub DeleteSubject {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = {
		success => 'boolean::false'
	};

	if (!@$errors) {
		$self->DBDeleteSubjectAndMessages($context->GetConfig()->GetDBH(), $values->{rowid});
		$data->{success} = 'boolean::true';
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}


#===============================================================================

=head2 GetMessages

Get messages for a subject

=head3 Incoming Arguments

=over

=item B<subject>: the subject rowid

=item B<count>: the number of messages to fetch

=item B<offset>: the page number

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		messages: [
			{
				rowid: ...
				message: ...
				subject_id: ...
				modifier_id: ...
				author_id: ...
				firstname: ...
				lastname: ...
				modifier_firstname: ...
				modifier_lastname: ...
				created: ...
				modified: ...
			}
		]
	}

=over

=back

=back

=cut

#===============================================================================
sub GetMessages {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'subject', 'count', 'offset'], 'disallow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = { };

	if (!@$errors) {
		$data->{messages} = $self->DBGetMessageList($context->GetConfig()->GetDBH(), $values->{subject}, $values->{offset}, $values->{count});
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}



#===============================================================================

=head2 GetMessage

Get a message

=head3 Incoming Arguments

=over

=item B<rowid>: the message's rowid

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		message: {
			rowid: ...
			message: ...
			subject_id: ...
			modifier_id: ...
			author_id: ...
			firstname: ...
			lastname: ...
			modifier_firstname: ...
			modifier_lastname: ...
			created: ...
			modified: ...
		}
	}

=over

=back

=back

=cut

#===============================================================================
sub GetMessage {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = { };

	if (!@$errors) {
		$data->{message} = $self->DBGetMessage($context->GetConfig()->GetDBH(), $values->{rowid});
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}



#===============================================================================

=head2 SetMessage

Create or update a message

=head3 Incoming Arguments

=over

=item B<rowid>: (optional) the message rowid

=item B<subject>: the subject rowid

=item B<message>: the message contents

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

On success:

	{
		message: {
			...
		}
	}

On error:

	{
		errors: [
			...
		]
	}

=over

=back

=back

=cut

#===============================================================================
sub SetMessage {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'subject' ], 'disallow_empty', 'want_int'],
			[ [ 'message' ], 'disallow_empty', 'noscript'],
			[ [ 'rowid' ], 'allow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = { };

	if (!@$errors) {
		if ($values->{rowid}) {
			# Update existing message
			$self->DBUpdateMessage($context->GetConfig()->GetDBH(), $values->{rowid}, {
				modified => time,
				modifier_id => $context->GetUser()->GetRowid(),
				message => $values->{message}
			});
		}
		else {
			# Create new message
			$values->{rowid} = $self->DBCreateMessage($context->GetConfig()->GetDBH(), $values->{subject}, $context->GetUser()->GetRowid(), $values->{message});
		}

		$data->{message} = $self->DBGetMessage($context->GetConfig()->GetDBH(), $values->{rowid});

		# TODO notify update, send e-mails (see Mioga2::Forum::UpdateMessage)
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}



#===============================================================================

=head2 DeleteMessage

Delete a message

=head3 Incoming Arguments

=over

=item B<rowid>: the message's rowid

=back

=head3 Return value

=over

A Perl hash to be converted to XML or JSON according to URL.

	{
		success: true|false,
		errors: [
			...
		]
	}

=over

=back

=back

=cut

#===============================================================================
sub DeleteMessage {
	my ($self, $context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'rowid' ], 'disallow_empty', 'want_int'],
		];

	my ($values, $errors) = ac_CheckArgs($context, $params);

	my $data = {
		success => 'boolean::false'
	};

	if (!@$errors) {
		$self->DBDeleteMessage($context->GetConfig()->GetDBH(), $values->{rowid});
		$data->{success} = 'boolean::true';
	}

	for (@$errors) {
		push (@{$data->{errors}}, { $_->[0] => $_->[1]->[0] });
	}

	return($data);
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


