# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME

Mioga2::Forum - The Mioga2 Forum application

=cut

# ============================================================================

package Mioga2::Forum;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'forum';

use Error qw(:try);
use POSIX qw(ceil);
use Mioga2::URI;
use Mioga2::Content::XSLT;
use Mioga2::Content::ASIS;
use Mioga2::tools::database;
use Mioga2::tools::args_checker;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Mioga2::tools::APIAuthz;
use Data::Dumper;
use XML::Atom::SimpleFeed;

my $debug = 0;

# ============================================================================

=head2 DisplayMain

 DisplayMain is the main display function

=head3 Generated XML For DisplayMain

	<DisplayMain>

        <miogacontext>See Mioga2::RequestContext</miogacontext>

		Forum informations (see GetXMLForumInfo)
		User Parameters (see GetXMLUserParameters)
		Main action list (see GetXMLMainActionList)
		Category and thematics list (see GetXMLCategoriesAndThematic)

	</DisplayMain>

=cut

# ============================================================================
sub DisplayMain
{
	my ($self, $context) = @_;
	my $user_id = $context->GetUser()->GetRowid();
	$self->{user_params} = $self->DBGetUserParameters($context->GetConfig()->GetDBH(), $user_id);
	#
	# Generate XML
	#
	my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayMain>";
	{
		my ($nr_instances, $xmlref_instances) = $self->AllSuchApplications($context);
		if ($nr_instances > 1) {
			$xml .= "<appnav>".$$xmlref_instances."</appnav>";
		}	
	}
	$xml .= $context->GetXML;
	$xml .= $self->GetXMLForumInfo($context);
	$xml .= $self->GetXMLUserParameters($context);
	$xml .= $self->GetXMLMainActionList($context);
	$xml .= $self->GetXMLCategoriesAndThematics($context);
	$xml .= "</DisplayMain>";

	print STDERR "Forum::DisplayMain xml = $xml\n" if($debug);
	#
	# Prepare content
	#
	my $content = new Mioga2::Content::XSLT($context, stylesheet => "forum.xsl",
														locale_domain => "forum_xsl");
	$content->SetContent($xml);

	return $content;
}
# ============================================================================

=head2 DisplayThematic

 Displaythematic presents subjects of a thematic with pagination

=cut

# ============================================================================
sub DisplayThematic
{
	my ($self, $context) = @_;
	my $user_id = $context->GetUser()->GetRowid();
	$self->{user_params} = $self->DBGetUserParameters($context->GetConfig()->GetDBH(), $user_id);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'thematic_id'], 'disallow_empty', 'want_int'],
													 [ [ 'category_id'], 'disallow_empty', 'want_int'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::DisplayThematic", ac_FormatErrors($errors));
	}
	my $thematic_id = $values->{thematic_id};
	my $category_id = $values->{category_id};
	#
	# Prepare content
	#
	my $xml = $self->FormatXMLDisplayThematic($context, "DisplayThematic", $category_id, $thematic_id);

	my $content = new Mioga2::Content::XSLT($context, stylesheet => "forum.xsl",
														locale_domain => "forum_xsl");
	$content->SetContent($xml);

	return $content;
}
# ============================================================================

=head2 DisplayNewMessages

 DisplayNewMessages presents all new messages dependending on subject_id value :
 if subject_id is 0 then all messages for thematic are presented
 if subject_id isn't 0 then all messages for this subject are presented

=cut

# ============================================================================
sub DisplayNewMessages
{
	my ($self, $context) = @_;
	my $user_id = $context->GetUser()->GetRowid();
	$self->{user_params} = $self->DBGetUserParameters($context->GetConfig()->GetDBH(), $user_id);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'thematic_id'], 'disallow_empty', 'want_int'],
													 [ [ 'category_id'], 'disallow_empty', 'want_int'],
													 [ [ 'subject_id'], 'disallow_empty', 'want_int'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::DisplayNewMessages", ac_FormatErrors($errors));
	}
	my $subject_id = $values->{subject_id};
	my $category_id = $values->{category_id};
	my $thematic_id = $values->{thematic_id};
	#
	# Generate subject_list
	#
	my $subject_list = $self->DBGetSubjectsWithNewMessages($context->GetConfig()->GetDBH(), $thematic_id, $user_id);
	#
	# Generate XML
	#
	my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayNewMessages>";
	$xml .= $context->GetXML;

	$xml .= $self->GetXMLForumInfo($context);
	$xml .= $self->GetXMLUserParameters($context);
	$xml .= $self->GetXMLMainActionList($context);

	foreach my $sid (@$subject_list) {
		$xml .= "<Subject>";
		$xml .= $self->GetXMLSubjectEnv($context, $sid->{rowid});
		$xml .= $self->GetXMLSubjectDatas($context, $sid->{rowid});
		$xml .= $self->GetXMLNewMessagesList($context, $sid->{rowid}, $user_id, $sid->{last_visit});
		$xml .= "</Subject>";
		# Set user visit date
		$self->DBSetUserLastVisit($context->GetConfig()->GetDBH(), $user_id, $sid->{rowid});
	}
	$xml .= "</DisplayNewMessages>";

	print STDERR "Forum::DisplayNewMessages xml = $xml\n" if($debug);
	#
	# Prepare content
	#
	my $content = new Mioga2::Content::XSLT($context, stylesheet => "forum.xsl", locale_domain => "forum_xsl");
	$content->SetContent($xml);

	return $content;
}
# ============================================================================

=head2 DisplaySubject

 DisplaySubject presents messages of a subject with pagination

=cut

# ============================================================================
sub DisplaySubject
{
	my ($self, $context) = @_;
	my $user_id = $context->GetUser()->GetRowid();
	$self->{user_params} = $self->DBGetUserParameters($context->GetConfig()->GetDBH(), $user_id);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'subject_id'], 'disallow_empty', 'want_int'],
													 [ [ 'page_num'], 'allow_empty', 'want_int'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::DisplaySubject", ac_FormatErrors($errors));
	}
	my $subject_id = $values->{subject_id};
	# page_num
	my $page_num = 1;
	my $messages_per_page = 50;   # TODO set it user variable
	if (exists($values->{page_num})) {
		$page_num = $values->{page_num};
		if ($page_num < 1) {
			$page_num = 1;
		}
	}
	#
	# Generate XML
	#
	my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplaySubject>";
	$xml .= $context->GetXML;

	$xml .= $self->GetXMLForumInfo($context);
	$xml .= $self->GetXMLUserParameters($context);
	$xml .= $self->GetXMLMainActionList($context);
	$xml .= "<referer>" . st_FormatXMLString($context->GetURI()->GetURI()) . "</referer>";
	$xml .= $self->GetXMLSubjectEnv($context, $subject_id);
	$xml .= $self->GetXMLSubjectDatas($context, $subject_id);
	$xml .= $self->GetXMLMessageList($context, $subject_id, $page_num, $messages_per_page);
	$xml .= "</DisplaySubject>";

	print STDERR "Forum::DisplaySubject xml = $xml\n" if($debug);
	#
	# Set user visit date
	#
	$self->DBSetUserLastVisit($context->GetConfig()->GetDBH(), $user_id, $subject_id);
	#
	# Prepare content
	#
	my $content = new Mioga2::Content::XSLT($context, stylesheet => "forum.xsl",
														locale_domain => "forum_xsl");
	$content->SetContent($xml);

	return $content;
}
# ============================================================================

=head2 ProcessMainAction

	This method process all actions for adminsitration :
	- set_user_params

=cut

# ============================================================================
sub ProcessMainAction
{
	my ($self, $context) = @_;
	print STDERR "Forum::ProcessMainAction context args : " . Dumper($context->{args}) . "\n" if ($debug);
	#
	# Analyze action and call right function
	#
	my $action;
	my $xml;
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'action'], 'disallow_empty'], ]);
	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::ProcessMainAction", ac_FormatErrors($errors));
	}
	$action = $values->{action};
	print STDERR "Forum::ProcessMainAction action = $action\n" if($debug);
	if ($action eq 'set_user_params') {
		return $self->SetUserParams($context);
	}
	else {
		warn "Mioga2::Forum::ProcessMainAction : action $action not recognized";
		throw Mioga2::Exception::Application("Mioga2::Forum::ProcessMainAction", __"action not recognized");
	}
}
# ============================================================================

=head2 ProcessAnimAction

	This method process all actions for adminsitration :
	- add_category
	- rename_category
	- suppress_category
	- add_thematic
	- suppress_thematic
	- rename_thematic
	- set_parameters

=cut

# ============================================================================
sub ProcessAnimAction
{
	my ($self, $context) = @_;
	print STDERR "Forum::ProcessAnimAction context args : " . Dumper($context->{args}) . "\n" if ($debug);
	#
	# Analyze action and call right function
	#
	my $action;
	my $xml;
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'action'], 'disallow_empty'], ]);
	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::ProcessAnimAction", ac_FormatErrors($errors));
	}
	$action = $values->{action};
	print STDERR "Forum::ProcessAnimAction action = $action\n" if($debug);
	if ($action eq 'add_category') {
		return $self->AddCategory($context);
	}
	elsif ($action eq 'add_thematic') {
		return $self->AddThematic($context);
	}
	elsif ($action eq 'rename_category') {
		return $self->RenameCategory($context);
	}
	elsif ($action eq 'rename_thematic') {
		return $self->RenameThematic($context);
	}
	elsif ($action eq 'edit_subject') {
		return $self->EditSubject($context);
	}
	elsif ($action eq 'suppress_category') {
		return $self->SuppressCategory($context);
	}
	elsif ($action eq 'suppress_thematic') {
		return $self->SuppressThematic($context);
	}
	elsif ($action eq 'suppress_subject') {
		return $self->SuppressSubject($context);
	}
	elsif ($action eq 'suppress_message') {
		return $self->SuppressMessage($context);
	}
	elsif ($action eq 'set_parameters') {
		return $self->SetParameters($context);
	}
	else {
		warn "Mioga2::Forum::ProcessAnimAction : action $action not recognized";
		throw Mioga2::Exception::Application("Mioga2::Forum::ProcessAnimAction", __"action not recognized");
	}
}
# ============================================================================

=head2 ProcessViewAction

	This method process all actions for view action :
	- view_thematic
	- create_subject
	- create_message

=cut

# ============================================================================
sub ProcessViewAction
{
	my ($self, $context) = @_;
	print STDERR "Forum::ProcessViewAction context args : " . Dumper($context->{args}) . "\n" if ($debug);
	#
	# Analyze action and call right function
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'action'], 'disallow_empty'], ]);
	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::ProcessViewAction", ac_FormatErrors($errors));
	}
	my $action = $values->{action};
	print STDERR "Forum::ProcessViewAction action = $action\n" if($debug);
	if ($action eq 'view_thematic') {
		return $self->DisplayThematic($context);
	}
	elsif ($action eq 'create_subject') {
		return $self->DisplayCreateSubject($context);
	}
	elsif ($action eq 'update_subject') {
		return $self->UpdateSubject($context);
	}
	elsif ($action eq 'create_message') {
		return $self->DisplayCreateMessage($context);
	}
	elsif ($action eq 'view_new_messages') {
		return $self->DisplayNewMessages($context);
	}
	elsif ($action eq 'update_message') {
		return $self->UpdateMessage($context);
	}
	elsif ($action eq 'edit_message') {
		return $self->EditMessage($context);
	}
	elsif ($action eq 'signal_message') {
		return $self->SignalMessage($context);
	}
	else {
		warn "Mioga2::Forum::ProcessViewAction : action $action not recognized";
		throw Mioga2::Exception::Application("Mioga2::Forum::ProcessViewAction", __"action not recognized");
	}
}
# ============================================================================
# AddCategory
# args :
#    label (mandatory)
# ============================================================================
sub AddCategory {
	my ($self, $context) = @_;
	print STDERR "Forum::AddCategory\n" if($debug);
	my $user_id = $context->GetUser()->GetRowid();
	$self->{user_params} = $self->DBGetUserParameters($context->GetConfig()->GetDBH(), $user_id);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'label'], 'allow_empty'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::AddCategory", ac_FormatErrors($errors));
	}
	#
	# Add category with fixed label if none given
	#
	my $label = __"New category";
	if (length($values->{label}) > 0) {
		$label = $values->{label};
	}
	my $category_id = $self->DBAddCategory($context->GetConfig()->GetDBH(), $label);
	$label = __"New thematic";
	my $desc = __"Thematic description";
	my $thematic_id = $self->DBAddThematic($context->GetConfig()->GetDBH(), $category_id, $label, $desc);

	my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<AddCategory>";
	$xml .= $context->GetXML;
	$xml .= $self->GetXMLUserParameters($context);
	$xml .= $self->GetXMLMainActionList($context);
	$xml .= $self->GetXMLCategory($context, $category_id);
	$xml .= "</AddCategory>";
	print STDERR "Forum::AddCategory xml = $xml\n" if($debug);

	my $content = new Mioga2::Content::XSLT($context, stylesheet => "forum_parts.xsl",
														locale_domain => "forum_xsl");
	$content->SetContent($xml);
	return $content;
}
# ============================================================================
# AddThematic
# args :
#    category_id (mandatory)
#    label (optionnal)
# ============================================================================
sub AddThematic {
	my ($self, $context) = @_;
	print STDERR "Forum::AddThematic\n" if($debug);
	my $user_id = $context->GetUser()->GetRowid();
	$self->{user_params} = $self->DBGetUserParameters($context->GetConfig()->GetDBH(), $user_id);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'category_id'], 'disallow_empty', 'want_int'],
													 [ [ 'desc'], 'allow_empty'], 
													 [ [ 'label'], 'allow_empty'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::AddThematic", ac_FormatErrors($errors));
	}
	my $category_id = $values->{category_id};
	#
	# process add thematic
	#
	my $label = __"New thematic";
	if (length($values->{label}) > 0) {
		$label = $values->{label};
	}
	my $desc = __"Thematic description";
	if (length($values->{desc}) > 0) {
		$desc = $values->{desc};
	}
	my $thematic_id = $self->DBAddThematic($context->GetConfig()->GetDBH(), $category_id, $label, $desc);

	my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<AddThematic>";
	$xml .= $context->GetXML;
	$xml .= $self->GetXMLUserParameters($context);
	$xml .= $self->GetXMLMainActionList($context);
	$xml .= $self->GetXMLThematic($context, $thematic_id);
	$xml .= "</AddThematic>";
	print STDERR "Forum::AddThematic xml = $xml\n" if($debug);

	my $content = new Mioga2::Content::XSLT($context, stylesheet => "forum_parts.xsl",
														locale_domain => "forum_xsl");
	$content->SetContent($xml);
	return $content;
}
# ============================================================================
# RenameCategory
# args :
#    rowid (mandatory)
#    value (mandatory)
# ============================================================================
sub RenameCategory {
	my ($self, $context) = @_;
	print STDERR "Forum::RenameCategory\n" if($debug);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'rowid'], 'disallow_empty', 'want_int'],
													 [ [ 'value'], 'disallow_empty'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::RenameCategory", ac_FormatErrors($errors));
	}
	my $rowid = $values->{rowid};
	my $value = $values->{value};

    $self->DBUpdateCategory($context->GetConfig()->GetDBH(), $rowid, $value);

    $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
	my $content = new Mioga2::Content::ASIS($context, MIME => 'text/plain');
	$content->SetContent($value);
	return $content;
}
# ============================================================================
# RenameThematic
# args :
#    rowid (mandatory)
#    field = (label|desc) (mandatory)
#    value (mandatory)
# ============================================================================
sub RenameThematic {
	my ($self, $context) = @_;
	print STDERR "Forum::RenameThematic\n" if($debug);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'rowid'], 'disallow_empty', 'want_int'],
													 [ [ 'field'], 'disallow_empty'],
													 [ [ 'value'], 'disallow_empty'], ]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::RenameThematic", ac_FormatErrors($errors));
	}
	my $rowid = $values->{rowid};
	my $value = $values->{value};
	my $field = $values->{field};
	$values = {};
	if ($field eq 'label') {
		$values->{label} = $context->{args}->{value};
	}
	else {
		$values->{description} = $context->{args}->{value};
	}

	$self->DBUpdateThematic($context->GetConfig()->GetDBH(), $rowid, $values);

    $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
	my $content = new Mioga2::Content::ASIS($context, MIME => 'text/plain');
	$content->SetContent($context->{args}->{value});
	return $content;
}
# ============================================================================
# SignalMessage
# args :
#    message_id (mandatory)
# ============================================================================
sub SignalMessage {
	my ($self, $context) = @_;
	print STDERR "Forum::SignalMessage\n" if($debug);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'message_id'], 'disallow_empty', 'want_int'],]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::SignalMessage", ac_FormatErrors($errors));
	}
	my $message_id = $values->{message_id};
	#
	# if forum is moderated, then send a mail to moderators
	#
	if ($self->{moderated}) {
	    my $user    = $context->GetUser();
	    my $group   = $context->GetGroup();
	    my $config   = $context->GetConfig();
		my $app_desc = $context->GetAppDesc();
		my $sender  = $user->GetName() . " <" . $user->GetEmail() . ">";
		my $subject = __"Message signaled in Forum";

		my $message = __"Hello" . "\n";
		$message .= "\n";
		$message .= __"This is an automatic message, please do not answer." . "\n";
		$message .= __"A message has been signaled by :" . "\n";
		$message .= $user->GetName() . "\n";
		$message .= __"please look at :" . "\n";
        my $uri = new Mioga2::URI($config, group => $group->GetIdent(), public => 0,
														application => $app_desc->GetIdent(),
														method => 'ProcessViewAction',
														args => { action => 'edit_message',
																  message_id => $message_id
																}
													);
		$message .= $config->GetProtocol()."://".$config->GetServerName().$uri->GetURI() . "\n";

		my $entity = MIME::Entity->build(From   => $sender,
                                    Subject => $subject,
									Charset => "UTF-8",
                                    Data    => $message);
		$self->SendMailToModerators($context, $entity);
	}

	my $content = new Mioga2::Content::ASIS($context, MIME => 'text/plain');
	$content->SetContent(__"Message signaled");
	return $content;
}
# ============================================================================
# SuppressCategory
# args :
#    rowid (mandatory)
# ============================================================================
sub SuppressCategory {
	my ($self, $context) = @_;
	print STDERR "Forum::SuppressCategory\n" if($debug);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'rowid'], 'disallow_empty', 'want_int'],]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::SuppressCategory", ac_FormatErrors($errors));
	}
	my $rowid = $values->{rowid};
	#
	# Verify category in Forum ?
	#
	my $result = $self->DBGetCategory($context->GetConfig()->GetDBH(), $rowid);
	if ($result->{forum_id} != $self->{forum_rowid}) {
		throw Mioga2::Exception::Application("Mioga2::Forum::SuppressCategory", __"You are not authorized in this forum");
	}
	#
	# Destroy category
	#
	$self->DBDeleteCategory($context->GetConfig()->GetDBH(), $rowid);

	return $self->DisplayMain($context);
}
# ============================================================================
# SuppressThematic
# args :
#    rowid (mandatory)
# ============================================================================
sub SuppressThematic {
	my ($self, $context) = @_;
	print STDERR "Forum::SuppressThematic\n" if($debug);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'rowid'], 'disallow_empty', 'want_int'],]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::SuppressThematic", ac_FormatErrors($errors));
	}
	my $rowid = $values->{rowid};
	#
	# Verify thematic in Forum ?
	#
	my $result = $self->DBGetThematic($context->GetConfig()->GetDBH(), $rowid);
	if ($result->{forum_id} != $self->{forum_rowid}) {
		throw Mioga2::Exception::Application("Mioga2::Forum::SuppressThematic", __"You are not authorized in this forum");
	}
	#
	# Destroy thematic
	#
	$self->DBDeleteThematic($context->GetConfig()->GetDBH(), $rowid);

	return $self->DisplayMain($context);
}
# ============================================================================
# SuppressSubject
# args :
#    subject_id (mandatory)
# ============================================================================
sub SuppressSubject {
	my ($self, $context) = @_;
	print STDERR "Forum::SuppressSubject\n" if($debug);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'subject_id'], 'disallow_empty', 'want_int'],]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::SuppressSubject", ac_FormatErrors($errors));
	}
	my $subject_id = $values->{subject_id};

	my $result = $self->DBGetSubjectEnv($context->GetConfig()->GetDBH(), $subject_id);
	print STDERR "Forum::SuppressSubject result forum__id = " . $result->{forum_id} . " self forum__rowid = " . $self->{forum_rowid} . "\n" if($debug);
	#
	# Verify subject in Forum ?
	#
	if ($result->{forum_id} != $self->{forum_rowid}) {
		throw Mioga2::Exception::Application("Mioga2::Forum::SuppressSubject", __"You are not authorized in this forum");
	}
	$self->DBDeleteSubjectAndMessages($context->GetConfig()->GetDBH(), $subject_id);
    $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
	return $self->DisplayThematic($context);
}
# ============================================================================
# SuppressMessage
# args :
#    subject_id (mandatory)
# ============================================================================
sub SuppressMessage {
	my ($self, $context) = @_;
	print STDERR "Forum::SuppressMessage\n" if($debug);
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'message_id'], 'disallow_empty', 'want_int'],]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::SuppressMessage", ac_FormatErrors($errors));
	}
	my $message_id = $values->{message_id};
	#
	# Get message datas, verify forum_id and delete message
	#
	my $result = $self->DBGetMessage($context->GetConfig()->GetDBH(), $message_id);
	if ($result->{forum_id} != $self->{forum_rowid}) {
		throw Mioga2::Exception::Application("Mioga2::Forum::SuppressSubject", __"You are not authorized in this forum");
	}
	my $dbh = $context->GetConfig->GetDBH;
	$self->DBDeleteMessage($dbh, $message_id);
	my $message_number = $self->DBGetNumberOfMessagesInSubject($dbh, $result->{subject_id});
	
    $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
	#
	# initialize values to redirect
	#
	$context->{args}->{subject_id} = $result->{subject_id};
	return $self->DisplaySubject($context) if ($message_number > 0);
	
	$context->{args}->{thematic_id} = $result->{thematic_id};
	$context->{args}->{category_id} = $result->{category_id};
  return $self->SuppressSubject($context);
}
# ============================================================================
# SetUserParams
# args :
#    rowid (mandatory)
# ============================================================================
sub SetUserParams {
	my ($self, $context) = @_;
	print STDERR "Forum::SetUserParams\n" if($debug);
	my $user_id = $context->GetUser()->GetRowid();
	$self->{user_params} = $self->DBGetUserParameters($context->GetConfig()->GetDBH(), $user_id);
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'subscribe' ], 'bool'],
                                         ]);

	$values->{user_id} = $context->GetUser()->GetRowid();
	if (scalar(@$errors) == 0) {
		$self->DBSetUserParameters($context->GetConfig()->GetDBH(), $values);
	}
	else {
		throw Mioga2::Exception::Application("Mioga2::Forum::SetUserParams", __"Args error");
	}

	return $self->DisplayMain($context);
}
# ============================================================================
# SetParameters
# args :
#    rowid (mandatory)
# ============================================================================
sub SetParameters {
	my ($self, $context) = @_;
	print STDERR "Forum::SetParameters\n" if($debug);
	my $user_id = $context->GetUser()->GetRowid();
	$self->{user_params} = $self->DBGetUserParameters($context->GetConfig()->GetDBH(), $user_id);
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'simple_ihm', 'moderated', 'subscription', 'mail_alert'], 'bool'],
                                                [ [ 'rowid' ], 'want_int'],
                                         ]);

	print STDERR "Forum::SetParameters values = ". Dumper($values) . "\n" if($debug);
	print STDERR "Forum::SetParameters errors = ". Dumper($errors) . "\n" if($debug);
	if (scalar(@$errors) == 0) {
		$self->DBSetForumInfo($context->GetConfig()->GetDBH(), $values);
		$self->{simple_ihm} = $values->{simple_ihm};
		$self->{moderated} = $values->{moderated};
		$self->{subscription} = $values->{subscription};
		$self->{mail_alert} = $values->{mail_alert};
		# TODO (perhaps) : set users's subsciption to false if subscription change to false.
	}
	else {
		throw Mioga2::Exception::Application("Mioga2::Forum::SetParameters", ac_FormatErrors($errors));
	}

	return $self->DisplayMain($context);
}
# ============================================================================
# UpdateSubject
# args :
# ============================================================================
sub UpdateSubject {
	my ($self, $context) = @_;
	print STDERR "Forum::UpdateSubject\n" if($debug);
	my $params;
	#
	# Get and verify parameters
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'show_message'], 'allow_empty', 'bool'],]);

	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::UpdateSubject", ac_FormatErrors($errors));
	}
	my $show_message = $values->{show_message};

	if ($show_message) {
		$params =  [ [ [ 'thematic_id', 'category_id', 'subject_id' ], 'want_int', 'disallow_empty'],
					 [ [ 'subject', 'message' ], 'disallow_empty'],
					 [ [ 'open', 'post_it' ], 'bool' ] ];
	}
	else {
		$params =  [ [ [ 'thematic_id', 'category_id', 'subject_id' ], 'want_int', 'disallow_empty'],
					 [ [ 'subject' ], 'disallow_empty'],
					 [ [ 'message'], 'allow_empty' ],
					 [ [ 'open', 'post_it' ], 'bool' ] ];
	}
	($values, $errors) = ac_CheckArgs($context, $params);
	print STDERR "Forum::UpdateSubject values = ". Dumper($values) . "\n" if($debug);
	print STDERR "Forum::UpdateSubject errors = ". Dumper($errors) . "\n" if($debug);
	my $subject_id = $values->{subject_id};
	$values->{author_id} = $context->GetUser()->GetRowid();
	$values->{modifier_id} = $context->GetUser()->GetRowid();
	if (scalar(@$errors) == 0) {
		if ($subject_id == 0) {
			$subject_id = $self->DBCreateSubject($context->GetConfig()->GetDBH(), $values);
			#
			# Set user visit date for creator
			#
			$self->DBSetUserLastVisit($context->GetConfig()->GetDBH(), $context->GetUser()->GetRowid(), $subject_id);
		}
		else {
			$self->DBUpdateSubject($context->GetConfig()->GetDBH(), $subject_id, $values);
		}
        $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
		return $self->DisplayThematic($context);
	}
	else {
		return $self->DisplaySubjectDialog($context, $values, $errors, $show_message);
	}
}
# ============================================================================
# EditSubject
# args :
#    subject_id (mandatory)
#    value (mandatory)
# ============================================================================
sub EditSubject {
	my ($self, $context) = @_;
	print STDERR "Forum::EditSubject\n" if($debug);
	#
	# Verify aguments
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'subject_id' ], 'disallow_empty', 'want_int'],
                                         ]);
	if (scalar(@$errors) != 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::EditSubject", __"No message_id arg");
	}
	$self->{user_params} = $self->DBGetUserParameters($context->GetConfig()->GetDBH(), $context->GetUser()->GetRowid());
	#
	# Get subject datas and verify forum_id
	#
	my $result = $self->DBGetSubjectDatas($context->GetConfig()->GetDBH(), $values->{subject_id});
	if ($result->{forum_id} != $self->{forum_rowid}) {
		throw Mioga2::Exception::Application("Mioga2::Forum::EditSubject", __"You are not authorized in this forum");
	}
	#
	# initialize values to display
	#
	$values->{category_id} = $result->{category_id};
	$values->{thematic_id} = $result->{thematic_id};
	$values->{subject_id} = $result->{subject_id};
	$values->{post_it} = $result->{post_it};
	$values->{open} = $result->{open};
	$values->{subject} = $result->{subject};
	#
	# Get and memorize referer
	#
	my $session = $context->GetSession();
	$session->{forum}->{referer} = $context->GetReferer();

	return $self->DisplaySubjectDialog($context, $values, $errors, 0);
}
# ============================================================================
# DisplaySubjectDialog
# args :
# ============================================================================
sub DisplaySubjectDialog {
	my ($self, $context, $values, $errors, $show_message) = @_;
	print STDERR "Forum::DisplaySubjectDialog\n" if($debug);
	$self->{user_params} = $self->DBGetUserParameters($context->GetConfig()->GetDBH(), $context->GetUser()->GetRowid());
	#
	# Get referer for cancel action
	#
	my $ccel_url = new Mioga2::URI($context->GetConfig(), group  => $context->GetGroup()->GetIdent(),
                                      public => 0,
                                      application => 'Forum',
                                      method => 'DisplayThematic',
                                      args   => { thematic_id => $values->{thematic_id},
                                      			category_id => $values->{category_id},
                                              }
                                      );
	my $cancel_url = $ccel_url->GetURI();
	my $session = $context->GetSession();
	# bug fix 804 don't know why there is a referer
	#if (exists($session->{forum}->{referer})) {
	#	print STDERR "Forum::DisplaySubjectDialog session exists\n" if($debug);
	#	$cancel_url = $session->{forum}->{referer}; 
	#}
	print STDERR "Forum::DisplaySubjectDialog cancel_url = $cancel_url\n" if($debug);
	#
	# Generate XML
	#
	my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplaySubjectDialog>";
	$xml .= $context->GetXML;
	$xml .= $self->GetXMLUserParameters($context);
	$xml .= $self->GetXMLMainActionList($context);
	$xml .= ac_XMLifyErrors($errors);
	$xml .= '<cancel_url>' . st_FormatXMLString($cancel_url) . '</cancel_url>';
	$xml .= '<EditSubject>';
	$xml .= '<category_id>' . $values->{category_id} . '</category_id>';
	$xml .= '<thematic_id>' . $values->{thematic_id} . '</thematic_id>';
	$xml .= '<subject_id>' . $values->{subject_id} . '</subject_id>';
	$xml .= '<post_it>' . $values->{post_it} . '</post_it>';
	$xml .= '<open>' . $values->{open} . '</open>';
	$xml .= '<subject>' . st_FormatXMLString($values->{subject}) . '</subject>';
	if ($show_message) {
		$xml .= '<show_message />';
	}
	$xml .= '<message>' . st_FormatXMLString($values->{message}) . '</message>';
	$xml .= '</EditSubject>';
	$xml .= "</DisplaySubjectDialog>";
	print STDERR "Forum::DisplaySubjectDialog xml = $xml\n" if($debug);

	my $content = new Mioga2::Content::XSLT($context, stylesheet => "forum.xsl",
														locale_domain => "forum_xsl");
	$content->SetContent($xml);
	return $content;
}
# ============================================================================
# DisplayCreateSubject
# args :
#    category_id (mandatory)
#    thematic_id (mandatory)
# ============================================================================
sub DisplayCreateSubject {
	my ($self, $context) = @_;
	print STDERR "Forum::DisplayCreateSubject\n" if($debug);
	#
	# Verify aguments
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'thematic_id', 'category_id' ], 'disallow_empty', 'want_int'],
                                         ]);
	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::DisplayCreateSubject", ac_FormatErrors($errors));
	}
	$values->{subject_id} = 0;
	$values->{subject} = "";
	$values->{message} = "";
	$values->{post_it} = '0';
	$values->{open} = '1';
	#
	# Get and memorize referer
	#
	my $session = $context->GetSession();
	$session->{forum}->{referer} = $context->GetReferer();

	return $self->DisplaySubjectDialog($context, $values, $errors, 1);
}
# ============================================================================
# DisplayMessageDialog
# args :
# ============================================================================
sub DisplayMessageDialog {
	my ($self, $context, $values, $errors) = @_;
	print STDERR "Forum::DisplayMessageDialog\n" if($debug);
	#
	# Get referer for cancel action
	#
	my $cancel_url = new Mioga2::URI($context->GetConfig(), group  => $context->GetGroup()->GetIdent(),
                                      public => 0,
                                      application => 'Forum',
                                      method => 'DisplaySubject',
                                      args   => { subject_id => $values->{subject_id},
                                              }
                                      );
	my $session = $context->GetSession();
	if (exists($session->{forum}->{referer})) {
		print STDERR "Forum::DisplayMessageDialog session exists\n" if($debug);
		$cancel_url = $session->{forum}->{referer}; 
	}
	print STDERR "Forum::DisplayMessageDialog cancel_url = $cancel_url\n" if($debug);
	#
	# Generate XML
	#
	my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayMessageDialog>";
	$xml .= $context->GetXML;
	$xml .= $self->GetXMLUserParameters($context);
	$xml .= $self->GetXMLMainActionList($context);
	$xml .= ac_XMLifyErrors($errors);
	$xml .= '<cancel_url>' . st_FormatXMLString($cancel_url) . '</cancel_url>';
	$xml .= '<EditMessage>';
	$xml .= '<category_id>' . $values->{category_id} . '</category_id>';
	$xml .= '<thematic_id>' . $values->{thematic_id} . '</thematic_id>';
	$xml .= '<subject_id>' . $values->{subject_id} . '</subject_id>';
	$xml .= '<message_id>' . $values->{message_id} . '</message_id>';
	$xml .= '<message>' . st_FormatXMLString($values->{message}) . '</message>';
	$xml .= '</EditMessage>';
	$xml .= $self->GetXMLLastMessagesList($context, $values->{subject_id}, 0);
	$xml .= "</DisplayMessageDialog>";
	print STDERR "Forum::DisplayMessageDialog xml = $xml\n" if($debug);

	my $content = new Mioga2::Content::XSLT($context, stylesheet => "forum.xsl",
														locale_domain => "forum_xsl");
	$content->SetContent($xml);
	return $content;
}
# ============================================================================
# DisplayCreateMessage
# args :
#    subject_id (mandatory)
#    category_id (mandatory)
#    thematic_id (mandatory)
# ============================================================================
sub DisplayCreateMessage {
	my ($self, $context) = @_;
	print STDERR "Forum::DisplayCreateMessage\n" if($debug);
	#
	# Verify aguments
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'subject_id', 'thematic_id', 'category_id' ], 'want_int', 'disallow_empty'],
                                         ]);
	if (scalar(@$errors) > 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::DisplayCreateMessage", ac_FormatErrors($errors));
	}
	#TODO for 'cite' option
	if (!exists($values->{ref_mess_id})) {
		$values->{ref_mess_id} = 0;
	}
	$values->{message_id} = 0;
	$values->{message} = "";
	#
	# Get and memorize referer
	#
	my $session = $context->GetSession();
	$session->{forum}->{referer} = $context->GetReferer();

	return $self->DisplayMessageDialog($context, $values, $errors);
										 
}
# ============================================================================
# EditMessage
# args :
#    message_id (mandatory)
# ============================================================================
sub EditMessage {
	my ($self, $context) = @_;
	print STDERR "Forum::EditMessage\n" if($debug);
	#
	# Verify aguments
	#
	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'message_id' ], 'want_int', 'disallow_empty'],
                                         ]);
	if (scalar(@$errors) != 0) {
		throw Mioga2::Exception::Application("Mioga2::Forum::EditMessage", __"No message_id arg");
	}
	if (!exists($values->{ref_mess_id})) {
		$values->{ref_mess_id} = 0;
	}
	$self->{user_params} = $self->DBGetUserParameters($context->GetConfig()->GetDBH(), $context->GetUser()->GetRowid());
	#
	# Get message datas and verify forum_id
	#
	my $result = $self->DBGetMessage($context->GetConfig()->GetDBH(), $values->{message_id});
	if ($result->{forum_id} != $self->{forum_rowid}) {
		throw Mioga2::Exception::Application("Mioga2::Forum::EditMessage", __"You are not authorized in this forum");
	}
	#
	# initialize values to display
	#
	$values->{category_id} = $result->{category_id};
	$values->{thematic_id} = $result->{thematic_id};
	$values->{subject_id} = $result->{subject_id};
	$values->{message} = $result->{message};
	#
	# Get and memorize referer
	#
	my $session = $context->GetSession();
	$session->{forum}->{referer} = $context->GetReferer();

	return $self->DisplayMessageDialog($context, $values, $errors);
}
# ============================================================================
# UpdateMessage
# args :
# ============================================================================
sub UpdateMessage {
	my ($self, $context) = @_;
	print STDERR "Forum::UpdateMessage\n" if($debug);

	my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'thematic_id', 'category_id', 'subject_id', 'message_id', 'message' ], 'disallow_empty'],
                                         ]);
	print STDERR "Forum::UpdateMessage values = ". Dumper($values) . "\n" if($debug);
	print STDERR "Forum::UpdateMessage errors = ". Dumper($errors) . "\n" if($debug);
	my $message_id = $values->{message_id};
	my $user_id = $context->GetUser()->GetRowid();
	if (scalar(@$errors) == 0) {
		if ($message_id == 0) {
			$values->{author_id} = $user_id;
			$values->{modifier_id} = $user_id;
			$message_id = $self->DBCreateMessage($context->GetConfig()->GetDBH(), $values->{subject_id}, $user_id, $values->{message});
		}
		else {
			$values->{modifier_id} = $user_id;
			$self->DBUpdateMessage($context->GetConfig()->GetDBH(), $message_id, $values);
		}
		# just to set modified date in subject record
		my $nothing = {};
		$self->DBUpdateSubject($context->GetConfig->GetDBH(), $values->{subject_id}, $nothing);
		$self->SendAlertMail($context, $values);
        $self->NotifyUpdate($context, type => ['RSS', 'SEARCH']);
		return $self->DisplaySubject($context);
	}
	else {
		return $self->DisplayMessageDialog($context, $values, $errors);
	}
}
# ============================================================================
# Private Methods
# ============================================================================

=head2 SendMailToSubscribers

 SendMailToSubscribers gets the subscriber list and send the given mail.

=cut

# ============================================================================
sub SendMailToSubscribers
{
  my ($self, $context, $entity, $subject_id, $user_id) = @_;
  print STDERR "Forum::SendMailToSubscribers\n" if($debug);
  my $config = $context->GetConfig();

  my @sub_list = $self->DBGetSubscriberList($config->GetDBH(), $subject_id, $user_id);
  my $list = new Mioga2::Old::UserList($config, rowid_list => \@sub_list);

  $list->SendMail($entity);
  $self->DBSetSubjectSendMail($config->GetDBH(), $subject_id);
}
# ============================================================================


=head2 SendMailToModerators

 SendMailToModerators gets the moderators list and send the given mail.

=cut

# ============================================================================
sub SendMailToModerators
{
	my ($self, $context, $entity) = @_;
	my $group = $context->GetGroup();
	my $app_desc = $context->GetAppDesc();
	my $config = $context->GetConfig();

	my @mod_list = ProfileGetUsersForAuthzFunction($config->GetDBH(), $group->GetRowid(), $app_desc->GetIdent(), 'Moder');
	my $list = new Mioga2::Old::UserList($config, rowid_list => \@mod_list);

	$list->SendMail($entity);
}
# ============================================================================

=head2 SendAlertMail

 SendAlertMail checks if it needs to send mail and
 send to user if moderator edit a message
 send to moderator if user post a message

 $values is a hash with ID of author and modifier

=cut

# ============================================================================
sub SendAlertMail
{
	my ($self, $context, $values) = @_;
	my $group = $context->GetGroup();
	my $user = $context->GetUser();
	my $app_desc = $context->GetAppDesc();
	my $config = $context->GetConfig();

	my $sender  = $user->GetName() . " <" . $user->GetEmail() . ">";
	print STDERR "Forum::SendAlertMail values = " . Dumper($values) . "\n" if($debug);
	#
	# User's post
	#
	if ($values->{modifier_id} == $values->{author_id}) {
		print STDERR "   mail_alert = $self->{mail_alert}\n" if($debug);
    if ($self->{mail_alert}) {
      # Create mail message
			my $subject = __"New message posted in Forum";

			my $message = __"Hello" . "\n";
			$message .= "\n";
			$message .= __"This is an automatic message, please do not answer." . "\n";
			$message .= __"A new message as been posted in Forum :" . "\n";
			my $result = $self->DBGetSubjectEnv($config->GetDBH(), $values->{subject_id});
			$message .= __"Category" . " : " . $result->{category_label} . "\n";
			$message .= __"Thematic" . " : " . $result->{thematic_label} . "\n";
			$message .= __"Subject" . " : " . $result->{subject} . "\n";
			$message .= __"please look at :" . "\n";
        	my $uri = new Mioga2::URI($config, group => $group->GetIdent(), public => 0,
														application => $app_desc->GetIdent(),
														method => 'DisplayNewMessages',
														args => { subject_id => $values->{subject_id},
																  thematic_id => $values->{thematic_id},
																  category_id => $values->{category_id},
																}
													);
			$message .= $config->GetProtocol()."://".$config->GetServerName().$uri->GetURI() . "\n";

			my $entity = MIME::Entity->build(From   => $sender,
										Subject => $subject,
										Charset => "UTF-8",
										Data    => $message);
			#if ($self->{moderated}) {
      # print STDERR "Forum::SendAlertMail to moderators\n" if($debug);
      # $self->SendMailToModerators($context, $entity);
      #}
      print STDERR "Forum::SendAlertMail to subscribers\n" if($debug);
      $self->SendMailToSubscribers($context, $entity, $values->{subject_id}, $values->{modifier_id});
		}
	}
	#
	# Moderator action
	#
	else {
		if ($self->{subscription} && $self->{user_params}->{subscribe} && $self->{mail_alert}) {
			print STDERR "Forum::SendAlertMail to user\n" if($debug);
			my $subject = __"A moderator has edited your message";

			my $message = __"Hello" . "\n";
			$message .= "\n";
			$message .= __"This is an automatic message, please do not answer." . "\n";
			$message .= __"Your message has been edited by  :" . "\n";
			$message .= $user->GetName() . "\n";
			$message .= __"please look at :" . "\n";
        	my $uri = new Mioga2::URI($config, group => $group->GetIdent(), public => 0,
														application => $app_desc->GetIdent(),
														method => 'DisplayNewMessages',
														args => { subject_id => $values->{subject_id},
																  thematic_id => $values->{thematic_id},
																  category_id => $values->{category_id},
																}
													);
			$message .= $config->GetProtocol()."://".$config->GetServerName().$uri->GetURI() . "\n";

			my $entity = MIME::Entity->build(From   => $sender,
										Subject => $subject,
										Charset => "UTF-8",
										Data    => $message);
			my $result = $self->DBGetMessage($config->GetDBH(), $values->{message_id});
			my $u = new Mioga2::Old::User($config, rowid => $result->{author_id});
			$u->SendMail($entity);
		}
	}
}
# ============================================================================
# XML methods returns xml chunks to compose the whole XML document
#   GetXMLCategoriesAndThematics
#   GetXMLForumInfo
# ============================================================================

=head2 GetXMLForumInfo

 GetXMLForumInfo gives xml chunks for genral forum parameters

        <rowid>value</rowid>
        <simple_ihm>t or f</simple_ihm>
        <moderated>t or f</moderated>
        <subscription>t or f</subscription>
        <mail_alert>t or f</mail_alert>

=cut

# ============================================================================
sub GetXMLForumInfo
{
	my ($self, $context) = @_;
	my $xml = "";
	$xml .= '<rowid>' . $self->{forum_rowid} . '</rowid>';
	$xml .= '<simple_ihm>' . $self->{simple_ihm} . '</simple_ihm>';
	$xml .= '<moderated>' . $self->{moderated} . '</moderated>';
	$xml .= '<subscription>' . $self->{subscription} . '</subscription>';
	$xml .= '<mail_alert>' . $self->{mail_alert} . '</mail_alert>';
	return $xml;
}
# ============================================================================
sub GetXMLUserParameters
{
	my ($self, $context) = @_;
	my $xml = "<UserParams>";
	#
	# Get informations on user to set actions
	#
	if ($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Anim")) {
		$xml .= '<anim_right label="' . st_FormatXMLString(__"Administration") . '" />';
	}
	if ($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Moder")) {
		$xml .= '<moder_right label="' . st_FormatXMLString(__"Moderate") . '" />';
	}
	if ($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), "Write")) {
		$xml .= '<write_right label="' . st_FormatXMLString(__"Write") . '" />';
	}
	#
	# Get forum preferences
	#
	$xml .= '<subscribe label="' . st_FormatXMLString(__"Subscribe mail alert") . '" >';
	$xml .= $self->{user_params}->{subscribe};
	$xml .= '</subscribe>';
	#
	$xml .= "</UserParams>";
	return $xml;
}
# ============================================================================
sub GetXMLMainActionList
{
	my ($self, $context) = @_;
	my $xml = "";
	#
	# Construct base URI for actions
	#
	my $baseURI = $context->GetConfig()->GetBinURI();
	$baseURI .= "/" .  $context->GetGroup()->GetIdent();
	$baseURI .= "/" . $self->GetAppDesc->{ident};
	print STDERR "Forum::GetXMLMainActionList baseURI = $baseURI\n" if($debug);
	#
	# Construct actions
	#
	$xml .= '<action id="add_category" method="ProcessAnimAction" label="' . st_FormatXMLString(__"Add category") . '" />';
	$xml .= '<action id="suppress_category" method="ProcessAnimAction" label="' . st_FormatXMLString(__"Suppress category") . '" />';
	$xml .= '<action id="rename_category" method="ProcessAnimAction" label="' . st_FormatXMLString(__"Rename category") . '" />';

	$xml .= '<action id="add_thematic" method="ProcessAnimAction" label="' . st_FormatXMLString(__"Add thematic") . '" />';
	$xml .= '<action id="suppress_thematic" method="ProcessAnimAction" label="' . st_FormatXMLString(__"Suppress thematic") . '" />';
	$xml .= '<action id="rename_thematic" method="ProcessAnimAction" label="' . st_FormatXMLString(__"Rename thematic") . '" />';

	$xml .= '<action id="set_parameters" method="ProcessAnimAction" label="' . st_FormatXMLString(__"Set parameters") . '" />';

	$xml .= '<action id="subscribe" method="ProcessMainAction" label="' . st_FormatXMLString(__"Subscribe") . '" />';
	$xml .= '<action id="view_thematic" method="ProcessViewAction" label="' . st_FormatXMLString(__"View thematic") . '" />';
	$xml .= '<action id="create_subject" method="ProcessViewAction" label="' . st_FormatXMLString(__"Create subject") . '" />';
	$xml .= '<action id="suppress_subject" method="ProcessAnimAction" label="' . st_FormatXMLString(__"Suppress subject") . '" />';
	$xml .= '<action id="edit_subject" method="ProcessAnimAction" label="' . st_FormatXMLString(__"Edit subject") . '" />';

	$xml .= '<action id="create_message" method="ProcessViewAction" label="' . st_FormatXMLString(__"Post new message") . '" />';
	$xml .= '<action id="view_new_messages" method="ProcessViewAction" label="' . st_FormatXMLString(__"View new messages") . '" />';
	$xml .= '<action id="edit_message" method="ProcessViewAction" label="' . st_FormatXMLString(__"Edit message") . '" />';
	$xml .= '<action id="signal_message" method="ProcessViewAction" label="' . st_FormatXMLString(__"Signal to moderator") . '" />';
	$xml .= '<action id="suppress_message" method="ProcessAnimAction" label="' . st_FormatXMLString(__"Suppress message") . '" />';

	return $xml;
}
# ============================================================================
sub FormatXMLCategory
{
	my ($self, $context, $category) = @_;
	my $xml = "";
	$xml .= '<category label="' . st_FormatXMLString($category->{label}) . '" rowid="' . $category->{rowid} . '" >';
	my $thematics = $self->DBGetThematics($context->GetConfig()->GetDBH(), $category->{rowid});
	foreach my $t (@$thematics) {
		$xml .= $self->FormatXMLThematic($context, $category->{rowid}, $t);
	}
	$xml .= '</category>';
	return $xml;
}
# ============================================================================
sub GetXMLCategory
{
	my ($self, $context, $category_id) = @_;
	my $xml = "";

	my $category = $self->DBGetCategory($context->GetConfig()->GetDBH(), $category_id);

	$xml .= $self->FormatXMLCategory($context, $category);
	return $xml;
}
# ============================================================================
sub GetXMLCategoriesAndThematics
{
	my ($self, $context) = @_;
	my $xml = "";

	my $categories = $self->DBGetCategories($context->GetConfig()->GetDBH());

	$xml .= "<Categories>";
	foreach my $entry (@$categories) {
		$xml .= $self->FormatXMLCategory($context, $entry);
	}
	$xml .= "</Categories>";

	return $xml;
}
# ============================================================================
sub FormatXMLThematic
{
	my ($self, $context, $category_id, $thematic) = @_;
	my $xml = "";

	if (!exists($thematic->{firstname})) {
		$thematic->{firstname} = "";
	}
	if (!exists($thematic->{lastname})) {
		$thematic->{lastname} = "";
	}
	if (!exists($thematic->{subject_id})) {
		$thematic->{subject_id} = "";
	}
	if (!exists($thematic->{message_id})) {
		$thematic->{message_id} = "";
	}
	if (!exists($thematic->{subject_count})) {
		$thematic->{subject_count} = 0;
	}
	if (!exists($thematic->{message_count})) {
		$thematic->{message_count} = 0;
	}
	if (!exists($thematic->{lastpost})) {
		$thematic->{lastpost} = "";
	}
	else {
		$thematic->{lastpost} = du_ISOToXMLInUserLocale($thematic->{lastpost}, $context->GetUser());
	}

	$xml .= '<thematic>';
	$xml .= '<label>' . st_FormatXMLString($thematic->{label}) . '</label>';
	$xml .= '<rowid>' . $thematic->{rowid} . '</rowid>';
	$xml .= '<category_id>' . $category_id . '</category_id>';
	$xml .= '<nb_subjects>' . $thematic->{subject_count} . '</nb_subjects>';
	$xml .= '<nb_messages>' . $thematic->{message_count} . '</nb_messages>';
	$xml .= '<firstname>' . st_FormatXMLString($thematic->{firstname}) . '</firstname>';
	$xml .= '<lastname>' . st_FormatXMLString($thematic->{lastname}) . '</lastname>';
	$xml .= '<subject_id>' . $thematic->{subject_id} . '</subject_id>';
	$xml .= '<message_id>' . $thematic->{message_id} . '</message_id>';
	$xml .= '<lastpost>' . $thematic->{lastpost} . '</lastpost>';
	$xml .= '<description>' . st_FormatXMLString($thematic->{description}) . '</description>';
	$xml .= '</thematic>';

	return $xml;
}
# ============================================================================
sub GetXMLThematic
{
	my ($self, $context, $rowid) = @_;
	my $xml = "";
	my $thematic = $self->DBGetThematic($context->GetConfig()->GetDBH(), $rowid);

	$xml .= $self->FormatXMLThematic($context, $thematic->{category_id}, $thematic);
	return $xml;
}
# ============================================================================
# FormatXMLDisplayThematic
# args :
# ============================================================================
sub FormatXMLDisplayThematic {
	my ($self, $context, $xmltag, $category_id, $thematic_id) = @_;
	print STDERR "Forum::FormatXMLDisplayThematic\n" if($debug);
	# page_num
	my $page_num = 1;
	my $subjects_per_page = 50;   # TODO set it user variable
	my $subject_count = $self->DBGetSubjectCount($context->GetConfig()->GetDBH(), $thematic_id);
	my $nb_pages = ceil($subject_count / $subjects_per_page);
	if (exists($context->{args}->{page})) {
		$page_num = $context->{args}->{page};
	}
	print STDERR "Forum::FormatXMLDisplayThematic arg page_num = $page_num\n" if($debug);
	if (exists($context->{args}->{view})) {
		if ($context->{args}->{view} eq 'next') {
			$page_num++;
		}
		elsif ($context->{args}->{view} eq 'previous') {
			$page_num--;
		}
	}
	if ($page_num > $nb_pages) {
		$page_num = $nb_pages;
	}
	if ($page_num < 1) {
		$page_num = 1;
	}
	print STDERR "Forum::FormatXMLDisplayThematic calculated page_num = $page_num\n" if($debug);
	#
	# Generate XML
	#
	my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<$xmltag>";
	$xml .= $context->GetXML;
	$xml .= $self->GetXMLForumInfo($context);
	$xml .= $self->GetXMLUserParameters($context);
	$xml .= $self->GetXMLMainActionList($context);
	$xml .= "<Thematic>";
	$xml .= $self->GetXMLCategory($context, $category_id);
	$xml .= $self->GetXMLThematic($context, $thematic_id);
	$xml .= "</Thematic>";
	$xml .= $self->GetXMLSubjectList($context, $thematic_id, $page_num, $nb_pages, $subjects_per_page);
	$xml .= "</$xmltag>";

	print STDERR "Forum::FormatXMLDisplayThematic xml = $xml\n" if($debug);
	return $xml;
}
# ============================================================================
sub FormatXMLSubject
{
	my ($self, $context, $entry) = @_;
	my $xml = "";

	my $lastvisit_sec = 0;
	if (exists($entry->{last_visit})) {
		$lastvisit_sec = du_ISOToSecond($entry->{last_visit});
	}
	my $lastpost_sec = du_ISOToSecond($entry->{lastpost});
	print STDERR "lastvisit_sec = $lastvisit_sec lastpost_sec = $lastpost_sec  lastpost = " . $entry->{lastpost} . "\n" if($debug);

	$entry->{lastpost} = du_ISOToXMLInUserLocale($entry->{lastpost}, $context->GetUser());
	print STDERR "after XML lastpost = " . $entry->{lastpost} . "\n" if($debug);

	$xml .= '<subject>';
	$xml .= '<label>' . st_FormatXMLString($entry->{subject}) . '</label>';
	$xml .= '<rowid>' . $entry->{rowid} . '</rowid>';
	$xml .= '<post_it>' . $entry->{post_it} . '</post_it>';
	$xml .= '<open>' . $entry->{open} . '</open>';
	$xml .= '<nb_messages>' . $entry->{message_count} . '</nb_messages>';
	$xml .= '<firstname>' . st_FormatXMLString($entry->{firstname}) . '</firstname>';
	$xml .= '<lastname>' . st_FormatXMLString($entry->{lastname}) . '</lastname>';
	$xml .= '<lastpost>' . $entry->{lastpost} . '</lastpost>';
	if ($lastvisit_sec < $lastpost_sec) {
		$xml .= '<new_messages />';
	}
	$xml .= '</subject>';
	return $xml;
}
# ============================================================================
sub GetXMLSubjectList
{
	my ($self, $context, $thematic_id, $page_num, $nb_pages, $subjects_per_page) = @_;
	my $xml = "";

	my $subjects = $self->DBGetSubjectList($context->GetConfig()->GetDBH(), $thematic_id, $context->GetUser()->GetRowid(), $page_num, $subjects_per_page);

	$xml .= '<Subjects page="' . $page_num . '" nb_pages="' . $nb_pages . '" >';
	foreach my $entry (@$subjects) {
		$xml .= $self->FormatXMLSubject($context, $entry);
	}
	$xml .= "</Subjects>";

	return $xml;
}
# ============================================================================
sub GetXMLSubjectEnv
{
	my ($self, $context, $subject_id) = @_;
	my $xml = "";
	my $result = $self->DBGetSubjectEnv($context->GetConfig()->GetDBH(), $subject_id);

	$xml .= '<subject_env>';
	$xml .= '<category_label>' . $result->{category_label} . '</category_label>';
	$xml .= '<category_id>' . $result->{category_id} . '</category_id>';
	$xml .= '<thematic_label>' . $result->{thematic_label} . '</thematic_label>';
	$xml .= '<thematic_id>' . $result->{thematic_id} . '</thematic_id>';
	$xml .= '</subject_env>';
	return $xml;
}
# ============================================================================
sub GetXMLSubjectDatas
{
	my ($self, $context, $subject_id) = @_;
	my $xml = "";
	my $subject = $self->DBGetSubject($context->GetConfig()->GetDBH(), $subject_id);

	$xml .= $self->FormatXMLSubject($context, $subject);
	return $xml;
}
# ============================================================================
sub GetXMLMessageList
{
	my ($self, $context, $subject_id, $page_num, $subjects_per_page) = @_;
	my $xml = "";

	my $messages = $self->DBGetMessageList($context->GetConfig()->GetDBH(), $subject_id, $page_num, $subjects_per_page);
	my $nb_pages = $self->DBGetMessageCount($context->GetConfig()->GetDBH(), $subject_id) / $subjects_per_page;

	my $user_id = $context->GetUser()->GetRowid();
	$xml .= '<Messages page="' . $page_num . '" nb_pages="' . $nb_pages . '" >';
	foreach my $entry (@$messages) {
		my $created_sec = du_ISOToSecond($entry->{created});
		my $modified_sec = du_ISOToSecond($entry->{modified});
		print STDERR "Forum::GetXMLMessageList  created_sec : $created_sec  modified_sec : $modified_sec\n" if ($debug);
		my $created = du_GetDateXMLInUserLocale($created_sec, $context->GetUser());
		my $modified = du_GetDateXMLInUserLocale($modified_sec, $context->GetUser());
		$xml .= '<message>';
		$xml .= '<created>' . $created . '</created>';
		$xml .= '<rowid>' . $entry->{rowid} . '</rowid>';
		$xml .= '<modified>' . $modified . '</modified>';
		$xml .= '<display_footer />';
		if ($created_sec < $modified_sec) {
			$xml .= '<have_been_modified />';
		}
		if ($user_id == $entry->{author_id}) {
			$xml .= '<author />';
		}
		$xml .= '<firstname>' . st_FormatXMLString($entry->{firstname}) . '</firstname>';
		$xml .= '<lastname>' . st_FormatXMLString($entry->{lastname}) . '</lastname>';
		$xml .= '<modifier_firstname>' . st_FormatXMLString($entry->{modifier_firstname}) . '</modifier_firstname>';
		$xml .= '<modifier_lastname>' . st_FormatXMLString($entry->{modifier_lastname}) . '</modifier_lastname>';
		$xml .= '<message_text>' . st_FormatXMLString($entry->{message}) . '</message_text>';
		$xml .= '</message>';
	}
	$xml .= "</Messages>";

	return $xml;
}
# ============================================================================
sub GetXMLLastMessagesList
{
	my ($self, $context, $subject_id, $base_id) = @_;
	my $xml = "";

	my $messages = $self->DBGetLastMessagesList($context->GetConfig()->GetDBH(), $subject_id, $base_id, 10);

	$xml .= '<Messages>';
	foreach my $entry (@$messages) {
		my $created_sec = du_ISOToSecond($entry->{created});
		my $modified_sec = du_ISOToSecond($entry->{modified});
		my $created = du_GetDateXMLInUserLocale($created_sec, $context->GetUser());
		my $modified = du_GetDateXMLInUserLocale($modified_sec, $context->GetUser());
		$xml .= '<message>';
		$xml .= '<no_menu />';
		$xml .= '<created>' . $created . '</created>';
		$xml .= '<rowid>' . $entry->{rowid} . '</rowid>';
		$xml .= '<modified>' . $modified . '</modified>';
		$xml .= '<firstname>' . st_FormatXMLString($entry->{firstname}) . '</firstname>';
		$xml .= '<lastname>' . st_FormatXMLString($entry->{lastname}) . '</lastname>';
		$xml .= '<modifier_firstname>' . st_FormatXMLString($entry->{modifier_firstname}) . '</modifier_firstname>';
		$xml .= '<modifier_lastname>' . st_FormatXMLString($entry->{modifier_lastname}) . '</modifier_lastname>';
		$xml .= '<message_text>' . st_FormatXMLString($entry->{message}) . '</message_text>';
		$xml .= '</message>';
	}
	$xml .= "</Messages>";

	return $xml;
}
# ============================================================================
sub GetXMLNewMessagesList
{
	my ($self, $context, $subject_id, $user_id, $last_visit) = @_;
	my $xml = "";
	print STDERR "Forum::GetXMLNewMessagesList last_visit = $last_visit\n" if($debug);

	my $messages = $self->DBGetNewMessagesList($context->GetConfig()->GetDBH(), $subject_id, $user_id, $last_visit);

	$xml .= '<Messages>';
	foreach my $entry (@$messages) {
		my $created_sec = du_ISOToSecond($entry->{created});
		my $modified_sec = du_ISOToSecond($entry->{modified});
		my $created = du_GetDateXMLInUserLocale($created_sec, $context->GetUser());
		my $modified = du_GetDateXMLInUserLocale($modified_sec, $context->GetUser());
		$xml .= '<message>';
		$xml .= '<created>' . $created . '</created>';
		$xml .= '<rowid>' . $entry->{rowid} . '</rowid>';
		$xml .= '<modified>' . $modified . '</modified>';
		$xml .= '<firstname>' . st_FormatXMLString($entry->{firstname}) . '</firstname>';
		$xml .= '<lastname>' . st_FormatXMLString($entry->{lastname}) . '</lastname>';
		$xml .= '<modifier_firstname>' . st_FormatXMLString($entry->{modifier_firstname}) . '</modifier_firstname>';
		$xml .= '<modifier_lastname>' . st_FormatXMLString($entry->{modifier_lastname}) . '</modifier_lastname>';
		$xml .= '<message_text>' . st_FormatXMLString($entry->{message}) . '</message_text>';
		$xml .= '<display_footer />';
		$xml .= '</message>';
	}
	$xml .= "</Messages>";

	return $xml;
}
# ============================================================================
# RevokeUserFromGroup ($self, $config, $group_id, $user_id, $group_animator_id)
#
#    Remove user data in database when a user is revoked of a group.
#
#    This function is called when :
#    - a user is revoked of a group
#    - a team is revoked of a group for each team member not namly
#      invited in group
#    - a user is revoked of a team  for each group where the team is
#      member and the user not namly invited in group
#
# ============================================================================
sub RevokeUserFromGroup
{
	my ($self, $config, $group_id, $user_id, $anim_id) = @_;
	print STDERR "Forum::RevokeUserFromGroup user_id = $user_id  group_id = $group_id  anim_id = $anim_id\n" if($debug);
	my $dbh = $config->GetDBH();

	my $sql = "UPDATE forum_message set author_id = $anim_id FROM forum_subject, forum_thematic, forum_category, forum WHERE author_id = $user_id"
				. " AND forum_message.subject_id=forum_subject.rowid"
				. " AND forum_subject.thematic_id=forum_thematic.rowid"
				. " AND forum_thematic.category_id = forum_category.rowid"
				. " AND forum_category.forum_id = forum.rowid"
				. " AND forum.group_id = $group_id";
	ExecSQL($dbh, $sql);
	$sql = "UPDATE forum_message set modifier_id = $anim_id FROM forum_subject, forum_thematic, forum_category, forum WHERE modifier_id = $user_id"
				. " AND forum_message.subject_id=forum_subject.rowid"
				. " AND forum_subject.thematic_id=forum_thematic.rowid"
				. " AND forum_thematic.category_id = forum_category.rowid"
				. " AND forum_category.forum_id = forum.rowid"
				. " AND forum.group_id = $group_id";
	ExecSQL($dbh, $sql);
	$sql = "DELETE FROM forum_user USING forum WHERE user_id = $user_id AND forum_user.forum_id=forum.rowid AND forum.group_id = $group_id";
	ExecSQL($dbh, $sql);
	$sql = "DELETE FROM forum_user2subject"
                . " USING forum_subject, forum_thematic, forum_category, forum"
				. " WHERE forum_user2subject.subject_id=forum_subject.rowid"
					. " AND forum_subject.thematic_id=forum_thematic.rowid"
					. " AND forum_thematic.category_id=forum_category.rowid"
					. " AND forum_category.forum_id=forum.rowid"
					. " AND forum.group_id = $group_id AND user_id = $user_id";
	ExecSQL($dbh, $sql);
}
# ============================================================================
# DeleteGroupData ($self, $config, $group_id)
#
#    Remove group data in database when a group is deleted.
#
#    This function is called when a group/user/resource is deleted.
#
# ============================================================================
sub DeleteGroupData
{
	my ($self, $config, $group_id) = @_;
	print STDERR "Forum::DeleteGroupData group_id = $group_id\n" if($debug);
	my $dbh = $config->GetDBH();

	my $sql = "DELETE FROM forum_user2subject USING forum_subject, forum_thematic, forum_category, forum"
	#my $sql = "DELETE FROM forum_user2subject"
				. " WHERE forum_user2subject.subject_id=forum_subject.rowid"
					. " AND forum_subject.thematic_id=forum_thematic.rowid"
					. " AND forum_thematic.category_id=forum_category.rowid"
					. " AND forum_category.forum_id=forum.rowid"
					. " AND forum.group_id = $group_id";
	ExecSQL($dbh, $sql);
	$sql = "DELETE FROM forum_user USING forum"
	#$sql = "DELETE FROM forum_user"
				. " WHERE forum_user.forum_id=forum.rowid"
					. " AND forum.group_id = $group_id";
	ExecSQL($dbh, $sql);
	$sql = "DELETE FROM forum_message USING forum_subject, forum_thematic, forum_category, forum"
	#$sql = "DELETE FROM forum_message"
				. " WHERE forum_message.subject_id=forum_subject.rowid"
					. " AND forum_subject.thematic_id=forum_thematic.rowid"
					. " AND forum_thematic.category_id=forum_category.rowid"
					. " AND forum_category.forum_id=forum.rowid"
					. " AND forum.group_id = $group_id";
	ExecSQL($dbh, $sql);
	$sql = "DELETE FROM forum_subject USING forum_thematic, forum_category, forum"
	#$sql = "DELETE FROM forum_subject"
				. " WHERE forum_subject.thematic_id=forum_thematic.rowid"
					. " AND forum_thematic.category_id=forum_category.rowid"
					. " AND forum_category.forum_id=forum.rowid"
					. " AND forum.group_id = $group_id";
	ExecSQL($dbh, $sql);
	$sql = "DELETE FROM forum_thematic USING forum_category, forum"
	#$sql = "DELETE FROM forum_thematic"
				. " WHERE forum_thematic.category_id=forum_category.rowid"
					. " AND forum_category.forum_id=forum.rowid"
					. " AND forum.group_id = $group_id";
	ExecSQL($dbh, $sql);
	$sql = "DELETE FROM forum_category USING forum"
	#$sql = "DELETE FROM forum_category"
				. " WHERE forum_category.forum_id=forum.rowid"
					. " AND forum.group_id = $group_id";
	ExecSQL($dbh, $sql);
	$sql = "DELETE FROM forum WHERE forum.group_id = $group_id";
	ExecSQL($dbh, $sql);
}

# ============================================================================
# GetRSSFeed
#   Retrieve feed of posts
# ============================================================================

sub GetRSSFeed {
    my ($self, $context, $ident, $feed, $max)   = @_;
    
    my $config  = $context->GetConfig;
    my $group;
    try {
        $group = Mioga2::Old::Group->new($config, ident => $ident);
    }
    otherwise {
        $group = Mioga2::Old::User->new($config, ident => $ident);
    };

    if (!$feed) {
        $feed   = XML::Atom::SimpleFeed->new(
            title   => __x("Forum feed of {group} group", group => $group->GetIdent),
            link    => { href => $config->GetProtocol . "://" . $config->GetServerName . $context->{uri}->GetURI, rel => 'self' },
            link    => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/Forum/DisplayMain",
            id      => "data:,group:".$group->GetIdent.":forum",
        );
    }
    
    $max            = 20 unless $max;
    my $group_id    = $group->GetRowid;
    my $group_ident = $group->GetIdent;
    my $posts       = SelectMultiple($config->GetDBH, "SELECT fm.*, fs.subject, ft.label AS thematic, fc.label AS category, u.firstname, u.lastname from m_user_base AS u, forum AS f, forum_category AS fc, forum_thematic AS ft, forum_subject AS fs, forum_message AS fm where f.group_id = $group_id AND fc.forum_id=f.rowid AND ft.category_id=fc.rowid AND fs.thematic_id=ft.rowid AND fm.subject_id=fs.rowid AND u.rowid=fm.author_id ORDER BY fm.modified DESC");

    foreach my $post (@$posts) {
        $feed->add_entry(
            title       => __x("[{group}] Message in {category}/{thematic}/{subject}", group => $group_ident, category => $post->{category}, thematic => $post->{thematic}, subject => $post->{subject}),
            link        => $config->GetProtocol . "://" . $config->GetServerName . $config->GetBinURI . "/" . $group->GetIdent . "/Forum/DisplaySubject?subject_id=$post->{subject_id}",
            id          => "data:,group:".$group->GetIdent.":forum:$post->{rowid}",
            author      => "$post->{firstname} $post->{lastname}",
            published   => Mioga2::Classes::Time->FromPSQL($post->{created})->RFC3339DateTime,
            updated     => Mioga2::Classes::Time->FromPSQL($post->{modified})->RFC3339DateTime,
            content     => $post->{message},
        );
    }
    return $feed;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


