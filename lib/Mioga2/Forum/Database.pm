# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME

Mioga2::Forum::Database - The Mioga2 Forum application database access methods

=cut

# ============================================================================

package Mioga2::Forum;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'forum';

use Error qw(:try);
use POSIX qw(ceil);
use Mioga2::URI;
use Mioga2::Content::XSLT;
use Mioga2::Content::ASIS;
use Mioga2::tools::database;
use Mioga2::tools::args_checker;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Mioga2::tools::APIAuthz;
use Data::Dumper;
use XML::Atom::SimpleFeed;

my $debug = 0;

# ============================================================================
# DB Access
# This functions are used to get data from database :
#   DBCreateForum
#   DBCreateForumUser
#   DBAddCategory
#   DBAddThematic
#   DBGetForumInfo
#   DBGetCategory
#   DBGetCategories
#   DBGetThematics
#   DBGetThematic(rowid)
#   DBGetCategoriesAndThematics
#   DBUpdateCategory
#   DBUpdateThematic
#   DBDeleteCategory
#   DBDeleteThematic
# ============================================================================
sub DBCreateForum
{
	my ($self, $dbh, $group_id) = @_;
	my $sql = "INSERT INTO forum (created, modified, group_id) VALUES (now(), now(), $group_id)";
	ExecSQL($dbh, $sql);
}
# ============================================================================
sub DBCreateForumUser
{
	my ($self, $dbh, $user_id) = @_;
	print STDERR "Forum::DBCreateForumUser forum_rowid = " .  $self->{forum_rowid} . "\n" if($debug);
	my $sql = "INSERT INTO forum_user (forum_id, user_id) VALUES (" . $self->{forum_rowid} . ", $user_id);";
	print STDERR "Forum::DBCreateForumUser sql = $sql\n" if($debug);
	ExecSQL($dbh, $sql);
}
# ============================================================================
sub DBAddCategory
{
	my ($self, $dbh, $label) = @_;
	my $category_id = undef;

	my $sql = "INSERT INTO forum_category (created, modified, label, forum_id)"
					. " VALUES (now(), now(), '" . st_FormatPostgreSQLString($label) . "', " . $self->{forum_rowid} . ");";
	ExecSQL($dbh, $sql);

	$category_id = GetLastInsertId($dbh, "forum_category");

	return $category_id;
}
# ============================================================================
sub DBAddThematic
{
	my ($self, $dbh, $category_id, $label, $desc) = @_;
	my $sql = "INSERT INTO forum_thematic (created, modified, label, category_id, description)"
					. " VALUES (now(), now(), '" . st_FormatPostgreSQLString($label) . "', " . $category_id
					.  ", '" . st_FormatPostgreSQLString($desc) . "' );";
	ExecSQL($dbh, $sql);
	my $thematic_id = GetLastInsertId($dbh, "forum_thematic");

	return $thematic_id;
}
# ============================================================================
sub DBCreateSubject
{
	my ($self, $dbh, $values) = @_;
	my $sql;

	print STDERR "Forum::DBCreateSubject\n" if($debug);
	BeginTransaction($dbh);
	try {
		$values->{created} = 'now()';
		$values->{modified} = 'now()';
		my $sql = BuildInsertRequest($values,
                                 table  => 'forum_subject',
                                 bool  => ['open', 'post_it' ],
                                 other  => ['created', 'modified', 'thematic_id'],
                                 string  => ['subject']
								 );
		print STDERR "Forum::DBCreateSubject sql = $sql\n" if($debug);
		ExecSQL($dbh, $sql);
		$values->{subject_id} = GetLastInsertId($dbh, "forum_subject");

		$sql = BuildInsertRequest($values,
                                 table  => 'forum_message',
                                 other  => ['created', 'modified', 'subject_id', 'author_id', 'modifier_id' ],
                                 string  => ['message']
								 );
		print STDERR "Forum::DBCreateSubject sql = $sql\n" if($debug);
		ExecSQL($dbh, $sql);
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};

	EndTransaction($dbh);

	return $values->{subject_id};
}
# ============================================================================
sub DBCreateMessage
{
	my ($self, $dbh, $subject_id, $author_id, $message) = @_;
	my $sql;
	my $mess_id = undef;

	BeginTransaction($dbh);
	try {
		$sql = "INSERT INTO forum_message (created, modified, author_id, modifier_id, subject_id, message)"
					. " VALUES (now(), now(), $author_id, $author_id, $subject_id, '" . st_FormatPostgreSQLString($message) . "' );";
		ExecSQL($dbh, $sql);
		$mess_id = GetLastInsertId($dbh, "forum_message");
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};

	EndTransaction($dbh);

	return $mess_id;
}
# ============================================================================
sub DBGetSubscriberList
{
  my ($self, $dbh, $subject_id, $user_id) = @_;

  my $sql = "SELECT forum_user.user_id FROM forum_user, forum_user2subject"
            ." WHERE forum_user.forum_id = $self->{forum_rowid}"
            ." AND forum_user.user_id = forum_user2subject.user_id"
            ." AND forum_user.user_id!=$user_id"
            ." AND forum_user2subject.subject_id=$subject_id"
            ." AND forum_user2subject.send_mail='f'";
  print STDERR "Forum::DBGetSubscriberList sql = $sql\n" if($debug);
  my $result = SelectMultiple($dbh, $sql);

  return (map { $_->{user_id} } @$result);
}
# ============================================================================

sub DBGetForumInfo
{
	my ($self, $dbh, $group_id) = @_;
	my $sql = "SELECT forum.* FROM forum WHERE forum.group_id = $group_id";
	print STDERR "Forum::DBGetForumInfo sql = $sql\n" if($debug);
	my $result = SelectSingle($dbh, $sql);
	#
	# If forum not created, create it and retry
	#
	if (!defined($result)) {
		$self->DBCreateForum($dbh, $group_id);
		$result = SelectSingle($dbh, $sql);
	}
	print STDERR "Forum::DBGetForumInfo result = ". Dumper($result) . "\n" if($debug);
	#
	# Get values
	#
	if (defined($result)) {
		$self->{forum_rowid} = $result->{rowid};
		$self->{simple_ihm} = $result->{simple_ihm};
		$self->{moderated} = $result->{moderated};
		$self->{subscription} = $result->{subscription};
		$self->{mail_alert} = $result->{mail_alert};
	}
	else {
		warn "Mioga2::Forum::DBGetForumInfo : Cannot get forum for the group : [". $self->{group_id} . "]";
		throw Mioga2::Exception::Application("Mioga2::Forum::DBGetForumInfo", __"Cannot get forum for the group");
	}
}
# ============================================================================
sub DBGetUserParameters
{
	my ($self, $dbh, $user_id) = @_;
	my $sql = "SELECT * FROM forum_user WHERE forum_id = " . $self->{forum_rowid} . " AND user_id=$user_id";
	my $result = SelectSingle($dbh, $sql);
	#
	# If the user never use forum, entry dooesn't exist : create it and retry
	#
	if (!defined($result)) {
		$self->DBCreateForumUser($dbh, $user_id);
		$result = SelectSingle($dbh, $sql);
	}
	print STDERR "Forum::DBGetUserParameters result = ". Dumper($result) . "\n" if($debug);
	#
	# Get values
	#
	if (!defined($result)) {
		warn "Mioga2::Forum::DBGetUserParameters : Cannot get forum parameters for the user : [". $user_id . "]";
		throw Mioga2::Exception::Application("Mioga2::Forum::DBGetUserParameters", __"Cannot get forum parameters for the user");
	}
	return $result;
}
# ============================================================================
sub DBSetForumInfo
{
	my ($self, $dbh, $values) = @_;
	$values->{modified} = 'now()';
	my $sql = BuildUpdateRequest($values,
                                 table  => 'forum',
                                 bool  => ['simple_ihm', 'moderated', 'subscription', 'mail_alert' ],
                                 other  => ['modified']
								 );
	$sql .= " WHERE rowid=" . $self->{forum_rowid};
	print STDERR "Forum::DBSetForumUser sql = $sql\n" if($debug);
	ExecSQL($dbh, $sql);
}
# ============================================================================
sub DBSetUserParameters
{
	my ($self, $dbh, $values) = @_;
	my $sql = BuildUpdateRequest($values,
                                 table  => 'forum_user',
                                 bool  => ['subscribe' ]
								 );
	$sql .= " WHERE forum_id=" . $self->{forum_rowid} . " AND user_id=" . $values->{user_id};
	print STDERR "Forum::DBSetUserParameters sql = $sql\n" if($debug);
	ExecSQL($dbh, $sql);
}
# ============================================================================
sub DBGetCategory
{
	my ($self, $dbh, $rowid) = @_;
	my $sql = "SELECT * FROM forum_category WHERE rowid=$rowid";
	print STDERR "Forum::DBGetCategory sql = $sql\n" if($debug);
	my $result = SelectSingle($dbh, $sql);
	print STDERR "Forum::DBGetCategory result = ". Dumper($result) . "\n" if($debug);

	return $result;
}
# ============================================================================
sub DBGetCategories
{
	my ($self, $dbh) = @_;
	my $sql = "SELECT * FROM forum_category WHERE forum_id = " . $self->{forum_rowid} . " ORDER by created";
	print STDERR "Forum::DBGetCategories sql = $sql\n" if($debug);
	my $result = SelectMultiple($dbh, $sql);
	print STDERR "Forum::DBGetCategories result = ". Dumper($result) . "\n" if($debug);

	return $result;
}
# ============================================================================
sub DBGetThematics
{
	my ($self, $dbh, $category_id) = @_;
	#
	# Get thematic, numbers of subjects and messages
	#
	my $sql = "select forum_thematic.*, thm_count_subjects(forum_thematic.rowid) as subject_count,"
					. " thm_count_messages(forum_thematic.rowid) as message_count,"
					. " thm_lastpost(forum_thematic.rowid) as message_id"
				. " FROM forum_thematic"
				. " WHERE category_id = $category_id"
				. " ORDER BY forum_thematic.created";
	print STDERR "Forum::DBGetThematics sql = $sql\n" if($debug);
	my $result = SelectMultiple($dbh, $sql);
	print STDERR "Forum::DBGetThematics result = ". Dumper($result) . "\n" if($debug);
	if (defined ($result)) {
		foreach my $entry (@$result) {
			if (exists($entry->{message_id}) && defined($entry->{message_id})) {
				$sql = "SELECT forum_message.author_id, forum_message.modified, m_user_base.firstname, m_user_base.lastname"
						. " FROM  forum_message, m_user_base"
						. " WHERE forum_message.author_id = m_user_base.rowid"
							. " AND forum_message.rowid = " . $entry->{message_id};

				my $res = SelectSingle($dbh, $sql);
				$entry->{firstname} = $res->{firstname};
				$entry->{lastname} = $res->{lastname};
				$entry->{author_id} = $res->{author_id};
				$entry->{lastpost} = $res->{modified};
			}
		}
	}

	return $result;
}
# ============================================================================
sub DBGetThematic
{
	my ($self, $dbh, $rowid) = @_;
	#my $sql = "SELECT * FROM forum_thematic WHERE rowid = $rowid";
	my $sql = "select forum_thematic.*, thm_count_subjects(forum_thematic.rowid) as subject_count,"
					. " thm_count_messages(forum_thematic.rowid) as message_count,"
					. " thm_lastpost(forum_thematic.rowid) as message_id,"
					. " forum_category.label as category, forum_category.forum_id as forum_id"
				. " FROM forum_thematic, forum_category"
				. " WHERE forum_thematic.rowid = $rowid AND forum_thematic.category_id=forum_category.rowid";
	print STDERR "Forum::DBGetThematic sql = $sql\n" if($debug);
	my $result = SelectSingle($dbh, $sql);
	print STDERR "Forum::DBGetThematic result = ". Dumper($result) . "\n" if($debug);
	if (exists($result->{message_id}) && defined($result->{message_id})) {
		$sql = "SELECT forum_message.author_id, forum_message.modified, m_user_base.firstname, m_user_base.lastname"
				. " FROM  forum_message, m_user_base"
				. " WHERE forum_message.author_id = m_user_base.rowid"
					. " AND forum_message.rowid = " . $result->{message_id};

		my $res = SelectSingle($dbh, $sql);
		$result->{firstname} = $res->{firstname};
		$result->{lastname} = $res->{lastname};
		$result->{author_id} = $res->{author_id};
		$result->{lastpost} = $res->{modified};
	}

	return $result;
}
# ============================================================================
sub DBGetCategoriesAndThematics
{
	my ($self, $dbh) = @_;
	my $sql = "SELECT category_id, category_label, thematic_id, thematic_label, thematic_desc"
					. ",count(subject_id), count(message_id), max(message_date)"
					. " FROM forum_catandthem group by category_id, category_label, thematic_id, thematic_label, thematic_desc"
					. " WHERE forum_id = " . $self->{forum_rowid};
	print STDERR "Forum::DBGetCategories sql = $sql\n" if($debug);
	my $result = SelectMultiple($dbh, $sql);
	print STDERR "Forum::DBGetCategories result = ". Dumper($result) . "\n" if($debug);

	return $result;
}
# ============================================================================
sub DBGetSubjectEnv
{
	my ($self, $dbh, $rowid) = @_;
	my $sql = "SELECT forum_category.label as category_label, forum_category.forum_id as forum_id, forum_thematic.category_id as category_id,"
					. " forum_thematic.rowid as thematic_id, forum_thematic.label as thematic_label,"
					. " forum_subject.subject as subject"
					. " FROM forum_category, forum_thematic, forum_subject"
					. " WHERE forum_category.rowid = forum_thematic.category_id"
						. " AND forum_thematic.rowid = forum_subject.thematic_id"
						. " AND forum_subject.rowid = $rowid";
	print STDERR "Forum::DBGetSubjectEnv sql = $sql\n" if($debug);
	my $result = SelectSingle($dbh, $sql);
	print STDERR "Forum::DBGetSubjectEnv result = ". Dumper($result) . "\n" if($debug);

	return $result;
}
# ============================================================================
sub DBGetSubject
{
	my ($self, $dbh, $rowid) = @_;
	#my $sql = "SELECT * FROM forum_subject WHERE rowid = $rowid";
	my $sql = "SELECT forum_subject.*, subject_count_messages(forum_subject.rowid) as message_count,"
					." subject_lastpost(forum_subject.rowid) as lastpost_id,"
					. " forum_message.author_id, forum_message.modified as lastpost,"
					. " m_user_base.firstname, m_user_base.lastname,"
					. " forum_user2subject.last_visit"
					. " FROM forum_subject LEFT JOIN forum_user2subject ON forum_subject.rowid = forum_user2subject.subject_id, forum_message, m_user_base"
 				. " WHERE forum_subject.rowid=$rowid AND forum_message.rowid = subject_lastpost(forum_subject.rowid)"
				. " AND m_user_base.rowid = forum_message.author_id";
	print STDERR "Forum::DBGetSubject sql = $sql\n" if($debug);
	my $result = SelectSingle($dbh, $sql);
	print STDERR "Forum::DBGetSubject result = ". Dumper($result) . "\n" if($debug);

	return $result;
}
# ============================================================================
sub DBGetSubjectDatas
{
	my ($self, $dbh, $rowid) = @_;
	my $sql = "SELECT forum_subject.rowid as subject_id, forum_subject.open as open, forum_subject.post_it as post_it,"
				. " forum_subject.subject as subject, forum_subject.thematic_id as thematic_id,"
				. " forum_category.rowid as category_id, forum_category.forum_id as forum_id"
				. " FROM forum_subject, forum_thematic, forum_category"
				. " WHERE forum_subject.rowid = $rowid AND forum_thematic.rowid = forum_subject.thematic_id"
					. " AND forum_thematic.category_id = forum_category.rowid";
	print STDERR "Forum::DBGetSubjectDatas sql = $sql\n" if($debug);
	my $result = SelectSingle($dbh, $sql);
	print STDERR "Forum::DBGetSubjectDatas result = ". Dumper($result) . "\n" if($debug);

	return $result;
}
# ============================================================================
sub DBGetSubjectCount
{
	my ($self, $dbh, $thematic_id) = @_;
	my $sql = "SELECT count(forum_subject.rowid) as count FROM forum_subject WHERE thematic_id = $thematic_id";
	my $result = SelectSingle($dbh, $sql);
	print STDERR "Forum::DBGetSubject result = ". Dumper($result) . "\n" if($debug);

	return $result->{count};
}
# ============================================================================
sub DBGetSubjectList
{
	my ($self, $dbh, $thematic_id, $user_id, $page_num, $subjects_per_page) = @_;
	my $offset = ($page_num - 1) *  $subjects_per_page;
	my $sql = "SELECT forum_subject.*, subject_count_messages(forum_subject.rowid) as message_count,"
					." subject_lastpost(forum_subject.rowid) as message_id"
					. " FROM forum_subject "
 				. " WHERE thematic_id = $thematic_id"
				. " ORDER BY forum_subject.post_it DESC, forum_subject.modified DESC"
				. " LIMIT $subjects_per_page OFFSET $offset";


	print STDERR "Forum::DBGetSubjectList sql = $sql\n" if($debug);
	my $result = SelectMultiple($dbh, $sql);
	print STDERR "Forum::DBGetSubjectList result = ". Dumper($result) . "\n" if($debug);
	if (defined ($result)) {
		foreach my $entry (@$result) {
			if (exists($entry->{message_id}) && defined($entry->{message_id})) {
				$sql = "SELECT forum_message.author_id, forum_message.modified, m_user_base.firstname, m_user_base.lastname,"
						. " (SELECT forum_user2subject.last_visit FROM forum_user2subject where forum_user2subject.subject_id=" . $entry->{rowid}
												. " AND forum_user2subject.user_id=$user_id) as last_visit" 
						. " FROM  forum_message, m_user_base"
						. " WHERE forum_message.author_id = m_user_base.rowid"
							. " AND forum_message.rowid = " . $entry->{message_id};

				print STDERR "Forum::DBGetSubjectList sql = $sql\n" if($debug);
				my $res = SelectSingle($dbh, $sql);
				$entry->{firstname} = $res->{firstname};
				$entry->{lastname} = $res->{lastname};
				$entry->{author_id} = $res->{author_id};
				$entry->{lastpost} = $res->{modified};
				$entry->{last_visit} = $res->{last_visit};
			}
		}
	}

	return $result;
}
# ============================================================================
sub DBGetSubjectsWithNewMessages
{
	my ($self, $dbh, $thematic_id, $user_id) = @_;
	my $sql = "SELECT rowid, last_visit FROM (SELECT rowid,modified,(SELECT last_visit FROM forum_user2subject WHERE subject_id = temp.rowid and user_id=$user_id) as last_visit FROM (SELECT forum_subject.rowid as rowid, max(forum_message.modified) as modified FROM forum_subject,forum_message WHERE thematic_id = $thematic_id and forum_message.subject_id=forum_subject.rowid group by forum_subject.rowid) as temp) as dummy where dummy.last_visit is null or dummy.last_visit < dummy.modified";
	print STDERR "Forum::DBGetSubjectsWithNewMessages sql = $sql\n" if($debug);
	my $result = SelectMultiple($dbh, $sql);
	print STDERR "Forum::DBGetSubjectsWithNewMessages result = ". Dumper($result) . "\n" if($debug);

	return $result;
}
# ============================================================================
sub DBGetMessageCount
{
	my ($self, $dbh, $rowid, $page_num, $messages_per_page) = @_;
	my $offset = ($page_num - 1) *  $messages_per_page;
	my $sql = "SELECT count(forum_message.rowid) as count FROM forum_message WHERE subject_id = $rowid";
	my $result = SelectSingle($dbh, $sql);
	print STDERR "Forum::DBGetMessageList result = ". Dumper($result) . "\n" if($debug);

	return $result->{count};
}
# ============================================================================
sub DBGetMessageList
{
	my ($self, $dbh, $rowid, $page_num, $messages_per_page) = @_;
	my $offset = ($page_num - 1) *  $messages_per_page;
	my $sql = "SELECT forum_message.*, m_user_base.firstname, m_user_base.lastname,"
					. " m_user_base_mod.firstname as modifier_firstname,  m_user_base_mod.lastname as modifier_lastname"
					. " FROM forum_message, m_user_base, m_user_base as m_user_base_mod"
					. " WHERE subject_id = $rowid AND m_user_base.rowid=forum_message.author_id"
					. " AND  m_user_base_mod.rowid=forum_message.modifier_id"
					. " ORDER BY created LIMIT $messages_per_page OFFSET $offset";
	my $result = SelectMultiple($dbh, $sql);
	print STDERR "Forum::DBGetMessageList result = ". Dumper($result) . "\n" if($debug);

	return $result;
}
# ============================================================================
sub DBGetLastMessagesList
{
	my ($self, $dbh, $subject_id, $base_id, $nb_messages) = @_;

	my $sql = "SELECT forum_message.*, m_user_base.firstname, m_user_base.lastname FROM forum_message, m_user_base"
					. " WHERE subject_id = $subject_id AND forum_message.author_id = m_user_base.rowid"
					. " ORDER BY created DESC LIMIT $nb_messages";
	print STDERR "Forum::DBGetLastMessagesList sql = $sql\n" if($debug);
	my $result = SelectMultiple($dbh, $sql);
	print STDERR "Forum::DBGetLastMessagesList result = ". Dumper($result) . "\n" if($debug);

	return $result;
}
# ============================================================================
sub DBGetNumberOfMessagesInSubject {
  my ($self, $dbh, $subject_id) = @_;
  
  my $result = SelectSingle($dbh, "SELECT count(rowid) FROM forum_message WHERE subject_id = $subject_id");
  print STDERR "Forum::DBGetNumberOfMessagesInSubject result = ". Dumper($result) . "\n" if($debug);

  return $result->{count};
}
# ============================================================================
sub DBGetNewMessagesList
{
	my ($self, $dbh, $subject_id, $user_id, $last_visit) = @_;

	my $sql;

	if (defined($last_visit)) {
		$sql = "SELECT forum_message.*, m_user_base.firstname, m_user_base.lastname,"
					. " m_user_base_mod.firstname as modifier_firstname,  m_user_base_mod.lastname as modifier_lastname"
					. " FROM forum_message, m_user_base, m_user_base as m_user_base_mod"
					. " WHERE subject_id = $subject_id AND m_user_base.rowid=forum_message.author_id"
					. " AND  m_user_base_mod.rowid=forum_message.modifier_id"
					. " AND  forum_message.modified > (SELECT last_visit FROM forum_user2subject WHERE subject_id = $subject_id AND user_id=$user_id)"
					. " ORDER BY created";
	}
	else {
		$sql = "SELECT forum_message.*, m_user_base.firstname, m_user_base.lastname,"
					. " m_user_base_mod.firstname as modifier_firstname,  m_user_base_mod.lastname as modifier_lastname"
					. " FROM forum_message, m_user_base, m_user_base as m_user_base_mod"
					. " WHERE subject_id = $subject_id AND m_user_base.rowid=forum_message.author_id"
					. " AND  m_user_base_mod.rowid=forum_message.modifier_id"
					. " ORDER BY created";
	}
	print STDERR "Forum::DBGetNewMessagesList sql = $sql\n" if($debug);
	my $result = SelectMultiple($dbh, $sql);
	print STDERR "Forum::DBGetNewMessagesList result = ". Dumper($result) . "\n" if($debug);

	return $result;
}
# ============================================================================
sub DBGetMessage
{
	my ($self, $dbh, $message_id) = @_;

	my $sql = "SELECT forum_message.*, m_user_base.firstname, m_user_base.lastname,"
					. " m_user_base_mod.firstname as modifier_firstname,  m_user_base_mod.lastname as modifier_lastname,"
					. " forum_thematic.rowid as thematic_id, forum_thematic.category_id as category_id,"
					. " forum_category.forum_id as forum_id"
					. " FROM forum_message, m_user_base, m_user_base as m_user_base_mod, forum_subject, forum_thematic, forum_category"
					. " WHERE forum_message.rowid = $message_id AND forum_message.subject_id = forum_subject.rowid"
									. " AND m_user_base.rowid = forum_message.author_id"
									. " AND m_user_base_mod.rowid = forum_message.modifier_id"
									. " AND forum_thematic.rowid = forum_subject.thematic_id"
									. " AND forum_category.rowid = forum_thematic.category_id";
	print STDERR "Forum::DBGetMessage sql = $sql\n" if($debug);
	my $result = SelectSingle($dbh, $sql);
	print STDERR "Forum::DBGetMessage result = ". Dumper($result) . "\n" if($debug);

	return $result;
}
# ============================================================================
sub DBUpdateCategory
{
	my ($self, $dbh, $rowid, $label) = @_;
	my $sql = "UPDATE forum_category  set modified=now(), label='" . st_FormatPostgreSQLString($label) . "'"
					. " WHERE rowid=$rowid";
	ExecSQL($dbh, $sql);
}
# ============================================================================
sub DBUpdateThematic
{
	my ($self, $dbh, $rowid, $values) = @_;
	my $sql = "UPDATE forum_thematic  set modified=now()";
	foreach my $key (keys(%$values)) {
		$sql .= ", $key='" . st_FormatPostgreSQLString($values->{$key}) . "'"
	}
	$sql .=	" WHERE rowid=$rowid";
	print STDERR "Forum::DBUpdateThematic sql = $sql\n" if($debug);
	ExecSQL($dbh, $sql);
}
# ============================================================================
sub DBUpdateSubject
{
	my ($self, $dbh, $subject_id, $values) = @_;
	print STDERR "Forum::DBUpdateSubject\n" if($debug);
	$values->{modified} = 'now()';

	my $sql = BuildUpdateRequest($values,
								 table  => 'forum_subject',
								 other  => ['modified'],
								 string  => ['subject'],
								 bool  => ['post_it', 'open' ]
								 );
	$sql .= " WHERE rowid=$subject_id";
	print STDERR "Forum::DBUpdateSubject sql = $sql\n" if($debug);
	ExecSQL($dbh, $sql);
}
# ============================================================================
sub DBUpdateMessage
{
	my ($self, $dbh, $message_id, $values) = @_;
	$values->{modified} = 'now()';

	my $sql = BuildUpdateRequest($values,
								 table  => 'forum_message',
								 other  => ['modified', 'modifier_id' ],
								 string  => ['message']
								 );
	$sql .= " WHERE rowid=$message_id";
	print STDERR "Forum::DBUpdateMessage sql = $sql\n" if($debug);
	ExecSQL($dbh, $sql);
}
# ============================================================================
sub DBSetUserLastVisit
{
	my ($self, $dbh, $user_id, $subject_id) = @_;
	my $sql = "SELECT last_visit FROM forum_user2subject WHERE user_id=$user_id AND subject_id=$subject_id";
	my $result = SelectSingle($dbh, $sql);
	if (defined($result)) {
		$sql = "UPDATE forum_user2subject  set last_visit=now(), send_mail='f' WHERE user_id=$user_id AND subject_id=$subject_id";
		print STDERR "Forum::DBSetUserLastVisit update\n" if($debug);
	}
	else {
		print STDERR "Forum::DBSetUserLastVisit insert\n" if($debug);
		$sql = "INSERT INTO forum_user2subject (last_visit, user_id, subject_id) VALUES(now(), $user_id, $subject_id)";
	}
	ExecSQL($dbh, $sql);
}
# ============================================================================
sub DBSetSubjectSendMail
{
  my ($self, $dbh, $subject_id) = @_;
  my $sql = "UPDATE forum_user2subject  set send_mail='t' WHERE subject_id=$subject_id";
  ExecSQL($dbh, $sql);
}
# ============================================================================
sub DBDeleteCategory
{
	my ($self, $dbh, $rowid) = @_;
	my $sql = "DELETE from forum_category WHERE rowid=$rowid";
	ExecSQL($dbh, $sql);
}
# ============================================================================
sub DBDeleteThematic
{
	my ($self, $dbh, $rowid) = @_;
	my $sql = "DELETE from forum_thematic WHERE rowid=$rowid";
	ExecSQL($dbh, $sql);
}
# ============================================================================
sub DBDeleteSubjectAndMessages
{
	my ($self, $dbh, $subject_id) = @_;

	BeginTransaction($dbh);
	try {
		my $sql = "DELETE from forum_message WHERE subject_id=$subject_id";
		ExecSQL($dbh, $sql);

		$sql = "DELETE from forum_user2subject WHERE subject_id=$subject_id";
		ExecSQL($dbh, $sql);

		$sql = "DELETE from forum_subject WHERE rowid=$subject_id";
		ExecSQL($dbh, $sql);
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};

	EndTransaction($dbh);
}
# ============================================================================
sub DBDeleteMessage
{
	my ($self, $dbh, $message_id) = @_;
	my $sql = "DELETE from forum_message WHERE rowid=$message_id";
	ExecSQL($dbh, $sql);
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


