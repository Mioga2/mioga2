# ============================================================================
# Mioga2 Project (C) 2003-2007 The MiogaII Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
# Description:

=head1 NAME

Mioga2::Contact - The Mioga2 address book application

=head1 DESCRIPTION

=cut

package Mioga2::Contact;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'contact';

use Mioga2::Application;
use Mioga2::Content::XSLT;
use Mioga2::Content::FILE;
use Mioga2::Exception::Simple;
use Data::Dumper;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Error qw(:try);
use POSIX qw(tmpnam);
use String::Checker;
use Mioga2::LargeList::Contact;
use File::MimeInfo::Magic;

require Mioga2::Contact::Database;

# ============================================================================
# Global vars
# ============================================================================

		
my $debug = 0;

my %errors = ('no_error' => 0,
              'write_access_error' => 100,
              'vCard_parse_error' => 200,
              'invalid_email_work' => 300,
              'invalid_email_home' => 301,
              'invalid_tel_work' => 302,
              'invalid_tel_home' => 303,
              'invalid_tel_fax' => 304,
              'invalid_tel_mobile' => 305,
              'invalid_photo' => 306,
              'name_required' => 307,
              'invalid_rowid' => 308,
              'invalid_filename' => 309,
              'invalid_params' => 310,
              );


# ============================================================================
# Private Methods
# ============================================================================
# ============================================================================
# Public Methods
# ============================================================================

# ============================================================================
# GetAppDesc
#
# GetAppDesc is used to get application description in order to initialize
# database. This method is mandatory for each mioga application
#
# ============================================================================
sub GetAppDesc
{
	my $self = shift;
    
    my %AppDesc = ( ident   => 'Contact',
	    	name	=> __('Address book'),
                package => 'Mioga2::Contact',
                description => __('The Mioga II address book application'),
				type    => 'normal',
                all_groups => 1,
                all_users  => 1,
                can_be_public => 0,
				is_user             => 1,
				is_group            => 1,
				is_resource         => 0,

				'functions' 	=> {    'Read' 	=> __('Contacts consultation functions'),
										'Write' => __('Contacts edition functions')
								   },
				'func_methods' 	=> {
					'Read' 	=> [ 'DisplayMain',
								 'DisplayContact', 'GetPhoto',
								 'ExportVcard', 'BrowseContact',
								 'SearchEngine', 'SearchContact'],
					'Write'	=> [ 'AddContact', 'EditContact', 
								 'SubmitContact', 'RemoveContact', 
								 'ImportVcard', 
								 ]
								 },

				sxml_methods => {},

				xml_methods  => {Read  => ['XMLGetContactsModifiedSince', 'XMLGetContactsRowids'],
							     Write => ['XMLDeleteContacts', 'XMLImportContacts'] },

				non_sensitive_methods => [
					'DisplayMain',
					'DisplayContact',
					'GetPhoto',
					'ExportVcard',
					'BrowseContact',
					'SearchEngine',
					'SearchContact',
					'AddContact',
					'EditContact',
					'SubmitContact',
					'RemoveContact',
					'ImportVcard',
				]

                );
    
	return \%AppDesc;
}



# ============================================================================
# GetXMLFunction
#
#    Return XML representing user permission for each function
#
# ============================================================================
sub GetXMLFunction 
{
    my ($self, $context) = @_;
    my $xml = "<Function>";

	my $session = $context->GetSession();
	my $config = $context->GetConfig();

    foreach my $app ('DisplayMain', 'DisplayContact', 'ExportVcard', 
                     'SearchForm', 'GetPhoto', 'AddContact', 'EditContact', 
                     'RemoveContact', 'ImportVcard', 'SubmitContact' ) {

		my $ctcgroup;
		try {
			$ctcgroup = new Mioga2::Old::Group($context->GetConfig(), rowid => $session->{Contact}->{cur_group});
		} 
		otherwise {
			$ctcgroup = new Mioga2::Old::User($context->GetConfig(), rowid => $session->{Contact}->{cur_group});
		};
		my $result = $self->CheckUserAccessOnMethod($context, $context->GetUser(), $ctcgroup, $app);

        
        if ($result) {
            $xml .= qq|<$app authorised="1"/>|;
        }
        else {
            $xml .= qq|<$app authorised="0"/>|;
        }
    }
    
    $xml .= "</Function>";
    
    return $xml;
}



# ============================================================================
# GetXMLGroupWrite
#
#    Return XML representing current user's groups having write permission
#
# ==========================================================================='
sub GetXMLGroupWrite
{
    my ($self, $context) = @_;

	my $config = $context->GetConfig();
	my $session = $context->GetSession();

    my $xml = "";
    
    my $dbh = $config->GetDBH();

    ## Retrieve user group list
    my $group_list = $context->GetUser()->GetExpandedGroups();
	my $group_rowids = $group_list->GetRowids();

	my $cur_group;

    if(exists $session->{Contact}->{cur_group}) {
	    $cur_group = $session->{Contact}->{cur_group};
    }
    else {
	    $cur_group = $group_list->[0];
    }
    
	unshift @$group_rowids, $context->GetUser()->GetRowid();

	foreach my $group_id (@$group_rowids) {
		my $group;
		try {
			$group = new Mioga2::Old::Group($context->GetConfig(), rowid => $group_id);
		} 
		otherwise {
			$group = new Mioga2::Old::User($context->GetConfig(), rowid => $group_id);
		};


        my $result = $self->CheckUserAccessOnFunction($context, $context->GetUser(),
													  $group, "Write");


	    next if($result != AUTHZ_OK);
	    
	    if($group_id == $cur_group) {
		    $xml .= qq|<group id="$group_id" selected="1">|.$group->GetIdent().qq|</group>|;
	    }
	    else {
		    $xml .= qq|<group id="$group_id">|.$group->GetIdent().qq|</group>|;
	    }
    }

    return $xml;
}

# ============================================================================

=head2 DisplayMain

    Generates the contact browser frame for user depending on user groups.

	See BrowseContact

=cut

# ============================================================================

sub DisplayMain
{    
    my ($self, $context) = @_;

 	my $session = $context->GetSession();

	# delete referers
    delete $session->{Contact}->{search_referer};
    delete $session->{Contact}->{referer};
    delete $session->{Contact}->{search_engine};
    
    return $self->BrowseContact($context);
}


# ============================================================================

=head2 SearchEngine
    
    Search engine for other apps

	See BrowseContact

=cut

# ============================================================================

sub SearchEngine
{    
    my ($self, $context) = @_;
    
    my $referer;
    my $action;
 	my $session = $context->GetSession();

    if(exists $context->{args}->{action}) {
	    $action  = $context->{args}->{action};
    }
    else {
	    $action = $session->{Contact}->{search_engine}->{action};
    }

    if(exists $context->{args}->{referer}) {
    	    $referer = $context->{args}->{referer};
    }
    else {
	    $referer = $session->{Contact}->{search_engine}->{referer};
    }

    $session->{Contact}->{search_engine}->{referer} = $referer;
    $session->{Contact}->{search_engine}->{action}  = $action;

    # remove options in referer url
    $session->{Contact}->{search_engine}->{referer} =~ s/\?.*$//;

    return $self->BrowseContact($context);
}



# ============================================================================

=head2 BrowseContact

    Generates the contact browser frame for user depending on user groups.

=head3 Generated XML

    <BrowseContact>

        <miogacontext>See Mioga2::RequestContext</miogacontext>

        <method>Currrent method ident</method>

        <referer>URL of calling application</referer>

        <action>Action URI</action>
        <action>Action URI</action>
        ...

        <!-- LargeList XML. See Mioga2::LargeList -->


        <!-- Current application functions description -->
	    <Function>
           <$func authorised="right of current user (0|1)"/>
           ...
        </Function>

     </BrowseContact>

=head3 List fields :

    mioga_users_sll : List All users of current Mioga2 instance
    
    Available fields :

        name            type                 description
    ----------------------------------------------------------------
       rowid        : rowid             : the contact rowid 
       name         : not empty string  : the contact name
       surname      : string            : the contact surname
       title        : string            : the contact title
       orgname      : string            : the contact organization name
       photo_data   : string            : the photo rowid
       photo_url    : string            : the photo url
       email_work   : string            : the contact work email
       email_home   : string            : the contact home email
       tel_work     : string            : the contact work telephone number
       tel_home     : string            : the contact home telephone number
       tel_mobile   : string            : the contact mobile telephone number   
       tel_fax      : string            : the contact fax number
       addr_home    : string            : the contact home address
       city_home    : string            : the contact home city
       postal_code_home : string            : the contact home postal code
       state_home   : string            : the contact home state
       country_home : string            : the contact home country
       addr_work    : string            : the contact work address
       city_work    : string            : the contact work city	  
       postal_code_work : string            : the contact work postal code
       state_work   : string            : the contact work state  
       country_work : string            : the contact work country
       url          : string            : the contact web site url
       comment      : string            : the contact comments

=head3 Form actions and fields

    LargeList actions. See Mioga2::LargeList.

=cut

# ============================================================================

sub BrowseContact {
    my ($self, $context) = @_;

	if(st_ArgExists($context, 'ActionDelete')) {
		return $self->RemoveContact($context);
	}

 	my $session = $context->GetSession();
 	my $config = $context->GetConfig();

    my $xml = '<?xml version="1.0"?>';
 
    ##
    ## Check args
    ##
    my $error = 0;
    if(exists $context->{args}->{group} ) {
	    my $group = $context->{args}->{group};
	    my $res = String::Checker::checkstring($group, ['disallow_empty', 'want_int']);
	    if(defined $res and @$res ) {
		    $error = $errors{'invalid_rowid'};
	    }
    }

    if(exists $context->{args}->{alpha} ) {
	    my $alpha = $context->{args}->{alpha};
	    my $res = String::Checker::checkstring($alpha, ['disallow_empty', 'want_int']);
	    if(defined $res and @$res) {
		    $error = $errors{'invalid_rowid'};
	    }
    }

    if(exists $context->{args}->{page} ) {
	    my $page = $context->{args}->{page};
	    my $res = String::Checker::checkstring($page, ['disallow_empty', 'want_int']);
	    if(defined $res and @$res) {
		    $error = $errors{'invalid_rowid'};
	    }
    }


    if(exists $context->{args}->{sort_by} ) {
	    my $sort_by = $context->{args}->{sort_by};
	    my $res = String::Checker::checkstring($sort_by, ['stripxws', 'disallow_empty', ['disallow_chars' => '\'"[]\\']]);
	    if(defined $res and @$res) {
		    $error = $errors{'invalid_params'};
	    }
    }

    if($error) {
	    return $self->_ErrorMessage($context, $error); 
    }

	if(! exists $session->{Contact}->{prevgroup} or
	   $session->{Contact}->{prevgroup} != $context->GetGroup()->GetRowid()) {
		$session->{Contact}->{cur_group} = $context->GetGroup()->GetRowid();
		$session->{Contact}->{prevgroup} = $context->GetGroup()->GetRowid();
 	}
	    
    # ---------------------------------------
    # Generate XML
    # ---------------------------------------
    $xml .= "<BrowseContact";
	if (defined ($session->{__internal}->{message})) {
		$xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
		delete $session->{__internal}->{message};
	}
	$xml .= ">";
    $xml .= $context->GetXML();
    $xml .= $self->SearchForm($context);

    $xml .= "<method>".st_FormatXMLString($context->GetMethod())."</method>";

    if(exists $session->{Contact}->{search_engine}->{action}) {
	    $xml .= "<referer>";
	    $xml .= st_FormatXMLString($session->{Contact}->{search_engine}->{referer});
	    $xml .= "</referer>";
	    $xml .= "<action>";
	    $xml .= st_FormatXMLString($session->{Contact}->{search_engine}->{action});
	    $xml .= "</action>"
    }

    
    my $largelist = new Mioga2::LargeList::Contact($context, "ContactDisplayMain");
    $xml .= $largelist->DisplayList($context);

    $xml .= $self->GetXMLFunction($context);
    $xml .= "</BrowseContact>";

 	my $content = new Mioga2::Content::XSLT($context, stylesheet => "contact.xsl", locale_domain => 'contact_xsl');
	$content->SetContent($xml);
    
    return $content;
}

# ============================================================================

=head2 RemoveContact

    Remove a contact

	See Mioga2::LargeList->RemoveContact

=cut

# ============================================================================

sub RemoveContact
{    
    my ($self, $context) = @_;

	my $session = $context->GetSession ();

    my $largelist = new Mioga2::LargeList::Contact($context, "ContactRemove");

    my $xml = '<?xml version="1.0"?>';
    my $tmpxml = $largelist->RemoveElem($context);

	if (defined ($tmpxml) && $tmpxml ne '') {
		$xml .= $tmpxml;
	}
	else {
		$session->{__internal}->{message}{text} = __('Contacts successfully deleted.');
		$session->{__internal}->{message}{type} = 'info';
		my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
		$content->SetContent($context->GetReferer());
		return $content;
	}


  	my $content = new Mioga2::Content::XSLT($context, stylesheet => "contact.xsl", locale_domain => 'contact_xsl');
	$content->SetContent($xml);

    return $content;
}

# ============================================================================

=head2 ExportVcard

    Export a contact in a vCard

	Return a VCARD file

=head3 Form actions and fields :
    
	There is only two required field :
    rowid : the contact rowid
    name  : the contact name

=cut

# ============================================================================

sub ExportVcard
{    
    my ($self, $context) = @_;
          
	my $config = $context->GetConfig();
    my $dbh = $config->{dbh};

    my $contact_id = $context->{args}->{rowid};    
    my $contact_name = $context->{args}->{name}; 

    my $error = 0;

    ##
    ## Check args
    ##
    my $res = String::Checker::checkstring($contact_id, ['disallow_empty', 'want_int']);
    if(defined $res and @$res) {
	    $error = $errors{'invalid_rowid'};
    }
  
    $res = String::Checker::checkstring($contact_name, ['stripxws', 'disallow_empty']);
    if(defined $res and @$res) {
	    $error = $errors{'invalid_filename'};
    }
    
    if($error) {
	    return $self->_ErrorMessage($context, $error);
    }
   
    
    ##
    ## Export the contact
    ##
    my $vcard_data = $self->ContactExportVcard($context, $contact_id);
    my $tmpfile = $config->GetTmpDir()."/".$contact_name.".vcf";

    open(F_VCARD, ">$tmpfile")
    or throw Mioga2::Exception::Simple(__x("Can't open file {file}", file => $tmpfile), $!);
    print F_VCARD $vcard_data;
    close F_VCARD;
    
	my $content = new Mioga2::Content::FILE($context, MIME => "text/x-vcard",
											          filename => $contact_name.".vcf");
    $content->SetContent($tmpfile);

    return $content;
}

# ============================================================================

=head2 ImportVcard

    Import a vCard file into the contact list

=head3 Generated XML

    <ImportVcard>

        <miogacontext>See Mioga2::RequestContext</miogacontext>

        <error>error id</err> 
        <referer>URL of calling application</referer>

        <!-- List of group where current user can access to Contact.
        <group id="group id" selected="1">
        <group id="group id" selected="0">
         ...

     </ImportVcard>

=head3 Form actions and fields :
    
    - ActionUploadOk : Import a contact.
      The required content is uploaded data of a VCard file.

=cut

# ============================================================================

sub ImportVcard
{    
    my ($self, $context) = @_;
          
    if(st_ArgExists($context, 'ActionUploadOk')) {
        return $self->_ImportActionOK($context);
    }    

    return $self->_ImportScreen($context);
}


### Import private methods
sub _ImportActionOK {
    my ($self, $context) = @_;

	my $session = $context->GetSession();
	my $config = $context->GetConfig();
    my $dbh = $config->{dbh};

    my $vcard_data = $context->GetUploadData();

    my $content;
    my $id_group = $context->{args}->{'group'};
    my $error = 0;


    ##
    ## Check args
    ##
    my $res = String::Checker::checkstring($id_group, ['disallow_empty', 'want_int']);
    if(defined $res and @$res) {
	    $error = $errors{'invalid_rowid'};
    }
    else {
	    try {
		    $self->ContactImportVcard($context, $id_group, $vcard_data);
	    }
	    otherwise {
		    my $except = shift;
		    
		    warn Dumper($except);
		    
		    $error = $errors{'vCard_parse_error'};
	    };
	    
	    if($error) {
		    $content = $self->_ImportScreen( $context, $error);
	    }
	    else {
			$session->{__internal}->{message}{text} = __('Contact successfully imported.');
			$session->{__internal}->{message}{type} = 'info';
			$content = new Mioga2::Content::REDIRECT($context, mode => 'external');
			$content->SetContent($session->{Contact}->{referer});
	    }
    }

    return $content;
}


sub _ImportScreen {
    my ($self, $context, $error) = @_;
    
	my $config = $context->GetConfig();
    my $dbh = $config->{dbh};
	my $session = $context->GetSession();

    $error = $errors{'no_error'} unless defined $error;

    my $referer = $context->GetReferer();
    if(exists $session->{Contact}->{referer}) {
	    $referer = $session->{Contact}->{referer};
    }
    else {
	    $session->{Contact}->{referer} = $referer;
    }
        
    my $xml = '<?xml version="1.0"?>';

    # ---------------------------------------
    # Generate XML
    # ---------------------------------------
    $xml .= "<ImportVcard>";
    $xml .= $context->GetXML();
    $xml .= "<error>$error</error>";
    $xml .= "<referer>".st_FormatXMLString($referer)."</referer>";

    $xml .= $self->GetXMLGroupWrite($context);

    $xml .= "</ImportVcard>";

  	my $content = new Mioga2::Content::XSLT($context, stylesheet => "contact.xsl", locale_domain => 'contact_xsl');
    $content->SetContent($xml);
    
    return $content;
}


# ============================================================================

=head2 AddContact

    Used to display the contact creation frame

=head3 Generated XML

    <EditContact group="new">

        <miogacontext>See Mioga2::RequestContext</miogacontext>

        <error>error id</err> 
        <referer>URL of calling application</referer>

        <!-- List of group where current user can access to Contact.
        <AvailaleGroups>
            <group id="group id" selected="1">
            <group id="group id" selected="0">
             ...	    
        </AvailaleGroups>

        <!-- list of fields values -->
        <$field_ident>field value</$field_ident> 
        ...

     </EditContact>

=head3 Form actions and fields

	See SubmitContact

=cut

# ============================================================================

sub AddContact
{    
    my ($self, $context, $error) = @_;
    $error = $errors{'no_error'} unless defined $error;

  
 	my $config = $context->GetConfig();
    my $dbh = $config->{dbh};
	my $session = $context->GetSession();

    my $referer = $context->GetReferer();
    if(exists $session->{Contact}->{referer}) {
	    $referer =  $session->{Contact}->{referer};
    }
    else {
	    $session->{Contact}->{referer} = $referer;		
    }

    my $xml = '<?xml version="1.0"?>';

    # ---------------------------------------
    # Generate XML    
    # ---------------------------------------
    $xml .= "<EditContact group=\"new\">";
    $xml .= $context->GetXML();
    $xml .= "<error>$error</error>";
    
	$xml .= "<AvailaleGroups>";
    $xml .= $self->GetXMLGroupWrite($context);
	$xml .= "</AvailaleGroups>";

    $xml .= "<referer>".st_FormatXMLString($referer)."</referer>";

    if($error) {
	    foreach my $key (keys %{$context->{args}}) {
		    $xml .= "<$key>".st_FormatXMLString($context->{args}->{$key})."</$key>";
	    }
    }

    $xml .= "</EditContact>";

  	my $content = new Mioga2::Content::XSLT($context, stylesheet => "contact.xsl", locale_domain => 'contact_xsl');
    $content->SetContent($xml);

    return $content;
}

# ============================================================================

=head2 EditContact

        Used to display the contact edition frame

=head3 Generated XML

    <EditContact group="group rowid">

        <miogacontext>See Mioga2::RequestContext</miogacontext>

        <error>error id</err> 
        <referer>URL of calling application</referer>

        <!-- list of fields values -->
        <$field_ident>field value</$field_ident> 
        ...

     </EditContact>

=head3 Form actions and fields

	See SubmitContact

=cut

# ============================================================================

sub EditContact
{    
	my ($self, $context, $error) = @_;
	
 	my $config = $context->GetConfig();
    my $dbh = $config->{dbh};
	my $session = $context->GetSession();

	my $referer = $context->GetReferer();
	if(exists $session->{Contact}->{referer}) {
		$referer =  $session->{Contact}->{referer};
	}
	else {
		$session->{Contact}->{referer} = $referer;
	}


	##
	## Check args
	##
	if(exists $context->{args}->{rowid}) {
		my $rowid = $context->{args}->{rowid};
		my $res = String::Checker::checkstring($rowid, ['disallow_empty', 'want_int']);
		if(defined $res and @$res) {
			$error = $errors{'invalid_rowid'};
			return $self->_ErrorMessage($context, $error);
		}
	}

	
	my $xml = '<?xml version="1.0"?>';
        
	# ---------------------------------------
	# Generate XML
	# ---------------------------------------

	$xml .= "<EditContact>";
	$xml .= $context->GetXML();
	$xml .= "<referer>".st_FormatXMLString($referer)."</referer>";
    $xml .= "<error>$error</error>";

	my $contact;
	if($error) {
		$contact = $context->{args};
	}
	else {
		$contact = $self->ContactGetContact($context, $context->{args}->{rowid});
	}

	foreach my $key (keys %$contact) {
		if(defined $contact->{$key}) {
			$xml .= "<$key>".st_FormatXMLString($contact->{$key})."</$key>";
		}
	}

	$xml .= "</EditContact>";
	    
  	my $content = new Mioga2::Content::XSLT($context, stylesheet => "contact.xsl", locale_domain => 'contact_xsl');
	$content->SetContent($xml);

	return $content;
}

# ============================================================================

=head2 SubmitContact

    Apply modification or create a new contact

	Redirect to referer with InlineMessage

=head3 Form actions and fields

    Available fields :

        name            type                 description
    ----------------------------------------------------------------
       rowid        : rowid             : the contact rowid 
       name         : not empty string  : the contact name
       surname      : string            : the contact surname
       title        : string            : the contact title
       orgname      : string            : the contact organization name
       photo_data   : string            : the uploaded photo data
       photo_url    : string            : the photo url
       email_work   : string            : the contact work email
       email_home   : string            : the contact home email
       tel_work     : string            : the contact work telephone number
       tel_home     : string            : the contact home telephone number
       tel_mobile   : string            : the contact mobile telephone number   
       tel_fax      : string            : the contact fax number
       addr_home    : string            : the contact home address
       city_home    : string            : the contact home city
       postal_code_home : string            : the contact home postal code
       state_home   : string            : the contact home state
       country_home : string            : the contact home country
       addr_work    : string            : the contact work address
       city_work    : string            : the contact work city	  
       postal_code_work : string            : the contact work postal code
       state_work   : string            : the contact work state  
       country_work : string            : the contact work country
       url          : string            : the contact web site url
       comment      : string            : the contact comments

=cut

# ============================================================================

sub SubmitContact
{    
    my ($self, $context) = @_;

 	my $config = $context->GetConfig();
    my $dbh = $config->GetDBH();
	my $session = $context->GetSession();

    my $args = $context->{args};

    my %contact;

    ##
    ## Check args
    ##
 
    my $error = $errors{'no_error'};
    
    if(exists $context->{args}->{rowid}) {
	    my $rowid = $context->{args}->{rowid};
	    my $res = String::Checker::checkstring($rowid, ['disallow_empty', 'want_int']);
	    if(defined $res and @$res) {
		    $error = $errors{'invalid_rowid'};
	    }
    }

    if(exists $context->{args}->{group}) {
	    my $group = $context->{args}->{group};
	    my $res = String::Checker::checkstring($group, ['disallow_empty', 'want_int']);
	    if(defined $res and @$res) {
		    $error = $errors{'invalid_rowid'};
	    }
    }

    if($error) {
	return $self->_ErrorMessage($context, $error);
    }

    ## Check if emails and tel number is valid
    my $string = $args->{name};
    my $res = String::Checker::checkstring($string, ['stripxws', 'disallow_empty']);
    if(defined $res and @$res) {
        $error = $errors{'name_required'};
    }

    $string = $args->{email_work};
    $res = String::Checker::checkstring($string, ['stripxws', 'allow_empty', 'want_email']);
    if(defined $res and @$res and $string ne "") {
        $error = $errors{'invalid_email_work'};
    }

    $string = $args->{email_home};
    $res = String::Checker::checkstring($string, ['stripxws', 'allow_empty', 'want_email']);
    if(defined $res and @$res and $string ne "") {
        $error = $errors{'invalid_email_home'};
    }

    # Convert photo to jpeg using 'convert'
    my $filename = tmpnam;
    if($args->{photo} ne "" and exists $context->{'upload'}) {
        my $photo_data = $context->GetUploadData();

		open(F_PHOTO, ">$filename")
			or throw Mioga2::Exception::Simple("Mioga2::Contact::SubmitContact", 
            __x("Error can not open {file}: {error}", file => $filename, error => $!));
	
		print F_PHOTO $photo_data;
		close F_PHOTO;
	
		my $mime = mimetype($filename);
		
		if($mime eq "image/jpeg" or
		   $mime eq "image/gif"  or
		   $mime eq "image/png") {
			
			$contact{photo_data} = $filename;
		}
		else {
			unlink($filename);
			
			$error = $errors{'invalid_photo'};
        }
		
    }
    elsif ($args->{delete_photo} eq '1')
    {
    	warn ("------------> delete photo") if ($debug > 0);
	$contact{photo_delete} = 'delete';
    }

    if($error != $errors{'no_error'}) {        
	    delete $args->{'ActionEditOK'};
	    delete $args->{'ActionEditOK.x'};
	    delete $args->{'ActionEditOK.y'};
	    
	    if(exists $args->{rowid} and $args->{rowid} ne "") {
		    return $self->EditContact($context, $error);
	    }
	    else {
		    return $self->AddContact($context, $error);
	    }
    }
    
    
    foreach my $var ("name", "surname", "title",
                     "orgname", "email_work", "email_home", "tel_work",    
                     "tel_home", "tel_mobile", "tel_fax", "addr_home", 
                     "city_home", "postal_code_home", "state_home",   
                     "country_home", "addr_work", "city_work", "postal_code_work",
                     "state_work", "country_work", "url", "comment") {
	    $contact{$var} = $args->{$var};
    }

    my $cur_action;
    if(exists $args->{rowid} and 
       $args->{rowid} ne "") {
	    $self->ContactModify($context, $args->{rowid}, \%contact);
	    $cur_action = "modify";
		$session->{__internal}->{message}{text} = __('Contact successfully modified.');
    }
    else {
	    $self->ContactAdd($context, $args->{group}, \%contact);
	    $cur_action = "create";
		$session->{__internal}->{message}{text} = __('Contact successfully created.');
   }
    
    if(-f $filename) {
	    unlink $filename;
    }
    
	$session->{__internal}->{message}{type} = 'info';
	my $content = new Mioga2::Content::REDIRECT($context, mode => 'external');
	$content->SetContent($session->{Contact}->{referer});
	return $content;
}


# ============================================================================

=head2 DisplayContact

    Display a contact 

=head3 Generated XML

    <DisplayContact group="new">

        <miogacontext>See Mioga2::RequestContext</miogacontext>

        <error>error id</err> 
        <referer>URL of calling application</referer>

        <!-- list of fields values -->
        <$field_ident>field value</$field_ident> 
        ...

     </DisplayContact>

=head3 Form actions and fields : 

    Available fields :

        name            type                 description
    ----------------------------------------------------------------
       rowid        : rowid             : the contact rowid 
       name         : not empty string  : the contact name
       surname      : string            : the contact surname
       title        : string            : the contact title
       orgname      : string            : the contact organization name
       photo_data   : string            : the photo rowid
       photo_url    : string            : the photo url
       email_work   : string            : the contact work email
       email_home   : string            : the contact home email
       tel_work     : string            : the contact work telephone number
       tel_home     : string            : the contact home telephone number
       tel_mobile   : string            : the contact mobile telephone number   
       tel_fax      : string            : the contact fax number
       addr_home    : string            : the contact home address
       city_home    : string            : the contact home city
       postal_code_home : string            : the contact home postal code
       state_home   : string            : the contact home state
       country_home : string            : the contact home country
       addr_work    : string            : the contact work address
       city_work    : string            : the contact work city	  
       postal_code_work : string            : the contact work postal code
       state_work   : string            : the contact work state  
       country_work : string            : the contact work country
       url          : string            : the contact web site url
       comment      : string            : the contact comments


=cut

# ============================================================================

sub DisplayContact
{    
    my ($self, $context) = @_;

    ##
    ## Check Args
    ##
    if(exists $context->{args}->{rowid}) {
	    my $rowid = $context->{args}->{rowid};
	    my $res = String::Checker::checkstring($rowid, ['disallow_empty', 'want_int']);
	    if(defined $res and @$res) {
		    return $self->_ErrorMessage($context,  $errors{'invalid_rowid'});
	    }
    }


    my $xml = '<?xml version="1.0"?>';
        
    # ---------------------------------------
    # Generate XML
    # ---------------------------------------
    $xml .= "<DisplayContact>";
    $xml .= $context->GetXML();
    $xml .= "<referer>".st_FormatXMLString($context->GetReferer())."</referer>";

    my $contact = $self->ContactGetContact($context, $context->{args}->{rowid});

    $contact->{comment} =~ s/\r\n/<br\/>/g;
    $contact->{comment} = st_FilterHTMLTags(\$contact->{comment}, @Mioga2::tools::args_checker::AllowedXHTMLTags);

    foreach my $key (keys %$contact) {
	    $xml .= "<$key>".st_FormatXMLString($contact->{$key})."</$key>";
    }

    $xml .= "</DisplayContact>";

  	my $content = new Mioga2::Content::XSLT($context, stylesheet => "contact.xsl", locale_domain => 'contact_xsl');
    $content->SetContent($xml);
    return $content;
}


# ============================================================================

=head2 GetPhoto

    return the photo

=cut

# ============================================================================

sub GetPhoto
{    
    my ($self, $context) = @_;

 	my $config = $context->GetConfig();
    my $dbh = $config->{dbh};

    ##
    ## Check args
    ##
    if(exists $context->{args}->{rowid}) {
	    my $oid = $context->{args}->{oid};
	    my $res = String::Checker::checkstring($oid, ['disallow_empty', 'want_int']);
	    if(defined $res and @$res) {
		    
		    return $self->_ErrorMessage($context,  $errors{'invalid_rowid'});
	    }
    }

 
    my $tmpfile = $self->ContactGetPhoto($context, $context->{args}->{oid});

    my $mime = mimetype($tmpfile);
    
	my $content = new Mioga2::Content::FILE($context, MIME => $mime);
    $content->SetContent($tmpfile);

    return $content;    
    
}


# ============================================================================

=head2 SearchContact
    
    Search contacts matching parameters

=head3 Generated XML

    <SearchContact>

        <miogacontext>See Mioga2::RequestContext</miogacontext>

        <method>Currrent method ident</method>

        <referer>URL of calling application</referer>

        <action>Action URI</action>
        <action>Action URI</action>
        ...

        <!-- LargeList XML. See Mioga2::LargeList->SearchElem -->


        <!-- Current application functions description -->
	    <Function>
           <$func authorised="right of current user (0|1)"/>
           ...
        </Function>


         <search_var>search field</search_var>
         <search_group>group to search</search_group>
         <search_type>search type</search_type>
         <search_text>search request</search_text>

     </SearchContact>

=head3 List fields

	See BrowseContact

=cut

# ============================================================================

sub SearchContact
{    
    my ($self, $context) = @_;
    
  	my $config = $context->GetConfig();
    my $dbh = $config->{dbh};
  	my $session = $context->GetSession();

    
    my $action;
    my $referer;
    if(exists $session->{Contact}->{search_engine}->{action}) {
        $action = $session->{Contact}->{search_engine}->{action};
        $referer = $session->{Contact}->{search_engine}->{referer};
    }
    elsif(exists $session->{Contact}->{search_referer}) {
	    $referer = $session->{Contact}->{search_referer};
    }
    else {
	    $referer = $context->GetReferer();
    }

    $session->{Contact}->{search_referer} = $referer;

    my $error = 0;
    if(exists $context->{args}->{sort_by} ) {
	    my $sort_by = $context->{args}->{sort_by};
	    my $res = String::Checker::checkstring($sort_by, ['stripxws', 'disallow_empty', ['disallow_chars' => '\'"[]\\']]);
	    if(defined $res and @$res) {
		    $error = $errors{'invalid_params'};
	    }
    }


    
    if($error) {
	    return $self->_ErrorMessage($context,  $error);
    }


    # ---------------------------------------
    # Generate XML
    # ---------------------------------------

    my $xml = '<?xml version="1.0"?>';
    
    $xml .= "<SearchContact>";
    $xml .= $context->GetXML();

    if(defined $action) {
	    $xml .= "<action>$action</action>";
    }

    $xml .= "<referer>";
    $xml .= st_FormatXMLString($session->{Contact}->{search_referer});
    $xml .= "</referer>";

    # ---------------------------------------
    # Build the contact list
    # ---------------------------------------
    my $params;
    my $args;
    my $var;
    $error = 0;
    
    if(!defined $context->{args}->{var}) {
	    $args = $session->{Contact}->{search_form};
    }
    else {
	    $args = $context->{args}
    }
    


    ##
    ## Check args
    ##
    if(exists $context->{args}->{page}) {
	    my $page = $context->{args}->{page};
	    my $res = String::Checker::checkstring($page, ['disallow_empty', 'want_int']);
	    if(defined $res and @$res) {
		    $error = $errors{'invalid_rowid'};
	    }
    }


    if(! defined $args->{group} or $args->{group} < 1 or $args->{group} > 2) {
	    $error = $errors{'invalid_params'};
    }
    
    if(! defined $args->{var} or $args->{var} < 1 or $args->{group} > 7) {
	    $error = $errors{'invalid_params'};
    }

    if(! defined $args->{type} or $args->{type} < 1 or $args->{type} > 2) {
	    $error = $errors{'invalid_params'};
    }

    my $text = $args->{text};
    my $res = String::Checker::checkstring($text, ['stripxws', 'disallow_empty', ['disallow_chars' => '\'"[]\\']]);
    if(defined $res and @$res) {
	    $error = $errors{'invalid_params'};
    }

    if($error) {
	    return $self->_ErrorMessage($context, $error);
    }


    if($args->{var} == 1) {
        $var = "name";
    }
    elsif($args->{var} == 2) {
        $var = "surname";
    }
    elsif($args->{var} == 3) {
        $var = "orgname";
    }
    elsif($args->{var} == 4) {
        $var = "city_work";
    }
    elsif($args->{var} == 5) {
        $var = "state_work";
    }
    elsif($args->{var} == 6) {
        $var = "country_work";
    }
    elsif($args->{var} == 7) {
        $var = "title";
    }

    push @{$params->{$var}}, $args->{text};
    
    my $largelist = new Mioga2::LargeList::Contact($context, "ContactSearchContact");
    $xml .= $largelist->SearchElem($context, $params, $args->{group}, $args->{type});

    $session->{Contact}->{search_form} = $args;

    $xml .= $self->GetXMLFunction($context);
    $xml .= $self->SearchForm($context);
    $xml .= "</SearchContact>";

  	my $content = new Mioga2::Content::XSLT($context, stylesheet => "contact.xsl", locale_domain => 'contact_xsl');
    $content->SetContent($xml);

    return $content;
}


# ============================================================================
# SearchForm
#    
#    return xml describing search form
#
# ============================================================================

sub SearchForm {
	my ($self, $context) = @_;

  	my $session = $context->GetSession();

	my $xml = "";

	if(exists $session->{Contact}->{search_form}->{var}) {
		$xml .= "<search_var>";
		$xml .= $session->{Contact}->{search_form}->{var};
		$xml .= "</search_var>";
	}
	
	if(exists $session->{Contact}->{search_form}->{group}) {
		$xml .= "<search_group>";
		$xml .= $session->{Contact}->{search_form}->{group};
		$xml .= "</search_group>";
	}	

	if(exists $session->{Contact}->{search_form}->{type}) {
		$xml .= "<search_type>";
		$xml .= $session->{Contact}->{search_form}->{type};
		$xml .= "</search_type>";
	}	

	if(exists $session->{Contact}->{search_form}->{text}) {
		$xml .= "<search_text>";
		$xml .= st_FormatXMLString($session->{Contact}->{search_form}->{text});
		$xml .= "</search_text>";
	}	

	return $xml;
}

# ============================================================================
# _SuccessMessage
#
#    _SuccessMessage display a success message
#
# ============================================================================
sub _SuccessMessage
{
    my ($self, $context, $action, $params) = @_;

	my $session = $context->GetSession();

    # -----------------------------
    # Generate XML
    # -----------------------------
    my $xml = '<?xml version="1.0"?>';
    
    $xml .= "<SuccessMessage>";
    $xml .= $context->GetXML();

    $xml .= "<referer>".st_FormatXMLString($session->{Contact}->{referer})."</referer>";
    $xml .= "<action>".$action."</action>";
    $xml .= "</SuccessMessage>";

    delete $session->{Contact}->{referer};


  	my $content = new Mioga2::Content::XSLT($context, stylesheet => "contact.xsl", locale_domain => 'contact_xsl');
    $content->SetContent($xml);

    return $content;
}



# ============================================================================
# _ErrorMessage
#
#    _ErrorMessage display an error message
#
# ============================================================================
sub _ErrorMessage
{
    my ($self, $context, $error) = @_;

    # -----------------------------
    # Generate XML
    # -----------------------------
    my $xml = '<?xml version="1.0"?>';
    
    $xml .= "<ErrorMessage>";
    $xml .= $context->GetXML();

    $xml .= "<referer>".st_FormatXMLString($context->GetReferer)."</referer>";
    $xml .= "<error>$error</error>";
    $xml .= "</ErrorMessage>";

	my $content = new Mioga2::Content::XSLT($context, stylesheet => "contact.xsl", locale_domain => 'contact_xsl');
    $content->SetContent($xml);

    return $content;
}




# ============================================================================
# XMLGetContactsModifiedSince ()
#
# contacts modified or created since the given date
#	
# ============================================================================

sub XMLGetContactsModifiedSince {
	my ($self, $context) = @_;

	my $xml = $self->ContactExportToXML($context, $context->GetGroup()->GetRowid(), $context->{args}->{date}, $context->{args}->{synchro_ident});

	my $content = new Mioga2::Content::XML($context);
	$content->SetContent($xml);

	return $content;
}


# ============================================================================
# XMLGetContactsRowids ()
#
# return contacts rowids
#	
# ============================================================================

sub XMLGetContactsRowids {
	my ($self, $context) = @_;

	my $xml = $self->ContactExportRowids($context, $context->GetGroup()->GetRowid(), $context->{args}->{synchro_ident});

	my $content = new Mioga2::Content::XML($context);
	$content->SetContent($xml);

	return $content;
}



# ============================================================================
# XMLImportContacts ()
#
#    Import given contacts into database.
#    
# ============================================================================

sub XMLImportContacts {
	my ($self, $context) = @_;

	my $xml = $self->ContactImportFromXML($context, 
										  $context->GetGroup()->GetRowid(), 
										  $context->{args}->{data}, 
										  $context->{args}->{synchro_ident});

	my $content = new Mioga2::Content::XML($context);
	$content->SetContent($xml);

	return $content;
}

# ============================================================================
# XMLDeleteContacts ()
#
#    Delete given contacts from database.
#
# ============================================================================

sub XMLDeleteContacts {
	my ($self, $context) = @_;

	my $xml = $self->ContactDeleteFromXML($context, $context->GetGroup()->GetRowid(), $context->{args}->{data});

	my $content = new Mioga2::Content::XML($context);
	$content->SetContent($xml);

	return $content;
}



# ============================================================================

=head1 DATA ACCESS METHODS DESCRIPTION

=cut

# ============================================================================



# ----------------------------------------------------------------------------
# ContactGetContact ($context, $ctc_id)
#
# return information for contact $ctc_id
#
# return value is a hashref like ContactAdd $params attributes
#
# ----------------------------------------------------------------------------
sub ContactGetContact {
    my ($self, $context, $ctc_id) = @_;

	my $config = $context->GetConfig();


    return $self->ContactDBGetContact ($config, $ctc_id);
}


# ----------------------------------------------------------------------------
# ContactExportVcard ($context, $rowid)
#
# ContactExportVcard convert the contact $rowid in vCard data.
#
# ---------------------------------------------------------------------------'
sub ContactExportVcard {
    my ($self, $context, $rowid) = @_;

	my $config = $context->GetConfig();


    $self->ContactDBExportVcard ($config, $rowid)
}



# ----------------------------------------------------------------------------
# ContactImportVcard ($context, $grp_id, $vcard_data)
#
# ContactImportVcard parse a vCard and add a contact for group grp_id. 
#
# ---------------------------------------------------------------------------'
sub ContactImportVcard {
    my ($self, $context, $grp_id, $vcard_data) = @_;

	my $config = $context->GetConfig();


    $self->ContactDBImportVcard ($config, $grp_id, $vcard_data)
}


# ----------------------------------------------------------------------------
# ContactAdd ($context, $grp_id, $params)
#
# ContactAdd create a new contact for group $grp_id.
#
# Contact parameters are passed in the $params hash ref :
#
# Available fields are :
#    name          : last name (required)
#    surname       : first name
#    photo_data    : JPEG data of contact's photo
#    photo_url     : the photo's URL
#    title         : title
#    orgname       : the organisation name
#
#    email_work    : professional email
#    email_home    : personal email
#
#    tel_work      : professional phone number
#    tel_home      : personal phone number
#    tel_mobile    : GSM phone number
#    tel_fax       : fax number
#
#    addr_home     : personal address
#    city_home     : personal city
#    postal_code_home : personal postal code
#    state_home    : personal state
#    country_home  : personal country
#
#    addr_work     : professional address
#    city_work     : professional city   
#    postal_code_work : professional postal code :
#    state_work    : professional state  
#    country_work  : professional country
#
#    url           : organisation web site URL
#
#    comment       : comments
#
# ----------------------------------------------------------------------------
sub ContactAdd
{
    my ($self, $context, $grp_id, $params) = @_;

	my $config = $context->GetConfig();


    $self->ContactDBAdd($config, $grp_id, $params);
}


# ----------------------------------------------------------------------------
# ContactModify ($context, $rowid, $params)
#
# ContactModify modify the contact $rowid
#
# ---------------------------------------------------------------------------'
sub ContactModify {
    my ($self, $context, $rowid, $params) = @_;

	my $config = $context->GetConfig();

           
    $self->ContactDBModify($config, $rowid, $params);
}


# ----------------------------------------------------------------------------
# ContactRemove ($context, $rowid)
#
# ContactRemove remove the contact $rowid
#
# ---------------------------------------------------------------------------'
sub ContactRemove {
    my ($self, $context, $rowid) = @_;

	my $config = $context->GetConfig();

           
    $self->ContactDBRemove($config, $rowid);
}


# ----------------------------------------------------------------------------
# ContactGetPhoto ($context, $oid, $filename)
#
# ContactGetPhoto export the phto with oid $oid in $filename 
#
# If $filename is not specified a new file will be created in /tmp.
#
# Return the file name
#
# ---------------------------------------------------------------------------'
sub ContactGetPhoto 
{
    my ($self, $context, $oid, $filename) = @_;

	my $config = $context->GetConfig();


	$self->ContactDBGetPhoto ($config, $oid, $filename);
}



# ============================================================================
# DeleteGroupData ($config, $group_id)
#
#    Remove group data in database when a group is deleted.
#
#    This function is called when a group/user/resource is deleted.
#
# ============================================================================

sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;

    $self->ContactDBDeleteGroupData($config, $group_id);
}


# ---------------------------------------------------------------------------'
# ContactGetNumberMatchingForGroup ($context, $grp_id, $params)
#
# ContactGetNumberMatchingForGroup return the number of contact matching regexps
# for group $grp_id
#
# $params is an hashref with ContactAdd keys and regexp's list as values
#
# ---------------------------------------------------------------------------'
sub ContactGetNumberMatchingForGroup {
    my ($self, $context, $grp_id, $params) = @_;

	my $config = $context->GetConfig();


    $self->ContactDBGetNumberMatchingForGroup($config, $grp_id, $params);
}


# ----------------------------------------------------------------------------
# ContactGetNumberForGroup ($context, $grp_id)
#
# ContactGetNumberForGroup return the number of contact for group $grp_id
#
# ---------------------------------------------------------------------------'
sub ContactGetNumberForGroup {
    my ($self, $context, $grp_id) = @_;

	my $config = $context->GetConfig();


    $self->ContactDBGetNumberForGroup($config, $grp_id);
}



# ---------------------------------------------------------------------------'
# ContactGetNumberMatching ($context, $grp_id_list, $params)
#
# ContactGetNumberMatching return the number of contact matching regexps
# for groups $grp_id_list
#
# $params is an hashref with ContactAdd keys and regexp's list as values
#
# ---------------------------------------------------------------------------'
sub ContactGetNumberMatching {
    my ($self, $context, $grp_id_list, $params) = @_;

	my $config = $context->GetConfig();


    $self->ContactDBGetNumberMatching($config, $grp_id_list, $params);
}

# ----------------------------------------------------------------------------
# ContactGetMatchingForGroupWithOffset ($context, $grp_id, $offset, $limit, $sort_field_name, $asc, $params)
#
# ContactGetForGroupWithOffset return the list of contact for group $grp_id
# with offset and limit.
#
# ---------------------------------------------------------------------------'
sub ContactGetMatchingForGroupWithOffset {
    my ($self, $context, $grp_id, $offset, $limit, $sort_field_name, $asc, $params) = @_;

	my $config = $context->GetConfig();


    $self->ContactDBGetMatchingForGroupWithOffset ($config, $grp_id, $offset, $limit, $sort_field_name, $asc, $params);
}


# ----------------------------------------------------------------------------
# ContactGetMatchingWithOffset ($context, $grp_id_list, $offset, $limit, $sort_field_name, $asc, $params)
#
# ContactGetForGroupWithOffset return the list of contact for groups $grp_id_list
# with offset and limit.
#
# ---------------------------------------------------------------------------'
sub ContactGetMatchingWithOffset {
    my ($self, $context, $grp_id_list, $offset, $limit, $sort_field_name, $asc, $params) = @_;

	my $config = $context->GetConfig();


	$self->ContactDBGetMatchingWithOffset ($config, $grp_id_list, $offset, $limit, $sort_field_name, $asc, $params);
}



# ---------------------------------------------------------------------------'
# ContactExportToXML
#
# ContactiExportToXML export $groupId's contacts to XML
#
# Return a xml string like :
#
# <ContactList>
#    <Contact>
#        <rowid>1</rowid>
#        <created>2002-04-04 09:00:39+00</created>
#        <modified>2002-04-04 09:00:39+00</modified>
#        <name>Toto</name>
#                
#        and all available fields (see ContactDBAdd) except photo_data.
#
#    </Contact>
#    <Contact>
#       .
#       .
#       .
#    </Contact>
# </ContactList>
#
# ---------------------------------------------------------------------------'
sub ContactExportToXML {
    my ($self, $context, $group_id, $modif_date, $synchro_ident) = @_;

	my $config = $context->GetConfig();


	$self->ContactDBExportToXML ($config, $group_id, $modif_date, $synchro_ident);
}


# ---------------------------------------------------------------------------'
# ContactExportRowids
#
# ContactExportRowids export $groupId's contact rowids to XML
#
# Return a xml string like :
#
# <RowidList>
#    <rowid>1</rowid>
#    <rowid>3</rowid>
#    ...
# </RowidList>
#
# ---------------------------------------------------------------------------'
sub ContactExportRowids {
    my ($self, $context, $group_id, $synchro_ident) = @_;

	my $config = $context->GetConfig();


	$self->ContactDBExportRowids ($config, $group_id, $synchro_ident);
}


# ---------------------------------------------------------------------------'
=head2 ContactDeleteFromXML

ContactDeleteFromXML delete contacts list described in XML data

The data xml string looks like :

<RowidList>
    <rowid>1</rowid>
    <rowid>3</rowid>
    ...
</RowidList>

=cut

# ---------------------------------------------------------------------------'
sub ContactDeleteFromXML {
    my ($self, $context, $group_id, $data) = @_;

	my $config = $context->GetConfig();


	$self->ContactDBDeleteFromXML ($config, $group_id, $data);
}


# ---------------------------------------------------------------------------'
# ContactImportFromXML
#
# ContactImportFromXML import contacts list described in XML data
#
# The data xml string looks like :
#
# <ContactList>
#    <Contact>
#        <rowid>1</rowid>
#        <remote_id>3</rowid>
#        <created>2002-04-04 09:00:39+00</created>
#        <modified>2002-04-04 09:00:39+00</modified>
#        <name>Toto</name>
#                
#        and all available fields (see ContactDBAdd) except photo_data.
#
#    </Contact>
#    <Contact>
#       .
#       .
#       .
#    </Contact>
# </ContactList>
#
# ---------------------------------------------------------------------------'
sub ContactImportFromXML {
    my ($self, $context, $group_id, $data, $synchro_ident) = @_;

	my $config = $context->GetConfig();

	$self->ContactDBImportFromXML ($config, $group_id, $data, $synchro_ident);
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The MiogaII Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


