# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Redirector.pm: Redirect URLs but login to a non http auth based URL before.

=head1 METHODS DESRIPTION

=cut

package Mioga2::Redirector;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'redirector';

use Error qw(:try);
use Data::Dumper;
use Mioga2::Content::REDIRECT;
use Mioga2::Application;
use URI::Escape;
use LWP;

use Mioga2::tools::APIAuthz;

my $debug = 0;

# ============================================================================

=head2 GetAppDesc ()

	Return the current application description.

=cut

# ============================================================================

sub GetAppDesc {
	my ($self, $context) = @_;

    my %AppDesc = ( ident   => 'Redirector',
                package => 'Mioga2::Redirector',
                description => __('The Mioga2 URL Redirector'),
				type    => 'library',
                all_groups => 1,
                all_users  => 1,
                can_be_public => 1,
				is_user            => 1,
				is_group           => 1,
				is_resource        => 1,

                functions   => { 'All' => __('All Functions') },

                func_methods  => { 'All' => [ 'Redirect' ],
                                 },
				public_methods => [
						'Redirect'
					],

				sxml_methods => {},

				xml_methods  => {},

                );
    
	return \%AppDesc;
}



# ============================================================================

=head2 TestAuthorize ($context)

	Check if the current user can access to the current application and 
	methods in the current group.

=cut

# ============================================================================

sub TestAuthorize {
	return AUTHZ_OK;
}


# ============================================================================

=head2 Redirect ($context)

	Redirect to an external url but first, login to a login page with 
	current user and password and catch Cookies.

=cut

# ============================================================================

sub Redirect {
	my ($self, $context) = @_;
	my $config = $context->GetConfig();


	my $ua = LWP::UserAgent->new(max_redirect => 0,
								 requests_redirectable => []);
	$ua->agent("Mioga2Redirector/2.0-beta1");

	if(exists $context->{args}->{logout_url}) {
		my $logout_url = $config->GetProtocol()."://".$config->GetServerName().$context->{args}->{logout_url};
		
		my $req = HTTP::Request->new(GET => $logout_url);
		$ua->request($req);
	}

	my $login_url = $context->{args}->{login_url};
	my $redirect_url = $context->{args}->{redirect_url};

	my $user_email = uri_escape($context->GetUser()->GetEmail());
	$login_url =~ s/\$\{user_email\}/$user_email/;

	my $user_ident = uri_escape($context->GetUser()->GetIdent());
	$login_url =~ s/\$\{user_ident\}/$user_ident/;
	
	my $mioga_domain = uri_escape($config->GetDomainName());
	$login_url =~ s/\$\{domain\}/$mioga_domain/;
	
	if($login_url =~ m!^/!) {
		$login_url = $config->GetProtocol()."://".$config->GetServerName()."$login_url";
	}

	my $req = HTTP::Request->new(GET => $login_url);
	my $res = $ua->request($req);

	my @cookies = $res->header("Set-Cookie");

	my $content = new Mioga2::Content::REDIRECT(mode => 'external');
	$content->SetContent($redirect_url, cookies => \@cookies);

	return $content;
}






# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga2 Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
