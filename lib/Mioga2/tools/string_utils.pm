# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
string_utils.pm: used for string manipulation

=head1 DESCRIPTION

	This collection of utilities helps to deal with string formats
	They can convert strings to XMLified format or Database suitable format...

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
package Mioga2::tools::string_utils;
use strict;
use utf8;

use Locale::TextDomain::UTF8 'tools_string_utils';
use vars qw(@ISA @EXPORT);

use URI::Escape;
use Mioga2::Content::XSLT;
use Mioga2::XML::Simple;
use Exporter;
use Text::Iconv;
use Encode::Detect::Detector;
use Data::Dumper;
use HTML::TokeParser::Simple;
use Encode;

@ISA = qw(Exporter);
@EXPORT = qw(st_NoAccent st_ConvertOctetXML st_CutString st_FormatXMLString st_FormatPostgreSQLString st_FormatPostgreSQLRegExpString st_FormatJSONString st_ArgExists st_SuccessMessage st_URIEscape st_URIUnescape st_FormatAccentRegexp st_AccentRegexp st_XMLDOMToHash st_Utf8ToISO st_HashToXML st_CreateSalt st_RemoveAccents st_PathEscape st_CheckUTF8 st_FilterHTMLTags);

my $debug = 0;

# ----------------------------------------------------------------------------

=head2 st_ConvertOctetXML ($file_size)

	return file_size in XML readable format

	ex : $size = 37575 => <filesize mult="k">37.</filesize>

	mult could be : 1, k, M, G

	$size in octet

=cut

# ----------------------------------------------------------------------------
sub st_ConvertOctetXML
{
	my ($size) = @_;
	my $rest = 0;

	if ($size < 1024)
	{
		return '<filesize mult="1">'.$size.'</filesize>';
	}
	elsif ($size < 1024*1024)
	{
		$rest = int(($size % 1024) / 100);
		$size = int($size / 1024);
		return '<filesize mult="k">'.$size.'.'.$rest.'</filesize>';
	}
	elsif ($size < 1024*1024*1024)
	{
		$rest = int(($size % (1024*1024)) / 100000);
		$size = int($size / (1024*1024));
		return '<filesize mult="M">'.$size.'.'.$rest.'</filesize>';
	}
	elsif ($size >= 1024*1024*1024)
	{
		$rest = int(($size % (1024*1024*1024))/100000000);
		$size = int($size / (1024*1024*1024));
		return '<filesize mult="G">'.$size.'.'.$rest.'</filesize>';
	}

}

# ----------------------------------------------------------------------------

=head2 st_ArgExists ($context, $arg_name)

	return true if $arg_name or $arg_name.x exists in current request arguments.

=cut

# ----------------------------------------------------------------------------
sub st_ArgExists
{
        my $context = shift;
        my $arg = shift;
        return ((exists($context->{args}->{"$arg"}) && $context->{args}->{"$arg"} ne "0") || exists($context->{args}->{"${arg}.x"}));
}



# ----------------------------------------------------------------------------

=head2 st_CutString ($string, $length)

	Takes a string $string and cut it at length $length

	return the cutted string

=cut

# ----------------------------------------------------------------------------
sub st_CutString
{
	my $str = shift;
	my $len = shift;

	$$str = substr $$str, 0, $len;

	return $str;
}

# ----------------------------------------------------------------------------

=head2 st_FormatXMLString($string)

	Escape all characters for XML PCDATA (&, <, > and ") and replace them by
	the correct value.
	It returns the modified string 

=cut

# ---------------------------------------------------------------------------"
sub st_FormatXMLString
{
    my ($string) = @_;

	if ($string =~ /[\&\<\>\"]/)
	{
		$string =~ s/\&/\&amp\;/g;
		$string =~ s/\</\&lt\;/g;
		$string =~ s/\>/\&gt\;/g;
		$string =~ s/\"/\&quot\;/g;
	}

	return st_CheckUTF8 ($string);
}

# ----------------------------------------------------------------------------

=head2 st_FormatPostgreSQLString($string)

	Escape all characters for PostgreSQL string (') and replace them by
	the correct value ('').
	It returns the modified string 

=cut

# ---------------------------------------------------------------------------'
sub st_FormatPostgreSQLString
{
    my ($string) = @_;
	$string = '' unless ($string);

	if ($string =~ /[\']/)
	{
		$string =~ s/'/''/g;
	}
	
	return st_CheckUTF8 ($string);
}


# ----------------------------------------------------------------------------

=head2 st_NoAccent($string)

	Write out the postgresql call to 'translate(string text, from text, to text)' to eliminate accents if any in $string. $string is here the first argument of translate (text).

=cut

# ---------------------------------------------------------------------------'
sub st_NoAccent
{
    my ($string) = @_;
    
	return "translate($string, 'ÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ', 'aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn')";
}

# ----------------------------------------------------------------------------

=head2 st_FormatPostgreSQLRegExpString($string) 

	Like st_FormatPostgreSQLString but :
	escape \ two time,
	escape ( and )
	escape [ and ]
	escape ^ $ . + ? *

=cut

# ---------------------------------------------------------------------------'
sub st_FormatPostgreSQLRegExpString
{
    my ($string) = @_;

	if ($string =~ /[\\\'\[\]\(\)\{\}\$\^\+\.\?\*]/)
	{
		$string =~ s/\\/\\\\/g;
		$string =~ s/'/''/g;
		$string =~ s/\{/\\\{/g;
		$string =~ s/\}/\\\}/g;
		$string =~ s/\(/\\\(/g;
		$string =~ s/\)/\\\)/g;
		$string =~ s/\[/\\\[/g;
		$string =~ s/\]/\\\]/g;
		$string =~ s/\$/\\\$/g;
		$string =~ s/\^/\\\^/g;
		$string =~ s/\./\\\./g;
		$string =~ s/\+/\\\+/g;
		$string =~ s/\?/\\\?/g;
		$string =~ s/\*/\\\*/g;
	}

	return $string;
}


#===============================================================================

=head2 st_FormatJSONString

Escape characters from string to be used as JSON

=cut

#===============================================================================
sub st_FormatJSONString {
	my ($string) = @_;

	if ($string =~ /\\/) {
		$string =~ s/\\/\\\\/g;
	}

	if ($string =~ /["\n{}\[\]\/\t]/) {
		$string =~ s/\n/\\n/g;
		$string =~ s/\r/\\r/g;
		$string =~ s/\t/\\t/g;
		$string =~ s/"/\\"/g;
		$string =~ s/\//\\\//g;
	}

	return ($string);
}	# ----------  end of subroutine st_FormatJSONString  ----------


# ============================================================================

=head2 st_FilterHTMLTags($text, $keep)

Improved version of the (soon obsolete) st_CleanupHTMLTags.
Removes all HTML/XML tags (tags formed by <XXXXX>) from a string, except those given
in the second argument.

Parameters are: a ref on a string (which will not be changed), 
and a hashref whose keys contain tags to keep
(if this parameter is undef or "{}", all tags are removed).
The values for each tag to keep can be:
* 1 , for keeping the tag without further inspections;
* { 'attribute1' => 1 }. The tag will always be kept, but all attributes except "attribute1"
will be purged. Any value for "attribute1" will be accepted.

Returns a new, cleaned up string.

=cut
# ----------------------------------------------------------------------------
sub st_FilterHTMLTags {
	my ($text, $keep) = @_;
	$keep = {} unless defined $keep;
	my $parser = HTML::TokeParser::Simple->new($text);
	my $result = "";
	
	while(my $token = $parser->get_token) {
		if ($token->is_tag) {
			my $keepvalue = $keep->{$token->get_tag};
			if (defined $keepvalue) {
				if ((not $token->is_start_tag) or ref($keepvalue) eq '' && $keepvalue == 1) {
					# Accept this tag without conditions
					$result .= $token->as_is;
					
				} elsif (ref($keepvalue) eq 'HASH') {
					# Check the attributes of this start tag
					my $attrhash = $token->get_attr;
					foreach my $attrkey (keys %$attrhash) {
						my $allowed_values = $keepvalue->{$attrkey};
						unless (defined($allowed_values) and $allowed_values == 1) {
							$token->delete_attr($attrkey);
						}
					}
					$result .= $token->as_is;
				}
			}
		} else {
			# This token is no tag
			$result .= $token->as_is;
		}
	}
	return $result;
}
# ============================================================================

=head2 st_SuccessMessage ($context, $name, $action, $referer)

	Deprecated - Success message XML generation.
	Use InlineMessage XSL template instead in combination with $session->{__internal}->{message}

=cut

# ============================================================================

sub st_SuccessMessage {
	my ($context, $stylesheet, $name, $action, $referer, $domain) = @_;
	my $content = new Mioga2::Content::XSLT($context, stylesheet => $stylesheet, locale_domain => $domain);

	my $xml = '<?xml version="1.0"?>';

	$xml .=	"<SuccessMessage>";
	$xml .= $context->GetXML();
	$xml .= "<name>".st_FormatXMLString($name)."</name>";
	$xml .= "<action>".st_FormatXMLString($action)."</action>";
	$xml .= "<referer>".st_FormatXMLString($referer)."</referer>";
	$xml .= "</SuccessMessage>";
	

	$content->SetContent($xml);
	return $content;
}

# ============================================================================

=head2 st_URIEscape ($string)

	escape chars in $string if needed and return escaped string 

=cut

# ============================================================================

sub st_URIEscape {
	my ($string) = @_;

	#$string = uri_escape($string); # use default chars : "^A-Za-z0-9\-_.!~*'()"
	$string = uri_escape($string, "\x00-\x22\x7f-\xff#&|'%\=\?") unless $string =~ /%[A-Fa-f0-9]{2}/;

	return $string;
}
# ============================================================================

=head2 st_URIUnescape ($string)

	unescape chars in $string and return unescaped string 

	'+' is converted to ' ' on args part

=cut

# ============================================================================

sub st_URIUnescape {
	my ($string) = @_;

	$string = Encode::decode ('utf8', uri_unescape($string));

	#
	# Change '+' to spaces for parameters of URI
	#
	$string =~ /([^?]*)\?(.*)/;
	my $a = $1;
	my $b = $2;
	warn ("st_URIUnEscape a = $a  b = $b\n") if ($debug);
	if (defined($b) && (length($b) > 0) ) {
		$b =~ s/\+/ /g;
		$string = "$a?$b";
	}

	warn ("st_URIUnEscape return = " . st_CheckUTF8 ($string) . "\n") if ($debug);
	return st_CheckUTF8 ($string);
}
# ============================================================================

=head2 st_FormatAccentRegexp ($string)
	
	Transform regexp to handle accent.

	Exemple : 
		[e]t[e] => [èéêëe]t[èéêëe]
	
=cut

# ============================================================================

sub st_FormatAccentRegexp {
	my $regexp = shift;

	$regexp =~ s/a/àáâãäåÀÁÂÃÄÅA/gi;
	$regexp =~ s/e/èéêëeÈÉÊËE/gi;
	$regexp =~ s/i/ìíîïiÌÍÎÏI/gi;
	$regexp =~ s/o/òóôõöøoÒÓÔÕÖØO/gi;
	$regexp =~ s/u/ùúûüuÙÚÛÜU/gi;
	$regexp =~ s/y/ÿyŸY/gi;
	$regexp =~ s/c/çcÇC/gi;
	$regexp =~ s/n/ñnÑN/gi;

	return $regexp;
}



# ============================================================================

=head2 st_AccentRegexp ($string)
	
	Transform regexp to handle accent.

	Exemple : 
	ete => èéêëetèéêëe
	
=cut

# ============================================================================

sub st_AccentRegexp {
	my $regexp = shift;

	$regexp =~ tr/ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ/aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn/;
	$regexp =~ s/a/[àáâãäåÀÁÂÃÄÅA]/gi;
	$regexp =~ s/e/[èéêëeÈÉÊËE]/gi;
	$regexp =~ s/i/[ìíîïiÌÍÎÏI]/gi;
	$regexp =~ s/o/[òóôõöøoÒÓÔÕÖØO]/gi;
	$regexp =~ s/u/[ùúûüuÙÚÛÜU]/gi;
	$regexp =~ s/y/[ÿyŸY]/gi;
	$regexp =~ s/c/[çcÇC]/gi;
	$regexp =~ s/n/[ñnÑN]/gi;

	return $regexp;
}

# ============================================================================

=head2 st_RemoveAccents ($string)
	
	Remove any accentued characters from $string.
	
=cut

# ============================================================================

sub st_RemoveAccents
{
	my $str = shift;
	
	$str =~ tr/ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ/aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn/;
	return $str;
}


# ============================================================================

=head2 st_XMLDOMToHash ($dom_tree, $elems, $attrs)
	
	Transform XML::LibXML Dom Tree to perl hash.

	if $elems or attrs are not defined, all the elements and attributes are
	converted.

=cut

# ============================================================================

sub st_XMLDOMToHash {
	my ($dom_tree, $elems, $attrs) = @_;

	my $xs = new Mioga2::XML::Simple();

	my @elem_nodes;
	if(defined $elems) {
		my %res;

		foreach my $elem (@$elems) {
			my @children = $dom_tree->getChildrenByTagName($elem);
			
			if(@children > 1) {
				$res{$elem} = [map {$xs->_XMLTree($_)} @children];
			}
			elsif(@children == 1) {
				$res{$elem} = $xs->_XMLTree($children[0]);
			}
		}

		foreach my $attr ($dom_tree->getAttributes) {
			if(! defined $attrs or 
			   grep { $attr eq $_ } @$attrs) {

				my $data = $attr->getData;
				$res{$attr->getName} = $data;

			}
		}

		return \%res;
	}
	else {
		return $xs->_XMLTree($dom_tree);
	}

}

# ============================================================================

=head2 st_HashToXML ($hash)
	
	Transform a perl hash to XML.

	THIS METHOD IS DEPRECATED AND SHOULD NOT BE USED ANYMORE. Use
	Mioga2::tools::Convert::PerlToXML method if you need to convert a Perl hash
	to XML.

=cut

# ============================================================================

sub st_HashToXML { # :WARNING:29/07/2010 17:01:SNI: THIS METHOD IS DEPRECATED. See note above.
	my ($tree, $prefix) = @_;

	my $xml = "";
	
	my $ext_prefix = "";
	if(defined $prefix) {
		$ext_prefix = "$prefix:";
	}

	foreach my $key (keys %$tree) {
		my $subtree = $tree->{$key};
		if(ref($subtree) eq "HASH" and keys(%$subtree) == 0) {
			$xml .= "<$ext_prefix$key/>";
		}
		elsif(ref($subtree) eq "HASH") {
			$xml .= "<$ext_prefix$key>";
			$xml .= st_HashToXML($subtree, $prefix);
			$xml .= "</$ext_prefix$key>";
		}
		elsif(ref($subtree) eq "ARRAY") {
			foreach my $elem (@$subtree) {
				$xml .= "<$ext_prefix$key>";
				if(ref($elem) eq "HASH") {			
					$xml .= st_HashToXML($elem, $prefix);
				}
				else {
					$xml .= st_FormatXMLString($elem);
				}
				$xml .= "</$ext_prefix$key>";
			}
		}
		else {
			$xml .= "<$ext_prefix$key>".st_FormatXMLString($subtree)."</$ext_prefix$key>";
		}
	}
	
	return $xml;
}

# ============================================================================

=head2 st_CreateSalt ()
	 
	Create a salt for pssword encryption modules

=cut

# ============================================================================

sub st_CreateSalt {
	srand time;
	

	sub GetRand {
		while(1) {
			my $rand = chr(int(rand(95)) + 33);
			
			if($rand =~ /^[A-Za-z0-9-_,\.]$/) {
				return $rand;
			}
        }
	}
	

	my $rand1 = GetRand();
	my $rand2 = GetRand();
	
	return $rand1.$rand2;   
}


#===============================================================================

=head2 st_PathEscape

Escape a filesystem path

=cut

#===============================================================================
sub st_PathEscape {
	my ($str) = @_;

	if ($str =~ /["' \[\]\{\}\(\)]/) {
		$str =~ s/"/\\"/g;
		$str =~ s/'/\\'/g;
		$str =~ s/ /\\ /g;
		$str =~ s/\[/\\\[/g;
		$str =~ s/\]/\\\]/g;
		$str =~ s/\{/\\\{/g;
		$str =~ s/\}/\\\}/g;
		$str =~ s/\(/\\\(/g;
		$str =~ s/\)/\\\)/g;
	}

	return ($str);
}	# ----------  end of subroutine st_PathEscape  ----------


#===============================================================================

=head2 st_Random

Generate a random string

=cut

#===============================================================================
sub st_Random {
	my ($length) = @_;

	my $str = '';
	my @chars = ('a'..'z', 'A'..'Z', '0'..'9');

	srand;

	my $_rand;
	for (my $i=0; $i < $length ;$i++) {
		$_rand = int(rand scalar (@chars));
		$str .= $chars[$_rand];
	}

	return ($str);
}	# ----------  end of subroutine st_Random  ----------


#===============================================================================

=head2 st_CheckUTF8

Check if string is UTF8 and convert it if needed

=cut

#===============================================================================
sub st_CheckUTF8 {
    my ($str) = @_;
    
    my $conv    = Text::Iconv->new('utf8', 'utf8');
    my $tmp_str = $conv->convert($str);
    unless ($tmp_str) {
        my $charset = detect($str) || 'iso-8859-15'; # defaults to latin9
        $conv = Text::Iconv->new($charset, "utf8");
        $str  = $conv->convert($str);
    }
	utf8::decode ($str) unless (utf8::is_utf8 ($str));
    return ($str);
}	# ----------  end of subroutine st_CheckUTF8  ----------

# ============================================================================

=head2 st_RandomData

Get random data from /dev/urandom. Takes as parameter the number of bytes
to return. Bytes might contain anything.

=cut

#===============================================================================
sub st_RandomData {
    my ($nr_char) = @_;  
    return `dd if=/dev/urandom bs=1 count=$nr_char 2>/dev/null`;
}

# ============================================================================



=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
