# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
date_utils.pm: used for dates manipulation

=head1 DESCRIPTION

	This collection of utilities helps to deal with dates

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::tools::date_utils;

my $debug=0;

use Locale::TextDomain::UTF8 'tools_dateutils';

use vars qw(@ISA @EXPORT);
use strict;
use Exporter;
use Date::Manip;
use Date::ICal;
use Data::Dumper;
use POSIX qw(strftime tzset);

@ISA = qw(Exporter);
@EXPORT = qw(du_GetNowISO du_ISOToXML du_ISOToXMLInUserLocale du_GetNowXML du_GetDateXML du_DateInterval
			 du_ISOToSecondInUserLocale du_ISOToSecond du_SecondToISO du_SecondToISOInUserLocale
			 du_YMDInUserLocaleToISOGMT
			 du_ConvertLocaleISOToGMT du_ConvertGMTISOToLocale
			 du_GetDateXMLInUserLocale du_GetDateInUserLocale du_GetMonthDuration
			 du_IsWorkingDayForUser du_IsWorkingDayForResource  du_IsHoliday du_GetNumberOfNotWorkingDayForUserBetween
			 du_GetNotWorkingDayForUserBetween
			 du_EnGMTToISO du_CreationTimeToISO du_FirstRecurDate
			 du_ISOToICal du_ISOLocalToICalGMT du_SecondToICal du_dayNumberToICal
			 du_ISOToHashInUserLocale);


# ----------------------------------------------------------------------------

=head2 du_GetDateXML ($time)

	return the given date (in second since epoch) in XML.


=head3 Generated XML 
	

	<sec>second</sec>
    <min>minute</min>
    <hour>hour</hour>
    <day>day</day>
    <month>month</month>
    <year>year</year>
    <wday>Day of week 0 = Sunday, 6 = Saturday</wday>
    <yday>Day of year</yday>

=cut

# ----------------------------------------------------------------------------
sub du_GetDateXML
{
	my $time = shift;

	my @date = localtime($time);

	my $xml = "<sec>$date[0]</sec>";
	$xml .= "<min>$date[1]</min>";
	$xml .= "<hour>$date[2]</hour>";
	$xml .= "<day>$date[3]</day>";
	$xml .= "<month>".($date[4]+1)."</month>";
	$xml .= "<year>".($date[5]+1900)."</year>";
	$xml .= "<wday>$date[6]</wday>";
	$xml .= "<yday>$date[7]</yday>";

	return $xml;
}


# ----------------------------------------------------------------------------

=head2 du_GetNowISO ()

 	return the current date in ISO format

=cut

# ----------------------------------------------------------------------------
sub du_GetNowISO
{
	my $res;

	{ 
		local $ENV{TZ} = 'GMT';
		tzset();
		
		$res = strftime("%Y-%m-%d %H:%M:%S", gmtime);
	}

	tzset();

	return $res;
}



# ----------------------------------------------------------------------------

=head2 du_ISOToXML ($iso)

	return the given ISO date in XML.

=cut

# ----------------------------------------------------------------------------
sub du_ISOToXML
{
	my $iso = shift;
	
	my $sec = du_ISOToSecond($iso);

	return du_GetDateXML($sec);
}

# ----------------------------------------------------------------------------

=head2 du_ISOToXMLInUserLocale ($iso, $user)

	return the given ISO date in XML in given user locale.

=cut

# ----------------------------------------------------------------------------
sub du_ISOToXMLInUserLocale
{
	my ($iso, $user) = @_;
	my $sec = du_ISOToSecond($iso);	

	return du_GetDateXMLInUserLocale($sec, $user);
}


#===============================================================================

=head2 du_ISOToHashInUserLocale ($iso, $user)

return the given ISO date as a Perl hash in given user locale.

=cut

#===============================================================================
sub du_ISOToHashInUserLocale {
	my ($iso, $user) = @_;

	my $sec = du_ISOToSecond($iso);

	return (du_GetDateHashInUserLocale ($sec, $user));
}	# ----------  end of subroutine du_ISOToHashInUserLocale ($iso, $user)  ----------


# ----------------------------------------------------------------------------

=head2 du_YMDInUserLocaleToISOGMT ($iso, $user)

	Convert a date with year, month and day (00:00:00 for hour)
	in User locale to an ISO date in GMT

=cut

# ----------------------------------------------------------------------------
sub du_YMDInUserLocaleToISOGMT
{
	my ($year, $month, $day, $user) = @_;

	my $sec;
	{
		local $ENV{TZ} = $user->GetTimezone();
		tzset();
		$sec = strftime("%Y-%m-%d  %H:%M:%S",0, 0, 0, $day, $month-1, $year-1900); 
	}

	tzset();

	return $sec;
}
# ----------------------------------------------------------------------------

=head2 du_ConvertLocaleISOToGMT ($iso, $user)

	Convert an ISO date in user locale to GMT.

=cut

# ----------------------------------------------------------------------------
sub du_ConvertLocaleISOToGMT
{
	my ($iso, $user) = @_;
	my $sec = du_ISOToSecondInUserLocale($iso, $user);

	my $res;
	{
		local $ENV{TZ} = $user->GetTimezone();
		tzset();
		$res = strftime("%Y-%m-%d %H:%M:%S", gmtime($sec));
	}

	tzset();

	return $res;
}

# ----------------------------------------------------------------------------

=head2 du_ConvertGMTISOToLocale ($iso, $user)

	Convert an ISO date in GMT to user locale.

=cut

# ----------------------------------------------------------------------------
sub du_ConvertGMTISOToLocale
{
	my ($iso, $user) = @_;
	my $sec = du_ISOToSecond($iso);
	my $res;
	{
		local $ENV{TZ} = $user->GetTimezone();
		tzset();
		$res = strftime("%Y-%m-%d %H:%M:%S", localtime($sec));
	}

	tzset();

	return $res;
}

# ----------------------------------------------------------------------------

=head2 du_GetNowXML ()

	return the current date in XML.

=cut

# ----------------------------------------------------------------------------
sub du_GetNowXML
{
	return du_GetDateXML(time);
}

# ----------------------------------------------------------------------------

=head2 du_EnGMTToISO ($date)

	Convert an English GMT date (Wed, 28 Jan 2004 08:18:48 GMT) to ISO GMT

=cut

# ----------------------------------------------------------------------------
sub du_EnGMTToISO
{
	my ($date) = @_;

	my %monthname = ('jan' => '01', 'feb' => '02', 'mar' => '03', 'apr' => '04', 'may' => '05',
					 'jun' => '06', 'jul' => '07', 'aug' => '08', 'sep' => '09', 
					 'oct' => 10, 'nov' => 11, 'dec' => 12);

	my ($day, $month, $year, $time) =
		($date =~ /^[^,]+,\s+(\d{2})\s+(\w{3})\s+(\d{4})\s+(\d{2}:\d{2}:\d{2})/);
	
	$month = $monthname{lc($month)};
	return "$year-$month-$day $time";
}

# ----------------------------------------------------------------------------

=head2 du_CreationTimeToISO ($string)

	Convert a DAV creation date (2004-01-28T08:18:48Z) to ISO GMT

=cut

# ----------------------------------------------------------------------------
sub du_CreationTimeToISO
{
	my $string = shift;

	$string =~ s/T/ /;
	$string =~ s/Z//;

	return $string;
}

# ----------------------------------------------------------------------------

=head2 du_ISOToSecond ($date)

	Convert an ISO date in second since epoch

=cut

# ----------------------------------------------------------------------------
sub du_ISOToSecond
{
	my ($date) = @_;
	my ($y, $m, $d, $H, $M, $S) = ($date =~ /^(\d{4})-(\d{2})-(\d{2})(?: (\d?\d):(\d{2}):(\d{2}))?/);

	if(!defined $H) {
		($H, $M, $S) = (0,0,0);
	}	

	my $res;

	{ 
		local $ENV{TZ} = 'GMT';
		tzset();
		
		$res = strftime("%s", $S, $M, $H, $d, $m-1, $y-1900);
	}
	tzset();
		
	return $res;
}


# ----------------------------------------------------------------------------

=head2 du_ISOToSecondInUserLocale ($date, $user)

	Convert an ISO date in second since epoch in user locale

=cut

# ----------------------------------------------------------------------------
sub du_ISOToSecondInUserLocale
{
	my ($date, $user) = @_;
	my ($y, $m, $d, $H, $M, $S) = ($date =~ /^(\d{4})-(\d{2})-(\d{2})(?: (\d?\d):(\d{2}):(\d{2}))?/);

	if(!defined $H) {
		($H, $M, $S) = (0,0,0);
	}	

	my $res;

	{ 
		local $ENV{TZ} = $user->GetTimezone();
		tzset();
		
		$res = strftime("%s", $S, $M, $H, $d, $m-1, $y-1900);
	}
	tzset();
		
	return $res;
}



# ----------------------------------------------------------------------------

=head2 du_SecondToISO ($date)

	Convert an date in second since epoch in ISO

=cut

# ----------------------------------------------------------------------------
sub du_SecondToISO
{
	my ($date) = @_;

	my $res;
	{ 
		local $ENV{TZ} = 'GMT';
		tzset();
		
		$res = strftime("%Y-%m-%d %H:%M:%S", gmtime($date));
	}
	tzset();
	
	return $res;
}



# ----------------------------------------------------------------------------

=head2 du_SecondToISOInUserLocale ($date, $user)

	Convert an date in second since epoch in ISO in user locale

=cut

# ----------------------------------------------------------------------------
sub du_SecondToISOInUserLocale
{
	my ($date, $user) = @_;

	my $res;
	{ 
		local $ENV{TZ} = $user->GetTimezone();
		tzset();
		$res = strftime("%Y-%m-%d %H:%M:%S", localtime($date));		
	}
	tzset();

	return $res;
}




# ----------------------------------------------------------------------------

=head2 du_DateInterval ($date1, $date2)

	return the interval in secondes between two ISO date (GMT)

=cut

# ----------------------------------------------------------------------------
sub du_DateInterval
{
	my ($date1, $date2) = @_;

	my $res;
	{ 
		local $ENV{TZ} = 'GMT';
		tzset();
		
		my ($y1, $m1, $d1, $H1, $M1, $S1) = ($date1 =~ /^(\d{4})-(\d{2})-(\d{2}) (\d?\d):(\d{2}):(\d{2})/);
		my $secdate1 = strftime("%s", $S1, $M1, $H1, $d1, $m1-1, $y1-1900);
		
		my ($y2, $m2, $d2, $H2, $M2, $S2) = ($date2 =~ /^(\d{4})-(\d{2})-(\d{2}) (\d?\d):(\d{2}):(\d{2})/);
		my $secdate2 = strftime("%s", $S2, $M2, $H2, $d2, $m2-1, $y2-1900);

		$res = abs($secdate2 - $secdate1)
	}
	tzset();
	
	return $res;
}



# ----------------------------------------------------------------------------

=head2 du_GetDateInUserLocal ($date, $user)

	return the date in list format (like localetime) in given user locale.

=cut

# ----------------------------------------------------------------------------
sub du_GetDateInUserLocale
{
	my ($date, $user) = @_;

	my @locale_date;

	my $timezone = $user->GetTimezone();

	{ 
		local $ENV{TZ} = $timezone;
		tzset();
		@locale_date = localtime($date);
	}
	tzset();

	return @locale_date;
}

# ----------------------------------------------------------------------------

=head2 du_GetDateXMLInUserLocale ($date, $user)

	return the date in XML in given user locale.

=cut

# ----------------------------------------------------------------------------
sub du_GetDateXMLInUserLocale
{
	my ($date, $user) = @_;

	my @locale_date;

	my $timezone = $user->GetTimezone();
	my $xml;

	{ 
		local $ENV{TZ} = $timezone;
		tzset();
		my $localdate = strftime("%s", localtime($date));
		$xml = du_GetDateXML($localdate);
	}
	tzset();

	return $xml;
}


#===============================================================================

=head2 du_GetDateHashInUserLocale ($date, $user)

return the date as Perl hash in given user locale.

=cut

#===============================================================================
sub du_GetDateHashInUserLocale {
	my ($date, $user) = @_;

	my $localdate;

	my $timezone = $user->GetTimezone();
	my $xml;

	{ 
		local $ENV{TZ} = $timezone;
		tzset();
		my @tmp = localtime ($date);
		for my $field (qw/sec min hour day month year wday yday/) {
			$localdate->{$field} = shift (@tmp);
		}
		$localdate->{month} += 1;
		$localdate->{year} += 1900;
	}
	tzset();

	return ($localdate);
}	# ----------  end of subroutine du_GetDateHashInUserLocale ($date, $user)  ----------



# ----------------------------------------------------------------------------

=head2 du_GetMonthDuration ($month, $year)

	return duration of hte given month of the given year

=cut

# ----------------------------------------------------------------------------

sub du_GetMonthDuration
{
   my ($month,$year) = @_;
   return 31 if( $month == 12 );

   my $b = strftime( "%j", 0, 0, 12, 1, $month-1, $year-1900 );
   my $e = strftime( "%j", 0, 0, 12, 1, $month, $year-1900 );

   return ($e-$b);
}




# ----------------------------------------------------------------------------

=head2 du_IsWorkingDayForUser ($config, $isodate, $user)

	return treu if the iso date is working for the given user

=cut

# ----------------------------------------------------------------------------

sub du_IsWorkingDayForUser
{
   my ($config, $date, $user) = @_;

   my $sec = du_ISOToSecond($date);
   my $dnum = strftime("%w", gmtime($sec));

   if(! $user->IsWorkingDay($dnum)) {
	   return 0;
   }

   return (du_IsHoliday($config, $date) ?0:1);
}


# ----------------------------------------------------------------------------

=head2 du_IsWorkingDayForResource ($config, $isodate, $resource)

	return treu if the iso date is working for the given resource

=cut

# ----------------------------------------------------------------------------

sub du_IsWorkingDayForResource
{
   my ($config, $date, $resource) = @_;

   my $sec = du_ISOToSecond($date);
   my $dnum = strftime("%w", gmtime($sec));

   if(! $resource->IsAvailableDay($dnum)) {
	   return 0;
   }

   return (du_IsHoliday($config, $date) ?0:1);
   
}


#----------------------------------------------------------------------------

=head2 du_IsHoliday ($config, $isodate)

	return true if the ISO date is a holiday.

=cut

# ----------------------------------------------------------------------------

my %dateCache;

sub du_IsHoliday
{
	my ($config, $date) = @_;

	my $holidays = $config->GetHolidays();
	my ($y, $m, $d) = ($date =~ /^(\d{4})-(\d{2})-(\d{2})/);
	my $sec = du_ISOToSecond($date);
	
	foreach my $fixed (@{$holidays->{fixed}}) {
		if($fixed->{month} == $m && $fixed->{day} == $d) {
			return 1;
		}
	}
	
	foreach my $moving (@{$holidays->{moving}}) {
		my $base = $moving->{base};
		
		if(! exists $dateCache{$base}->{$y}) {
			my($date) = ParseRecur("*$y:0:0:0:0:0:0*$base");
			$dateCache{$base}->{$y} = UnixDate($date, "%o");
		}
		
		if(($dateCache{$base}->{$y} + ($moving->{offset} * 86400)) == $sec ) {
			return 1;
		}
	}
	
	return 0;
}

#----------------------------------------------------------------------------

=head2 du_FirstRecurDate ($config, $iso_creation_date, $recur)

	return the first dat of recurrence

=cut

# ----------------------------------------------------------------------------
sub du_FirstRecurDate
{
	my ($config, $date, $recur) = @_;

	my ($y, $m, $d) = ($date =~ /^(\d{4})-(\d{2})-(\d{2})/);
	my $base_date = "$y-$m-$d 00:00:00";

	my $end_date;
	
	$recur =~ /^([0-9]+)/;
	$y += $1+1;
	$m++;
	$end_date = "$y-$m-$d 23:59:59";

	warn ("recur : $recur, base_date : $base_date, date : $date, end_date : $end_date") if ($debug > 0);
	my @dates = ParseRecur($recur, $base_date, $base_date, $end_date);

	return UnixDate($dates[0], "%Y-%m-%d %H:%M:%S");
}


# ----------------------------------------------------------------------------

=head2 du_GetNumberOfNotWorkingDayForUserBetween ($config, $date1, $date2, $user)

	return the number of not worked days between two iso dates for a user.

=cut

# ----------------------------------------------------------------------------

sub du_GetNumberOfNotWorkingDayForUserBetween
{
   my ($config, $date1, $date2, $user) = @_;

   ($date1) = ($date1 =~ /^(\d+-\d+-\d+)/);
   ($date2) = ($date2 =~ /^(\d+-\d+-\d+)/);

   my $sec1 = du_ISOToSecond($date1);
   my $sec2 = du_ISOToSecond($date2);

   my $nb_not_worked = 0;

   while($sec1 <= $sec2) {

	   my $dnum = strftime("%w", gmtime($sec1));

	   if(! $user->IsWorkingDay($dnum)) {
		   $nb_not_worked ++;
	   }
	   
	   else {
		   # else check holidays
		   my $holidays = $config->GetHolidays();
		   my ($d, $m, $y) = (gmtime($sec1))[3,4,5];
		   $y += 1900;
		   $m++;

		   foreach my $fixed (@{$holidays->{fixed}}) {
			   if($fixed->{month} == $m && $fixed->{day} == $d) {
				   $nb_not_worked++;
			   }
		   }
   
		   foreach my $moving (@{$holidays->{moving}}) {
			   my $base = $moving->{base};
			   
			   if(! exists $dateCache{$base}->{$y}) {
				   my($date) = ParseRecur("*$y:0:0:0:0:0:0*$base");
				   $dateCache{$base}->{$y} = UnixDate($date, "%o");
			   }
			   
			   if(($dateCache{$base}->{$y} + ($moving->{offset} * 86400)) == $sec1 ) {
				   $nb_not_worked++;
			   }
		   }
	   }

	   $sec1 += 86400;
   }

   return $nb_not_worked;
}


# ----------------------------------------------------------------------------

=head2 du_GetNotWorkingDayForUserBetween ($config, $date1, $date2, $user)

	return the list of not worked days (in second since epoch) 
	between two iso dates for a user.

=cut

# ----------------------------------------------------------------------------

sub du_GetNotWorkingDayForUserBetween
{
   my ($config, $date1, $date2, $user) = @_;

   ($date1) = ($date1 =~ /^(\d+-\d+-\d+)/);
   ($date2) = ($date2 =~ /^(\d+-\d+-\d+)/);

   my $sec1 = du_ISOToSecond($date1);
   my $sec2 = du_ISOToSecond($date2);

   my @res;

   while($sec1 <= $sec2) {

	   my $dnum = strftime("%w", gmtime($sec1));

	   if(! $user->IsWorkingDay($dnum)) {
		   push @res, $sec1;
	   }
	   
	   else {
		   # else check holidays
		   my $holidays = $config->GetHolidays();
		   my ($d, $m, $y) = (gmtime($sec1))[3,4,5];
		   $y += 1900;
		   $m++;

		   foreach my $fixed (@{$holidays->{fixed}}) {
			   if($fixed->{month} == $m && $fixed->{day} == $d) {
				   push @res, $sec1;
			   }
		   }
   
		   foreach my $moving (@{$holidays->{moving}}) {
			   my $base = $moving->{base};
			   
			   if(! exists $dateCache{$base}->{$y}) {
				   my($date) = ParseRecur("*$y:0:0:0:0:0:0*$base");
				   $dateCache{$base}->{$y} = UnixDate($date, "%o");
			   }
			   
			   if(($dateCache{$base}->{$y} + ($moving->{offset} * 86400)) == $sec1 ) {
				   push @res, $sec1;
			   }
		   }
	   }

	   $sec1 += 86400;
   }

   return \@res;
}


# ----------------------------------------------------------------------------

=head2 du_ISOToICal ($date)

	return the ISO date in ICalendar date formats.

=cut

# ----------------------------------------------------------------------------

sub du_ISOToICal
{
	my ($date) = @_;
	
	my $ical = Date::ICal->new( epoch => du_ISOToSecond($date) );
	return $ical->ical(localtime => 1);
}

# ----------------------------------------------------------------------------

=head2 du_ISOLocalToICalGMT ($date, $user)

	return a ISO date in user locale in ICalendar GMT date formats.

=cut

# ----------------------------------------------------------------------------

sub du_ISOLocalToICalGMT
{
	my ($date, $user) = @_;

	$date = du_ConvertLocaleISOToGMT($date, $user);

	my $ical = Date::ICal->new( epoch => du_ISOToSecond($date) );
	return $ical->ical();
}



# ----------------------------------------------------------------------------

=head2 du_SecondToICal ($date)

	return the date in second since epoch in ICalendar date formats.

=cut

# ----------------------------------------------------------------------------

sub du_SecondToICal
{
	my ($date) = @_;
	
	my $ical = Date::ICal->new( epoch => $date);
	return $ical->ical(localtime => 1);
}


# ----------------------------------------------------------------------------

=head2 du_dayNumberToICal ($day)

	return the day of week number (1=monday, 7=sunday)  in ICalendar day format.

=cut

# ----------------------------------------------------------------------------

sub du_dayNumberToICal
{
	my ($date) = @_;
   	
	my @days = qw(MO TU WE TH FR SA SU);

	return $days[$date-1];

}



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
