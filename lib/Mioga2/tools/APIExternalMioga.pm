# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
APIExternalMioga.pm: External Mioga table manipulation

=head1 DESCRIPTION

This module contains functions for creating, deleting and disabling external Mioga.

=head1 METHODS DESRIPTION

=cut

package Mioga2::tools::APIExternalMioga;
use strict;

use vars qw(@ISA @EXPORT);
use Error qw(:try);
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;

@ISA = qw(Exporter);
@EXPORT = qw( 
			  ExtMiogaCreate ExtMiogaDisable ExtMiogaEnable ExtMiogaDelete
            );

my $debug = 0;



# ============================================================================

=head2 ExtMiogaCreate ($config, $values)

	Add a new external mioga into database.
	
	$values is the external mioga description.

    $values = { server => $server_name,
				public_key => $public_key_file_path,
 			  };
	
=cut

# ============================================================================

sub ExtMiogaCreate {
	my ($config, $values) = @_;

	my $dbh = $config->GetDBH();

	try {
		BeginTransaction($dbh);
		
		my $res = SelectSingle($dbh, "SELECT rowid FROM m_external_mioga_status WHERE ident='active'");
		
		$values->{status_id} = $res->{rowid};
		$values->{mioga_id}  = $config->GetMiogaId(),
		$values->{created}   = 'now()';
		$values->{modified}  = 'now()';
		
		my $pubkey = $values->{public_key};
		$values->{public_key} = $dbh->func($pubkey, "lo_import");
		
		my $sql = BuildInsertRequest($values, table => 'm_external_mioga',
									          string => ['server'],
									          other => ['public_key', 'created', 'modified', 'status_id', 'mioga_id']);

		$values->{public_key} = $pubkey;
		
		ExecSQL($dbh, $sql);

		EndTransaction($dbh);
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};
}




# ============================================================================

=head2 ExtMiogaDelete ($dbh, $extmioga_id)

	Mark the external mioga with rowid $extmioga_id as deleted,
	
=cut

# ============================================================================

sub ExtMiogaDelete {
	my ($dbh, $extmioga_id) = @_;

	# ------------------------------------------------------
	# Delete the external mioga.
	# ------------------------------------------------------
	ExecSQL($dbh, "DELETE FROM m_external_mioga WHERE m_external_mioga.rowid = $extmioga_id");

}




# ============================================================================

=head2 ExtMiogaDisable ($dbh, $extmioga_id)

	Mark the external mioga with rowid $extmioga_id as disabled,
	
=cut

# ============================================================================

sub ExtMiogaDisable {
	my ($dbh, $extmioga_id) = @_;

	# ------------------------------------------------------
	# Backup user status
	# ------------------------------------------------------
	ExecSQL($dbh, "UPDATE m_user_base SET   previous_user_status = status_id
		                                  WHERE m_user_base.external_mioga_id=$extmioga_id");


	# ------------------------------------------------------
	# Disable all the user of this mioga
	# ------------------------------------------------------
	ExecSQL($dbh, "UPDATE m_user_base SET status_id = m_user_status.rowid 
		                                  FROM m_user_status WHERE m_user_status.ident='disabled' AND ".
			      "                             m_user_base.external_mioga_id=$extmioga_id");

	# ------------------------------------------------------
	# Disable the external mioga.
	# ------------------------------------------------------
	ExecSQL($dbh, "UPDATE m_external_mioga SET status_id = m_external_mioga_status.rowid 
		                                   FROM m_external_mioga_status WHERE m_external_mioga_status.ident='disabled' AND 
		                                         m_external_mioga.rowid=$extmioga_id");

}




# ============================================================================

=head2 ExtMiogaEnable ($dbh, $extmioga_id)

	Mark the external mioga with rowid $extmioga_id as enabled,
	
=cut

# ============================================================================

sub ExtMiogaEnable {
	my ($dbh, $extmioga_id) = @_;

	# ------------------------------------------------------
	# Enable the external mioga.
	# ------------------------------------------------------
	ExecSQL($dbh, "UPDATE m_external_mioga SET status_id = m_external_mioga_status.rowid 
		                                   FROM m_external_mioga_status WHERE m_external_mioga_status.ident='active' AND 
		                                         m_external_mioga.rowid=$extmioga_id");


	# ------------------------------------------------------
	# Restore the previous status of user
	# ------------------------------------------------------
	ExecSQL($dbh, "UPDATE m_user_base SET  status_id = previous_user_status 
		                                  WHERE m_user_base.external_mioga_id=$extmioga_id");


}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__


