# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
APIProfile.pm: Profiles creation, modification and deletion methods

=head1 DESCRIPTION

This module contains functions for creating modifying and deleting profiles.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
package Mioga2::tools::APIProfile;
use strict;

use Locale::TextDomain::UTF8 'tools_apiprofile';

use vars qw(@ISA @EXPORT);
use Error qw(:try);
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::string_utils;

@ISA = qw(Exporter);
@EXPORT = qw( 
			  ProfileCreate ProfileDelete ProfileCreateFunction ProfileClearFunctions ProfileAddGroup ProfileRemoveGroup ProfileDeleteGroup ProfileGetUser ProfileClearGroups
            );

my $debug = 0;



# ============================================================================

=head2 ProfileCreate ($dbh, $prof_ident, $group_id, $default_file_auth)

	Create a new profile named $prof_ident for group with id $group_id
	and give to this profile the auth $default_file_auth on all groups 
	uris.
	
=cut

# ============================================================================

sub ProfileCreate {
	my ($dbh, $prof_ident, $group_id, $default_file_auth) = @_;

	if(!defined $default_file_auth) {
		$default_file_auth = 0;
	}

	$prof_ident = st_FormatPostgreSQLString($prof_ident);


	my $result = SelectSingle($dbh, "SELECT nextval('m_profile_rowid_seq') AS rowid");
	my $rowid  = $result->{rowid};

	ExecSQL($dbh, "INSERT INTO m_profile (rowid,  created, modified, ident, group_id)
	                              VALUES ($rowid, now(), now(), '$prof_ident', $group_id)");
	

	AuthzDeclareProfileInGroup($dbh, $rowid, $group_id, $default_file_auth);

	return $rowid;
}



# ============================================================================

=head2 ProfileDelete ($dbh, $prof_id)

	Delete the profile  $prof_id.
	
=cut

# ============================================================================

sub ProfileDelete {
	my ($dbh, $prof_id) = @_;

	my $res = SelectSingle($dbh, "SELECT count(*) FROM m_profile_group WHERE profile_id = $prof_id");

	if($res->{count}) {
		throw Mioga2::Exception::Simple("APIProfile", "Profile not empty");
	}

	ProfileClearFunctions($dbh, $prof_id);

	AuthzDeleteAuthzForProfile ($dbh, $prof_id);

	ExecSQL($dbh, "DELETE FROM m_profile  WHERE rowid = $prof_id");
}



# ============================================================================

=head2 ProfileCreateFunction ($dbh, $prof_id, $func_id)

	Authorize the function $func_id for the profile $prof_id
	
=cut

# ============================================================================

sub ProfileCreateFunction {
	my ($dbh, $prof_id, $func_id) = @_;
	
	ExecSQL($dbh, "INSERT INTO m_profile_function (profile_id, function_id)
	                                       VALUES ($prof_id,   $func_id)");
}



# ============================================================================

=head2 ProfileClearFunctions ($dbh, $prof_id)

	Remove all functions of a profile
	
=cut

# ============================================================================

sub ProfileClearFunctions {
	my ($dbh, $prof_id) = @_;
	
	ExecSQL($dbh, "DELETE FROM m_profile_function WHERE profile_id = $prof_id");
}



# ============================================================================

=head2 ProfileClearGroups ($dbh, $prof_id, $def_prof_id)

	Remove all groups of a profile
	
=cut

# ============================================================================

sub ProfileClearGroups {
	my ($dbh, $prof_id, $def_prof_id) = @_;
	
	ExecSQL($dbh, "UPDATE m_profile_group SET profile_id = $def_prof_id WHERE profile_id = $prof_id");
}



# ============================================================================

=head2 ProfileAddGroup ($dbh, $prof_id, $group_id, $group_to_add_id)

	Add the group $group_id into the profile $prof_id
	
=cut

# ============================================================================

sub ProfileAddGroup {
	my ($dbh, $prof_id, $group_id, $group_to_add_id) = @_;
	
	try {
		ProfileRemoveGroup($dbh, $group_id, $group_to_add_id);
	}
	otherwise {
	};

	ExecSQL($dbh, "INSERT INTO m_profile_group (profile_id, group_id)
	                                    VALUES ($prof_id,   $group_to_add_id)");

}


# ============================================================================

=head2 ProfileRemoveGroup ($dbh, $group_id, $group_to_remove_id)

	Remove the group $group_to_remove_id from the profile of group $group_id
	
=cut

# ============================================================================

sub ProfileRemoveGroup {
	my ($dbh, $group_id, $group_to_remove_id) = @_;
	
	my $res = SelectSingle($dbh, "SELECT m_profile.rowid FROM m_profile, m_profile_group ".
						         "WHERE m_profile.group_id=$group_id AND ".
						         "      m_profile_group.group_id = $group_to_remove_id AND ".
						         "      m_profile.rowid = m_profile_group.profile_id");

	if(!defined $res) {
		return;
	}

	ExecSQL($dbh, "DELETE FROM m_profile_group WHERE profile_id=$res->{rowid} AND group_id = $group_to_remove_id");
}


# ============================================================================

=head2 ProfileDeleteGroup ($dbh, $group_id)

	Delete all the profiles of group $group_id.
	
=cut

# ============================================================================

sub ProfileDeleteGroup  {
	my ($dbh, $group_id) = @_;
	
	ExecSQL($dbh, "UPDATE m_group_base SET default_profile_id = NULL WHERE rowid = $group_id ");

	
	my $res = SelectMultiple($dbh, "SELECT * FROM m_profile WHERE group_id = $group_id");

	foreach my $prof (@$res) {
		ProfileDelete($dbh, $prof->{rowid});
	}
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Old::User

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
