#===============================================================================
#
#         FILE:  Convert.pm
#
#  DESCRIPTION:  Data conversion methods
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  29/07/2010 16:35
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Convert.pm: Data conversion methods.

=head1 DESCRIPTION

This module provides several data conversion methods.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::tools::Convert;

use Data::Dumper;

use Encode;
use JSON::XS;

use Mioga2::tools::string_utils;

my $debug = 0;


#===============================================================================

=head2 PerlToXML

Converts Perl data to XML.

=head3 Sample input

	my $data = {
		"user" => [
			{
				rowid => 1,
				ident => 'toto',
				active => {
					when => {
						'$a' => 'always'
					}
				}
			},
			{
				rowid => 2,
				ident => 'titi'
			},
		],
		data => 'Some data',
		complex => {
			type => {
				'$a' => 'info'
			},
			'' => 'toto'
		},
		node => {
			type => {
				'$a' => 'info'
			},
			blup => {
				'$a' => 'blop'
			},
			subnode => {
				key1 => 'val1',
				key2 => 'val2'
			}
		}
	};

=head3 Sample output

	<user>
		<active when="always"/>
		<ident>toto</ident>
		<rowid>1</rowid>
	</user>
	<user>
		<ident>titi</ident>
		<rowid>2</rowid>
	</user>
	<data>Some data</data>
	<complex type="info">toto</complex>
	<node type="info" blup="blop">
		<subnode>
			<key2>val2</key2>
			<key1>val1</key1>
		</subnode>
	</node>

=cut

#===============================================================================
sub PerlToXML {
	my ($data, $key) = @_;

	my $xml = '';
	my @content = ( );
	my @attributes = ( );

	for (ref ($data)) {
		if (/HASH/) {
			my $tmp = '';
			for (keys (%$data)) {
				if ((ref ($data->{$_}) eq 'HASH') && (exists ($data->{$_}->{'$a'}))) {
					push (@attributes, "$_=\"" . st_FormatXMLString ($data->{$_}->{'$a'}) . "\"");
				}
				else {
					if ($_ eq '') {
						$tmp .= st_FormatXMLString ($data->{$_});
					}
					else {
						$tmp .= PerlToXML ($data->{$_}, $_);
					}
				}
			}
			push (@content, $tmp);
		}
		elsif (/ARRAY/) {
			for (@$data) {
				push (@content, PerlToXML ($_, $key));
			}
			$key = '';
		}
		elsif (defined ($data)) {
			if ($data =~ /^(boolean::true|boolean::false)$/) {
				$data =~ s/boolean:://;
			}
			push (@content, st_FormatXMLString ($data));
		}
		else {
			# $data is undefined, ignore
		}
	}

	my $attributes = '';
	if (scalar (@attributes)) {
		$attributes = ' ' . join (' ', @attributes);
	}

	my $content = '';
	$key = st_FormatXMLString ($key) if ($key);

	# Check key for consistency (http://www.w3.org/TR/REC-xml/#NT-Name)
	if ($key && ($key !~ /^[:a-zA-Z_\x{c0}-\x{d6}\x{d8}-\x{f6}\x{f8}-\x{2ff}\x{370}-\x{37d}\x{37f}-\x{1fff}\x{200c}-\x{200d}\x{2070}-\x{218f}\x{2c00}-\x{2fef}\x{3001}-\x{d7ff}\x{f900}-\x{fdcf}\x{fdf0}-\x{fffd}\x{10000}-\x{effff}\-.0-9\x{b7}\x{300}-\x{36f}\x{203f}-\x{2040}]*$/)) {
		throw Mioga2::Exception::Simple ('Mioga2::tools::Convert::PerlToXML', "Invalid XML tag: $key");
	}

	if (scalar (@content) && defined ($content[0])) {
		my $open = $key ? "<$key$attributes>" : '';
		my $close = $key ? "</$key>" : '';
		$content = "$open" . join ("$close$open", @content) . "$close";
	}
	elsif (scalar (@content) && !defined ($content[0])) {
		$content = "<$key$attributes/>";
	}

	return ($content);
}	# ----------  end of subroutine PerlToXML  ----------


#===============================================================================

=head2 PerlToJSON

Convert a Perl structure to JSON

=head3 Sample input

	my $data = {
		"user" => [
			{
				rowid => 1,
				ident => 'toto',
				active => 1,
			},
			{
				rowid => 2,
				ident => 'titi'
			},
		],
		data => 'Some data',
		node => {
			subnode => {
				key1 => 'val1',
				key2 => 'val2'
			}
		}
	};

=head3 Sample output

	{
		"user":[
				{
					"rowid":1,
					"ident":"toto",
					"active":1
				},
				{
					"rowid":2,
					"ident":"titi"
				}
			],
		"data":"Some data",
		"node":{
			"subnode": {
				"key1":"val1",
				"key2":"val2"
			}
		}
	}

=cut

#===============================================================================
sub PerlToJSON {
	my ($data, $key) = @_;

	my @content = ();

	my $ref = ref ($data);
	if ($ref eq 'HASH') {
		# Data is a hash, process keys
		my @tmp;
		for my $key (keys (%$data)) {
			push (@tmp, PerlToJSON ($data->{$key}, $key));
		}
		push (@content, '{' . join (',', @tmp) . '}');
	}
	elsif ($ref eq 'ARRAY') {
		# Data is an array, process each entry
		my @tmp;
		for my $entry (@$data) {
			push (@tmp, PerlToJSON ($entry));
		}
		push (@content, '[' . join (',', @tmp) . ']');
	}
	else {
		$data = 'javascript::null' unless (defined ($data));
		if ($data =~ /^(boolean::true|boolean::false)$/) {
			$data =~ s/boolean:://;
		}
		elsif ($data =~ /^javascript::null$/) {
			$data =~ s/javascript:://;
		}
		elsif (($data !~ /^[0-9]$/) && ($data !~ /^[1-9][0-9]*$/) && ($data !~ /^(true|false|null)$/)) { # TODO ( - 02/01/2013 18:05): Bug #1333, remove true|false|null test when all WebServices are clean
			$data = "\"" . st_FormatJSONString ($data) . "\"" ;
		}
		push (@content, $data);
	}

	my $content;
	$content = "\"$key\":" if ($key);
	$content .= join (',', @content);

	return ($content);
}	# ----------  end of subroutine PerlToJSON  ----------


#===============================================================================

=head2 JSONToPerl

Convert a JSON string to a Perl data structure

=head3 Incoming Arguments

=over

=item I<$json>: A stringified-JSON structure

=back

=head3 Return value

=over

A Perl data structure matching stringified JSON input.

=back

=cut

#===============================================================================
sub JSONToPerl {
	my ($json) = @_;

	my $data = JSON::XS::decode_json (encode('UTF-8', $json));

	return ($data);
}	# ----------  end of subroutine JSONToPerl  ----------

1;

