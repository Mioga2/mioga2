# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
APIAuthz.pm: Manage URIs and ACLs.

=head1 DESCRIPTION

This module contains functions for creating/deleting URIs and managing ACLs.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::tools::APIAuthz;
use strict;

use utf8;
use Locale::TextDomain::UTF8 'tools_apiauthz';

use Exporter;
use Data::Dumper;
use Error qw(:try);
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::Constants;
use vars qw( @ISA @EXPORT );


# ============================================================================

=head2 Application Right return values

	AUTHZ_OK         => Authorized
	AUTHZ_FORBIDDEN  => Forbidden
	AUTHZ_DISCONNECT => Disconnect current user.
	
=cut

# ============================================================================

use constant AUTHZ_OK    => 1;
use constant AUTHZ_FORBIDDEN => 0;
use constant AUTHZ_DISCONNECT => -1;

@ISA = qw(Exporter);

@EXPORT = qw(
			 AUTHZ_OK AUTHZ_FORBIDDEN AUTHZ_DISCONNECT
			 AuthzAddURI AuthzDeclareURI AuthzCopyURI AuthzRenameURI AuthzDeleteURI AuthzDisableURIInheritance AuthzEnableURIInheritance
			 AuthzDeclareProfileInGroup AuthzAddAuthzForProfile AuthzAddAuthzForUser AuthzAddAuthzForTeam
			 AuthzDuplicateParentAuthz
			 AuthzUpdateAuthzForProfile AuthzUpdateAuthzForUser AuthzUpdateAuthzForTeam
			 AuthzDeleteAuthzForProfile AuthzDeleteAuthzForUser AuthzDeleteAuthzForTeam
			 AuthzTestAccessForURI AuthzTestRightAccessForURI AuthzTestAccessForRowids AuthzTestAccessForRequest
			 AuthzUpdateDirRights
			 AuthzListAccessibleURIs

			 ProfileTestAuthzFunction ProfileTestAuthzMethod ProfileGetUsersForAuthzFunction
			 );


my $debug = 0;

# ============================================================================

=head2 AuthzAddURI ($dbh, $uri, $group_id, $parent_uri_id, $user_id)

	Add a new URI in the database for the group $group_id with 
	$parent_uri_id as parent.

	$parent_uri_id is not required.
	
=cut

# ============================================================================

sub AuthzAddURI {
	my ($dbh, $uri, $group_id, $parent_uri_id, $user_id, $mime, $size) = @_;

	my $result = SelectSingle($dbh, "SELECT nextval('m_uri_rowid_seq') as rowid");
	my $rowid  = $result->{rowid};
	
	$parent_uri_id = $rowid unless defined $parent_uri_id;

	my ($directory) = ($uri =~ m/^(.*)\/[^\/]*$/);

	my $rc;
	if ($parent_uri_id != $rowid) {
		$rc = ExecSQL($dbh, "INSERT INTO m_uri (rowid,  uri,  group_id, parent_uri_id, stop_inheritance, modified, user_id, mimetype, size, directory_id, history)
	               VALUES ($rowid, '".st_FormatPostgreSQLString($uri)."', $group_id, $parent_uri_id, FALSE, NOW(), $user_id, '".st_FormatPostgreSQLString($mime)."', $size, (SELECT rowid FROM m_uri WHERE uri = '" . st_FormatPostgreSQLString($directory) . "'), (SELECT history FROM m_uri WHERE uri = '" . st_FormatPostgreSQLString ($directory) . "'))");
	}
	else {
		$rc = ExecSQL($dbh, "INSERT INTO m_uri (rowid,  uri,  group_id, parent_uri_id, stop_inheritance, modified, user_id, mimetype, size, directory_id, history)
	               VALUES ($rowid, '".st_FormatPostgreSQLString($uri)."', $group_id, $parent_uri_id, FALSE, NOW(), $user_id, '".st_FormatPostgreSQLString($mime)."', $size, (SELECT rowid FROM m_uri WHERE uri = '" . st_FormatPostgreSQLString($directory) . "'), 'f')");
	}
	if ($rc eq '0E0') {
		# This should not happen, if INSERT fails, the exception will come from DBI, anyway, it is better to have this check done here
		throw Mioga2::Exception::DB ('Mioga2::tools::APIAuthz::AuthzAddURI', "Inserting URI $uri failed, no records matched by SQL INSERT.");
	}
	
	return $rowid;
}



# ============================================================================

=head2 AuthzDeclareURI ($dbh, $uri, $user_id)

	Add a new URI in the database and update uri authorization cache.
	
=cut

# ============================================================================

sub AuthzDeclareURI {
	my ($dbh, $uri, $user_id, $mime, $size) = @_;

	$uri =~ s/\/$//;
	$uri =~ s/\/\//\//g;

	my $me = SelectSingle($dbh, "SELECT * FROM m_uri ".
							    "WHERE uri = '".st_FormatPostgreSQLString($uri)."'");

	if(defined $me) {
	  ExecSQL($dbh, "UPDATE m_uri SET modified = NOW(), mimetype = '".
	  st_FormatPostgreSQLString($mime)."', user_id=$user_id, size=$size WHERE uri = '".st_FormatPostgreSQLString($uri)."'");
		return 0;
	}

  my @uri_parts = split '/', $uri;
  pop @uri_parts;
  my @uris;
  foreach my $part (@uri_parts) {
    next unless $part;
	if (defined($uris[-1])) {
		print STDERR "AuthzDeclareURI uris[-1] = $uris[-1]  part = $part\n" if ($debug);
		push @uris, $uris[-1]."/$part";
	}
	else {
		push @uris, "/$part";
	}
  }
  @uris = map {st_FormatPostgreSQLString($_)} @uris;
  
	my $parent = SelectSingle($dbh, "SELECT * FROM m_uri ".
                      "WHERE (uri = '".join("' OR uri = '",@uris)."') AND rowid = parent_uri_id ".
                      "ORDER BY length(uri) DESC LIMIT 1");

	if(!defined $parent) {
		throw Mioga2::Exception::Simple("Mioga2::tools::APIAuthz", __x("No parent uri found for {uri}", uri => $uri));
	}

	my $uri_id = AuthzAddURI($dbh, $uri, $parent->{group_id}, $parent->{rowid}, $user_id, $mime, $size);
  
	return $uri_id;
}




# ============================================================================

=head2 AuthzCopyURI ($dbh, $src, $dest, $user_id)

	Execute a COPY command in the database.
	
=cut

# ============================================================================

sub AuthzCopyURI {
	my ($dbh, $src, $dest, $user_id, $mime, $size) = @_;

	$dest =~ s/\/$//;
	$dest =~ s/\/\//\//g;

	$src =~ s/\/$//;
	$src =~ s/\/\//\//g;

	my $retval = AuthzDeclareURI($dbh, $dest, $user_id, $mime, $size);


	my $uris = SelectMultiple($dbh, "SELECT * FROM m_uri ".
							        "WHERE uri ~ '^".st_FormatPostgreSQLRegExpString($src)."/'");
	
	foreach my $uri (@$uris) {
		my $newuri = $uri->{uri};
		$newuri =~ s/^$src/$dest/;
		AuthzDeclareURI($dbh, $newuri, $user_id, $uri->{mimetype}, $uri->{size});
	}
	
	return $retval;
}


# ============================================================================

=head2 AuthzRenameURI ($dbh, $src, $dest)

	Execute a COPY command in the database.
	
=cut

# ============================================================================

sub AuthzRenameURI {
	my ($dbh, $src, $dest) = @_;
	warn ("AuthzRenameURI src = $src  dest = $dest\n") if ($debug);

	$dest =~ s/\/$//;
	$dest =~ s/\/\//\//g;
	#$dest =~ /([^\/]*$)/;
	#my $newstring = $1;

	$src =~ s/\/$//;
	$src =~ s/\/\//\//g;
	#$src =~ /(.*\/)([^\/]*$)/;
	#my $prefix = $1;
	#my $oldstring = $2;
	warn ("AuthzRenameURI dest = $dest  src = $src\n") if ($debug);
	my $pos = 1;
	my $length = "char_length('" . st_FormatPostgreSQLString($src) . "')";
	
	my @uri_parts = split '/', $dest;
  pop @uri_parts;
  my @uris;
  foreach my $part (@uri_parts) {
    next unless $part;
    push @uris, $uris[-1]."/$part";
  }
  @uris = map {st_FormatPostgreSQLString($_)} @uris;
  
  my $parent = SelectSingle($dbh, "SELECT * FROM m_uri ".
                      "WHERE (uri = '".join("' OR uri = '",@uris)."') AND rowid = parent_uri_id ".
                      "ORDER BY length(uri) DESC LIMIT 1");

  if(!defined $parent) {
    throw Mioga2::Exception::Simple("Mioga2::tools::APIAuthz", __x("No parent uri found for {uri}", uri => $dest));
  }

  ExecSQL($dbh, "DELETE FROM m_uri WHERE  uri = '" . st_FormatPostgreSQLString($dest) . "'");
	my $sql = "update m_uri set modified=now(),uri = overlay(uri placing '" . st_FormatPostgreSQLString($dest) . "' from $pos for $length) where uri ~ '" . st_FormatPostgreSQLRegExpString($src) . "(/.*)?\$'";

	warn ("AuthzRenameURI sql = $sql\n") if ($debug);
	if (ExecSQL($dbh, $sql) eq '0E0') {
		throw Mioga2::Exception::DB ('Mioga2::tools::APIAuthz::AuthzRenameURI', "Renaming URI $src to $dest failed, no records matched by SQL UPDATE.");
	}

	my ($directory) = ($dest =~ m/^(.*)\/[^\/]*$/);
	ExecSQL ($dbh, 'UPDATE m_uri SET directory_id = (SELECT rowid FROM m_uri WHERE uri = ?) WHERE uri = ?', [$directory, $dest]);
	
	ExecSQL($dbh, "UPDATE m_uri SET parent_uri_id = $parent->{rowid}
	               WHERE uri ~ '" . st_FormatPostgreSQLRegExpString ($dest) . "(/.*)?\$'
	                 AND rowid <> parent_uri_id");

	return 1;
}
# ============================================================================

=head2 AuthzDeleteURI ($dbh, $uri)

	Remove an uri from database.
	
=cut

# ============================================================================

sub AuthzDeleteURI {
	my ($dbh, $uri) = @_;

    ExecSQL($dbh, "DELETE FROM file_comment USING m_uri WHERE m_uri.uri ~ '^".st_FormatPostgreSQLRegExpString($uri)."(\$|/)' AND file_comment.uri_id=m_uri.rowid");
	ExecSQL($dbh, "DELETE FROM m_authorize_profile ".
                  "USING m_authorize, m_uri " .
			      "WHERE  m_authorize_profile.authz_id = m_authorize.rowid AND ".
			      "       m_authorize.uri_id = m_uri.rowid AND ".
			      "       m_uri.uri ~ '^".st_FormatPostgreSQLRegExpString($uri)."(\$|/)'");

	ExecSQL($dbh, "DELETE FROM m_authorize_user ".
                  "USING m_authorize, m_uri " . 
			      "WHERE  m_authorize_user.authz_id = m_authorize.rowid AND ".
			      "       m_authorize.uri_id = m_uri.rowid AND ".
			      "       m_uri.uri ~ '^".st_FormatPostgreSQLRegExpString($uri)."(\$|/)'");

	ExecSQL($dbh, "DELETE FROM m_authorize_team ".
                  "USING m_authorize, m_uri " .
			      "WHERE  m_authorize_team.authz_id = m_authorize.rowid AND ".
			      "       m_authorize.uri_id = m_uri.rowid AND ".
			      "       m_uri.uri ~ '^".st_FormatPostgreSQLRegExpString($uri)."(\$|/)'");

	ExecSQL($dbh, "DELETE FROM m_authorize ".
                  "USING m_uri " .
			      "WHERE m_authorize.uri_id = m_uri.rowid AND ".
			      "      m_uri.uri ~ '^".st_FormatPostgreSQLRegExpString($uri)."(\$|/)'");

	my $regex = "^" . st_FormatPostgreSQLRegExpString($uri);
	ExecSQL($dbh, "DELETE FROM uri_data USING m_uri WHERE uri_data.uri_id = m_uri.rowid AND m_uri.uri ~ ?", [$regex]);
	ExecSQL($dbh, "DELETE FROM notice_files USING m_uri WHERE notice_files.uri_id = m_uri.rowid AND m_uri.uri ~ ?", [$regex]);

	my $rc = ExecSQL($dbh, "DELETE FROM m_uri WHERE uri ~ '^".st_FormatPostgreSQLRegExpString($uri)."(\$|/)'");
	if ($rc eq '0E0') {
		throw Mioga2::Exception::DB ('Mioga2::tools::APIAuthz::AuthzDeleteURI', "Deleting URI $uri failed, no records matched by SQL DELETE.");
	}
}



# ============================================================================

=head2 AuthzDisableURIInheritance ($dbh, $uri_id)

	Disable inheritance for the given URI
	
=cut

# ============================================================================

sub AuthzDisableURIInheritance {
	my ($dbh, $uri_id) = @_;

	ExecSQL($dbh, "UPDATE m_uri SET stop_inheritance = TRUE WHERE rowid = $uri_id"); 
} 


# ============================================================================

=head2 AuthzEnableURIInheritance ($dbh, $uri_id)

	Enable inheritance for the given URI
	
=cut

# ============================================================================

sub AuthzEnableURIInheritance {
	my ($dbh, $uri_id) = @_;

	ExecSQL($dbh, "UPDATE m_uri SET stop_inheritance = FALSE WHERE rowid = $uri_id"); 
}



# ============================================================================

=head2 AuthzDeclareProfileInGroup ($dbh, $profile_id, $group_id, $authz)

	Declare a new profile into authorization cache.
	$authz is the default authorization.

=cut

# ============================================================================

sub AuthzDeclareProfileInGroup {
	my ($dbh, $profile_id, $group_id, $authz) = @_;

	my $uris = SelectMultiple($dbh, "SELECT * FROM m_uri ".
							        "WHERE group_id = $group_id AND ".
							        "      parent_uri_id = rowid ORDER BY length(uri)");
	foreach my $uri (@$uris) {
		my $newauthz = AuthzGetAuthorizeSchema($dbh, $uri->{rowid}, $authz);
		ExecSQL($dbh, "INSERT INTO m_authorize_profile (authz_id, profile_id) VALUES ($newauthz, $profile_id)");
	}
}


# ============================================================================

=head2 AuthzAddAuthzForProfile ($dbh, $uri_id, $profile_id, $authz)

	Add a new authorization to profile $profile_id on uri $uri_id.
	
=cut

# ============================================================================

sub AuthzAddAuthzForProfile {
	my ($dbh, $uri_id, $profile_id, $authz) = @_;
	
	my $uri = SelectSingle($dbh, "SELECT * FROM m_uri ".
						         "WHERE  parent_uri_id = rowid AND ".
						         "       rowid = $uri_id ".
							     "ORDER BY length(uri)");

	if(defined $uri) {

		AuthzAddAuthzForProfileOnURI($dbh, $uri_id, $profile_id, $authz);
	
		my $children = SelectMultiple($dbh, "SELECT * FROM m_uri ".
									        "WHERE  rowid != $uri->{rowid} AND ".
									        "       uri ~ '^".st_FormatPostgreSQLRegExpString($uri->{uri})."(/)' AND ".
									        "       parent_uri_id = rowid AND ".
							                "       stop_inheritance IS FALSE ".
							                "ORDER BY length(uri)");

		foreach my $child (@$children) {
			AuthzAddAuthzForProfileOnURI($dbh, $child->{rowid}, $profile_id, $authz);
		}
	}
}


sub AuthzAddAuthzForProfileOnURI {
	my ($dbh, $uri_id, $profile_id, $authz) = @_;

	my $auth = SelectSingle($dbh, "SELECT m_authorize_profile.* ".
								  "FROM   m_authorize_profile, m_authorize ".
								  "WHERE  m_authorize.uri_id = $uri_id AND ".
								  "       m_authorize_profile.authz_id = m_authorize.rowid AND ".
								  "       m_authorize_profile.profile_id = $profile_id");
	
	if(!defined $auth) {
		
		my $rowid = AuthzGetAuthorizeSchema($dbh, $uri_id, $authz);		
		
		ExecSQL($dbh, "INSERT INTO m_authorize_profile (authz_id, profile_id) ".
				      "                         VALUES ($rowid,   $profile_id)");
	}
}


# ============================================================================

=head2 AuthzAddAuthzForUser ($dbh, $uri_id, $user_id, $authz)
	
	Add a new authorization to user $user_id on uri $uri_id.
	
=cut

# ============================================================================
	
sub AuthzAddAuthzForUser {
	my ($dbh, $uri_id, $user_id, $authz) = @_;
	
	my $uri = SelectSingle($dbh, "SELECT * FROM m_uri ".
						         "WHERE  parent_uri_id = rowid AND ".
						         "       rowid = $uri_id ".
							     "ORDER BY length(uri)");

	if(defined $uri) {

		AuthzAddAuthzForUserOnURI($dbh, $uri_id, $user_id, $authz);
	
		my $children = SelectMultiple($dbh, "SELECT * FROM m_uri ".
									        "WHERE  rowid != $uri->{rowid} AND ".
									        "       uri ~ '^".st_FormatPostgreSQLRegExpString($uri->{uri})."(/)' AND ".
									        "       parent_uri_id = rowid AND ".
							                "       stop_inheritance IS FALSE ".
							                "ORDER BY length(uri)");

		foreach my $child (@$children) {
			AuthzAddAuthzForUserOnURI($dbh, $child->{rowid}, $user_id, $authz);
		}
	}
}


sub AuthzAddAuthzForUserOnURI {
	my ($dbh, $uri_id, $user_id, $authz) = @_;

	my $auth = SelectSingle($dbh, "SELECT m_authorize_user.* ".
								  "FROM   m_authorize_user, m_authorize ".
								  "WHERE  m_authorize.uri_id = $uri_id AND ".
								  "       m_authorize_user.authz_id = m_authorize.rowid AND ".
								  "       m_authorize_user.user_id = $user_id");
	
	if(!defined $auth) {
		
		my $rowid = AuthzGetAuthorizeSchema($dbh, $uri_id, $authz);		
		
		ExecSQL($dbh, "INSERT INTO m_authorize_user (authz_id, user_id) ".
				      "                      VALUES ($rowid,   $user_id)");
	}
}


# ============================================================================

=head2 AuthzAddAuthzForTeam ($dbh, $uri_id, $team_id, $authz)

	Add a new authorization to team $team_id on uri $uri_id.
	
=cut

# ============================================================================

sub AuthzAddAuthzForTeam {
	my ($dbh, $uri_id, $team_id, $authz) = @_;
	
	my $uri = SelectSingle($dbh, "SELECT * FROM m_uri ".
						         "WHERE  parent_uri_id = rowid AND ".
						         "       rowid = $uri_id ".
							     "ORDER BY length(uri)");

	if(defined $uri) {

		AuthzAddAuthzForTeamOnURI($dbh, $uri_id, $team_id, $authz);
	
		my $children = SelectMultiple($dbh, "SELECT * FROM m_uri ".
									        "WHERE  rowid != $uri->{rowid} AND ".
									        "       uri ~ '^".st_FormatPostgreSQLRegExpString($uri->{uri})."(/)' AND ".
									        "       parent_uri_id = rowid AND ".
							                "       stop_inheritance IS FALSE ".
							                "ORDER BY length(uri)");

		foreach my $child (@$children) {
			AuthzAddAuthzForTeamOnURI($dbh, $child->{rowid}, $team_id, $authz);
		}
	}
}


sub AuthzAddAuthzForTeamOnURI {
	my ($dbh, $uri_id, $team_id, $authz) = @_;

	my $auth = SelectSingle($dbh, "SELECT m_authorize_team.* ".
								  "FROM   m_authorize_team, m_authorize ".
								  "WHERE  m_authorize.uri_id = $uri_id AND ".
								  "       m_authorize_team.authz_id = m_authorize.rowid AND ".
								  "       m_authorize_team.team_id = $team_id");
	
	if(!defined $auth) {
		
		my $rowid = AuthzGetAuthorizeSchema($dbh, $uri_id, $authz);		
		
		ExecSQL($dbh, "INSERT INTO m_authorize_team (authz_id, team_id) ".
				      "                      VALUES ($rowid,   $team_id)");
	}
}


# ============================================================================

=head2 AuthzUpdateAuthzForProfile ($dbh, $uri_id, $profile_id, $authz)

	Update authorization for profile $profile_id on uri $uri_id.
	
=cut

# ============================================================================

sub AuthzUpdateAuthzForProfile {
	my ($dbh, $uri_id, $profile_id, $authz) = @_;

	my $uri = SelectSingle($dbh, "SELECT m_authorize_profile.profile_id, m_authorize.access, m_uri.rowid, m_uri.uri ".
						         "FROM   m_authorize_profile, m_authorize, m_uri ".
								 "WHERE  m_uri.rowid = $uri_id AND ".
						         "       m_uri.parent_uri_id = m_uri.rowid AND ".
						         "       m_authorize.uri_id = m_uri.rowid AND ".
								 "       m_authorize_profile.authz_id = m_authorize.rowid AND ".
								 "       m_authorize_profile.profile_id = $profile_id");

	
	if(defined $uri) {
		AuthzUpdateAuthzForProfileOnURI($dbh, $uri_id, $profile_id, $authz);

		my $children = SelectMultiple($dbh, "SELECT m_authorize_profile.profile_id, m_authorize.access, m_uri.rowid, m_uri.uri ".
									        "FROM   m_authorize_profile, m_authorize, m_uri ".
									        "WHERE  m_uri.rowid != $uri->{rowid} AND ".
									        "       m_uri.uri ~ '^".st_FormatPostgreSQLRegExpString($uri->{uri})."/' AND ".
								            "       m_uri.parent_uri_id = m_uri.rowid AND ".
								            "       m_uri.stop_inheritance IS FALSE AND ".
								            "       m_authorize.uri_id = m_uri.rowid AND ".
								            "       m_authorize.access = $uri->{access} AND ".
								            "       m_authorize_profile.authz_id = m_authorize.rowid AND ".
								            "       m_authorize_profile.profile_id = $profile_id ".
							                "ORDER BY length(uri)");
		
		foreach my $child (@$children) {
			AuthzUpdateAuthzForProfileOnURI($dbh, $child->{rowid}, $profile_id, $authz);
		}
	}
}

sub AuthzUpdateAuthzForProfileOnURI {
	my ($dbh, $uri_id, $profile_id, $authz) = @_;

	my $old_authz_id = AuthzGetAuthorizeSchemaForProfile($dbh, $uri_id, $profile_id);
	my $rowid        = AuthzGetAuthorizeSchema($dbh, $uri_id, $authz);
	
	ExecSQL($dbh, "UPDATE m_authorize_profile ".
			      "SET    authz_id = $rowid ".
			      "WHERE  profile_id = $profile_id AND ".
				  "       authz_id = $old_authz_id");
	
	AuthzCleanAuthorizeSchema($dbh, $old_authz_id);
}

# ============================================================================

=head2 AuthzUpdateAuthzForUser ($dbh, $uri_id, $user_id, $authz)

	Update authorization for user $user_id on uri $uri_id.
	
=cut

# ============================================================================

sub AuthzUpdateAuthzForUser {
	my ($dbh, $uri_id, $user_id, $authz) = @_;

	my $uri = SelectSingle($dbh, "SELECT m_authorize_user.user_id, m_authorize.access, m_uri.rowid, m_uri.uri ".
						         "FROM   m_authorize_user, m_authorize, m_uri ".
								 "WHERE  m_uri.rowid = $uri_id AND ".
						         "       m_uri.parent_uri_id = m_uri.rowid AND ".
						         "       m_authorize.uri_id = m_uri.rowid AND ".
								 "       m_authorize_user.authz_id = m_authorize.rowid AND ".
								 "       m_authorize_user.user_id = $user_id");

	
	if(defined $uri) {
		AuthzUpdateAuthzForUserOnURI($dbh, $uri_id, $user_id, $authz);

		my $children = SelectMultiple($dbh, "SELECT m_authorize_user.user_id, m_authorize.access, m_uri.rowid, m_uri.uri ".
									        "FROM   m_authorize_user, m_authorize, m_uri ".
									        "WHERE  m_uri.rowid != $uri->{rowid} AND ".
									        "       m_uri.uri ~ '^".st_FormatPostgreSQLRegExpString($uri->{uri})."/' AND ".
								            "       m_uri.parent_uri_id = m_uri.rowid AND ".
								            "       m_uri.stop_inheritance IS FALSE AND ".
								            "       m_authorize.uri_id = m_uri.rowid AND ".
								            "       m_authorize.access = $uri->{access} AND ".
								            "       m_authorize_user.authz_id = m_authorize.rowid AND ".
								            "       m_authorize_user.user_id = $user_id ".
							                "ORDER BY length(uri)");
		
		foreach my $child (@$children) {
			AuthzUpdateAuthzForUserOnURI($dbh, $child->{rowid}, $user_id, $authz);
		}
	}
}

sub AuthzUpdateAuthzForUserOnURI {
	my ($dbh, $uri_id, $user_id, $authz) = @_;

	my $old_authz_id = AuthzGetAuthorizeSchemaForUser($dbh, $uri_id, $user_id);
	my $rowid        = AuthzGetAuthorizeSchema($dbh, $uri_id, $authz);
	
	ExecSQL($dbh, "UPDATE m_authorize_user ".
			      "SET    authz_id = $rowid ".
			      "WHERE  user_id = $user_id AND ".
				  "       authz_id = $old_authz_id");
	
	AuthzCleanAuthorizeSchema($dbh, $old_authz_id);
}


# ============================================================================

=head2 AuthzUpdateAuthzForTeam ($dbh, $uri_id, $team_id, $authz)

	Update authorization for team $team_id on uri $uri_id.
	
=cut

# ============================================================================

sub AuthzUpdateAuthzForTeam {
	my ($dbh, $uri_id, $team_id, $authz) = @_;

	my $uri = SelectSingle($dbh, "SELECT m_authorize_team.team_id, m_authorize.access, m_uri.rowid, m_uri.uri ".
						         "FROM   m_authorize_team, m_authorize, m_uri ".
								 "WHERE  m_uri.rowid = $uri_id AND ".
						         "       m_uri.parent_uri_id = m_uri.rowid AND ".
						         "       m_authorize.uri_id = m_uri.rowid AND ".
								 "       m_authorize_team.authz_id = m_authorize.rowid AND ".
								 "       m_authorize_team.team_id = $team_id");

	
	if(defined $uri) {
		AuthzUpdateAuthzForTeamOnURI($dbh, $uri_id, $team_id, $authz);

		my $children = SelectMultiple($dbh, "SELECT m_authorize_team.team_id, m_authorize.access, m_uri.rowid, m_uri.uri ".
									        "FROM   m_authorize_team, m_authorize, m_uri ".
									        "WHERE  m_uri.rowid != $uri->{rowid} AND ".
									        "       m_uri.uri ~ '^".st_FormatPostgreSQLRegExpString($uri->{uri})."/' AND ".
								            "       m_uri.parent_uri_id = m_uri.rowid AND ".
								            "       m_uri.stop_inheritance IS FALSE AND ".
								            "       m_authorize.uri_id = m_uri.rowid AND ".
								            "       m_authorize.access = $uri->{access} AND ".
								            "       m_authorize_team.authz_id = m_authorize.rowid AND ".
								            "       m_authorize_team.team_id = $team_id ".
							                "ORDER BY length(uri)");
		
		foreach my $child (@$children) {
			AuthzUpdateAuthzForTeamOnURI($dbh, $child->{rowid}, $team_id, $authz);
		}
	}
}

sub AuthzUpdateAuthzForTeamOnURI {
	my ($dbh, $uri_id, $team_id, $authz) = @_;

	my $old_authz_id = AuthzGetAuthorizeSchemaForTeam($dbh, $uri_id, $team_id);
	my $rowid        = AuthzGetAuthorizeSchema($dbh, $uri_id, $authz);
	
	ExecSQL($dbh, "UPDATE m_authorize_team ".
			      "SET    authz_id = $rowid ".
			      "WHERE  team_id = $team_id AND ".
				  "       authz_id = $old_authz_id");
	
	AuthzCleanAuthorizeSchema($dbh, $old_authz_id);
}


# ============================================================================

=head2 AuthzDeleteAuthzForProfile ($dbh, $profile_id)

	Delete authz entry for the given profile
	
=cut

# ============================================================================

sub AuthzDeleteAuthzForProfile {
	my ($dbh, $profile_id) = @_;

	my $authz = SelectMultiple($dbh, "SELECT * FROM m_authorize_profile ".
							         "WHERE profile_id = $profile_id ");
	foreach my $auth (@$authz) {
		ExecSQL($dbh, "DELETE FROM m_authorize_profile WHERE authz_id = $auth->{authz_id} AND profile_id = $profile_id");
		AuthzCleanAuthorizeSchema($dbh, $auth->{authz_id});
	}
}



# ============================================================================

=head2 AuthzDeleteAuthzForUser ($dbh, $uri_id, $user_id)

	Delete authz entry
	
=cut

# ============================================================================

sub AuthzDeleteAuthzForUser {
	my ($dbh, $uri_id, $user_id) = @_;

	my $uri = SelectSingle($dbh, "SELECT m_authorize_user.user_id, m_authorize.access, m_uri.rowid, m_uri.uri ".
						         "FROM   m_authorize_user, m_authorize, m_uri ".
								 "WHERE  m_uri.rowid = $uri_id AND ".
						         "       m_uri.parent_uri_id = m_uri.rowid AND ".
						         "       m_authorize.uri_id = m_uri.rowid AND ".
								 "       m_authorize_user.authz_id = m_authorize.rowid AND ".
								 "       m_authorize_user.user_id = $user_id");

	
	if(defined $uri) {
		AuthzDeleteAuthzForUserOnURI($dbh, $uri_id, $user_id);

		my $children = SelectMultiple($dbh, "SELECT m_authorize_user.user_id, m_authorize.access, m_uri.rowid, m_uri.uri ".
									        "FROM   m_authorize_user, m_authorize, m_uri ".
									        "WHERE  m_uri.rowid != $uri->{rowid} AND ".
								            "       m_uri.uri ~ '^".st_FormatPostgreSQLRegExpString($uri->{uri})."/' AND ".
								            "       m_uri.parent_uri_id = m_uri.rowid AND ".
								            "       m_uri.stop_inheritance IS FALSE AND ".
									        "       m_authorize.uri_id = m_uri.rowid AND ".
								            "       m_authorize.access = $uri->{access} AND ".
								            "       m_authorize_user.authz_id = m_authorize.rowid AND ".
								            "       m_authorize_user.user_id = $user_id ".
							                "ORDER BY length(uri)");

		foreach my $child (@$children) {
			AuthzDeleteAuthzForUserOnURI($dbh, $child->{rowid}, $user_id);
		}
	}
}


sub AuthzDeleteAuthzForUserOnURI {
	my ($dbh, $uri_id, $user_id) = @_;

	my $authz_id = AuthzGetAuthorizeSchemaForUser($dbh, $uri_id, $user_id);
	
	ExecSQL($dbh, "DELETE FROM m_authorize_user ".
				  "WHERE  user_id  = $user_id AND ".
				  "       authz_id = $authz_id");
		
	AuthzCleanAuthorizeSchema($dbh, $authz_id);
}


# ============================================================================

=head2 AuthzDeleteAuthzForTeam ($dbh, $uri_id, $team_id)

	Delete authz entry for a team
	
=cut

# ============================================================================

sub AuthzDeleteAuthzForTeam {
	my ($dbh, $uri_id, $team_id) = @_;

	my $uri = SelectSingle($dbh, "SELECT m_authorize_team.team_id, m_authorize.access, m_uri.rowid, m_uri.uri ".
						         "FROM   m_authorize_team, m_authorize, m_uri ".
								 "WHERE  m_uri.rowid = $uri_id AND ".
						         "       m_uri.parent_uri_id = m_uri.rowid AND ".
						         "       m_authorize.uri_id = m_uri.rowid AND ".
								 "       m_authorize_team.authz_id = m_authorize.rowid AND ".
								 "       m_authorize_team.team_id = $team_id");

	
	if(defined $uri) {
		AuthzDeleteAuthzForTeamOnURI($dbh, $uri_id, $team_id);

		my $children = SelectMultiple($dbh, "SELECT m_authorize_team.team_id, m_authorize.access, m_uri.rowid, m_uri.uri ".
									        "FROM   m_authorize_team, m_authorize, m_uri ".
									        "WHERE  m_uri.rowid != $uri->{rowid} AND ".
								            "       m_uri.uri ~ '^".st_FormatPostgreSQLRegExpString($uri->{uri})."/' AND ".
								            "       m_uri.parent_uri_id = m_uri.rowid AND ".
								            "       m_uri.stop_inheritance IS FALSE AND ".
									        "       m_authorize.uri_id = m_uri.rowid AND ".
								            "       m_authorize.access = $uri->{access} AND ".
								            "       m_authorize_team.authz_id = m_authorize.rowid AND ".
								            "       m_authorize_team.team_id = $team_id ".
							                "ORDER BY length(uri)");

		foreach my $child (@$children) {
			AuthzDeleteAuthzForTeamOnURI($dbh, $child->{rowid}, $team_id);
		}
	}
}


sub AuthzDeleteAuthzForTeamOnURI {
	my ($dbh, $uri_id, $team_id) = @_;

	my $authz_id = AuthzGetAuthorizeSchemaForTeam($dbh, $uri_id, $team_id);
	
	ExecSQL($dbh, "DELETE FROM m_authorize_team ".
				  "WHERE  team_id  = $team_id AND ".
				  "       authz_id = $authz_id");
		
	AuthzCleanAuthorizeSchema($dbh, $authz_id);
}




# ============================================================================

=head2 AuthzDuplicateParentAuthz ($dbh, $uri_id)

	Duplicate the parent authz for the given uri
	
=cut

# ============================================================================

sub AuthzDuplicateParentAuthz {
	my ($dbh, $uri_id) = @_;

	# Duplicate inherited authz
	
	my $res = SelectSingle($dbh, "SELECT * FROM m_uri WHERE rowid = $uri_id");
	my $uri = $res->{uri};
	my $parent_uri_id = $res->{parent_uri_id};
	
	if($res->{rowid} == $res->{parent_uri_id}) {
		return;
	}

	ExecSQL($dbh, "UPDATE m_uri SET parent_uri_id = rowid WHERE rowid = $uri_id");
	ExecSQL($dbh, "UPDATE m_uri SET parent_uri_id = $uri_id WHERE uri ~ '^" . st_FormatPostgreSQLRegExpString($res->{uri}) . "/' AND parent_uri_id = $parent_uri_id");

	my $profiles = SelectMultiple($dbh, "SELECT m_authorize.uri_id, m_authorize.access, m_authorize_profile.profile_id ".
								        "FROM   m_authorize_profile, m_authorize ".
								        "WHERE  m_authorize_profile.authz_id = m_authorize.rowid AND ".
								        "       m_authorize.uri_id = $parent_uri_id");

	foreach my $prof (@$profiles) {
		AuthzAddAuthzForProfile($dbh, $uri_id, $prof->{profile_id}, $prof->{access});
	}


	my $users = SelectMultiple($dbh, "SELECT m_authorize.uri_id, m_authorize.access, m_authorize_user.user_id ".
								        "FROM m_authorize_user, m_authorize ".
								        "WHERE  m_authorize_user.authz_id = m_authorize.rowid AND ".
								        "       m_authorize.uri_id = $parent_uri_id ");

	foreach my $usr (@$users) {
		AuthzAddAuthzForUser($dbh, $uri_id, $usr->{user_id}, $usr->{access});
	}



	my $teams = SelectMultiple($dbh, "SELECT m_authorize.uri_id, m_authorize.access, m_authorize_team.team_id ".
								      "FROM m_authorize_team, m_authorize ".
								      "WHERE  m_authorize_team.authz_id = m_authorize.rowid AND ".
								      "       m_authorize.uri_id = $parent_uri_id ");

	foreach my $team (@$teams) {
		AuthzAddAuthzForTeam($dbh, $uri_id, $team->{team_id}, $team->{access});
	}


}


# ============================================================================

=head2 ProfileTestAuthzFunction ($dbh, $user_id, $group_id, $app_ident, $function_ident)

	Check if the user $user working in group $group can access to
	the function $func_ident in the application $app_ident .
	
=cut

# ============================================================================

sub ProfileTestAuthzFunction
{
	my ($dbh, $user_id, $group_id, $app_ident, $function_ident) = @_;

	my $res = SelectSingle($dbh, "SELECT count(*) FROM m_profile_user_function
	                              WHERE user_id = $user_id and group_id = $group_id and
	                                    app_ident = '$app_ident' and function_ident = '$function_ident'");

	if(! $res->{count}) {
		$res = SelectSingle($dbh, "SELECT count(*) FROM m_profile_expanded_user_functio
 	                               WHERE user_id = $user_id and group_id = $group_id and
	                                     app_ident = '$app_ident' and function_ident = '$function_ident'");

	}

	return $res->{count};
}

# ============================================================================

=head2 ProfileGetUsersForAuthzFunction ($dbh, $group_id, $app_ident, $function_ident)

	Get a user list of users that can access to
	the function $func_ident in the application $app_ident in the group $group_id.
	
=cut

# ============================================================================

sub ProfileGetUsersForAuthzFunction
{
	my ($dbh, $group_id, $app_ident, $function_ident) = @_;

    my $sql = "SELECT user_id FROM m_profile_expanded_user_functio WHERE group_id = $group_id and app_ident = '$app_ident' and function_ident = '$function_ident' UNION SELECT user_id FROM m_profile_user_function WHERE group_id = $group_id and app_ident = '$app_ident' and function_ident = '$function_ident'";
	my $res = SelectMultiple($dbh, $sql);

	return (map { $_->{user_id} } @$res);
}


# ============================================================================

=head2 ProfileTestAuthzMethod ($dbh, $user_id, $group_id, $app_ident, $method_ident)

	Check if the user $user working in group $group can access to
	the function $func_ident in the application $app_ident .
	
=cut

# ============================================================================

sub ProfileTestAuthzMethod
{
	my ($dbh, $user_id, $group_id, $app_ident, $method_ident) = @_;

	my $res = SelectSingle($dbh, "SELECT count(*) FROM m_profile_user_method
	                              WHERE user_id = $user_id and group_id = $group_id and
	                                    app_ident = '$app_ident' and method_ident = '$method_ident'");

	if(! $res->{count}) {
		$res = SelectSingle($dbh, "SELECT count(*) FROM m_profile_expanded_user_method
 	                               WHERE user_id = $user_id and group_id = $group_id and
	                                     app_ident = '$app_ident' and method_ident = '$method_ident'");

	}

	return $res->{count};
}



# ============================================================================

=head2 AuthzTestAccessForURI ($config, $uri, $user, $group, $access)

	Check if the user $user_id working in group $group_id can access to
	the uri $path.
	
=cut

# ============================================================================

sub AuthzTestAccessForURI
{
    my($config, $uri, $user, $group, $access) = @_;

	my $right = AuthzTestRightAccessForURI($config, $uri, $user, $group, $access);

	if(defined $right) {
		return ($right  >= $access);		
	}

	return undef;
}

# ============================================================================

=head2 AuthzTestRightAccessForURI ($config, $uri, $user, $group)

	Check if the user $user_id working in group $group_id can and uri return 
	the access right
	
=cut

# ============================================================================

sub AuthzTestRightAccessForURI
{
    my($config, $uri, $user, $group) = @_;
	my $dbh = $config->GetDBH();

	my $path = $uri->GetWebDavPath();
	$path =~ s/^\///;

	my $group_data = $config->GetGroupDataDir();

    # Access to group_data is always forbidden
    # ========================================
    if($path =~ m;^$group_data(?:/|$);) {
        return(0);
    }
	   
    my $user_id  = $user->GetRowid();
    #my $group_id = $group->GetRowid();

    $path = $uri->GetURI;
	$path =~ s/\/$//;

    my $sql = "select authztestaccessforuri('".st_FormatPostgreSQLString($path)."', $user_id) as access";

    my $req = SelectSingle ($dbh, $sql);

   if(defined $req->{access} and ! $req->{access}) {
	   return $uri->IsPublic();
   }

    return $req->{access};
}

# ============================================================================

=head2 AuthzTestRightAccessForRowids ($config, $user, @rowids)

  Return a list of authorized rowids
  
=cut

# ============================================================================

sub AuthzTestAccessForRowids {
  my ($config, $user, @rowids)  = @_;
  
  my @auth_rowids;
  return @auth_rowids unless @rowids;
  
  my $dbh     = $config->GetDBH;
  my $user_id = $user->GetRowid;
  my $listid = "(".join(',', @rowids).")";
  
  my $res = SelectMultiple($dbh,
   "SELECT uri_id, (max(accesskey)%10) AS accesskey FROM 
        ( SELECT m_uri.rowid AS uri_id, (50 + m_authorize.access) AS accesskey 
          FROM m_authorize, m_uri, m_authorize_user 
        WHERE  m_uri.rowid IN $listid AND 
                 m_authorize.uri_id = m_uri.parent_uri_id AND 
                 m_authorize_user.authz_id = m_authorize.rowid AND m_authorize_user.user_id = $user_id

        UNION 
   
        SELECT m_uri.rowid AS uri_id, (40 + m_authorize.access) AS accesskey 
        FROM m_authorize, m_uri, m_authorize_team, m_group_group
        WHERE m_uri.rowid IN $listid AND
                m_authorize.uri_id = m_uri.parent_uri_id AND
                m_authorize_team.authz_id = m_authorize.rowid AND
                m_group_group.group_id = m_authorize_team.team_id AND
                m_group_group.invited_group_id=$user_id 
   
          UNION 
         
        SELECT m_uri.rowid AS uri_id, (30 + m_authorize.access) AS accesskey 
        FROM m_authorize, m_uri, m_authorize_team, m_group_expanded_user
        WHERE m_uri.rowid IN $listid AND
                m_authorize.uri_id = m_uri.parent_uri_id AND
              m_authorize_team.authz_id = m_authorize.rowid AND 
                m_group_expanded_user.group_id = m_authorize_team.team_id AND
                m_group_expanded_user.invited_group_id=$user_id 

        UNION

        SELECT m_uri.rowid AS uri_id, (20 + m_authorize.access) AS accesskey 
        FROM m_authorize, m_uri, m_authorize_profile, m_profile_group 
        WHERE m_uri.rowid IN $listid AND
                m_authorize.uri_id = m_uri.parent_uri_id AND
                m_authorize_profile.authz_id = m_authorize.rowid AND
                m_profile_group.profile_id = m_authorize_profile.profile_id AND
                m_profile_group.group_id=$user_id 

        UNION 
   
        SELECT m_uri.rowid AS uri_id, (10 + m_authorize.access) AS accesskey 
        FROM m_authorize, m_uri, m_authorize_profile, m_profile_expanded_user 
        WHERE m_uri.rowid IN $listid AND
                m_authorize.uri_id = m_uri.parent_uri_id AND
                m_authorize_profile.authz_id = m_authorize.rowid AND
                m_profile_expanded_user.profile_id = m_authorize_profile.profile_id AND
                m_profile_expanded_user.user_id=$user_id

     ) AS tmp_req

   GROUP BY (uri_id)");

    return @auth_rowids unless @$res;
    @auth_rowids = map { $_->{uri_id} } grep {$_->{accesskey} != 0} @$res;
    return @auth_rowids;
}

# ============================================================================

=head2 AuthzTestRightAccessForRequest ($config, $user, $request)

  Return a list of authorized rowids defined by a request
  
=cut

# ============================================================================

sub AuthzTestAccessForRequest {
  my ($config, $user, $request)  = @_;
  
  my @auth_rowids;
  return @auth_rowids unless $request;
  
  my $dbh     = $config->GetDBH;
  my $user_id = $user->GetRowid;
  
  my $res = SelectMultiple($dbh,
   "SELECT uri_id, (max(accesskey)%10) AS accesskey FROM 
        ( SELECT m_uri.rowid AS uri_id, (50 + m_authorize.access) AS accesskey 
          FROM m_authorize, m_uri, m_authorize_user 
        WHERE  m_uri.rowid IN ($request) AND 
                 m_authorize.uri_id = m_uri.parent_uri_id AND 
                 m_authorize_user.authz_id = m_authorize.rowid AND m_authorize_user.user_id = $user_id

        UNION 
   
        SELECT m_uri.rowid AS uri_id, (40 + m_authorize.access) AS accesskey 
        FROM m_authorize, m_uri, m_authorize_team, m_group_group
        WHERE m_uri.rowid IN ($request) AND
                m_authorize.uri_id = m_uri.parent_uri_id AND
                m_authorize_team.authz_id = m_authorize.rowid AND
                m_group_group.group_id = m_authorize_team.team_id AND
                m_group_group.invited_group_id=$user_id 
   
          UNION 
         
        SELECT m_uri.rowid AS uri_id, (30 + m_authorize.access) AS accesskey 
        FROM m_authorize, m_uri, m_authorize_team, m_group_expanded_user
        WHERE m_uri.rowid IN ($request) AND
                m_authorize.uri_id = m_uri.parent_uri_id AND
              m_authorize_team.authz_id = m_authorize.rowid AND 
                m_group_expanded_user.group_id = m_authorize_team.team_id AND
                m_group_expanded_user.invited_group_id=$user_id 

        UNION

        SELECT m_uri.rowid AS uri_id, (20 + m_authorize.access) AS accesskey 
        FROM m_authorize, m_uri, m_authorize_profile, m_profile_group 
        WHERE m_uri.rowid IN ($request) AND
                m_authorize.uri_id = m_uri.parent_uri_id AND
                m_authorize_profile.authz_id = m_authorize.rowid AND
                m_profile_group.profile_id = m_authorize_profile.profile_id AND
                m_profile_group.group_id=$user_id 

        UNION 
   
        SELECT m_uri.rowid AS uri_id, (10 + m_authorize.access) AS accesskey 
        FROM m_authorize, m_uri, m_authorize_profile, m_profile_expanded_user 
        WHERE m_uri.rowid IN ($request) AND
                m_authorize.uri_id = m_uri.parent_uri_id AND
                m_authorize_profile.authz_id = m_authorize.rowid AND
                m_profile_expanded_user.profile_id = m_authorize_profile.profile_id AND
                m_profile_expanded_user.user_id=$user_id

     ) AS tmp_req

   GROUP BY (uri_id)");

    return @auth_rowids unless @$res;
    @auth_rowids = map { $_->{uri_id} } grep {$_->{accesskey} != 0} @$res;
    return @auth_rowids;
}

# ============================================================================

=head2 AuthzGetAuthorizeSchema ($dbh, $uri_id, $authz)

	Return the m_authorize rowid corresponding to the given (uri_id, authz).
	If not exists, create it
	
=cut

# ============================================================================

sub AuthzGetAuthorizeSchema {
	my ($dbh, $uri_id, $authz) = @_;

	if(!defined $authz) {
		throw Mioga2::Exception::Simple("AuthzGetAuthorizeSchema", __"missing authz parameter");
	}

	my $rowid = SelectSingle($dbh, "SELECT * FROM m_authorize WHERE uri_id = $uri_id AND access = $authz");
	
	if(! defined $rowid) {
		$rowid = SelectSingle($dbh, "SELECT nextval('m_authorize_rowid_seq') as rowid");
		
		ExecSQL($dbh, "INSERT INTO m_authorize (rowid,  uri_id,  access) ".
	                  "                 VALUES ($rowid->{rowid}, $uri_id, $authz)");
	}
	
	return $rowid->{rowid};
}



# ============================================================================

=head2 AuthzGetAuthorizeSchemaForProfile ($dbh, $uri_id, $profile_id)

	Return the m_authorize rowid corresponding to the given (uri_id, profile_id).
	
=cut

# ============================================================================

sub AuthzGetAuthorizeSchemaForProfile {
	my ($dbh, $uri_id, $profile_id) = @_;

	my $rowid = SelectSingle($dbh, "SELECT m_authorize.rowid FROM m_authorize, m_authorize_profile ".
							       "WHERE  m_authorize.uri_id = $uri_id AND ".
							       "       m_authorize.rowid = m_authorize_profile.authz_id AND ".
							       "       m_authorize_profile.profile_id = $profile_id");
	
	if(! defined $rowid) {
		throw Mioga2::Exception::Simple("Mioga2::tools::APIAuthz", __x("No specific rights for profile {profile} on uri {uri}", profile => $profile_id, uri => $uri_id));
	}
	
	return $rowid->{rowid};
}



# ============================================================================

=head2 AuthzGetAuthorizeSchemaForUser ($dbh, $uri_id, $user_id)

	Return the m_authorize rowid corresponding to the given (uri_id, user_id).
	
=cut

# ============================================================================

sub AuthzGetAuthorizeSchemaForUser {
	my ($dbh, $uri_id, $user_id) = @_;

	my $rowid = SelectSingle($dbh, "SELECT m_authorize.rowid FROM m_authorize, m_authorize_user ".
							       "WHERE  m_authorize.uri_id = $uri_id AND ".
							       "       m_authorize.rowid = m_authorize_user.authz_id AND ".
							       "       m_authorize_user.user_id = $user_id");
	
	if(! defined $rowid) {
		throw Mioga2::Exception::Simple("Mioga2::tools::APIAuthz", __x("No specific rights for user {user} on uri {uri}", user => $user_id, uri => $uri_id));
	}
	
	return $rowid->{rowid};
}


# ============================================================================

=head2 AuthzGetAuthorizeSchemaForTeam ($dbh, $uri_id, $team_id)

	Return the m_authorize rowid corresponding to the given (uri_id, team_id).
	
=cut

# ============================================================================

sub AuthzGetAuthorizeSchemaForTeam {
	my ($dbh, $uri_id, $team_id) = @_;

	my $rowid = SelectSingle($dbh, "SELECT m_authorize.rowid FROM m_authorize, m_authorize_team ".
							       "WHERE  m_authorize.uri_id = $uri_id AND ".
							       "       m_authorize.rowid = m_authorize_team.authz_id AND ".
							       "       m_authorize_team.team_id = $team_id");
	
	if(! defined $rowid) {
		throw Mioga2::Exception::Simple("Mioga2::tools::APIAuthz", __x("No specific rights for team {team} on uri {uri}", team => $team_id, uri => $uri_id));
	}
	
	return $rowid->{rowid};
}


# ============================================================================

=head2 AuthzCleanAuthorizeSchema ($dbh, $authz_id)

	Remove this schema if its is no longer used
	
=cut

# ============================================================================

sub AuthzCleanAuthorizeSchema {
	my ($dbh, $authz_id) = @_;

	my $profile = SelectSingle($dbh, "SELECT count(*) FROM m_authorize_profile WHERE authz_id = $authz_id");
	my $team    = SelectSingle($dbh, "SELECT count(*) FROM m_authorize_team    WHERE authz_id = $authz_id");
	my $user    = SelectSingle($dbh, "SELECT count(*) FROM m_authorize_user    WHERE authz_id = $authz_id");
	
	if($profile->{count} == 0 and $user->{count} == 0 and $team->{count} == 0) {
		ExecSQL($dbh, "DELETE FROM m_authorize WHERE rowid = $authz_id");
	}

}

# ============================================================================

=head2 AuthzUpdateDirRights ($config, $path)

	Update parent_uri_id for uri matching path : $path. 
	
=cut

# ============================================================================

sub AuthzUpdateDirRights {
	my ($config, $path) = @_;

	$path =~ s/\/\//\//g;

	my $uri = $path;
	my $dbh = $config->GetDBH();

	my $private_dir = $config->GetPrivateDir();
	my $private_uri = $config->GetPrivateURI();
	my $public_dir = $config->GetPublicDir();
	my $public_uri = $config->GetPublicURI();


	my $base_uri;
	my $base_dir;
	if($path =~ /^$private_dir/) {
		$uri =~ s/^$private_dir/$private_uri/;

		$base_uri = $private_uri;
		$base_dir = $private_dir;
	}
	else {
		$uri =~ s/^$public_dir/$public_uri/;

		$base_uri = $public_uri;
		$base_dir = $public_dir;
	}

	my $parent_uri = SelectSingle($dbh, "SELECT * FROM m_uri WHERE uri ~ ('^' || uri2regexp('".st_FormatPostgreSQLRegExpString($uri)."') || '\$') ORDER BY length(uri) DESC LIMIT 1");

	if(!defined $parent_uri) {
		throw Mioga2::Exception::Simple("Mioga2::tools::APIAuthz", __x("No parent uri for {uri}", uri => $uri));
	}

	open(F_FILELIST, "find $path |")
		or die "Can't run find : $!";


	while(my $file = <F_FILELIST>) {
		chomp($file);
		
		$file =~ s/^$base_dir/$base_uri/;
		
		ExecSQL($dbh, "INSERT INTO m_uri (group_id, uri, user_id, parent_uri_id, stop_inheritance) ".
					  "VALUES ($parent_uri->{group_id}, '".st_FormatPostgreSQLString($file)."', $parent_uri->{user_id}, $parent_uri->{parent_uri_id}, FALSE)");

		
	}
}


#===============================================================================

=head2 AuthzListAccessibleURIs

Given a list of base URIs, return a list of all sub-URIs a user can access

=over

=item B<Parameters:>

=over

=item I<$config> A Mioga2::Config object.

=item I<$user> A Mioga2::Old::User object.

=item I<@base_rowids> An array containing the base URIs to check.

=back

=item B<Return:> An array of accessible URIs.

=back

=cut

#===============================================================================
sub AuthzListAccessibleURIs {
	my ($config, $user, @base_uris) = @_;

	my @uris = ();

	my $user_id = $user->GetRowid ();

	for (@base_uris) {
		my $uri = st_FormatPostgreSQLRegExpString ($_);
		my $res = SelectMultiple ($config->GetDBH (), "SELECT uri FROM ( SELECT m_uri.uri AS uri, (50 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_user WHERE m_uri.uri~'^$uri(/.*)?\$' AND m_authorize.uri_id = m_uri.parent_uri_id AND m_authorize_user.authz_id = m_authorize.rowid AND m_authorize_user.user_id = $user_id UNION SELECT m_uri.uri AS uri, (40 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_team, m_group_group WHERE  m_uri.uri~'^$uri(/.*)?\$' AND m_authorize.uri_id = m_uri.parent_uri_id AND m_authorize_team.authz_id = m_authorize.rowid AND m_group_group.group_id = m_authorize_team.team_id AND m_group_group.invited_group_id = $user_id UNION SELECT m_uri.uri AS uri, (30 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_profile, m_profile_group WHERE  m_uri.uri~'^$uri(/.*)?\$' AND m_authorize.uri_id = m_uri.parent_uri_id AND m_authorize_profile.authz_id = m_authorize.rowid AND m_profile_group.profile_id = m_authorize_profile.profile_id AND m_profile_group.group_id = $user_id UNION SELECT m_uri.uri AS uri, (20 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_profile, m_profile_expanded_user WHERE  m_uri.uri~'^$uri(/.*)?\$' AND m_authorize.uri_id = m_uri.parent_uri_id AND m_authorize_profile.authz_id = m_authorize.rowid AND m_profile_expanded_user.profile_id = m_authorize_profile.profile_id AND m_profile_expanded_user.user_id = $user_id ) AS tmp_req GROUP BY (uri) HAVING (max(accesskey)%10) > 0;");
		for (@$res) {
			push (@uris, $_->{uri});
		}
	}

	return (@uris);
}	# ----------  end of subroutine AuthzListAccessibleURIs  ----------

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Old::User

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
