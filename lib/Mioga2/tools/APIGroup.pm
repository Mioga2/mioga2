# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
APIGroup.pm: Group/User/Resource creation and deletion methods

=head1 DESCRIPTION

This module contains functions for creating and deleting groups, users
and resources.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
package Mioga2::tools::APIGroup;
use strict;

use Locale::TextDomain::UTF8 'tools_apigroup';

use vars qw(@ISA @EXPORT);
use Error qw(:try);
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::APIApplication;
use Mioga2::Exception::DB;
use Mioga2::Exception::Quota;
use Mioga2::tools::APIProfile;
use Mioga2::AppDesc;
use Mioga2::Config;
use Mioga2::Old::Group;
use Mioga2::Old::Resource;
use POSIX qw(strftime);
use File::Glob ':glob';
use File::MimeInfo::Magic;
use File::Path;
use Text::Iconv;

@ISA = qw(Exporter);
@EXPORT = qw( 
              TeamCreate
 			  TeamModify
 			  TeamDelete
              GroupCreate
 			  GroupModify
 			  GroupDelete
			  ResourceCreate
			  ResourceModify
              ResourceDisable
              ResourceEnable
              ResourceDelete
              UserLDAPCreate
              UserLocalCreate
              UserExternalCreate
			  UserExternalModify
			  UserNonLocalModify
			  UserCheckFormValues
			  UserModify
              UserDisable
              UserEnable
              UserDelete
			  UserLdapDelete
			  UserLdapEnable
              GroupInviteGroup
              GroupRevokeUser
              GroupRevokeTeam	  
			  ResourceRevokeGroup
              TeamRevokeUser
			  GroupChangeUserList
			  GroupChangeTeamList
			  UserChangeTeamList
			  UserChangeGroupList
			  TeamChangeUserList
			  TeamChangeGroupList
              GroupSetLastConnectionDate
			  SkelApplyDefaultGroup
			  SkelApplyDefaultUser
			  SkelApplyDefaultResource
			  SkelApplyGroup
			  SkelApplyUser
			  SkelApplyResource
			  SkelApplyGroupBaseXMLTree
			  CreateUser
			  LaunchMethodInApps
			  );

my $debug = 0;

# ============================================================================

=head2 TeamCreate ($config, $team, $no_transaction)

	Add a new team into database.
	
	$team is a team description containing.
	
	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub TeamCreate {
	my ($config, $group, $no_transaction) = @_;
	
	my $dbh = $config->GetDBH();
	
	$no_transaction = 0 unless defined $no_transaction;

	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	my %values = %$group;

	try {

		# ------------------------------------------------------
		# Set default values from Mioga configuration.
		# ------------------------------------------------------
		InitializeGroupValues($config, \%values);
		

		my $res = SelectSingle($dbh, "SELECT rowid FROM m_group_type
	                              WHERE ident = 'team'");
		$values{type_id} = $res->{rowid};
		$values{type} = 'team';

		$res = SelectSingle($dbh, "SELECT rowid FROM m_group_status
	                              WHERE ident = 'active'");
		$values{status_id} = $res->{rowid};
		
		
		$res = SelectSingle($dbh, "SELECT nextval('m_group_base_rowid_seq') AS rowid");
		$values{rowid}   = $res->{rowid};

		# ------------------------------------------------------
		# Fails if ident is not set.
		# ------------------------------------------------------
		if(! exists  $values{ident}) {
			throw Mioga2::Exception::Simple("APIGroup::TeamCreate",
											__"Missing team ident in team description.");
		}


		# ------------------------------------------------------
		# Fails if creator_id or anim_id is not set.
		# ------------------------------------------------------
		if(! exists  $values{creator_id}) {
			throw Mioga2::Exception::Simple("APIGroup::TeamCreate",
											__"Missing creator id in team description.");
		}
		
		$values{anim_id}     = $values{creator_id};
		$values{public_part} = 0;
		$values{autonomous}  = 0;

		my $sql = BuildInsertRequest(\%values, table  => 'm_group',
									 string => [ qw(description ident) ],
									 bool   => [ qw(public_part autonomous) ],
									 other  => [ qw(rowid created modified mioga_id type_id lang_id theme_id anim_id
													creator_id status_id)],
									 );
		ExecSQL($dbh, $sql);
		
		# ------------------------------------------------------
		# Initialize the team.
		# ------------------------------------------------------
		GroupInviteGroup($dbh, $values{rowid}, $values{anim_id});		
	}
	otherwise {
		my $err = shift;		
		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}

	return $values{rowid};
}



# ============================================================================

=head2 TeamModify ($config, $team, $no_transaction)

	Modify a team.
	
	$team is a team description (must contains a "rowid" field).

	if $no_transaction == 1, the SQL transactions are not used.

=cut

# ============================================================================

sub TeamModify {
	my ($config, $team, $no_transaction) = @_;

	my $dbh = $config->GetDBH();

	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {
		# ------------------------------------------------------
		# Set default values from Mioga configuration.
		# ------------------------------------------------------
		my %values = %$team;
		$values{modified} = 'now()';

		my $sql = BuildUpdateRequest(\%values, table  => 'm_group',
									 string => [ qw(description ident) ],
									 other  => [ qw(modified) ],
									 );

		$sql .= " WHERE rowid = $values{rowid}";

		ExecSQL($dbh, $sql);
		
	}
	otherwise {
		my $err = shift;		

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}


# ============================================================================

=head2 TeamDelete ($config, $team_id, $no_transaction)

	Delete the team and clean the database.

	if $no_transaction == 1, the SQL transactions are not used.

=cut

# ============================================================================

sub TeamDelete {
	my ($config, $team_id, $no_transaction) = @_;

	my $dbh = $config->GetDBH();

	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {
		my $team = new Mioga2::Old::Team($config, rowid => $team_id);
		
		##
		## Revoke team from all groups
		##
		my $groups = $team->GetGroups();
		my $users = $team->GetUsers();

		foreach my $group_id (@{$groups->GetRowids()}, @{$users->GetRowids()}) {
			GroupRevokeTeam($config, $group_id, $team_id);
		}
		
		##
		## Revoke all teams member
		##

		foreach my $user_id (@{$users->GetRowids()}) {
			TeamRevokeUser($config, $team_id, $user_id);
		}
		
		
		ExecSQL($config->GetDBH(), "DELETE FROM m_group_group WHERE group_id = $team_id");
		ExecSQL($dbh, "DELETE FROM m_group_user_last_conn WHERE group_id = $team_id");
		ExecSQL($dbh, "DELETE FROM m_group_user_last_conn WHERE user_id = $team_id");
		ExecSQL($config->GetDBH(), "DELETE FROM m_group_base WHERE rowid = $team_id");
		
	}
	otherwise {
		my $err = shift;
		
		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}


# ============================================================================

=head2 GroupCreate ($config, $group, $no_transaction)

	Add a new group into database.
	
	$group is a group description.

	if $no_transaction == 1, the SQL transactions are not used.

=cut

# ============================================================================

sub GroupCreate {
	my ($config, $group, $no_transaction) = @_;

	my $dbh = $config->GetDBH();
	
	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	my %values = %$group;
	try {

		CheckGroupQuota($config);

		# ------------------------------------------------------
		# Set default values from Mioga configuration.
		# ------------------------------------------------------
		InitializeGroupValues($config, \%values);
		
		my $res = SelectSingle($dbh, "SELECT rowid FROM m_group_type
	                              WHERE ident = 'group'");
		$values{type_id} = $res->{rowid};
		$values{type} = 'group';
		

		$res = SelectSingle($dbh, "SELECT rowid FROM m_group_status
	                              WHERE ident = 'active'");
		$values{status_id} = $res->{rowid};
		
		
		$res = SelectSingle($dbh, "SELECT nextval('m_group_base_rowid_seq') AS rowid");
		$values{rowid}   = $res->{rowid};

		# ------------------------------------------------------
		# Fails if ident is not set.
		# ------------------------------------------------------
		if(! exists  $values{ident}) {
			throw Mioga2::Exception::Simple("APIGroup::GroupCreate",
											__"Missing group ident in group description.");
		}


		# ------------------------------------------------------
		# Fails if creator_id or anim_id is not set.
		# ------------------------------------------------------
		if(! exists  $values{creator_id} or 
		   ! exists  $values{anim_id}) {
			throw Mioga2::Exception::Simple("APIGroup::GroupCreate",
											__"Missing creator or animator id in group description.");
		}
		

		my $sql = BuildInsertRequest(\%values, table  => 'm_group',
									 string => [ qw(description ident) ],
									 bool   => [ qw(public_part autonomous history) ],
									 other  => [ qw(rowid created modified mioga_id type_id lang_id theme_id anim_id
													creator_id status_id default_app_id)],
									 );
		ExecSQL($dbh, $sql);
		
		# ------------------------------------------------------
		# Initialize the group.
		# ------------------------------------------------------
		InitializeGroup($config, \%values);
		
	}
	otherwise {
		my $err = shift;		
		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}


	# # ------------------------------------------------------
	# # Apply the Group Skeleton.
	# # ------------------------------------------------------
	# SkelApplyDefaultGroup($config, \%values);

	# if(exists $group->{skeleton}) {
	# 	SkelApplyGroup($config, \%values, $config->GetSkeletonDir(). "/group/".$group->{skeleton});
	# }

	return $values{rowid};
}



# ============================================================================

=head2 GroupModify ($config, $group, $no_transaction)

	Modify a group.
	
	$group is a group description (must contains a "rowid" field).

	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub GroupModify {
	my ($config, $group, $no_transaction) = @_;

	my $dbh = $config->GetDBH();

	my $old_group = new Mioga2::Old::Group($config, rowid => $group->{rowid});
	
	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {
		# ------------------------------------------------------
		# Set default values from Mioga configuration.
		# ------------------------------------------------------
		my %values = %$group;
		$values{modified} = 'now()';

		# ------------------------------------------------------
		# Create the public part if required
		# ------------------------------------------------------
		if(exists $values{public_part} and
		   $values{public_part} and 
		   ! $old_group->HasPublicPart()) {

			CreatePublicPart($config, \%values);
		}
		
		# ------------------------------------------------------
		# Destroy the public part if required
		# ------------------------------------------------------
		if(exists $values{public_part} and
		   ! $values{public_part} and 
		   $old_group->HasPublicPart()) {

			DeletePublicSpace($config, $values{rowid});
		}
		
		# ------------------------------------------------------
		# Change the animator if required
		# ------------------------------------------------------
		if(exists $values{anim_id} and
		   $values{anim_id} != $old_group->GetAnimId() ) {
			
			ModifyGroupAnimator($config, \%values, $old_group);
		}

		# ------------------------------------------------------
        # Change ident
        # ------------------------------------------------------
        if (exists $values{ident} && $old_group->GetIdent ne $values{ident}) {
            my $old_ident   = $old_group->GetIdent;
            my $length      = "char_length('" . st_FormatPostgreSQLString($old_ident) . "')";
            
            # For private access
            my $pos = length($config->GetPrivateURI) + 2;
            my $sql = "update m_uri set uri = overlay(uri placing '" . st_FormatPostgreSQLString($values{ident}) . "' from $pos for $length), modified = NOW() where uri like '" . st_FormatPostgreSQLString($config->GetPrivateURI . "/" . $old_ident) . "%'";
            ExecSQL($dbh, $sql);
            
            my $old_path    = $config->GetPrivateDir . "/" . $old_ident;
            my $new_path    = $config->GetPrivateDir . "/" . $values{ident};
            rename $old_path, $new_path;
            
            # Same for public access
            $pos    = length($config->GetPublicURI) + 2;
            $sql    = "update m_uri set uri = overlay(uri placing '" . st_FormatPostgreSQLString($values{ident}) . "' from $pos for $length), modified = NOW() where uri like '" . st_FormatPostgreSQLString($config->GetPublicURI . "/" . $old_ident) . "%'";
            ExecSQL($dbh, $sql);
            
            $old_path   = $config->GetPublicDir . "/" . $old_ident;
            $new_path   = $config->GetPublicDir . "/" . $values{ident};
            rename $old_path, $new_path;
            
            # History
            if ($old_group->HasHistory) {
                my $history_dir = $config->GetMiogaFilesDir . "/history";
                rename "$history_dir/$old_ident", "$history_dir/$values{ident}" if -d "$history_dir/$old_ident";
            }
            
            # Update Search Engine cache
        }
        
		my $sql = BuildUpdateRequest(\%values, table  => 'm_group',
									 string => [ qw(description ident) ],
									 bool   => [ qw(public_part autonomous history) ],
									 other  => [ qw(modified anim_id default_profile_id lang_id theme_id default_app_id) ],
									 );

		$sql .= " WHERE rowid = $values{rowid}";

		ExecSQL($dbh, $sql);
	}
	otherwise {
		my $err = shift;		

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}


# ============================================================================

=head2 GroupBaseDelete ($config, $group_id)

	Delete a group/usr/resource/team and clean the database.

=cut

# ============================================================================

sub GroupBaseDelete {
	my ($config, $group_id) = @_;

	my $dbh = $config->GetDBH();

	my $group = Mioga2::Old::GroupBase->CreateObject($config, rowid => $group_id);

	##
	## Revoke all users
	##
	my $user_list = $group->GetInvitedUsers();
	
	foreach my $user_id (@{$user_list->GetRowids()}) {
		GroupRevokeUser($config, $group_id, $user_id, 1 ); # 1 => force_animator_deletion
	}
	
	
	##
	## Revoke all teams
	##
	my $team_list = $group->GetInvitedTeams();
	
	foreach my $team_id (@{$team_list->GetRowids()}) {
		GroupRevokeTeam($config, $group_id, $team_id);
	}
	
	
	##
	## Revoke group from all resources
	##
	my $res_list = new Mioga2::Old::ResourceList($config);
	$res_list->AddResourcesMemberOfGroup($group_id);
	
	foreach my $res_id (@{$res_list->GetRowids()}) {
		ResourceRevokeGroup ($config, $res_id, $group_id);
	}
	
	
	LaunchMethodInApps($config, "DeleteGroupData", $group_id);
	
	# ------------------------------------------------------
	# Change creator_id on groups to Mioga Administrator
	# ------------------------------------------------------
	
	ExecSQL($dbh, "UPDATE m_group_base SET creator_id = ".$config->GetAdminId()." WHERE creator_id = $group_id");
    
    if ($group_id == $config->GetAdminId()) {
    	ExecSQL($dbh, "UPDATE m_group_base SET anim_id = NULL WHERE anim_id = $group_id");
	}
	else {
    	ExecSQL($dbh, "UPDATE m_group_base SET anim_id = ".$config->GetAdminId()." WHERE anim_id = $group_id");
	}

	# ------------------------------------------------------
	# Delete the group.
	# ------------------------------------------------------
	
	ProfileDeleteGroup($dbh, $group_id);
	
	ApplicationDeleteGroup($dbh, $group_id);

	ExecSQL($dbh, "DELETE FROM m_group_user_last_conn WHERE group_id = $group_id");
	ExecSQL($dbh, "DELETE FROM m_group_user_last_conn WHERE user_id = $group_id");
	ExecSQL($dbh, "DELETE FROM m_group_base WHERE rowid=$group_id");

	return $group;
}

# ============================================================================

=head2 GroupDelete ($config, $group_id, $no_transaction)

	Delete the group and clean the database.
	
	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub GroupDelete {
	my ($config, $group_id, $no_transaction) = @_;

	my $dbh = $config->GetDBH();

	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {
		my $group = GroupBaseDelete($config, $group_id);
	}
	otherwise {
		my $err = shift;

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}

		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}



# ============================================================================

=head2 UserLDAPCreate ($config, $ldap_user, $no_transaction)

	Add a new ldap_user into database.
	
	$ldap_user is a ldap user description returned by the Mioga2:LDAP object.

	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub UserLDAPCreate {
	my ($config, $ldap_user, $no_transaction) = @_;
	
	# ------------------------------------------------------
	# Check if a local user with the same ident does not 
	# already exists.
	# ------------------------------------------------------
	my $dbh = $config->GetDBH();

	my $result = SelectSingle($dbh, "SELECT count(*) FROM m_group_base ".
							  "WHERE ident='$ldap_user->{ident}' AND ".
							  "      mioga_id = ".$config->GetMiogaId()
							  );

	if($result->{count} > 0) {
		throw Mioga2::Exception::User("APIGroup::UserLDAPCreate",
        __x("A local user with ident {ident} already exists", ident => $ldap_user->{ident}));
	}

	$ldap_user->{creator_id} = $config->GetAdminId ();
	

	CreateUser($config, $ldap_user, 'ldap', $no_transaction);
}


# ============================================================================

=head2 UserLocalCreate ($config, $local_user, $no_transaction)

	Add a new local_user into database.
	
	$local_user is a local user description. 
	Values not set in this structure are set to default values 
	from Mioga configuration.

	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub UserLocalCreate {
	my ($config, $local_user, $no_transaction) = @_;

	CreateUser($config, $local_user, 'local', $no_transaction);
}




# ============================================================================

=head2 UserExternalCreate ($config, $external_user)

	Add a new external_user into database.
	
	$external_user is a external user description. 
	Values not set in this structure are set to default values 
	from Mioga configuration.
	
=cut

# ============================================================================

sub UserExternalCreate {
	my ($config, $external_user) = @_;

	# ------------------------------------------------------
	# Check if a local/ldap user with the same ident does not 
	# already exists.
	# ------------------------------------------------------
	my $error = 0;;
	try {
		$external_user->{'external_ident'} = $external_user->{ident};
		my $user = new Mioga2::Old::User($config, ident => $external_user->{ident});

		# A user already exists, try to use email as ident
		$external_user->{ident} = $external_user->{email};
		$user = new Mioga2::Old::User($config, ident => $external_user->{email});

		$error = 1;
	}

	otherwise {
	};

	if($error) {
		throw Mioga2::Exception::User("APIGroup::UserExternalCreate",
        __x("A local user with ident {ident} or {email} already exists", ident => $external_user->{ident}, email => $external_user->{email}));
	}

	$external_user->{public_part} = 0;

	CreateUser($config, $external_user, 'external');
}




# ============================================================================

=head2 UserExternalModify ($config, $user, $no_transaction)

	Modify a external user.
	
	$user is a user description (must contains a "rowid" field).
	
	if $no_transaction == 1, the SQL transactions are not used.

=cut

# ============================================================================

sub UserExternalModify {
	my ($config, $user, $no_transaction) = @_;

	my $dbh = $config->GetDBH();

	my $old_user = new Mioga2::Old::User($config, rowid => $user->{rowid});

	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {
		# ------------------------------------------------------
		# Set default values from Mioga configuration.
		# ------------------------------------------------------
		my %values = %$user;
		$values{modified} = 'now()';

		my $type = $old_user->GetType();
		$values{type} = $type;

		delete $values{dn};
		delete $values{status};
		$values{data_expiration_date} = strftime("%Y-%m-%d %H:%M:%S", gmtime(time + 600));


		# ------------------------------------------------------
		# Crypt password
		# ------------------------------------------------------
		if(exists $values{password}) {
			UserModifyPassword($config, \%values);
		}
		
		my $sql = BuildUpdateRequest(\%values, table  => "m_user_base",
									 string => [ qw(firstname lastname email data_expiration_date password_expiration_date password) ],
									 other  => [ qw(modified) ],
									 );

		$sql .= " WHERE rowid = $values{rowid}";

		ExecSQL($dbh, $sql);
		
	}
	otherwise {
		my $err = shift;		

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}


# ============================================================================

=head2 UserNonLocalModify ($config, $user, $no_transaction)

	Modify a nonlocal user.
	
	$user is a user description (must contains a "rowid" field).
	
	if $no_transaction == 1, the SQL transactions are not used.

=cut

# ============================================================================

sub UserNonLocalModify {
	my ($config, $user, $no_transaction) = @_;
    print STDERR "UserNonLocalModify user = " . Dumper($user) . "\n" if ($debug);
    
	my $dbh = $config->GetDBH();

	my $old_user = new Mioga2::Old::User($config, rowid => $user->{rowid});

	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {
		# ------------------------------------------------------
		# Set default values from Mioga configuration.
		# ------------------------------------------------------
		my %values = %$user;
		$values{modified} = 'now()';
		$values{data_expiration_date} = strftime("%Y-%m-%d %H:%M:%S", gmtime(time + 600));

		my $type = $old_user->GetType();
		$values{type} = $type;

		if($type eq 'ldap_user') {
			$values{dn} = $old_user->GetDN();
		}
		print STDERR "UserNonLocalModify values = " . Dumper(\%values) . "\n" if ($debug);

		# ------------------------------------------------------
		# Crypt password
		# ------------------------------------------------------
		if(exists $values{password}) {
			UserModifyPassword($config, \%values);
		}
		$user->{password} = $values{password};
		
		my $sql = BuildUpdateRequest(\%values, table  => "m_user_base",
									 string => [ qw(data_expiration_date password_expiration_date password) ],
									 other  => [ qw(modified) ],
									 );

		$sql .= " WHERE rowid = $values{rowid}";

		print STDERR "UserNonLocalModify sql = $sql\n" if ($debug);
		ExecSQL($dbh, $sql);
		
	}
	otherwise {
		my $err = shift;		

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}


# ============================================================================

=head2 UserModify ($config, $user, $no_transaction)

	Modify a user.
	
	$user is a user description (must contains a "rowid" field).
	
	if $no_transaction == 1, the SQL transactions are not used.

=cut

# ============================================================================

sub UserModify {
	my ($config, $user, $no_transaction) = @_;

	print STDERR "Mioga2::tool::APIGroup::UserModify\n" if ($debug);

	my $dbh = $config->GetDBH();

	my $old_user = new Mioga2::Old::User($config, rowid => $user->{rowid});
	
	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}
	
	try {
		# ------------------------------------------------------
		# Set default values from Mioga configuration.
		# ------------------------------------------------------
		my %values = %$user;
		$values{modified} = 'now()';

		# ------------------------------------------------------
		# Create the public part if required
		# ------------------------------------------------------
		if(exists $values{public_part} and
		   $values{public_part} and 
		   ! $old_user->HasPublicPart()) {

			CreatePublicPart($config, \%values);
		}
		
		# ------------------------------------------------------
		# Destroy the public part if required
		# ------------------------------------------------------
		if(exists $values{public_part} and
		   ! $values{public_part} and 
		   $old_user->HasPublicPart()) {

			DeletePublicSpace($config, $values{rowid});
		}
		
		
		# ------------------------------------------------------
		# Initialize nonlocal user values
		# ------------------------------------------------------
		my $type = $old_user->GetType();
		$values{type} = $type;
		if($type ne 'local_user') {
			$values{data_expiration_date} = strftime("%Y-%m-%d %H:%M:%S", gmtime(time + 600));
			$values{password_expiration_date} = strftime("%Y-%m-%d %H:%M:%S", gmtime(0));
		}		

		if($type eq 'ldap_user') {
			$values{dn} = $old_user->GetDN();
		}

		# ------------------------------------------------------
		# Crypt password
		# ------------------------------------------------------
		if(exists $values{password}) {
			UserModifyPassword($config, \%values);
		}

		my $table = {'local_user' => 'm_user_base', 'ldap_user' => 'm_user_base', 'external_user' => 'm_user_base'}->{$type};
		my $sql = BuildUpdateRequest(\%values, table  => $table,
									 string => [ qw(ident firstname lastname email password working_days data_expiration_date password_expiration_date) ],
									 bool   => [ qw(public_part autonomous monday_first) ],
									 other  => [ qw(modified timezone_id time_per_day lang_id theme_id default_profile_id) ],
									 );

		$sql .= " WHERE rowid = $values{rowid}";

		ExecSQL($dbh, $sql);
	}
	otherwise {
		my $err = shift;

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}


# ============================================================================

=head2 ResourceCreate ($config, $resource, $no_transaction)

	Add a new resource into database.
	
	$resource is a resource description.

	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub ResourceCreate {
	my ($config, $resource, $no_transaction) = @_;

	my $dbh = $config->GetDBH();
	
	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	my %values = %$resource;
	try {
		CheckGroupQuota($config);

		# ------------------------------------------------------
		# Set default values from Mioga configuration.
		# ------------------------------------------------------
		InitializeGroupValues($config, \%values);
		
		# Like a user without status
		InitializeUserValues($config, \%values);
		delete $values{status_id};


		# Set the status to active.
		my $res = SelectSingle($dbh, "SELECT rowid FROM m_resource_status
                                  WHERE ident = 'active'");
		$values{status_id} = $res->{rowid};


		$res = SelectSingle($dbh, "SELECT rowid FROM m_group_type
	                           WHERE ident = 'resource'");
		$values{type_id} = $res->{rowid};
		$values{type} = 'resource';


		$res = SelectSingle($dbh, "SELECT nextval('m_group_base_rowid_seq') AS rowid");
		$values{rowid} = $res->{rowid};


		# ------------------------------------------------------
		# Fails if ident is not set.
		# ------------------------------------------------------
		if(! exists  $values{ident}) {
			throw Mioga2::Exception::Simple("APIGroup::ResourceCreate",
											__"Missing resource ident in group description.");
		}


		# ------------------------------------------------------
		# Fails if creator_id or anim_id is not set.
		# ------------------------------------------------------
		if(! exists  $values{creator_id} or 
		   ! exists  $values{anim_id}) {
			throw Mioga2::Exception::Simple("APIGroup::ResourceCreate",
											__"Missing creator or animator id in resource description.");
		}
		
		
		my $sql = BuildInsertRequest(\%values, table  => 'm_resource',
									 string => [ qw(description ident location working_days) ],
									 bool   => [ qw(public_part autonomous monday_first) ],
									 other  => [ qw(rowid created modified mioga_id type_id lang_id theme_id anim_id
													creator_id status_id timezone_id time_per_day) ],
									 );

		ExecSQL($dbh, $sql);
		
		# ------------------------------------------------------
		# Initialize the resource.
		# ------------------------------------------------------
		GroupInviteGroup($dbh, $values{rowid}, $values{anim_id});
		
	}
	otherwise {
		my $err = shift;		

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}

	SkelApplyDefaultResource($config, \%values);

	if(exists $resource->{skeleton}) {
		SkelApplyResource($config, \%values, $config->GetSkeletonDir(). "/resource/".$resource->{skeleton});
	}

	return $values{rowid};
}




# ============================================================================

=head2 ResourceModify ($config, $resource, $no_transaction)

	Modify a resource.
	
	$resource is a resource description (must contains a "rowid" field).
	
	if $no_transaction == 1, the SQL transactions are not used.

=cut

# ============================================================================

sub ResourceModify {
	my ($config, $resource, $no_transaction) = @_;

	my $dbh = $config->GetDBH();

	my $old_resource = new Mioga2::Old::Resource($config, rowid => $resource->{rowid});
	
	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {
		# ------------------------------------------------------
		# Set default values from Mioga configuration.
		# ------------------------------------------------------
		my %values = %$resource;
		$values{modified} = 'now()';

		# ------------------------------------------------------
		# Change the animator if required
		# ------------------------------------------------------
		if(exists $values{anim_id} and
		   $values{anim_id} != $old_resource->GetAnimId() ) {
			
			ModifyGroupAnimator($config, \%values, $old_resource);
		}

		
		my $sql = BuildUpdateRequest(\%values, table  => 'm_resource',
									 string => [ qw(description location) ],
									 other  => [ qw(modified anim_id default_profile_id lang_id theme_id status_id) ],
									 );

		$sql .= " WHERE rowid = $values{rowid}";

		ExecSQL($dbh, $sql);
		
	}
	otherwise {
		my $err = shift;		

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}

# ============================================================================

=head2 UserDisable ($config, $user_id, $no_transaction)

	Mark the user with rowid $user_id as disabled,
	Run the hook group script.

	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub UserDisable {
	my ($config, $user_id, $no_transaction) = @_;

	my $dbh = $config->GetDBH();

	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {

		# ------------------------------------------------------
		# Disable the user.
		# ------------------------------------------------------
		ExecSQL($dbh, "UPDATE m_user_base SET status_id = m_user_status.rowid 
	                                  FROM m_user_status WHERE m_user_status.ident='disabled' AND 
	                                        m_user_base.rowid=$user_id");
	}
	otherwise {
		my $err = shift;

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}

		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}


# ============================================================================

=head2 UserEnable ($config, $user_id, $no_transaction)

	Mark the user with rowid $user_id as active,
	Run the hook group script.
	
	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub UserEnable {
	my ($config, $user_id, $no_transaction) = @_;

	my $dbh = $config->GetDBH();

	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {

		# ------------------------------------------------------
		# Disable the user.
		# ------------------------------------------------------
		ExecSQL($dbh, "UPDATE m_user_base SET status_id = m_user_status.rowid, auth_fail = 0 
	                                  FROM m_user_status WHERE m_user_status.ident='active' AND 
	                                        m_user_base.rowid=$user_id");
	}
	otherwise {
		my $err = shift;

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}

		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}



# ============================================================================

=head2 UserDelete ($config, $user_id, $no_transaction)

	Remove the user with rowid $user_id.
	Run the hook group script.
	
	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub UserDelete {
	my ($config, $user_id, $no_transaction) = @_;

	my $dbh = $config->GetDBH();

	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {

		##
		## Revoke user from all groups
		##
		my $groups = new Mioga2::Old::GroupList($config);
		$groups->AddGroupsContainingUser($user_id);

		my $users = new Mioga2::Old::UserList($config);
		$users->AddUsersContainingUser($user_id);

		foreach my $group_id (@{$groups->GetRowids()}, @{$users->GetRowids()}) {
			if($group_id eq $user_id) {
				GroupRevokeUser($config, $group_id, $user_id, 1);
			}
			else {
				GroupRevokeUser($config, $group_id, $user_id);
			}

		}

		##
		## Revoke user from all teams
		##
		my $teams = new Mioga2::Old::TeamList($config);
		$teams->AddGroupsContainingUser($user_id);

		foreach my $team_id (@{$teams->GetRowids()}) {
			TeamRevokeUser($config, $team_id, $user_id);
		}
		

		my $user = GroupBaseDelete($config, $user_id);
		$user->DeleteSecretQuestion ();
	}
	otherwise {
		my $err = shift;

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}

		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}



# ============================================================================

=head2 UserLdapDelete ($config, $user_id, $no_transaction)

	Mark an ldap user with rowid $user_id as deleted_not_purged.
	
	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub UserLdapDelete {
	my ($config, $user_id, $no_transaction) = @_;

	$no_transaction = 0 unless defined $no_transaction;

	my $dbh = $config->GetDBH();

	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {

		# ------------------------------------------------------
		# Disable the user.
		# ------------------------------------------------------
		ExecSQL($dbh, "UPDATE m_user_base SET status_id = m_user_status.rowid 
	                                FROM m_user_status WHERE m_user_status.ident='deleted_not_purged' AND 
	                                      m_user_base.rowid=$user_id");

	}
	otherwise {
		my $err = shift;

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}

		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}



# ============================================================================

=head2 UserLdapEnable ($config, $user_id, $no_transaction)

	Mark an ldap user with rowid $user_id as active.
	
	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================
sub UserLdapEnable {
	my ($config, $user_id, $no_transaction) = @_;

	$no_transaction = 0 unless defined $no_transaction;

	my $dbh = $config->GetDBH();

	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {

		# ------------------------------------------------------
		# Enable the user.
		# ------------------------------------------------------
		ExecSQL($dbh, "UPDATE m_user_base SET status_id = m_user_status.rowid 
	                                FROM m_user_status WHERE m_user_status.ident='active' AND 
	                                      m_user_base.rowid=$user_id");

	}
	otherwise {
		my $err = shift;

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}

		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}


# ============================================================================

=head2 ResourceDisable ($config, $resource_id, $no_transaction)

	Mark the resource with rowid $resource_id as disabled,
	Run the hook group script.
	
	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub ResourceDisable {
	my ($config, $resource_id, $no_transaction) = @_;

	my $dbh = $config->GetDBH();

	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}
	
	try {
		
		# ------------------------------------------------------
		# Disable the resource.
		# ------------------------------------------------------
		ExecSQL($dbh, "UPDATE m_resource SET status_id = m_resource_status.rowid 
	                                 FROM m_resource_status WHERE m_resource_status.ident='disabled' AND 
	                                       m_resource.rowid=$resource_id");
	}
	otherwise {
		my $err = shift;

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}


# ============================================================================

=head2 ResourceDelete ($config, $resource_id, $no_transaction)

	Delete the resource and clean the database.
	
	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub ResourceDelete {
	my ($config, $resource_id, $no_transaction) = @_;

	my $dbh = $config->GetDBH();

	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {

		##
		## Revoke all groups
		##
		my $resource = new Mioga2::Old::Resource($config, rowid =>$resource_id);
		my $group_list = $resource->GetInvitedGroups();
		
		foreach my $group_id (@{$group_list->GetRowids()}) {
			ResourceRevokeGroup($config, $resource_id, $group_id);
		}
		
		GroupBaseDelete($config, $resource_id);
	}
	otherwise {
		my $err = shift;

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}



# ============================================================================

=head2 ResourceEnable ($config, $resource_id, $no_transaction)

	Mark the resource with rowid $resource_id as active,
	Run the hook group script.
	
	if $no_transaction == 1, the SQL transactions are not used.
	
=cut

# ============================================================================

sub ResourceEnable {
	my ($config, $resource_id, $no_transaction) = @_;

	my $dbh = $config->GetDBH();

	$no_transaction = 0 unless defined $no_transaction;
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	try {

		# ------------------------------------------------------
		# Disable the resource.
		# ------------------------------------------------------
		ExecSQL($dbh, "UPDATE m_resource SET status_id = m_resource_status.rowid 
	                                 FROM m_resource_status WHERE m_resource_status.ident='active' AND 
	                                       m_resource.rowid=$resource_id");
	}
	otherwise {
		my $err = shift;

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}

		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}
}


# ============================================================================

=head2 GroupInviteGroup ($dbh, $group_id, $group_to_invite_id)

	Invite the group with id $group_to_invite_id into the group $group_id
	
=cut

# ============================================================================

sub GroupInviteGroup {
	my ($dbh, $group_id, $group_to_invite_id) = @_;
	print STDERR "GroupInviteGroup (group_id = $group_id, group_to_invite_id = $group_to_invite_id)\n" if ($debug);

	my $sql;
	$sql = "INSERT INTO m_group_group (group_id,  invited_group_id) VALUES ($group_id, $group_to_invite_id)";
	print STDERR "sql = $sql\n" if ($debug);
	ExecSQL($dbh, $sql);	

	# Add this group in default profile

	print STDERR "Search Default Profile\n" if ($debug);
	my $res = SelectSingle($dbh, "SELECT default_profile_id FROM m_group_base WHERE rowid = $group_id");

	if(defined $res->{default_profile_id}) {
		print STDERR "Call ProfileAddGroup\n" if ($debug);
		ProfileAddGroup($dbh, $res->{default_profile_id}, $group_id, $group_to_invite_id);
	}

	# Initialize Last connection date if group_to_invite_id is a user
	$sql = "SELECT group_id, user_id FROM m_group_user_last_conn WHERE group_id=$group_id AND user_id=$group_to_invite_id";
	$res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		$sql = "INSERT INTO m_group_user_last_conn (group_id, user_id, last_connection)"
				. " SELECT $group_id AS group_id, $group_to_invite_id AS user_id, NULL AS last_connection "
				. " FROM m_user_base WHERE rowid = $group_to_invite_id";
		print STDERR "sql = $sql\n" if ($debug);
		ExecSQL($dbh, $sql);
	}
	else {
		# Ugh ? What's happening ? User already in last conn ?
		# Not very normal, but don't carsh because it's not really
		# important and can completly block the creation of a given 
		# user

		warn "group $group_to_invite_id already in last_connection table for group $group_id ??!";
	}
}



# ============================================================================

=head2 GroupRevokeUser ($config, $group_id, $user_to_revoke_id, $force_animator_deletion)

	Revoke the user with id $user_to_revoke_id from the group $group_id
	
=cut

# ============================================================================

sub GroupRevokeUser {
	my ($config, $group_id, $user_to_revoke_id, $force_animator_deletion) = @_;

	$force_animator_deletion = 0 unless defined $force_animator_deletion;

	my $dbh = $config->GetDBH();

	my $group = Mioga2::Old::GroupBase->CreateObject($config, rowid => $group_id);

	if(!$force_animator_deletion and $group->GetAnimId() eq $user_to_revoke_id) {
		throw Mioga2::Exception::Simple("Mioga2::tools::APIGroup", __"Can't revoke the group animator");
	}

	my $transaction = 0;
	if( $dbh->{AutoCommit}) {
		BeginTransaction($dbh);
		$transaction=1;		
	}

	try {
		my $anim_id = $group->GetAnimId();
		ExecSQL($dbh, "UPDATE uri_history set user_id=$anim_id WHERE uri_id in (SELECT rowid FROM m_uri WHERE group_id = $group_id AND user_id = $user_to_revoke_id)");
		ExecSQL($dbh, "UPDATE m_uri set user_id=$anim_id WHERE group_id = $group_id AND user_id = $user_to_revoke_id");
		
		LaunchMethodInApps($config, "RevokeUserFromGroup", $group_id, $user_to_revoke_id, $group->GetAnimId());
		
		ProfileRemoveGroup($dbh, $group_id, $user_to_revoke_id);
		
		ExecSQL($dbh, "DELETE FROM m_group_user_last_conn WHERE group_id = $group_id AND user_id = $user_to_revoke_id");
		ExecSQL($dbh, "DELETE FROM m_group_group WHERE group_id = $group_id AND invited_group_id = $user_to_revoke_id");
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh) if $transaction;
		$err->throw;
	};

	EndTransaction($dbh) if $transaction;
}



# ============================================================================

=head2 GroupRevokeTeam ($config, $group_id, $team_to_revoke_id)

	Revoke the team with id $team_to_revoke_id from the group $group_id
	
=cut

# ============================================================================

sub GroupRevokeTeam {
	my ($config, $group_id, $team_to_revoke_id) = @_;

	my $dbh = $config->GetDBH();

	my $group = Mioga2::Old::GroupBase->CreateObject($config, rowid => $group_id);

	my $user_list = $group->GetGroupMembersNotNamlyInvited($team_to_revoke_id);

	my $transaction = 0;
	if($dbh->{AutoCommit}) {
		BeginTransaction($dbh);
		$transaction=1;		
	}

	LaunchMethodInApps($config, "RevokeTeamFromGroup", $group_id, $team_to_revoke_id);

	try {
		
		foreach my $user_id (@{$user_list->GetRowids()}) {
			LaunchMethodInApps($config, "RevokeUserFromGroup", $group_id, $user_id, $group->GetAnimId());
		}

		ProfileRemoveGroup($dbh, $group_id, $team_to_revoke_id);
		ExecSQL($dbh, "DELETE FROM m_group_user_last_conn WHERE group_id = $group_id AND user_id IN ".
				"        (SELECT invited_group_id FROM m_group_group WHERE group_id = $team_to_revoke_id)");
		ExecSQL($dbh, "DELETE FROM m_group_group WHERE group_id = $group_id AND invited_group_id = $team_to_revoke_id");

	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh) if $transaction;
		$err->throw;
	};

	EndTransaction($dbh) if $transaction;
}



# ============================================================================

=head2 TeamRevokeUser ($config, $team_id, $user_to_revoke_id)

	Revoke the user with id $user_to_revoke_id from the team $team_id
	
=cut

# ============================================================================

sub TeamRevokeUser {
	my ($config, $team_id, $user_to_revoke_id) = @_;

	my $dbh = $config->GetDBH();

	my $team = new Mioga2::Old::Team($config, rowid => $team_id);

	my $group_list = $team->GetGroupsWithUserNotNamlyInvited($user_to_revoke_id);
	my $user_list = $team->GetUsersWithUserNotNamlyInvited($user_to_revoke_id);

	my $transaction = 0;
	if($dbh->{AutoCommit}) {
		BeginTransaction($dbh);
		$transaction=1;		
	}

	try {

		my @rowids = (@{$group_list->GetRowids()}, @{$user_list->GetRowids()});
		my @anim_ids = (@{$group_list->GetAnimIds()}, @{$user_list->GetAnimIds()});
		my $nbgroup = @rowids;

		for(my $i = 0; $i < $nbgroup; $i++) {
			my $group_id = $rowids[$i];
			my $anim_id = $anim_ids[$i];

			LaunchMethodInApps($config, "RevokeUserFromGroup", $group_id, $user_to_revoke_id, $anim_id);
		}

		ExecSQL($dbh, "DELETE FROM m_group_group WHERE group_id = $team_id AND invited_group_id = $user_to_revoke_id");
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh) if $transaction;
		$err->throw;
	};

	EndTransaction($dbh) if $transaction;
}


# ============================================================================

=head2 ResourceRevokeGroup ($dbh, $resource_id, $group_to_revoke_id)

	Revoke the group with id $group_to_revoke_id from the resource $resource_id
	
=cut

# ============================================================================

sub ResourceRevokeGroup {
	my ($config, $resource_id, $group_to_revoke_id) = @_;

	my $dbh = $config->GetDBH();

	my $resource = new Mioga2::Old::Resource($config, rowid => $resource_id);

	my $user_list = $resource->GetGroupMembersNotNamlyInvited($group_to_revoke_id);

	my $transaction = 0;
	if($dbh->{AutoCommit}) {
		BeginTransaction($dbh);
		$transaction=1;		
	}

	try {
		
		foreach my $user_id (@{$user_list->GetRowids()}) {
			LaunchMethodInApps($config, "RevokeUserFromGroup", $resource_id, $user_id, $resource->GetAnimId());
		}

		ProfileRemoveGroup($dbh, $resource_id, $group_to_revoke_id);
		ExecSQL($dbh, "DELETE FROM m_group_group WHERE group_id = $resource_id AND invited_group_id = $group_to_revoke_id");

	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh) if $transaction;
		$err->throw;
	};

	EndTransaction($dbh) if $transaction;
	

}



# ============================================================================

=head2 UserChangeTeamList ($dbh, $user_id, $team_list)

	Change the team list of the given user. 
	
=cut

# ============================================================================

sub UserChangeTeamList {
	my ($config, $user_id, $team_list) = @_;
	print STDERR "UserChangeTeamList()\n" if ($debug);

	my $dbh = $config->GetDBH();

	BeginTransaction($dbh);

	try {
		my $oldlist = SelectMultiple($dbh, "SELECT m_group_group.group_id ".
									 "FROM  m_group_group, m_group, m_group_type ".
									 "WHERE m_group_group.invited_group_id = $user_id AND ".
									 "      m_group.rowid = m_group_group.group_id AND ".
									 "      m_group_type.rowid = m_group.type_id AND ".
									 "      m_group_type.ident = 'team'");

		#
		# add new teams 
		#
		print STDERR "add new team\n" if ($debug);
		foreach my $team (@$team_list) {
			if(! grep {$_->{group_id} == $team} @$oldlist) {
				print STDERR "team = $team  user_id = $user_id\n" if ($debug);
				GroupInviteGroup($dbh, $team, $user_id);
			}
		}
		
		#
		# remove old teams 
		#
		print STDERR "revoke old team\n" if ($debug);
		foreach my $team (@$oldlist) {
			if(! grep {$_ == $team->{group_id}} @$team_list) {
				print STDERR "team = ".$team->{group_id}."  user_id = $user_id\n" if ($debug);
				TeamRevokeUser($config, $team->{group_id}, $user_id);
			}
		}
		
	}
	otherwise {
		my $err = shift;
		print STDERR " UserChangeTeamList err = ".Dumper($err)."\n" if ($debug);
		RollbackTransaction($dbh);
		$err->throw;
	};

	EndTransaction($dbh);
	

}


# ============================================================================

=head2 UserChangeGroupList ($dbh, $user_id, $group_list)

	Change the group list of the given user. 
	
=cut

# ============================================================================

sub UserChangeGroupList {
	my ($config, $user_id, $group_list) = @_;

	my $dbh = $config->GetDBH();

	BeginTransaction($dbh);

	try {
		my $oldlist = SelectMultiple($dbh, "SELECT m_group_group.group_id ".
									 "FROM  m_group_group, m_group, m_group_type ".
									 "WHERE m_group_group.invited_group_id = $user_id AND ".
									 "      m_group.rowid = m_group_group.group_id AND ".
									 "      m_group_type.rowid = m_group.type_id AND ".
									 "      m_group_type.ident = 'group'");

		#
		# add new groups 
		#
		foreach my $group (@$group_list) {
			if(! grep {$_->{group_id} == $group} @$oldlist) {
				GroupInviteGroup($dbh, $group, $user_id);
			}
		}
		
		#
		# remove old groups 
		#
		foreach my $group (@$oldlist) {
			if(! grep {$_ == $group->{group_id}} @$group_list) {
				GroupRevokeUser($config, $group->{group_id}, $user_id);
			}
		}
		
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};

	EndTransaction($dbh);
	

}


# ============================================================================

=head2 GroupChangeUserList ($dbh, $group_id, $user_list)

	Change the user list of the given group. 
	
=cut

# ============================================================================

sub GroupChangeUserList {
	my ($config, $group_id, $user_list) = @_;

	my $dbh = $config->GetDBH();

	BeginTransaction($dbh);

	try {
		my $oldlist = SelectMultiple($dbh, "SELECT m_group_group.invited_group_id ".
									 "FROM  m_group_group, m_user_base ".
									 "WHERE m_group_group.group_id = $group_id AND ".
									 "      m_user_base.rowid = m_group_group.invited_group_id");

		#
		# add new users 
		#
		foreach my $user (@$user_list) {
			if(! grep {$_->{invited_group_id} == $user} @$oldlist) {
				GroupInviteGroup($dbh, $group_id, $user);
			}
		}
		
		#
		# remove old users 
		#
		foreach my $user (@$oldlist) {
			if(! grep {$_ == $user->{invited_group_id}} @$user_list) {
				GroupRevokeUser($config, $group_id, $user->{invited_group_id});
			}
		}
		
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};

	EndTransaction($dbh);
	
}


# ============================================================================

=head2 GroupChangeTeamList ($dbh, $group_id, $team_list)

	Change the team list of the given group. 
	
=cut

# ============================================================================

sub GroupChangeTeamList {
	my ($config, $group_id, $team_list) = @_;

	my $dbh = $config->GetDBH();

	BeginTransaction($dbh);

	try {
		my $oldlist = SelectMultiple($dbh, "SELECT m_group_group.invited_group_id ".
									 "FROM  m_group_group, m_group, m_group_type ".
									 "WHERE m_group_group.group_id = $group_id AND ".
									 "      m_group.rowid = m_group_group.invited_group_id AND ".
									 "      m_group_type.rowid = m_group.type_id AND ".
									 "      m_group_type.ident = 'team'");

		#
		# add new teams 
		#
		foreach my $team (@$team_list) {
			if(! grep {$_->{invited_group_id} == $team} @$oldlist) {
				GroupInviteGroup($dbh, $group_id, $team);
			}
		}
		
		#
		# remove old teams 
		#
		foreach my $team (@$oldlist) {
			if(! grep {$_ == $team->{invited_group_id}} @$team_list) {
				GroupRevokeTeam($config, $group_id, $team->{invited_group_id});
			}
		}
		
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};

	EndTransaction($dbh);
	

}


# ============================================================================

=head2 TeamChangeGroupList ($dbh, $team_id, $group_list)

    Change the group list of the given team. 
    
=cut

# ============================================================================

sub TeamChangeGroupList {
    my ($config, $team_id, $group_list) = @_;

    my $dbh = $config->GetDBH();

    BeginTransaction($dbh);

    try {
        my $oldlist = SelectMultiple($dbh, "SELECT m_group_group.group_id ".
                                     "FROM  m_group_group, m_group, m_group_type ".
                                     "WHERE m_group_group.invited_group_id = $team_id AND ".
                                     "      m_group.rowid = m_group_group.group_id AND ".
                                     "      m_group_type.rowid = m_group.type_id AND ".
                                     "      m_group_type.ident = 'group'");

        #
        # add new groups 
        #
        foreach my $group (@$group_list) {
            if(! grep {$_->{group_id} == $group} @$oldlist) {
                GroupInviteGroup($dbh, $group, $team_id);
            }
        }
        
        #
        # remove old groups 
        #
        foreach my $group (@$oldlist) {
            if(! grep {$_ == $group->{group_id}} @$group_list) {
                GroupRevokeTeam($config, $group->{group_id}, $team_id);
            }
        }
        
    }
    otherwise {
        my $err = shift;
        RollbackTransaction($dbh);
        $err->throw;
    };

    EndTransaction($dbh);
    

}

# ============================================================================

=head2 TeamChangeUserList ($dbh, $team_id, $user_list)

	Change the user list of the given team. 
	
=cut

# ============================================================================

sub TeamChangeUserList {
	my ($config, $team_id, $user_list) = @_;

	my $dbh = $config->GetDBH();

	BeginTransaction($dbh);

	try {
		my $oldlist = SelectMultiple($dbh, "SELECT m_group_group.invited_group_id ".
									 "FROM  m_group_group, m_user_base ".
									 "WHERE m_group_group.group_id = $team_id AND ".
									 "      m_user_base.rowid = m_group_group.invited_group_id");


		#
		# add new users 
		#
		foreach my $user (@$user_list) {
			if(! grep {$_->{invited_group_id} == $user} @$oldlist) {
				GroupInviteGroup($dbh, $team_id, $user);
			}
		}
		
		#
		# remove old users 
		#
		foreach my $user (@$oldlist) {
			if(! grep {$_ == $user->{invited_group_id}} @$user_list) {
				TeamRevokeUser($config, $team_id, $user->{invited_group_id});
			}
		}
		
	}
	otherwise {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};

	EndTransaction($dbh);
}
# ============================================================================

=head2 GroupSetLastConnectionDate ($dbh, $group_id, $user_id)

	Update the last connection date for user $user_id in group $group_id 
	
=cut

# ============================================================================

sub GroupSetLastConnectionDate {
	my ($dbh, $group_id, $user_id) = @_;


	my $res = ExecSQL($dbh, "UPDATE m_group_user_last_conn SET last_connection = now() WHERE group_id = $group_id AND user_id  = $user_id");
}
# ============================================================================

=head2 SkelApplyDefaultGroup ($config, $group_values)
	
	Apply the default group skeleton 

=cut

# ============================================================================
sub SkelApplyDefaultGroup
{
	my ($conf, $grp) = @_;

	my $file = $conf->GetSkeletonDir(). "/system/group.xml";
	my $xmltree = SkelApplyGroup($conf, $grp, $file);
	
}
# ============================================================================

=head2 SkelApplyGroup ($config, $group_values, $skel_file)
	
	Apply the group skeleton file $skel_file.
	$group_values contains group values.

=cut

# ============================================================================

sub SkelApplyGroup
{
	my ($conf, $grp, $skel_file) = @_;

	my $dir;
	
	if ($skel_file ne "" and -f $skel_file)
	{
		my $xmltree = SkelApplyGroupBase($conf, $grp, $skel_file);
		
		if (exists($xmltree->{initialize_applications}))
		{
			SkelGroupApplicationInit($conf, $grp, $xmltree->{initialize_applications}->[0]);
		}
	}

	if(-d "${skel_file}.d") {
		foreach my $file (<${skel_file}.d/*>) {
			SkelApplyGroup($conf, $grp, $file);
		}
	}
}




# ============================================================================

=head2 SkelApplyDefaultUser ($config, $user_values)
	
	Apply the default user skeleton 

=cut

# ============================================================================

sub SkelApplyDefaultUser
{
	my ($conf, $usr) = @_;

	my $file = $conf->GetSkeletonDir(). "/system/user.xml";
	my $xmltree = SkelApplyUser($conf, $usr, $file);
	
}


# ============================================================================

=head2 SkelApplyUser ($config, $user_values, $skel_file)
	
	Apply the user skeleton 

=cut

# ============================================================================

sub SkelApplyUser
{
	my ($conf, $usr, $skel_file) = @_;

	my $dir;
	
	if (defined $skel_file and -f $skel_file)
	{
		my $xmltree = SkelApplyGroupBase($conf, $usr, $skel_file);
		
		if (exists($xmltree->{initialize_applications}))
		{
			SkelUserApplicationInit($conf, $usr, $xmltree->{initialize_applications}->[0]);
		}
	}

	if(-d "${skel_file}.d") {
		foreach my $file (<${skel_file}.d/*>) {
			SkelApplyUser($conf, $usr, $file);
		}
	}
}


# ============================================================================

=head2 SkelApplyDefaultResource ($config, $resource_values)
	
	Apply the default resource skeleton 

=cut

# ============================================================================

sub SkelApplyDefaultResource
{
	my ($conf, $res) = @_;

	my $lang = SelectSingle ($conf->GetDBH (), "SELECT ident FROM m_lang WHERE rowid = $res->{lang_id};")->{ident};

	my $file = $conf->GetSkeletonDir(). "/$lang/system/resource.xml";
	my $xmltree = SkelApplyResource($conf, $res, $file);
	
}


# ============================================================================

=head2 SkelApplyResource ($config, $res_values)
	
	Apply the resource skeleton 

=cut

# ============================================================================

sub SkelApplyResource
{
	my ($conf, $res, $skel_file) = @_;

	my $dir;
	
	if ($skel_file ne "" and -f $skel_file)
	{
		my $xmltree = SkelApplyGroupBase($conf, $res, $skel_file);
		
		if (exists($xmltree->{initialize_applications}))
		{
			SkelResourceApplicationInit($conf, $res, $xmltree->{initialize_applications}->[0]);
		}
	}

	if(-d "${skel_file}.d") {
		foreach my $file (<${skel_file}.d/*>) {
			SkelApplyResource($conf, $res, $file);
		}
	}
	
}


# ============================================================================

=head2 SkelGroupApplicationInit ($conf, $usr, $init_apps)
	
	Apply the group skeleton 

=cut

# ============================================================================

sub SkelGroupApplicationInit
{
	my ($conf, $grp, $init_apps) = @_;

	foreach my $application (@{$init_apps->{application}})
	{
		my $app_name = $application->{name};
		my $app_desc = new Mioga2::AppDesc ($conf,ident => $app_name);
		my $app = $app_desc->CreateApplication();
		$app->InitializeGroupApplication($conf, $grp, $application);
	}
}


# ============================================================================

=head2 SkelUserApplicationInit ($conf, $usr, $init_apps)
	
	Apply the user skeleton 

=cut

# ============================================================================

sub SkelUserApplicationInit
{
	my ($conf, $usr, $init_apps) = @_;

	foreach my $application (@{$init_apps->{application}})
	{
		my $app_name = $application->{name};
		my $app_desc = new Mioga2::AppDesc ($conf,ident => $app_name);
		my $app = $app_desc->CreateApplication();
		$app->InitializeUserApplication($conf, $usr, $application);
	}
}



# ============================================================================

=head2 SkelResourceApplicationInit ($conf, $res, $init_apps)
	
	Apply the resource skeleton 

=cut

# ============================================================================

sub SkelResourceApplicationInit
{
	my ($conf, $res, $init_apps) = @_;

	foreach my $application (@{$init_apps->{application}})
	{
		my $app_name = $application->{name};
		my $app_desc = new Mioga2::AppDesc ($conf,ident => $app_name);
		my $app = $app_desc->CreateApplication();
		$app->InitializeResourceApplication($conf, $res, $application);
	}
}



# ============================================================================

=head2 SkelApplyGroupBase ($config, $grp, $file)
	
	Apply the user skeleton 

=cut

# ============================================================================

sub SkelApplyGroupBase {
	my ($conf, $grp, $file) = @_;

	my $dbh = $conf->GetDBH();

	my $data = "";

	open(FILE, "<$file")
		or die "Can't open $file : $!";
	while (<FILE>) { $data .= $_ };
	close (FILE);

	$data =~ s/<--!.*?-->//gsm; # remove comments

	my $xml		  = new Mioga2::XML::Simple(forcearray => 1);
	my $xmltree  = $xml->XMLin($data);
	my $skel_name = (exists($xmltree->{'name'}))? $xmltree->{'name'} : "";

	return SkelApplyGroupBaseXMLTree($conf, $grp, $xmltree);
}



# ============================================================================

=head2 SkelApplyGroupBaseXMLTree ($config, $group, $xmltree)

	Apply skeleton on xmltree.

=cut

# ============================================================================

sub SkelApplyGroupBaseXMLTree {
	my ($conf, $grp, $xmltree) = @_;

	# -----------------------------------------------------------
	# setting available applications
	# -----------------------------------------------------------
	SkelEnableApplications($conf, $grp, $xmltree);
	SkelDisableApplications($conf, $grp, $xmltree);

	# -----------------------------------------------------------
	# setting profiles
	# -----------------------------------------------------------
	SkelCreateProfiles($conf, $grp, $xmltree);

	SkelChangeProfiles($conf, $grp, $xmltree);

	SkelSetDefaultProfile($conf, $grp, $xmltree);

	# -----------------------------------------------------------
	# invite users
	# -----------------------------------------------------------
	SkelInviteUsersGroups($conf, $grp, $xmltree);

	# -----------------------------------------------------------
	# add group to be create in a user private space, team or 
	# group
	# -----------------------------------------------------------
	SkelAddUsersGroups($conf, $grp, $xmltree);

	# -----------------------------------------------------------
	# delete profiles
	# -----------------------------------------------------------
	SkelDeleteProfiles($conf, $grp, $xmltree);

	# -----------------------------------------------------------
	# Create file and dir
	# -----------------------------------------------------------
	SkelCreateFileDir($conf, $grp, $xmltree);
	
	# -----------------------------------------------------------
	# Apply file rights
	# -----------------------------------------------------------
	SkelApplyFileRights($conf, $grp, $xmltree);
	

	return $xmltree;
}


# ============================================================================

=head2 SkelEnableApplications ($config, $group, $xmltree)

	Apply Application activation skeletons

=cut

# ============================================================================

sub SkelEnableApplications {
	my ($conf, $grp, $xmltree) = @_;

	my $dbh = $conf->GetDBH();

	if (exists $xmltree->{'available_applications'})
	{
		foreach my $app(@{$xmltree->{'available_applications'}->[0]->{'app'}})
		{
			try {
				my $app_desc = new Mioga2::AppDesc($conf, ident  => $app->{'name'});
				my $app_rowid = $app_desc->GetRowid();
				
				try {
					ApplicationEnableInGroup($dbh, $app_rowid, $grp->{rowid});
				}
				
				otherwise {
					# ignore already enabled apps
				};
			}
			catch Mioga2::Exception::Application with {
				print STDERR "GroupSkel: Application '$app->{name}' doesn't exist !! \n";
			};
		}
	}

}



# ============================================================================

=head2 SkelDisableApplications ($config, $group, $xmltree)

	Apply Application activation skeletons

=cut

# ============================================================================

sub SkelDisableApplications {
	my ($conf, $grp, $xmltree) = @_;

	my $dbh = $conf->GetDBH();

	if (exists $xmltree->{'disable_applications'})
	{
		foreach my $app(@{$xmltree->{'disable_applications'}->[0]->{'app'}})
		{
			try {
				my $app_desc = new Mioga2::AppDesc($conf, ident  => $app->{'name'});
				my $app_rowid = $app_desc->GetRowid();
				
				try {
					ApplicationDisableInGroup($dbh, $app_rowid, $grp->{rowid});
				}
				
				otherwise {
					# ignore already disabled apps
				};
			}
			catch Mioga2::Exception::Application with {
				print STDERR "GroupSkel: Application '$app->{name}' doesn't exist !! \n";
			};
		}
	}

}



# ============================================================================

=head2 SkelSetDefaultProfile ($config, $group, $xmltree)

	Apply default profile skeletons

=cut

# ============================================================================

sub SkelSetDefaultProfile {
	my ($conf, $grp, $xmltree) = @_;

	my $dbh = $conf->GetDBH();

	if (exists $xmltree->{'default_profile'})
	{
		my $def_prof_name = $xmltree->{'default_profile'}->[0]->{name};

		my $res = SelectSingle($dbh, "SELECT * FROM m_profile WHERE ident = '$def_prof_name' AND group_id = $grp->{rowid}");
		my $profile_id = $res->{rowid};

		if (defined $profile_id)
		{
			ExecSQL($dbh, "UPDATE m_group_base SET default_profile_id = $profile_id WHERE rowid = $grp->{rowid}");
			$grp->{default_profile_id} = $profile_id;
		}
		else
		{
			print STDERR "GroupSkel : cannot set default profile to '$def_prof_name' : unknown profile\n";
		}
	}
}


# ============================================================================

=head2 SkelCreateProfiles ($config, $group, $xmltree)

	Apply profile creation skeletons

=cut

# ============================================================================

sub SkelCreateProfiles {
	my ($conf, $grp, $xmltree) = @_;

	my $dbh = $conf->GetDBH();

	if (exists $xmltree->{'create_profile'})
	{
		foreach my $prof (@{$xmltree->{'create_profile'}})
		{
			my $profile_name = $prof->{'name'}->[0];

			try {

				my $profile_id = ProfileCreate($dbh, $profile_name, $grp->{rowid}, 0);
				SkelEnableApplicationInProfile($conf, $grp, $profile_id, $prof);

			} 
			
			catch Mioga2::Exception::DB with {
				warn Dumper(shift);
				print STDERR "A profile named $profile_name already exists\n";
			}
			
			otherwise {
				my $err = shift;

				print STDERR "Error while $profile_name profile creation\n";
				warn Dumper($err);
			};
		}
	}
}


# ============================================================================

=head2 SkelCreateProfiles ($config, $group, $xmltree)

	Apply profile modification skeletons

=cut

# ============================================================================

sub SkelChangeProfiles {
	my ($conf, $grp, $xmltree) = @_;

	my $dbh = $conf->GetDBH();

	if (exists $xmltree->{'change_profile'})
	{

		foreach my $prof (@{$xmltree->{'change_profile'}})
		{
			my $profile_name = $prof->{'name'}->[0];

			try {

				my $res = SelectSingle($dbh, "SELECT * FROM m_profile ".
									   "WHERE ident = '$profile_name' AND group_id = $grp->{rowid}");


				next unless (defined $res);

				my $profile_id = $res->{rowid};
				
				SkelEnableApplicationInProfile($conf, $grp, $profile_id, $prof);
			} 
			
			catch Mioga2::Exception::DB with {
				warn Dumper(shift);
				print STDERR "A profile named $profile_name already exists\n";
			}
			
			otherwise {
				my $err = shift;

				print STDERR "Error while $profile_name profile creation\n";
				warn Dumper($err);
			};
		}
	}
}

# ============================================================================

=head2 SkelEnableApplicationInProfile ($config, $group, $xmltree)

	Apply profile application creation skeletons

=cut

# ============================================================================


sub SkelEnableApplicationInProfile {
	my ($conf, $grp, $profile_id, $prof) = @_;
	
	my $dbh = $conf->GetDBH();
	
	if (exists($prof->{'application'}))
	{
		foreach my $app (@{$prof->{'application'}})
		{
			
			try {
				my $app_desc = new Mioga2::AppDesc($conf, ident => $app->{'name'}->[0]);
				my $app_rowid = $app_desc->GetRowid();
				
				# create function list if all_functions tag exists
				# ------------------------------------------------
				
				if (exists($app->{'all_functions'}))
				{
					$app->{'function'} = $app_desc->GetFunctions();
				}
				
				foreach my $func (@{$app->{'function'}})
				{
					my $func_id = $app_desc->GetFunctionsIdsFromIdent($func);
					
					
					if ($func_id)
					{
						ProfileCreateFunction($dbh, $profile_id, $func_id);
					}
					else
					{
						print STDERR "GroupSkel : Function '$func' doesn't belong to app '$app->{name}->[0]' !!! \n";
					}
				}
			}
			
			catch Mioga2::Exception::Application with {
				print STDERR "Unknown application $app->{'name'}->[0]\n";
			};
			
		}
	}
}



# ============================================================================

=head2 SkelDeleteProfiles ($config, $group, $xmltree)

	Apply profile delete skeletons

=cut

# ============================================================================

sub SkelDeleteProfiles {
	my ($conf, $grp, $xmltree) = @_;

	my $dbh = $conf->GetDBH();

	if (exists $xmltree->{'delete_profile'})
	{
		foreach my $prof (@{$xmltree->{'delete_profile'}})
		{
			my $profile_name = $prof->{'name'}->[0];

			try {

				my $res = SelectSingle($dbh, "SELECT * FROM m_profile ".
									   "WHERE ident = '$profile_name' AND group_id = $grp->{rowid}");


				next unless (defined $res);

				ProfileDelete($dbh, $res->{rowid});

			} 
			
			catch Mioga2::Exception::DB with {
				my $err =shift;
				
				print STDERR "A profile named $profile_name already exists\n";
			}
			
			otherwise {
				my $err = shift;

				print STDERR "Error while $profile_name profile creation\n";
				warn Dumper($err);
			};
		}
	}
}



# ============================================================================

=head2 SkelInviteUsersGroups ($config, $group, $xmltree)

	Apply user/group invitation skeleton

=cut

# ============================================================================

sub SkelInviteUsersGroups {
	my ($conf, $grp, $xmltree) = @_;

	my $animator = new Mioga2::Old::User($conf, rowid => $grp->{anim_id});
	my $dbh = $conf->GetDBH();

	if (exists $xmltree->{'invite_user'})
	{
		
		foreach my $user(@{$xmltree->{'invite_user'}->[0]->{'user'}})
		{

			try {
				my $ident = $user->{ident};
				
				my $user_obj;
				my $is_anim = 0;
				
				if($ident eq '$$anim_ident$$') {
					$user_obj = $animator;
					$is_anim = 1;
				}
				else {
					$user_obj = new Mioga2::Old::User($conf, ident => $ident);
				}
				

				SkelInviteGroupBase($conf, $grp, $user, $user_obj, $is_anim);
			}

			catch Mioga2::Exception::User with {
				print STDERR "GroupSkel: User with ident '$user->{ident}' doesn't exist !! \n";
			};
		}
	}


	if (exists $xmltree->{'invite_team'})
	{
		
		foreach my $group(@{$xmltree->{'invite_team'}->[0]->{'team'}})
		{

			try {
				my $ident = $group->{ident};
				
				my $group_obj;
				
				$group_obj = new Mioga2::Old::Team($conf, ident => $ident);

				SkelInviteGroupBase($conf, $grp, $group, $group_obj);
			}

			catch Mioga2::Exception::Group with {
				print STDERR "GroupSkel: Team with ident '$group->{ident}' doesn't exist !! \n";
			};
		}
	}
}


# ============================================================================

=head2 SkelInviteGroupBase ($config, $group, $usertree, $user_obj)

	Apply group_base invitation skeleton

=cut

# ============================================================================

sub SkelInviteGroupBase {
	my ($conf, $grp, $group, $group_obj, $is_anim) = @_;

	$is_anim = 0 unless defined $is_anim;

	my $dbh = $conf->GetDBH();

	my $group_id  = $group_obj->GetRowid();
	
	my $profile_id = 0;
	
	if(exists $group->{'profile'}) {
		my $res = SelectSingle($dbh, "SELECT * FROM m_profile WHERE ident = '$group->{profile}' AND group_id = $grp->{rowid}");
		$profile_id = $res->{rowid};
	}
	
	else {
		$profile_id = $grp->{default_profile_id};
	}
	
	my $res = SelectSingle($dbh, "SELECT * FROM m_group_group WHERE group_id = $grp->{rowid} AND ".
						   "                                  invited_group_id = $group_id");

	if(! defined $res) {
		GroupInviteGroup($dbh, $grp->{rowid}, $group_id);
	}
	
	if (defined $profile_id and $profile_id)
	{
		ProfileAddGroup($dbh, $profile_id, $grp->{rowid}, $group_id);
	}
}




# ============================================================================

=head2 SkelAddUsersGroups ($config, $group, $xmltree)

	Apply user/group invitation skeleton

=cut

# ============================================================================

sub SkelAddUsersGroups {
	my ($conf, $grp, $xmltree) = @_;

	my $animator = new Mioga2::Old::User($conf, rowid => $grp->{anim_id});
	my $dbh = $conf->GetDBH();

	if (exists $xmltree->{'add_in_group'})
	{
		
		foreach my $group(@{$xmltree->{'add_in_group'}->[0]->{'group'}})
		{

			try {
				my $ident = $group->{ident};
				
				my $group_obj;
				$group_obj = Mioga2::Old::GroupBase->CreateObject($conf, ident => $ident);

				GroupInviteGroup($dbh, $group_obj->GetRowid(), $grp->{rowid});

				if(exists $group->{profile}) {
					my $res = SelectSingle($dbh, "SELECT * FROM m_profile ".
										   "WHERE ident = '$group->{profile}' AND ".
										   "      group_id = ".$group_obj->GetRowid());

					my $profile_id = $res->{rowid};
					ProfileAddGroup($dbh, $profile_id, $group_obj->GetRowid(), $grp->{rowid});
				}
			}

			catch Mioga2::Exception::Group with {
				print STDERR "GroupSkel: Team with ident '$group->{ident}' doesn't exist !! \n";
			};
		}
	}
}


# ============================================================================

=head2 SkelCreateFileDir ($config, $group, $xmltree)

	Apply Files ACLs

=cut

# ============================================================================
	
sub SkelCreateFileDir {
	my ($conf, $grp, $xmltree) = @_;

	my $dbh = $conf->GetDBH();


	if(exists $xmltree->{create_dir}) {

		foreach my $dir (@{$xmltree->{create_dir}}) {
			my $uri;
			my $path;

			if($dir->{space} eq 'private') {
				$uri = $conf->GetPrivateURI()."/".$grp->{ident};
				$path = $conf->GetPrivateDir()."/".$grp->{ident};
			}
			elsif(exists $grp->{public_part} and $grp->{public_part}) {
				$uri = $conf->GetPublicURI()."/".$grp->{ident};
				$path = $conf->GetPublicDir()."/".$grp->{ident};
			}
			else {
				next;
			}

			my $name = $dir->{name};

                        $uri  .= "/".$name;
                        $path .= "/".$name;

			
			mkpath($path);
			my $size = (-s $path) || 0;
			AuthzDeclareURI($dbh, $uri, $grp->{anim_id}, 'directory', $size);
		}
	}

	if(exists $xmltree->{create_file}) {

		my $admin_home = $conf->GetPrivateDir()."/".$conf->GetAdminIdent();

		foreach my $dir (@{$xmltree->{create_file}}) {
			my $uri;
			my $path;

			if($dir->{space} eq 'private') {
				$uri = $conf->GetPrivateURI()."/".$grp->{ident};
				$path = $conf->GetPrivateDir()."/".$grp->{ident};
			}
			elsif(exists $grp->{public_part} and $grp->{public_part}) {
				$uri = $conf->GetPublicURI()."/".$grp->{ident};
				$path = $conf->GetPublicDir()."/".$grp->{ident};
			}
			else {
				next;
			}
			my $name = $dir->{name};

			$uri  .= "/".$name;
			$path .= "/".$name;

			my $group_ident = $grp->{'ident'};
			my $group_id = $grp->{'rowid'};

			open(F_FILE, "$admin_home/$dir->{src}")
				or throw Mioga2::Exception::Simple("APIGroup::SkelCreateFileDir", 
                __x("Can't open {admin_home}/{directory}: {error}", admin_home => $admin_home, directory => $dir->{src}, error => $!));

			my $data;
			{
				local $/ = undef;
				$data = <F_FILE>;
			}

			close(F_FILE);

			$data =~ s/\$\$(.*?)\$\$/eval("\$$1")||""/ge;
			
			open(F_FILE, ">$path")
				or throw Mioga2::Exception::Simple("APIGroup::SkelCreateFileDir", 
                __x("Can't open {path}: {error}", path => $path, error => $!));
			
			print F_FILE $data;

			close(F_FILE);
			
      my $mime = mimetype($path);
      utf8::encode($mime);
      $mime  = 'directory' if $mime =~ /directory/;
      my $size = (-s $path) || 0;
			AuthzDeclareURI($dbh, $uri, $grp->{anim_id}, $mime, $size);
		}
	}
}



# ============================================================================

=head2 SkelApplyFileRights ($config, $group, $usertree)

	Apply Files ACLs

=cut

# ============================================================================
	
sub SkelApplyFileRights {
	my ($conf, $grp, $xmltree) = @_;

	if(exists $xmltree->{root_acl}) {

		foreach my $root_acl (@{$xmltree->{root_acl}}) {
			my $uri;
			
			if($root_acl->{space} eq 'private') {
				$uri = $conf->GetPrivateURI()."/".$grp->{ident};
			}
			elsif(exists $grp->{public_part} and $grp->{public_part}) {
				$uri = $conf->GetPublicURI()."/".$grp->{ident};
			}
			else {
				next;
			}
			
			SkelApplyAcls($conf, $grp, $root_acl, $uri);
		}
	}


	if(exists $xmltree->{dirfile}) {
		foreach my $acl (@{$xmltree->{dirfile}}) {
			my $uri;
			if($acl->{space} eq 'private') {
				$uri = $conf->GetPrivateURI()."/".$grp->{ident};
			}
			elsif(exists $grp->{public_part} and $grp->{public_part}) {
				$uri = $conf->GetPublicURI()."/".$grp->{ident};
			}
			else {
				next;
			}
			
			my $name = $acl->{uri};

			$uri .= "/".$name;

			$uri =~ s!//!/!g;
			
			SkelApplyAcls($conf, $grp, $acl, $uri);
		}
	}

}


# ============================================================================

=head2 SkelApplyAcls ($config, $group, $acltree, $uri)

	Apply ACL on uri

=cut

# ============================================================================

sub SkelApplyAcls {
	my ($conf, $grp, $acltree, $uri) = @_;

	my $dbh = $conf->GetDBH();

	my $q_uri = st_FormatPostgreSQLString($uri);
	my $uri_data = SelectSingle($dbh, "SELECT * FROM m_uri WHERE uri = '$q_uri'");

	if(!defined $uri_data) {
		warn "Unknown uri $uri";
		return;
	}

	my $uri_id = $uri_data->{rowid};

	AuthzDuplicateParentAuthz($dbh, $uri_id);
	
	if(exists $acltree->{inherite} and $acltree->{inherite} == 0) {
		AuthzDisableURIInheritance($dbh, $uri_id);
	}
	

	if(exists $acltree->{default_acl}) {
		my $profiles = SelectMultiple($dbh, "SELECT * FROM m_profile WHERE group_id = $uri_data->{group_id}");

		foreach my $prof (@$profiles) {
			AuthzUpdateAuthzForProfile($dbh, $uri_id, $prof->{rowid}, AclToId($acltree->{default_acl}));
		}
	}

	if(exists $acltree->{acl}) {
		foreach my $acl (@{$acltree->{acl}}) {
			if(exists $acl->{profile}) {
				my $profile_id = SelectSingle($dbh, "SELECT * FROM m_profile WHERE ident = '$acl->{profile}' AND group_id = $grp->{rowid}");
				
				if(!defined $profile_id) {
					warn "Bad profile : $acl->{profile}";
					next;
				}
				
				$profile_id = $profile_id->{rowid};
				
				AuthzUpdateAuthzForProfile($dbh, $uri_id, $profile_id, AclToId($acl->{acl}));
			}
			
			if(exists $acl->{team}) {
				try {
					my $team = new Mioga2::Old::Team($conf, ident => $acl->{team});
					
					AuthzAddAuthzForTeam($dbh, $uri_id, $team->GetRowid(), AclToId($acl->{acl}));
					AuthzUpdateAuthzForTeam($dbh, $uri_id, $team->GetRowid(), AclToId($acl->{acl}));
				}
				otherwise {
					warn "Bad team : $acl->{team}";
				};
			}
			
			if(exists $acl->{user}) {
				try {
					my $user_id;
					if($acl->{user} eq '$$anim_ident$$') {
						$user_id = $grp->{anim_id};
					}
					else {
						my $user = new Mioga2::Old::User($conf, ident =>  $acl->{user});
						$user_id = $user->GetRowid();
					}

					AuthzAddAuthzForUser($dbh, $uri_id, $user_id, AclToId($acl->{acl}));
					AuthzUpdateAuthzForUser($dbh, $uri_id, $user_id, AclToId($acl->{acl}));
				}
				otherwise {
					warn Dumper(shift);
					warn "Bad user : $acl->{user}";
				};
			}
		}
	}
}


# ============================================================================

=head2 AclToId ($aclstr)

	Return ACL id from aclstr.

=cut

# ============================================================================

sub AclToId {
	my ($acl) = @_;

	my %hash = (none => 0, read => 1, readwrite => 2);

	if(!exists $hash{$acl}) {
		throw Mioga2::Exception::Simple("Mioga2::tools::APIGroup", __x("Bad acl {acl}", acl => $acl));
	}

	return $hash{$acl};
}



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 UserModifyPassword ($config, $values)

	Crypt user password according to mioga configuration

=cut

# ============================================================================

sub UserModifyPassword {
	my ($config, $values) = @_;
	print STDERR "UserModifyPassword\n" if($debug);

	# BUG #1350 - Check if password string is already encrypted, otherwise, encrypt it
	if ($values->{password} !~ /^{[\w]+}/) {
		my $crypter = $config->LoadPasswordCrypter();
		$values->{password} = $crypter->CryptPassword($config, $values);
	}

	if($values->{type} ne 'local_user') {
		$values->{password_expiration_date} = strftime("%Y-%m-%d %H:%M:%S", gmtime(time + 600));
	}

	
	my ($type) = ($values->{type} =~ /^(.*)_user$/);
    if ( ($type eq 'external_user') || (($type eq 'ldap') && (exists $values->{dont_modify_on_ldap}) && ($values->{dont_modify_on_ldap} ne 'yes')) ) {
		print STDERR "UserModifyPassword push password modify type = $type\n" if($debug);
		my $passmod;
		eval "require Mioga2::Password::Modifier::$type; \$passmod = new Mioga2::Password::Modifier::$type; ";

		if(defined $passmod) {
			$passmod->ModifyPassword($config, $values);
		}
		else {
			warn "passmod not defined : $@";
		}
	}
	else {
		print STDERR "UserModifyPassword ldap user and dont_modify_on_ldap eq yes\n" if($debug);
	}
}

# ============================================================================

=head2 InitializeGroupValues ($config, $values)

	Set the group default values from Mioga configuration 
	for fields not yet defined in $values

=cut

# ============================================================================

sub InitializeGroupValues {
	my ($config, $values) = @_;

	my $dbh = $config->GetDBH();
	
	$values->{mioga_id} = $config->GetMiogaId();

	#
	# Check ident unicity
	#
	my $idents = SelectSingle($dbh, 
							  "SELECT count(*) ".
							  "FROM m_group_base ".
							  "WHERE ident = '".st_FormatPostgreSQLString($values->{ident})."' AND ".
							  "      mioga_id = $values->{mioga_id}");

	if($idents->{count} > 0) {
		throw Mioga2::Exception::Simple("APIGroup::InitializeGroupValues",
        __x("Ident is already in use: {ident}", ident => $values->{ident}));
	}

	#
	# Initialize default values
	#
	if(! exists $values->{theme_id}) {
		$values->{theme_id} = $config->GetDefaultThemeId();

		if(! defined $values->{theme_id}) {
			$values->{theme_id} = "NULL";
		}
	}
	if(! exists $values->{lang_id}) {
		$values->{lang_id} = $config->GetDefaultLangId();

		if(! defined $values->{lang_id}) {
			$values->{lang_id} = "NULL";
		}
	}
	
	foreach my $field (qw( public_part autonomous)) {
		if(! exists $values->{$field}) {
			my $func = $field;
			$func =~ s/(^|_)(.)/ucfirst($2)/ge;
			$func = "IsDefault$func";

			if($config->$func()) {
				$values->{$field} = 1;
			}
			else {
				$values->{$field} = 0;
			}
		}
	}

	$values->{created} = "now()";
	$values->{modified} = "now()";
	

	$values->{disk_space_used} = 0;
}


# ============================================================================

=head2 InitializeUserValues ($config, $values)

	Set the user default values from Mioga configuration 
	for fields not yet defined in $values

=cut

# ============================================================================

sub InitializeUserValues {
	my ($config, $values) = @_;

	my $dbh = $config->GetDBH();

	if (!defined ($values->{status_id})) {
		my $res     = SelectSingle($dbh, "SELECT rowid FROM m_user_status
									  WHERE ident = 'active'");
		$values->{status_id} = $res->{rowid};
	}


	foreach my $field (qw(working_days time_per_day)) {

		my $func = $field;
		$func =~ s/(^|_)(.)/ucfirst($2)/ge;
		$func = "GetDefault$func";

		if(! exists $values->{$field}) {
			$values->{$field} = $config->$func();
		}
	}
	

	if(! exists $values->{monday_first}) {
		if($config->IsDefaultMondayFirst()) {
			$values->{monday_first} = 1;
		}
		else {
			$values->{monday_first} = 0;
		}
	}


	if(! exists $values->{public_part}) {
		$values->{public_part} = $config->IsDefaultPublicPart();
	}
	
	if(! exists $values->{timezone_id}) {
		my $res =  SelectSingle($dbh, "SELECT rowid FROM m_timezone
	                               WHERE ident = '".$config->GetDefaultTimezone()."'");

		if(defined $res) {
			$values->{timezone_id} = $res->{rowid};
		}
		else {
			$values->{timezone_id} = "NULL";
		}
	}
	
}


# ============================================================================

=head2 CreateUser ($config, $user, $type, $external_values)

	Add a new user of type $type (local or ldap) into database.
	
	$user is a user description. 
	Values not set in this structure are set to default values 
	from Mioga configuration.

	This function :

	=over 4

	=item -
	Create the group in database

	=item -
	Run InitializeGroup

	=item -
	Apply the user skeletons

	=back

	
=cut

# ============================================================================

sub CreateUser {
	my ($config, $user, $type, $no_transaction) = @_;

	$no_transaction = 0 unless defined $no_transaction;
	
	if(! grep { $_ eq $type } qw(local ldap external) ) {
		throw Mioga2::Exception::Simple("APIGroup::CreateUser",
        __x("Unknown user type: {type}", type => $type));
	}

	my $dbh = $config->GetDBH();
	
	if(! $no_transaction) {
		BeginTransaction($dbh);
	}

	my %values;
	try {

		# ------------------------------------------------------
		# Set default values from Mioga configuration.
		# ------------------------------------------------------
		%values = %$user;
		InitializeGroupValues($config, \%values);
		InitializeUserValues($config, \%values);
		
		my $res = SelectSingle($dbh, "SELECT rowid FROM m_group_type
	                              WHERE ident = '${type}_user'");
		$values{type_id} = $res->{rowid};
		$values{type} = "${type}_user";

		$res = SelectSingle($dbh, "SELECT nextval('m_group_base_rowid_seq') AS rowid");
		$values{rowid}   = $res->{rowid};
		$values{anim_id} = $res->{rowid};
		
		
		# ------------------------------------------------------
		# Crypt password
		# ------------------------------------------------------

		if(exists $values{password}) {
			UserModifyPassword($config, \%values);
		}
		
		# ------------------------------------------------------
		# Initialize nonlocal user values
		# ------------------------------------------------------
		if($type ne 'local') {
			$values{data_expiration_date} = strftime("%Y-%m-%d %H:%M:%S", gmtime(time + 600));
			$values{password_expiration_date} = strftime("%Y-%m-%d %H:%M:%S", gmtime(0));
		}		

		# ------------------------------------------------------
		# Run SQL requests.
		# ------------------------------------------------------
		my $table = {'local' => 'm_user_base', 'ldap' => 'm_user_base', 'external' => 'm_user_base'}->{$type};
		my $sql = BuildInsertRequest(\%values, table  => $table,
									 string => [ qw(ident dn working_days
													firstname lastname email password
													data_expiration_date password_expiration_date external_ident) ],
									 bool   => [ qw(public_part autonomous monday_first history) ],
									 other  => [ qw(rowid created modified mioga_id type_id lang_id theme_id anim_id
													creator_id status_id timezone_id time_per_day external_mioga_id) ],
									 );
		ExecSQL($dbh, $sql);


		
		# ------------------------------------------------------
		# Initialize the group.
		# ------------------------------------------------------
		InitializeGroup($config, \%values);
		
	}
	otherwise {
		my $err = shift;		

		if(! $no_transaction) {
			RollbackTransaction($dbh);
		}
		
		$err->throw;
	};

	if(! $no_transaction) {
		EndTransaction($dbh);
	}

	# ------------------------------------------------------
	# Apply the User Skeleton.
	# ------------------------------------------------------
	# SkelApplyDefaultUser($config, \%values);
	# 
	# if(exists $user->{skeleton}) {
	# 	SkelApplyUser($config, \%values, $config->GetSkeletonDir(). "/user/".$user->{skeleton});
	# }

	return $values{rowid};
}



# ============================================================================

=head2 InitializeGroup ($config, $values)

	InitializeGroup :

	=over 4

	=item - 
	Invite the group animator in the group

	=item - 
	Create private and public directories.

	=item - 
	Initialize authorization

	=item - 
	Run group creation hook

	=back
	
=cut

# ============================================================================

sub InitializeGroup {
	my ($config, $values) = @_;

	my $dbh = $config->GetDBH();

	# ------------------------------------------------------
	# Invite the animator in his group.
	# ------------------------------------------------------
	GroupInviteGroup($dbh, $values->{rowid}, $values->{anim_id});

	# # ------------------------------------------------------
	# # Create private space directory.
	# # Declare URI.
	# # Give Write ACL to the animator.
	# # ------------------------------------------------------

	# my $private_dir = $config->GetPrivateDir().'/'.$values->{ident};
	# my $private_uri = $config->GetPrivateURI().'/'.$values->{ident};

	# if (! (-d $private_dir)) {
	# 	mkdir ($private_dir, 0770)
	# 		or throw Mioga2::Exception::Simple("APIGroup::InitializeGroup", 
    #         __x("Cannot create dir: {directory}", directory => $private_dir));
	# }

    # my $size = (-s $private_dir) || 0;
	# my $priv_uri_id = AuthzAddURI($dbh, $private_uri, $values->{rowid}, undef, $values->{anim_id}, 'directory', $size);

	# # ------------------------------------------------------
	# # Create public space directory if required.
	# # Declare URI.
	# # Give Write ACL to the animator.
	# # Set default ACL to Read.
	# # ------------------------------------------------------
	# if($values->{public_part}) {
	# 	CreatePublicPart($config, $values);
	# }
}



# ============================================================================

=head2 CreatePublicPart ($config, $values)

	Create the group public part and initialize ACL.

=cut

# ============================================================================

sub CreatePublicPart {
	my ($config, $values) = @_;

	my $dbh = $config->GetDBH();

	my $public_dir = $config->GetPublicDir().'/'.$values->{ident};
	my $public_uri = $config->GetPublicURI().'/'.$values->{ident};
	
	if (! (-d $public_dir)) {
		mkdir ($public_dir, 0770)
			or throw Mioga2::Exception::Simple("APIGroup::CreatePublicPart", 
            __x("Cannot create dir: {directory}", directory => $public_dir));
	}

    my $size = (-s $public_dir) || 0;
	my $pub_uri_id = AuthzAddURI($dbh, $public_uri, $values->{rowid}, undef, $values->{anim_id}, 'directory', $size);


	my $profiles = SelectMultiple($dbh, "SELECT * FROM m_profile WHERE group_id = $values->{rowid}");

	if(defined $profiles) {
		foreach my $prof (@$profiles) {
			AuthzAddAuthzForProfile($dbh, $pub_uri_id, $prof->{rowid}, 0);
		}
	}
}



# ============================================================================

=head2 ModifyGroupAnimator ($config, $values, $old_group)

	Change the group animator :

	=over 4

	=item Invite the new animator in the group 

	=item Substitute animators in group profiles

	=item Revoke the old animator

	=back

=cut

# ============================================================================

sub ModifyGroupAnimator {
	my ($config, $values, $old_group) = @_;

	my $dbh = $config->GetDBH();

	#
	# Check if the new animator is already invited in group.
	# If not, invite him.
	#
	my $res = SelectSingle($dbh, "SELECT * FROM m_group_group ".
						   "WHERE group_id = $values->{rowid} AND ".
						   "      invited_group_id = $values->{anim_id}");
	
	if(!defined $res) {
		GroupInviteGroup($dbh, $values->{rowid}, $values->{anim_id});
	}
	
	#
	# Look for animator profile.
	#
	$res = SelectSingle($dbh, "SELECT m_profile.rowid FROM m_profile, m_profile_group ".
						"WHERE m_profile.group_id = $values->{rowid} AND ".
						"      m_profile_group.profile_id = m_profile.rowid AND ".
						"      m_profile_group.group_id = ".$old_group->GetAnimId());

	#
	# Update the new animator profile.
	#
	my $sql = "UPDATE m_profile_group SET profile_id = $res->{rowid}
           FROM m_profile WHERE  m_profile.group_id         = $values->{rowid} AND
                  m_profile_group.profile_id = m_profile.rowid AND
                  m_profile_group.group_id   = $values->{anim_id}";

	ExecSQL($dbh, $sql);

	#
	# Change animator_id in group_base table
	#
	$sql = "UPDATE m_group_base SET anim_id = $values->{anim_id} WHERE rowid = $values->{rowid}";
	ExecSQL($dbh, $sql);

	#
	# Revoke the old animator
	#
	GroupRevokeUser($config, $values->{rowid}, $old_group->GetAnimId());

}


# ============================================================================

=head2 LaunchMethodInApps ($config, $method, @params)

	Try to launch the given methods for all applications.
	
=cut

# ============================================================================

sub LaunchMethodInApps {
	my ($config, $method, @args) = @_;

	my $dbh = $config->GetDBH();

	my $apps = SelectMultiple($dbh, "SELECT * FROM m_application, m_instance_application WHERE m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = ".$config->GetMiogaId());

	foreach my $app (@$apps) {
		try {
			eval "require $app->{package};";
			my $appobj = "$app->{package}"->new ($config);
			$appobj->$method($config, @args);
		}
		otherwise {
			print STDERR "LaunchMethodInApps failed for application $app->{package} in instance " . $config->GetIdent() . "\n";
		};
	}
}


# ============================================================================

=head2 DeletePublicSpace ($config, $rowid)

	Move the public space of a group in his private space.

=cut

# ============================================================================

sub DeletePublicSpace {
	my ($config, $rowid) = @_;

	my $dbh = $config->GetDBH();

	my $public_uri = $config->GetPublicURI();
	my $public_dir = $config->GetPublicDir();
	my $private_uri = $config->GetPrivateURI();
	my $private_dir = $config->GetPrivateDir();

	my $res = SelectSingle($dbh, "SELECT * FROM m_uri WHERE group_id = $rowid AND uri ~ '^$public_uri/[^/]*\$'");

	if(! defined $res) {
		return;
	}
    
	$res->{uri} =~ /^$public_uri\/(.*)$/;
	my $ident = $1;
	
	my $pubdir  = $public_dir."/$ident";
	my $destfile = $private_dir."/$ident/public-space";
	
	if(-f "$destfile.tar.gz") {
		my $i;
		for($i = 0; -f "$destfile-$i.tar.gz"; $i++) {}
	}

	$destfile .= ".tar.gz";
	
	system("tar czf $destfile $pubdir 2> /dev/null");
	rmtree($pubdir);

    my $size = (-s $destfile) || 0;
	$destfile =~ s!^$private_dir/!$private_uri/!;
	AuthzDeclareURI($dbh, $destfile, $rowid, 'application/octet-stream', $size);
	AuthzDeleteURI($dbh, $res->{uri});
}


# ============================================================================

=head2 CheckGroupQuota ($config)

	Check if group quota is excedeed for current instance.

=cut

# ============================================================================

sub CheckGroupQuota {
	my ($config) = @_;

	my $dbh = $config->GetDBH();

	# ------------------------------------------------------
	# Check Max Allowed Group
	# ------------------------------------------------------
	my $max_allowed_group = $config->GetMaxAllowedGroup();
	
	if(defined $max_allowed_group && ($max_allowed_group > 0)) {
		my $nb_groups = SelectSingle($dbh,
									 "SELECT count(m_group_base.rowid) ".
									 "FROM m_group_base ".
									 "WHERE m_group_base.type_id = 1 AND m_group_base.mioga_id = ".$config->GetMiogaId());
		
		if($nb_groups->{count} >= $max_allowed_group) {
			throw Mioga2::Exception::Quota("Group Create", __"Group Quota Excedeed");
		}
	}
}

# ============================================================================

=head2 UserCheckFormValues ($context, $user_type)

Check form values for a user.

=over

=item B<Parameters:>

=over

=item I<$context:> Mioga2 context.

=item I<$user_type:> A Mioga2::Old::User::GetType user type.

=back

=item B<Return:> The array returned by Mioga2::tools::args_checker::ac_CheckArgs.

=back

=cut

# ============================================================================

sub UserCheckFormValues {
	my ($context, $user_type) = @_;

	# Initialize parameters list according to user type
	my $params = [];
	for ($user_type) {
		if (/local_user/) {
			$params = [
				[ [ 'firstname', 'lastname'], 'disallow_empty', [ 'max' => 128 ]],
				[ [ 'email' ], 'want_email', 'disallow_empty', [ 'max' => 128 ] ],
				[ [ 'password' ], 'allow_empty' ],
				[ [ 'new_password_1', 'new_password_2' ], 'allow_empty', [ 'max' => 128 ], [ 'password_policy', $context->GetConfig () ] ],
			];
		}
		elsif (/ldap_user/) {
			$params = [
				[ [ 'firstname', 'lastname'], 'disallow_empty', [ 'max' => 128 ]],
				[ [ 'email' ], 'want_email', 'disallow_empty', [ 'max' => 128 ] ],
				[ [ 'password' ], 'allow_empty' ],
				[ [ 'new_password_1', 'new_password_2' ], 'allow_empty', [ 'max' => 128 ], [ 'password_policy', $context->GetConfig () ] ],
			];
		}
	}

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	#-------------------------------------------------------------------------------
	# Remove any duplicate 'password_policy' error
	#-------------------------------------------------------------------------------
	my $id = -1;
	for (@$errors) {
		if ($_->[1][0] eq 'password_policy') {
			$id++;
		}
	}
	if ($id > 0) {
		splice (@$errors, $id, 1);
	}

	return ($values, $errors);
}


# ============================================================================

=head1 AUTHORS

	The Mioga2 Project <developers@mioga2.org>

	=head1 SEE ALSO

	Mioga2 Mioga2::Old::User Mioga2::Old::Group Mioga2::Old::Resource

	=head1 COPYRIGHT

	Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

	This module is free software; you can redistribute it and/or
	modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;

__END__

