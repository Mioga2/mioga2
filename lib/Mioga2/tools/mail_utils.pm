# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
mail_utils.pm: utilities for sending mails

=head1 DESCRIPTION

This module contains functions for sending mails.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
# $Log$
# Revision 1.1  2003/12/01 08:55:36  lsimonneau
# Initial revision
#
# ============================================================================
#

package Mioga2::tools::mail_utils;
use strict;

use Locale::TextDomain::UTF8 'tools_mailutils';

use vars qw(@ISA @EXPORT);
use Error qw(:try);
use Data::Dumper;

@ISA = qw(Exporter);
@EXPORT = qw( 
			  mu_SendMail
            );

my $debug = 0;



# ============================================================================

=head2 mu_SendMail ($config, MIME::Entity)

	Send the mail description in entity.
	
=cut

# ============================================================================

sub mu_SendMail
{
	my ($config, $entity) = @_;

	open(MAIL, "| /usr/lib/sendmail -i -t -fpostmaster\@".$config->GetDomainName());
	$entity->print(\*MAIL);
	close(MAIL);
}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 DBI

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
