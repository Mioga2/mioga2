# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
args_checker.pm: used for checking arguments values

=head1 DESCRIPTION

	This collection of utilities helps to check arguments and 
	generate errors.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::tools::args_checker;
use strict;

use Locale::TextDomain::UTF8 'tools_argschecker';

use vars qw(@ISA @EXPORT);
use Exporter;
use Error qw(:try);
use Data::Dumper;
use String::Checker;
use Mioga2::tools::string_utils;
use Mioga2::Old::User;
use Mioga2::Old::Resource;
use Mioga2::Old::Group;
use Mioga2::Exception::User;
use Mioga2::Exception::Resource;
use Mioga2::Exception::Group;
use Date::Calc qw(check_time check_date);

@ISA = qw(Exporter);
@EXPORT = qw(ac_CheckArgs ac_XMLifyErrors ac_XMLifyArgs ac_InitArgChecker ac_I18NizeErrors ac_ForceArray ac_ConvertToJSONXML ac_SetParameters ac_IsParametersChecked ac_GetParametersStatus ac_FormatErrors ac_CheckValue ac_CheckCSRFToken);

my $debug = 0;

my $ac_registred;
my $list_checker_func;
my $ac_params;  # Global variable initialized by ac_SetParameters

# ----------------------------------------------------------------------------

=head2 ac_CheckArgs($context, $field_desc)

	Check if request arguments match field description

	field_desc is an array like : 
	
	[ [ [ 'field_name_1', 'field_name_2', ... ], 'check_1', 'check_2'],
	  [ [ 'field_name_3', 'field_name_4', ... ], 'check_x', 'check_y'],
	  ...
    ]

	Available checks are :
	'bool'           => is a boolean value
	"valid_ident"    => is a valid user/group ident
	'valid_profile_ident' => is a valid profile ident in group
	'mioga_user_id'  => a valid existing Mioga2 user rowid
	'mioga_group_id' => a valid existing Mioga2 group rowid
	'mioga_team_id'  => a valid existing Mioga2 team rowid
	'mioga_profile_id'  => a valid existing Mioga2 profile rowid in a given group
	'unused_profile_id' => a valid existing Mioga2 profile rowid in a given group that is not affected to any user or team
	'mioga_resource_id' => a valid existing Mioga2 resource rowid
	'mioga_lang_id'  => a valid existing Mioga2 lang rowid
	'mioga_theme_id' => a valid existing Mioga2 theme rowid
	'mioga_app_id' => a Mioga2 application rowid enabled in a group (passing a Mioga2::GroupList) or accessible to an instance (passing a Mioga2::Config)
	'mioga_user_ids' => a list of valid existing Mioga2 user rowid
	'mioga_group_ids' => a list of valid existing Mioga2 group rowid
	'mioga_team_ids'  => a list of valid existing Mioga2 team rowid
	'mioga_resource_ids' => a list of valid existing Mioga2 resource rowid
	'not_used_group_ident' => an ident not yet used by a group/team/user/resource
	'xhtmlstring' => a valid xhtml string
	'uri'         => a valid URI
	'url'         => a valid URL
	'password_policy'    => a string matching password policy for instance
	'path'        => a correct path (inside Mioga file space)
	'filename'    => a correct file name (with no path indication)
	'full_int'    => an integer positive or negative
	'unused_email'    => an e-mail unique within the instance
	'datetime'    => a date / time as stored into the DB
	'valid_domain_name' => a valid domain name
	'want_email' => a valid email, more restrictive version than the one from String::Checker
	'JSON_array_int' => a valid JSON array of integer
	'noscript' => a string containing no script tag
	'csrf_token' => a valid CSRF Token
	every check supported by String::Checker

=cut

# ---------------------------------------------------------------------------'
sub ac_CheckArgs
{
    my ($context, $field_desc) = @_;

	my $values = {};
	my $errors = [];

	ac_InitArgChecker($context->GetConfig());

	foreach my $fields (@$field_desc) {
		my ($keys, @args) = @$fields;
		my $allow_empty = 0;
		$allow_empty = 1 if (grep (/^allow_empty$/, @args));

		foreach my $key (@$keys) {
			my $string;
			my $res;

			$ac_params->{$key}->{status} = 1;

			if(ref($key) eq 'ARRAY') {
				$string = [];
				foreach my $elem (@$key) {
					push @$string,  $context->{args}->{$elem} unless (!defined ($context->{args}->{$elem}) && $allow_empty);
				}

				$res = ac_listchecker($string, [@args]);

			}
			else {
				$string = $context->{args}->{$key};
				if (ref($string) eq 'ARRAY') {
					for (@$string) {
						$res = String::Checker::checkstring($_, [@args]);
					}
				}
				else {
					$res = String::Checker::checkstring($string, [@args]) unless (!defined ($string) && $allow_empty);
				}
			}

			if(ref($key) eq 'ARRAY') {
				my $nbelem = @$key;
				for(my $i=0; $i < $nbelem; $i++) {
					$values->{$key->[$i]} = $string->[$i] unless (!defined ($string->[$i]) && $allow_empty);
				}
			}
			else {
				$values->{$key} = $string unless (!defined ($string) && $allow_empty);
			}
			if(defined $res and @$res) {
				push @$errors, [$key, $res];
			}
		}
	}

	return ($values, $errors);
}


#===============================================================================

=head2 ac_CheckValue

Check a single value against constraints

=cut

#===============================================================================
sub ac_CheckValue {
	my ($value, @constraints) = @_;

	my $res = String::Checker::checkstring($value, [@constraints]);

	return ($res);
}	# ----------  end of subroutine ac_CheckValue  ----------


#----------------------------------------------------------------------------

=head2 ac_XMLifyArgs($arg_list, $current_values)

	Return XML String representing arguments.
	
	$current_values is a hash ref returned by ac_CheckArgs

=head3 Generated XML

    <$arg_name>$arg_value</$arg_name>
    <$arg2_name>$arg2_value</$arg2_name>
    ...
   
=cut

# ---------------------------------------------------------------------------'
sub ac_XMLifyArgs
{
    my ($arg_list, $current_values) = @_;
	
	my $xml = "";
	
	foreach my $arg (@$arg_list) {
		if(exists $current_values->{$arg} and ref($current_values->{$arg}) eq "ARRAY") {
			foreach my $val (@{$current_values->{$arg}})
			{
				$xml .= qq|<$arg>|.st_FormatXMLString($val)."</$arg>";
			}
		}
		elsif(exists $current_values->{$arg}) {
			$xml .= qq|<$arg>|.st_FormatXMLString($current_values->{$arg})."</$arg>";
		}
		else {
			$xml .= qq|<$arg/>|;
		}
	}

	return $xml;
}


#----------------------------------------------------------------------------

=head2 ac_XMLifyErrors($errors)

	Return XML String the errors (returned by ac_CheckArgs).

	$errors is an ARRAY ref returned by ac_CheckArgs


=head3 Generated XML

    <errors>
       <err arg="Error argument">
           <type>error type</type>
           <type>error type</type>
           ...
       </err>
       ...
    </errors>

=cut

# ---------------------------------------------------------------------------'
sub ac_XMLifyErrors
{
    my ($errors) = @_;

	my $xml = "<errors>";
	
	foreach my $err (@$errors) {
		my ($arg, $errlist) = @$err;
		
		$xml .= qq|<err arg="|.st_FormatXMLString($arg).qq|">|;

		foreach my $e (@$errlist) {
			$xml .= "<type>".st_FormatXMLString($e)."</type>";
		}
		$xml .= "</err>";
	}

	$xml .= "</errors>";
	
	return $xml;
}
#----------------------------------------------------------------------------

=head2 ac_ForceArray($arg)

	transforms scalar args to an array

=cut

# ---------------------------------------------------------------------------'
sub ac_ForceArray
{
    my ($arg) = @_;
	print STDERR "ac_ForceArray\n" if ($debug);
	my $values = [];
	print STDERR "arg = ".Dumper($arg)."\n" if ($debug);

	if (defined($arg)) {
		if (ref $arg eq 'ARRAY') {
			$values = $arg;
		}
		elsif ($arg ne '') {
			push @$values, $arg;
		}
	}
	print STDERR "values = ".Dumper($values)."\n" if ($debug);
	
	return $values;
}
#----------------------------------------------------------------------------

=head2 ac_I18NizeErrors($errors)

	Translate error messages in place

	$errors is an ARRAY ref returned by ac_CheckArgs

=cut

# ---------------------------------------------------------------------------'
sub ac_I18NizeErrors {
    my ($errors) = @_;
	print STDERR "ac_I18NizeErrors\n" if ($debug);
	
	my $ierr = 0;
	for ($ierr= 0; $ierr < @$errors; $ierr++) {
		print STDERR "ierr = $ierr 0 = ".$errors->[$ierr]->[0]."\n" if ($debug);
		my $imess = 0;
		for ($imess= 0; $imess < @{$errors->[$ierr]->[1]}; $imess++) {
			my $mess = $errors->[$ierr]->[1]->[$imess];
			print STDERR "imess = $mess mess = $mess\n" if ($debug);
			$errors->[$ierr]->[1]->[$imess] = __($mess);
			utf8::decode($errors->[$ierr]->[1]->[$imess]);
		}
	}

	print STDERR "after errors = ".Dumper($errors)."\n" if ($debug);
}
#----------------------------------------------------------------------------

=head2 ac_ConvertToJSONXML($errors)

	Modify errors structure to convert in JSON or XML with WSWrapper

	$errors is an ARRAY ref returned by ac_CheckArgs

=cut

# ---------------------------------------------------------------------------'
sub ac_ConvertToJSONXML {
    my ($errors) = @_;
	print STDERR "ac_ConvertToJSONXML\n" if ($debug);
	
	my $array = [];
	my $ierr = 0;
	for ($ierr= 0; $ierr < @$errors; $ierr++) {
		print STDERR "ierr = $ierr 0 = ".$errors->[$ierr]->[0]."\n" if ($debug);
		my $imess = 0;
		my $errs = [];
		for ($imess= 0; $imess < @{$errors->[$ierr]->[1]}; $imess++) {
			push @$errs, { '$t' => $errors->[$ierr]->[1]->[$imess]};
		}
		push @$array, { $errors->[$ierr]->[0] => $errs };
	}

	print STDERR "after array = ".Dumper($array)."\n" if ($debug);
	return $array;
}
#----------------------------------------------------------------------------

=head2 ac_SetParameters($args)

	Intialize parameters HASH

	$args is an HASH ref from the context

=cut

# ---------------------------------------------------------------------------'
sub ac_SetParameters {
    my ($args) = @_;
	print STDERR "ac_SetParameters\n" if ($debug);

	print STDERR "args ".Dumper($args)."\n" if ($debug);
	$ac_params = {};
	foreach my $key (keys(%$args)) {
		$ac_params->{$key} = { name => $key,
								  status => 0,
								};
	}
	
}
#----------------------------------------------------------------------------

=head2 ac_IsParametersChecked($args)

	Verify if all given parameters had been checked

=cut

# ---------------------------------------------------------------------------'
sub ac_IsParametersChecked {
    my ($args) = @_;
	print STDERR "ac_IsParametersChecked\n" if ($debug);
	my $val = 0;

	foreach my $key (keys(%$ac_params)) {
		$val++ if (!$ac_params->{$key}->{status});
	}
	return $val;
}
#----------------------------------------------------------------------------

=head2 ac_GetParametersStatus($args)

	Returns the parameters for status

=cut

# ---------------------------------------------------------------------------'
sub ac_GetParametersStatus {
    my ($args) = @_;
	print STDERR "ac_GetParametersStatus\n" if ($debug);

	return $ac_params;
}
#----------------------------------------------------------------------------

=head2 ac_FormatErrors($errors)

	Format errors ARRAY as a string

=cut

# ---------------------------------------------------------------------------'
sub ac_FormatErrors {
    my ($errors) = @_;
	print STDERR "ac_FormatErrors\n" if ($debug);
	my $string = "";

	my $ierr = 0;
	for ($ierr= 0; $ierr < @$errors; $ierr++) {
		print STDERR "ierr = $ierr 0 = ".$errors->[$ierr]->[0]."\n" if ($debug);
		$string .= " " . $errors->[$ierr]->[0] . " " . join(',', @{$errors->[$ierr]->[1]});
	}

	return $string;
}
# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ----------------------------------------------------------------------------
#
#ac_listchecker($list, $args)
#
#	Run the list check functions.
#
# ---------------------------------------------------------------------------'
sub ac_listchecker
{
	my ($list, $args) = @_;
	
	my @errors;

	foreach my $arg (@$args) {
		if(ref($arg) eq 'ARRAY') {
			my ($funcname, @params) = @$arg;
			
			my $func = $list_checker_func->{$funcname};
			my $res = &$func(\$list, \@params);
			
			if(defined $res) {
				push @errors, $funcname;
			}		
		}
		else {
			my $func = $list_checker_func->{$arg};
			my $res = &$func(\$list);
			
			if(defined $res) {
				push @errors, $arg;
			}
		}
	}
	
	return \@errors;
}

sub ac_InitArgChecker {
	my ($config) = @_;

	if($ac_registred) {
		return;
	}

	String::Checker::register_check('bool', \&ac_CheckBool);
	String::Checker::register_check('mioga_user_id', \&ac_CheckMiogaUserId);
	String::Checker::register_check('mioga_group_id', \&ac_CheckMiogaGroupId);
	String::Checker::register_check('mioga_team_id', \&ac_CheckMiogaTeamId);
	String::Checker::register_check('mioga_profile_id', \&ac_CheckMiogaProfileId);
	String::Checker::register_check('unused_profile_id', \&ac_CheckMiogaUnusedProfileId);
	String::Checker::register_check('mioga_resource_id', \&ac_CheckMiogaResourceId);
	String::Checker::register_check('mioga_lang_id', \&ac_CheckMiogaLangId);
	String::Checker::register_check('mioga_theme_id', \&ac_CheckMiogaThemeId);
	String::Checker::register_check('mioga_app_id', \&ac_CheckMiogaApplicationId);
	String::Checker::register_check('mioga_user_ids', \&ac_CheckMiogaUserIds);
	String::Checker::register_check('mioga_group_ids', \&ac_CheckMiogaGroupIds);
	String::Checker::register_check('mioga_team_ids', \&ac_CheckMiogaTeamIds);
	String::Checker::register_check('mioga_resource_ids', \&ac_CheckMiogaResourceIds);
	String::Checker::register_check('valid_ident', \&ac_CheckValidIdent);
	String::Checker::register_check('valid_profile_ident', \&ac_CheckValidProfileIdent);
	String::Checker::register_check('not_used_group_ident', \&ac_CheckNotUsedGroupIdent);
	String::Checker::register_check('xhtmlstring', \&ac_CheckXhtmlString);
	String::Checker::register_check('uri', \&ac_CheckURI);
	String::Checker::register_check('url', \&ac_CheckURL);
    String::Checker::register_check('modify_group_ident', \&ac_CheckModifyGroupIdent);
    String::Checker::register_check('password_policy', \&ac_CheckPassword);
    String::Checker::register_check('location', \&ac_CheckLocation);
    String::Checker::register_check('filename', \&ac_CheckFileName);
    String::Checker::register_check('path', \&ac_CheckPath);
    String::Checker::register_check('full_int', \&ac_CheckFullInt);
    String::Checker::register_check('unused_email', \&ac_CheckUnusedEmail);
    String::Checker::register_check('pgdatetime', \&ac_PgCheckDateTime);
    String::Checker::register_check('valid_domain_name', \&ac_CheckDomainName);
    String::Checker::register_check('want_email', \&ac_CheckEmail);
    String::Checker::register_check('JSON_array_int', \&ac_CheckJSONIntArray);
    String::Checker::register_check('noscript', \&ac_CheckNoScript);
    String::Checker::register_check('csrf_token', \&ac_CheckCSRFToken);

	$list_checker_func = {};

	$list_checker_func->{is_list} = \&ac_CheckList;
	$list_checker_func->{date} = \&ac_CheckDate;
	$list_checker_func->{time} = \&ac_CheckTime;
	$list_checker_func->{datetime} = \&ac_CheckDateTime;

	$ac_registred = 1;

}

sub ac_CheckList { 
	my $str = shift;
	
	if(defined $$str) {
		
		if(ref($$str) eq 'ARRAY') {
			return undef;
		}
		elsif ($$str ne '')
		{
			$$str = [$$str];
		}			
		else
		{
			$$str = [];
		}
		
		return undef;
	}
	
	$$str = [];
	return undef;
}


sub ac_CheckBool{ 
	my $str = shift;

	if(defined $$str) {
		
		if($$str eq 'on' or $$str eq '1') {
			$$str = '1';
		}
		else {
			$$str = '0';				
		}			
		
		return undef;
	}
	
	$$str = '0';				
	return undef;
}

sub ac_CheckMiogaUserId{
	my ($str, $config) = @_;
	
	if(! defined $$str or $$str eq '') {
		return undef;
	}
	
	my $result;
	try {
		my $user = new Mioga2::Old::User($config, rowid => $$str);
		$result = undef;
	}
	otherwise {
		$result = 1;
	};
	
	return $result;
}

sub ac_CheckMiogaGroupId{
	my ($str, $config) = @_;
	
	if(! defined $$str) {
		return undef;
	}
	
	my $result;
	try {
		my $user = new Mioga2::Old::Group($config, rowid => $$str);
		$result = undef;
	}
	otherwise {
		$result = 1;
	};
	
	return $result;
}


sub ac_CheckMiogaTeamId{
	my ($str, $config) = @_;
	
	if(! defined $$str) {
		return undef;
	}
	
	my $result;
	try {
		my $user = new Mioga2::Old::Team($config, rowid => $$str);
		$result = undef;
	}
	otherwise {
		$result = 1;
	};
	
	return $result;
}


sub ac_CheckMiogaResourceId{
	my ($str, $config) = @_;
	
	if(! defined $$str) {
		return undef;
	}
	
	my $result;
	try {
		my $user = new Mioga2::Old::Resource($config, rowid => $$str);
		$result = undef;
	}
	otherwise {
		$result = 1;
	};
	
	return $result;
}

sub ac_CheckMiogaUserIds{
	my ($str, $config) = @_;
	
	if(! defined $$str) {
		return undef;
	}
	
	my @rowids;
	if (ref ($$str) ne 'ARRAY') {
		@rowids = split (/,/, $$str);
	}
	else {
		@rowids = @$$str;
	}

	my $result;
	foreach my $user (@rowids) {
		$result = ac_CheckMiogaUserId(\$user, $config);
		last if $result;
	}
	
	return $result;
}


sub ac_CheckMiogaGroupIds{
	my ($str, $config) = @_;
	
	if(! defined $$str) {
		return undef;
	}
	
	my @rowids;
	if (ref ($$str) ne 'ARRAY') {
		@rowids = split (/,/, $$str);
	}
	else {
		@rowids = @$$str;
	}

	my $result;
	foreach my $group (@rowids) {
		$result = ac_CheckMiogaGroupId(\$group, $config);
		last if $result;
	}
	
	return $result;
}

sub ac_CheckMiogaResourceIds{
	my ($str, $config) = @_;
	
	if(! defined $$str) {
		return undef;
	}
	
	my @rowids;
	if (ref ($$str) ne 'ARRAY') {
		@rowids = split (/,/, $$str);
	}
	else {
		@rowids = @$$str;
	}

	my $result;
	foreach my $resource (@rowids) {
		$result = ac_CheckMiogaResourceId(\$resource, $config);
		last if $result;
	}
	
	return $result;
}

sub ac_CheckMiogaTeamIds{
	my ($str, $config) = @_;
	
	if(! defined $$str) {
		return undef;
	}
	
	my @rowids;
	if (ref ($$str) ne 'ARRAY') {
		@rowids = split (/,/, $$str);
	}
	else {
		@rowids = @$$str;
	}

	my $result;
	foreach my $team (@rowids) {
		$result = ac_CheckMiogaTeamId(\$team, $config);
		last if $result;
	}
	
	return $result;
}


sub ac_CheckValidIdent {
	my ($str, $config) = @_;
	
	if(! defined $$str or $$str =~ /[^A-Za-z0-9\-_\.@]/) {
		return 1;
	}

	if ("/$$str" eq $config->GetMiogaConf ()->GetUserURI ()) {
		return (1);
	}
	
	return undef;
}

sub ac_CheckNotUsedGroupIdent {
	my ($str, $config) = @_;
	
	if(! defined $$str) {
		return undef;
	}
	
	my $result;
	try {
		
		my $group = Mioga2::Old::GroupBase->CreateObject($config, ident => $$str);
		$result = 1;
	}
	otherwise {
		$result = undef;
	};
	
	
	return $result;
}

sub ac_CheckModifyGroupIdent {
    my ($str, $options) = @_;
    
    return undef if !defined $$str;
    
    my $session = $options->{session};
    my $config  = $options->{config};
    my $result;
    try {
        my $group   = Mioga2::Old::GroupBase->CreateObject($config, ident => $$str);
        $result     = 1;
        $result     = undef if $group->GetRowid == $session->{rowid} && $group->GetIdent eq $session->{ident};
    }
    otherwise {
        $result = undef;
    };
    
    return $result;
}

sub ac_CheckDate {
	my ($str, $params) = @_;
	
	if(! defined $$str) {
		return undef;
	}
	
	if(check_date(@$$str)) {
		return undef;
	}
	
	return 1;
}


sub ac_CheckTime {
	my ($str, $params) = @_;

	if(! defined $$str) {
		return undef;
	}
	
	my ($hour, $min, $sec) = @$$str;

	if(!exists $params->{use_second} or  ! $params->{use_second}) {
		$sec = 0;
	}

	if(check_time($hour, $min, $sec)) {
		return undef;
	}
	
	return 1;
}


sub ac_CheckDateTime {
	my ($str, $params) = @_;
	
	if(! defined $$str) {
		return undef;
	}
	
	my ($year, $month, $day, $hour, $min, $sec) = @$$str;

	if(! $params->{use_second}) {
		$sec = 0;
	}

	if(check_time($year, $month, $day) and check_time($hour, $min, $sec)) {
		return undef;
	}
	
	return 1;
}



sub ac_CheckXhtmlString {
	my ($str, $params) = @_;
	
	if(! defined $$str) {
		return undef;
	}

	st_CleanupHTMLTags($str, $params->{allowed_tags});

	return undef;
}


#===============================================================================

=head2 ac_CheckPassword

Check that a string matches the current instance password policy

=cut

#===============================================================================
sub ac_CheckPassword {
	my ($str, $config) = @_;

	if ($$str ne '') {
		#-------------------------------------------------------------------------------
		# Check length
		#-------------------------------------------------------------------------------
		if (length($$str) < $config->{pwd_min_length}) {
			return 1;
		}

		#-------------------------------------------------------------------------------
		# Check number of letters
		#-------------------------------------------------------------------------------
		my $chk = $$str;
		if (($chk =~ s/([a-z])/$1/gi) < $config->{pwd_min_letter}) {
			return 1;
		}

		#-------------------------------------------------------------------------------
		# Check number of digits
		#-------------------------------------------------------------------------------
		$chk = $$str;
		if (($chk =~ s/([0-9])/$1/gi) < $config->{pwd_min_digit}) {
			return 1;
		}

		#-------------------------------------------------------------------------------
		# Check number of special characters
		#-------------------------------------------------------------------------------
		$chk = $$str;
		if (($chk =~ s/([^a-z0-9])/$1/gi) < $config->{pwd_min_special}) {
			return 1;
		}

		#-------------------------------------------------------------------------------
		# Count number of case changes
		#-------------------------------------------------------------------------------
		$chk = $$str;
		$chk =~ s/[^[:alpha:]]//g;
		if ((($chk =~ s/([a-z][A-Z])/$1/g) + ($chk =~ s/([A-Z][a-z])/$1/g)) < $config->{pwd_min_chcase}) {
			return 1;
		}
	}

	return (undef);
}	# ----------  end of subroutine ac_CheckPassword  ----------


#===============================================================================

=head2 ac_CheckLocation

Check that the provided string is a valid location

=cut

#===============================================================================
sub ac_CheckLocation {
	my ($str, $config) = @_;

	if ($$str ne '') {
		my $base_path;

		if (ref($config) eq 'Mioga2::Config') {
			my $base_uri = $config->GetMiogaConf()->GetBasePath();
			my $instance = $config->GetMiogaIdent ();

			$base_path = "$base_uri/$instance";
		}
		else {
			$base_path = '/' . $config->GetBasePath();
		}

		$base_path =~ s#//#/#g;

		if ($$str !~ /^$base_path.*$/) {
			return (1);
		}

		if ($$str =~ /\/\.\.\//) {
			return (1);
		}
	}

	return (undef);
}	# ----------  end of subroutine ac_CheckLocation  ----------


#===============================================================================

=head2 ac_CheckFileName

Check that the provided string is a file name without path

=cut

#===============================================================================
sub ac_CheckFileName {
	my ($str) = @_;

	if ($$str =~ /\//) {
		return (1);
	}

	return (undef);
}	# ----------  end of subroutine ac_CheckFileName  ----------


#===============================================================================

=head2 ac_CheckPath

Check that the provided string is a Mioga filesystem path

=cut

#===============================================================================
sub ac_CheckPath {
	my ($str, $config) = @_;

	if ($$str ne '') {
		my $install_dir = $config->GetMiogaConf()->GetInstallDir();
		my $base_dir = $config->GetMiogaConf()->GetBaseDir();
		my $instance = $config->GetMiogaIdent ();

		my $base_path = "$install_dir/$instance/$base_dir";
		$base_path =~ s#//#/#g;

		if ($$str !~ /^$base_path.*$/) {
			return (1);
		}

		if ($$str =~ /\/\.\.\//) {
			return (1);
		}
	}

	return (undef);
}	# ----------  end of subroutine ac_CheckPath  ----------


#===============================================================================

=head2 ac_CheckFullInt

Check that the provided string is an integer positive or negative

=cut

#===============================================================================
sub ac_CheckFullInt {
	my ($str, $config) = @_;

	if ($$str ne '') {
		if ($$str !~ /^[+-]?\d*$/) {
			return (1);
		}
	}

	return (undef);
}	# ----------  end of subroutine ac_CheckPath  ----------


#===============================================================================

=head2 ac_CheckUnusedEmail

Check that the e-mail is not already used in the current instance

=cut

#===============================================================================
sub ac_CheckUnusedEmail {
	my ($str, $config) = @_;

	if(! defined $$str) {
		return undef;
	}
	
	my $result;
	try {
		my $user = new Mioga2::Old::User ($config, (email => lc ($$str)));
		$result = 1;
	}
	otherwise {
		$result = undef;
	};
	
	return ($result);
}	# ----------  end of subroutine ac_CheckUnusedEmail  ----------


#===============================================================================

=head2 ac_PgCheckDateTime

Check a date / time as stored into DB

=cut

#===============================================================================
sub ac_PgCheckDateTime {
	my ($str) = @_;

	my $result = undef;

	if ($$str ne '') {
		if ($$str !~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}( [0-9]{2}:[0-9]{2}:[0-9]{2}(\.[0-9]*)?)?$/) {
			$result = 1;
		}
	}

	return ($result);
}	# ----------  end of subroutine ac_CheckDateTime  ----------


#===============================================================================

=head2 ac_CheckDomainName

Check a domain name

=cut

#===============================================================================
sub ac_CheckDomainName {
	my ($str) = @_;

	my $result = undef;

	if ($$str ne '') {
		if ($$str !~ /^[\w-]+\.[\w\.-]+$/) {
			$result = 1;
		}
	}

	return ($result);
}	# ----------  end of subroutine ac_CheckDomainName  ----------


#===============================================================================

=head2 ac_CheckEmail

Check an email address

=cut

#===============================================================================
sub ac_CheckEmail {
	my ($str) = @_;

	my $result = undef;

	if ($$str ne '') {
		if ($$str !~ /^[A-Z0-9.'_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i) {
			$result = 1;
		}
	}

	return ($result);
}	# ----------  end of subroutine ac_CheckEmail  ----------


#===============================================================================

=head2 ac_CheckJSONIntArray

Check a JSON array of integer

=cut

#===============================================================================
sub ac_CheckJSONIntArray {
	my ($str) = @_;

	my $result = undef;
	if ($$str ne '') {
		if ($$str !~ /^\[([ ]?[0-9]+[ ]?\]$)|(([ ]?[0-9]+[ ]?,[ ]?)+[ ]?[0-9]+[ ]?\]$)|(]$)/ ) {
			$result = 1;
		}
	}
	return ($result);
}	# ----------  end of subroutine ac_CheckJSONIntArray  ----------

#===============================================================================

=head2 ac_CheckMiogaProfileId

Check profile rowid is valid for group

=cut

#===============================================================================
sub ac_CheckMiogaProfileId {
	my ($str, $group) = @_;

	my $result = $group->HasProfile ($$str);

	return ($result ? 0 : 1);
}	# ----------  end of subroutine ac_CheckMiogaProfileId  ----------


#===============================================================================

=head2 ac_CheckMiogaUnusedProfileId

Check profile rowid has no user or team affected

=cut

#===============================================================================
sub ac_CheckMiogaUnusedProfileId {
	my ($str, $config) = @_;

	my $db = $config->GetDBObject ();

	my $res = $db->SelectSingle ('SELECT * FROM m_profile_group WHERE profile_id = ?;', [$$str]);

	return (defined ($res) ? 1 : 0);
}	# ----------  end of subroutine ac_CheckMiogaUnusedProfileId  ----------


#===============================================================================

=head2 ac_CheckMiogaLangId

Check a lang rowid

=cut

#===============================================================================
sub ac_CheckMiogaLangId {
	my ($str, $config) = @_;

	my $db = $config->GetDBObject ();

	my $res = $db->SelectSingle ('SELECT * FROM m_lang WHERE rowid = ?;', [$$str]);

	return (defined ($res) ? 0 : 1);
}	# ----------  end of subroutine ac_CheckMiogaLangId  ----------


#===============================================================================

=head2 ac_CheckMiogaThemeId

Check a theme rowid

=cut

#===============================================================================
sub ac_CheckMiogaThemeId {
	my ($str, $config) = @_;

	my $db = $config->GetDBObject ();

	my $res = $db->SelectSingle ('SELECT * FROM m_theme WHERE rowid = ? AND mioga_id = ?;', [$$str, $config->GetMiogaId ()]);

	return (defined ($res) ? 0 : 1);
}	# ----------  end of subroutine ac_CheckMiogaThemeId  ----------


#===============================================================================

=head2 ac_CheckMiogaApplicationId

Check an application rowid as accessible to an instance (passing a Mioga2::Config) or to a group (passing a Mioga2::GroupList)

=cut

#===============================================================================
sub ac_CheckMiogaApplicationId {
	my ($str, $object) = @_;

	my $result;

	if (ref ($object) eq 'Mioga2::GroupList') {
		# Check application is active in group
		$result = ($object->HasApplication ($$str)) ? 0 : 1;
	}
	else {
		# Check application is active for instance
		my $db = $object->GetDBObject ();
		my $res = $db->SelectSingle ('SELECT * FROM m_instance_application WHERE application_id = ? AND mioga_id = ?;', [$$str, $object->GetMiogaId ()]);
		$result = (defined ($res)) ? 0 : 1;
	}

	return ($result);
}	# ----------  end of subroutine ac_CheckMiogaApplicationId  ----------


#===============================================================================

=head2 ac_CheckValidProfileIdent

Check a profile ident is valid (ident corresponds to rowid or ident is not used in group)

=head3 Incoming Arguments

=over

=item I<$str>: a reference to string to check

=item I<$data>: a reference to an array containing:

=over

=item I<$group>: a Mioga2::GroupList matching profile's group

=item I<$rowid>: the profile's rowid

=back

=back

=cut

#===============================================================================
sub ac_CheckValidProfileIdent {
	my ($str, $data) = @_;

	my $res = 1;

	# Extract members of $data
	my ($config, $group, $rowid) = @$data;

	my $db = $config->GetDBObject ();

	my $profile = $db->SelectSingle ('SELECT * FROM m_profile WHERE ident = ? AND group_id = ?;', [$$str, $group->Get ('rowid')]);
	if (!$profile) {
		# Profile doesn't exist in group, value is OK
		if ($$str =~ /\w/) {
			$res = undef;
		}
	}
	elsif ($profile->{rowid} == $rowid) {
		# Profile ident corresponds to profile rowid, value is OK
		$res = undef;
	}

	return ($res);
}	# ----------  end of subroutine ac_CheckValidProfileIdent  ----------


#===============================================================================

=head2 ac_CheckNoScript

Check an email address

=cut

#===============================================================================
sub ac_CheckNoScript {
	my ($str) = @_;

	my $result = undef;

	if ($$str ne '') {
		if ($$str =~ /<script>/i) {
			$result = 1;
		}
	}

	return ($result);
}	# ----------  end of subroutine ac_CheckNoScript  ----------


#===============================================================================

=head2 ac_CheckCSRFToken

Check CSRF Token is valid

=cut

#===============================================================================
sub ac_CheckCSRFToken {
	my ($token, $data) = @_;

	# Extract members of $data
	my ($config, $user) = @$data;

	my $result = 1;

	if ($config->GetDBObject()->SelectSingle('SELECT * FROM m_auth_session WHERE email=? AND csrftoken=?;', [$user->GetEmail(), $$token])) {
		$result = undef;
	}
	else {
		throw Mioga2::Exception::Simple("Dispatch::handler", "Rejecting CSRF unsafe request");
	}

	return ($result);
}	# ----------  end of subroutine ac_CheckCSRFToken  ----------

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
