# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
APIApplication.pm: Methods used to create/delete, enable/disable applications.

=head1 DESCRIPTION

This module contains functions for creating or deleting application and to enable
or disable application in a given group.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::tools::APIApplication;
use strict;

use Locale::TextDomain::UTF8 'tools_apiapplication';

use vars qw(@ISA @EXPORT);
use Error qw(:try);
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;


@ISA = qw(Exporter);
@EXPORT = qw(ApplicationDeleteGroup
			 ApplicationDisallowForGroup ApplicationAllowForGroup
			 ApplicationDisallowForAllGroups ApplicationAllowForAllGroups
			 ApplicationDisallowForAllUsers ApplicationAllowForAllUsers
			 ApplicationEnableInGroup ApplicationDisableInGroup
			 ApplicationPublicInGroup ApplicationPrivateInGroup
			 ApplicationGetValues ApplicationGetFunctionIdFromIdent
			 ApplicationCreate FunctionCreate MethodCreate
			 ApplicationUpdate FunctionUpdate				
			 ApplicationRemove FunctionRemove MethodRemove
			 );

my $debug = 0;



# ============================================================================

=head2 ApplicationDeleteGroup ($dbh, $group_id)

	Delete the group $group_id in applications tables.

=cut

# ============================================================================

sub ApplicationDeleteGroup {
	my ($dbh, $group_id) = @_;

	ExecSQL($dbh, "DELETE FROM m_application_group WHERE group_id = $group_id");	
	ExecSQL($dbh, "DELETE FROM m_application_group_allowed WHERE group_id = $group_id");	
}


# ============================================================================

=head2 ApplicationAllowForGroup ($dbh, $app_id, $group_id)

	Add the given group to the list of allowed groups for application $app_id

=cut

# ============================================================================

sub ApplicationAllowForGroup {
	my ($dbh, $app_id, $group_id) = @_;

	ExecSQL($dbh, "INSERT INTO m_application_group ( application_id, group_id) 
	                                        VALUES ($app_id, $group_id)");
	
}


# ============================================================================

=head2 ApplicationDisallowForGroup ($dbh, $app_id, $group_id)

	Remove the given group to the list of allowed groups for application $app_id

=cut

# ============================================================================

sub ApplicationDisallowForGroup {
	my ($dbh, $app_id, $group_id) = @_;

	ExecSQL($dbh, "DELETE FROM m_application_group WHERE application_id=$app_id AND group_id=$group_id");

	ApplicationDisableInGroup($dbh, $app_id, $group_id);
}




# ============================================================================

=head2 ApplicationAllowForAllGroups ($config, $app_id)

	Allow the application $app_id for all groups

=cut

# ============================================================================

sub ApplicationAllowForAllGroups {
	my ($config, $app_id) = @_;

	my $dbh = $config->GetDBH();

	ExecSQL($dbh, "UPDATE  m_instance_application SET all_groups = 't' WHERE application_id = $app_id AND mioga_id = ".$config->GetMiogaId());
}


# ============================================================================

=head2 ApplicationDisallowForAllGroups ($config, $app_id)

	Disallow the application $app_id for all groups

=cut

# ============================================================================

sub ApplicationDisallowForAllGroups {
	my ($config, $app_id) = @_;

	my $dbh = $config->GetDBH();

	ExecSQL($dbh, "UPDATE m_instance_application SET all_groups = 'f' WHERE application_id = $app_id AND mioga_id = ".$config->GetMiogaId());

	
	ApplicationDisableInAllGroup($dbh, $app_id);
}


# ============================================================================

=head2 ApplicationAllowForAllUsers ($config, $app_id)

	Allow the application $app_id for all users

=cut

# ============================================================================

sub ApplicationAllowForAllUsers {
	my ($config, $app_id) = @_;

	my $dbh = $config->GetDBH();

	ExecSQL($dbh, "UPDATE  m_instance_application SET all_users = 't' WHERE application_id = $app_id AND mioga_id = ".$config->GetMiogaId());
}


# ============================================================================

=head2 ApplicationDisallowForAllUsers ($config, $app_id)

	Disallow the application $app_id for all users

=cut

# ============================================================================

sub ApplicationDisallowForAllUsers {
	my ($config, $app_id) = @_;

	my $dbh = $config->GetDBH();

	ExecSQL($dbh, "UPDATE m_instance_application SET all_users = 'f' WHERE application_id = $app_id AND mioga_id = ".$config->GetMiogaId());
	
	ApplicationDisableInAllGroup($dbh, $app_id);
}


# ============================================================================

=head2 ApplicationEnableInGroup ($dbh, $app_id, $group_id)

	Enable an application for a given group

=cut

# ============================================================================

sub ApplicationEnableInGroup {
	my ($dbh, $app_id, $group_id) = @_;

	my $sql = "select count(*) from m_application_group_allowed where application_id = $app_id and group_id = $group_id";

	my $res = SelectSingle($dbh, $sql);

	if (defined($res) and $res->{count} == 0)
	{
		ExecSQL($dbh, "INSERT INTO m_application_group_allowed (created, modified, application_id, group_id, is_public) 
	                                                VALUES (now(), now(), $app_id, $group_id, 'f')");
	}
	
}



# ============================================================================

=head2 ApplicationDisableInGroup ($dbh, $app_id, $group_id)

	Disable an application for a given group

=cut

# ============================================================================

sub ApplicationDisableInGroup {
	my ($dbh, $app_id, $group_id) = @_;

	ExecSQL($dbh, "DELETE FROM m_profile_function
                          USING m_function, m_profile
	                      WHERE function_id = m_function.rowid AND 
                                m_function.application_id = $app_id AND
                                profile_id = m_profile.rowid AND
                                m_profile.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM m_application_group_allowed
	                      WHERE application_id = $app_id AND
	                            group_id = $group_id");
}





# ============================================================================

=head2 ApplicationDisableInAllGroup ($dbh, $app_id)

	Disable an application for alls groups

=cut

# ============================================================================

sub ApplicationDisableInAllGroup {
	my ($dbh, $app_id) = @_;

	ExecSQL($dbh, "DELETE FROM m_profile_function
                          USING m_function, m_profile
	                      WHERE function_id = m_function.rowid AND 
                                m_function.application_id = $app_id AND
                                profile_id = m_profile.rowid AND
                                m_profile.group_id  NOT IN (SELECT m_application_group.group_id
                                                            FROM   m_application_group 
                                                            WHERE  m_application_group.application_id=$app_id)");


	ExecSQL($dbh, "DELETE FROM m_application_group_allowed
	                      WHERE application_id = $app_id AND
	                            group_id NOT IN (SELECT m_application_group.group_id
                                                   FROM   m_application_group 
                                                   WHERE  m_application_group.application_id=$app_id)");
	

}



# ============================================================================

=head2 ApplicationPublicInGroup ($dbh, $app_id, $group_id)

	Make the application public for a given group

=cut

# ============================================================================

sub ApplicationPublicInGroup {
	my ($dbh, $app_id, $group_id) = @_;

	ExecSQL($dbh, "UPDATE m_application_group_allowed SET is_public = 't' ".
			      "WHERE group_id = $group_id AND application_id = $app_id");	
}



# ============================================================================

=head2 ApplicationPrivateInGroup ($dbh, $app_id, $group_id)

	Make the application private for a given group

=cut

# ============================================================================

sub ApplicationPrivateInGroup {
	my ($dbh, $app_id, $group_id) = @_;

	ExecSQL($dbh, "UPDATE m_application_group_allowed SET is_public = 'f' ".
			      "WHERE group_id = $group_id AND application_id = $app_id");	
}



# ============================================================================

=head2 ApplicationCreate ($config, $params)

	Declare a new application in the database. 

	Parameters are :

=over 4

=item ident : The application identifier

=item package : The perl package name

=item description : The application description

=item can_be_public : Can the application be public ?

=item is_user : Is the application usable in user workspace ?

=item is_group : Is the application usable in group workspace ?

=item is_resource : Is the application usable in resource workspace ?

=item type : The application type.

=back
	
=cut

# ============================================================================

sub ApplicationCreate {
	my ($miogaconf, $params) = @_;

	my $dbh = $miogaconf->GetDBH();

	$params->{created} = 'now()';
	$params->{modified} = 'now()';

	my $type = SelectSingle($dbh, "SELECT * FROM m_application_type WHERE ident = '$params->{type}'");
	$params->{type_id} = $type->{rowid};

	my $sql = BuildInsertRequest($params,  table  => 'm_application',
										   string => ['ident', 'package', 'description'],
										   bool   => ['can_be_public', 'is_user', 'is_group', 'is_resource'],
                                           other  => ['modified', 'created', 'type_id'],
								 );

	ExecSQL($dbh, $sql);

	my $ident = $params->{ident};
	$ident = st_FormatPostgreSQLString($ident);

	my $res = SelectSingle($dbh, "SELECT * FROM m_application WHERE ident = '$ident'");

	return $res->{rowid};
}



# ============================================================================

=head2 ApplicationUpdate($config, $rowid, $params)

ApplicationUpdate update an application in DB
Args are:

=over 4

=item ident : The application identifier

=item package : The perl package name

=item description : The application description

=item can_be_public : Can the application be public ?

=item all_groups : Available for all groups by default ?

=item all_users : Available for all users by default ?

=item is_user : Is the application usable in user workspace ?

=item is_group : Is the application usable in group workspace ?

=item is_resource : Is the application usable in resource workspace ?

=item type : The application type.

=back

=cut

# ============================================================================

sub ApplicationUpdate
{
	my ($config, $rowid, $params) = @_;
	
	my $dbh = $config->GetDBH();

	$params->{modified} = 'now()';

	if(exists $params->{type}) {
		my $type = SelectSingle($dbh, "SELECT * FROM m_application_type WHERE ident = '$params->{type}'");
		$params->{type_id} = $type->{rowid};
	}

	my $sql = BuildUpdateRequest($params, table => 'm_application',
								          string => ['ident', 'package', 'description'],
								          bool   => ['can_be_public', 'is_user', 'is_group', 'is_resource'],
								          other  => ['rowid', 'modified', 'type_id'],
								 );
	$sql .= " WHERE rowid=$rowid";

	ExecSQL($dbh, $sql);
}



# ============================================================================

=head2 ApplicationRemove($dbh, $rowid)

ApplicationRemove remove an application (and its functions and methods) from DB

=cut

# ==========================================================================='

sub ApplicationRemove
{
        my ($dbh, $rowid) = @_;
        my $sql = "DELETE FROM m_method USING m_function WHERE m_function.application_id = $rowid AND m_method.function_id = m_function.rowid";
        ExecSQL($dbh, $sql);

		ExecSQL($dbh, "DELETE FROM m_application_group_allowed WHERE application_id = $rowid");

        $sql = "DELETE FROM m_application_group WHERE application_id=$rowid";
        ExecSQL($dbh, $sql);

        $sql = "DELETE FROM m_profile_function USING m_function WHERE m_function.application_id = $rowid AND m_profile_function.function_id = m_function.rowid";
        ExecSQL($dbh, $sql);

        $sql = "DELETE FROM m_function WHERE m_function.application_id = $rowid";
        ExecSQL($dbh, $sql);

        $sql = "DELETE FROM m_instance_application WHERE application_id = $rowid";
        ExecSQL($dbh, $sql);    

        $sql = "DELETE FROM m_application WHERE rowid = $rowid";
        ExecSQL($dbh, $sql);    
}


# ============================================================================

=head2 FunctionCreate($dbh, $ident, $description, $application_id)

FunctionCreate adds a function for given application

returns rowid of newly created function

=cut

# ==========================================================================='

sub FunctionCreate
{
        my ($dbh, $ident, $description, $application_id) = @_;
        $description = st_FormatPostgreSQLString($description);

        my $sql = "insert into m_function(created, modified, ident, description, application_id)
                                   values(now(), now(), '$ident', '$description', $application_id)";
        
        ExecSQL($dbh, $sql);

        $sql = "select rowid from m_function where ident = '$ident' and application_id=$application_id";
        my $result = SelectSingle($dbh, $sql);

        if (!defined($result))
        {
            throw Mioga2::Exception::Simple("CreateFunction()", __x("Cannot get rowid for function ident = '{ident}' and application_id = {application_id}", ident => $ident, application_id => $application_id));
        }
        my $rowid = $result->{rowid};

        return $rowid;
}



# ============================================================================

=head2 FunctionUpdate($dbh, $func_id, $ident, $description)

FunctionUpdate update a function

=cut

# ==========================================================================='

sub FunctionUpdate
{
        my ($dbh, $func_id, $ident, $description) = @_;
        $description = st_FormatPostgreSQLString($description);

        my $sql = "UPDATE m_function SET modified = now(), ident = '$ident', description = '$description' WHERE rowid = $func_id";
        ExecSQL($dbh, $sql);
}


# ============================================================================

=head2 FunctionRemove($dbh, $func_id)

FunctionRemove remove a function (and its methods) from DB

=cut

# ==========================================================================='
sub FunctionRemove
{
        my ($dbh, $func_id) = @_;

        my $sql = "DELETE FROM m_method WHERE function_id = $func_id";
        ExecSQL($dbh, $sql);

        $sql = "DELETE FROM m_profile_function WHERE function_id = $func_id";
        ExecSQL($dbh, $sql);
        
        $sql = "DELETE FROM m_function WHERE rowid = $func_id";
        ExecSQL($dbh, $sql);
}




# ============================================================================

=head2 MethodCreate($dbh, $ident, $function_id, $type)

MethodCreate adds a method for the given function

returns rowid of newly created method

=cut

# ============================================================================

sub MethodCreate
{
        my ($dbh, $ident, $function_id, $type) = @_;

		my $type_id = SelectSingle($dbh, "SELECT * FROM m_method_type WHERE ident='$type'");

		if(!defined $type_id) {
			throw Mioga2::Exception::Simple("APIApplication", "Unknown method type $type");
		}

        my $sql = "insert into m_method(created, modified, ident, function_id, type_id)
                   values(now(), now(), '$ident', $function_id, $type_id->{rowid})";
        
        ExecSQL($dbh, $sql);

        $sql = "select rowid from m_method where ident = '$ident' and function_id=$function_id";
        my $result = SelectSingle($dbh, $sql);
        
        if (!defined($result))
        {
            throw Mioga2::Exception::Simple("CreateMethod()", __x("Cannot get rowid for Method ident = '{ident}'  and function_id={function_id}", ident => $ident, function_id => $function_id));
        }

        my $rowid = $result->{rowid};

        return $rowid;
}



# ============================================================================

=head2 MethodRemove($dbh, $method_id)

MethodRemove remove a method from DB

returns rowid of newly created method

=cut

# ==========================================================================='

sub MethodRemove
{
        my ($dbh, $method_id) = @_;

        my $sql = "DELETE FROM m_method WHERE rowid=$method_id";
        ExecSQL($dbh, $sql);
}


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Old::User

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
