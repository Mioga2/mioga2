# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
database.pm: Database Access methods

=head1 DESCRIPTION

This module contains functions for sending requests to database and managing
transactions.

This module is to be deprecated. Use object-oriented Mioga2::Database instead.

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::tools::database;
use strict;

use Locale::TextDomain::UTF8 'tools_database';

use vars qw(@ISA @EXPORT);
use Error qw(:try);
use Data::Dumper;
use Mioga2::Exception::DB;
use Mioga2::tools::string_utils;

@ISA = qw(Exporter);
@EXPORT = qw( 
              SelectSingle 
              SelectMultiple 
              ExecSQL
			  GetLastInsertId
			  BeginTransaction EndTransaction RollbackTransaction
			  BuildInsertRequest BuildUpdateRequest
            );

my $debug = 0;



# ============================================================================

=head2 SelectSingle ($dbh, $request, $args)

	Run an SQL request that return just one entry.
	
=cut

# ============================================================================

sub SelectSingle
{
	my ($dbh, $select, $args) = @_;
	my $result;
	
	if(!defined $args) {
		$args = [];
	}

	# Check request for possible injection
	CheckRequest ($select);

  print STDERR "SelectSingle: $select\n" if $debug;
	my $sth = $dbh->prepare($select);

	if (defined($sth)) {

		if ($sth->execute(@$args)) {

			my $row = $sth->fetchrow_hashref();
			if (defined($row)) {

				$result = { };
				my $key;

				foreach $key (keys(%$row)) {
					$result->{$key} = $row->{$key};
				}
			}
		}
		else {
			throw Mioga2::Exception::DB("SelectSingle()", $DBI::err,  $DBI::errstr, $select, $args);
		}
	}
	else {
		throw Mioga2::Exception::DB("SelectSingle()", $DBI::err,  $DBI::errstr, $select, $args);
	}
	
	return $result;
}



# ============================================================================

=head2 SelectMultiple ($dbh, $request, $args)

	Run an SQL request that return multiple entry.
	
=cut

# ============================================================================

sub SelectMultiple
{
	my ($dbh, $select, $args) = @_;
	my $result;
	
	if(!defined $args) {
		$args = [];
	}

	# Check request for possible injection
	CheckRequest ($select);

  print STDERR "SelectMultiple: $select\n" if $debug;
	my $sth = $dbh->prepare($select);
	if (defined($sth)) {
		my $res = $sth->execute(@$args);

		if (defined($res)) {
			my $irow = 0;
			my $rows = $sth->fetchall_arrayref();

			if (defined($rows)) {
				$result = [ ];
				my $row;

				foreach $row (@$rows) {
					$result->[$irow] = { };

					my $col;
					my $icol = 0;
					my $name;

					foreach $col (@$row) {
						$name = $sth->{NAME_lc}->[$icol];
						$result->[$irow]->{$name} = $col;
						$icol++;
					}
					$irow++;
				}
			}
		}
		else {
			throw Mioga2::Exception::DB("SelectMultiple()", $DBI::err,  $DBI::errstr, $select, $args);
		}
	}
	else {
		throw Mioga2::Exception::DB("SelectMultiple()", $DBI::err,  $DBI::errstr, $select, $args);
	}
	
	return $result;
}



# ============================================================================

=head2 ExecSQL ($dbh, $request, $args)

	Run an SQL request that return no entry (INSERT, DELETE, ...).
	
=cut

# ============================================================================

sub ExecSQL
{
	my ($dbh, $sql, $args) = @_;

	if(!defined $args) {
		$args = [];
	}

	# Check request for possible injection
	CheckRequest ($sql);

  print STDERR "ExecSQL: $sql\n" if $debug;
	my $rv;
	if (@$args == 0)
	{
		$rv = $dbh->do($sql, {pg_direct => 1});
	}
	else 
	{
		my $sth = $dbh->prepare($sql);
		$rv  = $sth->execute(@$args);
	}
	if (!defined($rv)) {
		throw Mioga2::Exception::DB("ExecSQL()", $DBI::err,  $DBI::errstr, $sql, $args);
	}

	return $rv;
}
# ============================================================================

=head2 GetLastInsertId ($dbh, $table_name)

	Read the last insert id for given table and return it. Must be used on a
	transaction, after an insert order, on table with a serial field.
	
=cut

# ============================================================================
sub GetLastInsertId
{
	my ($dbh, $table_name) = @_;

  print STDERR "GetLastInsertId for $table_name\n" if $debug;
	my $rv  = $dbh->last_insert_id(undef,undef,$table_name,undef);
	if (!defined($rv)) {
		throw Mioga2::Exception::DB("GetLastInsertId()", $DBI::err,  $DBI::errstr, "");
	}

	return $rv;
}
# ============================================================================

=head2 BeginTransaction ($dbh)
	
	Start a SQL transaction. 
	MUST be ended by a EndTransaction (that commit the modification to database)
	or a RollbackTransaction (that roll back modification since BeginTransaction).
	
=cut

# ============================================================================

sub BeginTransaction
{
	my ($dbh) = @_;

  print STDERR "Beginning transaction\n" if $debug;
	if($dbh->{AutoCommit})
	{
		$dbh->{AutoCommit} = 0;
		
		warn("Mioga2::tools::database::BeginTransaction() ok") if ($debug > 0);
	}
	else
	{
		warn("Mioga2::tools::database::BeginTransaction() Already in transaction mode") if ($debug > 0);
		throw Mioga2::Exception::DB("BeginTransaction()",
		                            __"try to begin transaction while already in transaction mode", "AutoCommit = 0");
	}
}



# ============================================================================

=head2 EndTransaction ($dbh)
	
	End a SQL transaction with a COMMIT. 
	
=cut

# ============================================================================

sub EndTransaction
{
	my ($dbh) = @_;
	
	print STDERR "Ending transaction\n" if $debug;
	if (! $dbh->{AutoCommit}) {
		warn("Mioga2::tools::database::EndTransaction() : commit") if ($debug > 0);

		$dbh->commit();

		$dbh->{AutoCommit} = 1;
	}

	else {
		throw Mioga2::Exception::DB("database::EndTransaction", 
		                            __"Can't commit when AutoCommit is On", "AutoCommit = 1");
	}
}



# ============================================================================

=head2 RollbackTransaction ($dbh)
	
	End a SQL transaction with a ROLLBACK. 
	
=cut

# ============================================================================

sub RollbackTransaction
{
	my ($dbh) = @_;
	
	print STDERR "Rollback transaction\n" if $debug;
	if (! $dbh->{AutoCommit}) {
		warn("Mioga2::tools::database::EndTransaction() : commit") if ($debug > 0);

		$dbh->rollback();

		$dbh->{AutoCommit} = 1;
	}
	else {
		throw Mioga2::Exception::DB("database::RollbackTransaction", 
		                            __"Can't rollback when AutoCommit is On", "AutoCommit = 1");
	}
}




# ============================================================================

=head2 BuildInsertRequest ($values, %params)

	Build an INSERT INTO ...  sql request based on values and params.

	Params are :
	   table => table_name,
	   bool => list of boolean attr name
	   string => list of string attr name
	   other => other fields. If not present, all the field present in %params 
	            are added.
	
=cut

# ============================================================================

sub BuildInsertRequest {
	my ($values, %params) = @_;

	my @keys;
	my @vals;

	foreach my $key ( keys %$values ) {

		if(grep {$_ eq $key} @{$params{bool}}) {
			push @keys, $key;
	
			if($values->{$key}) {
				push @vals, "'t'";
			}
			else {
				push @vals, "'f'";
			}
		}

		elsif(grep {$_ eq $key} @{$params{string}}) {
			push @keys, $key;
			push @vals, "'".st_FormatPostgreSQLString($values->{$key})."'";
		}

		elsif( (! exists $params{other}) or
			   ( grep {$_ eq $key} @{$params{other}}) )  {
			push @keys, $key;
			push @vals, $values->{$key};
		}
	}

	my $sql = "INSERT INTO $params{table} ( " . join(',', @keys) . ')' .
  		      "VALUES (" . join(",", @vals) .")";

  print STDERR "BuildInsertRequest SQL: $sql\n" if $debug;
	return $sql;
}



# ============================================================================

=head2 BuildUpdateRequest ($values, %params)

	Build an UPDTATE ...  sql request based on values and params.

	Params are :
	   table => table_name,
	   bool => list of boolean attr name
	   string => list of string attr name
	   other => other fields. If not present, all the field present in %params 
	            are added.
	
=cut

# ============================================================================

sub BuildUpdateRequest {
	my ($values, %params) = @_;

	my @entries;
	my $val;

	foreach my $key ( keys %$values ) {
		if(grep {$_ eq $key} @{$params{bool}}) {

			if($values->{$key}) {
				$val = "'t'";
			}
			else {
				$val = "'f'";
			}

			push @entries, "$key=$val";
		}

		elsif(grep {$_ eq $key} @{$params{string}}) {
			push @entries, "$key='".st_FormatPostgreSQLString($values->{$key})."'";
		}

		elsif( (! exists $params{other}) or
			   ( grep {$_ eq $key} @{$params{other}}) )  {
			push @entries, "$key=$values->{$key}";
		}
	}


	my $sql = "UPDATE $params{table} SET " . join(',', @entries);

  print STDERR "BuildUpdateRequest SQL: $sql\n" if $debug;
	return $sql;
}


#===============================================================================

=head2 CheckRequest

Check that a request doesn't seem to contain code injection and raise a warning if suspicious

=cut

#===============================================================================
sub CheckRequest {
	my ($req) = @_;

	# Count total number of single quotes
	my $chk = $req;
	my $nb_quotes = ($chk =~ s/(\')/$1/g);

	# Count number of backslashed single quotes
	$chk = $req;
	my $nb_backslashed = ($chk =~ s/(\\\')/$1/g);

	# Count number of doubled single quotes
	$chk = $req;
	my $nb_doubled = ($chk =~ s/(\'\')/$1/g);

	# Count number of unescaped quotes
	my $nb_unesc = $nb_quotes - $nb_backslashed - 2*$nb_doubled;

	if ($nb_unesc%2) {
		print STDERR "Mioga2::tools::database::CheckRequest suspicious request \"$req\"\n";
	}
}	# ----------  end of subroutine CheckRequest  ----------

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 DBI

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
