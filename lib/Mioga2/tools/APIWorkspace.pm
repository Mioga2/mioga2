# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
APIWorkspace.pm: workspace manipulation methods

=head1 DESCRIPTION

This module contains functions for manipulating workspace datas

=head1 METHODS DESRIPTION

=cut
package Mioga2::tools::APIWorkspace;
use strict;

use Locale::TextDomain::UTF8 'tools_apiworkspace';

use vars qw(@ISA @EXPORT);
use Error qw(:try);
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;

@ISA = qw(Exporter);
@EXPORT = qw( 
		WorkspaceGetMenuBranchesForUser
		WorkspaceGetSubmenuBranches
		WorkspaceGetMenuLeafs
		WorkspaceGetMainMenuLeafs
		WorkspaceGetDefaultURL
		WorkspaceGetEntries
		WorkspaceGetVisibleEntries
		WorkspaceUpdateEntriesVisibility
		WorkspaceUpdateEntryOrder
		WorkspaceInitializeUser
		WorkspaceURLModify
		WorkspaceResizeModify
		WorkspaceGetResize
		WorkspaceCreateDefaultURL
		WorkspaceCreateUserEntry
		WorkspaceLinkModify
		WorkspaceLinkAdd
		WorkspaceLinkDelete
		WorkspaceGetLink
		WorkspaceGetLinks
            );

my $debug = 0;


# ============================================================================

=head2 WorkspaceGetMenuBranchesForUser($dbh, $user_id)

	return the first categories for a user menu

=cut

# ============================================================================
sub WorkspaceGetMenuBranchesForUser
{
	my ($dbh, $user_id) = @_;
	my $sql = "select *
	           from workspace_branch 
		   where 
		   user_id = $user_id 
		   and ancestor_id = 0
		   order by number ASC";
	
	my $res = SelectMultiple ($dbh, $sql);

	if (!defined($res))
	{
		throw Mioga2::Exception::DB("Mioga2::tools::APIWorkspace::WorkspaceGetMenuBranchForUser", $DBI::err,  $DBI::errstr, $sql);
	}

	return $res;

}


# ============================================================================

=head2 WorkspaceGetSubmenuBranches($dbh, $branch_id)

	return ordered list of submenu branches for the given branch

=cut

# ============================================================================
sub WorkspaceGetSubmenuBranches
{
	my ($dbh, $branch_id) = @_;
	my $sql = "select *
	           from workspace_branch 
		   where 
		   ancestor_id = $branch_id 
		   order by number ASC";
	
	my $res = SelectMultiple ($dbh, $sql);

	#if (!defined($res))
	#{
	#	throw Mioga2::Exception::DB("Mioga2::tools::APIWorkspace::WorkspaceGetSubmenuBranches", $DBI::err,  $DBI::errstr, $sql);
	#}

	return $res;

}


# ============================================================================

=head2 WorkspaceGetMenuLeafs($dbh, $branch_id)

	return ordered list of leafs (links) of the given branch

=cut

# ============================================================================
sub WorkspaceGetMenuLeafs
{
	my ($dbh, $branch_id) = @_;
	my $sql = "select *
	           from workspace_link 
		   where 
		   ancestor_id = $branch_id 
		   order by number ASC";
	
	my $res = SelectMultiple ($dbh, $sql);

	if (!defined($res))
	{
		throw Mioga2::Exception::DB("Mioga2::tools::APIWorkspace::WorkspaceGetMenuLeafs", $DBI::err,  $DBI::errstr, $sql);
	}

	return $res;

}

# ============================================================================

=head2 WorkspaceGetMainMenuLeafs($dbh, $user_id)

	return ordered list of leafs (links) for the user $user_id

=cut

# ============================================================================
sub WorkspaceGetMainMenuLeafs
{
	my ($dbh, $user_id) = @_;
	my $sql = "select *
	           from workspace_link 
		   where 
		   ancestor_id is NULL
		   and user_id = $user_id
		   order by number ASC";
	
	my $res = SelectMultiple ($dbh, $sql);

	if (!defined($res))
	{
		throw Mioga2::Exception::DB("Mioga2::tools::APIWorkspace::WorkspaceGetMenuLeafs", $DBI::err,  $DBI::errstr, $sql);
	}

	return $res;

}

# ============================================================================

=head2 WorkspaceGetDefaultURL($dbh, $user_id)

	return default URL for the given user

=cut

# ============================================================================
sub WorkspaceGetDefaultURL
{
	my ($dbh, $user_id) = @_;
	my $sql = "select workspace_default_url.default_url, m_home_url.home_url
	           from workspace_default_url, m_home_url
		   where 
		   workspace_default_url.user_id = $user_id AND 
           m_home_url.user_id = $user_id";
	
	my $res = SelectSingle ($dbh, $sql);

	warn Dumper($res) if ($debug);

	return $res;

}

# ============================================================================

=head2 WorkspaceGetEntries($dbh, $user_id)

	return  entries in workspace menu for user_id $user_id

=cut

# ============================================================================
sub WorkspaceGetEntries
{
	my ($dbh, $user_id) = @_;
	my $sql = "select *
	           from workspace_display_entry 
		   where 
		   user_id = $user_id 
		   order by number
		   ";
	
	my $res = SelectMultiple ($dbh, $sql);

	return $res;

}

# ============================================================================

=head2 WorkspaceGetVisibleEntries($dbh, $user_id)

	return visble entries in workspace menu for user_id $user_id

=cut

# ============================================================================
sub WorkspaceGetVisibleEntries
{
	my ($dbh, $user_id) = @_;
	my $sql = "select *
	           from workspace_display_entry 
		   where 
		   user_id = $user_id 
		   and display_ok = 't'
		   order by number
		   ";
	
	my $res = SelectMultiple ($dbh, $sql);

	return $res;

}

# ============================================================================

=head2 WorkspaceUpdateEntriesVisibility($dbh, $user_id, $entries)

	update the entries display ok attribute

	$entry is a hash_ref :

	$entry->{entry_1} == 0 if the entry is not selected
	$entry->{entry_2} == 1 if the entry is selected

=cut

# ============================================================================
sub WorkspaceUpdateEntriesVisibility
{
	my ($dbh, $user_id, $entries) = @_;

	my $sql_selected = "update workspace_display_entry set display_ok = 't' where user_id = $user_id and ident in ('";
	my $sql_deselected = "update workspace_display_entry set display_ok = 'f' where user_id = $user_id and ident in ('";
	
	my @selected_entries;
	my @deselected_entries;

	foreach my $key (keys %{$entries})
	{
		if ( $entries->{$key} == 1 )
		{
			push (@selected_entries, $key);
		}
		else
		{
			push (@deselected_entries, $key);
		}
	}

	
	$sql_selected .= join ("', '", @selected_entries);
	$sql_deselected .= join ("', '", @deselected_entries);

	$sql_selected .= "')";
	$sql_deselected .= "')";

	if (@selected_entries)
	{
		ExecSQL($dbh, $sql_selected);
	}

	if (@deselected_entries)
	{
		ExecSQL($dbh, $sql_deselected);
	}
}

# ============================================================================

=head2 WorkspaceUpdateEntryOrder($dbh, $user_id, $entry_name, $value)

	update the entry $entry_name number attribute with $value for the
	user $user_id

=cut

# ============================================================================
sub WorkspaceUpdateEntryOrder
{
	my ($dbh, $user_id, $entry_name, $value) = @_;

	my $sql = "update workspace_display_entry set number = $value where user_id = $user_id and ident = '$entry_name'";
	
	ExecSQL($dbh, $sql);
}

# ============================================================================

=head2 WorkspaceInitializeUser($conf, $user_id)

	return the first categories for a user menu

=cut

# ============================================================================
sub WorkspaceInitializeUser
{
	my ($config, $user_id) = @_;

	my $dbh = $config->GetDBH();

	my $sql_count_url = "select count(*) as count from workspace_default_url where user_id = $user_id";
	my $count_url = SelectSingle($dbh, $sql_count_url);
	
	if (defined($count_url) and $count_url->{count} == 0)
	{
		# we create the new default url for user $values{user_id}
		# -------------------------------------------------------
		my $values = { created => 'now()', modified => 'now()', user_id => $user_id, default_url => '' };
			
		my $sql = BuildInsertRequest($values,  table  => 'workspace_default_url',
									 string => ['default_url'],
									 other  => ['modified', 'created', 'user_id'],
									 );
		
		
		ExecSQL($dbh, $sql);
	}

	$sql_count_url = "select count(*) as count from m_home_url where user_id = $user_id";
	$count_url = SelectSingle($dbh, $sql_count_url);
		
	if (defined($count_url) and $count_url->{count} == 0)
	{
		# we create the new default url for user $values{user_id}
		# -------------------------------------------------------
		my $values = { created => 'now()', modified => 'now()', user_id => $user_id, home_url => '' };
		
		my $sql = BuildInsertRequest($values,  table  => 'm_home_url',
									 string => ['home_url'],
									 other  => ['modified', 'created', 'user_id'],
									 );
		
		
		ExecSQL($dbh, $sql);
	}


}
# ============================================================================

=head2 WorkspaceURLModify ($config, $url)

	Modify a default url or create it if necessary.
	
	$url is a url description (must contains a "rowid" field).
	
=cut

# ============================================================================
sub WorkspaceURLModify
{
	my ($config, $url) = @_;

	my $dbh = $config->GetDBH();

	# ------------------------------------------------------
	# Set default values from Mioga configuration.
	# ------------------------------------------------------
	my %values = %$url;
	$values{modified} = 'now()';


	# ------------------------------------------------------
	# make the choice between update or creation
	# for the default url
	# ------------------------------------------------------
	if(exists $values{default_url}) {
		# we update the default_url
		# -------------------------
		my $sql = BuildUpdateRequest(\%values, table  => "workspace_default_url",
									 string => [ qw(default_url) ],
									 other  => [ qw(modified) ],
									 );
		
		$sql .= " WHERE user_id = $values{user_id}";
		
		ExecSQL($dbh, $sql);
	}


	# ------------------------------------------------------
	# make the choice between update or creation
	# for the home url
	# ------------------------------------------------------
	if(exists $values{home_url}) {
		# we update the default_url
		# -------------------------
		my $sql = BuildUpdateRequest(\%values, table  => "m_home_url",
									 string => [ qw(home_url) ],
									 other  => [ qw(modified) ],
									 );
		
		$sql .= " WHERE user_id = $values{user_id}";
		
		
		ExecSQL($dbh, $sql);
	}
}

# ============================================================================

=head2 WorkspaceResizeModify ($config, $resize)

	Modify a default resize mode or create it if necessary.
	
	$resize is a resizing description (must contains a "user_id" field).
	
=cut

# ============================================================================
sub WorkspaceResizeModify
{
	my ($config, $resize) = @_;

	my $dbh = $config->GetDBH();

	# ------------------------------------------------------
	# Set default values from Mioga configuration.
	# ------------------------------------------------------
	my %values = %$resize;
	$values{modified} = 'now()';


	# ------------------------------------------------------
	# make the choice between update or creation
	# for the default url
	# ------------------------------------------------------
	my $sql_count_url = "select count(*) as count from workspace_resize where user_id = $values{user_id}";
	my $count_url = SelectSingle($dbh, $sql_count_url);

	if (defined($count_url) and $count_url->{count} == 1)
	{
		# we update the default_url
		# -------------------------
		my $sql = BuildUpdateRequest(\%values, table  => "workspace_resize",
									           bool => [ qw(resize) ],
									           other  => [ qw(modified) ],
									 );

		$sql .= " WHERE user_id = $values{user_id}";


		ExecSQL($dbh, $sql);
	}
	else
	{
		# we create the new default url for user $values{user_id}
		# -------------------------------------------------------
		$values{created} = 'now()';

		my $sql = BuildInsertRequest(\%values,  table  => 'workspace_resize',
						bool => ['resize'],
						other  => ['modified', 'created', 'user_id'],
			);


		ExecSQL($dbh, $sql);
	}
}

# ============================================================================

=head2 WorkspaceGetResize ($dbh, $user_id)

	return menu resizable preference for the given user $user_id
	
=cut

# ============================================================================
sub WorkspaceGetResize
{
	my ($dbh, $user_id) = @_;

	my $sql = "select * from workspace_resize where user_id = $user_id";

	my $result = SelectSingle($dbh, $sql);

	if (!defined($result))
	{
		return 0;
	}

	if ($result->{resize})
	{
		return 1;
	}
	else
	{
		return 0;
	}

}


# ============================================================================

=head2 WorkspaceCreateDefaultURL($conf, $params)

	Create default URL for User. $param must have user_id and default_url entries

=cut

# ============================================================================
sub WorkspaceCreateDefaultURL
{
	my ($conf, $params) = @_;

	my $dbh = $conf->GetDBH();

	$params->{created} = 'now()';
	$params->{modified} = 'now()';

	ExecSQL($dbh, "DELETE FROM workspace_default_url WHERE user_id = " . $params->{user_id});

	my $sql = BuildInsertRequest($params,  table  => 'workspace_default_url',
					string => ['default_url'],
					other  => ['modified', 'created', 'user_id'],
	);

	ExecSQL($dbh, $sql);
}

# ============================================================================

=head2 WorkspaceLinkModify ($dbh, $link)

	Modify a a link 

	$link is a link description (must contains a "rowid" field).
	
=cut

# ============================================================================
sub WorkspaceLinkModify
{
	my ($dbh, $link) = @_;

	my %values = %$link;
	$values{modified} = 'now()';

	my $sql;
	# we update the link
	# -------------------------
	if (defined($values{number}))
	{
		$sql = BuildUpdateRequest(\%values, table  => "workspace_link",
										   string => [ qw(link ident) ],
										   other  => [ qw(modified number) ],
									 );
	}
	else
	{
		$sql = BuildUpdateRequest(\%values, table  => "workspace_link",
										   string => [ qw(link ident) ],
										   other  => [ qw(modified) ],
									 );
	}

	$sql .= " WHERE rowid = $values{rowid}";

	ExecSQL($dbh, $sql);
}

# ============================================================================

=head2 WorkspaceLinkAdd ($dbh, $link)

	Modify a a link 

	$link is a hasref describing link
	
=cut

# ============================================================================
sub WorkspaceLinkAdd
{
	my ($dbh, $link) = @_;

	my %values = %$link;
	$values{modified} = 'now()';
	$values{created} = 'now()';

	my $max = SelectSingle($dbh, "select max(number) as max from workspace_link where user_id = $values{user_id}");

	if(defined $max and exists $max->{max} and defined $max->{max}) {
		$values{number} = $max->{max} + 1;
	}
	else {
		$values{number} = 0;
	}


	# we update the link
	# -------------------------
	my $sql = BuildInsertRequest(\%values, table  => "workspace_link",
									   string => [ qw(link ident) ],
									   other  => [ qw(modified created number user_id) ],
								 );

	ExecSQL($dbh, $sql);
}

# ============================================================================

=head2 WorkspaceLinkDelete ($dbh, $link_id)

	Delete a link  with rowid = $link_id
	
=cut

# ============================================================================
sub WorkspaceLinkDelete
{
	my ($dbh, $link_id) = @_;

	my $sql = "delete from workspace_link where rowid = $link_id";

	ExecSQL($dbh, $sql);
}

# ============================================================================

=head2 WorkspaceGetLink ($dbh, $rowid)

	return link for the given rowid or undef

=cut

# ============================================================================
sub WorkspaceGetLink
{
	my ($dbh, $rowid) = @_;

	my $sql = "select * from workspace_link where rowid = $rowid";

	my $result = SelectSingle ($dbh, $sql);

	return $result;
}

# ============================================================================

=head2 WorkspaceGetLinks ($dbh, $user_id)

	return links for the given user_id or undef

=cut

# ============================================================================
sub WorkspaceGetLinks
{
	my ($dbh, $user_id) = @_;

	my $sql = "select * from workspace_link where user_id = $user_id order by number";

	my $result = SelectMultiple ($dbh, $sql);

	return $result;
}

# ============================================================================

=head2 WorkspaceCreateUserEntry($conf, $params)

	Create user entry in workspace menu. $param must have user_id, ident, display_ok and number entries

=cut

# ============================================================================
sub WorkspaceCreateUserEntry
{
	my ($conf, $params) = @_;

	my $dbh = $conf->GetDBH();

	$params->{created} = 'now()';
	$params->{modified} = 'now()';
	my $sql = BuildInsertRequest($params,  table  => 'workspace_display_entry',
					string => ['ident'],
					other  => ['modified', 'created', 'user_id', 'number'],
					bool  => ['display_ok'],
	);

	ExecSQL($dbh, $sql);
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Workspace

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
