# $Header$
# ============================================================================
# MiogaII Project (C) 2003 The MiogaII Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description: 

=head1 NAME
	
APIPoll.pm : API for Poll management class

=head1 DESCRIPTION

This API is useful for poll management

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================
# $Log$
# Revision 1.6  2004/06/14 11:26:38  clargeau
# adding description in animation mode
# changing question type representation
# changing question/answer field width
#
# Revision 1.5  2004/06/02 08:03:00  clargeau
# fixing bugs 0000343 0000344 0000345 0000346 0000347
#
# Revision 1.4  2004/05/18 08:35:48  lsimonneau
# Major features enhancements. Implementation of user/team/group/resource deletion.
#
# Revision 1.3  2004/03/18 14:51:54  clargeau
# adding extraction functions
#
# Revision 1.2  2004/03/17 15:49:48  clargeau
# adding user validation
# need better results extraction
#
# Revision 1.1  2004/03/16 15:21:02  clargeau
# initial commit for poll application
# just translate Mioga 1.xx Poll to Mioga2 Poll
#
#
# ============================================================================
#

package Mioga2::tools::APIPoll;

use vars qw(@ISA @EXPORT);
use strict;
use Exporter;
use Error qw(:try);
use Mioga2::Exception::Simple;
use Mioga2::tools::database;
use Data::Dumper;

my $debug = 0;

@ISA = qw(Exporter);
@EXPORT = qw(CheckToClosePoll PollCreate PollQuestionCreate PollCommentCreate PollAnswerCreate PollGetListForCreator PollGetInfos PollGetQuestionInfos PollGetAnswerInfos PollGetCommentInfos PollGetNbQuestion PollIsAnonymous PollAddComment PollAddVote PollQuestionIsSingle PollDeleteAllVotes PollDeleteVote PollDeleteCommentVote QuestionTotalVote AnswerTotalVote PollGetComments PollCountVoters PollGetQuestionIds PollDeleteComment PollDeleteAnswers PollDeleteQuestions PollDeleteQuestion PollDeletePoll GetEntirePoll GetEntirePollForGroup GetPollResultForGroup  PollVotedNotClosed PollGetUserVote PollGetUserComment PollUserClose PollGetValidatorUsers PollGetUserPoll PollGetUserAnswer PollGetAnswerForQuestion PollRevokeUserFromGroup PollDeleteGroupData);


# ----------------------------------------------------------------------------
sub PollCreate
{
	my ($dbh, $poll_label, $poll_desc, $group_id, $poll_anonymous, $poll_begin_wait, $poll_begin_now, $poll_begin_at, $begin_date, $poll_ending_voters, $poll_ending_at, $poll_ending_date, $poll_user_percent, $poll_user_visibility, $poll_closed) = @_;
	my $sql;
	my $result;
	my $rowid;

	$sql = "insert into poll_description (created, modified, poll_label, description, group_id, anonymous, begin_wait, begin_now, begin_at, begin_date, ending_voters, ending_at, ending_date, user_percent, user_visibility, closed) values(now(), now(), '$poll_label', '$poll_desc', $group_id, '$poll_anonymous', '$poll_begin_wait', '$poll_begin_now', '$poll_begin_at', '$begin_date', '$poll_ending_voters', '$poll_ending_at', '$poll_ending_date', $poll_user_percent, '$poll_user_visibility', '$poll_closed')";
	ExecSQL($dbh, $sql);
	$sql = "select rowid from poll_description where poll_label = '$poll_label' and description='$poll_desc' and group_id=$group_id and anonymous='$poll_anonymous' and begin_wait='$poll_begin_wait' and begin_now='$poll_begin_now' and begin_at='$poll_begin_at' and begin_date='$begin_date' and ending_voters='$poll_ending_voters' and ending_at='$poll_ending_at' and ending_date='$poll_ending_date' and user_percent=$poll_user_percent and user_visibility='$poll_user_visibility' order by rowid desc";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollCreate()", "Cannot get poll for ident = '$poll_label'");
		return undef;
	}
	$rowid = $result->{rowid};

	return $rowid;
}

# ----------------------------------------------------------------------------
sub PollQuestionCreate
{
	my ($dbh, $poll_rowid, $question_label, $type_unique) = @_;
	my $sql;
	my $result;
	my $rowid;

	$sql = "insert into poll_question (created, modified, poll_id, single, question_label) values (now(), now(), $poll_rowid, '$type_unique', '$question_label')";
	ExecSQL($dbh, $sql);
	$sql = "select rowid from poll_question where question_label = '$question_label' and poll_id=$poll_rowid and single='$type_unique' order by rowid desc";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollQuestionCreate()", "Cannot get question for ident = '$question_label'");
		return undef;
	}
	$rowid = $result->{rowid};

	return $rowid;
}

# ----------------------------------------------------------------------------
sub PollCommentCreate
{
	my ($dbh, $poll_rowid, $poll_comment_label) = @_;
	my $sql;
	my $result;
	my $rowid;

	$sql = "insert into poll_comment (created, modified, poll_id, comment_label) values (now(), now(), $poll_rowid, '$poll_comment_label')";
	ExecSQL($dbh, $sql);
	$sql = "select rowid from poll_comment where comment_label = '$poll_comment_label' and poll_id=$poll_rowid order by rowid desc";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollCommentCreate()", "Cannot get comment for ident = '$poll_comment_label'");
		return undef;
	}
	$rowid = $result->{rowid};

	return $rowid;
}

# ----------------------------------------------------------------------------
sub PollAnswerCreate
{
	my ($dbh, $question_rowid, $answer_label) = @_;
	my $sql;
	my $result;
	my $rowid;

	$sql = "insert into poll_answer (created, modified, question_id, answer_label) values (now(), now(), $question_rowid, '$answer_label')";
	ExecSQL($dbh, $sql);
	$sql = "select rowid from poll_answer where answer_label = '$answer_label'";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollAnswerCreate()", "Cannot get answer for ident = '$answer_label'");
		return undef;
	}
	$rowid = $result->{rowid};

	return $rowid;
}


# ----------------------------------------------------------------------------
sub PollGetInfos
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "select poll_label, description, group_id from poll_description where rowid = $rowid";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollGetInfos()", "Cannot get list of group for = '$rowid'");
	}
	return $result;
}

# ----------------------------------------------------------------------------
sub GetEntirePoll
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "select * from poll_description where rowid = $rowid";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("GetEntirePoll()", "Cannot get list of group for = '$rowid'");
	}
	return $result;
}

# ----------------------------------------------------------------------------
sub CheckToClosePoll
{
	my ($dbh, $group_id, $nb_voters) = @_;
	my $sql = "update poll_description set closed='t' 
	           where ending_voters='t' 
		   and user_percent < ( 
		   	select( 
				select count (*) from ( 
					SELECT count(*) 
					from poll_vote_result, poll_question,  m_group_base 
					where poll_question.rowid = poll_vote_result.question_id 
					and poll_description.rowid = poll_question.poll_id 
					and m_group_base.rowid = poll_vote_result.user_id 
					group by m_group_base.rowid) as total
				)*100/($nb_voters) as percent
                  )";
	ExecSQL($dbh, $sql);
}
# ============================================================================

=head2 GetEntirePollForGroup ($dbh, $group_id, $user_id)

	GetEntirePollForGroup return group polls where user can place vote

=cut

# ============================================================================
sub GetEntirePollForGroup
{
	my ($dbh, $group_id, $user_id) = @_;
	my $sql;
	my $result;

	$sql = "select rowid, poll_label 
	from poll_description 
	where 
		begin_wait='f' 
		and group_id = $group_id 
		and (
			begin_now='t' 
			or (
				begin_at='t' 
				and begin_date<=now()
			)
		) 
		and not(
			(
				ending_at='t' and ending_date < now()
			) 
			or (
				ending_voters='t' and closed='t'
			)
		) 
		and (
			ending_voters='t' 
			or ending_at='t'
		) 
		and (
			(
				select count(*) from poll_vote_result, poll_question 
				where poll_vote_result.user_id=$user_id 
				and poll_vote_result.question_id=poll_question.rowid 
				and poll_question.poll_id=poll_description.rowid
			)=0
		)";
	$result = SelectMultiple($dbh, $sql);

	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("GetEntirePollForGroup()", "Cannot get list of poll for group '$group_id'");
	}
	return $result;
}

# ============================================================================

=head2 GetPollResultForGroup ($dbh, $group_id, $is_anim)

	GetPollResultForGroup return results of ended polls

=cut

# ============================================================================
sub GetPollResultForGroup
{
	my ($dbh, $group_id, $is_anim) = @_;
	my $sql;
	my $result;

	$sql = "select rowid, poll_label
	from poll_description
	where
	begin_wait='f'
	and group_id = $group_id
	and ( begin_now='t'
		or ( begin_at='t' and begin_date<=now()))
	and ( ( ending_at='t' and ending_date < now())
		or ( ending_voters='t' and closed='t'))";
	
	$sql .= "and user_visibility='t'" unless ($is_anim);
	$result = SelectMultiple($dbh, $sql);

	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("GetPollResultForGroup()", "Cannot get list of group for = '$group_id'");
	}

	return $result;
}

# ----------------------------------------------------------------------------
sub PollQuestionIsSingle
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "select single from poll_question where rowid = $rowid";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollQuestionIsSingle()", "Cannot get list of poll_question for rowid = '$rowid'");
	}
	return $result;
}

# ----------------------------------------------------------------------------
sub PollGetQuestionInfos
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "select rowid, question_label, single from poll_question where poll_id = $rowid order by rowid";
	$result = SelectMultiple($dbh, $sql);
	#if (!defined($result))
	#{
	#	throw Mioga2::Exception::Simple("PollGetQuestionInfos()", "Cannot get list of question for = '$rowid'");
	#}
	return $result;
}

# ----------------------------------------------------------------------------
sub PollGetComments
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	#$sql = "select firstname, lastname, comment from m_user, poll_comment_answer where comment_id = $rowid and user_id = m_user.rowid";
	$sql = "select * from poll_comment_answer where comment_id = $rowid";
	$result = SelectMultiple($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollGetComments()", "Cannot get list of comments");
	}
	return $result;
}

# ----------------------------------------------------------------------------
sub PollGetNbQuestion
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "select count(*) from poll_question where poll_id = $rowid";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollGetQuestionInfos()", "Cannot get list of question for = '$rowid'");
	}
	return $result;
}

# ----------------------------------------------------------------------------
sub PollIsAnonymous
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "select anonymous from poll_description where rowid = $rowid";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollIsAnonymous()", "Cannot get list of anonymous for = '$rowid'");
	}
	return $result;
}

# ----------------------------------------------------------------------------
sub PollAddComment
{
	my ($dbh, $comment_id, $comment, $user_id) = @_;
	my $sql;
	my $result;

	$sql = "insert into poll_comment_answer (created, modified, comment_id, user_id, comment) values (now(), now(), $comment_id, $user_id, '$comment')";
	ExecSQL($dbh, $sql);
	$sql = "select rowid from poll_comment_answer where user_id = $user_id and comment_id = $comment_id";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollAddComment()", "Cannot set comment_answer for comment_id = $comment_id and user_id = $user_id");
		return undef;
	}
	 my $rowid = $result->{rowid};

	return $rowid;
}

# ----------------------------------------------------------------------------
sub PollAddVote
{
	my ($dbh, $user_id, $question_id, $answer_id) = @_;
	my $sql;
	my $result;

	$sql = "insert into poll_vote_result (modified, user_id, question_id, answer_id) values (now(), $user_id, $question_id, $answer_id)";
	ExecSQL($dbh, $sql);
	$sql = "select rowid from poll_vote_result where question_id = $question_id and user_id = $user_id";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollAddVote()", "Cannot set vaot_answer for question_id = $question_id and user_id = $user_id");
		return undef;
	}
	 my $rowid = $result->{rowid};

	return $rowid;
}

# ----------------------------------------------------------------------------
sub PollDeleteVote
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "delete from poll_vote_result where rowid = $rowid";
	ExecSQL($dbh, $sql);
}

# ----------------------------------------------------------------------------

=head2 PollDeleteAllVotes ($dbh, $user_id, $poll_id)

	delete all votes for poll $poll_id

=cut

# ----------------------------------------------------------------------------
sub PollDeleteAllVotes
{
	my ($dbh, $user_id, $poll_id) = @_;
	my $sql;
	my $result;

	$sql = "delete from poll_vote_result where user_id = $user_id and question_id in (select rowid from poll_question where poll_id = $poll_id)";
	ExecSQL($dbh, $sql);
}

# ----------------------------------------------------------------------------
sub QuestionTotalVote
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "select count(*) from poll_vote_result where question_id = $rowid";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("QuestionTotalVote()", "");
		return undef;
	}
	my $total = $result->{count};

	return $total;
}

# ----------------------------------------------------------------------------
sub AnswerTotalVote
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "select count(*) from poll_vote_result where answer_id = $rowid";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("AnswerTotalVote()", "");
		return undef;
	}
	my $total = $result->{count};

	return $total;
}

# ----------------------------------------------------------------------------
sub PollCountVoters
{
	my ($dbh, $rowid) = @_;

	my $sql = "select distinct user_id from poll_vote_result,poll_question where poll_question.poll_id=$rowid and poll_question.rowid=poll_vote_result.question_id";

	my $result = SelectMultiple($dbh, $sql);

	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollCountVoters()", "");
		return undef;
	}

	return @$result;
}

# ----------------------------------------------------------------------------
sub PollVotedNotClosed
{
	my ($dbh, $rowid) = @_;

	my $sql = "select distinct poll_description.* 
	           from poll_vote_result,poll_question, poll_description
		   where poll_vote_result.user_id=$rowid 
		   and poll_question.rowid=poll_vote_result.question_id 
		   and poll_id not in 
		   (
		   	select poll_user_close.poll_id from poll_user_close 
			where poll_user_close.user_id = $rowid
		   )
		   and poll_id = poll_description.rowid";

	#warn $sql;
	my $result = SelectMultiple($dbh, $sql);

	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollVotedNotClosed()", "");
		return undef;
	}

	return $result;
}

# ----------------------------------------------------------------------------

=head2 PollGetUserVote ($dbh, $user_id, $question_id)

	return rowids of answer for the user $user_id to the question $question_id

=cut

# ----------------------------------------------------------------------------
sub PollGetUserVote
{
	my ($dbh, $user_id, $question_id) = @_;

	my $sql = "select answer_id
	           from poll_vote_result
		   where
		   user_id = $user_id
		   and question_id = $question_id";

	#warn $sql;
	my $result = SelectMultiple($dbh, $sql);

	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollGetUserVote()", "");
		return undef;
	}

	return $result;
}

# ----------------------------------------------------------------------------

=head2 PollUserClose ($dbh, $user_id, $poll_id)

	return close poll

=cut

# ----------------------------------------------------------------------------
sub PollUserClose
{
	my ($dbh, $user_id, $poll_id) = @_;

	my $sql = "insert into poll_user_close
	           (created, modified, poll_id, user_id)
		   values (now(), now(), $poll_id, $user_id)";

	ExecSQL($dbh, $sql);

}

# ----------------------------------------------------------------------------

=head2 PollGetUserComment ($dbh, $user_id, $comment_id)

	return comment of user to the comment $comment_id

=cut

# ----------------------------------------------------------------------------
sub PollGetUserComment
{
	my ($dbh, $comment_id) = @_;

	my $sql = "select *
	           from poll_comment_answer
		   where
		   comment_id = $comment_id";

	#warn $sql;
	my $result = SelectSingle($dbh, $sql);

	return $result;
}

# ----------------------------------------------------------------------------
sub PollDeleteCommentVote
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "delete from poll_comment_answer where rowid = $rowid";
	ExecSQL($dbh, $sql);
}

# ----------------------------------------------------------------------------
sub PollGetAnswerInfos
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "select rowid, answer_label from poll_answer where question_id = $rowid order by rowid";
	$result = SelectMultiple($dbh, $sql);
	#if (!defined($result))
	#{
	#	throw Mioga2::Exception::Simple("PollGetAnswerInfos()", "Cannot get list of answer for = '$rowid'");
	#}
	return $result;
}

# ----------------------------------------------------------------------------
sub PollGetQuestionIds
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "select rowid from poll_question where poll_id = $rowid";
	$result = SelectMultiple($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollGetQuestionIds()", "Cannot get list of answer for = '$rowid'");
	}
	return $result;
}

# ----------------------------------------------------------------------------
sub PollDeleteComment
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "delete from poll_comment where poll_id = $rowid";
	ExecSQL ($dbh, $sql);
}

# ----------------------------------------------------------------------------

=head2

	PollDeleteAnswers($dbh, $rowid_list) deletes answers with rowid in 
	$rowid_list

=cut

# ----------------------------------------------------------------------------
sub PollDeleteAnswers
{
	my ($dbh, $rowid_list) = @_;
	my $sql;
	my $result;

	$sql = "delete from poll_answer where rowid in (";
	$sql .= join (',', @$rowid_list);
	$sql .= ")";
	ExecSQL ($dbh, $sql);
}

# ----------------------------------------------------------------------------

=head2

	PollDeleteQuestions($dbh, $rowid_list) deletes answers with rowid in 
	$rowid_list

=cut

# ----------------------------------------------------------------------------
sub PollDeleteQuestions
{
	my ($dbh, $rowid_list) = @_;
	my $sql;
	my $result;

	$sql = "delete from poll_question where rowid in (";
	$sql .= join (',', @$rowid_list);
	$sql .= ")";
	ExecSQL ($dbh, $sql);
}

# ----------------------------------------------------------------------------
sub PollDeletePoll
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "delete from poll_description where rowid = $rowid";
	ExecSQL ($dbh, $sql);
}

# ----------------------------------------------------------------------------
sub PollDeleteQuestion
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "delete from poll_answer where question_id = $rowid";
	ExecSQL ($dbh, $sql);

	$sql = "delete from poll_question where rowid = $rowid";
	ExecSQL ($dbh, $sql);
}

# ----------------------------------------------------------------------------
sub PollGetCommentInfos
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "select rowid, comment_label from poll_comment where poll_id = $rowid";
	$result = SelectSingle($dbh, $sql);
	if (!defined($result))
	{
		return undef;
	}
	return $result;
}

# ----------------------------------------------------------------------------

=head2 PollGetListForCreator($dbh, $poll_id, $selection)

	Return list of polls for the given poll_id and the $sselection status

	$selection : running, closed or waiting

=cut

# ----------------------------------------------------------------------------
sub PollGetListForCreator
{
	my ($dbh, $rowid, $selection) = @_;

	my $sql = "select rowid, poll_label, description from poll_description where group_id = $rowid";

	if ($selection eq 'running')
	{
		$sql .= " and begin_wait='f' and (closed='f' or (begin_at='t' and ending_date>now()))";
	}
	elsif ($selection eq 'closed')
	{
		$sql .= " and begin_wait='f' and (closed='t' or (begin_at='t' and ending_date<now()))";
	}
	elsif ($selection eq 'waiting')
	{
		$sql .= " and begin_wait='t'";
	}

	my $result = SelectMultiple($dbh, $sql);

	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollGetListForCreator()", "Cannot get list of group for = '$rowid'");
	}

	return $result;
}

# ----------------------------------------------------------------------------
sub PollGetCommentIds
{
	my ($dbh, $rowid) = @_;
	my $sql;
	my $result;

	$sql = "select rowid from poll_comment where poll_id = $rowid";
	$result = SelectMultiple($dbh, $sql);
	if (!defined($result))
	{
		throw Mioga2::Exception::Simple("PollGetCommentIds()", "Cannot get list of comment for = '$rowid'");
	}
	return $result;
}
# ----------------------------------------------------------------------------

=head2 PollGetValidatorUsers

	PollGetValidatorUsers ($dbh, $poll_id)

	return list of user_id having closed poll $poll_id

=cut

# ----------------------------------------------------------------------------
sub PollGetValidatorUsers
{
	my ($dbh, $poll_id) = @_;

	my $sql = "select user_id
                   from poll_user_close
		   where
		   poll_user_close.poll_id = $poll_id
	          ";

	my $result = SelectMultiple($dbh, $sql);

	return $result;
}

# ----------------------------------------------------------------------------

=head2 PollGetUserPoll

	PollGetUserPoll ($dbh, $user_id, $poll_id)

	return poll description for closed poll by user $user_id

=cut

# ----------------------------------------------------------------------------
sub PollGetUserPoll
{
	my ($dbh, $user_id, $poll_id) = @_;

	my $sql = "select poll_description.poll_label, poll_description.description, poll_question.rowid as question_id, poll_question.question_label 
                   from poll_description, poll_question, poll_user_close
		   where
		   poll_user_close.poll_id = $poll_id
		   and poll_user_close.user_id = $user_id
		   and poll_description.rowid = poll_user_close.poll_id
		   and poll_question.poll_id = poll_description.rowid
	          ";

	my $result = SelectMultiple($dbh, $sql);

	return $result;
}

# ----------------------------------------------------------------------------

=head2 PollGetUserAnswer

	PollGetUserAnswer ($dbh, $user_id, $question_id)

	return poll description for closed poll by user $user_id

=cut

# ----------------------------------------------------------------------------
sub PollGetUserAnswer
{
	my ($dbh, $user_id, $question_id) = @_;

	my $sql = "
                  select poll_answer.*
		  from poll_answer, poll_vote_result
		  where
		  poll_vote_result.user_id = $user_id
		  and poll_vote_result.question_id = $question_id
		  and poll_answer.rowid = poll_vote_result.answer_id
		  ";

	my $result = SelectMultiple($dbh, $sql);

	return $result;
}

# ----------------------------------------------------------------------------

=head2 PollGetAnswerForQuestion

	PollGetAnswerForQuestion ($dbh, $question_id)

	return answer_label, number of answers for question $question_id

=cut

# ----------------------------------------------------------------------------
sub PollGetAnswerForQuestion
{
	my ($dbh, $question_id) = @_;

	my $sql = "select count(answer_id) as count, answer_label  from poll_answer left join poll_vote_result on (poll_answer.rowid = poll_vote_result.answer_id) where poll_answer.rowid in (select rowid from poll_answer where poll_answer.question_id = $question_id) group by answer_label";

	my $result = SelectMultiple($dbh, $sql);

	return $result;
}


# ============================================================================

=head2 PollRevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

	Remove user data in database when a user is revoked of a group.

	This function is called when :
	- a user is revoked of a group
	- a team is revoked of a group for each team member not namly
	  invited in group
	- a user is revoked of a team  for each group where the team is
	  member and the user not namly invited in group

=cut

# ============================================================================

sub PollRevokeUserFromGroup {
	my ( $config, $group_id, $user_id, $group_animator_id) = @_;

	my $dbh = $config->GetDBH();

	ExecSQL($dbh, "DELETE FROM poll_comment_answer ".
                  "USING poll_comment, poll_description " .
			      "WHERE poll_comment_answer.user_id = $user_id AND ".
			      "      poll_comment_answer.comment_id = poll_comment.rowid AND ".
			      "      poll_comment.poll_id = poll_description.rowid AND ".
			      "      poll_description.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM poll_user_close ".
                  "USING poll_description " .
			      "WHERE poll_user_close.user_id = $user_id AND ".
			      "      poll_user_close.poll_id = poll_description.rowid AND ".
			      "      poll_description.group_id = $group_id");
}



# ============================================================================

=head2 PollDeleteGroupData ($config, $group_id)

	Remove group data in database when a group is deleted.

	This function is called when a group/user/resource is deleted.

=cut

# ============================================================================

sub PollDeleteGroupData {
	my ($config, $group_id) = @_;

	my $dbh = $config->GetDBH();

	ExecSQL($dbh, "DELETE FROM poll_vote_result ".
                  "USING poll_question, poll_description " .
			      "WHERE poll_vote_result.question_id = poll_question.rowid AND ".
			      "      poll_question.poll_id = poll_description.rowid AND ".
			      "      poll_description.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM poll_answer ".
                  "USING poll_question, poll_description " .
			      "WHERE poll_answer.question_id = poll_question.rowid AND ".
			      "      poll_question.poll_id = poll_description.rowid AND ".
			      "      poll_description.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM poll_question ".
                  "USING poll_description " .
			      "WHERE poll_question.poll_id = poll_description.rowid AND ".
			      "      poll_description.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM poll_comment_answer ".
                  "USING poll_comment, poll_description " .
			      "WHERE poll_comment_answer.comment_id = poll_comment.rowid AND ".
			      "      poll_comment.poll_id = poll_description.rowid AND ".
			      "      poll_description.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM poll_comment ".
                  "USING poll_description " .
			      "WHERE poll_comment.poll_id = poll_description.rowid AND ".
			      "      poll_description.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM poll_user_close ".
                  "USING poll_description " .
			      "WHERE poll_user_close.poll_id = poll_description.rowid AND ".
			      "      poll_description.group_id = $group_id");

	ExecSQL($dbh, "DELETE FROM poll_description ".
			      "WHERE poll_description.group_id = $group_id");

}


1;
