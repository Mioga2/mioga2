# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
RequestContext.pm: Access class to the Apache request's parameters.

=head1 DESCRIPTION

This module permits to access to the apache request's parameters, like 
URI, access method (GET, PUT, ...), URI arguments, etc.

=head1 METHODS DESRIPTION

=cut

# ============================================================================

package Mioga2::RequestContext;
use strict;

use Locale::TextDomain::UTF8 'requestcontext';

use Error qw(:try);
use Data::Dumper;
use Mioga2::Config;
use Mioga2::URI;
use Mioga2::Crypto;
use Mioga2::Session;
use Mioga2::AppDesc;
use Mioga2::RSS;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::date_utils;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;
use Mioga2::GroupList;
use Mioga2::UserList;
use Mioga2::Login;
use Encode::Detect::Detector;
use Mioga2::Constants;

my $debug = 0;


# ============================================================================

=head2 new ($request, $config)

	Create the object and initialize from the Apache request.
	return a new Mioga2::RequestContext object.
	
=cut

# ============================================================================
sub new {
	my ($class, $r, $config) = @_;

	my $self = {};
	$self->{config} = $config;

	bless($self, $class);

	$self->Initialize($r, $config);

	return $self;
}
# ============================================================================

=head2 GetXML ()

	return an XML representation of the current context.

=head3 Generated XML

	<miogacontext>

       <!-- URI Description -->

       <uri base="base uri (typically /Mioga2/Mioga)"
	        image="image uri (typically /Mioga2/Mioga/images)"
	        help="help uri (typically /Mioga2/Mioga/help)"|;

            bin_uri="bin uri (typically /Mioga2/Mioga/bin)"
            pubbin_uri="public bin uri (typically /Mioga2/Mioga/pubbin)"|;
            xml_uri="xml transaction uri (typically /Mioga2/Mioga/xml)"|;
	        sxml_uri="secure xml uri (typically /Mioga2/Mioga/sxml)"
	        pubxml_uri="public xml transaction uri (typically /Mioga2/Mioga/pubxml)"
			public_dav_uri="public webdav URI (typically /Mioga2/Mioga2/public)"
	        private_dav_uri="private webdav URI (typically /Mioga2/Mioga2/home)"
	        
	        public_dav="public webdav URI for current group (typically /Mioga2/Mioga/public/<group_ident>)"
        	private_dav="private webdav URI for current group (typically /Mioga2/Mioga/public/<group_ident>)"
        	private_bin="private application URI for current group (typically /Mioga2/Mioga/bin/<group_ident>)"
			public_bin="public application URI for current group (typically /Mioga2/Mioga/pubbin/<group_ident>)"
        	sxml="secure XML transaction URI for current group (typically /Mioga2/Mioga/sxml/<group_ident>)"
        	xml="XML transaction URI for current group (typically /Mioga2/Mioga/xml/<group_ident>)"
        	pubxml="public XML transaction URI for current group (typically /Mioga2/Mioga/sxml/<group_ident>)"
	
			login_uri="Login handler uri (typically /Mioga2/Login/)"
 	     />

       <time>Current time. See Mioga2::tools::date_utils->du_GetDateXML</time>

       <user ident="Current user ident"
	   	  home_url="Current user portal URL">
	      Current user name
       </user>

       <group ident="Current group ident" type="Current group type (group|resource|local_user|ldap_user|external_user)" 
	          lang="current user language">
	       Current group name
       </group>

	   <currenturi>Current URI</currenturi>

       <dir>
	      <xsldir>xsl directory</xsldir>
          <langdir>currant language directory</langdir>
	   </dir>

	   <application>
   	       <ident>Current Applicaiton ident</ident>
	       <method>Current method</method>
    	   <type>CUrrent application type</type>
           <name>Current application localized name</name>
           <description>Current application localized description</description>
       </application>
       <referer>Previous visited URL (Referer HTTP header)</referer>
  
	   <history>
           <referer>Last Portal URI</referer>
       </history>

	</miogacontext>

=cut

# ============================================================================

sub GetXML {
  my ($self, %options) = @_;

  return (Mioga2::tools::Convert::PerlToXML ($self->GetContext ()));
}

# ============================================================================

=head2 GetContext

  return a data hash of the current context
    
=cut

# ============================================================================

sub GetContext {
	my ($self, %options) = @_;
	print STDERR "Mioga2::RequestContext::GetContext entering\n" if ($debug);

	# General data
	print STDERR "   get general data\n" if ($debug);
	my $data = {
		miogacontext  => {
			mioga_version => $self->GetConfig->GetMiogaVersion,
			currenturi    => $self->GetURI->GetURI."",
			dir           => {
				xsldir    => $self->{config}->GetXSLDir,
				langdir   => $self->{config}->GetLangDir,
			},
			referer       => $self->{referer},
			uri           => $self->{uri}->GetData($self->{config}),
			is_public     => $self->IsPublic,
		}
	};

	# Instance-specific data
	$data->{miogacontext}->{instance} = {
		ident      => $self->GetConfig ()->GetMiogaIdent (),
	};

	# User-specific data
	if(exists $self->{user}) {
		print STDERR "   get user data\n" if ($debug);
		print STDERR "   user ".Dumper($self->{user})."\n" if ($debug);
		$data->{miogacontext}->{user} = {
			rowid         => $self->{user}->GetRowid,
			ident         => $self->{user}->GetIdent,
			is_autonomous => $self->{user}->IsAutonomous,
			home_url      => "$self->{session}->{__internal}->{user_home_url}",
			firstname     => $self->{user}->GetFirstname,
			lastname      => $self->{user}->GetLastname,
			name          => $self->{user}->GetName,
			email         => $self->{user}->GetEmail,
			lang          => $self->{user}->GetLang,
		};
		$data->{miogacontext}->{time} = du_ISOToHashInUserLocale(du_GetNowISO, $self->{user});

		# User session
		print STDERR "   get session data\n" if ($debug);
		my $session = $self->GetSession;
		$session->{no_header} = 0 if ($options{force_header} or $self->{args}->{force_header});
		if ($options{no_header} or $self->{args}->{no_header} or $session->{no_header} == 1) {
			$session->{no_header} = 1;
			$data->{miogacontext}->{NoHeader} = {};
		}

		# Home URL
		if(! exists $self->{session}->{__internal}->{user_home_url}) {
			$self->{session}->{__internal}->{user_home_url} = "".$self->{user}->GetHomeURL."";
		}

		# Browsing history
		if(exists $self->{session}->{__internal}->{history}) {
			$data->{miogacontext}->{history} = {
				referer   => $self->{session}->{__internal}->{history},
			};
		}

		# Rights to current application method
		if (!$self->IsPublic ()) {
			$data->{UserRights} = {};
			my $rights = $self->GetApplication ()->GetUserRights ();
			for my $right (keys (%$rights)) {
				$data->{UserRights}->{$right} = $rights->{$right};
			}
		}

		# Feeds
		print STDERR "   get feeds data\n" if ($debug);
		$data->{miogacontext}->{feeds} = {};
		my $config  = $self->GetConfig;
		my $user    = $self->GetUser;
		my $rss = Mioga2::RSS->new($config);
		if ($rss->CheckUserAccessOnMethod( $self, $user, $user, 'DisplayMain' )) {
			my $feed_path   = $config->GetBinURI . "/" . $user->GetIdent . "/RSS/Feed";
			my $dbh         = $config->GetDBH;
			$data->{miogacontext}->{feeds}->{feed}  = [{link => $feed_path, 'name' => __('Global feed')}];

			my $options = SelectSingle($dbh, "SELECT * FROM rss_option WHERE user_id = ".$user->GetRowid);
			if ($options && $options->{feed_per_group} == 1) {
				my $groups  = SelectMultiple($dbh, "SELECT DISTINCT g.rowid, g.ident FROM rss_subscription AS r, m_group_base AS g WHERE r.group_id = g.rowid AND user_id = " . $user->GetRowid);
				foreach my $group (@$groups) {
					push @{$data->{miogacontext}->{feeds}->{feed}}, {link => "$feed_path?group=$group->{rowid}", 'name' => __x('{group} group feed', group => $group->{ident})};
				}
			}
		}
	}

	# Group-specific data
	if (exists ($self->{group})) {
		print STDERR "   get group data\n" if ($debug);
		print STDERR "   group ".Dumper($self->{group})."\n" if ($debug);
		my $type  = $self->{group}->GetType;
		my $theme = $self->{group}->GetTheme;
		my $lang  = $self->{group}->GetLang;

		$data->{miogacontext}->{group}  = {
			ident => $self->{group}->GetIdent,
			type  => $type,
			lang  => $lang,
			theme => $theme,
		};

		# Ident according to type (user or group)
		if($type =~ /_user$/) {
			$data->{miogacontext}->{group}->{ident} = $self->{group}->GetName;
		}
		else {
			$data->{miogacontext}->{group}->{ident} = $self->{group}->GetIdent;
		}
	}

	# Application-specific data
	if(exists $self->{app_desc}) {
		print STDERR "   get application data\n" if ($debug);
		print STDERR "   application ".Dumper($self->{app_desc})."\n" if ($debug);
		$data->{miogacontext}->{application} = {
			ident       => $self->{app_desc}->GetIdent,
			name        => $self->{app_desc}->GetIdent,
			description => $self->{app_desc}->GetDescription,
			method      => $self->{method},
			type        => $self->{app_desc}->GetType,
		};

		if (exists $self->{user}) {
			$data->{miogacontext}->{application}->{name} = $self->{app_desc}->GetLocaleName($self->{user});
			$data->{miogacontext}->{application}->{description} = $self->{app_desc}->GetLocaleDescription($self->{user});
		}
	}

	# Web authentication
	if ($self->{authen_web}) {
		$data->{miogacontext}->{authen_web} = 1;
		$data->{miogacontext}->{uri}->{logout} = Mioga2::Login::GetLogoutURI ($self->GetConfig ()->GetMiogaConf ());
	}

	# Shared admin
	$data->{miogacontext}->{shared_admin} = $self->GetConfig ()->GetMiogaConf ()->IsSharedAdmin ();
	$data->{miogacontext}->{centralized_users} = $self->GetConfig ()->GetMiogaConf ()->IsCentralizedUsers ();

	# CSRF Token
	$data->{miogacontext}->{csrftoken} = %{$self->GetConfig()->GetDBObject()->SelectSingle('SELECT * FROM m_auth_session WHERE email=?;', [$self->GetUser()->GetEmail()])}->{csrftoken};

	print STDERR "   data = ".Dumper($data)."\n" if ($debug);
	return $data;
}

# ============================================================================

=head2 SetGroup ($group)

	set the current group.
	
=cut

# ============================================================================

sub SetGroup {
	my ($self, $group) = @_;

	$self->{group} = $group;
}



# ============================================================================

=head2 SetUser ($user)

	set the current user.
	
=cut

# ============================================================================

sub SetUser {
	my ($self, $user, $auth) = @_;

	$self->{user} = $user;

	# ------------------------------------------------------
	# Construct session_id from ident and passwd
	# ------------------------------------------------------
	$self->{session_id} = $user->GetIdent();

	#-------------------------------------------------------------------------------
	# Initialize user authentication attributes for internal requests
	#-------------------------------------------------------------------------------
	for my $attr (qw/session_token plain_password/) {
		$self->{$attr} = $auth->{$attr} if (defined ($auth->{$attr}));
	}

	# -------------------------------------------------
	# Load or create the user session.
	# -------------------------------------------------
	my %session;
	$self->{raw_session} = tie %session, 'Mioga2::Session', $self->{config}, $self->{session_id}, $self->{config}->GetTmpDir(), $self->{uri};
	$self->{session} = \%session;
}



# ============================================================================

=head2 CreateApplication ()

	Create the application from URI.
	return a Mioga2::Application object.
	
=cut

# ============================================================================

sub CreateApplication {
	my ($self) = @_;
	
	$self->{application} = $self->{app_desc}->CreateApplication();
	return $self->{application};
}



# ============================================================================

=head2 GetUploadData ()

	Return the value of uploaded data (for POST request)
	
=cut

# ============================================================================

sub GetUploadData {
	my ($self) = @_;

	if(exists $self->{upload}) {
        my $bb = $self->{upload}->bb;
        my $buffer;
        $bb->flatten($buffer);
        return $buffer;
	}
	
	return undef;
}


#===============================================================================

=head2 GetMultipleUploadData

Get the value of multiple uploaded data (for POST request)

=cut

#===============================================================================
sub GetMultipleUploadData {
	my ($self) = @_;

	my $data = undef;

	for (keys (%{$self->{multiupload}})) {
		my $bb = $self->{multiupload}->{$_}->bb;
		my $buffer;
		$bb->flatten ($buffer);
		$data->{$_} = $buffer;
	}

	return ($data);
}	# ----------  end of subroutine GetMultipleUploadData  ----------

# ============================================================================

=head2 GetUploadSize ()

  Return the size of uploaded data
  
=cut

# ============================================================================

sub GetUploadSize {
  my ($self) = @_;
  
  return $self->{upload}->size;
}

# ============================================================================

=head2 GetUploadFH ()

	Return a filehandle for uploaded datas (for POST request)
	
=cut

# ============================================================================

sub GetUploadFH {
	my ($self) = @_;
	
	if(exists $self->{upload}) {
		return $self->{upload}->fh;
	}
	else {
		print STDERR __"RequestContext::GetUploadFH upload object not initiliazed!\n";
	}
	
	return undef;
}

# ============================================================================

=head2 GetApplication ()

	Return the application object.
	
=cut

# ============================================================================

sub GetApplication {
	my ($self) = @_;
	
	return $self->{application};
}



# ============================================================================

=head2 GetMethod ()

	Return the method specified in URI or xml transaction.
	
=cut

# ============================================================================

sub GetMethod {
	my ($self) = @_;
	
	return $self->{method};
}



# ============================================================================

=head2 GetGroupIdent ()

	Return the group ident specified in URI or xml transaction.
	
=cut

# ============================================================================

sub GetGroupIdent {
	my ($self) = @_;
	
	if($self->{uri}->IsXML()) {
		return $self->{group_ident};
	}
	else {
		return $self->{uri}->GetGroupIdent();;
	}
}



# ============================================================================

=head2 GetSender ()

	Return the sender public key ident for SXML Transaction.
	
=cut

# ============================================================================

sub GetSender {
	my ($self) = @_;

	if(!$self->{uri}->IsSXML()) {
		throw Mioga2::Exception::Simple("Mioga2::RequestContext::GetSender", __"Not an SXML Transaction");
	}

	return $self->{sender};
}



# ============================================================================

=head2 GetAppDesc ()

	Return the application description (AppDesc) object.
	
=cut

# ============================================================================

sub GetAppDesc {
	my ($self) = @_;
	
	return $self->{app_desc};
}



# ============================================================================

=head2 GetURI ()

	Return the URI object.
	
=cut

# ============================================================================

sub GetURI {
	my ($self) = @_;

	return $self->{uri};
}

# ============================================================================

=head2 GetConfig ()

	Return the current Config object.
	
=cut

# ============================================================================

sub GetConfig {
	my ($self) = @_;
	
	return $self->{config};
}



# ============================================================================

=head2 GetUser ()

	Return the current user object.
	
=cut

# ============================================================================

sub GetUser {
	my ($self) = @_;
	
	if(! exists $self->{user}) {
		throw Mioga2::Exception::Simple("Mioga2::RequestContext::GetUser",
										__"Context not fully initialized.\nUse SetUser.");
	}

	return $self->{user};
}


# ============================================================================

=head2 HasUser ()

	Return true if user is defined. This is useful in public context when
	user browser sent a valid cookie.
	
=cut

# ============================================================================

sub HasUser {
	my ($self) = @_;

	return (exists($self->{user}) && defined ($self->{user}));
}



# ============================================================================

=head2 GetGroup ()

	Return the current group object.
	
=cut

# ============================================================================

sub GetGroup {
	my ($self) = @_;
	
	if(! exists $self->{group}) {
		throw Mioga2::Exception::Simple("Mioga2::RequestContext::GetGroup",
										__"Context not fully initialized or current group is a resource.\nUse SetGroup or IsResource.");
	}
	
	return $self->{group};
}


# ============================================================================

=head2 GetSession ()

	return the current session hash
	
=cut

# ============================================================================

sub GetSession {
	my ($self) = @_;

	if(! exists $self->{session}) {
		throw Mioga2::Exception::Simple("Mioga2::RequestContext::GetSession",
										__"Context not fully initialized or public application.\nUse SetUser.");
	}

	return $self->{session};
}



# ============================================================================

=head2 GetAccessType ()

	Return the current access type (AUHTZ_READ or AUTHZ_WRITE).
	
=cut

# ============================================================================

sub GetAccessType {
	my ($self) = @_;

	if ( grep { $self->{access_method} eq $_ } qw(OPTIONS GET HEAD PROPFIND)) {
		return AUTHZ_READ;
	}
	else {
		return AUTHZ_WRITE;
	}
}



# ============================================================================

=head2 GetAccessMethod ()

	Return the current access method.
	
=cut

# ============================================================================

sub GetAccessMethod {
	my ($self) = @_;

	return $self->{access_method};
}


# ============================================================================

=head2 GetUserAuth ()

	Return user authentication data, which can be: 
		{
			token => ...
		}
	or
		{
			ident => ...,
			password => ...
		}

=cut

# ============================================================================

sub GetUserAuth {
	my ($self) = @_;

	my $auth = {};

	if ($self->IsAuthenWeb ()) {
		$auth = {
			token => $self->{session_token}
		};
	}
	else {
		$auth = {
			ident => $self->{user}->GetIdent (),
			password => $self->{plain_password}
		};
	}

	return ($auth);
}



# ============================================================================

=head2 GetReferer ()

	Return the HTTP referer.
	
=cut

# ============================================================================

sub GetReferer {
	my ($self) = @_;

	return $self->{referer};
}


# ============================================================================

=head2 GetOrigin ()

	Return the HTTP Origin header.

=cut

# ============================================================================

sub GetOrigin {
	my ($self) = @_;

	return $self->{origin};
}



# ============================================================================

=head2 GetBrowser ()

	Return the User Agent (Browser) name.
	
=cut

# ============================================================================

sub GetBrowser {
	my ($self) = @_;

	return $self->{browser};
}

# ============================================================================

=head2 IsIE ()

	Return true if current browser is IE.
	
=cut

# ============================================================================

sub IsIE {
	my ($self) = @_;

	return $self->GetBrowser() =~ /MSIE/;
}

# ============================================================================

=head2 GetDefaultLang ()

	Return the default language sent by the browser.
	
=cut

# ============================================================================

sub GetDefaultLang {
	my ($self) = @_;

	return $self->{default_lang};
}



# ============================================================================

=head2 GetDefaultEncoding ()

	Return the default encoding sent by the browser.
	
=cut

# ============================================================================

sub GetDefaultEncoding {
	my ($self) = @_;

	return $self->{default_encoding};
}


#===============================================================================

=head2 IsPublic

Returns true or false whether the current application is public or not.

=cut

#===============================================================================
sub IsPublic {
	my ($self) = @_;

	return ($self->GetURI ()->IsPublic ());
}	# ----------  end of subroutine IsPublic  ----------


#===============================================================================

=head2 IsAuthenWeb

Returns whether AuthenWeb is used or not

=cut

#===============================================================================
sub IsAuthenWeb {
	my ($self) = @_;

	return ($self->{authen_web});
}	# ----------  end of subroutine IsAuthenWeb  ----------


#===============================================================================

=head2 GetProtocol

Get protocol that is currently used

=cut

#===============================================================================
sub GetProtocol {
	my ($self) = @_;

	return ($self->{protocol});
}	# ----------  end of subroutine GetProtocol  ----------


#===============================================================================

=head2 GetPort

Get port that is currently used

=cut

#===============================================================================
sub GetPort {
	my ($self) = @_;

	return ($self->{port});
}	# ----------  end of subroutine GetPort  ----------



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


# ============================================================================

=head2 Initialize ($request, $config)

	Initialize the RequestContext Object from apache request.
	Used by new.

=cut

# ============================================================================

sub Initialize {
	my ($self, $r, $config) = @_;

	$self->{uri}	= $r->pnotes('uri_obj');
	if(!defined $self->{uri}) {
		my $real_uri = $r->uri;
		$real_uri =~ s!/$!!;

		my $args = $r->args;
		if($args ne '') {
			$real_uri .= "?$args";
		}

		$self->{uri} = new Mioga2::URI($config, uri => $real_uri);
	}

	#-----------------------------------------------------------
	# For Secure XML transaction, check the message signature 
	# validity, and bind the user object to the Mioga
	# Administrator
	#-----------------------------------------------------------
	if($self->{uri}->IsSXML()) {
		$self->InitializeSecureXML($r, $config);
		return;
	}

	if($self->{uri}->IsXML()) {
		$self->InitializeXML($r, $config);
		return;
	}

	if($self->{uri}->IsApplication()) {
		$self->{method} = $self->{uri}->GetMethod();
		$self->{args}	= {};
	}
	
	$self->{access_method} = $r->method;
    $self->{xhr} = $r->headers_in->{"X-Requested-With"};

	#-------------------------------------------------------------------------------
	# Protocol and port information
	#-------------------------------------------------------------------------------
	$self->{port} = $r->connection ()->local_addr ()->port ();
	$self->{protocol} = $self->GetConfig ()->GetProtocol ();

	#-------------------------------------------------------------------------------
	# Web-based authentication
	#-------------------------------------------------------------------------------
	$self->{authen_web} = $r->pnotes->{authen_web};

	# -------------------------------------------------
	# Parse arguments
	# -------------------------------------------------
	if($self->{access_method} eq 'POST') {
		$self->{args} = $self->ParsePostParams($r);
	}

	else {
		$self->{args} = $self->ParseNonPostParams($r);
	}
	
	# -------------------------------------------------
	# referer,browser and language
	# -------------------------------------------------
	$self->ParseHeaders($r);

	# -------------------------------------------------
	# Parse URI
	# -------------------------------------------------
	$self->ParseURI();
}



# ============================================================================

=head2 ParseHeaders ($request)

	Parse the HTTP headers and extract referer, browser, lang and encoding*
	informations.

=cut

# ============================================================================


sub ParseHeaders {
	my ($self, $r) = @_;

	my $headers = $r->headers_in;
	warn __x("RequestContext::ParseHeaders headers = {headers}.", headers => Dumper($headers)) if ($debug);

	$self->{referer} = $headers->{'Referer'};
	$self->{origin}  = $headers->{'Origin'};
	$self->{browser} = $headers->{'User-Agent'};
	$self->{'csrf-token'} = $headers->{'X-CSRF-Token'};
	$self->{default_lang} = defined ($headers->{'Accept-Language'}) ? $headers->{'Accept-Language'} : $self->{config}->GetDefaultLang ();
	$self->{default_encoding} = $headers->{'Accept-Charset'};
}
# ============================================================================

=head2 InitializeSecureXML ($request, $config)

	Initialize the RequestContext Object for secure XML transaction.
	
	Decrypt and check data and parse XML command.

=cut

# ============================================================================

sub InitializeSecureXML {
	my ($self, $r, $config) = @_;

	my ($xmldata, $sender) = $self->VerifySecureTransaction($r, $config);
	$self->ParseXMLTransaction($xmldata);

	# Bind the current user and group to Admin
	my $user = new Mioga2::Old::User($config, ident => $config->GetAdminIdent());
	
	$self->SetUser($user);
	$self->SetGroup($user);

	$self->{sender} = $sender;

	$self->ParseHeaders($r);
}




# ============================================================================

=head2 InitializeXML ($request, $config)

	Initialize the RequestContext Object for XML transaction.
	
	Check data and parse XML command.

=cut

# ============================================================================

sub InitializeXML {
	my ($self, $r, $config) = @_;

	#-----------------------------------------------------------
	# Retrieve XML data
	#-----------------------------------------------------------
	my($apr) = Apache2::Request->new($r);

	my $xmldata;
    my @keys = $apr->upload();
    my $upload = $apr->upload($keys[0]);
    $upload->bb->flatten($xmldata);
	
	$self->ParseXMLTransaction($xmldata);
	$self->ParseHeaders($r);
}



# ============================================================================

=head2 ParseXMLTransaction ($xmldata)

	Parse an XML Transaction.

=cut

# ============================================================================

sub ParseXMLTransaction {
    my($self, $xmldata) = @_;

	my $xs = new Mioga2::XML::Simple(forcearray => 1);
	my $xmltree = $xs->XMLin($xmldata);

	# Initialize appdesc
	if(!exists $xmltree->{Application}) {
		throw Mioga2::Exception::Simple("Mioga2::RequestContext::ParseXMLTransaction", __"Bad Request: missing Application");
	}

	if(!exists $xmltree->{Method}) {
		throw Mioga2::Exception::Simple("Mioga2::RequestContext::ParseXMLTransaction", __"Bad Request: missing Method");
	}

	$self->{app_desc} = new Mioga2::AppDesc($self->{config}, ident => $xmltree->{Application}->[0]);
	$self->{method} = $xmltree->{Method}->[0];

	if(exists $xmltree->{Group}) {
		$self->{group_ident} = $xmltree->{Group}->[0];
	}

	# Parse Arguments
	if(! exists $xmltree->{Arg}) {
		$self->{args} = {};
		return;
	}

	foreach my $arg (@{$xmltree->{Arg}})
	{
		my $name = $arg->{name};
		my $value;
		if(exists $arg->{content}) {
			$value = $arg->{content}->[0];
		}
		else {
			my @keys = grep {ref($arg->{$_}) eq 'ARRAY'} keys(%$arg);
			
			if(@keys > 1) {
				throw Mioga2::Exception::Simple("Mioga2::RequestContext::ParseXMLTransaction", __x("Bad Request: malformed XML argument {argument_name}.", argument_name => $name));
			}
			elsif(@keys and @{$arg->{$keys[0]}} > 1) {
				throw Mioga2::Exception::Simple("Mioga2::RequestContext::ParseXMLTransaction", __x("Bad Request: malformed XML argument {argument_name}.", argument_name => $name));
			}
			elsif(@keys) {
				$value = $arg->{$keys[0]}->[0];
			}
			else {
				$value = "";
			}
		}

		if (exists($self->{args}->{$name}))
		{
			if (! ref($self->{args}->{$name})) {
				my $prec_val = $self->{args}->{$name};
				$self->{args}->{$name} = [];
				push @{$self->{args}->{$name}}, $prec_val;
			}
			push @{$self->{args}->{$name}}, $value;
		}
		else
		{
			$self->{args}->{$name} = $value;
		}
	}
}



# ============================================================================

=head2 VerifySecureTransaction ($request, $config)

	Check the signature of a secure XML transaction and decrypt data.

=cut

# ============================================================================

sub VerifySecureTransaction {
    my($self, $r, $config) = @_;

	#-----------------------------------------------------------
	# Retrieve Crypted XML data
	#-----------------------------------------------------------
	my($apr) = Apache2::Request->new($r);

	my $data;
    my @keys = $apr->upload();
    my $upload = $apr->upload($keys[0]);
    $upload->bb->flatten($data);

	my $crypto = new Mioga2::Crypto($config);
	my ($xmldata, $sender) = $crypto->VerifyDecryptData($data);
	
	return ($xmldata, $sender);
}


# ============================================================================

=head2 ParsePostParams ($request)

	Parse URI arguments for POST requests.

=cut

# ============================================================================

sub ParsePostParams {
	my ($self, $r) = @_;
	my %args;

	print STDERR "RequestContext::ParsePostParams\n" if ($debug);
	my($apr) = Apache2::Request->new($r);
	
	for my $param ($apr->param()) {

		my @params = $apr->param($param);
		for (@params) {
			$_ = st_CheckUTF8 ($_);
		}

		if(@params == 1) {
			$args{$param} = $params[0];
		}
		else {
			$args{$param} = \@params;
		}
	}
	
    my @keys = $apr->upload();
    print STDERR __x("ParsePostParams keys: {keys}\n", 'keys' => join(', ', @keys)) if ($debug);
    if (scalar(@keys) > 0) {
		if (scalar (@keys) > 1) {
			for (@keys) {
				$self->{multiupload}->{$_} = $apr->upload ($_);
				print STDERR __x("ParsePostParams size: {size}\n", size => $apr->upload ($_)->size) if ($debug);
			}
		}
		else {
	        $self->{upload} = $apr->upload($keys[0]);
			print STDERR __x("ParsePostParams size: {size}\n", size => $self->{upload}->size) if ($debug);
		}
    }
	
	return \%args;
}



# ============================================================================

=head2 ParseNonPostParams ($request)

	Parse URI arguments for Non POST requests.

=cut

# ============================================================================

sub ParseNonPostParams {
	my ($self, $r) = @_;
	my %args;

	print STDERR "RequestContext::ParseNonPostParams\n" if ($debug);
	my $args = $r->args();
	my @values = map { st_URIUnescape($_) } split(/[\=\&]/, $args);
	

	for (my $i = 0; $i < @values; $i += 2)
	{
		my $key = $values[$i];
		my $val = st_CheckUTF8 ($values[$i + 1]);

		if (exists($args{$key}))
		{
			if (! ref($args{$key}))
			{
				my $prec_val = $args{$key};
				$args{$key} = [];
				push @{$args{$key}}, $prec_val;
			}
			push @{$args{$key}}, $val;
		}
		else
		{
			$args{$key} = $val;
		}
	}
	
	return \%args;
}


# ============================================================================

=head2 ParseURI ($request)

	Parse URI. 

=cut

# ============================================================================

sub ParseURI {
	my ($self) = @_;

	$self->{private} = $self->{uri}->IsPrivate();

	if($self->{uri}->IsApplication()) {
		$self->{app_desc} = new Mioga2::AppDesc($self->{config}, ident => $self->{uri}->GetAppIdent());
	}

}

# ============================================================================

=head2 IsXHR ()

=over

=item Is the request an AJAX one (prototype library) ?

=item B<return :> true if it is, undef if not.

=back

=cut

# ============================================================================

sub IsXHR {
    my ($self) = @_;
    
    return $self->{xhr} =~ /XMLHttpRequest/i;
}


#===============================================================================

=head2 SetAPIVersion

Adapt context to Mioga2 API version.

=cut

#===============================================================================
sub SetAPIVersion {
	my ($self) = @_;

	my $app_desc = $self->{application}->GetAppDesc ();

	my $version = $app_desc->{api_version};
	print STDERR "[Mioga2::RequestContext::SetAPIVersion] API version for application is " . (($version eq '') ? 'undefined' : $version) . "\n" if ($debug);

	if ($version) {
		if ($version >= 2.3) {
			if ($self->{user}) {
				$self->{user} = Mioga2::UserList->new ($self->GetConfig (), { attributes => { ident => $self->{user}->GetIdent () } });
			}
			my $type = SelectSingle ($self->GetConfig ()->GetDBH (), "SELECT m_group_type.ident FROM m_group_type, m_group_base WHERE m_group_type.rowid = m_group_base.type_id AND m_group_base.ident = '" . $self->{group}->GetIdent () . "' AND m_group_base.mioga_id = " . $self->GetConfig ()->GetMiogaId () . ";")->{ident};
			if ($type =~ /user/) {
				$self->{group} = Mioga2::UserList->new ($self->GetConfig (), { attributes => { ident => $self->{group}->GetIdent () } });
			}
			elsif ($type =~ /resource/) {
				$self->{group} = Mioga2::ResourceList->new ($self->GetConfig (), { attributes => { ident => $self->{group}->GetIdent () } });
			}
			else {
				$self->{group} = Mioga2::GroupList->new ($self->GetConfig (), { attributes => { ident => $self->{group}->GetIdent () } });
			}
		}
	}
}	# ----------  end of subroutine SetAPIVersion  ----------


#===============================================================================

=head2 IsCSRFSafe

Return true if request is safe regarding CSRF protection.

For a request to be safe it has to be issued to a non-sensitive method or meet
the following criteria (all):

=over

=item be issued from a page on local server

=item be something else than GET

=item provide a valid CSRF token in one of the two forms:

=over

=item "X-CSRF-Token" request argument

=item "X-CSRF-Token" request header

=back

=back

=head3 Incoming Arguments

=over

None

=back

=head3 Return value

=over

True or false

=over

=back

=back

=cut

#===============================================================================
sub IsCSRFSafe {
	my ($self) = @_;

	my $safe = 0;

	if ($self->GetAppDesc()->IsSensitiveMethod($self->GetMethod())) {
		# Request to a sensitive method, deep-check
		if (($self->GetReferer() ne '') || ($self->GetOrigin() ne '')) {
			# HTTP Referer or Origin header present, request should come from Mioga2 server
			my ($origin) = ((($self->GetReferer() ne '') ? $self->GetReferer() : $self->GetOrigin()) =~ m~^(http[s]?://[^/]*/).*$~);

			# Allowed origins
			my @origins = (
					$self->GetProtocol() . '://' . $self->GetConfig()->GetServerName() . '/'
				);

			if (grep(/^$origin$/, @origins)) {
				# Origin is allowed, check for HTTP verb
				if ($self->GetAccessMethod() ne 'GET') {
					# HTTP verb is correct, check for token
					my ($token) = ($self->{'csrf-token'} ? $self->{'csrf-token'} : $self->{args}->{'X-CSRF-Token'});

					if ($self->GetConfig()->GetDBObject()->SelectSingle('SELECT * FROM m_auth_session WHERE email=? AND csrftoken=?;', [$self->GetUser()->GetEmail(), $token])) {
						# Token matches value in DB, request can be served
						$safe = 1;

						# Delete argument to avoid having it recorded as not-checked (see Mioga2::Dispatch)
						delete($self->{args}->{'X-CSRF-Token'});
					}
				}
			}
		}
	}
	else {
		# Request to a non-sensitive method, safe
		$safe = 1;
	}

	print STDERR "[Mioga2::RequestContext::IsCSRFSafe] Method: " . $self->GetAccessMethod () . " " . $self->GetAppDesc()->GetPackage() . "::" . $self->GetMethod() . ", safe: $safe" . "\n" if $debug;

	return $safe;
}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga2 Mioga2::Config Mioga2::URI Mioga2::Application Mioga2::AppDesc
Mioga2::Old::User Mioga2::Old::Group Mioga2::Old::Resource Mioga2::UserList
Mioga2::GroupList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
