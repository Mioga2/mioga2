#===============================================================================
#
#         FILE:  TaskCategory.pm
#
#  DESCRIPTION:  Mioga2 task category management object.
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  14/10/2010 12:00
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

TaskCategory.pm: Mioga2 task category management object.

=head1 DESCRIPTION

This object handles Mioga2 task categories.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::TaskCategory;

use Mioga2::tools::database;

use Data::Dumper;

my $debug = 0;


#===============================================================================

=head2 new

Create new instance of object.

=cut

#===============================================================================
sub new {
	my ($class, $config) = @_;
	print STDERR "[Mioga2::TaskCategory::new] Entering.\n" if ($debug);

	my $self = { };
	bless($self, $class);

	$self->{config} = $config;
	$self->{dbh} = $config->GetDBH ();

	print STDERR "[Mioga2::TaskCategory::new] Leaving\n" if ($debug);
	return ($self);
}	# ----------  end of subroutine new  ----------


#===============================================================================

=head2 Create

Create a new task category

=cut

#===============================================================================
sub Create {
	my ($self, %attributes) = @_;
	print STDERR "[Mioga2::TaskCategory::Create] Entering. Attributes: " . Dumper \%attributes if ($debug);

	my @keys = ();
	my @vals = ();
	for (keys (%attributes)) {
		push (@keys, $_);
		push (@vals, $self->{dbh}->quote ($attributes{$_}));
	}
	my $sql = "INSERT INTO task_category (created, modified, mioga_id, " . join (', ', @keys) . ") VALUES (now (), now (), " . $self->{config}->GetMiogaId () . ", " . join (', ', @vals) . ");";
	ExecSQL ($self->{dbh}, $sql);

	# Return rowid
	my $rowid = GetLastInsertId ($self->{dbh}, 'task_category');

	print STDERR "[Mioga2::TaskCategory::Create] Leaving. TaskCategory rowid: $rowid\n" if ($debug);
	return ($rowid);
}	# ----------  end of subroutine Create  ----------


#===============================================================================

=head2 Update

Update a new task category

=cut

#===============================================================================
sub Update {
	my ($self, %attributes) = @_;
	print STDERR "[Mioga2::TaskCategory::Update] Entering. Attributes: " . Dumper \%attributes if ($debug);

	my $rowid = delete ($attributes{rowid});
	my @vals = ();
	for (keys (%attributes)) {
		push (@vals, "$_ = " . $self->{dbh}->quote ($attributes{$_}));
	}
	push (@vals, 'modified = now ()');
	my $sql = "UPDATE task_category SET " . join (', ', @vals) . " WHERE rowid = $rowid;";
	ExecSQL ($self->{dbh}, $sql);

	print STDERR "[Mioga2::TaskCategory::Update] Leaving. TaskCategory rowid: $rowid\n" if ($debug);
	return ($rowid);
}	# ----------  end of subroutine Update  ----------


#===============================================================================

=head2 Delete

Delete a task category.

=cut

#===============================================================================
sub Delete {
	my ($self, $rowid) = @_;

	#-------------------------------------------------------------------------------
	# Sanity checks
	#-------------------------------------------------------------------------------
	my $task_category = $self->GetTaskCategory ($rowid);

	# Check if task category actually exists
	if (!$task_category) {
		throw Mioga2::Exception::Simple ('Mioga2::TaskCategory::Delete', 'Task category does not exist.');
	}

	# Check if task category is in use
	if ($task_category->{nb_tasks}) {
		throw Mioga2::Exception::Simple ('Mioga2::TaskCategory::Delete', 'Task category is in use.');
	}

	#-------------------------------------------------------------------------------
	# Delete task category from DB
	#-------------------------------------------------------------------------------
	ExecSQL ($self->{dbh}, "DELETE FROM task_category WHERE rowid = $rowid;");
}	# ----------  end of subroutine Delete  ----------


#===============================================================================

=head2 GetList

Get task categorys for current instance

=cut

#===============================================================================
sub GetList {
	my ($self) = @_;

	my $list = SelectMultiple ($self->{dbh}, "SELECT task_category.*, (SELECT count (*) FROM (SELECT rowid FROM org_task WHERE category_id = task_category.rowid UNION SELECT rowid FROM cal_event WHERE category_id = task_category.rowid) AS T1) AS nb_tasks FROM task_category WHERE mioga_id = " . $self->{config}->GetMiogaId () . ' ORDER BY task_category.name;');

	return ($list);
}	# ----------  end of subroutine GetList  ----------


#===============================================================================

=head2 GetTaskCategory

Get task category details from rowid.

=cut

#===============================================================================
sub GetTaskCategory {
	my ($self, $rowid) = @_;

	my $task_category = SelectSingle ($self->{dbh}, "SELECT task_category.*, (SELECT count (*) FROM org_task WHERE category_id = task_category.rowid) AS nb_tasks FROM task_category WHERE rowid = $rowid;");

	return ($task_category);
}	# ----------  end of subroutine GetTaskCategory  ----------

1;

