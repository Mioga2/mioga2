# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Synchro.pm: Mioga Synchronization (mioga_rowid => remote_rowid) maniupulation functions.

=head1 METHODS DESRIPTION

=cut

package Mioga2::Synchro;
use strict;

use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Data::Dumper;
use Error qw(:try);


sub new {
	my ($class) = @_;

	return bless ({}, $class);
}


# ============================================================================

=head2 GetLastSynchroDate ($config, $synchro_ident)

	Return last synchro date in second since epoch for $synchro_ident.

=cut

# ============================================================================

sub GetLastSynchroDate
{
	my ($self, $config, $synchro_ident) = @_;

	my $dbh = $config->GetDBH();

	my $date = SelectSingle($config->GetDBH(), 
							"SELECT * FROM synchro_agents ".
							"WHERE mioga_id = ".$config->GetMiogaId()." AND ".
							"      ident = '".st_FormatPostgreSQLString($synchro_ident)."'");
	
	if(defined $date and $date->{last_synchro_date} eq '') {
		return "1970-01-01 00:00:00";
	}

	if(!defined $date) {
		ExecSQL($dbh, "INSERT INTO synchro_agents (mioga_id, ident) ".
				      "VALUES (".$config->GetMiogaId().", '".st_FormatPostgreSQLString($synchro_ident)."')");

		return "1970-01-01 00:00:00";
	}

	return $date->{last_synchro_date};
}

# ============================================================================

=head2 SetLastSynchroDate ($config, $synchro_ident)

	Update last synchro date for $synchro_ident.

=cut

# ============================================================================

sub SetLastSynchroDate
{
	my ($self, $config, $synchro_ident) = @_;

	ExecSQL($config->GetDBH(), 
			"UPDATE synchro_agents ".
			"SET last_synchro_date = now() " .
			"WHERE mioga_id = ".$config->GetMiogaId()." AND ".
			"      ident = '".st_FormatPostgreSQLString($synchro_ident)."'");
}

# ============================================================================

=head2 GetRemoteRowids ($config, $synchro_ident, $app_ident, $rowids)

	Return remote rowids corresponding to ($synchro_ident, $app_ident) for given rowids.

=cut

# ============================================================================

sub GetRemoteRowids 
{
	my ($self, $config, $synchro_ident, $app_ident, $rowids) = @_;

	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();

	my $res = SelectSingle($dbh, "SELECT count(*) FROM synchro_agents ".
						         "WHERE ident = '$synchro_ident' AND mioga_id = $mioga_id");
	if($res->{count} == 0) {
		ExecSQL($dbh, "INSERT INTO synchro_agents (ident, mioga_id) VALUES ('$synchro_ident', $mioga_id)");
	}


	my $rowidlist;
	if(@$rowids) {
		$rowidlist = SelectMultiple($dbh, "SELECT * FROM synchro_rowids, synchro_agents, m_application, m_instance_application ".
			 					          "WHERE synchro_rowids.mioga_rowid IN (".join(',', @$rowids).") AND ".
		 						          "      synchro_rowids.synchro_id = synchro_agents.rowid AND ".
		 						          "      synchro_agents.ident = '$synchro_ident' AND ".
		 						          "      synchro_rowids.app_id = m_application.rowid AND ".
								          "      m_application.ident = '$app_ident' AND ".
										  "      m_application.rowid = m_instance_application.application_id AND ".
								          "      m_instance_application.mioga_id = $mioga_id");
	}
	else {
		$rowidlist = [];
	}
	
	
	my %res = map { ($_->{mioga_rowid}, $_->{remote_rowid}) } @$rowidlist;
	
	return \%res;
}


# ============================================================================

=head2 AddRemoteRowids ($config, $synchro_ident, $app_ident, $rowids)

	Add remote rowids corresponding to ($synchro_ident, $app_ident) for given rowids.

=cut

# ============================================================================

sub AddRemoteRowids 
{
	my ($self, $config, $synchro_ident, $app_ident, $remoteids) = @_;

	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();

	my $res = SelectSingle($dbh, "SELECT count(*) FROM synchro_agents ".
						         "WHERE ident = '$synchro_ident' AND mioga_id = $mioga_id");
	if($res->{count} == 0) {
		ExecSQL($dbh, "INSERT INTO synchro_agents (ident, mioga_id) VALUES ('$synchro_ident', $mioga_id)");
	}

	$res = SelectSingle($dbh, "SELECT * FROM synchro_agents ".
						      "WHERE ident = '$synchro_ident' AND mioga_id = $mioga_id");

	my $app = SelectSingle($dbh, "SELECT * FROM m_application, m_instance_application ".
						         "WHERE m_application.ident = '$app_ident' AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id");

	foreach my $mioga_rowid (keys %$remoteids) {
		ExecSQL($dbh, "INSERT INTO synchro_rowids (synchro_id, app_id, mioga_rowid, remote_rowid) ".
				      "VALUES ($res->{rowid}, $app->{rowid}, $mioga_rowid, $remoteids->{$mioga_rowid})");
	}

}


# ============================================================================

=head2 DeleteRowids ($config, $rowids)

	Delete remote rowids for given local rowids.

=cut

# ============================================================================

sub DeleteRowids 
{
	my ($self, $config, $app_ident, $rowids) = @_;

	if(!@$rowids) {
		return;
	}

	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();

	my $app = SelectSingle($dbh, "SELECT * FROM m_application, m_instance_application ".
						         "WHERE m_application.ident = '$app_ident' AND m_application.rowid = m_instance_application.application_id AND m_instance_application.mioga_id = $mioga_id");
	
	ExecSQL($dbh, "DELETE FROM synchro_rowids WHERE app_id = $app->{rowid} AND mioga_rowid IN (".join(',', @$rowids).")");

}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__





