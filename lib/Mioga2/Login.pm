#===============================================================================
#
#         FILE:  Login.pm
#
#  DESCRIPTION:  Mioga2 HTML login handler.
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  29/11/2010 16:07
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Login.pm: Mioga2 HTML login handler.

=head1 DESCRIPTION

This module handles form-based authentication to Mioga2.

=head1 METHODS DESRIPTION

=cut

use strict;
use warnings;
use open ':encoding(utf8)';

package Mioga2::Login;

use Locale::TextDomain::UTF8 'login';

use Mioga2::Login::Doris;
use Mioga2::Login::Specific;
use Mioga2::Login::PageMacros;
use Mioga2::Login::Contact;
use Mioga2::tools::args_checker;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::URI;
use Mioga2::RequestContext;
use Mioga2::MiogaConf;
use Mioga2::InstanceList;
use Mioga2::tools::Convert;

use Data::Dumper;
use Error qw(:try);
use POSIX qw(:locale_h);
use Digest::MD5 qw(md5_hex);
use CGI::Cookie;

my $debug = 0;

my $default_route = 'DisplayMain';
$Mioga2::Login::routes = {
	DisplayMain => {
		GET  => 'DisplayMain',
		POST => 'Login',
	},
	LoginWS => {
		POST => 'LoginWS'
	},
	Logout => {
		GET  => 'Logout',
	},
	PasswordRecovery => {
		GET  => 'PasswordRecovery',
		POST => 'GenerateRecoveryLink',
	},
	ReinitPassword => {
		GET  => 'ReinitForm',
		POST => 'ReinitPassword',
	},
	Contact => {
		GET  => 'ContactForm',
		POST => 'ProcessContact',
	},
	Captcha => {
		GET  => 'GetCaptcha',
	},
	Error   => {
		GET  => 'Error',
	},
	SelectInstance => {
		GET  => 'SelectInstance',
	},
};
$Mioga2::Login::login_data_path = '';


sub handler : method {
    my($class, $r) = @_;

	print STDERR "[Mioga2::Login] Entering login handler\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Load MiogaConf
	#-------------------------------------------------------------------------------
	my $config_file = $r->dir_config("MiogaConfig");
	my $miogaconf = new Mioga2::MiogaConf($config_file);


	#-------------------------------------------------------------------------------
	# Initialize path to login files
	#-------------------------------------------------------------------------------
	$Mioga2::Login::login_data_path = $miogaconf->GetInstallDir () . $miogaconf->GetLoginURI ();

	#-------------------------------------------------------------------------------
	# Extract method from URL
	#-------------------------------------------------------------------------------
	my $context = Mioga2::RequestContext->new ($r, $miogaconf);

	#-------------------------------------------------------------------------------
	# Get cookie if exists
	#-------------------------------------------------------------------------------
	my %cookies = CGI::Cookie->fetch($r);
	my $cookie = $cookies{mioga_session};
	print STDERR " cookie = $cookie\n" if ($debug);
	$context->{session_token} = $cookie->value () if ($cookie);


	#-------------------------------------------------------------------------------
	# Set language
	#-------------------------------------------------------------------------------
	my ($lang, $country) = ($context->GetDefaultLang () =~ m/([a-zA-Z]{2})[-_]([a-zA-Z]{2})/);
	if (!$country && !$lang && $context->GetDefaultLang () =~ /^([a-zA-Z]{2})$/) {
		$lang = $context->GetDefaultLang ();
		$country = $lang;
	}
	my $locale = (defined ($lang)) ? $lang . "_" . uc ($country) : $context->GetConfig ()->GetDefaultLang ();
	setlocale(LC_ALL, "$locale.UTF-8");
	print STDERR "[Mioga2::Login::handler] Language string from browser: " . $context->GetDefaultLang () . "\n" if ($debug);
	print STDERR "[Mioga2::Login::handler] Internal locale: $locale\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Initialize login module
	#-------------------------------------------------------------------------------
	Initialize ($miogaconf);

	#-------------------------------------------------------------------------------
	# Get handler for method
	#-------------------------------------------------------------------------------
	my $method = $context->GetURI ()->GetMethod ();
	$method ||= $default_route;
	my $handler = $Mioga2::Login::routes->{$method}->{$r->method ()};
	if (!$handler) {
		throw Mioga2::Exception::Simple ("Mioga2::Login::handler", "[Mioga2::Login::handler] No route matches method " . $r->method () . " $method");
	}
	print STDERR "[Mioga2::Login::handler] Running method $handler\n" if ($debug);
	my $content = &{\&{$handler}} ($context);

	#-------------------------------------------------------------------------------
	# Pass WebServices data through formatter
	#-------------------------------------------------------------------------------
	if ($context->GetURI ()->IsWS ()) {
		$content = FormatWSData ($context, $content);
	}

	#-------------------------------------------------------------------------------
	# Format output
	#-------------------------------------------------------------------------------
	my $data = $content->Generate ($r);
	my $encoding  = $content->GetEncoding();
	my $mime_type = $content->GetMIME();
	my $can_cache = $content->CanCache();
	if ($encoding) {
		$r->content_encoding($encoding);
		$mime_type .= "; charset=$encoding";
	}
	$r->no_cache(! $can_cache);
	$r->content_type($mime_type);
	if ($data) {
		$r->print ($data);
	}

	$r->pnotes('context', 0);

	return $content->GetReturnCode();
}


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================


#===============================================================================

=head2 DisplayMain

Display login form

=cut

#===============================================================================
sub DisplayMain {
	my ($context) = @_;
	print STDERR "[Mioga2::Login::DisplayMain] Entering\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'target' ], 'allow_empty', ['location', $context->GetConfig()], 'noscript' ],
			[ [ 'failed' ], 'allow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "[Mioga2::Login::DisplayMain] Args: " . Dumper $values if ($debug);

	if (@$errors) {
		throw Mioga2::Exception::Simple ('Mioga2::Login::DisplayMain', '[Mioga2::Login::DisplayMain] Incorrect argument value: ' . Dumper $errors);
	}

	#-------------------------------------------------------------------------------
	# Get lang from user browser
	#-------------------------------------------------------------------------------
	my $lang = GetLang ($context);

	#-------------------------------------------------------------------------------
	# Parse page and translate macros
	#-------------------------------------------------------------------------------
	print STDERR "[Mioga2::Login::DisplayMain] page to parse: $Mioga2::Login::login_data_path/login-$lang.html\n" if ($debug);
	my $page;
	if (-e "$Mioga2::Login::login_data_path/login-$lang.html") {
		open (FH, '<', "$Mioga2::Login::login_data_path/login-$lang.html") or die "File $Mioga2::Login::login_data_path/login-$lang.html not found";
		$page = join ('', <FH>);
		ProcessPageMacros ($context, \$page, { failed => $values->{failed} });
		close (FH);
	}
	else {
		$page = '
			<html>
				<head>
					<title>Error - Mioga2 Authentication</title>
				</head>
				<body>
					<p>The login system pages could not be found. Your Mioga2 installation seems to be incomplete. Please contact an administrator.</p>
				</body>
			</html>';
	}

	#-------------------------------------------------------------------------------
	# Initialize content
	#-------------------------------------------------------------------------------
	my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/html');
	$content->SetContent ($page);

	return ($content);
}	# ----------  end of subroutine DisplayMain  ----------


#===============================================================================

=head2 AbstractLogin

Check user login.

=cut

#===============================================================================
sub AbstractLogin {
	my ($context) = @_;
	print STDERR "[Mioga2::Login::Login] Entering\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'login' ], 'disallow_empty', 'want_email' ],
			[ [ 'password' ], 'disallow_empty' ],
			[ [ 'target' ], 'allow_empty' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my ($target, $instance);
	if ($values->{target}) {
		#-------------------------------------------------------------------------------
		# Instance-dependent login
		#-------------------------------------------------------------------------------


		#-------------------------------------------------------------------------------
		# Set target URI
		#-------------------------------------------------------------------------------
		$target = new Mioga2::URI ($context->GetConfig (), uri => $values->{target}, missing_domain => 1);
	}
	else {
		#-------------------------------------------------------------------------------
		# Instance-independent login
		#-------------------------------------------------------------------------------

		my $db = $context->GetConfig ()->GetDBObject ();

		# Get first instance the user is in
		my $res = $db->SelectSingle ('SELECT ident FROM m_mioga WHERE rowid = (SELECT mioga_id AS instance_id FROM m_user_base WHERE email = ? LIMIT 1)', [ $values->{login} ]);
		if (!$res) {
			# User is unknown, get first existing instance so that further processing doesn't fail
			# This will obviously lead the authentication request to be rejected as email is unknown in randomly-picked instance.
			$res = $db->SelectSingle ('SELECT ident FROM m_mioga LIMIT 1');
		}
		$instance = $res->{ident};
	}


	#-------------------------------------------------------------------------------
	# Check user login / password and generate a session ID
	#-------------------------------------------------------------------------------
	my ($session_token, $csrf_token);
	my ($session_cookie, $csrf_cookie);
	my $user;
	my $config;
	if (!@$errors) {
		# Check user login / password and generate cookie
		$config = Mioga2::Config->new ($context->GetConfig (), $target ? $target->GetMiogaIdent () : $instance);
		$user = Mioga2::UserList->new ($config, { attributes => { login => $values->{login} } });
		if (($user->Count () == 1) && ($user->CheckPassword ($values->{password}))) {
			print STDERR "[Mioga2::Login::Login] User " . $values->{login} . " login OK\n" if ($debug);
			($session_token, $csrf_token) = GetToken ($context->GetConfig (), $user);
			$session_cookie = GetCookie ($context->GetConfig (), $user, $session_token);
			$csrf_cookie = GetCSRFCookie ($context->GetConfig (), $csrf_token);
			$context->{config} = $config;
			$context->SetUser ($user);
		}
		else {
			print STDERR "[Mioga2::Login::Login] User " . $values->{login} . " login NOT OK\n" if ($debug);
		}
	}
	else {
		print STDERR "[Mioga2::Login::Login] Errors: " . Dumper $errors if ($debug);
	}

	return ($errors, $session_cookie, $csrf_cookie, $target, $user);
}

#===============================================================================

=head2 Login

Check user login.

=cut

#===============================================================================
sub Login {
	my ($context) = @_;
	my ($errors, $session_cookie, $csrf_cookie, $target, $user) = AbstractLogin($context);
	my $content;

	#-------------------------------------------------------------------------------
	# Redirect user, generate cookie and record session ID to DB
	#-------------------------------------------------------------------------------
	if (!@$errors && $session_cookie) {
		print STDERR "[Mioga2::Login::Login] Redirecting user\n" if ($debug);
		# Redirect user to correct page
		my $redirect_uri;
		if ($target) {
			$redirect_uri = $context->GetConfig ()->GetProtocol () . "://" . $context->GetConfig ()->GetServerName () . $target->GetURI ();
		}
		else {
			$redirect_uri = 'SelectInstance';
		}

		$content = new Mioga2::Content::REDIRECT ($context, mode => 'external');
		$content->SetContent($redirect_uri);
		push (@{$content->{cookies}}, $session_cookie);
		push (@{$content->{cookies}}, $csrf_cookie);
	}
	else {
		# If errors, redirect to login main page
		$content = new Mioga2::Content::REDIRECT ($context, mode => 'external');
		my $args = '?failed=1';
		if ($target) {
			$args .= '&target=' . $target->GetURI ();
		}
		$content->SetContent($args);
	}

	print STDERR "[Mioga2::Login::Login] Leaving\n" if ($debug);

	return ($content);
}	# ----------  end of subroutine Login  ----------

#===============================================================================

=head2 LoginWS

Check user login.

=cut

#===============================================================================
sub LoginWS {
	my ($context) = @_;
	my ($errors, $cookie, $target, $user) = AbstractLogin($context);

	my $content = Mioga2::Content::ASIS->new($context, MIME => "application/json");

	#-------------------------------------------------------------------------------
	# Redirect user, generate cookie and record session ID to DB
	#-------------------------------------------------------------------------------
	if (!@$errors && $cookie) {
		$content->SetContent(Mioga2::tools::Convert::PerlToJSON (
			{
				firstname => $user->Get('firstname'),
				lastname => $user->Get('lastname'),
				ident => $user->Get('ident'),
				email => $user->Get('email')
			}
		));
		push (@{$content->{cookies}}, $cookie);
	}
	else {
		$content->SetContent(Mioga2::tools::Convert::PerlToJSON({ failed => 1 }));
	}

	print STDERR "[Mioga2::Login::LoginWS] Leaving\n" if ($debug);

	return($content);
}
#===============================================================================

=head2 IsValidToken

B<Warning:> $config is a Mioga2::Config to target instance.

=cut

#===============================================================================
sub IsValidToken {
	my ($config, $token) = @_;
	print STDERR "[Mioga2::Login::IsValidToken] Entering token = $token\n" if ($debug);
	if (!defined($token)) {
		print STDERR "[Mioga2::Login::IsValidToken] token not defined\n" if ($debug);
		return 0;
	}

	my $res = SelectSingle ($config->GetDBH (), "SELECT * FROM m_auth_session WHERE token = " . $config->GetDBH ()->quote ($token) . " AND modified > to_timestamp (" . (time - $config->GetMiogaConf ()->GetAuthenTimeout ()) . ")");
	if ($res) {
	print STDERR "[Mioga2::Login::IsValidToken] token OK \n" if ($debug);
		return 1;
	}
	print STDERR "[Mioga2::Login::IsValidToken] token KO \n" if ($debug);
	return 0;
}	# ----------  end of subroutine IsValidToken  ----------


#===============================================================================

=head2 GetUser

Get Mioga2::UserList object from given token

B<Warning:> $config is a Mioga2::Config to target instance.

=cut

#===============================================================================
sub GetUser {
	my ($config, $target, $token) = @_;

	my $user;
	my $res = SelectSingle ($config->GetDBH (), "SELECT * FROM m_auth_session WHERE token = " . $config->GetDBH ()->quote ($token) . " AND modified > to_timestamp (" . (time - $config->GetMiogaConf ()->GetAuthenTimeout ()) . ")");
	if ($res) {
		try {
			$user = new Mioga2::Old::User ($config, login => $res->{email});
		}
		catch Mioga2::Exception::User with {
			# Nothing useful to be done here, just to prevent server crash
		};
	}

	return $user;
}	# ----------  end of subroutine GetUser  ----------




#===============================================================================

=head2 GetToken

Create Token for user

=cut

#===============================================================================
sub GetToken {
	my ($miogaconf, $user) = @_;
	print STDERR "[Mioga2::Login::GetToken] Entering\n" if ($debug);

	my $dbh = $miogaconf->GetDBH ();

	my ($session_token, $csrf_token);
	my $timeout = $miogaconf->GetAuthenTimeout ();
	# suppress old tokens
	print STDERR "[Mioga2::Login::GetToken] Deleting old cookies\n" if ($debug);
	my $sql = "DELETE FROM m_auth_session WHERE modified < to_timestamp (" . (time - $timeout) . ");";
	print STDERR "[Mioga2::Login::GetToken] sql =  $sql\n" if ($debug);
	ExecSQL ($dbh, $sql);
	$session_token = md5_hex (time . " " . $user->GetFirstname () . " " . $user->GetLastname () . " " . $user->GetPassword ());
	$csrf_token    = md5_hex (time . " " . $session_token);
	print STDERR "[Mioga2::Login::GetToken] Generate token = $session_token\n" if ($debug);

	# Record token and user params into DB
	#ExecSQL ($dbh, "DELETE FROM m_auth_session WHERE email = " . $dbh->quote ($user->GetEmail ()));
	ExecSQL ($dbh, "INSERT INTO m_auth_session (token, email, csrftoken, created, modified) VALUES (?, ?, ?, now (), now ());", [$session_token, $user->GetEmail (), $csrf_token]);
	print STDERR "[Mioga2::Login::GetToken] token saved\n" if ($debug);

	return ($session_token, $csrf_token);
}	# ----------  end of subroutine GetToken  ----------

#===============================================================================

=head2 UpdateToken

Set modified date for token to current time

=cut

#===============================================================================
sub UpdateToken {
	my ($miogaconf, $token) = @_;
	print STDERR "[Mioga2::Login::UpdateToken] Entering\n" if ($debug);

	my $dbh = $miogaconf->GetDBH ();

	my $sql = "UPDATE m_auth_session SET modified=now() WHERE token = ?";
	print STDERR "[Mioga2::Login::UpdateToken] sql =  $sql\n" if ($debug);
	ExecSQL ($dbh, $sql, [$token]);

}	# ----------  end of subroutine UpdateToken  ----------


#===============================================================================

=head2 GetCookie

Get cookie for user

=cut

#===============================================================================
sub GetCookie {
	my ($miogaconf, $user, $token) = @_;
	print STDERR "[Mioga2::Login::GetCookie] Entering\n" if ($debug);

	my $authen_timeout = $miogaconf->GetAuthenTimeout () / 60;
	my $cookie = CGI::Cookie->new (-name => 'mioga_session', -value => $token, -expires => '+' . $authen_timeout . 'm');

	return ($cookie);
}	# ----------  end of subroutine GetCookie  ----------


#===============================================================================

=head2 GetCSRFCookie

Get CSRF protection cookie

=cut

#===============================================================================
sub GetCSRFCookie {
	my ($miogaconf, $token) = @_;
	print STDERR "[Mioga2::Login::GetCSRFCookie] Entering\n" if ($debug);

	my $authen_timeout = $miogaconf->GetAuthenTimeout () / 60;
	my $cookie = CGI::Cookie->new (-name => 'X-CSRF-Token', -value => $token, -expires => '+' . $authen_timeout . 'm');

	return ($cookie);
}	# ----------  end of subroutine GetCSRFCookie  ----------


#===============================================================================

=head2 Logout

Destroy user login information from session

=cut

#===============================================================================
sub Logout {
	my ($context) = @_;
	print STDERR "[Mioga2::Login::Logout] Entering\n" if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'target' ], 'allow_empty' ],
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);
	print STDERR "[Mioga2::Login::Logout] Data: " . Dumper $values if ($debug);

	#-------------------------------------------------------------------------------
	# Automatically get target from referer
	#-------------------------------------------------------------------------------
	if (!$values->{target}) {
		$values->{target} = $context->GetReferer ();
		my $toremove = $context->GetConfig ()->GetProtocol () . "://" . $context->GetConfig ()->GetServerName ();
		$values->{target} =~ s/^$toremove//;
	}

	#-------------------------------------------------------------------------------
	# Set target URI (currently mandatory, will be optional once user are
	# decorrelated from instances)
	#-------------------------------------------------------------------------------
	my $target = new Mioga2::URI ($context->GetConfig (), uri => $values->{target}, missing_domain => 1);

	#-------------------------------------------------------------------------------
	# Remove session from DB
	#-------------------------------------------------------------------------------
	my $dbh = $context->GetConfig ()->GetDBH ();
	ExecSQL ($dbh, "DELETE FROM m_auth_session WHERE token = ?;", [$context->GetUserAuth ()->{token}]);

	#-------------------------------------------------------------------------------
	# Redirect to login main page
	#-------------------------------------------------------------------------------
	my $redirect_uri = Mioga2::Login::GetLoginURI ($context->GetConfig (), $target);
	my $content = new Mioga2::Content::REDIRECT ($context, mode => 'external');
	$content->SetContent($redirect_uri);

	print STDERR "[Mioga2::Login::Logout] Leaving\n" if ($debug);
	return ($content);
}	# ----------  end of subroutine Logout  ----------


#===============================================================================

=head2 Error

Display an error page

=cut

#===============================================================================
sub Error {
	my ($context) = @_;

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'code' ], 'disallow_empty', 'want_int' ],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	if (@$errors) {
		$values->{code} = 500;
	}

	#-------------------------------------------------------------------------------
	# Get lang from user browser
	#-------------------------------------------------------------------------------
	my $lang = GetLang ($context);

	#-------------------------------------------------------------------------------
	# Parse page and translate macros
	#-------------------------------------------------------------------------------
	print STDERR "[Mioga2::Login::Error] page to parse: $Mioga2::Login::login_data_path/error-$values->{code}-$lang.html\n" if ($debug);
	open (FH, '<', "$Mioga2::Login::login_data_path/error-$values->{code}-$lang.html") or die "File $Mioga2::Login::login_data_path/error-$values->{code}-$lang.html not found";
	my $page = join ('', <FH>);
	ProcessPageMacros ($context, \$page);
	close (FH);

	#-------------------------------------------------------------------------------
	# Initialize content
	#-------------------------------------------------------------------------------
	my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/html');
	$content->SetContent ($page);

	return ($content);
}	# ----------  end of subroutine Error  ----------


#===============================================================================

=head2 CleanupExpired

Remove expired tokens from DB

=cut

#===============================================================================
sub CleanupExpired {
	my ($miogaconf) = @_;

	my $dbh = $miogaconf->GetDBH ();
	my $timeout = $miogaconf->GetAuthenTimeout ();
	ExecSQL ($dbh, "DELETE FROM m_auth_session WHERE modified < to_timestamp (" . (time - $timeout) . ")");
	ExecSQL ($dbh, "DELETE FROM m_passwd_reinit WHERE expiration_date < to_timestamp (" . time . ")");
}	# ----------  end of subroutine CleanupExpired  ----------


#===============================================================================

=head2 FormatWSData

Format data to be returned by a WebService

=head3 Incoming Arguments

=over

=item B<$context>: A Mioga2::RequestContext

=item B<$data>: The data structure to convert

=back

=head3 Return value

A Mioga2::Content::ASIS containing the converted data structure.

=back

=cut

#===============================================================================
sub FormatWSData {
	my ($context, $data) = @_;

	my ($output, $mime, $name);

	my $method = $context->GetURI ()->GetMethod ();
	my $format = $context->GetURI ()->GetFormat ();

	if ($format eq 'xml') {
		$data   = {$method => $data }; # have to set a root element for XML
		$output = Mioga2::tools::Convert::PerlToXML ($data);
		$mime   = "text/xml";
		$name   = $method."-".time.".$format";
	}
	elsif ($format eq 'json') {
		$output = Mioga2::tools::Convert::PerlToJSON ($data);
		$mime   = "application/x-json";
		$name   = $method."-".time.".$format";
	}
	else {
		throw Mioga2::Exception::Simple ("[Mioga2::Login::FormatWSData]", __("Bad format"));
	}

	print STDERR "[Mioga2::Login::FormatWSData] Output: $output\n" if ($debug);

	my $content = Mioga2::Content::ASIS->new ($context, MIME => $mime);
	$content->SetContent ($output);

	return ($content);
}	# ----------  end of subroutine FormatWSData  ----------


#===============================================================================

=head2 GetLang

Get lang from browser or from default value

=head3 Incoming Arguments

=over

=item I<$context>: The Mioga2::RequestContext

=back

=head3 Return value

=over

The lang code (2 letters)

=back

=cut

#===============================================================================
sub GetLang {
	my ($context) = @_;

	my ($lang) = defined ($context->GetDefaultLang ()) ? ($context->GetDefaultLang () =~ m/^([a-z]*)/) : ($context->GetConfig ()->GetDefaultLang () =~ m/^([a-z]*)/);

	return ($lang);
}	# ----------  end of subroutine GetLang  ----------



#===============================================================================

=head2 SelectInstance

Displays a list of instances the connected user is in so he can choose which one to go to.

=head3 Incoming Arguments

=over

None, user is identified from cookie.

=back

=head3 Return value

=over

The select page contents.

=back

=cut

#===============================================================================
sub SelectInstance {
	my ($context) = @_;

	#-------------------------------------------------------------------------------
	# Get instance names
	#-------------------------------------------------------------------------------
	my $db = $context->GetConfig ()->GetDBObject ();
	my $res = $db->SelectMultiple ('SELECT ident FROM m_mioga WHERE rowid IN (SELECT mioga_id FROM m_user_base WHERE email = (SELECT email FROM m_auth_session WHERE token = ?));', [$context->GetUserAuth ()->{token}]);

	if (!@$res) {
		# This should not happen as requests coming here should already have been authenticated.
		# Anyway, redirect hackers to main login page.
		my $content = new Mioga2::Content::REDIRECT ($context, mode => 'external');
		$content->SetContent('DisplayMain');
		return ($content);
	}

	my @instances = map { $_->{ident} } @$res;

	#-------------------------------------------------------------------------------
	# Get lang from user browser
	#-------------------------------------------------------------------------------
	my $lang = GetLang ($context);

	#-------------------------------------------------------------------------------
	# Parse page and translate macros
	#-------------------------------------------------------------------------------
	print STDERR "[Mioga2::Login::SelectInstance] page to parse: $Mioga2::Login::login_data_path/select-instance-$lang.html\n" if ($debug);
	my $page;
	if (-e "$Mioga2::Login::login_data_path/select-instance-$lang.html") {
		open (FH, '<', "$Mioga2::Login::login_data_path/select-instance-$lang.html") or die "File $Mioga2::Login::login_data_path/select-instance-$lang.html not found";
		$page = join ('', <FH>);
		ProcessPageMacros ($context, \$page, { instances => \@instances });
		close (FH);
	}
	else {
		$page = '
			<html>
				<head>
					<title>Error - Mioga2 Authentication</title>
				</head>
				<body>
					<p>The login system pages could not be found. Your Mioga2 installation seems to be incomplete. Please contact an administrator.</p>
				</body>
			</html>';
	}

	#-------------------------------------------------------------------------------
	# Initialize content
	#-------------------------------------------------------------------------------
	my $content = Mioga2::Content::ASIS->new ($context, MIME => 'text/html');
	$content->SetContent ($page);

	return ($content);
}	# ----------  end of subroutine SelectInstance  ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2::Authen, Mioga2::Dispatch, Mioga2::Cleanup, Mioga2::Login::Specific,
Mioga2::Login::Doris, Mioga2::Login::PageMacros, Mioga2::Login::Contact

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;

