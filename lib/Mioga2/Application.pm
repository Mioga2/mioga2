# ============================================================================
# Mioga2 Project (C) 2003-2010 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Application.pm : Virtual class for Mioga2 Applications.

=head1 DESCRIPTION

This module permits implement standard TestAuthorize (based on profiles).

=head1 METHODS DESRIPTION

=cut

#
# ============================================================================

package Mioga2::Application;

use strict;
use Error qw(:try);
use Data::Dumper;
use POSIX qw(:locale_h);
use Locale::TextDomain::UTF8 'application';

use Mioga2::tools::APIAuthz;
use Mioga2::tools::string_utils;
use Mioga2::tools::Convert;
use XML::Atom::SimpleFeed;
use Mioga2::Content::ASIS;
use Mioga2::Exception::Simple;
use Mioga2::ApplicationList;
use Mioga2::GroupList;
use Mioga2::UserList;
use Mioga2::URI;

my $debug = 0;


# ============================================================================

=head2 new (I<$config>)

	Create an Application object and run the Initialize method.
	return a new Mioga2::Application instance.
	
=cut

# ============================================================================


sub new
{
	my ($class, $config) = @_;

	my $self = { };
	bless($self, $class);

	$self->{db} = $config->{db} if (exists ($config->{db}));

	$self->Initialize($config);

	return $self;
}


# ============================================================================

=head2 Initialize (I<$config>)

	Method that can be overloaded by Application implementation to initialize
	variable, resource, etc, at the object creation time.

	Return nothing.

=cut

# ============================================================================

sub Initialize {
	my ($self, $config) = @_;

	return;
}


#===============================================================================

=head2 InitializeFromContext

Initialize application according to request context.

This method is not intended to be overloaded as it provides generic facilities
to Mioga2 applications. If you have specific initialization needs, see InitApp
method below.

=cut

#===============================================================================
sub InitializeFromContext {
	my ($self, $context) = @_;

	# Store configuration
	$self->{config} = $context->GetConfig ();

	if (!$context->IsPublic ()) {
		my $user = $context->GetUser ();
		my $group = $context->GetGroup ();

		# Initialize user access rights to application functions
		# See GetUserRights method below to access this information from the application itself
		my $user_rights = { };
		my $appdesc = $self->GetAppDesc ();
		for my $func_ident (keys (%{$self->GetAppDesc ()->{func_methods}})) {
			$self->{user_rights}->{$func_ident} = $self->CheckUserAccessOnFunction ($context, $user, $group, $func_ident);
		}
	}

	$self->InitApp ($context);
}	# ----------  end of subroutine InitializeFromContext  ----------


#===============================================================================

=head2 InitApp

Application-specific initialization, to be overloaded.

=cut

#===============================================================================
sub InitApp {
	my ($self, $context) = @_;
}	# ----------  end of subroutine InitApp  ----------

# ============================================================================

=head2 InitializeGroupApplication ()

	Method that can be overloaded by Application implementation to initialize
	application values for groups

	Return nothing.

=cut

# ============================================================================

sub InitializeGroupApplication {
	my ($self) = @_;

	return;
}

# ============================================================================

=head2 InitializeResourcesApplication ()

	Method that can be overloaded by Application implementation to initialize
	application values for resources

	Return nothing.

=cut

# ============================================================================

sub InitializeResourcesApplication {
	my ($self) = @_;

	return;
}

# ============================================================================

=head2 InitializeUserApplication ()

	Method that can be overloaded by Application implementation to initialize
	application values for users

	Return nothing.

=cut

# ============================================================================

sub InitializeUserApplication {
	my ($self) = @_;

	return;
}



# ============================================================================

=head2 TestAuthorize ($context)

	Check if the current user can access to the current application and 
	methods in the current group.

=cut

# ============================================================================

sub TestAuthorize {
	my ($self, $context) = @_;

	my $config = $context->GetConfig();

	my $method = $context->GetMethod();

	my $user   = $context->GetUser();
	my $group  = $context->GetGroup();		

	return $self->CheckUserAccessOnMethod($context, $user, $group, $method);
}




# ============================================================================

=head2 CheckUserAccessOnMethod ($context, $user, $group, $method_ident)

	Check if the user $user working in group $group can access to
	the method $method_ident in the current application .
	
=cut

# ============================================================================

sub CheckUserAccessOnMethod {
	my ($self, $context, $user, $group, $method_ident) = @_;

	my $config = $context->GetConfig();
	my $dbh    = $config->GetDBH();

	my $app_desc = $self->GetAppDesc();
	my $app_ident = $app_desc->{ident};

	my $result = ProfileTestAuthzMethod($dbh, $user->GetRowid(), $group->GetRowid(),
	                                          $app_ident, $method_ident);


	if ($result) {
		return AUTHZ_OK;
	}

	else {
		return AUTHZ_FORBIDDEN;
	}

}



# ============================================================================

=head2 CheckUserAccessOnFunction ($context, $user, $group, $func_ident)

	Check if the user $user working in group $group can access to
	the function $func_ident in the current application .
	
=cut

# ============================================================================

sub CheckUserAccessOnFunction {
	my ($self, $context, $user, $group, $func_ident) = @_;

	my $config = $context->GetConfig();
	my $dbh    = $config->GetDBH();

	my $app_desc = $self->GetAppDesc();
	my $app_ident = $app_desc->{ident};

	my $result = ProfileTestAuthzFunction($dbh, $user->GetRowid(), $group->GetRowid(),
	                                            $app_ident, $func_ident);

	if ($result) {
		return AUTHZ_OK;
	}
	else {
		return AUTHZ_FORBIDDEN;
	}

}



# ============================================================================

=head2 RevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

	Remove user data in database when a user is revoked of a group.

	This function is called when :
	- a user is revoked of a group
	- a team is revoked of a group for each team member not namly
	  invited in group
	- a user is revoked of a team  for each group where the team is
	  member and the user not namly invited in group

=cut

# ============================================================================

sub RevokeUserFromGroup {
	my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;

}


# ============================================================================

=head2 RevokeTeamFromGroup ($config, $group_id, $team_id)

	Remove team data in database when a team is revoked of a group.

	This function is called when :
	- a team is revoked of a group

=cut

# ============================================================================

sub RevokeTeamFromGroup {
	my ($self, $config, $group_id, $team_id) = @_;

}


# ============================================================================

=head2 DeleteGroupData ($config, $group_id)

	Remove group data in database when a group is deleted.

	This function is called when a group/user/resource is deleted.

=cut

# ============================================================================

sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;

}


# ============================================================================

=head2 I18nize (I<$package_name>, I<$config>)

=over

=item Method to use gettext in a class.

=item B<parameters :>

=over

=item I<$config> : the mioga2 config object.

=item I<$package_name> : name of the package to translate.

=item I<$locale> : label for locale (fr_FR, ...)

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================

sub I18nize {
    my ($self, $config, $pkg_name, $locale) = @_;
    return unless $config;
    
    my $old_locale = setlocale(LC_ALL, "$locale.UTF-8");
}

# ============================================================================

=head2 WSWrapper ($context, $method, $format)

  Call $method as a WebService and return its results in $format format (XML or JSON).

=cut

# ============================================================================

sub WSWrapper {
  my ($self, $context, $method, $format) = @_;
  
  $self->{data}     = {};
  my $data          = $self->$method($context);
  
  my ($output, $mime, $name);
  
  if ($format eq 'xml') {
    $data   = {$method => $data }; # have to set a root element for XML
	$output = Mioga2::tools::Convert::PerlToXML ($data);
    $mime   = $self->{mimetype} || "text/xml";
    $name   = $method."-".time.".$format";
  }
  elsif ($format eq 'json') {
    my $cb  = $context->{args}->{callback};
    
	$output = Mioga2::tools::Convert::PerlToJSON ($data);
	utf8::decode ($output) unless (utf8::is_utf8 ($output));
    $mime   = $self->{mimetype} || "application/x-json";
    $name   = $method."-".time.".$format";
    
    if ($cb) {
      $output = "$cb($output);";
      $mime   = "text/javascript";
    }
  }
  else {
	throw Mioga2::Exception::Simple("Application::WSWrapper", __("Bad format"));
  }
  
  my $content = Mioga2::Content::ASIS->new($context, MIME => $mime);
  $content->SetContent($output);
  return $content;
}

# ============================================================================

=head2 HasRSS ()

  Checks if current application can generate a RSS feed or not.

=cut

# ============================================================================

sub HasRSS {
    my ($self)  = @_;
    return defined(&{(ref($self) || $self)."::GetRSSFeed"});
}

# ============================================================================

=head2 NotifyUpdate ()

  Send a message to the notify daemon to update resources (RSS, search engine cache, etc.)

=cut

# ============================================================================
sub NotifyUpdate {
    my ($self, $context, %options)  = @_;
    
    my $config          = $context->GetConfig;
    my $group_id        = $context->GetGroup->GetRowid;
    my $user_id         = $context->GetUser->GetRowid;
    my $mioga_id        = $config->GetMiogaId;
    my $default_options = {mioga_id => $mioga_id, user_id => $user_id, group_id => $group_id, application => $self->GetAppDesc->{package}};
    %options            = (%$default_options, %options);
    
    $config->WriteNotifierFIFO(Mioga2::tools::Convert::PerlToJSON (\%options)."\n");
}

# ============================================================================

=head2 AllSuchApplications ($context)

  Returns a tuple (number_of_groups, reference to a chunk of XML)
  for all application instances that this user has access to.
  <AllSuchApplications>
    <app href="http://srv/instance/link/to/this/application">Group name A</group>
    <app href="http://srv/instance/link/to/other/application">Group name B</group>
    ...
  </AllSuchApplications> 

=cut

# ============================================================================
sub AllSuchApplications {
	my ($self, $context) = @_;
	my $config = $context->GetConfig;
	my $method = $context->GetMethod;
	my $app_id = $self->GetAppDesc->{ident};
	
	my $userlist  = new Mioga2::UserList ($config, { attributes => { rowid => $context->GetUser->GetRowid } });
	my $applist   = new Mioga2::ApplicationList ($config, { attributes => { ident => $app_id } });
 	my $grouplist = new Mioga2::GroupList ($config, { attributes => { expanded_users => $userlist, applications => $applist } });
 	$grouplist->Load();
 	my @grouphashes = $grouplist->GetGroups();
 	
 	my $xml = "";
 	foreach my $grouphash (@grouphashes) {
 		my $group_id = $grouphash->{ident};
 		my $uri_str = Mioga2::URI->new($config, application => $app_id, method => $method,
 				group => $group_id, public => 0) ->GetURI(); # TODO: check "public"!
 		$xml .= "<group><ident>${group_id}</ident><url>${uri_str}</url></group>";
 	}
 	return (scalar(@grouphashes), \$xml);
}


#===============================================================================

=head2 GetUserRights

Get user rights table

=cut

#===============================================================================
sub GetUserRights {
	my ($self) = @_;

	return ($self->{user_rights});
}	# ----------  end of subroutine GetUserRights  ----------



# ============================================================================
=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga2 Mioga2::Config Mioga2::URI Mioga2::Application Mioga2::AppDesc
Mioga2::Old::User Mioga2::Old::Group Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2010, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
