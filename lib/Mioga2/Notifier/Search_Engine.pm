# ============================================================================
# Mioga2 Project (C) 2010 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
    
Search_Engine.pm : The notifier module for the search engine.

=head1 METHODS DESCRIPTION

=cut


package Mioga2::Notifier::Search_Engine;
use strict;
use warnings;
use open ':encoding(utf8)';

use Data::Dumper;
use Sys::Syslog;
use Mioga2::tools::database;
use Fcntl;

my $debug = 0;


my %groups;
my $ENGINE_FIFO;

sub new {
    my ($class, $config) = @_;
    my $self = { config => $config };
    bless $self, $class;

	if (!sysopen(SE_FIFO, $config->{searchenginefifo}, O_WRONLY|O_NONBLOCK)) {
        print STDERR "Can't open ".$config->{searchenginefifo}." fifo for writing!";
        return;
    }
    $ENGINE_FIFO = *SE_FIFO;

    return $self;
}


sub notify {
    my ($self, $data)  = @_;
    syslog("debug", "Search notify");
    syslog("debug", Dumper($data)) if ($debug);

	$groups{$data->{group_id}} += 1;
    syslog("debug", Dumper(\%groups)) if ($debug);
}

sub flush {
    my ($self)  = @_;
    syslog("debug", "Search flush");
    syslog("debug", Dumper(\%groups)) if ($debug);
	my @group_list = keys(%groups);
	if (@group_list > 0) {
		my $config = $self->{config};
		my $sql = "SELECT DISTINCT m_mioga.ident FROM m_mioga, m_group_base WHERE m_mioga.rowid=m_group_base.mioga_id AND m_group_base.rowid in (".join(',',@group_list).")";
    	syslog("debug", "sql = $sql") if ($debug);
		my $res = SelectMultiple($config->GetDBH, $sql);
    	syslog("debug", Dumper($res)) if ($debug);
		foreach my $r (@$res) {
			syswrite($ENGINE_FIFO, $r->{ident}."\n");
		}
	}
}

1;
