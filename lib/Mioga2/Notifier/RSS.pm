# ============================================================================
# Mioga2 Project (C) 2010 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
    
RSS.pm : The notifier module for news feeds.

=head1 METHODS DESCRIPTION

=cut


package Mioga2::Notifier::RSS;
use strict;
use warnings;
use open ':encoding(utf8)';

use Data::Dumper;
use Mioga2::tools::database;
use Sys::Syslog;

sub new {
    my ($class, $config) = @_;
    my $self = { config => $config };
    bless $self, $class;
    return $self;
}


sub notify {
    my ($self, $data)  = @_;
    my $config = $self->{config};
    
    my $mioga_ident = SelectSingle($config->GetDBH, "SELECT ident FROM m_mioga WHERE rowid=$data->{mioga_id}");
    if (!$mioga_ident) {
        syslog("error", "Mioga instance with id $data->{mioga_id} not found!");
        return;
    }
    $mioga_ident    = $mioga_ident->{ident};
    my $cache_path  = "$config->{install_dir}/$mioga_ident" . $config->GetMiogaFilesDir . "/feeds";
    my @files;
    if (int($data->{group_id}) == 0) {
        @files  = glob("$cache_path/user-$data->{user_id}/*");
    }
    else {
        @files  = glob("$cache_path/*/global.atom $cache_path/*/group-$data->{group_id}.atom");
    }
    unlink @files;
    syslog("debug", "Deleted files: ".join(', ', @files));	
}

sub flush { }


1;