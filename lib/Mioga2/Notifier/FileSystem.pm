# ============================================================================
# Mioga2 Project (C) 2010 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
    
FileSystem.pm : The notifier module for filesystem maintenance.

=head1 METHODS DESCRIPTION

=cut


package Mioga2::Notifier::FileSystem;
use strict;
use warnings;
use open ':encoding(utf8)';

use Data::Dumper;
use Sys::Syslog;
use Mioga2::tools::database;
use Fcntl;

my $debug = 0;

sub new {
    my ($class, $miogaconf) = @_;
    my $self = { 
    	miogaconf           => $miogaconf
    };
    bless $self, $class;
    return $self;
}


sub notify {
    my ($self, $data)  = @_;

	my $config = $self->{miogaconf};

    if (!exists($data->{uris})) {
    	syslog ("warning", "Notifier::FileSystem got a message without 'uris'");
    } elsif (ref($data->{uris}) ne 'ARRAY') {
    	syslog ("warning", "Notifier::FileSystem got a message, but 'uris' is not an arrayref");
    }
    if (exists($data->{uris}) and ref($data->{uris}) eq 'ARRAY') {
		my $sql = 'SELECT * FROM m_uri WHERE uri IN (' . (join (', ', ('?') x @{$data->{uris}})) . ')';
		# Convert \\/ to / and remove last member of URI to get parent directory
		my @uris = map { s~\\/~/~g; s~^(.*)/[^/]*$~$1~; $_ } @{$data->{uris}};
		my $res = SelectMultiple ($config->GetDBH (), $sql, \@uris);
		@{$self->{uris}} = map { $_->{uri} } @$res;
    }
}

sub flush {
    my ($self)  = @_;
    syslog("debug", "[Notifier::FileSystem::flush] Entering");

	my $config = $self->{miogaconf};

	# Delay flush so that mass-operations are treated as a single block
	sleep (5);

	for my $uri (@{$self->{uris}}) {
    	syslog("debug", "[Notifier::FileSystem::flush] Processing URI $uri");

		# Adjust elements and size fields for each parent directory
		my $sql = 'UPDATE m_uri SET elements = T3.elements, size = T3.size FROM (WITH RECURSIVE uri (rowid, uri, directory_id, size) AS (SELECT rowid, uri, directory_id, size FROM m_uri WHERE uri = ? UNION SELECT m_uri.rowid, m_uri.uri, m_uri.directory_id, m_uri.size FROM uri, m_uri where m_uri.rowid = uri.directory_id) SELECT uri.rowid, COALESCE (T1.elements, 0) AS elements, COALESCE (uri.size + T2.size_offset, 0) AS size FROM (SELECT data.size - COALESCE (m_uri.size, 0) AS size_offset FROM m_uri, (SELECT COALESCE (sum (size), 0) AS size FROM m_uri WHERE directory_id = (SELECT rowid FROM m_uri WHERE uri = ?)) AS data WHERE m_uri.uri = ?) AS T2, uri LEFT JOIN (SELECT directory_id, count(*) AS elements FROM m_uri WHERE directory_id IN (WITH RECURSIVE uri (rowid, uri, directory_id) AS (SELECT rowid, uri, directory_id FROM m_uri WHERE uri = ? UNION SELECT m_uri.rowid, m_uri.uri, m_uri.directory_id FROM uri, m_uri where m_uri.rowid = uri.directory_id) SELECT rowid FROM uri) GROUP BY m_uri.directory_id) AS T1 ON uri.rowid = T1.directory_id) AS T3 WHERE m_uri.rowid = T3.rowid;';
		ExecSQL ($config->GetDBH (), $sql, [$uri, $uri, $uri, $uri]);

		# Update group's disk_space_used
		$sql = 'UPDATE m_group SET disk_space_used = T1.size FROM (SELECT group_id, sum (size) AS size FROM m_uri WHERE group_id = (SELECT group_id FROM m_uri WHERE uri = ?) AND directory_id IS NULL GROUP BY group_id) AS T1 WHERE T1.group_id = m_group.rowid;';
		ExecSQL ($config->GetDBH (), $sql, [$uri]);
	}

    syslog("debug", "[Notifier::FileSystem::flush] Leaving");
}

1;
