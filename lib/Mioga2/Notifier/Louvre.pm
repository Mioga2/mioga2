# ============================================================================
# Mioga2 Project (C) 2010 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Description:

=head1 NAME
    
Louvre.pm : The notifier module for Louvre, the Mioga2 Gallery creator.

=head1 METHODS DESCRIPTION

=cut


package Mioga2::Notifier::Louvre;
use strict;
use warnings;
use open ':encoding(utf8)';

use Error qw(:try);
use Data::Dumper;
use Mioga2::Louvre;
use Sys::Syslog;

sub new {
    my ($class, $miogaconf) = @_;
    my $self = { 
    	miogaconf           => $miogaconf,
		stored_uris         => {},
    };
    bless $self, $class;
    return $self;
}


sub notify {
    my ($self, $data)  = @_;
    if (!exists($data->{uris})) {
    	syslog("warning", "Notifier::Louvre got a message without 'uris'");
    } elsif (ref($data->{uris}) ne 'ARRAY') {
    	syslog("warning", "Notifier::Louvre got a message, but 'uris' is not an arrayref");
    }
    if (exists($data->{uris}) and ref($data->{uris}) eq 'ARRAY') {
    	foreach my $uri (@{$data->{uris}}) {
    		foreach my $parent_uri (@{ Mioga2::Louvre::Gallery->parent_uris($uri) }) {
    			$self->{stored_uris}->{$parent_uri} = 1;
    			syslog("debug", "Notifier::Louvre saves URI: $parent_uri");
    		}
    	}
    }
}


sub flush {
    my ($self) = @_;
    # Find all galleries in need of an update.
    syslog("debug", "Louvre->flush() starts");
	my $galleries;
	try {
		$galleries = Mioga2::Louvre::Gallery::SQL->galleries_for_URIs(
				$self->{miogaconf}, [ keys %{$self->{stored_uris}} ]);
	}
	otherwise {
		my $err = shift;
		syslog ("warning", "Failed to fetch data");
		syslog ("warning", $err->stringify ());
	};

    foreach my $gallery (@$galleries) {
    	syslog("debug", "Gallery needs updating: $gallery->{uri}");
		try {
			$gallery->update();
		}
		otherwise {
			my $err = shift;
			syslog ("warning", "Gallery $gallery->{uri} update failed");
			syslog ("warning", $err->stringify ());
		};
    }
    $self->{stored_uris} = {};
    syslog("debug", "Louvre->flush() returns");
}


1;
