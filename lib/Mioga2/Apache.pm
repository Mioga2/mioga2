# ============================================================================
# MiogaII Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Apache.pm: Load Apache2.

            Define HTTP_* constants for HTTP response code
            (this is a legacy package)

=head1 METHODS DESRIPTION

Available HTTP response code are :
 - HTTP_OK            => 200 OK
 - HTTP_REDIRECT      => 307 TEMPORARILY MOVED
 - HTTP_BAD_REQUEST   => 400 BAD REQUEST 
 - HTTP_AUTH_REQUIRED => 401 AUTH REQUIRED
 - HTTP_FORBIDDEN     => 403 FORBIDDEN
 - HTTP_NOT_FOUND     => 404 NOT FOUND
 - HTTP_SERVER_ERROR  => 500 INTERNAL SERVER ERROR

 - HTTP_DECLINED      => Return code of mod_perl handlers when handler decline the task

=cut

package Mioga2::Apache;
use Mioga2::Apache::Apache2;

use constant MP2 => 1;
use constant HTTP_OK            => Apache2::Const::OK;
use constant HTTP_AUTH_REQUIRED => Apache2::Const::HTTP_UNAUTHORIZED;
use constant HTTP_NOT_FOUND     => Apache2::Const::HTTP_NOT_FOUND;
use constant HTTP_FORBIDDEN     => Apache2::Const::HTTP_FORBIDDEN;
use constant HTTP_DECLINED      => Apache2::Const::DECLINED;
use constant HTTP_SERVER_ERROR  => Apache2::Const::HTTP_INTERNAL_SERVER_ERROR;
use constant HTTP_REDIRECT      => Apache2::Const::HTTP_MOVED_TEMPORARILY;
use constant HTTP_BAD_REQUEST   => Apache2::Const::HTTP_BAD_REQUEST;

1;

