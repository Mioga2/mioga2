# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
#	Descr

=head1 NAME

Mioga2::Planning - The Mioga2 planning application

=head1 METHODS DESCRIPTION

=cut

#
# ============================================================================

package Mioga2::Planning;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'planning';

use Mioga2::Content::XSLT;
use Data::Dumper;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Mioga2::tools::args_checker;
use Date::Manip;
use Error qw(:try);
use POSIX qw(floor ceil strftime tzset);

my $debug = 0;

my %errors = (
    'no_error'            => 0,
    'not_intranet_mode'   => 100,
    'bad_date'            => 101,
    'bad_div_number'      => 102,
    'unauthorized_access' => 200,
);

# ============================================================================
# GetAppDesc ()
#
# Return the current application description.
#
# ============================================================================

sub GetAppDesc {
    my ($self, $context) = @_;

    my %AppDesc = (
        ident              => 'Planning',
        name               => __('Planning'),
        package            => 'Mioga2::Planning',
        description        => __('The Mioga2 planning application'),
        type               => 'normal',
        all_groups         => 1,
        all_users          => 1,
        can_be_public      => 0,
		is_user             => 1,
		is_group            => 1,
		is_resource         => 0,

        functions => {
            'Read'  => __('Consultation functions'),
            'Admin' => __('Configuration functions'),
        },

        func_methods => {
            Read  => [ 'DisplayMain', 'ViewTasks' ],
            Admin => ['ViewPrefs'],
        },

        sxml_methods => {},

        xml_methods => {},

		non_sensitive_methods => [
			'DisplayMain',
			'ViewTasks',
			'ViewPrefs'
		]
    );

    return \%AppDesc;
}

# ============================================================================

=head2 DisplayMain

    DisplayMain is the main function
    If no specific args are given, it displays the default home page.
    If an arg is given refering to a specific function, it calls this function

=head3 Generated XML

	<DisplayMain>

      <miogacontext>See Mioga2::RequestContext</miogacontext>

      <!-- Error identifier -->
      <error id="$error" />

      <user>
         
      </user>

      <!-- for each resource -->
      <resource>
		 like user content.
         
      </resource>
	  
    </DisplayMain>    

=cut

# ============================================================================
sub DisplayMain {
    my ($self, $context) = @_;
    my $error  = 0;
    my $params = {};

    print STDERR "Planning::DisplayMain\n" if ($debug);

    $ENV{TZ} = "GMT";
    tzset();

    $error = $self->GetPreferences($context, $params);
    $error = $self->VerifyParams($context, $params);

    # -------------------------------------------------
    # Get holidays list and generate divisions
    # -------------------------------------------------
    my $holidays = $self->_GetHolidays($context, $params->{start_seconds}, $params->{stop_seconds});
    print STDERR "holidays = " . join(",", @$holidays) . "\n" if ($debug);

    my $divs = $self->GenerateDivs($context, $params, $holidays);

    # -------------------------------------------------
    # Create Content object and initialize XML
    # -------------------------------------------------
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'planning.xsl', locale_domain => 'planning_xsl');

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<DisplayMain>";
    $xml .= $context->GetXML();
    $xml .= qq(<error id="$error" />\n);

    $xml .= '<Params year="' . $params->{start_year} . '" month ="' . $params->{start_month} . '" day="' . $params->{start_day} . '" nb_week="' . $params->{nb_week} . '" />';

    if ($self->CheckUserAccessOnFunction($context, $context->GetUser(), $context->GetGroup(), 'Admin')) {
        $xml .= "<actions>";
        $xml .= "<action>ViewPrefs</action>";
        $xml .= "</actions>";
    }

    $xml .= $self->GenerateXML($context, $params, $divs);
    $xml .= "</DisplayMain>";

    print STDERR "xml = $xml\n" if ($debug);

    $content->SetContent($xml);
    return $content;
}

# ============================================================================

=head2 ViewPrefs 

	Show and set parameters for planning view

=head3 Generated XML

	<ViewPrefs>

      <miogacontext>See Mioga2::RequestContext</miogacontext>

	  <error id="$error" />
	  
	</ViewPrefs>

=cut

# ============================================================================

sub ViewPrefs {
    my ($self, $context) = @_;
    my $error  = 0;
    my $params = {};
    my $args   = $context->{args};

    print STDERR "Planning::ViewPrefs\n" if ($debug);

    $error = $self->GetPreferences($context, $params);

    #
    # Create Content object and initialize XML
    #
    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'planning.xsl', locale_domain => 'planning_xsl');

    my $xml = '<?xml version="1.0"?>';
    $xml .= "<ViewPrefs>";
    $xml .= $context->GetXML();
    $xml .= qq(<error id="$error" />\n);

    $xml .= '<show_we value="' . $params->{show_we} . '" />';
    $xml .= '<view_type value="' . $params->{view_type} . '" />';
    $xml .= '<show_global value="' . $params->{show_global} . '" />';
    $xml .= '<app value="' . st_FormatPostgreSQLString($params->{app}) . '"/>';
    $xml .= '<use_order value="' . $params->{use_order} . '" />';

    my $friends   = $context->GetGroup()->GetExpandedUsers();
    my $resources = $context->GetGroup()->GetInvitedResources();
    my $order     = $self->GetUsersOrder($context);

    my $user_ids        = $friends->GetRowids();
    my $user_idents     = $friends->GetIdents();
    my $user_firstnames = $friends->GetFirstnames();
    my $user_lastnames  = $friends->GetLastnames();

    push @$user_ids,    @{ $resources->GetRowids() };
    push @$user_idents, @{ $resources->GetIdents() };

    my $nb_users = @$user_ids;

    for (my $i = 0 ; $i < $nb_users ; $i++) {
        $xml .= "<user rowid=\"" . $user_ids->[$i] . "\">";
        if ($params->{use_order}) {
            $xml .= "<order>" . $order->{ $user_ids->[$i] } . "</order>";
        }
        if (defined($user_firstnames->[$i])) {
            $xml .= "<firstname>" . $user_firstnames->[$i] . "</firstname>";
            $xml .= "<lastname>" . $user_lastnames->[$i] . "</lastname>";
        }
        else {
            $xml .= "<firstname></firstname>";
            $xml .= "<lastname>" . $user_idents->[$i] . "</lastname>";
        }
        $xml .= "</user>";
    }
    if ($params->{use_order}) {
        foreach my $sep (@{ $order->{sep} }) {
            $xml .= "<user rowid=\"0\"><order>$sep</order></user>";
        }
    }

    $xml .= "</ViewPrefs>";
    print STDERR "xml = $xml\n" if ($debug);
    $content->SetContent($xml);

    return $content;
}

# ============================================================================

=head2 ViewTasks 

	Resume tasks present in a planning division

=head3 Generated XML

	<ViewTasks>

      <miogacontext>See Mioga2::RequestContext</miogacontext>

	  <error id="$error" />
	  
      <!-- if the current tasks is for a resource -->
      <resource ident="resource ident"/>
      
      <!-- if the current tasks is for a user -->
      <user ident="user ident" fn="user first name" ln="user last name"/>

      <!-- Current Date -->
	  <date>
          See Mioga2::tools::date_utils->du_ISOToXML
      </date>

      <!-- 
         For organizer tasks. 
	     accessright parameter is access right of current user on organizer (0=none, 1=read, 2=write)
          -->
	  <Agenda accessright="$accessright">
 
         <!-- For each organizer task -->
         <org_task>
		
      		<id is_strict="0 if task is periodic">task rowid</id>
     		<name>task name</name>
		    <start day="day" month="month" year="year" dow="day of week" hour="H" minute="M" second="S"/>
       		<stop hour="hour" minute="minute"/>

         </org_task>
         ...
 
      </Agenda>
 
      <!-- Task application tasks -->      
      <GlobalTasks>
 		
	     <global_task>

            <id>task rowid</id>

         	<name>Task name</name>
          	<group_ident>Group Ident</group_ident>

        
            <!-- task start date (dow = day of week) -->
            <start day="$d" month="$m" year="$y" dow="$dow"/>

            <!-- task stop date (dow = day of week) -->
            <stop day="$d" month="$m" year="$y" dow="$dow"/>

            <!-- Task Load value -->
 	        <load_value day="$ld" hour="$lh" minute="$lm"/>
          
            <!-- Task Progress -->
         	<progress>progress</progress>

            <!-- Task category parameter -->
            <private>is private</private>
            <bgcolor>bg color in #RRGGBB</bgcolor>
            <fgcolor>fg color in #RRGGBB</fgcolor>

       	</global_task>

        ....
    

    </GlobalTasks>
</ViewTasks>

=cut

# ============================================================================

sub ViewTasks {
    my ($self, $context) = @_;
    my $error = 0;
    my $args  = $context->{args};

    $ENV{TZ} = "GMT";
    tzset();

    my $session = $context->GetSession();
    my $config  = $context->GetConfig();

    my $content = new Mioga2::Content::XSLT($context, stylesheet => 'planning.xsl', locale_domain => 'planning_xsl');
    my $xml = '<?xml version="1.0"?>';
    $xml .= "<ViewTasks";

    if (defined($session->{__internal}->{message})) {
        $xml .= " message=\"$session->{__internal}->{message}{text}\" type=\"$session->{__internal}->{message}{type}\"";
        delete $session->{__internal}->{message};
    }
    $xml .= ">";

    $xml .= $context->GetXML();

    my $grp_ident = $args->{ident};

    $xml .= "<desturi ";
    $xml .= qq| public_bin="| . st_FormatXMLString($config->GetPubBinURI()) . '/' . st_FormatXMLString($grp_ident) . qq|"|;
    $xml .= qq| private_bin="| . st_FormatXMLString($config->GetBinURI()) . '/' . st_FormatXMLString($grp_ident) . qq|"|;
    $xml .= "/>";

    my $args_ident = $args->{ident};

    $xml .= qq(<error id="$error" />\n);

    my $user;

    try {
        $user = new Mioga2::Old::User($config, ident => $args_ident);
    }
    otherwise {
        $user = new Mioga2::Old::Resource($config, ident => $args_ident);
    };

    my $user_id = $user->GetRowid();

    if ($user->GetType() eq 'resource') {
        $xml .= qq|<resource ident="$args_ident"/>|;
    }
    else {
        $xml .= qq|<user ident="$args_ident" fn="| . $user->GetFirstname() . qq|" ln="| . $user->GetLastname() . qq|"/>|;
    }

    my ($start, $stop);
    if (exists $args->{day}) {
        my $day = strftime("%s", 0, 0, 0, $args->{day}, $args->{month} - 1, $args->{year} - 1900);

        $start = strftime("%Y-%m-%d 00:00:00", gmtime($day));
        $stop  = strftime("%Y-%m-%d 23:59:59", gmtime($day));
    }
    else {
        my $day = UnixDate("$args->{year}-w$args->{wofy}", "%s");

        $start = strftime("%Y-%m-%d 00:00:00", gmtime($day));
        $stop  = strftime("%Y-%m-%d 23:59:59", gmtime($day + 6 * 86400));
    }

    $xml .= "<date>" . du_ISOToXML($start) . "</date>";

    my $ApiOrganizer = new Mioga2::Organizer();
    my $organizer_tasks = $ApiOrganizer->Org_ListAllStrictPeriodicTasks($context, $user_id, $user->{timezone_id}, $start, $stop);

    #my $project_tasks = ProjectListProjectTasks($context->{config}->{dbh}, $user_id, $start, $stop);
    my $accessright;

    if ($ApiOrganizer->CheckUserAccessOnFunction($context, $context->GetUser(), $user, 'Write')) {
        $accessright = 2;
    }
    elsif ($ApiOrganizer->CheckUserAccessOnFunction($context, $context->GetUser(), $user, 'Read')) {
        $accessright = 1;
    }
    else {
        $accessright = 0;
    }

    $xml .= "<Agenda accessright=\"$accessright\">";

    foreach my $task (@$organizer_tasks) {

        $xml .= "<org_task>";

        my $is_strict = 0;
        if ($task->{type} eq 'planned') {
            $is_strict = 1;
            $task->{start} = du_ConvertGMTISOToLocale($task->{start}, $user);
        }

        $xml .= qq|<id is_strict="$is_strict">$task->{rowid}</id>|;

        $xml .= "<name>" . st_FormatXMLString($task->{name}) . "</name>";

        my ($y, $m, $d, $H, $M, $S) = ($task->{start} =~ /(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/);
        my $date = strftime("%s", $S, $M, $H, $d, $m - 1, $y - 1900);

        my ($s_hour, $s_min);
        my $dow = strftime("%u", gmtime($date));

        $xml .= qq|<start day="$d" month="$m" year="$y" dow="$dow" hour="$H" minute="$M" second="$S"/>|;

        $date += $task->{duration};
        $s_hour = strftime("%H", localtime($date));
        $s_min  = strftime("%M", localtime($date));

        $xml .= qq|<stop hour="$s_hour" minute="$s_min"/>|;

        my $load_value = $task->{duration};
        my ($lh, $lm);

        if ($load_value >= 3600) {
            $lh = int($load_value / 3600);
            $lm = int($load_value % 3600) / 60;
        }
        else {
            $lh = 0;
            $lm = int($load_value / 60) / 60;
        }

        if ($task->{private} == 1 and $context->GetUser()->GetRowid() == $user_id) {
            $task->{private} = 0;
        }

        $xml .= qq|<duration_value hour="$lh" minute="$lm"/>|;
        $xml .= qq|<private>$task->{private}</private>|;
        $xml .= qq|<bgcolor>$task->{bgcolor}</bgcolor>|;
        $xml .= qq|<fgcolor>$task->{fgcolor}</fgcolor>|;

        $xml .= "</org_task>";
    }

    $xml .= "</Agenda>";

    if ($user->GetType() ne 'resource') {

        my $tasks = new Mioga2::Tasks();
        my $global_tasks = $tasks->GetExtendedTaskList($context, $user_id, $start, $stop);

        $xml .= "<GlobalTasks>";

        foreach my $task (@$global_tasks) {

            my $group;
            try {
                try {
                    $group = new Mioga2::Old::Group($config, rowid => $task->{group_id});

                }
                catch Mioga2::Exception::Group with {
                    my $error = shift;
                    $group = new Mioga2::Old::User($config, rowid => $task->{group_id});
                };
            }
            catch Mioga2::Exception::User with {
                my $error = shift;
                $group = new Mioga2::Old::Resource($config, rowid => $task->{group_id});
            };

            if (!$tasks->CheckUserAccessOnFunction($context, $user, $group, 'Read')) {
                next;
            }

            if ($task->{private} == 1 and ($context->GetUser()->GetRowid() == $task->{creator_id} or $context->GetUser()->GetRowid() == $task->{delegate_id})) {
                $task->{private} = 0;
            }

            $xml .= "<global_task>";

            $xml .= "<id>$task->{rowid}</id>";

            $xml .= "<name>" . st_FormatXMLString($task->{name}) . "</name>";
            $xml .= "<group_ident>" . st_FormatXMLString($group->GetIdent()) . "</group_ident>";
            my ($y, $m, $d) = ($task->{start} =~ /(\d+)-(\d+)-(\d+)/);
            my $dow = strftime("%u", 0, 0, 0, $d, $m - 1, $y - 1900);
            $xml .= qq|<start day="$d" month="$m" year="$y" dow="$dow"/>|;

            ($y, $m, $d) = ($task->{stop} =~ /(\d+)-(\d+)-(\d+)/);
            $dow = strftime("%u", 0, 0, 0, $d, $m - 1, $y - 1900);
            $xml .= qq|<stop day="$d" month="$m" year="$y" dow="$dow"/>|;

            my $load_unit  = $task->{load_unit};
            my $load_value = $task->{load_value};
            my ($ld, $lh, $lm) = (0, 0, 0);

            if ($load_unit == 1) {
                $ld = 0;
                $lh = int($load_value / 60);
                $lm = $load_value % 60;

                $load_value = $lh;

                $load_unit++;
            }

            if ($load_unit == 2) {
                $ld = int($load_value / 24);
                $lh = $load_value % 24;
            }

            if ($load_unit == 3) {
                $ld = $load_value;
            }

            $xml .= qq|<load_value day="$ld" hour="$lh" minute="$lm"/>|;
            $xml .= "<progress>$task->{progress}</progress>";

            $xml .= qq|<private>$task->{private}</private>|;
            $xml .= qq|<bgcolor>$task->{bgcolor}</bgcolor>|;
            $xml .= qq|<fgcolor>$task->{fgcolor}</fgcolor>|;

            $xml .= "</global_task>";
        }

        $xml .= "</GlobalTasks>";

    }

    $xml .= "</ViewTasks>";
    print STDERR "xml = $xml\n" if ($debug);
    $content->SetContent($xml);
    return $content;

}

# ============================================================================
# GetUsersOrder get the order if any for users
# ============================================================================
sub GetUsersOrder {
    my ($self, $context) = @_;

    my $config = $context->GetConfig;
    my $dbh    = $config->GetDBH;
    my $group  = $context->GetGroup->GetRowid;

    my $results = SelectMultiple(
        $dbh, "SELECT planning_user_order.* 
                                      FROM planning_user_order, planning_pref 
                                      WHERE planning_pref.group_id = $group 
                                      AND planning_user_order.planning_id = planning_pref.rowid 
                                      ORDER BY planning_user_order.rowid"
    );
    return {} unless @$results;

    my %orders;
    $orders{sep} = ();
    foreach my $res (@$results) {
        if ($res->{user_id}) {
            $orders{ $res->{user_id} } = $res->{rowid};
        }
        else {
            push @{ $orders{sep} }, $res->{rowid};
        }
    }
    return \%orders;
}

# ============================================================================
# GetUsersTasks construct the XML code needed to display planning
# ============================================================================
sub GetUsersTasks {
    my ($self, $context, $params) = @_;
    my $error     = 0;
    my $userstask = {};

    print STDERR "Planning::GetUsersTasks()\n" if ($debug);

    my $friends   = $context->GetGroup()->GetExpandedUsers();
    my $resources = $context->GetGroup()->GetInvitedResources();

    print STDERR "friends : " . Dumper($friends) . "\n" if ($debug > 2);

    my $user_ids        = $friends->GetRowids();
    my $user_tzs        = $friends->GetTimezones();
    my $user_idents     = $friends->GetIdents();
    my $user_firstnames = $friends->GetFirstnames();
    my $user_lastnames  = $friends->GetLastnames();

    push @$user_ids,    @{ $resources->GetRowids() };
    push @$user_tzs,    @{ $resources->GetTimezones() };
    push @$user_idents, @{ $resources->GetIdents() };

    my $nb_users = @$user_ids;

    my $start = strftime("%Y-%m-%d 00:00:00", gmtime($params->{start_seconds}));
    my $stop  = strftime("%Y-%m-%d 23:59:59", gmtime($params->{stop_seconds}));
    my $org   = new Mioga2::Organizer();

    my @today = gmtime();
    my $today_sec = strftime("%s", 0, 0, 0, $today[3], $today[4], $today[5]);
    print STDERR "today_sec = $today_sec\n" if ($debug);

    my $order = $self->GetUsersOrder($context);

    for (my $i = 0 ; $i < $nb_users ; $i++) {
        my %userdef;
        if (defined($user_firstnames->[$i])) {
            $userdef{firstname} = $user_firstnames->[$i];
            $userdef{lastname}  = $user_lastnames->[$i];
        }
        else {
            $userdef{firstname} = "";
            $userdef{lastname}  = $user_idents->[$i];
        }
        $userdef{ident} = $user_idents->[$i];

        $userstask->{ $user_ids->[$i] }->{userdef}     = \%userdef;
        $userstask->{ $user_ids->[$i] }->{plannedtask} = [];

        if ($params->{use_order}) {
            $userstask->{ $user_ids->[$i] }->{order} = $order->{ $user_ids->[$i] };
        }

        my $org_tasks = $org->Org_ListAllStrictPeriodicTasks($context, $user_ids->[$i], $user_tzs->[$i], $start, $stop);

        print STDERR "tasks : " . Dumper($org_tasks) . "\n" if ($debug);
        foreach my $pt (@$org_tasks) {
            my %task;
            if ($pt->{private}) {
                $task{private}     = 1;
                $task{name}        = "";
                $task{description} = "";
            }
            else {
                $task{private}     = 0;
                $task{name}        = $pt->{name};
                $task{description} = $pt->{description};
            }

            my $begin_date = {};
            my $end_date   = {};

            $begin_date->{iso} = du_ConvertGMTISOToLocale($pt->{start}, $context->GetUser());

            my ($y, $m, $d, $H, $M, $S) = ($begin_date->{iso} =~ /^(\d{4})-(\d{2})-(\d{2})(?: (\d?\d):(\d{2}):(\d{2}))?/);
            $begin_date->{year}    = $y;
            $begin_date->{month}   = $m;
            $begin_date->{day}     = $d;
            $begin_date->{hour}    = $H;
            $begin_date->{minute}  = $M;
            $begin_date->{sec}     = $S;
            $begin_date->{seconds} = du_ISOToSecond($begin_date->{iso});
            $end_date->{seconds}   = $begin_date->{seconds} + $pt->{duration};
            $end_date->{iso}       = du_SecondToISO($end_date->{seconds});
            ($y, $m, $d, $H, $M, $S) = ($end_date->{iso} =~ /^(\d{4})-(\d{2})-(\d{2})(?: (\d?\d):(\d{2}):(\d{2}))?/);
            $end_date->{year}   = $y;
            $end_date->{month}  = $m;
            $end_date->{day}    = $d;
            $end_date->{hour}   = $H;
            $end_date->{minute} = $M;
            $end_date->{sec}    = $S;

            $task{is_planned} = $pt->{is_planned};
            $task{begin}      = $begin_date;
            $task{end}        = $end_date;
            $task{duration}   = $pt->{duration};
            $task{outside}    = $pt->{outside};

            if ($task{is_planned}) {

                #
                # Calculate position and width for one day
                #      + $params{begin_work_hour}
                # | Z1 |        Z2           | Z3 |
                #                            + $params{end_work_hour}
                #
                $task{wz1} = 0;
                $task{wz2} = 0;
                $task{wz3} = 0;
                my $z1_duration = $params->{begin_work_hour};
                my $z2_duration = $params->{end_work_hour} - $params->{begin_work_hour};
                my $z3_duration = 24 - $params->{end_work_hour};
                my $duration    = $task{duration} / 3600;
                print STDERR "z1_duration = $z1_duration z2_duration = $z2_duration z3_duration = $z3_duration\n" if ($debug);
                print STDERR "task_name = " . $task{name} . " duration = $duration\n" if ($debug);
                my $delta_duration;

                if ($begin_date->{hour} < $params->{begin_work_hour}) {
                    print STDERR "begin in z1\n" if ($debug);
                    $task{pos} = ceil(100 * $begin_date->{hour} / $z1_duration);
                    $delta_duration = $z1_duration - $begin_date->{hour};
                    if ($duration > $delta_duration) {
                        $task{wz1} = ceil($delta_duration);
                    }
                    else {
                        $task{wz1} = ceil($duration);
                    }
                    $duration -= $task{wz1};
                }
                elsif ($begin_date->{hour} >= $params->{begin_work_hour} && $begin_date->{hour} < $params->{end_work_hour}) {
                    print STDERR "begin in z2\n" if ($debug);
                    $task{pos} = ceil(100 * ($begin_date->{hour} - $params->{begin_work_hour}) / $z2_duration);
                    $delta_duration = floor($z2_duration - ($begin_date->{hour} - $params->{begin_work_hour}));
                    if ($duration > $delta_duration) {
                        $task{wz2} = ceil($delta_duration);
                    }
                    else {
                        $task{wz2} = ceil($duration);
                    }
                    $duration -= $task{wz2};
                }
                elsif ($begin_date->{hour} >= $params->{end_work_hour}) {
                    print STDERR "begin in z3\n" if ($debug);
                    $task{pos} = ceil(100 * ($begin_date->{hour} - $params->{end_work_hour}) / $z3_duration);
                    $task{wz3} = ceil($duration);
                    $duration -= $task{wz3};
                }

                #
                # rest of duration to put in following zone
                #
                print STDERR "rest of duration = $duration\n" if ($debug);
                print STDERR "task->wz1 = " . $task{wz1} . " task->wz2 = " . $task{wz2} . " task->wz3 = " . $task{wz3} . " task->pos = " . $task{pos} . "\n" if ($debug);
                if ($duration > 0) {
                    if ($task{wz1} > 0) {
                        if ($duration > $z2_duration) {
                            $task{wz2} = $z2_duration;
                            $duration -= $z2_duration;
                        }
                        else {
                            $task{wz2} = $duration;
                        }
                    }
                    if ($task{wz2} > 0) {
                        $task{wz3} = $duration;
                    }
                }
                $task{wz1} *= ceil(100 / $z1_duration);
                $task{wz2} *= ceil(100 / $z2_duration);
                $task{wz3} *= ceil(100 / $z3_duration);
                print STDERR "task->wz1 = " . $task{wz1} . " task->wz2 = " . $task{wz2} . " task->wz3 = " . $task{wz3} . " task->pos = " . $task{pos} . "\n" if ($debug);
            }
            $task{bgcolor} = $pt->{bgcolor};
            $task{fgcolor} = $pt->{fgcolor};

            push @{ $userstask->{ $user_ids->[$i] }->{plannedtask} }, \%task;
        }

        #
        # Get and process global task if needed
        #
        if ($params->{show_global}) {
            my $mioga2_tasks = new Mioga2::Tasks();
            my $global_tasks = $mioga2_tasks->GetExtendedTaskList($context, $user_ids->[$i], $start, $stop);
            print STDERR "global_tasks : " . Dumper($global_tasks) if ($debug);

            foreach my $pt (@$global_tasks) {
                my %task;
                $task{name}        = $pt->{name};
                $task{description} = $pt->{description};

                my $begin_date = {};
                my $end_date   = {};
                my $load;

                #$begin_date->{iso} = du_ConvertGMTISOToLocale($pt->{start}, $context->GetUser());
                $begin_date->{iso} = $pt->{start};
                my ($y, $m, $d) = ($begin_date->{iso} =~ /^(\d{4})-(\d{2})-(\d{2})?/);
                $begin_date->{year}    = $y;
                $begin_date->{month}   = $m;
                $begin_date->{day}     = $d;
                $begin_date->{seconds} = du_ISOToSecond($begin_date->{iso});

                #$end_date->{iso} = du_ConvertGMTISOToLocale($pt->{stop}, $context->GetUser());
                $end_date->{iso} = $pt->{stop};
                ($y, $m, $d) = ($end_date->{iso} =~ /^(\d{4})-(\d{2})-(\d{2})?/);
                $end_date->{year}    = $y;
                $end_date->{month}   = $m;
                $end_date->{day}     = $d;
                $end_date->{seconds} = du_ISOToSecond($end_date->{iso});

                $task{begin}   = $begin_date;
                $task{end}     = $end_date;
                $task{bgcolor} = $pt->{bgcolor};

                #
                # Calculate the amount of used day and report after today
                #
                $load = $pt->{load_value};
                if ($pt->{load_unit} == 1) {
                    $load *= 60;
                }
                elsif ($pt->{load_unit} == 2) {
                    $load *= 3600;
                }
                else {
                    $load *= 86400;
                }

                my $progress = $load * $pt->{progress} / 100;
                $task{day_before_today} = $progress / 86400;
                $task{day_after_today}  = ($load - $progress) / 86400;
                print STDERR "name = " . $pt->{name} . "  load = $load   progress : $progress  day_before_today = " . $task{day_before_today} . " day_after_today : " . $task{day_after_today} . "\n" if ($debug);

                #
                # If stop_constraint is fixed and task end < today, we don't cut
                # else if end if > today, we adjust to not overflow end date
                #
                if ($pt->{stop_constraint} == 1) {
                    if ($end_date->{seconds} < $self->{today}) {
                        $task{day_before_today} = $load / 86400;
                        $task{day_after_today}  = 0;
                    }
                    else {
                        my $days_to_end = ($end_date->{seconds} - $self->{today}) / 86400;
                        print STDERR " days_to_end = $days_to_end\n" if ($debug);
                        if ($task{day_after_today} > $days_to_end) {
                            my $diff = $task{day_after_today} - $days_to_end;
                            $task{day_after_today} -= $diff;
                            $task{day_before_today} += $diff;
                        }
                    }
                }

                #
                # Suppress before_today days that cannot be seen
                # because they are before the beginning of display
                #
                if ($params->{start_seconds} > $begin_date->{seconds}) {
                    my $passed_days = ($params->{start_seconds} - $begin_date->{seconds}) / 86400;
                    print STDERR " passed_days = $passed_days\n" if ($debug);
                    $task{day_before_today} -= $passed_days;
                    if ($task{day_before_today} < 0) {
                        $task{day_before_today} = 0;
                    }
                }
                print STDERR " day_before_today = " . $task{day_before_today} . " day_after_today : " . $task{day_after_today} . "\n" if ($debug);

                push @{ $userstask->{ $user_ids->[$i] }->{globaltask} }, \%task;
            }
        }
    }

    print STDERR Dumper($userstask) if ($debug > 1);
    return $userstask;
}

# ============================================================================
# ProcessTaskList generate XML tag for users task
# if date is before today, we set a flag per day for global task with progress set
# else we put a flag per day an shift depends on global load of all tasks
# ============================================================================
sub ProcessTaskList {
    my ($self, $divs, $tasklist, $divs_global, $param) = @_;
    my $xml        = "";
    my $div_count  = @{ $divs->{divs} };
    my $task_count = @$tasklist;
    my $idiv       = 0;
    my $itask      = 0;
    my $iglobal    = 0;

    print STDERR "ProcessTaskList div_count = $div_count  task_count = $task_count\n" if ($debug);

    while ($idiv < $div_count) {
        if (   (($itask < $task_count) && ($tasklist->[$itask]->{begin}->{seconds} < $divs->{divs}->[$idiv]->{stop}))
            || (($divs_global->{nb_globaltasks} > 0) && ($divs_global->{data}->[$idiv]->[0] > 0)))
        {
            $xml .= '<div_user order="' . $divs->{divs}->[$idiv]->{order} . '">' . "\n";
            while (($itask < $task_count) && ($tasklist->[$itask]->{begin}->{seconds} < $divs->{divs}->[$idiv]->{stop})) {
                if (int($tasklist->[$itask]->{begin}->{day}) != int($divs->{divs}->[$idiv]->{day})) {
                    $itask++;
                    next;
                }

                if ($tasklist->[$itask]->{is_planned}) {
                    $xml .= '<task text="' . st_FormatXMLString($tasklist->[$itask]->{name}) . '"' . ' is_planned="1"' . ' begin_hour="' . $tasklist->[$itask]->{begin}->{hour} . '"' . ' begin_min="' . $tasklist->[$itask]->{begin}->{minute} . '"' . ' end_hour="' . $tasklist->[$itask]->{end}->{hour} . '"' . ' end_min="' . $tasklist->[$itask]->{end}->{minute} . '"' . ' duration="' . $tasklist->[$itask]->{duration} . '"' . ' outside="' . $tasklist->[$itask]->{outside} . '"' . ' color="' . $tasklist->[$itask]->{bgcolor} . '"' . ' position="' . $tasklist->[$itask]->{pos} . '"' . ' wz1="' . $tasklist->[$itask]->{wz1} . '"' . ' wz2="' . $tasklist->[$itask]->{wz2} . '"' . ' wz3="' . $tasklist->[$itask]->{wz3} . '"' . " />\n";
                }
                else {
                    $xml .= '<day_task text="' . st_FormatXMLString($tasklist->[$itask]->{name}) . '"' . ' is_planned="0"' . ' outside="' . $tasklist->[$itask]->{outside} . '"' . ' begin_hour="0"' . ' begin_min="0"' . ' end_hour="0"' . ' end_min="0"' . ' duration="0"' . ' color="' . $tasklist->[$itask]->{bgcolor} . '"' . " />\n";
                }
                $itask++;
            }
            if (($param->{view_type} ne 'week') && ($divs_global->{nb_globaltasks} > 0) && ($divs_global->{data}->[$idiv]->[0] > 0)) {
                for (my $igt = 0 ; $igt < $divs_global->{nb_globaltasks} ; $igt++) {
                    $xml .= '<globaltask value="' . $divs_global->{data}->[$idiv]->[ $igt + 1 ] . '"' . ' text="' . $divs_global->{tasks}->[$igt]->{text} . '"' . ' color="' . $divs_global->{tasks}->[$igt]->{color} . '"' . "/>\n";
                }
            }
            $xml .= "</div_user>\n";
        }
        else {
            if (($param->{view_type} ne 'week') && ($divs_global->{nb_globaltasks} > 0) && ($divs_global->{data}->[$idiv])) {
                $xml .= '<div_user order="' . $divs->{divs}->[$idiv]->{order} . '">' . "\n";
                for (my $igt = 0 ; $igt < $divs_global->{nb_globaltasks} ; $igt++) {
                    $xml .= '<globaltask value="' . $divs_global->{data}->[$idiv]->[ $igt + 1 ] . '"' . ' text="' . $divs_global->{tasks}->[$igt]->{text} . '"' . ' color="' . $divs_global->{tasks}->[$igt]->{color} . '"' . "/>\n";
                }
                $xml .= "</div_user>\n";
            }
        }
        $idiv++;
    }

    return $xml;
}

# ============================================================================
# ProcessGlobalTask generate XML tag for users global task
# if date is before today, we set a flag per day for global task with progress set
# else we put a flag per day an shift depends on global load of all tasks
# The divs_global data is a [nb_divs]X[nb_globaltask + 1] table
# The first row is an indicator for an empty cell if set to 0
# The value of a cell can be :
# 0 : nothing
# 1 : task color
# 2 : task delayed
# 3 : end margin space
# ============================================================================
sub ProcessGlobalTask {
    my ($self, $divs, $globaltask) = @_;
    my $div_count    = @{ $divs->{divs} };
    my $task_count   = @$globaltask;
    my $idiv         = 0;
    my $itask        = 0;
    my $stock_before = 0;
    my $stock_after  = 0;
    my $divs_global  = {};

    print STDERR "ProcessGlobalTask div_count = $div_count  task_count = $task_count\n" if ($debug);
    $divs_global->{nb_globaltasks} = $task_count;
    $divs_global->{data}           = [];
    $divs_global->{tasks}          = [];

    for ($itask = 0 ; $itask < $task_count ; $itask++) {
        my $fl_after = 0;    # set to 1 after today;

        $divs_global->{tasks}->[$itask]->{color} = $globaltask->[$itask]->{bgcolor};
        $divs_global->{tasks}->[$itask]->{text}  = $globaltask->[$itask]->{name};
        $stock_before                            = $globaltask->[$itask]->{day_before_today};
        $stock_after                             = $globaltask->[$itask]->{day_after_today};
        print STDERR " itask = $itask name = " . $globaltask->[$itask]->{name} . "  stock_before = $stock_before  stock_after = $stock_after\n" if ($debug);

        #
        # If visu begins after today
        # Initialize stock_after to a negative bvalue corresponding to
        # the difference between today and the begin of display
        # only for task with a bgein date before start of display.
        if ($divs->{divs}->[0]->{start} > $self->{today}) {
            $fl_after = 1;
            if ($globaltask->[$itask]->{begin}->{seconds} <= $self->{today}) {
                $stock_after -= (floor(($divs->{divs}->[0]->{start} - $self->{today}) / 86400));
            }
            elsif ($globaltask->[$itask]->{begin}->{seconds} <= $divs->{divs}->[0]->{start}) {
                $stock_after -= (floor(($divs->{divs}->[0]->{start} - $globaltask->[$itask]->{begin}->{seconds}) / 86400));
            }
            if ($stock_after < 0) {
                $stock_after = 0;
            }
            print STDERR "   fl_after = $fl_after stock_after = $stock_after\n" if ($debug);
        }

        for ($idiv = 0 ; $idiv < $div_count ; $idiv++) {
            my $set = 0;
            if (!$fl_after && ($divs->{divs}->[$idiv]->{stop} > $self->{today})) {
                $fl_after = 1;

                # Get the rest of days if not used
                $stock_after += $stock_before;
            }

            #
            # If the task can be plotted
            #
            print STDERR "idiv = $idiv  stock_before = $stock_before  stock_after = $stock_after\n" if ($debug);
            print STDERR " begin seconds = " . $globaltask->[$itask]->{begin}->{seconds} . " begin day = " . $globaltask->[$itask]->{begin}->{day} . " div start = " . $divs->{divs}->[$idiv]->{start} . " div day = " . $divs->{divs}->[$idiv]->{day} . "\n" if ($debug);

            if ($divs->{divs}->[$idiv]->{start} >= $globaltask->[$itask]->{begin}->{seconds}) {

                if (($stock_before > 0) && (!$fl_after)) {
                    $stock_before--;
                    $set = 1;
                }
                elsif ($fl_after && ($stock_after > 0)) {
                    print STDERR "   stock_after = $stock_after\n" if ($debug);
                    $stock_after--;
                    $set = 1;
                }
                elsif ($fl_after && ($stock_after == 0)) {
                    if ($divs->{divs}->[$idiv]->{stop} < $globaltask->[$itask]->{end}->{seconds}) {
                        print STDERR "   end margin for task\n" if ($debug);
                        $set = 3;
                    }
                }
            }
            $divs_global->{data}->[$idiv]->[0] += $set;
            $divs_global->{data}->[$idiv]->[ $itask + 1 ] = $set;
        }
    }

    print STDERR Dumper($divs_global) if ($debug);
    return $divs_global;
}

# ============================================================================
# GenerateXML construct the XML code needed to display planning
# ============================================================================
sub GenerateXML {
    my ($self, $context, $param, $divs) = @_;
    my $error = 0;

    print STDERR "Planning::GenerateXML()\n" if ($debug);

    my $xml = '<ViewType app="' . st_FormatPostgreSQLString($param->{app}) . '" type="' . $param->{view_type} . '" show_global="' . $param->{show_global} . '" nb_cols="' . $param->{nb_cols} . '"' . " />\n";

    if (defined($divs->{maindiv})) {
        foreach my $div (@{ $divs->{maindiv} }) {
            $xml .= '<main_div month="' . $div->{month} . '"' . ' wofy="' . $div->{wofy} . '"' . ' begin_year="' . $div->{begin_year} . '"' . ' begin_month="' . $div->{begin_month} . '"' . ' begin_day="' . $div->{begin_day} . '"' . ' end_year="' . $div->{end_year} . '"' . ' end_month="' . $div->{end_month} . '"' . ' end_day="' . $div->{end_day} . '"' . ' count="' . $div->{count} . '"';
            $xml .= " />\n";
        }
    }
    foreach my $div (@{ $divs->{divs} }) {
        $xml .= '<division order="' . $div->{order} . '"' . ' day="' . $div->{day} . '"' . ' month="' . $div->{month} . '"' . ' year="' . $div->{year} . '"' . ' wofy="' . $div->{wofy} . '"' . ' wday="' . $div->{wday} . '"';
        if (defined($div->{current}) && $div->{current}) {
            $xml .= ' current="yes"';
        }
        if (defined($div->{holiday}) && $div->{holiday}) {
            $xml .= ' holiday="yes"';
        }
        $xml .= " />\n";
    }

    my $userstask = $self->GetUsersTasks($context, $param);
    foreach my $key (keys(%$userstask)) {
        my $user = $userstask->{$key};
        $xml .= '<user id="' . $key . '"' . ">\n";
        $xml .= "<order>" . $user->{order} . "</order>" if $param->{use_order};
        $xml .= "<firstname>" . st_FormatXMLString($user->{userdef}->{firstname}) . "</firstname>\n";
        $xml .= "<lastname>" . st_FormatXMLString($user->{userdef}->{lastname}) . "</lastname>\n";
        $xml .= "<ident>" . st_FormatXMLString($user->{userdef}->{ident}) . "</ident>\n";

        my $divs_global;
        if (defined($user->{globaltask})) {
            $divs_global = $self->ProcessGlobalTask($divs, $user->{globaltask});
        }
        else {
            $divs_global->{nb_globaltasks} = 0;
        }

        if (defined($user->{plannedtask})) {
            $xml .= $self->ProcessTaskList($divs, $user->{plannedtask}, $divs_global, $param);
        }

        $xml .= "</user>\n";
    }
    if ($param->{use_order}) {
        my $order = $self->GetUsersOrder($context);
        foreach my $sep (@{ $order->{sep} }) {
            $xml .= "<user id=\"0\"><order>$sep</order></user>";
        }
    }

    return $xml;
}

# ============================================================================
# GenerateDivs construct an hash with subdivision for display
# ============================================================================
sub GenerateDivs {
    my ($self, $context, $param, $holidays) = @_;
    my $error = 0;
    my $divs  = {};

    print STDERR "Planning::GenerateDivs()\n" if ($debug);

    my $uinfos = $context->GetUser();
    my ($beg_year, $beg_month, $beg_day) = ($param->{startDate} =~ /^(\d{4})-(\d{2})-(\d{2})?/);
    my ($end_year, $end_month, $end_day) = ($param->{stopDate}  =~ /^(\d{4})-(\d{2})-(\d{2})?/);

    my $incSeconds  = 86400;
    my $crtSeconds  = $param->{start_seconds};
    my $nextSeconds = $param->{start_seconds} + $incSeconds;

    my $val = 1;
    {
        local $ENV{TZ} = $uinfos->GetTimezone();
        tzset();
        while ($crtSeconds < $param->{stop_seconds}) {
            $nextSeconds = $crtSeconds + $incSeconds;
            my $date = strftime("%S,%M,%H,%d,%m,%Y,%w,%V", localtime($crtSeconds));
            print STDERR " val = $val  crtSeconds = $crtSeconds  date =$date  today = " . $self->{today} . " nextSeconds = $nextSeconds \n" if ($debug);
            my @crtDate = split(',', $date);
            if (($param->{show_we}) || (($crtDate[6] != 0) && ($crtDate[6] != 6))) {
                my $div = {};
                $div->{order} = $val;
                $div->{day}   = $crtDate[3];
                $div->{month} = $crtDate[4];
                $div->{year}  = $crtDate[5];
                $div->{wday}  = $crtDate[6];
                $div->{wofy}  = $crtDate[7];
                $div->{start} = strftime("%s", $crtDate[0], $crtDate[1], $crtDate[2], $crtDate[3], $crtDate[4] - 1, $crtDate[5] - 1900);
                $div->{stop}  = $div->{start} + $incSeconds - 1;

                if (($crtSeconds <= $self->{today}) && ($nextSeconds > $self->{today})) {
                    $div->{current} = 1;
                }
                my $date_iso = strftime("%Y-%m-%d %H:%M:%S", localtime($crtSeconds));

                #$div->{holiday} = du_IsHoliday($context->GetConfig(), $date_iso);
                $div->{holiday} = 0;
                foreach my $h (@$holidays) {
                    if ($crtSeconds == $h) {
                        $div->{holiday} = 1;
                        last;
                    }
                }
                push @{ $divs->{divs} }, $div;
                print STDERR "day =" . $crtDate[3] . " month = " . ($crtDate[4] + 1) . "\n" if ($debug);
                $val++;
            }
            $crtSeconds = $nextSeconds;
        }
    }
    tzset();

    $param->{nb_cols} = $val - 1;
    my $inc = 5;
    if ($param->{show_we}) {
        $inc = 7;
    }
    my $imain = 0;
    for (my $i = 0 ; $i < $param->{nb_cols} ; $i += $inc) {
        print STDERR "i = $i  imain =$imain\n" if ($debug);
        $divs->{maindiv}->[$imain]->{month}       = $divs->{divs}->[$i]->{month};
        $divs->{maindiv}->[$imain]->{wofy}        = $divs->{divs}->[$i]->{wofy};
        $divs->{maindiv}->[$imain]->{count}       = $inc;
        $divs->{maindiv}->[$imain]->{begin_year}  = sprintf("%04d", $divs->{divs}->[$i]->{year});
        $divs->{maindiv}->[$imain]->{begin_month} = sprintf("%02d", $divs->{divs}->[$i]->{month});
        $divs->{maindiv}->[$imain]->{begin_day}   = sprintf("%02d", $divs->{divs}->[$i]->{day});
        $divs->{maindiv}->[$imain]->{end_year}    = sprintf("%04d", $divs->{divs}->[ $i + $inc - 1 ]->{year});
        $divs->{maindiv}->[$imain]->{end_month}   = sprintf("%02d", $divs->{divs}->[ $i + $inc - 1 ]->{month});
        $divs->{maindiv}->[$imain]->{end_day}     = sprintf("%02d", $divs->{divs}->[ $i + $inc - 1 ]->{day});
        $imain++;
    }

    print STDERR Dumper($divs) if ($debug > 1);
    return $divs;
}

# ============================================================================
# UpdateUsersOrder updates order of users in DB
# ============================================================================
sub UpdateUsersOrder {
    my ($self, $context) = @_;

    my $users  = $context->{args}->{users};
    my $config = $context->GetConfig;
    my $dbh    = $config->GetDBH;
    my $group  = $context->GetGroup->GetRowid;

    my $plan_id = SelectSingle(
        $dbh, "SELECT rowid 
                                      FROM planning_pref 
                                      WHERE group_id=$group"
    );
    $plan_id = $plan_id->{rowid};
    ExecSQL(
        $dbh, "DELETE FROM planning_user_order 
                 WHERE planning_user_order.planning_id=$plan_id"
    );
    foreach my $user (@$users) {
        $user = int($user);
        $user = 'NULL' if $user == 0;
        ExecSQL(
            $dbh, "INSERT INTO planning_user_order(planning_id, user_id) 
                   VALUES($plan_id, $user)"
        );
    }
}

# ============================================================================
# GetPreferences Read preferences in parameters or database
# ============================================================================
sub GetPreferences {
    my ($self, $context, $params) = @_;
    my $error = 0;
    print STDERR "Planning::GetPreferences()\n" if ($debug);

    #
    # TODO : must be in prefs
    #
    $params->{begin_work_hour} = 8;
    $params->{end_work_hour}   = 20;

    if (defined($context->{args}->{set_pref}) || defined($context->{args}->{'set_pref.x'})) {

        #
        # Get and verify parameters
        #
        my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'show_we', 'show_global', 'use_order' ], 'disallow_empty', 'bool' ], [ ['users'], 'stripxws', 'allow_empty', 'want_int' ], [ [ 'view_type', 'app', 'set_pref' ], 'stripxws', 'disallow_empty' ], ]);

        if (scalar(@$errors) > 0) {
            throw Mioga2::Exception::Application("Mioga2::Planning::VerifyParams", ac_FormatErrors($errors));
        }

        #
        print STDERR "Planning::GetPreferences set_pref set, so read from context\n" if ($debug);
        $params->{show_we}     = $context->{args}->{show_we};
        $params->{view_type}   = $context->{args}->{view_type};
        $params->{show_global} = $context->{args}->{show_global};
        $params->{app}         = $context->{args}->{app};
        $params->{use_order}   = $context->{args}->{use_order};

        my $sql = "select rowid from planning_pref where group_id = " . $context->GetGroup()->GetRowid() . ";";
        print STDERR "sql = $sql\n" if ($debug);
        my $pref = SelectSingle($context->{config}->GetDBH(), $sql);
        print STDERR 'planning_pref = ' . Dumper($pref) . "\n" if ($debug);

        my $show_we;
        if ($params->{show_we}) {
            $show_we = "t";
        }
        else {
            $show_we = "f";
        }

        my $show_global;
        if ($params->{show_global}) {
            $show_global = "t";
        }
        else {
            $show_global = "f";
        }

        my $use_order;
        if ($params->{use_order}) {
            $use_order = "t";
        }
        else {
            $use_order = "f";
        }

        if (defined($pref)) {
            $sql = "update planning_pref 
			        set modified=now(), 
			        show_global='$show_global', 
			        show_we='$show_we',
			        use_order='$use_order', 
			        view_type='" . st_FormatPostgreSQLString($params->{view_type}) . "', app='" . st_FormatPostgreSQLString($params->{app}) . "' 
			        where rowid=" . $pref->{rowid} . ";";
        }
        else {
            $sql = "insert into planning_pref (created, modified, group_id, show_we, view_type, show_global, app, use_order) 
			        VALUES(now(), now(), " . $context->GetGroup()->GetRowid() . ", '$show_we', '" . st_FormatPostgreSQLString($params->{view_type}) . "', '$show_global', '" . st_FormatPostgreSQLString($params->{app}) . "', '$use_order');";
        }
        print STDERR "sql = $sql\n" if ($debug);
        ExecSQL($context->{config}->GetDBH(), $sql);
        $self->UpdateUsersOrder($context) if $use_order eq 't';
    }
    else {

        #
        # Get and verify parameters
        #
        my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'show_we', 'show_global' ], 'allow_empty', 'bool' ], [ [ 'view_type', 'app' ], 'stripxws', 'allow_empty' ], ]);

        if (scalar(@$errors) > 0) {
            throw Mioga2::Exception::Application("Mioga2::Planning::VerifyParams", ac_FormatErrors($errors));
        }

        # --------------------------------------------------------
        # Read database parameters
        # --------------------------------------------------------
        my $sql = "select * from planning_pref where group_id = " . $context->GetGroup()->GetRowid() . ";";
        print STDERR "sql = $sql\n" if ($debug);
        my $pref = SelectSingle($context->{config}->GetDBH(), $sql);
        print STDERR 'planning_pref = ' . Dumper($pref) . "\n" if ($debug);

        if (defined($pref)) {
            $params->{show_we}     = $pref->{show_we};
            $params->{show_global} = $pref->{show_global};
            $params->{view_type}   = $pref->{view_type};
            $params->{app}         = $pref->{app};
            $params->{use_order}   = $pref->{use_order};
        }
        else {
            print STDERR "pas de pref\n" if ($debug);
            $params->{show_we}     = 0;
            $params->{view_type}   = 'week';
            $params->{show_global} = 1;
            $params->{app}         = '/Planning/ViewTasks';
            $params->{use_order}   = 0;
        }

        # --------------------------------------------------------
        # Overwrite date parameters if set session
        # --------------------------------------------------------
        my $session = $context->GetSession();
        if (defined($session->{planning})) {
            $params->{start_day}   = $session->{planning}->{args}->{day};
            $params->{start_month} = $session->{planning}->{args}->{month};
            $params->{start_year}  = $session->{planning}->{args}->{year};
        }

        # --------------------------------------------------------
        # Overwrite parameters if set on command line
        # --------------------------------------------------------
        if (defined($context->{args}->{show_we})) {
            print STDERR 'overwrite show_we = ' . $context->{args}->{show_we} . "\n" if ($debug);
            $params->{show_we} = $context->{args}->{show_we};
        }
        if (defined($context->{args}->{view_type})) {
            print STDERR 'overwrite view_type = ' . $context->{args}->{view_type} . "\n" if ($debug);
            $params->{view_type} = $context->{args}->{view_type};
        }
        if (defined($context->{args}->{show_global})) {
            print STDERR 'overwrite show_global = ' . $context->{args}->{show_global} . "\n" if ($debug);
            $params->{show_global} = $context->{args}->{show_global};
        }

        #if (defined($context->{args}->{use_order}))
        #{
        #print STDERR 'overwrite use_order = ' . $context->{args}->{use_order} . "\n" if($debug);
        #$params->{use_order} = $context->{args}->{use_order};
        #}
    }
    print STDERR "params getPref : " . Dumper($params) if ($debug);
}

# ============================================================================
# VerifyParams checks parameters and initialize params hash
# ============================================================================
sub VerifyParams {
    my ($self, $context, $params) = @_;
    my $error = 0;
    print STDERR "Planning::VerifyParams()\n" if ($debug);

    my $session = $context->GetSession();
    my $uinfos  = $context->GetUser();

    #
    # Get and verify parameters
    #
    my ($values, $errors) = ac_CheckArgs($context, [ [ [ 'day', 'month', 'year', 'nb_week' ], 'allow_empty', 'want_int' ], [ ['reload'], 'stripxws', 'allow_empty' ], [ [ 'next_week', 'previous_week' ], 'stripxws', 'allow_empty' ], [ [ 'next_month', 'previous_month' ], 'stripxws', 'allow_empty' ], [ [ 'next_step', 'previous_step' ], 'stripxws', 'allow_empty' ], [ ['view_type'], 'stripxws', 'allow_empty' ], ]);

    if (scalar(@$errors) > 0) {
        throw Mioga2::Exception::Application("Mioga2::Planning::VerifyParams", ac_FormatErrors($errors));
    }

    #
    # Calculate today date in seconds since EPOCH for GMT and User's locale
    #
    my $gmt_iso = du_GetNowISO();
    my $iso = du_ConvertGMTISOToLocale($gmt_iso, $uinfos);
    print STDERR "today date in GMT gmt_iso = $gmt_iso locale : iso = $iso\n" if ($debug);
    ($self->{today_year}, $self->{today_month}, $self->{today_day}) = ($iso =~ /^(\d{4})-(\d{2})-(\d{2})?/);
    $iso = $self->{today_year} . "-" . $self->{today_month} . "-" . $self->{today_day} . " 00:00:00";
    $self->{today} = du_ISOToSecond($iso);
    if ($debug) {
        $iso = du_SecondToISO($self->{today});
        print STDERR "corrected today in locale iso = $iso  today in seconds = " . $self->{today} . "\n";

        $iso = du_ConvertLocaleISOToGMT($iso, $uinfos);
        my $sec = du_ISOToSecond($iso);
        print STDERR "corrected today in GMT iso = $iso  today in seconds = $sec\n";

    }

    # --------------------------------------------------------
    # Get start date, from params or session or default today
    # We work in user locale
    # --------------------------------------------------------
    if (($values->{day} > 0) && ($values->{month} > 0) && ($values->{year} > 0)) {
        print STDERR "Start date defined on context\n" if ($debug);
        $params->{start_seconds} = strftime("%s", 0, 0, 0, $values->{day}, $values->{month} - 1, $values->{year} - 1900);
    }
    elsif (exists $session->{planning}->{args}) {
        print STDERR "Start date defined on session\n" if ($debug);
        $params->{start_seconds} = strftime("%s", 0, 0, 0, $session->{planning}->{args}->{day}, $session->{planning}->{args}->{month} - 1, $session->{planning}->{args}->{year} - 1900);
    }
    else {
        print STDERR "Start date not defined, get today\n" if ($debug);
        $params->{start_seconds} = $self->{today};
    }

    # --------------------------------------------------------
    # Get type of view from context if defined.
    # Overload pref choice
    # --------------------------------------------------------
	if ($values->{view_type}) {
		if ($values->{view_type} eq 'month') {
			$params->{view_type} = 'month';
		}
		else {
			$params->{view_type} = 'week';
		}
	}
    if ($params->{view_type} eq 'month') {
        if ($values->{nb_week} > 0) {
            $params->{nb_week} = $values->{nb_week};
        }
        else {
            $params->{nb_week} = 5;
        }
    }
    else {
        $params->{nb_week}     = 1;
        $params->{show_global} = 0;
    }
    $params->{nb_week} = 5 if ($params->{nb_week} < 1);
    print STDERR "nb_week = " . $params->{nb_week} . "\n" if ($debug);

    # --------------------------------------------------------
    # Next or previous week
    # --------------------------------------------------------
    if ($values->{next_week} == 1) {
        print STDERR "Next week defined\n" if ($debug);
        $params->{start_seconds} += 86400 * 7;

    }
    elsif ($values->{previous_week} == 1) {
        print STDERR "Previous week defined\n" if ($debug);
        $params->{start_seconds} -= 86400 * 7;
    }

    # --------------------------------------------------------
    # Next or previous month
    # --------------------------------------------------------
    if ($values->{next_month} == 1) {
        print STDERR "Next month defined\n" if ($debug);
        $params->{start_seconds} += 86400 * 30;

    }
    elsif ($values->{previous_month} == 1) {
        print STDERR "Previous month defined\n" if ($debug);
        $params->{start_seconds} -= 86400 * 30;
    }

    # --------------------------------------------------------
    # Next or previous step
    # --------------------------------------------------------
    if ($values->{next_step} == 1) {
        print STDERR "Next step defined\n" if ($debug);
        $params->{start_seconds} += 86400 * $params->{nb_week} * 7;

    }
    elsif ($values->{previous_step} == 1) {
        print STDERR "Previous step defined\n" if ($debug);
        $params->{start_seconds} -= 86400 * $params->{nb_week} * 7;
    }

    # --------------------------------------------------------
    # Verify calculus and limits
    # --------------------------------------------------------
    if ($params->{start_seconds} <= 0) {
        $params->{start_seconds} = 86400 * 7;
    }
    if ($debug) {
        $iso = du_SecondToISO($params->{start_seconds});
        print STDERR "Start date iso = $iso\n";
    }

    # --------------------------------------------------------
    # Set params dates from start_seconds
    # --------------------------------------------------------
    print STDERR "IsMondayFirst = " . $uinfos->IsMondayFirst() . "\n" if ($debug);

    #Round start_date on the first day of week
    $params->{start_seconds} -= 86400 * (strftime($uinfos->IsMondayFirst() ? "%u" : "%w", gmtime($params->{start_seconds})) - ($uinfos->IsMondayFirst() ? 1 : 0));
    $params->{startDate} = du_SecondToISO($params->{start_seconds});

    $params->{stop_seconds} = $params->{start_seconds} + $params->{nb_week} * 604800 - 1;

    #Round stop_date on the last day of week
    $params->{stop_seconds} += 86400 * (7 - strftime($uinfos->IsMondayFirst() ? "%u" : "%w", gmtime($params->{stop_seconds})) - ($uinfos->IsMondayFirst() ? 0 : 1));
    $params->{stopDate} = du_SecondToISO($params->{stop_seconds});

    ($params->{start_year}, $params->{start_month}, $params->{start_day}) = ($params->{startDate} =~ /^(\d{4})-(\d{2})-(\d{2})?/);
    if ($debug) {
        $iso = du_SecondToISO($params->{start_seconds});
        print STDERR "Rounded start date iso = $iso\n";
        $iso = du_SecondToISO($params->{stop_seconds});
        print STDERR "Stop date iso = $iso\n";
    }

    # --------------------------------------------------------
    #  Save current values in session.
    # --------------------------------------------------------
    $session->{planning}->{args}->{day}   = $params->{start_day};
    $session->{planning}->{args}->{month} = $params->{start_month};
    $session->{planning}->{args}->{year}  = $params->{start_year};

    print STDERR "params = " . Dumper($params) if ($debug);
}

# ============================================================================
# Private sub _RoundDuration: returns a rounded value of duration according
# to the maximum value it can get.
# ============================================================================
sub _RoundDuration {
    my $duration = shift;
    my $max      = shift;

    $duration = floor($duration * $max);
    $duration = $max if ($duration > $max);
    $duration = floor($max / 10) if (($duration > 0) && ($duration < (floor($max / 10))));

    return $duration;
}

# ============================================================================
# private sub _GetLoadFromUnit return the multiplier to obtain minutes from
# a load unit that can be 1 (minutes), 2 (hours) or 3 (days)
# ============================================================================
sub _GetLoadFromUnit {
    my ($self, $unit, $time_per_day) = @_;

    return (0, 1, 60, $time_per_day)[$unit];
}

# ============================================================================

sub _GetStartOfWeek {
    my ($self, $date, $monday_first) = @_;
    return $date - (86400 * (strftime($monday_first ? "%u" : "%w", gmtime($date)) - ($monday_first ? 1 : 0)));
}

sub _GetEndOfWeek {
    my ($self, $date, $monday_first) = @_;

    return $date + 86400 * (7 - strftime($monday_first ? "%u" : "%w", gmtime($date)) - ($monday_first ? 0 : 1));
}

sub _GetHolidays {
    my ($self, $context, $start, $end) = @_;

    my $config = $context->GetConfig();

    my $holidays = $config->GetHolidays();
    print STDERR "holidays = " . Dumper($holidays) . "\n" if ($debug);
    my @list;
    my @start = gmtime($start);
    my @end   = gmtime($end);

    for my $fixed (@{ $holidays->{fixed} }) {
        my $date;
        my $year = $start[5];

        $date = strftime("%s", 0, 0, 0, $fixed->{day}, $fixed->{month} - 1, $year);
        $date = strftime("%s", 0, 0, 0, $fixed->{day}, $fixed->{month} - 1, ++$year) if ($date < $start);
        while ($date <= $end) {
            push @list, $date;
            $date = strftime("%s", 0, 0, 0, $fixed->{day}, $fixed->{month} - 1, ++$year);
        }
    }

    my %cache;
    for my $moving (@{ $holidays->{moving} }) {
        my $base   = $moving->{base};
        my $offset = $moving->{offset};
        my ($sec, $day, $month);
        my $year = $start[5];

        # Compute base date for each year of the period
        my $base_date;
        for (my $y = $year ; $y <= $end[5] ; $y++) {
            if (!exists $cache{"$base-$y"}) {
                ($base_date) = ParseRecur("*" . ($y + 1900) . ":0:0:0:0:0:0*$base");
                $cache{"$base-$y"} = UnixDate($base_date, "%o");
            }
        }

        # Compute each occurrence for the period
        my $date;
        my @time = gmtime($cache{"$base-$year"} + ($offset * 24 * 60 * 60));
        $date = strftime("%s", 0, 0, 0, $time[3], $time[4], $year);
        while ($date <= $end) {
            push @list, $date;
            $year++;
            my @time = gmtime($cache{"$base-$year"} + ($offset * 24 * 60 * 60));
            $date = strftime("%s", 0, 0, 0, $time[3], $time[4], $year);
        }
    }

    my @sorted_list = sort @list;
    return \@sorted_list;
}

# ============================================================================

=head2 RevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

	Remove user data in database when a user is revoked of a group.

	This function is called when :
	- a user is revoked of a group
	- a team is revoked of a group for each team member not namly
	  invited in group
	- a user is revoked of a team  for each group where the team is
	  member and the user not namly invited in group

=cut

# ============================================================================
sub RevokeUserFromGroup {
    my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;

    my $dbh = $config->GetDBH();
    my $sql = "DELETE FROM planning_user_order WHERE rowid in (select planning_user_order.rowid from planning_user_order,planning_pref where user_id=$user_id and planning_id=planning_pref.rowid and planning_pref.group_id=$group_id)";
    ExecSQL($dbh, $sql);
}

# ============================================================================
# DeleteGroupData : method to destroy record in database for one group or user
#
# ============================================================================
sub DeleteGroupData {
    my ($self, $config, $group_id) = @_;

    my $dbh = $config->GetDBH();
    my $sql = "DELETE FROM planning_pref WHERE group_id=$group_id";
    ExecSQL($dbh, $sql);
}

# ============================================================================
1;
__END__
