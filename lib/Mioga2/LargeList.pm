# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
# ============================================================================
#
# Description:

=head1 NAME

Mioga2::LargeList - package used for building large list with indices and
                    search form.

                    see Mioga2::SimpleLargeList for more simple large lists

=head1 DESCRIPTION

=cut

package Mioga2::LargeList;
use strict;

use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Data::Dumper;
use POSIX qw(ceil);
use String::Checker;

# ============================================================================

=head2 new

    new LargeList($self, $context, $list_name);

    Create a new LargeList.

    Required parameters are :

      - context             : The current Mioga::RequestContext.
      
      - list_name           : Name of the list
        
=cut

# ============================================================================

sub new {
    my ($class, $context, $list_name) = @_;
    my $self = {};
    bless($self, $class);

    my $config = $context->GetConfig();

    Mioga2::LargeList::CheckArgs($context);

    $self->{list_name} = $list_name;

    if ($self->Initialize($context)) {
        return $self;
    }
    return undef;
}

# ============================================================================
#
# Initialize
#
#    $self->Initialize($self, $context);
#
#    Initialize LargeList parameters.
#
#    Required parameters are :
#
#      - default_sort_field  : Name of the field on which the list is sorted by default.
#
#      - max_index_size      : The number of pages displayed in index.
#                              If the number of page is greater than max_index_size,
#                              the symbols << and >> appears before and after the first
#                              and the last displayed page, allowing to acces to the
#                              previous and the next pages.
#
#      - nb_elem_per_page    : Number of elements per page.
#
#    The following method set default values.
#
#    Initialize MUST be overloaded and MUST defined list_name.
#
# ============================================================================

sub Initialize {
    my ($self, $context) = @_;

    my $config = $context->GetConfig();

    $self->{max_index_size}   = 10;
    $self->{nb_elem_per_page} = $config->GetLargeListDefaultLPP();
}

# ============================================================================
#
# GetNumberOfElems
#
#    $self->GetNumberOfElems($context);
#
#    Return the number of element to display.
#    This method MUST be overloaded.
#
#    This method is used to process page index.
#
# ============================================================================

sub GetNumberOfElems {
}

# ============================================================================
#
# GetNumberOfElemsMatching
#
#    $self->GetNumberOfElems($context, $pattern);
#
#    Return the number of element matching given patterns.
#    This method MUST be overloaded.
#
#    This method is used by SearchElem to process page index.
#
#    See SearchElem for more details on $patterns
#
# ============================================================================
sub GetNumberOfElemsMatching {
}

# ============================================================================
#
# GetElemsMatching
#
#    $self->GetElemsMatching($context, $pattern, $offset, $sort_field_name, $asc);
#
#    Return <nb_elem_per_page> elements begining to the <offset>th and matching given patterns.
#
#    Elements must be sorted on the field $sort_field_name.
#    If $asc = 0 elements are sorted descending, otherwise elements are sorted ascending.
#
#    This method MUST be overloaded.
#
#    See SearchElem for description of $pattern
#
# ============================================================================
sub GetElemsMatching {
}

# ============================================================================
#
# GetElemsList
#
#    $self->GetElemsList($context, $offset, $sort_field_name, $asc);
#
#    Return <nb_elem_per_page> elements begining to the <offset>th
#
#    Elements must be sorted on the field $sort_field_name.
#    If $asc = 0 elements are sorted descending, otherwise elements are sorted ascending.
#
#    This method MUST be overloaded.
#
# ============================================================================
sub GetElemsList {
}

# ============================================================================
#
# GetElem
#
#    $self->GetElem($context, $rowid);
#
#    Return a hash containing information about element $rowid
#
# ============================================================================
sub GetElem {
}

# ============================================================================
#
# DeleteElem
#
#    $self->DeleteElem($context, $rowid);
#
#    Delete the element with id $rowid.
#
#    This method MUST be overloaded.
#
#    This method is called after the confirmation page if askConfirmForDelete
#    is set.
#
# ============================================================================
sub DeleteElem {
}

#
#
#
#

# ============================================================================

=head2 DisplayList

    $self->DisplayList($context);

    return XML string describing the large list


    Required arguments in URL ($context->{args}):
    page : the current page is number

    Optional arguments in URL :
    sort_by : the name of the current sort field.
    nb_elem_per_page : the name of the current sort field.

=head3 Generated XML

   <!-- List of page (index) -->
   <LargeListIndex size="number of page" current_index="Number of currently selected page" max_size="Maximal size of index">
	
       <index>Index number</index>
       <index>Index number</index>
       ...
			
   </LargeListIndex>

   <!-- List content description -->

   <List nb_elem_per_page="number of element per page" name="list name"
	     cur_sort_field="Current sort field name" |;
	     cur_sort_asc="1 = sort ascending, 0 = sort descending">

        <!-- For each row -->
		<Row>
	        <$field_name>Field Value</$field_name>
	        <$field_name>Field Value</$field_name>
			...
 	    </Row>
        ...

   </List>

=cut

# ==========================================================================='

sub DisplayList {
    my ($self, $context) = @_;

    my $session = $context->GetSession();

    $self->_cleanRemoveCaches($context);

    $self->_initCurPage($context, 'list');

    my $nb_of_elem = $self->GetNumberOfElems($context);

    my $xml = $self->_GeneratePageIndex($context, $nb_of_elem);

    my $cur_page = $self->{cur_page};

    $self->_InitSortField($context);

    my $asc             = $session->{LargeList}->{ $self->{list_name} }->{cur_sort_asc};
    my $sort_field_name = $session->{LargeList}->{ $self->{list_name} }->{cur_sort_field};
    my $list = $self->GetElemsList($context, $cur_page * $self->{nb_elem_per_page}, $sort_field_name, $asc);

    $xml .= $self->_GenList($context, $list, $nb_of_elem);

    return $xml;
}

# ============================================================================

=head2 RemoveElem

    $self->RemoveElem($context, $user_xml_data);

    Remove an element in the list.

    Required arguments in URL ($context->{args}):
    select_delete_<rowid> : the rowid of elements to remove.
    
    Optional arguments in URL :
    sort_by : the name of the current sort field.

=head3 Generated XML


	<AskConfirmForDelete>

        <miogacontext>See Mioga2::RequestContext</miogacontext>

        <referer>URL of calling application</referer>

        <name>List name</name>

        <!-- List content description -->

        <List nb_elem_per_page="number of element per page" name="list name"
	          cur_sort_field="Current sort field name" |;
              cur_sort_asc="1 = sort ascending, 0 = sort descending">

           <!-- For each row -->
	       <Row>
	          <$field_name>Field Value</$field_name>
	          <$field_name>Field Value</$field_name>
  			  ...
 	       </Row>
           ...

        </List>

	</AskConfirmForDelete>


    <!-- For success message -->
    <SuccessMessage>

        <miogacontext>See Mioga2::RequestContext</miogacontext>

        <referer>URL of calling application</referer>

		<name>List name</name>
		<action>remove</action>
		<nb_items>Number of selected item</nb_items>

	</SuccessMessage>
    

=cut

# ============================================================================

sub RemoveElem {
    my ($self, $context, $user_xml_data) = @_;

    my $session = $context->GetSession();
    my $config  = $context->GetConfig();

    my $rowid_list   = [];
    my $list         = [];
    my $is_confirmed = 0;

    foreach my $key (keys %{ $context->{args} }) {
        $key = st_URIUnescape($key);
        next unless $key =~ /select_delete_(.*)/;
        my $rowid = $1;

        push @$rowid_list, $rowid;

        my $elem = $self->GetElem($context, $rowid);

        push @$list, $elem if defined $elem;
    }

    if (@$list) {
        $is_confirmed = 0;
        delete $session->{LargeList}->{ $self->{list_name} }->{rowid_to_remove};
        delete $session->{LargeList}->{ $self->{list_name} }->{elem_list};
    }
    else {
        $is_confirmed = 1;
    }

    if (exists $session->{LargeList}->{ $self->{list_name} }->{rowid_to_remove}) {
        $rowid_list   = $session->{LargeList}->{ $self->{list_name} }->{rowid_to_remove};
        $is_confirmed = 1;
    }

    if (!$config->AskConfirmForDelete()) {
        $is_confirmed = 1;
    }

    my $xml;

    if ($is_confirmed) {

        foreach my $rowid (@$rowid_list) {
            $self->DeleteElem($context, $rowid);
        }

        my $referer;
        if (exists $session->{LargeList}->{ $self->{list_name} }->{referer}) {
            $referer = $session->{LargeList}->{ $self->{list_name} }->{referer};
        }
        elsif (exists $context->{args}->{referer}) {
            $referer = $context->{args}->{referer};
        }
        else {
            $referer = $context->GetReferer();
        }

        $referer =~ s/select_delete_(\w+)&?//g;

        $xml = "<SuccessMessage>";
        $xml .= $context->GetXML();
        $xml .= "<name>" . st_FormatXMLString($self->{list_name}) . "</name>";
        $xml .= "<action>remove</action>";
        $xml .= "<nb_items>" . @$rowid_list . "</nb_items>";
        $xml .= "<referer>" . st_FormatXMLString($referer) . "</referer>";
        $xml .= $user_xml_data if defined $user_xml_data;
        $xml .= "</SuccessMessage>";

        delete $session->{LargeList}->{ $self->{list_name} }->{referer};
        delete $session->{LargeList}->{ $self->{list_name} }->{rowid_to_remove};
        delete $session->{LargeList}->{ $self->{list_name} }->{elem_list};

        $context->{referer} = $referer;
        $xml = undef;
    }
    else {
        my $referer;
        if (exists $context->{args}->{referer}) {
            $referer = $context->{args}->{referer};
        }
        else {
            $referer = $context->GetReferer();
        }

        $referer =~ s/select_delete_(\w+)&?//g;
        $session->{LargeList}->{ $self->{list_name} }->{referer} = $referer;

        $xml = "<AskConfirmForDelete>";
        $xml .= $context->GetXML;
        $xml .= "<referer>" . st_FormatXMLString($referer) . "</referer>";
        $xml .= "<name>" . st_FormatXMLString($self->{list_name}) . "</name>";

        $xml .= $self->_GenList($context, $list, scalar(@$list));
        $xml .= $user_xml_data if defined $user_xml_data;
        $xml .= "</AskConfirmForDelete>";

        $session->{LargeList}->{ $self->{list_name} }->{rowid_to_remove} = $rowid_list;
        $session->{LargeList}->{ $self->{list_name} }->{elem_list}       = $list;
    }

    return $xml;
}

# ============================================================================

=head2 SearchElem

    $self->SearchElem($context, $params);

    Search elements in the list.
    
    $params is a hashref like :
    { field_name => [list of regexp], ...  }

    Required arguments in URL ($context->{args}):
    page : the current page's number

    Optional arguments in URL :
    sort_by : the name of the current sort field.

    regexp are simple regular expression
    (* for zero or more characters, ? for zero or one character)
    

    Example :
    
    { firstname => ["Laurent", "Gilles"],
      city      => ["Orsay"] }

    stand for : Elements with name equal to Laurent or Gilles
                          and city equal to Orsay.
   

=head3 Generated XML 

    See DisplayList

=cut

# ==========================================================================='

sub SearchElem {
    my ($self, $context, $params) = @_;

    my $session = $context->GetSession();

    $self->_cleanRemoveCaches($context);
    $self->_initCurPage($context, 'search');

    my $nb_of_elem = $self->GetNumberOfElemsMatching($context, $params);

    my $xml = "";
    $xml .= $self->_GeneratePageIndex($context, $nb_of_elem);

    $self->_InitSortField($context);

    my $asc             = $session->{LargeList}->{ $self->{list_name} }->{cur_sort_asc};
    my $sort_field_name = $session->{LargeList}->{ $self->{list_name} }->{cur_sort_field};

    my $cur_page = $self->{cur_page};
    my $list = $self->GetElemsMatching($context, $params, $cur_page * $self->{nb_elem_per_page}, $sort_field_name, $asc);

    $xml .= $self->_GenList($context, $list, $nb_of_elem);

    return $xml;
}

# ============================================================================
#
# GetPostgresRegExp($regexp, $begin_with)
#
#    $self->GetPostgresRegExp($regexp, $begin_with);
#
#    convert SearchElem  reg exp to PostgreSQL reg exp
#
#    if $begin_with == 1, add a ^ before all regexp
#
# ============================================================================

sub GetPostgresRegExp {
    my ($self, $regexp, $begin_with) = @_;

    my $res;

    foreach my $key (keys %$regexp) {
        foreach my $rexp (@{ $regexp->{$key} }) {

            my $re = $rexp;
            if ($re eq '') {
                $re = '+';
            }
            else {
                $re = st_FormatPostgreSQLRegExpString($re);

                $re =~ s/\\\*/.*/g;
                $re =~ s/\\\\/\\/g;
            }

            if ($begin_with == 1) {
                $re = "^$re";
            }

            push @{ $res->{$key} }, $re;
        }
    }

    return $res;
}

###
### Private methods.
###

# ============================================================================
#
# _InitSortField
#
#    $self->_InitSortField($context);
#
#    Initialize cur_sort_field and cur_sort_asc.
#
# ============================================================================

sub _InitSortField {
    my ($self, $context) = @_;

    my $session = $context->GetSession();

    my $asc;
    my $sort_field_name;

    my $listname = $self->{list_name};

    if (exists $context->{args}->{ "sortby_" . $listname }) {
        $sort_field_name = $context->{args}->{ "sortby_" . $listname };
        $sort_field_name =~ /sortby_(.*)_([^_]+)/;

        $sort_field_name = $1;
        if ($2 eq 'asc') {
            $asc = 1;
        }
        else {
            $asc = 0;
        }
    }

    if (!defined $sort_field_name) {
        if (exists $session->{LargeList}->{ $self->{list_name} }->{cur_sort_field}) {
            $sort_field_name = $session->{LargeList}->{ $self->{list_name} }->{cur_sort_field};
            $asc             = $session->{LargeList}->{ $self->{list_name} }->{cur_sort_asc};
        }
        else {
            $sort_field_name = $self->{default_sort_field};
            $asc             = 1;
        }
    }

    $session->{LargeList}->{ $self->{list_name} }->{cur_sort_asc}   = $asc;
    $session->{LargeList}->{ $self->{list_name} }->{cur_sort_field} = $sort_field_name;
}

# ============================================================================
#
# _GenList
#
#    $self->_GenList($context, $list);
#
#    return XML string describing a list of hash
#
# ============================================================================
sub _GenList {
    my ($self, $context, $list) = @_;

    my $session = $context->GetSession();

    my $xml = qq|<List nb_elem_per_page="$self->{nb_elem_per_page}" name="| . st_FormatXMLString($self->{list_name}) . qq|" |;
    $xml .= qq|cur_sort_field="$session->{LargeList}->{$self->{list_name}}->{cur_sort_field}" |;
    $xml .= qq|cur_sort_asc="$session->{LargeList}->{$self->{list_name}}->{cur_sort_asc}">|;

    foreach my $elem (@$list) {
        $xml .= qq|<Row>|;

        foreach my $key (keys %$elem) {
            if (defined $elem->{$key}) {
                $xml .= "<$key>";

                $xml .= st_FormatXMLString($elem->{$key});

                $xml .= "</$key>";
            }
        }

        $xml .= "</Row>";
    }

    $xml .= "</List>";
    return $xml;
}

# ============================================================================
#
# _GeneratePageIndex
#
#    $self->_GeneratePageIndex($context, $nb_of_elem);
#
#    return XML string describing the current index.
#
# ============================================================================

sub _GeneratePageIndex {
    my ($self, $context, $nb_of_elem) = @_;

    my $session = $context->GetSession();

    my $nb_elem_per_page;
    if (exists $context->{args}->{ "nb_elem_per_page_" . $self->{list_name} }) {
        my $nb_elem = $context->{args}->{ "nb_elem_per_page_" . $self->{list_name} };

        my $res = String::Checker::checkstring($nb_elem, [ 'disallow_empty', 'want_int' ]);

        if (defined $res and @$res) {
            $nb_elem_per_page = $self->{nb_elem_per_page};
        }
        else {
            $nb_elem_per_page = $context->{args}->{ "nb_elem_per_page_" . $self->{list_name} };
        }
    }
    elsif (exists $session->{persistent}->{LargeList}->{ $self->{list_name} }->{nb_elem_per_page}) {
        $nb_elem_per_page = $session->{persistent}->{LargeList}->{ $self->{list_name} }->{nb_elem_per_page};
    }
    else {
        $nb_elem_per_page = $self->{nb_elem_per_page};
    }

    if ($nb_elem_per_page == 0) {
        $nb_elem_per_page = $self->{nb_elem_per_page};
    }

    $self->{nb_elem_per_page} = int($nb_elem_per_page);
    $session->{persistent}->{LargeList}->{ $self->{list_name} }->{nb_elem_per_page} = int($nb_elem_per_page);

    my $cur_page = $context->{args}->{page} - 1;

    while ($cur_page and $cur_page * $nb_elem_per_page >= $nb_of_elem) {
        $cur_page--;
    }

    $self->{cur_page} = $cur_page;

    my $nb_page = ceil($nb_of_elem / $nb_elem_per_page);
    my $xml = qq|<LargeListIndex size="$nb_page" current_index="| . ($cur_page + 1) . qq|" max_size="$self->{max_index_size}">|;

    if ($nb_of_elem > $nb_elem_per_page) {

        my $start = $cur_page - ($self->{max_index_size} / 2);
        if ($start < 0) {
            $start = 0;
        }

        for (my $i = $start ; $i < ($start + $self->{max_index_size}) and $i * $nb_elem_per_page < $nb_of_elem ; $i++) {

            $xml .= "<index>" . ($i + 1) . "</index>";

        }
    }

    $xml .= "</LargeListIndex>";

    return $xml;
}

# ============================================================================
#
# _cleanRemoveCaches
#
#    $self->_cleanRemoveCaches($context);
#
#    Clean all remove caches
#
# ============================================================================

sub _cleanRemoveCaches {
    my ($self, $context) = @_;

    my $session = $context->GetSession();

    foreach my $cachedList (keys %{ $session->{LargeList} }) {
        delete $session->{LargeList}->{$cachedList}->{rowid_to_remove};
        delete $session->{LargeList}->{$cachedList}->{elem_list};
    }
}

# ============================================================================
#
# _initCurPage
#
#    $self->_initCurPage($context);
#
#    Initialize $context->{args}->{page}
#
# ============================================================================

sub _initCurPage {
    my ($self, $context, $type) = @_;

    my $session = $context->GetSession();

    my $cur_page;

    if (exists $context->{args}->{ "pagenb_" . $self->{list_name} }
        and $context->{args}->{ "pagenb_" . $self->{list_name} } ne '')
    {
        $cur_page = $context->{args}->{ "pagenb_" . $self->{list_name} };
    }

    if (defined $cur_page) {
        $session->{LargeList}->{ $self->{list_name} }->{"${type}_page"} = $cur_page;
        $context->{args}->{page} = $cur_page;
    }
    else {
        if ($type eq 'main') {
            delete $session->{LargeList}->{ $self->{list_name} }->{"search_page"};
        }

        if (exists $session->{LargeList}->{ $self->{list_name} }->{"${type}_page"}) {
            $context->{args}->{page} = $session->{LargeList}->{ $self->{list_name} }->{"${type}_page"};
        }
        else {
            $context->{args}->{page} = 1;
        }
    }

    if ($context->{args}->{page} <= 0) {
        $context->{args}->{page} = 1;
    }
}

#===============================================================================

=head2 CheckArgs

Check incoming arguments via ac_CheckArgs

=cut

#===============================================================================
sub CheckArgs {
    my ($context) = @_;

    #-------------------------------------------------------------------------------
    # Check incoming arguments
    #-------------------------------------------------------------------------------
    my @nbelems   = grep (/^nb_elem_per_page_.*/,    keys(%{ $context->{args} }));
    my @pagenb    = grep (/^pagenb_.*/,              keys(%{ $context->{args} }));
    my @sortby    = grep (/^sortby_.*/,              keys(%{ $context->{args} }));
    my @selsimple = grep (/^select_simple_rowid_.*/, keys(%{ $context->{args} }));
    my @selfunc   = grep (/^select_function_.*/,     keys(%{ $context->{args} }));
    my @change    = grep (/^select_change_.*/,       keys(%{ $context->{args} }));
    my @action    = grep (/^.*_act/,                 keys(%{ $context->{args} }));
    my @search    = grep (/^sll_search_.*/,          keys(%{ $context->{args} }));
    my @delete    = grep (/^select_delete_.*/,       keys(%{ $context->{args} }));
    my $chkparams = [ [ [ 'sll_adv_search_act', 'sll_delete_act' ], 'allow_empty' ], [ [ 'select_valid_act', 'select_modify_act', 'act_change_elem_per_page' ], 'allow_empty' ], [ ['page'], 'allow_empty', 'want_int' ], [ \@sortby, 'allow_empty' ], [ \@nbelems, 'allow_empty', 'want_int' ], [ \@pagenb, 'allow_empty', 'want_int' ], [ \@selsimple, 'allow_empty', 'want_int' ], [ \@selfunc, 'allow_empty' ], [ \@search, 'allow_empty' ], [ \@delete, 'allow_empty' ], [ \@change, 'allow_empty' ], [ \@action, 'disallow_empty' ], ];
    my ($values, $errors) = ac_CheckArgs($context, $chkparams);

    if (@$errors) {
        throw Mioga2::Exception::Application("LargeList::CheckArgs", "Wrong argument value." . Dumper $errors);
    }
}    # ----------  end of subroutine CheckArgs  ----------

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::SimpleLargeList

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
