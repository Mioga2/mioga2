# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: 

=head1 NAME
	
Session.pm: Mioga2 session management

=head1 DESCRIPTION

	All useful method for Mioga2 session manipulation

=head1 METHODS DESRIPTION

=cut
package Mioga2::Session;
use strict;

use Error qw(:try);
use POSIX qw(tmpnam);
use Storable;
use Benchmark::Timer;
use Data::Dumper;
use Mioga2::URI;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;
use Fcntl ':flock'; # import LOCK_* constants

my $NEW = 1;
my $MODIFIED = 2;
my $SYNCED = 4;
my $DELETED = 8;

my $debug=0;
my $timer=0;


# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ----------------------------------------------------------------------------

=head2 TIEHASH ($config, $sessionId, $tmp_dir, $uri)

=cut

# ----------------------------------------------------------------------------
sub TIEHASH {
	my $class = shift;
	my $config = shift;
	my $sessionId = shift;
	my $tmp_dir = shift;
	my $uri = shift;
	my $bench;

	print STDERR "Mioga2::Session::TIEHASH $$ \n" if ($debug  > 0);
	if($timer) {
		$bench = new Benchmark::Timer();
		$bench->start("Mioga2::Session $$ overall");
	}
	unless (-d $tmp_dir)
	{
		mkdir $tmp_dir, 0777;
	}

	my $self = {
		config      => $config,
		status       => $NEW,
		uri         => $uri,
		session_id => $sessionId,
		tmp_dir => $tmp_dir,
		data         => {},
	};
	bless $self, $class;

	
	## Don't allow applications to view the session of the others.
	## So, just load the applications session.
	
	my $key = "";
	if($uri->IsWebDav()) {
		$key = "dav";
	}
	elsif($uri->IsApplication()) {
		$key = $uri->GetAppIdent();
	}
	else {
		$key = "unknown-xml";
	}
	
	$self->{key} = $key;
	
	if ($self->test_retrieve_data($tmp_dir, $sessionId))
	{
		open(F_MIOGA_SESSION, ">>$tmp_dir/$sessionId.lock");
		flock(F_MIOGA_SESSION, LOCK_SH);

		my $data = $self->retrieve_data($tmp_dir, $sessionId);

		flock(F_MIOGA_SESSION, LOCK_UN);
		close(F_MIOGA_SESSION);

		if(exists $data->{$key}) {
			$self->{data} = $data->{$key};
		}

		if(exists $data->{__internal}) {
			$self->{data}->{__internal} = $data->{__internal};
		}

		if(exists $data->{persistent}->{$key}) {
			$self->{data}->{persistent} = $data->{persistent}->{$key};
		}
	}
	else {
		open(F_MIOGA_SESSION, ">>$tmp_dir/$sessionId.lock");
		flock(F_MIOGA_SESSION, LOCK_SH);

		my $persistent_data = $self->retrieve_persistent_data($tmp_dir, $sessionId);

		my $data = {persistent => $persistent_data};
		Storable::lock_store($data, "$tmp_dir/$sessionId");

		flock(F_MIOGA_SESSION, LOCK_UN);
		close(F_MIOGA_SESSION);

		if(exists $persistent_data->{$key}) {
			$self->{data}->{persistent} = $persistent_data->{$key};
		}
		else {
			$self->{data}->{persistent} = {};
		}
	}
	if($timer)  {
		$bench->stop();
		print STDERR $bench->report();
	}

	print STDERR "Mioga2::Session::TIEHASH " . Dumper $self->{data} if ($debug);
	return $self;
}

# ----------------------------------------------------------------------------

=head2 FETCH ($key)

=cut

# ----------------------------------------------------------------------------
sub FETCH {
	my $self = shift;
	my $key  = shift;
	print STDERR "Mioga2::Session::FETCH\n" if ($debug > 1);
        
	return $self->{data}->{$key};
}

# ----------------------------------------------------------------------------

=head2 STORE ($key, $value)

=cut

# ----------------------------------------------------------------------------
sub STORE {
	my $self  = shift;
	my $key   = shift;
	my $value = shift;
    
	print STDERR "Mioga2::Session::STORE\n" if ($debug > 1);
	$self->{data}->{$key} = $value;
    
	$self->{status} |= $MODIFIED;
    
	return $self->{data}->{$key};
}

# ----------------------------------------------------------------------------

=head2 EXISTS ($key)

=cut

# ----------------------------------------------------------------------------
sub EXISTS {
	my $self = shift;
	my $key  = shift;
	print STDERR "Mioga2::Session::EXISTS\n" if ($debug > 1);
    
	return exists $self->{data}->{$key};
}

# ----------------------------------------------------------------------------

=head2 DELETE ($key)

=cut

# ----------------------------------------------------------------------------
sub DELETE {
	my $self = shift;
	my $key  = shift;
	print STDERR "Mioga2::Session::DELETE\n" if ($debug > 1);
    	
	$self->{status} |= $MODIFIED;
    	
	delete $self->{data}->{$key};
}

# ----------------------------------------------------------------------------

=head2 CLEAR

=cut

# ----------------------------------------------------------------------------
sub CLEAR {
	my $self = shift;
	print STDERR "Mioga2::Session::CLEAR\n" if ($debug > 1);
    	
	$self->{data} = {};
}

# ----------------------------------------------------------------------------

=head2 FIRSTKEY ()

=cut

# ----------------------------------------------------------------------------
sub FIRSTKEY {
	my $self = shift;
	print STDERR "Mioga2::Session::FIRSTKEY\n" if ($debug > 1);
	    
	my $reset = keys %{$self->{data}};
	return each %{$self->{data}};
}

# ----------------------------------------------------------------------------

=head2 NEXTKEY ()

=cut

# ----------------------------------------------------------------------------
sub NEXTKEY {
	my $self = shift;
	print STDERR "Mioga2::Session::NEXTKEY\n" if ($debug > 1);
	
	return each %{$self->{data}};
}

# ----------------------------------------------------------------------------

=head2 DESTROY ()

=cut

# ----------------------------------------------------------------------------
sub DESTROY {
	my $self = shift;
	print STDERR "Mioga2::Session::DESTROY $$ \n" if ($debug > 0);
	print STDERR "Mioga2::Session::DESTROY" . Dumper $self->{data} if ($debug);
	
	$self->save;
}


# ----------------------------------------------------------------------------

=head2 save ()

=cut

# ----------------------------------------------------------------------------
sub save
{
	my ($self) = @_;
	print STDERR "Mioga2::Session::save  status = $self->{status}\n" if ($debug > 0);

	return unless (
		$self->{status} & $MODIFIED ||
		$self->{status} & $NEW ||
		$self->{status} & $DELETED
	);

	if ($self->{status} & $MODIFIED)
	{
		$self->store_data($self->{data}, $self->{tmp_dir}, $self->{session_id});
		$self->{status} &= ($self->{status} ^ $MODIFIED);
		$self->{status} |= $SYNCED;

		return;
	}

	if ($self->{status} & $NEW)
	{
		$self->store_data($self->{data}, $self->{tmp_dir}, $self->{session_id});
		$self->{status} &= ($self->{status} ^ $NEW);
		$self->{status} |= $SYNCED;
		$self->{status} &= ($self->{status} ^ $MODIFIED);

		return;
	}

}

# ----------------------------------------------------------------------------

=head2 store_data ($data, $path, $name)

=cut
# ----------------------------------------------------------------------------
sub store_data
{
	my ($self, $data, $path, $name) = @_;

	return if(exists $data->{dont_store_session} and $data->{dont_store_session});

 	open(F_MIOGA_SESSION, ">>$path/$name.lock");
 	flock(F_MIOGA_SESSION, LOCK_EX);

	my $curdata = {};
	if($self->test_retrieve_data($path, $name)) {
		$curdata = $self->retrieve_data($path, $name);
	}

	$curdata->{persistent}->{$self->{key}} = $data->{persistent};
	delete $data->{persistent};

	$curdata->{$self->{key}} = $data;

	if(exists $data->{__internal}) {
		$curdata->{__internal} = $data->{__internal};
		delete $data->{__internal};
	}

	Storable::lock_store($curdata, "$path/$name");

 	$self->store_persistent_data($curdata->{persistent}, $path, $name);

	flock(F_MIOGA_SESSION, LOCK_UN);
 	close(F_MIOGA_SESSION);

}

# ----------------------------------------------------------------------------

=head2 store_persistent_data ($data, $path, $session_id)

=cut
# ----------------------------------------------------------------------------
sub store_persistent_data
{
	my ($self, $data, $path, $session_id) = @_;
	print STDERR "Mioga2::Session::store_persistent_data  sessionId = $session_id\n" if ($debug > 0);

	my $file = tmpnam;

	Storable::store($data, $file);

	my $dbh = $self->{config}->GetDBH();
	try {

		BeginTransaction($dbh);

		my $loid = SelectSingle($dbh, "SELECT data ".
							   "FROM m_persistent_session ".
							   "WHERE session_id = '".st_FormatPostgreSQLString($session_id)."'");

		if(defined $loid) {
			$dbh->func($loid->{data}, 'lo_unlink');
		}

	
		ExecSQL($dbh, "DELETE FROM m_persistent_session ".
				      "WHERE session_id = '".st_FormatPostgreSQLString($session_id)."'");
	

		$loid = $dbh->func($file, 'lo_import');
		if(!defined $loid) {
			warn $dbh->errstr;
			warn $dbh->err;
			throw Mioga2::Exception::Simple("Mioga2::Session->store_persistent_data", 
											"Can't import persistent session"); 
		}

		ExecSQL($dbh, "INSERT INTO m_persistent_session (session_id, data) ".
		    	      "VALUES ('".st_FormatPostgreSQLString($session_id)."', $loid)");

		EndTransaction($dbh);

	}
	otherwise {
		my $err = shift;
		warn $err->{-text};

		RollbackTransaction($dbh);
	};

	unlink($file);

}

# ----------------------------------------------------------------------------

=head2 test_retrieve_data

=cut

# ----------------------------------------------------------------------------
sub test_retrieve_data
{
	my ($self, $path, $name) = @_;
	print STDERR "Mioga2::Session::test_retrieve  path = $path   name = $name\n" if ($debug > 0);

	return (-e "$path/$name");
}

# ----------------------------------------------------------------------------

=head2 retrieve_data

=cut

# ----------------------------------------------------------------------------
sub retrieve_data
{
	my ($self, $path, $name) = @_;
	print STDERR "Mioga2::Session::retrieve  path = $path   name = $name\n" if ($debug > 0);

	my $data = Storable::lock_retrieve("$path/$name");

	return $data;
}

# ----------------------------------------------------------------------------

=head2 retrieve_persistent_data

=cut

# ----------------------------------------------------------------------------
sub retrieve_persistent_data
{
	my ($self, $path, $sessionId) = @_;
	print STDERR "Mioga2::Session::retrieve_persistent_data  sessionId = $sessionId\n" if ($debug > 0);

	my $file = tmpnam;

	my $dbh = $self->{config}->GetDBH();
	
	my $data = {};
	try {
		BeginTransaction($dbh);
		
		my $loid = SelectSingle($dbh, "SELECT data ".
								      "FROM m_persistent_session ".
								      "WHERE session_id = '".st_FormatPostgreSQLString($sessionId)."'");
		
		if(! defined $loid) {
			return;
		}

		$dbh->func($loid->{data}, $file, 'lo_export');
			
		if(-e $file) {
			$data = Storable::retrieve("$file");
		}

		EndTransaction($dbh);
	}
	otherwise {
		my $err = shift;

		RollbackTransaction($dbh);
		warn $err->{-text};
	};

	if(-e $file) {
		unlink($file);		
	}

	return $data;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
