# ============================================================================
# MiogaII Project (C) 2003-2007 The MiogaII Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
ExternalMioga.pm: Access class to External Mioga.

=head1 DESCRIPTION

This module permits to access to the External Mioga declaration
and to crypt and sign data for this Mioga.

=head1 METHODS DESRIPTION

=cut

package Mioga2::ExternalMioga;
use strict;

use Locale::TextDomain::UTF8 'externalmioga';

use Error qw(:try);
use Data::Dumper;
use Mioga2;
use Mioga2::Crypto;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Mioga2::Exception::ExternalMioga;
use LWP::UserAgent;
use HTTP::Request::Common;
use POSIX qw(tmpnam);

my $debug = 0;

my %miogaConfig;

# ============================================================================

=head2 new ($config, %params)

	Create the Mioga2::ExternalMioga object from Mioga2::Config 
	and params like :

       server   => $server_name the server name.
	or user_rowid => $user_rowid the server of user with given rowid
	or keyident   => $key_ident  the server with given key ident
	or rowid      => $rowid      the server rowid

=cut

# ============================================================================

sub new {
	my ($class, $config, %params) = @_;

	my $self = {config => $config};

	bless($self, $class);

	if(exists $params{rowid}) {
		$self->InitializeFromRowid($params{rowid});
	}
	elsif(exists $params{user_rowid}) {
		$self->InitializeFromUserRowid($params{user_rowid});
	}
	elsif(exists $params{keyident}) {
		$self->InitializeFromKeyIdent($params{keyident});
	}
	elsif(exists $params{server}) {
		$self->InitializeFromServerName($params{server});
	}

	$self->{crypto} = new Mioga2::Crypto($self->{config});

	return $self;
}



# ============================================================================

=head2 GetServer ()

	Return the External Mioga server name

=cut

# ============================================================================

sub GetServer {
	my ($self) = @_;

	return $self->{values}->{server};
}

# ============================================================================

=head2 GetRowid ()

	Return the External Mioga rowid 
=cut

# ============================================================================

sub GetRowid {
	my ($self) = @_;

	return $self->{values}->{rowid};
}

# ============================================================================

=head2 GetStatus ()

	Return the External Mioga status

=cut

# ============================================================================

sub GetStatus {
	my ($self) = @_;


	my $dbh = $self->{config}->GetDBH();

	my $status = SelectSingle($dbh, "SELECT ident FROM m_external_mioga_status 
	                                 WHERE rowid=$self->{values}->{status_id}");
	
	if(!defined $status) {
		throw Mioga2::Exception::ExternalMioga("Mioga2::ExternalMioga::GetStatus", 
        __x("Unknown status id {status_id}", status_id => $self->{values}->{status_id}));
	}

	return $status->{ident};
}



# ============================================================================

=head2 SendRequest (%params)

	Send a request to an external mioga.
 
    Params are : 
      - appliction => the appliction name
      - method => the method name
      - args => the request arguments (hash like $context->{args})
      
=cut

# ============================================================================

sub SendRequest {
	my ($self, %params) = @_;

	if($self->GetStatus() ne 'active') {
		throw Mioga2::Exception::ExternalMioga("Mioga2::ExternalMioga::SendRequest", 
											   __"External Mioga disabled");
	}

	my $xml = '<?xml version="1.0"?>';
	$xml .= "<Mioga2Request>";
	$xml .= qq|<Mioga version="$Mioga2::VERSION"/>|;
	$xml .= "<Application>$params{application}</Application>";
	$xml .= "<Method>$params{method}</Method>";

	foreach my $key (keys %{$params{args}}) {
		if(defined $params{args}->{$key}) {
			$xml .= qq|<Arg name="$key">|.st_FormatXMLString($params{args}->{$key})."</Arg>";
		}
		else {
			$xml .= qq|<Arg name="$key"/>|;
		}
	}

	$xml .= "</Mioga2Request>";

	my $data = $self->CryptAndSignData($xml);

	my $tmpfile = tmpnam;

	open(F_TMPFILE, ">$tmpfile")
		or die "Can't open $tmpfile : $!";

	print F_TMPFILE $data;

	close(F_TMPFILE);

	# Create a request
	my $req = POST $self->{values}->{server}."/sxml", Content_Type => 'form-data',
	                                          Content      => [ date   => ["$tmpfile"] ];


	my $ua = LWP::UserAgent->new;
	my $res = $ua->request($req);

	unlink($tmpfile);
	
	if($res->is_error) {
		throw Mioga2::Exception::ExternalMioga("Mioga2::ExternalMioga::SendRequest", $res->message);
	}

	
	my $result;
	try {
		($result) = $self->{crypto}->VerifyDecryptData($res->content);
	}

	catch Mioga2::Exception::Crypto with {
		my $err = shift;

		warn $res->content if $debug;
		throw Mioga2::Exception::ExternalMioga("Mioga2::ExternalMioga::SendRequest", $err->as_string);		
	};
	
	return $result;

}



# ============================================================================

=head2 CryptAndSignData ($data)

	Crypt and sign data that will be sent to this external mioga

=cut

# ============================================================================

sub CryptAndSignData {
	my ($self, $data) = @_;

	if($self->GetStatus() ne 'active') {
		throw Mioga2::Exception::ExternalMioga("Mioga2::ExternalMioga::CryptAndSignData", 
											   __"External Mioga disabled");
	}

	return $self->{crypto}->CryptAndSignDataForServer($self->{values}->{server}, $data);
}


#===============================================================================

=head2 GetList

Get list of declared external Miogas within the instance.

=cut

#===============================================================================
sub GetList {
	my ($self) = @_;

	my $list = SelectMultiple ($self->{config}->GetDBH (), "SELECT m_external_mioga.*, m_external_mioga_status.ident as status, (SELECT count(*) FROM m_user_base WHERE external_mioga_id = m_external_mioga.rowid) as nb_user FROM m_external_mioga, m_external_mioga_status WHERE m_external_mioga.status_id = m_external_mioga_status.rowid AND m_external_mioga.mioga_id = " . $self->{config}->GetMiogaId () . ";");

	return ($list);
}	# ----------  end of subroutine GetList  ----------



# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 InitializeFromRowid ($rowid)

	Initialize the ExternalMioga object from external mioga rowid.

=cut

# ============================================================================

sub InitializeFromRowid {
	my ($self, $rowid) = @_;
	
	my $dbh = $self->{config}->GetDBH();
	
	my $res = SelectSingle($dbh, "SELECT m_external_mioga.*, m_external_mioga_status.ident AS status, (SELECT count(*) FROM m_user_base WHERE external_mioga_id = m_external_mioga.rowid) AS nb_users FROM m_external_mioga, m_external_mioga_status WHERE m_external_mioga_status.rowid = m_external_mioga.status_id AND m_external_mioga.rowid = $rowid");

	if(!defined $res) {
		throw Mioga2::Exception::ExternalMioga("Mioga2::ExternalMioga::InitializeFromRowid",
        __x("Can't find external mioga declaration with rowid {rowid}", rowid => $rowid));
	}


	$self->{values} = $res;
}



# ============================================================================

=head2 InitializeFromUserRowid ($rowid)

	Initialize the ExternalMioga object from external user rowid.

=cut

# ============================================================================

sub InitializeFromUserRowid {
	my ($self, $rowid) = @_;
	
	my $dbh = $self->{config}->GetDBH();
	
	my $res = SelectSingle($dbh, "SELECT m_external_mioga.* FROM m_external_mioga, m_user_base ".
						         "WHERE m_external_mioga.rowid = m_user_base.external_mioga_id AND m_user_base.rowid = $rowid ");

	if(!defined $res) {
		throw Mioga2::Exception::ExternalMioga("Mioga2::ExternalMioga::InitializeFromRowid",
        __x("Can't find external mioga declaration of user with rowid {rowid}", rowid => $rowid));
	}

	$self->{values} = $res;
}



# ============================================================================

=head2 InitializeFromKeyIdent ($keyident)

	Initialize the ExternalMioga object from external mioga public key ident.

=cut

# ============================================================================

sub InitializeFromKeyIdent {
	my ($self, $keyident) = @_;
	
	$keyident =~ /\((.*)\)/ ;
	my $server = $1;
	
	$self->InitializeFromServerName($server);
}



# ============================================================================

=head2 InitializeFromServerName ($server_name)

	Initialize the ExternalMioga object from external mioga server name.

=cut

# ============================================================================

sub InitializeFromServerName {
	my ($self, $server) = @_;

	my $dbh = $self->{config}->GetDBH();
	my $res = SelectSingle($dbh, "SELECT * FROM m_external_mioga WHERE server = '$server'");

	if(!defined $res) {
		throw Mioga2::Exception::ExternalMioga("Mioga2::ExternalMioga::InitializeFromServerName",
        __x("Can't find external mioga declaration with server name {server}", server => $server));
	}


	$self->{values} = $res;
}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga2 Mioga2::Config Mioga2::URI Mioga2::Application Mioga2::AppDesc
Mioga2::Old::User Mioga2::Old::Group Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2007, The MiogaII Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
