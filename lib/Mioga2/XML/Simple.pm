# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#   Description: Simple utilities to convert XML file in perl hashes
#
# ============================================================================

package Mioga2::XML::Simple;
use XML::LibXML;
use Devel::Peek;

######################################
# public methods
######################################

######################################
# constructor
#
# parameters : 
#  Only forcearray is supported,
#  See XML::Simple for more details
######################################
sub new {
    my ($classname, %params) = @_;

    my $self = {};
    bless($self, $classname);
    
    $self->{parser} = XML::LibXML->new();

    $self->{FORCEARRAY} = 0;
	if(defined $params{forcearray}) {
		$self->{FORCEARRAY} = $params{forcearray};
    }

    return $self;
}


######################################
# XMLin
#
# Parse an XML string.
# Parameter : an xml string or 
#             an xml filename
######################################
sub XMLin {
    my ($self, $text) = @_;

    my $parser = $self->{parser};
    my $doc;
    
    if($text !~ m{<.*?>}s) {
      $doc = $parser->parse_file($text) or die "Can't open $text : $!";
    }
    else {
      $doc = $parser->parse_string($text);
    }

    my $rootelement = $doc->getDocumentElement;
    return $self->_XMLTree($rootelement);
}


######################################
# private methods
######################################
sub _XMLTree {
    my ($self, $node)  = @_;

    my @children;
    my %elements_node_res;

    ##
    ## Parse each child of XML node
    ##
    for(my $child = $node->getFirstChild; 
        defined $child;
        $child = $child->getNextSibling) {
        
        ## if it's a text node
        if($child->getType == 3) {
          my $data = $child->getData;
          if(defined $data and $data !~ /^\s*$/s) {
            push(@{$elements_node_res{content}}, $data);
          }
        }

        ## if it's a tree node 
        elsif($child->getType == 1) {
            my $cname = $child->getName;
            push @{$elements_node_res{$cname}}, $self->_XMLTree($child);
        }
    }

    my @keys = keys %elements_node_res;

    ## Remove array with one element (if forcearray is not set)
    if($self->{FORCEARRAY} == 0) {
        foreach my $key (@keys) {
            if(@{$elements_node_res{$key}} == 1) {
                $elements_node_res{$key} = $elements_node_res{$key}->[0];
            }
        }
    }

    ## Parse attributes
    foreach my $child ($node->getAttributes) {
		  my $data = $child->getData;
      my $cname = $child->getLocalName;
		  $elements_node_res{$cname} = $data;
    }
    
    @keys = keys %elements_node_res;
    if(@keys == 0) {
        return {};
    }

    if(@keys == 1 and $keys[0] eq "content") {
        if($self->{FORCEARRAY} == 1 and @{$elements_node_res{content}} == 1) {
            return $elements_node_res{content}->[0];
        }
        else {
            return $elements_node_res{content};
        }
    }

    return \%elements_node_res;
}

1;
