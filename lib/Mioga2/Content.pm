# ============================================================================
# MiogaII Project (C) 2003-2007 The MiogaII Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Content.pm: Virtual class to response content.

=head1 DESCRIPTION

This module is a virtual class for content generator implementation.

=head1 METHODS DESRIPTION

=cut

package Mioga2::Content;

use strict;
use Error qw(:try);
use Data::Dumper;
use Mioga2::Apache;

# ============================================================================

=head2 new ($context, %params)

	Create the object and tun the Initialize method.
	return a new Mioga2::Content object.
	
=cut

# ============================================================================

sub new
{
	my ($class, $context, %params) = @_;
	my $self = { };
	
	$self->{context} = $context;

	bless($self, $class);
	$self->Initialize($context, %params);
	
	return $self;
}



# ============================================================================

=head2 Initialize ($context, %params)

	This method can be overloaded by Content implementation to initialize 
	the object when it was created. 
	
=cut

# ============================================================================

sub Initialize
{
	my ($self, $context, %params) = @_;
	return;
}



# ============================================================================

=head2 SetContent ($content)

	This method copy given data in $self->{content}.
	$self->{content} will be used by implementation of Generate method.


=cut

# ============================================================================
sub SetContent
{
	my ($self, $content) = @_;
	$self->{content} = $content;
}



# ============================================================================

=head2 GetEncoding ()

	Return the encoding of generated content. Must be overloaded.

=cut

# ============================================================================
sub GetEncoding
{
	my ($self) = @_;
	return;
}



# ============================================================================

=head2 CanCache ()

	Return 1 if the browser can cache the content, 0 otherwise. 
	Must be overloaded.

=cut

# ============================================================================
sub CanCache {
	my ($self) = @_;
	return 0;
}



# ============================================================================

=head2 GetMIME ()

	Return the MIME type of generated content. Must be overloaded.

=cut

# ============================================================================
sub GetMIME {
	my ($self) = @_;

	return "";
}



# ============================================================================

=head2 GetReturnCode ()

	Return the code to return to the user agent (OK, REDIRECT).

	OK by default.

=cut

# ============================================================================
sub GetReturnCode {
	my ($self) = @_;

	return Mioga2::Apache::HTTP_OK;
}



# ============================================================================

=head2 Generate ($request)

	This method must generate the output and MUST by overloaded by
	implementation.
	
	$request is an apache request object.

	Implementation MUST Apache 1 and Apache 2.
	So Apache depends must by loaded with require in the Initialize or a  
	BEGIN statement.
	

=cut

# ============================================================================
sub Generate
{
	my ($self, $r) = @_;

	return;
}



# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga2 Mioga2::Config Mioga2::URI Mioga2::Application Mioga2::AppDesc
Mioga2::Old::User Mioga2::Old::Group Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2007, The MiogaII Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
