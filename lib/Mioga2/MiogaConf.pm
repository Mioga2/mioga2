# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
MiogaConf.pm: Access class to the Mioga configuration file.

=head1 DESCRIPTION

This module permits to access to the Mioga configuration parameters.

=head1 METHODS DESCRIPTION

=cut

#
# ============================================================================

package Mioga2::MiogaConf;
use strict;

use Locale::TextDomain::UTF8 'miogaconf';

use Error qw(:try);
use Data::Dumper;
use Net::LDAP;
use DBI;
use POSIX qw(tzset);
use Fcntl;

use Mioga2::XML::Simple;
use Mioga2::tools::database;
use Mioga2::Apache;
use Mioga2::Database;

my $debug = 0;

my %miogaConfCache;

# ============================================================================

=head2 new ($file_path)

	Create the Mioga2::MiogaConf object from configuration file.
	return a new Mioga2::MiogaConf object.

=cut

# ============================================================================

sub new {
	my ($class, $file, %params) = @_;
	
	# ------------------------------------------------------
	# Force timezone to GMT
	# ------------------------------------------------------
	$ENV{TZ} = 'GMT';
	tzset();

	# ------------------------------------------------------
	# Check if the config object for Mioga2::MiogaConf $file is
	# cached.
	# If not, create a new object.
	# ------------------------------------------------------
	if( exists $miogaConfCache{$file}) {
		return $miogaConfCache{$file};
	}


	if(! -e $file) {
		throw Mioga2::Exception::Simple("Mioga2::MiogaConf::new", __x("{filename} does not exist", filename => $file));
	}

	my $xs = new Mioga2::XML::Simple();
	my $self = $xs->XMLin($file);

	$self->{file} = $file;

	if (!defined($self))
	{
		throw Mioga2::Exception::Simple("Mioga2::MiogaConf::new", __x("Cannot parse config file: <{config_file}>", config_file => $file));
	}

	bless($self, $class);

	if(! exists $params{nodb} or $params{nodb} != 1) {
		$self->ConnectDBI();
	}

	if(! exists $params{nocache} or $params{nocache} != 1) {
		$miogaConfCache{$file} = $self;
	}

	return $self;
}




# ============================================================================

=head2 GetDBH ()

	return database connection object (DBH)
	
=cut

# ============================================================================

sub GetDBH {
	my ($self) = @_;

	# ------------------------------------------------------
	# Check if the connection is active.
	# If not, reconnect to the database.
	# ------------------------------------------------------
	if(! $self->{dbh}->ping) {
		$self->{dbh}->disconnect;
		$self->ConnectDBI();
	}
	
	return $self->{dbh};
}


#===============================================================================

=head2 GetDBObject

Get Mioga2::Database object

=cut

#===============================================================================
sub GetDBObject {
	my ($self) = @_;

	return ($self->{db});
}	# ----------  end of subroutine GetDBObject  ----------



# ============================================================================

=head2 GetMiogaConfPath ()

	return the path to the Mioga.conf file. 
	
=cut

# ============================================================================

sub GetMiogaConfPath {
	my ($self) = @_;

	return $self->{file};
}

# ============================================================================

=head2 GetServerName ()

	return Mioga server name.
	
=cut

# ============================================================================

sub GetServerName {
	my ($self) = @_;

#   Problem with virtual hosts. Return the main ServerName.
#   Use HTTP 1.1 Host header.

    return Apache2::RequestUtil->request->headers_in->{host};
}



# ============================================================================

=head2 GetProtocol ()

	return Mioga server protocol (http or https).
	
=cut

# ============================================================================

sub GetProtocol {
	my ($self) = @_;

	if (defined ($self->{protocol}) && (ref ($self->{protocol}) ne 'HASH')) {
		# Get protocol from Mioga2 config
		return ($self->{protocol});
	}
	else {
		# Get protocol from Apache2 request
		my $port = Apache2::ServerUtil->server()->port();

		if($port == 0) {
			$port = Apache2::RequestUtil->request->connection()->local_addr()->port();
		}

		if($port == 443) {
			return 'https';
		}

		return 'http';
	}
}

# ============================================================================

=head2 GetFilenameEncoding ()

	return the encoding for filenames
	
=cut

# ============================================================================

sub GetFilenameEncoding {
	my ($self) = @_;

	return $self->{filename_encoding};
}


# ============================================================================

=head2 GetBinariesDir ()

	return the binaries dir.
	
=cut

# ============================================================================

sub GetBinariesDir {
	my ($self) = @_;

	return $self->{binaries_dir};
}

# ============================================================================

=head2 GetConfParameter ($param_name)

	return value of conf parameter
	
=cut

# ============================================================================

sub GetConfParameter {
	my ($self, $param_name) = @_;

	return $self->{$param_name};
}


# ============================================================================

=head2 GetSearchTmpDir ()

	return Mioga search temporary folder path.
	
=cut

# ============================================================================

sub GetSearchTmpDir {
	my ($self) = @_;

	return $self->{search_tmp_dir};
}


# ============================================================================

=head2 GetLargeListDefaultLPP ()

	return the default number of line per page of large lists.
	
=cut

# ============================================================================

sub GetLargeListDefaultLPP {
	my ($self) = @_;

	return $self->{largelist_default_lpp};
}


# ============================================================================

=head2 GetTmpDir ()

	return Mioga temporary folder path.
	
=cut

# ============================================================================

sub GetTmpDir {
	my ($self) = @_;

	return $self->{tmp_dir};
}



# ============================================================================

=head2 GetSmtpServer ()

	return 1 if the SMTP Server used by Mioga.
	
=cut

# ============================================================================

sub GetSmtpServer {
	my ($self) = @_;

	return $self->{smtp_server};
}




# ============================================================================

=head2 GetSearchPort ()

	return the port of the search daemon.
	
=cut

# ============================================================================

sub GetSearchPort {
	my ($self) = @_;

	return $self->{search_port_number};
}



# ============================================================================

=head2 GetBasePath ()

	return the base path.
	
=cut

# ============================================================================

sub GetBasePath {
	my ($self) = @_;

	if(ref($self->{base_uri}) eq 'HASH') {
		return '';
	}

	return $self->{base_uri};
}

# ============================================================================

=head2 GetDAVBasePath ()

	return the DAV base path.
	
=cut

# ============================================================================

sub GetDAVBasePath {
  my $self = shift;
  
  return $self->{dav_base_uri};
}

# ============================================================================

=head2 GetImagePath ()

	return the image path.
	
=cut

# ============================================================================

sub GetImagePath {
	my ($self) = @_;

	return $self->{image_uri};
}



# ============================================================================

=head2 GetGroupDataPath ()

	return Mioga group data path.
	
=cut

# ============================================================================

sub GetGroupDataPath {
	my ($self) = @_;

	return $self->{group_data};
}


# ============================================================================

=head2 GetHelpPath ()

	return the help path.
	
=cut

# ============================================================================

sub GetHelpPath {
	my ($self) = @_;

	return $self->{help_uri};
}



# ============================================================================

=head2 GetBinPath ()

	return the private application path.
	
=cut

# ============================================================================

sub GetBinPath {
	my ($self) = @_;

	return $self->{bin_uri};
}



# ============================================================================

=head2 GetPubBinPath ()

	return the public application path.
	
=cut

# ============================================================================

sub GetPubBinPath {
	my ($self) = @_;

	return $self->{pubbin_uri};
}



# ============================================================================

=head2 GetSXMLPath ()

	return the secured xml transaction path.
	
=cut

# ============================================================================

sub GetSXMLPath {
	my ($self) = @_;

	return $self->{sxml_uri};
}



# ============================================================================

=head2 GetXMLPath ()

	return the authenticated xml transaction path.
	
=cut

# ============================================================================

sub GetXMLPath {
	my ($self) = @_;

	return $self->{xml_uri};
}



# ============================================================================

=head2 GetPubXMLPath ()

	return the public xml transaction path.
	
=cut

# ============================================================================

sub GetPubXMLPath {
	my ($self) = @_;

	return $self->{pubxml_uri};
}



# ============================================================================

=head2 GetPrivatePath ()

	return the private webdav path.
	
=cut

# ============================================================================

sub GetPrivatePath {
	my ($self) = @_;

	return $self->{private_uri};
}



# ============================================================================

=head2 GetPublicPath ()

	return the public webdav path.
	
=cut

# ============================================================================

sub GetPublicPath {
	my ($self) = @_;

	return $self->{public_uri};
}



# ============================================================================

=head2 GetInstallDir ()

	return the install dir path.
	
=cut

# ============================================================================

sub GetInstallDir {
	my ($self) = @_;

	return $self->{install_dir};
}



# ============================================================================

=head2 GetXSLDir ()

	return the xsl dir.
	
=cut

# ============================================================================

sub GetXSLDir {
	my ($self) = @_;

	return $self->{xsl_dir};
}


# ============================================================================

=head2 GetDefaultLang ()

	return the default lang dir.
	
=cut

# ============================================================================

sub GetDefaultLang {
	my ($self) = @_;

	return $self->{default_values}->{default_lang};
}


# ============================================================================

=head2 GetLangDir ()

	return the lang dir.
	
=cut

# ============================================================================

sub GetLangDir {
	my ($self) = @_;

	return $self->{lang_dir};
}

# ============================================================================

=head2 GetLocalesPath ()

  return path where locales are installed.
  
=cut

# ============================================================================

sub GetLocalesPath {
  my ($self)  = @_;
  
  return $self->{locales_installdir};
}

# ============================================================================

=head2 GetMiogaPrefix ()

  return installation prefix.
  
=cut

# ============================================================================

sub GetMiogaPrefix {
  my ($self)  = @_;
  
  return $self->{mioga_prefix};
}

# ============================================================================

=head2 GetBaseDir ()

	return the base dir.
	
=cut

# ============================================================================

sub GetBaseDir {
	my ($self) = @_;

	return $self->{base_dir};
}



# ============================================================================

=head2 GetImageDir ()

	return the image dir.
	
=cut

# ============================================================================

sub GetImageDir {
	my ($self) = @_;

	return $self->{image_dir};
}


# ============================================================================

=head2 GetHelpDir ()

	return the help dir.
	
=cut

# ============================================================================

sub GetHelpDir {
	my ($self) = @_;

	return $self->{help_dir};
}



# ============================================================================

=head2 GetPrivateDir ()

	return the private dir.
	
=cut

# ============================================================================

sub GetPrivateDir {
	my ($self) = @_;

	return $self->{private_dir};
}



# ============================================================================

=head2 GetPublicDir ()

	return the public dir.
	
=cut

# ============================================================================

sub GetPublicDir {
	my ($self) = @_;

	return $self->{public_dir};
}



# ============================================================================

=head2 GetSkeletonDir ()

	return the skeleton dir.
	
=cut

# ============================================================================

sub GetSkeletonDir {
	my ($self) = @_;

	return $self->{skeleton_dir};
}



# ============================================================================

=head2 GetMiogaFilesDir ()

	return the mioga files dir.
	
=cut

# ============================================================================

sub GetMiogaFilesDir {
	my ($self) = @_;

	return $self->{mioga_files};
}


# ============================================================================

=head2 GetApacheUser ()

	return the apache user.
	
=cut

# ============================================================================

sub GetApacheUser {
	my ($self) = @_;

	return $self->{apache_user};
}

# ============================================================================

=head2 GetApacheGroup ()

	return the apache group.
	
=cut

# ============================================================================

sub GetApacheGroup {
	my ($self) = @_;

	return $self->{apache_group};
}


# ============================================================================

=head2 DumpXML ()

	return true if dump xml is true.
	
=cut

# ============================================================================

sub DumpXML {
	my ($self) = @_;

	return (exists $self->{dumpxml});
}


# ============================================================================

=head2 GetMiogaVersion ()

  Return current Mioga2 version.
  
=cut

# ============================================================================

sub GetMiogaVersion {
  return $Mioga2::VERSION;
}


#===============================================================================

=head2 GetDAVProtocol

Get mod_dav access protocol

=cut

#===============================================================================
sub GetDAVProtocol {
	my ($self) = @_;

	return ($self->{dav_protocol});
}	# ----------  end of subroutine GetDAVProtocol  ----------


#===============================================================================

=head2 GetDAVHost

Get mod_dav access host

=cut

#===============================================================================
sub GetDAVHost {
	my ($self) = @_;

	return ($self->{dav_host});
}	# ----------  end of subroutine GetDAVHost  ----------


#===============================================================================

=head2 GetDAVPort

Get mod_dav access port

=cut

#===============================================================================
sub GetDAVPort {
	my ($self) = @_;

	return ($self->{dav_port});
}	# ----------  end of subroutine GetDAVPort  ----------

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut

# ============================================================================

# ============================================================================

=head2 ConnectDBI ()

	Establish the DBI database connection.
	
=cut

# ============================================================================

sub ConnectDBI {
	my ($self) = @_;

	# ------------------------------------------------------
	# Connect to DBI database
	# ------------------------------------------------------
	$self->{db} = Mioga2::Database->new (%{$self->{database}});
	$self->{dbh} = $self->{db}->GetDBH ();

	if (!defined($self->{dbh}))	{
		throw Mioga2::Exception::DB("Mioga2::Config::ConnectDBI", 
									$DBI::errstr, __"Cannot open connection with database", "");
	}

	ExecSQL( $self->{dbh}, "SET TIME ZONE 'GMT'");
	ExecSQL( $self->{dbh}, "SET DATESTYLE TO ISO, European");

	my $res = SelectSingle($self->{dbh}, "SHOW server_encoding");
	ExecSQL( $self->{dbh}, "SET client_encoding TO '$res->{server_encoding}'");
}

# ============================================================================

=head2 ConnectNotifierFIFO ()

    Establish the connection to notifier fifo.
    
=cut

# ============================================================================

sub ConnectNotifierFIFO {
    my ($self) = @_;

    if (!sysopen(FIFO, $self->{notifierfifo}, O_WRONLY|O_NONBLOCK)) {
        print STDERR "Can't open notifier fifo for writing!";
        return;
    }
    $self->{notifier} = *FIFO;
}

# ============================================================================

=head2 WriteNotifierFIFO ()

    Write to notifier fifo.
    
=cut

# ============================================================================

sub WriteNotifierFIFO {
    my ($self, $data) = @_;

    $self->ConnectNotifierFIFO if !$self->{notifier};
    return if !$self->{notifier};
    
    syswrite($self->{notifier}, $data);
}


#===============================================================================

=head2 GetActivationDelay

Get default account activation delay

=cut

#===============================================================================
sub GetActivationDelay {
	my ($self) = @_;

	return ($self->{activation_delay});
}	# ----------  end of subroutine GetActivationDelay  ----------

# ============================================================================


#===============================================================================

=head2 GetCaptchaDefaults

Get default values for captcha module.

=cut

#===============================================================================
sub GetCaptchaDefaults {
	my ($self) = @_;

	my $captcha;
	for (qw/length width lines wave swirl noise border expire skew rotate/) {
		$captcha->{$_} = $self->{captcha}->{$_};
	}

	return ($captcha);
}	# ----------  end of subroutine GetCaptchaDefaults  ----------


#===============================================================================

=head2 GetAuthenTimeout

Get authentication sessions timeout

=cut

#===============================================================================
sub GetAuthenTimeout {
	my ($self) = @_;

	return ($self->{authen_timeout});
}	# ----------  end of subroutine GetAuthenTimeout  ----------


#===============================================================================

=head2 GetLoginURI

Get login handler URI

=cut

#===============================================================================
sub GetLoginURI {
	my ($self) = @_;

	return ($self->{login_uri});
}	# ----------  end of subroutine GetLoginURI  ----------


#===============================================================================

=head2 GetUserURI

Get user personal spaces URI

=cut

#===============================================================================
sub GetUserURI {
	my ($self) = @_;

	return ($self->{user_uri});
}	# ----------  end of subroutine GetUserURI  ----------


#===============================================================================

=head2 GetInternalURI

Get internal URIs prefix (URIs that are not handled by Mioga2)

=cut

#===============================================================================
sub GetInternalURI {
	my ($self) = @_;

	return ($self->{int_uri});
}	# ----------  end of subroutine GetInternalURI  ----------


#===============================================================================

=head2 GetExternalURI

Get external URIs prefix (URIs that are not handled by Mioga2)

=cut

#===============================================================================
sub GetExternalURI {
	my ($self) = @_;

	return ($self->{ext_uri});
}	# ----------  end of subroutine GetExternalURI  ----------


#===============================================================================

=head2 ShowSourceInstanceName

Returns the value of show_source_instance_name configuration flag. This flag is used by Bottin to indicate or not which instance the users are imported from.

=cut

#===============================================================================
sub ShowSourceInstanceName {
	my ($self) = @_;

	return (exists ($self->{show_source_instance_name}));
}	# ----------  end of subroutine ShowSourceInstanceName  ----------


#===============================================================================

=head2 GetAdminMacros

Get instance skeleton $$ADMIN_...$$ macro replacement values.

=cut

#===============================================================================
sub GetAdminMacros {
	my ($self) = @_;

	my $macros = { };

	$macros->{ADMIN_IDENT} = $self->{users}->{admin}->{ident};
	$macros->{ADMIN_EMAIL} = $self->{users}->{admin}->{email};
	$macros->{ADMIN_PASSWORD} = $self->{users}->{admin}->{password};
	$macros->{ADMIN_TYPE} = $self->{users}->{admin}->{type};

	return ($macros);
}	# ----------  end of subroutine GetAdminMacros  ----------


#===============================================================================

=head2 IsSharedAdmin

Returns admin account sharing preference.

=cut

#===============================================================================
sub IsSharedAdmin {
	my ($self) = @_;

	my $status = defined ($self->{users}->{admin}->{shared}) ? 1 : 0;

	return ($status);
}	# ----------  end of subroutine IsSharedAdmin  ----------


#===============================================================================

=head2 IsCentralizedUsers

Returns user centralization preference.

=cut

#===============================================================================
sub IsCentralizedUsers {
	my ($self) = @_;

	my $status = defined ($self->{users}->{centralize}) ? 1 : 0;

	return ($status);
}	# ----------  end of subroutine IsCentralizedUsers  ----------


#===============================================================================

=head2 GetLDAPDefaults

Returns the default LDAP configuration from Mioga.conf

=head3 Incoming Arguments

=over

None.

=back

=head3 Return value

A hash containing LDAP default values.

=back

=cut

#===============================================================================
sub GetLDAPDefaults {
	my ($self) = @_;

	my $ldap = { };

	for my $attr (qw/ldap_host ldap_port ldap_bind_dn ldap_bind_pwd ldap_user_base_dn ldap_user_filter ldap_user_scope ldap_user_ident_attr ldap_user_firstname_attr ldap_user_lastname_attr ldap_user_email_attr ldap_user_pwd_attr ldap_user_desc_attr ldap_user_inst_attr/) {
		$ldap->{$attr} = $self->{$attr};
	}

	return ($ldap);
}	# ----------  end of subroutine GetLDAPDefaults  ----------


# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

mod_perl Mioga2 Mioga2::Config Mioga2::URI Mioga2::Application Mioga2::AppDesc
Mioga2::Old::User Mioga2::Old::Group Mioga2::Old::Resource

=head1 COPYRIGHT

Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
