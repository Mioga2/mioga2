# ============================================================================
# Mioga2 Project (C) 2005-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#	Description: 

=head1 NAME
	
F<FileBrowser.pm>: The Mioga2 file browser.

=head1 METHODS DESRIPTION

=cut

package Mioga2::FileBrowser;
use strict;

use base qw(Mioga2::Application);
use Locale::TextDomain::UTF8 'filebrowser';

use Error qw(:try);
use Data::Dumper;

use Mioga2::Content::XSLT;
use Mioga2::tools::database;
use Mioga2::tools::date_utils;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::APIExternalMioga;
use Mioga2::tools::APIApplication;
use Mioga2::Exception::Group;
use Mioga2::Exception::User;
use Mioga2::Exception::Resource;
use Mioga2::Exception::Simple;
use Mioga2::Exception::Quota;
use Mioga2::URI;
use Mioga2::Magellan::DAV;

my $debug = 0;

# ============================================================================

=head2 GetAppDesc ()

=over

=item The current application description.

=item B<parameters :> I<None>.

=item B<return :> an application description hash (I<%AppDesc>).

=back

=cut

# ============================================================================

sub GetAppDesc
{
	my ($self, $context) = @_;

    my %AppDesc = ( ident   => 'FileBrowser',
	    	name	=> __('File browser'),
                package => 'Mioga2::FileBrowser',
                description => __('The Mioga2 FileBrowser Application '),
				type    => 'normal',
                all_groups => 1,
                all_users  => 1,
                can_be_public => 1,
				is_user             => 0,
				is_group            => 1,
				is_resource         => 0,
				functions   => { 'FileBrowser' => __('FileBrowser Functions'),
								},
                func_methods  => { 'FileBrowser' => [ 'DisplayMain',
														'DisplayMain2',
													   'GetResourcesXML',
													   'DisplayURI',
									   				],
							   		},
				public_methods => [
							'DisplayMain',
							'DisplayMain2',
							'GetResourcesXML',
							'DisplayURI'
						],
				non_sensitive_methods => [
							'DisplayMain',
							'DisplayMain2',
							'GetResourcesXML',
							'DisplayURI'
						],
                );
                
	return \%AppDesc;
}
# ============================================================================

=head2 DisplayMain ()

=over

=item The main function. It displays files and their infos by calling
DisplayURI.

=item B<parameters :> I<None>.

=item B<return :> a I<Mioga2::Content> object.

=back

=cut

# ============================================================================
sub DisplayMain
{
	my ($self, $context) = @_;

	my $content = new Mioga2::Content::REDIRECT($context, mode => "external");
	$content->SetContent("DisplayURI?uri=".$context->GetConfig()->GetPrivateURI() .'/'. $context->GetGroup()->GetIdent());

	return $content;
}
sub DisplayMain2
{
	my ($self, $context) = @_;
	#
	# Initialize session and get dir infos
	#
	$self->IntializeDirInfos($context);

	my $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= "<DisplayMain>";
	$xml .= $context->GetXML();

	$xml .= "</DisplayMain>";
	my $content = new Mioga2::Content::XSLT($context, stylesheet => "filebrowser.xsl", locale_domain => 'filebrowser_xsl');
	$content->SetContent($xml);
	warn Dumper($xml) if $debug;

	return $content;
}
# ============================================================================

=head2 DisplayURI ()

=over

=item It displays files and their infos.

=item B<parameters :> I<None>.

=item B<return :> a I<Mioga2::Content> object.

=back

=cut

# ============================================================================
sub DisplayURI
{
	my ($self, $context) = @_;

	my $config = $context->GetConfig ();

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ ['uri'], 'disallow_empty', ['location', $config]],
			[ ['root'], 'allow_empty', ['location', $config]],
			[ ['sort'], 'allow_empty', 'stripwxps'],
		];
	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $xml = "<?xml version=\"1.0\"?>";
	$xml .= "<DisplayURI>";
	$xml .= $context->GetXML();
	$xml .= $self->GetHeaders($context, $values);
	
	$xml .= $self->GetResourcesXML($context, $values->{uri}, 1, $values->{sort}, $values->{root});
	
	$xml .= "</DisplayURI>";
	my $content = new Mioga2::Content::XSLT($context, stylesheet => "filebrowser.xsl", locale_domain => 'filebrowser_xsl');
	$content->SetContent($xml);
	warn Dumper($xml) if $debug;

	return $content;
}
# ============================================================================

=head2 GetResourcesXML (I<$uri>, I<$show_files>, I<$sort>)

=over

=item Returns resources at $uri (including files if $show_files is set to 1)
in a XML form.

=item B<parameters :>

=over

=item I<$uri> : uri (string) pointing to a directory.

=item I<$show_files> : boolean (0|1) to return files or not.

=item I<$sort> : sort by header name.

=back

=item B<return :> a I<xml string> representing resources available.

=back

=cut

# ============================================================================
sub GetResourcesXML
{
	my ($self, $context, $uri, $show_files, $sort, $root) = @_;
    
	my $xml;
	my $mioga_id   = $context->GetConfig->GetMiogaId;
	my @resources  = $self->GetResources($context, $uri, $show_files, $sort);
	my $results    = SelectMultiple($context->GetConfig()->GetDBH(), "SELECT
property FROM m_dav_property WHERE enabled=true AND mioga_id=$mioga_id");
	
	$uri =~ s/\/$//;
    if (defined $root && $root !~ /^\s*$/ && $uri !~ /$root/) {
        $uri = $root;
    }
	$xml .= "<resource uri=\"" . st_FormatXMLString ($uri) . "\" parent=\"" . $self->GetParentURI($uri) .
"\" follow_parent=\"" . 
            $self->FollowParent($context, (not defined $root) ?
$self->GetParentURI($uri) : $uri, $root) . "\"" .
            ((defined $root) ? " root=\"$root\"" : "") . ">";
    $xml .= $self->GetNavigationURI($context, $uri, $root);
            
	foreach my $res (@resources)
	{
		my $date;
		if ($context->IsPublic ()) {
			$date = du_GetDateXML (du_ISOToSecond($res->{date}));
		}
		else {
			$date = du_GetDateXMLInUserLocale(du_ISOToSecond($res->{date}), $context->GetUser());
		}

		$xml .= "<item ext=\"" . $self->GetExtension($res) . "\" type=\"" .
$self->GetType($res) . "\" size=\"" . 
			$self->GetSize($res) . "\" fancy_size=\"" .
$self->GetFancySize($res) . "\">";
		$xml .= "<uri short=\"" . $self->GetShortURI($res) . "\" long=\"" .
$self->GetLongURI($res) . "\"/>";
		$xml .= "<date>" . $date .  "</date>";
		foreach my $rec (@{$results})
		{
			$xml .= "<property name=\"" . $rec->{property} . "\" value=\"" .
$res->{$rec->{property}} . "\"/>";
		}
		$xml .= "</item>";
	}
	$xml .= "</resource>";
	
	return $xml;
}
# ============================================================================

=head2 GetHeaders ()

=over

=item Get the headers including enabled properties.

=item B<parameters :> I<None>.

=item B<return :> a I<xml string> containing headers name.

=back

=cut

# ============================================================================
sub GetHeaders
{
	my ($self, $context, $values) = @_;

  my $mioga_id  = $context->GetConfig->GetMiogaId;
	my $results = SelectMultiple($context->GetConfig()->GetDBH(), "SELECT
property FROM m_dav_property WHERE enabled=true AND mioga_id=$mioga_id");
	my $xml = "<headers>";
	$xml .= "<header name=\"Name\" link=\"n\" enabled=\"";
	$xml .= ($values->{"sort"} eq "n") ? 1 : 0;
	$xml .= "\"/>";
	
	$xml .= "<header name=\"Last modified\" link=\"lm\" enabled=\"";
	$xml .= ($values->{"sort"} eq "lm") ? 1 : 0;
	$xml .= "\"/>";
	
	$xml .= "<header name=\"Size\" link=\"s\" enabled=\"";
	$xml .= ($values->{"sort"} eq "s") ? 1 : 0;
	$xml .= "\"/>";
	
	foreach my $rec (@{$results})
	{
		$xml .= "<header name=\"" . $rec->{property} . "\" link=\"p_" .
$rec->{property} . "\" enabled=\"";
		$xml .= ($values->{"sort"} eq "p_".$rec->{property}) ? 1 : 0;
		$xml .= "\"/>";
	}
	$xml .= "</headers>";

	return $xml;
}

# ============================================================================
# PRIVATE METHODS
# ============================================================================


# ============================================================================

=head2 GetType (I<$resource>)

=over

=item Get the type of a resource.

=item B<parameters :>

=over

=item I<$resource> : a DAV hash resource.

=back

=item B<return :> a I<string> containing the resource type.

=back

=cut

# ============================================================================

sub GetType
{
	my ($self, $resource) = @_;
	my @audio = ("ogg","mp3","wav","wma","mp4","aac","flac","mpc","ra");
	my @image =
("png","jpg","bmp","tga","psd","xcf","tif","tiff","ico","xbm","xpm","pcx",
"sgi","pnm","psp","svg","gif");
	my @video = ("avi","qt","mkv","ogm","wmv");
	my @spreadsheet = ("xls","sxc","sdc","slk","csv");
	my @wordproc = ("doc","sxw","rtf","wpd","sdw");
	my @presentation = ("ppt","sxi","pps","sda","sdd");
	my @certificates = ("ca");
	my @compress = ("zip","bz2","gz","z","rar","ace","7z");
	my @css = ("css");
	my @html = ("html","htm");
	my @pdf = ("pdf");
	my @ps = ("ps","eps");
	my @xml = ("xml");
	my @text = ("txt");

	return "directory" if ($resource->{"type"} eq "collection");
	my $ext = $self->GetExtension($resource);
	return "audio" if (grep(/^$ext$/,@audio));
	return "image" if (grep(/^$ext$/,@image));
	return "video" if (grep(/^$ext$/,@video));
	return "spreadsheet" if (grep(/^$ext$/,@spreadsheet));
	return "wordproc" if (grep(/^$ext$/,@wordproc));
	return "presentation" if (grep(/^$ext$/,@presentation));
	return "certificates" if (grep(/^$ext$/,@certificates));
	return "compressed" if (grep(/^$ext$/,@compress));
	return "css" if (grep(/^$ext$/,@css));
	return "html" if (grep(/^$ext$/,@html));
	return "pdf" if (grep(/^$ext$/,@pdf));
	return "ps" if (grep(/^$ext$/,@ps));
	return "xml" if (grep(/^$ext$/,@xml));
	return "text" if (grep(/^$ext$/,@text));
	return "readme" if ($ext eq "" and lc($self->GetShortURI($resource)) eq
"readme");
	return "generic";
}


# ============================================================================

=head2 GetParentURI (I<$uri>)

=over

=item Get the parent of given $uri.

=item B<parameters :>

=over

=item I<$uri> : a I<string> containing an uri.

=back

=item B<return :> the parent uri as a I<string>.

=back

=cut

# ============================================================================

sub GetParentURI
{
	my ($self, $uri) = @_;

	my @path = split /\//, $uri;
	pop @path;
	return st_URIEscape (join('/', @path));
}


# ============================================================================

=head2 GetExtension (I<$resource>)

=over

=item Get the given $resource extension.

=item B<parameters :>

=over

=item I<$resource> : a DAV hash resource.

=back

=item B<return :> the extension as a I<string>.

=back

=cut

# ============================================================================

sub GetExtension
{
	my ($self, $resource) = @_;

	my @path = split /\//, $resource->{short_name};
	my @ext = split /\./, $path[-1];
	return (@ext < 2) ? "" : lc($ext[-1]);
}


# ============================================================================

=head2 GetSize (I<$resource>)

=over

=item Get the given $resource raw size.

=item B<parameters :>

=over

=item I<$resource> : a DAV hash resource.

=back

=item B<return :> size as a I<string>. If $resource is a collection then
return '-'.

=back

=cut

# ============================================================================

sub GetSize
{
	my ($self, $resource) = @_;

	return "-" if ($resource->{'type'} eq "collection");
	my $size = $resource->{'length'};
	return "0" if (not defined $size or $size eq '');
	return $size;
}


# ============================================================================

=head2 GetFancySize (I<$resource>)

=over

=item Get the given $resource size nicely displayed.

=item B<parameters :>

=over

=item I<$resource> : a DAV hash resource.

=back

=item B<return :> size as a I<string>. If $resource is a collection then
return '-'.

=back

=cut

# ============================================================================

sub GetFancySize
{
	my ($self, $resource) = @_;

	my $size = $self->GetSize($resource);
	return $size if ($size eq "-");
	return $self->ConvertBytes($size);
}

# ============================================================================

=head2 ConvertBytes (I<$size>)

=over

=item Takes $size as a size in bytes and returns a rounded size in kB, MB or
GB.

=item B<parameters :>

=over

=item I<$size> : size in bytes.

=back

=item B<return :> size as a I<string>.

=back

=cut

# ============================================================================

sub ConvertBytes
{
	my ($self, $size) = @_;
	my $rest = 0;

	if ($size < 1024)
	{
		return $size;
	}
	elsif ($size < 1024*1024)
	{
		$rest = int(($size % 1024) / 100);
		$size = int($size / 1024);
		return "$size.$rest k";
	}
	elsif ($size < 1024*1024*1024)
	{
		$rest = int(($size % (1024*1024)) / 100000);
		$size = int($size / (1024*1024));
		return "$size.$rest M";
	}
	elsif ($size >= 1024*1024*1024)
	{
		$rest = int(($size % (1024*1024*1024))/100000000);
		$size = int($size / (1024*1024*1024));
		return "$size.$rest G";
	}

}

# ============================================================================

=head2 GetResources (I<$uri>, I<$show_files>, I<$sort>)
	
=over

=item Take an uri, and return a list containing viewable folders and/or files.

=item B<parameters :>

=over

=item I<$uri> : uri (string) pointing to a directory.

=item I<$show_files> : boolean to return files or not.

=item I<$sort> : sort by header name.

=back

=item B<return :> a list containing directories and/or files user is
authorized to see.

=back

=cut

# ============================================================================

sub GetResources
{
	my ($self, $context, $uri, $show_files, $sort) = @_;
	my ($resources, @uris, %collection, @props);
	
  my $mioga_id  = $context->GetConfig->GetMiogaId;
	my $results = SelectMultiple($context->GetConfig()->GetDBH(), "SELECT
property FROM m_dav_property WHERE enabled=true AND mioga_id=$mioga_id");
	foreach my $rec (@{$results}) {
		push @props, $rec->{property};
	}
	
	$resources = Mioga2::Magellan::DAV::GetCollection($context,
				$context->GetUserAuth (),
$context->GetConfig()->GetProtocol()."://".$context->GetConfig()->
GetServerName(), $uri, @props);

	# Remove the consistency flag
	pop (@$resources);

	foreach (0..$#{$resources}) {
		next if (($show_files == 1 and ${$resources}[$_]->{type} eq "file")
or 
				  ${$resources}[$_]->{type} eq "collection");
		delete ${$resources}[$_];
	}

	$sort = "d" if (not defined $sort);
	return $self->SortResources($sort, $resources);
}


# ============================================================================

=head2 GetShortURI (I<$resource>)

=over

=item Get the last part of URI.

=item B<parameters :>

=over

=item I<$resource> : a DAV hash resource.

=back

=item B<return :> the last part of URI as a string.

=back

=cut

# ============================================================================

sub GetShortURI
{
	my ($self, $resource) = @_;
	my $path = $resource->{short_name};
	my @split = split /\//, $path;
	$path = pop @split;
	$path =~ s/\/$//;
    
	return (st_FormatXMLString ($path));
}


# ============================================================================

=head2 GetLongURI (I<$resource>)

=over

=item Get the complete URI of $resource.

=item B<parameters :>

=over

=item I<$resource> : a DAV hash resource.

=back

=item B<return :> the complete URI as a string.

=back

=cut

# ============================================================================

sub GetLongURI
{
	my ($self, $resource) = @_;
	my $path = $resource->{name};
	$path =~ s/\/$//;
	
	return st_URIEscape($path);
}


# ============================================================================

=head2 SortResources (I<$sort>, I<$resources>)

=over

=item Get resources sorted by $sort.

=item B<parameters :>

=over

=item I<$resources> : list of resources.

=item I<$sort> : type of sort ('n' for name, 'lm' for last modified, 's' for
size, 'p_aproperty' for a property named 'aproperty'.

=back

=item B<return :> a resources list sorted.

=back

=cut

# ============================================================================

sub SortResources
{
	my ($self, $sort, $resources) = @_;

	if ($sort =~ /^p_(.*)$/)
	{
		my $prop = $1;
		return sort { st_RemoveAccents(lc $a->{$prop}) cmp st_RemoveAccents(lc
$b->{$prop}) } @{$resources};
	}
	if ($sort eq "d")
	{
		my @sorted_res = sort { st_RemoveAccents(lc $self->GetShortURI($a))
cmp st_RemoveAccents(lc $self->GetShortURI($b)) } @{$resources};
		return sort { $a->{type} cmp $b->{type} } @sorted_res;
	}
	if ($sort eq "n")
	{
		return sort { st_RemoveAccents(lc $self->GetShortURI($a)) cmp
st_RemoveAccents(lc $self->GetShortURI($b)) } @{$resources};
	}
	if ($sort eq "lm")
	{
		return sort { du_ISOToSecond($a->{date}) <=>
du_ISOToSecond($b->{date}) } @{$resources};
	}
	if ($sort eq "s")
	{
		return sort { $self->GetSize($a) <=> $self->GetSize($b) }
@{$resources};
	}
}


# ============================================================================

=head2 GetNavigationURI (I<$context>, I<$uri>, I<$root>)

=over

=item Generate xml representing navigation through $uri.

=item B<parameters :>

=over

=item I<$context> : current Mioga2 context.

=item I<$uri> : base uri.

=item I<$root> : where to stop in uri.


=back

=item B<return :> xml representation as a string.

=back

=cut

# ============================================================================

sub GetNavigationURI
{
    my ($self, $context, $uri, $root) = @_;

    my $xml     = "<nav_uri>";
    my $config  = $context->GetConfig();
    my $home    = 0;
    my ($tags, $url);
    my @slices  = split /\//, $uri;
    shift @slices while ($slices[0] eq '');
    my $public  = $config->GetPublicPath();
    my @private = split /\//, $config->GetPrivateURI();
    my @rootSlices;
    
    if (defined $root) {
        @rootSlices = split /\//, $root;
    }

    foreach (@slices)
    {
        $url .= "/$_";
        if ($home == 1)
        {
            $xml .= "<tag text=\"" . st_FormatXMLString ($_) . "\" uri=\"" . st_URIEscape ($url) . "\"/>";
        }
        else
        {
            $xml .= "<tag text=\"$_\"/>";
            if (defined $root) {
                $home = 1 if ($_ eq $rootSlices[-2]);
            }
            else {
                $home = 1 if ($_ eq $private[-1] or ($_ eq
    $config->GetMiogaIdent() && $uri =~ /$public/));
            }
        }
    }
    
    $xml .= "</nav_uri>";
    return $xml;
}


# ============================================================================

=head2 FollowParent (I<$context>, I<$uri>)

=over

=item Determine if parent $uri has to be followed.

=item B<parameters :>

=over

=item I<$context> : current Mioga2 context.

=item I<$uri> : current uri.


=back

=item B<return :> a boolean.

=back

=cut

# ============================================================================

sub FollowParent
{
	my ($self, $context, $uri, $root) = @_;
	my $config = $context->GetConfig();
	my $public = $config->GetPublicPath();
	my @private = split /\//, $config->GetPrivateURI();
	
	my @split = split /\//, $uri;
    
    return 0 if ($split[-1] eq $private[-1] or $split[-1] eq
$config->GetMiogaIdent());
    if (defined $root) {
        return 0 if ($uri eq $root);
    }
	return 1;
}

# ============================================================================
# InitializeDirInfos
# Prepare session and get directories information (Dir tree).
# ============================================================================
sub InitializeDirInfos {
	my ($self, $context) = @_;
	#
	# Initialize session if needed
	#
	my $session = $context->GetSession();
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2005-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================

1;

__END__

