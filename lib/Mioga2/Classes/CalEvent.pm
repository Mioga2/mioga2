# ============================================================================
# Mioga2 Project (C) The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; version 2 only.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
CalEvent.pm: Mioga 2 CalEvent object.

=head1 DESCRIPTION

This object permits to extract events with expansion of recursive events.

=head1 SYNOPSIS

 use Mioga2::CalEvent;



=head1 METHODS DESCRIPTION

=cut

# ============================================================================

package Mioga2::Classes::CalEvent;
use strict;

use Locale::TextDomain::UTF8 'classes_calevent';

use Error qw(:try);
use Data::Dumper;
use Mioga2::Classes::Time;
use DateTime::Event::ICal;
use Data::ICal;
use Data::ICal::Entry::Event;
use DateTime::Format::ICal;

my $debug = 0;

# ============================================================================

=head2 GetEventsForUser ($params)

=over

=item Get event list for given period and all calendars for one user.

=item B<parameters:>

=over

=item I<$params{start_day}>: day of the month (between 1 and 31).

=item I<$params{start_month}>: month number (between 1 and 12).

=item I<$params{start_year}>: year number (4 digits).

=item I<$params{end_day}>: day of the month (between 1 and 31).

=item I<$params{end_month}>: month number (between 1 and 12).

=item I<$params{end_year}>: year number (4 digits).

=item I<$params{owner_cals}>: flag to get owned calendars

=item I<$params{other_cals}>: flag to get other users calendars

=item I<$params{group_cals}>: flag to get group calendars

=item I<$params{owner_id}>: owner_id of calendars

=item I<$params{user_id}>: user_id of caller

=item I<$params{db}>: database object

=back

=item B<return:> Array of events grouped by calendars

Throw exception on error.

=back

=cut

# ============================================================================

sub GetEventsForUser {
    my ($params) = @_;
	print STDERR "Classes::CalEvent::GetEventsForUser params = ".Dumper($params)."\n" if ($debug);

	my @result;

	if (!exists $params->{db}) {
		throw Mioga2::Exception::Simple("Mioga2::Classes::CalEvent::GetEventsForUser", __"Missing parameter db_obj.");
	}
	if (!exists $params->{user_id}) {
		throw Mioga2::Exception::Simple("Mioga2::Classes::CalEvent::GetEventsForUser", __"Missing parameter user_id.");
	}
	if (!exists $params->{start_year} || !exists $params->{start_month} || !exists $params->{start_day}) {
		throw Mioga2::Exception::Simple("Mioga2::Classes::CalEvent::GetEventsForUser", __"Missing parameter start date.");
	}
	if (!exists $params->{end_year} || !exists $params->{end_month} || !exists $params->{end_day}) {
		throw Mioga2::Exception::Simple("Mioga2::Classes::CalEvent::GetEventsForUser", __"Missing parameter start date.");
	}
	#
	# Construct calendar list function of params
	#
	my @types;
	if (exists($params->{owner_cals}) && $params->{owner_cals}) {
		push @types, "type=0";
	}
	if (exists($params->{other_cals}) && $params->{other_cals}) {
		push @types, "type=1";
	}
	if (exists($params->{group_cals}) && $params->{group_cals}) {
		push @types, "type=2";
	}
	# if no choices return empty result
	if (scalar(@types) == 0) {
		return \@result;
	}
	my $sql = "SELECT calendar_id, type, ident FROM user2cal WHERE user_id = ? AND (" . join(" OR ", @types).")";
	print STDERR "sql = $sql\n" if ($debug);
	my $cals = $params->{db}->SelectMultiple($sql, [$params->{owner_id}]);
	print STDERR "cals = ".Dumper($cals)."\n" if ($debug);

	$params->{cal_ids} = [];
	my %res_idx;
	my $i = 0;
	foreach my $c (@$cals) {
		push @{$params->{cal_ids}}, $c->{calendar_id};
		my $t = "owner";
		if ($c->{type} == 1) {
			$t = "other";
		}
		elsif ($c->{type} == 2) {
			$t = "group";
		}
		push @result, { calendar => $c->{ident}
		               , calendar_id => $c->{calendar_id} 
		               , type => $t
		               , events =>[] }; 
		$res_idx{$c->{calendar_id}} = $i;
		$i++;
	}
	print STDERR "res_idx = ".Dumper(\%res_idx)."\n" if ($debug);

	my $events = Mioga2::Classes::CalEvent::GetEventsForCals($params);
	print STDERR "events = ".Dumper($events)."\n" if ($debug);

	foreach my $e (@$events) {
		print STDERR "calendar_id = $e->{calendar_id}   res_idx = ".$res_idx{$e->{calendar_id}}."\n" if ($debug);
		push @{$result[$res_idx{$e->{calendar_id}}]->{events}}, $e;
	}
	print STDERR "result = ".Dumper(\@result)."\n" if ($debug);

    return \@result;
}
# ============================================================================

=head2 GetEventsForCals ($params)

=over

=item Get event list for given period and list of calendars

=item B<parameters:>

=over

=item I<$params{start_day}>: day of the month (between 1 and 31).

=item I<$params{start_month}>: month number (between 1 and 12).

=item I<$params{start_year}>: year number (4 digits).

=item I<$params{end_day}>: day of the month (between 1 and 31).

=item I<$params{end_month}>: month number (between 1 and 12).

=item I<$params{end_year}>: year number (4 digits).

=item I<$params{calendar_ids}>: array of calendar ids

=item I<$params{user_id}>: user_id of caller

=item I<$params{db}>: database object

=back

=item B<return:> Array of events

Throw exception on error.

=back

=cut

# ============================================================================

sub GetEventsForCals {
    my ($params) = @_;
	print STDERR "Classes::CalEvent::GetEventsForCals params = ".Dumper($params)."\n" if ($debug);

	if (!exists $params->{db}) {
		throw Mioga2::Exception::Simple("Mioga2::Classes::CalEvent::GetEventsForCals", __"Missing parameter db_obj.");
	}
	if (!exists $params->{user_id}) {
		throw Mioga2::Exception::Simple("Mioga2::Classes::CalEvent::GetEventsForCals", __"Missing parameter user_id.");
	}
	if (!exists $params->{start_year} || !exists $params->{start_month} || !exists $params->{start_day}) {
		throw Mioga2::Exception::Simple("Mioga2::Classes::CalEvent::GetEventsForCals", __"Missing parameter start date.");
	}
	if (!exists $params->{end_year} || !exists $params->{end_month} || !exists $params->{end_day}) {
		throw Mioga2::Exception::Simple("Mioga2::Classes::CalEvent::GetEventsForCals", __"Missing parameter start date.");
	}
	if (!exists $params->{cal_ids}) {
		throw Mioga2::Exception::Simple("Mioga2::Classes::CalEvent::GetEventsForCals", __"Missing parameter cal_ids.");
	}
	if (scalar(@{$params->{cal_ids}}) == 0) {
		throw Mioga2::Exception::Simple("Mioga2::Classes::CalEvent::GetEventsForCals", __"No calendars.");
	}

	my $access = Mioga2::Classes::CalEvent::GetCalendarAccess($params->{db}, $params->{user_id}, $params->{cal_ids});

	my $start_limit = sprintf("%.4d-%.2d-%.2d 00:00:00", $params->{start_year}, $params->{start_month}, $params->{start_day});
	my $end_limit = sprintf("%.4d-%.2d-%.2d 23:59:59", $params->{end_year}, $params->{end_month}, $params->{end_day});
	print STDERR "start_limit = $start_limit   end_limit = $end_limit\n" if ($debug);

	my $sql = "SELECT 
					cal_event.*, 
					task_category.private,
					task_category.name AS cat_name, 
					CASE 
						WHEN task_category.private = 't' AND task_category.rowid = cal_event.category_id 
						THEN 't' 
						ELSE 'f' 
					END AS private_event 
				FROM 
					cal_event, 
					task_category 
				WHERE 
					task_category.rowid = cal_event.category_id AND 
					calendar_id in (".join(",", @{$params->{cal_ids}}).")"
										." AND ( fl_recur='t' OR ((dstart >= ? OR dend >= ?) AND (dstart <= ? OR dend <= ?)) )";

	print STDERR "sql = $sql\n" if ($debug);
	my $res = $params->{db}->SelectMultiple($sql, [$start_limit, $start_limit, $end_limit, $end_limit]);
	print STDERR "res = ".Dumper($res)."\n" if ($debug);
	my @events;
	# Process task to prepare return structure
	foreach my $r (@$res) {
		print STDERR "r = " . Dumper($r) . "\n" if ($debug);
		print STDERR "access = " . $access->{$r->{calendar_id}} . "\n" if ($debug);
		if (exists($access->{$r->{calendar_id}})) {
			if ($access->{$r->{calendar_id}} < 1 || ($r->{private_event} eq 't' && $params->{user_id} != $r->{user_id}) ) {
				$r->{subject} = __('Busy');
				$r->{description} = "";
			}
		}
		else {
			throw Mioga2::Exception::Simple("Mioga2::Classes::CalEvent::GetEventsForCals", __"No access value for calendar_id !.");
		}
		if ($r->{fl_recur}) {
			print STDERR "recurrence\n" if ($debug);
			#
			# Process recurrence to generate individual tasks in the given interval
			#
			#
			# Put start and end in DateTime format and process rrule
			#
			my $dstart = Mioga2::Classes::Time->FromPSQL($r->{dstart});
			my $dend = Mioga2::Classes::Time->FromPSQL($r->{dend});
			my $dtstart;
			my $dtend;
			if ($r->{allday}) {
				$dtstart = DateTime->new(year => $dstart->year, month  => $dstart->mon, day => $dstart->mday, time_zone => 'UTC');
				$dtend = DateTime->new(year => $dend->year, month  => $dend->mon, day => $dend->mday);
			}
			else {
				$dtstart = DateTime->new(year => $dstart->year, month  => $dstart->mon, day => $dstart->mday,
									hour => $dstart->hour, minute  => $dstart->min, second => $dstart->sec, time_zone => 'UTC');
				$dtend = DateTime->new(year => $dend->year, month  => $dend->mon, day => $dend->mday,
									hour => $dend->hour, minute  => $dend->min, second => $dend->sec);
			}
			my $rrule = Mioga2::tools::Convert::JSONToPerl($r->{rrule});
			print STDERR "rrule = ".Dumper($rrule)."\n" if ($debug >= 2);
			my $decode = DecodeEvent($dtstart, $rrule);
			my $set = DateTime::Event::ICal->recur(%{$decode});
			#
			#  Create DateTimeSpan from limits and create iterator
			# 
			my $start_limit = DateTime->new(year => $params->{start_year}, month => $params->{start_month},  day=> $params->{start_day});
			my $end_limit = DateTime->new(year => $params->{end_year}, month => $params->{end_month},  day=> $params->{end_day}, hour=>23, minute=>59, second=>59);

			my $iter = $set->iterator;
			my $dt;
			while ($dt = $iter->next) {
				print STDERR "=> iter dt = ". $dt->datetime . " start_limit dt = ". $start_limit->datetime . " end_limit dt = ". $end_limit->datetime . "\n" if ($debug);
				print STDERR "epoch dt = ". $dt->epoch() . "   start_limit = ".$start_limit->epoch()." end_limit = ".$end_limit->epoch()."\n" if ($debug);
				if ( ($dt->epoch() >= $start_limit->epoch())  &&  ($dt->epoch() <= $end_limit->epoch()) ) {
					my %new_r = %{$r};
					my $ddiff = $dtend->subtract_datetime_absolute($dtstart);
					print STDERR "ddiff = ". Dumper($ddiff->in_units('years', 'months', 'weeks', 'days')) . "\n" if ($debug);
					my $new_dtend = $dt->clone();
					$new_dtend->add_duration($ddiff);
					print STDERR "new_dtend = ". $new_dtend->datetime . "\n" if ($debug);
					print STDERR "dtend = ". $dtend->datetime . "\n" if ($debug);
					$new_r{dstart} = $dt->ymd('-') . ' ' . $dt->hms(':');
					$new_r{dend} = $new_dtend->ymd('-') . ' ' . $new_dtend->hms(':');
					$new_r{base_start} = $dtstart->ymd('-') . ' ' . $dtstart->hms(':');
					$new_r{base_end} = $dtend->ymd('-') . ' ' . $dtend->hms(':');
					print STDERR "new_r = ". Dumper(\%new_r) . "\n" if ($debug);

					push @events, \%new_r;
				}
				last if ($dt->epoch() > $end_limit->epoch());
			}
		}
		else {
			push @events, $r;
		}
	}

    return \@events;
}

# ============================================================================

=head2 GetCalendarAccess ($db, $user_id, $cal_ids)

=over

=item Get the access right on calendar list for user.

=item B<parameters:>

=over

=item I<$db>: Database object

=item I<$params{user_id}>: user_id of caller

=item I<$cal_ids}>: array of calendar ids

=back

=item B<return:> Array of access rights

Throw exception on error.

=back

=cut

# ============================================================================
sub GetCalendarAccess {
	my ($db, $user_id, $cal_ids) = @_;
	print STDERR "Mioga2::Classes::CalEvent::GetCalendarAccess user_id = $user_id, cal_ids = ".Dumper($cal_ids)."\n" if ($debug);

	my $sql = "SELECT rowid, testaccessoncalendar(rowid ,?) as access FROM calendar WHERE rowid in (".join(",", @$cal_ids).")";
	print STDERR "sql = $sql\n" if ($debug);
	my $res = $db->SelectMultiple($sql, [$user_id]);
	print STDERR "access res= ".Dumper($res) if ($debug);
	my $ret = {};
	foreach my $r (@$res) {
		$ret->{$r->{rowid}} = $r->{access};
	}
	print STDERR "access ret= ".Dumper($ret) if ($debug);
	return $ret;
}
# ============================================================================

=head2 DecodeEvent

	Parse the ICal event to get DateTime recur parameters

=cut

# ============================================================================
sub DecodeEvent {
	my ($dtstart, $rrule) = @_;
	print STDERR "Mioga2::Classes::CalEvent::DecodeEvent rrule = ".Dumper($rrule)."\n" if ($debug);
	my $args = {};

	$args->{dtstart} = $dtstart;
	$args->{freq}     = $rrule->{repeat};
	$args->{interval} = $rrule->{period};
	if ($rrule->{endChoice} eq "2") {
		$args->{count} = $rrule->{endCount};
	}
	elsif ($rrule->{endChoice} eq "3") {
		$rrule->{endDate} =~ /^(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d)\.\d+Z$/;
		print STDERR "    endDate 1 = $1  2 = $2  3 = $3\n" if ($debug);
		my $dendDate = DateTime->new(year => $1, month  => $2, day => $3, hour => $4, minute => $5, second => $6 , time_zone => 'UTC');
		$dendDate->add_duration(DateTime::Duration->new(hours => 23, minutes => 59, seconds => 59 ));
		$args->{until} = $dendDate;
	}

	my @days = qw(su mo tu we th fr sa);
	if ($rrule->{repeat} eq 'weekly') {
		$args->{byday} = [];
		for (my $i = 0; $i < 7; $i++) {
			if ($rrule->{weekdays}->[$i]) {
				push @{$args->{byday}}, $days[$i];
			}
		}
	}
	elsif ($rrule->{repeat} eq 'monthly') {
		if ($rrule->{monthmode} == 1) {
			$args->{bymonthday} = $rrule->{fixmonthday};
		}
		elsif ($rrule->{monthmode} == 2) {
			$args->{byday} = [];
			my $num = $rrule->{monthdaypos};
			$num = "-1" if ($num == 5);
			push @{$args->{byday}}, $num . $days[$rrule->{monthday}];
		}
		else { # last day of month
			$args->{bymonthday} = -1;
		}
	}
	elsif ($rrule->{repeat} eq 'yearly') {
			$args->{bymonthday} = $rrule->{fixyearday};
			$args->{bymonth} = $rrule->{fixyearmonth} + 1;
	}


	print STDERR "args = ".Dumper($args)."\n" if ($debug);
	return $args;
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
