# ============================================================================
# Mioga2 Project (C) 2006-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; version 2 only.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Time.pm: Mioga 2 Time object.

=head1 METHODS DESCRIPTION

=cut

# ============================================================================

package Mioga2::Classes::Time;
use strict;

use base qw(Time::Piece);
use Locale::TextDomain::UTF8 'classes_time';

use Time::Seconds;
use Error qw(:try);
use Data::Dumper;
use POSIX qw(ceil);

# ============================================================================

=head2 new (%options)

=over

=item Create a new Mioga2 Time object. Parameters not specified will defaults to current time. Thus, if no parameter is specified at all, the object will default to current datetime.

=item B<parameters:>

=over

=item I<$options{day}>: day of the month (between 1 and 31).

=item I<$options{month}>: month number (between 1 and 12).

=item I<$options{year}>: year number (4 digits).

=item I<$options{hour}>: hour (between 0 and 23).

=item I<$options{min}>: minutes (between 0 and 59).

=item I<$options{sec}>: seconds (between 0 and 59).

=back

=item B<return:> Mioga2 Time object.

=back

=cut

# ============================================================================

sub new {
    my ($proto, %options) = @_;
    
    my $class       = ref($proto) || $proto;
    my $cur_time    = $class->SUPER::new();
     
    $options{day}   = $cur_time->mday   unless (defined $options{day});
    $options{month} = $cur_time->mon    unless (defined $options{month});
    $options{year}  = $cur_time->year   unless (defined $options{year});
    $options{hour}  = $cur_time->hour   unless (defined $options{hour});
    $options{min}   = $cur_time->min    unless (defined $options{min});
    $options{sec}   = $cur_time->sec    unless (defined $options{sec});
    
    my $self    = $class->SUPER::strptime("$options{year}-$options{month}-$options{day}T$options{hour}:$options{min}:$options{sec}", "%Y-%m-%dT%H:%M:%S");
    bless ($self, $class);
    return $self;
}

# ============================================================================

=head2 strftimetz ($format, $timezone)

=over

=item Same as strftime but take a timezone such as 'Europe/Paris'.

=item B<parameters:>

=over

=item I<$format>: format same as strftime.

=item I<$timezone>: a timezone in tzinfo format.

=back

=item B<return:> a string representing the date time.

=back

=cut

# ============================================================================

sub strftimetz {
  my ($self, $format, $tz) = @_;
  my $seconds = $self->epoch;
  
  my $res;
  { 
    local $ENV{TZ} = $tz;
    POSIX::tzset();
    $res = POSIX::strftime($format, localtime($seconds));   
  }
  POSIX::tzset();

  return $res;
}

# ============================================================================

=head2 BeginningOfWeek ()

=over

=item Retrieve the beginning of week.

=item B<parameters:> none.

=item B<return:> MiogaTime object representing the beginning of week.

=back

=cut

# ============================================================================

sub BeginningOfWeek {
    my ($self) = @_;
    
    my $cur_wday    = $self->wday();
    $cur_wday       = 8 if ($cur_wday == 1);
    my $offset      = $cur_wday - 2;
    
    return ($self - $offset * ONE_DAY)->BeginningOfDay;
}

# ============================================================================

=head2 EndOfWeek ()

=over

=item Retrieve the end of week.

=item B<parameters:> none.

=item B<return:> MiogaTime object at end of week.

=back

=cut

# ============================================================================

sub EndOfWeek {
    my ($self) = @_;
    
    return $self->BeginningOfWeek + ONE_WEEK - 1;
}

# ============================================================================

=head2 BeginningOfDay ()

=over

=item Retrieve beginning of day.

=item B<parameters:> none.

=item B<return:> MiogaTime object at beginning of day.

=back

=cut

# ============================================================================

sub BeginningOfDay {
    my ($self) = @_;
    
    return $self - $self->hour * ONE_HOUR - $self->min * ONE_MINUTE - $self->sec;
}

# ============================================================================

=head2 EndOfDay ()

=over

=item Retrieve the end of day.

=item B<parameters:> none.

=item B<return:> MiogaTime object at end of day.

=back

=cut

# ============================================================================

sub EndOfDay {
    my ($self) = @_;
    
    return $self->BeginningOfDay + ONE_DAY - 1;
}

# ============================================================================

=head2 BeginningOfMonth ()

=over

=item Retrieve the beginning of month.

=item B<parameters:> none.

=item B<return:> MiogaTime object at beginning of month.

=back

=cut

# ============================================================================

sub BeginningOfMonth {
    my ($self) = @_;
    
    return ($self - ($self->mday - 1) * ONE_DAY)->BeginningOfDay;
}

# ============================================================================

=head2 EndOfMonth ()

=over

=item Retrieve the end of month.

=item B<parameters:> none.

=item B<return:> MiogaTime object at end of month.

=back

=cut

# ============================================================================

sub EndOfMonth {
    my ($self) = @_;
    
    return $self->BeginningOfMonth + $self->month_last_day * ONE_DAY - 1;
}

# ============================================================================

=head2 NextMonth ()

=over

=item Retrieve the next month with current day properties (ie. January 10th will become February 10th with the same time informations).

=item B<parameters:> none.

=item B<return:> MiogaTime object at next month.

=back

=cut

# ============================================================================

sub NextMonth {
    my ($self) = @_;
    
    return $self->EndOfMonth + 1 + ($self->mday - 1) * ONE_DAY + $self->hour * ONE_HOUR + $self->min * ONE_MINUTE + $self->sec;
}

# ============================================================================

=head2 PreviousMonth ()

=over

=item Retrieve the previous month with current day properties (ie. February 10th will become January 10th with the same time informations).

=item B<parameters:> none.

=item B<return:> MiogaTime object at previous month.

=back

=cut

# ============================================================================

sub PreviousMonth {
    my ($self) = @_;
    
    return ($self->BeginningOfMonth - 1)->BeginningOfMonth + ($self->mday - 1) * ONE_DAY + $self->hour * ONE_HOUR + $self->min * ONE_MINUTE + $self->sec;
}

# ============================================================================

=head2 NextDay ()

=over

=item Retrieve the next day with current day properties (ie. January 10th will become January 11th with the same time informations).

=item B<parameters:> none.

=item B<return:> MiogaTime object at next day.

=back

=cut

# ============================================================================

sub NextDay {
    my ($self) = @_;
    
    return $self + ONE_DAY;
}

# ============================================================================

=head2 PreviousDay ()

=over

=item Retrieve the previous day with current day properties (ie. January 10th will become January 9th with the same time informations).

=item B<parameters:> none.

=item B<return:> MiogaTime object at previous day.

=back

=cut

# ============================================================================

sub PreviousDay {
    my ($self) = @_;
    
    return $self - ONE_DAY;
}

# ============================================================================

=head2 InRange ($dt1, $dt2)

=over

=item Is current datetime in range between $dt1 and $dt2.

=item B<parameters:>

=over

=item I<$dt1>: first datetime object.

=item I<$dt2>: second datetime object.

=back

=item B<return:> true or false.

=back

=cut

# ============================================================================

sub InRange {
    my ($self, $dt1, $dt2) = @_;
    
    return ($self >= $dt1 and $self <= $dt2);
}

# ============================================================================

=head2 FromPSQL ($string)

=over

=item Convert a datetime from PostgreSQL (as string) into a Mioga2 Time object.

=item B<parameters:>

=over

=item I<$string>: string from DB.

=back

=item B<return:> a Mioga2 Time object.

=back

=cut

# ============================================================================

sub FromPSQL {
    my ($self, $string) = @_;
    
    $string =~ s/\.\d*$//;
    return $self->strptime($string, "%Y-%m-%d %H:%M:%S");
}

# ============================================================================

=head2 RFC3339DateTime ()

=over

=item Convert a Mioga2 Time object to a RFC3339-compliant time string.

=item B<parameters:> none.

=item B<return:> a RFC3339-compliant time string.

=back

=cut

# ============================================================================

sub RFC3339DateTime {
	my ($self) = @_;

	return $self->datetime . 'Z';
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2006-2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
