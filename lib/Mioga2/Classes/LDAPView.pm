# ============================================================================
# Mioga2 Project (C) 2007-2010 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
LDAPView.pm : The Mioga2 view for LDAP access

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Classes::LDAPView;
use strict;
use base qw(Mioga2::Classes::View);


use Locale::TextDomain::UTF8 'ldap_view';

use Error qw(:try);
use Mioga2::LDAP;
use Mioga2::tools::database;
use Mioga2::tools::Convert;
use Mioga2::Exception::Simple;
use Data::Dumper;

my $debug = 0;

# ============================================================================

=head2 new ($options)

=over

=item Create a new Mioga2 Grid object.

=back

=cut

# ============================================================================
sub new {
	my ($proto, $options) = @_;
	print STDERR "LDAPView::new()\n" if ($debug);

	my $class = ref($proto) || $proto;

	my $self  = {};
	bless $self, $class;

	if (!exists($options->{ldap})) {
		throw Mioga2::Exception::Simple("LDAPView::new", __"ldap parameter not given.");
	}
	$self->{ldap} = $options->{ldap};

	if (!exists($options->{desc})) {
		throw Mioga2::Exception::Simple("LDAPView::new", __"desc parameter not given.");
	}
	$self->{desc} = $options->{desc};

	$self->{index} = 'dn';

	$self->Initialize();

	return $self;
}
# ============================================================================

=head2 GetFilterFieldList ()

=over

=item Returns list of field to be in filter.

=back

=cut

# ============================================================================
sub GetFilterFieldList {
	my ($self) = @_;
	print STDERR "LDAPView::GetFilterFieldList()\n" if ($debug);
	return $self->{desc}->{filter};
}
# ============================================================================

=head2 GetFieldList ()

=over

=item Returns list of field to be displayed.

=back

=cut

# ============================================================================
sub GetFieldList {
	my ($self) = @_;
	print STDERR "LDAPView::GetFieldList()\n" if ($debug);
	return $self->{desc}->{fields};
}


#===============================================================================

=head2 GetDataTable

=over

=item Return view contents as Perl array of hashes

=back

=cut

#===============================================================================
sub GetDataTable {
	my ($self, $params) = @_;

	print STDERR "LDAPView::GetDataTable()\n" if ($debug);

	my $mioga_ident = $params->{instance};
	my $dbh = $self->{ldap}->{config}->GetDBH ();

	$params->{ldap_params}->{use_instance_user_filter} = 0;

	my $res = $self->{ldap}->SearchUsers (%{$params->{ldap_params}});

	my $table = [];
	foreach my $row (@{$res}) {
		my %fields;
		foreach my $f (@{$self->{fields}}) {
			if ($f ne $self->{index}) {
				if ($f =~ /^count\(.*\)$/) {
					my $field = $f;
					$field =~ s/^count\((.*)\)$/$1/;
					$fields{$field} = scalar (@{$row->{$field}});
				}
				elsif ($row->{$f} ne '') {
					$row->{$f} =~ s/\'/\\\'/g;
					$fields{$f} = $row->{$f};
				}
				else {
					# Try to get value from DB
					my $req = '';
					for ($f) {
						if (/last_connection/) {
							$req = "SELECT last_connection FROM m_group_user_last_conn where user_id=(SELECT rowid FROM m_user_base WHERE dn=? AND mioga_id=(SELECT rowid FROM m_mioga WHERE ident=?)) AND last_connection IS NOT NULL ORDER BY last_connection DESC LIMIT 1";
						}
						elsif (/status/) {
							$req = "SELECT (SELECT ident FROM m_user_status WHERE rowid=(SELECT status_id FROM m_user_base WHERE dn=? AND mioga_id=(SELECT rowid FROM m_mioga WHERE ident=?))) AS status";
						}
						else {
							$req = "SELECT $f FROM m_user_base WHERE rowid=(SELECT rowid FROM m_user_base WHERE dn=? AND mioga_id=(SELECT rowid FROM m_mioga WHERE ident=?))";
						}
					}
					if ($req ne '') {
						my $res = SelectSingle ($dbh, $req, ["$row->{dn}", $mioga_ident]);
						$fields{$f} = $res->{$f};
					}
				}
			}
			else {
				$fields{rowid} = $row->{$f};
			}
		}
		push (@$table, \%fields);
	}

	return ($table);
}	# ----------  end of subroutine GetDataTable  ----------


# ============================================================================

=head2 GetData ($params)

=over

=item Returns data according to mode parameter

=back

=cut

# ============================================================================
sub GetData {
	my ($self, $params) = @_;
	print STDERR "LDAPView::GetData()\n" if ($debug);
	my $data;

	if ($params->{mode} eq 'dojo_grid') {
		$data = {
			identifier => 'ident',
			label      => 'ident',
			items      => $self->GetDataTable($params),
		};
	}
	elsif ($params->{mode} eq 'json') {
		$data = $self->GetDataTable($params);
	}

	if (!$params->{as_object}) {
		$data = Mioga2::tools::Convert::PerlToJSON ($data);
	}

	print STDERR "[Mioga2::Classes::LDAPView::GetData] data: " . Dumper $data if ($debug);
	return $data;
}
# ============================================================================
# Private methods
# ============================================================================
# Initialize
# ============================================================================
sub Initialize {
	my ($self) = @_;
	print STDERR "LDAPView::Initialize()\n" if ($debug);
	$self->{fields} = [];
	foreach my $f (@{$self->{desc}->{fields}}) {
		push @{$self->{fields}}, $f->[0];
	}
	print STDERR "  fields = ".Dumper($self->{fields})."\n" if ($debug);
}
# ============================================================================
# GetLDAPFilter
# ============================================================================
sub GetLDAPFilter {
	my ($self) = @_;
	print STDERR "LDAPView::GetLDAPFilter()\n" if ($debug);
	my $filter = "";

	

	print STDERR "   filter = $filter\n" if ($debug);
	return $filter;
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2007-2010, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__

