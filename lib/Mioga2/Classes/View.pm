# ============================================================================
# Mioga2 Project (C) 2007-2010 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
View.pm : The Mioga2 generic base class View for datas

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Classes::View;
use strict;

use Locale::TextDomain::UTF8 'view';

use Error qw(:try);
use Mioga2::Exception::Simple;
use Data::Dumper;

my $debug = 0;

# ============================================================================

=head2 new ($options)

=over

=item Create a new Mioga2 View object.

=back

=cut

# ============================================================================
sub new {
	my ($proto, $options) = @_;
	print STDERR "View::new()\n" if ($debug);

	my $class = ref($proto) || $proto;

	my $self  = {};
	bless $self, $class;

	if (!exists($options->{desc})) {
		throw Mioga2::Exception::Simple("ObjView::new", __"desc parameter not given.");
	}
	$self->{desc} = $options->{desc};

	$self->Initialize();

	return ($self);
}
# ============================================================================

=head2 GetFilterFieldList ()

=over

=item Returns list of field to be in filter.

=back

=cut

# ============================================================================
sub GetFilterFieldList {
	my ($self) = @_;
	print STDERR "View::GetFilterFieldList()\n" if ($debug);
	return $self->{desc}->{filter};
}
# ============================================================================

=head2 GetFieldList ()

=over

=item Returns list of field to be displayed.

=back

=cut

# ============================================================================
sub GetFieldList {
	my ($self) = @_;
	print STDERR "View::GetFieldList()\n" if ($debug);
	return $self->{desc}->{fields};
}


#===============================================================================

=head2 GetFilterMode

Get filter mode type

=cut

#===============================================================================
sub GetFilterMode {
	my ($self) = @_;

	return ($self->{desc}->{filter_mode});
}	# ----------  end of subroutine GetFilterMode  ----------


#===============================================================================

=head2 GetFilterMatch

Get filter match type

=cut

#===============================================================================
sub GetFilterMatch {
	my ($self) = @_;

	return ($self->{desc}->{filter_match});
}	# ----------  end of subroutine GetFilterMatch  ----------
# ============================================================================

=head2 GetXML ()

=over

=item Returns XML description for Grid.

=back

=cut

# ============================================================================
sub GetXML {
	my ($self) = @_;
	return "";
}
# ============================================================================
# Private methods
# ============================================================================
# Initialize
# ============================================================================
sub Initialize {
	my ($self) = @_;
	print STDERR "View::Initialize()\n" if ($debug);
	$self->{fields} = [];
	foreach my $f (@{$self->{desc}->{fields}}) {
		push @{$self->{fields}}, $f->[0];
	}
	print STDERR "  fields = ".Dumper($self->{fields})."\n" if ($debug);
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2007-2010, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__

