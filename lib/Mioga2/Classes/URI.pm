# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; version 2 only.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
URI.pm: Mioga 2 URI object.

=head1 METHODS DESCRIPTION

=cut

# ============================================================================

package Mioga2::Classes::URI;
use strict;

use base qw(URI);

use Error qw(:try);
use Data::Dumper;
use URI::Escape;
use Encode;
use Encode::Detect::Detector;
use Text::Iconv;

my $debug = 0;

# ============================================================================

=head2 new (%options)

=over

=item Create a new Mioga2 URI object.

=item B<parameters:>

=over

=item I<$options{uri}>: the uri to construct object from.

=item I<$options{scheme}>: optional. The $scheme argument is only used when uri is relative.

=back

=item B<return:> Mioga2 URI object.

=back

=cut

# ============================================================================

sub new {
  my ($proto, %options) = @_;
    
  my $class = ref($proto) || $proto;
  
  # convert uri to UTF-8
  my $uri     = uri_unescape($options{uri});
  
  # attempt to see if uri is utf8 to avoid detection
  my $conv    = Text::Iconv->new('utf8', 'utf8');
  my $tmp_uri = $conv->convert($uri);
  
  unless ($tmp_uri) {
    my $charset = detect($uri) || 'iso-8859-15'; # defaults to latin9
    warn "charset = '$charset' for uri = '$uri'" if $debug;
    $conv = Text::Iconv->new($charset, "utf8");
    $uri  = $conv->convert($uri);
    warn "==> converted uri = '$uri'" if $debug;
  }
  
  my $self  = {};
  $self->{uri}  = URI->new($uri);
  bless $self, $class;
}

# ============================================================================

=head2 as_string ()

=over

=item Retrieve URI as an unescaped string in canonical form. See as_escaped_string to retrieve an escaped string.

=item B<parameters:> none.

=item B<return:> URI object as a string.

=back

=cut

# ============================================================================

sub as_string {
  my $self  = shift;
  
  return uri_unescape($self->{uri}->as_iri);
}

# ============================================================================

=head2 as_escaped_string ()

=over

=item Retrieve URI as an escaped string in canonical form. Alias for canonical().

=item B<parameters:> none.

=item B<return:> URI object as a string.

=back

=cut

# ============================================================================

sub as_escaped_string {
  my $self  = shift;
   
  return uri_escape($self->canonical, "\x00-\x20\x7f-\xff#&|'%\=\?") unless $self->{uri}->authority;
  my $scheme    = $self->{uri}->scheme;
  my $host_port = $self->{uri}->host_port;
  $host_port    =~ s/:80//;
  my $base_uri  = "$scheme://$host_port";
  return "$base_uri/".uri_escape($self->{uri}->rel($base_uri), "\x00-\x20\x7f-\xff#&|'%\=\?");
}

# ============================================================================

=head2 as_trimed_string ()

=over

=item Retrieve URI as string but without any trailing slash.

=item B<parameters:> none.

=item B<return:> URI object as a string.

=back

=cut

# ============================================================================

sub as_trimed_string {
  my $self  = shift;
  my $uri   = $self->as_string;
   
  $uri  =~ s!/+$!!;
  return $uri;
}

# ============================================================================

=head2 path_segments ()

  same as path_segments from URI without any parameters.
  
=cut

# ============================================================================

sub path_segments {
  my $self = shift;
  
  my $uri = $self->as_string;
  $uri =~ s!^.*://[^/]+/?!!;
  return split("/", $uri);
}

# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
