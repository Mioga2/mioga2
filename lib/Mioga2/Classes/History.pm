# ============================================================================
# Mioga2 Project (C) 2007 The Mioga2 Project
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; version 2 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
# Description: 

=head1 NAME
  
History.pm: History related utility functions.

=head1 METHODS DESCRIPTION

=cut

# ============================================================================

package Mioga2::Classes::History;
use strict;

use Error qw(:try);
use Data::Dumper;

# ============================================================================

=head2 GetHistoryPath ($uri, $group_ident)

  Split $uri path from $group_ident to end of uri and extract filename from uri.
  
=cut

# ============================================================================

sub GetHistoryPath {
  my ($uri, $group) = @_;
  
  my @segments  = $uri->path_segments;
  my $name      = pop @segments;
  shift @segments while ($segments[0] ne $group);
  
  return ($name, @segments);
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2007, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__