# ============================================================================
# Mioga2 Project (C) 2007-2010 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#
#	Description: 

=head1 NAME
	
Dojosp.pm : The Mioga2 Dojosp application.

=head1 METHODS DESCRIPTION

=cut

package Mioga2::Classes::Grid;
use strict;

use Locale::TextDomain::UTF8 'grid';

use Error qw(:try);
#use Mioga2::tools::database;
#use Mioga2::tools::args_checker;
use Mioga2::tools::string_utils;
use Mioga2::Exception::Simple;
use Data::Dumper;

my $debug = 0;

# ============================================================================

=head2 new ($options)

=over

=item Create a new Mioga2 Grid object.

=back

=cut

# ============================================================================
sub new {
  my ($proto, $options) = @_;
	print STDERR "Grid::new()\n" if ($debug);

	my $class = ref($proto) || $proto;

	my $self  = {};
	bless $self, $class;
	if (!exists($options->{ident})) {
		throw Mioga2::Exception::Simple("Grid::new", __"ident parameter not given.");
	}
	$self->{ident} = $options->{ident};

	if (!exists($options->{view})) {
		throw Mioga2::Exception::Simple("Grid::new", __"view parameter not given.");
	}
	$self->{view} = $options->{view};

	if (!exists($options->{get_data})) {
		throw Mioga2::Exception::Simple("Grid::new", __"get_data parameter not given.");
	}
	$self->{get_data} = $options->{get_data};

	if (!exists($options->{nodename})) {
		throw Mioga2::Exception::Simple("Grid::new", __"nodename parameter not given.");
	}
	$self->{nodename} = $options->{nodename};

	if (!exists($options->{identifier})) {
		throw Mioga2::Exception::Simple("Grid::new", __"identifier parameter not given.");
	}
	$self->{identifier} = $options->{identifier};

	if (!exists($options->{label})) {
		throw Mioga2::Exception::Simple("Grid::new", __"label parameter not given.");
	}
	$self->{label} = $options->{label};

	$self->{sort_field} = $options->{sort_field} if ($options->{sort_field});
	$self->{column_widths} = $options->{column_widths} if ($options->{column_widths});

	if (exists($options->{default_action})) {
		$self->{default_action} = $options->{default_action};
	}

	if (exists($options->{select_actions})) {
		$self->{select_actions} = $options->{select_actions};
	}

	if (!exists($options->{confirm_fields})) {
		throw Mioga2::Exception::Simple("Grid::new", __"confirm_fields parameter not given.");
	}
	$self->{confirm_fields} = $options->{confirm_fields};

	return $self;
}
# ============================================================================

=head2 GetXML ()

=over

=item Returns XML description for Grid.

=back

=cut

# ============================================================================
sub GetXML {
	my ($self) = @_;
	print STDERR "Grid::new()\n" if ($debug);
	print STDERR "self = ".Dumper($self)."\n" if ($debug);

    my $xml = '<Grid name="'.$self->{ident}.'">';
    $xml .= "<ident>".$self->{ident}."</ident>\n";
    $xml .= "<GetData>".$self->{get_data}."</GetData>\n";
    $xml .= "<NodeName>".$self->{nodename}."</NodeName>\n";
    $xml .= "<Identifier>".$self->{identifier}."</Identifier>\n";
    $xml .= "<Label>".$self->{label}."</Label>\n";
    $xml .= "<SortField>".$self->{sort_field}."</SortField>\n" if ($self->{sort_field});
    $xml .= "<DefaultAction>".$self->{default_action}."</DefaultAction>\n";

	# Fields for confirmations
	$xml .= "<ConfirmFields>'" . join ("', '", @{$self->{confirm_fields}}) . "'</ConfirmFields>";

	if (exists($self->{select_actions})) {
		foreach my $act (@{$self->{select_actions}}) {
    		$xml .= '<SelectAction function="'.$act->{function}.'" type="' . $act->{type} . '" func="' . $act->{func} . '" post_var="' . $act->{post_var} . '" check_function="' . ($act->{check_function} ? $act->{check_function} : 'function () { return (1); }') . '" error_function="' . $act->{errors} . '">'.st_FormatXMLString($act->{label}).'</SelectAction>';
		}
	}

	my $fieldlist = $self->{view}->GetFieldList();
	print STDERR "fieldlist = ".Dumper($fieldlist)."\n" if ($debug);
	# Compute width from number of columns, because IE does not handle correctly width="auto"
	foreach my $field (@{$fieldlist}) {
		my $field_ident = $field->[0];
		if ($field->[0] =~ /^count\(.*\)$/) {
			$field_ident = $field->[0];
			$field_ident =~ s/^count\((.*)\)$/$1/;
		}

		# Process custom width if any
		my $width;
		if ($self->{column_widths}) {
			if (exists ($self->{column_widths}->{$field_ident})) {
				$width = $self->{column_widths}->{$field_ident} ;
			}
			else {
				my $offset = ((scalar (@$fieldlist) - scalar (keys (%{$self->{column_widths}})) - 1) == 0) ? 0 : 1;
				$width = int (100 / (scalar (@$fieldlist) - scalar (keys (%{$self->{column_widths}})) - $offset)) . "%";
			}
		}
		else {
			$width = int (100 / (scalar (@$fieldlist) - 1)) . "%";
		}

		$xml .= "<Field width=\"$width\">";
		$xml .= "<ident>".$field_ident."</ident>";
		$xml .= "<type>".$field->[1]."</type>";
		if ($field->[2]) {
			$xml .= "<label>".$field->[2]."</label>";
		}
		else {
			$xml .= "<label>".$field->[0]."</label>";
		}
		$xml .= "</Field>";
	}

	my $filter_fieldlist = $self->{view}->GetFilterFieldList();
	print STDERR "filter_fieldlist = ".Dumper($filter_fieldlist)."\n" if ($debug);
    $xml .= "<Filter ws='' mode='" . $self->{view}->GetFilterMode () . "' match='" . $self->{view}->GetFilterMatch () . "'>";
	foreach my $field (@{$filter_fieldlist}) {
    	$xml .= '<Field type="'.$field->{type}.'" ' . (($field->{focus} == 1) ? 'focus="1"' : '') . '>';
    	$xml .= '<ident>'.$field->{ident}.'</ident>';
    	$xml .= '<label>'.$field->{label}.'</label>';
    	$xml .= '<value>'.$field->{value}.'</value>';
    	$xml .= '<url>'.$field->{url}.'</url>';
    	$xml .= '<nodename>'.$field->{nodename}.'</nodename>';
    	$xml .= '<identifier>'.$field->{identifier}.'</identifier>';
    	#if ($field->{type} eq 'list') {
		#	foreach my $val (keys(%{$field->{values}})) {
    	#		$xml .= '<item value="'.$val.'">'.$field->{values}->{$val}.'</item>';
		#	}
		#}
    	$xml .= "</Field>\n";
	}
    $xml .= "</Filter>\n";

    $xml .= "</Grid>";
	return $xml;
}
# ============================================================================

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::AppDesc Mioga2::Application

=head1 COPYRIGHT

Copyright (C) 2007-2010, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__

