#===============================================================================
#
#  Copyright (c) year 1999-2013, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

Haussmann.pm: the Mioga2 Project application

=head1 DESCRIPTION

The Mioga2::Haussmann application is used to manage project in a collaborative way.

=head1 METHODS DESCRIPTION

=cut

#
# ============================================================================

package Mioga2::Haussmann;
use base qw(Mioga2::Application);
use strict;
use warnings;
use open ':encoding(utf8)';

use vars qw($VERSION);
$VERSION = "2.0.0";

use JSON::XS;

use Locale::TextDomain::UTF8 'haussmann';
use Error qw(:try);
use Mioga2::Application;
use Mioga2::UserList;
use Mioga2::GroupList;
use Mioga2::ApplicationList;

use Mioga2::TagList;
use Mioga2::Config;
use Mioga2::URI;
use Mioga2::Content::XSLT;
use Mioga2::Content::REDIRECT;
use Mioga2::Exception::Simple;
use Mioga2::Exception::DB;
use Mioga2::tools::string_utils;
use Mioga2::tools::args_checker;
use Mioga2::tools::date_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIAuthz;
use Mioga2::Classes::Time;
use MIME::Entity;
use Data::Dumper;
use POSIX;
use Mioga2::tools::Convert;
use Error qw(:try);

my $debug = 0;

# ============================================================================
# GetAppDesc ()
# ============================================================================
sub GetAppDesc {
	my ($self, $context) = @_;
    my %AppDesc = (
    	ident   => 'Haussmann',
	    name	=> __('Haussmann'),
        package => 'Mioga2::Haussmann',
		type => 'normal',
        description => __('The Mioga2 Project Manager Application'),
        is_user             => 0,
        is_group            => 1,
        is_resource         => 0,
        api_version 		=> '2.4',
        all_groups 			=> 1,
        all_users  			=> 0,
        can_be_public 		=> 0,
        usable_by_resource 	=> 0,

        functions   => { 
			'Read'                 => __('Read functions'),
			'WriteTask'            => __('Write task functions'),
			'WriteProjectIfOwner'  => __('Write project functions if he is project owner'),
			'Anim'                 => __('Animation functions')
		},
        func_methods  => { 
			Read =>                ['SetUserToAccessTask', 'DisplayMain', 'GetLangList', 'GetTagList', 'GetModelList', 'GetProjectList', 'GetProject', 'GetProjectByTask', 'GetProjectByResult', 'GetTaskList', 'GetTask', 'GetTaskNoteList', 'GetResultList', 'GetResultNoteList'],
			WriteTask =>           ['SetUserToAccessTask', 'SetTask', 'CreateTaskNote', 'SetTaskNote', 'AddAttendees', 'RemoveAttendee'],
			WriteProjectIfOwner => ['SetUserToAccessTask', 'GetUsers', 'DeleteTask', 'SetTaskList', 'DeleteTaskNote', 'CreateProject', 'SetProject', 'DeleteProject', 'SetResult', 'DeleteResult','CreateResult', 'CreateResultNote', 'SetResultNote', 'DeleteResultNote', 'CreateModel', 'SetModel'],
			Anim =>                ['SetUserToAccessTask', 'GetUsers', 'DeleteTask', 'SetTaskList', 'DeleteTaskNote', 'CreateProject', 'SetProject', 'DeleteProject', 'SetResult', 'DeleteResult','CreateResult', 'CreateResultNote', 'SetResultNote', 'DeleteResultNote', 'CreateModel', 'SetModel']
		},
		public_methods => [
			'DisplayMain'
		],
		non_sensitive_methods => [
			'DisplayMain',
			'GetLangList',
			'GetModelList',
			'GetProject',
			'GetProjectByResult',
			'GetProjectByTask',
			'GetProjectList',
			'GetResultList',
			'GetResultNoteList',
			'GetTagList',
			'GetTask',
			'GetTaskList',
			'GetTaskNoteList',
			'GetUsers',

			# WARNING: Because of the huuuuge number of bugs in this unused application, it has not been tested after CSRF protection.
			# The methods below may fail because of CSRF but can’t be tested as the application fails to start because of lots untested code (AJAX returns, etc.)

			# 'AddAttendees',
			# 'CreateModel',
			# 'CreateProject',
			# 'CreateResultNote',
			# 'CreateTaskNote',
			# 'DeleteProject',
			# 'DeleteResult',
			# 'CreateResult',
			# 'DeleteResultNote',
			# 'DeleteTask',
			# 'DeleteTaskNote',
			# 'RemoveAttendee',
			# 'SetModel',
			# 'SetProject',
			# 'SetResult',
			# 'SetResultNote',
			# 'SetTask',
			# 'SetTaskList',
			# 'SetTaskNote',
			# 'SetUserToAccessTask',
		]
    );
	return \%AppDesc;
}

#===============================================================================

=head1 INTERFACE METHODS

=cut

#===============================================================================

=head2 DisplayMain

Display maain project window
	HTTP GET parameters:

=head3 Generated XML

=over

	<DisplayMain>
		<miogacontext>See Mioga2::RequestContext</miogacontext>
	</DisplayMain>

=back

=cut

# ============================================================================
sub DisplayMain {
	my ($self, $context) = @_;
	print STDERR "Haussmann::DisplayMain\n" if ($debug);
	my $xml;
	my $langList;
	my $displayContent;
	my $response;
	my $session = $context->GetSession();
	$xml = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xml .= '<DisplayMain>';
	$xml .= $context->GetXML();
	#	langList
	$langList = GetLangList($self,$context);
	if ($langList->{status} eq "OK") {
		$displayContent->{LangList} = $langList->{data};
	}
	else {
		print STDERR "Mioga2::Haussmann::DisplayMain] GetLangList failed\n";
	}
	#	TagsList
	my $taglist = Mioga2::TagList->new ($context->GetConfig ());
	@{$displayContent->{tag}} = map { s/(["'])/\\$1/g; $_ } @{$taglist->GetTags ()};
	$displayContent->{free_tags} = $taglist->GetFreeTagsStatus ();

	$xml .= Mioga2::tools::Convert::PerlToXML ($displayContent);
	$xml .= '</DisplayMain>';
	#	utf8::decode($xml);
	$response = new Mioga2::Content::XSLT($context, stylesheet => 'haussmann.xsl', locale_domain => 'haussmann_xsl');
	$response->SetContent($xml);
	warn Dumper $xml if $debug;
	return $response;
}	# ----------  end of subroutine DisplayMain  ----------

#===============================================================================

=head1 WEBSERVICE METHODS

=cut

#===============================================================================

#============================================================================

=head2 CreateProject (I<context>)

=over

=item Web service to creates a new project and associates this new project as current project in session persistent variable.

=item B<parameters :>

=item I<context> : context with specifics arguments :

=item I<label> : label of new project.

=item I<[model_id]> : id of model will be associated with this new project.

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => 
		{
			project_id : int,
			label : string
		}
}

=back

=cut

# ============================================================================
sub CreateProject {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['label'], 'disallow_empty', 'stripxws' ],
		[ ['model_id'], 'stripxws', 'allow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::CreateProject] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	my $session = $context->GetSession();

	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::CreateProject] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			$values->{group_id} = $context->GetGroup()->Get('rowid');
			$values->{owner_id} = $context->GetUser()->Get('rowid');
			$response->{data} = DbCreateProject($self, $values);
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::CreateProject] project create failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}	# ----------  end of subroutine CreateProject  ----------
# ============================================================================

=head2 CreateResult (I<context>)

=over

=item Web service to creates a new result.

=item B<parameters :> 

=item I<context> : context with specifics arguments :

=item I<project_id> : id of project will be associated with this new task.

=item I<label> : Label of task.

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => result
}

=back

=cut

# ============================================================================
sub CreateResult {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['label'], 'disallow_empty', 'stripxws' ]
	]);
	print STDERR "[Mioga2::Haussmann::CreateResult] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::CreateResult] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});

			if (defined ($proj_id) || $self->GetUserRights()->{Anim}) {
				$values->{result_id} = DbCreateResult($self, $values);
				$response->{data} = DbGetResult($self, $values);
			}
			else {
				throw Mioga2::Exception("Haussmann::CreateResult", __("User not owner of project or does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::CreateResult] create result failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}	# ----------  end of subroutine CreateResult  ----------

=head2 SetResult (I<context>)

=over

=item Web service to updates result.

=item B<parameters :> 

=item I<context> : context with specifics arguments :

=item I<project_id> : id of project

=item I<result_id> : id of result will be updated

=item I<[label]> : label of result will be updated

=item I<[tags]> : tags associated to this result

=item I<[budget]> : budget of result

=item I<[current_status]> : budget of result

=item I<status> : history of result status as text.
					For example : 
					"[
						{
							status: 0, ##### index of result status that defined in advanced parameters of project. ####
							date: "2012-08-01",
							comment: "text of comment", 
						},
						{
							status: 1, ##### index of result status that defined in advanced parameters of project. ####
							date: "2012-08-21",
							comment: "text of comment",
						}
					]"

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => result
}

=back

=cut

# ============================================================================
sub SetResult {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Haussmann::SetResult]" if ($debug);
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['result_id'], 'disallow_empty', 'want_int' ],
		[ ['project_id'], 'disallow_empty', 'want_int' ],
		[ ['label'], 'allow_empty', 'stripxws' ],
		[ ['pend'], 'allow_empty', 'stripxws' ],
		[ ['checked'], 'stripxws', 'allow_empty', 'bool' ],
		[ ['tags'], 'stripxws', 'allow_empty', ['xhtmlstring'] ],
		[ ['budget'], 'stripxws', 'allow_empty', 'want_int' ],
		[ ['current_status'], 'stripxws', 'allow_empty', 'want_int'],
		[ ['status'], 'stripxws', 'allow_empty']
	]);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};

	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::SetResult] . ac_CheckArgs error: " . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $resultSetResult = 0;
			my $user_id = $context->GetUser()->Get('rowid');
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			if (defined ($proj_id) || $self->GetUserRights()->{Anim}) {
				$resultSetResult = DbSetResult($self, $values);
				if ($resultSetResult == 1) {
					$response->{data} = DbGetResult ($self, $values);
				}
				else {
					print STDERR "[Mioga2::Haussmann::SetResult] DB update result is failed.\n";
				}
			}
			else {
				throw Mioga2::Exception("Haussmann::SetResult", __("User not owner of project or does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::SetResult] SetResult failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}

# ============================================================================

=head2 DeleteResult (I<context>)

=over

=item Web service to deletes an existing result.

=item B<parameters :> 

=item I<context> : context with specifics arguments :

=item I<project_id> : id of project. 

=item I<result_id> : id of result that will be deleted. 

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => id of deleted result
}

=back

=cut

# ============================================================================
sub DeleteResult {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['result_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::DeleteResult] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	my $user_id = $context->GetUser()->Get('rowid');
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::DeleteResult] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			if (defined ($proj_id) || $self->GetUserRights()->{Anim}) {
				$response->{data} = DbDeleteResult($self, $values);
			}
			else {
				throw Mioga2::Exception("Haussmann::DeleteResult", __("User not owner of project or does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DeleteResult] delete result failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}
# ============================================================================

=head2 CreateResultNote (I<context>)

=over

=item Web service to creates a result note.

=item B<parameters :> 

=item I<context> : context with specifics arguments :

=item I<project_id> : id of project

=item I<result_id> : Id of result that note will be associatd

=item I<text> : Text of this note

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => resultNote
}

=back

=cut

# ============================================================================
sub CreateResultNote {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['result_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['text'], 'stripxws', 'allow_empty' ]
	]);
	print STDERR "[Mioga2::Haussmann::CreateResultNote] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::CreateResultNote] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			if (defined ($proj_id) || $self->GetUserRights()->{Anim}) {
				$values->{group_id} = $context->GetGroup()->Get('rowid');
				$values->{owner_id} = $context->GetUser()->Get('rowid');
				my $request_result = DbCreateResultNote($self, $values);
				if ($request_result->{result} == 1) {
					$values->{result_note_id} = $request_result->{result_note_id};
					$values->{user_id} = $values->{owner_id};
					$response->{data} = DbGetResultNote($self, $values);
				}
				else {
					print STDERR "[Mioga2::Haussmann::CreateResultNote] create result note failed.\n";
				}
			}
			else {
				throw Mioga2::Exception("Haussmann::CreateResultNote", __("User not owner of project or does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::CreateResultNote] create result note failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}
# ============================================================================

=head2 SetResultNote (I<context>)

=over

=item Web service to updates an existing result note.

=item B<parameters :> 

=item I<context> : context with specifics arguments :

=item I<project_id> : id of project. 

=item I<result_id> : Id of result that note is associated. 

=item I<result_note_id> : Id of result note that will be updated. 

=item I<text> : Text of this note. 

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => resultNote
}

=back

=cut

# ============================================================================
sub SetResultNote {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['result_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['result_note_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['text'], 'stripxws', 'allow_empty' ]
	]);
	print STDERR "[Mioga2::Haussmann::SetResultNote] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::SetResultNote] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			if (defined ($proj_id) || $self->GetUserRights()->{Anim}) {
				$values->{group_id} = $context->GetGroup()->Get('rowid');
				my $request_result = DbSetResultNote($self, $values);
				if ($request_result == 1) {
					$response->{data} = DbGetResultNote($self,$values);
				}
			}
			else {
				throw Mioga2::Exception("Haussmann::SetResultNote", __("User not owner of project or does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::SetResultNote] set result note failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}

# ============================================================================

=head2 DeleteResultNote (I<context>)

=over

=item Web service to deletes an existing result note.

=item B<parameters :> 

=item I<context> : context with specifics arguments :

=item I<project_id> : id of project

=item I<result_id> : Id of result that note is associatd

=item I<result_note_id> :  Id of resul note that will be deleted

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => id of deleted result note
}

=back

=cut

# ============================================================================
sub DeleteResultNote {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['result_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['result_note_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::DeleteResultNote] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::DeleteResultNote] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			if (defined ($proj_id) || $self->GetUserRights()->{Anim}) {
				$response->{data} = DbDeleteResultNote($self, $values);
			}
			else {
				throw Mioga2::Exception("Haussmann::DeleteResultNote", __("User not owner of project or does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DeleteResultNote] delete result note failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}


# ============================================================================

=head2 SetTask (I<context>)

=over

=item Web service to updates task.

=item B<parameters :> 

=item I<context> : context with specifics arguments :

=item I<task_id> : id of task will be updated

=item I<project_id> : id of project

=item I<[label]> : label of task will be updated

=item I<[description]> : description of task will be updated

=item I<[workload]> : workload of task will be updated

=item I<[pstart]> : date of task is started

=item I<[pend]> : date of task is ended

=item I<[tags]> : tags associated to this task

=item I<[progress]> : text contains history of task progress. 
					For example : 
					"[
						{
							"date":"2012-08-01",
							"value":12
						},
						{
							"date":"2012-08-20",
							"value":15
						}
					]"

=item I<[current_progress]> : integer represent percent of current progress of task that will be updated.

=item I<[status]> : text contains history of task status.
					For example:
					"[
						{
							"status":"0", 		##### index of task status that defined in advanced parameters. ####
							"date":"2012/08/02",
							"comment":"text that comments this task status"
						},
						{
							... IDEM ...
						}
					]"

=item I<[current_status]> : Integer that represents last index of task status defined in advanced parameter of project. This index that attributes to this task as last status. 

=item I<[position_tree]> : Text array contains : id of tree container to this task, index for this task in this container and tree directory for this task.
							For example : [1,3,["treeLevel5","treeLevel2","treeLevel1"]]

=item I<[position_list]> : Text contains array contains : index of list, index of container to this task and index for this task in this container.
							For example : [2,0,5]

=item I<[position_result]> : Text contains array contains : index of result container to this task and index for this task in this container.
							For example : [2,0]

=item I<[fineblanking]> : text contains list of unit task.
					For example:
					"[
						{
							"label":"My unit task",
							"workload":200,
							"checked":true,
							"date":"2013-02-01",
							"comment":"My comment here"
						},
						{
							... IDEM ...
						}
					]"

=item I<[delegated_user_id]> : Id of user that is delegated to this task

=item I<[dstart]> : date of task is started for delegated user

=item I<[dstart]> : date of task is ended for delegated user

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => task
}

=back

=cut

# ============================================================================
sub SetTask {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Haussmann::SetTask]" if ($debug);
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['task_id'], 'disallow_empty', 'want_int' ],
		[ ['project_id'], 'disallow_empty', 'want_int' ],
		[ ['label'], 'allow_empty', 'stripxws' ],
		[ ['description'], 'stripxws', 'allow_empty' ],
		[ ['workload'], 'stripxws', 'allow_empty', 'want_int' ],
		[ ['pstart'], 'stripxws', 'allow_empty' ],
		[ ['pend'], 'stripxws', 'allow_empty' ],
		[ ['tags'], 'stripxws', 'allow_empty', ['xhtmlstring'] ],
		
		[ ['progress'], 'stripxws', 'allow_empty' ],
		[ ['current_progress'], 'stripxws', 'allow_empty', 'want_float' ],
		
		[ ['status'], 'stripxws', 'allow_empty'],
		[ ['current_status'], 'stripxws', 'allow_empty', 'want_int'],
		
		[ ['position_tree'], 'stripxws', 'allow_empty' ],
		[ ['position_list'], 'stripxws', 'allow_empty' ],
		[ ['position_result'], 'stripxws', 'allow_empty' ],
		
		[ ['fineblanking'], 'stripxws', 'allow_empty', ['xhtmlstring'] ],
		
		[ ['delegated_user_id'], 'allow_empty', 'want_int' ],
		[ ['dstart'], 'stripxws', 'allow_empty' ],
		[ ['dend'], 'stripxws', 'allow_empty' ]
	]);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	my $resultSetTask = 0;
	my $setLastAccess = 0;
	my $user_id = $context->GetUser()->Get('rowid');
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::SetTask] . ac_CheckArgs error: " . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $t_id = $self->DbVerifyDelegatedAgentOfTask($user_id, $values->{task_id});
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			if ($self->GetUserRights()->{WriteProjectIfOwner} == 0 && $self->GetUserRights()->{Anim} == 0) {
				if (exists $values->{label} ) {
					delete $values->{label};
				}
				if (exists $values->{description} ) {
					delete $values->{description};
				}
				if (exists $values->{workload} ) {
					delete $values->{workload};
				}
				if (exists $values->{pstart} ) {
					delete $values->{pstart};
				}
				if (exists $values->{pend} ) {
					delete $values->{pend};
				}
				if (exists $values->{status} ) {
					delete $values->{status};
				}
				if (exists $values->{current_status} ) {
					delete $values->{current_status};
				}
				if (exists $values->{delegated_user_id} ) {
					delete $values->{delegated_user_id};
				}
			}
			if (defined ($t_id) || defined ($proj_id) || $self->GetUserRights()->{Anim} ) {
				$resultSetTask = DbSetTask($self, $values);
				
				if ($resultSetTask == 1 && exists $values->{delegated_user_id}) {
					$resultSetTask = DbSetUser2ProjectTask($self, $values);
				}
				if ($resultSetTask == 1) {
					$values->{user_id} = $user_id;
					$setLastAccess = DbSetUserToAccessTask($self, $values);
					$response->{data} = DbGetTask ($self, $values);
				}
				else {
					print STDERR "[Mioga2::Haussmann::SetTask] set task is failed.\n";
				}
			}
			else {
				throw Mioga2::Exception("Haussmann::SetTask", __("User not owner of project and not delegated user of task and does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::SetTask] SetTask failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}

# ============================================================================

=head2 DeleteTask (I<context>)

=over

=item Web service to delete an existing task.

=item B<parameters :> 

=item I<context> : context with specifics arguments :

=item I<project_id> : id of project. 

=item I<task_id> : id of task that will be deleted. 

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => id of deleted task
}

=back

=cut

# ============================================================================
sub DeleteTask {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['task_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::DeleteTask] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	my $user_id = $context->GetUser()->Get('rowid');
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::DeleteTask] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
				if (defined ($proj_id) || $self->GetUserRights()->{Anim}) {
					$response->{data} = DbDeleteTask($self, $values);
				}
				else {
					throw Mioga2::Exception("Haussmann::DeleteTask", __("User not owner of project or does not have Anim right"));
				}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DeleteTask] delete task failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}

# ============================================================================

=head2 SetTaskList (I<context>)

=over

=item Web service to updates a list of task (s).

=item B<parameters :> 

=item I<context> : context with specifics arguments :

=item I<task_list> : Text contains array contains Hash contains task. See subroutine SetTask. 

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => 1 is taskList updated with success OR task if new task is created and task list is updated
}

=back

=cut

# ============================================================================
sub SetTaskList {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Haussmann::SetTaskList]" if ($debug);
	my $taskList = [];
	my $resultSetTask = 0;
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['task_list'], 'stripxws', 'disallow_empty' ]
	]);
	
	if (not @$errors) {
		$values->{task_list} = JSON::XS::decode_json($values->{task_list});
		foreach  my $task (@{$values->{task_list}}) {
			$context->{args} = $task;
			if (exists $task->{task_id}) {
				($values, $errors) = ac_CheckArgs($context, [
					[ ['task_id'], 'disallow_empty', 'want_int' ],
					[ ['project_id'], 'disallow_empty', 'want_int' ],
					[ ['position_tree'], 'stripxws', 'disallow_empty' ],
					[ ['position_list'], 'stripxws', 'disallow_empty' ],
					[ ['position_result'], 'stripxws', 'disallow_empty' ],
					[ ['set_modified'], 'stripxws', 'allow_empty', [ 'match', 'f' ] ]
				]);
			}
			else {
				($values, $errors) = ac_CheckArgs($context, [
					[ ['label'], 'disallow_empty', 'stripxws' ],
					[ ['project_id'], 'disallow_empty', 'want_int' ],
					[ ['position_tree'], 'stripxws', 'disallow_empty' ],
					[ ['position_list'], 'stripxws', 'disallow_empty' ],
					[ ['position_result'], 'stripxws', 'disallow_empty' ],
					[ ['set_modified'], 'stripxws', 'allow_empty', [ 'match', 'f' ] ]
				]);
			}
			if (@$errors) {
				$response->{status} = "KO";
				foreach my $error (@$errors) {
					push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
					print STDERR "[Mioga2::Haussmann::SetTaskList] . ac_CheckArgs error:\n" . Dumper $response if ($debug);
				}
			}
			else {
				push @$taskList, $task;
			}
		}
	}
	else {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::SetTaskList] . ac_CheckArgs error:\n" . Dumper $response if ($debug);
		}
	}

	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::SetTaskList] . ac_CheckArgs error:\n" . Dumper $response if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');

			foreach my $task (@$taskList) {
				my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $task->{project_id});
				if (exists $task->{task_id}) {
					if (defined ($proj_id) || $self->GetUserRights()->{Anim} ) {
						$resultSetTask = DbSetTask($self, $task);
						if (not exists $task->{set_modified}) {
							$task->{user_id} = $user_id;
							DbSetUserToAccessTask($self, $task);
						}
					}
					else {
						throw Mioga2::Exception("Haussmann::SetTaskList", __("User not owner of project or does not have Anim right"));
					}
				}
				else {
					if (defined ($proj_id) || $self->GetUserRights()->{Anim}) {
						my $new_task_id = DbCreateTask($self, $task);
						my $params = {
							task_id => $new_task_id,
							user_id => $user_id
						};
						DbSetUserToAccessTask($self, $params);
						$response->{data} = DbGetTask($self, $params);
					}
					else {
						throw Mioga2::Exception("Haussmann::SetTaskList", __("User not owner of project or does not have Anim right"));
					}
				}
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::SetTaskList] SetTaskList failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}
# ============================================================================

=head2 CreateTaskNote (I<context>)

=over

=item Web service to creates a task note.

=item B<parameters :> 

=item I<context> : context with specifics arguments :

=item I<project_id> : id of project

=item I<task_id> : Id of task that note will be associatd

=item I<[text]> : Text of this note

=back

=item B<return :> hash contains :
{
	status => OK or KO,
	errors  => [],
	data    => taskNote
}

=back

=cut

# ============================================================================
sub CreateTaskNote {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['task_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['text'], 'stripxws', 'allow_empty' ]
	]);
	print STDERR "[Mioga2::Haussmann::CreateTaskNote] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::CreateTaskNote] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			my $t_id = $self->DbVerifyDelegatedAgentOfTask($user_id, $values->{task_id});
			
			if ( $self->GetUserRights()->{Anim} || $self->GetUserRights()->{WriteProjectIfOwner} || $self->GetUserRights()->{WriteTask}) {
				$values->{group_id} = $context->GetGroup()->Get('rowid');
				$values->{owner_id} = $context->GetUser()->Get('rowid');
				my $request_result = DbCreateTaskNote($self, $values);
				if ($request_result->{result} == 1) {
					$values->{task_note_id} = $request_result->{task_note_id};
					$values->{user_id} = $values->{owner_id};
					$response->{data} = DbGetTaskNote($self, $values);
				}
				else {
					print STDERR "[Mioga2::Haussmann::CreateTaskNote] create task note failed.\n";
				}
			}
			else {
				throw Mioga2::Exception("Haussmann::CreateTaskNote", __("User not owner of project and not delegated user of task and does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::CreateTaskNote] create task note failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}
 
# ============================================================================

=head2 SetTaskNote (I<context>)

=over

=item Web service to updates an existing task note.

=item B<parameters :>

=item I<context> : context with specifics arguments :

=item I<project_id> : id of project

=item I<task_id> : Id of task that note will be associated

=item I<task_note_id> : Id task note that will be updated

=item I<[text]> : Text of this note

=back

=item B<return :> hash contains :
{
	status => OK or KO,
	errors  => [],
	data    => taskNote
}

=back

=cut

# ============================================================================
sub SetTaskNote {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['task_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['task_note_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['text'], 'stripxws', 'allow_empty' ]
	]);
	print STDERR "[Mioga2::Haussmann::SetTaskNote] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::SetTaskNote] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			my $t_id = $self->DbVerifyDelegatedAgentOfTask($user_id, $values->{task_id});
			if (defined ($proj_id) || defined ($t_id) || $self->GetUserRights()->{Anim}) {
				$values->{group_id} = $context->GetGroup()->Get('rowid');
				my $request_result = DbSetTaskNote($self, $values);
				if ($request_result == 1) {
					$response->{data} = DbGetTaskNote($self,$values);
				}
			}
			else {
				throw Mioga2::Exception("Haussmann::SetTaskNote", __("User not owner of project and not delegated user of task and does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::SetTaskNote] set task note failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}
 
# ============================================================================

=head2 AddAttendees (I<context>)

=over

=item Web service to add a list of attendees.

=item B<parameters :>

=item I<context> : context with specifics arguments :

=item I<rowids> : list of rowids to add

=item I<task_id> : Id of task that users will be associated

=back

=item B<return :> hash contains :
{
	status => OK or KO,
	errors  => [],
	data    => attendee list
}

=back

=cut

# ============================================================================
sub AddAttendees {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Haussmann::AddAttendees]\n" if ($debug);

	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['task_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['rowids'], 'stripxws', 'disallow_empty' ],
	]);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::AddAttendees] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			my $t_id = $self->DbVerifyDelegatedAgentOfTask($user_id, $values->{task_id});
			if (defined ($proj_id) || defined ($t_id) || $self->GetUserRights()->{Anim}) {
				$values->{rowids} =~ s/^\[//;
				$values->{rowids} =~ s/\]$//;
				print STDERR "rowids = $values->{rowids}\n" if ($debug);
				my @rowids = split(/,/, $values->{rowids});
				print STDERR "rowids = @rowids\n" if ($debug);
				$response->{data} = $self->DbAddAttendees($values->{task_id}, \@rowids);
			}
			else {
				throw Mioga2::Exception("Haussmann::AddAttendees", __("User not owner of project and not delegated user of task and does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::AddAttendees] set task note failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}
 
# ============================================================================

=head2 RemoveAttendee (I<context>)

=over

=item Web service to remove one attendee.

=item B<parameters :>

=item I<context> : context with specifics arguments :

=item I<user_id> : rowid of attendee to remove

=item I<task_id> : Id of task that attendee is associated

=back

=item B<return :> hash contains :
{
	status => OK or KO,
	errors  => [],
	data    => attendee list
}

=back

=cut

# ============================================================================
sub RemoveAttendee {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Haussmann::RemoveAttendee]\n" if ($debug);

	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['task_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['user_id'], 'stripxws', 'disallow_empty', 'want_int' ],
	]);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::RemoveAttendee] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			my $t_id = $self->DbVerifyDelegatedAgentOfTask($user_id, $values->{task_id});
			if (defined ($proj_id) || defined ($t_id) || $self->GetUserRights()->{Anim}) {
				$response->{data} = $self->DbRemoveAttendee($values->{task_id}, $values->{user_id});
			}
			else {
				throw Mioga2::Exception("Haussmann::RemoveAttendee", __("User not owner of project and not delegated user of task and does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::RemoveAttendee] set task note failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}

# ============================================================================

=head2 DeleteTaskNote (I<context>)

=over

=item Web service to deletes an existing task note

=item B<parameters :>

=item I<context> : context with specifics arguments :

=item I<project_id> : id of project

=item I<task_id> : Id of task that note is associatd

=item I<task_note_id> :  Id of task note that will be deleted

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => if of deleted task note
}

=back

=cut

# ============================================================================
sub DeleteTaskNote {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['task_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['task_note_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::DeleteTaskNote] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::DeleteTaskNote] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			if (defined ($proj_id) || $self->GetUserRights()->{Anim}) {
				$response->{data} = DbDeleteTaskNote($self, $values);
			}
			else {
				throw Mioga2::Exception("Haussmann::DeleteTaskNote", __("User not owner of project or does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DeleteTaskNote] delete task note failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}

# ============================================================================

=head2 SetModel (I<context>)

=over

=item Web service to creates or updates a project model

=item B<parameters :> 

=item I<context> : context with specifics arguments :

=item I<label> : Label of this model

=item I<advanced_params> : advanced parameters will be writen as new model

=item I<[model_id]> : id of model that will be updated

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => model
}

=back

=cut

# ============================================================================
sub SetModel {
	my ($self, $context) = @_;

	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['label'], 'stripxws', 'disallow_empty' ],
		[ ['advanced_params'], 'stripxws', 'disallow_empty' ],
		[ ['model_id'], 'stripxws', 'allow_empty', 'want_int' ],
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::SetModel] args : " . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	my $session = $context->GetSession();
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::SetModel] . ac_CheckArgs error: " . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			$values->{group_id} = $context->GetGroup()->Get('rowid');
			$response->{data} = DbSetModel($self, $values);
			if (exists $response->{data}->{error}) {
				$response->{status} = "KO";
				push (@{$response->{errors}}, $response->{data}->{error});
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::SeModel] model update or created failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}

# ============================================================================

=head2 SetProject (I<context>)

=over

=item Web service to updates an existing project.

=item B<parameters :>

=item I<context> : context with specifics arguments :

=item I<project_id> : Id of project that will be updated

=item I<label> : Label of project

=item I<started> : Start date of project

=item I<planned> : Prediction date of endding project

=item I<use_result> : Boolean value that determines if result is used or not for this project

=item I<[ended]> : Effective end date of project

=item I<[description]> : Description of projet

=item I<[bgcolor]> : code of background color

=item I<[color]> : code of color

=item I<[current_status]> : current status of project

=item I<[status]> : history status of project

=item I<[base_dir]> : directory of project

=item I<[tags]> : list of tags associates with this project

=item I<[complementary_data]> : complementary data of project

=item I<[advanced_params]> : advanced parameters to this project

=item I<[model_id]> : id of model that will be associated to this project.

=item I<[task_tree]> : Array of tree view structure.

=back

=item B<return :> return list of errors.

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => project
}

=cut

# ============================================================================
sub SetProject {
	my ($self, $context) = @_;

	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['label'], 'disallow_empty', 'stripxws' ],
		[ ['started'], 'stripxws', 'disallow_empty' ],
		[ ['planned'], 'stripxws', 'disallow_empty' ],
		[ ['bgcolor'], 'stripxws', 'allow_empty' ],
		[ ['color'], 'stripxws', 'allow_empty' ],
		[ ['use_result'], 'stripxws', 'disallow_empty', 'bool' ],
		[ ['ended'], 'stripxws', 'allow_empty' ],
		[ ['description'], 'stripxws', 'allow_empty' ],
		[ ['current_status'], 'stripxws', 'allow_empty', 'want_int' ],
		[ ['base_dir'], 'stripxws', 'allow_empty', ['max' => 150], ['xhtmlstring'] ],
		[ ['tags'], 'stripxws', 'allow_empty', ['max' => 150], ['xhtmlstring'] ],
		[ ['status'], 'stripxws', 'allow_empty', ['xhtmlstring'] ],
		[ ['complementary_data'], 'stripxws', 'allow_empty' ],
		[ ['task_tree'], 'stripxws', 'allow_empty' ],
		[ ['advanced_params'], 'allow_empty' ],
		[ ['model_id'], 'stripxws', 'allow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::SetProject] args : " . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::SetProject] . ac_CheckArgs error: " . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			if (defined ($proj_id) || $self->GetUserRights()->{Anim}) {
				my $request_result = DbSetProject($self, $values);
				if ($request_result == 1) {
					$response->{data} = DbGetProject ($self, $values);
				}
			}
			else {
				throw Mioga2::Exception("Haussmann::SetProject", __("User not owner of project or does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::SeProject] set project failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}

#===============================================================================

=head2 DeleteProject

=item Web service that removes a project.

=head3 Incoming Arguments

=over

=item B<project_id>: The rowid of the project that will be deleted.

=back

=back

B<Note:> 	This method removes the current project. 
			The DB method corresponding to remove all task, result ... affected to this project and on success remove this project,
			it retrieves project list and if it disempty, associates last project as current project in session persistent variable.

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => id of removed project
}

=cut

#===============================================================================
sub DeleteProject {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::DeleteProject] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::DeleteProject] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			my $group_id = $context->GetGroup()->Get('rowid');
			my $proj_id = $self->DbVerifyOwnerOfProject($user_id, $values->{project_id});
			if (defined ($proj_id) || $self->GetUserRights()->{Anim}) {
				# delete tasks of project and project
				$response->{data} = DbDeleteProject($self, $values);
				my $args = {
					group_id => [$group_id],
					user_id => $user_id
				};
				my $session = $context->GetSession();
				my $projectList = [];
				DbDeleteCurrentProjectId ($self, $values->{project_id});
				$projectList = DbGetProjectList ($self, $args);
			}
			else {
				throw Mioga2::Exception("Mioga2::Haussmann::DeleteProject", __("User not owner of project or does not have Anim right"));
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DeleteProject] delete project failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}

# ============================================================================

=head2 DbVerifyOwnerOfProject

	Verify if current user is owner of project

	Return project_id for project.

=cut

# ============================================================================
sub DbVerifyOwnerOfProject {
	my ($self, $owner_id, $project_id) = @_;
	print STDERR "Haussmann::DBVerifyOwnerOfProject owner_id = $owner_id  project_id = $project_id\n" if ($debug);
	my $proj_id = undef;
	# Verify if user is owner of project
	# To verify IDs, the best is to get the project with all parameters and table joint
	# and get the project rowid in return
	my $sql = "SELECT project.rowid FROM project LEFT JOIN m_user_base ON m_user_base.rowid = project.owner_id "
				." WHERE project.rowid = ? AND m_user_base.rowid = ?";
	print STDERR "sql = $sql\n" if ($debug);
	my $res = $self->{db}->SelectSingle($sql, [$project_id, $owner_id]);
	print STDERR "res = ".Dumper($res)."\n" if ($debug);
	$proj_id = $res->{rowid} if (defined($res));

	return $proj_id;
}
# ============================================================================

=head2 DbVerifyDelegatedAgentOfTask

	Verify if current user is delegated agent of task

	Return task_id for task.

=cut

# ============================================================================
sub DbVerifyDelegatedAgentOfTask {
	my ($self, $delegated_id, $task_id) = @_;
	print STDERR "Haussmann::DbVerifyDelegatedAgentOfTask delegated_id = $delegated_id task_id = $task_id\n" if ($debug);
	my $t_id = undef;
	# Verify if user is delegated agent of task
	# and get the task rowid in return
	my $sql = "SELECT user2project_task.task_id FROM user2project_task "
				." WHERE user2project_task.user_id = ? AND user2project_task.task_id = ?";
	print STDERR "sql = $sql\n" if ($debug);
	my $res = $self->{db}->SelectSingle($sql, [$delegated_id, $task_id]);
	print STDERR "res = ".Dumper($res)."\n" if ($debug);
	$t_id = $res->{task_id} if (defined($res));

	return $t_id;
}
# ============================================================================

=head2 DbAddAttendees

	Add a list of attendees to a task

	Return current list of attendees

=cut

# ============================================================================
sub DbAddAttendees {
	my ($self, $task_id, $rowids) = @_;
	print STDERR "Haussmann::DbAddAttendees task_id = $task_id\n" if ($debug);
	my $sql = "INSERT INTO attendee2project_task (user_id, task_id) VALUES( ?, ?)";
	print STDERR "sql = $sql\n" if ($debug);
	foreach my $user_id (@$rowids) {
		$self->{db}->ExecSQL($sql, [$user_id, $task_id]);
		
	}

	$sql = "SELECT attendee2project_task.user_id, m_user_base.firstname,m_user_base.lastname, m_user_base.email FROM attendee2project_task,m_user_base WHERE m_user_base.rowid = attendee2project_task.user_id AND task_id = ?";
	my $attendees = $self->{db}->SelectMultiple($sql, [ $task_id ]);
	print STDERR "attendees = ".Dumper($attendees)."\n" if ($debug);
	return $attendees;
}
# ============================================================================

=head2 DbRemoveAttendee

	Remove a user for attendee list of a task

	Return current list of attendees

=cut

# ============================================================================
sub DbRemoveAttendee {
	my ($self, $task_id, $user_id) = @_;
	print STDERR "Haussmann::DbRemoveAttendee task_id = $task_id\n" if ($debug);
	my $sql = "DELETE FROM attendee2project_task WHERE user_id = ? AND task_id = ?";
	print STDERR "sql = $sql\n" if ($debug);
	$self->{db}->ExecSQL($sql, [$user_id, $task_id]);

	$sql = "SELECT attendee2project_task.user_id, m_user_base.firstname,m_user_base.lastname, m_user_base.email FROM attendee2project_task,m_user_base WHERE m_user_base.rowid = attendee2project_task.user_id AND task_id = ?";
	my $attendees = $self->{db}->SelectMultiple($sql, [ $task_id ]);
	print STDERR "attendees = ".Dumper($attendees)."\n" if ($debug);
	return $attendees;
}

#===============================================================================

=head2 GetUsers

Get list of current group users.

=head3 Incoming Arguments

=over

=item I<max>: The max user count to be returned.

=item I<lastname>: The first char of lastname with *

=item I<app_name>: The name of application

=back

=head3 Return value

=over

A Perl data structure to be converted to JSON or XML:
	[
		{
			rowid: ...,
			ident: ...,
			firstname: ...,
			lastname: ...,
			email: ...
		},
		...
	]

=back

=cut

#===============================================================================
sub GetUsers {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Haussmann::GetUsers] Entering, args: " . Dumper $context->{args} if ($debug);

	#-------------------------------------------------------------------------------
	# Check incoming arguments
	#-------------------------------------------------------------------------------
	my $params = [
			[ [ 'max' ], 'allow_empty', 'want_int' ],
			[ [ 'lastname' ], 'allow_empty', 'stripxws' ],
			[ [ 'app_name' ], 'allow_empty', [ 'match', 'Haussmann' ] ]
		];

	my ($values, $errors) = ac_CheckArgs ($context, $params);

	my $response = {
		status => "OK",
		errors => [],
		data => []
	};

	if (!@$errors) {
		my $config = $context->GetConfig ();

		# Get users
		my $userlist = $context->GetGroup()->GetExpandedUserList();
		my @users = $userlist->GetUsers ();
		
		if (exists $values->{lastname}) {
			my ($lastname) = ($values->{lastname} =~ m/(.)\*/);
			if ($lastname =~ /[A-Z]/) {
				my $list = Mioga2::UserList->new ($config, { attributes => { lastname => $lastname }, match => 'begins' });
				my @users = $list->GetUsers ();
				$response->{data} = \@users;
			}
			else {
				my @users = $userlist->GetUsers ();
				@{$response->{data}} = grep { $_->{lastname} !~ /^[A-Z]/i } @users;
			}
		} elsif ($values->{max} && (scalar (@users) > $values->{max})) {
			$response->{data} = scalar (@users);
		} else {
			$response->{data} = \@users;
		}
	}
	else {
		$response->{status} = "KO";
		$response->{errors} = \@$errors;
	}

	print STDERR "[Mioga2::Haussmann::GetUsers] Leaving, data: " . Dumper $response if ($debug);
	return $response;
}	# ----------  end of subroutine GetUsers  ----------

=head2 GetProject (I<context>)

=over

=item Web service to get a project.

=item B<parameters :> 

=item I<context> : context with specific parameter:

=item I<project_id> : Id of project that will be returned.

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    =>project
}

=back

=cut

# ============================================================================
sub GetProject {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		 [ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::GetProject] args :\n" . Dumper $values if ($debug);
	my $session = $context->GetSession();
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};

	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::GetProject] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			$values->{user_id} = $context->GetUser()->Get('rowid');
			$values->{group_id} = $context->GetGroup()->Get('rowid');
			$response->{data} = DbGetProject ($self, $values);
			my $proj_id = $self->DbVerifyOwnerOfProject($values->{user_id}, $response->{data}->{project_id});
			if (defined ($proj_id)) {
				$response->{data}->{user_is_proj_owner} = 1;
			}
			else {
				$response->{data}->{user_is_proj_owner} = 0;
			}
			foreach my $key (keys %{$response->{data}}) {
				utf8::decode($response->{data}->{$key});
			}
			DbSetCurrentProjectId ($self, $values->{user_id}, $values->{group_id}, $response->{data}->{project_id});
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DbGetProject] get project failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}
# ============================================================================

=head2 GetProjectByTask (I<context>)

=over

=item Web service to get a project according to task id.

=item B<parameters :> 

=item I<context> : context with specific parameter:

=item I<project_id> : Id of task according to project.

=back

=item B<return :> hash contains : 
{
	status => "OK" or "KO",
	errors  => [],
	data    => project
}

=back

=cut

# ============================================================================
sub GetProjectByTask {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		 [ ['task_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::GetProjectByTask] args :\n" . Dumper $values if ($debug);
	my $session = $context->GetSession();
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};

	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::GetProjectByTask] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			$values->{user_id} = $context->GetUser()->Get('rowid');
			$values->{group_id} = $context->GetGroup()->Get('rowid');
			$response->{data} = DbGetProjectByTask ($self, $values);
			if (defined $response->{data} ) {
				DbSetCurrentProjectId ($self, $values->{user_id}, $values->{group_id}, $response->{data}->{project_id});
				foreach my $key (keys %{$response->{data}}) {
					utf8::decode($response->{data}->{$key});
				}
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DbGetProjectByTask] get project failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}

# ============================================================================

=head2 GetProjectByResult (I<context>)

=over

=item Web service to get a project acording to result id.

=item B<parameters :> 

=item I<context> : context with specific parameter:

=item I<result_id> : Id of result according to project.

=back

=item B<return :> hash contains : 
{
	status => "OK" or "KO",
	errors  => [],
	data    => project
}

=back

=cut

# ============================================================================
sub GetProjectByResult {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		 [ ['result_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::GetProjectByResult] args :\n" . Dumper $values if ($debug);
	my $session = $context->GetSession();
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};

	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::GetProjectByResult] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			$values->{user_id} = $context->GetUser()->Get('rowid');
			$values->{group_id} = $context->GetGroup()->Get('rowid');
			$response->{data} = DbGetProjectByResult ($self, $values);
			if (defined $response->{data} ) {
				DbSetCurrentProjectId ($self, $values->{user_id}, $values->{group_id}, $response->{data}->{project_id});
				foreach my $key (keys %{$response->{data}}) {
					utf8::decode($response->{data}->{$key});
				}
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DbGetProjectByResult] project get failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}
# ============================================================================

=head2 GetResultList (I<context>)

=over

=item Web service that retrieve result list.

=item B<parameters :> 

=item I<context> : context with specific parameter:

=item I<project_id> : Id of project.

=back

=item B<return :> hash contains : 
{
	status => 0 or 1,
	errors  => [],
	data    => [
		{
			project 1
		},
		{
			project 2
		}
	]
}

=back

=cut

# ============================================================================
sub GetResultList {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		 [ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::GetResultList] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::GetResultList] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			$response->{data} = DbGetResultList ($self, $values);
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DbGetResultList] get result list failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}
# ============================================================================

=head2 GetResultNoteList (I<context>)

=over

=item Web service that retrieves result note list.

=item B<parameters :> 

=item I<context> : context with specific parameter:

=item I<project_id> : Id of project

=item I<result_id> : Id of result

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => [
		{
			result 1
		},
		{
			result 2
		}
	]
}

=back

=cut

# ============================================================================
sub GetResultNoteList {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		 [ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		 [ ['result_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::GetResultNoteList] args :\n" . Dumper $values if ($debug);
	$values->{user_id} = $context->GetUser()->Get('rowid');
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::GetResultNoteList] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			$response->{data} = DbGetResultNoteList ($self, $values);
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DbGetResultNoteList] get result note list failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}
# ============================================================================

=head2 GetTaskList (I<context>)

=over

=item Web service that retrieves task list.

=item B<parameters :> 

=item I<context> : context with specific parameter:

=item I<project_id> : Id of project.

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => [
		{
			task 1
		},
		{
			task 2
		}
	]
}

=back

=cut

# ============================================================================
sub GetTaskList {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		 [ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::GetTaskList] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::GetTaskList] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			$response->{data} = DbGetTaskList ($self, $values);

			foreach my $task (@{$response->{data}}) {
				my $t_id = $self->DbVerifyDelegatedAgentOfTask($user_id, $task->{task_id});
				my $last_access = DbGetUserTaskAccess ($self, $user_id, $task->{task_id});
				$task->{last_access} = $last_access->{last_access};
				if (defined ($t_id)) {
					$task->{user_is_task_delegated} = 1;
				}
				else {
					$task->{user_is_task_delegated} = 0;
				}
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DbGetTaskList] get task list failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}
# ============================================================================

=head2 GetTask (I<context>)

=over

=item Web service that retrieves task

=item B<parameters :> 

=item I<context> : context with specific parameter:

=item I<project_id> : Id of project

=item I<task_id> : Id of task

=item I<[last_access]> : date of last access of task by current user

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => task
}

=back

=cut

# ============================================================================
sub GetTask {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['task_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		[ ['last_access'], 'stripxws', 'allow_empty' ]
	]);
	print STDERR "[Mioga2::Haussmann::GetTask] args :\n" . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::GetTask] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $user_id = $context->GetUser()->Get('rowid');
			$response->{data} = DbGetTask ($self, $values);
			if (exists $response->{data}->{task_id}) {
				my $t_id = $self->DbVerifyDelegatedAgentOfTask($user_id, $response->{data}->{task_id});
				if (defined ($t_id)) {
					$response->{data}->{user_is_task_delegated} = 1;
				}
				else {
					$response->{data}->{user_is_task_delegated} = 0;
				}
				my $request_result;
				my $params = {};
				$params->{user_id} = $user_id;
				$params->{task_id} = $response->{data}->{task_id};
				
				if (exists $values->{last_access}) {
					$params->{last_access} = $values->{last_access};
					$request_result = DbSetUserToAccessTask ($self, $params);
				}
				else {
					$request_result = DbSetUserToAccessTask ($self,$params);
				}
				if ($request_result == 1) {
					my $last_access = DbGetUserTaskAccess ($self, $user_id, $response->{data}->{task_id});
					$response->{data}->{last_access} = $last_access->{last_access};
				}
			}
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DbGetTask] get task failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}

# ============================================================================

=head2 GetTaskNoteList (I<context>)

=over

=item Web service that retrieves task note list.

=item B<parameters :> 

=item I<context> : context with specific parameter:

=item I<project_id> : Id of project

=item I<task_id> : Id of task

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => [
		{
			taskNote 1
		},
		{
			taskNote 2
		}
	]
}

=back

=cut

# ============================================================================
sub GetTaskNoteList {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		 [ ['project_id'], 'stripxws', 'disallow_empty', 'want_int' ],
		 [ ['task_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::GetTaskNoteList] args :\n" . Dumper $values if ($debug);
	$values->{user_id} = $context->GetUser()->Get('rowid');
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::GetTaskNoteList] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			$response->{data} = DbGetTaskNoteList ($self, $values);
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DbGetTaskNoteList] get task note list failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}

# ============================================================================

=head2 GetProjectList (I<context>)

=over

=item Web service that retrieves list of project according to user group_id.

=item B<parameters :> 

=item I<context> : context.

=item I<projects_id> : list of project id that will be rerieved

=item I<project_detail_id> : list of project id that will be rerieved with her tasks

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => [
		{
			project 1
		},
		{
			project 2
		}
	]
}

=back

=cut

# ============================================================================
sub GetProjectList {
	my ($self, $context) = @_;
	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['projects_id'], 'stripxws', 'allow_empty', 'JSON_array_int' ],
		[ ['project_detail_id'], 'stripxws', 'allow_empty', 'JSON_array_int' ]
	]);

	print STDERR "[Mioga2::Haussmann::GetProjectList]\n" if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};

	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::GetProjectList] . ac_CheckArgs error:\n" . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			my $project_ids = undef;
			my $detail_ids = undef;
			if (exists($values->{projects_id})) {
				$values->{projects_id} = Mioga2::tools::Convert::JSONToPerl($values->{projects_id});
			}
			if (exists($values->{project_detail_id})) {
				$values->{project_detail_id} = Mioga2::tools::Convert::JSONToPerl($values->{project_detail_id});
			}
			(exists $self->{current_group_id}) ? $values->{group_id} = $self->{current_group_id} : $values->{group_id} = [$context->GetGroup()->Get('rowid')];
			
			if (exists $self->{current_group_id}) {
				$values->{group_id} = $self->{current_group_id};
			}
			else {
				$values->{group_id} = [$context->GetGroup()->Get('rowid')];
			}
			$values->{user_id} = $context->GetUser()->Get('rowid');
	
			$response->{data}->{project_list} = DbGetProjectList($self, $values);
	
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::DbGetProjectList] get projectList failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}

	return $response;
}

# ============================================================================

=head2 GetModelList (I<context>)

=over

=item Web service that retrieves list of model according to user group_id.

=item B<parameters :> 

=item I<context> : context.

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    => [
		{
			model 1
		},
		{
			model 2
		}
	]
}

=back

=cut

# ============================================================================
sub GetModelList {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Haussmann::GetModelList]\n" if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	my $values = {};

	$self->{db}->BeginTransaction ();
	try {
		$values = {
			group_id => $context->GetGroup()->Get('rowid')
		};
		$response->{data} = DbGetModelList($self, $values);
		$self->{db}->EndTransaction ();
	}
	otherwise {
		my $err = shift;
		warn Dumper $err if $debug;
		print STDERR "[Mioga2::Haussmann::DbGetModelList] get modelList failed.\n";
		print STDERR $err->stringify ($context);
		$self->{db}->RollbackTransaction ();
		$response->{status} = "KO";
	};
	return $response;
}

# ============================================================================

=head2 GetLangList (I<context>)

=over

=item Web service that retrieves list of lang.

=item B<parameters :> 

=item I<context> : context.

=back

=item B<return :> hash contains : 
{
	status => OK or KO,
	errors  => [],
	data    =>  {
          'lang' => [
                      {
                        'locale' => 'en_US',
                        'ident' => 'en_US',
                        'rowid' => 1
                      },
                      {
                        'locale' => 'fr_FR',
                        'ident' => 'fr_FR',
                        'rowid' => 2
                      }
                    ]
    }

}

=back

=cut

# ============================================================================
sub GetLangList {
	my ($self, $context) = @_;
	print STDERR "[Mioga2::Haussmann::GetLangList]\n" if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	my $langList;
	
	$self->{db}->BeginTransaction ();
	try {
		$response->{data} = DbGetLangList($self);
		$self->{db}->EndTransaction ();
	}
	otherwise {
		my $err = shift;
		warn Dumper $err if $debug;
		print STDERR "[Mioga2::Haussmann::GetLangList] get lang list failed.\n";
		print STDERR $err->stringify ($context);
		$self->{db}->RollbackTransaction ();
		$response->{status} = "KO";
	};
	return $response;
}

# ============================================================================

=head1 PRIVATE METHODS DESCRIPTION

=cut
# ============================================================================

# ============================================================================

=head2 DbCreateProject (I<values >)

=over

=item Database method to insert a new project.

=item B<parameters :>

=over

=item I<values> : hash contains values defined in subroutine CreateProject().

=back

=item B<return :> hash contains : 
	{
		label : "string",
		project_id : int
	}

=back

=cut

# ============================================================================

sub DbCreateProject {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbCreateProject]\n" if ($debug);
	my $request_result;
	my $result;
	if (not exists $values->{model_id}) {
		$request_result = $self->{db}->ExecSQL("INSERT INTO project(created, modified, started, planned, group_id, owner_id, label) VALUES(Now(),Now(),Now(),Now(),?,?,?)",[$values->{group_id}, $values->{owner_id}, $values->{label}]);
		$result->{project_id} = $self->{db}->GetLastInsertId("project");
	}
	else {
		my $model = $self->{db}->SelectSingle("SELECT model FROM project_model WHERE rowid=?", [$values->{model_id}]);
		$request_result = $self->{db}->ExecSQL("INSERT INTO project(created, modified, started, planned, group_id, owner_id, advanced_params, label, model_id) VALUES(Now(),Now(),Now(),Now(),?,?,?,?,?)",[$values->{group_id}, $values->{owner_id}, $model->{model}, $values->{label}, $values->{model_id}]);
		$result->{project_id} = $self->{db}->GetLastInsertId("project");
	}

	if ($request_result == 1) {
		$result->{label} = $values->{label};
		$result = DbGetProject ($self, {project_id => $result->{project_id}});
		return $result;
	}
	else {
		print STDERR "[Mioga2::Haussmann::DbCreateProject] DB request to create project is failed.";
	}
}
# ============================================================================

=head2 DbCreateResult (I<values >)

=over

=item Database method to insert a new result.

=item B<parameters :>

=over

=item I<values> : hash contains values defined in subroutine : CreateResult().

=back

=item B<return :> id of created result.

=back

=cut

# ============================================================================
sub DbCreateResult {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbCreateResult] args :\n" if ($debug);
	my $result_id;

	$self->{db}->ExecSQL("INSERT INTO project_result(created, modified,label,project_id, budget) VALUES(NOW(),NOW(),?,?,?)",[$values->{label}, $values->{project_id}, $values->{budget}]);
	$result_id = $self->{db}->GetLastInsertId("project_result");
	return $result_id;
}
# ============================================================================

=head2 DbCreateTask (I<values >)

=over

=item Database method to insert a new task.

=item B<parameters :>

=over

=item I<values> : hash contains values defined in subroutine : CreateTask().

=back

=item B<return :> id of created task.

=back

=cut

# ============================================================================
sub DbCreateTask {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbCreateTask] args :\n" if ($debug);
	my $task_id;
	my $request_result;
	my $result;

	$self->{db}->ExecSQL("INSERT INTO project_task(project_id, label, position_tree, position_list, position_result) VALUES(?,?,?,?,?)",[$values->{project_id},$values->{label},$values->{position_tree},$values->{position_list},$values->{position_result}]);
	$task_id = $self->{db}->GetLastInsertId("project_task");
	$result = $task_id;
	return $result;
}

# ============================================================================
=head2 DbSetProject (I<values >)

=over

=item Database method to update project.

=item B<parameters :>

=over

=item I<values> : hash of values defined in setProject().

=back

=item B<return :> 1 if success or nothing if faillure

=back

=cut

# ============================================================================

sub DbSetProject {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbSetProject] args :\n" if ($debug);
	my $request_result;
	my $keys = [];
	my $data = [];

	push @$keys, 'modified=NOW()';
	push @$keys, 'label=?';
	push @$data, $values->{label};

	push @$keys, 'started=?';
	push @$data, $values->{started};

	push @$keys, 'planned=?';
	push @$data, $values->{planned};

	push @$keys, 'description=?';
	push @$data, $values->{description};

	push @$keys, 'base_dir=?';
	push @$data, $values->{base_dir};

	if (exists $values->{color}){
		push @$keys, 'color=?';
		push @$data, $values->{color};
	}
	
	if (exists $values->{bgcolor}){
		push @$keys, 'bgcolor=?';
		push @$data, $values->{bgcolor};
	}

	push @$keys, 'tags=?';
	push @$data, $values->{tags};

	push @$keys, 'status=?';
	push @$data, $values->{status};

	push @$keys, 'use_result=?';
	push @$data, $values->{use_result};

	push @$keys, 'complementary_data=?';
	push @$data, $values->{complementary_data};

	push @$keys, 'task_tree=?';
	push @$data, $values->{task_tree};

	push @$keys, 'advanced_params=?';
	push @$data, $values->{advanced_params};

	if ($values->{ended} eq "null"){
		push @$keys, 'ended=NULL';
	}
	else {
		push @$keys, 'ended=?';
		push @$data, $values->{ended};
	}

	if ($values->{current_status} eq ""){
		push @$keys, 'current_status=NULL';
	}
	else {
		push @$keys, 'current_status=?';
		push @$data, $values->{current_status};
	}

	if ($values->{model_id} eq ""){
		push @$keys, 'model_id=NULL';
	}
	else {
		push @$keys, 'model_id=?';
		push @$data, $values->{model_id};
	}

	push @$data, $values->{project_id};
	warn "UPDATE project SET " . join(",", @$keys) . " WHERE rowid=?" if $debug;
	warn Dumper $data if $debug;
	$request_result = $self->{db}->ExecSQL("UPDATE project SET " . join(",", @$keys) . " WHERE rowid=?" , $data);

	if ($request_result == 1) { 
		return 1;
	}
	else {
		print STDERR "[Mioga2::Haussmann::DbSetProject] DB update project is failed.\n";
	}
}

# ============================================================================

=head2 DbSetProjectModel (I<values >)

=over

=item Database method to update models of project.

=item B<parameters :>

=over

=item I<values> : hash of values defined in SetProjectModel().

=back

=item B<return :> nothing

=back

=cut

# ============================================================================
sub DbSetProjectModel {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbSetProjectModel] loaded\n"  if ($debug);
	$self->{db}->ExecSQL("UPDATE project SET modified=NOW(), model_id=?, advanced_params=? WHERE rowid=?",[$values->{model_id}, $values->{advanced_params}, $values->{project_id}]);
}

# ============================================================================

=head2 DbDeleteProject (I<values >)

=over

=item Database method to delete project.

=item B<parameters :>

=over

=item I<values> : hash of values contains project_id

=back

=item B<return :> result of DB request

=back

=cut

# ============================================================================

sub DbDeleteProject {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbDeleteProject]\n" if ($debug);
	my $request_result;
	my $sql;

	$sql = "DELETE FROM user2project_task WHERE user2project_task.task_id IN ( SELECT rowid FROM project_task WHERE project_id=?)";
	$self->{db}->ExecSQL($sql, [$values->{project_id}]);
	
	$sql = "DELETE FROM user2task_access WHERE user2task_access.task_id IN ( SELECT rowid FROM project_task WHERE project_id=?)";
	$self->{db}->ExecSQL($sql, [$values->{project_id}]);
	
	$sql = "DELETE FROM current_project_user2group_id WHERE current_project_user2group_id.current_project_id=?";
	$self->{db}->ExecSQL($sql, [$values->{project_id}]);
	
	$sql = "DELETE FROM project_task_note WHERE project_task_note.project_id=?";
	$self->{db}->ExecSQL($sql, [$values->{project_id}]);
	
	$sql = "DELETE FROM project_result_note WHERE project_result_note.project_id=?";
	$self->{db}->ExecSQL($sql, [$values->{project_id}]);
	
	$sql = "DELETE FROM project_result WHERE project_result.project_id=?";
	$self->{db}->ExecSQL($sql, [$values->{project_id}]);
	
	$sql = "DELETE FROM project_task WHERE project_task.project_id=?";
	$self->{db}->ExecSQL($sql, [$values->{project_id}]);
	
	$sql = "DELETE FROM project WHERE project.rowid=?";
	$self->{db}->ExecSQL($sql, [$values->{project_id}]);

	return $request_result;
}

# ============================================================================
=head2 DbSetResult (I<values>)

=over

=item Database method to update a result.

=item B<parameters :>

=over

=item I<values> : hash of result.

=back

=item B<return :> 1 if request is a success 

=back

=cut

# ============================================================================
sub DbSetResult {
	my ($self, $values) = @_;
	
	my $request_result = 0;
	my $keys = [];
	my $data = [];

	push @$keys, 'modified=NOW()';
	if (exists $values->{project_id}) {
		push @$keys, 'project_id=?';
		push @$data, $values->{project_id};
	}
	if (exists $values->{label}) {
		push @$keys, 'label=?';
		push @$data, $values->{label};
	}
	if (exists $values->{budget}) {
		push @$keys, 'budget=?';
		push @$data, $values->{budget};
	}
	if (exists $values->{pend}) {
		
		if ($values->{pend} ne 'null') {
			push @$keys, 'pend=?';
			push @$data, $values->{pend} ;
		}
		else {
			push @$keys, 'pend=NULL';
		}
		
	}
	if (exists $values->{tags}){
		push @$keys, 'tags=?';
		push @$data, $values->{tags};
	}
	if (exists $values->{status}) {
		push @$keys, 'status=?';
		push @$data, $values->{status};
		if (exists $values->{current_status}){
			push @$keys, 'current_status=?';
			push @$data, $values->{current_status};
		}
		else {
			push @$keys, 'current_status=NULL';
		}
	}
	if (exists $values->{checked}) {
		push @$keys, 'checked=?';
		push @$data, $values->{checked};
	}
	push @$data, $values->{result_id};

	warn "UPDATE project_result SET " . join(",", @$keys) . " WHERE rowid=?" if $debug;
	warn Dumper $data if $debug;
	$request_result = $self->{db}->ExecSQL("UPDATE project_result SET " . join(",", @$keys) . " WHERE rowid=?" , $data);

	if ($request_result == 1) { 
		return 1;
	}
	else {
		print STDERR "[Mioga2::Haussmann::DbSetResult] DB update project result is failed.\n";
	}
}
# ============================================================================

=head2 DbDeleteResult (I<values >)

=over

=item Database method to delete an existing result according toproject_id

=item B<parameters :>

=over

=item I<values> : hash contains values defined in method : DeleteResult().

=back

=item B<return :> id of deleted result

=back

=cut

# ============================================================================
sub DbDeleteResult {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbDeleteResult]\n" if ($debug);
	my $request_result;
	my $response;
#	delete result note according to task_id.
	$request_result = $self->{db}->ExecSQL("DELETE FROM project_result_note WHERE result_id=?",[$values->{result_id}]);
	$request_result = $self->{db}->ExecSQL("DELETE FROM project_result WHERE rowid=?",[$values->{result_id}]);

#	$request_result = $self->{db}->ExecSQL("DELETE FROM project_task WHERE rowid=?",[$values->{task_id}]);
	warn "result DB delete result.\n" if $debug;
	warn Dumper $request_result if $debug;
	return $values->{result_id};
}
# ============================================================================

=head2 DbCreateResultNote (I<values >)

=over

=item Database method to create a new Result note.

=item B<parameters :>

=over

=item I<values> : hash contains values defined in method : CreateResultNote().

=back

=item B<return :> 1 if request is a success or 0 if error

=back

=cut

# ============================================================================
sub DbCreateResultNote {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbCreateResultNote] args :\n" if ($debug);
	my $result_note_id;
	my $request_result;
	my $result = {};

	$request_result = $self->{db}->ExecSQL(
		"INSERT INTO project_result_note(created, modified,project_id, result_id, text, group_id, owner_id) 
		VALUES(NOW(),NOW(),?,?,?,?,?)",[$values->{project_id},$values->{result_id},$values->{text},$values->{group_id},$values->{owner_id}]);
	$result_note_id = $self->{db}->GetLastInsertId("project_result_note");
	$result->{result_note_id} = $result_note_id;

	if ($request_result == 1) { 
		$result->{result} = 1;
	}
	else {
		$result->{result} = 0;
	}
	return $result;
}
# ============================================================================

=head2 DbSetResultNote (I<values >)

=over

=item Database method to update an existing result note according to result_note_id, group_id, project_id and result_id

=item B<parameters :>

=over

=item I<values> : hash contains values defined in method : SetResultNote().

=back

=item B<return :> 1 if request is success or 0

=back

=cut

# ============================================================================
sub DbSetResultNote {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbSetResultNote] args :\n" if ($debug);
	my $request_result;
	
	$request_result = $self->{db}->ExecSQL("UPDATE project_result_note SET modified=NOW(), text=? WHERE rowid=? AND group_id=? AND project_id=? AND result_id=?",[$values->{text},$values->{result_note_id},$values->{group_id},$values->{project_id},$values->{result_id}]);
	if ($request_result == 1) {
		return 1;
	}
	else {
		return 0;
	}
}

# ============================================================================

=head2 DbDeleteResultNote (I<values >)

=over

=item Database method to delete an existing result note according to result_note_id, group_id, project_id and result_id

=item B<parameters :>

=over

=item I<values> : hash contains values defined in method : DeleteResultNote().

=back

=item B<return :> empty string

=back

=cut

# ============================================================================
sub DbDeleteResultNote {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbDeleteResultNote]\n" if ($debug);
	my $request_result;

	$request_result = $self->{db}->ExecSQL("DELETE FROM project_result_note WHERE rowid=? AND project_id=? AND result_id=?",[$values->{result_note_id},$values->{project_id},$values->{result_id}]);
	warn "result DB delete result note.\n" if $debug;
	warn Dumper $request_result if $debug;
	return "";
}
# ============================================================================
=head2 DbSetTask (I<values>)

=over

=item Database method to update a task.

=item B<parameters :>

=over

=item I<values> : hash of task.

=back

=item B<return :> 1 if request is success

=back

=cut

# ============================================================================
sub DbSetTask {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbSetTask]\n" if ($debug);
	my $request_result = 0;
	my $keys = [];
	my $data = [];

	if (not exists $values->{set_modified}) {
		push @$keys, 'modified=NOW()';
	}
	
	if (exists $values->{project_id}) {
		push @$keys, 'project_id=?';
		push @$data, $values->{project_id};
	}
	if (exists $values->{label}) {
		push @$keys, 'label=?';
		push @$data, $values->{label};
	}
	if (exists $values->{description}){
		push @$keys, 'description=?';
		push @$data, $values->{description};
	}
	if (exists $values->{workload}) {
		push @$keys, 'workload=?';
		push @$data, $values->{workload};
	}
	if (exists $values->{tags}){
		push @$keys, 'tags=?';
		push @$data, $values->{tags};
	}
	if (exists $values->{progress}) {
		push @$keys, 'progress=?';
		push @$data, $values->{progress};
		if (exists $values->{current_progress}) {
			push @$keys, 'current_progress=?';
			push @$data, $values->{current_progress};
		}
		else {
			push @$keys, 'current_progress=NULL';
		}
	}
	if (exists $values->{status}) {
		push @$keys, 'status=?';
		push @$data, $values->{status};
		if (exists $values->{current_status}){
			push @$keys, 'current_status=?';
			push @$data, $values->{current_status};
		}
		else {
			push @$keys, 'current_status=NULL';
		}
	}
	if (exists $values->{fineblanking}){
		push @$keys, 'fineblanking=?';
		push @$data, $values->{fineblanking};
	}
	if (exists $values->{position_tree}) {
		push @$keys, 'position_tree=?';
		push @$data, $values->{position_tree};
	}
	if (exists $values->{position_list}) {
		push @$keys, 'position_list=?';
		push @$data, $values->{position_list};
	}
	if (exists $values->{position_result}) {
		push @$keys, 'position_result=?';
		push @$data, $values->{position_result};
	}
	if (exists $values->{pstart}) {
		if ($values->{pstart} eq "null"){
			push @$keys, 'pstart=NULL';
		}
		else {
			push @$keys, 'pstart=?';
			push @$data, $values->{pstart};
		}
	}
	if (exists $values->{pend}) {
		if ($values->{pend} eq "null"){
			push @$keys, 'pend=NULL';
		}
		else {
			push @$keys, 'pend=?';
			push @$data, $values->{pend};
		}
	}

	push @$data, $values->{task_id};
	
	warn "UPDATE project_task SET " . join(",", @$keys) . " WHERE rowid=?" if $debug;
	warn Dumper $data if $debug;
	$request_result = $self->{db}->ExecSQL("UPDATE project_task SET " . join(",", @$keys) . " WHERE rowid=?" , $data);

	#	If request is sucess , return 1, else return 0;
	if ($request_result == 1) { 
		return 1;
	}
	else {
		print STDERR "[Mioga2::Haussmann::DbSetTask] DB update project task is failed.\n";
	}
}

# ============================================================================
=head2 DbSetUser2ProjectTask (I<values>)

=over

=item Database method to create or update a task user affectation.

=item B<parameters :>

=over

=item I<values> : hash of task.

=back

=item B<return :> 1 is request is success

=back

=cut

# ============================================================================
sub DbSetUser2ProjectTask {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbSetUser2ProjectTask]\n" if ($debug);

	my $request_result = 0;
	my $keys = [];
	my $data = [];

	push @$keys, 'user_id=?';
	push @$data, $values->{delegated_user_id};
	if (exists $values->{dstart}) {
		if ($values->{dstart} eq "null"){
			push @$keys, 'dstart=NULL';
		}
		else {
			push @$keys, 'dstart=?';
			push @$data, $values->{dstart};
		}
	}
	if (exists $values->{dend}) {
		if ($values->{dend} eq "null"){
			push @$keys, 'dend=NULL';
		}
		else {
			push @$keys, 'dend=?';
			push @$data, $values->{dend};
		}
	}
	
	push @$data, $values->{task_id};
	warn Dumper "UPDATE user2project_task SET " . join(",", @$keys) . " WHERE task_id=?" if $debug;
	warn Dumper $data if $debug;
	$request_result = $self->{db}->ExecSQL("UPDATE user2project_task SET " . join(",", @$keys) . " WHERE task_id=?" , $data);
	#	If request is sucess , return 1, else return 0;
	if ($request_result == 1) { 
		return 1;
	}
	else {
		$request_result = $self->{db}->ExecSQL("INSERT INTO user2project_task (user_id, task_id) VALUES(?,?)" , [$values->{delegated_user_id}, $values->{task_id}]);
		if ($request_result == 1) { 
			return 1;
		}
		else {
			print STDERR "[Mioga2::Haussmann::DbSetUser2ProjectTask] DB create User2ProjectTask is failed.\n";
		}
	}
}

# ============================================================================

=head2 DbDeleteTask (I<values >)

=over

=item Database method to delete an existing task

=item B<parameters :>

=over

=item I<values> : hash contains values defined in method : DeleteTask().

=back

=item B<return :> id of deleted task

=back

=cut

# ============================================================================
sub DbDeleteTask {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbDeleteTask]\n" if ($debug);
	my $request_result;
	my $response;
#	delete task note according to task_id.
	$request_result = $self->{db}->ExecSQL("DELETE FROM user2project_task WHERE task_id=?",[$values->{task_id}]);
	if ($request_result == "1") {
		$self->{db}->ExecSQL("DELETE FROM project_task_note WHERE task_id=?",[$values->{task_id}]);
	}
	$request_result = $self->{db}->ExecSQL("DELETE FROM user2task_access WHERE task_id=?",[$values->{task_id}]);
	$request_result = $self->{db}->ExecSQL("DELETE FROM project_task WHERE rowid=?",[$values->{task_id}]);
	warn "result DB delete task.\n" if $debug;
	warn Dumper $request_result if $debug;
	return $values->{task_id};
}

# ============================================================================
=head2 DbSetTaskList (I<values>)

=over

=item Database method to update each task of list getted in argument.

=item B<parameters :>

=over

=item I<values> : array of task.

=back

=item B<return :> nothing.

=back

=cut

# ============================================================================
sub DbSetTaskList {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbSetTaskList]\n" if ($debug);
	for (my $indexTask = 0; $indexTask < scalar(@$values); $indexTask ++) {
		$self->{db}->ExecSQL("UPDATE project_task SET modified=?, label=?, description=?, manager_id=?, workload=?, progress=?, current_progress=?, pstart=?, pend=?, status=?, current_status=?, priority=?, tags=?, position_tree=?, position_list=?, position_result=? WHERE rowid=? ",
		[ $values->[$indexTask]->{modified},$values->[$indexTask]->{label}, $values->[$indexTask]->{description}, $values->[$indexTask]->{manager_id}, $values->[$indexTask]->{workload}, $values->[$indexTask]->{progress}, $values->[$indexTask]->{current_progress}, $values->[$indexTask]->{planned}, $values->[$indexTask]->{ended}, $values->[$indexTask]->{status}, $values->[$indexTask]->{current_status}, $values->[$indexTask]->{priority}, $values->[$indexTask]->{tags}, $values->[$indexTask]->{position_tree},$values->[$indexTask]->{position_list},$values->[$indexTask]->{position_result},  $values->[$indexTask]->{task_id}]);
	}
}

# ============================================================================

=head2 DbCreateTaskNote (I<values >)

=over

=item Database method to create a new task note.

=item B<parameters :>

=over

=item I<values> : hash contains values defined in method : CreateTaskNote().

=back

=item B<return :> id of created task note.

=back

=cut

# ============================================================================
sub DbCreateTaskNote {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbCreateTaskNote] args :\n" if ($debug);
	my $task_note_id;
	my $request_result;
	my $result = {};

	$request_result = $self->{db}->ExecSQL(
		"INSERT INTO project_task_note(created, modified,project_id, task_id, text, group_id, owner_id) 
		VALUES(NOW(),NOW(),?,?,?,?,?)",[$values->{project_id},$values->{task_id},$values->{text},$values->{group_id},$values->{owner_id}]);
	$task_note_id = $self->{db}->GetLastInsertId("project_task_note");
	$result->{task_note_id} = $task_note_id;

	if ($request_result == 1) { 
		$result->{result} = 1;
	}
	else {
		$result->{result} = 0;
	}
	return $result;
}

# ============================================================================

=head2 DbSetTaskNote (I<values >)

=over

=item Database method to update an existing task note according to task_note_id, group_id, project_id and task_id

=item B<parameters :>

=over

=item I<values> : hash contains values defined in method : SetTaskNote().

=back

=item B<return :> 1 if request is success or 0

=back

=cut

# ============================================================================
sub DbSetTaskNote {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbSetTaskNote] args :\n" if ($debug);
	my $request_result;
	
	$request_result = $self->{db}->ExecSQL("UPDATE project_task_note SET modified=NOW(), text=? WHERE rowid=? AND group_id=? AND project_id=? AND task_id=?",[$values->{text},$values->{task_note_id},$values->{group_id},$values->{project_id},$values->{task_id}]);
	if ($request_result == 1) {
		return 1;
	}
	else {
		return 0;
	}
}

# ============================================================================

=head2 DbDeleteTaskNote (I<values >)

=over

=item Database method to delete an existing task note according to task_note_id, group_id, project_id and task_id

=item B<parameters :>

=over

=item I<values> : hash contains values defined in method : DeleteTaskNote().

=back

=item B<return :> empty string

=back

=cut

# ============================================================================
sub DbDeleteTaskNote {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbDeleteTaskNote]\n" if ($debug);
	my $request_result;

	$request_result = $self->{db}->ExecSQL("DELETE FROM project_task_note WHERE rowid=? AND project_id=? AND task_id=?",[$values->{task_note_id},$values->{project_id},$values->{task_id}]);
	warn "result DB delete task note.\n" if $debug;
	warn Dumper $request_result if $debug;
	return "";
}

# ============================================================================
=head2 DbSetModel (I<values>)

=over

=item Database method to insert model if dont already exists or update projects model.

=item B<parameters :>

=over

=item I<values> : hash of values defined in SetModel().

=back

=item B<return :> model created or model updated

=back

=cut

# ============================================================================

sub DbSetModel {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbSetModel] args :\n" if ($debug);
	warn Dumper $values if $debug;

	my $response = $values;
	my $request_result;
	my $countModel;

	if (exists($values->{model_id})) {
		$countModel = $self->{db}->SelectSingle("SELECT rowid, label FROM project_model WHERE project_model.label = ? AND project_model.group_id = ?", [$values->{label}, $values->{group_id}]);
		if ($countModel->{rowid} ne "". $values->{model_id} && $countModel->{label} eq $values->{label}) {
			$response->{error} = __('This model already exists');
		}
		else {
			$self->{db}->ExecSQL("UPDATE project_model SET modified=now(), label=?, model=? WHERE rowid=?", [$values->{label}, $values->{advanced_params}, $values->{model_id}]);
		}
	}
	else {
		$countModel = $self->{db}->SelectSingle("SELECT count(rowid) FROM project_model WHERE project_model.label = ? AND project_model.group_id = ?", [$values->{label}, $values->{group_id}]);
		if ($countModel->{count} ne '0') {
			$response->{error} = __('This model already exists');
		}
		else {
			$self->{db}->ExecSQL("INSERT INTO project_model(created, modified, group_id, label, model) VALUES(now(),now(),?,?,?)", [$values->{group_id}, $values->{label}, $values->{advanced_params}]);
			$response->{model_id} = $self->{db}->GetLastInsertId("project_model");
		}
	}
	return $response;
}
# ============================================================================

=head2 DbGetProject (I<rowid>)

=over

=item Database method to retrieve a projects data

=item B<parameters :>

=over

=item I<project_id> : Id of project.

=back

=item B<return :> hash of projects data.

=back

=cut

# ============================================================================

sub DbGetProject {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbGetProject] rowid :\n" if ($debug);
	my $project;
	$project = $self->{db}->SelectSingle("
		SELECT project.rowid AS project_id,
		       project.*,
		       project.owner_id AS project_owner_id,
		       m_user_base.firstname AS project_owner_firstname,
		       m_user_base.lastname AS project_owner_lastname,
		       m_user_base.email AS project_owner_email,
		       m_group.ident AS group_ident 
		FROM project 
		     LEFT JOIN m_user_base ON m_user_base.rowid = project.owner_id,
		      m_group 
		WHERE project.rowid=? AND m_group.rowid = project.group_id ", [$values->{project_id}]);

	$project->{project_id} = delete $project->{rowid};

	return $project;
}
# ============================================================================

=head2 DbGetProjectByTask (I<rowid>)

=over

=item Database method to retrieve project according to task_id

=item B<parameters :>

=over

=item I<project_id> : Id of task.

=back

=item B<return :> hash of projects data.

=back

=cut

# ============================================================================

sub DbGetProjectByTask {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbGetProjectByTask] rowid :\n" if ($debug);
	my $project;
	$project = $self->{db}->SelectSingle("
		SELECT project.rowid AS project_id,
		       project.*,
		       project.owner_id AS project_owner_id,
		       m_user_base.firstname AS project_owner_firstname,
		       m_user_base.lastname AS project_owner_lastname,
		       m_user_base.email AS project_owner_email,
		       m_group.ident AS group_ident 
		FROM project 
		     LEFT JOIN m_user_base ON m_user_base.rowid = project.owner_id,
		     m_group 
		WHERE m_group.rowid = project.group_id AND project.rowid= ( SELECT project_id FROM project_task WHERE project_task.rowid = ?)", [$values->{task_id}]);

	return $project;
}
# ============================================================================

=head2 DbGetProjectByResult (I<rowid>)

=over

=item Database method to retrieve project according to result_id

=item B<parameters :>

=over

=item I<project_id> : Id of result

=back

=item B<return :> hash of projects data.

=back

=cut

# ============================================================================

sub DbGetProjectByResult {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbGetProjectByResult] rowid :\n" if ($debug);
	warn Dumper $values->{result_id} if $debug;
	my $project;
	$project = $self->{db}->SelectSingle("
		SELECT project.rowid AS project_id,
		       project.*,
		       project.owner_id AS project_owner_id,
		       m_user_base.firstname AS project_owner_firstname,
		       m_user_base.lastname AS project_owner_lastname,
		       m_user_base.email AS project_owner_email,
		       m_group.ident AS group_ident 
		FROM project 
		     LEFT JOIN m_user_base ON m_user_base.rowid = project.owner_id,
		     m_group 
		WHERE m_group.rowid = project.group_id AND project.rowid= ( SELECT project_id FROM project_result WHERE project_result.rowid = ?)", [$values->{result_id}]);

	return $project;
}
# ============================================================================

=head2 DbGetModel (I<values>)

=over

=item Database method to retrieve a projects model

=item B<parameters :>

=over

=item I<rowid> : id of model.

=back

=item B<return :> hash of projects model.

=back

=cut

# ============================================================================

sub DbGetModel { 
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbGetModel]\n" if ($debug);
	my $model;
	$model = $self->{db}->SelectSingle("SELECT rowid as model_id, label, model FROM project_model WHERE rowid=?", [$values->{model_id}]);
	return $model;
}

# ============================================================================

=head2 DbGetProjectList (I<group_id>)

=over

=item Database method to retrieve a projects list according to group id. It not retieves project if rowid in project_hide variable. 

=item It retrieves project task if rowid of project is in project_detail variable.

=item B<parameters :>

=over

=item I<group_id> : id of group to contains the search projects.

=item I<user_id> : id of user

=item I<projects_id> : list of project id that will be rerieved

=item I<project_detail_id> : list of project id that will be rerieved with her tasks

=back

=item B<return :> hash of projects list.

=back

=cut

# ============================================================================

sub DbGetProjectList {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbGetProjectList]\n" if ($debug);

	my $response = [];
	my $tl = [];
	my $proj_tasks = [];
	my $bind = [];
	my $str = [];
	my $where = "";
	my $filter_group_user = "";
	my $filter_proj = "";
	my $filter_task = "";

	push (@$bind,$values->{user_id});
	push (@$bind,$values->{user_id});

	if (exists($values->{projects_id}) ) {
		if (scalar @{$values->{projects_id}}) {
			$filter_proj = " AND project.rowid IN (";
			foreach my $id (@{$values->{projects_id}}) {
				push(@$bind, $id);
				push(@$str, "?");
			}
			$filter_proj = $filter_proj . join(",", @$str) . ") ";
			$str = [];
		}
		else {
			print STDERR "[Mioga2::Haussmann::DbGetProjectList] No project id to display.\n" if $debug;
		}
	}
	else {
		print STDERR "[Mioga2::Haussmann::DbGetProjectList] No projects_id exists.\n" if $debug;
	}
	
	if (scalar @{$values->{group_id}}) {
		$filter_group_user = " AND project.group_id IN (";
		foreach my $id (@{$values->{group_id}}) {
			push(@$bind, $id);
			push(@$str, "?");
		}
		$filter_group_user = $filter_group_user . join(",", @$str) . ")";
		$str = [];
		
	}
	else {
		print STDERR "[Mioga2::Haussmann::DbGetProjectList] No group that the user is invited\n" if $debug;
	}
	
	my $sql = "SELECT DISTINCT project.rowid AS project_id,
		       project.owner_id AS project_owner_id,
		       m_user_base.firstname AS project_owner_firstname,
		       m_user_base.lastname AS project_owner_lastname,
		       m_user_base.email AS project_owner_email,
		       m_group.ident AS group_ident,
		       project.label,
		       project.created,
		       project.description ,
		       project.started,
		       project.planned,
		       project.current_status,
		       project.color, 
		       project.advanced_params, 
		       CASE WHEN current_project_user2group_id.current_project_id = project.rowid AND current_project_user2group_id.user_id=? THEN 't'
		       ELSE 'f'
		       END AS current_project_id
		       
		FROM m_group, project 
		       LEFT JOIN m_user_base ON m_user_base.rowid = project.owner_id 
		       LEFT JOIN current_project_user2group_id ON current_project_user2group_id.current_project_id = project.rowid AND current_project_user2group_id.user_id=? 
		       WHERE m_group.rowid = project.group_id " 
		. $filter_proj . $filter_group_user . " ORDER BY project.started DESC";
	
	$response = $self->{db}->SelectMultiple($sql, $bind);

	foreach my $p (@$response) {
		$p->{result} = $self->{db}->SelectMultiple(
		"SELECT project_result.rowid AS result_id,
	       project_result.label, 
	       project_result.pend,
	       project_result.created 
		FROM project_result 
		WHERE project_result.project_id=? ORDER BY project_result.pend", [$p->{project_id}]);
	}

	if (exists($values->{project_detail_id})) {
		if (scalar @{$values->{project_detail_id}}) {
			$bind = [];
			$str = [];

			foreach my $id (@{$values->{project_detail_id}}) {
				push(@$bind, $id);
				push(@$str, "?");
			}
			$filter_task = join(",", @$str);

			$tl = $self->{db}->SelectMultiple(
				"SELECT project_task.rowid AS task_id,
						project_task.project_id,
						project_task.created,
						project_task.label,
						project_task.current_progress,
						project_task.pstart,
						project_task.pend 
				FROM project_task
				WHERE project_task.project_id IN (" . $filter_task . ") ORDER BY project_task.pstart", $bind
			);

			foreach my $p (@$response) {
				$proj_tasks = [];
				foreach my $task (@$tl) {
					if ($p->{project_id} == $task->{project_id}) {
						push(@$proj_tasks, $task);
					}
				}
				$p->{tasks} = $proj_tasks;
			}
		}
		else {
			print STDERR "[Mioga2::Haussmann::DbGetProjectList] No project_detail_id.\n" if $debug;
		}
	}
	else {
		print STDERR "[Mioga2::Haussmann::DbGetProjectList] No project_detail_id exists.\n" if $debug;
	}
	return $response;
}
# ============================================================================

=head2 DbSetCurrentProjectId (I<user_id, group_id, current_project_id >)

=over

=item Database method to create or update current project id according to user and group

=item B<parameters :>

=over

=item I<user_id> : id of current user

=over

=item I<group_id> : id of current user group

=over

=item I<current_project_id> : id of current project

=back

=item B<return :>

=back

=cut

# ============================================================================

sub DbSetCurrentProjectId {
	my ($self, $user_id, $group_id, $current_project_id) = @_;
	print STDERR "[Mioga2::Haussmann::DbSetCurrentProjectId]\n" if ($debug);
	my $countEntry = $self->{db}->SelectSingle("SELECT COUNT(user_id) FROM current_project_user2group_id WHERE user_id = ? AND group_id = ?", [$user_id, $group_id]);
	if ($countEntry->{count} eq '0') {
		$self->{db}->ExecSQL("INSERT INTO current_project_user2group_id (user_id, group_id, current_project_id) VALUES(?,?,?)", [$user_id, $group_id, $current_project_id]);
	}
	else {
		$self->{db}->ExecSQL("UPDATE current_project_user2group_id SET current_project_id=? WHERE user_id=? AND group_id=?", [$current_project_id, $user_id, $group_id]);
	}
}
# ============================================================================

=head2 DbDeleteCurrentProjectId (I<removed_proj_id>)

=over

=item Database method to update current project id according to user and group

=item B<parameters :>

=over

=item I<removed_proj_id> : id of project

=back

=item B<return :> 

=back

=cut

# ============================================================================
sub DbDeleteCurrentProjectId {
	my ($self, $removed_proj_id) = @_;
	$self->{db}->ExecSQL("DELETE FROM current_project_user2group_id WHERE current_project_id=?", [$removed_proj_id]);
}

# ============================================================================

=head2 DbGetResultList (I<$values>)

=over

=item Database method to retrieve all results of current project.

=item B<parameters :>

=over

=item I<project_id> : Id of current project that results is associated

=back

=item B<return :> Array of hash of result.

=back

=cut

# ============================================================================
sub DbGetResultList {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbGetResultList]\n" if ($debug);
	my $resultList;

	$resultList = $self->{db}->SelectMultiple("SELECT rowid AS result_id, project_result.* FROM project_result WHERE project_id=?", [$values->{project_id}]);

	return $resultList;
}
# ============================================================================

=head2 DbGetResult(I<$values>)

=over

=item Database method to retrieve informations of result.

=item B<parameters :>

=over

=item I<result_id> : Id of result

=back

=item B<return :> hash of informations result.

=back

=cut

# ============================================================================
sub DbGetResult {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbGetResult]\n" if ($debug);
	my $result;
	$result = $self->{db}->SelectSingle("SELECT project_result.rowid AS result_id, project_result.* FROM project_result, m_user_base WHERE project_result.rowid=?", [$values->{result_id}]);
	return $result;
}
# ============================================================================

=head2 DbGetResultNoteList (I<$values>)

=over

=item Database method to retrieve a all result note of result

=item B<parameters :>

=over

=item I<project_id> : Id of current project that results is associated

=back

=item B<return :> Array of result note contains firstname and lastname of result note owner

=back

=cut

# ============================================================================
sub DbGetResultNoteList {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbGetResultNoteList]\n" if ($debug);
	my $taskNoteList;
	my $project_id;
	my $task_id;

	$taskNoteList = $self->{db}->SelectMultiple(
		"SELECT 
			project_result_note.rowid AS result_note_id,
			project_result_note.*,
			m_user_base.firstname,
			m_user_base.lastname,
			m_user_base.email 
		FROM project_result_note 
			LEFT JOIN m_user_base ON m_user_base.rowid = project_result_note.owner_id 
		WHERE  project_id=? AND result_id=?", [$values->{project_id}, $values->{result_id}]);

	return $taskNoteList;
}
# ============================================================================

=head2 DbGetResultNote(I<$values>)

=over

=item Database method to retrieve result note 

=item B<parameters :>

=over

=item I<values> : parameter defined in subroutine GetResultNote()

=back

=item B<return :> hash of informations result note contains firstname and lastname of result note owner

=back

=cut

# ============================================================================
sub DbGetResultNote {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbGetResultNote]\n" if ($debug);
	my $resultNote;
	$resultNote = $self->{db}->SelectSingle(
		"SELECT 
			project_result_note.rowid AS result_note_id,
			project_result_note.*,
			m_user_base.firstname,
			m_user_base.lastname,
			m_user_base.email 
		FROM project_result_note 
			LEFT JOIN m_user_base ON m_user_base.rowid = project_result_note.owner_id 
		WHERE project_result_note.rowid=? AND project_result_note.project_id=? AND project_result_note.result_id=?", [ $values->{result_note_id},$values->{project_id},$values->{result_id}]);

	return $resultNote;
}
# ============================================================================

=head2 DbGetTaskList (I<$values>)

=over

=item Database method to retrieve all tasks of current project.

=item B<parameters :>

=over

=item I<project_id> : Id of current project that tasks is associated

=back

=item B<return :> Aray of hash of task.

=back

=cut

# ============================================================================
sub DbGetTaskList {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbGetTaskList]\n" if ($debug);
	my $taskList;
	my $project_id;
	my $task_id;
	$taskList = $self->{db}->SelectMultiple("
		SELECT project_task_view.rowid AS task_id,
		       project_task_view.user_id AS delegated_user_id,
		       m_user_base.firstname AS delegated_user_firstname,
		       m_user_base.lastname AS delegated_user_lastname,
		       m_user_base.email AS delegated_user_email,
		       project_task_view.* 
		FROM project_task_view
		     LEFT JOIN m_user_base ON m_user_base.rowid = project_task_view.user_id
		WHERE project_task_view.project_id=?", [$values->{project_id}]);

	if (defined($taskList)) {
		my $sql = "SELECT user_project_task_view.rowid, user_project_task_view.user_id, m_user_base.firstname,m_user_base.lastname, m_user_base.email FROM user_project_task_view,m_user_base WHERE m_user_base.rowid = user_project_task_view.user_id AND type='a' AND project_id = ?";
		my $attendees = $self->{db}->SelectMultiple($sql, [ $values->{project_id} ]);
		
		my $att_idx = {};
		foreach my $att (@$attendees) {
			push @{$att_idx->{$att->{rowid}}}, $att;
		}
		print STDERR "    att_idx = ".Dumper($att_idx)."\n" if ($debug);

		foreach my $t (@$taskList) {
			if (exists($att_idx->{$t->{rowid}})) {
				$t->{attendees} = $att_idx->{$t->{rowid}};
			}
			else {
				$t->{attendees} = [];
			}
		}
	}
	print STDERR "    taskList = ".Dumper($taskList)."\n" if ($debug);
	return $taskList;
}

# ============================================================================

=head2 DbGetUserTaskAccess (I<$values>)

=over

=item Database method to retrieve user access to task

=item B<parameters :>

=over

=item I<user_id> : Id of user

=over

=item I<task_id> : Id of task

=back

=item B<return :> Aray of hash of task.

=back

=cut

# ============================================================================
sub DbGetUserTaskAccess {
	my ($self, $user_id, $task_id) = @_;

	print STDERR "[Mioga2::Haussmann::DbGetUserTaskAccess]\n" if ($debug);

	my $user_access = $self->{db}->SelectSingle("SELECT * FROM user2task_access WHERE user_id=? AND task_id=?", [$user_id, $task_id]);
	return $user_access;
}

# ============================================================================

=head2 DbGetTask(I<$values>)

=over

=item Database method to retrieve project task.

=item B<parameters :>

=over

=item I<task_id> : Id of task

=back

=item B<return :> hash of informations task.

=back

=cut

# ============================================================================
sub DbGetTask {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbGetTask]\n" if ($debug);
	my $keys = [];
	my $data = [];

	push @$keys, 'project_task_view.rowid=?';
	push @$data, $values->{task_id};

	if (exists($values->{delegated_user_id})) {
		push @$keys, 'm_user_base.rowid=?';
		push @$data, $values->{delegated_user_id};
	}
	my $task;
	$task = $self->{db}->SelectSingle("
		SELECT project_task_view.rowid AS task_id,
		       project_task_view.*,
		       m_user_base.firstname AS delegated_user_firstname,
		       m_user_base.lastname AS delegated_user_lastname,
		       m_user_base.email AS delegated_user_email,
		       project_task_view.user_id AS delegated_user_id 
		FROM project_task_view
		     LEFT JOIN m_user_base ON m_user_base.rowid = project_task_view.user_id
		WHERE " . join(" AND ", @$keys), $data);

	if (defined($task)) {
		my $sql = "SELECT attendee2project_task.user_id, m_user_base.firstname,m_user_base.lastname, m_user_base.email FROM attendee2project_task,m_user_base WHERE m_user_base.rowid = attendee2project_task.user_id AND task_id = ?";
		$task->{attendees} = $self->{db}->SelectMultiple($sql, [ $values->{task_id} ]);
	}

	return $task;
}



# ============================================================================

=head2 SetUserToAccessTask(I<$values>)

=over

=item method to create or update user task access

=item B<parameters :>

=over

=item I<user_id> : Id of user

=over

=item I<task_id> : Id of task

=over

=item I<[last_access]> : date of last task access by current user

=back

=item B<return :> nothing

=back

=cut

# ============================================================================
sub SetUserToAccessTask {
	my ($self, $context) = @_;

	my ($values, $errors) = ac_CheckArgs($context, [
		[ ['task_id'], 'stripxws', 'disallow_empty', 'want_int' ]
	]);
	print STDERR "[Mioga2::Haussmann::SetUserToAccessTask] args : " . Dumper $values if ($debug);
	my $response = {
		status => "OK",
		errors => [],
		data => {}
	};
	my $session = $context->GetSession();
	if (@$errors) {
		$response->{status} = "KO";
		foreach my $error (@$errors) {
			push (@{$response->{errors}}, { $error->[0] => $error->[1]->[0] });
			print STDERR "[Mioga2::Haussmann::SetUserToAccessTask] . ac_CheckArgs error: " . Dumper $error if ($debug);
		}
	}
	else {
		$self->{db}->BeginTransaction ();
		try {
			$values->{user_id} = $context->GetUser()->Get('rowid');
			DbSetUserToAccessTask($self, $values);
			$self->{db}->EndTransaction ();
		}
		otherwise {
			my $err = shift;
			warn Dumper $err if $debug;
			print STDERR "[Mioga2::Haussmann::SetUserToAccessTask] update last task access is failed.\n";
			print STDERR $err->stringify ($context);
			$self->{db}->RollbackTransaction ();
			$response->{status} = "KO";
		};
	}
	return $response;
}
# ============================================================================

=head2 DbSetUserToAccessTask(I<$values>)

=over

=item Database method to create or update user task access

=item B<parameters :>

=over

=item I<user_id> : Id of user

=over

=item I<task_id> : Id of task

=over

=item I<[last_access]> : date of last task access by current user

=back

=item B<return :> nothing

=back

=cut

# ============================================================================
sub DbSetUserToAccessTask {
	my ($self, $values) = @_;
	print STDERR "[Mioga2::Haussmann::DbSetUserToAccessTask]\n" if ($debug);
	my $request_result;
	my $exist_row = $self->{db}->SelectSingle("SELECT * FROM user2task_access WHERE user_id=? AND task_id=?", [$values->{user_id}, $values->{task_id}]);
	if (defined ($exist_row)) {
		$request_result = $self->{db}->ExecSQL("UPDATE user2task_access SET last_access=now() WHERE user_id=? AND task_id=?", [$values->{user_id}, $values->{task_id}]);
	}
	else {
		$request_result = $self->{db}->ExecSQL("INSERT INTO user2task_access(user_id, task_id, last_access) VALUES(?,?,now())", [$values->{user_id}, $values->{task_id}]);
	}
	return $request_result;
}


# ============================================================================

=head2 DbGetTaskNoteList (I<$values>)

=over

=item Database method to retrieve all task note of task

=item B<parameters :>

=over

=item I<project_id> : Id of current project that tasks is associated

=over

=item I<task_id> : Id of task

=back

=item B<return :> Array of task note contains firstname and lastname of task note owner

=back

=cut

# ============================================================================
sub DbGetTaskNoteList {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbGetTaskNoteList]\n" if ($debug);
	my $taskNoteList;
	my $project_id;
	my $task_id;

	$taskNoteList = $self->{db}->SelectMultiple(
		"SELECT 
			project_task_note.rowid AS task_note_id,
			project_task_note.*,
			m_user_base.firstname,
			m_user_base.lastname,
			m_user_base.email 
		FROM project_task_note 
			LEFT JOIN m_user_base ON m_user_base.rowid = project_task_note.owner_id 
		WHERE  project_id=? AND task_id=?", [$values->{project_id}, $values->{task_id}]);

	return $taskNoteList;
}
# ============================================================================

=head2 DbGetTaskNote(I<$values>)

=over

=item Database method to retrieve task note 

=item B<parameters :>

=over

=item I<project_id> : id of project

=item I<task_id> : id of task

=item I<task_note_id> : id of task note

=back

=item B<return :> hash of informations task note contains firstname and lastname of task note owner

=back

=cut

# ============================================================================
sub DbGetTaskNote {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbGetTaskNote]\n" if ($debug);
	my $taskNote;
	$taskNote = $self->{db}->SelectSingle(
		"SELECT 
			project_task_note.rowid AS task_note_id,
			project_task_note.*,
			m_user_base.firstname,
			m_user_base.lastname,
			m_user_base.email 
		FROM project_task_note 
			LEFT JOIN m_user_base ON m_user_base.rowid = project_task_note.owner_id 
		WHERE project_task_note.rowid=? AND project_task_note.project_id=? AND project_task_note.task_id=?", [ $values->{task_note_id},$values->{project_id},$values->{task_id}]);
	return $taskNote;
}

# ============================================================================

=head2 DbGetModelList (I<group_id>)

=over

=item Database method to retrieve a models list according to group id.

=item B<parameters :>

=over

=item I<group_id> : Id of group to contains the search models.

=back

=item B<return :> hash of models.

=back

=cut

# ============================================================================

sub DbGetModelList {
	my ($self, $values) = @_;

	print STDERR "[Mioga2::Haussmann::DbGetModelList]\n" if ($debug);
	my $modelList;
	$modelList = $self->{db}->SelectMultiple("SELECT label, rowid AS model_id, model FROM project_model WHERE group_id=?", [$values->{group_id}]);
	return $modelList;
}

# ============================================================================

=head2 DbGetLangList (I<group_id>)

=over

=item Database method to retrieve a lang list.

=item B<parameters :>

=over

=item I<group_id> : Id of group of current user.

=back

=item B<return :> Array of lang.

=back

=cut

# ============================================================================
sub DbGetLangList {
	my ($self) = @_;
	my $langList;
	my $result;
	$result = $self->{db}->SelectMultiple("SELECT * FROM m_lang");
	$langList->{"lang"} = $result;
	return $langList;
}

# ============================================================================

=head2 RevokeUserFromGroup ($config, $group_id, $user_id, $group_animator_id)

	Remove user data in database when a user is revoked of a group.

	This function is called when :
	- a user is revoked of a group
	- a team is revoked of a group for each team member not namly
	  invited in group
	- a user is revoked of a team  for each group where the team is
	  member and the user not namly invited in group

=cut

# ============================================================================

sub RevokeUserFromGroup {
	my ($self, $config, $group_id, $user_id, $group_animator_id) = @_;

	print STDERR "Haussmann::RevokeUserFromGroup\n" if ($debug);
	my $sql_update_data;
	my $sql_delete_data;
#	Update data
	$sql_update_data = "UPDATE project SET owner_id=? WHERE owner_id=? AND group_id=?";
	$self->{db}->ExecSQL($sql_update_data, [$group_animator_id, $user_id, $group_id]);
	$sql_update_data = "UPDATE project_task_note SET owner_id=? WHERE owner_id=? AND group_id=?";
	$self->{db}->ExecSQL($sql_update_data, [$group_animator_id, $user_id, $group_id]);
	$sql_update_data = "UPDATE project_result_note SET owner_id=? WHERE owner_id=? AND group_id=?";
	$self->{db}->ExecSQL($sql_update_data, [$group_animator_id, $user_id, $group_id]);
#	Delete data
	$sql_delete_data = "DELETE FROM user2project_task 
						WHERE user2project_task.user_id=? AND user2project_task.task_id IN (
							SELECT rowid FROM project_task WHERE project_id IN (
								SELECT rowid from project WHERE group_id=?
							)
						)";
	$self->{db}->ExecSQL($sql_delete_data, [$user_id, $group_id]);
	$sql_delete_data = "DELETE FROM user2task_access 
						WHERE user2task_access.user_id=? AND user2task_access.task_id IN (
							SELECT rowid FROM project_task WHERE project_id IN (
								SELECT rowid from project WHERE group_id=?
							)
						)";
	$self->{db}->ExecSQL($sql_delete_data, [$user_id, $group_id]);
		$sql_delete_data = "DELETE FROM current_project_user2group_id WHERE current_project_user2group_id.user_id=? AND current_project_user2group_id.group_id=?";
	$self->{db}->ExecSQL($sql_delete_data, [$user_id, $group_id]);

}

# ============================================================================

=head2 DeleteGroupData ($config, $group_id)

	Remove group data in database when a group is deleted.

	This function is called when a group/user/resource is deleted.

=cut

# ============================================================================

sub DeleteGroupData {
	my ($self, $config, $group_id) = @_;
	print STDERR "Haussmann::DeleteGroupData for group_id $group_id\n" if ($debug);

	# DELETE project
	my $proj_list = DbGetProjectList($self, {group_id => [$group_id]});
	foreach my $project (@$proj_list) {
		DbDeleteProject ($self, {project_id => $project->{project_id}});
	}
	# DELETE models
	my $sql_delete_model = "DELETE FROM project_model WHERE group_id=?";
	$self->{db}->ExecSQL($sql_delete_model, [$group_id]);
}

=head1 AUTHORS

The Mioga2 Project <developers@mioga2.org>

=head1 SEE ALSO

Mioga2 Mioga2::Application Mioga2::Authen Mioga2::Authz

=head1 COPYRIGHT

Copyright (C) 2003-2012, The Mioga2 Project. All Rights Reserved.

This module is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

=cut

# ============================================================================
1;
__END__
