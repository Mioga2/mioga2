# $Header$
# ============================================================================
# Mioga II Project (C) 2003 The MiogaII Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
# $Log$
# Revision 1.2  2003/12/01 12:55:38  lsimonneau
# Remove duplicate files.
#
# Revision 1.1.1.1  2003/12/01 08:55:36  lsimonneau
# MiogaII comes to life !!
#
# ============================================================================

sub DisplayQuestionNumber {
    my ($question, $min, $max, $default) = @_;

    print "$question [$default] ";
    my $rep = <STDIN>;
    chomp($rep);

    return $rep;
}


sub DisplayQuestionList {
    my ($question, $values, $default) = @_;

    my $options = "[" . join("|", @$values) . "]";
    
    print "$question $options [$default] ";
    my $rep = <STDIN>;
    chomp($rep);

    return $rep;
}



sub DisplayQuestionAlpha {
    my ($question, $default) = @_;
    
    print "$question [$default] ";
    my $rep = <STDIN>;
    chomp($rep);   

    return $rep;
}


sub DisplayQuestionYesNo {
    my ($question, $default) = @_;
    
    print "$question [yes|no] [$default] ";
    my $rep = <STDIN>;
    chomp($rep);   

    return $rep;
}


sub DisplayInfo {
    my @infos = @_;

    print join("", @infos);
}


sub DisplayMessage {
    my @infos = @_;

    print join("", @infos)."\n";

    print "Press any key to continue ...";
    <STDIN>;
}




sub DisplayMenu {
    my ($config, $menu, $is_submenu) = @_;

    ClearScreen();
		
    my @list;
    my $i = 0;
    
    if($is_submenu) {
	$list[$i] = "previous menu";
    }
    else {
	$list[$i] = "end config";
    }
    
    $i++;


    my $maxsize = 0;
    
    foreach my $conf (@$menu)
    {
	if($conf->{type} ne 'submenu' and length($conf->{name}) > $maxsize) {
	    $maxsize = length($conf->{name});
	}
	
	$list[$i] = $conf->{name};
	$i++;
    }
    
    my $last_was_submenu = 0;
    my $nbItems = @list;
    
    print "\n\n===========================\n";
    print "\n 0 - $list[0]\n\n";
    for ($i = 1; $i < $nbItems; $i++)
    {
	if($menu->[$i-1]->{type} eq 'submenu') {
	    if(! $last_was_submenu) {
		$last_was_submenu = 1;
		print "\nSubmenu for other parameters :\n\n";
	    }
	    
	    printf("%2i - $list[$i]\n", $i);
	}
	else {
	    printf("%2i - $list[$i]", $i);
	    
	    for (my $j = length($list[$i]); $j < $maxsize; $j++) {
		print " ";
	    }
	    print "\t:\t$config->{$list[$i]}\n";
	}
	
    }
    
    print "\n";

    my $result = AskQuestionNumber("Which parameter ?", 0, $nbItems - 1, 0);
    return $result;
}


sub ClearScreen {
    system("clear");
}

1;
