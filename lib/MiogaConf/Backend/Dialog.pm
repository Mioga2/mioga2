# $Header$
# ============================================================================
# Mioga II Project (C) 2003 The MiogaII Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
# $Log$
# Revision 1.2  2003/12/01 12:55:37  lsimonneau
# Remove duplicate files.
#
# Revision 1.1.1.1  2003/12/01 08:55:36  lsimonneau
# MiogaII comes to life !!
#
# ============================================================================

use POSIX qw(tmpnam);
use Term::ReadKey;

my $Info;
my $InfoPid;

sub escape_string {
    my $string = shift;
    $string =~ s/"/\\"/g; #'

    return "\"$string\"";
}

sub get_screen_size {
    my $cmdline = `which dialog`;
    chomp $cmdline;

    my @size = GetTerminalSize();

    return ($size[1], $size[0]);
}


sub display_dialog {
    my $options = shift;

    my $cmdline = `which dialog`;
    chomp $cmdline;

    my $cmd = "$cmdline --cr-wrap --stdout";
    $cmd .= " --backtitle 'Mioga configuration' ";

    $cmd .= join(' ', @{$options->{options}});
    $cmd .= " --title ".escape_string($options->{title});
    
    $cmd .= " --$options->{widget} ";
    $cmd .= escape_string($options->{text});
    $cmd .= " $options->{height} $options->{width} ";
				      
    $cmd .= join(' ', @{$options->{args}});

    my $res;
    if($options->{widget} ne 'tailboxbg') {
	if(defined $Info) {
	    sleep(2);
	    
	    kill(15, $InfoPid);
	    close($Info);
	    undef $Info;
	}
    
	$cmd .= " 2> /dev/null";	    

	$res = `$cmd`;

	if($options->{widget} eq 'yesno') {
	    return $?;
	}
	else {
	    return $res;
	}
    }
    else {
	$cmd .= " 2>&1 > /dev/null";

	open(F_TAIL, "$cmd |")
	    or die "Can't run dialog : $!";
	
	$res = <F_TAIL>;
	close(F_TAIL);

	return $res;
    }
}



sub DisplayQuestionNumber {
    my ($question, $min, $max, $default) = @_;

    my $opt = { widget  => "inputbox",
		args    => [],
		options => [],
		title   => "Mioga configuration",
		text    => "\n$question \n\nThe value must be between $min and $max.\n\n",
		height  => 0,
		width   => 0,
	      };


    my $rep = display_dialog($opt);
    return $rep;
}


sub DisplayQuestionList {
    my ($question, $values, $default) = @_;

    my $menu_size = @$values;
    my ($height, $width) = get_screen_size();

    $height *= 0.80;
    $height = int($height);

    if($height-10 < $menu_size) {
	$menu_size = $height - 10;
    }
    else {
	$height = $menu_size + 10;
    }

    $width *= 0.60;
    $width = int($width);

    my $i=1;
    my $nbItems = @$values;
    
    foreach my $val (@$values)
    {
		if(length($val) > 14) {
			$val = substr($value, 0, 10)." ...";
		}
		
        push @menu_list, "'$conf->{name}' '$value'";
    }

   

    my $opt = { widget  => "menu",
		args    => \@menu_list,
		options => [],
		title   => 'Configuration de mioga',
		text    => "\nSelect a parameter : \n\n",
		height  => 17,
		width   => 60,
	      };

    my $rep = display_dialog($opt);
    return $rep;
}



sub DisplayQuestionAlpha {
    my ($question, $default) = @_;
    
    my $opt = { widget  => "inputbox",
	            args    => [$default],
	            options => [],
	            title   => "Mioga configuration",
	            text    => "\n$question\n\n",
	            height  => 0,
	            width   => 0,
	      };


    my $rep = display_dialog($opt);
    return $rep;
}


sub DisplayQuestionYesNo {
    my ($question, $default) = @_;

    my $options;
    if($default eq 'no') {
	$options = '--defaultno';
    }
    
    my $opt = { widget  => "yesno",
		args    => [],
		options => [$options],
		title   => "Mioga configuration",
		text    => "\n$question\n\n",
		height  => 0,
		width   => 0,
	      };


    my $retval = display_dialog($opt);
    my $rep;
    
    if($retval == 0) {
	$rep = 'yes';
    }
    elsif($retval == 1) {
	$rep = 'no';
    }
    else {
	$rep = '';
    }

    return $rep;
}


sub DisplayInfo {
    my @infos = @_;

    if(!defined $Info) {
	my $filename = tmpnam;

	open ($Info, ">$filename")
	    or die "Can't open $filename : $!";

	my $oldfh = select($Info); $| = 1; select($oldfh);

	my ($height, $width) = get_screen_size();

	$height *= 0.80;
	$height = int($height);

	$width *= 0.70;
	$width = int($width);

	# Run tailboxbg

	my $opt = { widget  => "tailboxbg",
		    args    => [],
		    options => ['--no-kill'],
		    title   => "Mioga configuration",
		    text    => $filename,
		    height  => $height,
		    width   => $width,
		  };

	my $res = display_dialog($opt);
	$InfoPid = $res;

    }

    print $Info join("", @infos);
}


sub DisplayMessage {
    my @infos = @_;

    my $opt = { widget  => "msgbox",
		args    => [],
		options => [],
		title   => "Mioga configuration",
		text    => "\n".join('', @infos)."\n\n",
		height  => 0,
		width   => 0,
	      };


    display_dialog($opt);
}




sub DisplayMenu {
    my ($config, $menu, $is_submenu) = @_;

    my $menu_size = @$menu + 1;
    my ($height, $width) = get_screen_size();

    $height *= 0.80;
    $height = int($height);

    if($height-10 < $menu_size) {
	$menu_size = $height - 10;
    }
    else {
	$height = $menu_size + 10;
    }

    $width *= 0.60;
    $width = int($width);

    my @menu_list = ($menu_size);

    if($is_submenu) {
	push @menu_list, "'0 - previous menu' ''";
    }
    else {
	push @menu_list, "'0 - end config' ''";
    }
    
    
    my $i=1;
    my $nbItems = @$menu;
    
    foreach my $conf (@$menu)
    {
	if($conf->{type} eq 'submenu') {
	    push @menu_list, "'$i - $conf->{name}' ''";
	}
	else {
	    my $value = $config->{$conf->{name}};

	    if(length($value) > 14) {
		$value = substr($value, 0, 10)." ...";
	    }

	    push @menu_list, "'$i - $conf->{name}' '$value'";
	}

	$i++;
	
    }
   

    my $opt = { widget  => "menu",
		args    => \@menu_list,
		options => [],
		title   => 'Configuration de mioga',
		text    => "\nSelect a parameter : \n\n",
		height  => 17,
		width   => 60,
	      };

    my $res = display_dialog($opt);
    $res =~ s/^(\d+).*/$1/g;
    
    return $res;
}


sub ClearScreen {
    system("clear");
}

1;
