# ============================================================================
# Mioga2 Project (C) 2003-2007 The Mioga2 Project
#
#	This program is free software; you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the
#	Free Software Foundation; either version 2, or (at your option) any
#	later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#   02110-1301, USA.
# ============================================================================
#	Description:
# ============================================================================

package MiogaConf;

use strict;
use Mioga2::XML::Simple;
use Mioga2::tools::string_utils;
use Error qw(:try);
use List::Util qw[max];
use File::Glob ':glob';

my @ConfigDefault = ( { name	   => 'mioga_conf',
                        question => "Path to Mioga.conf ?",
                        type       => 'slash start',
                        default  => '/var/lib/Mioga2/conf/Mioga.conf' ,
                        xpath      => '/default_mioga_conf',
                    },
                     
                      { name       => 'init_sql', 
                        question => "Initialize database ?", 
                        type       => 'bool', 
                        default  => 'yes',
                        xpath      => '/init_sql',
                    },
                );

# ----------------------------------------------------------------------------
# Constructor
# ----------------------------------------------------------------------------
sub new {
	my ($class, %param) = @_;

	my $self = { };
	bless($self, $class);	 
	
	if(!exists $param{config}) {
		die "parameter 'config' required in MiogaConf.";
	}

	if(! -f $param{config}) {
		die "$param{config} does not exists";
	}

	if(exists $param{backend}) {
		eval "require MiogaConf::Backend::$param{backend}";
	}
	else {
		require MiogaConf::Backend::Text;
	}

	$self->{dont_check_depends} = (exists $param{dont_check_depends}?$param{dont_check_depends}:0);
	$self->{dont_change_config} = (exists $param{dont_change_config}?$param{dont_change_config}:0);


	if(exists $param{dir}) {
		$self->{dir} = $param{dir};
	}
	else {
		$self->{dir} = ".";
	}
	
	if(exists $param{force_init_sql}) {
		$self->{force_init_sql} = $param{force_init_sql};
	}
	else {
		$self->{force_init_sql} = 0;
	}
	
	
	
	if(exists $param{verbose}) {
		$self->{verbose} = $param{verbose};
	}
	else {
		$self->{verbose} = 0;
	}
	
	if(exists $param{is_main}) {
		$self->{is_main} = $param{is_main};
	}
	else {
		$self->{is_main} = 0;
	}
	

	
	$self->{CONFIG} = $self->LoadConfFile($param{config}, $param{is_main});
	
	return $self;
}


sub GetVersionPackage {
	my ($self) = @_;

	return $self->{CONFIG}{version}->[0]->{module};
}


#-------------------------------------------------------------------------------
# Check CDN URI is valid (no instance with that name already exist, does not
# correspond to default instance name)
#-------------------------------------------------------------------------------
sub CheckCDNURI {
	my ($self, $config) = @_;
	my $ret = 1;

	if ($config->{cdn_server} eq '') {
		my $path = $config->{install_dir} . $config->{cdn_uri} . '/MiogaFiles';
		if ((! -d $path) && ($config->{cdn_uri} ne '/' . $config->{instance_ident})) {
		}
		else {
			print "CDN URI is invalid, an instance with that name already exists\n";
			$ret = 0;
		}
	}

	return ($ret);
}


#-------------------------------------------------------------------------------
# Check login URI is valid (no instance with that name already exist, does not
# correspond to default instance name)
#-------------------------------------------------------------------------------
sub CheckLoginURI {
	my ($self, $config) = @_;
	my $ret = 0;

	if ($config->{html_login} eq 'yes') {
		my $path = $config->{install_dir} . $config->{login_uri} . '/MiogaFiles';
		if ((! -d $path) && ($config->{login_uri} ne '/' . $config->{instance_ident})) {
			$ret = 1;
		}
		else {
			print "Login URI is invalid, an instance with that name already exists\n";
		}
	}
	else {
		$ret = 1;
	}

	return ($ret);
}


#-------------------------------------------------------------------------------
# Check user URI is valid (no instance with that name already exist, does not
# correspond to default instance name)
#-------------------------------------------------------------------------------
sub CheckUserURI {
	my ($self, $config) = @_;
	my $ret = 0;

	if ((-d $config->{install_dir} . $config->{user_uri}) || ($config->{user_uri} eq '/' . $config->{instance_ident})) {
		print "An instance named '$config->{user_uri}' exists or is to be created. This keyworkd is reserved for user personal space. Please fix this before proceeding.\n";
	}
	else {
		$ret = 1;
	}

	return ($ret);
}


#-------------------------------------------------------------------------------
# Check internal URI is valid (no instance with that name already exist, does not
# correspond to default instance name)
#-------------------------------------------------------------------------------
sub CheckInternalURI {
	my ($self, $config) = @_;
	my $ret = 0;

	my $path = $config->{install_dir} . $config->{int_uri} . '/MiogaFiles';
	if ((! -d $path) && ($config->{int_uri} ne '/' . $config->{instance_ident})) {
		$ret = 1;
	}
	else {
		print "Internal URI is invalid, an instance with that name already exists\n";
	}

	return ($ret);
}


#-------------------------------------------------------------------------------
# Check external URI is valid (no instance with that name already exist, does not
# correspond to default instance name)
#-------------------------------------------------------------------------------
sub CheckExternalURI {
	my ($self, $config) = @_;
	my $ret = 0;

	my $path = $config->{install_dir} . $config->{ext_uri} . '/MiogaFiles';
	if ((! -d $path) && ($config->{ext_uri} ne '/' . $config->{instance_ident})) {
		$ret = 1;
	}
	else {
		print "External URI is invalid, an instance with that name already exists\n";
	}

	return ($ret);
}

# ----------------------------------------------------------------------------
# Main configuration process
# ----------------------------------------------------------------------------
sub Configure
{
	my ($self, %params) = @_;
	my $config = $self->ReadConfig( "$self->{dir}/config.mk" );
	
	if(! $self->{dont_check_depends}) {
		if(! $self->CheckDepends()) {
			DisplayInfo("Installation aborted\n");
			return 0;
		}
		
		if(! $self->CheckVersion()) {
			DisplayInfo("Installation aborted\n");
			return 0;
		}
	}

	if(! $self->{dont_change_config}) {
		my $result = AskQuestionYesNo("Do you want to modify current configuration ?", 'yes');
		if ($result eq 'yes')
		{
			$self->ModifyConfig( $config, $self->{CONFIG}->{config}->[0]->{parameter} );
		}
	}

	my ($installed, $packaged) = $self->GetVersions();

	$config->{installedversion}    = $installed;
	$config->{packagedversion}     = $packaged;
	$config->{locales_installdir}  = $params{locales_installdir};
	$config->{mioga_prefix}   = $params{mioga_prefix};

	$self->WriteConfig("$self->{dir}/config.mk", $config );

	$self->WriteMiogaConf( $config );
	
	if (!$self->CheckLoginURI ($config)) {
		DisplayInfo ("Installation aborted\n");
		return 0;
	}
	
	if (!$self->CheckUserURI ($config)) {
		DisplayInfo ("Installation aborted\n");
		return 0;
	}
	
	if (!$self->CheckCDNURI ($config)) {
		DisplayInfo ("Installation aborted\n");
		return 0;
	}
	
	if (!$self->CheckInternalURI ($config)) {
		DisplayInfo ("Installation aborted\n");
		return 0;
	}
	
	if (!$self->CheckExternalURI ($config)) {
		DisplayInfo ("Installation aborted\n");
		return 0;
	}

	return 1;
}


# ----------------------------------------------------------------------------
# Main install process
# ----------------------------------------------------------------------------
sub Install
{
	my ($self, $miogaconf) = @_;
	my $config = $self->ReadConfig( "$self->{dir}/config.mk" );

	my $dbh = $miogaconf->GetDBH();

	print "MiogaConf::Install  installedversion ". $config->{installedversion}."\n";
	if($self->{force_init_sql} == 1 or 
	   $config->{init_sql} eq 'yes') {
		# Run base_cleaner.sql
		my $cleaner = $self->{dir}."/sql/base_cleaner.sql";
		if (-e $cleaner) {
			try {
				$self->LoadSchema($dbh, $cleaner);
			} catch Error::Simple with {
				print STDERR "Cleaner doesn't run, but try to load schema before stop\n";
				print STDERR "   If it's the frist run, it's not necessary to run cleaner !\n";
			};

		}
		# Run schema_base.sql
		$self->LoadSchema($dbh);
	}
	elsif($config->{installedversion}) {
		# Run update-database
		
		$self->UpdateDatabase($dbh, $config);
	}

	if($config->{installedversion} or $self->{is_main} == 0) {
		require Mioga2::Config;

		my $instances = GetInstances($miogaconf);
		foreach my $inst (@$instances) {
			print " Update instance : $inst\n";
			my $instconfig = new Mioga2::Config($miogaconf, $inst);
			$self->UpgradeAppDesc($instconfig);
		}
		
		$self->RunHooks($miogaconf);
	}		
	
	$dbh->disconnect();

}

sub RunHooks {
	my ($self, $miogaconf) = @_;
	if(exists $self->{CONFIG}->{upgrade_hook}) {
		my $confdir = $miogaconf->GetInstallDir()."/conf";
		foreach my $hook (@{$self->{CONFIG}->{upgrade_hook}}) {
			system("$confdir/Config.hook.d/$hook configxml=$confdir/Config.xml miogaconf=$confdir/Mioga.conf");
		}
	}
}

# ----------------------------------------------------------------------------
# Upgrade AppDesc
# ----------------------------------------------------------------------------
sub UpgradeAppDesc
{
	my($self, $config) = @_;
	my $dbh = $config->GetDBH();
	my $mioga_id = $config->GetMiogaId();
	
	require Mioga2::tools::database;
	import Mioga2::tools::database;

	require Mioga2::tools::APIApplication;
	import Mioga2::tools::APIApplication;

	my %updated_app;
	# Loop through applications present in XML file *AND* applications registered in the DB
	for my $applications ($self->{CONFIG}->{applications}->[0]->{app}, SelectMultiple ($dbh, "SELECT * FROM m_application;")) {
		## Update database from XML (Add new apps/functions/methods and update others)
		foreach my $app (@$applications) {
			next if ($updated_app{$app->{ident}});
			$updated_app{$app->{ident}} = 1;

			# First install application if not already installed of update it if already installed (system-wide)
			my $res = SelectSingle($dbh, "SELECT * FROM m_application WHERE ident = '$app->{ident}'");

			eval "require $app->{package}"; warn $@ if $@;
			
			my $apps = "$app->{package}"->new;
			my $appdesc = $apps->GetAppDesc();
			$self->CheckApp ($app->{package}, $appdesc);
			
			if(defined $res) {
				$self->UpdateApp($config, $res->{rowid}, $appdesc);
			}
			else {
				$self->CreateApp($config, $appdesc);
			}

			# Now application is installed, activate it for current instance if not already activated
			$res = SelectSingle($dbh, "SELECT * FROM m_application, m_instance_application WHERE m_instance_application.application_id = m_application.rowid AND m_application.ident = '$app->{ident}' AND m_instance_application.mioga_id = $mioga_id");

			if (!defined ($res)) {
				my $package = $app->{package};
				ExecSQL ($dbh, "INSERT INTO m_instance_application (application_id, mioga_id, all_users, all_groups, all_resources) (SELECT m_application.rowid AS application_id, $mioga_id AS mioga_id, '" . ($appdesc->{all_users} ? 't' : 'f') . "' AS all_users, '" . ($appdesc->{all_groups} ? 't' : 'f') . "' AS all_groups, '" . ($appdesc->{all_resources} ? 't' : 'f') . "' AS all_resources FROM m_application WHERE package = " . $dbh->quote ($package) . ");");
			}
		}
	}
}
# ----------------------------------------------------------------------------
# Private Methods
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Merge Conf
# ----------------------------------------------------------------------------
sub MergeConf
{
	my ($self, $filename, $config) = @_;

	open(F_CONF, $filename) 
		or die "Can't open $filename for reading : $!";
	
	my $xml = "";
	{
		local $/ = undef;
		$xml = <F_CONF>;
	}
	
	close(F_CONF);

	my $xs = new Mioga2::XML::Simple();
	my $conf = $xs->XMLin($xml);

	foreach my $key (keys %{$conf})
	{
		if (!exists ($config->{$key}))
		{
			$config->{$key} = $conf->{$key};
		}
	}
}

# ----------------------------------------------------------------------------
# Merge ConfMk
# ----------------------------------------------------------------------------
sub MergeConfMk
{
	my ($self, $filename, $config) = @_;

	open(F_CONF, $filename) 
		or die "Can't open $filename for reading : $!";
	
	my $xml = "";
	{
		local $/ = undef;
		$xml = <F_CONF>;
	}
	
	close(F_CONF);

	my $xs = new Mioga2::XML::Simple();
	my $conf = $xs->XMLin($xml);

	$self->PlanHash($conf, $config);
}

# ----------------------------------------------------------------------------
# PlanHash
# ----------------------------------------------------------------------------
sub PlanHash
{
	my ($self, $config, $result) = @_;

	foreach my $key (keys %$config)
	{
		if (ref ($config->{$key}) eq 'HASH')
		{
			if (! keys %{$config->{$key}})
			{
				$result->{$key}  = 'yes' if !exists $result->{$key};
			}
			else
			{
				$self->PlanHash($config->{$key}, $result);
			}
		}
		else
		{
			$result->{$key} = $config->{$key} if  !exists $result->{$key};
		}
	}
}


# ----------------------------------------------------------------------------
# Load and check XML config file
# ----------------------------------------------------------------------------
sub LoadConfFile {
	my ($self, $conf_file, $is_main) = @_;

	$is_main = 0 unless defined $is_main;

	open(F_CONF, $conf_file) 
		or die "Can't open $conf_file for reading : $!";
	
	my $xml = "";
	{
		local $/ = undef;
		$xml = <F_CONF>;
	}
	
	close(F_CONF);

	my $xs = new Mioga2::XML::Simple(forcearray => 1);
	my $conf = $xs->XMLin($xml);

	if(! exists $conf->{config}) {
		$conf->{config}->[0]->{parameter} = [];
	}

	if(! exists $conf->{config}->[0]->{filename}) {
		$conf->{config}->[0]->{filename} = "Mioga.conf";
	}

	foreach my $param (@ConfigDefault) {
		next if($is_main and $param->{name} eq 'mioga_conf');

		unshift @{$conf->{config}->[0]->{parameter}}, $param;
	}

	#
	# Load and merge <conf_file>.d/*
	# 

	if(-d "$conf_file.d") {
		foreach my $file (glob ("$conf_file.d/*")) {
			my $subconf = $self->LoadConfFile($file, $is_main);
			
			if(exists $subconf->{applications}) {
				push @{$conf->{applications}->[0]->{app}}, @{$subconf->{applications}->[0]->{app}};
			}

			if(exists $subconf->{upgrade_hook}) {
				push @{$conf->{upgrade_hook}}, @{$subconf->{upgrade_hook}};
			}
		}
	}
	
	return $conf;
}


# ----------------------------------------------------------------------------
# Check already installed package version
# ----------------------------------------------------------------------------
sub CheckVersion {
	my ($self) = @_;

	my $package_name = $self->{CONFIG}->{version}->[0]->{module};

	my ($installed_version, $packaged_version) = $self->GetVersions();

	if(! $installed_version) {
		return 1;
	}


	elsif( $installed_version eq $packaged_version) {
		return 1;
	}

	else {	
		my $result = AskQuestionYesNo("Installed versions ($installed_version) and version of this package ($packaged_version) are differents.\n\nDo you really want to update $package_name ?", 'no');
		return 0 if ($result eq 'no');
	}

	return 1;
}
# ----------------------------------------------------------------------------
# Load schema_base.sql into database.
# ----------------------------------------------------------------------------
sub LoadSchema {
	my ($self, $dbh, $filename) = @_;

	if(!defined $filename) {
		$filename = "$self->{dir}/sql/schema_base.sql";
	}
	print "Load schema : $filename\n";
	open(F_SCHEMA, "$filename")
		or die "Can't open $self->{dir}/sql/schema_base.sql for reading : $!";

	require Mioga2::tools::database;
	import Mioga2::tools::database;

	BeginTransaction($dbh);

	try {
		my $sql = "";
		my $line;
		my $nb_quote=0;
		while($line = <F_SCHEMA>) {
			$line =~ s/--.*$//g;
			next if $line =~ /^\s*$/;
			
			$line =~ s/^\s*//g;
			
			$sql .= $line;
			
			while ($line =~ /(^|[^\\])?\'/g) {
				$nb_quote++;
			}
			
            # Try to have a pair number of quote in string, avoid escaping ones
			#
			if($line =~ /;/ and ! ($nb_quote % 2)) {
				if(! defined $dbh->do($sql)) {
                    print STDERR "SQL Error for sql = $sql\n";
					throw Error::Simple("SQL Error");
				}
				
				$sql = "";
				$nb_quote = 0;
			}
		}
		EndTransaction($dbh);
	} catch Error::Simple with {
		my $err = shift;
		RollbackTransaction($dbh);
		$err->throw;
	};

	close(F_SCHEMA);
}
# ----------------------------------------------------------------------------
# UpdateDatabase
# ----------------------------------------------------------------------------
sub UpdateDatabase {
	my ($self, $dbh, $config) = @_;


	my @files = glob ("$self->{dir}/sql/update-*");

	use Data::Dumper;
	my @updates;

	foreach my $file (@files) {
		my ($prev, $next) = ($file =~ /update-([^_]+)_(.+?)\.((sql)|(pl))/);

		next unless defined $prev;
		next unless defined $next;

		if(CmpMiogaVersion($prev, $config->{installedversion}) <= 0) {
			# Push version update unless already pushed (in case version update has both PL and SQL scripts)
			push @updates, [$prev, $next] unless (@updates && (($updates[-1]->[0] eq $prev) && ($updates[-1]->[1] eq $next)));
		}
	}
	
	@updates = sort {CmpMiogaVersion($b->[0], $a->[0])} @updates;

	foreach my $upt (@updates) {
		my $filebase = "$self->{dir}/sql/update-".$upt->[0]."_".$upt->[1];

		print "Apply $filebase\n";

		if(-f "$filebase.sql") {			
			$self->LoadSchema($dbh, "$filebase.sql");
		}
		if(-f "$filebase.pl") {
			system("perl $filebase.pl");
		}
	}
}
# ----------------------------------------------------------------------------
# Compare version number
# ----------------------------------------------------------------------------
sub CmpVersion {
	my ($a, $b) = @_;
	
	my @al = split(/[-~_\.]/, $a);
	my @bl = split(/[-~_\.]/, $b);

	# Normalize both arrays to get the same number of items
	for (my $j=0; $j<max(scalar (@al),scalar (@bl)); $j++) {
		$al[$j] = 0 if (!defined ($al[$j]));
		$bl[$j] = 0 if (!defined ($bl[$j]));
	}
	
	my $i = 0;

	while(1) {
		if(@bl <= $i and @al <= $i ) {
			return 0;
		}

		elsif(@bl <= $i) {
			return -1;
		}

		elsif(@al <= $i) {
			return 1;
		}
		
		elsif($al[$i] =~ /^\d+$/) {
			if(int ($al[$i]) > int ($bl[$i])) {
				return -1;
			}	   
			elsif(int ($al[$i]) < int ($bl[$i])) {
				return 1;
			}
		}
		
		else {
			if($al[$i] gt $bl[$i]) {
				return -1;
			}	   
			elsif($al[$i] lt $bl[$i]) {
				return 1;
			}
		}

		$i++;
	}
	return 0;
}

# ----------------------------------------------------------------------------
# Compare Mioga version number
# version number is n.n.n-xxxxn
# xxxxx can be alpha, beta or rc and last n is number
# ----------------------------------------------------------------------------
sub CmpMiogaVersion {
	my ($a, $b) = @_;
	
	my ($av, $at) = split(/-/, $a);
	my ($bv, $bt) = split(/-/, $b);

	my $cmp = CmpVersion($av, $bv);
	
	if ($cmp == 0)
	{
		if (defined($at) && defined($bt)) {
			if($at gt $bt) {
				$cmp = -1;
			}	   
			elsif($at lt $bt) {
				$cmp = 1;
			}
		}
		elsif (defined($at)) {
			$cmp = 1;
		}
		elsif (defined($bt)) {
			$cmp = -1;
		}
	}
	return $cmp;
}
# ----------------------------------------------------------------------------
# Return installed and package version
# ----------------------------------------------------------------------------
sub GetVersions {
	my ($self) = @_;

	my $package_name = $self->{CONFIG}->{version}->[0]->{module};

	my $eval_inst = "
	    eval \"require $package_name\";".'
	    print $'."$package_name".'::VERSION if(!$@);
	   ';

	my $eval_pack = "
	    use lib \"$self->{dir}/lib\";
	    eval \"require $package_name\";".'
	    print $'."$package_name".'::VERSION if(!$@);
	   ';

	my $installed_version = `perl -e '$eval_inst'`;
	my $packaged_version  = `perl -e '$eval_pack'`;

	return ($installed_version,  $packaged_version);
}



# ----------------------------------------------------------------------------
# Check if the package $what is installed and if
# it's version is at least $min_version.
#
# Return FALSE if a package is not installed or
# if its version is too old.
# ----------------------------------------------------------------------------
sub CheckDepsVersion
{
	my($self, $mod_name, $min_version) = @_;
	my $error = 0;

	DisplayInfo("Checking for $mod_name... ");
	
	my $cur_vers;
	eval "require $mod_name; ".'$cur_vers = $'."$mod_name".'::VERSION;';
	if ($@)
	{
		$error = 1;
	}
	elsif( (defined $min_version) and (CmpVersion ($min_version, $cur_vers) < 0) ) {
		$error = 1;
	}
	

	if($error)	{
		DisplayInfo(" failed\n");
		return 0;
	}

	DisplayInfo("$cur_vers ok\n");
	return 1;
}


# ----------------------------------------------------------------------------
# Check if the C library $name is installed and if
# it's version is at least $min_version.
#
# Return FALSE if a package is not installed or
# if its version is too old.
# ----------------------------------------------------------------------------
sub CheckCLibVersion
{
	my ($self, $name, $version) = @_;


	DisplayInfo("Checking for Lib$name ... ");
	
	my $lib_exists = `which $name-config`;

	if ($?)
	{
		DisplayInfo(" failed\n");
		return 0;
	}

	chomp($lib_exists);
	
	my $installed_version = `$name-config --version`;
	chomp($installed_version);

	if (CmpVersion($version, $installed_version) >= 0)
	{ 
		DisplayInfo(" ok\n");
	}
	else
	{
		DisplayInfo (" failed, please upgrade your library.\n");
		return 0;
	}
	
	return 1;
}


# ----------------------------------------------------------------------------
# Check dependencies
# ----------------------------------------------------------------------------
sub CheckDepends
{
	my($self, $show_errors) = @_;

	$show_errors = 1 unless defined $show_errors;
	
	my @missing;
	my @missing_clib;

	foreach my $dep (@{$self->{CONFIG}->{dependencies}->[0]->{dep}}) {
		my $version;
		if(exists $dep->{version}) {
			$version = $dep->{version};
		}

		if(exists $dep->{max_perl_version} and
		   $] > $dep->{max_perl_version}) {
			next;
		}

		if(exists $dep->{min_perl_version} and
		   $] < $dep->{min_perl_version}) {
			next;
		}


		if( ! $self->CheckDepsVersion($dep->{module}, $version)) {
			push @missing, $dep;
		}
	}
	

	foreach my $dep (@{$self->{CONFIG}->{dependencies}->[0]->{clib}}) {
		my $version;
		if(exists $dep->{version}) {
			$version = $dep->{version};
		}

		if( ! $self->CheckCLibVersion($dep->{name}, $version)) {
			push @missing_clib, $dep;
		}
	}


	if(@missing or @missing_clib) {

		my $message = "";
		if(@missing) {
			$message .= "\nThere are missing dependencies.\n\nMioga requires :\n";
			
			foreach my $mis ( @missing) {
				$message .= "\t$mis->{module} module".((exists $mis->{version})?", version >= $mis->{version}":"")."\n";
			}
		}

		if(@missing_clib) {
			$message .= "\nThere are missing C Librairies.\n\nMioga requires :\n";
			
			foreach my $mis ( @missing_clib) {
				$message .= "\tLib$mis->{name} library".((exists $mis->{version})?", version >= $mis->{version}":"")."\n";
			}
		}

		DisplayMessage($message) if $show_errors;

		return 0;
	}

	return 1;
}

# ------------------------------------------------------------
# Application
# ------------------------------------------------------------
sub CreateApp
{
	my $self = shift;
	my $config = shift;
	my $app = shift;
	my $dbh = $config->GetDBH();

	print "Create application : $app->{ident}\n" if $self->{verbose};

	my $app_rowid = ApplicationCreate($config, $app);

	foreach my $func (keys %{$app->{functions}})
	{
		my $func_rowid = FunctionCreate($dbh, $func, $app->{functions}->{$func}, $app_rowid);

		foreach my $method (@{$app->{func_methods}->{$func}})
		{
			my $method_rowid = MethodCreate($dbh, $method, $func_rowid, "normal");
		}

		foreach my $method (@{$app->{xml_methods}->{$func}})
		{
			my $method_rowid = MethodCreate($dbh, $method, $func_rowid, "xml");
		}

		foreach my $method (@{$app->{sxml_methods}->{$func}})
		{
			my $method_rowid = MethodCreate($dbh, $method, $func_rowid, "sxml");
		}
	}
}

# ------------------------------------------------------------
# Function
# ------------------------------------------------------------
sub CreateFunc
{
	my $self = shift;
	my $dbh = shift;	
	my $app_rowid = shift;
	my $app = shift;
	my $func = shift;
	print "Create function : $func\n" if $self->{verbose};

	my $func_rowid = FunctionCreate($dbh, $func, $app->{functions}->{$func}, $app_rowid);

	foreach my $method (@{$app->{func_methods}->{$func}})
	{
		my $method_rowid = MethodCreate($dbh, $method, $func_rowid, 'normal');
	}

	foreach my $method (@{$app->{xml_methods}->{$func}})
	{
		my $method_rowid = MethodCreate($dbh, $method, $func_rowid, "xml");
	}
	
	foreach my $method (@{$app->{sxml_methods}->{$func}})
	{
		my $method_rowid = MethodCreate($dbh, $method, $func_rowid, "sxml");
	}
}

# ------------------------------------------------------------
# Application
# ------------------------------------------------------------
sub UpdateApp
{
	my $self = shift;
	my $config = shift;
	my $app_rowid = shift;
	my $app = shift;
	print "Update application : $app->{ident}\n" if $self->{verbose};

	my $dbh = $config->GetDBH();

	ApplicationUpdate($config, $app_rowid, $app);
	foreach my $func (keys %{$app->{functions}})
	{
		my $res = SelectSingle($dbh, "SELECT * FROM m_function WHERE ident = '$func' AND
		                                                             application_id = $app_rowid");
		if(defined $res) {
			$self->UpdateFunc($dbh, $res->{rowid}, $app, $func);
		}
		else {
			$self->CreateFunc($dbh, $app_rowid, $app, $func);
		}
	}
}

# ------------------------------------------------------------
# Function
# ------------------------------------------------------------
sub UpdateFunc
{
	my $self = shift;
	my $dbh = shift;
	my $func_rowid = shift;
	my $app = shift;
	my $func = shift;
	print "Update function : $func\n" if $self->{verbose};
	
	FunctionUpdate($dbh, $func_rowid, $func, $app->{functions}->{$func});
	
	my $tbl_metd = $app->{func_methods}->{$func};
	foreach my $metd (@$tbl_metd)
	{
		my $res = SelectSingle($dbh, "SELECT * FROM m_method WHERE ident = '$metd' AND
		                                                           function_id = $func_rowid");
		
		if(! defined $res) {
			MethodCreate($dbh, $metd, $func_rowid, "normal");
			print "Add method $metd\n" if $self->{verbose};
		}
	}

	$tbl_metd = $app->{xml_methods}->{$func};
	foreach my $metd (@$tbl_metd)
	{
		my $res = SelectSingle($dbh, "SELECT * FROM m_method WHERE ident = '$metd' AND
		                                                           function_id = $func_rowid");
		
		if(! defined $res) {
			MethodCreate($dbh, $metd, $func_rowid, "xml");
			print "Add method $metd\n" if $self->{verbose};
		}
	}

	$tbl_metd = $app->{sxml_methods}->{$func};
	foreach my $metd (@$tbl_metd)
	{
		my $res = SelectSingle($dbh, "SELECT * FROM m_method WHERE ident = '$metd' AND
		                                                           function_id = $func_rowid");
		
		if(! defined $res) {
			MethodCreate($dbh, $metd, $func_rowid, "sxml");
			print "Add method $metd\n" if $self->{verbose};
		}
	}

}


#===============================================================================

=head2 CheckApp

Check application's AppDesc is correct

=cut

#===============================================================================
sub CheckApp {
	my ($self, $package, $appdesc) = @_;

	my $status = 1;

	# Check application description
	for my $attr (qw/package ident description type/) {
		if (!exists ($appdesc->{$attr}) || !defined ($appdesc->{$attr})) {
			warn "$package has no value for attribute '$attr'";
			$status = 0;
		}
	}

	# Check application attributes
	for my $attr (qw/is_user is_group is_resource can_be_public all_users all_groups/) {
		if (!exists ($appdesc->{$attr}) || !defined ($appdesc->{$attr}) || (($appdesc->{$attr} != 0) && ($appdesc->{$attr} != 1))) {
			warn "$package has no or invalid value for attribute '$attr'";
			$status = 0;
		}
	}

	# Check functions and methods (mandatory), mioglets and (S)XML methods (optional)
	for my $attr (qw/functions func_methods func_mioglets xml_methods sxml_methods/) {
		my $mandatory = ($attr =~ /(functions|func_methods)/) ? 1 : 0;
		if ($mandatory && (!exists ($appdesc->{$attr}) || !defined ($appdesc->{$attr}))) {
			warn "$package has no value for attribute '$attr'";
			$status = 0;
		}

		if (($mandatory || exists ($appdesc->{$attr})) && (ref ($appdesc->{$attr}) ne 'HASH')) {
			warn "$package has invalid value for attribute '$attr'";
			$status = 0;
		}
	}

	# Check public methods and mioglets
	for my $attr (qw/public_methods public_mioglets/) {
		if (exists ($appdesc->{$attr}) && defined ($appdesc->{$attr}) && (ref ($appdesc->{$attr}) ne 'ARRAY')) {
			warn "$package has invalid value for attribute '$attr'";
			$status = 0;
		}
	}

	# Check (deeper) functions and methods association
	for my $function (keys (%{$appdesc->{functions}})) {
		if (!exists ($appdesc->{func_methods}->{$function}) || (ref ($appdesc->{func_methods}->{$function}) ne 'ARRAY')) {
			warn "$package has invalid value for attribute 'func_methods->{$function}'";
			$status = 0;
		}
	}

	die "$package is inconsistent, installation aborted" unless ($status);
}	# ----------  end of subroutine CheckApp  ----------


# ----------------------------------------------------------------------------
# Initialize default config values from xml parameters.
# Analyze submenu recursively.
# Used by ReadConfig.
# ----------------------------------------------------------------------------
sub _initConfigDefault {
	my ($config, $submenu) = @_;
	
	foreach my $conf (@$submenu)
	{
		if($conf->{type} eq 'submenu') {
			_initConfigDefault($config, $conf->{parameter});
		}
		elsif(exists $conf->{default}) {
			$config->{lc($conf->{name})} = $conf->{default};
		}
		else {
			die "No default value for $conf->{name} in config description";
		}
	}
}


# ----------------------------------------------------------------------------
# Read config file if exist, else create one with default values
# ----------------------------------------------------------------------------
sub ReadConfig
{
	my ($self, $configFile) = @_;

	my $config = { };

	_initConfigDefault($config, $self->{CONFIG}->{config}->[0]->{parameter});

	if (-e $configFile)
	{
		my @lines;
		my $line;
		my $var;
		my $value;
		
		open(FILE, "<$configFile") or die "Cannot open $configFile : $!";
		@lines = <FILE>;
		close FILE;
		
		@lines = grep !/^#/, @lines;		# suppress comments
		@lines = grep !/^\s+$/, @lines;		# suppress blank lines
		
		foreach $line (@lines)
		{
			chomp $line;
			$line =~ s/^\s+//;		# suppress leading blank
			$line =~ s/\s+$//;		# suppress trailing blank
			($var, $value) = split /=/,$line,2;
			$var =~ s/\s+$//;
			$value =~ s/^\s+//;
			
			if (defined($var) && defined($value))
			{
				$config->{lc($var)} = $value;
			}
		}
	}
	
	return $config;
}

# ----------------------------------------------------------------------------
# AskQuestionNumber
# ----------------------------------------------------------------------------
sub AskQuestionNumber
{
	my ($question, $min, $max, $default) = @_;
	my $result;
	do
	{
		my $rep = DisplayQuestionNumber($question, $min, $max, $default);
		
		if (length($rep) == 0)
		{
			$rep = $default;
		}
		if (($rep >= $min) && ($rep <= $max) )
		{
			$result = $rep;
		}
	} while (!defined($result));

	return $result;
}

# ----------------------------------------------------------------------------
# AskQuestionList
# ----------------------------------------------------------------------------
sub AskQuestionList
{
	my ($question, $val_str, $default) = @_;
	my $result;

	my @values = split(/\s*,\s*/, $val_str);
	
	do
	{
		my $rep = DisplayQuestionList($question, \@values, $default);
		
		if (length($rep) == 0)
		{
			$rep = $default;
		}
		if (grep /$rep/, @values)
		{
			$result = $rep;
		}
	} while (!defined($result));

	return $result;
}

# ----------------------------------------------------------------------------
# AskQuestionYesNo
# ----------------------------------------------------------------------------
sub AskQuestionYesNo
{
	my ($question, $default) = @_;
	my $result;
	do
	{
		my $rep = DisplayQuestionYesNo($question, $default);
		
		if (length($rep) == 0)
		{
			$rep = $default;
		}
		if (grep /$rep/, qw(yes no))
		{
			$result = $rep;
		}
	} while (!defined($result));

	return $result;
}

# ----------------------------------------------------------------------------
# AskQuestionAlpha
# ----------------------------------------------------------------------------
sub AskQuestionAlpha
{
	my ($question, $default, $type) = @_;
	my $result;
	do
	{
		my $rep = DisplayQuestionAlpha($question, $default);

		$rep =~ s/^\s*//;
		$rep =~ s/\s*$//;
		if (length($rep) == 0)
		{
			$rep = $default;
		}
		if (length($rep) >= 0)
		{
			if( $type eq "path" )
			{
				if( $rep =~ m:^/: ) {
					$result = $rep;
				}
			}
			elsif( $type eq "dir" or $type eq "file") {
				if( $rep =~ m:^/[^/]+$: ) {
					$result = $rep;
				}
			}
			else {
				$result = $rep;
			}
		}
	} while (!defined($result));

	return $result;
}

# ----------------------------------------------------------------------------
# Ask user for configuration
# ----------------------------------------------------------------------------
sub ModifyConfig
{
	my ($self, $config, $menu, $is_submenu) = @_;
	$is_submenu = 0 unless defined $is_submenu;

	my $result;

	my $continue = 1;
	my $rep;

	do
	{
		$result = DisplayMenu($config, $menu, $is_submenu);
		
		if ($result > 0)
		{
			my $param = $menu->[$result-1];
			my $type = $param->{type};
			my $name = lc($param->{name});

			if ($type eq 'submenu')
			{
				$self->ModifyConfig($config, $param->{parameter}, 1);
			}
			else {
				if ($type eq 'enum')
				{
					$rep = AskQuestionList( $param->{question},
											$param->{values},
											$config->{$name});
				}
				elsif ($type eq 'bool')
				{
					$rep = AskQuestionYesNo( $param->{question},
											 $config->{$name});
				}
				else
				{
					$rep = AskQuestionAlpha($param->{question},
											$config->{$name},
											$type);
				}
				$config->{$name} = $rep;
			}
		}
		else
		{
			$continue = 0;
		}
		
	} while ($continue);
}


# ----------------------------------------------------------------------------
# Write config file
# ----------------------------------------------------------------------------
sub WriteConfig
{
	my ($self, $configFile, $config) = @_;

	if(! $self->{is_main}) {
		$self->MergeConfMk($config->{mioga_conf}, $config);
	}

	open (FILE,">$configFile") or die "Cannot open $configFile for writing : $!";
	foreach my $key (keys %{$config})
	{
		print FILE uc($key) . " = $config->{$key}\n";
	}
	

	close(FILE);
}



# ----------------------------------------------------------------------------
#  Build Mioga config file
# ----------------------------------------------------------------------------
sub BuildMiogaConf
{
	my ($self, $config, $node, $result) = @_;

	foreach my $param (@$node) {
		my $type  = $param->{type};
		my $name  = $param->{name};
		my $xpath = $param->{xpath};

		if ($type eq 'submenu')
		{
			$self->BuildMiogaConf($config, $param->{parameter}, $result);
		}
		
		elsif(!defined $xpath) {
			next;
		}

		elsif ($type eq 'bool')
		{
			$self->PutInResult($result, $xpath) if ($config->{$name} eq 'yes');
		}
		else
		{
			$self->PutInResult($result, $xpath, $config->{$name});
		}
	}
}

# ----------------------------------------------------------------------------
#  Put result in xml tree
# ----------------------------------------------------------------------------
sub PutInResult
{
	my ($self, $result, $xpath, $val) = @_;

	$xpath =~ s/^\/(.*)/$1/;
	my @path = split(/\//, $xpath);
	
	my $treepath = join("}->{", @path);
	$treepath = "{$treepath}";

	if(defined $val) {
		$val = st_FormatXMLString ($val);
		eval '$result->'.$treepath.' = $val';
	}
	else {
		eval '$result->'.$treepath." = undef";
	}
}


# ----------------------------------------------------------------------------
#  Write Mioga config file
# ----------------------------------------------------------------------------
sub WriteMiogaConf
{
	my ($self, $config) = @_;
	my $result = {};

	$self->BuildMiogaConf($config, $self->{CONFIG}->{config}->[0]->{parameter}, $result);

	# test if we need to merge existing conf file
	# -------------------------------------------
	if (! $self->{is_main} and exists($config->{mioga_conf}))
	{
		$self->MergeConf($config->{mioga_conf}, $result);
	}
	
	$result->{locales_installdir} = $config->{locales_installdir};
	$result->{mioga_prefix}   = $config->{mioga_prefix};
	my $xml = '<?xml version="1.0"?>';
	$xml .= "\n<MiogaConf>\n";
	$xml .= $self->BuildXML($result);
	$xml .= "</MiogaConf>\n";

	open(F_CONF, ">$self->{dir}/web/conf/".$self->{CONFIG}->{config}->[0]->{filename})
		or die "Can't open web/conf/".$self->{CONFIG}->{config}->[0]->{filename}." for writing : $!";

	print F_CONF $xml;

	close(F_CONF);
}


# ----------------------------------------------------------------------------
#  Create XML
# ----------------------------------------------------------------------------
sub BuildXML
{
	my ($self, $result) = @_;
	my $xml = "";

	foreach my $key (keys %$result) {
		if(!defined $result->{$key}) {
			$xml .= "<$key/>\n";
		}
		elsif(ref($result->{$key}) eq 'HASH') {
			$xml .= "<$key>";
			$xml .= $self->BuildXML($result->{$key});
			$xml .= "</$key>\n";		
		}
		else {
			$xml .= "<$key>";
			$xml .= st_FormatXMLString ($result->{$key});
			$xml .= "</$key>\n";
		}
	}

	return $xml;
}


# ----------------------------------------------------------------------------
#  Return Instance List
# ----------------------------------------------------------------------------
sub GetInstances {
	my $miogaconf = shift;
	
	require Mioga2::tools::database;
	import Mioga2::tools::database;

	my $res = SelectMultiple($miogaconf->GetDBH(), "SELECT * FROM m_mioga");
	my @inst = map {$_->{ident}} @$res;
	return \@inst;
}


1;
