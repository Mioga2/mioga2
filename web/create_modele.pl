#!/usr/bin/perl -w
# 
# This script create an instance webtree model in $(INSTALL_DIR)/conf directory
#

use strict;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Getopt::Long;
use File::Copy;
use Data::Dumper;

my $verbose = 0;
my $help = 0;
my $path_mioga_conf = "conf/Mioga.conf";


# =========================================================================
# Usage
# =========================================================================
sub Usage {
	print "Usage : $0 [options]\n";
	print "    Options :\n";
	print "        --conf='path to Mioga.conf' : default to conf/Mioga.conf\n";
	print "        --verbose    : verbose mode\n";
	print "        --help       : print usage\n";

}
# =========================================================================
# CreateDir
# =========================================================================
sub CreateDir {
	my ($dir) = @_;
	my @path_elems = split('/', $dir);
	my $path = "";
	if ($dir =~ /^\//) {
		shift @path_elems;
		$path = "/";
	}
	foreach my $d (@path_elems) {
		$path .= "$d";
		if (!-d $path) {
			mkdir $path or die "Cannot create dir <$path> errno = $!\n";
		}
		$path .= "/";
	}
}
# =========================================================================
# CopyRecursivelyDir
# =========================================================================
sub CopyRecursivelyDir {
	my ($src, $dest) = @_;
}
# =========================================================================
# Main script
# =========================================================================

my $result = GetOptions ("help" => \$help,
			          "conf=s"  => \$path_mioga_conf,
			          "verbose"  => \$verbose);

if (!$result) {
	Usage;
	exit -1;
}

if ($help) {
	Usage;
	exit(0);
}

my $miogaconf = new Mioga2::MiogaConf($path_mioga_conf);
#print Dumper($miogaconf);

my $install_dir = $miogaconf->GetInstallDir();
my $conf_dir = $install_dir."/conf";
my $model_dir = $conf_dir."/model";
print "Install Dir = $install_dir  conf_dir = $conf_dir\n" if ($verbose);

print "create model dir\n" if ($verbose);
CreateDir($model_dir);

print "create MiogaFiles dir\n" if ($verbose);
CreateDir($model_dir.$miogaconf->GetMiogaFilesDir);

print "create base dir\n" if ($verbose);
my $base_dir = $model_dir.$miogaconf->GetBaseDir;
CreateDir($base_dir);

print "create lang dir\n" if ($verbose);
my $lang_dir = $model_dir.$miogaconf->GetLangDir;
CreateDir($lang_dir);

print "create xsl dir\n" if ($verbose);
my $xsl_dir = $model_dir.$miogaconf->GetXSLDir;
CreateDir($xsl_dir);

print "create private dir\n" if ($verbose);
my $private_dir = $model_dir.$miogaconf->GetPrivateDir;
CreateDir($private_dir);

print "create public dir\n" if ($verbose);
my $public_dir = $model_dir.$miogaconf->GetPublicDir;
CreateDir($public_dir);

print "create admin home dir\n" if ($verbose);
my $admin_dir = $private_dir."/admin";
CreateDir($admin_dir);

print "create admin home skeleton dir\n" if ($verbose);
my $skeleton_dir = $admin_dir.$miogaconf->GetSkeletonDir;
CreateDir($skeleton_dir);


my $def_lang = $miogaconf->GetDefaultLang;
print "Default lang = $def_lang\n" if ($verbose);

my $cmd;

print "copy skeletons dir\n" if ($verbose);
$cmd = "cp -rf ./skel/$def_lang/* $skeleton_dir/";
system($cmd);

print "copy portal files\n" if ($verbose);
$cmd = "cp -rf ./portal/$def_lang/* $admin_dir/";
system($cmd);

print "copy lang files\n" if ($verbose);
$cmd = "cp -rf ./lang/* $lang_dir/";
system($cmd);

print "copy jslib dir\n" if ($verbose);
$cmd = "cp -rf ./jslib $base_dir/jslib";
system($cmd);

