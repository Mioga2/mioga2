#!/bin/sh

if [ $# -lt 4 ]
then
	echo "Usage : $0 theme_name install_dir instance dest_name"
	echo " from web dir of Mioga2 module"
	exit -1
fi

if [ ! -d themes ]
then
	echo " Must be run in web subdir"
	exit -1
fi

theme_name=$1
install_dir=$2
instance=$3
dest_name=$4

echo "install_dir $install_dir"
echo "instance $instance"
echo "theme_name $theme_name"
echo "destname $dest_name"

xsl_dir="$install_dir/$instance/web/xsl/$dest_name"
if [ ! -d $xsl_dir ]
then
	echo "XSL dir doesn't exists. Create it ..."
	mkdir $xsl_dir
#	exit -1
fi

theme_dir="$install_dir/$instance/web/htdocs/themes/$dest_name"
if [ ! -d $theme_dir ]
then
	echo "Theme dir doesn't exists. Create it ..."
	mkdir -p $theme_dir
	mkdir $theme_dir/css $theme_dir/javascript $theme_dir/images
#	exit -1
fi

cp themes/$theme_name/xsl/* $xsl_dir/

cp themes/$theme_name/css/* $theme_dir/css/
cp -r themes/$theme_name/javascript/* $theme_dir/javascript/
cp -r themes/$theme_name/images/* $theme_dir/images/
