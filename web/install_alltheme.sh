#!/bin/sh

if [ $# -lt 2 ]
then
	echo "Usage : $0 conf_path theme_name"
	exit -1
fi

theme_name=$2

for inst in `mioga2_info.pl --conf=$1 instances`
do
	echo $inst
	xsl_dir=`mioga2_info.pl --conf=$1 --instance=$inst xsl_dir`
	if [ $? -ne 0 ]
	then
		echo "Cannot get xsl_dir for instance $inst"
		exit -1
	fi
	theme_dir=`mioga2_info.pl --conf=$1 --instance=$inst theme_dir`
	if [ $? -ne 0 ]
	then
		echo "Cannot get theme_dir for instance $inst"
		exit -1
	fi

	for theme in `mioga2_info.pl --conf=$1 --instance=$inst themes`
	do
		echo $theme
		cp $theme_name/xsl/* $xsl_dir/$theme_name/

		cp $theme_name/css/* $theme_dir/$theme_name/css/
		cp $theme_name/javascript/* $theme_dir/$theme_name/javascript/
		cp -r $theme_name/images/* $theme_dir/$theme_name/images/
	done
done
