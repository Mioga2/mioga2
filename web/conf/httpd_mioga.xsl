<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl">
<xsl:output method="text" indent="yes" encoding="utf-8" />

<xsl:template match="MiogaConf">

<!-- TODO handle base_uri correctly -->
<xsl:variable name="base_uri">
	<xsl:choose>
		<xsl:when test="base_uri = ''">/</xsl:when>
		<xsl:otherwise><xsl:value-of select="base_uri" /></xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="install_dir"><xsl:value-of select="install_dir" /></xsl:variable>
<xsl:variable name="ext_uri"><xsl:value-of select="ext_uri" /></xsl:variable>
<xsl:variable name="base_dir"><xsl:value-of select="base_dir" /></xsl:variable>
<xsl:variable name="dav_base_uri"><xsl:value-of select="dav_base_uri" /></xsl:variable>
<xsl:variable name="private_dir"><xsl:value-of select="private_dir" /></xsl:variable>
<xsl:variable name="public_dir"><xsl:value-of select="public_dir" /></xsl:variable>
<xsl:variable name="login_uri"><xsl:value-of select="login_uri" /></xsl:variable>


<!-- The internal WebDAV configuration -->
<exsl:document href="apache/mioga-internal-dav.conf" method="text">
#
# Mioga2 internal WebDAV configuration file
#

AliasMatch ^<xsl:value-of select="$dav_base_uri" />/([^/]+)(.*) <xsl:value-of select="$install_dir" />/$1<xsl:value-of select="$base_dir" />$2

&lt;Location <xsl:value-of select="$dav_base_uri" />&gt;
	Require local
&lt;/Location&gt;

&lt;Directory <xsl:value-of select="$install_dir" />/*<xsl:value-of select="$base_dir" /><xsl:value-of select="$private_dir"/>&gt;
	Require local

	Dav On
	Options Indexes FollowSymLinks
	AllowOverride None

	DirectoryIndex disabled
&lt;/Directory&gt;

&lt;Directory <xsl:value-of select="$install_dir" />/*<xsl:value-of select="$base_dir" /><xsl:value-of select="$public_dir"/>&gt;
	Require local

	Dav On
	Options Indexes FollowSymLinks
	AllowOverride None

	DirectoryIndex disabled
&lt;/Directory&gt;
</exsl:document>


<!-- The main handler configuration -->
<exsl:document href="apache/mioga-main.conf" method="text">
#
# Mioga2 main handler
#

APREQ2_ReadLimit 10737418240
APREQ2_TempDir /var/tmp

PerlSetVar MiogaConfig <xsl:value-of select="$install_dir" />/conf/Mioga.conf
PerlRequire <xsl:value-of select="$install_dir" />/conf/startup.pl

PerlOptions +GlobalRequest

PerlTransHandler Mioga2::Router

ErrorDocument 403 <xsl:value-of select="$base_uri"/><xsl:value-of select="$login_uri"/>/Error?code=403
ErrorDocument 404 <xsl:value-of select="$base_uri"/><xsl:value-of select="$login_uri"/>/Error?code=404
ErrorDocument 500 <xsl:value-of select="$base_uri"/><xsl:value-of select="$login_uri"/>/Error?code=500

SetEnvIf Origin "^(null|app.*)$" ORIGIN_SUB_DOMAIN=$1
Header always set Access-Control-Allow-Origin: "%{ORIGIN_SUB_DOMAIN}e" env=ORIGIN_SUB_DOMAIN
Header always set Access-Control-Allow-Credentials: true

# CWE-693 protection
Header always set X-Frame-Options "SAMEORIGIN"

<xsl:choose>
	<xsl:when test="$base_uri = '/'">
		<xsl:choose>
			<xsl:when test="$ext_uri = ''">
&lt;LocationMatch ^/(?!<xsl:value-of select="substring ($dav_base_uri,2)"/>)&gt;
			</xsl:when>
			<xsl:otherwise>
&lt;LocationMatch ^/(?!<xsl:value-of select="substring ($dav_base_uri,2)"/>|<xsl:value-of select="substring ($ext_uri,2)"/>/)&gt;
			</xsl:otherwise>
		</xsl:choose>
	</xsl:when>
	<xsl:otherwise>
&lt;Location <xsl:value-of select="$base_uri" />&gt;
	</xsl:otherwise>
</xsl:choose>
	SetHandler modperl
	AuthType Basic
	AuthName Mioga2
	require valid-user
	AddOutputFilterByType DEFLATE text/xml application/x-json
	&lt;IfModule mod_headers.c&gt;
		RequestHeader unset Accept-Encoding
	&lt;/IfModule&gt;
<xsl:choose>
	<xsl:when test="$base_uri = '/'">
&lt;/LocationMatch&gt;
	</xsl:when>
	<xsl:otherwise>
&lt;/Location&gt;
	</xsl:otherwise>
</xsl:choose>
</exsl:document>

</xsl:template>

</xsl:stylesheet>
