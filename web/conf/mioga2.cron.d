# Update disk space two time in a day (noon and midnight)
0 */12 * * * www-data mioga2discspace.pl /etc/mioga2/Mioga.conf

# Evry 2 minutes run LDAP/External user syncrhonizer
*/2 * * * * www-data mioga2_nonlocal_synchronizer.pl /etc/mioga2/Mioga.conf

# Evry hour run quota checker
0 * * * * www-data quotasupervisor.pl /etc/mioga2/Mioga.conf

# Clean not used session evry minutes
* * * * * www-data find /var/tmp/mioga -type f -amin +60 -exec rm {} \;

