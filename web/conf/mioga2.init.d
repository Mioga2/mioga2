#! /bin/sh
### BEGIN INIT INFO
# Provides:          mioga-notifier
# Required-Start:    $local_fs $remote_fs $network $syslog $named
# Required-Stop:     $local_fs $remote_fs $network $syslog $named
# Should-Start:      postgresql
# Should-Stop:       postgresql
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: mioga2 initscript for Debian 6
# Description:       This file should be used to construct scripts to be
#                    placed in /etc/init.d.
### END INIT INFO

# $Header: /commun/alixen/cvsroot/Mioga2/debian/mioga2.init.d,v 1.1 2004/12/20 13:56:25 lsimonneau Exp $
# ============================================================================
# MIOGA Project (C) 2003 Alixen
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
# $Log: mioga2.init.d,v $
# Revision 1.1  2004/12/20 13:56:25  lsimonneau
# Add native debian package support.
#
# ============================================================================

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

NAME=mioga2
MAILINGLIST="no"
MIOGACONF=/var/lib/Mioga2/conf/Mioga.conf
DESC="Mioga2 daemons"

# Read configuration variable file if it is present
[ -r /etc/default/$NAME ] && . /etc/default/$NAME

# Define LSB log_* functions.
. /lib/lsb/init-functions

case "$1" in
  start)
        log_daemon_msg "Starting $DESC" "$NAME"
	echo

        if [ ! -d /var/spool/mioga ]; then
                mkdir /var/spool/mioga
		chgrp www-data /var/spool/mioga
		chmod 775 /var/spool/mioga
        fi

        if [ ! -d /var/run/mioga ]; then
                mkdir /var/run/mioga
		chgrp www-data /var/run/mioga
		chmod 775 /var/run/mioga
        fi

        if [ ! -p /var/spool/mioga/fifo ]; then
                mkfifo -m 666 /var/spool/mioga/fifo
        fi

        if [ ! -p /var/spool/mioga/notifier ]; then
                mkfifo -m 666 /var/spool/mioga/notifier
        fi

        if [ ! -p /var/spool/mioga/searchengine ]; then
                mkfifo -m 666 /var/spool/mioga/searchengine
        fi

# start services 
	if [ $MAILINGLIST = "yes" ]; then
		log_daemon_msg "Starting MailingList"
        	start-stop-daemon --start --quiet -c www-data:www-data \
                	--exec `which miogamailinglist.pl` -- $MIOGACONF
		case "$?" in
			0) log_end_msg 0 ;;
			1) log_progress_msg "already started"
		   	log_end_msg 0 ;;
			*) log_end_msg 1 ;;
		esac
	fi

	log_daemon_msg "Starting Notifier"
        start-stop-daemon --start --quiet --background -c root:root \
                --exec `which notifier.pl` -m --pidfile /var/run/mioga/notifier.pid -- $MIOGACONF
	case "$?" in
		0) log_end_msg 0 ;;
		1) log_progress_msg "already started"
		   log_end_msg 0 ;;
		*) log_end_msg 1 ;;
	esac

	log_daemon_msg "Starting SearchEngine"
	start-stop-daemon --start --quiet --background -c www-data:www-data \
                --exec `which searchengine.pl` -m --pidfile /var/run/mioga/searchengine.pid -- $MIOGACONF
	case "$?" in
		0) log_end_msg 0 ;;
		1) log_progress_msg "already started"
		   log_end_msg 0 ;;
		*) log_end_msg 1 ;;
	esac

        echo "."

        ;;
  stop)
        log_daemon_msg "Stopping $DESC" "$NAME"
	echo
	# "start-stop-daemon" Return
	#   0 if daemon has been stopped
	#   1 if daemon was already stopped
	#   other if daemon could not be stopped or a failure occurred
	if [ $MAILINGLIST = "yes" ]; then
		log_daemon_msg "Stopping MailingList"
        	start-stop-daemon --stop --quiet --pidfile /var/run/mioga/miogamailinglist.pid
		case "$?" in
			0) log_end_msg 0
			rm -f /var/run/mioga/miogamailinglist.pid ;;
			1) log_progress_msg "already stopped"
		   	log_end_msg 0 ;;
			*) log_end_msg 1 ;;
		esac
	fi

	log_daemon_msg "Stopping Notifier"
        start-stop-daemon --stop --quiet --pidfile /var/run/mioga/notifier.pid
	case "$?" in
		0) log_end_msg 0
		rm -f /var/run/mioga/notifier.pid ;;
		1) log_progress_msg "already stopped"
		   log_end_msg 0 ;;
		*) log_end_msg 1 ;;
	esac

	log_daemon_msg "Stopping SearchEngine"
	start-stop-daemon --stop --quiet --pidfile /var/run/mioga/searchengine.pid
	case "$?" in
		0) log_end_msg 0
		rm -f /var/run/mioga/searchengine.pid ;;
		1) log_progress_msg "already stopped"
		   log_end_msg 0 ;;
		*) log_end_msg 1 ;;
	esac

        echo "."
        ;;
  *)
        N=/etc/init.d/$NAME
        # echo "Usage: $N {start|stop|restart|reload|force-reload}" >&2
        echo "Usage: $N {start|stop}" >&2
        exit 1
        ;;
esac

exit 0
