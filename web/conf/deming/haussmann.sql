-- The statement below needs to be adapted to you installation first.
-- Replace <task_category_name> with the name of the category you want to link to Haussmann tasks
-- Replace <instance_name> with the instance you want this link to be created in
INSERT INTO activity_report_source SELECT task_category.rowid AS task_category_id, 'haussmann' AS ident FROM task_category, m_mioga WHERE task_category.mioga_id = m_mioga.rowid AND m_mioga.ident = '<instance_name>' AND task_category.name = '<task_category_name>';


CREATE TABLE activity_report_to_haussmann (
	report_id     int4 UNIQUE NOT NULL,
	task_id       int4 NOT NULL,

	FOREIGN KEY (report_id) REFERENCES activity_report (rowid),
	FOREIGN KEY (task_id) REFERENCES project_task (rowid)
);

CREATE VIEW haussmann_activity_report AS
	SELECT
		activity_report.rowid,
		activity_report.user_id,
		activity_report.dtstart,
		activity_report.dtend,
		activity_report.dtend - activity_report.dtstart AS duration,
		activity_report.notes,
		activity_report_source.ident AS source,
		project.rowid AS cat_id,
		project.label AS cat,
		project_task.rowid AS subcat_id,
		project_task.label AS subcat
	FROM
		activity_report,
		activity_report_source,
		activity_report_to_haussmann,
		project,
		project_task
	WHERE
		activity_report_source.ident = 'haussmann' AND
		activity_report.rowid = activity_report_to_haussmann.report_id AND
		project.rowid = project_task.project_id AND
		project_task.rowid = activity_report_to_haussmann.task_id;
