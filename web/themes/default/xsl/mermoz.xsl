<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ext="urn:ext" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<xsl:include href="tiny_mce.xsl"/>
<xsl:include href="scriptaculous.xsl"/>

<!-- ==============================
    Root document
    =============================== -->

<xsl:template name="mermoz-css">
  <link rel="stylesheet" type="text/css" href="{$theme_uri}/css/mermoz.css" media="screen" />
</xsl:template>

<xsl:template name="mermoz-js">
  <script src="{$theme_uri}/javascript/mermoz.js" type="text/javascript"></script>
  <script src="{$theme_uri}/javascript/locale/mermoz-lang-{//miogacontext/group/lang}.js" type="text/javascript"></script>
</xsl:template>

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
    <xsl:call-template name="favicon"/>
    <xsl:call-template name="title"/>
    <xsl:call-template name="scriptaculous-js"/>
    <xsl:call-template name="tinymce-js"/>
    <xsl:call-template name="tinymce-init-js"/>
    <xsl:call-template name="mioga-css"/>
    <xsl:call-template name="mermoz-css"/>
    <xsl:call-template name="mermoz-js"/>
	<xsl:call-template name="theme-css"/>
</head>
<body class="mermoz">
    <xsl:call-template name="DisplayAppTitle"/>
    <xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- =======================================
    DisplayMain
    ======================================= -->

<xsl:template match="DisplayMain">
	<script type="text/javascript">
		recipient_count = <xsl:value-of select="//CurrentGroup/count"/>;
		max_recipients = <xsl:value-of select="//MaxRecipients"/>;
	</script>
	<div id="global">
    <xsl:if test="Success">
        <div class="success"><xsl:value-of select="mioga:gettext('Message sent successfully.')"/></div>
    </xsl:if>
    <div class="contacts">
        <h2><xsl:value-of select="mioga:gettext('Recipients')"/></h2>
        <ul>
            <xsl:for-each select="ContactList/Group">
                <xsl:sort select="ident"/>
                <li class="group">
                    <xsl:choose>
                        <xsl:when test="count(Team) &gt; 0 or count(User) &gt; 0">
                            <img src="{$theme_uri}/images/unfold.gif" id="toggle-group-{rowid}"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <img src="{$theme_uri}/images/tee.png"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <label for="group-{rowid}"><xsl:value-of select="mioga:gettext('%s group (%d)', ident, count)"/></label> 
                    <input type="checkbox" value="group-{rowid}" name="group-{rowid}" id="group-{rowid}">
						<xsl:attribute name="onChange">updateCount (event, <xsl:value-of select="count"/>);</xsl:attribute>
                        <xsl:if test="rowid = //CurrentGroup/rowid">
                            <xsl:attribute name="checked" value="checked"/>
                        </xsl:if>
                    </input>
                    <xsl:if test="count(Team) &gt; 0">
                        <ul style="display: none;">
                            <xsl:for-each select="Team">
                                <xsl:sort select="ident"/>
                                <li class="team">
                                    <img src="{$theme_uri}/images/elbow.gif"/>&#160;
                                    <label for="group-{../rowid}-team-{rowid}"><xsl:value-of select="mioga:gettext('%s team (%d)', ident, count)"/></label> 
                                    <input type="checkbox" name="team-{rowid}" id="group-{../rowid}-team-{rowid}" value="team-{rowid}">
										<xsl:attribute name="onChange">updateCount (event, <xsl:value-of select="count"/>);</xsl:attribute>
                                        <xsl:if test="../rowid = //CurrentGroup/rowid">
                                            <xsl:attribute name="checked" value="checked"/>
                                        </xsl:if>
                                    </input>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </xsl:if>
                    <xsl:if test="count(User) &gt; 0">
                        <ul style="display: none;">
                            <xsl:for-each select="User">
                                <xsl:sort select="name"/>
                                <li class="user">
                                    <img src="{$theme_uri}/images/elbow.gif"/>&#160;
                                    <label for="group-{../rowid}-user-{rowid}"><xsl:value-of select="name"/></label>
                                    <input type="checkbox" name="user-{rowid}" id="group-{../rowid}-user-{rowid}" value="user-{rowid}">
										<xsl:attribute name="onChange">updateCount (event, 1);</xsl:attribute>
                                        <xsl:if test="../rowid = //CurrentGroup/rowid">
                                            <xsl:attribute name="checked" value="checked"/>
                                        </xsl:if>
                                    </input>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </xsl:if>
                </li>
            </xsl:for-each>
        </ul>
    </div>
    <div class="dialog form">
        <div class="sbox border_color">
            <h2 class="title_bg_color"><xsl:value-of select="mioga:gettext('Send mail')"/></h2>
            <div class="content">
                <form action="SendMail" method="POST" onsubmit="return check_data();">
                    <input type="hidden" name="recipients" id="recipients" value=""/>
					<xsl:call-template name="CSRF-input"/>
                    <p>
                        <label><xsl:value-of select="mioga:gettext('Sender:')"/></label>&#160;<xsl:value-of select="CurrentUser/name"/> (<xsl:value-of select="CurrentUser/email"/>)                        
                    </p>
                    <p>
                        <label for="subject"><xsl:value-of select="mioga:gettext('Subject:')"/>&#160;</label>
                        <input class="text" type="text" id="subject" name="subject">
                            <xsl:if test="Subject">
                                <xsl:attribute name="value"><xsl:value-of select="Subject"/></xsl:attribute>
                            </xsl:if>
                        </input>
                    </p>
					<p>
						<label for="detail-recipients"><xsl:value-of select="mioga:gettext ('Detail recipients')"/></label>
						<input type="checkbox" id="detail-recipients" name="detail-recipients" checked="checked"/>
					</p>
                    <p>
                        <label for="contents"><xsl:value-of select="mioga:gettext('Message')"/></label><br/>
                        <textarea name="contents" id="contents" cols="80" rows="20" class="mceEditor">
                            <xsl:if test="Message">
                                <xsl:value-of select="Message"/>
                            </xsl:if>
                        </textarea> 
                    </p>
                    <p>
                        <label for="toggle-signature"><xsl:value-of select="mioga:gettext('Signature')"/></label> 
                        <input class="checkbox" type="checkbox" name="toggle-signature" value="1" id="toggle-signature">
                            <xsl:if test="Signature/enabled = '1'"><xsl:attribute name="checked" value="checked"/></xsl:if>
                        </input>
                        <br/>
                        <textarea disabled="disabled" class="disabled" name="signature" id="signature" cols="80" rows="3">
                            <xsl:value-of select="Signature/contents"/>
                        </textarea>
                    </p>
                    <p>
                        <ul class="button_list">
                            <li>
                                <input type="submit" class="button" value="OK"/>
                            </li>
                        </ul>
                    </p>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var image_path = '<xsl:value-of select="$theme_uri"/>/images';
        init_mermoz({signature_enabled: '<xsl:value-of select="Signature/enabled"/>'});
    </script>
	</div>
</xsl:template>

</xsl:stylesheet>
