<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
<!--  <xsl:output method="html"/> -->

  
  <!-- ========================
       MIOGA MENU
       ======================== -->
  
  <!-- generate the tabs menu -->
  
  <xsl:template name="MiogaMenu">
	<div>
		<ul class="tabs">
				  <xsl:apply-templates select="." mode="MiogaMenuBody"/>
		</ul>
		<div class="clear-both"></div>
	</div>
  </xsl:template>
  
  
  
  <!-- add a tab to a tabs based menu -->
  
<xsl:template name="MiogaMenuTab">
	<xsl:param name="label"/>
	<xsl:param name="href"/>
	<xsl:param name="active" select="$href = $mioga_method" />
	<li>
		<xsl:if test="$active='1'">
			<xsl:attribute name="class">selected</xsl:attribute>
		</xsl:if>
		<a href="{$href}">
			<xsl:call-template name="MiogaMenuTranslation">
				<xsl:with-param name="label" select="$label"/>
			</xsl:call-template>
		</a>
	</li>
      
</xsl:template>
  
</xsl:stylesheet>
