<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>
<xsl:import href="jquery.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<!-- ===============
	root node
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="title"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="jquery-js"/>
	<xsl:call-template name="jquery-ui-1.12.1"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery-ui-mioga2.css"/>

	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/fullcalendar.css"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.miniColors.css"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.tableScrollableSortable.css"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.userSelect.css"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.itemSelect.css"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/mioga.ui.minical.css"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.dropDown.css"/>
	<script src="{$theme_uri}/javascript/chronos.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/chronos.css"/>
	<xsl:call-template name="theme-css"/>
	<xsl:apply-templates mode="head"/>
</head>
<body class="chronos mioga">
	<xsl:call-template name="DisplayAppTitle">
	</xsl:call-template>

	<xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- ===============
	DisplayMain
	================ -->

<xsl:template match="/DisplayMain" mode="head">
	<script type="text/javascript">

	$(document).ready(function(){
		$( "#progressbar" ).progressbar({ value: 0 });
		$('#chronos').chronos({
								dayNamesMin : ["<xsl:value-of select="mioga:gettext('Su')"/>",
											   "<xsl:value-of select="mioga:gettext('Mo')"/>",
											   "<xsl:value-of select="mioga:gettext('Tu')"/>",
											   "<xsl:value-of select="mioga:gettext('We')"/>",
											   "<xsl:value-of select="mioga:gettext('Th')"/>",
											   "<xsl:value-of select="mioga:gettext('Fr')"/>",
											   "<xsl:value-of select="mioga:gettext('Sa')"/>" ],
								dayNamesShort : ["<xsl:value-of select="mioga:gettext('Sun')"/>",
											   "<xsl:value-of select="mioga:gettext('Mon')"/>",
											   "<xsl:value-of select="mioga:gettext('Tue')"/>",
											   "<xsl:value-of select="mioga:gettext('Wed')"/>",
											   "<xsl:value-of select="mioga:gettext('Thu')"/>",
											   "<xsl:value-of select="mioga:gettext('Fri')"/>",
											   "<xsl:value-of select="mioga:gettext('Sat')"/>" ],
								dayNames : ["<xsl:value-of select="mioga:gettext('Sunday')"/>",
											   "<xsl:value-of select="mioga:gettext('Monday')"/>",
											   "<xsl:value-of select="mioga:gettext('Tuesday')"/>",
											   "<xsl:value-of select="mioga:gettext('Wednesday')"/>",
											   "<xsl:value-of select="mioga:gettext('Thursday')"/>",
											   "<xsl:value-of select="mioga:gettext('Friday')"/>",
											   "<xsl:value-of select="mioga:gettext('Saturday')"/>" ],
								monthNames:[ "<xsl:value-of select="mioga:gettext('January')"/>",
											 "<xsl:value-of select="mioga:gettext('February')"/>",
											 "<xsl:value-of select="mioga:gettext('March')"/>",
											 "<xsl:value-of select="mioga:gettext('April')"/>",
											 "<xsl:value-of select="mioga:gettext('May')"/>",
											 "<xsl:value-of select="mioga:gettext('June')"/>",
											 "<xsl:value-of select="mioga:gettext('July')"/>",
											 "<xsl:value-of select="mioga:gettext('August')"/>",
											 "<xsl:value-of select="mioga:gettext('September')"/>",
											 "<xsl:value-of select="mioga:gettext('October')"/>",
											 "<xsl:value-of select="mioga:gettext('November')"/>",
											 "<xsl:value-of select="mioga:gettext('December')"/>" ],
								monthNamesShort:[ "<xsl:value-of select="mioga:gettext('Jan')"/>",
											 "<xsl:value-of select="mioga:gettext('Feb')"/>",
											 "<xsl:value-of select="mioga:gettext('Mar')"/>",
											 "<xsl:value-of select="mioga:gettext('Apr')"/>",
											 "<xsl:value-of select="mioga:gettext('May')"/>",
											 "<xsl:value-of select="mioga:gettext('Jun')"/>",
											 "<xsl:value-of select="mioga:gettext('Jul')"/>",
											 "<xsl:value-of select="mioga:gettext('Aug')"/>",
											 "<xsl:value-of select="mioga:gettext('Sep')"/>",
											 "<xsl:value-of select="mioga:gettext('Oct')"/>",
											 "<xsl:value-of select="mioga:gettext('Nov')"/>",
											 "<xsl:value-of select="mioga:gettext('Dec')"/>" ],
								buttonText : { today : "<xsl:value-of select="mioga:gettext('today')"/>",
												month: "<xsl:value-of select="mioga:gettext('month')"/>",
												week : "<xsl:value-of select="mioga:gettext('week')"/>",
												day  : "<xsl:value-of select="mioga:gettext('day')"/>" },
								allDayText : "<xsl:value-of select="mioga:gettext('all-day')"/>",
								columnFormat : {	month: "<xsl:value-of select="mioga:gettext('ddd')"/>",
													week: "<xsl:value-of select="mioga:gettext('ddd M/d')"/>",
													day: "<xsl:value-of select="mioga:gettext('dddd M/d')"/>" },
								firstDay : parseInt("<xsl:value-of select="mioga:gettext('firstDay')"/>", 10),
								i18n : {
										progressBarText:"<xsl:value-of select="mioga:gettext('Chronos initializing, please wait ...')"/>",
										nextText : "<xsl:value-of select="mioga:gettext('Next')"/>",
										previousText : "<xsl:value-of select="mioga:gettext('Previous')"/>",
										createText : "<xsl:value-of select="mioga:gettext('Create event')"/>",
										configurationText : "<xsl:value-of select="mioga:gettext('Configuration')"/>",
										projectTasksText : "<xsl:value-of select="mioga:gettext('Project Tasks')"/>",

										tasksText : "<xsl:value-of select="mioga:gettext('Tasks / Todos')"/>",
										dateText : "<xsl:value-of select="mioga:gettext('Date')"/>",
										evInformations:"<xsl:value-of select="mioga:gettext('Informations')"/>",
										evOptions:"<xsl:value-of select="mioga:gettext('Options')"/>",
										evSubject:"<xsl:value-of select="mioga:gettext('Subject')"/>",
										evDescription:"<xsl:value-of select="mioga:gettext('Description')"/>",
										evStart:"<xsl:value-of select="mioga:gettext('From')"/>",
										evEnd:"<xsl:value-of select="mioga:gettext('to')"/>",
										evCalendar:"<xsl:value-of select="mioga:gettext('Calendar')"/>",
										evAllDay:"<xsl:value-of select="mioga:gettext('All day')"/>",
										evCategory:"<xsl:value-of select="mioga:gettext('Category')"/>",
										evRRule:"<xsl:value-of select="mioga:gettext('Recurrence')"/>",
										evRepeat:"<xsl:value-of select="mioga:gettext('Repeat every')"/>",
										evRDay:"<xsl:value-of select="mioga:gettext('days')"/>",
										evRDayOfWeek:"<xsl:value-of select="mioga:gettext('Day of week')"/>",
										evRWeek:"<xsl:value-of select="mioga:gettext('weeks')"/>",
										evRMonth:"<xsl:value-of select="mioga:gettext('months')"/>",
										evRYear:"<xsl:value-of select="mioga:gettext('years')"/>",
										evRPeriod:"<xsl:value-of select="mioga:gettext('Period')"/>",
										evRend:"<xsl:value-of select="mioga:gettext('End')"/>",
										evRthe:"<xsl:value-of select="mioga:gettext('the')"/>",
										evRendNever:"<xsl:value-of select="mioga:gettext('Never')"/>",
										evRendCount:"<xsl:value-of select="mioga:gettext('Count')"/>",
										evRendDate:"<xsl:value-of select="mioga:gettext('Date')"/>",
										evRweekDay:"<xsl:value-of select="mioga:gettext('Day of week')"/>",
										evRmonthDay:"<xsl:value-of select="mioga:gettext('Day of month')"/>",
										evRlastMonthDay:"<xsl:value-of select="mioga:gettext('Last day of month')"/>",
										evRfirst:"<xsl:value-of select="mioga:gettext('First')"/>",
										evRsecond:"<xsl:value-of select="mioga:gettext('Second')"/>",
										evRthird:"<xsl:value-of select="mioga:gettext('Third')"/>",
										evRfourth:"<xsl:value-of select="mioga:gettext('Fourth')"/>",
										evRlast:"<xsl:value-of select="mioga:gettext('Last')"/>",
										dateFormat:"<xsl:value-of select="mioga:gettext('dd/mm/yy')"/>",
										dateFormatLong:"<xsl:value-of select="mioga:gettext('ddd, dd MMM yyyy')"/>",
										createEventTitle:"<xsl:value-of select="mioga:gettext('Create an event')"/>",
										editEventTitle:"<xsl:value-of select="mioga:gettext('Edit an event')"/>",
										addCalendarTitle:"<xsl:value-of select="mioga:gettext('Add a calendar')"/>",
										editCalendarTitle:"<xsl:value-of select="mioga:gettext('Modify the calendar')"/>",
										viewCalendar:"<xsl:value-of select="mioga:gettext('Calendar view')"/>",
										viewList:"<xsl:value-of select="mioga:gettext('List view')"/>",
										calendarListTitle:"<xsl:value-of select="mioga:gettext('Calendars')"/>",
										otherParamsTitle:"<xsl:value-of select="mioga:gettext('Other parameters')"/>",
										showWeekend : "<xsl:value-of select="mioga:gettext('Show weekends')"/>",
										firstHour : "<xsl:value-of select="mioga:gettext('First displayed hour')"/>",
										colorscheme : "<xsl:value-of select="mioga:gettext('Color mode')"/>",
										colorModeActivity : "<xsl:value-of select="mioga:gettext('Background color for activity, border for calendar')"/>",
										colorModeCalendar : "<xsl:value-of select="mioga:gettext('Background color for calendar, border for activity')"/>",
										visibleCal: "<xsl:value-of select="mioga:gettext('Visible')"/>",
										hiddenCal: "<xsl:value-of select="mioga:gettext('Hidden')"/>",
										calUserEdit:"<xsl:value-of select="mioga:gettext('Agenda personnel')"/>",
										ownerCalendars : "<xsl:value-of select="mioga:gettext('My calendars')"/>",
										groupCalendars : "<xsl:value-of select="mioga:gettext('Group calendars')"/>",
										otherCalendars : "<xsl:value-of select="mioga:gettext('Other calendars')"/>",
										addNewCalendar : "<xsl:value-of select="mioga:gettext('Add a calendar')"/>",
										addAccessRight : "<xsl:value-of select="mioga:gettext('Add an access right')"/>",
										accessRightTitle : "<xsl:value-of select="mioga:gettext('Add access right')"/>",
										readOnlyLabel : "<xsl:value-of select="mioga:gettext('Read only')"/>",
										readWriteLabel : "<xsl:value-of select="mioga:gettext('Read / Write')"/>",
										userAccessLabel : "<xsl:value-of select="mioga:gettext('User')"/>",
										suppressAccess : "<xsl:value-of select="mioga:gettext('Suppress access')"/>",
										teamAccessLabel : "<xsl:value-of select="mioga:gettext('Team')"/>",
										addLabel : "<xsl:value-of select="mioga:gettext('Add')"/>",
										searchLabel : "<xsl:value-of select="mioga:gettext('Search')"/>",
										firstnameLabel : "<xsl:value-of select="mioga:gettext('firstname')"/>",
										lastnameLabel : "<xsl:value-of select="mioga:gettext('lastname')"/>",
										emailLabel : "<xsl:value-of select="mioga:gettext('Email')"/>",
										notFoundLabel : "<xsl:value-of select="mioga:gettext('User not found ...')"/>",
										selectAllLabel : "<xsl:value-of select="mioga:gettext('Select all')"/>",
										selectNoneLabel : "<xsl:value-of select="mioga:gettext('Select none')"/>",
										allLabel : "<xsl:value-of select="mioga:gettext('All')"/>",
										exampleEvent : "<xsl:value-of select="mioga:gettext('Event')"/>",
										suppressCalendar : "<xsl:value-of select="mioga:gettext('Suppress calendar')"/>",
										exportICS : "<xsl:value-of select="mioga:gettext('Export to iCalendar format (ICS)')"/>",
										returnToMain : "<xsl:value-of select="mioga:gettext('Return to calendar')"/>",
										saveButton : "<xsl:value-of select="mioga:gettext('Save')"/>",
										calName : "<xsl:value-of select="mioga:gettext('Name')"/>",
										calColor : "<xsl:value-of select="mioga:gettext('Text color')"/>",
										calBGColor : "<xsl:value-of select="mioga:gettext('Background color')"/>",
										calColor : "<xsl:value-of select="mioga:gettext('Text color')"/>",
										calBGColor : "<xsl:value-of select="mioga:gettext('Background color')"/>",
										groupColor : "<xsl:value-of select="mioga:gettext('Calendar text color')"/>",
										groupBGColor : "<xsl:value-of select="mioga:gettext('Calendar background color')"/>",
										userSearchLabel : "<xsl:value-of select="mioga:gettext('Search user')"/>",
										userFromGroupLabel : "<xsl:value-of select="mioga:gettext('Users from groups')"/>",
										teamLabel : "<xsl:value-of select="mioga:gettext('Teams')"/>",
										detailText:"<xsl:value-of select="mioga:gettext('detail ...')"/>",
										OKLabel:"<xsl:value-of select="mioga:gettext('OK')"/>",
										applyLabel:"<xsl:value-of select="mioga:gettext('Apply')"/>",
										suppressLabel:"<xsl:value-of select="mioga:gettext('Suppress')"/>",
										supOccurence:"<xsl:value-of select="mioga:gettext('Suppress all occurences')"/>",
										confirmSuppressUserCal:"<xsl:value-of select="mioga:gettext('Delete calendar with all events ?')"/>",
										confirmDetachCal:"<xsl:value-of select="mioga:gettext('Detach from calendar ?')"/>",
										closeLabel:"<xsl:value-of select="mioga:gettext('Close')"/>",
										cancelLabel:"<xsl:value-of select="mioga:gettext('Cancel')"/>",
										noProjectTask : "<xsl:value-of select="mioga:gettext('No project tasks to display')"/>",
										infoProjectTask : "<xsl:value-of select="mioga:gettext('Project task detail')"/>",
										infoUserPlanning  : "<xsl:value-of select="mioga:gettext('User planning')"/>",
										responsableLabel : "<xsl:value-of select="mioga:gettext('R')"/>",
										attendeeLabel : "<xsl:value-of select="mioga:gettext('A')"/>",
										eventLabel : "<xsl:value-of select="mioga:gettext('Visible')"/>",
										ptEventSet : "<xsl:value-of select="mioga:gettext('Show event')"/>",
										ptEventUnset : "<xsl:value-of select="mioga:gettext('Do not show event')"/>",
										projectLabel : "<xsl:value-of select="mioga:gettext('Project')"/>",
										groupLabel : "<xsl:value-of select="mioga:gettext('Group')"/>",
										taskLabel : "<xsl:value-of select="mioga:gettext('Task')"/>",
										progressLabel : "<xsl:value-of select="mioga:gettext('Progress (%)')"/>",
										workloadLabel : "<xsl:value-of select="mioga:gettext('Workload (h)')"/>",
										pstartLabel : "<xsl:value-of select="mioga:gettext('Planned begin')"/>",
										pendLabel : "<xsl:value-of select="mioga:gettext('Planned end')"/>",
										dstartLabel : "<xsl:value-of select="mioga:gettext('Work start')"/>",
										dendLabel : "<xsl:value-of select="mioga:gettext('Work end')"/>",
										editProjectTaskTitle:"<xsl:value-of select="mioga:gettext('Edit a project task')"/>",
										hourText:"<xsl:value-of select="mioga:gettext('Hour')"/>",
										minuteText:"<xsl:value-of select="mioga:gettext('Minute')"/>",

										errorProjectTaskIsAllday : "<xsl:value-of select="mioga:gettext('Project task is only allday')"/>",
										errorProjectTaskIsAllday : "<xsl:value-of select="mioga:gettext('Project task is only allday')"/>",
										errorCannotModifyProjectTask : "<xsl:value-of select="mioga:gettext('Cannot modify project task')"/>",
										errorCannotModifyRecurEvent : "<xsl:value-of select="mioga:gettext('Cannot modify recurrent event')"/>",
										errorCalendarReadOnly : "<xsl:value-of select="mioga:gettext('This calendar is read only')"/>",
										errorNoCalendar : "<xsl:value-of select="mioga:gettext('There is no calendar, create one')"/>",
										errorNoCategories : "<xsl:value-of select="mioga:gettext('There is no defined categories, ask administrator to create one')"/>",
										errorNoCurrentCalendar : "<xsl:value-of select="mioga:gettext('There is no current calendar selected')"/>",
										categoryLegend: "<xsl:value-of select="mioga:gettext('Category legend')"/>",
										reattachWindow: "<xsl:value-of select="mioga:gettext('Reattach the window')"/>"
										}
							});
	 });
	</script>
</xsl:template>

<xsl:template match="/DisplayMain">
	<div id="chronos" class="chronos-main"></div>
</xsl:template>

</xsl:stylesheet>
