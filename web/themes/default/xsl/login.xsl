<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<xsl:include href="scriptaculous.xsl"/>
<xsl:include href="inline_message.xsl"/>

<!-- ===============
	root document
	================ -->

<xsl:template name="login-css">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/login.css" media="screen" />
</xsl:template>

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="scriptaculous-js"/>

	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>

	<xsl:call-template name="login-css"/>

	<xsl:apply-templates mode="head"/>
	<xsl:call-template name="theme-css"/>
</head>
<body id="Login_body" class="Login mioga">
	<xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- ===============
	DisplayMain
	================ -->

<xsl:template match="DisplayMain" mode="head">
</xsl:template>

<xsl:template match="DisplayMain">
	<div class="login-box">
		<form class="form" method="post">
			<xsl:attribute name="action">?target=<xsl:value-of select="Target"/></xsl:attribute>
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Login')"/></legend>
				<input type="hidden" name="target">
					<xsl:attribute name="value"><xsl:value-of select="Target"/></xsl:attribute>
				</input>
				<div class="form-item">
					<label for="login"><xsl:value-of select="mioga:gettext ('Email address')"/></label>
					<input name="login"/>
				</div>
				<div class="form-item">
					<label for="password"><xsl:value-of select="mioga:gettext ('Password')"/></label>
					<input type="password" name="password"/>
				</div>
				<ul class="button_list">
					<li><button type="submit" class="button"><xsl:value-of select="mioga:gettext ('Login')"/></button></li>
				</ul>
			</fieldset>
		</form>
	</div>
</xsl:template>

</xsl:stylesheet>
