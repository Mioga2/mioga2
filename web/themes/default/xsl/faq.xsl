<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:output method="html" indent="yes"/>
<xsl:include href="base.xsl"/>
<xsl:include href="tiny_mce.xsl"/>
<xsl:include href="jquery.xsl"/>
<xsl:include href="dojotoolkit.xsl"/>

<!-- ===============
	root document
	================ -->

<xsl:template match="/">
	<xsl:call-template name="doctype"/>
<html>
  <head>
    <xsl:call-template name="mioga-css"/>
    <xsl:call-template name="jquery-js"/>
    <xsl:call-template name="mioga-js"/>
    <link rel="stylesheet" type="text/css" href="{$theme_uri}/css/faq.css" media="screen" />
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
    <xsl:call-template name="dojo-css"/>
    <xsl:call-template name="dojo-js-debug"/>
    <xsl:call-template name="mioga_dojo-js"/>
    <xsl:call-template name="tinymce-js"/>
    <xsl:call-template name="tinymce-init-js"/>
    
    <script type="text/javascript" src="{$theme_uri}/javascript/faq.js" />
    <!-- Values that must be known _once_ in the JavaScript -->
    <script type="text/javascript">
      var faqVars = {
        idcat: "<xsl:value-of select="/ViewFaq/idcat"/>",
		i18n: {
			edit_tooltip: "<xsl:value-of select="mioga:gettext ('Modify this entry')"/>",
			delete_tooltip: "<xsl:value-of select="mioga:gettext ('Delete question')"/>"
		},
        <xsl:choose>
          <xsl:when test="/ViewFaq/admin='yes'">adminSession: true</xsl:when>
          <xsl:otherwise>adminSession: false</xsl:otherwise>
        </xsl:choose>
      };
    </script>
  	<xsl:call-template name="theme-css"/>
</head>
	<xsl:apply-templates />
</html>
</xsl:template>
  
<!--
================================================================================
ViewFaq
================================================================================
-->

<xsl:template match="ViewFaq">
  <body id="faq" class="mioga" xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
      <xsl:with-param name="help">applications.html#faq</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="AppNav" />

    <xsl:if test="count(category)!=0 or admin='yes'">
      <div id="category_box">
        <div id="categories"> 
          <xsl:for-each select="category">
            <xsl:element name="div">
              <xsl:attribute name="id">cat_<xsl:value-of select="rowid" /></xsl:attribute>
              <xsl:choose>
                <xsl:when test="position() mod 2 = 0">
                  <xsl:attribute name="class">category even</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="class">category odd</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
                      
              <xsl:if test="../admin='yes'">
                <xsl:element name="div">
                  <xsl:choose>
                    <xsl:when test="@empty='true'">
                      <xsl:attribute name="class">icon_delete</xsl:attribute>
                      <xsl:attribute name="title">
                        <xsl:call-template name="faqTranslation">
                          <xsl:with-param name="type">delete-category</xsl:with-param>
                        </xsl:call-template>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="class">deletion_not_possible</xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:element>
                <xsl:element name="div">
                  <xsl:attribute name="class">icon_edit</xsl:attribute>
                  <xsl:attribute name="title">
                    <xsl:call-template name="faqTranslation">
                      <xsl:with-param name="type">modify-category</xsl:with-param>
                    </xsl:call-template>
                  </xsl:attribute>
                </xsl:element>
              </xsl:if>              
              
              <xsl:element name="a">
                <xsl:attribute name="class">cat_link</xsl:attribute>
                <xsl:attribute name="href">
                  <xsl:value-of select="../action/@method" />?idcat=<xsl:value-of select="./rowid"/>
                </xsl:attribute>
                <xsl:value-of select="./title" disable-output-escaping="yes"/>
              </xsl:element>
            </xsl:element>
          </xsl:for-each>
              
        </div>  
        <xsl:if test="./admin='yes'">
          <div id="div_btn_cat_new">
            <button id="btn_cat_new" class="button">
              <xsl:call-template name="faqTranslation">
                <xsl:with-param name="type">add-category</xsl:with-param>
              </xsl:call-template>
            </button>
          </div>
        </xsl:if>
      </div>
    </xsl:if>
    
    <div id="faq_content">
      <xsl:if test="count(entry)>0">
        <ul class="hmenu">
          <li><a href="#" id="open_entries"><xsl:value-of select="mioga:gettext('Open all entries')"/></a></li>
          <li><a href="#" id="close_entries"><xsl:value-of select="mioga:gettext('Close all entries')"/></a></li>
        </ul>
      </xsl:if>
      
      <xsl:if test="./admin='yes'">
        <div id="div_btns_entry">
          <span id="span_btn_entry_new">
            <button id="btn_entry_new" class="button">
              <xsl:call-template name="faqTranslation">
                <xsl:with-param name="type">add-entry</xsl:with-param>
              </xsl:call-template>
            </button>
          </span>
        </div>
      </xsl:if>
      
      <div id="faq_entries">
        <xsl:choose>
          <xsl:when test="count(entry)>0">        
            <xsl:for-each select="entry">
              <xsl:element name="div">
                <xsl:attribute name="class">faq_entry</xsl:attribute>
                <xsl:attribute name="id">faq_entry_<xsl:value-of select="rowid"/></xsl:attribute>
                <xsl:attribute name="dojoType">mioga.FaqPane</xsl:attribute>
                <xsl:attribute name="title"><xsl:value-of select="title" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:if test="../admin='yes'">
                  <xsl:attribute name="entry_id"><xsl:value-of select="rowid"/></xsl:attribute>
                  <xsl:attribute name="adminSession">true</xsl:attribute>
                </xsl:if>
                <div class="question">
                  <xsl:value-of select="question" disable-output-escaping="yes"/>
                </div>
                <div class="answer">
                  <xsl:value-of select="answer" disable-output-escaping="yes"/>
                </div>
              </xsl:element>
            </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
            <p><xsl:value-of select="mioga:gettext('This category is empty.')"/></p>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="admin='yes'">
          <div id="faq_entry_new" dojoType="mioga.FaqPane" newEntry="true">
            <xsl:attribute name="category_id"><xsl:value-of select="idcat" /></xsl:attribute>
          </div>
        </xsl:if>
      </div>
    </div> 
  </body>
</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<xsl:template name="faqTranslation">
  <xsl:param name="type"/>

  <xsl:choose>
    <xsl:when test="$type='sub-categories'"><xsl:value-of select="mioga:gettext('Sub categories')"/></xsl:when>
    <xsl:when test="$type='add-category'"><xsl:value-of select="mioga:gettext('Add category')"/></xsl:when>
    <xsl:when test="$type='modify-category'"><xsl:value-of select="mioga:gettext('Modify this category')"/></xsl:when>
    <xsl:when test="$type='delete-category'"><xsl:value-of select="mioga:gettext('Delete this category')"/></xsl:when>
    <xsl:when test="$type='no-sub-categories'"><xsl:value-of select="mioga:gettext('There is no sub category')"/></xsl:when>
    <xsl:when test="$type='questions-answers'"><xsl:value-of select="mioga:gettext('Questions and answers')"/></xsl:when>
    <xsl:when test="$type='add-entry'"><xsl:value-of select="mioga:gettext('Add entry')"/></xsl:when>
    <xsl:when test="$type='no-question-answer'"><xsl:value-of select="mioga:gettext('No question/answer')"/></xsl:when>
    <xsl:when test="$type='modify-entry'"><xsl:value-of select="mioga:gettext('Modify this entry')"/></xsl:when>
    <xsl:when test="$type='delete-question'"><xsl:value-of select="mioga:gettext('Delete question')"/></xsl:when>
    <xsl:when test="$type='confirm-delete-question'"><xsl:value-of select="mioga:gettext('Are you sure you want to delete the following questions?')"/></xsl:when>
    <xsl:when test="$type='confirm-delete-category'"><xsl:value-of select="mioga:gettext('Are you sure you want to delete this category?')"/></xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
