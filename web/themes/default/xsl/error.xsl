<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
  <xsl:output method="html"/>
 
  <xsl:include href="base.xsl"/>

  <xsl:template match="DisplayErrorMessage" mode="header">
    <xsl:choose>
      <xsl:when test="./ExceptionType='ExternalMioga'">
        <xsl:apply-templates select="ExceptionType"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="errorMessageTranslation">
          <xsl:with-param name="type">type-error</xsl:with-param>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

<xsl:template match="DisplayErrorMessage">
<body xsl:use-attribute-sets="body_attr">
    <br/>
    <center>
      <p>
        <font size="+1" color="red">
          <b><xsl:apply-templates select="." mode="header"/></b>
        </font>
      </p>

      <br/>
      <br/>
      <br/>
      
      <table xsl:use-attribute-sets="simple_table">
        <xsl:if test="./ExceptionType='ExternalMioga'">
          <tr>
            <td nowrap="nowrap">
              <p>
                <xsl:call-template name="errorMessageTranslation">
                  <xsl:with-param name="type">unvailable</xsl:with-param>
                </xsl:call-template>
              </p>
            </td>
          </tr>
          <tr>
            <td nowrap="nowrap"><font size="+1">&#160;</font></td>
          </tr>
          <tr>
            <td nowrap="nowrap">
              <p>
                <xsl:call-template name="errorMessageTranslation">
                  <xsl:with-param name="type">returned-error</xsl:with-param>
                </xsl:call-template>
              </p>
            </td>
          </tr>
        </xsl:if>
        <tr>
          <td nowrap="nowrap">
            <p>
              <xsl:value-of select="ExceptionMessage" disable-output-escaping="yes"/>
            </p>
          </td>
        </tr>
      </table>
      
      <br/>
      <br/>
      <br/>
      
      <p>
        <font size="+1">
          <b>
            <xsl:call-template name="errorMessageTranslation">
              <xsl:with-param name="type">contact-admin</xsl:with-param>
            </xsl:call-template>
          </b>
        </font>
      </p>
    </center>
</body>
</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<xsl:template match="ExceptionType">
    <xsl:choose>
        <xsl:when test=".='Simple'"><xsl:value-of select="mioga:gettext('Application error (Simple)')"/></xsl:when>
        <xsl:when test=".='DB'"><xsl:value-of select="mioga:gettext('Database access error (DB)')"/></xsl:when>
        <xsl:when test=".='ExternalMioga'"><xsl:value-of select="mioga:gettext('Unable to connect to external Mioga.')"/></xsl:when>
        <xsl:otherwise><xsl:value-of select="mioga:gettext('Unknown error (Other)')"/></xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="errorMessageTranslation">
    <xsl:param name="type" />
    <xsl:choose>
        <xsl:when test="$type='contact-admin'"><xsl:value-of select="mioga:gettext('Please contact your system administrator to get more information.')"/></xsl:when>
        <xsl:when test="$type='returned-error'"><xsl:value-of select="mioga:gettext('Error returned by server:')"/></xsl:when>
        <xsl:when test="$type='unavailable'">
          <xsl:value-of select="mioga:gettext('External Mioga is most likely temporarily unavailable.')"/>
          <br/>
          <xsl:value-of select="mioga:gettext('Please retry later.')"/>
        </xsl:when>
        <xsl:when test="$type='type-error'">
          <xsl:variable name="error_type"><xsl:apply-templates select="ExceptionType"/></xsl:variable>
          <xsl:value-of select="mioga:gettext('A %s error occured.', string($error_type))"/>
        </xsl:when>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
