<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ext="urn:ext" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<xsl:template name="rss-css">
  <link rel="stylesheet" type="text/css" href="{$theme_uri}/css/rss.css" media="screen" />
</xsl:template>

<xsl:template name="rss-js">
  <script src="{$theme_uri}/javascript/rss.js" type="text/javascript"></script>
</xsl:template>

<!-- ==============================
    Root document
    =============================== -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
    <xsl:call-template name="favicon"/>
    <xsl:call-template name="title"/>
    <script type="text/javascript" src="{$jslib_uri}/scriptaculous/prototype.js"></script>
    <xsl:call-template name="mioga-css"/>
    <xsl:call-template name="rss-css"/>
    <xsl:call-template name="rss-js"/>
	<xsl:call-template name="theme-css"/>
</head>
<body class="rss">
  <xsl:call-template name="DisplayAppTitle"/>
  <xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- =======================================
    DisplayMain
    ======================================= -->

<xsl:template match="DisplayMain">
    <xsl:if test="Success">
        <div class="success"><xsl:value-of select="mioga:gettext('Options updated successfully.')"/></div>
    </xsl:if>
    <div class="dialog">
        <div class="sbox border_color">
            <h2 class="title_bg_color"><xsl:value-of select="mioga:gettext('Select RSS feeds')"/></h2>
            <div class="content">
                <form action="DisplayMain" method="post">
                    <h2 class="fpg">
                        <input type="checkbox" value="1" name="feed_per_group" id="feed_per_group">
                            <xsl:if test="//FeedPerGroup">
                                <xsl:attribute name="checked" value="checked"/>
                            </xsl:if>
                        </input>&#160;<label for="feed_per_group"><xsl:value-of select="mioga:gettext('Show one feed for each group')"/></label>
                    </h2>
                    <ul class="feeds">
                        <h2><xsl:value-of select="mioga:gettext('Feeds')"/></h2>
                        <xsl:for-each select="Group">
                            <xsl:sort select="ident"/>
							<xsl:variable name="group_ident"><xsl:value-of select="ident"/></xsl:variable>
                            <li class="group">
                                <xsl:choose>
                                    <xsl:when test="count(Application) &gt; 0">
                                        <img src="{$theme_uri}/images/unfold.gif" id="toggle-group-{$group_ident}"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <img src="{$theme_uri}/images/bullet_blue.png"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <label for="group-{$group_ident}"><xsl:value-of select="mioga:gettext('%s group', $group_ident)"/></label> 
                                <input type="checkbox" value="" name="group-{$group_ident}" id="group-{$group_ident}">
                                    <xsl:if test="count(Application[selected &gt; 0]) = count(Application)">
                                        <xsl:attribute name="checked" value="checked"/>
                                    </xsl:if>
                                </input>
                                <xsl:if test="count(Application) &gt; 0">
                                    <ul style="display: none;">
                                        <xsl:for-each select="Application">
                                            <xsl:sort select="name"/>
                                            <li class="application">
                                                <img src="{$theme_uri}/images/elbow.gif"/>&#160;
                                                <label for="group-{$group_ident}-app-{id}"><xsl:value-of select="name"/></label> 
                                                <input type="checkbox" name="group-{$group_ident}" id="group-{$group_ident}-app-{id}" value="{id}">
                                                    <xsl:if test="selected &gt; 0">
                                                        <xsl:attribute name="checked" value="checked"/>
                                                    </xsl:if>
                                                </input>
                                            </li>
                                        </xsl:for-each>
                                    </ul>
                                </xsl:if>
                            </li>
                        </xsl:for-each>
                    </ul>
                    <div class="files">
                        <h2><xsl:value-of select="mioga:gettext('Subscribe to file update notifications')"/></h2>
                        <ul>
                            <xsl:for-each select="File">
                                <xsl:sort select="group"/>
                                <li class="group">
                                    <input type="checkbox" value="{group}" name="files" id="files-{group}">
                                        <xsl:if test="selected &gt; 0">
                                            <xsl:attribute name="checked" value="checked"/>
                                        </xsl:if>
                                    </input>&#160;
                                    <label for="files-{group}"><xsl:value-of select="mioga:gettext('%s group', group)"/></label> 
                                </li>
                            </xsl:for-each>
                        </ul>
                    </div>
                    <ul class="button_list clear">
                        <li>
                            <input type="submit" class="button" value="{mioga:gettext('Save')}"/>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var image_path = '<xsl:value-of select="$theme_uri"/>/images';
        init_rss();
    </script>
</xsl:template>

</xsl:stylesheet>
