<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<!-- ===============================
     EditContact
     Main contacts edition page
     =============================== -->
<xsl:template match="EditContact">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">applications.html#contact</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle">
      <xsl:with-param name="title">
        <xsl:call-template name="editAction"/>
      </xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="error"/>
    
    <xsl:element name="form">
      <xsl:attribute name="method">POST</xsl:attribute>
      <xsl:attribute name="enctype">multipart/form-data</xsl:attribute>
      <xsl:attribute name="action"><xsl:value-of select="$private_bin_uri" />/Contact/SubmitContact</xsl:attribute>
      
      <xsl:if test="rowid">
        <input>
          <xsl:attribute name="type">hidden</xsl:attribute>
          <xsl:attribute name="name">rowid</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="rowid"/></xsl:attribute>
        </input>
      </xsl:if>

      <xsl:call-template name="choose-group" />

      <br/>
      
      <center>
          <table cellspacing="10">
              <tr valign="top">
                  <td>
                      <xsl:call-template name="edition-zone" >
                          <xsl:with-param name="zone">edit-identity</xsl:with-param>
                          <xsl:with-param name="title">identity</xsl:with-param>
                          <xsl:with-param name="icon">identity.gif</xsl:with-param>
                      </xsl:call-template>
                  </td>
                  
                  <td rowspan="3">
                      <hr class="{$mioga-title-color} {$mioga-title-bg-color}"  style="height: 100%; border: 0; width: 1px; margin-right: 10px; margin-left: 10px;"/>
                  </td>
                  
                  <td>
                      <xsl:call-template name="edition-zone" >
                          <xsl:with-param name="zone">edit-comments</xsl:with-param>
                          <xsl:with-param name="title">comments</xsl:with-param>
                          <xsl:with-param name="icon">comments.gif</xsl:with-param>
                      </xsl:call-template>
                  </td>
              </tr>
              
              <tr valign="top">
                  <td>
                      <xsl:call-template name="edition-zone" >
                          <xsl:with-param name="zone">edit-work-address</xsl:with-param>
                          <xsl:with-param name="title">addr-pro</xsl:with-param>
                          <xsl:with-param name="icon">work.gif</xsl:with-param>
                      </xsl:call-template>
                  </td>
                  
                  <td>
                      <xsl:call-template name="edition-zone" >
                          <xsl:with-param name="zone">edit-telephone</xsl:with-param>
                          <xsl:with-param name="title">phone</xsl:with-param>
                          <xsl:with-param name="icon">telephone.gif</xsl:with-param>
                      </xsl:call-template>
                  </td>
              </tr>
              
              <tr valign="top">
                  <td>
                      <xsl:call-template name="edition-zone" >
                          <xsl:with-param name="zone">edit-home-address</xsl:with-param>
                          <xsl:with-param name="title">addr-pers</xsl:with-param>
                          <xsl:with-param name="icon">16x16/actions/go-home.png</xsl:with-param>
                      </xsl:call-template>
                  </td>
                  <td>
                      <xsl:call-template name="edition-zone">
                          <xsl:with-param name="zone">edit-email</xsl:with-param>
                          <xsl:with-param name="title">mail</xsl:with-param>
                          <xsl:with-param name="icon">email.gif</xsl:with-param>
                      </xsl:call-template>
                  </td>
              </tr>
          </table>
          
          <xsl:call-template name="submit-buttons"/>
      </center>
  </xsl:element>
      

</body>
</xsl:template>



<!-- ===============================
     edition-zone

     Edition zone includes a title 
     and a body
     =============================== -->
<xsl:template name="edition-zone">
    <xsl:param name="title"/>
    <xsl:param name="zone"/>
    
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <th colspan="3" align="left" valign="top" nowrap="1">
        <img src="{$image_uri}/16x16/actions/arrow-right.png" />
        <font>
          <xsl:attribute name="class"><xsl:value-of select="$mioga-title-color" /></xsl:attribute>
          <img src="{$image_uri}/transparent_fill.gif" width="5" height="1" />
            <xsl:call-template name="ceTranslation">
              <xsl:with-param name="type" select="$title"/>
            </xsl:call-template> : 
        </font>
      </th>
    </tr>
    <tr>
      <!--    
      <td align="center" valign="top">
        <img>
          <xsl:attribute name="src"><xsl:value-of select="$image_uri" />/<xsl:value-of select="$icon" /></xsl:attribute>
        </img>
      </td>
      -->
      <td><img src="{$image_uri}/transparent_fill.gif" width="20" height="1" /></td>
      <td align="left">
        <xsl:choose>
          <xsl:when test="$zone='edit-identity'"><xsl:call-template name="edit-identity" /></xsl:when>
          <xsl:when test="$zone='edit-telephone'"><xsl:call-template name="edit-telephone" /></xsl:when>
          <xsl:when test="$zone='edit-email'"><xsl:call-template name="edit-email" /></xsl:when>
          <xsl:when test="$zone='edit-work-address'"><xsl:call-template name="edit-work-address" /></xsl:when>
          <xsl:when test="$zone='edit-home-address'"><xsl:call-template name="edit-home-address" /></xsl:when>
          <xsl:when test="$zone='edit-comments'"><xsl:call-template name="edit-comments" /></xsl:when>
        </xsl:choose>
      </td>
    </tr>
  </table>
</xsl:template>


<!-- ===============================
     zone-field

     It's a table row containing 
     an edition field with  
     a label and a text input.
     =============================== -->
<xsl:template name="edit-zone-field">
    <xsl:param name="field-text"/>
    <xsl:param name="field-name"/>
    <xsl:param name="field-default"/>
    
  <tr>
    <td nowrap="1">                
      <font>
        <xsl:attribute name="class"><xsl:value-of select="$mioga-title-color" /></xsl:attribute>
            <xsl:call-template name="ceTranslation">
              <xsl:with-param name="type" select="$field-text"/>
            </xsl:call-template> :
      </font>
    </td>
    <td>
      <xsl:element name="input">
        <xsl:attribute name="type">text</xsl:attribute>  
        <xsl:attribute name="maxlength">100</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="$field-name" /></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="$field-default" /></xsl:attribute>
      </xsl:element>
    </td>
  </tr>
</xsl:template>


<!-- ===============================
     zone-field-cells

     It's two <td>  containing 
     a label and a text input.
     =============================== -->
<xsl:template name="edit-zone-field-cells">
    <xsl:param name="field-text"/>
    <xsl:param name="field-name"/>
    <xsl:param name="field-default"/>
    
  <td nowrap="1">                
    <font>
      <xsl:attribute name="class"><xsl:value-of select="$mioga-title-color" /></xsl:attribute>
      <xsl:call-template name="ceTranslation">
        <xsl:with-param name="type" select="$field-text"/>
      </xsl:call-template> :
    </font>
  </td>
  <td>
    <xsl:element name="input">
      <xsl:attribute name="type">text</xsl:attribute>  
      <xsl:attribute name="maxlength">100</xsl:attribute>
      <xsl:attribute name="name"><xsl:value-of select="$field-name" /></xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="$field-default" /></xsl:attribute>
    </xsl:element>
  </td>
</xsl:template>



<!-- ===============================
     choose-group
     =============================== -->
<xsl:template name="choose-group">
  <xsl:choose>
      <xsl:when test="count(./AvailaleGroups/group)=1">
          <input>
              <xsl:attribute name="type">hidden</xsl:attribute>
              <xsl:attribute name="name">group</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="AvailaleGroups/group/@id"/></xsl:attribute>
          </input>
    </xsl:when>
    <xsl:otherwise>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <th align="left" valign="top">        
          <xsl:choose>
            <xsl:when test="@group='new'">
              <img src="{$image_uri}/16x16/actions/arrow-right.png" />&#160;
                <font>
                  <xsl:attribute name="class"><xsl:value-of select="$mioga-title-color" /></xsl:attribute>
                  <xsl:call-template name="ceTranslation">
                    <xsl:with-param name="type">add-contact</xsl:with-param>
                  </xsl:call-template>&#160;
                </font>
                <select name="group">
                    <xsl:for-each select="AvailaleGroups/group">
                        <xsl:element name="option">
                            <xsl:attribute name="value"><xsl:value-of select="@id" /></xsl:attribute>
                            <xsl:if test="@selected=1">
                                <xsl:attribute name="selected">1</xsl:attribute>
                            </xsl:if>
                            <xsl:value-of select="."/>
                        </xsl:element>
                    </xsl:for-each>
                </select>
              </xsl:when>
            </xsl:choose>        
          </th>
        </tr>
      </table>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- ===============================
     identity
     =============================== -->
<xsl:template name="edit-identity" >
  <table border="0" cellspacing="0" cellpadding="2">
    <tr>
      <xsl:call-template name="edit-zone-field-cells">
        <xsl:with-param name="field-text">first-name</xsl:with-param>
        <xsl:with-param name="field-name">surname</xsl:with-param>
        <xsl:with-param name="field-default"><xsl:value-of select="surname" /></xsl:with-param>
      </xsl:call-template>
      
      <xsl:call-template name="edit-zone-field-cells">
        <xsl:with-param name="field-text">last-name</xsl:with-param>
        <xsl:with-param name="field-name">name</xsl:with-param>
        <xsl:with-param name="field-default"><xsl:value-of select="name" /></xsl:with-param>
      </xsl:call-template>
          
    </tr>
    <tr>
      <xsl:call-template name="edit-zone-field-cells">
        <xsl:with-param name="field-text">title</xsl:with-param>
        <xsl:with-param name="field-name">title</xsl:with-param>
        <xsl:with-param name="field-default"><xsl:value-of select="title" /></xsl:with-param>
      </xsl:call-template>
      
      <xsl:call-template name="edit-zone-field-cells">
        <xsl:with-param name="field-text">company</xsl:with-param>
        <xsl:with-param name="field-name">orgname</xsl:with-param>
        <xsl:with-param name="field-default"><xsl:value-of select="orgname" /></xsl:with-param>
      </xsl:call-template>
    </tr>

    <xsl:call-template name="edit-zone-field">
      <xsl:with-param name="field-text">website</xsl:with-param>
      <xsl:with-param name="field-name">url</xsl:with-param>
      <xsl:with-param name="field-default"><xsl:value-of select="url" /></xsl:with-param>
    </xsl:call-template>
    
    <tr>
      <td>
        <font>
          <xsl:attribute name="class"><xsl:value-of select="$mioga-title-color" /></xsl:attribute>
            <xsl:call-template name="ceTranslation">
              <xsl:with-param name="type">photo</xsl:with-param>
            </xsl:call-template>&#160;: 
        </font>
      </td>
      <td colspan="3" align="left">
        <xsl:element name="input">
          <xsl:attribute name="type">file</xsl:attribute>
          <xsl:attribute name="name">photo</xsl:attribute>
          <xsl:attribute name="size">20</xsl:attribute>
          <xsl:attribute name="maxlength">255</xsl:attribute>
        </xsl:element>
      </td>
    </tr>          
    <xsl:if test="photo_data!=''" >
    <tr>
       <td>
        <font>
          <xsl:attribute name="class"><xsl:value-of select="$mioga-title-color" /></xsl:attribute>
            <xsl:call-template name="ceTranslation">
              <xsl:with-param name="type">del-photo</xsl:with-param>
            </xsl:call-template>&#160;
        </font>
       </td>
       <td>
          <input type="checkbox" name="delete_photo" value="1" />
       </td>
       <td colspan="2">
            <img height="100">
              <xsl:attribute name="src"><xsl:value-of select="$private_bin_uri" />/Contact/GetPhoto?oid=<xsl:value-of select="photo_data"/></xsl:attribute>
            </img>
        </td>
     </tr>
    </xsl:if>
  </table>
</xsl:template>
    
<!-- ===============================
     telephone
     =============================== -->
<xsl:template name="edit-telephone" >
  <table border="0" cellspacing="0" cellpadding="2">
    <xsl:call-template name="edit-zone-field">
      <xsl:with-param name="field-text">work</xsl:with-param>
      <xsl:with-param name="field-name">tel_work</xsl:with-param>
      <xsl:with-param name="field-default"><xsl:value-of select="tel_work" /></xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="edit-zone-field">
      <xsl:with-param name="field-text">mobile</xsl:with-param>
      <xsl:with-param name="field-name">tel_mobile</xsl:with-param>
      <xsl:with-param name="field-default"><xsl:value-of select="tel_mobile" /></xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="edit-zone-field">
      <xsl:with-param name="field-text">fax</xsl:with-param>
      <xsl:with-param name="field-name">tel_fax</xsl:with-param>
      <xsl:with-param name="field-default"><xsl:value-of select="tel_fax" /></xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="edit-zone-field">
      <xsl:with-param name="field-text">home</xsl:with-param>
      <xsl:with-param name="field-name">tel_home</xsl:with-param>
      <xsl:with-param name="field-default"><xsl:value-of select="tel_home" /></xsl:with-param>
    </xsl:call-template>
  </table>
</xsl:template>
    

<!-- ===============================
     email
     =============================== -->
<xsl:template name="edit-email" >
  <table border="0" cellspacing="0" cellpadding="2">
    <xsl:call-template name="edit-zone-field">
      <xsl:with-param name="field-text">professional</xsl:with-param>
      <xsl:with-param name="field-name">email_work</xsl:with-param>
      <xsl:with-param name="field-default"><xsl:value-of select="email_work" /></xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="edit-zone-field">
      <xsl:with-param name="field-text">personal</xsl:with-param>
      <xsl:with-param name="field-name">email_home</xsl:with-param>
      <xsl:with-param name="field-default"><xsl:value-of select="email_home" /></xsl:with-param>
    </xsl:call-template>
    
  </table>
</xsl:template>



<!-- ===============================
     work adresse
     =============================== -->
<xsl:template name="edit-work-address" >
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <table border="0" cellspacing="0" cellpadding="2">
          <xsl:call-template name="edit-zone-field">
            <xsl:with-param name="field-text">street</xsl:with-param>
            <xsl:with-param name="field-name">addr_work</xsl:with-param>
            <xsl:with-param name="field-default"><xsl:value-of select="addr_work" /></xsl:with-param>
          </xsl:call-template>
          
          <xsl:call-template name="edit-zone-field">
            <xsl:with-param name="field-text">postal-code</xsl:with-param>
            <xsl:with-param name="field-name">postal_code_work</xsl:with-param>
            <xsl:with-param name="field-default"><xsl:value-of select="postal_code_work" /></xsl:with-param>
          </xsl:call-template>
          
          <xsl:call-template name="edit-zone-field">
            <xsl:with-param name="field-text">state</xsl:with-param>
            <xsl:with-param name="field-name">state_work</xsl:with-param>
            <xsl:with-param name="field-default"><xsl:value-of select="state_work" /></xsl:with-param>
          </xsl:call-template>
        </table>
      </td>            
      <td>
        <table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td><img src="{$image_uri}/transparent_fill.gif" width="20" height="20" /></td>
          </tr>
          
          <xsl:call-template name="edit-zone-field">
            <xsl:with-param name="field-text">city</xsl:with-param>
            <xsl:with-param name="field-name">city_work</xsl:with-param>
            <xsl:with-param name="field-default"><xsl:value-of select="city_work" /></xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="edit-zone-field">
            <xsl:with-param name="field-text">country</xsl:with-param>
            <xsl:with-param name="field-name">country_work</xsl:with-param>
            <xsl:with-param name="field-default"><xsl:value-of select="country_work" /></xsl:with-param>
          </xsl:call-template>
        </table>
      </td>
    </tr>
  </table>
</xsl:template>

<!-- ===============================
     home adresse
     =============================== -->
<xsl:template name="edit-home-address" >
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <table border="0" cellspacing="0" cellpadding="2">
          <xsl:call-template name="edit-zone-field">
            <xsl:with-param name="field-text">street</xsl:with-param>
            <xsl:with-param name="field-name">addr_home</xsl:with-param>
            <xsl:with-param name="field-default"><xsl:value-of select="addr_home" /></xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="edit-zone-field">
            <xsl:with-param name="field-text">postal-code</xsl:with-param>
            <xsl:with-param name="field-name">postal_code_home</xsl:with-param>
            <xsl:with-param name="field-default"><xsl:value-of select="postal_code_home" /></xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="edit-zone-field">
            <xsl:with-param name="field-text">state</xsl:with-param>
            <xsl:with-param name="field-name">state_home</xsl:with-param>
            <xsl:with-param name="field-default"><xsl:value-of select="state_home" /></xsl:with-param>
          </xsl:call-template>

        </table>
      </td>
      <td>
        <table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td><img src="{$image_uri}/transparent_fill.gif" width="20" height="20" /></td>
          </tr>
          <xsl:call-template name="edit-zone-field">
            <xsl:with-param name="field-text">city</xsl:with-param>
            <xsl:with-param name="field-name">city_home</xsl:with-param>
            <xsl:with-param name="field-default"><xsl:value-of select="city_home" /></xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="edit-zone-field">
            <xsl:with-param name="field-text">country</xsl:with-param>
            <xsl:with-param name="field-name">country_home</xsl:with-param>
            <xsl:with-param name="field-default"><xsl:value-of select="country_home" /></xsl:with-param>
          </xsl:call-template>
        </table>
      </td>
    </tr>
  </table>
</xsl:template>

<!-- ===============================
     comments
     =============================== -->
<xsl:template name="edit-comments">
  <xsl:element name="textarea">
    <xsl:attribute name="name">comment</xsl:attribute>
    <xsl:attribute name="cols">40</xsl:attribute>
    <xsl:attribute name="rows">3</xsl:attribute>
    <xsl:value-of select="comment" />
  </xsl:element>
</xsl:template>

<!-- ===============================
     submit buttons
     =============================== -->
<xsl:template name="submit-buttons">
    <br/>

    <center>
        <xsl:call-template name="ok-cancel-form-button">
            <xsl:with-param name="name">ActionEditOK</xsl:with-param>
            <xsl:with-param name="referer" select="referer"/>
        </xsl:call-template>
    </center>

</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<xsl:template name="editAction">
  <xsl:choose>
    <xsl:when test="rowid"><xsl:value-of select="mioga:gettext('Modify contact')"/></xsl:when>
    <xsl:otherwise><xsl:value-of select="mioga:gettext('Create contact')"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="ceTranslation">
  <xsl:param name="type"/>

  <xsl:choose>
    <xsl:when test="$type='identity'"><xsl:value-of select="mioga:gettext('Identity')"/></xsl:when>
    <xsl:when test="$type='comments'"><xsl:value-of select="mioga:gettext('Comments')"/></xsl:when>
    <xsl:when test="$type='phone'"><xsl:value-of select="mioga:gettext('Telephone')"/></xsl:when>
    <xsl:when test="$type='mail'"><xsl:value-of select="mioga:gettext('Email')"/></xsl:when>
    <xsl:when test="$type='addr-pro'"><xsl:value-of select="mioga:gettext('Professional address')"/></xsl:when>
    <xsl:when test="$type='addr-pers'"><xsl:value-of select="mioga:gettext('Personal address')"/></xsl:when>
    <xsl:when test="$type='add-contact'"><xsl:value-of select="mioga:gettext('Add a new contact in group')"/></xsl:when>
    <xsl:when test="$type='first-name'"><xsl:value-of select="mioga:gettext('Firstname')"/></xsl:when>
    <xsl:when test="$type='last-name'"><xsl:value-of select="mioga:gettext('Lastname')"/></xsl:when>
    <xsl:when test="$type='title'"><xsl:value-of select="mioga:gettext('Title')"/></xsl:when>
    <xsl:when test="$type='company'"><xsl:value-of select="mioga:gettext('Company')"/></xsl:when>
    <xsl:when test="$type='website'"><xsl:value-of select="mioga:gettext('Web site')"/></xsl:when>
    <xsl:when test="$type='photo'"><xsl:value-of select="mioga:gettext('Photo')"/></xsl:when>
    <xsl:when test="$type='del-photo'"><xsl:value-of select="mioga:gettext('Delete photo')"/></xsl:when>
    <xsl:when test="$type='work'"><xsl:value-of select="mioga:gettext('Work')"/></xsl:when>
    <xsl:when test="$type='mobile'"><xsl:value-of select="mioga:gettext('Mobile')"/></xsl:when>
    <xsl:when test="$type='fax'"><xsl:value-of select="mioga:gettext('Fax')"/></xsl:when>
    <xsl:when test="$type='home'"><xsl:value-of select="mioga:gettext('Domicile')"/></xsl:when>
    <xsl:when test="$type='personal'"><xsl:value-of select="mioga:gettext('Personal')"/></xsl:when>
    <xsl:when test="$type='professional'"><xsl:value-of select="mioga:gettext('Professional')"/></xsl:when>
    <xsl:when test="$type='street'"><xsl:value-of select="mioga:gettext('Street')"/></xsl:when>
    <xsl:when test="$type='postal-code'"><xsl:value-of select="mioga:gettext('Zip code')"/></xsl:when>
    <xsl:when test="$type='city'"><xsl:value-of select="mioga:gettext('City')"/></xsl:when>
    <xsl:when test="$type='state'"><xsl:value-of select="mioga:gettext('State')"/></xsl:when>
    <xsl:when test="$type='country'"><xsl:value-of select="mioga:gettext('Country')"/></xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
