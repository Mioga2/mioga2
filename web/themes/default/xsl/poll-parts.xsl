<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" encoding="UTF-8" media-type="text/html"/>

<xsl:include href="scriptaculous.xsl"/>

<!--
=====================
 Utilities templates
=====================
-->

<!-- escape asostrophes very useful for javascript -->
<xsl:template name="escape-apos">
   <xsl:param name="string" />
   <!-- create an $apos variable to make it easier to refer to -->
   <xsl:variable name="apos" select='"&apos;"' />
   <xsl:choose>
      <!-- if the string contains an apostrophe... -->
      <xsl:when test='contains($string, $apos)'>
         <!-- ... give the value before the apostrophe... -->
         <xsl:value-of select="substring-before($string, $apos)" />
         <!-- ... the escaped apostrophe ... -->
         <xsl:text>\'</xsl:text>
         <!-- ... and the result of applying the template to the
                  string after the apostrophe -->
         <xsl:call-template name="escape-apos">
            <xsl:with-param name="string"
                            select="substring-after($string, $apos)" />
         </xsl:call-template>
      </xsl:when>
      <!-- otherwise... -->
      <xsl:otherwise>
         <!-- ... just give the value of the string -->
         <xsl:value-of select="$string" />
      </xsl:otherwise>
   </xsl:choose>
</xsl:template>

<!--
===============
 Templates
===============
-->

<xsl:template match="/">
    <xsl:apply-templates/>
</xsl:template>

<!--
=======================================
 Polls
=======================================
-->
<xsl:template match="Polls">
    <xsl:param name="page"/>
    
    <xsl:for-each select="./Poll">
    	<div id="poll-{@rowid}" class="poll sbox border_color">
            <h2 class="title_bg_color">
                <xsl:choose>
                	<xsl:when test="$page = 'DisplayMain'">
                        <xsl:choose>
                        	<xsl:when test="@validated or @closed = 1">
                                <xsl:choose>
                                    <xsl:when test="@visible = 1 or (@visible = 0 and //CanAnim)"><a href="ViewResults?rowid={@rowid}"><xsl:value-of select="Title"/></a></xsl:when>
                                    <xsl:otherwise><xsl:value-of select="Title"/></xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                        	<xsl:otherwise><a href="AnswerPoll?rowid={@rowid}"><xsl:value-of select="Title"/></a></xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                	<xsl:otherwise><a href="EditPoll?rowid={@rowid}"><xsl:value-of select="Title"/></a></xsl:otherwise>
                </xsl:choose>
                <xsl:if test="//CanAnim">
                  <a class="template" href="CreatePoll?rowid={@rowid}" title="{mioga:gettext('Create a new poll from this one as a template')}"><img src="{$image_uri}/16x16/actions/document-new.png"/></a>
                	<a href="#" class="delete"><xsl:attribute name="onclick">if (confirm("<xsl:value-of select="mioga:gettext('Are you sure you wish to delete this poll and all its data?')"/>")) { new Ajax.Request("DeletePoll", { evalScripts: true, asynchronous: true, parameters: "rowid=<xsl:value-of select="@rowid"/>" } ) }; return false;</xsl:attribute><img src="{$image_uri}/16x16/actions/trash-empty.png"/></a>
                </xsl:if>
            </h2>
            <h3>
                <xsl:value-of select="@questions"/>&#160;<xsl:value-of select="mioga:gettext('questions')"/>
            </h3>
            <p><xsl:value-of select="Description"/></p>
        </div>
    </xsl:for-each>
</xsl:template>

<!--
===========================
 Displays a question
===========================
-->
<xsl:template match="Question">
    <xsl:param name="js_mode"/>
    <xsl:param name="page"/>
    
    <!-- js variables -->
    <xsl:variable name="question_title">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="Label"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="Label"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="free_answer">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Other (please provide an answer):')"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="mioga:gettext('Other (please provide an answer):')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="confirm_delete">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Are you sure you want to delete this question?')"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="mioga:gettext('Are you sure you want to delete this question?')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="edit">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Edit')"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="mioga:gettext('Edit')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="delete">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Delete')"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="mioga:gettext('Delete')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="question_header">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Question')"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="mioga:gettext('Question')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="fa_text">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="//Answer/Text"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="//Answer/Text"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!-- end of js variables -->
    
    <div class="question sbox border_color" id="q-{@rowid}">
        <h2 class="title_bg_color"><xsl:value-of select="$question_header"/>&#160;<xsl:value-of select="@rowid + 1"/>
            <xsl:if test="$page != 'AnswerPoll'">
            	<a class="edit" href="#" title="{$edit}"><xsl:attribute name="onclick">location.href="EditQuestion?idx=<xsl:value-of select="@rowid"/>&amp;"+Form.serialize("poll-form");</xsl:attribute>
                <img src="{$image_uri}/16x16/actions/document-edit.png" alt="{$edit}"/></a>
                <a class="delete" href="#" title="{$delete}">
                    <xsl:attribute name="onclick">if (confirm("<xsl:value-of select="$confirm_delete"/>")) { new Ajax.Request("DeleteQuestion", { evalScripts: true, asynchronous: true, parameters: "idx=<xsl:value-of select="@rowid"/>" } ) }; return false;</xsl:attribute>
                    <img src="{$image_uri}/16x16/actions/trash-empty.png" alt="{$delete}"/>
                </a>
            </xsl:if>
        </h2>
        <p><xsl:value-of select="$question_title"/></p>
        <xsl:if test="contains(@type, 'FREE')">
            <input type="hidden" name="answer" value="text-answer"/>
        	<xsl:choose>
        		<xsl:when test="contains(@type, 'LONGTEXT')"><textarea name="text-answer"><xsl:value-of select="$fa_text"/></textarea></xsl:when>
        		<xsl:otherwise><input type="text" name="text-answer" value="{$fa_text}"/></xsl:otherwise>
        	</xsl:choose>
        </xsl:if>
        <xsl:if test='contains(@type, "MULTI") or contains(@type, "SINGLE")'>
            <ul>
        	<xsl:for-each select="Choices/Choice">
                <!-- js variables -->
                <xsl:variable name="label">
                  <xsl:choose>
                    <xsl:when test="$js_mode = 1">
                      <xsl:call-template name="escape-apos">
                        <xsl:with-param name="string" select="Label"/>
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="Label"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:variable>
                <!-- end of js variables -->
                
        		<li><label for="{@type}-{@rowid}"><input type="{@type}" name="answer" value="{@rowid}" id="{@type}-{@rowid}">
                    <xsl:if test="//Answer/Choices/Choice/Label = Label">
                    	<xsl:attribute name="checked">checked</xsl:attribute>
                    </xsl:if>
                </input><xsl:value-of select="$label"/></label></li>
        	</xsl:for-each>
            <xsl:if test="contains(@type, '_F')">
                <li><label for="text-answer"><input type="{Choices/Choice[1]/@type}" name="answer" value="text-answer" id="text-answer">
                    <xsl:if test="//Answer/Text != ''">
                    	<xsl:attribute name="checked">checked</xsl:attribute>
                    </xsl:if>
                </input><xsl:value-of select="$free_answer"/></label><xsl:choose>
                    <xsl:when test="contains(@type, 'LONGTEXT')"><textarea name="text-answer"><xsl:value-of select="$fa_text"/></textarea></xsl:when>
                    <xsl:otherwise><input type="text" name="text-answer" value="{$fa_text}"/></xsl:otherwise>
                </xsl:choose></li>
            </xsl:if>
            </ul>
        </xsl:if>
        <xsl:if test="//errors/err[@arg = 'answer']">
            <xsl:call-template name="error-message">
                <xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'answer']/type" /></xsl:with-param>
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="//errors/err[@arg = 'text-answer']">
            <xsl:call-template name="error-message">
                <xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'text-answer']/type" /></xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </div>
</xsl:template>

<!--
=======================================
 AddOrEditQuestion common template
=======================================
-->
<xsl:template name="AddOrEditQuestion">
    <xsl:param name="page"/>
    
    <xsl:apply-templates select="Question" mode="edition">
        <xsl:with-param name="page" select="$page"/>
    </xsl:apply-templates>
    
    <script type="text/javascript">
        answer_count = <xsl:value-of select="count(Question/Choices/Choice)"/>;
    </script>
</xsl:template>

<!--
=======================================
 Question in edition mode
=======================================
-->
<xsl:template match="Question" mode="edition">
    <xsl:param name="page"/>
    
	<!-- js variables -->
    <xsl:variable name="title">
        <xsl:if test="$page = 'EditQuestion'">
            <xsl:value-of select="mioga:gettext('Editing question')"/>
        </xsl:if>
        <xsl:if test="$page = 'AddQuestion'">
            <xsl:value-of select="mioga:gettext('Adding new question')"/>
        </xsl:if>
    </xsl:variable>
    
    <xsl:variable name="q_label" select="Label"/>
    <xsl:variable name="label" select="mioga:gettext('Question text')"/>
    <xsl:variable name="q_type" select="mioga:gettext('Type of question')"/>
    <xsl:variable name="simple_type" select="mioga:gettext('Single choice answers')"/>
    <xsl:variable name="simple_f_type" select="mioga:gettext('Single choice answers with free answer')"/>
    <xsl:variable name="multi_type" select="mioga:gettext('Multiple choice answers')"/>
    <xsl:variable name="multi_f_type" select="mioga:gettext('Multiple choice answers with free answer')"/>
    <xsl:variable name="free_type" select="mioga:gettext('Free answer')"/>
    <xsl:variable name="fa_type" select="mioga:gettext('Type of free answer')"/>
    <xsl:variable name="fa_st" select="mioga:gettext('Short text')"/>
    <xsl:variable name="fa_lt" select="mioga:gettext('Long text')"/>
    <xsl:variable name="fa_int" select="mioga:gettext('Numerical integer')"/>
    <xsl:variable name="fa_float" select="mioga:gettext('Numerical float')"/>
    <xsl:variable name="delete" select="mioga:gettext('Delete this answer')"/>
    <xsl:variable name="delete_confirm" select="mioga:gettext('Are you sure you want to delete this answer?')"/>
    <xsl:variable name="ok" select="mioga:gettext('Ok')"/>
    <xsl:variable name="cancel" select="mioga:gettext('Cancel')"/>
    <xsl:variable name="answers" select="mioga:gettext('Answers')"/>
    <xsl:variable name="add_answer" select="mioga:gettext('Add a new answer')"/>
    <!-- end of js variables -->
    
    <div class="question sbox border_color">
        <h2 class="title_bg_color"><xsl:value-of select="$title"/></h2>
        
        <div class="content">
        <form action="UpdateQuestion" method="post">
            <input type="hidden" name="rowid" value="{@rowid}"/>
            <input type="hidden" name="page" value="{$page}"/>
			<xsl:call-template name="CSRF-input"/>
            
            <table class="question" border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="75%">
                        <fieldset>
                        <legend><xsl:value-of select="$label"/></legend>
                            <textarea cols="20" rows="15" name="q_label" id="q_title"><xsl:value-of select="$q_label"/></textarea>
                            <xsl:if test="//errors/err[@arg = 'q_label']">
                            	<xsl:call-template name="error-message">
                                    	<xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'q_label']/type" /></xsl:with-param>
                            	</xsl:call-template>
                            </xsl:if>
                        </fieldset>
                    </td>
                    <td width="25%">
                        <fieldset>
                        <legend><xsl:value-of select="$q_type"/></legend>
                        <ul>
                            <li><label for="st"><input id="st" type="radio" name="q_type" value="SINGLE">
                                <xsl:attribute name="onclick">$("fa_type").hide(); $("answers-box").show();</xsl:attribute>
                                <xsl:if test="contains(@type, 'SINGLE') and contains(@type, '_F') = false()">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input><xsl:value-of select="$simple_type"/></label></li>
                            <li><label for="sft"><input id="sft" type="radio" name="q_type" value="SINGLE_F">
                                <xsl:attribute name="onclick">$("fa_type").show(); $("answers-box").show();</xsl:attribute>
                                <xsl:if test="contains(@type, 'SINGLE_F')">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input><xsl:value-of select="$simple_f_type"/></label></li>
                            <li><label for="mt"><input id="mt" type="radio" name="q_type" value="MULTI">
                                <xsl:attribute name="onclick">$("fa_type").hide(); $("answers-box").show();</xsl:attribute>
                                <xsl:if test="contains(@type, 'MULTI') and contains(@type, '_F') = false()">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input><xsl:value-of select="$multi_type"/></label></li>
                            <li><label for="mft"><input id="mft" type="radio" name="q_type" value="MULTI_F">
                                <xsl:attribute name="onclick">$("fa_type").show(); $("answers-box").show();</xsl:attribute>
                                <xsl:if test="contains(@type, 'MULTI_F')">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input><xsl:value-of select="$multi_f_type"/></label></li>
                            <li><label for="ft"><input id="ft" type="radio" name="q_type" value="FREE">
                                <xsl:attribute name="onclick">$("fa_type").show(); $("answers-box").hide();</xsl:attribute>
                                <xsl:if test="contains(@type, 'FREE')">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input><xsl:value-of select="$free_type"/></label></li>
                        </ul>
                        <xsl:if test="//errors/err[@arg = 'q_type']">
                        	<xsl:call-template name="error-message">
                                	<xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'q_type']/type" /></xsl:with-param>
                        	</xsl:call-template>
                        </xsl:if>
                    </fieldset>
                    <fieldset id="fa_type">
                        <xsl:if test="contains(@type, 'FREE') = false() and contains(@type, '_F') = false()">
                            <xsl:attribute name="style">display: none</xsl:attribute>
                        </xsl:if>
                        <legend><xsl:value-of select="$fa_type"/></legend>
                        <ul>
                            <li><label for="fa_st"><input id="fa_st" type="radio" name="fa_type" value="SHORTTEXT">
                                <xsl:if test="contains(@type, '_SHORTTEXT') or (contains(@type, '_LONGTEXT') = false() and contains(@type, '_SHORTTEXT') = false() and contains(@type, '_INTEGER') = false() and contains(@type, '_FLOAT') = false())">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input><xsl:value-of select="$fa_st"/></label></li>
                            <li><label for="fa_lt"><input id="fa_lt" type="radio" name="fa_type" value="LONGTEXT">
                                <xsl:if test="contains(@type, '_LONGTEXT')">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input><xsl:value-of select="$fa_lt"/></label></li>
                            <li><label for="fa_int"><input id="fa_int" type="radio" name="fa_type" value="INTEGER">
                                <xsl:if test="contains(@type, '_INTEGER')">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input><xsl:value-of select="$fa_int"/></label></li>
                            <li><label for="fa_float"><input id="fa_float" type="radio" name="fa_type" value="FLOAT">
                                <xsl:if test="contains(@type, '_FLOAT')">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input><xsl:value-of select="$fa_float"/></label></li>
                        </ul>
                    </fieldset>
                    </td>
                </tr>
            </table>
            
            <fieldset id="answers-box">
                <xsl:if test="contains(@type, 'FREE')">
                	<xsl:attribute name="style">display: none;</xsl:attribute>
                </xsl:if>
                <legend><xsl:value-of select="$answers"/></legend>
                <xsl:if test="//errors/err[@arg = 'answers']">
                    <xsl:call-template name="error-message">
                            <xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'answers']/type" /></xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
                <ul class="hmenu">
                    <li><a href="#" class="add_answer" alt="{$add_answer}" title="{$add_answer}"><xsl:attribute name="onclick">return addAnswer("<xsl:value-of select="$delete_confirm"/>", "<xsl:value-of select="$image_uri"/>", "<xsl:value-of select="$delete"/>");</xsl:attribute><xsl:value-of select="$add_answer"/></a></li>
                </ul>
                
                <ol id="answers-list">
                <xsl:for-each select="Choices/Choice">
                    <!-- js var -->
                    <xsl:variable name="choice" select="Label"/>
                    <!-- end of js var -->
                    
                    <li id="a-{@rowid}">
                        <input type="text" class="text" name="answers" value="{$choice}"/>
                        <a href="#"><xsl:attribute name="onclick">if (confirm("<xsl:value-of select="$delete_confirm"/>")) { new Effect.Fade(this.parentNode, { duration: 0.5}); Element.remove(this.parentNode.parentNode); }; return false;</xsl:attribute><img src="{$image_uri}/16x16/actions/trash-empty.png" alt="{$delete}" title="{$delete}"/></a>
                    </li>
                </xsl:for-each>
                </ol>
            </fieldset>
            
            <ul class="button_list">
                <li>
                    <input class="button" type="submit" value="{$ok}"/>
                </li>
                <li>
                    <a class="button" href="{//Referer}"><xsl:value-of select="$cancel"/></a>
                </li>
            </ul>
        </form>
        </div>
    </div>
</xsl:template>

<!--
=================
 Display options
=================
-->
<xsl:template match="Options">   
    <!-- using variable to handle javascript mode -->
        <xsl:variable name="title">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Modifying options')"/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="ok">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Ok')"/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="cancel">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Cancel')"/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="visible_label">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Allow results to be visible by anyone')"/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="anon_label">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Poll is anonymous')"/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="start_date_legend">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Publication date')"/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="wait">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Wait')"/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="now">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Immediately')"/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="fixed_date">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Fixed date:')"/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="end_date_legend">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Closing condition')"/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="percent">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Answers percent')"/>
          </xsl:call-template>
        </xsl:variable>
    <!-- end of variables definition -->
    
    <div class="sbox border_color">
        <h2 class="title_bg_color"><xsl:value-of select="$title"/></h2>
        <div class="content">
            <form action="UpdateOptions" method="post" id="options-form">
            <xsl:attribute name="onsubmit">new Ajax.Request("UpdateOptions", {evalScripts:true, asynchronous:true, parameters:encodeURI(Form.serialize(this)) }); return false;</xsl:attribute>
                
                <p><label for="visible">
                    <input type="checkbox" value="1" id="visible" name="visible">
                        <xsl:if test="Visible = 1">
                        	<xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                    </input><xsl:value-of select="$visible_label"/>
                </label></p>
                <p><label for="anon">
                    <input type="checkbox" value="1" id="anon" name="anonymous">
                        <xsl:if test="Anonymous = 1">
                        	<xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                    </input><xsl:value-of select="$anon_label"/>
                </label></p>
                
                <fieldset>
                    <legend><xsl:value-of select="$start_date_legend"/></legend>
                    <xsl:if test="//errors/err[@arg = 'start_date']">
                        <xsl:call-template name="error-message">
                                <xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'start_date']/type" /></xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
                    <p><label for="wait">
                        <input id="wait" type="radio" name="start_date" value="wait">
                            <xsl:if test="StartDate = 'wait'">
                            	<xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                        </input><xsl:value-of select="$wait"/>
                    </label></p>
                    <p><label for="now">
                        <input id="now" type="radio" name="start_date" value="now">
                            <xsl:if test="StartDate = 'now'">
                            	<xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                        </input><xsl:value-of select="$now"/>
                    </label></p>
                    <p><label for="s_fixed">
                        <input id="s_fixed" type="radio" name="start_date" value="fixed">
                            <xsl:if test="StartDate = 'fixed'">
                            	<xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                        </input><xsl:value-of select="$fixed_date"/></label>&#160;<input type="hidden" id="s_fixed_date" name="s_fixed_date" value="{FixedStartDate}"/><span id="s_date_formatted"><xsl:value-of select="FixedStartDateFormatted"/>&#160;</span><a href="#"  name="calendar_s_link" id="calendar_s_link"><xsl:attribute name="onclick">calsd.showCalendar("calendar_s_link"); return false;</xsl:attribute><img src="{$image_uri}/calendar.gif" border="0" width="16" height="16"/></a><div id="div_calsd" class="calendar_popup" style="position:absolute;z-index:20;visibility:hidden;"></div></p>
                </fieldset>
                
                <fieldset>
                    <legend><xsl:value-of select="$end_date_legend"/></legend>
                    <div id="div_caled" class="calendar_popup" style="position: absolute; z-index: 20; visibility: hidden;">&#160;</div>
                    <xsl:if test="//errors/err[@arg = 'end_date']">
                        <xsl:call-template name="error-message">
                                <xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'end_date']/type" /></xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
                    <p><label for="percent">
                        <input id="percent" type="radio" name="end_date" value="percent">
                            <xsl:if test="EndDate = 'percent'">
                            	<xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                            <xsl:attribute name="onclick">$("e_percent").removeAttribute("disabled");</xsl:attribute>
                        </input><xsl:value-of select="$percent"/></label>&#160;<input type="text" id="e_percent" name="percent" disabled="disabled" size="3" value="{PercentEndDate}"/></p>
                    <p><label for="e_fixed">
                        <input id="e_fixed" type="radio" name="end_date" value="fixed">
                            <xsl:if test="EndDate = 'fixed'">
                            	<xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                            <xsl:attribute name="onclick">$("e_percent").setAttribute("disabled", "disabled");</xsl:attribute>
                        </input><xsl:value-of select="$fixed_date"/></label>&#160;<input type="hidden" id="e_fixed_date" name="e_fixed_date" value="{FixedEndDate}"/><span id="e_date_formatted"><xsl:value-of select="FixedEndDateFormatted"/>&#160;</span><a href="#"  name="calendar_e_link" id="calendar_e_link"><xsl:attribute name="onclick">caled.showCalendar("calendar_e_link"); return false;</xsl:attribute><img src="{$image_uri}/calendar.gif" border="0" width="16" height="16"/></a>
                    </p>
                </fieldset>
                
                <ul class="button_list">
                    <li>
                        <input class="button" type="submit" value="{$ok}"/>
                    </li>
                    <li>
                        <a class="button" href="#"><xsl:attribute name="onclick">$("disable-window").hide(); $("options-box").hide(); return false;</xsl:attribute><xsl:value-of select="$cancel"/></a>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</xsl:template>

</xsl:stylesheet>
