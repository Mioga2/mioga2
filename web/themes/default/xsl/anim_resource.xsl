<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="anim_user.xsl"/>

<xsl:output method="html" indent="yes"/>

<!-- =================================
     Tabs Menu description
     ================================= -->

<xsl:template match="*" mode="MiogaMenuBody">
  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayUsers</xsl:with-param>
    <xsl:with-param name="label">DisplayUsers</xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayTeams</xsl:with-param>
    <xsl:with-param name="label">DisplayTeams</xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayThemes</xsl:with-param> 
    <xsl:with-param name="label">DisplayThemes</xsl:with-param> 
  </xsl:call-template>

  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayProfiles</xsl:with-param>
    <xsl:with-param name="label">DisplayProfiles</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<xsl:template name="MiogaMenuTranslation">
  <xsl:param name="label"/>
  <xsl:choose>
    <xsl:when test="$label='DisplayUsers'"><xsl:value-of select="mioga:gettext('Users')"/></xsl:when>
    <xsl:when test="$label='DisplayTeams'"><xsl:value-of select="mioga:gettext('Teams')"/></xsl:when>
    <xsl:when test="$label='DisplayThemes'"><xsl:value-of select="mioga:gettext('Themes and languages')"/></xsl:when>
    <xsl:when test="$label='DisplayProfiles'"><xsl:value-of select="mioga:gettext('Profiles')"/></xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
