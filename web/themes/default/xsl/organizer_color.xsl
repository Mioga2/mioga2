<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html"/>

<xsl:variable name="conflict_bgcolor">#ff7070</xsl:variable>
<xsl:variable name="noconflict_bgcolor">#bbffbb</xsl:variable>
<xsl:variable name="nonworking_bgcolor">#f95c5c</xsl:variable>
<xsl:variable name="selected_orange_color">#fa7d2e</xsl:variable>
<xsl:variable name="separator_color">#888888</xsl:variable>

<xsl:variable name="org_priority1_color">#ffa8a8</xsl:variable>
<xsl:variable name="org_priority2_color">#ffcaa8</xsl:variable>
<xsl:variable name="org_priority3_color">#d3d7ff</xsl:variable>
<xsl:variable name="org_priority4_color">#e8ffd3</xsl:variable>

<xsl:variable name="month_left_color">#dbdbed</xsl:variable>

</xsl:stylesheet>
