<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:include href="scriptaculous.xsl"/>
<!--
Headers
-->
<xsl:template name="mioga-css">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/mioga.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{$theme_uri}/css/booking.css" media="screen" />
</xsl:template>


<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
    <xsl:call-template name="mioga-js"/>
    <xsl:call-template name="calendar-js"/>
    <xsl:call-template name="favicon"/>
    <xsl:call-template name="title"/>
    <xsl:call-template name="scriptaculous-js"/>
	<xsl:call-template name="theme-css"/>
</head>
<body class="booking">
    <xsl:call-template name="DisplayAppTitle">
    <xsl:with-param name="help">booking.html</xsl:with-param>
    </xsl:call-template>
    <xsl:apply-templates />
</body>
</html>
</xsl:template>


<!--
 DisplayMain
-->
<xsl:template match="DisplayMain">
    <div class="container">
        <xsl:apply-templates select="BookingsToValidate"/>
        <xsl:apply-templates select="UserBookings" />
        <xsl:apply-templates select="Resources" />
    </div>
</xsl:template>

<!--
=======================================
 BookingsToValidate
=======================================
-->
<xsl:template match="BookingsToValidate">
    <table class="sbox border_color" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr class="title_bg_color">
            <th colspan="3"><xsl:value-of select="mioga:gettext('Bookings to validate')"/></th>
        </tr>
        <tr class="subtitle">
            <th width="50%"><xsl:value-of select="mioga:gettext('Resource')"/></th>
            <th width="25%"><xsl:value-of select="mioga:gettext('Beginning on')"/></th>
            <th width="25%"><xsl:value-of select="mioga:gettext('Ending on')"/></th>
        </tr>
        <xsl:for-each select="Booking">
		<tr>
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="position() mod 2 = 0">even</xsl:when>
                    <xsl:otherwise>odd</xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            
            <td class="resource"><a class="view" href="ViewBooking?rowid={@id}"><xsl:value-of select="Resource"/></a></td>
            <td class="centered"><xsl:value-of select="Start"/></td>
            <td class="centered"><xsl:value-of select="Stop"/></td>
        </tr>
        </xsl:for-each>
        <xsl:if test="count(Booking) = 0">
        <tr>
            <td  class="resource" colspan="3"><xsl:value-of select="mioga:gettext('There is no booking needing your attention at this time.')"/></td>
        </tr>
        </xsl:if>
    </table>
</xsl:template>

<!--
=======================================
 ViewBooking
=======================================
-->
<xsl:template match="ViewBooking">
    
    <div style="text-align: center;">
        <form method="post" action="UpdateBooking">
            <input type="hidden" name="rowid" value="{Task/@id}"/>
            <input id="action" type="hidden" name="action" value=""/>
        <table class="sbox border_color" style="margin-left: auto; margin-right: auto;" cellpadding="0" border="0" cellspacing="0" width="50%">
            <tr class="title_bg_color">
                <th colspan="2"><xsl:value-of select="mioga:gettext('Details of booking')"/></th>
            </tr>
            <tr>
                <td width="40%"><xsl:value-of select="mioga:gettext('Resource:')"/></td>
                <td width="60%"><xsl:value-of select="Task/Resource"/></td>
            </tr>
            <tr>
                <td><xsl:value-of select="mioga:gettext('User:')"/></td>
                <td><xsl:value-of select="Task/Name"/></td>
            </tr>
            <tr>
            	<td><xsl:value-of select="mioga:gettext('Category:')"/></td>
                <td><xsl:value-of select="Task/Category"/></td>
            </tr>
            <tr>
            	<td><xsl:value-of select="mioga:gettext('Start date:')"/></td>
                <td><xsl:value-of select="Task/Start"/></td>
            </tr>
            <tr>
            	<td><xsl:value-of select="mioga:gettext('End date:')"/></td>
                <td><xsl:value-of select="Task/Stop"/></td>
            </tr>
            <tr>
            	<td><xsl:value-of select="mioga:gettext('Description:')"/></td>
                <td><xsl:value-of select="Task/Description"/></td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <ul class="button_list">
                        <li>
                            <input class="button" type="submit" value="{mioga:gettext('Validate')}">
                                <xsl:attribute name="onclick">$("action").value = "validate"; return confirm("<xsl:value-of select="mioga:gettext('Are you sure you wish to validate this booking?')"/>");</xsl:attribute>
                            </input>
                        </li>
                        <li>
                            <input id="decline" class="button" type="submit" value="{mioga:gettext('Decline')}">
                                <xsl:attribute name="onclick">$("action").value = "decline"; return confirm("<xsl:value-of select="mioga:gettext('Are you sure you wish to decline this booking?')"/>");</xsl:attribute>
                            </input>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
        </form>
    </div>
</xsl:template>

<!--
=======================================
 NoSuchBooking
=======================================
-->
<xsl:template match="NoSuchBooking">
    
    <div class="no-such-booking">
		<div class="sbox border_color">
			<h2 class="title_bg_color"><xsl:value-of select="mioga:gettext ('Details of booking')"/></h2>
			<div class="content">
				<p>
					<xsl:value-of select="mioga:gettext ('This booking does not exist.')"/>
				</p>
			</div>
		</div>
    </div>
</xsl:template>

<!--
=======================================
 UserBookings
=======================================
-->
<xsl:template match="UserBookings">
    <table class="sbox border_color" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr class="title_bg_color">
            <th colspan="3"><xsl:value-of select="mioga:gettext('Your bookings waiting approval')"/></th>
        </tr>
        <tr class="subtitle">
            <th width="50%"><xsl:value-of select="mioga:gettext('Resource')"/></th>
            <th width="25%"><xsl:value-of select="mioga:gettext('Beginning on')"/></th>
            <th wdith="25%"><xsl:value-of select="mioga:gettext('Ending on')"/></th>
        </tr>
        <xsl:for-each select="Booking">
		<tr>
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="position() mod 2 = 0">even</xsl:when>
                    <xsl:otherwise>odd</xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            
            <xsl:variable name="edit_url">
                <xsl:choose>
                	<xsl:when test="@type = 'periodic'">EditSimplePeriodicTask</xsl:when>
                	<xsl:otherwise>EditStrictTask</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            
            <td class="resource"><div><a class="edit" href="{$edit_url}?taskrowid={@id}&amp;resource_id={Resource/@id}"><xsl:value-of select="Resource"/></a> <a class="delete" href="UpdateBooking?action=delete&amp;rowid={@id}">
            <xsl:attribute name="onclick">return confirm("<xsl:value-of select="mioga:gettext('Are you sure you wish to delete this booking?')"/>");</xsl:attribute>
            <img src="{$image_uri}/16x16/actions/trash-empty.png"/></a></div></td>
            <td class="centered"><xsl:value-of select="Start"/></td>
            <td class="centered"><xsl:value-of select="Stop"/></td>
        </tr>
        </xsl:for-each>
        <xsl:if test="count(Booking) = 0">
        <tr>
            <td class="resource" colspan="3"><xsl:value-of select='mioga:gettext("You currently don&apos;t have any booking waiting approval.")'/></td>
        </tr>
        </xsl:if>
    </table>
</xsl:template>

<!--
=======================================
 Resources
=======================================
-->
<xsl:template match="Resources">
	<table class="sbox border_color" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr class="title_bg_color">
            <th colspan="2"><xsl:value-of select="mioga:gettext('Resources available for this group')"/></th>
        </tr>
        <tr class="subtitle">
            <th width="50%"><xsl:value-of select="mioga:gettext('Resource')"/></th>
            <th width="50%"><xsl:value-of select="mioga:gettext('Animator')"/></th>
        </tr>
        <xsl:for-each select="Resource">
		<tr>
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="position() mod 2 = 0">even</xsl:when>
                    <xsl:otherwise>odd</xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            
            <td class="resource"><a href="CreateSimplePeriodicTask?resource_id={@id}" class="new-document"><xsl:value-of select="."/></a></td>
            <td class="centered"><xsl:value-of select="@owner"/></td>
        </tr>
        </xsl:for-each>
        <xsl:if test="count(Resource) = 0">
        <tr>
            <td class="resource" colspan="2"><xsl:value-of select="mioga:gettext('Sorry, there is no resource available at this time. You should invite some resources into this group.')"/></td>
        </tr>
        </xsl:if>
    </table>
</xsl:template>

</xsl:stylesheet>
