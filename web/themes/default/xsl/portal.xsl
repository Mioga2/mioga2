<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="html"/>

<!-- ===============
	root document
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/portal.css" media="screen" />
	<xsl:if test="/Portal/Mioga2Portal/global/css_file">
		<link rel="stylesheet" type="text/css" href="{$private_dav_uri}/{/Portal/Mioga2Portal/global/css_file}" media="screen" />
	</xsl:if>


	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="theme-css"/>
</head>
<body id="portal_body" class="portal">
	<xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- =================================
     Declare keys used to access to 
     content and mioglet definitions
     ================================= -->
<xsl:key name="content" match="content" use="@id"/>
<xsl:key name="mioglet" match="miogletdesc" use="@id"/>

<!-- =================================
     Portal Main templates
     ================================= -->

<xsl:template match="Portal">
    <xsl:apply-templates select="Mioga2Portal"/>
</xsl:template>

<xsl:template match="Mioga2Portal">
<body>
	<xsl:apply-templates select="portal/*"/>                
</body>
</xsl:template>

<!-- =================================
     hbox : Handle horizontal box declaration
     ================================= -->

<xsl:template match="hbox">
    <table id="{@id}" class="dHBox">
        <xsl:call-template name="container-attrs"/>
       
        <tr valign="top">
            <xsl:for-each select="box">
                <td class="Box" id="{@id}">
                    <xsl:apply-templates select="."/>
                </td>
            </xsl:for-each>
        </tr>

        <xsl:if test="count(box) = 0">
            <tr><td>&#160;</td></tr>
        </xsl:if>

       
    </table>
</xsl:template>

<!-- =================================
     vbox : Handle vertical box declaration
     ================================= -->

<xsl:template match="vbox">
    <table id="{@id}" class="dVBox">
        <xsl:call-template name="container-attrs"/>
        <xsl:for-each select="box">
            <tr valign="top">
                <td name="box" id="{@id}">
                    <xsl:apply-templates select="."/>
                </td>
            </tr>
        </xsl:for-each>

        <xsl:if test="count(box) = 0">
            <tr><td>&#160;</td></tr>
        </xsl:if>

    </table>
</xsl:template>


<!-- =================================
     box : Handle box declaration
     ================================= -->

<xsl:template match="box">
    <xsl:call-template name="box-attrs"/>

    <xsl:choose>
        <xsl:when test="@ref">
            <xsl:variable name="content_ref" select="key('content', @ref)"/>
            
            <xsl:choose>
                <xsl:when test="$content_ref">
                    <xsl:for-each select="$content_ref/*">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="mioga:gettext('Invalid reference: %s', string(@ref))"/>
                </xsl:otherwise>
            </xsl:choose>
            
        </xsl:when>
        <xsl:otherwise>
            <xsl:apply-templates select="*"/>            
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<!-- =================================
     container-attrs : Handle container attributes
     ================================= -->

<xsl:template name="container-attrs">

    <xsl:attribute name="cellspacing">
        <xsl:choose>
            <xsl:when test="@spacing"><xsl:value-of select="@spacing"/></xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
    </xsl:attribute>

    <xsl:attribute name="cellpadding">
        <xsl:choose>
            <xsl:when test="@padding"><xsl:value-of select="@padding"/></xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
    </xsl:attribute>

</xsl:template>

<!-- =================================
     box-attrs : Handle box attributes
     ================================= -->

<xsl:template name="box-attrs">
    <xsl:if test="@width">
        <xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute>
    </xsl:if>    

    <xsl:variable name="style">
        <xsl:if test="@border">
            <xsl:if test="contains(@border, 'left')">border-left-width: 1px; </xsl:if>
            <xsl:if test="contains(@border, 'right')">border-right-width: 1px; </xsl:if>
            <xsl:if test="contains(@border, 'top')">border-top-width: 1px; </xsl:if>
            <xsl:if test="contains(@border, 'bottom')">border-bottom-width: 1px; </xsl:if>
            <xsl:choose>
                <xsl:when test="contains(@border, 'all')">border-width: 1px; </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="contains(@border, 'left') = 0">border-left-width: 0px; </xsl:if>
                    <xsl:if test="contains(@border, 'right') = 0">border-right-width: 0px; </xsl:if>
                    <xsl:if test="contains(@border, 'top') = 0">border-top-width: 0px; </xsl:if>
                    <xsl:if test="contains(@border, 'bottom') = 0">border-bottom-width: 0px; </xsl:if>
                </xsl:otherwise>
            </xsl:choose>border-style: solid; border-color: <xsl:value-of select="$mioga-box-title-bg-color"/>;</xsl:if>
        <xsl:if test="../@padding-top">padding-top: <xsl:value-of select="../@padding-top"/>;</xsl:if>
        <xsl:if test="../@padding-bottom">padding-bottom: <xsl:value-of select="../@padding-bottom"/>;</xsl:if>
        <xsl:if test="../@padding-right">padding-right: <xsl:value-of select="../@padding-right"/>;</xsl:if>
        <xsl:if test="../@padding-left">padding-left: <xsl:value-of select="../@padding-left"/>;</xsl:if>
    </xsl:variable>

    <xsl:if test="$style != ''">
        <xsl:attribute name="style"><xsl:value-of select="$style"/></xsl:attribute>
    </xsl:if>

    <xsl:if test="@bgcolor">
        <xsl:attribute name="bgcolor"><xsl:value-of select="@bgcolor"/></xsl:attribute>
    </xsl:if>

</xsl:template>

<!-- =================================
     html : Handle inline html
     ================================= -->

<xsl:template match="html">
    <xsl:attribute name="name">html</xsl:attribute>
    <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>

    <xsl:variable name="title">
        <xsl:choose>
            <xsl:when test="title">
                <xsl:value-of select="title"/>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
        
    <xsl:variable name="body">
        <xsl:choose>
            <xsl:when test="count(content/*) != 0">
                <xsl:copy-of select="content/*"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="content"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:choose>
        <xsl:when test="boxed">
            <xsl:call-template name="MiogaBox">
                <xsl:with-param name="no-title" select="1 - count(titled)"/>
                <xsl:with-param name="title" select="$title"/>
                <xsl:with-param name="body" select="$body"/>
                <xsl:with-param name="width">100%</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:if test="titled">
                <xsl:call-template name="MiogletTitle">
                    <xsl:with-param name="title" select="$title"/>
                </xsl:call-template>
            </xsl:if>
            
            <xsl:copy-of select="$body"/>
        </xsl:otherwise>
    </xsl:choose>
    

</xsl:template>


<!-- =================================
     TitledBox : Handle titled box
     ================================= -->

<xsl:template match="TitledBox">
    <xsl:variable name="title">
        <xsl:value-of select="title"/>
    </xsl:variable>

    <xsl:attribute name="class">titled-box</xsl:attribute>

    <xsl:call-template name="MiogaBox">
        <xsl:with-param name="no-title" select="1 - count(title)"/>
        <xsl:with-param name="title" select="$title"/>
        <xsl:with-param name="width" select="@width"/>
        <xsl:with-param name="body">
            <table border="0" id="{@id}">
                <xsl:call-template name="container-attrs"/>
                <tr>
                    <td>
                        <xsl:apply-templates select="*[name(.) != 'title']"/>
                    </td>
                </tr>
            </table>
        </xsl:with-param>
    </xsl:call-template>
</xsl:template>


<!-- =================================
     mioglet : Handle mioglets
     ================================= -->

<xsl:template match="mioglet">
    <xsl:variable name="mioglet_ref" select="key('mioglet', @ref)"/>
    <xsl:variable name="node" select="."/>
    
    <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>

    <xsl:for-each select="$mioglet_ref/*">

        <xsl:attribute name="name"><xsl:value-of select="local-name(.)"/></xsl:attribute>                        

        <xsl:variable name="title">
            <xsl:choose>
                <xsl:when test="$node/title">
                    <xsl:value-of select="$node/title"/>
                </xsl:when>
                <xsl:when test="./title">
                    <xsl:value-of select="./title"/>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="body">
            <xsl:apply-templates select=".">
                <xsl:with-param name="node" select="$node"/>
            </xsl:apply-templates>
        </xsl:variable>

        <xsl:variable name="css_id">
            <xsl:choose>
                <xsl:when test="$node/css_id">
                    <xsl:value-of select="$node/css_id"/>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="$node/boxed">
                <xsl:call-template name="PortalBox">
                    <xsl:with-param name="no-title" select="1 - count($node/titled)"/>
                    <xsl:with-param name="title" select="$title"/>
                    <xsl:with-param name="body" select="$body"/>
                    <xsl:with-param name="css_id" select="$css_id"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="$node/titled">
                    <xsl:call-template name="MiogletTitle">
                        <xsl:with-param name="title" select="$title"/>
                    </xsl:call-template>
                </xsl:if>
                
                <xsl:copy-of select="$body"/>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:for-each>

</xsl:template>

<!-- =================================
     MiogletTitle : Display the title of mioglets
     ================================= -->

<xsl:template name="MiogletTitle">
    <xsl:param name="title"/>

    <xsl:call-template name="MiogaTitle">
        <xsl:with-param name="title" select="$title"/>
    </xsl:call-template>

</xsl:template>

<!-- ================================
	PortalBox : replace MiogaBox for Portal
	TODO : must replace MiogaBox in widget-tools
	================================= -->

<xsl:template name="PortalBox">
	<xsl:param name="no-title">0</xsl:param>
	<xsl:param name="title"/>
	<xsl:param name="body"/>
	<xsl:param name="css_id"/>
      
	<div class="portal_box" id="{$css_id}">
		<xsl:if test="$no-title != 1">
			<h2 class="pbox_title"><xsl:copy-of select="$title"/></h2>
		</xsl:if>

		<div class="pbox_content">
			<xsl:choose>
				<xsl:when test="$body">
					<xsl:copy-of select="$body"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="." mode="MiogaBoxBody"/>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</div>
</xsl:template>

<!-- =================================
     MiogletOtherLink : Display tiny "Other ..." link
     ================================= -->

<xsl:template name="MiogletOtherLink">
    <xsl:param name="href"/>
    <xsl:value-of select="mioga:gettext('Other:')"/>&#160;<a href="{$href}" style="font-size: small">[...]</a>
</xsl:template>

</xsl:stylesheet>
