<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:file="http://www.mioga2.org/portal/mioglets/File">

<xsl:output method="html"/>

<!-- =================================
     Handle Folders
     ================================= -->

<xsl:template match="file:Folder" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">file_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">mode</xsl:with-param>
        <xsl:with-param name="value">user</xsl:with-param>        
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">folder</xsl:with-param>
        <xsl:with-param name="value" select="$node/folder"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">show_files</xsl:with-param>
        <xsl:with-param name="value" select="$node/show_files"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">use_filebrowser</xsl:with-param>
        <xsl:with-param name="value" select="$node/use_filebrowser"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">chroot</xsl:with-param>
        <xsl:with-param name="value" select="$node/chroot"/>
    </xsl:call-template>
</xsl:template>


<xsl:template match="file:Folder">
    <xsl:param name="node"/>
    
    <xsl:if test="count(item) != 0">
    <table class="fileFolder" cellpadding="5" cellspacing="0">
    <xsl:for-each select="item">
	    <tr>
		    <xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
					<xsl:otherwise>oddRow</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<td>
				<xsl:variable name="fimage">
					<xsl:choose>
						<xsl:when test="@type = 'collection'"><xsl:value-of select="$image_uri"/>/16x16/mimetypes/inode-directory.png</xsl:when>
						<xsl:otherwise><xsl:value-of select="$image_uri"/>/16x16/mimetypes/unknown.png</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<a style="background: url({$fimage}); padding-left: 20px; background-repeat: no-repeat;" href="{@link}"><xsl:value-of select="."/></a>
			</td>
      </tr>
    </xsl:for-each>
    </table>
    </xsl:if>
</xsl:template>

<!-- =================================
     Handle LastModified
     ================================= -->

<xsl:template match="file:LastModified" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">file_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">mode</xsl:with-param>
        <xsl:with-param name="value">user</xsl:with-param>        
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">limit</xsl:with-param>
        <xsl:with-param name="value" select="$node/limit"/>
    </xsl:call-template>
</xsl:template>


<xsl:template match="file:LastModified">
    <xsl:param name="node"/>
    
    <xsl:if test="count(entry) != 0">
    <table class="fileLastModified" cellpadding="5" cellspacing="0" width="100%">
    <xsl:for-each select="entry">
      <tr>
        <xsl:attribute name="class">
        <xsl:choose>
          <xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
          <xsl:otherwise>oddRow</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <td>
        <a href="{uri}"><xsl:value-of select="uri/@cn"/></a> 
      </td>
      <td style="font-style: italic; font-size: x-small; color: grey">
        <xsl:value-of select="modified"/>
      </td>
      <td style="font-style: italic; font-size: x-small; color: grey">
        <xsl:value-of select="user"/>
      </td>
      </tr>
    </xsl:for-each>
    </table>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>
