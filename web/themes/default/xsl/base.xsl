<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
  
<!--  <xsl:output method="html"/> -->

  <xsl:import href="ie.xsl"/>

  <xsl:include href="button.xsl"/>
  <xsl:include href="widget-colors.xsl"/>
  <xsl:include href="widget-attribute-sets.xsl"/>
  <xsl:include href="widget-tools.xsl"/>
  <xsl:include href="widget-text.xsl"/>
  <xsl:include href="widget-menu.xsl"/>
  <xsl:include href="widget-action-box.xsl"/>
  <xsl:include href="widget-form.xsl"/>
  <xsl:include href="widget-report.xsl"/>
  <xsl:include href="widget-calendar.xsl"/>
  

  <!-- =================================
	   Standard variables description
	   ================================= -->
  
  <xsl:variable name="base_uri" select="//miogacontext/uri/base" />
  <xsl:variable name="bin_uri" select="//miogacontext/uri/bin_uri" />
  <xsl:variable name="public_bin_uri" select="//miogacontext/uri/public_bin" />
  <xsl:variable name="private_bin_uri" select="//miogacontext/uri/private_bin" />
  <xsl:variable name="user_bin_uri" ><xsl:value-of select="//miogacontext/uri/bin_uri" />/<xsl:value-of select="//miogacontext/user/ident" /></xsl:variable>
  <xsl:variable name="public_dav_uri" select="//miogacontext/uri/public_dav" />
  <xsl:variable name="private_dav_uri" select="//miogacontext/uri/private_dav" />
  <xsl:variable name="public_dav" select="//miogacontext/uri/public_dav_uri" />
  <xsl:variable name="private_dav" select="//miogacontext/uri/private_dav_uri" />
  <xsl:variable name="logout_uri" select="//miogacontext/uri/logout" />
  <xsl:variable name="cdn_uri" select="//miogacontext/uri/cdn_uri"/>
  <xsl:variable name="mioga_version" select="//miogacontext/mioga_version" />
  <xsl:variable name="jslib_uri"><xsl:value-of select="$cdn_uri" />/<xsl:value-of select="$mioga_version" />/jslib</xsl:variable>
  <xsl:variable name="theme_uri"><xsl:value-of select="$base_uri" />/themes/<xsl:value-of select="//miogacontext/group/theme" /></xsl:variable>
  <xsl:variable name="image_uri"><xsl:value-of select="$theme_uri" /><xsl:value-of select="//miogacontext/uri/image" /></xsl:variable>
  <xsl:variable name="help_uri"><xsl:value-of select="$cdn_uri" />/<xsl:value-of select="$mioga_version" />/help</xsl:variable>
  <xsl:variable name="mioga_application" select="//miogacontext/application/ident" />
  <xsl:variable name="mioga_method" select="//miogacontext/application/method" />
  <xsl:variable name="mioga_context" select="//miogacontext" />


<!-- ================================= 

	 MiogaAppIcon
	 
	 Return application icon

	 ================================= -->
	
<xsl:variable name="app_i18n_filename"><xsl:value-of select="$mioga_context/dir/langdir"/>/<xsl:value-of select="$mioga_context/group/lang"/>.xml</xsl:variable>
<xsl:variable name="app_i18n" select="document($app_i18n_filename)"/>

<xsl:template name="MiogaAppIcon">
	<xsl:param name="app_ident"/>
	<xsl:variable name="app_node" select="$app_i18n/lang/application[@ident=$app_ident]"/>

	<xsl:choose>
		<xsl:when test="$app_node/icon"><xsl:value-of select="$app_node/icon"/></xsl:when>
		<xsl:otherwise>application.png</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- ================================= 

	 MiogaAppName
	 
	 Return localized application name

	 ================================= -->
	
<xsl:template name="MiogaAppName">
	<xsl:param name="app_ident"/>
	<xsl:variable name="app_node" select="$app_i18n/lang/application[@ident=$app_ident]"/>
	<xsl:value-of select="$app_node/name"/>
</xsl:template>

<!-- ================================= 

	 MiogaAppDesc
	 
	 Return localized application description

	 ================================= -->
	
<xsl:template name="MiogaAppDesc">
	<xsl:param name="app_ident"/>
	<xsl:variable name="app_node" select="$app_i18n/lang/application[@ident=$app_ident]"/>
	<xsl:value-of select="$app_node/description"/>
</xsl:template>

<!-- ================================= 

   NavigationBar
   
   displays navigation bar

   ================================= -->

<xsl:template name="NavigationBar">
  <div id="menu">
    <ul id="context">
      <li>
        <a id="help" title="mioga:gettext('Current application help')" href="#"><xsl:value-of select="mioga:gettext('Help')"/></a>
      </li>
    </ul>
    <ul id="menuroot">
      <li class="item">
        <a id="homebreadcrumb" class="breadcrumb" href="{//miogacontext/user/home_url}">
          <em>
            <span><xsl:value-of select="mioga:gettext('Home')"/></span>
          </em>
        </a>
      </li>
      <li class="item">
        <a href="#" class="breadcrumb">
          <em><span class="icon-{//miogacontext/application/ident}"><xsl:value-of select="//miogacontext/application/ident"/></span></em>
        </a>
      </li>
    </ul>
  </div>
</xsl:template>

<!-- ================================= 

	 form template

	 Display a form.

	 <xsl:call-template name="form">
		 <xsl:with-param name="name"	value=""/>   Required : The form name
		 <xsl:with-param name="action"  value=""/>   Required : The action href ("action" attribute of <form> html tag)
		 <xsl:with-param name="method"  value=""/>   Required : The method used (GET POST, PUT, ...)
		 <xsl:with-param name="enctype" value=""/>   Optional : The enctype of the form (enctype attribute of <form> html tag) 
		 <xsl:with-param name="nbcols"  value=""/>   Optional : Number of columns in the form (2 by default)
	 </xsl:call-template>

	 ================================= -->
	

<xsl:template name="form">
	<xsl:param name="name"></xsl:param>
	<xsl:param name="type"/>
	<xsl:param name="action"/>
	<xsl:param name="method"/>
	<xsl:param name="enctype"/>
	<xsl:param name="nbcols">2</xsl:param>
	<xsl:param name="width"></xsl:param>
	<xsl:param name="no_button">0</xsl:param>
 
	<form method="{$method}">
		<xsl:if test="$width!=''">
			<xsl:attribute name="style">width: <xsl:value-of select="$width"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$enctype">
			<xsl:attribute name="enctype"><xsl:value-of select="$enctype"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$action">
			<xsl:attribute name="action"><xsl:value-of select="$action"/></xsl:attribute>
		</xsl:if>


		<table xsl:use-attribute-sets="mioga-form-table" cellspacing="0" align="center">
			<xsl:if test="$width!=''">
				<xsl:attribute name="width">100%</xsl:attribute>
			</xsl:if>
			<xsl:if test="$name != ''">
				<tr xsl:use-attribute-sets="mioga-form-table-title">
					<td colspan="{$nbcols}" width="100%">
						<font class="{$mioga-title-color}">
							<xsl:value-of select="$name"/>
						</font>
					</td>
				</tr>
			
				<tr>
					<td colspan="{$nbcols}">&#160;</td>
				</tr>
				
			</xsl:if>

			<xsl:apply-templates select="."  mode="form-body">
				<xsl:with-param name="type" select="$type"/>
			</xsl:apply-templates>
			
			<xsl:if test="$no_button = 0">
				<tr>
					<td colspan="{$nbcols}">&#160;</td>
				</tr>
				
				<xsl:apply-templates select="."  mode="form-button">
					<xsl:with-param name="type" select="$type"/>
				</xsl:apply-templates>
			</xsl:if>
	   </table>
	</form>
</xsl:template>


<!-- =================================
	 Argcheck errors.

	 Display all errors messages generated 
	 by form validation.

	 Based on XML generated by ac_XMLifyErrors 
	 perl function.
	 ================================= -->

<xsl:template name="arg_errors">
	<xsl:variable name="callback" select="."/>

	<xsl:for-each select="errors/err">
		<xsl:variable name="name" select="@arg"/>
		
		
		<xsl:variable name="i18n-name">
			<xsl:apply-templates select="../.." mode="arg-checker-field-name">
				<xsl:with-param name="name" select="@arg"/>
			</xsl:apply-templates>
		</xsl:variable>
		
		<xsl:variable name="value">
			<xsl:for-each select="../../fields/*">
				<xsl:if test="local-name(.)=$name"><xsl:value-of select="."/></xsl:if>
			</xsl:for-each>
		</xsl:variable>
		
		<xsl:for-each select="type">
			<p class="error">
				<xsl:call-template name="errorTranslation">
					<xsl:with-param name="name" select="$name"/>
					<xsl:with-param name="i18n-name" select="$i18n-name"/>
					<xsl:with-param name="value" select="$value"/>
					<xsl:with-param name="callback" select="$callback"/>
				</xsl:call-template>
			</p>
		</xsl:for-each>
	</xsl:for-each>
</xsl:template>

<!-- =================================
	 Locale Date templates
	 ================================= -->

<!-- =================================
	 LocalDateAbr

	 Print abreviated date in locale format.

	 Based on XML generated by du_GetDateXML or 
	 du_GetDateXMLInUserLocale

	 ================================= -->

<xsl:template name="LocalDateAbr">
	<xsl:param name="node" select="."/>

	<xsl:call-template name="LocalDayOfWeekAbr">
		<xsl:with-param name="daynum" select="$node/wday"/>
	</xsl:call-template>&#160;<xsl:value-of select="$node/day"/>&#160;<xsl:call-template name="LocalMonthAbr">
		<xsl:with-param name="monthnum" select="$node/month"/>
	</xsl:call-template>&#160;<xsl:value-of select="$node/year"/>
	
</xsl:template>


<!-- =================================
	 LocalDateShort

	 Print date in locale format in short format.
	 Month name are not printed.
	 (expl in ISO format "Wed 2004-06-02")

	 Based on XML generated by du_GetDateXML or 
	 du_GetDateXMLInUserLocale

	 ================================= -->

<xsl:template name="LocalDateShort">
	<xsl:param name="node" select="."/>

	<xsl:call-template name="LocalDayOfWeekAbr">
		<xsl:with-param name="daynum" select="$node/wday"/>
	</xsl:call-template>&#160;<xsl:value-of select="$node/day"/>/<xsl:if test="string-length($node/month)=1">0</xsl:if><xsl:value-of select="$node/month"/>/<xsl:value-of select="$node/year"/>
	
</xsl:template>


<!-- =================================
	 LocalDateTime

	 Print full length date and time
	 in locale format. 

	 Based on XML generated by du_GetDateXML or 
	 du_GetDateXMLInUserLocale

	 ================================= -->

<xsl:template name="LocalDateTime">
	<xsl:param name="node" select="."/>

	<xsl:call-template name="LocalDayOfWeek">
		<xsl:with-param name="daynum" select="$node/wday"/>
	</xsl:call-template>&#160;<xsl:value-of select="$node/day"/>&#160;<xsl:call-template name="LocalMonth">
		<xsl:with-param name="monthnum" select="$node/month"/>
	</xsl:call-template>&#160;<xsl:value-of select="$node/year"/>&#160;<xsl:call-template name="LocalTime"><xsl:with-param name="node" select="$node"/></xsl:call-template>
</xsl:template>


  <!-- =================================
	   LocalTime
	   
	   Print time in locale format. 
	   
	   Based on XML generated by du_GetDateXML or 
	   du_GetDateXMLInUserLocale
	   
	   ================================= -->

  <xsl:template name="LocalTime">
	<xsl:param name="node" select="."/>
	
	<xsl:param name="hour" select="$node/hour"/>
	<xsl:param name="min" select="$node/min"/>
	<xsl:param name="sec" select="$node/sec"/>

	<xsl:param name="nosec">0</xsl:param>

	<xsl:if test="string-length($hour)=1">0</xsl:if><xsl:value-of select="$hour"/>
	<xsl:if test="$min!=0">
		<xsl:text>:</xsl:text>
		<xsl:if test="string-length($min)=1">0</xsl:if><xsl:value-of select="$min"/>
		<xsl:if test="$nosec != 1 and $sec!=0">:<xsl:if test="string-length($sec)=1">0</xsl:if><xsl:value-of select="$sec"/></xsl:if>
	</xsl:if>
  </xsl:template>


  <!-- =================================
	   LocalDateInput
	   
	   Display a set of select form input 
	   to select dates. 
	   
	   Parameters :
	   year-value  : current values of year
	   month-value : current values of month (0-11)
	   day-value   : current values of year
	   year-name   : name of the year parameter
	   month-name  : name of the month parameter
	   day-name	: name of the year parameter
	   first-year  : first year to display
	   
	   ================================= -->

<xsl:template name="LocalDateInput">
	<xsl:param name="year-value"/>
	<xsl:param name="month-value"/>
	<xsl:param name="day-value"/>
	<xsl:param name="year-name"/>
	<xsl:param name="month-name"/>
	<xsl:param name="day-name"/>
	<xsl:param name="first-year"/>

	<select name="{$day-name}">
		<option value=""></option>

		<xsl:call-template name="forloop-option">
			<xsl:with-param name="from">1</xsl:with-param>
			<xsl:with-param name="to">31</xsl:with-param>
			<xsl:with-param name="step">1</xsl:with-param>
			<xsl:with-param name="selected-value" select="$day-value"/>
		</xsl:call-template>
	</select>&#160;<select name="{$month-name}">
		<option value=""></option>

		<xsl:call-template name="LocalDateMonthOptions">
			<xsl:with-param name="value" select="$month-value"/>
			<xsl:with-param name="name" select="$month-name"/>
		</xsl:call-template>
	</select>&#160;<select name="{$year-name}">
		<option value=""></option>

		<xsl:call-template name="forloop-option">
			<xsl:with-param name="from" select="$first-year"/>
			<xsl:with-param name="to" select="$first-year + 3"/>
			<xsl:with-param name="step">1</xsl:with-param>
			<xsl:with-param name="selected-value" select="$year-value"/>
		</xsl:call-template>
	</select>
	
</xsl:template>


<!-- =================================
	 LocalDateTimeInput

	 Display a set of select form input 
	 to select dates. 

	 Parameters :
	 year-value  : current values of year
	 month-value : current values of month (0-11)
	 day-value   : current values of day
	 hour-value  : current values of hour
	 min-value   : current values of min
	 sec-value   : current values of sec
	 year-name   : name of the year parameter
	 month-name  : name of the month parameter
	 day-name	: name of the year parameter
	 hour-name   : name of the hour parameter
	 min-name	: name of the min parameter
	 sec-name	: name of the sec parameter
	 use-second  : display or not the second field
	 first-year  : first year to display
	 minute-step : step of minutes

	 ================================= -->

<xsl:template name="LocalDateTimeInput">
	<xsl:param name="year-value"/>
	<xsl:param name="month-value"/>
	<xsl:param name="day-value"/>
	<xsl:param name="hour-value"/>
	<xsl:param name="min-value"/>
	<xsl:param name="sec-value"/>
	<xsl:param name="year-name"/>
	<xsl:param name="month-name"/>
	<xsl:param name="day-name"/>
	<xsl:param name="hour-name"/>
	<xsl:param name="min-name"/>
	<xsl:param name="sec-name"/>
	<xsl:param name="use-second">0</xsl:param>
	<xsl:param name="first-year"/>
	<xsl:param name="minute-step">1</xsl:param>

	<select name="{$day-name}">
		<option value=""></option>

		<xsl:call-template name="forloop-option">
			<xsl:with-param name="from">1</xsl:with-param>
			<xsl:with-param name="to">31</xsl:with-param>
			<xsl:with-param name="step">1</xsl:with-param>
			<xsl:with-param name="selected-value" select="$day-value"/>
		</xsl:call-template>
	</select>&#160;<select name="{$month-name}">
		<option value=""></option>

		<xsl:call-template name="LocalDateMonthOptions">
			<xsl:with-param name="value" select="$month-value"/>
			<xsl:with-param name="name" select="$month-name"/>
		</xsl:call-template>
	</select>&#160;<select name="{$year-name}">
		<option value=""></option>

		<xsl:call-template name="forloop-option">
			<xsl:with-param name="from" select="$first-year"/>
			<xsl:with-param name="to" select="$first-year + 3"/>
			<xsl:with-param name="step">1</xsl:with-param>
			<xsl:with-param name="selected-value" select="$year-value"/>
		</xsl:call-template>
	</select><br/><select name="{$hour-name}">
		<option value=""></option>

		<xsl:call-template name="forloop-option">
			<xsl:with-param name="from" select="0"/>
			<xsl:with-param name="to" select="23"/>
			<xsl:with-param name="step">1</xsl:with-param>
			<xsl:with-param name="selected-value" select="$hour-value"/>
		</xsl:call-template>
	</select>&#160;h&#160;<select name="{$min-name}">
		<option value=""></option>

		<xsl:call-template name="forloop-option">
			<xsl:with-param name="from" select="0"/>
			<xsl:with-param name="to" select="59"/>
			<xsl:with-param name="step" select="$minute-step"/>
			<xsl:with-param name="selected-value" select="$min-value"/>
		</xsl:call-template>
	</select><xsl:if test="$use-second != 0">&#160;m&#160;<select name="{$sec-name}">
		<option value=""></option>

		<xsl:call-template name="forloop-option">
			<xsl:with-param name="from" select="0"/>
			<xsl:with-param name="to" select="59"/>
			<xsl:with-param name="step">1</xsl:with-param>
			<xsl:with-param name="selected-value" select="$sec-value"/>
		</xsl:call-template>
	</select></xsl:if>
	
</xsl:template>

<!-- =================================
	 LocalTimeInput

	 Display a set of select form input 
	 to select dates. 

	 Parameters :
	 hour-value  : current values of hour
	 min-value   : current values of min
	 sec-value   : current values of sec
	 hour-name   : name of the hour parameter
	 min-name	: name of the min parameter
	 sec-name	: name of the sec parameter
	 use-second  : display or not the second field
	 minute-step : step of minutes

	 ================================= -->

<xsl:template name="LocalTimeInput">
	<xsl:param name="hour-value"/>
	<xsl:param name="min-value"/>
	<xsl:param name="sec-value"/>
	<xsl:param name="hour-name"/>
	<xsl:param name="min-name"/>
	<xsl:param name="sec-name"/>
	<xsl:param name="use-second">0</xsl:param>
	<xsl:param name="minute-step">1</xsl:param>

	<nobr>
	<select name="{$hour-name}">
		<option value=""></option>

		<xsl:call-template name="forloop-option">
			<xsl:with-param name="from" select="0"/>
			<xsl:with-param name="to" select="23"/>
			<xsl:with-param name="step">1</xsl:with-param>
			<xsl:with-param name="selected-value" select="$hour-value"/>
		</xsl:call-template>
	</select>&#160;h&#160;<select name="{$min-name}">
		<option value=""></option>

		<xsl:call-template name="forloop-option">
			<xsl:with-param name="from" select="0"/>
			<xsl:with-param name="to" select="59"/>
			<xsl:with-param name="step" select="$minute-step"/>
			<xsl:with-param name="selected-value" select="$min-value"/>
		</xsl:call-template>
	</select><xsl:if test="$use-second!=0">&#160;m&#160;<select name="{$sec-name}">
		<option value=""></option>

		<xsl:call-template name="forloop-option">
			<xsl:with-param name="from" select="0"/>
			<xsl:with-param name="to" select="59"/>
			<xsl:with-param name="step">1</xsl:with-param>
			<xsl:with-param name="selected-value" select="$sec-value"/>
		</xsl:call-template>
	</select>&#160;s</xsl:if>
	</nobr>
</xsl:template>

<!-- =================================
	 LocalDurationInput

	 Display a set of select form input 
	 to select dates. 

	 Parameters :
	 day-value   : current values of hour
	 hour-value  : current values of hour
	 min-value   : current values of min
	 sec-value   : current values of sec
	 day-name	: name of the day parameter
	 hour-name   : name of the hour parameter
	 min-name	: name of the min parameter
	 sec-name	: name of the sec parameter
	 use-second  : display or not the second field
	 minute-step : step of minutes
	 min-grad	: minimal graduation (day, hour, minute or second)
	 max-grad	: maximal graduation (day, hour, minute or second)
	 min-step	: step of the minimal graduation
	 max-value   : max value of the maximal graduation

	 ================================= -->

<xsl:template name="LocalDurationInput">
	<xsl:param name="value"/>
	<xsl:param name="name"/>
	<xsl:param name="graduation-name"/>
	<xsl:param name="graduation-value"/>

	<input type="text" name="{$name}" value="{$value}"/>&#160;<select name="{$graduation-name}">
		<option value="min">minutes</option>
		<option value="hour">heures</option>
		<option value="sec">secondes</option>
	</select>
	
</xsl:template>

<!-- Used by LocalDateInput -->

<xsl:template name="forloop-option">
	<xsl:param name="from"/>
	<xsl:param name="to"/>
	<xsl:param name="step">1</xsl:param>
	<xsl:param name="selected-value"></xsl:param>

	<xsl:if test="$from &lt;= $to">
		<option value="{$from}">
			<xsl:if test="$from = $selected-value">
				<xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>

			<xsl:value-of select="$from"/>
		</option>

		<xsl:call-template name="forloop-option">
			<xsl:with-param name="from" select="$from + $step"/>
			<xsl:with-param name="to" select="$to"/>
			<xsl:with-param name="step" select="$step"/>
			<xsl:with-param name="selected-value" select="$selected-value"/>
		</xsl:call-template>
	</xsl:if>

</xsl:template>

<!-- Used by LocalDateInput -->

<xsl:template name="LocalDateMonthOptions">
	<xsl:param name="value"/>
	<xsl:param name="from">0</xsl:param>
	<xsl:param name="to">11</xsl:param>
	

	<xsl:if test="$from &lt;= $to">
		
		<option value="{$from+1}">
			<xsl:if test="$from+1 = $value">
				<xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>

			<xsl:call-template name="LocalMonth">
				<xsl:with-param name="monthnum" select="$from + 1"/>
			</xsl:call-template>
		</option>

		<xsl:call-template name="LocalDateMonthOptions">
			<xsl:with-param name="from" select="$from + 1"/>
			<xsl:with-param name="to" select="$to"/>
			<xsl:with-param name="value" select="$value"/>
		</xsl:call-template>
	</xsl:if>


</xsl:template>

<!-- =================================
	 SuccessMessage

	 Display a success message window.

	 Parameters :
	 message : message to display
	 referer : address to redirect
	 reload_menu, target : reload the $target frame when ok button is clicked.

	 ================================= -->

<xsl:template name="SuccessMessage">
  <xsl:param name="message"/>
  <xsl:param name="referer"/>
  <xsl:param name="reload_menu">0</xsl:param>

  <xsl:choose>
	  <xsl:when test="count(ancestor::*)=0">
		  <xsl:call-template name="SuccessMessageTop">
			  <xsl:with-param name="message" select="$message"/>
			  <xsl:with-param name="referer" select="$referer"/>
			  <xsl:with-param name="reload_menu" select="$reload_menu"/>
		  </xsl:call-template>
	  </xsl:when>

	  <xsl:otherwise>
		  <xsl:call-template name="SuccessMessageNotTop">
			  <xsl:with-param name="message" select="$message"/>
			  <xsl:with-param name="referer" select="$referer"/>
			  <xsl:with-param name="reload_menu" select="$reload_menu"/>
		  </xsl:call-template>		  
	  </xsl:otherwise>
  </xsl:choose>

</xsl:template>

<xsl:template name="SuccessMessageTop">
  <xsl:param name="message"/>
  <xsl:param name="referer"/>
  <xsl:param name="reload_menu"/>

  <body xsl:use-attribute-sets="body_attr">
	  <xsl:call-template name="DisplayAppTitle"/>

  </body>	
</xsl:template>

<xsl:template name="SuccessMessageNotTop">
  <xsl:param name="message"/>
  <xsl:param name="referer"/>
  <xsl:param name="reload_menu"/>
  
  <div xsl:use-attribute-sets="app_message_body_attr">
	  <xsl:call-template name="MiogaMessage">
		  <xsl:with-param name="emphasis">1</xsl:with-param>
		  <xsl:with-param name="message">
			  <xsl:value-of select="$message"/>
		  </xsl:with-param>
	  </xsl:call-template>
  </div>
  
  <div xsl:use-attribute-sets="app_footer_attr">
	  <xsl:call-template name="back-button">
		  <xsl:with-param name="href"><xsl:value-of select="$referer"/></xsl:with-param>
		  <xsl:with-param name="reload_menu" select="$reload_menu"/>
	  </xsl:call-template>
  </div>
</xsl:template>


<!-- =================================
	 Confirm

	 Display a confirmation window.

	 Parameters :
	 question : message to display
	 valid_action_uri : href to call when the OK button is clicked
	 cancel_uri : href to call when the cancel button is clicked
	 display_body : if true, apply the calling template in confirm-body mode.
	 type : the button type. Supported values are ok-cancel (the default) and yes-no
	 ================================= -->

<xsl:template name="Confirm">
  <!-- Param -->
  <xsl:param name="question"/>
  <xsl:param name="valid_action_uri"/>
  <xsl:param name="cancel_uri"/>
  <xsl:param name="display_body">0</xsl:param>
  <xsl:param name="type">ok-cancel</xsl:param>

  <!-- Param -->
  <body xsl:use-attribute-sets="body_attr">
	<xsl:call-template name="DisplayAppTitle"/>

	
	<div xsl:use-attribute-sets="app_message_body_attr">
		
		<xsl:call-template name="MiogaMessage">
			<xsl:with-param name="emphasis">1</xsl:with-param>
			<xsl:with-param name="message" select="$question"/>
		</xsl:call-template>
	
		<xsl:if test="$display_body = 1">
			<xsl:apply-templates select="." mode="confirm-body"/>
		</xsl:if>
	  
	</div>

	<div xsl:use-attribute-sets="app_footer_attr">
		<xsl:choose>
			<xsl:when test="$type = 'ok-cancel'">
				<xsl:call-template name="ok-cancel-button">
					<xsl:with-param name="ok-href" select="$valid_action_uri"/>
					<xsl:with-param name="cancel-href" select="$cancel_uri"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="yes-no-button">
					<xsl:with-param name="yes-href" select="$valid_action_uri"/>
					<xsl:with-param name="no-href" select="$cancel_uri"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</div>
  </body>
</xsl:template>

  <!-- =================================
	   DisplayAppTitle
	   
	   Display the top navigation bar.
	   
	   ================================= -->
  
  <xsl:template name="DisplayAppTitle">
	  <xsl:param name="help"/>

	  <xsl:variable name="miogacontext" select="//miogacontext"/>
	
    <xsl:if test="count($miogacontext/NoHeader) = 0">
	<div id="apptitle">
		<table id="apptitle-header">
			<tr>
				<td>
					<div id="header-logo">&#160;</div>
				</td>
				<td class="header-title">
					<span>&#160;</span>
				</td>
			</tr>
		</table>
		<table height="27px" width="100%" class="top_menu">
		  <tr>
			<td nowrap="nowrap" style="padding-right: 20px; padding-right: 5px; font-weight: bold">
			  <xsl:call-template name="topBarLocationTranslation" />
			  <xsl:text>&#160;</xsl:text>
			</td>
			<td width="100%" align="center" nowrap="nowrap" style="padding-right: 20px; font-weight: bold">
			  <xsl:choose>
				<xsl:when test="count($miogacontext/application) != 0 and $miogacontext/application/name != ''">
				  <xsl:value-of select="$miogacontext/application/name"/>
				</xsl:when>
				<xsl:when test="count($miogacontext/currenturi) != 0">
				  <xsl:call-template name="IdentToDesc">
					<xsl:with-param name="ident"><xsl:value-of select="$miogacontext/currenturi" /></xsl:with-param>
					<xsl:with-param name="noicon">1</xsl:with-param>
				  </xsl:call-template>						
				</xsl:when>
				<xsl:otherwise>&#160;</xsl:otherwise>
			  </xsl:choose>
			</td>
			
			<td nowrap="nowrap" style="padding-right: 20px; font-size: small">
			  <xsl:for-each select="$mioga_context/time">
				<xsl:call-template name="LocalDateAbr"/>
				<xsl:text>&#160;-&#160;</xsl:text>
				<xsl:call-template name="LocalTime">
				  <xsl:with-param name="nosec">1</xsl:with-param>
				</xsl:call-template>
			  </xsl:for-each>
			</td>

			<xsl:if test="count($mioga_context/history/referer) != 0">
				<td class="shortcut-icon" nowrap="nowrap" style="padding-right: 5px">
					<a href="{$mioga_context/history/referer}">
						<img src="{$image_uri}/16x16/actions/go-previous.png" border="0">
							<xsl:attribute name="alt">
								<xsl:call-template name="shortcutTranslation">
									<xsl:with-param name="type">back-history</xsl:with-param>
								</xsl:call-template>
							</xsl:attribute>
							<xsl:attribute name="title">
								<xsl:call-template name="shortcutTranslation">
									<xsl:with-param name="type">back-history</xsl:with-param>
								</xsl:call-template>
							</xsl:attribute>
						</img>
					</a>
				</td>
			</xsl:if>


			<xsl:variable name="home_url">
				<xsl:choose>
					<xsl:when test="count($miogacontext/user/home_url) = 0 or $miogacontext/user/home_url = ''"><xsl:value-of select="$base_uri"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="$miogacontext/user/home_url"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<td class="shortcut-icon" nowrap="nowrap" style="padding-right: 5px">
			  <a href="{$home_url}" target="_top">
				<img src="{$image_uri}/16x16/actions/go-home.png" border="0">
				  <xsl:attribute name="alt">
					<xsl:call-template name="shortcutTranslation">
					  <xsl:with-param name="type">default-page</xsl:with-param>
					</xsl:call-template>
				  </xsl:attribute>
				  <xsl:attribute name="title">
					<xsl:call-template name="shortcutTranslation">
					  <xsl:with-param name="type">default-page</xsl:with-param>
					</xsl:call-template>
				  </xsl:attribute>
				</img>
			  </a>
			</td>
			
			<td class="shortcut-icon" nowrap="nowrap" style="padding-right: 5px">
			  <a href="{$bin_uri}/__MIOGA-USER__/Organizer/DisplayMain" >
				<img src="{$image_uri}/16x16/apps/organizer.png" border="0">
				  <xsl:attribute name="alt">
					<xsl:call-template name="shortcutTranslation">
					  <xsl:with-param name="type">agenda</xsl:with-param>
					</xsl:call-template>
				  </xsl:attribute>
				  <xsl:attribute name="title">
					<xsl:call-template name="shortcutTranslation">
					  <xsl:with-param name="type">agenda</xsl:with-param>
					</xsl:call-template>
				  </xsl:attribute>
				</img>
			  </a>
			</td>
			
			<td class="shortcut-icon" nowrap="nowrap" style="padding-right: 5px">
			  <a href="{$bin_uri}/__MIOGA-USER__/Magellan/DisplayMain" >
				<img src="{$image_uri}/16x16/apps/file-manager.png" border="0">
				  <xsl:attribute name="alt">
					<xsl:call-template name="shortcutTranslation">
					  <xsl:with-param name="type">my-files</xsl:with-param>
					</xsl:call-template>
				  </xsl:attribute>
				  <xsl:attribute name="title">
					<xsl:call-template name="shortcutTranslation">
					  <xsl:with-param name="type">my-files</xsl:with-param>
					</xsl:call-template>
				  </xsl:attribute>
				</img>
			  </a>
			</td>
			
			<xsl:if test="count(//miogacontext/feeds/feed) &gt; 0">
				<td class="shortcut-icon" nowrap="nowrap" style="padding-right: 5px">
					<a title="{//miogacontext/feeds/feed[1]}" href="{//miogacontext/feeds/feed[1]/link}" >
						<img alt="{//miogacontext/feeds/feed[1]}" src="{$image_uri}/16x16/apps/rss.png" border="0"/>
					</a>
				</td>
			</xsl:if>
			
			<xsl:if test="$help != ''">
				<td class="shortcut-icon" nowrap="nowrap" style="padding-right: 5px">
					<a href="{$help_uri}/{$mioga_context/group/lang}/{$help}" target="_new">
						<img src="{$image_uri}/16x16/actions/help-contents.png" border="0">
							<xsl:attribute name="alt">
								<xsl:call-template name="shortcutTranslation">
									<xsl:with-param name="type">help</xsl:with-param>
								</xsl:call-template>
							</xsl:attribute>
							<xsl:attribute name="title">
								<xsl:call-template name="shortcutTranslation">
									<xsl:with-param name="type">help</xsl:with-param>
								</xsl:call-template>
							</xsl:attribute>
						</img>
					</a>
				</td>
			</xsl:if>
			
			<xsl:if test="//miogacontext/authen_web">
				<xsl:if test="$logout_uri != ''">
					<td class="shortcut-icon" nowrap="nowrap" style="padding-right: 5px">
					  <a href="{$logout_uri}">
						<img src="{$image_uri}/16x16/actions/logout.png" border="0">
						  <xsl:attribute name="alt">
							<xsl:call-template name="shortcutTranslation">
							  <xsl:with-param name="type">logout</xsl:with-param>
							</xsl:call-template>
						  </xsl:attribute>
						  <xsl:attribute name="title">
							<xsl:call-template name="shortcutTranslation">
							  <xsl:with-param name="type">logout</xsl:with-param>
							</xsl:call-template>
						  </xsl:attribute>
						</img>
					  </a>
					</td>
				</xsl:if>
			</xsl:if>
		  </tr>
		</table>
	</div>
    </xsl:if>
  </xsl:template>

<!-- =================================
	 Private tempaltes
	 ================================= -->

<!-- =================================
	 HTML document root
	 
	 Handle the / element, print html header (css links, <head>, and <title>)
	 and run apply-templates on root elements.
	 
	 ================================= -->

<xsl:template name="mioga-css">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/mioga.css" media="screen" />
</xsl:template>

<xsl:template name="theme-css">
	<xsl:call-template name="ie-hacks"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/theme.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/theme_print.css" media="print"/>
</xsl:template>

<xsl:template name="mioga-js">
	<script type="text/javascript" src="{$theme_uri}/javascript/mioga.js"></script>
	<xsl:variable name="is_public">
		<xsl:choose>
			<xsl:when test="//miogacontext/is_public = 1">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<script type="text/javascript">
		var mioga_context = {
				  base_uri          : "<xsl:value-of select="$base_uri" />",
				  bin_uri           : "<xsl:value-of select="$bin_uri" />",
				  public_bin_uri    : "<xsl:value-of select="$public_bin_uri" />",
				  private_bin_uri   : "<xsl:value-of select="$private_bin_uri" />",
				  user_bin_uri      : "<xsl:value-of select="$user_bin_uri" />",
				  public_dav_uri    : "<xsl:value-of select="$public_dav_uri" />",
				  private_dav_uri   : "<xsl:value-of select="$private_dav_uri" />",
				  public_dav        : "<xsl:value-of select="$public_dav" />",
				  private_dav       : "<xsl:value-of select="$private_dav" />",
				  logout_uri        : "<xsl:value-of select="$logout_uri" />",
				  cdn_uri           : "<xsl:value-of select="$cdn_uri" />",
				  jslib_uri         : "<xsl:value-of select="$jslib_uri" />",
				  theme_uri         : "<xsl:value-of select="$theme_uri" />",
				  image_uri         : "<xsl:value-of select="$image_uri" />",
				  help_uri          : "<xsl:value-of select="$help_uri" />",
				  mioga_version     : "<xsl:value-of select="$mioga_version" />",
				  application       : {
					  name          : "<xsl:value-of select="$mioga_application" />",
					  method        : "<xsl:value-of select="$mioga_method" />",
					  is_public     : <xsl:value-of select="$is_public"/>
				  },
				  instance          : {
					  ident         : "<xsl:value-of select="//miogacontext/instance/ident"/>"
				  }
		};

		<!-- User-specific data, if connected user -->
		<xsl:if test="//miogacontext/user">
			mioga_context.user = {
				rowid:     <xsl:value-of select="//miogacontext/user/rowid"/>,
				ident:     "<xsl:value-of select="//miogacontext/user/ident"/>",
				firstname: "<xsl:value-of select="//miogacontext/user/firstname"/>",
				lastname:  "<xsl:value-of select="//miogacontext/user/lastname"/>",
				name:      "<xsl:value-of select="//miogacontext/user/name"/>",
				email:     "<xsl:value-of select="//miogacontext/user/email"/>",
				lang:      "<xsl:value-of select="//miogacontext/user/lang"/>"
			};
			mioga_context.rights = {
				<xsl:apply-templates select="//UserRights/*"/>
			};
		</xsl:if>

		<!-- Group-specific data -->
		<xsl:if test="//miogacontext/group">
			mioga_context.group = {
				ident: "<xsl:value-of select="//miogacontext/group/ident"/>",
				type: "<xsl:value-of select="//miogacontext/group/type"/>",
				lang: "<xsl:value-of select="//miogacontext/group/lang"/>",
				theme: "<xsl:value-of select="//miogacontext/group/theme"/>"
			};
		</xsl:if>
	</script>
</xsl:template>

<!-- Function access rights -->
<xsl:template match="//UserRights/*">
	<xsl:variable name="active"><xsl:choose><xsl:when test=". = 1">true</xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose></xsl:variable>
	<xsl:value-of select="name (.)"/>: <xsl:value-of select="$active"/><xsl:if test="not (position () = last ())">,</xsl:if>
</xsl:template>
<xsl:template name="console-js">
	<script type="text/javascript">
		if (!window.console) window.console = {};
		if (!window.console.log) window.console.log = function () {};
		if (!window.console.dir) window.console.dir = function () {};
		if (!window.console.debug) window.console.debug = function () {};
	</script>
</xsl:template>

<xsl:template name="objectkeys-js">
	<script type="text/javascript">
		Object.keys = Object.keys || function(o) {
			var result = [];
			for(var name in o) {
				if (o.hasOwnProperty(name))
					result.push(name);
			}
			return result;
		};
	</script>
</xsl:template>

<xsl:template name="calendar-js">
	<script type="text/javascript" src="{$theme_uri}/javascript/AnchorPosition.js"></script>
	<script type="text/javascript" src="{$theme_uri}/javascript/date.js"></script>
	<script type="text/javascript" src="{$theme_uri}/javascript/CalendarPopup.js"></script>
	<script type="text/javascript" src="{$theme_uri}/javascript/PopupWindow.js"></script>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/calendar_popup.css" media="screen" />
</xsl:template>


<xsl:template name="doctype">
	<xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
</xsl:template>

<xsl:template name="favicon">
	<link rel="SHORTCUT ICON" href="{$image_uri}/favicon.gif"/>
</xsl:template>

<xsl:template name="title">
    <meta name="mioga2-version" content="{$mioga_version}"/>
    <xsl:for-each select="//miogacontext/feeds/feed">
        <link rel="alternate" type="application/atom+xml" title="{name}" href="{link}" />
    </xsl:for-each>
	<title>Mioga - <xsl:value-of select="//application/name"/> - <xsl:value-of select="//application/description"/></title>
</xsl:template>

<xsl:template match="/">
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="console-js"/>
	<xsl:call-template name="calendar-js"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="theme-css"/>
</head>
	<xsl:apply-templates />
</html>
</xsl:template>

<!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	 TEMPLATE TO REWRITE

	 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->



<!-- =================================
	 Locale File Size Printing	 
	 ================================= -->

<xsl:template match="filesize" name="filesize">

<xsl:choose>
	<xsl:when test="./@mult='1'"><xsl:value-of select="." />&#160;<xsl:value-of select="mioga:gettext('b')"/></xsl:when>
	<xsl:when test="./@mult='k'"><xsl:value-of select="." />&#160;<xsl:value-of select="mioga:gettext('kb')"/></xsl:when>
	<xsl:when test="./@mult='M'"><xsl:value-of select="." />&#160;<xsl:value-of select="mioga:gettext('Mb')"/></xsl:when>
	<xsl:when test="./@mult='G'"><xsl:value-of select="." />&#160;<xsl:value-of select="mioga:gettext('Gb')"/></xsl:when>
	<xsl:otherwise></xsl:otherwise>
</xsl:choose>

</xsl:template>


<!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	 DEPRECATED STYLES. MUST NOT BE USED ANYMORE

	 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->




<!-- =================================
	 simple_table

	 Table without border, spacing, padding.
	 
	 ================================= -->

<xsl:attribute-set name="simple_table">
	<xsl:attribute name="border">0</xsl:attribute>
	<xsl:attribute name="cellpadding">0</xsl:attribute>
	<xsl:attribute name="cellspacing">0</xsl:attribute>
</xsl:attribute-set>

<!-- =================================
	 simple_table_100 (width = 100%)

	 Table without border, spacing, padding 
	 and 100% width
	 
	 ================================= -->

<xsl:attribute-set name="simple_table_100">
	<xsl:attribute name="border">0</xsl:attribute>
	<xsl:attribute name="cellpadding">0</xsl:attribute>
	<xsl:attribute name="cellspacing">0</xsl:attribute>
	<xsl:attribute name="width">100%</xsl:attribute>
</xsl:attribute-set>


<!-- =================================
	 border_table
	 ================================= -->

<xsl:attribute-set name="border_table">
	<xsl:attribute name="border">0</xsl:attribute>
	<xsl:attribute name="cellpadding">0</xsl:attribute>
	<xsl:attribute name="cellspacing">0</xsl:attribute>
	<xsl:attribute name="class"><xsl:value-of select="$mioga-list-title-bg-color"/></xsl:attribute>
</xsl:attribute-set>



<!-- =================================
	 content_table
	 ================================= -->

<xsl:attribute-set name="content_table">
	<xsl:attribute name="border">0</xsl:attribute>
	<xsl:attribute name="cellpadding">3</xsl:attribute>
	<xsl:attribute name="cellspacing">1</xsl:attribute>
	<xsl:attribute name="class"><xsl:value-of select="$mioga-list-title-bg-color"/></xsl:attribute>
 	<xsl:attribute name="width">100%</xsl:attribute>   
</xsl:attribute-set>


<!-- =================================
	 dialog template
	 ================================= -->

<xsl:template name="dialog">
    <xsl:param name="dialog_width"></xsl:param>
<table xsl:use-attribute-sets="border_table">
<xsl:if test="$dialog_width!=''">
	<xsl:attribute name="width"><xsl:value-of select="$dialog_width" /></xsl:attribute>
</xsl:if>
<tr>
	<td>
		<table xsl:use-attribute-sets="content_table">
		<tr class="mioga-box-background">
			<td>
				<xsl:apply-templates select="."  mode="body" />
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</xsl:template>



<!-- =================================
	 simple form template
	 ================================= -->

<xsl:template name="simple-form">
	<xsl:param name="name"/>
	<xsl:param name="action"/>
	<xsl:param name="method"/>
	<xsl:param name="enctype"/>
	
	<form method="{$method}" name="{$action}">
		<xsl:if test="$enctype">
			<xsl:attribute name="enctype"><xsl:value-of select="$enctype"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$action">
			<xsl:attribute name="action"><xsl:value-of select="$action"/></xsl:attribute>
		</xsl:if>



		<table  cellspacing="0" >
			<tr>
				<td colspan="2" nowrap="nowrap">
					<font class="{$mioga-title-color}">
						<xsl:value-of select="$name"/>
					</font>
				</td>
			</tr>
			
			<tr>
				<td colspan="2">&#160;</td>
			</tr>
			
			<xsl:apply-templates select="."  mode="form-body"/>
			
			<tr>
				<td colspan="2">&#160;</td>
			</tr>

			<xsl:apply-templates select="."  mode="form-button"/>

	   </table>
	</form>
</xsl:template>




<!-- =================================
	 table template
	 ================================= -->

<xsl:template name="table">
	<xsl:param name="cellspacing">1</xsl:param>
	<xsl:param name="cellpadding">3</xsl:param>
	<xsl:param name="table_width">100%</xsl:param>
	<xsl:param name="class"></xsl:param>
	<table xsl:use-attribute-sets="border_table">
		<xsl:if test="$class!=''">
			<xsl:attribute name="class"><xsl:value-of select="$class"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$table_width!=''">
			<xsl:attribute name="width"><xsl:value-of select="$table_width" /></xsl:attribute>
		</xsl:if>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="{$cellspacing}" cellpadding="{$cellpadding}">
					<tr class="mioga-box-title">
						<xsl:apply-templates select="."  mode="table-title"/>
					</tr>
					<xsl:apply-templates select="."  mode="table-body"/>
				</table>
			</td>
		</tr>
	</table>
</xsl:template>


<!-- =================================
	 small-form template
	 ================================= -->

<xsl:template name="small-form">
	<xsl:param name="name"/>
	<xsl:param name="action"/>
	<xsl:param name="method"/>
	<xsl:param name="enctype"/>
 
	<form method="{$method}" name="{$action}">
		<xsl:if test="$enctype">
			<xsl:attribute name="enctype"><xsl:value-of select="$enctype"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$action">
			<xsl:attribute name="action"><xsl:value-of select="$action"/></xsl:attribute>
		</xsl:if>


		<table xsl:use-attribute-sets="mioga-form-table" cellspacing="0">
			<tr>
				<td colspan="2" width="100%">
					<font class="{$mioga-title-color}">
						<xsl:value-of select="$name"/>
					</font>
				</td>
			</tr>
			
			
			<tr>
				<td align="center">
					 <xsl:apply-templates select="."  mode="form-body"/>
		</td>
				<td align="center">
					 <xsl:apply-templates select="."  mode="form-button"/>
		</td>
			</tr>

<!--			<tr>
				<td colspan="2" width="100%">&#160;</td>
			</tr> -->
			
	   </table>
	</form>
</xsl:template>

  <!-- ====================
	   == FR TRANSLATION ==
	   ==================== -->

  <xsl:template name="topBarLocationTranslation">
	<xsl:choose>
		<xsl:when test="//miogacontext/group/type = 'group'"><xsl:value-of select="mioga:gettext('%s work group', string(//miogacontext/group/ident))"/></xsl:when>
	  <xsl:when test="//miogacontext/group/type = 'resource'"><xsl:value-of select="mioga:gettext('%s resource', string(//miogacontext/group/ident))"/></xsl:when>
	  <xsl:otherwise><xsl:value-of select='mioga:gettext("%s&apos;s workspace", string(//miogacontext/group/ident))'/></xsl:otherwise>
	</xsl:choose>
  </xsl:template>

  <!-- Common MiogaII Values Translation -->
  <xsl:template name="mioga2-common-value-translation">
	<xsl:param name="name"/>
	<xsl:param name="value"/>
	
	<xsl:choose>
	  <xsl:when test="$name = 'status'">
		<xsl:choose>
		  <xsl:when test="$value = 'active'"><xsl:value-of select="mioga:gettext('Active')"/></xsl:when>
		  <xsl:when test="$value = 'deleted_not_purged'"><xsl:value-of select="mioga:gettext('Del. not purged')"/></xsl:when>
		  <xsl:when test="$value = 'purged'"><xsl:value-of select="mioga:gettext('Destroyed')"/></xsl:when>
		  <xsl:when test="$value = 'deleted'"><xsl:value-of select="mioga:gettext('Deleted')"/></xsl:when>
		  <xsl:when test="$value = 'disabled'"><xsl:value-of select="mioga:gettext('Disabled')"/></xsl:when>
		  <xsl:when test="$value = 'disabled_attacked'"><xsl:value-of select="mioga:gettext('Disabled auth. fail')"/></xsl:when>
		</xsl:choose>
	  </xsl:when>
	  
	  <xsl:when test="$name = 'type'">
		<xsl:choose>
		  <xsl:when test="$value = 'local_user'"><xsl:value-of select="mioga:gettext('Local')"/></xsl:when>
		  <xsl:when test="$value = 'ldap_user'"><xsl:value-of select="mioga:gettext('LDAP')"/></xsl:when>
		  <xsl:when test="$value = 'external_user'"><xsl:value-of select="mioga:gettext('External')"/></xsl:when>
		</xsl:choose>
	  </xsl:when>
	  
	  <xsl:when test="$name = 'usable_by_resource' or $name = 'can_be_public'">
		<xsl:choose>
		  <xsl:when test="$value = 1"><xsl:value-of select="mioga:gettext('Yes')"/></xsl:when>
		  <xsl:otherwise><xsl:value-of select="mioga:gettext('No')"/></xsl:otherwise>
		</xsl:choose>
	  </xsl:when>
	  
	  <xsl:when test="$name = 'all_groups'">
		<xsl:choose>
		  <xsl:when test="$value &gt; 0"><xsl:value-of select="mioga:gettext('All')"/></xsl:when>
		  <xsl:otherwise><xsl:value-of select="mioga:gettext('View list')"/></xsl:otherwise>
		</xsl:choose>
	  </xsl:when>
	  
	  <xsl:when test="$name = 'all_users'">
		<xsl:choose>
		  <xsl:when test="$value &gt; 0"><xsl:value-of select="mioga:gettext('All')"/></xsl:when>
		  <xsl:otherwise><xsl:value-of select="mioga:gettext('View list')"/></xsl:otherwise>
		</xsl:choose>
	  </xsl:when>
		
	  <xsl:otherwise>
		<xsl:value-of select="$value"/>
	  </xsl:otherwise>
	</xsl:choose>
  </xsl:template>

  <!-- top bar shortcut translation -->
  <xsl:template name="shortcutTranslation">
	<xsl:param name="type" />

	<xsl:choose>
	  <xsl:when test="$type='default-page'"><xsl:value-of select="mioga:gettext('Reload my home page')"/></xsl:when>
	  <xsl:when test="$type='agenda'"><xsl:value-of select="mioga:gettext('My calendar')"/></xsl:when>
	  <xsl:when test="$type='my-files'"><xsl:value-of select="mioga:gettext('My files')"/></xsl:when>
	  <xsl:when test="$type='add-application'"><xsl:value-of select="mioga:gettext('Add current application to my links')"/></xsl:when>
	  <xsl:when test="$type='help'"><xsl:value-of select="mioga:gettext('Help')"/></xsl:when>
	  <xsl:when test="$type='logout'"><xsl:value-of select="mioga:gettext('Logout')"/></xsl:when>
	</xsl:choose>
  </xsl:template>

  
  <!-- Common MiogaII fields name translation -->
  <xsl:template name="mioga2-common-name-translation">
	<xsl:param name="name"/>
	
	<xsl:choose>
	  <xsl:when test="$name = 'server'"><xsl:value-of select="mioga:gettext('Server')"/></xsl:when>
	  <xsl:when test="$name = 'path'"><xsl:value-of select="mioga:gettext('Folder')"/></xsl:when>
	  <xsl:when test="$name = 'change_app_status'"><xsl:value-of select="mioga:gettext('Active')"/></xsl:when>
	  <xsl:when test="$name = 'anim_ident' or $name='anim_id'"><xsl:value-of select="mioga:gettext('Animator')"/></xsl:when>
	  <xsl:when test="$name = 'user_name'"><xsl:value-of select="mioga:gettext('Name')"/></xsl:when>
	  <xsl:when test="contains($name, 'ident')"><xsl:value-of select="mioga:gettext('Ident')"/></xsl:when>
	  <xsl:when test="$name = 'resource_location' or $name= 'location'"><xsl:value-of select="mioga:gettext('Location')"/></xsl:when>
	  <xsl:when test="$name = 'description'"><xsl:value-of select="mioga:gettext('Description')"/></xsl:when>
	  <xsl:when test="$name = 'private'"><xsl:value-of select="mioga:gettext('Private')"/></xsl:when>
	  <xsl:when test="$name = 'public_part'"><xsl:value-of select="mioga:gettext('Public space')"/></xsl:when>
	  <xsl:when test="$name = 'history'"><xsl:value-of select="mioga:gettext('Files history')"/></xsl:when>
	  <xsl:when test="$name = 'name'"><xsl:value-of select="mioga:gettext('Name')"/></xsl:when>
	  <xsl:when test="contains($name, 'email')"><xsl:value-of select="mioga:gettext('Email')"/></xsl:when>
	  <xsl:when test="contains($name, 'firstname')"><xsl:value-of select="mioga:gettext('Firstname')"/></xsl:when>
	  <xsl:when test="contains($name, 'lastname')"><xsl:value-of select="mioga:gettext('Lastname')"/></xsl:when>
	  <xsl:when test="contains($name, 'disk_space_used')"><xsl:value-of select="mioga:gettext('Size')"/></xsl:when>
	  <xsl:when test="$name = 'status'"><xsl:value-of select="mioga:gettext('Status')"/></xsl:when>
	  <xsl:when test="$name = 'type'"><xsl:value-of select="mioga:gettext('Type')"/></xsl:when>
	  <xsl:when test="$name = 'autonomous'"><xsl:value-of select="mioga:gettext('Autonomous')"/></xsl:when>
	  <xsl:when test="$name = 'timezone'"><xsl:value-of select="mioga:gettext('Time zone')"/></xsl:when>
	  <xsl:when test="$name = 'time_per_day'"><xsl:value-of select="mioga:gettext('Daily worked time')"/></xsl:when>
	  <xsl:when test="$name = 'working_days'"><xsl:value-of select="mioga:gettext('Worked days')"/></xsl:when>
	  <xsl:when test="$name = 'monday_first'"><xsl:value-of select="mioga:gettext('First day of week')"/></xsl:when>
	  <xsl:when test="$name = 'new_password_1'"><xsl:value-of select="mioga:gettext('New password')"/></xsl:when>
	  <xsl:when test="$name = 'new_password_2'"><xsl:value-of select="mioga:gettext('New password (confirmation)')"/></xsl:when>
	  <xsl:when test="$name = 'password'"><xsl:value-of select="mioga:gettext('Password')"/></xsl:when>
	  <xsl:when test="$name = 'password2'"><xsl:value-of select="mioga:gettext('Password (confirmation)')"/></xsl:when>
	  <xsl:when test="$name ='user_password_2'"><xsl:value-of select="mioga:gettext('Password (confirmation)')"/></xsl:when>
	  <xsl:when test="contains($name, 'password')"><xsl:value-of select="mioga:gettext('Password')"/></xsl:when>
	  <xsl:when test="$name = 'public_key'"><xsl:value-of select="mioga:gettext('Public key file')"/></xsl:when>
	  <xsl:when test="$name = 'public_key_file'"><xsl:value-of select="mioga:gettext('Public key file')"/></xsl:when>
	  <xsl:when test="$name = 'private_key_file'"><xsl:value-of select="mioga:gettext('Private key file')"/></xsl:when>
	  <xsl:when test="$name = 'usable_by_resource'"><xsl:value-of select="mioga:gettext('Resource')"/></xsl:when>
	  <xsl:when test="$name = 'can_be_public'"><xsl:value-of select="mioga:gettext('Public')"/></xsl:when>
	  <xsl:when test="$name = 'all_groups'"><xsl:value-of select="mioga:gettext('Groups')"/></xsl:when>
	  <xsl:when test="$name = 'all_users'"><xsl:value-of select="mioga:gettext('Users')"/></xsl:when>
	  <xsl:when test="$name = 'last_connection'"><xsl:value-of select="mioga:gettext('Last connection')"/></xsl:when>
      <xsl:when test="$name = 'rights'"><xsl:value-of select="mioga:gettext('Rights')"/></xsl:when>
	  <xsl:when test="$name = 'profile'"><xsl:value-of select="mioga:gettext('Profile')"/></xsl:when>
	  <xsl:when test="$name = 'is_public'"><xsl:value-of select="mioga:gettext('Public')"/></xsl:when>
	  <xsl:when test="$name = 'resize'"><xsl:value-of select="mioga:gettext('Resizable menu')"/></xsl:when>
	  <xsl:when test="$name = 'skeleton'"><xsl:value-of select="mioga:gettext('Model')"/></xsl:when>
	</xsl:choose>
  </xsl:template>

  <!-- LocalDate - Print full length date in locale format -->
  <xsl:template name="LocalDate">
	<xsl:param name="day" select="./day"/>
	<xsl:param name="month" select="./month"/>
	<xsl:param name="year" select="./year"/>

	<!-- day -->
	<xsl:call-template name="LocalDayOfWeek">
	  <xsl:with-param name="daynum" select="./wday"/>
	</xsl:call-template>
	<xsl:text>&#160;</xsl:text>
	<!-- day number -->
	<xsl:value-of select="$day"/>
	<xsl:text>&#160;</xsl:text>
	<!-- month -->
	<xsl:call-template name="LocalMonth">
	  <xsl:with-param name="monthnum" select="$month"/>
	</xsl:call-template>
	<xsl:text>&#160;</xsl:text>
	<!-- year -->
	<xsl:value-of select="$year"/>
  </xsl:template>

  <!-- LocalDayOfWeek - Print full length day of week name -->
  <xsl:template name="LocalDayOfWeek">
	<xsl:param name="daynum"/>
	
	<xsl:choose>
	  <xsl:when test="$daynum=0"><xsl:value-of select="mioga:gettext('Sunday')"/></xsl:when>
	  <xsl:when test="$daynum=1"><xsl:value-of select="mioga:gettext('Monday')"/></xsl:when>
	  <xsl:when test="$daynum=2"><xsl:value-of select="mioga:gettext('Tuesday')"/></xsl:when>
	  <xsl:when test="$daynum=3"><xsl:value-of select="mioga:gettext('Wednesday')"/></xsl:when>
	  <xsl:when test="$daynum=4"><xsl:value-of select="mioga:gettext('Thursday')"/></xsl:when>
	  <xsl:when test="$daynum=5"><xsl:value-of select="mioga:gettext('Friday')"/></xsl:when>
	  <xsl:when test="$daynum=6"><xsl:value-of select="mioga:gettext('Saturday')"/></xsl:when>
	</xsl:choose>
  </xsl:template>

  <!-- LocalMonth - Print full length month name -->
  <xsl:template name="LocalMonth">
	<xsl:param name="monthnum"/>
	
	<xsl:choose>
	  <xsl:when test="$monthnum=1"><xsl:value-of select="mioga:gettext('January')"/></xsl:when>
	  <xsl:when test="$monthnum=2"><xsl:value-of select="mioga:gettext('February')"/></xsl:when>
	  <xsl:when test="$monthnum=3"><xsl:value-of select="mioga:gettext('March')"/></xsl:when>
	  <xsl:when test="$monthnum=4"><xsl:value-of select="mioga:gettext('April')"/></xsl:when>
	  <xsl:when test="$monthnum=5"><xsl:value-of select="mioga:gettext('May')"/></xsl:when>
	  <xsl:when test="$monthnum=6"><xsl:value-of select="mioga:gettext('June')"/></xsl:when>
	  <xsl:when test="$monthnum=7"><xsl:value-of select="mioga:gettext('July')"/></xsl:when>
	  <xsl:when test="$monthnum=8"><xsl:value-of select="mioga:gettext('August')"/></xsl:when>
	  <xsl:when test="$monthnum=9"><xsl:value-of select="mioga:gettext('September')"/></xsl:when>
	  <xsl:when test="$monthnum=10"><xsl:value-of select="mioga:gettext('October')"/></xsl:when>
	  <xsl:when test="$monthnum=11"><xsl:value-of select="mioga:gettext('November')"/></xsl:when>
	  <xsl:when test="$monthnum=12"><xsl:value-of select="mioga:gettext('December')"/></xsl:when>
	</xsl:choose>
  </xsl:template>
 
  <!-- LocalDayOfWeekAbr - Print abreviated day of week name -->
  <xsl:template name="LocalDayOfWeekAbr">
	<xsl:param name="daynum"/>
	
	<xsl:choose>
	  <xsl:when test="$daynum=0"><xsl:value-of select="mioga:gettext('Sun')"/></xsl:when>
	  <xsl:when test="$daynum=1"><xsl:value-of select="mioga:gettext('Mon')"/></xsl:when>
	  <xsl:when test="$daynum=2"><xsl:value-of select="mioga:gettext('Tue')"/></xsl:when>
	  <xsl:when test="$daynum=3"><xsl:value-of select="mioga:gettext('Wed')"/></xsl:when>
	  <xsl:when test="$daynum=4"><xsl:value-of select="mioga:gettext('Thu')"/></xsl:when>
	  <xsl:when test="$daynum=5"><xsl:value-of select="mioga:gettext('Fri')"/></xsl:when>
	  <xsl:when test="$daynum=6"><xsl:value-of select="mioga:gettext('Sat')"/></xsl:when>
	</xsl:choose>
  </xsl:template>

  <!-- LocalMonth - Print abreviated month name -->
  <xsl:template name="LocalMonthAbr">
	<xsl:param name="monthnum"/>
	
	<xsl:choose>
	  <xsl:when test="$monthnum=1"><xsl:value-of select="mioga:gettext('Jan')"/></xsl:when>
	  <xsl:when test="$monthnum=2"><xsl:value-of select="mioga:gettext('Feb')"/></xsl:when>
	  <xsl:when test="$monthnum=3"><xsl:value-of select="mioga:gettext('Mar')"/></xsl:when>
	  <xsl:when test="$monthnum=4"><xsl:value-of select="mioga:gettext('Apr')"/></xsl:when>
	  <xsl:when test="$monthnum=5"><xsl:value-of select="mioga:gettext('May')"/></xsl:when>
	  <xsl:when test="$monthnum=6"><xsl:value-of select="mioga:gettext('Jun')"/></xsl:when>
	  <xsl:when test="$monthnum=7"><xsl:value-of select="mioga:gettext('Jul')"/></xsl:when>
	  <xsl:when test="$monthnum=8"><xsl:value-of select="mioga:gettext('Aug')"/></xsl:when>
	  <xsl:when test="$monthnum=9"><xsl:value-of select="mioga:gettext('Sep')"/></xsl:when>
	  <xsl:when test="$monthnum=10"><xsl:value-of select="mioga:gettext('Oct')"/></xsl:when>
	  <xsl:when test="$monthnum=11"><xsl:value-of select="mioga:gettext('Nov')"/></xsl:when>
	  <xsl:when test="$monthnum=12"><xsl:value-of select="mioga:gettext('Dec')"/></xsl:when>
	</xsl:choose>
  </xsl:template>

  <xsl:template name="errorTranslation">
	<xsl:param name="value"/>
	<xsl:param name="i18n-name"/>
	<xsl:param name="name"/>
	<xsl:param name="callback"/>

	<xsl:choose>
	  <xsl:when test=".='time'"><xsl:value-of select="mioga:gettext('Input time is not valid.')"/></xsl:when>
	  <xsl:when test=".='date'"><xsl:value-of select="mioga:gettext('Input date is not valid.')"/></xsl:when>
	  <xsl:when test=".='datetime'"><xsl:value-of select="mioga:gettext('Input date and time are not valid.')"/></xsl:when>
	  <xsl:when test=".='want_email'"><xsl:value-of select="$value"/>&#160;<xsl:value-of select="mioga:gettext('is not a valid email address')"/></xsl:when>
	  <xsl:when test=".='unused_email'"><xsl:value-of select="mioga:gettext('Email address is already used.')"/></xsl:when>
	  <xsl:when test=".='mioga_resource_id'"><xsl:value-of select="$value"/>&#160;<xsl:value-of select="mioga:gettext('is not a valid resource')"/></xsl:when>
	  <xsl:when test=".='mioga_user_id'"><xsl:value-of select="$value"/>&#160;<xsl:value-of select="mioga:gettext('is not a valid user')"/></xsl:when>
	  <xsl:when test=".='mioga_group_id'"><xsl:value-of select="$value"/>&#160;<xsl:value-of select="mioga:gettext('is not a valid group')"/></xsl:when>
	  <xsl:when test=".='valid_ident' and count(../../err[@arg=$name and ./type='disallow_empty']) = 0 ">
      <xsl:value-of select="mioga:gettext('&quot;%s&quot; is not a valid identifier.', string($value))"/>
		<br/>
    <xsl:value-of select="mioga:gettext('Identifier can only contain non-accented characters, numbers, - and _.')"/>
	  </xsl:when>
	  <xsl:when test=".='not_used_group_ident' or .='modify_group_ident'">
      <xsl:value-of select="mioga:gettext('User, group, team or resource called &quot;%s&quot; already exists.', string($value))"/>
	  </xsl:when>
	  <xsl:when test=".='disallow_empty'">
      <xsl:value-of select="mioga:gettext('&quot;%s&quot; field must be filled', string($i18n-name))"/>
    </xsl:when>
	  <xsl:when test=".='max'">
      <xsl:value-of select="mioga:gettext('&quot;%s&quot; field is too long', string($i18n-name))"/>
    </xsl:when>
	  <xsl:when test=".='incorrect_password'"><xsl:value-of select="mioga:gettext('Input passwords are invalid')"/></xsl:when>
	  <xsl:when test=".='file_already_exists'"><xsl:value-of select="mioga:gettext('File or folder with this name already exists')"/></xsl:when>
	  <xsl:when test=".='password_policy'">
	  	<xsl:value-of select="mioga:gettext('Password does not match security policy.')"/>
		&#160;
		<xsl:value-of select="mioga:gettext('Passwords should contain at least:')"/>
		<div align="left">
			<ul>
				<li><xsl:value-of select="mioga:gettext('%d characters,', string(../../../PasswordPolicy/Length))"/></li>
				<li><xsl:value-of select="mioga:gettext('%d letters,', string(../../../PasswordPolicy/Letters))"/></li>
				<li><xsl:value-of select="mioga:gettext('%d digits,', string(../../../PasswordPolicy/Digits))"/></li>
				<li><xsl:value-of select="mioga:gettext('%d special characters,', string(../../../PasswordPolicy/Special))"/></li>
				<li><xsl:value-of select="mioga:gettext('%d case changes.', string(../../../PasswordPolicy/ChCase))"/></li>
			</ul>
		</div>
	  </xsl:when>
	  <xsl:otherwise>
		  <xsl:apply-templates select="$callback" mode="arg-checker-unknown-args">
			  <xsl:with-param name="name" select="."/>
			  <xsl:with-param name="arg" select="$name"/>
		  </xsl:apply-templates>
	  </xsl:otherwise>
	</xsl:choose>
  </xsl:template>

<!-- ======================
	 ==				     ==
	 == MIOGA COMPONENTS ==
	 ==				     ==
	 ====================== -->

<!-- ===============================
	error-message
	================================ -->

<xsl:template name="error-message">
	<xsl:param name="type">none</xsl:param>
	<xsl:choose>
		<xsl:when test="$type = 'disallow_empty'">
			<p class="error"><xsl:value-of select="mioga:gettext('Field must not be empty')"/></p>
		</xsl:when>
        <xsl:when test="$type = 'already_exists'">
            <p class="error"><xsl:value-of select="mioga:gettext('Field already exists')"/></p>
        </xsl:when>
        <xsl:when test="$type = 'choose_option'">
            <p class="error"><xsl:value-of select="mioga:gettext('Please make a choice')"/></p>
        </xsl:when>
        <xsl:when test="$type = 'want_percent'">
            <p class="error"><xsl:value-of select="mioga:gettext('Invalid percent value')"/></p>
        </xsl:when>
        <xsl:when test="$type = 'want_int'">
            <p class="error"><xsl:value-of select="mioga:gettext('Please provide an integer value.')"/></p>
        </xsl:when>
        <xsl:when test="$type = 'want_float'">
            <p class="error"><xsl:value-of select="mioga:gettext('Please provide a floating value.')"/></p>
        </xsl:when>
		<xsl:otherwise>
			<p class="error"><xsl:value-of select="mioga:gettext('Unknown error code')"/></p>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- ===============================
	Linebreak converter
	================================ -->

<!-- Substitute \n to <br/> -->
<xsl:template name="ConvertLinebreaks">
	<xsl:param name="string"/>
	<xsl:param name="from">\n</xsl:param>
	<xsl:param name="to">
		<br />
	</xsl:param>
	<xsl:choose>
		<xsl:when test="contains($string, $from)">
			<xsl:value-of select="substring-before($string, $from)"/>
			<xsl:copy-of select="$to"/>
			<xsl:call-template name="ConvertLinebreaks">
				<xsl:with-param name="string" select="substring-after($string, $from)" />
				<xsl:with-param name="from" select="$from" />
				<xsl:with-param name="to" select="$to" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$string" />
		</xsl:otherwise>
	</xsl:choose>
 </xsl:template>


<!-- ===============================
	Application navigator
	================================ -->
<xsl:template name="AppNav">
	<ul class="app-nav">
		<!-- Navigation through groups (is supported) -->
		<xsl:if test="count (appnav/group) != 0">
			<li>
                <xsl:call-template name="AppNav_Select"/>
			</li>
		</xsl:if>

		<!-- Navigation through application pages -->
		<xsl:for-each select="appnav/step">
			<li>
				<xsl:if test="position() != 1">
					<xsl:attribute name="class">chained</xsl:attribute>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="./url!=''">
						<a>
							<xsl:attribute name="href"><xsl:value-of select="./url"/></xsl:attribute>
							<xsl:value-of select="./label"/>
						</a>
					</xsl:when>
					<xsl:otherwise test="./url=''">
						<xsl:value-of select="./label"/>
					</xsl:otherwise>
				</xsl:choose>
			</li>
		</xsl:for-each>
	</ul>
</xsl:template>

<!-- Can be called by AppNav or stand-alone, to get just the SELECT -->
<xsl:template name="AppNav_Select">
  <xsl:param name="selected"><xsl:value-of select="miogacontext/group/ident"/></xsl:param>
  <select id="navigation_groups">
    <xsl:attribute name="title">
      <xsl:value-of select="mioga:gettext('Change group')"/>
    </xsl:attribute>
    <xsl:attribute name="onChange">navigation_groups();</xsl:attribute>
    <xsl:for-each select="appnav/group">
      <xsl:element name="option">
        <xsl:attribute name="value"><xsl:value-of select="./url" /></xsl:attribute>
        <xsl:if test="$selected=./ident">
          <xsl:attribute name="selected"/>
        </xsl:if>
        <xsl:value-of select="./ident" />
      </xsl:element>
    </xsl:for-each>
  </select>
</xsl:template>
    

<xsl:template name="CSRF-input">
	<input type="hidden" name="X-CSRF-Token"><xsl:attribute name="value"><xsl:value-of select="//miogacontext/csrftoken"/></xsl:attribute></input>
</xsl:template>

</xsl:stylesheet>
