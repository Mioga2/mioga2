<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:org="http://www.mioga2.org/portal/mioglets/Organizer" xmlns:mioga="urn:mioga">

<xsl:output method="html"/>

<!-- =================================
     Handle Calendar
     ================================= -->

<!-- Editor Form -->

<xsl:template match="org:Calendar" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorModeEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">org_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>
</xsl:template>


<!-- main Calendar template -->

<xsl:template match="org:Calendar">
    <xsl:param name="node"/>

    <div style="text-align: center;">
        <xsl:call-template name="MiogaReportMini">
            <xsl:with-param name="no-title">1</xsl:with-param>
            <xsl:with-param name="no-border">1</xsl:with-param>
            
            <xsl:with-param name="body">
                <xsl:call-template name="MiogaCalendar">
                    <xsl:with-param name="label">calendar</xsl:with-param>
                    <xsl:with-param name="base_href">
                        <xsl:value-of select="$bin_uri"/>/<xsl:choose>
                            <xsl:when test="$node/mode='group'"><xsl:value-of select="$mioga_context/group/ident"/></xsl:when>
                            <xsl:otherwise><xsl:value-of select="$mioga_context/user/ident"/></xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </div>
</xsl:template>


<!-- =================================
     Handle TaskList
     ================================= -->

<!-- Editor Form -->

<xsl:template match="org:TaskList" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorModeEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">org_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>
</xsl:template>


<!-- main TaskList template -->

<xsl:template match="org:TaskList">
    <xsl:param name="node"/>

    <table class="orgTaskList" cellpadding="5" cellspacing="0">
        <xsl:if test="count(task) = 0">
            <tr>
                <td>
                    <i><xsl:call-template name="org-no-task"/></i>
                </td>
            </tr>
        </xsl:if>

        <xsl:variable name="base_href">
            <xsl:choose>
                <xsl:when test="../mode = 'group'"><xsl:value-of select="$bin_uri"/>/<xsl:value-of select="$mioga_context/group/ident"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="$bin_uri"/>/<xsl:value-of select="$mioga_context/user/ident"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:for-each select="task">
            <tr valign="middle">

                <xsl:if test="$node/boxed">
					<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
							<xsl:otherwise>oddRow</xsl:otherwise>
						</xsl:choose>
				   </xsl:attribute>
                </xsl:if>

                <td class="datetime">
                    <xsl:choose>
                        <xsl:when test="start and is_planned = 1">
                            <xsl:call-template name="LocalTime">
                                <xsl:with-param name="nosec">1</xsl:with-param>
                                <xsl:with-param name="node" select="start"/>
                            </xsl:call-template>&#160;-&#160;
                            <xsl:call-template name="LocalTime">
                                <xsl:with-param name="nosec">1</xsl:with-param>
                                <xsl:with-param name="node" select="stop"/>
                            </xsl:call-template>&#160;:
                        </xsl:when>
                        <xsl:otherwise>&#160;</xsl:otherwise>
                    </xsl:choose>
                </td>

                <td class="name">
                    <xsl:value-of select="name"/>
                </td>

                <td class="task">
                    <xsl:variable name="href">
                        <xsl:choose>
                            <xsl:when test="type = 'periodic'"><xsl:value-of select="$base_href"/>/Organizer/EditPeriodicTask?taskrowid=<xsl:value-of select="rowid"/>&amp;mode=initial&amp;day=<xsl:value-of select="$mioga_context/time/day"/>&amp;month=<xsl:value-of select="$mioga_context/time/month"/>&amp;year=<xsl:value-of select="$mioga_context/time/year"/></xsl:when>
                            <xsl:otherwise><xsl:value-of select="$base_href"/>/Organizer/EditStrictTask?taskrowid=<xsl:value-of select="rowid"/>&amp;mode=initial</xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>

                    <xsl:variable name="org-other-label">
                        <xsl:call-template name="org-other-label"/>
                    </xsl:variable>

                    <a title="{$org-other-label}" alt="{$org-other-label}" href="{$href}">[...]</a>
                </td>
            </tr>
        </xsl:for-each>

        <tr>
            <xsl:if test="$node/boxed">
				<xsl:attribute name="class">
					<xsl:choose>
						<xsl:when test="count(task) != 0 and count(task) mod 2 = 0">evenRow</xsl:when>
						<xsl:otherwise>oddRow</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
            </xsl:if>

            <td colspan="3">
                <xsl:call-template name="MiogletOtherLink">
                    <xsl:with-param name="href"><xsl:value-of select="$base_href"/>/Organizer/ViewDay?day=<xsl:value-of select="$mioga_context/time/year"/>-<xsl:value-of select="$mioga_context/time/month"/>-<xsl:value-of select="$mioga_context/time/day"/></xsl:with-param>
                </xsl:call-template>
            </td>
        </tr>

    </table>
</xsl:template>



<!-- =================================
     Handle TodoList
     ================================= -->

<!-- Editor Form -->

<xsl:template match="org:TodoList" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorModeEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">org_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>
</xsl:template>


<!-- main TodoList template -->

<xsl:template match="org:TodoList">
    <xsl:param name="node"/>

    <table class="orgTodoList" cellpadding="5" cellspacing="0">
        <xsl:if test="count(task) = 0">
            <tr>
                <td>
                    <i><xsl:call-template name="org-no-todo-task"/></i>
                </td>
            </tr>
        </xsl:if>

        <xsl:variable name="base_href">
            <xsl:choose>
                <xsl:when test="../mode = 'group'"><xsl:value-of select="$bin_uri"/>/<xsl:value-of select="$mioga_context/group/ident"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="$bin_uri"/>/<xsl:value-of select="$mioga_context/user/ident"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:for-each select="task">
            <tr style="vertical-align: middle;">

                <xsl:if test="$node/boxed">
					<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
							<xsl:otherwise>oddRow</xsl:otherwise>
						</xsl:choose>
				   </xsl:attribute>
                </xsl:if>

                <td class="task">
                    <xsl:attribute name="style">
                        <xsl:choose>
                            <xsl:when test="priority = 'very urgent'">background: <xsl:value-of select="$org_priority1_color"/>;</xsl:when>
                            <xsl:when test="priority = 'urgent'">background: <xsl:value-of select="$org_priority2_color"/>;</xsl:when>
                            <xsl:when test="priority = 'normal'">background: <xsl:value-of select="$org_priority3_color"/>;</xsl:when>
                            <xsl:when test="priority = 'not urgent'">background: <xsl:value-of select="$org_priority4_color"/>;</xsl:when>
                            <xsl:otherwise>background: <xsl:value-of select="$mioga-bg-color"/>;</xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    &#160;&#160;
                </td>

                <td class="name">
                    <xsl:value-of select="name"/>
                </td>

                <td class="editTask">

                    <xsl:variable name="href"><xsl:value-of select="$base_href"/>/Organizer/EditTodoTask?taskrowid=<xsl:value-of select="rowid"/>&amp;mode=initial</xsl:variable>

                    <xsl:variable name="org-other-label">
                        <xsl:call-template name="org-other-label"/>
                    </xsl:variable>
                    
                    <a title="{$org-other-label}" alt="{$org-other-label}" href="{$href}">[...]</a>
                </td>
            </tr>
        </xsl:for-each>

        <tr>
            <xsl:if test="$node/boxed">
				<xsl:attribute name="class">
					<xsl:choose>
						<xsl:when test="count(task) != 0 and count(task) mod 2 = 0">evenRow</xsl:when>
						<xsl:otherwise>oddRow</xsl:otherwise>
					</xsl:choose>
			   </xsl:attribute>
            </xsl:if>
            
            <td colspan="3">
                <xsl:call-template name="MiogletOtherLink">
                    <xsl:with-param name="href"><xsl:value-of select="$base_href"/>/Organizer/ViewDay?day=<xsl:value-of select="$mioga_context/time/year"/>-<xsl:value-of select="$mioga_context/time/month"/>-<xsl:value-of select="$mioga_context/time/day"/></xsl:with-param>
                </xsl:call-template>
            </td>
        </tr>

    </table>
</xsl:template>

<!-- =================================
     Handle EventList
     ================================= -->

<!-- Editor Form -->

<xsl:template match="org:EventList" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorModeEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">org_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">duration</xsl:with-param>
        <xsl:with-param name="value" select="$node/duration"/>
    </xsl:call-template>

</xsl:template>


<!-- main EventList template -->

<xsl:template match="org:EventList">
    <xsl:param name="node"/>

    <table class="orgEventList" cellpadding="5" cellspacing="0">
        <xsl:if test="count(task) = 0">
            <tr>
                <td>
                    <i><xsl:call-template name="org-no-task"/></i>
                </td>
            </tr>
        </xsl:if>

        <xsl:variable name="base_href">
            <xsl:choose>
                <xsl:when test="./@mode = 'group'"><xsl:value-of select="$bin_uri"/>/<xsl:value-of select="$mioga_context/group/ident"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="$bin_uri"/>/<xsl:value-of select="$mioga_context/user/ident"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:for-each select="task">
            <tr>
                <xsl:if test="$node/boxed">
					<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
							<xsl:otherwise>oddRow</xsl:otherwise>
						</xsl:choose>
				   </xsl:attribute>
                </xsl:if>

				<td class="day">
					<xsl:value-of select="start/day" />/<xsl:value-of select="start/month" />/<xsl:value-of select="start/year" />
				</td>
				<td class="duration">
					de <xsl:value-of select="start/hour" />:<xsl:value-of select="start/min" /> à <xsl:value-of select="stop/hour" />:<xsl:value-of select="stop/min" />
				</td>
                <td class="name">
					<xsl:value-of select="name" />
				</td>
                <td class="description">
					<pre><xsl:value-of select="description" /></pre>
				</td>
            </tr>
        </xsl:for-each>

        <tr>
            <xsl:if test="$node/boxed">
				<xsl:attribute name="class">
					<xsl:choose>
						<xsl:when test="count(task) != 0 and count(task) mod 2 = 0">evenRow</xsl:when>
						<xsl:otherwise>oddRow</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
            </xsl:if>

            <td colspan="4">
                <xsl:call-template name="MiogletOtherLink">
                    <xsl:with-param name="href"><xsl:value-of select="$base_href"/>/Organizer/ViewMonth?day=<xsl:value-of select="$mioga_context/time/year"/>-<xsl:value-of select="$mioga_context/time/month"/>-<xsl:value-of select="$mioga_context/time/day"/></xsl:with-param>
                </xsl:call-template>
            </td>
        </tr>

    </table>
</xsl:template>

<xsl:template name="org-other-label">
  <xsl:value-of select="mioga:gettext('Click to view task')"/>
</xsl:template>

<xsl:template name="org-no-task">
  <xsl:value-of select="mioga:gettext('No task')"/>
</xsl:template>

<xsl:template name="org-no-todo-task">
  <xsl:value-of select="mioga:gettext('No todo task')"/>
</xsl:template>

</xsl:stylesheet>
