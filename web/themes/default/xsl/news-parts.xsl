<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" encoding="UTF-8" media-type="text/html"/>

<xsl:include href="scriptaculous.xsl"/>
<!-- ===============
	Variables
	================ -->

<xsl:variable name="anim_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Animation">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="moder_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Moderation">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="write_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Write">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="read_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Read">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<!--
=====================
 Utilities templates
=====================
-->

<!-- escape asostrophes very useful for javascript -->
<xsl:template name="escape-apos">
   <xsl:param name="string" />
   <!-- create an $apos variable to make it easier to refer to -->
   <xsl:variable name="apos" select='"&apos;"' />
   <xsl:choose>
      <!-- if the string contains an apostrophe... -->
      <xsl:when test='contains($string, $apos)'>
         <!-- ... give the value before the apostrophe... -->
         <xsl:value-of select="substring-before($string, $apos)" />
         <!-- ... the escaped apostrophe ... -->
         <xsl:text>\'</xsl:text>
         <!-- ... and the result of applying the template to the
                  string after the apostrophe -->
         <xsl:call-template name="escape-apos">
            <xsl:with-param name="string"
                            select="substring-after($string, $apos)" />
         </xsl:call-template>
      </xsl:when>
      <!-- otherwise... -->
      <xsl:otherwise>
         <!-- ... just give the value of the string -->
         <xsl:value-of select="$string" />
      </xsl:otherwise>
   </xsl:choose>
</xsl:template>

<!--
===============
 Templates
===============
-->

<xsl:template match="/">
    <xsl:apply-templates/>
</xsl:template>

<!--
===========================
 Write or Edit a news item
===========================
-->
<xsl:template name="WriteOrEditNewsItem">
    <xsl:param name="action"/>
    <!-- js variables -->
    <xsl:variable name="title" select="Actions/*[name()=$action]"/>
    <xsl:variable name="news_title" select="//Actions/NewsTitle"/>
    <xsl:variable name="newsitem_title" select="NewsItem/Title"/>
    <xsl:variable name="news" select="//Actions/News"/>
    <xsl:variable name="contents" select="NewsItem/Contents"/>
    <xsl:variable name="save_draft" select="//Actions/SaveDraft"/>
    <xsl:variable name="validate" select="//Actions/Validate"/>
    <xsl:variable name="decline" select="//Actions/Decline"/>
    <xsl:variable name="publish_news" select="//Actions/PublishNews"/>
    <xsl:variable name="cancel" select="//Actions/Cancel"/>
    <xsl:variable name="page" select="//Page"/>
    <!-- end of js variables -->
    
    <div class="sbox border_color">
        <h2 class="title_bg_color"><xsl:value-of select="$title"/></h2>
        <div class="content">
        <form method="post" action="UpdateNewsItem">
		<xsl:call-template name="CSRF-input"/>
        <xsl:if test="$action = 'UpdateNews'">
        <input type="hidden" name="rowid" value="{NewsItem/@id}"/>
        </xsl:if>
        <xsl:if test="NewsItem/@old_id">
        <input type="hidden" name="old_id" value="{NewsItem/@old_id}"/>
        </xsl:if>
        <input type="hidden" name="status" value="{NewsItem/@status}"/>
        <input type="hidden" name="page" value="{Page}"/>
        <xsl:if test="CanSaveAsDraft">
        <input type="hidden" name="candraft" value="1"/>
        </xsl:if>
        <input type="hidden" name="editaction" value="{$action}"/>
    
        <div class="form">
            <fieldset>
                <legend><xsl:value-of select="$news_title"/></legend>
                <input size="30" type="text" name="title" id="title" value="{$newsitem_title}">
                    <xsl:attribute name="class">text border_color<xsl:if test="//errors/err[@arg = 'title']"> error-box</xsl:if></xsl:attribute>
                </input>
                <xsl:if test="//errors/err[@arg = 'title']">
                    <xsl:call-template name="error-message">
                        <xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'title']/type" /></xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
            </fieldset>
            <fieldset>
                <legend><xsl:value-of select="$news"/></legend>
                <textarea name="text" id="contents"><xsl:attribute name="class">mceEditor border_color<xsl:if test="//errors/err[@arg = 'text']"> error-box</xsl:if></xsl:attribute><xsl:value-of select="$contents"/></textarea>
                <xsl:if test="//errors/err[@arg = 'text']">
                    <xsl:call-template name="error-message">
                        <xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'text']/type" /></xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
            </fieldset>
            <ul class="button_list">
                <xsl:if test="CanSaveAsDraft">
                <li>
                    <button class="button" type="submit" value="{$save_draft}">
                        <xsl:attribute name="onclick">this.name="draft";</xsl:attribute>
						<xsl:value-of select="$save_draft"/>
                    </button>
                </li>
                </xsl:if>
                <xsl:choose>
                    <xsl:when test="NewsItem/CanBeValidated">
                    <li>
                        <button class="button" type="submit" value="{$validate}">
                        <xsl:attribute name="onclick">this.name="validate";</xsl:attribute>
						<xsl:value-of select="$validate"/>
                    </button>
                    </li>
                    <li>
                        <button class="button" type="submit" value="{$decline}">
                        <xsl:attribute name="onclick">this.name="decline";</xsl:attribute>
						<xsl:value-of select="$decline"/>
                    </button>
                    </li>
                    </xsl:when>
                    <xsl:otherwise>
                    <li>
                        <button class="button" type="submit" value="{$publish_news}"><xsl:value-of select="$publish_news"/></button>
                    </li>
                    </xsl:otherwise>
                </xsl:choose>
                <li>
                    <a class="button" href="{$page}"><xsl:value-of select="$cancel"/></a>
                </li>
            </ul>
        </div>
    
        </form>
        </div>
    </div>
</xsl:template>

<!--
=================
 Display options
=================
-->
<xsl:template match="Options">
    <xsl:param name="js_mode"/>
    
    <!-- using variable to handle javascript mode -->
        <xsl:variable name="title">
            <xsl:choose>
                <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Options/Title"/></xsl:call-template></xsl:when>
                <xsl:otherwise><xsl:value-of select="//Options/Title"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="message">
            <xsl:choose>
                <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Message"/></xsl:call-template></xsl:when>
                <xsl:otherwise><xsl:value-of select="//Message"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="ok">
            <xsl:choose>
                <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Options/Ok"/></xsl:call-template></xsl:when>
                <xsl:otherwise><xsl:value-of select="//Options/Ok"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="cancel">
            <xsl:choose>
                <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Options/Cancel"/></xsl:call-template></xsl:when>
                <xsl:otherwise><xsl:value-of select="//Options/Cancel"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
    <!-- end of variables definition -->
    
    <div class="sbox border_color">
        <h2 class="title_bg_color"><xsl:value-of select="$title"/></h2>
        <div class="content">
            <xsl:if test="//Message"><p><xsl:value-of select="$message"/></p></xsl:if>
            <form action="UpdateOptions" method="post">
            <xsl:attribute name="onsubmit">new Ajax.Request("UpdateOptions", {evalScripts:true, asynchronous:true, parameters:encodeURI(Form.serialize(this)) }); return false;</xsl:attribute>
            <xsl:for-each select="Option">
                <!-- js variables -->
                <xsl:variable name="label">
                    <xsl:choose>
                        <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="@label"/></xsl:call-template></xsl:when>
                        <xsl:otherwise><xsl:value-of select="@label"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <!-- end of js variables -->
        
                <xsl:choose>
                    <xsl:when test="@type = 'text'">
                        <label for="{@field}-id"><xsl:value-of select="$label"/></label>&#160;<input id="{@field}-id" size="3" type="text" name="{@field}" value="{.}"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <label for="{@field}-id"><input id="{@field}-id" type="checkbox" name="{@field}" value="1">
                            <xsl:if test=".=1"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                        </input>&#160;<xsl:value-of select="$label"/></label>
                    </xsl:otherwise>
                </xsl:choose><br/>
            </xsl:for-each>
                <ul class="button_list">
                    <li>
                        <input class="button" type="submit" value="{$ok}"/>
                    </li>
                    <li>
                        <a class="button" href="#"><xsl:attribute name="onclick">$("disable-window").hide(); $("options-box").hide(); return false;</xsl:attribute><xsl:value-of select="$cancel"/></a>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</xsl:template>

<!--
=======================================
 Display news
=======================================
-->
<xsl:template match="News">
    <xsl:param name="js_mode"/>
    <xsl:param name="page"/>
    
    <!-- js variables -->
    <xsl:variable name="validate">
        <xsl:choose>
            <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Actions/Validate"/></xsl:call-template></xsl:when>
            <xsl:otherwise><xsl:value-of select="//Actions/Validate"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="decline_confirm">
        <xsl:choose>
            <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Actions/DeclineConfirm"/></xsl:call-template></xsl:when>
            <xsl:otherwise><xsl:value-of select="//Actions/DeclineConfirm"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="decline">
        <xsl:choose>
            <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Actions/Decline"/></xsl:call-template></xsl:when>
            <xsl:otherwise><xsl:value-of select="//Actions/Decline"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="edit">
        <xsl:choose>
            <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Actions/Edit"/></xsl:call-template></xsl:when>
            <xsl:otherwise><xsl:value-of select="//Actions/Edit"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="delete_confirm">
        <xsl:choose>
            <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Actions/DeleteConfirm"/></xsl:call-template></xsl:when>
            <xsl:otherwise><xsl:value-of select="//Actions/DeleteConfirm"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="delete_news">
        <xsl:choose>
            <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Actions/DeleteNews"/></xsl:call-template></xsl:when>
            <xsl:otherwise><xsl:value-of select="//Actions/DeleteNews"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <!-- end of js variables -->
    
    <xsl:for-each select="./NewsItem">
        <!-- using variable to handle javascript mode -->
        <xsl:variable name="title">
            <xsl:choose>
                <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="Title"/></xsl:call-template></xsl:when>
                <xsl:otherwise><xsl:value-of select="Title"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="contents">
                <xsl:choose>
                    <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="Contents"/></xsl:call-template></xsl:when>
                    <xsl:otherwise><xsl:value-of select="Contents" disable-output-escaping="yes"/></xsl:otherwise>
                </xsl:choose>
        </xsl:variable>
        <!-- end of variables definition -->
        
        <div class="newsitem sbox border_color">
            <h2 class="title_bg_color"><xsl:value-of select="$title"/>
            <xsl:if test="./CanBeValidated">
                <a href="#"><xsl:attribute name="onclick">new Ajax.Request("ValidateNewsItem", {evalScripts:true, asynchronous:true, parameters: "rowid=<xsl:value-of select="@id"/>" }); return false;</xsl:attribute><img class="validate" src="{$image_uri}/16x16/actions/dialog-ok-apply.png" title="{$validate}" alt="{$validate}" /></a>
                <a href="#"><xsl:attribute name="onclick">if (confirm("<xsl:value-of select="$decline_confirm"/>")) { new Ajax.Request("DeclineNewsItem", {evalScripts:true, asynchronous:true, parameters: "rowid=<xsl:value-of select="@id"/>" }); }; return false;</xsl:attribute><img class="decline" src="{$image_uri}/16x16/actions/dialog-cancel.png" title="{$decline}" alt="{$decline}" /></a>
            </xsl:if>
            <xsl:if test="./CanBeEdited">
                <a href="EditNewsItem?rowid={@id}&amp;page={$page}"><img class="edit" src="{$image_uri}/16x16/actions/document-edit.png" title="{$edit}" alt="{$edit}" /></a>
            </xsl:if>
            <xsl:if test="$moder_right=1">
                <a href="#"><xsl:attribute name="onclick">if (confirm("<xsl:value-of select="$delete_confirm"/>")) { new Ajax.Request("DeleteNewsItem", {evalScripts:true, asynchronous:true, parameters: "rowid=<xsl:value-of select="@id"/>&amp;page=<xsl:value-of select="$page"/>"}); }; return false;</xsl:attribute><img class="delete" src="{$image_uri}/16x16/actions/trash-empty.png" title="{$delete_news}" alt="{$delete_news}"/></a>
            </xsl:if>
            </h2>
            <h3 class="news">
				<xsl:for-each select="Infos">
					<xsl:choose>
						<xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="."/></xsl:call-template></xsl:when>
						<xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
					</xsl:choose>
					<br/>
				</xsl:for-each>
                <span class="news_id">#<xsl:value-of select="@id"/></span>
            </h3>
            
            <xsl:value-of select="$contents" disable-output-escaping="yes"/>
        </div>
    </xsl:for-each>
</xsl:template>


</xsl:stylesheet>
