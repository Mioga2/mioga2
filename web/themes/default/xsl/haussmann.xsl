<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
	<xsl:import href="base.xsl"/>
	<xsl:import href="jquery.xsl"/>
	<xsl:include href="tags.xsl"/>
	<xsl:output method="html" indent="yes" encoding="UTF-8"/>
	<!-- ===============
		root node
		================ -->
	<xsl:template match="/">
		<xsl:call-template name="doctype"/>
		<html>
			<head>
				<xsl:call-template name="title"/>
				<xsl:call-template name="favicon"/>

				<xsl:call-template name="mioga-css"/>
				<xsl:call-template name="mioga-js"/>

				<xsl:call-template name="jquery-js"/>
				<xsl:call-template name="tags-js"/>
				<xsl:call-template name="tags-css"/>
				<xsl:call-template name="jquery-ui"/>
				<xsl:call-template name="console-js"/>

				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/haussmann.css" media="screen"/>
				
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.miniColors.css"/>
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.fileBrowser.css" media="screen"/>
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.fileselector.css" media="screen"/>
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.fileuploader.css" media="screen"/>
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.itemSelect.css" media="screen"/>
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.foldable.css" media="screen" />
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.extensibleList_haussmann.css" media="screen"/>
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.tableScrollableSortable.css" media="screen"/>
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.userSelect.css" media="screen"/>
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jqueryui-editable.css" media="screen"/>
				
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/mioga.css" media="print"/>
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery-ui-mioga2.css" media="print"/>
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.foldable.css" media="print" />
				<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/haussmann_print.css" media="print"/>
				
				<script src="{$theme_uri}/javascript/mioga.js" type="text/javascript"></script>
				<script src="{$theme_uri}/javascript/jquery.ellipsis.js" type="text/javascript"></script>
				<script src="{$theme_uri}/javascript/haussmann.js" type="text/javascript"></script>
				
				<xsl:apply-templates mode="head"/>
				<xsl:call-template name="theme-css"/>
			</head>

			<body class="haussmann mioga">
				<xsl:call-template name="DisplayAppTitle" />
				<div id="haussmann" class="haussmann-main"></div>
			</body>
		</html>
	</xsl:template>
	<!-- ===============
		DisplayMain
		================ -->
	<mioga:strings>
		<i18nString>add_attendee_label</i18nString>
		<i18nString>add_attendees</i18nString>
		<i18nString>attendeeCount</i18nString>
		<i18nString>add</i18nString>
		<i18nString>hour</i18nString>
		<i18nString>hours</i18nString>
		<i18nString>date</i18nString>
		<i18nString>dates</i18nString>
		<i18nString>plural</i18nString>
		<i18nString>singular</i18nString>
		<i18nString>informations</i18nString>
		<i18nString>history</i18nString>
		<i18nString>firstname</i18nString>
		<i18nString>lastname</i18nString>
		<i18nString>email</i18nString>
		<i18nString>label</i18nString>
		<i18nString>description</i18nString>
		<i18nString>workload</i18nString>
		<i18nString>dirOfProject</i18nString>
		<i18nString>status</i18nString>
		<i18nString>comment</i18nString>
		<i18nString>progress</i18nString>
		<i18nString>tags</i18nString>
		<i18nString>model</i18nString>
		<i18nString>resultList</i18nString>
		<i18nString>task_list</i18nString>
		<i18nString>budget</i18nString>
		<i18nString>notes</i18nString>
		<i18nString>fineblanking</i18nString>
		<i18nString>advancedParameters</i18nString>
		<i18nString>complementaryData</i18nString>
		<i18nString>started_date</i18nString>
		<i18nString>planned_date</i18nString>
		<i18nString>p_start</i18nString>
		<i18nString>p_end</i18nString>
		<i18nString>w_start</i18nString>
		<i18nString>w_end</i18nString>
		<i18nString>ended_date</i18nString>
		<i18nString>use_result</i18nString>
		<i18nString>color</i18nString>
		<i18nString>bgColor</i18nString>
		<i18nString>groupingAffectation</i18nString>
		<i18nString>labelOfNewProject</i18nString>
		<i18nString>searchProject</i18nString>
		<i18nString>groupingByTree</i18nString>
		<i18nString>complDataType_multi</i18nString>
		<i18nString>viewsList</i18nString>
		<i18nString>delegation</i18nString>
		<i18nString>affectation_personn</i18nString>
		<i18nString>projectFiles</i18nString>
		<i18nString>title_result_status</i18nString>
		<i18nString>title_task_status</i18nString>
		<i18nString>title_project_status</i18nString>
		<i18nString>title_compl_data_status</i18nString>
		<i18nString>title_vocabulary</i18nString>
		<i18nString>title_grouping_tasks</i18nString>
		<i18nString>title_groupingTree</i18nString>
		<i18nString>title_groupingList</i18nString>
		<i18nString>All</i18nString>
		<i18nString>unaffected</i18nString>
		<i18nString>none</i18nString>
		<i18nString>New</i18nString>
		<i18nString>not_affected</i18nString>
		<i18nString>not_delegated</i18nString>
		<i18nString>undefined_status</i18nString>
		<i18nString>filterByStatus</i18nString>
		<i18nString>end</i18nString>
		<i18nString>checked</i18nString>
		<i18nString>validated</i18nString>
		<i18nString>edit_mod</i18nString>
		<i18nString>read_mod</i18nString>
		<i18nString>close</i18nString>
		<i18nString>save</i18nString>
		<i18nString>apply</i18nString>
		<i18nString>cancel</i18nString>
		<i18nString>edit</i18nString>
		<i18nString>create</i18nString>
		<i18nString>createAndEdit</i18nString>
		<i18nString>saveAndContinue</i18nString>
		<i18nString>destroy</i18nString>
		<i18nString>remove</i18nString>
		<i18nString>removeItem</i18nString>
		<i18nString>select_all</i18nString>
		<i18nString>select_none</i18nString>
		<i18nString>choose</i18nString>
		<i18nString>changeProject</i18nString>
		<i18nString>createProject</i18nString>
		<i18nString>editProject</i18nString>
		<i18nString>edit_task</i18nString>
		<i18nString>delete_task</i18nString>
		<i18nString>createNote</i18nString>
		<i18nString>createNewModel</i18nString>
		<i18nString>updateModel</i18nString>
		<i18nString>edit_result</i18nString>
		<i18nString>delete_result</i18nString>
		<i18nString>create_fine_blanking</i18nString>
		<i18nString>edit_fine_blanking</i18nString>
		<i18nString>edit_task_note</i18nString>
		<i18nString>edit_result_note</i18nString>
		<i18nString>choose_personn</i18nString>
		<i18nString>comment_of</i18nString>
		<i18nString>choose_project_dir</i18nString>
		<i18nString>add_task</i18nString>
		<i18nString>add_result</i18nString>
		<i18nString>editGroupingResult</i18nString>
		<i18nString>editGroupingList</i18nString>
		<i18nString>showAll</i18nString>
		<i18nString>hideAll</i18nString>
		<i18nString>hide</i18nString>
		<i18nString>show</i18nString>
		<i18nString>back</i18nString>
		<i18nString>hide_history</i18nString>
		<i18nString>show_history</i18nString>
		<i18nString>result_view</i18nString>
		<i18nString>projectList</i18nString>
		<i18nString>backToProject</i18nString>
		<i18nString>alert</i18nString>
		<i18nString>permanently_remove_message</i18nString>
		<i18nString>remove_message_confirmation</i18nString>
		<i18nString>saveProjectToContinue</i18nString>
		<i18nString>task_notification_message</i18nString>
		<i18nString>progressBarText</i18nString>
		<i18nString>PB_main</i18nString>
		<i18nString>disallowEmpty</i18nString>
		<i18nString>colorDisallowEmpty</i18nString>
		<i18nString>bgColorDisallowEmpty</i18nString>
		<i18nString>progressDateDisallowEmpty</i18nString>
		<i18nString>statusDateDisallowEmpty</i18nString>
		<i18nString>textDisallowEmpty</i18nString>
		<i18nString>groupingSubListDisallowEmpty</i18nString>
		<i18nString>labelDisallowEmpty</i18nString>
		<i18nString>groupingListDisallowEmpty</i18nString>
		<i18nString>groupingTreeDisallowEmpty</i18nString>
		<i18nString>resultStatusDisallowEmpty</i18nString>
		<i18nString>taskStatusDisallowEmpty</i18nString>
		<i18nString>projectStatusDisallowEmpty</i18nString>
		<i18nString>complementaryDataDisallowEmpty</i18nString>
		<i18nString>groupingListEmpty</i18nString>
		<i18nString>labelRequired</i18nString>
		<i18nString>resultStatusAlreadyExists</i18nString>
		<i18nString>taskStatusAlreadyExists</i18nString>
		<i18nString>projectStatusAlreadyExists</i18nString>
		<i18nString>complementaryDataAlreadyExists</i18nString>
		<i18nString>groupingTreeAlreadyExists</i18nString>
		<i18nString>groupingListAlreadyExists</i18nString>
		<i18nString>groupingSubListAlreadyExists</i18nString>
		<i18nString>projectAlreadyExists</i18nString>
		<i18nString>resultAlreadyExists</i18nString>
		<i18nString>taskAlreadyExists</i18nString>
		<i18nString>modelAlreadyExists</i18nString>
		<i18nString>mustBePosNum</i18nString>
		<i18nString>mustBeSubList</i18nString>
		<i18nString>workloadMustBeNum</i18nString>
		<i18nString>budgetMustBeNum</i18nString>
		<i18nString>progress_percent_error</i18nString>
		<i18nString>all_field_of_vocabulary_must_be_filled</i18nString>
		<i18nString>errorStartedPlanned</i18nString>
		<i18nString>errorStartedEnded</i18nString>
		<i18nString>dateRequired</i18nString>
		<i18nString>dateInvalid</i18nString>
		<i18nString>progressDateInvalid</i18nString>
		<i18nString>pstartInvalid</i18nString>
		<i18nString>pendInvalid</i18nString>
		<i18nString>progressDateRequired</i18nString>
		<i18nString>progressValueRequired</i18nString>
		<i18nString>statusDateRequired</i18nString>
		<i18nString>plannedDateRequired</i18nString>
		<i18nString>startedDateRequired</i18nString>
		<i18nString>pstartMinToDay</i18nString>
		<i18nString>pendMinToDay</i18nString>
		<i18nString>pendMinPstart</i18nString>
		<i18nString>errorPstartPend</i18nString>
		<i18nString>baseDirNotFound</i18nString>
		<i18nString>noExistingProject</i18nString>
		<i18nString>rightsInsufficient</i18nString>
		<i18nString>rightsInsufficientForAccess</i18nString>
		<i18nString>baseDirNotAllowed</i18nString>
		<i18nString>badColorFormat</i18nString>
		<i18nString>badBgColorFormat</i18nString>
		<i18nString>successSaveDelegation</i18nString>
		<i18nString>successSaveAffectation</i18nString>
		<i18nString>successSaveInformation</i18nString>
		<i18nString>successSaveStatus</i18nString>
		<i18nString>successCreateProgressItem</i18nString>
		<i18nString>successCreateStatusItem</i18nString>
		<i18nString>successUpdateProject</i18nString>
		<i18nString>successCreateStatusItemTmp</i18nString>
		<i18nString>model_created_success</i18nString>
		<i18nString>model_updated_success</i18nString>
		<i18nString>confirmSuppressProject</i18nString>
		<i18nString>errorDeleteProject</i18nString>
		<i18nString>mustBeApplyProject</i18nString>
		<i18nString>backwardAllLabel</i18nString>
		<i18nString>showGantt</i18nString>
		<i18nString>backwardLabel</i18nString>
		<i18nString>forwardLabel</i18nString>
		<i18nString>forwardAllLabel</i18nString>
		<i18nString>zoomInLabel</i18nString>
		<i18nString>zoomOutLabel</i18nString>
		<i18nString>todayLabel</i18nString>
		<i18nString>configurationText</i18nString>
		<i18nString>showDetailsTitle</i18nString>
		<i18nString>closeLabel</i18nString>
		<i18nString>noTasksText</i18nString>
		<i18nString>weekLabel</i18nString>
		<i18nString>weeksLabel</i18nString>
		<i18nString>hideTimeline</i18nString>
		<i18nString>showTimeline</i18nString>
		<i18nString>display</i18nString>
		<i18nString>detail</i18nString>
		<i18nString>display_filter</i18nString>
		<i18nString>all_projects</i18nString>
		<i18nString>not_indicated</i18nString>
		<i18nString>milestone</i18nString>
		<i18nString>ganttView</i18nString>
		<i18nString>timelineView</i18nString>
	</mioga:strings>
	
	<xsl:template match="/DisplayMain" mode="head">
		<xsl:variable name="no_header">
			<xsl:choose>
				<xsl:when test="no_header = 1">1</xsl:when>
				<xsl:when test="no_header = 0">0</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<script type='text/javascript'>
			Date.prototype.getWeek = function() {
				var onejan = new Date(this.getFullYear(),0,1);
				return Math.ceil((((this - onejan) / 86400000) + onejan.getDay()+1)/7);
			}
			Date.prototype.getLastDayOfMonth = function() {
				var end_date = new Date();
				end_date.setDate(1);
				if (this.getMonth() === 11) {
					end_date.setMonth(0);
					end_date.setFullYear(this.getFullYear() + 1);
				}
				else {
					end_date.setMonth(this.getMonth() + 1);
					end_date.setFullYear(this.getFullYear());
				}
				end_date.setDate(end_date.getDate() - 1);
				return end_date;
			}
			Date.prototype.parseMiogaDate = function(val, date_only) {
				var re;
				if (date_only) {
					re = /^(\d+)-(\d+)-(\d+)/;
				} else {
					re = /^(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/;
				}
				var res = re.exec(val);
				this.setUTCFullYear(res[1]);
				this.setUTCMonth(res[2] - 1);
				this.setUTCDate(res[3]);
				if (date_only) {
					this.setUTCHours(0);
					this.setUTCMinutes(0);
					this.setUTCSeconds(0);
					this.setUTCMilliseconds(0);
				} else {
					this.setUTCHours(res[4]);
					this.setUTCMinutes(res[5]);
					this.setUTCSeconds(res[6]);
					this.setUTCMilliseconds(0);
				}
			}
		</script>
		<script type="text/javascript">
			var available_tags = Array (
				"<xsl:call-template name="join">
					<xsl:with-param name="valueList" select="tag"/>
					<xsl:with-param name="separator" select="'&quot;, &quot;'"/>
				</xsl:call-template>"
			);
			var free_tags = <xsl:value-of select="free_tags"/>;
			var tag_does_not_exist = "<xsl:value-of select="mioga:gettext ('This tag does not exist and will be ignored.')"/>";
			var no_header = <xsl:value-of select="$no_header"/>;
			
			var tags_info = {
				available_tags : available_tags,
				free_tags : free_tags,
				tag_does_not_exist : tag_does_not_exist,
				no_header :no_header
			};
			mioga_context.private_dav = '<xsl:value-of select="$private_dav"/>';
		</script>
		<script type="text/javascript">
			<!-- ==============================================================
				READY javascript function ProjectReady ().
			 ============================================================== -->
			$(document).ready(function(){
				
				
				var i18n = {
					<xsl:apply-templates select="document('')/*/mioga:strings/i18nString"/>
				};
				i18n["monthNames"] = [ "<xsl:value-of select="mioga:gettext('January')"/>",
													 "<xsl:value-of select="mioga:gettext('February')"/>",
													 "<xsl:value-of select="mioga:gettext('March')"/>",
													 "<xsl:value-of select="mioga:gettext('April')"/>",
													 "<xsl:value-of select="mioga:gettext('May')"/>",
													 "<xsl:value-of select="mioga:gettext('June')"/>",
													 "<xsl:value-of select="mioga:gettext('July')"/>",
													 "<xsl:value-of select="mioga:gettext('August')"/>",
													 "<xsl:value-of select="mioga:gettext('September')"/>",
													 "<xsl:value-of select="mioga:gettext('October')"/>",
													 "<xsl:value-of select="mioga:gettext('November')"/>",
													 "<xsl:value-of select="mioga:gettext('December')"/>" ];
				
				$("#haussmann").haussmann({
					private_dav : "<xsl:value-of select="$private_dav_uri"/>",
					user_lang : mioga_context.user.lang,
					tags_info : tags_info,
					i18n : i18n
				});
			});
		</script>
	</xsl:template>
	<xsl:template name="join" >
		<xsl:param name="valueList" select="''"/>
		<xsl:param name="separator" select="','"/>
		<xsl:for-each select="$valueList">
			<xsl:choose>
				<xsl:when test="position() = 1">
					<xsl:value-of select="."/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat($separator, .) "/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="i18nString">
		<xsl:variable name="translation"><xsl:call-template name="escapeString"/></xsl:variable>
		<xsl:if test="$translation!=.">
			"<xsl:value-of select="."/>": "<xsl:value-of select="$translation"/>"<xsl:if test="position() != last()">,</xsl:if>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="escapeString">
		<!-- Escape newlines -->
		<xsl:call-template name="escapeNewline">
			<xsl:with-param name="pText">
				<!-- Escape quotes -->
				<xsl:call-template name="escapeQuote">
					<xsl:with-param name="pText">
						<xsl:value-of select="mioga:gettext (.)"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="escapeQuote">
		<xsl:param name="pText"/>
	
		<xsl:if test="string-length($pText) >0">
			<xsl:value-of select="substring-before(concat($pText, '&quot;'), '&quot;')"/>
	
			<xsl:if test="contains($pText, '&quot;')">
				<xsl:text>\"</xsl:text>
	
				<xsl:call-template name="escapeQuote">
					<xsl:with-param name="pText" select="substring-after($pText, '&quot;')"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="escapeNewline">
		<xsl:param name="pText"/>
	
		<xsl:if test="string-length($pText) >0">
			<xsl:value-of select= "substring-before(concat($pText, '&#10;'), '&#10;')"/>
	
			<xsl:if test="contains($pText, '&#10;')">
				<xsl:text>\n</xsl:text>
	
				<xsl:call-template name="escapeNewline">
					<xsl:with-param name="pText" select="substring-after($pText, '&#10;')"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	
</xsl:stylesheet>
