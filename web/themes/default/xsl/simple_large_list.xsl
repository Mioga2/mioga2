<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<xsl:include href="large_list.xsl" />

<!--
     SimpleLargeList templates.
     
     paramaters : 
     bgindex : pages index background color. (default $mioga-list-title-bg-color (base.xsl))

     fgindex : pages index foreground color. (default $mioga-list-title-text-color (base.xsl))

     topindex :    1 => display a page index on the top of the list.
                   0 => don't display a page index on the top of the list.
                   (default 1)

     bottomindex : 1 => display a page index on the bottom of the list.
                   0 => don't display a page index on the bottom of the list.
                   (default 1)

     bgcolor :     List background color.

     tablewidth :  width of the list. (default 100%)
     
     displayaction : Display the liste title (default 1) 

     displaytitle  : Display the liste title (default 1) 

     -->

<xsl:template match="SimpleLargeList">
    <xsl:param name="label"/>
    <xsl:param name="advancedsearch" select="0"/>
    <xsl:param name="bgindex" select="$mioga-list-title-bg-color"/>
    <xsl:param name="fgindex" select="$mioga-list-title-text-color"/>
    <xsl:param name="topindex">0</xsl:param>
    <xsl:param name="bottomindex">1</xsl:param>
    <xsl:param name="bgcolor" select="$mioga-list-title-bg-color"/>
    <xsl:param name="tablewidth">100%</xsl:param>
    <xsl:param name="displaytitle">1</xsl:param>
    <xsl:param name="displayaction">0</xsl:param>
    <xsl:param name="no_form">0</xsl:param>
    <xsl:param name="no-lpp">0</xsl:param>
    <xsl:param name="select-menu">0</xsl:param>
    <xsl:param name="nb-search-column">1</xsl:param>

    <xsl:choose>
        <xsl:when test="./AskConfirmForDelete">
            <xsl:apply-templates select="AskConfirmForDelete">
                <xsl:with-param name="bgindex" select="$bgindex"/>
                <xsl:with-param name="fgindex" select="$fgindex"/>
                <xsl:with-param name="topindex" select="$topindex"/>
                <xsl:with-param name="bottomindex" select="$bottomindex"/>
                <xsl:with-param name="bgcolor" select="$bgcolor"/>
                <xsl:with-param name="tablewidth" select="$tablewidth"/>
                <xsl:with-param name="displaytitle" select="$displaytitle"/>
            </xsl:apply-templates>
        </xsl:when>

        <xsl:when test="./SuccessMessage and SuccessMessage/nb_items = 0">
            <xsl:call-template name="NoSelectedItem"/>
        </xsl:when>

        <xsl:when test="./SuccessMessage">
            <xsl:apply-templates select="SuccessMessage"/>
        </xsl:when>

        <xsl:otherwise>

            <xsl:if test="$advancedsearch=1">
                <xsl:apply-templates select="searchform" mode="advanced">
                     <xsl:with-param name="tablewidth" select="$tablewidth"/>
                     <xsl:with-param name="nb-search-column" select="$nb-search-column"/>
                     <xsl:with-param name="label" select="$label"/>
               </xsl:apply-templates>
            </xsl:if>
            
            <br/>
            
            
            <xsl:if test="$no_form=0">
                <xsl:text disable-output-escaping="yes">&lt;form method="GET" action="</xsl:text><xsl:value-of select="local-name(..)"/><xsl:text disable-output-escaping="yes">" name="</xsl:text><xsl:value-of select="local-name(..)"/><xsl:text disable-output-escaping="yes">Sll"&gt;</xsl:text>
            </xsl:if>
                          
                <xsl:if test="$advancedsearch=0 or count(List/Row) != 0">
                    <xsl:variable name="app_action"><xsl:value-of select="$mioga_method" /></xsl:variable>
					  <xsl:if test="$select-menu = '1'">
							<ul class="hmenu">
								<li><a href="{$app_action}?select_all=1" class="action" id="select_all"><xsl:value-of select="mioga:gettext('Select all')"/></a></li>
								<li><a href="{$app_action}?unselect_all=1" class="action" id="unselect_all"><xsl:value-of select="mioga:gettext('Deselect all')"/></a></li>
							</ul>
					  </xsl:if>

                    <xsl:call-template name="LargeList">
                        <xsl:with-param name="bgindex" select="$bgindex"/>
                        <xsl:with-param name="fgindex" select="$fgindex"/>
                        <xsl:with-param name="topindex" select="$topindex"/>
                        <xsl:with-param name="bottomindex" select="$bottomindex"/>
                        <xsl:with-param name="bgcolor" select="$bgcolor"/>
                        <xsl:with-param name="tablewidth" select="$tablewidth"/>
                        <xsl:with-param name="displaytitle" select="$displaytitle"/>               
                        <xsl:with-param name="no_form">1</xsl:with-param>
                        <xsl:with-param name="no-lpp" select="$no-lpp"/>
                        <xsl:with-param name="method_context" select="local-name(..)"/>
                        <xsl:with-param name="lppinbottomindex">
                            <xsl:choose>
                                <xsl:when test="count(./fields/field[@type='delete'])!=0 or count(./fields/field[@type='checkboxes'])!=0 ">0</xsl:when>
                                <xsl:otherwise>1</xsl:otherwise>
                            </xsl:choose>
                        </xsl:with-param>
                    </xsl:call-template>
           
                </xsl:if>

             <xsl:if test="$no_form=0">
                 <xsl:text disable-output-escaping="yes">&lt;/form&gt;</xsl:text>
            </xsl:if>

            <xsl:if test="$advancedsearch=0">
                <xsl:apply-templates select="searchform">
                    <xsl:with-param name="tablewidth" select="$tablewidth"/>
                    <xsl:with-param name="label" select="$label"/>
                </xsl:apply-templates>
           </xsl:if>
                     
        </xsl:otherwise>

    </xsl:choose>

</xsl:template>


<!-- =================================
     Private templates
     ================================= -->

<!-- =================================
     Large List title handler
     ================================= -->

<xsl:template name="header-button">
    <xsl:param name="name"/>
    <xsl:param name="value"/>
    <xsl:param name="callback"/>

    <xsl:variable name="listname" select="../../List[1]/@name"/>

    <xsl:variable name="onclick">submitForm("<xsl:value-of select="$callback"/>Sll", "sortby_<xsl:value-of select="$listname"/>", "<xsl:value-of select="$name"/>")</xsl:variable>
    <a href="#" onclick="{$onclick}">
        <xsl:value-of select="$value"/>
    </a>
    <xsl:choose>
        <xsl:when test="../../List/@cur_sort_field=@name and ../../List/@cur_sort_asc=1">
            &#160;<a href="#" onclick="{$onclick}"><img src="{$image_uri}/16x16/actions/view-sort-descending.png" border="0" style="background-color: transparent"/></a>
        </xsl:when>
        <xsl:when test="../../List/@cur_sort_field=@name">
            &#160;<a href="#" onclick="{$onclick}"><img src="{$image_uri}/16x16/actions/view-sort-ascending.png" border="0" style="background-color: transparent"/></a>
        </xsl:when> 
    </xsl:choose>
</xsl:template>

<xsl:template match="SimpleLargeList" mode="list-title">
    <xsl:param name="disp_action">1</xsl:param>
    <xsl:param name="callback"><xsl:value-of select=".."/></xsl:param>
    <xsl:param name="callback-name"><xsl:value-of select="local-name(..)"/></xsl:param>

    <input type="hidden" name="sortby_{List/@name}"  value=""/>
    <input type="hidden" name="pagenb_{List/@name}"  value=""/>


    <xsl:for-each select="./fields/field">
        
        <xsl:if test="@type != 'rowid' and @type != 'hidden'">
        
            <xsl:variable name="name">
                <xsl:apply-templates select="../../.." mode="sll-field-name">
                    <xsl:with-param name="name" select="@name"/>
                </xsl:apply-templates>
            </xsl:variable>

            <xsl:choose>
                <xsl:when test="@type != 'modify' and @type != 'delete' and @type != 'checkboxes' and @type != 'simple_checkboxes'">
                    <th class="ll_header" nowrap="nowrap"  valign="middle">
                        <xsl:choose>
                            <xsl:when test="$disp_action=1 and $name != ''">
                               <xsl:variable name="asc">
                                    <xsl:choose>
                                        <xsl:when test="../../List/@cur_sort_field=@name and ../../List/@cur_sort_asc=1">desc</xsl:when>
                                        <xsl:when test="../../List/@cur_sort_field=@name">asc</xsl:when>
                                        <xsl:otherwise>asc</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>

                                <xsl:call-template name="header-button">
                                    <xsl:with-param name="name">sortby_<xsl:value-of select="@name"/>_<xsl:value-of select="$asc"/></xsl:with-param>
                                    <xsl:with-param name="value" select="$name"/>
                                    <xsl:with-param name="callback" select="name(/*)"/>
                                </xsl:call-template>

                            </xsl:when>
                            
                            <xsl:otherwise>
								<span class="mioga-list-title">
                                    <xsl:value-of select="$name"/>
								</span>
                            </xsl:otherwise>
                            
                        </xsl:choose>                
                        
                        
                    </th>
                </xsl:when>
                
                <xsl:when test="@type = 'checkboxes' and $disp_action=1">
                    <th class="ll_header" nowrap="nowrap">
						<span class="mioga-list-title">
                            <xsl:apply-templates select="../../.." mode="sll-field-name">
                                <xsl:with-param name="name" select="@name"/>
                            </xsl:apply-templates>
						</span>
                    </th>
                </xsl:when>
                
                <xsl:when test="@type = 'simple_checkboxes' and $disp_action=1">
                    <th class="ll_header" nowrap="nowrap">
						<span class="mioga-list-title">
                            <xsl:apply-templates select="../../.." mode="sll-field-name">
                                <xsl:with-param name="name" select="@name"/>
                            </xsl:apply-templates>
                        </span>
                    </th>
                </xsl:when>
                
                <xsl:when test="$disp_action=1">
                    <th class="ll_header" nowrap="nowrap">
						<span class="mioga-list-title">
                            <xsl:call-template name="SllFieldName">
                                <xsl:with-param name="name" select="@type"/>
                            </xsl:call-template>
                        </span>
                    </th>
                </xsl:when>
                
            </xsl:choose>
        </xsl:if>
        
    </xsl:for-each>
</xsl:template>


<!-- =================================
     Large List body handler
     ================================= -->

<xsl:template name="SllListRow">
    <xsl:param name="disp_action">1</xsl:param>
    <xsl:param name="callback" select="../../.."/>
    <xsl:param name="callback-name"><xsl:value-of select="local-name(../../..)"/></xsl:param>

    <xsl:for-each select="./*">
        
        <xsl:choose>
            <xsl:when test="@type = 'date'">
                <td nowrap="nowrap">
                    <xsl:if test="day">
                        <xsl:call-template name="LocalDate"/>
                    </xsl:if>
                </td>
            </xsl:when>

            <xsl:when test="@type = 'datetime'">
                <td nowrap="nowrap">
                    <xsl:if test="day">
                        <xsl:call-template name="LocalDateTime"/>
                    </xsl:if>
                </td>
            </xsl:when>

            <xsl:when test="@type = 'dateabr'">
                <td nowrap="nowrap">
                    <xsl:if test="day">
                        <xsl:call-template name="LocalDateAbr"/>
                    </xsl:if>
                </td>
            </xsl:when>

            <xsl:when test="@type = 'dateshort'">
                <td nowrap="nowrap">
                    <xsl:if test="day">
                        <xsl:call-template name="LocalDateShort"/>
                    </xsl:if>
                </td>
            </xsl:when>

            <xsl:when test="@type = 'filesize'">
                <td nowrap="nowrap">
                    <xsl:apply-templates select="filesize"/>
                </td>
            </xsl:when>

            <xsl:when test="$disp_action=0 and @type!='rowid' and @type!='modify' and @type!='delete' and @type!='checkboxes' and @type != 'hidden' and @type!='simple_checkboxes'">
                <td nowrap="nowrap">
                    <xsl:apply-templates select="$callback" mode="sll-translate-values">
                        <xsl:with-param name="name" select="local-name(.)"/>
                        <xsl:with-param name="value" select="."/>
                        <xsl:with-param name="row" select=".."/>
                    </xsl:apply-templates>
                </td>
            </xsl:when>
            
            <xsl:when test="@type='modify' and $disp_action=1">
                <td align="center" nowrap="nowrap">
                    <xsl:choose>
                        <xsl:when test="@enabled=1">
                            <a href="{@function}?{../rowid/@name}={../rowid}">
                                <img src="{$image_uri}/16x16/actions/document-edit.png" >
                                    <xsl:attribute name="alt">
                                        <xsl:call-template name="SllAltName">
                                            <xsl:with-param name="name">modify</xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:attribute>

                                    <xsl:attribute name="title">
                                        <xsl:call-template name="SllAltName">
                                            <xsl:with-param name="name">modify</xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:attribute>
                                </img>
                            </a>
                        </xsl:when>
                        <xsl:otherwise>
                            &#160;
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
            </xsl:when>
            
            <xsl:when test="@type='delete' and $disp_action=1">
                <td align="center" nowrap="nowrap">
                    <xsl:choose>
                        <xsl:when test="@enabled=1">
                            <input type="checkbox" name="select_delete_{../rowid}" value="1">
                                <xsl:if test="@checked=1">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input>
                        </xsl:when>
                        <xsl:otherwise>
                            &#160;
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
            </xsl:when>
            
            <xsl:when test="@type='checkboxes' and $disp_action=1">
                <td align="center" nowrap="nowrap">
                    <xsl:choose>
                        <xsl:when test="@enabled=1">
                            <input type="checkbox" name="select_{@name}_{../rowid}" value="1">
                                <xsl:if test="@checked=1">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input>
                        </xsl:when>
                        <xsl:otherwise>
                            &#160;
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
            </xsl:when>
            
            <xsl:when test="@type='simple_checkboxes' and $disp_action=1">
                <td align="center" nowrap="nowrap">
                    <xsl:choose>
                        <xsl:when test="@enabled=1">
                            <input type="checkbox" name="select_simple_{@name}_{../rowid}" value="1">
                                <xsl:if test="@checked=1">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input>
                        </xsl:when>
                        <xsl:otherwise>
                            &#160;
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
            </xsl:when>
            
            <xsl:when test="@type = 'normal'">
                <td nowrap="nowrap">
                    <xsl:apply-templates select="$callback" mode="sll-translate-values">
                        <xsl:with-param name="name" select="local-name(.)"/>
                        <xsl:with-param name="value" select="."/>
                        <xsl:with-param name="row" select=".."/>
                    </xsl:apply-templates>
                </td>
            </xsl:when>
            
            <xsl:when test="@type = 'link'">
                <td nowrap="nowrap">
                    <a href="{@url}?{../rowid/@name}={../rowid}">
                        <xsl:apply-templates select="$callback" mode="sll-translate-values">
                            <xsl:with-param name="name" select="local-name(.)"/>
                            <xsl:with-param name="value" select="."/>
                            <xsl:with-param name="row" select=".."/>
                        </xsl:apply-templates>
                    </a>
                </td>
            </xsl:when>
            
            <xsl:when test="@type = 'email'">
                <td nowrap="nowrap">
                    <a href="mailto:{.}" alt="{.}" title="{.}">
                        <xsl:choose>
                            <xsl:when test="string-length(.) > 20">
                                <xsl:value-of select="substring(., 0, 17)"/>...
                            </xsl:when>
                            <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
                        </xsl:choose>
                    </a>
                </td>
            </xsl:when>
            
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
        
    </xsl:for-each>
</xsl:template>


<xsl:template name="SllListActions">
    <xsl:param name="disp_action">1</xsl:param>
    <xsl:param name="no-lpp">0</xsl:param>
    <xsl:param name="callback" select=".."/>
    <xsl:param name="callback-name"><xsl:value-of select="local-name(..)"/></xsl:param>

    <xsl:if test="count(./fields/field[@type='delete'])!=0 or count(./fields/field[@type='checkboxes'])!=0">
        <xsl:if test="count(./fields/field) &lt; 3">
            <xsl:message>
              <xsl:value-of select="mioga:gettext('LargeList should have more than three displayed columns')"/>
            </xsl:message>
        </xsl:if>

        <tr>

            <xsl:for-each select="List/Row[1]/*">
                
                <xsl:if test="(@type = 'delete' or @type='checkboxes') and count(preceding-sibling::*[@type = 'delete' or @type='checkboxes']) = 0">
                    <xsl:variable name="colspan" select="position() - count(preceding-sibling::*[@type = 'rowid' or @type='hidden']) - 1"/>
                    <xsl:choose>
                        <xsl:when test="$no-lpp=0">
                            <th colspan="{$colspan}">
                                <xsl:call-template name="LinePerPage">
                                    <xsl:with-param name="method_context" select="$callback-name"/>
                                    <xsl:with-param name="curval" select="../../@nb_elem_per_page"/>
                                </xsl:call-template>
                            </th>
                        </xsl:when>
                        <xsl:otherwise>
                            <th colspan="{$colspan}">&#160;</th>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>


                <xsl:choose>
                    <xsl:when test="@type='checkboxes'">
                        <th align="center">
                            <xsl:apply-templates select="$callback" mode="sll-checkboxes-button">
                                <xsl:with-param name="name" select="@name"/>
                            </xsl:apply-templates>
                        </th>
                    </xsl:when>
                    
                    <xsl:when test="@type='delete'">
                        <th align="center">
                            <xsl:call-template name="delete-form-button">
                                <xsl:with-param name="name">sll_delete_act</xsl:with-param>
                                <xsl:with-param name="value" select="../../@name"/>
                            </xsl:call-template>
                        </th>
                    </xsl:when>
                    <xsl:when test="@type != 'rowid'  and @type != 'hidden' and count(preceding-sibling::*[@type = 'delete' or @type='checkboxes']) > 0">
                        <th>&#160;</th>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
            
        </tr>
    </xsl:if>

</xsl:template>


<xsl:template match="SimpleLargeList" mode="list-body"  name="SllListBody">
    <xsl:param name="disp_action">1</xsl:param>
    <xsl:param name="no-lpp">0</xsl:param>
    <xsl:param name="callback" select=".."/>
    <xsl:param name="callback-name"><xsl:value-of select="local-name(..)"/></xsl:param>
    
    <xsl:for-each select="List/Row">
       
        <tr>
            <xsl:choose>
                <xsl:when test="position() mod 2 = 1">
					<xsl:attribute name="class">list-body odd</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
					<xsl:attribute name="class">list-body even</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
                

            <xsl:call-template name="SllListRow">
                <xsl:with-param name="callback" select="$callback"/>
                <xsl:with-param name="callback-name" select="$callback-name"/>
                <xsl:with-param name="disp_action" select="$disp_action"/>
            </xsl:call-template>
        </tr>
    </xsl:for-each>
        
    <xsl:call-template name="SllListActions">
        <xsl:with-param name="callback" select="$callback"/>
        <xsl:with-param name="callback-name" select="$callback-name"/>
        <xsl:with-param name="disp_action" select="$disp_action"/>                
        <xsl:with-param name="no-lpp" select="$no-lpp"/>
    </xsl:call-template>

</xsl:template>
    


<!-- =================================
     Search form body
     ================================= -->

<xsl:template name="SllSearchFormBody">
    <xsl:param name="callback"><xsl:value-of select="local-name(../..)"/></xsl:param>
    <xsl:param name="label"/>

    <xsl:variable name="searchtitle">
        <xsl:call-template name="MiogaFormTranslation">
            <xsl:with-param name="label" select="$label"/>
        </xsl:call-template>
    </xsl:variable>
    
    <td nowrap="1">
        <xsl:value-of select="$searchtitle"/>&#160;:&#160;
    </td>
    <td>
        <select name="sll_search_field">

            <xsl:for-each select="field">
                <option value="{@name}">
                    <xsl:if test="../curval/sll_search_field = @name">
                        <xsl:attribute name="selected">1</xsl:attribute>
                    </xsl:if>

                    <xsl:apply-templates select="../../.." mode="sll-field-name">
                        <xsl:with-param name="name" select="@name"/>
                    </xsl:apply-templates>

                </option>
            </xsl:for-each>

        </select>
    </td>
    <td nowrap="1">
        <select name="sll_start_with">
            <option value="0">

                <xsl:if test="curval/sll_start_with=0">
                    <xsl:attribute name="selected">1</xsl:attribute>
                </xsl:if>

                <xsl:call-template name="SllFieldName">
                    <xsl:with-param name="name">not_begin_with</xsl:with-param>
                </xsl:call-template>

            </option>
            <option value="1">

                <xsl:if test="curval/sll_start_with=1">
                    <xsl:attribute name="selected">1</xsl:attribute>
                </xsl:if>

                <xsl:call-template name="SllFieldName">
                    <xsl:with-param name="name">begin_with</xsl:with-param>
                </xsl:call-template>

            </option>
        </select>
    </td>
    <td>
        <input type="text" name="sll_search_text" value="{curval/sll_search_text}"/>
    </td>
    <td>
        <xsl:call-template name="search-form-button">
            <xsl:with-param name="name">sll_search_act</xsl:with-param>
            <xsl:with-param name="formname"><xsl:value-of select="$callback"/></xsl:with-param>
        </xsl:call-template>
    </td>
</xsl:template>



<!-- =================================
     Search main template
     ================================= -->

<xsl:template match="searchform">
    <xsl:param name="tablewidth"/>
    <xsl:param name="callback"><xsl:value-of select="local-name(../..)"/></xsl:param>
    <xsl:param name="label"/>

    <form method="GET" action="{$callback}">

        <table cellspacing="0" cellpadding="5" border="0" class="mioga-sll-619">
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="2" border="0" class="mioga-sll-619">
                        <tr>
                            <td width="100%">&#160;</td>
                            
                            <xsl:call-template name="SllSearchFormBody">
                                <xsl:with-param name="label" select="$label"/>
                            </xsl:call-template>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </form>
    
</xsl:template>



<!-- =================================
     Advanced search fields
     ================================= -->
<xsl:template name="SllAdvSearchFieldContent">
    <xsl:param name="name"/>
    <xsl:param name="value"/>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label" select="$name"/>
        <xsl:with-param name="value" select="$value"/>
    </xsl:call-template>

</xsl:template>

<xsl:template name="searchform-inputs">
    <xsl:param name="nb-search-column"/>
    <xsl:param name="cur-col"/>
    
    <xsl:for-each select="field[position() mod $nb-search-column = $cur-col]">
        <xsl:apply-templates select="../../.." mode="advanced-search-field-content">
            <xsl:with-param name="name">sll_search_<xsl:value-of select="@name"/></xsl:with-param>
            <xsl:with-param name="value" select="@value"/>
        </xsl:apply-templates>
        
    </xsl:for-each>

    <xsl:if test="$cur-col != 0">
        <xsl:if test="$nb-search-column != 1">
            <xsl:call-template name="MiogaFormVertSep"/>
        </xsl:if>

        <xsl:call-template name="searchform-inputs">
            <xsl:with-param name="nb-search-column" select="$nb-search-column"/>
            <xsl:with-param name="cur-col" select="($cur-col+1) mod $nb-search-column"/>
        </xsl:call-template>
    </xsl:if>

</xsl:template>


<xsl:template match="searchform" mode="MiogaFormBody">
    <xsl:param name="param"/>

    <xsl:call-template name="searchform-inputs">
        <xsl:with-param name="nb-search-column" select="$param"/>
        <xsl:with-param name="cur-col">1</xsl:with-param>
    </xsl:call-template>
    
</xsl:template>

<xsl:template match="searchform" mode="MiogaFormButton">
    <xsl:variable name="callback" select="local-name(../..)"/>


    <xsl:call-template name="search-form-button">
        <xsl:with-param name="name">sll_adv_search_act</xsl:with-param>
    </xsl:call-template>

    <xsl:if test="field[@value!='']">
        &#160;
        <xsl:call-template name="view-all-button">
            <xsl:with-param name="href"><xsl:value-of select="$callback"/>?<xsl:for-each select="field">sll_search_<xsl:value-of select="@name"/>=&amp;<xsl:if test="position()=last()">sll_adv_search_act=Rechercher</xsl:if></xsl:for-each></xsl:with-param>
        </xsl:call-template>                
    </xsl:if>

</xsl:template>

<!-- =================================
     Advanced search main template
     ================================= -->

<xsl:template match="searchform" mode="advanced">
    <xsl:param name="label"/>
    <xsl:param name="nb-search-column"/>
    <xsl:param name="callback"><xsl:value-of select="local-name(../..)"/></xsl:param>
	<xsl:param name="highlight">
		<xsl:choose>
			<xsl:when test="field[@value!='']">
				1
			</xsl:when>
			<xsl:otherwise>
				0
			</xsl:otherwise>
		</xsl:choose>
	</xsl:param>

    <xsl:call-template name="MiogaForm">
        <xsl:with-param name="label" select="$label"/>
        <xsl:with-param name="action" select="$callback"/>
        <xsl:with-param name="method">POST</xsl:with-param>
        <xsl:with-param name="param" select="$nb-search-column"/>
        <xsl:with-param name="highlight" select="$highlight"/>
    </xsl:call-template>
</xsl:template>



<!-- =================================
     Large List deletion confirmation handler
     ================================= -->


<xsl:template match="AskConfirmForDelete" mode="list-title">
    <xsl:apply-templates select=".." mode="list-title">
        <xsl:with-param name="disp_action">0</xsl:with-param>
        <xsl:with-param name="callback" select="../.."/>
    </xsl:apply-templates>
</xsl:template>

<xsl:template match="AskConfirmForDelete" mode="list-body">
    <xsl:call-template name="SllListBody">
        <xsl:with-param name="disp_action">0</xsl:with-param>
        <xsl:with-param name="callback" select="../.."/>
    </xsl:call-template>
</xsl:template>


<xsl:template match="AskConfirmForDelete">
    <xsl:param name="bgindex"/>
    <xsl:param name="fgindex"/>
    <xsl:param name="topindex"/>
    <xsl:param name="bottomindex"/>
    <xsl:param name="bgcolor"/>
    <xsl:param name="tablewidth"/>
    <xsl:param name="displaytitle"/>
    <xsl:param name="callback" select="local-name(../..)"/>

    <table xsl:use-attribute-sets="simple_table">
        <tr>
            <td><img src="{$image_uri}/transparent_fill.gif" width="20" height="1" /></td>
            <td><img src="{$image_uri}/16x16/actions/arrow-right.png" /></td>
            <td><img src="{$image_uri}/transparent_fill.gif" width="10" height="1" /></td>
            <td><span class="mioga-list-title"><xsl:apply-templates select="." mode="confirm-text"/></span></td>
        </tr>
    </table>
    
    <br/>

    <center>
        <xsl:call-template name="LargeList">
            <xsl:with-param name="topindex">0</xsl:with-param>
            <xsl:with-param name="bottomindex">0</xsl:with-param>
            <xsl:with-param name="tablewidth">60%</xsl:with-param>
            <xsl:with-param name="method_context" select="local-name(../..)"/>
        </xsl:call-template>
    
    </center>

    <br/>
    
    <center>

        <xsl:call-template name="ok-cancel-button">
            <xsl:with-param name="ok-href"><xsl:value-of select="$callback"/>?sll_delete_act=1</xsl:with-param>
            <xsl:with-param name="cancel-href" select="referer"></xsl:with-param>
        </xsl:call-template>

    </center>

</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<!-- Large List success message handler -->
<xsl:template match="SimpleLargeList" mode="success-message">
  <xsl:value-of select="mioga:gettext('Deletion successfully done')"/>
</xsl:template>

<!-- Large List success message handler -->
<xsl:template name="NoSelectedItem">
  <xsl:variable name="message"><xsl:value-of select="mioga:gettext('No selected element')"/></xsl:variable>
  
  <xsl:call-template name="SuccessMessage">
    <xsl:with-param name="message" select="$message"/>
    <xsl:with-param name="referer" select="SuccessMessage/referer"/>
  </xsl:call-template>
</xsl:template>

<xsl:template match="AskConfirmForDelete" mode="confirm-text">
  <xsl:value-of select="mioga:gettext('Are you sure you want to delete the following elements?')"/>
</xsl:template>

<!-- Internal list fields strings -->
<xsl:template name="SllFieldName">
  <xsl:param name="name"/>
  <xsl:choose>
    <xsl:when test="$name = 'modify'"></xsl:when>
    <xsl:when test="$name = 'delete'"></xsl:when>
    <xsl:when test="$name = 'not_begin_with'"><xsl:value-of select="mioga:gettext('contains')"/></xsl:when>
    <xsl:when test="$name = 'begin_with'"><xsl:value-of select="mioga:gettext('begins with')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="SllAltName">
  <xsl:param name="name"/>
  <xsl:choose>
    <xsl:when test="$name = 'modify'"><xsl:value-of select="mioga:gettext('Modify')"/></xsl:when>
    <xsl:when test="$name = 'delete'"><xsl:value-of select="mioga:gettext('Delete')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="view-all-button">
    <xsl:param name="href"/>
    <xsl:call-template name="button">
        <xsl:with-param name="name">show_all</xsl:with-param>
        <xsl:with-param name="href" select="$href"/>
    </xsl:call-template>
</xsl:template>

</xsl:stylesheet>
