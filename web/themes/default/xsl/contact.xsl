<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

  <xsl:output method="html" indent="yes"/>
  <xsl:include href="base.xsl"/>
  <xsl:include href="contact-edition.xsl"/>
  <xsl:include href="contact-visu.xsl"/>
  <xsl:include href="large_list.xsl"/>
  <xsl:include href="scriptaculous.xsl"/>
  <xsl:include href="inline_message.xsl"/>

<!-- ===============
	root document
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="scriptaculous-js"/>
	<xsl:call-template name="theme-css"/>
</head>
	<xsl:apply-templates />
</html>
</xsl:template>
  
<!-- ===============================
     BrowseContact
     =============================== -->

<xsl:template name="ListTitlePrintLink">
  <xsl:param name="method_context"/>
  <xsl:param name="name"/>
  <xsl:param name="i18n_name"/>

  <xsl:variable name="listname" select="List/@name"/>
  
  <xsl:variable name="asc">
    <xsl:choose>
      <xsl:when test="List/@cur_sort_field=$name and List/@cur_sort_asc=1">desc</xsl:when>
      <xsl:when test="List/@cur_sort_field=$name">asc</xsl:when>
      <xsl:otherwise>asc</xsl:otherwise>
    </xsl:choose>        
  </xsl:variable>
  
  <th nowrap="1">
    <a href="{$private_bin_uri}/Contact/{$method_context}?sortby_{$listname}=sortby_{$name}_{$asc}">
      <font class="{$mioga-list-title-text-color}"><xsl:value-of select="$i18n_name"/></font>
    </a>
    <xsl:choose>
      <xsl:when test="List/@cur_sort_field=$name and List/@cur_sort_asc=1">
        &#160;
        <a href="{$method_context}?sortby_{$listname}=sortby_{$name}_desc"><img src="{$image_uri}/16x16/actions/view-sort-descending.png" border="0"/></a>
      </xsl:when>
      <xsl:when test="List/@cur_sort_field=$name">
        &#160;
        <a href="{$method_context}?sortby_{$listname}=sortby_{$name}_asc"><img src="{$image_uri}/16x16/actions/view-sort-ascending.png" border="0"/></a>
      </xsl:when>
    </xsl:choose>
  </th>
</xsl:template>

<xsl:template match="BrowseContact" mode="list-body" name="ListBody">
  <xsl:param name="method_context">DisplayMain</xsl:param>

  <form method="GET" action="DisplayMain" name="RemoveContact">
    <xsl:for-each select="List/Row">
      <tr>

    <xsl:choose>
      <xsl:when test="position() mod 2 = 0">
        <xsl:attribute name="class"><xsl:value-of select="$mioga-list-even-row-bg-color" /></xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color" /></xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

    <td nowrap="1"><xsl:value-of select="surname"/>&#160;</td>

    <td nowrap="1">
      <xsl:choose>
        <xsl:when test="/*/action">
          <a href="{/*/referer}?action={/*/action}&amp;id={rowid}"><xsl:value-of select="name"/></a>
        </xsl:when>
        <xsl:when test="/*/Function/DisplayContact/@authorised='1'">
          <a href="{$private_bin_uri}/Contact/DisplayContact?rowid={rowid}"><xsl:value-of select="name"/></a>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="name"/>
        </xsl:otherwise>
      </xsl:choose>
    </td>
    <td><xsl:value-of select="orgname"/>&#160;</td>

    <td nowrap="1">
      <a href="mailto:{email_work}"><xsl:value-of select="email_work" /></a>&#160;
    </td>

    <td nowrap="1"><xsl:value-of select="tel_work"/>&#160;</td>

    <td><xsl:value-of select="addr_work"/>&#160;<xsl:value-of select="postal_code_work"/>&#160;<xsl:value-of select="city_work"/></td>

    <xsl:if test="/*/Function/RemoveContact/@authorised='1'">
      <td align="center">
        <input type="checkbox" name="select_delete_{rowid}"/>
      </td>
    </xsl:if>

    <xsl:if test="/*/Function/EditContact/@authorised='1'">
      <td align="center">
        <a href="{$private_bin_uri}/Contact/EditContact?rowid={rowid}">
          <xsl:variable name="edit">
            <xsl:call-template name="contactTranslation">
              <xsl:with-param name="type">edit</xsl:with-param>
            </xsl:call-template>        
          </xsl:variable>
          <img src="{$image_uri}/16x16/actions/document-edit.png" alt="{$edit}"/>
        </a>
      </td>
    </xsl:if>

    <xsl:if test="/*/Function/ExportVcard/@authorised='1'">
      <td align="center">
        <xsl:variable name="export">
          <xsl:call-template name="contactTranslation">
            <xsl:with-param name="type">export</xsl:with-param>
          </xsl:call-template>        
        </xsl:variable>
        <a href="{$private_bin_uri}/Contact/ExportVcard?rowid={rowid}&amp;name={name}">
          <img src="{$image_uri}/16x16/actions/download.png" border="0" alt="{$export}"/>
        </a>            
      </td>
    </xsl:if>
  </tr> 
  </xsl:for-each>

  <tr class="{$mioga-list-title-bg-color}">
      
      <th colspan="6" align="left">
          <xsl:call-template name="LinePerPage">
              <xsl:with-param name="method_context" select="method"/>              
              <xsl:with-param name="listname" select="List/@name"/>              
          </xsl:call-template>
      </th>
      <xsl:if test="/*/Function/RemoveContact/@authorised='1'">
          <th align="center">
              <xsl:call-template name="delete-form-button">
                  <xsl:with-param name="name">ActionDelete</xsl:with-param>
              </xsl:call-template>
          </th>
      </xsl:if>
      <xsl:if test="/*/Function/EditContact/@authorised='1'">
          <td>&#160;</td>
      </xsl:if>
      <xsl:if test="/*/Function/ExportVcard/@authorised='1'">
          <td>&#160;</td>
      </xsl:if>
  </tr>

  </form>
</xsl:template>

<xsl:template match="BrowseContact" >
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">applications.html#contact</xsl:with-param>
    </xsl:call-template>

      <!-- Status message -->
      <xsl:if test="@message != ''">
         <xsl:call-template name="InlineMessage">
            <xsl:with-param name="type" select="@type"/>
            <xsl:with-param name="message" select="@message"/>
         </xsl:call-template>
      </xsl:if>

    <xsl:call-template name="MiogaTitle"/>
      
    <br/>
    
    <xsl:call-template name="error"/>
    
    <xsl:call-template name="MiogaAction">
      <xsl:with-param name="wrap">1</xsl:with-param>
    </xsl:call-template>

      
    <br/>


    <center>
      <xsl:apply-templates select="group_index">
        <xsl:with-param name="method_context" select="method"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="alpha_index">
        <xsl:with-param name="method_context" select="method"/>
      </xsl:apply-templates>
      
      <xsl:call-template name="LargeList">
          <xsl:with-param name="tablewidth">95%</xsl:with-param>
          <xsl:with-param name="topindex">0</xsl:with-param>
          <xsl:with-param name="bottomindex">1</xsl:with-param>
          <xsl:with-param name="lppinbottomindex">0</xsl:with-param>
          <xsl:with-param name="method_context" select="method"/>
      </xsl:call-template>
      

      <xsl:call-template name="SearchForm"/>
        

    </center>

  </body>
</xsl:template>

<!-- ===============================
     Index
     =============================== -->
<xsl:template match="group_index">
    <xsl:param name="method_context"/>
    
  <table border="0" class="{$mioga-list-even-row-bg-color}" cellpadding="4" cellspacing="0" width="95%" >
    <tr align="left" >
      <th nowrap="1">
        <xsl:call-template name="contactTranslation">
          <xsl:with-param name="type">directory</xsl:with-param>
        </xsl:call-template>        
      </th>

      <xsl:for-each select="item">
        <xsl:choose>
          <xsl:when test="@is_selected='1'">
            <th nowrap="1"><xsl:value-of select="." /></th>
          </xsl:when>
          
          <xsl:otherwise>
            <th nowrap="1">
                <a href="{$bin_uri}/{.}/Contact/{$method_context}?group={@id}"><xsl:value-of select="."/></a>
            </th>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>

      <th width="100%">&#160;</th>
    </tr>
  </table>
</xsl:template>

<xsl:template match="alpha_index">
    <xsl:param name="method_context"/>
  <table border="0" class="{$mioga-list-even-row-bg-color}" cellpadding="4" cellspacing="0" width="95%" >
    <tr align="left" >
      <th nowrap="1">
        <xsl:call-template name="contactTranslation">
          <xsl:with-param name="type">index</xsl:with-param>
        </xsl:call-template>        
      </th>

      <xsl:for-each select="item">
        <xsl:choose>
          <xsl:when test="@is_selected='1'">
            <th nowrap="1"><xsl:value-of select="." /></th>
          </xsl:when>
          
          <xsl:otherwise>
            <th nowrap="1">
              <a href="{$private_bin_uri}/Contact/{$method_context}?alpha={@id}"><xsl:value-of select="."/></a>
            </th>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>

    <th width="100%">&#160;</th>
  </tr>

  </table>
</xsl:template>
 


<!-- ===============================
     ImportVcard
     =============================== -->
<xsl:template match="ImportVcard" mode="form-body">
    <tr>
        <td>
          <xsl:call-template name="contactTranslation">
            <xsl:with-param name="type">import-directory</xsl:with-param>
          </xsl:call-template>
        </td>
        <td>
            <select name="group">
                <xsl:for-each select="group">
                    <xsl:element name="option">
                        <xsl:attribute name="value"><xsl:value-of select="@id" /></xsl:attribute>
                        <xsl:if test="@selected=1">
                            <xsl:attribute name="selected">1</xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="."/>
                    </xsl:element>
                </xsl:for-each>
            </select>
        </td>
    </tr>
    <tr>
        <td>
        <xsl:call-template name="contactTranslation">
          <xsl:with-param name="type">file-name</xsl:with-param>
        </xsl:call-template>
        </td>
        <td>
            <xsl:element name="input">
                <xsl:attribute name="type">file</xsl:attribute>
                <xsl:attribute name="name">vCardFile</xsl:attribute>
                <xsl:attribute name="size">30</xsl:attribute>
                <xsl:attribute name="maxlength">255</xsl:attribute>
            </xsl:element>
        </td>
    </tr>
    
</xsl:template>

<xsl:template match="ImportVcard" mode="form-button">
    <tr>
        <td colspan="2" align="center">
            <xsl:call-template name="ok-cancel-form-button">
                <xsl:with-param name="name">ActionUploadOk</xsl:with-param>
                <xsl:with-param name="referer" select="referer"/>
            </xsl:call-template>
        </td>
    </tr>
    
</xsl:template>

<xsl:template match="ImportVcard">
<body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">applications.html#contact</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>
    
    <xsl:call-template name="error"/>
    
    <center>
        <xsl:call-template name="form">
            <xsl:with-param name="method">POST</xsl:with-param>
            <xsl:with-param name="enctype">multipart/form-data</xsl:with-param>
            <xsl:with-param name="name">
              <xsl:call-template name="contactTranslation">
                <xsl:with-param name="type">import-contact</xsl:with-param>
              </xsl:call-template>        
            </xsl:with-param>
            <xsl:with-param name="action">ImportVcard</xsl:with-param>
        </xsl:call-template>
    </center>
</body> 
</xsl:template>

<!-- 
     AskConfirmForDelete
     -->

<xsl:template match="AskConfirmForDelete" mode="list-title">
  <xsl:call-template name="ListTitle">
    <xsl:with-param name="method_context">RemoveContact</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="AskConfirmForDelete" mode="list-body">
  <xsl:call-template name="ListBody">
    <xsl:with-param name="method_context">RemoveContact</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="AskConfirmForDelete" mode="confirm-body">
  <center>
    <xsl:call-template name="LargeList">
      <xsl:with-param name="tablewidth">90%</xsl:with-param>
      <xsl:with-param name="method_context">RemoveContact</xsl:with-param>
      <xsl:with-param name="topindex">0</xsl:with-param>
      <xsl:with-param name="bottomindex">0</xsl:with-param>
    </xsl:call-template>
  </center>
</xsl:template>

<xsl:template match="AskConfirmForDelete">
    <xsl:call-template name="Confirm">
      <xsl:with-param name="title">
        <xsl:call-template name="contactTranslation">
          <xsl:with-param name="type">delete-contact</xsl:with-param>
        </xsl:call-template>        
      </xsl:with-param>
      <xsl:with-param name="help_uri">/aide/contact.html</xsl:with-param>
      <xsl:with-param name="question">
        <xsl:call-template name="contactTranslation">
          <xsl:with-param name="type">delete-contact-question</xsl:with-param>
        </xsl:call-template>        
      </xsl:with-param>
      <xsl:with-param name="valid_action_uri"><xsl:value-of select="$private_bin_uri"/>/Contact/RemoveContact</xsl:with-param>
      <xsl:with-param name="cancel_uri"><xsl:value-of select="referer"/></xsl:with-param>
      <xsl:with-param name="display_body">1</xsl:with-param>
    </xsl:call-template>
</xsl:template>


<xsl:template name="SearchForm">
  <form method="GET" action="{$private_bin_uri}/Contact/SearchContact" name="SearchContact">
    <table cellspacing="0" cellpadding="5" border="0" class="{$mioga-list-even-row-bg-color}" width="95%">
      <tr class="{$mioga-bg-color}"><td>
        <img src="{$image_uri}/transparent_fill.gif" width="5" height="10"/>    
      </td></tr>
      <tr><td>
      <table cellspacing="0" cellpadding="2" border="0" class="{$mioga-list-even-row-bg-color}" width="100%">
      
        <tr>
          <td width="100%">&#160;</td>
          <td nowrap="1">
            <xsl:call-template name="contactTranslation">
              <xsl:with-param name="type">search-in</xsl:with-param>
            </xsl:call-template>
          </td>
          <td>
              <select  name="group">
                  <option value="1">
                      <xsl:if test="search_group=1">
                          <xsl:attribute name="selected">1</xsl:attribute>
                      </xsl:if>
                      <xsl:call-template name="contactTranslation">
                        <xsl:with-param name="type">current-directory</xsl:with-param>
                      </xsl:call-template>
                  </option>
                  <option value="2">
                      <xsl:if test="search_group=2">
                          <xsl:attribute name="selected">1</xsl:attribute>
                      </xsl:if>
                      <xsl:call-template name="contactTranslation">
                        <xsl:with-param name="type">all-directory</xsl:with-param>
                      </xsl:call-template>
                  </option>
              </select>
          </td>
          <td nowrap="1">
            <xsl:call-template name="contactTranslation">
              <xsl:with-param name="type">which</xsl:with-param>
            </xsl:call-template>
          </td>
          <td>
            <select  name="var">
              <option value="1">
                <xsl:if test="search_var=1">
                  <xsl:attribute name="selected">1</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="contactTranslation">
                  <xsl:with-param name="type">first-name</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="2">
                <xsl:if test="search_var=2">
                  <xsl:attribute name="selected">1</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="contactTranslation">
                  <xsl:with-param name="type">last-name</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="3">
                <xsl:if test="search_var=3">
                  <xsl:attribute name="selected">1</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="contactTranslation">
                  <xsl:with-param name="type">company</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="4">
                <xsl:if test="search_var=4">
                  <xsl:attribute name="selected">1</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="contactTranslation">
                  <xsl:with-param name="type">city</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="5">
                <xsl:if test="search_var=5">
                  <xsl:attribute name="selected">1</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="contactTranslation">
                  <xsl:with-param name="type">state</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="6">
                <xsl:if test="search_var=6">
                  <xsl:attribute name="selected">1</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="contactTranslation">
                  <xsl:with-param name="type">country</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="7">
                <xsl:if test="search_var=7">
                  <xsl:attribute name="selected">1</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="contactTranslation">
                  <xsl:with-param name="type">title</xsl:with-param>
                </xsl:call-template>
              </option>
            </select>
          </td>
          <td>
            <select  name="type">
              <option value="1">
                <xsl:if test="search_type=1">
                  <xsl:attribute name="selected">1</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="contactTranslation">
                  <xsl:with-param name="type">starts-with</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="2">
                <xsl:if test="search_type=2">
                  <xsl:attribute name="selected">1</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="contactTranslation">
                  <xsl:with-param name="type">contains</xsl:with-param>
                </xsl:call-template>
             </option>
            </select>
          </td>
          <td>
            <input type="text" name="text" value="{search_text}"/>
          </td>
          <td>
              <xsl:call-template name="search-form-button">
                  <xsl:with-param name="name">ActionSearchOk</xsl:with-param>
              </xsl:call-template>
          </td>
        </tr>
        
      </table>
      </td></tr>
    </table>
  </form>
</xsl:template>

<!-- ===============================
     SearchContact
     =============================== -->
<xsl:template match="SearchContact" mode="list-title">
  <xsl:call-template name="ListTitle">
    <xsl:with-param name="method_context">SearchContact</xsl:with-param>
  </xsl:call-template>
</xsl:template>


<xsl:template match="SearchContact" mode="list-body">
  <xsl:call-template name="ListBody">
    <xsl:with-param name="method_context">SearchContact</xsl:with-param>
  </xsl:call-template>
</xsl:template>


<xsl:template match="SearchContact" >
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">applications.html#contact</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>
    <br/>

    <xsl:call-template name="error"/>
    
    <xsl:call-template name="MiogaAction">
       <xsl:with-param name="wrap">1</xsl:with-param>
    </xsl:call-template>

      
    <br/>

    <xsl:call-template name="LargeList">
      <xsl:with-param name="topindex">0</xsl:with-param>
      <xsl:with-param name="method_context">SearchContact</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="SearchForm"/>
  </body>
</xsl:template>

<xsl:template match="*" mode="MiogaActionBody">
  <xsl:variable name="function" select="local-name(.)"/>
  
  <xsl:choose>
    <xsl:when test="$function='SearchContact'">
      <xsl:choose>
        <xsl:when test="action">
          <xsl:call-template name="MiogaActionItem">
            <xsl:with-param name="label">Back</xsl:with-param>
            <xsl:with-param name="href">SearchEngine</xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="MiogaActionItem">
            <xsl:with-param name="label">BackApp</xsl:with-param>
            <xsl:with-param name="href" select="referer"/>
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:call-template name="MiogaActionItem">
            <xsl:with-param name="label">Back</xsl:with-param>
            <xsl:with-param name="href" select="referer"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>

    <xsl:when test="$function='BrowseContact'">
      <xsl:if test="Function/AddContact/@authorised='1'">
        <xsl:call-template name="MiogaActionItem">
          <xsl:with-param name="label">AddContact</xsl:with-param>
          <xsl:with-param name="href">AddContact</xsl:with-param>
        </xsl:call-template>
      </xsl:if>
      
      <xsl:if test="Function/ImportVcard/@authorised='1'">
        <xsl:call-template name="MiogaActionItem">
          <xsl:with-param name="label">ImportVcard</xsl:with-param>
          <xsl:with-param name="href">ImportVcard</xsl:with-param>
        </xsl:call-template>
      </xsl:if>
                
      <xsl:if test="referer">
        <xsl:call-template name="MiogaActionItem">
          <xsl:with-param name="label">Back</xsl:with-param>
          <xsl:with-param name="href" select="referer"/>
        </xsl:call-template>
      </xsl:if>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="ErrorMessage">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">applications.html#contact</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>
    <center>
      <xsl:call-template name="error"/>
      
      <br/><br/>

      <xsl:variable name="back">
        <xsl:call-template name="contactTranslation">
          <xsl:with-param name="type">back</xsl:with-param>
        </xsl:call-template>        
      </xsl:variable>
      
      <a href="{referer}"><img src="{$image_uri}/retour.gif" border="0" alt=" {$back} "/></a>
    </center>

 
  </body>
</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<xsl:template name="error">
  <xsl:if test="error!=0">
    <center><p><font color="red"><b>
    <xsl:choose>
        <xsl:when test="error='100'"><xsl:value-of select='mioga:gettext("You don&apos;t have writing rights")'/></xsl:when>
        <xsl:when test="error='200'"><xsl:value-of select="mioga:gettext('Invalid vCard file')"/></xsl:when>
        <xsl:when test="error='300'"><xsl:value-of select="mioga:gettext('Invalid Email->Professional field')"/></xsl:when>
        <xsl:when test="error='301'"><xsl:value-of select="mioga:gettext('Invalid Email->Personal field')"/></xsl:when>
        <xsl:when test="error='302'"><xsl:value-of select="mioga:gettext('Invalid Telephone->Work field')"/></xsl:when>
        <xsl:when test="error='303'"><xsl:value-of select="mioga:gettext('Invalid Telephone->Domicile field')"/></xsl:when>
        <xsl:when test="error='304'"><xsl:value-of select="mioga:gettext('Invalid Telephone->Fax field')"/></xsl:when>
        <xsl:when test="error='305'"><xsl:value-of select="mioga:gettext('Invalid Telephone->Mobile field')"/></xsl:when>
        <xsl:when test="error='306'"><xsl:value-of select="mioga:gettext('Invalid photo')"/></xsl:when>
        <xsl:when test="error='307'"><xsl:value-of select="mioga:gettext('Name field is mandatory')"/></xsl:when>
        <xsl:when test="error='308'"><xsl:value-of select="mioga:gettext('Invalid rowid')"/></xsl:when>
        <xsl:when test="error='309'"><xsl:value-of select="mioga:gettext('Invalid filename')"/></xsl:when>
        <xsl:when test="error='310'"><xsl:value-of select="mioga:gettext('Invalid parameters')"/></xsl:when>
      </xsl:choose>
    </b></font></p></center>
  </xsl:if>
</xsl:template>

<xsl:template name="contactTranslation">
  <xsl:param name="type"/>

  <xsl:choose>
    <xsl:when test="$type='directory'"><xsl:value-of select="mioga:gettext('Directories:')"/>&#160;</xsl:when>
    <xsl:when test="$type='index'"><xsl:value-of select="mioga:gettext('Index:')"/>&#160;</xsl:when>
    <xsl:when test="$type='import-directory'"><xsl:value-of select="mioga:gettext('Import to directory:')"/>&#160;</xsl:when>
    <xsl:when test="$type='file-name'"><xsl:value-of select="mioga:gettext('Filename:')"/>&#160;</xsl:when>
    <xsl:when test="$type='import-contact'"><xsl:value-of select="mioga:gettext('Import contact')"/></xsl:when>
    <xsl:when test="$type='delete-contact'"><xsl:value-of select="mioga:gettext('Delete contact')"/></xsl:when>
    <xsl:when test="$type='delete-contact-question'"><xsl:value-of select="mioga:gettext('Are you sure you want to delete the following contacts?')"/></xsl:when>
    <xsl:when test="$type='search-in'"><xsl:value-of select="mioga:gettext('Search for contact in')"/></xsl:when>
    <xsl:when test="$type='current-directory'"><xsl:value-of select="mioga:gettext('current directory')"/></xsl:when>
    <xsl:when test="$type='all-directory'"><xsl:value-of select="mioga:gettext('all directories')"/></xsl:when>
    <xsl:when test="$type='back'"><xsl:value-of select="mioga:gettext('Back')"/></xsl:when>
    <xsl:when test="$type='which'"><xsl:value-of select="mioga:gettext('which')"/></xsl:when>
    <xsl:when test="$type='last-name'"><xsl:value-of select="mioga:gettext('firstname')"/></xsl:when>
    <xsl:when test="$type='first-name'"><xsl:value-of select="mioga:gettext('lastname')"/></xsl:when>
    <xsl:when test="$type='city'"><xsl:value-of select="mioga:gettext('city')"/></xsl:when>
    <xsl:when test="$type='company'"><xsl:value-of select="mioga:gettext('company')"/></xsl:when>
    <xsl:when test="$type='state'"><xsl:value-of select="mioga:gettext('state')"/></xsl:when>
    <xsl:when test="$type='country'"><xsl:value-of select="mioga:gettext('country')"/></xsl:when>
    <xsl:when test="$type='title'"><xsl:value-of select="mioga:gettext('title')"/></xsl:when>
    <xsl:when test="$type='starts-with'"><xsl:value-of select="mioga:gettext('begin with')"/></xsl:when>
    <xsl:when test="$type='contains'"><xsl:value-of select="mioga:gettext('contains')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="MiogaTitleTranslation">
  <xsl:choose>
    <xsl:when test="name(.)='BrowseContact'"></xsl:when>
    <xsl:when test="name(.)='ImportVcard'"><xsl:value-of select="mioga:gettext('Import vcard format contact')"/></xsl:when>
    <xsl:when test="name(.)='SuccessMessage'"></xsl:when>
    <xsl:when test="name(.)='SearchContact'"></xsl:when>
    <xsl:when test="name(.)='ErrorMessage'"></xsl:when>
    <xsl:when test="name(.)='DisplayContact'"></xsl:when>
  </xsl:choose>
</xsl:template>


<xsl:template name="MiogaActionTranslation">
  <xsl:param name="label"/>
  <xsl:choose>
    <xsl:when test="$label='BackApp'"><xsl:value-of select="mioga:gettext('Return to previous application')"/></xsl:when>
    <xsl:when test="$label='ImportVcard'"><xsl:value-of select="mioga:gettext('Import contact')"/></xsl:when>
    <xsl:when test="$label='AddContact'"><xsl:value-of select="mioga:gettext('Add contact')"/></xsl:when>
    <xsl:when test="$label='Back'"><xsl:value-of select="mioga:gettext('Return to previous page')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="BrowseContact" mode="list-title" name="ListTitle">
  <xsl:param name="method_context">DisplayMain</xsl:param>

  <xsl:call-template name="ListTitlePrintLink">
      <xsl:with-param name="method_context" select="$method_context"/>
      <xsl:with-param name="name">surname</xsl:with-param>
      <xsl:with-param name="i18n_name"><xsl:value-of select="mioga:gettext('Firstname')"/></xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="ListTitlePrintLink">
      <xsl:with-param name="method_context" select="$method_context"/>
      <xsl:with-param name="name">name</xsl:with-param>
      <xsl:with-param name="i18n_name"><xsl:value-of select="mioga:gettext('Lastname')"/></xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="ListTitlePrintLink">
      <xsl:with-param name="method_context" select="$method_context"/>
      <xsl:with-param name="name">orgname</xsl:with-param>
      <xsl:with-param name="i18n_name"><xsl:value-of select="mioga:gettext('Company')"/></xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="ListTitlePrintLink">
      <xsl:with-param name="method_context" select="$method_context"/>
      <xsl:with-param name="name">email_work</xsl:with-param>
      <xsl:with-param name="i18n_name"><xsl:value-of select="mioga:gettext('Professional email')"/></xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="ListTitlePrintLink">
      <xsl:with-param name="method_context" select="$method_context"/>
      <xsl:with-param name="name">tel_work</xsl:with-param>
      <xsl:with-param name="i18n_name"><xsl:value-of select="mioga:gettext('Professional telephone')"/></xsl:with-param>
  </xsl:call-template>

  <xsl:call-template name="ListTitlePrintLink">
      <xsl:with-param name="method_context" select="$method_context"/>
      <xsl:with-param name="name">addr_work</xsl:with-param>
      <xsl:with-param name="i18n_name"><xsl:value-of select="mioga:gettext('Professional address')"/></xsl:with-param>
  </xsl:call-template>
  
  <xsl:if test="/*/Function/RemoveContact/@authorised='1'">
    <th nowrap="1"><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Deletion')"/></font></th>
  </xsl:if>
  <xsl:if test="/*/Function/EditContact/@authorised='1'">
    <th nowrap="1">&#160;</th>
  </xsl:if>
  <xsl:if test="/*/Function/ExportVcard/@authorised='1'">
    <th nowrap="1">&#160;</th>
  </xsl:if>
</xsl:template>

<xsl:template match="SuccessMessage">
    <xsl:call-template name="SuccessMessage">
      <xsl:with-param name="message">
        <xsl:choose>
          <xsl:when test="action = 'create'"><xsl:value-of select="mioga:gettext('Creation')"/></xsl:when>
          <xsl:when test="action = 'modify'"><xsl:value-of select="mioga:gettext('Modification')"/></xsl:when>
          <xsl:when test="action = 'remove'"><xsl:value-of select="mioga:gettext('Deletion')"/></xsl:when>
          <xsl:when test="action = 'import'"><xsl:value-of select="mioga:gettext('Import')"/></xsl:when>
        </xsl:choose>
        <xsl:value-of select="mioga:gettext('correctly done')"/><xsl:if test="action!='import'"></xsl:if>             
      </xsl:with-param>
      <xsl:with-param name="referer" select="referer"/>
    </xsl:call-template>
</xsl:template>
  
</xsl:stylesheet>
