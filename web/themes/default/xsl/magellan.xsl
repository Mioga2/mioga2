<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ext="urn:ext" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<xsl:include href="extjs.xsl"/>
<xsl:include href="tags.xsl"/>

<!-- ==============================
	Root document
	=============================== -->

<xsl:template name="magellan-css">
  <link rel="stylesheet" type="text/css" href="{$theme_uri}/css/magellan.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="IncludeMime?css" media="screen" />
</xsl:template>

<xsl:template name="magellan-js">
  <script src="IncludeMime?js" type="text/javascript"></script>
	<script src="{$theme_uri}/javascript/magellan.js" type="text/javascript"></script>
  <script src="{$theme_uri}/javascript/locale/magellan-lang-{//miogacontext/group/lang}.js" type="text/javascript"></script>
  <script src="{$theme_uri}/javascript/jquery.ellipsis.js" type="text/javascript"></script>
</xsl:template>

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
    <xsl:call-template name="favicon"/>
    <xsl:call-template name="title"/>
    <xsl:call-template name="console-js"/>
	<xsl:call-template name="tags-js"/>
    <script type="text/javascript" src="{$jslib_uri}/scriptaculous/prototype.js"></script>
    <xsl:call-template name="extjs"/>
    <xsl:call-template name="magellan-js"/>
    <xsl:call-template name="mioga-css"/>
    <xsl:call-template name="magellan-css"/>
	<xsl:call-template name="tags-css"/>
	<xsl:call-template name="theme-css"/>
</head>
<body class="magellan">
  <xsl:call-template name="DisplayAppTitle"/>
  <xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- =======================================
	DisplayMain
	======================================= -->

<xsl:template match="DisplayMain">
<xsl:variable name="no_header">
	<xsl:choose>
		<xsl:when test="no_header = 1">1</xsl:when>
		<xsl:when test="no_header = 0">0</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<script type="text/javascript">
	var available_tags = Array (
		"<xsl:call-template name="join">
			<xsl:with-param name="valueList" select="tag"/>
			<xsl:with-param name="separator" select="'&quot;, &quot;'"/>
		</xsl:call-template>"
	);
	var free_tags = <xsl:value-of select="free_tags"/>;
	var tag_does_not_exist = "<xsl:value-of select="mioga:gettext ('This tag does not exist and will be ignored.')"/>";
	var no_header = <xsl:value-of select="$no_header"/>;
</script>
<script type="text/javascript">
Ext.onReady(function() {
  Magellan.app.init({
    rootString: "<xsl:value-of select='Instance'/>",
    homePath: "<xsl:value-of select='miogacontext/uri/private_dav'/>",
    basePath: "<xsl:value-of select='miogacontext/uri/base'/>",
    version: "<xsl:value-of select='Infos/version'/>",
    codename: "<xsl:value-of select='Infos/codename'/>",
    forcedPath: "<xsl:value-of select='Path'/>",
    isUserApp: "<xsl:value-of select='isUserApp'/>",
    canSendMail: <xsl:choose><xsl:when test="CanSendMail">true</xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose>,
	viewMode: "<xsl:value-of select='Preferences/view_mode'/>"
  });
}, Magellan);
</script>

<span id="metrics" class="x-panel-body x-editable">&#160;</span>
<div id="rights_mgt" style="display: none;" class="x-panel-body">
  <iframe id="rights_iframe" scrolling="auto" frameborder="0" name="rights_iframe" height="100%" width="100%">&#160;</iframe>
</div>

<div id="props_mgt" style="display: none;" class="x-panel-body">
	<form class="form" id="file-props-form">
		<fieldset>
			<legend><xsl:value-of select="mioga:gettext ('File properties')"/></legend>
			<div class="box">
				<div class="box-item" id="properties-data">
					<table class="form">
						<tr>
							<td class="form-label">
								<label for="description"><xsl:value-of select="mioga:gettext ('Description:')"/></label>
							</td>
							<td>
								<textarea name="description"></textarea>
							</td>
							<td class="properties-notice">
								<div class="checkbox-item">
									<input type="checkbox" id="override-description" onclick="ToggleOverrideDescription ();"/>
									<label class="checkbox-label" for="override-description"><xsl:value-of select="mioga:gettext ('Apply to')"/></label>
								</div>
							</td>
							<td rowspan="3" id="properties-multiple">
								<ul></ul>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr>
							<td class="form-label">
								<label for="tags"><xsl:value-of select="mioga:gettext ('Tags:')"/></label>
							</td>
							<td>
								<div id="tag-container"></div>
							</td>
							<td class="properties-notice">
								<div class="checkbox-item">
									<input type="radio" id="mode-add-tag" name="mode" value="add"/>
									<label class="checkbox-label" for="mode-add-tag"><xsl:value-of select="mioga:gettext ('Add to')"/></label>
								</div>
								<div class="checkbox-item">
									<input type="radio" id="mode-replace-tag" name="mode" value="replace" checked="true"/>
									<label class="checkbox-label" for="mode-replace-tag"><xsl:value-of select="mioga:gettext ('Replace in')"/></label>
								</div>
								<div class="checkbox-item">
									<input type="radio" id="mode-remove-tag" name="mode" value="remove"/>
									<label class="checkbox-label" for="mode-remove-tag"><xsl:value-of select="mioga:gettext ('Remove from')"/></label>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</fieldset>
	</form>
</div>

<div id="properties">
  <div id="prop_lock" style="visibility: hidden;">&#160;</div>
  <div class="infos">
    <div id="prop_infos">
		<table>
			<tr>
				<td>
					<label><xsl:value-of select="mioga:gettext('Name:')"/></label>
				</td>
				<td>
					<span id="prop_name">-</span>
				</td>
			</tr>
			<tr>
				<td>
					<label><xsl:value-of select="mioga:gettext('Type:')"/></label>
				</td>
				<td>
					<span id="prop_type">-</span>
				</td>
			</tr>
			<tr>
				<td>
					<label><xsl:value-of select="mioga:gettext('Size:')"/></label>
				</td>
				<td>
					<span id="prop_size">-</span>
				</td>
			</tr>
			<tr>
				<td>
					<label class="rights-label" onclick="Magellan.app.showRightsDialog ();"><xsl:value-of select="mioga:gettext('Rights:')"/></label>
				</td>
				<td>
					<span id="prop_rights">-</span>
				</td>
			</tr>
			<tr>
				<td>
					<label><xsl:value-of select="mioga:gettext('Last modified:')"/></label>
				</td>
				<td>
					<span id="prop_modified">-</span>
				</td>
			</tr>
			<tr>
				<td>
					<label><xsl:value-of select="mioga:gettext('Modified by:')"/></label>
				</td>
				<td>
					<span id="prop_author">-</span>
				</td>
			</tr>
			<tr id="prop_lock_owner" style="visibility: hidden;">
				<td>
					<label><xsl:value-of select="mioga:gettext('Locked by:')"/></label>
				</td>
				<td>
					<span id="prop_lock_owner_val">-</span>
				</td>
			</tr>
		</table>
    </div>
    <div id="comment_infos">
		<table>
			<tr>
				<td>
					<label class="comments-label" onclick="Magellan.app.showCommentsDialog ();"><xsl:value-of select="mioga:gettext('Comment(s):')"/></label>
				</td>
				<td>
					<span id="prop_comments">-</span>
				</td>
			</tr>
			<tr>
				<td>
					<label class="properties-label" onclick="Magellan.app.showPropertiesDialog ();"><xsl:value-of select="mioga:gettext ('Description:')"/></label>
				</td>
				<td>
					<div id="prop_description">-</div>
				</td>
			</tr>
			<tr>
				<td>
					<label class="properties-label" onclick="Magellan.app.showPropertiesDialog ();"><xsl:value-of select="mioga:gettext ('Tags:')"/></label>
				</td>
				<td>
					<div id="prop_tags">-</div>
				</td>
			</tr>
		</table>
    </div>
	<div class="clear-both"></div>
    <p id="prop_extra_infos" style="display: none;">&#160;</p>
    <div id="louvre_infos" style="display: none;">
      <xsl:value-of select="mioga:gettext('This folder is part of a gallery.')"/>
      <xsl:text> </xsl:text>
      <a id="louvre_link" href="" target="_blank">
        <xsl:value-of select="mioga:gettext('Click here')"/>
      </a>
      <xsl:text> </xsl:text>
      <xsl:value-of select="mioga:gettext('to see it.')"/>
    </div>
  </div>
</div>
</xsl:template>

<!-- =======================================
	AccessRights
	======================================= -->

<xsl:template match="AccessRights">
	<xsl:variable name="inherite">
		<xsl:value-of select="inherite"/>
	</xsl:variable>
	<h1>
		<xsl:value-of select="mioga:gettext('Modify rights on %s', string(dispuri))"/>
	</h1>

	<div id="leftcolumn">
		<!-- Inheritance -->
		<form class="form" id="inheritance">
			<fieldset>
				<legend>
					<xsl:value-of select="mioga:gettext('Inheritance')"/>
				</legend>
				
				<!-- Information message -->
				<xsl:call-template name="InfoMessage">
					<xsl:with-param name="id">inheritance_info</xsl:with-param>
					<xsl:with-param name="title">
						<xsl:value-of select="mioga:gettext('Inheritance')"/>
					</xsl:with-param>
					<xsl:with-param name="message">
						<p>
							<xsl:value-of select="mioga:gettext('If the rights are inherited, then the rights defined on the parent directory apply to the current object (file or directory). The rights that are inherited from the parent directory are highlighted in the tables. The rights that are not highlighted are specifically defined for the current object.')"/>
						</p>
						<p>
							<xsl:value-of select="mioga:gettext('If the rights are not inherited, then all the rights defined on the parent directory are ignored. The rights you define on this page will apply to the children objects (files or directories) if the current object is a directory.')"/>
						</p>
					</xsl:with-param>
				</xsl:call-template>

				<div class="form-item">
					<label for="inherit_yes">
						<xsl:value-of select="mioga:gettext('The rights are inherited')"/>
					</label>
					<input type="radio" value="inherite" name="inheritance" id="inherit_yes">
						<xsl:if test="inherite=1">
						  <xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input>
				</div>
				<div class="form-item">
					<label for="inherit_no">
						<xsl:value-of select="mioga:gettext('The rights are NOT inherited')"/>
					</label>
					<input type="radio" value="stop" name="inheritance" id="inherit_no">
						<xsl:if test="inherite=0">
						  <xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input>
				</div>
				<div class="form-item">
					<input type="hidden" name="act_change_inheritance" value="OK"/>
					<ul class="button_list">
						<li>
							<a class="button" onClick="document.getElementById('inheritance').submit();">OK</a>
						</li>
					</ul>
				</div>
			</fieldset>
		</form>

		<!-- Rights on profiles -->
		<xsl:call-template name="RightsTable">
			<xsl:with-param name="type">profile</xsl:with-param>
			<xsl:with-param name="node" select="Profile"/>
		</xsl:call-template>
	</div>

	<div id="rightcolumn">
		<!-- Rights on users -->
		<xsl:call-template name="RightsTable">
			<xsl:with-param name="type">user</xsl:with-param>
			<xsl:with-param name="node" select="User"/>
		</xsl:call-template>

		<!-- Rights on teams -->
		<xsl:call-template name="RightsTable">
			<xsl:with-param name="type">team</xsl:with-param>
			<xsl:with-param name="node" select="Team"/>
		</xsl:call-template>
	</div>
</xsl:template>

<!-- Generic template to display table of rights (for profiles, users and teams) -->
<xsl:template name="RightsTable">
	<xsl:param name="type"/>
	<xsl:param name="node"/>

	<!-- Fieldset legend -->
	<xsl:variable name="legend">
		<xsl:choose>
			<xsl:when test="$type='profile'">
				<xsl:value-of select="mioga:gettext('Profiles rights')"/>
			</xsl:when>
			<xsl:when test="$type='user'">
				<xsl:value-of select="mioga:gettext('Specific rights for users')"/>
			</xsl:when>
			<xsl:when test="$type='team'">
				<xsl:value-of select="mioga:gettext('Specific rights for teams')"/>
			</xsl:when>
		</xsl:choose>
	</xsl:variable>

	<!-- Form post action -->
	<xsl:variable name="action">act_change_<xsl:value-of select="$type"/></xsl:variable>

	<form class="form">
		<xsl:attribute name="id">form-<xsl:value-of select="$type"/></xsl:attribute>
		<fieldset>
			<legend>
				<xsl:value-of select="$legend"/>
			</legend>
				
			<!-- Information message -->
			<xsl:call-template name="InfoMessage">
				<xsl:with-param name="id"><xsl:value-of select="$type"/>_info</xsl:with-param>
				<xsl:with-param name="title">
					<xsl:value-of select="$legend"/>
				</xsl:with-param>
				<xsl:with-param name="message">
					<p>
						<xsl:value-of select="mioga:gettext('The rights for users, teams and profiles respect the following hierarchy (from the strongest to the weakest):')"/>
					</p>
					<ol>
						<li>
							<xsl:value-of select="mioga:gettext('user,')"/>
						</li>
						<li>
							<xsl:value-of select="mioga:gettext('team,')"/>
						</li>
						<li>
							<xsl:value-of select="mioga:gettext('profile.')"/>
						</li>
					</ol>
					<p>
						<xsl:value-of select="mioga:gettext('Meaning of the table headers:')"/>
					</p>
					<ol>
						<li>
							<xsl:value-of select="mioga:gettext('N: No right')"/>
						</li>
						<li>
							<xsl:value-of select="mioga:gettext('R: Read only')"/>
						</li>
						<li>
							<xsl:value-of select="mioga:gettext('R/W: Read/Write')"/>
						</li>
					</ol>
				</xsl:with-param>
			</xsl:call-template>
    
			<xsl:choose>
				<xsl:when test="count($node)=0">
					<p class="note">
						<xsl:value-of select="mioga:gettext('No specific right.')"/>
						&#160;
						<a>
							<xsl:attribute name="href">AccessRights?act_add_<xsl:value-of select="$type"/>=OK</xsl:attribute>
							<img src="{$theme_uri}/images/16x16/actions/list-add.png">
								<xsl:attribute name="title">
									<xsl:value-of select="mioga:gettext('Add specific rights')"/>
								</xsl:attribute>
							</img>
						</a>
					</p>
				</xsl:when>
				<xsl:otherwise>
					<table class="list border_color wide">
						<th>
						</th>
						<th>
							<xsl:value-of select="mioga:gettext('N')"/>
						</th>
						<th>
							<xsl:value-of select="mioga:gettext('R')"/>
						</th>
						<th>
							<xsl:value-of select="mioga:gettext('R/W')"/>
						</th>
						<xsl:if test="$type!='profile'">
							<th>
							</th>
						</xsl:if>
						<xsl:for-each select="$node">
							<xsl:sort select="@access" data-type="number" order="ascending" />
							<tr>
								<xsl:choose>
									<xsl:when test="@inherited=1">
										<xsl:attribute name="class">highlight</xsl:attribute>
									</xsl:when>
								</xsl:choose>
								<td>
									<xsl:if test="@inherited=1">
										<a href="ParentAccessRights?{$type}_id={@rowid}&amp;uri={../uri}&amp;referer={../referer}"><xsl:value-of select="."/></a>
									</xsl:if>
									<xsl:if test="@inherited=0">
										<xsl:value-of select="."/>
									</xsl:if>
								</td>
								<td class="center">
									<input type="radio" value="0">
										<xsl:attribute name="name">
											<xsl:value-of select="$type"/>_<xsl:value-of select="@rowid"/>
										</xsl:attribute>
										<xsl:if test="@access=0">
											<xsl:attribute name="checked">
												checked
											</xsl:attribute>
										</xsl:if>
									</input>
								</td>
								<td class="center">
									<input type="radio" value="1">
										<xsl:attribute name="name">
											<xsl:value-of select="$type"/>_<xsl:value-of select="@rowid"/>
										</xsl:attribute>
										<xsl:if test="@access=1">
											<xsl:attribute name="checked">
												checked
											</xsl:attribute>
										</xsl:if>
									</input>
								</td>
								<td class="center">
									<input type="radio" value="2">
										<xsl:attribute name="name">
											<xsl:value-of select="$type"/>_<xsl:value-of select="@rowid"/>
										</xsl:attribute>
										<xsl:if test="@access=2">
											<xsl:attribute name="checked">
												checked
											</xsl:attribute>
										</xsl:if>
									</input>
								</td>
								<xsl:if test="$type!='profile'">
									<td class="center">
										<a>
											<xsl:attribute name="href">AccessRights?act_delete_<xsl:value-of select="$type"/>=OK&amp;delete_<xsl:value-of select="$type"/>_<xsl:value-of select="@rowid"/>=1</xsl:attribute>
											<img src="{$theme_uri}/images/16x16/actions/trash-empty.png">
												<xsl:attribute name="title">
													<xsl:value-of select="mioga:gettext('Remove this specific right')"/>
												</xsl:attribute>
											</img>
										</a>
									</td>
								</xsl:if>
							</tr>
						</xsl:for-each>
						<xsl:if test="$type!='profile'">
							<tr>
								<td colspan="5" class="center">
									<a>
										<xsl:attribute name="href">AccessRights?act_add_<xsl:value-of select="$type"/>=OK</xsl:attribute>
										<img src="{$theme_uri}/images/16x16/actions/list-add.png">
											<xsl:attribute name="title">
												<xsl:value-of select="mioga:gettext('Add specific rights')"/>
											</xsl:attribute>
										</img>
									</a>
								</td>
							</tr>
						</xsl:if>
					</table>
					<div class="form-item">
						<input type="hidden" value="OK">
							<xsl:attribute name="name">act_change_<xsl:value-of select="$type"/></xsl:attribute>
						</input>
						<ul class="button_list">
							<li>
								<a class="button">
									<xsl:attribute name="onclick">document.getElementById('form-<xsl:value-of select="$type"/>').submit();</xsl:attribute>
									OK
								</a>
							</li>
						</ul>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</fieldset>
	</form>
</xsl:template>

<!-- Info image and associated tooltip -->
<xsl:template name="InfoMessage">
	<xsl:param name="id"/>
	<xsl:param name="title"/>
	<xsl:param name="message"/>

	<!-- Sorry about this hidden div, but there doesn't seem to be a proper way to satisfy both XSLT and JavaScript... -->
	<div id="{$id}_message" style="display:none">
		<xsl:copy-of select="$message"/>
	</div>

	<div class="info">
		<img id="{$id}" src="{$theme_uri}/images/16x16/emblems/emblem-info.png"/>
		<script language="JavaScript">
			new Ext.ToolTip ({
					target: '<xsl:value-of select="$id"/>',
					title: '<xsl:copy-of select="$title"/>',
					html: $('<xsl:value-of select="$id"/>_message').innerHTML,
					closable: true,
					autoHide: false
				});
		</script>
	</div>
</xsl:template>

<xsl:template name="join" >
	<xsl:param name="valueList" select="''"/>
	<xsl:param name="separator" select="','"/>
	<xsl:for-each select="$valueList">
		<xsl:choose>
			<xsl:when test="position() = 1">
				<xsl:value-of select="."/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($separator, .) "/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
</xsl:template>

</xsl:stylesheet>
