<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>
<xsl:import href="jquery.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<!-- ===============
	root node
	================ -->

<xsl:template match="/">
	<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="title"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="console-js"/>
	<xsl:call-template name="objectkeys-js"/>
	<xsl:call-template name="jquery-js"/>
	<xsl:call-template name="jquery-ui"/>
	<xsl:call-template name="jquery-form-js"/>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>

	<!-- Mioga2 jQuery plugins -->
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.tableScrollable.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.itemSelect.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.tableScrollableSortable.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.userSelect.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.inlineChooser.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.fileselector.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.extensibleList.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.treeview.css" media="screen" />

	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/diderot.css"/>
	<script src="{$theme_uri}/javascript/diderot.js" type="text/javascript"></script>
	<xsl:apply-templates mode="head"/>
	<xsl:call-template name="theme-css"/>
</head>
<body class="mioga">
	<xsl:call-template name="DisplayAppTitle">
	</xsl:call-template>

	<div class="diderot fluid" >
		<xsl:apply-templates />
	</div>
</body>
</html>
</xsl:template>


<!-- ===============
	DisplayMain
	================ -->

<mioga:strings>
	<i18nString>progressBarText</i18nString>
	<i18nString>no_ui_for_page</i18nString>
	<i18nString>proceed_search</i18nString>
	<i18nString>subexpression</i18nString>
	<i18nString>special-subexp</i18nString>
	<i18nString>close_subexpr</i18nString>
	<i18nString>store_template</i18nString>
	<i18nString>notice_field_name</i18nString>
	<i18nString>notice_field_type</i18nString>
	<i18nString>mandatory</i18nString>
	<i18nString>close_value_list</i18nString>
	<i18nString>notice_fields</i18nString>
	<i18nString>notice_fields_order</i18nString>
	<i18nString>add_line</i18nString>
	<i18nString>edit_value_list</i18nString>
	<i18nString>template_store_success</i18nString>
	<i18nString>template_attributes</i18nString>
	<i18nString>template_title</i18nString>
	<i18nString>template_description</i18nString>
	<i18nString>no_ui</i18nString>
	<i18nString>field_type_string_label</i18nString>
	<i18nString>field_type_text_label</i18nString>
	<i18nString>field_type_value_label</i18nString>
	<i18nString>field_type_values_label</i18nString>
	<i18nString>field_type_mandatory_on</i18nString>
	<i18nString>notice_template_edit_title</i18nString>
	<i18nString>operator_and_label</i18nString>
	<i18nString>operator_or_label</i18nString>
	<i18nString>operator_not_label</i18nString>
	<i18nString>constraint_contains_any</i18nString>
	<i18nString>constraint_contains_all</i18nString>
	<i18nString>constraint_equals</i18nString>
	<i18nString>constraint_does_not_contain</i18nString>
	<i18nString>notice_store_success</i18nString>
	<i18nString>notice_view_title</i18nString>
	<i18nString>notice_edit_title</i18nString>
	<i18nString>store_notice</i18nString>
	<i18nString>associated_documents</i18nString>
	<i18nString>download_file</i18nString>
	<i18nString>edit_notice_btn</i18nString>
	<i18nString>cancel_store_notice</i18nString>
	<i18nString>confirm_cancel</i18nString>
	<i18nString>edit_template</i18nString>
	<i18nString>create_notice</i18nString>
	<i18nString>recently_modified_notices</i18nString>
	<i18nString>search_notice</i18nString>
	<i18nString>all_values</i18nString>
	<i18nString>no_notice_to_display</i18nString>
	<i18nString>last_modified</i18nString>
	<i18nString>home_page</i18nString>
	<i18nString>search_failed</i18nString>
	<i18nString>modify_search</i18nString>
	<i18nString>new_search</i18nString>
	<i18nString>description</i18nString>
	<i18nString>notice_excluded_fields</i18nString>
	<i18nString>notice_excluded_fields_help</i18nString>
	<i18nString>notices</i18nString>
	<i18nString>notices_in_category</i18nString>
	<i18nString>create_category</i18nString>
	<i18nString>new_category</i18nString>
	<i18nString>create</i18nString>
	<i18nString>cancel</i18nString>
	<i18nString>category_name</i18nString>
	<i18nString>documentary_base_attributes</i18nString>
	<i18nString>delete_category</i18nString>
	<i18nString>rename_category</i18nString>
	<i18nString>rename</i18nString>
	<i18nString>category_rename_success</i18nString>
	<i18nString>no_notice_template_yet</i18nString>
	<i18nString>simple_search_label</i18nString>
	<i18nString>search_terms</i18nString>
	<i18nString>advanced_search_label</i18nString>
	<i18nString>add_files</i18nString>
	<i18nString>close_filepicker</i18nString>
	<i18nString>clear_form</i18nString>
	<i18nString>uncategorized_notices</i18nString>
	<i18nString>notice_category_change_success</i18nString>
	<i18nString>manage_access_rights</i18nString>
	<i18nString>access_rights</i18nString>
	<i18nString>root_category</i18nString>
	<i18nString>profile_rights</i18nString>
	<i18nString>team_rights</i18nString>
	<i18nString>user_rights</i18nString>
	<i18nString>inheritance</i18nString>
	<i18nString>inherited_rights</i18nString>
	<i18nString>not_inherited_rights</i18nString>
	<i18nString>no_right</i18nString>
	<i18nString>read_only</i18nString>
	<i18nString>read_write</i18nString>
	<i18nString>user_label_mask</i18nString>
	<i18nString>access_rights_update_success</i18nString>
	<i18nString>remove_specific_right</i18nString>
	<i18nString>add_teams</i18nString>
	<i18nString>add_users</i18nString>
	<i18nString>specific_right_add_success</i18nString>
	<i18nString>no_more_teams_to_add</i18nString>
	<i18nString>no_more_users_to_add</i18nString>
	<i18nString>back</i18nString>
	<i18nString>close</i18nString>
	<i18nString>attach_files</i18nString>
	<i18nString>files_to_attach</i18nString>
	<i18nString>select_existing_notice</i18nString>
	<i18nString>attach_files_help</i18nString>
	<i18nString>select_all</i18nString>
	<i18nString>deselect_all</i18nString>
	<i18nString>add</i18nString>
	<i18nString>firstname</i18nString>
	<i18nString>lastname</i18nString>
	<i18nString>email</i18nString>
	<i18nString>broken_link</i18nString>
	<i18nString>no_advanced_search_criteria</i18nString>
	<i18nString>hidden_files</i18nString>
	<i18nString>open_file</i18nString>
	<i18nString>view_notice</i18nString>
	<i18nString>notice_static_fields</i18nString>
	<i18nString>advanced_search</i18nString>
	<i18nString>simple_search</i18nString>
	<i18nString>all_notices</i18nString>
	<i18nString>last_search_results</i18nString>
</mioga:strings>

<xsl:template match="/DisplayMain" mode="head">
	<script type="text/javascript">
		$(document).ready (function () {
			$('#diderot-main').diderot ({
				i18n: {
					<!-- UI translation -->
					<xsl:apply-templates select="document('')/*/mioga:strings/i18nString"/>
				},
				access: <xsl:value-of select="access"/>
			});
		});
	</script>
</xsl:template>

<xsl:template match="i18nString">
	<xsl:variable name="translation"><xsl:call-template name="escapeString"/></xsl:variable>
	<xsl:if test="$translation!=.">
		"<xsl:value-of select="."/>": "<xsl:value-of select="$translation"/>"<xsl:if test="position() != last()">,</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template match="/DisplayMain">
	<div id="diderot-main"></div>
</xsl:template>

<xsl:template name="escapeString">
	<!-- Escape newlines -->
	<xsl:call-template name="escapeNewline">
		<xsl:with-param name="pText">
			<!-- Escape quotes -->
			<xsl:call-template name="escapeQuote">
				<xsl:with-param name="pText">
					<xsl:value-of select="mioga:gettext (.)"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:with-param>
	</xsl:call-template>
</xsl:template>

<xsl:template name="escapeQuote">
	<xsl:param name="pText"/>

	<xsl:if test="string-length($pText) >0">
		<xsl:value-of select="substring-before(concat($pText, '&quot;'), '&quot;')"/>

		<xsl:if test="contains($pText, '&quot;')">
			<xsl:text>\"</xsl:text>

			<xsl:call-template name="escapeQuote">
				<xsl:with-param name="pText" select="substring-after($pText, '&quot;')"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="escapeNewline">
	<xsl:param name="pText"/>

	<xsl:if test="string-length($pText) >0">
		<xsl:value-of select= "substring-before(concat($pText, '&#10;'), '&#10;')"/>

		<xsl:if test="contains($pText, '&#10;')">
			<xsl:text>\n</xsl:text>

			<xsl:call-template name="escapeNewline">
				<xsl:with-param name="pText" select="substring-after($pText, '&#10;')"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>
