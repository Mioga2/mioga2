<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<!--  <xsl:output method="html"/> -->

<!--
     MiogaReport : Create a new report
     
     - no-title: (0|1) Display or not the title (default:0)
     - no-border: (0|1) Display or not the border (default:0)
     - param: parameters form template with mode MiogaReportBody

     -->

<xsl:template name="MiogaReport">
    <xsl:param name="label"/>
    
    <xsl:param name="no-title">0</xsl:param>
    <xsl:param name="no-button">0</xsl:param>
    <xsl:param name="no-border">0</xsl:param>
    
    <xsl:param name="param"></xsl:param>
    
    <xsl:variable name="body">
        <table xsl:use-attribute-sets="mioga-empty-table">
            <tr valign="top">
                <td>
                    <table xsl:use-attribute-sets="mioga-empty-table">
                        <tr valign="top">
                            <td>
                                <xsl:call-template name="MiogaFormOpenZone1"/>
                                <xsl:call-template name="MiogaFormOpenZone2"/>
                                
                                <xsl:apply-templates select="."  mode="MiogaReportBody">
                                    <xsl:with-param name="param" select="$param"/>
                                </xsl:apply-templates>
                                
                                <xsl:call-template name="MiogaFormCloseZone2"/>
                                <xsl:call-template name="MiogaFormCloseZone1"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <xsl:if test="$no-button != 1">
                <tr>
                    <td>&#160;</td>
                </tr>
                
                <tr>
                    <td align="center">
                        <xsl:apply-templates select="."  mode="MiogaReportButton">
                            <xsl:with-param name="param" select="$param"/>
                        </xsl:apply-templates>
                    </td>
                </tr>
            </xsl:if>
        </table>
        
    </xsl:variable>

    <xsl:choose>
        <xsl:when test="$no-border=0">
            <xsl:call-template name="MiogaBox">
                <xsl:with-param name="no-title" select="$no-title"/>
                <xsl:with-param name="wrap">1</xsl:with-param>
                <xsl:with-param name="center">1</xsl:with-param>
                <xsl:with-param name="title">
                    <xsl:if test="$no-title != 1">
                        <xsl:call-template name="MiogaReportTranslation">
                            <xsl:with-param name="label" select="$label"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:with-param>
                
                <xsl:with-param name="body" select="$body"/>
            </xsl:call-template>            
        </xsl:when>
        <xsl:otherwise>
            <table xsl:use-attribute-sets="mioga-empty-table" align="center">
                <xsl:if test="$no-title=0">
                    <tr valign="top">
                        <td align="left">
                            <xsl:call-template name="MiogaTitle">
                                <xsl:with-param name="title">
                                    <xsl:call-template name="MiogaReportTranslation">
                                        <xsl:with-param name="label" select="$label"/>
                                    </xsl:call-template>
                                </xsl:with-param>
                            </xsl:call-template>
                        </td>
                    </tr>

                    <tr>
                        <td>&#160;</td>
                    </tr>
                
                </xsl:if>

                <tr>
                    <td align="center">
                        <xsl:copy-of select="$body"/>
                    </td>
                </tr>

            </table>
        </xsl:otherwise>
    </xsl:choose>
    
</xsl:template>



<!--
     MiogaReportMini : Create a new mini-report
     
     Like a mini form ... but for report.

     -->


<xsl:template name="MiogaReportMini">
  <xsl:param name="label"/>
  
  <xsl:param name="no-title">0</xsl:param>
  <xsl:param name="no-border">0</xsl:param>

  <xsl:param name="body"></xsl:param>

  <xsl:variable name="body">
      
      <table xsl:use-attribute-sets="mioga-empty-table">
          <!-- body -->
          <xsl:choose>
              <xsl:when test="$body">
                  <xsl:copy-of select="$body"/>
              </xsl:when>
              <xsl:otherwise>
                  <xsl:apply-templates select="." mode="MiogaReportMiniBody"/>
              </xsl:otherwise>
          </xsl:choose>
      </table>
  </xsl:variable>

  
  <xsl:choose>
      <xsl:when test="no-border=0">
          <xsl:call-template name="MiogaBox">
              <xsl:with-param name="no-title" select="$no-title"/>
              <xsl:with-param name="title">
                  <xsl:if test="$no-title != 1">
                      <xsl:call-template name="MiogaReportTranslation">
                          <xsl:with-param name="label" select="$label"/>
                      </xsl:call-template>
                  </xsl:if>
              </xsl:with-param>
              
              <xsl:with-param name="body" select="$body"/>
          </xsl:call-template>            
      </xsl:when>
      <xsl:otherwise>
          <xsl:copy-of select="$body"/>
      </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!--
     MiogaReportTitle - Display form titles
     
     - label : identify the title that will be displayed
     -->

<xsl:template name="MiogaReportTitle">
  <xsl:param name="label"/>
  
  <tr>
      <td colspan="2">
          <table xsl:use-attribute-sets="mioga-empty-table">
              <tr>
                  <td><img src="{$image_uri}/16x16/actions/arrow-right.png"/></td>
                  <td>&#160;</td>
                  <td>
                      <b>
                          <xsl:call-template name="MiogaReportInputTranslation">
                              <xsl:with-param name="label" select="$label"/>
                          </xsl:call-template>&#160;:&#160;
                      </b>
                  </td>
              </tr>
          </table>
      </td>
  </tr>
</xsl:template>


<!-- ================
     Report Input
     
     - label : name of the text input, used for translation
     - value: default value
     - disposition: 0 -> inline; 1 -> label on top of input
     
     ================ -->

<!-- Simple text input 
     -->
<xsl:template name="MiogaReportText">
  <xsl:param name="label"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>
  <xsl:param name="disposition">0</xsl:param>

  <xsl:call-template name="MiogaReportInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
        <xsl:value-of select="$value"/>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>


<!-- Display result for a boolean  -->
<xsl:template name="MiogaReportCheckbox">
  <xsl:param name="label"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>

  <xsl:call-template name="MiogaReportInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="body">
        <xsl:choose>
            <xsl:when test="$value = 1 or $value='on'">
                <xsl:call-template name="WidgetReportTranslation">
                    <xsl:with-param name="label">yes</xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="WidgetReportTranslation">
                    <xsl:with-param name="label">no</xsl:with-param>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>


<!-- Large text input (textarea)
     - width : box width
     - length : box height
     -->

<xsl:template name="MiogaReportTextArea">
  <xsl:param name="label"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>
  <xsl:param name="width">30</xsl:param>
  <xsl:param name="disposition">1</xsl:param>

  <tr>
      <td>
          <xsl:if test="$disposition=1">
              <xsl:attribute name="colspan">2</xsl:attribute>
          </xsl:if>
          <p xsl:use-attribute-sets="mioga-form-input-label-attr">
              <xsl:call-template name="MiogaReportInputTranslation">
                  <xsl:with-param name="label" select="$label"/>
              </xsl:call-template>&#160;:&#160;
          </p>

      </td>

      <xsl:if test="$disposition=1">
          <xsl:text disable-output-escaping="yes"><![CDATA[</tr><tr>]]></xsl:text>
      </xsl:if>

      <td>
          <xsl:if test="$disposition=1">
              <xsl:attribute name="colspan">2</xsl:attribute>
          </xsl:if>
          <xsl:if test="$disposition=0">
              <xsl:attribute name="valign">top</xsl:attribute>
          </xsl:if>
          <xsl:value-of disable-output-escaping="yes" select="$value"/>
      </td>
  </tr>
</xsl:template>



<!-- Date Input
     - first-year: first year in the year select
     - nb-year: year count to display in the year select
     - year-value, month-value, day-value: default value for year, month and day
     - year-name, month-name, day-name: default value for year, month and day
     -->
<xsl:template name="MiogaReportDate">
  <xsl:param name="label"/>
  <xsl:param name="disposition">0</xsl:param>

  <!-- default values -->

  <xsl:param name="year-value" select="fields/*[name(.)=concat($label, '_year')]"/>
  <xsl:param name="month-value" select="fields/*[name(.)=concat($label, '_month')]"/>
  <xsl:param name="day-value" select="fields/*[name(.)=concat($label, '_day')]"/>
  
  <xsl:call-template name="MiogaReportInputInternal">
      <xsl:with-param name="label" select="$label"/>
      <xsl:with-param name="disposition" select="$disposition"/>
      <xsl:with-param name="body">
          <xsl:call-template name="MiogaReportInputDateInternal">
              <xsl:with-param name="day-value" select="$day-value"/>
              <xsl:with-param name="month-value" select="$month-value -1"/>
              <xsl:with-param name="year-value" select="$year-value"/>
          </xsl:call-template>
      </xsl:with-param>
  </xsl:call-template>
</xsl:template>



<!-- Flexible report Input (date with flexible constraint)
     - year-value, month-value, day-value: default value for year, month and day
     -->
<xsl:template name="MiogaReportFlexibleDate">
  <xsl:param name="label"/>

  <!-- default values -->

  <xsl:param name="year-value" select="fields/*[name(.)=concat($label, '_year')]"/>
  <xsl:param name="month-value" select="fields/*[name(.)=concat($label, '_month')]"/>
  <xsl:param name="day-value" select="fields/*[name(.)=concat($label, '_day')]"/>
  <xsl:param name="constraint-value" select="fields/*[name(.)=concat($label, '_constraint')]"/>
  
  <xsl:call-template name="MiogaReportInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="body">
      <xsl:call-template name="MiogaReportInputDateInternal">
          <xsl:with-param name="day-value" select="$day-value"/>
          <xsl:with-param name="month-value" select="$month-value -1"/>
          <xsl:with-param name="year-value" select="$year-value"/>
      </xsl:call-template>&#160;<xsl:call-template name="MiogaReportInputFlexibleContraintInternal">
          <xsl:with-param name="constraint-value" select="$constraint-value"/>
      </xsl:call-template>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>


<!-- 
     internal implementation for date
     -->

<xsl:template name="MiogaReportInputDateInternal">
  <!-- default values -->
  <xsl:param name="year-value"/>
  <xsl:param name="month-value"/>
  <xsl:param name="day-value"/>

  <xsl:value-of select="$day-value"/>&#160;<xsl:call-template name="monthTranslation">
      <xsl:with-param name="month" select="$month-value"/>
  </xsl:call-template>&#160;<xsl:value-of select="$year-value"/>
</xsl:template>


<xsl:template name="MiogaReportInputFlexibleContraintInternal">
  <!-- default values -->
  <xsl:param name="constraint-value"/>

  <xsl:call-template name="flexibleConstraintTranslation">
      <xsl:with-param name="name" select="$constraint-value"/>
  </xsl:call-template>
</xsl:template>




<!-- 
     Duration Report
     -->

<xsl:template name="MiogaReportDuration">
  <xsl:param name="label"/>
  <xsl:param name="value" select="fields/*[name(.)=concat($label,'_value')]"/>
  <xsl:param name="value_unit" select="fields/*[name(.)=concat($label,'_unit')]"/>

  <xsl:call-template name="MiogaReportInputInternal">
    <xsl:with-param name="label" select="$label"/>

    <xsl:with-param name="body">
        <table xsl:use-attribute-sets="mioga-empty-table">
            <tr>
                <td>
                    <xsl:value-of select="$value"/>
                </td>
                <td>
                    <xsl:if test="$value != ''">
                        <xsl:call-template name="durationTranslation">
                            <xsl:with-param name="label">
                                <xsl:choose>
                                    <xsl:when test="$value_unit = 0">second</xsl:when>
                                    <xsl:when test="$value_unit = 1">minute</xsl:when>
                                    <xsl:when test="$value_unit = 2">hour</xsl:when>
                                    <xsl:when test="$value_unit = 3">day</xsl:when>
                                </xsl:choose>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
                </td>
            </tr>
        </table>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>



<!-- 
     Display Time Interval
     -->
<xsl:template name="MiogaReportTimeInterval">
  <xsl:param name="label"/>

  <!-- default values -->
  <xsl:param name="hour-value-1"></xsl:param>
  <xsl:param name="min-value-1"></xsl:param>
  <xsl:param name="sec-value-1"></xsl:param>
  <xsl:param name="hour-value-2"></xsl:param>
  <xsl:param name="min-value-2"></xsl:param>
  <xsl:param name="sec-value-2"></xsl:param>

  <!-- misc -->
  <xsl:param name="use-second">1</xsl:param>

  <tr>
    <td xsl:use-attribute-sets="mioga-form-input-label-attr">
      <xsl:call-template name="MiogaReportInputTranslation">
        <xsl:with-param name="label" select="$label"/>
      </xsl:call-template>&#160;:&#160;
    </td>
    <td>
        <table xsl:use-attribute-sets="mioga-empty-table">
            <tr>
                <td>
                    <xsl:call-template name="MiogaReportTimeInternal">
                        <xsl:with-param name="hour-value" select="$hour-value-1"/>
                        <xsl:with-param name="min-value" select="$min-value-1"/>
                        <xsl:with-param name="sec-value" select="$sec-value-1"/>
                        <xsl:with-param name="use-second" select="$use-second"/>
                    </xsl:call-template>
                </td>
                <td>
                    &#160;<xsl:call-template name="WidgetReportTranslation">
                        <xsl:with-param name="label">to</xsl:with-param>
                    </xsl:call-template>&#160;
                </td>
                <td>
                    <xsl:call-template name="MiogaReportTimeInternal">
                        <xsl:with-param name="hour-value" select="$hour-value-2"/>
                        <xsl:with-param name="min-value" select="$min-value-2"/>
                        <xsl:with-param name="sec-value" select="$sec-value-2"/>
                        <xsl:with-param name="use-second" select="$use-second"/>
                    </xsl:call-template>                
                </td>
            </tr>
        </table>
    </td>
  </tr>
</xsl:template>

<!-- internal implementation for time -->
<xsl:template name="MiogaReportTimeInternal">
  <!-- default values -->
  <xsl:param name="hour-value"/>
  <xsl:param name="min-value"/>
  <xsl:param name="sec-value"/>

  <!-- misc -->
  <xsl:param name="use-second">1</xsl:param>

  <xsl:value-of select="$hour-value"/>
  <xsl:text>:</xsl:text>
  <xsl:value-of select="$min-value"/>
  <xsl:if test="$use-second=1">
      <xsl:text>:</xsl:text>
      <xsl:value-of select="$sec-value"/>
  </xsl:if>
</xsl:template>


<!-- 
     Input with anything you want, "content" parameter is paste with xsl:copy-of
     -->
<xsl:template name="MiogaReportMisc">
  <xsl:param name="label"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>
  <xsl:param name="content"></xsl:param>
  <xsl:param name="disposition">0</xsl:param>
  
  <xsl:call-template name="MiogaReportInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <xsl:copy-of select="$content"/>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>



<!-- PRIVATE TEMPLATES -->

<xsl:template name="MiogaReportInputInternal">
  <xsl:param name="label"/>
  <xsl:param name="body"/>
  <xsl:param name="disposition"/>

  <tr>
    <td xsl:use-attribute-sets="mioga-form-input-label-attr">
        <xsl:call-template name="MiogaReportInputTranslation">
            <xsl:with-param name="label" select="$label"/>
        </xsl:call-template>&#160;:&#160;
    </td>
    <xsl:if test="$disposition=1">
        <xsl:text disable-output-escaping="yes"><![CDATA[</tr><tr>]]></xsl:text>
    </xsl:if>
    <td>
      <xsl:copy-of select="$body"/>
    </td>
  </tr>
</xsl:template>


<xsl:template name="WidgetReportTranslation">
    <xsl:param name="label"/>
    <xsl:choose>
        <xsl:when test="$label='to'"><xsl:value-of select="mioga:gettext('to')"/></xsl:when>
        <xsl:when test="$label='yes'"><xsl:value-of select="mioga:gettext('yes')"/></xsl:when>
        <xsl:when test="$label='no'"><xsl:value-of select="mioga:gettext('no')"/></xsl:when>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
