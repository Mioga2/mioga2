<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" encoding="UTF-8" media-type="text/html"/>

<xsl:include href="scriptaculous.xsl"/>
<!-- ===============
	Variables
	================ -->

<xsl:variable name="anim_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Animation">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="moder_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Moderation">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="write_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Write">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="read_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Read">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<!--
=====================
 Utilities templates
=====================
-->

<!-- escape asostrophes very useful for javascript -->
<xsl:template name="escape-apos">
   <xsl:param name="string" />
   <!-- create an $apos variable to make it easier to refer to -->
   <xsl:variable name="apos" select='"&apos;"' />
   <xsl:choose>
      <!-- if the string contains an apostrophe... -->
      <xsl:when test='contains($string, $apos)'>
         <!-- ... give the value before the apostrophe... -->
         <xsl:value-of select="substring-before($string, $apos)" />
         <!-- ... the escaped apostrophe ... -->
         <xsl:text>\'</xsl:text>
         <!-- ... and the result of applying the template to the
                  string after the apostrophe -->
         <xsl:call-template name="escape-apos">
            <xsl:with-param name="string"
                            select="substring-after($string, $apos)" />
         </xsl:call-template>
      </xsl:when>
      <!-- otherwise... -->
      <xsl:otherwise>
         <!-- ... just give the value of the string -->
         <xsl:value-of select="$string" />
      </xsl:otherwise>
   </xsl:choose>
</xsl:template>

<!--
===============
 Templates
===============
-->

<xsl:template match="/">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="UpdateCategory" >
    <xsl:apply-templates select="Category"/>
</xsl:template>

<!--
===========================
 Write or Edit an article
===========================
-->
<xsl:template name="WriteOrEditArticle">
    <xsl:param name="action"/>
    <!-- js variables -->
    <xsl:variable name="title" select="Actions/*[name()=$action]"/>
    <xsl:variable name="article_field_title" select="//Actions/ArticleTitle"/>
    <xsl:variable name="article_field_header" select="//Actions/ArticleHeader"/>
    <xsl:variable name="article_header" select="Article/Header"/>
    <xsl:variable name="article_title" select="Article/Title"/>
    <xsl:variable name="article" select="//Actions/Article"/>
    <xsl:variable name="contents" select="Article/Contents"/>
    <xsl:variable name="save_draft" select="//Actions/SaveDraft"/>
    <xsl:variable name="validate" select="//Actions/Validate"/>
    <xsl:variable name="decline" select="//Actions/Decline"/>
    <xsl:variable name="publish_article" select="//Actions/PublishArticle"/>
    <xsl:variable name="cancel" select="//Actions/Cancel"/>
    <xsl:variable name="category_choices" select="//Actions/CategoryChoices"/>
    <xsl:variable name="page" select="//Page"/>
    <!-- end of js variables -->
    
    <div class="edit-box sbox border_color">
        <h2 class="title_bg_color"><xsl:value-of select="$title"/></h2>
        <div class="content">
        <form method="post" action="UpdateArticle">
        <xsl:if test="$action = 'UpdateArticle'">
        <input type="hidden" name="rowid" value="{Article/rowid}"/>
        </xsl:if>
        <xsl:if test="Article/old_id">
        <input type="hidden" name="old_id" value="{Article/old_id}"/>
        </xsl:if>
        <input type="hidden" name="status" value="{Article/status}"/>
        <input type="hidden" name="page" value="{Page}"/>
        <xsl:if test="CanSaveAsDraft">
        <input type="hidden" name="candraft" value="1"/>
        </xsl:if>
        <input type="hidden" name="editaction" value="{$action}"/>
		<xsl:call-template name="CSRF-input"/>
    
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td width="20%">
                <div class="categories sbox border_color">
                    <h2 class="title_bg_color"><xsl:value-of select="$category_choices"/></h2>
                    <xsl:if test="//errors/err[@arg = 'categories']">
                        <xsl:call-template name="error-message">
                            <xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'categories']/type" /></xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
                    <ul>
                    <xsl:if test="//errors/err[@arg = 'categories']">
                        <xsl:attribute name="class">error-box</xsl:attribute>
                    </xsl:if>
                    <xsl:for-each select="Categories/Category">
                        <xsl:if test="rowid">
                            <!-- js variables -->
                            <xsl:variable name="category_name" select="label"/>
                            <!-- end of js variables -->
                            
                            <xsl:variable name="even_or_odd">
                                <xsl:choose>
                                    <xsl:when test="position() mod 2 = 0">even</xsl:when>
                                    <xsl:otherwise>odd</xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            
                            <li class="{$even_or_odd}"><label for="catg-{rowid}"><input name="categories" id="catg-{rowid}" type="checkbox" value="{rowid}"><xsl:if test="selected"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><xsl:value-of select="$category_name"/></label></li>
                        </xsl:if>
                    </xsl:for-each>
                    </ul>
                </div>
            </td>
    
            <td width="80%">
                <div class="form">
                    <fieldset>
                        <legend><xsl:value-of select="$article_field_title"/></legend>
                        <input size="30" type="text" name="title" id="title" value="{$article_title}">
                        <xsl:attribute name="class">text border_color<xsl:if test="//errors/err[@arg = 'title']"> error-box</xsl:if></xsl:attribute>
                        </input>
                        <xsl:if test="//errors/err[@arg = 'title']">
                            <xsl:call-template name="error-message">
                                <xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'title']/type" /></xsl:with-param>
                            </xsl:call-template>
                        </xsl:if>
                    </fieldset>
                    <fieldset>
                        <legend><xsl:value-of select="$article_field_header"/></legend>
                        <textarea class="border_color" name="header" id="header" cols="40" rows="3"><xsl:value-of select="$article_header"/></textarea>
                    </fieldset>
                    <fieldset>
                        <legend><xsl:value-of select="$article"/></legend>
                        <textarea name="contents" id="contents"><xsl:attribute name="class">mceEditor border_color<xsl:if test="//errors/err[@arg = 'contents']"> error-box</xsl:if></xsl:attribute><xsl:value-of select="$contents"/></textarea>
                        <xsl:if test="//errors/err[@arg = 'contents']">
                            <xsl:call-template name="error-message">
                                <xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'contents']/type" /></xsl:with-param>
                            </xsl:call-template>
                        </xsl:if>
                    </fieldset>
                    <ul class="button_list">
                        <xsl:if test="CanSaveAsDraft">
                        <li>
                            <button class="button" type="submit" value="{$save_draft}">
                                <xsl:attribute name="onclick">this.name="draft";</xsl:attribute>
								<xsl:value-of select="$save_draft"/>
                            </button>
                        </li>
                        </xsl:if>
                        <xsl:choose>
                            <xsl:when test="Article/CanBeValidated">
                            <li>
                                <button class="button" type="submit" value="{$validate}">
                                <xsl:attribute name="onclick">this.name="validate";</xsl:attribute>
								<xsl:value-of select="$validate"/>
                            </button>
                            </li>
                            <li>
                                <button class="button" type="submit" value="{$decline}">
                                <xsl:attribute name="onclick">this.name="decline";</xsl:attribute>
								<xsl:value-of select="$decline"/>
                            </button>
                            </li>
                            </xsl:when>
                            <xsl:otherwise>
                            <li>
                                <button class="button" type="submit" value="{$publish_article}">
									<xsl:value-of select="$publish_article"/>
								</button>
                            </li>
                            </xsl:otherwise>
                        </xsl:choose>
                        <li>
                            <a class="button" href="{$page}"><xsl:value-of select="$cancel"/></a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
    </table>
    
        </form>
        </div>
    </div>
</xsl:template>

<!--
=================
 Display options
=================
-->
<xsl:template match="Options">
    <xsl:param name="js_mode"/>
    
    <!-- using variable to handle javascript mode -->
        <xsl:variable name="title">
            <xsl:choose>
                <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Options/Title"/></xsl:call-template></xsl:when>
                <xsl:otherwise><xsl:value-of select="//Options/Title"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="message">
            <xsl:choose>
                <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Message"/></xsl:call-template></xsl:when>
                <xsl:otherwise><xsl:value-of select="//Message"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="ok">
            <xsl:choose>
                <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Options/Ok"/></xsl:call-template></xsl:when>
                <xsl:otherwise><xsl:value-of select="//Options/Ok"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="cancel">
            <xsl:choose>
                <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Options/Cancel"/></xsl:call-template></xsl:when>
                <xsl:otherwise><xsl:value-of select="//Options/Cancel"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
    <!-- end of variables definition -->
    
    <div class="sbox border_color">
        <h2 class="title_bg_color"><xsl:value-of select="$title"/></h2>
        <div class="content">
            <xsl:if test="//Message"><p><xsl:value-of select="$message"/></p></xsl:if>
            <form action="UpdateOptions" method="post">
            <xsl:attribute name="onsubmit">new Ajax.Request("UpdateOptions", {evalScripts:true, asynchronous:true, parameters:encodeURI(Form.serialize(this)) }); return false;</xsl:attribute>
            <input type="hidden" name="old_ui" value="{//Option[@field='extended_ui']}"/>
            <xsl:for-each select="Option">
                <!-- js variables -->
                <xsl:variable name="label">
                    <xsl:choose>
                        <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="label"/></xsl:call-template></xsl:when>
                        <xsl:otherwise><xsl:value-of select="label"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <!-- end of js variables -->
        
                <label for="{field}-id"><input id="{field}-id" type="checkbox" name="{field}" value="1">
                    <xsl:if test="checked=1"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input>&#160;<xsl:value-of select="$label"/></label><br/>
            </xsl:for-each>
                <ul class="button_list">
                    <li>
                        <button class="button" type="submit" value="{$ok}"><xsl:value-of select="$ok"/></button>
                    </li>
                    <li>
                        <a class="button" href="#"><xsl:attribute name="onclick">$("disable-window").hide(); $("options-box").hide(); return false;</xsl:attribute><xsl:value-of select="$cancel"/></a>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</xsl:template>

<!--
=======================================
 Display categories
=======================================
-->
<xsl:template match="Categories">
    <xsl:param name="js_mode"/>
    <xsl:param name="page"/>
    
    <!-- js variables -->
    <xsl:variable name="edit">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="//Categories/Edit"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="//Categories/Edit"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="delete_confirm">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="//Categories/DeleteConfirm"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="//Categories/DeleteConfirm"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="delete">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="//Categories/Delete"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="//Categories/Delete"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!-- js variables -->
    
    <ul>
    <xsl:if test="//errors/err[@arg = 'categories']">
        <xsl:attribute name="class">error</xsl:attribute>
        <xsl:call-template name="error-message">
            <xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'categories']/type" /></xsl:with-param>
        </xsl:call-template>
    </xsl:if>
    <xsl:for-each select="Category">
        <!-- js variables -->
        <xsl:variable name="category_name">
          <xsl:choose>
            <xsl:when test="$js_mode = 1">
              <xsl:call-template name="escape-apos">
                <xsl:with-param name="string" select="label"/>
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="label"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <!-- end of js variables -->
        
        <xsl:variable name="ctgedit_id">ctg<xsl:value-of select="rowid" />_edit</xsl:variable>
        <xsl:variable name="ctgedit_id_ext"><xsl:value-of select="$ctgedit_id" />_ext</xsl:variable>
        
        <xsl:variable name="even_or_odd">
            <xsl:choose>
                <xsl:when test="position() mod 2 = 0">even</xsl:when>
                <xsl:otherwise>odd</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="params">
            <xsl:if test="rowid">?category_id=<xsl:value-of select="rowid"/></xsl:if>
        </xsl:variable>
        
        <li class="{$even_or_odd}"><a id="{$ctgedit_id}" href="{action}{$params}"><xsl:value-of select="$category_name"/></a>
        <xsl:if test="$anim_right=1 and rowid">
            <img class="edit" id="{$ctgedit_id_ext}" src="{$image_uri}/16x16/actions/document-edit.png" title="{$edit}" alt="{$edit}" />
            <xsl:if test="in_use=0">
            <a href="#"><xsl:attribute name="onclick">if (confirm("<xsl:value-of select="$delete_confirm"/>")) { new Ajax.Request("DeleteCategory", {evalScripts:true, onLoading: loading, onComplete: complete, asynchronous:true, parameters: "rowid=<xsl:value-of select="rowid"/>&amp;page=<xsl:value-of select="$page"/>"}); }; return false;</xsl:attribute><img class="delete" src="{$image_uri}/16x16/actions/trash-empty.png" title="{$delete}" alt="{$delete}"/></a>
            </xsl:if>
        </xsl:if>
        </li>
        <xsl:if test="$anim_right=1 and rowid">
            <xsl:call-template name="inplace-editor-extcontrol">
				<xsl:with-param name="element"><xsl:value-of select="$ctgedit_id" /></xsl:with-param>
				<xsl:with-param name="external_id"><xsl:value-of select="$ctgedit_id_ext" /></xsl:with-param>
				<xsl:with-param name="url">UpdateCategory?rowid=<xsl:value-of select="rowid" /></xsl:with-param>
                <xsl:with-param name="size">18</xsl:with-param>
			</xsl:call-template>
        </xsl:if>
    </xsl:for-each>
    </ul>
</xsl:template>

<!--
=======================================
 Category for AjaxInPlaceEditor
=======================================
-->
<xsl:template match="Category">
<xsl:value-of select="name"/>
</xsl:template>

<!--
=======================================
 Display articles
=======================================
-->
<xsl:template match="Articles">
    <xsl:param name="js_mode"/>
    <xsl:param name="page"/>
    
    <!-- js variables -->
    <xsl:variable name="validate">
        <xsl:choose>
            <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Actions/Validate"/></xsl:call-template></xsl:when>
            <xsl:otherwise><xsl:value-of select="//Actions/Validate"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="decline_confirm">
        <xsl:choose>
            <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Actions/DeclineConfirm"/></xsl:call-template></xsl:when>
            <xsl:otherwise><xsl:value-of select="//Actions/DeclineConfirm"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="validate_confirm">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('Are you sure you want to validate this article?')"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="mioga:gettext('Are you sure you want to validate this article?')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="decline">
        <xsl:choose>
            <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Actions/Decline"/></xsl:call-template></xsl:when>
            <xsl:otherwise><xsl:value-of select="//Actions/Decline"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="edit">
        <xsl:choose>
            <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Actions/Edit"/></xsl:call-template></xsl:when>
            <xsl:otherwise><xsl:value-of select="//Actions/Edit"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="delete_confirm">
        <xsl:choose>
            <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Actions/DeleteConfirm"/></xsl:call-template></xsl:when>
            <xsl:otherwise><xsl:value-of select="//Actions/DeleteConfirm"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="delete_article">
        <xsl:choose>
            <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="//Actions/DeleteArticle"/></xsl:call-template></xsl:when>
            <xsl:otherwise><xsl:value-of select="//Actions/DeleteArticle"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="no_article">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext('There is no article available at the moment.')"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="mioga:gettext('There is no article available at the moment.')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="article_help">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="mioga:gettext(' To create a new one, verify that one category exists at least and click on &quot;Write a new article&quot;.')"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="mioga:gettext(' To create a new one, verify that one category exists at least and click on &quot;Write a new article&quot;.')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!-- end of js variables -->
    
    <xsl:if test="count(Article) = 0">
    	<p class="information"><xsl:value-of select="$no_article"/><xsl:if test="$write_right = 1"><xsl:value-of select="$article_help"/></xsl:if></p>
    </xsl:if>
    
    <xsl:for-each select="./Article">
        <!-- js variables -->
        <xsl:variable name="title">
          <xsl:choose>
            <xsl:when test="$js_mode = 1">
              <xsl:call-template name="escape-apos">
                <xsl:with-param name="string" select="Title"/>
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="Title"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="contents">
                <xsl:choose>
                    <xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="Contents"/></xsl:call-template></xsl:when>
                    <xsl:otherwise><xsl:value-of select="Contents" disable-output-escaping="yes"/></xsl:otherwise>
                </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="header">
          <xsl:choose>
            <xsl:when test="$js_mode = 1">
              <xsl:call-template name="escape-apos">
                <xsl:with-param name="string" select="Header"/>
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="Header"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <!-- end of js variables -->
        
        <div id="article-{rowid}" class="article sbox border_color">
            <h2 class="title_bg_color"><a style="color: #0332B2" href="ViewArticle?article_id={rowid}"><xsl:value-of select="$title"/></a>
            <xsl:if test="./CanBeValidated">
                <a href="#"><xsl:attribute name="onclick">if (confirm("<xsl:value-of select="$validate_confirm"/>")) { new Ajax.Request("ValidateArticle", {evalScripts:true, asynchronous:true, parameters: "rowid=<xsl:value-of select="rowid"/>" }) }; return false;</xsl:attribute><img class="validate" src="{$image_uri}/16x16/actions/dialog-ok-apply.png" title="{$validate}" alt="{$validate}" /></a>
                <a href="#"><xsl:attribute name="onclick">if (confirm("<xsl:value-of select="$decline_confirm"/>")) { new Ajax.Request("DeclineArticle", {evalScripts:true, asynchronous:true, parameters: "rowid=<xsl:value-of select="rowid"/>" }); }; return false;</xsl:attribute><img class="decline" src="{$image_uri}/16x16/actions/dialog-cancel.png" title="{$decline}" alt="{$decline}" /></a>
            </xsl:if>
            <xsl:if test="./CanBeEdited">
                <a href="EditArticle?rowid={rowid}&amp;page={$page}"><img class="edit" src="{$image_uri}/16x16/actions/document-edit.png" title="{$edit}" alt="{$edit}" /></a>
            </xsl:if>
            <xsl:if test="$moder_right=1">
                <xsl:variable name="onlyctg">
                    <xsl:choose>
                        <xsl:when test="//OnlyCategory">&amp;category_id=<xsl:value-of select="//OnlyCategory/id"/></xsl:when>
                        <xsl:otherwise></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <a href="#"><xsl:attribute name="onclick">if (confirm("<xsl:value-of select="$delete_confirm"/>")) { new Ajax.Request("DeleteArticle", { evalScripts:true, asynchronous:true, parameters: "rowid=<xsl:value-of select="rowid"/><xsl:value-of select="$onlyctg"/>&amp;page=<xsl:value-of select="$page"/>" }); }; return false;</xsl:attribute><img class="delete" src="{$image_uri}/16x16/actions/trash-empty.png" title="{$delete_article}" alt="{$delete_article}"/></a>
            </xsl:if>
            </h2>
            <h3 class="article">
				<xsl:for-each select="Infos">
					<xsl:choose>
						<xsl:when test="$js_mode=1"><xsl:call-template name="escape-apos"><xsl:with-param name="string" select="."/></xsl:call-template></xsl:when>
						<xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
					</xsl:choose>
					<br/>
				</xsl:for-each>
				<xsl:value-of select="mioga:gettext('Categories:')"/>
				&#160;
				<xsl:for-each select="Categories/Category">
					<!-- js variables -->
					<xsl:variable name="category_name">
					  <xsl:choose>
						<xsl:when test="$js_mode = 1">
						  <xsl:call-template name="escape-apos">
							<xsl:with-param name="string" select="label"/>
						  </xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
						  <xsl:value-of select="label"/>
						</xsl:otherwise>
					  </xsl:choose>
					</xsl:variable>
					<!-- end of js variables -->
					
					<xsl:if test="position() != 1">,</xsl:if>&#160;<a href="{action}?category_id={rowid}"><xsl:value-of select="$category_name"/></a>
				</xsl:for-each>
                <span class="article_id"><a href="DisplayArticle?article_id={rowid}">#<xsl:value-of select="rowid"/></a></span>
            </h3>
            <pre class="chapo"><xsl:value-of select="$header" disable-output-escaping="yes"/></pre>
            <div class="content">
                <xsl:value-of select="$contents" disable-output-escaping="yes"/>
            </div>
            <xsl:if test="//AccessRights/ReadComment">
                <p class="comments_number"><a href="ViewArticle?article_id={rowid}"><xsl:choose>
                    <xsl:when test="CommentsNumber &gt; 0">
                        <xsl:value-of select="mioga:gettext('%d comment(s)', CommentsNumber)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="mioga:gettext('No comment')"/>
                    </xsl:otherwise>
                </xsl:choose></a></p>
            </xsl:if>
        </div>
    </xsl:for-each>
</xsl:template>

<!--
=======================================
 DisabledNewArticle
=======================================
-->
<xsl:template name="DisabledNewArticle">
    <xsl:param name="js_mode"/>
    
    <xsl:variable name="label">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="//Actions/WriteArticle"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="//Actions/WriteArticle"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
	<span class="disabled_new_article"><xsl:value-of select="$label"/></span>
</xsl:template>

<!--
=======================================
 NewArticle
=======================================
-->
<xsl:template name="NewArticle">
    <xsl:param name="js_mode"/>
    <xsl:param name="page"/>
    
    <xsl:variable name="label">
      <xsl:choose>
        <xsl:when test="$js_mode = 1">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="//Actions/WriteArticle"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="//Actions/WriteArticle"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
	<a class="new_article" href="WriteArticle?page={$page}"><xsl:value-of select="$label"/></a>
</xsl:template>

</xsl:stylesheet>
