<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />
<xsl:include href="box.xsl"/>
<xsl:include href="dojotoolkit.xsl"/>
<xsl:include href="grid.xsl"/>

<!-- ===============
	root document
	================ -->

<xsl:template name="colbert-css">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/colbert.css" media="screen" />
</xsl:template>

<xsl:template name="colbert-js">
	<script src="{$theme_uri}/javascript/colbert.js" type="text/javascript"></script>
</xsl:template>

<xsl:template match="/">
	<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>


	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="dojo-css"/>
	<xsl:call-template name="dojo-js"/>
	<xsl:call-template name="mioga_dojo-js"/>

	<xsl:call-template name="colbert-css"/>
	<xsl:call-template name="colbert-js"/>

	<xsl:apply-templates mode="head"/>

	<xsl:call-template name="theme-css"/>
</head>
<body id="colbert_body" class="colbert mioga">
	<script type="text/javascript">
		var lang = '<xsl:value-of select="//miogacontext/group/lang"/>';
		var default_group_id = '<xsl:value-of select="//miogacontext/default_group"/>';
	</script>
	<xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- ===============
	DisplayMain
	================ -->

<xsl:template match="DisplayMain" mode="head">
	<script type="text/javascript">
		dojo.require ("mioga.data.ItemFileReadStore");
	</script>
</xsl:template>

<xsl:template match="DisplayMain">
	<xsl:call-template name="DisplayAppTitle">
		<xsl:with-param name="help">applications.html#colbert</xsl:with-param>
	</xsl:call-template>

	<!-- Application navigator -->
	<xsl:call-template name="AppNav"/>

	<xsl:if test="//UserRights/Administration = 1 or //UserRights/UsersRead = 1 or //UserRights/UsersWrite = 1 or //UserRights/UsersCreate = 1 or //UserRights/Reporting = 1">
		<div class="block_info">
			<xsl:call-template name="box">
				<xsl:with-param name="title"><xsl:value-of select="mioga:gettext('Users')"/></xsl:with-param>
				<xsl:with-param name="content">
					<div id="users_infos">
						<div class="ie_hack1"></div>
						<ul id="users_actions" class="actions vmenu">
							<xsl:if test="//UserRights/Administration = 1 or //UserRights/UsersRead = 1 or //UserRights/UsersWrite = 1 or //UserRights/UsersCreate = 1">
								<li>
									<a href="DisplayUsers">
										<xsl:value-of select="mioga:gettext('Show all users')"/>
									</a>
								</li>
								<li>
									<a href="#" onclick="dijit.byId ('search_users').show ();">
										<xsl:value-of select="mioga:gettext('Show some users')"/>
									</a>
								</li>
							</xsl:if>
							<xsl:if test="//UserRights/Reporting = 1">
								<li>
									<a href="DisplayUserGroupReport">
										<xsl:value-of select="mioga:gettext('Show users to groups affectations')"/>
									</a>
								</li>
								<li>
									<a href="ExportUserGroupReport">
										<xsl:value-of select="mioga:gettext('Export users to groups affectations')"/>
									</a>
								</li>
								<li>
									<a href="DisplayUserTeamReport">
										<xsl:value-of select="mioga:gettext('Show users to teams affectations')"/>
									</a>
								</li>
								<li>
									<a href="ExportUserTeamReport">
										<xsl:value-of select="mioga:gettext('Export users to teams affectations')"/>
									</a>
								</li>
							</xsl:if>
						</ul>
						<p onclick="document.location.href='DisplayUsers'"><span class="count"><xsl:value-of select="users/total"/></span><xsl:value-of select="mioga:gettext('Total users count: ')"/></p>
						<p onclick="document.location.href='DisplayUsers?type=local_user'"><span class="count"><xsl:value-of select="users/local"/></span><xsl:value-of select="mioga:gettext('Local users count: ')"/></p>
						<p onclick="document.location.href='DisplayUsers?type=ldap_user'"><span class="count"><xsl:value-of select="users/ldap"/></span><xsl:value-of select="mioga:gettext('LDAP users count: ')"/></p>
						<p onclick="document.location.href='DisplayUsers?type=external_user'"><span class="count"><xsl:value-of select="users/external"/></span><xsl:value-of select="mioga:gettext('External users count: ')"/></p>
						<div class="ie_hack2"></div>
					</div>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:if>

	<xsl:if test="//UserRights/Administration = 1 or //UserRights/GroupsRead = 1 or //UserRights/GroupsWrite = 1 or //UserRights/GroupsCreate = 1">
		<div class="block_info">
			<xsl:call-template name="box">
				<xsl:with-param name="title"><xsl:value-of select="mioga:gettext('Groups')"/></xsl:with-param>
				<xsl:with-param name="content">
					<div id="groups_infos">
						<div class="ie_hack1"></div>
						<ul id="groups_actions" class="actions vmenu">
							<li>
								<a href="DisplayGroups">
									<xsl:value-of select="mioga:gettext('Show all groups')"/>
								</a>
							</li>
							<li>
								<a href="#" onclick="dijit.byId ('search_groups').show ();">
									<xsl:value-of select="mioga:gettext('Show some groups')"/>
								</a>
							</li>
						</ul>
						<p onclick="document.location.href='DisplayGroups'"><span class="count"><xsl:value-of select="groups/total"/></span><xsl:value-of select="mioga:gettext('Total groups count: ')"/></p>
						<div class="ie_hack2"></div>
					</div>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:if>

	<xsl:if test="//UserRights/Administration = 1 or //UserRights/TeamsRead = 1 or //UserRights/TeamsWrite = 1 or //UserRights/TeamsCreate = 1 or //UserRights/Reporting = 1">
		<div class="block_info">
			<xsl:call-template name="box">
				<xsl:with-param name="title"><xsl:value-of select="mioga:gettext('Teams')"/></xsl:with-param>
				<xsl:with-param name="content">
					<div id="teams_infos">
						<div class="ie_hack1"></div>
						<ul id="teams_actions" class="actions vmenu">
							<xsl:if test="//UserRights/Administration = 1 or //UserRights/TeamsRead = 1 or //UserRights/TeamsWrite = 1 or //UserRights/TeamsCreate = 1">
								<li>
									<a href="DisplayTeams">
										<xsl:value-of select="mioga:gettext('Show all teams')"/>
									</a>
								</li>
							<li>
								<a href="#" onclick="dijit.byId ('search_teams').show ();">
									<xsl:value-of select="mioga:gettext('Show some teams')"/>
								</a>
							</li>
							</xsl:if>
							<xsl:if test="//UserRights/Reporting = 1">
								<li>
									<a href="DisplayTeamGroupReport">
										<xsl:value-of select="mioga:gettext('Show teams to groups affectations')"/>
									</a>
								</li>
								<li>
									<a href="ExportTeamGroupReport">
										<xsl:value-of select="mioga:gettext('Export teams to groups affectations')"/>
									</a>
								</li>
							</xsl:if>
						</ul>
						<p onclick="document.location.href='DisplayTeams'"><span class="count"><xsl:value-of select="teams/total"/></span><xsl:value-of select="mioga:gettext('Total teams count: ')"/></p>
						<div class="ie_hack2"></div>
					</div>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:if>

	<xsl:if test="//UserRights/ResourcesRead = 1 or //UserRights/ResourcesWrite = 1 or //UserRights/ResourcesCreate = 1 or //UserRights/Administration = 1 or //UserRights/Base = 1">
		<div class="block_info">
			<xsl:call-template name="box">
				<xsl:with-param name="title"><xsl:value-of select="mioga:gettext('Miscellaneous')"/></xsl:with-param>
				<xsl:with-param name="content">
					<div id="misc_infos">
						<div class="ie_hack1"></div>
						<ul id="misc_actions" class="actions vmenu">
							<xsl:if test="//UserRights/Administration = 1">
								<li>
									<a href="DisplayApplications">
										<xsl:value-of select="mioga:gettext('Show applications')"/>
									</a>
								</li>
							</xsl:if>
							<xsl:if test="//UserRights/Administration = 1 or //UserRights/ResourcesCreate = 1 or //UserRights/ResourcesWrite = 1 or //UserRights/ResourcesRead = 1">
								<li>
									<a href="DisplayResources">
										<xsl:value-of select="mioga:gettext('Show resources')"/>
									</a>
								</li>
							</xsl:if>
							<xsl:if test="//UserRights/Administration = 1 or //UserRights/Base = 1">
								<li>
									<a href="DisplayTaskCategories">
										<xsl:value-of select="mioga:gettext('Show task categories')"/>
									</a>
								</li>
							</xsl:if>
							<xsl:if test="//UserRights/Administration = 1">
								<li>
									<a href="DisplayThemes">
										<xsl:value-of select="mioga:gettext('Show themes')"/>
									</a>
								</li>
								<li>
									<a href="DisplayTagLists">
										<xsl:value-of select="mioga:gettext('Show tag lists')"/>
									</a>
								</li>
								<li>
									<a href="DisplayExternalMiogas">
										<xsl:value-of select="mioga:gettext('Show external Mioga connections')"/>
									</a>
								</li>
								<li>
									<a href="GetPublicKey">
										<xsl:value-of select="mioga:gettext('Get public key')"/>
									</a>
								</li>
							</xsl:if>
						</ul>
						<p></p>
						<div class="ie_hack2"></div>
					</div>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:if>

	<div id="search_users" class="search_users_dialog form" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Show some users')"/></xsl:attribute>
		<form action="DisplayUsers" method="POST">
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Filter')"/></legend>
				<div class="count-container">
					<span class="counter-value"></span>&#160;<span class="matches-note" style="display:none"><xsl:value-of select="mioga:gettext ('match(es)')"/></span>
				</div>
				<div class="form-item">
					<label><xsl:value-of select="mioga:gettext ('Match type')"/></label>
					<select name="match" class="filter-field" dojoType="dijit.form.FilteringSelect" onkeypress="TriggerCount ('search_users', 'GetUsers');">
						<option value="begins"><xsl:value-of select="mioga:gettext ('Begins with')"/></option>
						<option value="contains"><xsl:value-of select="mioga:gettext ('Contains')"/></option>
						<option value="ends"><xsl:value-of select="mioga:gettext ('Ends with')"/></option>
					</select>
				</div>
				<div class="form-item">
					<label for="firstname"><xsl:value-of select="mioga:gettext ('Firstname')"/></label>
					<input type="text" class="" name="firstname" onkeypress="TriggerCount ('search_users', 'GetUsers');"/>
				</div>
				<div class="form-item">
					<label for="lastname"><xsl:value-of select="mioga:gettext ('Lastname')"/></label>
					<input type="text" name="lastname" onkeypress="TriggerCount ('search_users', 'GetUsers');"/>
				</div>
				<div class="form-item">
					<label for="email"><xsl:value-of select="mioga:gettext ('Email')"/></label>
					<input type="text" name="email" onkeypress="TriggerCount ('search_users', 'GetUsers');"/>
				</div>
				<div class="form-item">
					<label for="filter_form_users_type"><xsl:value-of select="mioga:gettext ('Type')"/></label>
					<span dojoType="mioga.data.ItemFileReadStore" jsId="filter_form_users_typeStore" label="label" url="GetUserTypes.json" nodename="type" identifier="ident"></span>
					<input id="filter_form_users_type" class="filter-field" dojoType="dijit.form.FilteringSelect" store="filter_form_users_typeStore" required="false" searchAttr="label" name="type"/>
				</div>
				<div class="form-item">
					<label for="filter_form_users_status"><xsl:value-of select="mioga:gettext ('Status')"/></label>
					<span dojoType="mioga.data.ItemFileReadStore" jsId="filter_form_users_statusStore" label="label" url="GetUserStatuses.json" nodename="status" identifier="ident"></span>
					<input id="filter_form_users_status" class="filter-field" dojoType="dijit.form.FilteringSelect" store="filter_form_users_statusStore" required="false" searchAttr="label" name="status"/>
				</div>

				<ul class="button_list">
					<li><button class="button" type="submit"><xsl:value-of select="mioga:gettext ('OK')"/></button></li>
					<li><a class="button cancel" href="#" onclick="dijit.byId ('search_users').hide ();"><xsl:value-of select="mioga:gettext ('Cancel')"/></a></li>
				</ul>
			</fieldset>
		</form>
	</div>

	<div id="search_groups" class="search_groups_dialog form" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Show some groups')"/></xsl:attribute>
		<form action="DisplayGroups" method="POST">
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Filter')"/></legend>
				<div class="count-container">
					<span class="counter-value"></span>&#160;<span class="matches-note" style="display:none"><xsl:value-of select="mioga:gettext ('match(es)')"/></span>
				</div>
				<div class="form-item">
					<label><xsl:value-of select="mioga:gettext ('Match type')"/></label>
					<select name="match" class="filter-field" dojoType="dijit.form.FilteringSelect" onkeypress="TriggerCount ('search_groups', 'GetGroups');">
						<option value="begins"><xsl:value-of select="mioga:gettext ('Begins with')"/></option>
						<option value="contains"><xsl:value-of select="mioga:gettext ('Contains')"/></option>
						<option value="ends"><xsl:value-of select="mioga:gettext ('Ends with')"/></option>
					</select>
				</div>
				<div class="form-item">
					<label for="ident"><xsl:value-of select="mioga:gettext ('Identifier')"/></label>
					<input type="text" class="" name="ident" onkeypress="TriggerCount ('search_groups', 'GetGroups');"/>
				</div>
				<div class="form-item">
					<label for="animator"><xsl:value-of select="mioga:gettext ('Animator')"/></label>
					<input type="text" name="animator" onkeypress="TriggerCount ('search_groups', 'GetGroups');"/>
				</div>

				<ul class="button_list">
					<li><button class="button" type="submit"><xsl:value-of select="mioga:gettext ('OK')"/></button></li>
					<li><a class="button cancel" href="#" onclick="dijit.byId ('search_groups').hide ();"><xsl:value-of select="mioga:gettext ('Cancel')"/></a></li>
				</ul>
			</fieldset>
		</form>
	</div>

	<div id="search_teams" class="search_teams_dialog form" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Show some teams')"/></xsl:attribute>
		<form action="DisplayTeams" method="POST">
			<fieldset>
				<div class="count-container">
					<span class="counter-value"></span>&#160;<span class="matches-note" style="display:none"><xsl:value-of select="mioga:gettext ('match(es)')"/></span>
				</div>
				<legend><xsl:value-of select="mioga:gettext ('Filter')"/></legend>
				<div class="form-item">
					<label><xsl:value-of select="mioga:gettext ('Match type')"/></label>
					<select name="match" class="filter-field" dojoType="dijit.form.FilteringSelect" onkeypress="TriggerCount ('search_teams', 'GetTeams');">
						<option value="begins"><xsl:value-of select="mioga:gettext ('Begins with')"/></option>
						<option value="contains"><xsl:value-of select="mioga:gettext ('Contains')"/></option>
						<option value="ends"><xsl:value-of select="mioga:gettext ('Ends with')"/></option>
					</select>
				</div>
				<div class="form-item">
					<label for="ident"><xsl:value-of select="mioga:gettext ('Identifier')"/></label>
					<input type="text" class="" name="ident" onkeypress="TriggerCount ('search_teams', 'GetTeams');"/>
				</div>

				<ul class="button_list">
					<li><button class="button" type="submit"><xsl:value-of select="mioga:gettext ('OK')"/></button></li>
					<li><a class="button cancel" href="#" onclick="dijit.byId ('search_teams').hide ();"><xsl:value-of select="mioga:gettext ('Cancel')"/></a></li>
				</ul>
			</fieldset>
		</form>
	</div>
</xsl:template>


<xsl:template match="UploadSkeletons" mode="head">
</xsl:template>

<xsl:template match="UploadSkeletons">
	<div id="upload-skeletons-errors">
		<xsl:if test="errors != ''">
			<ul>
				<xsl:for-each select="errors/err">
					<li><xsl:value-of select="mioga:gettext (@field)"/>: <xsl:value-of select="."/></li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</div>
	<script type="text/javascript">
		parent.UploadSkeletonsCallback (document.getElementById('upload-skeletons-errors').innerHTML);
	</script>
</xsl:template>

<!-- ===============
	DisplayUsers
	================ -->

<xsl:template match="DisplayUsers" mode="head">
	<xsl:variable name="ident"><xsl:value-of select="Grid[@name='users']/ident" /></xsl:variable>
	<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="filter_form_id">filter_form_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:apply-templates select="Grid" mode="head"/>

	<script type="text/javascript">
		var grid_id = '<xsl:value-of select="$grid_id" />';
		var filter_form_id = '<xsl:value-of select="$filter_form_id" />';
		var dataStoreUrl = '<xsl:value-of select="Grid/GetData" />';
		var dataStoreNodeName = '<xsl:value-of select="Grid/NodeName" />';
		var dataStoreIdentifier = '<xsl:value-of select="Grid/Identifier" />';
		var dataStoreLabel = '<xsl:value-of select="Grid/Label" />';
		var methodName = '<xsl:value-of select="name()"/>';
	</script>
</xsl:template>

<xsl:template match="DisplayUsers">
	<div id="border-main" dojoType="dijit.layout.BorderContainer" style="margin:0;padding:0;height:100%;">
		<div dojoType="dijit.layout.ContentPane" region="top">
			<!-- DisplayAppTitle -->
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#colbert</xsl:with-param>
			</xsl:call-template>

			<!-- Application navigator -->
			<xsl:call-template name="AppNav"/>
		</div>
		<div id="border-users" dojoType="dijit.layout.BorderContainer" region="center" style="margin:0;padding:0;height:100%;width:100%;">
			<!-- Actions -->
			<div dojoType="dijit.layout.ContentPane" region="left" style="width:20%;margin:0;padding:0;" splitter="false">
				<ul class="actions vmenu">
					<xsl:if test="//miogacontext/centralized_users = 0 and (//UserRights/Administration = 1 or //UserRights/UsersCreate = 1)">
						<li>
							<a href="javascript:void (0);">
								<xsl:attribute name="onclick"><xsl:value-of select="Grid/DefaultAction" />(null); return false;</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Add local user')"/>
							</a>
						</li>
						<li>
							<a href="javascript:void (0);">
								<xsl:attribute name="onclick">DisplayUserImportDialog ();</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Import users')"/>
							</a>
						</li>
					</xsl:if>
					<li>
						<a href="ExportUsers">
							<xsl:value-of select="mioga:gettext('Export users')"/>
						</a>
					</li>
					<xsl:if test="//miogacontext/centralized_users = 0 and (//UserRights/Administration = 1 or //UserRights/UsersCreate = 1)">
						<li>
							<a href="javascript:void (0);">
								<xsl:attribute name="onclick">DisplayExternalUserInviteDialog ();</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Invite external users')"/>
							</a>
						</li>
					</xsl:if>
					<xsl:if test="//UserRights/Administration = 1">
						<li>
							<a href="DownloadSkeletons?type=user">
								<xsl:value-of select="mioga:gettext ('Download skeletons')"/>
							</a>
						</li>
						<li>
							<a href="javascript:void (0);">
								<xsl:attribute name="onclick">DisplaySkeletonsUploadDialog ();</xsl:attribute>
								<xsl:value-of select="mioga:gettext ('Upload skeletons')"/>
							</a>
						</li>
						<li>
							<a href="javascript:void (0);">
								<xsl:attribute name="onclick">DisplayPasswordPolicyDialog ();</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Set password policy')"/>
							</a>
						</li>
					</xsl:if>
				</ul>
			</div>

			<!-- List -->
			<div dojoType="dijit.layout.ContentPane" region="center" style="margin:0;padding:0;" splitter="false">
				<!-- Users grid-->
				<xsl:variable name="ident"><xsl:value-of select="Grid[@name='users']/ident" /></xsl:variable>
				<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:apply-templates select="Grid[@name='users']">
					<xsl:with-param name="ident" select="$ident" />
					<xsl:with-param name="grid_id" select="$grid_id" />
					<xsl:with-param name="store_id" select="$store_id" />
					<xsl:with-param name="read_only"><xsl:if test="//miogacontext/centralized_users = 1 or (//UserRights/Administration = 0 and //UserRights/UsersCreate = 0 and //UserRights/UsersWrite = 0)">1</xsl:if></xsl:with-param>
					<xsl:with-param name="default_sort_field">lastname</xsl:with-param>
					<xsl:with-param name="store_prefs">StorePreferences</xsl:with-param>
					<xsl:with-param name="dojo_i18n_bundle">colbert</xsl:with-param>
				</xsl:apply-templates>
			</div>
		</div>
	</div>

	<div id="edit_user" class="edit_user_dialog form" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Edit user')"/></xsl:attribute>
		<div id="edit_user_error_block">
		</div>
		<div id="tabbed_dialog" dojoType="mioga.TabbedDialog" onValidate="ValidateUser ();" onCancel="CloseUserDialog ();">
			<div dojoType="mioga.TabItem" id="user-attributes-tab">
				<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('User {ident} Attributes')"/></xsl:attribute>
				<fieldset>
					<legend><xsl:value-of select="mioga:gettext('User Attributes')"/></legend>
					<!-- Information message -->
					<xsl:call-template name="InfoMessage">
						<xsl:with-param name="id">identifier_info</xsl:with-param>
						<xsl:with-param name="title">
							<xsl:value-of select="mioga:gettext('Identifier')"/>
						</xsl:with-param>
						<xsl:with-param name="message">
							<p>
								<xsl:value-of select="mioga:gettext ('The identifier is optional, you can define it or leave it empty to be auto-generated with a random sequence of characters.')"/>
							</p>
						</xsl:with-param>
					</xsl:call-template>
					<div id="user_attributes" dojoType="mioga.UserAttributes"></div>
				</fieldset>
			</div>
			<div dojoType="mioga.TabItem" id="user-groups-tab">
				<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('User {ident} Groups')"/></xsl:attribute>
				<div id="user_attributes_groups" dojoType="mioga.SelectList">
					<xsl:attribute name="sort_fields">group_sort_fields</xsl:attribute>
					<xsl:attribute name="sort">
						<xsl:choose>
							<xsl:when test="//preferences/grouplist/field != ''"><xsl:value-of select="//preferences/grouplist/field"/></xsl:when>
							<xsl:otherwise>ident</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="reverse">
						<xsl:choose>
							<xsl:when test="//preferences/grouplist/order != ''"><xsl:value-of select="//preferences/grouplist/order"/></xsl:when>
							<xsl:otherwise>1</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="store_prefs">function () { StorePreferences (); }</xsl:attribute>
					<xsl:attribute name="secondary_sort_field">"ident"</xsl:attribute>
					<xsl:attribute name="sel_label"><xsl:value-of select="mioga:gettext ('User is member')"/></xsl:attribute>
					<xsl:attribute name="unsel_label"><xsl:value-of select="mioga:gettext ('User is not member')"/></xsl:attribute>
				</div>
			</div>
			<div dojoType="mioga.TabItem" id="user-teams-tab">
				<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('User {ident} Teams')"/></xsl:attribute>
				<div id="user_attributes_teams" dojoType="mioga.SelectList">
					<xsl:attribute name="sort_fields">team_sort_fields</xsl:attribute>
					<xsl:attribute name="sort">
						<xsl:choose>
							<xsl:when test="//preferences/teamlist/field != ''"><xsl:value-of select="//preferences/teamlist/field"/></xsl:when>
							<xsl:otherwise>ident</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="reverse">
						<xsl:choose>
							<xsl:when test="//preferences/teamlist/order != ''"><xsl:value-of select="//preferences/teamlist/order"/></xsl:when>
							<xsl:otherwise>1</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="store_prefs">function () { StorePreferences (); }</xsl:attribute>
					<xsl:attribute name="secondary_sort_field">"ident"</xsl:attribute>
					<xsl:attribute name="sel_label"><xsl:value-of select="mioga:gettext ('User is member')"/></xsl:attribute>
					<xsl:attribute name="unsel_label"><xsl:value-of select="mioga:gettext ('User is not member')"/></xsl:attribute>
				</div>
			</div>
			<div dojoType="mioga.TabItem" id="user-apps-tab">
				<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('User {ident} Applications')"/></xsl:attribute>
				<div id="user_attributes_applications_label"></div>
				<div id="user_attributes_applications" dojoType="mioga.SelectList" label="label">
					<xsl:attribute name="sort_fields">application_sort_fields</xsl:attribute>
					<xsl:attribute name="sort">
						<xsl:choose>
							<xsl:when test="//preferences/applicationlist/field != ''"><xsl:value-of select="//preferences/applicationlist/field"/></xsl:when>
							<xsl:otherwise>label</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="reverse">
						<xsl:choose>
							<xsl:when test="//preferences/applicationlist/order != ''"><xsl:value-of select="//preferences/applicationlist/order"/></xsl:when>
							<xsl:otherwise>1</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="store_prefs">function () { StorePreferences (); }</xsl:attribute>
					<xsl:attribute name="secondary_sort_field">"label"</xsl:attribute>
					<xsl:attribute name="sel_label"><xsl:value-of select="mioga:gettext ('User is allowed to use')"/></xsl:attribute>
					<xsl:attribute name="unsel_label"><xsl:value-of select="mioga:gettext ('User is not allowed to use')"/></xsl:attribute>
				</div>
			</div>
		</div>
	</div>

	<div id="external_user_invite" class="external_user_invite_dialog" dojoType="dijit.Dialog" onValidate="ValidateExternalUserInvite ();">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Edit user')"/></xsl:attribute>
		<div id="external_user_invite_error_block">
		</div>
		<div dojoType="dijit.form.Form" id="external_user_invite_form" class="dojo-form">
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Invite external users')"/></legend>
				<div class="form-item">
					<label for="ext_mioga_id"><xsl:value-of select="mioga:gettext ('Server')"/></label>
					<div dojoType="mioga.data.ItemFileReadStore" jsId="extmStore" url="GetExternalMiogas.json" nodename="external_mioga" identifier="rowid"></div>
					<input dojoType="dijit.form.FilteringSelect" class="user-invite-input" id="external_user_server" name="ext_mioga_id" store="extmStore" searchAttr="server" onchange="UpdateExternalUserList ();" required="false"/>
				</div>

				<div id="external-user-skel-chooser" class="form-item">
					<label id="external_user_skeleton_label" for="skeleton"><xsl:value-of select="mioga:gettext ('Skeleton')"/></label>
					<div dojoType="mioga.data.ItemFileReadStore" jsId="skelStore" url="GetSkeletons.json?type=user" nodename="skeleton" identifier="file"></div>
					<input dojoType="dijit.form.FilteringSelect" class="user-invite-input" id="external_user_skeleton" name="skeleton" store="skelStore" searchAttr="name" required="false"/>
				</div>

				<div id="external-users-container" class="form-item">
					<div id="external_users" dojoType="mioga.SelectList" label="label" sort="lastname">
						<xsl:attribute name="sort_fields">user_sort_fields</xsl:attribute>
						<xsl:attribute name="secondary_sort_field">"lastname"</xsl:attribute>
						<xsl:attribute name="sort_highlight">1</xsl:attribute>
						<xsl:attribute name="sel_label"><xsl:value-of select="mioga:gettext ('User is invited')"/></xsl:attribute>
						<xsl:attribute name="unsel_label"><xsl:value-of select="mioga:gettext ('User is not invited')"/></xsl:attribute>
					</div>
				</div>
				<ul class="button_list">
					<li>
						<a class="button" onClick="ValidateExternalUserInvite ();"><xsl:value-of select="mioga:gettext ('Invite users')"/></a>
					</li>
				</ul>
			</fieldset>
		</div>
	</div>

	<!-- Import dialog -->
	<iframe id="form_submit_iframe" name="form_submit_iframe" style="width:0px;height:0px;display:none;">
	</iframe>
	<div id="import_users" class="import_users_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Import users')"/></xsl:attribute>
		<div id="import_users_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="import_users_error_block_content"></div>
		</div>
		<form dojoType="dijit.form.Form" target="form_submit_iframe" id="import_users_form" class="dojo-form" action="ImportUsers" method="post" enctype="multipart/form-data">
			<xsl:attribute name="onsubmit">ShowUnderlay ();</xsl:attribute>
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Select CSV file')"/></legend>
				<!-- Information message -->
				<xsl:call-template name="InfoMessage">
					<xsl:with-param name="id">skeleton_info</xsl:with-param>
					<xsl:with-param name="title">
						<xsl:value-of select="mioga:gettext('CSV file format')"/>
					</xsl:with-param>
					<xsl:with-param name="message">
						<p>
							<xsl:value-of select="mioga:gettext ('The file you upload must be a CSV file with quotes around the different values and comma as separator.')"/>
						</p>
						<p>
							<xsl:value-of select="mioga:gettext ('The file must contain the following information in the following order:')"/>
						</p>
						<ol>
							<li><xsl:value-of select="mioga:gettext ('Lastname')"/></li>
							<li><xsl:value-of select="mioga:gettext ('Firstname')"/></li>
							<li><xsl:value-of select="mioga:gettext ('Email')"/></li>
							<li><xsl:value-of select="mioga:gettext ('Identifier')"/></li>
							<li><xsl:value-of select="mioga:gettext ('Password')"/></li>
							<li><xsl:value-of select="mioga:gettext ('Skeleton file')"/></li>
						</ol>
						<p>
							<xsl:value-of select="mioga:gettext ('The identifier is not mandatory, it can be empty but must be present. In case the identifier is empty, it will be randomly generated by Mioga.')"/>
						</p>
						<p>
							<xsl:value-of select="mioga:gettext ('The skeleton value is not the skeleton name but the associated XML file. Default skeleton filename is 50-standard.xml but it may have been changed by your instance administrator, in this case, you should ask him the list of valid skeleton file names.')"/>
						</p>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="CSRF-input"/>
				<div class="form-item">
					<label for="file"><xsl:value-of select="mioga:gettext ('CSV file')"/></label>
					<input type="file" id="file" name="file"/>
				</div>
				<div class="form-item">
					<label for="mode"><xsl:value-of select="mioga:gettext ('Mode')"/></label>
					<select dojoType="dijit.form.FilteringSelect" class="import-users-input" name="mode" required="false">
						<option value="create"><xsl:value-of select="mioga:gettext ('Create')"/></option>
						<option value="update"><xsl:value-of select="mioga:gettext ('Update')"/></option>
					</select>
				</div>
				<ul class="button_list">
					<li>
						<button class="button" type="submit"><xsl:value-of select="mioga:gettext ('Import')"/></button>
					</li>
				</ul>
			</fieldset>
		</form>
	</div>

	<!-- Skeleton upload dialog -->
	<iframe id="form_submit_iframe" name="form_submit_iframe" style="width:0px;height:0px;display:none;">
	</iframe>
	<div id="upload_skeletons" class="upload_skeletons_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Upload skeletons')"/></xsl:attribute>
		<div id="upload_skeletons_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="upload_skeletons_error_block_content"></div>
		</div>
		<form dojoType="dijit.form.Form" target="form_submit_iframe" id="upload_skeletons_form" class="dojo-form" action="UploadSkeletons" method="post" enctype="multipart/form-data">
			<xsl:attribute name="onsubmit">ShowUnderlay ();</xsl:attribute>
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Select a skeletons archive')"/></legend>
				<div class="form-item">
					<input type="hidden" name="type" value="user"/>
				</div>
				<xsl:call-template name="CSRF-input"/>
				<div class="form-item">
					<label for="file"><xsl:value-of select="mioga:gettext ('Skeletons ZIP archive')"/></label>
					<input type="file" id="file" name="skeletons_archive"/>
				</div>
				<ul class="button_list">
					<li>
						<button class="button" type="submit"><xsl:value-of select="mioga:gettext ('Upload')"/></button>
					</li>
				</ul>
			</fieldset>
		</form>
	</div>

	<!-- Password policy dialog -->
	<div dojoType="dijit.Dialog" id="password_policy_dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Password policy')"/></xsl:attribute>
		<div id="password_policy_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="password_policy_error_block_content"></div>
		</div>
		<div dojoType="dijit.form.Form" id="password_policy_form" class="dojo-form">
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Password policy')"/></legend>
				<div class="form-item">
					<label id="use_secret_question_label" for="use_secret_question"><xsl:value-of select="mioga:gettext ('Secure passwords with secret question')"/></label>
					<input dojoType="dijit.form.CheckBox" class="instance-security-attributes-input" id="instance_use_secret_question" name="use_secret_question"/>
				</div>
				<div class="form-item">
					<label id="pwd_min_length_label" for="pwd_min_length"><xsl:value-of select="mioga:gettext ('Minimum length')"/></label>
					<input dojoType="dijit.form.NumberSpinner" class="instance-security-attributes-input" id="instance_pwd_min_length" name="pwd_min_length">
						<xsl:attribute name="constraints">{min:0}</xsl:attribute>
					</input>
				</div>
				<div class="form-item">
					<label id="pwd_min_letter_label" for="pwd_min_letter"><xsl:value-of select="mioga:gettext ('Minimum number of letters')"/></label>
					<input dojoType="dijit.form.NumberSpinner" class="instance-security-attributes-input" id="instance_pwd_min_letter" name="pwd_min_letter">
						<xsl:attribute name="constraints">{min:0}</xsl:attribute>
					</input>
				</div>
				<div class="form-item">
					<label id="pwd_min_digit_label" for="pwd_min_digit"><xsl:value-of select="mioga:gettext ('Minimum number of digits')"/></label>
					<div dojoType="dijit.form.NumberSpinner" class="instance-security-attributes-input" id="instance_pwd_min_digit" name="pwd_min_digit">
						<xsl:attribute name="constraints">{min:0}</xsl:attribute>
					</div>
				</div>
				<div class="form-item">
					<label id="pwd_min_special_label" for="pwd_min_special"><xsl:value-of select="mioga:gettext ('Minimum number of special characters')"/></label>
					<input dojoType="dijit.form.NumberSpinner" class="instance-security-attributes-input" id="instance_pwd_min_special" name="pwd_min_special">
						<xsl:attribute name="constraints">{min:0}</xsl:attribute>
					</input>
				</div>
				<div class="form-item">
					<label id="pwd_min_chcase_label" for="pwd_min_chcase"><xsl:value-of select="mioga:gettext ('Minimum number of case changes')"/></label>
					<input dojoType="dijit.form.NumberSpinner" class="instance-security-attributes-input" id="instance_pwd_min_chcase" name="pwd_min_chcase">
						<xsl:attribute name="constraints">{min:0}</xsl:attribute>
					</input>
				</div>
				<ul class="button_list">
					<li>
						<a class="button"  onClick="ValidatePasswordPolicy ();"><xsl:value-of select="mioga:gettext ('Update')"/></a>
					</li>
				</ul>
			</fieldset>
		</div>
	</div>
</xsl:template>

<!-- ===============
	DisplayGroups
	================ -->

<xsl:template match="DisplayGroups" mode="head">
	<xsl:variable name="ident"><xsl:value-of select="Grid[@name='groups']/ident" /></xsl:variable>
	<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="filter_form_id">filter_form_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:apply-templates select="Grid" mode="head"/>

	<script type="text/javascript">
		var grid_id = '<xsl:value-of select="$grid_id" />';
		var filter_form_id = '<xsl:value-of select="$filter_form_id" />';
		var dataStoreUrl = '<xsl:value-of select="Grid/GetData" />';
		var dataStoreNodeName = '<xsl:value-of select="Grid/NodeName" />';
		var dataStoreIdentifier = '<xsl:value-of select="Grid/Identifier" />';
		var dataStoreLabel = '<xsl:value-of select="Grid/Label" />';
		var methodName = '<xsl:value-of select="name()"/>';
	</script>
</xsl:template>

<xsl:template match="DisplayGroups">
	<div id="border-main" dojoType="dijit.layout.BorderContainer" style="margin:0;padding:0;height:100%; width:100%">
		<div dojoType="dijit.layout.ContentPane" region="top">
			<!-- DisplayAppTitle -->
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#colbert</xsl:with-param>
			</xsl:call-template>

			<!-- Application navigator -->
			<xsl:call-template name="AppNav"/>
		</div>
		<div id="border-groups" dojoType="dijit.layout.BorderContainer" region="center" style="margin:0;padding:0;height:100%; width:100%">
			<!-- Actions -->
			<div dojoType="dijit.layout.ContentPane" region="left" style="width:20%;margin:0;padding:0;" splitter="false">
				<ul class="actions vmenu">
					<xsl:if test="//UserRights/Administration = 1 or //UserRights/GroupsCreate = 1">
						<li>
							<a href="javascript:void (0);">
								<xsl:attribute name="onclick"><xsl:value-of select="Grid/DefaultAction" />(null); return false;</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Add group')"/>
							</a>
						</li>
					</xsl:if>
					<xsl:if test="//UserRights/Administration = 1">
						<li>
							<a href="DownloadSkeletons?type=group">
								<xsl:value-of select="mioga:gettext ('Download skeletons')"/>
							</a>
						</li>
						<li>
							<a href="javascript:void (0);">
								<xsl:attribute name="onclick">DisplaySkeletonsUploadDialog ();</xsl:attribute>
								<xsl:value-of select="mioga:gettext ('Upload skeletons')"/>
							</a>
						</li>
					</xsl:if>
				</ul>
			</div>

			<!-- List -->
			<div dojoType="dijit.layout.ContentPane" region="center" style="margin:0;padding:0;" splitter="false">
				<!-- Groups grid-->
				<xsl:variable name="ident"><xsl:value-of select="Grid[@name='groups']/ident" /></xsl:variable>
				<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:apply-templates select="Grid[@name='groups']">
					<xsl:with-param name="ident" select="$ident" />
					<xsl:with-param name="grid_id" select="$grid_id" />
					<xsl:with-param name="store_id" select="$store_id" />
					<xsl:with-param name="read_only"><xsl:if test="//UserRights/Administration = 0 and //UserRights/GroupsCreate = 0 and //UserRights/GroupsWrite = 0">1</xsl:if></xsl:with-param>
					<xsl:with-param name="default_sort_field">ident</xsl:with-param>
					<xsl:with-param name="store_prefs">StorePreferences</xsl:with-param>
					<xsl:with-param name="dojo_i18n_bundle">colbert</xsl:with-param>
				</xsl:apply-templates>
			</div>
		</div>
	</div>

	<div id="edit_group" class="edit_group_dialog form" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Edit group')"/></xsl:attribute>
		<div id="edit_group_error_block">
		</div>
		<div id="tabbed_dialog" dojoType="mioga.TabbedDialog" onValidate="ValidateGroup ();" onCancel="CloseGroupDialog ();">
			<div dojoType="mioga.TabItem" id="group-attributes-tab">
				<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Group {ident} Attributes')"/></xsl:attribute>
				<fieldset>
					<legend><xsl:value-of select="mioga:gettext('Group Attributes')"/></legend>
					<div id="group_attributes" dojoType="mioga.GroupAttributes"></div>
				</fieldset>
			</div>
			<div dojoType="mioga.TabItem" id="group-users-tab">
				<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Group {ident} Users')"/></xsl:attribute>
				<div id="group_users" dojoType="mioga.SelectList" label="label">
					<xsl:attribute name="sort_fields">user_sort_fields</xsl:attribute>
					<xsl:attribute name="sort">
						<xsl:choose>
							<xsl:when test="//preferences/userlist/field != ''"><xsl:value-of select="//preferences/userlist/field"/></xsl:when>
							<xsl:otherwise>lastname</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="reverse">
						<xsl:choose>
							<xsl:when test="//preferences/userlist/order != ''"><xsl:value-of select="//preferences/userlist/order"/></xsl:when>
							<xsl:otherwise>1</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="store_prefs">function () { StorePreferences (); }</xsl:attribute>
					<xsl:attribute name="secondary_sort_field">"lastname"</xsl:attribute>
					<xsl:attribute name="sort_highlight">1</xsl:attribute>
					<xsl:attribute name="sel_label"><xsl:value-of select="mioga:gettext ('Group contains')"/></xsl:attribute>
					<xsl:attribute name="unsel_label"><xsl:value-of select="mioga:gettext ('Group does not contain')"/></xsl:attribute>
				</div>
			</div>
			<div dojoType="mioga.TabItem" id="group-teams-tab">
				<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Group {ident} Teams')"/></xsl:attribute>
				<div id="group_teams" dojoType="mioga.SelectList">
					<xsl:attribute name="sort_fields">team_sort_fields</xsl:attribute>
					<xsl:attribute name="sort">
						<xsl:choose>
							<xsl:when test="//preferences/teamlist/field != ''"><xsl:value-of select="//preferences/teamlist/field"/></xsl:when>
							<xsl:otherwise>ident</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="reverse">
						<xsl:choose>
							<xsl:when test="//preferences/teamlist/order != ''"><xsl:value-of select="//preferences/teamlist/order"/></xsl:when>
							<xsl:otherwise>1</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="store_prefs">function () { StorePreferences (); }</xsl:attribute>
					<xsl:attribute name="secondary_sort_field">"ident"</xsl:attribute>
					<xsl:attribute name="sel_label"><xsl:value-of select="mioga:gettext ('Group contains')"/></xsl:attribute>
					<xsl:attribute name="unsel_label"><xsl:value-of select="mioga:gettext ('Group does not contain')"/></xsl:attribute>
				</div>
			</div>
			<div dojoType="mioga.TabItem" id="group-apps-tab">
				<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Group {ident} Applications')"/></xsl:attribute>
				<div id="group_attributes_applications_label"></div>
				<div id="group_applications" dojoType="mioga.SelectList" label="label">
					<xsl:attribute name="sort_fields">application_sort_fields</xsl:attribute>
					<xsl:attribute name="sort">
						<xsl:choose>
							<xsl:when test="//preferences/applicationlist/field != ''"><xsl:value-of select="//preferences/applicationlist/field"/></xsl:when>
							<xsl:otherwise>label</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="reverse">
						<xsl:choose>
							<xsl:when test="//preferences/applicationlist/order != ''"><xsl:value-of select="//preferences/applicationlist/order"/></xsl:when>
							<xsl:otherwise>1</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="store_prefs">function () { StorePreferences (); }</xsl:attribute>
					<xsl:attribute name="secondary_sort_field">"label"</xsl:attribute>
					<xsl:attribute name="sel_label"><xsl:value-of select="mioga:gettext ('Group is allowed to use')"/></xsl:attribute>
					<xsl:attribute name="unsel_label"><xsl:value-of select="mioga:gettext ('Group is not allowed to use')"/></xsl:attribute>
				</div>
			</div>
		</div>
	</div>

	<!-- Skeleton upload dialog -->
	<iframe id="form_submit_iframe" name="form_submit_iframe" style="width:0px;height:0px;display:none;">
	</iframe>
	<div id="upload_skeletons" class="upload_skeletons_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Upload skeletons')"/></xsl:attribute>
		<div id="upload_skeletons_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="upload_skeletons_error_block_content"></div>
		</div>
		<form dojoType="dijit.form.Form" target="form_submit_iframe" id="upload_skeletons_form" class="dojo-form" action="UploadSkeletons" method="post" enctype="multipart/form-data">
			<xsl:attribute name="onsubmit">ShowUnderlay ();</xsl:attribute>
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Select a skeletons archive')"/></legend>
				<div class="form-item">
					<input type="hidden" name="type" value="group"/>
				</div>
				<xsl:call-template name="CSRF-input"/>
				<div class="form-item">
					<label for="file"><xsl:value-of select="mioga:gettext ('Skeletons ZIP archive')"/></label>
					<input type="file" id="file" name="skeletons_archive"/>
				</div>
				<ul class="button_list">
					<li>
						<button class="button" type="submit"><xsl:value-of select="mioga:gettext ('Upload')"/></button>
					</li>
				</ul>
			</fieldset>
		</form>
	</div>
</xsl:template>

<!-- ===============
	DisplayTeams
	================ -->

<xsl:template match="DisplayTeams" mode="head">
	<xsl:variable name="ident"><xsl:value-of select="Grid[@name='teams']/ident" /></xsl:variable>
	<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="filter_form_id">filter_form_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:apply-templates select="Grid" mode="head"/>

	<script type="text/javascript">
		var grid_id = '<xsl:value-of select="$grid_id" />';
		var filter_form_id = '<xsl:value-of select="$filter_form_id" />';
		var dataStoreUrl = '<xsl:value-of select="Grid/GetData" />';
		var dataStoreNodeName = '<xsl:value-of select="Grid/NodeName" />';
		var dataStoreIdentifier = '<xsl:value-of select="Grid/Identifier" />';
		var dataStoreLabel = '<xsl:value-of select="Grid/Label" />';
		var methodName = '<xsl:value-of select="name()"/>';
	</script>
</xsl:template>

<xsl:template match="DisplayTeams">
	<div id="border-main" dojoType="dijit.layout.BorderContainer" style="margin:0;padding:0;height:100%; width:100%">
		<div dojoType="dijit.layout.ContentPane" region="top">
			<!-- DisplayAppTitle -->
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#colbert</xsl:with-param>
			</xsl:call-template>

			<!-- Application navigator -->
			<xsl:call-template name="AppNav"/>
		</div>
		<div id="border-teams" dojoType="dijit.layout.BorderContainer" region="center" style="margin:0;padding:0;height:100%; width:100%">
			<!-- Actions -->
			<div dojoType="dijit.layout.ContentPane" region="left" style="width:20%;margin:0;padding:0;" splitter="false">
				<ul class="actions vmenu">
					<xsl:if test="//UserRights/Administration = 1 or //UserRights/TeamsCreate = 1">
						<li>
							<a href="javascript:void (0);">
								<xsl:attribute name="onclick"><xsl:value-of select="Grid/DefaultAction" />(null); return false;</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Add team')"/>
							</a>
						</li>
						<li>
							<a href="javascript:void (0);">
								<xsl:attribute name="onclick">DisplayUserTeamImportDialog ();</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Import user / team affectations')"/>
							</a>
						</li>
						<li>
							<a href="javascript:void (0);">
								<xsl:attribute name="onclick">DisplayTeamGroupImportDialog ();</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Import team / group affectations')"/>
							</a>
						</li>
					</xsl:if>
				</ul>
			</div>

			<!-- List -->
			<div dojoType="dijit.layout.ContentPane" region="center" style="margin:0;padding:0;" splitter="false">
				<!-- Teams grid-->
				<xsl:variable name="ident"><xsl:value-of select="Grid[@name='teams']/ident" /></xsl:variable>
				<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:apply-templates select="Grid[@name='teams']">
					<xsl:with-param name="ident" select="$ident" />
					<xsl:with-param name="grid_id" select="$grid_id" />
					<xsl:with-param name="store_id" select="$store_id" />
					<xsl:with-param name="read_only"><xsl:if test="//UserRights/Administration = 0 and //UserRights/TeamsCreate = 0 and //UserRights/TeamsWrite = 0">1</xsl:if></xsl:with-param>
					<xsl:with-param name="default_sort_field">ident</xsl:with-param>
					<xsl:with-param name="store_prefs">StorePreferences</xsl:with-param>
					<xsl:with-param name="dojo_i18n_bundle">colbert</xsl:with-param>
				</xsl:apply-templates>
			</div>
		</div>
	</div>

	<div id="edit_team" class="edit_team_dialog form" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Edit team')"/></xsl:attribute>
		<div id="edit_team_error_block">
		</div>
		<div id="tabbed_dialog" dojoType="mioga.TabbedDialog" onValidate="ValidateTeam ();" onCancel="CloseTeamDialog ();">
			<div dojoType="mioga.TabItem" id="team-attributes-tab">
				<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Team {ident} Attributes')"/></xsl:attribute>
				<fieldset>
					<legend><xsl:value-of select="mioga:gettext('Team Attributes')"/></legend>
					<div id="team_attributes" dojoType="mioga.TeamAttributes"></div>
				</fieldset>
			</div>
			<div dojoType="mioga.TabItem" id="team-users-tab">
				<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Team {ident} Users')"/></xsl:attribute>
				<div id="team_users" dojoType="mioga.SelectList" label="label">
					<xsl:attribute name="sort_fields">user_sort_fields</xsl:attribute>
					<xsl:attribute name="sort">
						<xsl:choose>
							<xsl:when test="//preferences/userlist/field != ''"><xsl:value-of select="//preferences/userlist/field"/></xsl:when>
							<xsl:otherwise>lastname</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="reverse">
						<xsl:choose>
							<xsl:when test="//preferences/userlist/order != ''"><xsl:value-of select="//preferences/userlist/order"/></xsl:when>
							<xsl:otherwise>1</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="store_prefs">function () { StorePreferences (); }</xsl:attribute>
					<xsl:attribute name="secondary_sort_field">"lastname"</xsl:attribute>
					<xsl:attribute name="sort_highlight">1</xsl:attribute>
					<xsl:attribute name="sel_label"><xsl:value-of select="mioga:gettext ('Team contains')"/></xsl:attribute>
					<xsl:attribute name="unsel_label"><xsl:value-of select="mioga:gettext ('Team does not contain')"/></xsl:attribute>
				</div>
			</div>
			<div dojoType="mioga.TabItem" id="team-groups-tab">
				<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Team {ident} Groups')"/></xsl:attribute>
				<div id="team_groups" dojoType="mioga.SelectList">
					<xsl:attribute name="sort_fields">group_sort_fields</xsl:attribute>
					<xsl:attribute name="sort">
						<xsl:choose>
							<xsl:when test="//preferences/grouplist/field != ''"><xsl:value-of select="//preferences/grouplist/field"/></xsl:when>
							<xsl:otherwise>ident</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="reverse">
						<xsl:choose>
							<xsl:when test="//preferences/grouplist/order != ''"><xsl:value-of select="//preferences/grouplist/order"/></xsl:when>
							<xsl:otherwise>1</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="secondary_sort_field">"ident"</xsl:attribute>
					<xsl:attribute name="sel_label"><xsl:value-of select="mioga:gettext ('Team is member')"/></xsl:attribute>
					<xsl:attribute name="unsel_label"><xsl:value-of select="mioga:gettext ('Team is not member')"/></xsl:attribute>
					<xsl:attribute name="store_prefs">function () { StorePreferences (); }</xsl:attribute>
				</div>
			</div>
		</div>
	</div>

	<!-- Import dialog -->
	<iframe id="form_submit_iframe" name="form_submit_iframe" style="width:0px;height:0px;display:none;">
	</iframe>
	<div id="import_user_teams" class="import_teams_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Import user / team affectations')"/></xsl:attribute>
		<div id="import_user_teams_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="import_user_teams_error_block_content"></div>
		</div>
		<form dojoType="dijit.form.Form" target="form_submit_iframe" id="import_user_teams_form" class="dojo-form" action="ImportUserTeams" method="post" enctype="multipart/form-data">
			<xsl:attribute name="onsubmit">ShowUnderlay ();</xsl:attribute>
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Select CSV file')"/></legend>
				<!-- Information message -->
				<xsl:call-template name="InfoMessage">
					<xsl:with-param name="id">user-teams-csv</xsl:with-param>
					<xsl:with-param name="title">
						<xsl:value-of select="mioga:gettext('CSV file format')"/>
					</xsl:with-param>
					<xsl:with-param name="message">
						<p>
							<xsl:value-of select="mioga:gettext ('The file you upload must be a CSV file with quotes around the different values and comma as separator.')"/>
						</p>
						<p>
							<xsl:value-of select="mioga:gettext ('The file must contain the following information in the following order:')"/>
						</p>
						<ol>
							<li><xsl:value-of select="mioga:gettext ('Email')"/></li>
							<li><xsl:value-of select="mioga:gettext ('Team identifier')"/></li>
						</ol>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="CSRF-input"/>
				<div class="form-item">
					<label for="file"><xsl:value-of select="mioga:gettext ('CSV file')"/></label>
					<input type="file" id="file" name="file"/>
				</div>
				<ul class="button_list">
					<li>
						<button class="button" type="submit"><xsl:value-of select="mioga:gettext ('Import')"/></button>
					</li>
				</ul>
			</fieldset>
		</form>
	</div>

	<div id="import_team_groups" class="import_teams_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Import team / group affectations')"/></xsl:attribute>
		<div id="import_team_groups_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="import_team_groups_error_block_content"></div>
		</div>
		<form dojoType="dijit.form.Form" target="form_submit_iframe" id="import_team_groups_form" class="dojo-form" action="ImportTeamGroups" method="post" enctype="multipart/form-data">
			<xsl:attribute name="onsubmit">ShowUnderlay ();</xsl:attribute>
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Select CSV file')"/></legend>
				<!-- Information message -->
				<xsl:call-template name="InfoMessage">
					<xsl:with-param name="id">teams-groups-csv</xsl:with-param>
					<xsl:with-param name="title">
						<xsl:value-of select="mioga:gettext('CSV file format')"/>
					</xsl:with-param>
					<xsl:with-param name="message">
						<p>
							<xsl:value-of select="mioga:gettext ('The file you upload must be a CSV file with quotes around the different values and comma as separator.')"/>
						</p>
						<p>
							<xsl:value-of select="mioga:gettext ('The file must contain the following information in the following order:')"/>
						</p>
						<ol>
							<li><xsl:value-of select="mioga:gettext ('Team identifier')"/></li>
							<li><xsl:value-of select="mioga:gettext ('Group identifier')"/></li>
							<li><xsl:value-of select="mioga:gettext ('Profile name')"/></li>
						</ol>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="CSRF-input"/>
				<div class="form-item">
					<label for="file"><xsl:value-of select="mioga:gettext ('CSV file')"/></label>
					<input type="file" id="file" name="file"/>
				</div>
				<ul class="button_list">
					<li>
						<button class="button" type="submit"><xsl:value-of select="mioga:gettext ('Import')"/></button>
					</li>
				</ul>
			</fieldset>
		</form>
	</div>
</xsl:template>


<xsl:template match="ImportUserTeams" mode="head">
</xsl:template>

<xsl:template match="ImportUserTeams">
	<div id="import-user-teams-errors">
		<xsl:if test="Import != ''">
			<ul>
				<xsl:for-each select="Import">
					<li>
						<xsl:value-of select="mioga:gettext ('Line')"/>&#160;
						<xsl:value-of select="./line"/>
						:&#160;
						<xsl:value-of select="./error"/>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</div>
	<script type="text/javascript">
		parent.UserTeamImportCallback (document.getElementById('import-user-teams-errors').innerHTML);
	</script>
</xsl:template>


<xsl:template match="ImportTeamGroups" mode="head">
</xsl:template>

<xsl:template match="ImportTeamGroups">
	<div id="import-team-groups-errors">
		<xsl:if test="Import != ''">
			<ul>
				<xsl:for-each select="Import">
					<li>
						<xsl:value-of select="mioga:gettext ('Line')"/>&#160;
						<xsl:value-of select="./line"/>
						:&#160;
						<xsl:value-of select="./error"/>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</div>
	<script type="text/javascript">
		parent.TeamGroupImportCallback (document.getElementById('import-team-groups-errors').innerHTML);
	</script>
</xsl:template>

<!-- ===============
	DisplayExternalMiogas
	================ -->

<xsl:template match="DisplayExternalMiogas" mode="head">
	<xsl:variable name="ident"><xsl:value-of select="Grid[@name='external_miogas']/ident" /></xsl:variable>
	<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="filter_form_id">filter_form_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:apply-templates select="Grid" mode="head"/>

	<script type="text/javascript">
		var grid_id = '<xsl:value-of select="$grid_id" />';
		var filter_form_id = '<xsl:value-of select="$filter_form_id" />';
		var dataStoreUrl = '<xsl:value-of select="Grid/GetData" />';
		var dataStoreNodeName = '<xsl:value-of select="Grid/NodeName" />';
		var dataStoreIdentifier = '<xsl:value-of select="Grid/Identifier" />';
		var dataStoreLabel = '<xsl:value-of select="Grid/Label" />';
		var methodName = '<xsl:value-of select="name()"/>';
	</script>
</xsl:template>

<xsl:template match="DisplayExternalMiogas">
	<div id="border-main" dojoType="dijit.layout.BorderContainer" style="margin:0;padding:0;height:100%; width:100%">
		<div dojoType="dijit.layout.ContentPane" region="top">
			<!-- DisplayAppTitle -->
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#colbert</xsl:with-param>
			</xsl:call-template>

			<!-- Application navigator -->
			<xsl:call-template name="AppNav"/>
		</div>
		<div id="border-external-miogas" dojoType="dijit.layout.BorderContainer" region="center" style="margin:0;padding:0;height:100%; width:100%">
			<!-- Actions -->
			<div dojoType="dijit.layout.ContentPane" region="left" style="width:20%;margin:0;padding:0;" splitter="false">
				<ul class="actions vmenu">
					<li>
						<a href="javascript:void (0);" onclick="DisplayExtMiogaDialog ();">
							<xsl:value-of select="mioga:gettext('Add external Mioga')"/>
						</a>
					</li>
				</ul>
			</div>

			<!-- List -->
			<div dojoType="dijit.layout.ContentPane" region="center" style="margin:0;padding:0;" splitter="false">
				<!-- External Mioga grid-->
				<xsl:variable name="ident"><xsl:value-of select="Grid[@name='external_miogas']/ident" /></xsl:variable>
				<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:apply-templates select="Grid[@name='external_miogas']">
					<xsl:with-param name="ident" select="$ident" />
					<xsl:with-param name="grid_id" select="$grid_id" />
					<xsl:with-param name="store_id" select="$store_id" />
					<xsl:with-param name="default_sort_field">server</xsl:with-param>
					<xsl:with-param name="store_prefs">StorePreferences</xsl:with-param>
					<xsl:with-param name="dojo_i18n_bundle">colbert</xsl:with-param>
				</xsl:apply-templates>
			</div>
		</div>
	</div>

	<iframe id="form_submit_iframe" name="form_submit_iframe" style="width:0px;height:0px;display:none;">
	</iframe>
	<div id="add_ext_mioga" class="add_ext_mioga_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Add external Mioga')"/></xsl:attribute>
		<div id="add_ext_mioga_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="add_ext_mioga_error_block_content"></div>
		</div>
		<form dojoType="dijit.form.Form" target="form_submit_iframe" id="ext_mioga_form" class="dojo-form" action="SetExternalMioga" method="post" enctype="multipart/form-data">
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Select public key')"/></legend>
				<div class="form-item">
					<label for="public_key"><xsl:value-of select="mioga:gettext ('Public key')"/></label>
					<input type="file" id="public_key" name="public_key"/>
				</div>
				<ul class="button_list">
					<li>
						<button class="button" type="submit"><xsl:value-of select="mioga:gettext ('Add')"/></button>
					</li>
				</ul>
			</fieldset>
		</form>
	</div>
</xsl:template>


<xsl:template match="SetExternalMioga" mode="head">
</xsl:template>

<xsl:template match="SetExternalMioga">
	<div id="set-external-mioga-errors">
		<xsl:if test="error != ''">
			<ul>
				<xsl:for-each select="error">
					<li>
						<xsl:value-of select="."/>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</div>
	<script type="text/javascript">
		parent.ExtMiogaCreateCallback (document.getElementById('set-external-mioga-errors').innerHTML);
	</script>
</xsl:template>

<!-- ===============
	DisplayTaskCategories
	================ -->

<xsl:template match="DisplayTaskCategories" mode="head">
	<xsl:variable name="ident"><xsl:value-of select="Grid[@name='task_categories']/ident" /></xsl:variable>
	<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="filter_form_id">filter_form_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:apply-templates select="Grid" mode="head"/>

	<script type="text/javascript">
		var grid_id = '<xsl:value-of select="$grid_id" />';
		var filter_form_id = '<xsl:value-of select="$filter_form_id" />';
		var dataStoreUrl = '<xsl:value-of select="Grid/GetData" />';
		var dataStoreNodeName = '<xsl:value-of select="Grid/NodeName" />';
		var dataStoreIdentifier = '<xsl:value-of select="Grid/Identifier" />';
		var dataStoreLabel = '<xsl:value-of select="Grid/Label" />';
		var methodName = '<xsl:value-of select="name()"/>';
	</script>
</xsl:template>

<xsl:template match="DisplayTaskCategories">
	<div id="border-main" dojoType="dijit.layout.BorderContainer" style="margin:0;padding:0;height:100%; width:100%">
		<div dojoType="dijit.layout.ContentPane" region="top">
			<!-- DisplayAppTitle -->
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#colbert</xsl:with-param>
			</xsl:call-template>

			<!-- Application navigator -->
			<xsl:call-template name="AppNav"/>
		</div>
		<div id="border-task-categories" dojoType="dijit.layout.BorderContainer" region="center" style="margin:0;padding:0;height:100%; width:100%">
			<!-- Actions -->
			<div dojoType="dijit.layout.ContentPane" region="left" style="width:20%;margin:0;padding:0;" splitter="false">
				<ul class="actions vmenu">
					<li>
						<a href="javascript:void (0);">
							<xsl:attribute name="onclick"><xsl:value-of select="Grid/DefaultAction" />(null); return false;</xsl:attribute>
							<xsl:value-of select="mioga:gettext('Add task category')"/>
						</a>
					</li>
				</ul>
			</div>

			<!-- List -->
			<div dojoType="dijit.layout.ContentPane" region="center" style="margin:0;padding:0;" splitter="false">
				<!-- Task categories grid-->
				<xsl:variable name="ident"><xsl:value-of select="Grid[@name='task_categories']/ident" /></xsl:variable>
				<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:apply-templates select="Grid[@name='task_categories']">
					<xsl:with-param name="ident" select="$ident" />
					<xsl:with-param name="grid_id" select="$grid_id" />
					<xsl:with-param name="store_id" select="$store_id" />
					<xsl:with-param name="default_sort_field">label</xsl:with-param>
					<xsl:with-param name="store_prefs">StorePreferences</xsl:with-param>
					<xsl:with-param name="dojo_i18n_bundle">colbert</xsl:with-param>
				</xsl:apply-templates>
			</div>
		</div>
	</div>

	<!-- Task category dialog -->
	<div id="task_category" class="task_category_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Edit task category')"/></xsl:attribute>
		<div id="task_category_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="task_category_error_block_content"></div>
		</div>
		<div dojoType="dijit.form.Form" id="task_category_form" class="dojo-form">
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Task category')"/></legend>
				<input type="hidden" dojoType="dijit.form.TextBox" name="rowid"/>
				<div class="form-item">
					<label for="name" id="task_category_name_label"><xsl:value-of select="mioga:gettext ('Name')"/></label>
					<input dojoType="dijit.form.TextBox" class="task-category-input" name="name"/>
				</div>
				<div class="form-item">
					<label for="bgcolor" id="task_category_bgcolor_label"><xsl:value-of select="mioga:gettext ('Background color')"/></label>
					<div dojoType="dijit.ColorPalette" id="bgcolor"></div>
				</div>
				<div class="form-item">
					<label for="fgcolor" id="task_category_fgcolor_label"><xsl:value-of select="mioga:gettext ('Foreground color')"/></label>
					<div dojoType="dijit.ColorPalette" id="fgcolor"></div>
				</div>
				<div class="form-item">
					<label for="private"><xsl:value-of select="mioga:gettext ('Private tasks')"/></label>
					<input dojoType="dijit.form.CheckBox" name="private" id="task_category_private"/>
				</div>
				<ul class="button_list">
					<li>
						<span class="button" onclick="ValidateTaskCategory ();"><xsl:value-of select="mioga:gettext ('OK')"/></span>
					</li>
				</ul>
			</fieldset>
		</div>
	</div>
</xsl:template>

<!-- ===============
	DisplayResources
	================ -->

<xsl:template match="DisplayResources" mode="head">
	<xsl:variable name="ident"><xsl:value-of select="Grid[@name='resources']/ident" /></xsl:variable>
	<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="filter_form_id">filter_form_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:apply-templates select="Grid" mode="head"/>

	<script type="text/javascript">
		var grid_id = '<xsl:value-of select="$grid_id" />';
		var filter_form_id = '<xsl:value-of select="$filter_form_id" />';
		var dataStoreUrl = '<xsl:value-of select="Grid/GetData" />';
		var dataStoreNodeName = '<xsl:value-of select="Grid/NodeName" />';
		var dataStoreIdentifier = '<xsl:value-of select="Grid/Identifier" />';
		var dataStoreLabel = '<xsl:value-of select="Grid/Label" />';
		var methodName = '<xsl:value-of select="name()"/>';
	</script>
</xsl:template>

<xsl:template match="DisplayResources">
	<div id="border-main" dojoType="dijit.layout.BorderContainer" style="margin:0;padding:0;height:100%; width:100%">
		<div dojoType="dijit.layout.ContentPane" region="top">
			<!-- DisplayAppTitle -->
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#colbert</xsl:with-param>
			</xsl:call-template>

			<!-- Application navigator -->
			<xsl:call-template name="AppNav"/>
		</div>
		<div id="border-resources" dojoType="dijit.layout.BorderContainer" region="center" style="margin:0;padding:0;height:100%; width:100%">
			<!-- Actions -->
			<div dojoType="dijit.layout.ContentPane" region="left" style="width:20%;margin:0;padding:0;" splitter="false">
				<ul class="actions vmenu">
					<xsl:if test="//UserRights/Administration = 1 or //UserRights/ResourcesCreate = 1">
						<li>
							<a href="javascript:void (0);">
								<xsl:attribute name="onclick"><xsl:value-of select="Grid/DefaultAction" />(null); return false;</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Add resource')"/>
							</a>
						</li>
					</xsl:if>
				</ul>
			</div>

			<!-- List -->
			<div dojoType="dijit.layout.ContentPane" region="center" style="margin:0;padding:0;" splitter="false">
				<!-- Resources grid-->
				<xsl:variable name="ident"><xsl:value-of select="Grid[@name='resources']/ident" /></xsl:variable>
				<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:apply-templates select="Grid[@name='resources']">
					<xsl:with-param name="ident" select="$ident" />
					<xsl:with-param name="grid_id" select="$grid_id" />
					<xsl:with-param name="store_id" select="$store_id" />
					<xsl:with-param name="read_only"><xsl:if test="//UserRights/Administration = 0 and //UserRights/ResourcesCreate = 0 and //UserRights/ResourcesWrite = 0">1</xsl:if></xsl:with-param>
					<xsl:with-param name="default_sort_field">ident</xsl:with-param>
					<xsl:with-param name="store_prefs">StorePreferences</xsl:with-param>
					<xsl:with-param name="dojo_i18n_bundle">colbert</xsl:with-param>
				</xsl:apply-templates>
			</div>
		</div>
	</div>

	<!-- Resource dialog -->
	<div id="resource" class="resource_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Edit resource')"/></xsl:attribute>
		<div id="resource_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="resource_error_block_content"></div>
		</div>
		<div dojoType="dijit.form.Form" id="resource_form" class="dojo-form">
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Resource')"/></legend>
				<input type="hidden" dojoType="dijit.form.TextBox" name="rowid"/>
				<div class="form-item">
					<label for="ident" id="resource_ident_label"><xsl:value-of select="mioga:gettext ('Identifier')"/></label>
					<input dojoType="dijit.form.TextBox" class="edit-resource-input" name="ident" id="resource_ident"/>
				</div>
				<div class="form-item">
					<label for="location" id="resource_location_label"><xsl:value-of select="mioga:gettext ('Location')"/></label>
					<input dojoType="dijit.form.TextBox" class="edit-resource-input" name="location" />
				</div>
				<div class="form-item">
					<label for="description" id="resource_description_label"><xsl:value-of select="mioga:gettext ('Description')"/></label>
					<input dojoType="dijit.form.Textarea" class="edit-resource-input" name="description" />
				</div>
				<div class="form-item">
					<label for="animator" id="resource_anim_id_label"><xsl:value-of select="mioga:gettext ('Animator')"/></label>
					<div dojoType="mioga.data.ItemFileReadStore" jsId="animStore" url="GetUsers.json" nodename="user" identifier="rowid"></div>
					<input dojoType="dijit.form.FilteringSelect" class="edit-resource-input" id="resource_animator" name="anim_id" store="animStore" searchAttr="label" autoComplete="false" required="false">
						<xsl:attribute name="queryExpr">*${0}*</xsl:attribute>
					</input>
						<script type="text/javascript">
							dojo.addOnLoad (function () {
								animStore.comparatorMap = {};
								animStore.comparatorMap['label'] = function (a, b) {
									if ((typeof (a) == 'string') &amp;&amp; (typeof (b) == 'string')) {
										var a = unaccent (a.toLowerCase());
										var b = unaccent (b.toLowerCase());
									}

									if (a&gt;b) return 1;
									if (a&lt;b) return -1;
									return 0;
								};
							});
						</script>
					</div>
				<ul class="button_list">
					<li>
						<span class="button" onclick="ValidateResource ();"><xsl:value-of select="mioga:gettext ('OK')"/></span>
					</li>
				</ul>
			</fieldset>
		</div>
	</div>
</xsl:template>

<!-- ===============
	DisplayThemes
	================ -->

<xsl:template match="DisplayThemes" mode="head">
	<xsl:variable name="ident"><xsl:value-of select="Grid[@name='themes']/ident" /></xsl:variable>
	<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="filter_form_id">filter_form_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:apply-templates select="Grid" mode="head"/>

	<script type="text/javascript">
		var grid_id = '<xsl:value-of select="$grid_id" />';
		var filter_form_id = '<xsl:value-of select="$filter_form_id" />';
		var dataStoreUrl = '<xsl:value-of select="Grid/GetData" />';
		var dataStoreNodeName = '<xsl:value-of select="Grid/NodeName" />';
		var dataStoreIdentifier = '<xsl:value-of select="Grid/Identifier" />';
		var dataStoreLabel = '<xsl:value-of select="Grid/Label" />';
		var methodName = '<xsl:value-of select="name()"/>';
	</script>
</xsl:template>

<xsl:template match="DisplayThemes">
	<div id="border-main" dojoType="dijit.layout.BorderContainer" style="margin:0;padding:0;height:100%; width:100%">
		<div dojoType="dijit.layout.ContentPane" region="top">
			<!-- DisplayAppTitle -->
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#colbert</xsl:with-param>
			</xsl:call-template>

			<!-- Application navigator -->
			<xsl:call-template name="AppNav"/>
		</div>
		<div id="border-themes" dojoType="dijit.layout.BorderContainer" region="center" style="margin:0;padding:0;height:100%; width:100%">
			<!-- Actions -->
			<div dojoType="dijit.layout.ContentPane" region="left" style="width:20%;margin:0;padding:0;" splitter="false">
				<ul class="actions vmenu">
					<li>
						<a href="javascript:void (0);">
							<xsl:attribute name="onclick">DisplayThemeDialog ();</xsl:attribute>
							<xsl:value-of select="mioga:gettext('Add theme')"/>
						</a>
					</li>
				</ul>
			</div>

			<!-- List -->
			<div dojoType="dijit.layout.ContentPane" region="center" style="margin:0;padding:0;" splitter="false">
				<!-- Themes grid-->
				<xsl:variable name="ident"><xsl:value-of select="Grid[@name='themes']/ident" /></xsl:variable>
				<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:apply-templates select="Grid[@name='themes']">
					<xsl:with-param name="ident" select="$ident" />
					<xsl:with-param name="grid_id" select="$grid_id" />
					<xsl:with-param name="store_id" select="$store_id" />
					<xsl:with-param name="default_sort_field">name</xsl:with-param>
					<xsl:with-param name="store_prefs">StorePreferences</xsl:with-param>
					<xsl:with-param name="dojo_i18n_bundle">colbert</xsl:with-param>
				</xsl:apply-templates>
			</div>
		</div>
	</div>

	<!-- Theme dialog -->
	<div id="theme" class="theme_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Add theme')"/></xsl:attribute>
		<div id="theme_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="theme_error_block_content"></div>
		</div>
		<div dojoType="dijit.form.Form" id="theme_form" class="dojo-form">
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Theme')"/></legend>
				<div class="form-item">
					<label for="ident" id="theme_ident_label"><xsl:value-of select="mioga:gettext ('Identifier')"/></label>
					<input dojoType="dijit.form.TextBox" class="theme-attributes-input" name="ident" id="theme_ident"/>
				</div>
				<div class="form-item">
					<label for="name" id="theme_name_label"><xsl:value-of select="mioga:gettext ('Name')"/></label>
					<input dojoType="dijit.form.TextBox" class="theme-attributes-input" name="name" />
				</div>
				<div class="form-item">
					<label for="path" id="theme_path_label"><xsl:value-of select="mioga:gettext ('Folder')"/></label>
					<input dojoType="dijit.form.TextBox" class="theme-attributes-input" name="path">
						<xsl:attribute name="value"><xsl:value-of select="./miogacontext/uri/private_dav"/>/themes</xsl:attribute>
					</input>
				</div>
				<ul class="button_list">
					<li>
						<span class="button" onclick="ValidateTheme ();"><xsl:value-of select="mioga:gettext ('OK')"/></span>
					</li>
				</ul>
			</fieldset>
		</div>
	</div>
</xsl:template>

<!-- ===============
	DisplayApplications
	================ -->

<xsl:template match="DisplayApplications" mode="head">
	<xsl:variable name="ident"><xsl:value-of select="Grid[@name='applications']/ident" /></xsl:variable>
	<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="filter_form_id">filter_form_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:apply-templates select="Grid" mode="head"/>

	<script type="text/javascript">
		var grid_id = '<xsl:value-of select="$grid_id" />';
		var filter_form_id = '<xsl:value-of select="$filter_form_id" />';
		var dataStoreUrl = '<xsl:value-of select="Grid/GetData" />';
		var dataStoreNodeName = '<xsl:value-of select="Grid/NodeName" />';
		var dataStoreIdentifier = '<xsl:value-of select="Grid/Identifier" />';
		var dataStoreLabel = '<xsl:value-of select="Grid/Label" />';
		var methodName = '<xsl:value-of select="name()"/>';
	</script>
</xsl:template>

<xsl:template match="DisplayApplications">
	<div id="border-main" dojoType="dijit.layout.BorderContainer" style="margin:0;padding:0;height:100%; width:100%">
		<div dojoType="dijit.layout.ContentPane" region="top">
			<!-- DisplayAppTitle -->
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#colbert</xsl:with-param>
			</xsl:call-template>

			<!-- Application navigator -->
			<xsl:call-template name="AppNav"/>
		</div>
		<div id="border-applications" dojoType="dijit.layout.BorderContainer" region="center" style="margin:0;padding:0;height:100%; width:100%">
			<!-- List -->
			<div dojoType="dijit.layout.ContentPane" region="center" style="margin:0;padding:0;" splitter="false">
				<!-- Applications grid-->
				<xsl:variable name="ident"><xsl:value-of select="Grid[@name='applications']/ident" /></xsl:variable>
				<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:apply-templates select="Grid[@name='applications']">
					<xsl:with-param name="ident" select="$ident" />
					<xsl:with-param name="grid_id" select="$grid_id" />
					<xsl:with-param name="store_id" select="$store_id" />
					<xsl:with-param name="default_sort_field">label</xsl:with-param>
					<xsl:with-param name="store_prefs">StorePreferences</xsl:with-param>
					<xsl:with-param name="dojo_i18n_bundle">colbert</xsl:with-param>
				</xsl:apply-templates>
			</div>
		</div>
	</div>

	<!-- Applications dialog -->
	<div id="edit_application" class="edit_application_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Edit application')"/></xsl:attribute>
		<div id="edit_application_error_block">
		</div>
		<div id="tabbed_dialog" dojoType="mioga.TabbedDialog" onValidate="ValidateApplication ();" onCancel="CloseApplicationDialog ();">
			<div dojoType="dijit.form.Form" id="application_attributes_form" class="dojo-form">
				<input type="hidden" dojoType="dijit.form.TextBox" name="rowid"/>
				<div dojoType="mioga.TabItem" id="application-users-tab">
					<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Users that can use application {ident}')"/></xsl:attribute>
					<ul class="hmenu">
						<li>
							<a href="javascript:void (0);" onclick="SelectAllUsersOrGroups ('application_attributes_users');"><xsl:value-of select="mioga:gettext ('Allow all users to use this application')"/></a>
						</li>
					</ul>
					<div class="clear-both"></div>
					<div id="application_attributes_users" dojoType="mioga.SelectList" label="label">
						<xsl:attribute name="sort_fields">user_sort_fields</xsl:attribute>
						<xsl:attribute name="sort">
							<xsl:choose>
								<xsl:when test="//preferences/userlist/field != ''"><xsl:value-of select="//preferences/userlist/field"/></xsl:when>
								<xsl:otherwise>lastname</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="reverse">
							<xsl:choose>
								<xsl:when test="//preferences/userlist/order != ''"><xsl:value-of select="//preferences/userlist/order"/></xsl:when>
								<xsl:otherwise>1</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="store_prefs">function () { StorePreferences (); }</xsl:attribute>
						<xsl:attribute name="secondary_sort_field">"lastname"</xsl:attribute>
						<xsl:attribute name="sort_highlight">1</xsl:attribute>
						<xsl:attribute name="sel_label"><xsl:value-of select="mioga:gettext ('User is allowed to use')"/></xsl:attribute>
						<xsl:attribute name="unsel_label"><xsl:value-of select="mioga:gettext ('User is not allowed to use')"/></xsl:attribute>
					</div>
				</div>
				<div dojoType="mioga.TabItem" id="application-groups-tab">
					<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Groups that can use application {ident}')"/></xsl:attribute>
					<ul class="hmenu">
						<li>
							<a href="javascript:void (0);" onclick="SelectAllUsersOrGroups ('application_attributes_groups');"><xsl:value-of select="mioga:gettext ('Allow all groups to use this application')"/></a>
						</li>
					</ul>
					<div class="clear-both"></div>
					<div id="application_attributes_groups" dojoType="mioga.SelectList">
						<xsl:attribute name="sort_fields">group_sort_fields</xsl:attribute>
						<xsl:attribute name="sort">
							<xsl:choose>
								<xsl:when test="//preferences/grouplist/field != ''"><xsl:value-of select="//preferences/grouplist/field"/></xsl:when>
								<xsl:otherwise>ident</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="reverse">
							<xsl:choose>
								<xsl:when test="//preferences/grouplist/order != ''"><xsl:value-of select="//preferences/grouplist/order"/></xsl:when>
								<xsl:otherwise>1</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="store_prefs">function () { StorePreferences (); }</xsl:attribute>
						<xsl:attribute name="secondary_sort_field">"ident"</xsl:attribute>
						<xsl:attribute name="sel_label"><xsl:value-of select="mioga:gettext ('Group is allowed to use')"/></xsl:attribute>
						<xsl:attribute name="unsel_label"><xsl:value-of select="mioga:gettext ('Group is not allowed to use')"/></xsl:attribute>
					</div>
				</div>
			</div>
		</div>
	</div>
</xsl:template>

<!-- ===============
	DisplayUserGroupReport
	================ -->

<xsl:template match="DisplayUserGroupReport" mode="head">
</xsl:template>

<xsl:template match="DisplayUserGroupReport">
	<div id="border-main" style="margin:0;padding:0;height:100%;">
		<div>
			<!-- DisplayAppTitle -->
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#colbert</xsl:with-param>
			</xsl:call-template>

			<!-- Application navigator -->
			<xsl:call-template name="AppNav"/>
		</div>

		<div class="synthesis">
			<h1><xsl:value-of select="mioga:gettext ('User to groups affectations')"/></h1>
			<div class="synthesis-container">
				<table class="list border_color">
					<tr>
						<th><xsl:value-of select="mioga:gettext ('Firstname')"/></th>
						<th><xsl:value-of select="mioga:gettext ('Lastname')"/></th>
						<th><xsl:value-of select="mioga:gettext ('Email')"/></th>
						<!-- Add columns for groups -->
						<xsl:for-each select="//Report/user[1]/group">
							<th><xsl:value-of select="group_ident"/></th>
						</xsl:for-each>
					</tr>
					<xsl:for-each select="//Report/user">
						<xsl:variable name="user_tooltip"><xsl:value-of select="firstname"/>&#160;<xsl:value-of select="lastname"/>&#160;(<xsl:value-of select="email"/>)</xsl:variable>
						<tr>
							<td><xsl:value-of select="firstname"/></td>
							<td><xsl:value-of select="lastname"/></td>
							<td><xsl:value-of select="email"/></td>
							<xsl:for-each select="group">
								<xsl:variable name="group_tooltip"><xsl:value-of select="group_ident"/></xsl:variable>
								<td>
									<xsl:attribute name="title"><xsl:value-of select="$user_tooltip"/>&#160;/&#160;<xsl:value-of select="$group_tooltip"/></xsl:attribute>
									<ul>
										<xsl:for-each select="profile">
											<li><xsl:value-of select="profile_ident"/></li>
										</xsl:for-each>
									</ul>
								</td>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</table>
			</div>
		</div>
	</div>
</xsl:template>

<!-- ===============
	DisplayUserTeamReport
	================ -->

<xsl:template match="DisplayUserTeamReport" mode="head">
</xsl:template>

<xsl:template match="DisplayUserTeamReport">
	<div id="border-main" style="margin:0;padding:0;height:100%;">
		<div>
			<!-- DisplayAppTitle -->
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#colbert</xsl:with-param>
			</xsl:call-template>

			<!-- Application navigator -->
			<xsl:call-template name="AppNav"/>
		</div>

		<div class="synthesis">
			<h1><xsl:value-of select="mioga:gettext ('User to teams affectations')"/></h1>
			<div class="synthesis-container">
				<table class="list border_color">
					<tr>
						<th><xsl:value-of select="mioga:gettext ('Firstname')"/></th>
						<th><xsl:value-of select="mioga:gettext ('Lastname')"/></th>
						<th><xsl:value-of select="mioga:gettext ('Email')"/></th>
						<!-- Add columns for teams -->
						<xsl:for-each select="//Report/user[1]/team">
							<th><xsl:value-of select="team_ident"/></th>
						</xsl:for-each>
					</tr>
					<xsl:for-each select="//Report/user">
						<xsl:variable name="user_tooltip"><xsl:value-of select="firstname"/>&#160;<xsl:value-of select="lastname"/>&#160;(<xsl:value-of select="email"/>)</xsl:variable>
						<tr>
							<td><xsl:value-of select="firstname"/></td>
							<td><xsl:value-of select="lastname"/></td>
							<td><xsl:value-of select="email"/></td>
							<xsl:for-each select="team">
								<xsl:variable name="team_tooltip"><xsl:value-of select="team_ident"/></xsl:variable>
								<td>
									<xsl:attribute name="title"><xsl:value-of select="$user_tooltip"/>&#160;/&#160;<xsl:value-of select="$team_tooltip"/></xsl:attribute>
									<xsl:if test="is_member = 1"><div class="is-member"></div></xsl:if>
								</td>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</table>
			</div>
		</div>
	</div>
</xsl:template>

<!-- ===============
	DisplayTeamGroupReport
	================ -->

<xsl:template match="DisplayTeamGroupReport" mode="head">
</xsl:template>

<xsl:template match="DisplayTeamGroupReport">
	<div id="border-main" style="margin:0;padding:0;height:100%;">
		<div>
			<!-- DisplayAppTitle -->
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#colbert</xsl:with-param>
			</xsl:call-template>

			<!-- Application navigator -->
			<xsl:call-template name="AppNav"/>
		</div>

		<div class="synthesis">
			<h1><xsl:value-of select="mioga:gettext ('Team to groups affectations')"/></h1>
			<div class="synthesis-container">
				<table class="list border_color">
					<tr>
						<th><xsl:value-of select="mioga:gettext ('Team')"/></th>
						<!-- Add columns for groups -->
						<xsl:for-each select="//Report/team[1]/group">
							<th><xsl:value-of select="group_ident"/></th>
						</xsl:for-each>
					</tr>
					<xsl:for-each select="//Report/team">
						<xsl:variable name="team_tooltip"><xsl:value-of select="ident"/></xsl:variable>
						<tr>
							<td><xsl:value-of select="ident"/></td>
							<xsl:for-each select="group">
								<xsl:variable name="group_tooltip"><xsl:value-of select="group_ident"/></xsl:variable>
								<td>
									<xsl:attribute name="title"><xsl:value-of select="$team_tooltip"/>&#160;/&#160;<xsl:value-of select="$group_tooltip"/></xsl:attribute>
									<ul>
										<xsl:for-each select="profile">
											<li><xsl:value-of select="profile_ident"/></li>
										</xsl:for-each>
									</ul>
								</td>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</table>
			</div>
		</div>
	</div>
</xsl:template>

<!-- ===============
	DisplayTagLists
	================ -->

<xsl:template match="DisplayTagLists" mode="head">
</xsl:template>

<xsl:template match="DisplayTagLists">
	<xsl:call-template name="AppNav"/>
	<div id="taglist-box">
		<div id="taglist-actions">
			<ul class="actions vmenu">
				<li>
					<a onclick="DisplayTaglistImportDialog ();" href="#"><xsl:value-of select="mioga:gettext ('Import tags from file')"/></a>
				</li>
				<li>
					<a href="#">
						<xsl:attribute name="onclick">
							ToggleFreeTags ();
						</xsl:attribute>
						<xsl:choose>
							<xsl:when test="free_tags = 1">
								<xsl:value-of select="mioga:gettext ('Disable free tags')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="mioga:gettext ('Enable free tags')"/>
							</xsl:otherwise>
						</xsl:choose>
					</a>
				</li>
			</ul>
		</div>
		<div id="taglist-container">
			<table class="list">
				<tr>
					<th colspan="2"><xsl:value-of select="mioga:gettext ('Catalog')"/></th>
					<th><xsl:value-of select="mioga:gettext ('Tags')"/></th>
					<!-- <th><xsl:value-of select="mioga:gettext ('Delete')"/></th> -->
				</tr>
				<xsl:for-each select="catalog">
					<tr>
						<td class="filename">
							<a>
								<xsl:attribute name="href">GetTagsCatalog?catalog_id=<xsl:value-of select="rowid"/></xsl:attribute>
								<xsl:choose>
									<xsl:when test="ident != ''">
										<xsl:value-of select="ident"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="mioga:gettext ('Virtual catalog of free tags')"/>
									</xsl:otherwise>
								</xsl:choose>
							</a>
						</td>
						<td class="actions">
							<a class="delete-icon" href="#">
								<xsl:attribute name="onclick">
									if (confirm ("<xsl:value-of select="mioga:gettext ('Please confirm deletion of tags catalog')"/>&#160;<xsl:value-of select="ident"/>")) {
										DeleteTagsCatalog (<xsl:value-of select="rowid"/>);
									};
								</xsl:attribute>
								<div class="delete-icon">&#160;</div>
							</a>
						</td>
						<td><xsl:value-of select="tags"/></td>
					</tr>
				</xsl:for-each>
			</table>
		</div>
	</div>

	<!-- Import dialog -->
	<iframe id="form_submit_iframe" name="form_submit_iframe" style="width:0px;height:0px;display:none;">
	</iframe>
	<div id="import_taglist" class="import_taglist_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Import tags from file')"/></xsl:attribute>
		<div id="import_taglist_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="import_taglist_error_block_content"></div>
		</div>
		<form dojoType="dijit.form.Form" target="form_submit_iframe" id="import_taglist_form" class="dojo-form" action="SetTagList" method="post" enctype="multipart/form-data">
			<xsl:attribute name="onsubmit">ShowUnderlay ();</xsl:attribute>
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Select tag file')"/></legend>
				<!-- Information message -->
				<xsl:call-template name="InfoMessage">
					<xsl:with-param name="id">tag-file-info</xsl:with-param>
					<xsl:with-param name="title">
						<xsl:value-of select="mioga:gettext('Tag file format')"/>
					</xsl:with-param>
					<xsl:with-param name="message">
						<p>
							<xsl:value-of select="mioga:gettext ('The file you upload must contain a list of tags, one per line.')"/>
						</p>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="CSRF-input"/>
				<div class="form-item">
					<label for="file"><xsl:value-of select="mioga:gettext ('Tag file')"/></label>
					<input type="file" id="file" name="file"/>
				</div>
				<ul class="button_list">
					<li>
						<button class="button" type="submit"><xsl:value-of select="mioga:gettext ('Import')"/></button>
					</li>
				</ul>
			</fieldset>
		</form>
	</div>
</xsl:template>



<!-- ===============
	DisplayCatalogTags
	================ -->

<xsl:template match="DisplayCatalogTags" mode="head">
</xsl:template>

<xsl:template match="DisplayCatalogTags">
	<xsl:call-template name="AppNav"/>
	<h1><xsl:value-of select="mioga:gettext ('Tags in catalog')"/>&#160;<xsl:value-of select="catalog_name"/></h1>
	<ul class="tag-list">
		<xsl:for-each select="tag">
			<li><xsl:value-of select="."/></li>
		</xsl:for-each>
	</ul>
</xsl:template>


<!-- Info image and associated tooltip -->
<xsl:template name="InfoMessage">
	<xsl:param name="id"/>
	<xsl:param name="title"/>
	<xsl:param name="message"/>

	<div class="info">
		<img id="{$id}" src="{$theme_uri}/images/16x16/emblems/emblem-info.png"/>
		<div dojoType="dijit.Tooltip" connectId="{$id}">
			<h1><xsl:copy-of select="$title"/></h1>
			<div>
				<xsl:copy-of select="$message"/>
			</div>
		</div>
	</div>
</xsl:template>

</xsl:stylesheet>
