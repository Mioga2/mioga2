<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<xsl:output method="html"/>
<xsl:include href="base.xsl"/>
<xsl:include href="organizer_base.xsl"/>
<xsl:include href="organizer_form.xsl"/>
<xsl:include href="organizer_translation.xsl"/>
<xsl:include href="simple_large_list.xsl"/>
<xsl:include href="scriptaculous.xsl"/>
<xsl:include href="inline_message.xsl"/>

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="calendar-js"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="scriptaculous-js"/>
	<xsl:call-template name="theme-css"/>
</head>
	<xsl:apply-templates />
</html>
</xsl:template>


<!-- *********************************************************************** -->
<!-- * ViewDay                                                             * -->
<!-- *********************************************************************** -->
<xsl:template match="ViewDay">
   <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>
        
      <!-- Status message -->
      <xsl:if test="@message != ''">
         <xsl:call-template name="InlineMessage">
            <xsl:with-param name="type" select="@type"/>
            <xsl:with-param name="message" select="@message"/>
         </xsl:call-template>
      </xsl:if>

      <xsl:call-template name="TopControl">
         <xsl:with-param name="now_day"    select="@now_day"/>
         <xsl:with-param name="now_month"  select="@now_month"/>
         <xsl:with-param name="now_year"   select="@now_year"/>
         <xsl:with-param name="now_hour"   select="@now_hour"/>
         <xsl:with-param name="now_minute" select="@now_minute"/>
         <xsl:with-param name="now_method" select="'ViewDay'"/>
         <xsl:with-param name="day"        select="@day"/>
         <xsl:with-param name="month"      select="Calendar/@month"/>
         <xsl:with-param name="year"       select="Calendar/@year"/>
         <xsl:with-param name="startYear"  select="@startYear"/>
         <xsl:with-param name="stopYear"   select="@stopYear"/>
      </xsl:call-template>
      <table border="0" cellspacing="20" width="100%">
         <tr><td valign="top" width="10%">
            <xsl:call-template name="LeftControl"/>
         </td><td valign="top" width="90%">
            <xsl:for-each select="DayTasks">
               <xsl:call-template name="table">
                  <xsl:with-param name="table_width">100%</xsl:with-param>
               </xsl:call-template>
            </xsl:for-each>
         </td></tr>
      </table>
   </body>
</xsl:template>

<xsl:variable name="percentByFlexibleTask">2</xsl:variable>

<xsl:template match="DayTasks" mode="table-title">
   <th width="10%">
       <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
              <td align="right">
                  <a href="{$private_bin_uri}/Organizer/ViewDay?day={/ViewDay/@prev_year}-{/ViewDay/@prev_month}-{/ViewDay/@prev_day}">
                      <img src="{$image_uri}/16x16/actions/go-previous.png" alt="" border="0"/>
                  </a>
              </td>
              <td>
                  <b>
                      <font>
                          <xsl:choose>
                              <xsl:when test="../@isWorkingDay = 0">
                                  <xsl:attribute name="class"><xsl:value-of select="$nonworking_bgcolor"/></xsl:attribute>
                              </xsl:when>
                              <xsl:otherwise>
                                  <xsl:attribute name="class"><xsl:value-of select="$mioga-list-title-text-color"/></xsl:attribute>
                              </xsl:otherwise>
                          </xsl:choose>
                          <center>
                              <xsl:value-of select="/ViewDay/@day"/><br/>
                              <xsl:call-template name="Month-name">
                                  <xsl:with-param name="month_number" select="/ViewDay/Calendar/@month"/>
                              </xsl:call-template>
                          </center>
                      </font>
                  </b>
              </td>
              <td align="left">
                  <a href="{$private_bin_uri}/Organizer/ViewDay?day={/ViewDay/@next_year}-{/ViewDay/@next_month}-{/ViewDay/@next_day}">
                      <img src="{$image_uri}/16x16/actions/go-next.png" alt="" border="0"/>
                  </a>
              </td>
          </tr>
      </table>
  </th>
  <th width="{90-$percentByFlexibleTask*number(@flexibleTasks)}%">
      <font>
         <xsl:choose>
            <xsl:when test="../@isWorkingDay = 0">
               <xsl:attribute name="class"><xsl:value-of select="$nonworking_bgcolor"/></xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
               <xsl:attribute name="class"><xsl:value-of select="$mioga-list-title-text-color"/></xsl:attribute>
            </xsl:otherwise>
         </xsl:choose>
         <xsl:call-template name="organizerTranslation">
           <xsl:with-param name="type">label</xsl:with-param>
         </xsl:call-template>
      </font>
   </th>
   <xsl:if test ="@flexibleTasks &gt; 0">
      <td width="{$percentByFlexibleTask*number(@flexibleTasks)}%" colspan="{@flexibleTasks}"></td>
   </xsl:if>
</xsl:template>

<xsl:template match="DayTasks" mode="table-body">
   <xsl:variable name="day" select="../@day"/>
   <xsl:variable name="month" select="../Calendar/@month"/>
   <xsl:variable name="year" select="../Calendar/@year"/>

   <xsl:for-each select="DayTask">
         <tr>
            <xsl:if test="@rowspan != 0">
                <xsl:choose>
                  <xsl:when test="position() mod 2 = 0">
                     <xsl:attribute name="class"><xsl:value-of select="$mioga-list-even-row-bg-color"/></xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color"/></xsl:attribute>
                  </xsl:otherwise>
               </xsl:choose>
               <xsl:element name="td">
                  <xsl:attribute name="align">center</xsl:attribute>
                  <xsl:attribute name="rowspan"><xsl:value-of select="@rowspan"/></xsl:attribute>
                  <xsl:attribute name="width"><xsl:text disable-output-escaping="yes">10%</xsl:text></xsl:attribute>
                  <xsl:if test="@conflict=1">
                      <xsl:attribute name="class"><xsl:value-of select="$conflict_bgcolor"/></xsl:attribute>
                  </xsl:if>
                  <xsl:variable name="element_name">
                     <xsl:choose>
                        <xsl:when test="@method ='none' or @private=1">font</xsl:when>
                        <xsl:otherwise>a</xsl:otherwise>
                     </xsl:choose>
                  </xsl:variable>
                  <xsl:element name="{$element_name}">
                     <xsl:if test="$element_name = 'a'">
                        <xsl:attribute name="href">
                           <xsl:choose>
                              <xsl:when test="@method = 'EditStrictTask'">
                                 <xsl:value-of select="$private_bin_uri"/>/Organizer/<xsl:value-of select="@method"/>?taskrowid=<xsl:value-of select="@taskrowid"/>&amp;mode=initial</xsl:when>
                              <xsl:when test="@method = 'EditPeriodicTask' or @method = 'ViewPeriodicTask'">
                                 <xsl:value-of select="$private_bin_uri"/>/Organizer/<xsl:value-of select="@method"/>?day=<xsl:value-of select="$day"/>&amp;month=<xsl:value-of select="$month"/>&amp;year=<xsl:value-of select="$year"/>&amp;taskrowid=<xsl:value-of select="@taskrowid"/>&amp;mode=initial</xsl:when>
                              <xsl:when test="@method = 'CreateStrictOrPeriodicTask'">
                                 <xsl:value-of select="$private_bin_uri"/>/Organizer/<xsl:value-of select="@method"/>?day=<xsl:value-of select="$day"/>&amp;month=<xsl:value-of select="$month"/>&amp;year=<xsl:value-of select="$year"/>&amp;startHour=<xsl:value-of select="@startHour"/>&amp;startMin=<xsl:value-of select="@startMin"/>&amp;stopHour=<xsl:value-of select="@stopHour"/>&amp;stopMin=<xsl:value-of select="@stopMin"/>&amp;taskrowid=-1&amp;mode=initial</xsl:when>
                              <xsl:otherwise>
                                  <xsl:value-of select="$private_bin_uri"/>/Organizer/<xsl:value-of select="@method"/>?taskrowid=<xsl:value-of select="@taskrowid"/></xsl:otherwise>
                           </xsl:choose>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:choose>
                        <xsl:when test="@planned = 0">*</xsl:when>
                        <xsl:when test="@printTime = 1">
                           <xsl:value-of select="@startHour"/>:<xsl:value-of select="@startMin"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:value-of select="@startHour"/>:<xsl:value-of select="@startMin"/>
                           <br/>
                           <xsl:value-of select="@stopHour"/>:<xsl:value-of select="@stopMin"/>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:element>
               </xsl:element>
               <td align="left" width="{90-$percentByFlexibleTask*number(../@flexibleTasks)}%" rowspan="{@rowspan}">
                  <xsl:attribute name="rowspan"><xsl:value-of select="@rowspan"/></xsl:attribute>
                  <xsl:if test="@bgcolor!=''">
                      <xsl:attribute name="bgcolor"><xsl:value-of select="@bgcolor"/></xsl:attribute>
                  </xsl:if>

                  <font>
                      <xsl:if test="@fgcolor!=''">
                          <xsl:attribute name="color"><xsl:value-of select="@fgcolor"/></xsl:attribute>
                      </xsl:if>
                      &#160;
                      <xsl:choose>
                          <xsl:when test="@private=1">
                            <i>
                              <xsl:call-template name="organizerTranslation">
                                <xsl:with-param name="type">private-task</xsl:with-param>
                              </xsl:call-template>
                            </i>
                          </xsl:when>
                          <xsl:otherwise>
                              <a>
                                  <xsl:if test="@fgcolor!=''">
                                      <xsl:attribute name="style">color: <xsl:value-of select="@fgcolor"/></xsl:attribute>
                                  </xsl:if>
                                  <xsl:attribute name="href">
                                      <xsl:choose>
                                          <xsl:when test="@method = 'EditPeriodicTask' or @method = 'ViewPeriodicTask'"><xsl:value-of select="$private_bin_uri"/>/Organizer/<xsl:value-of select="@method"/>?day=<xsl:value-of select="$day"/>&amp;month=<xsl:value-of select="$month"/>&amp;year=<xsl:value-of select="$year"/>&amp;taskrowid=<xsl:value-of select="@taskrowid"/>&amp;mode=initial</xsl:when>
                                          <xsl:otherwise><xsl:value-of select="$private_bin_uri"/>/Organizer/<xsl:value-of select="@method"/>?taskrowid=<xsl:value-of select="@taskrowid"/>&amp;mode=initial</xsl:otherwise>
                                      </xsl:choose>
                                  </xsl:attribute>
                                  <xsl:value-of select="."/>
                              </a>
                              <xsl:if test="@first_name!='' and @last_name!=''">
                                  &#160;&#160;(
                                <xsl:call-template name="organizerTranslation">
                                  <xsl:with-param name="type">contact</xsl:with-param>
                                </xsl:call-template>
                                <xsl:value-of select="@first_name"/>&#160;<xsl:value-of select="@last_name"/>)
                              </xsl:if>
                          </xsl:otherwise>
                      </xsl:choose>
                  </font>
               </td>
            </xsl:if>
            <xsl:for-each select="FlexibleTask">
               <td rowspan="{@rowspan}" width="{$percentByFlexibleTask}%">
                   <xsl:choose>
                       <xsl:when test="@empty = 0">
                           <xsl:choose>
                               <xsl:when test="@bgcolor = ''">
                                   <xsl:attribute name="class"><xsl:value-of select="$noconflict_bgcolor"/></xsl:attribute>
                               </xsl:when>
                               <xsl:otherwise>
                                   <xsl:attribute name="bgcolor"><xsl:value-of select="@bgcolor"/></xsl:attribute>
                               </xsl:otherwise>
                           </xsl:choose>
                       </xsl:when>
                       <xsl:otherwise>
                           <xsl:attribute name="class"><xsl:value-of select="$mioga-bg-color"/></xsl:attribute>
                       </xsl:otherwise>
                   </xsl:choose>
                   <font color="{@fgcolor}">
                       &#160;
                   </font>
               </td>
            </xsl:for-each>
         </tr>
   </xsl:for-each>

   <xsl:for-each select="FlexibleTasks">
      <xsl:for-each select="FlexibleTask">
         <tr class="{$mioga-bg-color}">
            <td colspan="{2+@id}" align="right">

               <xsl:variable name="element_name">
                  <xsl:choose>
                     <xsl:when test="@type ='none' or @private=1">font</xsl:when>
                     <xsl:otherwise                >a</xsl:otherwise>
                  </xsl:choose>
               </xsl:variable>
               <xsl:element name="{$element_name}">
                  <xsl:if test="$element_name = 'a'">
                     <xsl:variable name="url">
                        <xsl:choose>
                           <xsl:when test="@type = 'flexible'">Tasks/EditTask?rowid</xsl:when>
                           <xsl:when test="@type = 'project'"  >Project/ProjectDisplayTask?taskid</xsl:when>
                           <xsl:otherwise                      >Organizer/DisplayMain</xsl:otherwise>
                        </xsl:choose>
                     </xsl:variable>
                     <xsl:attribute name="href"><xsl:value-of select="$private_bin_uri"/>/<xsl:value-of select="$url"/>=<xsl:value-of select="@taskrowid"/></xsl:attribute>
                  </xsl:if>
                  <xsl:choose>
                      <xsl:when test="@private=1">
                          <i>
                            <xsl:call-template name="organizerTranslation">
                              <xsl:with-param name="type">private-task</xsl:with-param>
                            </xsl:call-template>
                          </i>
                      </xsl:when>
                      <xsl:otherwise>
                          <xsl:value-of select="@name"/>
                      </xsl:otherwise>
                  </xsl:choose>
               </xsl:element>

            </td>
            <td align="left" valign="top"><img src="{$image_uri}/fleche_tacheflex.gif" alt="" border="0"/></td>
            <xsl:if test="last()-@id &gt; 1">
               <td colspan="{last() -@id -1}">
                   &#160;
               </td>
            </xsl:if>
         </tr>
      </xsl:for-each>
   </xsl:for-each>
</xsl:template>

<!-- *********************************************************************** -->
<!-- * ViewWeek                                                            * -->
<!-- *********************************************************************** -->
<xsl:template match="ViewWeek">
   <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>
        
      <!-- Status message -->
      <xsl:if test="@message != ''">
         <xsl:call-template name="InlineMessage">
            <xsl:with-param name="type" select="@type"/>
            <xsl:with-param name="message" select="@message"/>
         </xsl:call-template>
      </xsl:if>

      <xsl:call-template name="TopControl">
         <xsl:with-param name="now_day"    select="@now_day"/>
         <xsl:with-param name="now_month"  select="@now_month"/>
         <xsl:with-param name="now_year"   select="@now_year"/>
         <xsl:with-param name="now_hour"   select="@now_hour"/>
         <xsl:with-param name="now_minute" select="@now_minute"/>
         <xsl:with-param name="now_method" select="'ViewWeek'"/>
         <xsl:with-param name="day"        select="Calendar/@firstDay"/>
         <xsl:with-param name="month"      select="Calendar/@month"/>
         <xsl:with-param name="year"       select="Calendar/@year"/>
         <xsl:with-param name="startYear"  select="@startYear"/>
         <xsl:with-param name="stopYear"   select="@stopYear"/>
      </xsl:call-template>
      <table border="0" cellspacing="20" width="100%">
         <tr><td valign="top" width="10%">
            <xsl:call-template name="LeftControl"/>
         </td><td valign="top" width="90%">
            <xsl:for-each select="WeekTasks">
                <table xsl:use-attribute-sets="mioga-border-table" width="100%">
                    <tr>
                        <td>
                <table xsl:use-attribute-sets="mioga-content-table">
                    <tr>
                        <xsl:apply-templates select="." mode="table-title"/>
                    </tr>

                    <xsl:apply-templates select="." mode="table-body"/>
                </table>
                        </td>
                    </tr>
                </table>
            </xsl:for-each>
         </td></tr>
      </table>
   </body>
</xsl:template>

<xsl:template match="WeekTasks" mode="table-title">
  <th width="16%" align="center">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
    <td align="right">
      <a href="{$private_bin_uri}/Organizer/ViewWeek?week={@previousYear}-{@previousWeek}">
        <img src="{$image_uri}/16x16/actions/go-previous.png" alt="" border="0"/>
      </a>
    </td><th>
    <font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Week')"/><br /><xsl:value-of select="../@week"/>
  </font>
</th><td align="left">
<a href="{$private_bin_uri}/Organizer/ViewWeek?week={@nextYear}-{@nextWeek}">
  <img src="{$image_uri}/16x16/actions/go-next.png" alt="" border="0"/>
</a>
</td>
</tr></table>
   </th>
   <xsl:for-each select="WeekDays/WeekDay">
      <th width="12%">
         <a href="{$private_bin_uri}/Organizer/ViewDay?day={@year}-{@month}-{@day}">
            <font>
               <xsl:choose>
                  <xsl:when test="@isWorkingDay = 0">
                     <xsl:attribute name="class"><xsl:value-of select="$nonworking_bgcolor"/></xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:attribute name="class"><xsl:value-of select="$mioga-list-title-text-color"/></xsl:attribute>
                  </xsl:otherwise>
               </xsl:choose>
               <xsl:call-template name="Day-name">
                  <xsl:with-param name="day_number" select="@index"/>
                  <xsl:with-param name="short" select="3"/>
               </xsl:call-template>
               <br />
               <xsl:value-of select="@day"/>
            </font>
         </a>
      </th>
   </xsl:for-each>
</xsl:template>

<xsl:template match="WeekTasks" mode="table-body">
   <xsl:for-each select="WeekTime">
      <tr>
          <xsl:variable name="next-sibling" select="following-sibling::WeekTime"/>
          <xsl:variable name="prev-sibling" select="preceding-sibling::WeekTime"/>

          <xsl:choose>
              <xsl:when test="position() mod 2 = 0">
                  <xsl:attribute name="class"><xsl:value-of select="$mioga-list-even-row-bg-color"/></xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                  <xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color"/></xsl:attribute>
              </xsl:otherwise>
          </xsl:choose>
          <td align="center">
              <xsl:choose>
                  <xsl:when test="@is_planned=0">&#160;</xsl:when>
                  <xsl:when test="@printTime = 1">
                      <xsl:value-of select="@startHour"/>:<xsl:value-of select="@startMin"/>
                  </xsl:when>
                  <xsl:otherwise>
                      <xsl:value-of select="@startHour"/>:<xsl:value-of select="@startMin"/>
                      <br/>
                      <xsl:value-of select="@stopHour"/>:<xsl:value-of select="@stopMin"/>
                  </xsl:otherwise>
              </xsl:choose>
          </td>
          <xsl:for-each select="WeekTimePeriod">
              <xsl:variable name="index" select="position()"/>
              <xsl:if test="@rowspan != 0">
                  <td rowspan="{@rowspan}">
                      <xsl:choose>
                          <xsl:when test="@empty = 1">
                              <xsl:attribute name="align">center</xsl:attribute>
                              <xsl:if test="@canWrite = 1">
                                  <xsl:attribute name="style">cursor: pointer</xsl:attribute>
                                  <xsl:attribute name="onclick">location='CreateStrictOrPeriodicTask?day=<xsl:value-of select="../../WeekDays[1]/WeekDay[$index]/@day"/>&amp;month=<xsl:value-of select="../../WeekDays[1]/WeekDay[$index]/@month"/>&amp;year=<xsl:value-of select="../../WeekDays[1]/WeekDay[$index]/@year"/>&amp;startHour=<xsl:value-of select="../@startHour"/>&amp;startMin=<xsl:value-of select="../@startMin"/>&amp;stopHour=<xsl:value-of select="../@stopHour"/>&amp;stopMin=<xsl:value-of select="../@stopMin"/>&amp;taskrowid=-1&amp;mode=initial'</xsl:attribute>
                              </xsl:if>
                              
                              <xsl:variable name="element_name">
                                  <xsl:choose>
                                      <xsl:when test="@canWrite = 1">a</xsl:when>
                                 <xsl:otherwise                >font</xsl:otherwise>
                              </xsl:choose>
                           </xsl:variable>
                        <xsl:element name="{$element_name}">
                              <xsl:if test="$element_name = 'a'">
                                 <xsl:attribute name="href">
                                    <xsl:value-of select="$private_bin_uri"/>/Organizer/CreateStrictOrPeriodicTask?day=<xsl:value-of select="../../WeekDays[1]/WeekDay[$index]/@day"/>&amp;month=<xsl:value-of select="../../WeekDays[1]/WeekDay[$index]/@month"/>&amp;year=<xsl:value-of select="../../WeekDays[1]/WeekDay[$index]/@year"/>&amp;startHour=<xsl:value-of select="../@startHour"/>&amp;startMin=<xsl:value-of select="../@startMin"/>&amp;stopHour=<xsl:value-of select="../@stopHour"/>&amp;stopMin=<xsl:value-of select="../@stopMin"/>&amp;taskrowid=-1&amp;mode=initial</xsl:attribute>
                              </xsl:if>
                              <img src="{$image_uri}/transparent_fill.gif" width="50" height="20" border="0">
                                 <xsl:attribute name="alt">
                                   <xsl:call-template name="createTaskTranslation">
                                     <xsl:with-param name="element"><xsl:value-of select="$element_name"/></xsl:with-param>
                                   </xsl:call-template>
                                 </xsl:attribute>
                                 <xsl:attribute name="title">
                                   <xsl:call-template name="createTaskTranslation">
                                     <xsl:with-param name="element"><xsl:value-of select="$element_name"/></xsl:with-param>
                                   </xsl:call-template>
                                 </xsl:attribute>
                              </img>
                           </xsl:element>
                     </xsl:when>

                     <xsl:otherwise>
                         <xsl:variable name="bgcolor">
                             <xsl:choose>
                                 <xsl:when test="count(WeekTask[@conflict=1]) > 0"><xsl:value-of select="$conflict_bgcolor"/></xsl:when>
                                 <xsl:otherwise><xsl:value-of select="WeekTask[position() = last()]/@bgcolor"/></xsl:otherwise>
                             </xsl:choose>
                         </xsl:variable>

                         <xsl:variable name="color">
                             <xsl:choose>
                                 <xsl:when test="WeekTask/@conflict = 0"><xsl:value-of select="WeekTask/@fgcolor"/></xsl:when>
                                 <xsl:otherwise><xsl:value-of select="$mioga-title-color"/></xsl:otherwise>
                             </xsl:choose>
                         </xsl:variable>

                         <xsl:variable name="bottom-border">
                             <xsl:choose>
                                 <xsl:when test="$next-sibling/WeekTimePeriod[$index]/WeekTask[1]/@taskrowid = ./WeekTask[position() = last()]/@taskrowid">0</xsl:when>
                                 <xsl:otherwise>1</xsl:otherwise>
                             </xsl:choose>
                         </xsl:variable>
                         
                         <xsl:attribute name="style">border-bottom-width: <xsl:value-of select="$bottom-border"/>px; color: <xsl:value-of select="$color"/>; background-color: <xsl:value-of select="$bgcolor"/></xsl:attribute>

                         <font size="-1">
                             <xsl:for-each select="WeekTask">
                                 <div> 

                                 <xsl:message>
                                     <xsl:value-of select="position()"/>
                                     <xsl:value-of select="."/>
                                     <xsl:value-of select="$index"/>
                                 </xsl:message>

                                     <xsl:variable name="is_first">
                                         <xsl:choose>
                                             <!--
                                             <xsl:when test="position() = 1 and $prev-sibling/WeekTimePeriod[$index]/WeekTask/@taskrowid = ./@taskrowid">0</xsl:when>
                                             <xsl:when test="preceding-sibling::WeekTask[last()]/@taskrowid = ./@taskrowid">0</xsl:when>
                                             -->
                                             <xsl:when test="($prev-sibling/WeekTimePeriod[$index]/WeekTask/@taskrowid = ./@taskrowid) or (preceding-sibling::WeekTask[last()]/@taskrowid = ./@taskrowid)">0</xsl:when>
                                             <xsl:otherwise>1</xsl:otherwise>
                                         </xsl:choose>
                                     </xsl:variable>

                                    <xsl:variable name="task-bottom-border">
                                         <xsl:choose>
                                             <xsl:when test="position() != last() and following-sibling::WeekTask[1]/@taskrowid != ./@taskrowid">1</xsl:when>
                                             <xsl:otherwise>0</xsl:otherwise>
                                         </xsl:choose>
                                     </xsl:variable>
                                     
                                     <xsl:choose>
                                         <xsl:when test="@conflict = 0">
                                             <xsl:attribute name="style">border-bottom-width: <xsl:value-of select="$task-bottom-border"/>px; color: <xsl:value-of select="@fgcolor"/>; background-color: <xsl:value-of select="@bgcolor"/>; padding: 2px</xsl:attribute>
                                         </xsl:when>
                                         <xsl:otherwise>
                                             <xsl:attribute name="style">border-bottom-width: <xsl:value-of select="$task-bottom-border"/>px; background-color: <xsl:value-of select="$conflict_bgcolor"/>; padding: 2px</xsl:attribute>
                                         </xsl:otherwise>
                                     </xsl:choose>
                                     
                                     <xsl:choose>
                                         <xsl:when test="$is_first=1">

                                 <xsl:choose>
                                     <xsl:when test="@private=1">
                                       <i>
                                         <xsl:call-template name="organizerTranslation">
                                           <xsl:with-param name="type">private-task</xsl:with-param>
                                         </xsl:call-template>
                                       </i>
                                     </xsl:when>
                                     <xsl:otherwise>
                                         <a>
                                             <xsl:choose>
                                                 <xsl:when test="@conflict = 0">
                                                     <xsl:attribute name="style">color: <xsl:value-of select="@fgcolor"/>; background-color: <xsl:value-of select="@bgcolor"/>; padding: 2px</xsl:attribute>
                                                 </xsl:when>
                                                 <xsl:otherwise>
                                                     <xsl:attribute name="style">background-color: <xsl:value-of select="$conflict_bgcolor"/>; padding: 2px</xsl:attribute>
                                                 </xsl:otherwise>
                                             </xsl:choose>
                                             <xsl:attribute name="href">
                                                 <xsl:choose>
                                                     <xsl:when test="@method = 'EditStrictTask'"><xsl:value-of select="$private_bin_uri"/>/Organizer/<xsl:value-of select="@method"/>?taskrowid=<xsl:value-of select="@taskrowid"/>&amp;mode=initial</xsl:when>
                                                     
                                                     <xsl:when test="@method = 'EditPeriodicTask' or @method = 'ViewPeriodicTask'"><xsl:value-of select="$private_bin_uri"/>/Organizer/<xsl:value-of select="@method"/>?day=<xsl:value-of select="../../../WeekDays[1]/WeekDay[$index]/@day"/>&amp;month=<xsl:value-of select="../../../WeekDays[1]/WeekDay[$index]/@month"/>&amp;year=<xsl:value-of select="../../../WeekDays[1]/WeekDay[$index]/@year"/>&amp;taskrowid=<xsl:value-of select="@taskrowid"/>&amp;mode=initial</xsl:when>
                                                     
                                                     <xsl:otherwise><xsl:value-of select="$private_bin_uri"/>/Organizer/<xsl:value-of select="@method"/>?taskrowid=<xsl:value-of select="@taskrowid"/></xsl:otherwise>
                                                     
                                                 </xsl:choose>
                                             </xsl:attribute>

                                             <xsl:if test="../../@is_planned!=0">
                                                 <xsl:value-of select="@startHour"/>:<xsl:value-of select="@startMin"/>&#160;
                                             </xsl:if>
                                             <xsl:value-of select="."/>
                                         </a>
                                     </xsl:otherwise>
                                 </xsl:choose>

                                        </xsl:when>
                                        <xsl:otherwise>&#160;</xsl:otherwise>
                                    </xsl:choose>

                                 </div>
                             </xsl:for-each>
                         </font>
                     </xsl:otherwise>
                  </xsl:choose>
               </td>
            </xsl:if>
         </xsl:for-each>
      </tr>
   </xsl:for-each>
   <tr>
      <td width="16%" colspan="29"><img src="{$image_uri}/transparent_fill.gif" border="0" alt="" width="1" height="1"/></td>
   </tr>
   <xsl:for-each select="FlexibleTasks/FlexibleTask">
      <tr class="{$mioga-list-odd-row-bg-color}">
         <td width="16%">

            <xsl:variable name="element_name">
               <xsl:choose>
                  <xsl:when test="@type ='none' or @private=1">font</xsl:when>
                  <xsl:otherwise                >a</xsl:otherwise>
               </xsl:choose>
            </xsl:variable>
            <xsl:element name="{$element_name}">
               <xsl:if test="$element_name = 'a'">
                  <xsl:variable name="url">
                     <xsl:choose>
                        <xsl:when test="@type = 'flexible'">Tasks/EditTask?rowid</xsl:when>
                        <xsl:when test="@type = 'project'"  >Project/ProjectDisplayTask?taskid</xsl:when>
                        <xsl:otherwise                      >Organizer/DisplayMain</xsl:otherwise>
                     </xsl:choose>
                  </xsl:variable>
                  <xsl:attribute name="href"><xsl:value-of select="$private_bin_uri"/>/<xsl:value-of select="$url"/>=<xsl:value-of select="@taskrowid"/></xsl:attribute>
               </xsl:if>
               <xsl:choose>
                   <xsl:when test="@private=1">
                     <i>
                       <xsl:call-template name="organizerTranslation">
                         <xsl:with-param name="type">private-task</xsl:with-param>
                       </xsl:call-template>
                     </i>
                   </xsl:when>
                   <xsl:otherwise><xsl:value-of select="@name"/></xsl:otherwise>
               </xsl:choose>
            </xsl:element>

         </td>
         <xsl:for-each select="FlexiblePeriod">
            <td colspan="{@colspan}">
                <xsl:choose>
                    <xsl:when test="@empty = 0">
                        <xsl:choose>
                            <xsl:when test="../@type = 'project'">
                                <xsl:attribute name="class"><xsl:value-of select="$noconflict_bgcolor"/></xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="bgcolor"><xsl:value-of select="../@bgcolor"/></xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="class"><xsl:value-of select="$mioga-bg-color"/></xsl:attribute>
                    </xsl:otherwise>
                </xsl:choose>
                <font class="{@fgcolor}">&#160;</font>
            </td>
         </xsl:for-each>
      </tr>
   </xsl:for-each>
</xsl:template>

<!-- *********************************************************************** -->
<!-- * ViewMonth                                                           * -->
<!-- *********************************************************************** -->
<xsl:template match="ViewMonth">
   <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>
        
      <!-- Status message -->
      <xsl:if test="@message != ''">
         <xsl:call-template name="InlineMessage">
            <xsl:with-param name="type" select="@type"/>
            <xsl:with-param name="message" select="@message"/>
         </xsl:call-template>
      </xsl:if>

      <xsl:call-template name="TopControl">
         <xsl:with-param name="now_day"    select="@now_day"/>
         <xsl:with-param name="now_month"  select="@now_month"/>
         <xsl:with-param name="now_year"   select="@now_year"/>
         <xsl:with-param name="now_hour"   select="@now_hour"/>
         <xsl:with-param name="now_minute" select="@now_minute"/>
         <xsl:with-param name="now_method" select="'ViewMonth'"/>
         <xsl:with-param name="day"        select="1"/>
         <xsl:with-param name="month"      select="Calendar/@month"/>
         <xsl:with-param name="year"       select="Calendar/@year"/>
         <xsl:with-param name="startYear"  select="@startYear"/>
         <xsl:with-param name="stopYear"   select="@stopYear"/>
      </xsl:call-template>
      <table border="0" cellspacing="20" width="100%">
         <tr><td valign="top" width="10%">
            <xsl:call-template name="LeftControl"/>
         </td><td valign="top" width="90%">
            <xsl:for-each select="MonthTasks">
               <xsl:call-template name="table">
                  <xsl:with-param name="table_width">100%</xsl:with-param>
                  <xsl:with-param name="cellpadding">0</xsl:with-param>
               </xsl:call-template>
            </xsl:for-each>
         </td></tr>
      </table>
   </body>
</xsl:template>

<xsl:template match="MonthTasks" mode="table-title">
   <th width="2%" align="center" class="{$mioga-list-title-bg-color}">
      <font class="{$mioga-list-title-text-color}"></font>
   </th>
   <td width="14%" align="center"><table border="0" width="center" cellpadding="3" cellspacing="0"><tr><th>
      <font class="{$mioga-list-title-text-color}">
         <xsl:call-template name="Day-name">
            <xsl:with-param name="day_number" select="1"/>
            <xsl:with-param name="short" select="3"/>
         </xsl:call-template>
      </font>
   </th></tr></table></td>
   <td width="14%" align="center"><table border="0" width="center" cellpadding="3" cellspacing="0"><tr><th>
      <font class="{$mioga-list-title-text-color}">
         <xsl:call-template name="Day-name">
            <xsl:with-param name="day_number" select="2"/>
            <xsl:with-param name="short" select="3"/>
         </xsl:call-template>
      </font>
   </th></tr></table></td>
   <td width="14%" align="center"><table border="0" width="center" cellpadding="3" cellspacing="0"><tr><th>
      <font class="{$mioga-list-title-text-color}">
         <xsl:call-template name="Day-name">
            <xsl:with-param name="day_number" select="3"/>
            <xsl:with-param name="short" select="3"/>
         </xsl:call-template>
      </font>
   </th></tr></table></td>
   <td width="14%" align="center"><table border="0" width="center" cellpadding="3" cellspacing="0"><tr><th>
      <font class="{$mioga-list-title-text-color}">
         <xsl:call-template name="Day-name">
            <xsl:with-param name="day_number" select="4"/>
            <xsl:with-param name="short" select="3"/>
         </xsl:call-template>
      </font>
   </th></tr></table></td>
   <td width="14%" align="center"><table border="0" width="center" cellpadding="3" cellspacing="0"><tr><th>
      <font class="{$mioga-list-title-text-color}">
         <xsl:call-template name="Day-name">
            <xsl:with-param name="day_number" select="5"/>
            <xsl:with-param name="short" select="3"/>
         </xsl:call-template>
      </font>
   </th></tr></table></td>
   <td width="14%" align="center"><table border="0" width="center" cellpadding="3" cellspacing="0"><tr><th>
      <font class="{$mioga-list-title-text-color}">
         <xsl:call-template name="Day-name">
            <xsl:with-param name="day_number" select="6"/>
            <xsl:with-param name="short" select="3"/>
         </xsl:call-template>
      </font>
   </th></tr></table></td>
   <td width="14%" align="center"><table border="0" width="center" cellpadding="3" cellspacing="0"><tr><th>
      <font class="{$mioga-list-title-text-color}">
         <xsl:call-template name="Day-name">
            <xsl:with-param name="day_number" select="7"/>
            <xsl:with-param name="short" select="3"/>
         </xsl:call-template>
      </font>
   </th></tr></table></td>
</xsl:template>

<xsl:template match="MonthTasks" mode="table-body">
   <xsl:for-each select="Week">
      <xsl:variable name="localbgcolor">
         <xsl:choose>
            <xsl:when test="position() mod 2 = 0"><xsl:value-of select="$mioga-list-even-row-bg-color"/></xsl:when>
            <xsl:otherwise                       ><xsl:value-of select="$mioga-list-odd-row-bg-color"/></xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <tr class="{$localbgcolor}">
         <td align="center" width="2%" bgcolor="{$month_left_color}">
            <br/>
            <br/>
            <br/>
            &#160;<a href="{$private_bin_uri}/Organizer/ViewWeek?week={@year}-{@number}"><font class="{$mioga-list-title-bg-color}"><b><xsl:value-of select="@number"/></b></font></a>&#160;
            <br/>
            <br/>
            <br/>
            <br/>
         </td>
         <xsl:for-each select="MonthDay">
            <td align="left" valign="top" width="14%">
               <xsl:choose>
                  <xsl:when test="@day">
                     <table border="0" width="100%" cellpadding="1" cellspacing="0">
                     <tr>
                        <td width="10%">
                           <table border="0" width="100%" cellpadding="2" cellspacing="2"><tr>
                              <th class="{$mioga-list-title-bg-color}">
                                 <a href="{$private_bin_uri}/Organizer/ViewDay?day={@year}-{@month}-{@day}">
                                    <font>
                                       <xsl:choose>
                                          <xsl:when test="@isWorkingDay = 0">
                                             <xsl:attribute name="class"><xsl:value-of select="$nonworking_bgcolor"/></xsl:attribute>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <xsl:attribute name="class"><xsl:value-of select="$mioga-list-title-text-color"/></xsl:attribute>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:value-of select="@day"/>
                                    </font>
                                 </a>
                              </th>
                           </tr></table>
                        </td>
                        <td width="90%">
                            &#160;
                        </td>
                     </tr>
                     <xsl:for-each select="FlexibleTask">
                        <tr>
                           <td colspan="2">
                              <xsl:attribute name="bgcolor">
                                 <xsl:choose>
                                    <xsl:when test="@empty = 0"      ><xsl:value-of select="@bgcolor"/></xsl:when>
                                    <xsl:otherwise                   ><xsl:value-of select="$localbgcolor"/></xsl:otherwise>
                                 </xsl:choose>
                              </xsl:attribute>

                              <table border="0" width="100%" cellpadding="0" cellspacing="0"><tr>
                                 <td align="center">
                                     <font>

                                    <xsl:choose>
                                       <xsl:when test="@empty = 0">
                                          <xsl:choose>
                                             <xsl:when test="@type = 'project'">
                                                <xsl:attribute name="bgcolor"><xsl:value-of select="$noconflict_bgcolor"/></xsl:attribute>
                                             </xsl:when>
                                             <xsl:when test="@conflict = 0">
                                                <xsl:attribute name="bgcolor"><xsl:value-of select="@bgcolor"/></xsl:attribute>
                                                <xsl:attribute name="color"><xsl:value-of select="@fgcolor"/></xsl:attribute>
                                             </xsl:when>
                                             <xsl:otherwise>
                                                <xsl:attribute name="bgcolor"><xsl:value-of select="$conflict_bgcolor"/></xsl:attribute>
                                             </xsl:otherwise>
                                          </xsl:choose>
                                          
                                          <xsl:variable name="element_name">
                                             <xsl:choose>
                                                <xsl:when test="@type ='none' or @private=1">font</xsl:when>
                                                <xsl:otherwise                >a</xsl:otherwise>
                                             </xsl:choose>
                                          </xsl:variable>
                                          <xsl:element name="{$element_name}">
                                             <xsl:if test="$element_name = 'a'">
                                                <xsl:variable name="url">
                                                   <xsl:choose>
                                                       <xsl:when test="@type = 'flexible'">Tasks/EditTask?rowid</xsl:when>
                                                       <xsl:when test="@type = 'project'"  >Project/ProjectDisplayTask?taskid</xsl:when>
                                                      <xsl:otherwise                      >Organizer/DisplayMain</xsl:otherwise>
                                                   </xsl:choose>
                                                </xsl:variable>
                                                <xsl:attribute name="href"><xsl:value-of select="$private_bin_uri"/>/<xsl:value-of select="$url"/>=<xsl:value-of select="@taskrowid"/></xsl:attribute>
                                             </xsl:if>
                                             <img src="{$image_uri}/transparent_fill.gif" border="0" style="width: 100%" height="6"
                                                  alt="{@name}" title="{@name}"/>
                                          </xsl:element>

                                       </xsl:when>
                                       <xsl:otherwise>
                                          <img src="{$image_uri}/transparent_fill.gif" border="0" style="width: 100%"  height="6"/>
                                       </xsl:otherwise>
                                    </xsl:choose>
                                     </font>
                                 </td>
                              </tr></table>
                           </td>
                        </tr>
                     </xsl:for-each>
                     <tr><td colspan="2">
                        <table border="0" width="100%" cellpadding="2">
                           <xsl:for-each select="MonthTask">
                              <tr><td>
                              <div>
                                  
                                 <xsl:variable name="bgcolor">
                                     <xsl:choose>
                                         <xsl:when test="@conflict=1"><xsl:value-of select="$conflict_bgcolor"/></xsl:when>
                                         <xsl:otherwise><xsl:value-of select="@bgcolor"/></xsl:otherwise>
                                     </xsl:choose>
                                 </xsl:variable>

                                 <xsl:variable name="fgcolor">
                                     <xsl:choose>
                                         <xsl:when test="@conflict=1"><xsl:value-of select="$mioga-text-color"/></xsl:when>
                                         <xsl:otherwise><xsl:value-of select="@fgcolor"/></xsl:otherwise>
                                     </xsl:choose>
                                 </xsl:variable>

                                 <xsl:attribute name="style">background-color: <xsl:value-of select="$bgcolor"/>; color: <xsl:value-of select="$fgcolor"/></xsl:attribute>

                                 <xsl:variable name="element_name">
                                    <xsl:choose>
                                       <xsl:when test="@method ='none' or @private=1">font</xsl:when>
                                       <xsl:otherwise                  >a</xsl:otherwise>
                                    </xsl:choose>
                                 </xsl:variable>
                                 <xsl:element name="{$element_name}">
                                     <xsl:attribute name="style">background-color: <xsl:value-of select="$bgcolor"/>; color: <xsl:value-of select="$fgcolor"/></xsl:attribute>
                                     <xsl:if test="$element_name = 'a'">
                                        <xsl:attribute name="href">
                                          <xsl:choose>
                                             <xsl:when test="@method = 'EditStrictTask'">
                                                <xsl:value-of select="$private_bin_uri"/>/Organizer/<xsl:value-of select="@method"/>?taskrowid=<xsl:value-of select="@taskrowid"/>&amp;mode=initial</xsl:when>
                                             <xsl:when test="@method = 'EditPeriodicTask' or @method = 'ViewPeriodicTask'">
                                                <xsl:value-of select="$private_bin_uri"/>/Organizer/<xsl:value-of select="@method"/>?day=<xsl:value-of select="../@day"/>&amp;month=<xsl:value-of select="../@month"/>&amp;year=<xsl:value-of select="../@year"/>&amp;taskrowid=<xsl:value-of select="@taskrowid"/>&amp;mode=initial</xsl:when>
                                             <xsl:otherwise>
                                                 <xsl:value-of select="$private_bin_uri"/>/Organizer/<xsl:value-of select="@method"/>?taskrowid=<xsl:value-of select="@taskrowid"/></xsl:otherwise>
                                          </xsl:choose>
                                       </xsl:attribute>
                                    </xsl:if>
                                    <xsl:if test="@is_planned=1">
                                        <font size="-1">
                                            <xsl:value-of select="@startHour"/>:<xsl:value-of select="@startMin"/>
                                        </font>
                                        &#160;
                                    </xsl:if>
                                    <xsl:choose>
                                        <xsl:when test="@private=1">
                                          <i>
                                            <xsl:call-template name="organizerTranslation">
                                              <xsl:with-param name="type">private-task</xsl:with-param>
                                            </xsl:call-template>
                                          </i>
                                        </xsl:when>
                                        <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
                                    </xsl:choose>
                                 </xsl:element>
                             </div>
                              </td></tr>
                           </xsl:for-each>
                        </table>
                     </td></tr>
                     </table>
                  </xsl:when>
                  <xsl:otherwise>
                      &#160;
                  </xsl:otherwise>
               </xsl:choose>
            </td>
         </xsl:for-each>
      </tr>
   </xsl:for-each>
</xsl:template>


<!-- *********************************************************************** -->
<!-- * TopControl for View*                                                * -->
<!-- *********************************************************************** -->
<xsl:template name="TopControl">
   <xsl:param name="now_day"/>
   <xsl:param name="now_month"/>
   <xsl:param name="now_year"/>
   <xsl:param name="now_hour"/>
   <xsl:param name="now_minute"/>
   <xsl:param name="now_method"/>
   <xsl:param name="day"/>
   <xsl:param name="month"/>
   <xsl:param name="year"/>
   <xsl:param name="startYear"/>
   <xsl:param name="stopYear"/>
   
   <br/>

   <form method="GET" action="{$private_bin_uri}/Organizer/DisplayMain" name="DisplayMain">
   <input type="hidden" name="method" value="{$now_method}"/>
   <table border="0" cellspacing="0" cellspadding="0" width="95%" align="center"><tr>
   <td nowrap="yes">
     <xsl:call-template name="organizerTranslation">
       <xsl:with-param name="type">user-organizer</xsl:with-param>
     </xsl:call-template>
   </td>
   <td nowrap="yes">
       <xsl:call-template name="org_select"/>
   </td>
   <td>
       <xsl:call-template name="ok-form-button">
           <xsl:with-param name="name">change_organizer</xsl:with-param>
           <xsl:with-param name="formname">DisplayMain</xsl:with-param>
       </xsl:call-template>
   </td>
   <td>
      <img src="{$image_uri}/transparent_fill.gif" border="0" alt="" width="10" height="1"/>
   </td>
   <td><img src="{$image_uri}/16x16/actions/arrow-right.png" /></td>
   <td>
       <a href="{$bin_uri}/{$mioga_context/user/ident}/Workspace/ModifyUserTimeParams"><font class="{$mioga-title-color}">
      <xsl:call-template name="organizerTranslation">
        <xsl:with-param name="type">preferences</xsl:with-param>
      </xsl:call-template>
      </font></a>
   </td>
   <td>
      <img src="{$image_uri}/transparent_fill.gif" border="0" alt="" width="10" height="1"/>
   </td>
   <td><img src="{$image_uri}/16x16/actions/arrow-right.png" /></td>
   <td nowrap="nowrap">
       <a href="{$private_bin_uri}/Organizer/ExportOrganizer"><font class="{$mioga-title-color}">
      <xsl:call-template name="organizerTranslation">
        <xsl:with-param name="type">export</xsl:with-param>
      </xsl:call-template>
      </font></a>
   </td>
   <td width="90%">
      <img src="{$image_uri}/transparent_fill.gif" border="0" alt="" width="20" height="1"/>
   </td>
   <td nowrap="yes" align="right">
      <b><font class="{$mioga-title-color}">
         <xsl:value-of select="$day"/>
         &#160;
         <xsl:call-template name="Month-name">
            <xsl:with-param name="month_number" select="$month"/>
         </xsl:call-template>
         &#160;
         <xsl:value-of select="$year"/>
      </font></b>
   </td>
   </tr>
   <tr>
   <td nowrap="yes">
      <xsl:call-template name="organizerTranslation">
        <xsl:with-param name="type">goto</xsl:with-param>
      </xsl:call-template>
      <img src="{$image_uri}/transparent_fill.gif" border="0" alt="" width="1" height="1"/>
   </td><td nowrap="yes" class="tiny-from">
      <xsl:call-template name="Task-day-month-year">
         <xsl:with-param name="value_day"       select="number($day)"/>
         <xsl:with-param name="param_day"       select="'day'"/>
         <xsl:with-param name="value_month"     select="number($month)"/>
         <xsl:with-param name="param_month"     select="'month'"/>
         <xsl:with-param name="value_year"      select="number($year)"/>
         <xsl:with-param name="param_year"      select="'year'"/>
         <xsl:with-param name="value_startYear" select="$startYear"/>
         <xsl:with-param name="value_stopYear"  select="$stopYear"/>
      </xsl:call-template>
   </td>
   <td>
       <xsl:call-template name="ok-form-button">
           <xsl:with-param name="name">go</xsl:with-param>
           <xsl:with-param name="formname">DisplayMain</xsl:with-param>
       </xsl:call-template>
   </td>
   <td>
      <img src="{$image_uri}/transparent_fill.gif" border="0" alt="" width="20" height="1"/>
   </td>
   <td><img src="{$image_uri}/16x16/actions/arrow-right.png" /></td>
   <td>
      <a href="{$private_bin_uri}/Organizer/DisplayMain?method={$now_method}&amp;day={$now_day}&amp;month={$now_month}&amp;year={$now_year}">
        <font class="{$mioga-title-color}">
          <xsl:call-template name="organizerTranslation">
            <xsl:with-param name="type">today</xsl:with-param>
          </xsl:call-template>
        </font>
      </a>
   </td>
   <td>
      <img src="{$image_uri}/transparent_fill.gif" border="0" alt="" width="20" height="1"/>
   </td>
   <td>
      <img src="{$image_uri}/transparent_fill.gif" border="0" alt="" width="20" height="1"/>
   </td>
   </tr></table>
   </form>
</xsl:template>

<xsl:template name="org_select">
  <select name="new_organizer">
    <xsl:for-each select="OtherOrganizer/OtherTeamList/Team">
     <optgroup>
       <xsl:attribute name="label"><xsl:call-template name="organizerTranslation">
        <xsl:with-param name="type">team</xsl:with-param>
      </xsl:call-template>&#160;<xsl:value-of select="./@ident"/></xsl:attribute>
       <xsl:for-each select="User">
         <option onclick="location='{$bin_uri}/{./@ident}/Organizer/DisplayMain'" value="{./@ident}">
          <xsl:if test="../../../@selected=./@ident">
            <xsl:attribute name="selected">selected</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="."/>
        </option>
       </xsl:for-each>
     </optgroup>
    </xsl:for-each>
    <optgroup>
      <xsl:attribute name="label"><xsl:call-template name="organizerTranslation">
        <xsl:with-param name="type">other-users</xsl:with-param>
      </xsl:call-template></xsl:attribute>
    <xsl:if test="count(OtherOrganizer/OtherUserList/user) = 0">
      <option value="" style="font-style: italic; text-align: center">
        <xsl:call-template name="organizerTranslation">
          <xsl:with-param name="type">none</xsl:with-param>
        </xsl:call-template>
      </option>
    </xsl:if>
    <xsl:for-each select="OtherOrganizer/OtherUserList/user">
      <xsl:sort select="./@ident" order="ascending"/>
      <option onclick="location='{$bin_uri}/{@ident}/Organizer/DisplayMain'" value="{@ident}">
        <xsl:if test="../../@selected=./@ident">
          <xsl:attribute name="selected">selected</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="."/>
      </option>
    </xsl:for-each>
    </optgroup>
    
    <optgroup>
      <xsl:attribute name="label"><xsl:call-template name="organizerTranslation">
        <xsl:with-param name="type">other-groups</xsl:with-param>
      </xsl:call-template></xsl:attribute>
    <xsl:if test="count(OtherOrganizer/OtherGroupList/group) = 0">
      <option value="" style="font-style: italic; text-align: center">
        <xsl:call-template name="organizerTranslation">
          <xsl:with-param name="type">none</xsl:with-param>
        </xsl:call-template>
      </option>
    </xsl:if>
    <xsl:for-each select="OtherOrganizer/OtherGroupList/group">
      <xsl:sort select="./@ident" order="ascending"/>
      <option onclick="location='{$bin_uri}/{@ident}/Organizer/DisplayMain'"  value="{@ident}">
        <xsl:if test="../../@selected=./@ident">
          <xsl:attribute name="selected">selected</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="."/>
      </option>
    </xsl:for-each>
    </optgroup>
    
    <optgroup>
      <xsl:attribute name="label"><xsl:call-template name="organizerTranslation">
        <xsl:with-param name="type">other-resources</xsl:with-param>
      </xsl:call-template></xsl:attribute>
    <xsl:if test="count(OtherOrganizer/OtherResourceList/resource) = 0">
      <option value="" style="font-style: italic; text-align: center">
        <xsl:call-template name="organizerTranslation">
          <xsl:with-param name="type">none</xsl:with-param>
        </xsl:call-template>
      </option>
    </xsl:if>
    <xsl:for-each select="OtherOrganizer/OtherResourceList/resource">
      <xsl:sort select="./@ident" order="ascending"/>
      <option onclick="location='{$bin_uri}/{@ident}/Organizer/DisplayMain'"  value="{@ident}">
        <xsl:if test="../../@selected=./@ident">
          <xsl:attribute name="selected">selected</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="."/>
      </option>
    </xsl:for-each>
    </optgroup>
  </select>    
</xsl:template>

<!-- *********************************************************************** -->
<!-- * LeftControl for View*                                               * -->
<!-- *********************************************************************** -->
<xsl:template name="LeftControl">
   <xsl:for-each select="Calendar">
      <xsl:call-template name="table">
         <xsl:with-param name="table_width">100%</xsl:with-param>
         <xsl:with-param name="cellspacing">0</xsl:with-param>
         <xsl:with-param name="cellpadding">1</xsl:with-param>
      </xsl:call-template>
      <br/>
   </xsl:for-each>
   <xsl:for-each select="ToDoTasks">
      <xsl:call-template name="table">
          <xsl:with-param name="table_width">100%</xsl:with-param>
      </xsl:call-template>
      <br/>
   </xsl:for-each>
   <xsl:for-each select="MeetingList">
      <xsl:call-template name="table">
         <xsl:with-param name="table_width">100%</xsl:with-param>
      </xsl:call-template>
      <br/>
   </xsl:for-each>
</xsl:template>


<!-- *********************************************************************** -->
<!-- * ToDoTasks for View*                                                 * -->
<!-- *********************************************************************** -->
<xsl:template match="ToDoTasks" mode="table-title">
   <th colspan="3">
      <xsl:variable name="element_name">
         <xsl:choose>
            <xsl:when test="@canWrite = 1">a</xsl:when>
            <xsl:otherwise                >font</xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:element name="{$element_name}">
         <xsl:if test="$element_name = 'a'">
            <xsl:attribute name="href"><xsl:value-of select="$private_bin_uri"/>/Organizer/CreateToDoTask?mode=initial</xsl:attribute>
         </xsl:if>
         <font class="{$mioga-list-title-text-color}">
           <xsl:call-template name="organizerTranslation">
             <xsl:with-param name="type">todo</xsl:with-param>
           </xsl:call-template>
         </font>
      </xsl:element>
   </th>
</xsl:template>

<xsl:template match="ToDoTasks" mode="table-body">
   <xsl:for-each select="ToDoTask">
      <tr>
          <td width="10%" align="center">
              <xsl:attribute name="bgcolor">
                  <xsl:choose>
                      <xsl:when test="@priority = 'very urgent'"><xsl:value-of select="$org_priority1_color"/></xsl:when>
                      <xsl:when test="@priority = 'urgent'"><xsl:value-of select="$org_priority2_color"/></xsl:when>
                      <xsl:when test="@priority = 'normal'"><xsl:value-of select="$org_priority3_color"/></xsl:when>
                      <xsl:when test="@priority = 'not urgent'"><xsl:value-of select="$org_priority4_color"/></xsl:when>
                      <xsl:otherwise                ><xsl:value-of select="$mioga-bg-color"/></xsl:otherwise>
                  </xsl:choose>
              </xsl:attribute>
              &#160;
              <!--
              <xsl:choose>
                  <xsl:when test="../@canWrite!=1 or @method ='none' or @private=1">
                      <xsl:choose>
                          <xsl:when test="@done=1">x</xsl:when>
                          <xsl:otherwise>&#160;</xsl:otherwise>
                      </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                      <input type="checkbox" onclick="location='{$private_bin_uri}/Organizer/TodoChangeStatus?taskrowid={@taskrowid}'">
                          <xsl:if test="@done='1'">
                              <xsl:attribute name="checked">checked</xsl:attribute>
                          </xsl:if>
                      </input>
                  </xsl:otherwise>
              </xsl:choose>
              -->
          </td>
          <td  width="100%" align="center"  bgcolor="{@bgcolor}">
              <font color="{@fgcolor}">
                  <xsl:choose>
                      <xsl:when test="@method ='none' or @private=1">
                          <xsl:choose>
                              <xsl:when test="@private=1">
                                <i>
                                  <xsl:call-template name="organizerTranslation">
                                    <xsl:with-param name="type">private-task</xsl:with-param>
                                  </xsl:call-template>
                                </i>
                              </xsl:when>
                              <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
                          </xsl:choose>
                      </xsl:when>
                      
                      <xsl:otherwise>
                          <a style="color: {@fgcolor}" href="{$private_bin_uri}/{@method}={@taskrowid}&amp;mode=initial"><xsl:value-of select="."/></a>
                      </xsl:otherwise>
                  </xsl:choose>
              </font>          
          </td>
          <xsl:if test="../@canWrite=1">
              <td bgcolor="{@bgcolor}">
                  <xsl:choose>
                      <xsl:when test="@method ='none' or @private=1">&#160;</xsl:when>
                      <xsl:otherwise>
                          <a href="TodoDelete?taskrowid={@taskrowid}"><img border="0" src="{$image_uri}/16x16/actions/trash-empty.png"/></a>
                      </xsl:otherwise>
                  </xsl:choose>
              </td>
          </xsl:if>
     </tr>
   </xsl:for-each>
</xsl:template>

<!-- *********************************************************************** -->
<!-- * Meetings for View*                                                  * -->
<!-- *********************************************************************** -->
<xsl:template match="MeetingList" mode="table-title">
  <th>
    <xsl:variable name="element_name">
      <xsl:choose>
        <xsl:when test="@canWrite = 1">a</xsl:when>
        <xsl:otherwise>font</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:element name="{$element_name}">
      <xsl:if test="$element_name = 'a'">
        <xsl:attribute name="href"><xsl:value-of select="$private_bin_uri"/>/Organizer/CreateMeeting</xsl:attribute>
      </xsl:if>
      <font class="{$mioga-list-title-text-color}">
        <xsl:call-template name="organizerTranslation">
          <xsl:with-param name="type">requested-meeting</xsl:with-param>
        </xsl:call-template>
      </font>
    </xsl:element>
  </th>
</xsl:template>

<xsl:template match="MeetingList" mode="table-body">
   <xsl:for-each select="Meeting">
      <tr>
         <xsl:attribute name="bgcolor">
            <xsl:choose>
               <xsl:when test="position() mod 2 = 0">
                  <xsl:value-of select="$mioga-list-even-row-bg-color"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="$mioga-list-odd-row-bg-color"/>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:attribute>
         <td align="center" bgcolor="{@bgcolor}">
            <xsl:choose>
               <xsl:when test="@private = 1">
                   <i style="color: {@fgcolor}">
                     <xsl:call-template name="organizerTranslation">
                       <xsl:with-param name="type">private-meeting</xsl:with-param>
                     </xsl:call-template>
                   </i>
               </xsl:when>
               <xsl:when test="@canWrite = 1">
                   <a style="color: {@fgcolor}" href="{$private_bin_uri}/Organizer/CreateMeeting?rowid={@rowid}"><xsl:value-of select="."/></a>
               </xsl:when>
               <xsl:otherwise>
                   <a style="color: {@fgcolor}" href="{$private_bin_uri}/Organizer/ViewMeeting?rowid={@rowid}"><xsl:value-of select="."/></a>
               </xsl:otherwise>
            </xsl:choose>
         </td>
      </tr>
   </xsl:for-each>
</xsl:template>

<!-- *********************************************************************** -->
<!-- * Error                                                               * -->
<!-- *********************************************************************** -->
<xsl:template match="Error">
   <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>
        
      <xsl:value-of select="."/>
   </body>
</xsl:template>

<xsl:template match="ExportOrganizer" mode="MiogaFormBody">
    
    <xsl:call-template name="MiogaFormInputSelect">
        <xsl:with-param name="label">format</xsl:with-param>
        <xsl:with-param name="choices" select="available_format/format/@ident"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputSelect">
        <xsl:with-param name="label">start_date</xsl:with-param>
        <xsl:with-param name="type">0</xsl:with-param>
        <xsl:with-param name="choices" select="start_date/date"/>
    </xsl:call-template>

    
</xsl:template>

<xsl:template match="ExportOrganizer" mode="MiogaFormButton">
    
    <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">export_act</xsl:with-param>
        <xsl:with-param name="referer" select="referer"/>
    </xsl:call-template>

</xsl:template>

<xsl:template match="ExportOrganizer">
    <body xsl:use-attribute-sets="body_attr">
        <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="MiogaTitle"/>

        <div xsl:use-attribute-sets="app_form_body_attr">
            <xsl:call-template name="MiogaForm">
                <xsl:with-param name="label" select="name(.)"/>
                <xsl:with-param name="action" select="name(.)"/>
                <xsl:with-param name="method">GET</xsl:with-param>
            </xsl:call-template>
        </div>
    </body>
</xsl:template>



</xsl:stylesheet>
