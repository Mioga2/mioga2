<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>
<xsl:import href="articles-parts.xsl"/>

<xsl:output method="xml" indent="no" omit-xml-declaration="yes" encoding="UTF-8" media-type="text/javascript"/>

<xsl:include href="scriptaculous.xsl"/>
<!-- ===============
	Variables
	================ -->

<xsl:variable name="anim_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Animation">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="moder_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Moderation">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="write_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Write">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="read_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Read">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>


<!--
===============
 Templates
===============
-->

<xsl:template match="/">
    <xsl:apply-templates/>
</xsl:template>


<!--
=======================================
 DisplayArticleList
=======================================
-->
<xsl:template name="DisplayArticleList">
    <xsl:variable name="articles">
        <xsl:apply-templates select="Articles">
            <xsl:with-param name="page" select="Page"/>
            <xsl:with-param name="js_mode">1</xsl:with-param>
        </xsl:apply-templates>
    </xsl:variable>
    Element.update("articles-box",'<xsl:copy-of select="$articles"/>');
</xsl:template>

<!--
=======================================
 EditBox
=======================================
-->
<xsl:template name="EditBox">
    <xsl:variable name="article">
        <xsl:call-template name="WriteOrEditArticle">
            <xsl:with-param name="action" select="$action"/>
        </xsl:call-template>
    </xsl:variable>
    
    Element.update("edit-box",'<xsl:copy-of select="$article"/>');
    tinyMCE.idCounter=0;
    $("edit-box").show();
    tinyMCE.execCommand("mceAddControl", false, "contents");
    $("disable-window").show();
    setVisible($("edit-box"));
</xsl:template>



<!--
==================
JS for UpdateArticle
==================
-->
<xsl:template match="UpdateArticle">
    <xsl:choose>
        <xsl:when test="//errors">
            <xsl:variable name="article">
                <xsl:call-template name="WriteOrEditArticle">
                    <xsl:with-param name="action" select="//EditAction"/>
                </xsl:call-template>
            </xsl:variable>
            
            Element.update("edit-box",'<xsl:copy-of select="$article"/>');
            tinyMCE.idCounter=0;
            tinyMCE.execCommand("mceAddControl", false, "contents");
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="DisplayArticleList"/>
            <xsl:call-template name="CategoriesTemplate"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<!--
=======================================
 UpdateOptions JS
=======================================
-->
<xsl:template match="UpdateOptions">
    $("disable-window").hide();
    $("options-box").hide();
</xsl:template>

<!--
=======================================
 DisplayOptions JS
=======================================
-->
<xsl:template match="DisplayOptions">
    <xsl:variable name="options">
        <xsl:apply-templates select="Options">
            <xsl:with-param name="js_mode">1</xsl:with-param>
        </xsl:apply-templates>
    </xsl:variable>
    
    Element.update("options-box", '<xsl:copy-of select="$options"/>');
    $("disable-window").show();
    $("options-box").show();
</xsl:template>

<!--
=======================================
 ValidateArticle JS
=======================================
-->
<xsl:template match="ValidateArticle">
    <xsl:call-template name="DisplayArticleList"/>
</xsl:template>

<!--
=======================================
 DeleteArticle JS
=======================================
-->
<xsl:template match="DeleteArticle" >
    <xsl:call-template name="CategoriesTemplate"/>
    new Effect.Fade($("article-<xsl:value-of select="DeletedArticle"/>"), { duration: 0.5});
</xsl:template>

<!--
=======================================
 DeclineArticle JS
=======================================
-->
<xsl:template match="DeclineArticle">
    <xsl:call-template name="DisplayArticleList"/>
</xsl:template>

<!--
=======================================
 EditArticle JS
=======================================
-->
<xsl:template match="EditArticle">
    <xsl:call-template name="EditBox">
        <xsl:with-param name="action">UpdateArticle</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<!--
=======================================
 WriteArticle JS
=======================================
-->
<xsl:template match="WriteArticle">
    <xsl:call-template name="EditBox">
        <xsl:with-param name="action">WriteArticle</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<!--
=======================================
 CreateCategory JS
=======================================
-->
<xsl:template match="CreateCategory">
	<xsl:call-template name="CategoriesTemplate"/>
</xsl:template>

<!--
=======================================
 DeleteCategory JS
=======================================
-->
<xsl:template match="DeleteCategory" >
    <xsl:call-template name="CategoriesTemplate"/>
</xsl:template>

<!--
=======================================
 CategoriesTemplate
=======================================
-->
<xsl:template name="CategoriesTemplate">
	<xsl:variable name="categories">
        <xsl:apply-templates select="Categories">
            <xsl:with-param name="js_mode">1</xsl:with-param>
            <xsl:with-param name="page" select="Page"/>
        </xsl:apply-templates>
    </xsl:variable>
    
    <xsl:variable name="new_article">
        <xsl:choose>
        	<xsl:when test="NoCategory"><xsl:call-template name="DisabledNewArticle">
            <xsl:with-param name="js_mode">1</xsl:with-param>
        </xsl:call-template></xsl:when>
        	<xsl:otherwise><xsl:call-template name="NewArticle">
            <xsl:with-param name="js_mode">1</xsl:with-param>
            <xsl:with-param name="page" select="Page"/>
        </xsl:call-template></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    Element.update("catg-box", '<xsl:copy-of select="$categories"/>');
    Element.update("new_article", '<xsl:copy-of select="$new_article"/>');
</xsl:template>

<!--
=======================================
 SaveArticle
=======================================
-->
<xsl:template match="SaveArticle">
	<xsl:choose>
		<xsl:when test="//errors">
        <xsl:variable name="error">
          <xsl:call-template name="escape-apos">
            <xsl:with-param name="string" select="//errors/Description"/>
          </xsl:call-template>
        </xsl:variable>
        Element.update("dl-error", '<xsl:copy-of select="$error"/>');
        $("dl-error").show();
        </xsl:when>
		<xsl:otherwise>
        $("dl-error").hide();
        $("download-box").hide();
        $("disable-window").hide();
        </xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>
