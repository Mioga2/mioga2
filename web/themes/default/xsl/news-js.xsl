<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>
<xsl:import href="news-parts.xsl"/>

<xsl:output method="xml" indent="no" omit-xml-declaration="yes" encoding="UTF-8" media-type="text/javascript"/>

<xsl:include href="scriptaculous.xsl"/>
<!-- ===============
	Variables
	================ -->

<xsl:variable name="anim_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Animation">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="moder_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Moderation">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="write_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Write">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="read_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Read">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>


<!--
===============
 Templates
===============
-->





<xsl:template match="/">
    <xsl:apply-templates/>
</xsl:template>

<!--
=======================================
 DisplayNewsList
=======================================
-->
<xsl:template name="DisplayNewsList">
    <xsl:variable name="news">
        <xsl:apply-templates select="News">
            <xsl:with-param name="page" select="Page"/>
            <xsl:with-param name="js_mode">1</xsl:with-param>
        </xsl:apply-templates>
    </xsl:variable>
    
    $("disable-window").hide();
    setHidden($("edit-box"));
    $("edit-box").hide();
    Element.update("news-box",'<xsl:copy-of select="$news"/>');
</xsl:template>


<!--
=======================================
 UpdateOptions JS
=======================================
-->
<xsl:template match="UpdateOptions">
    $("disable-window").hide();
    $("options-box").hide();
</xsl:template>

<!--
=======================================
 DisplayOptions JS
=======================================
-->
<xsl:template match="DisplayOptions">
    <xsl:variable name="options">
        <xsl:apply-templates select="Options">
            <xsl:with-param name="js_mode">1</xsl:with-param>
        </xsl:apply-templates>
    </xsl:variable>
    
    Element.update("options-box", '<xsl:copy-of select="$options"/>');
    $("disable-window").show();
    $("options-box").show();
</xsl:template>

<!--
=======================================
 ValidateNewsItem JS
=======================================
-->
<xsl:template match="ValidateNewsItem">
    <xsl:call-template name="DisplayNewsList"/>
</xsl:template>

<!--
=======================================
 DeleteNewsItem JS
=======================================
-->
<xsl:template match="DeleteNewsItem" >
    <xsl:call-template name="DisplayNewsList"/>
</xsl:template>

<!--
=======================================
 DeclineNewsItem JS
=======================================
-->
<xsl:template match="DeclineNewsItem">
    <xsl:call-template name="DisplayNewsList"/>
</xsl:template>

</xsl:stylesheet>
