<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"
  doctype-public="-//W3C//DTD HTML 4.01//EN"
  doctype-system="http://www.w3.org/TR/html4/strict.dtd"
/>
<xsl:include href="box.xsl"/>
<xsl:include href="dojotoolkit.xsl"/>
<xsl:include href="grid.xsl"/>

<!-- ===============
	root document
	================ -->

<xsl:template name="colbertsa-css">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/colbertsa.css" media="screen" />
</xsl:template>

<xsl:template name="colbertsa-js">
	<script src="{$theme_uri}/javascript/colbertsa.js" type="text/javascript"></script>
</xsl:template>

<xsl:template match="/">
<html>
<head>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>


	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="dojo-css"/>
	<xsl:call-template name="dojo-js"/>
	<xsl:call-template name="mioga_dojo-js"/>

	<xsl:call-template name="colbertsa-css"/>
	<xsl:call-template name="colbertsa-js"/>

	<xsl:apply-templates mode="head"/>

	<xsl:call-template name="theme-css"/>
</head>
<body id="colbertsa_body" class="colbertsa mioga">
	<script type="text/javascript">
		var lang = '<xsl:value-of select="//miogacontext/group/lang"/>';
	</script>
	<xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- ===============
	DisplayMain
	================ -->

<xsl:template match="DisplayMain" mode="head">
	<xsl:variable name="ident"><xsl:value-of select="Grid[@name='instances']/ident" /></xsl:variable>
	<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="filter_form_id">filter_form_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:apply-templates select="Grid" mode="head"/>

	<script type="text/javascript">
		var grid_id = '<xsl:value-of select="$grid_id" />';
		var dataStoreUrl = '<xsl:value-of select="Grid/GetData" />';
		var dataStoreNodeName = '<xsl:value-of select="Grid/NodeName" />';
		var dataStoreIdentifier = '<xsl:value-of select="Grid/Identifier" />';
		var dataStoreLabel = '<xsl:value-of select="Grid/Label" />';
		var filter_form_id = '<xsl:value-of select="$filter_form_id" />';
	</script>
</xsl:template>

<xsl:template match="DisplayMain">
	<div id="border-main" dojoType="dijit.layout.BorderContainer" style="margin:0;padding:0;height:100%; width:100%">
		<!-- DisplayAppTitle -->
		<div dojoType="dijit.layout.ContentPane" region="top" style="margin:0;padding:0;" splitter="false">
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#colbertsa</xsl:with-param>
			</xsl:call-template>
		</div>

		<div id="border-instances" dojoType="dijit.layout.BorderContainer" region="center" style="margin:0;padding:0;height:100%; width:100%">
			<!-- Actions -->
			<div dojoType="dijit.layout.ContentPane" region="left" style="width: 20%;margin:0;padding-left:5px;padding-top:5px;" splitter="false">
				<ul class="vmenu">
					<li>
						<a id="add-instance" href="javascript:void (0);">
							<xsl:attribute name="onclick"><xsl:value-of select="Grid/DefaultAction" />(null); return false;</xsl:attribute>
							<xsl:value-of select="mioga:gettext('Add instance')"/>
						</a>
					</li>
					<li>
						<a href="DownloadSkeletons?type=instance">
							<xsl:value-of select="mioga:gettext ('Download skeletons')"/>
						</a>
					</li>
					<li>
						<a href="javascript:void (0);">
							<xsl:attribute name="onclick">DisplaySkeletonsUploadDialog ();</xsl:attribute>
							<xsl:value-of select="mioga:gettext ('Upload skeletons')"/>
						</a>
					</li>
					<li>
						<a href="DownloadLoginResources">
							<xsl:value-of select="mioga:gettext ('Download login system resources')"/>
						</a>
					</li>
					<li>
						<a href="javascript:void (0);">
							<xsl:attribute name="onclick">DisplayLoginResourcesUploadDialog ();</xsl:attribute>
							<xsl:value-of select="mioga:gettext ('Upload login system resources')"/>
						</a>
					</li>
				</ul>
			</div>

			<!-- List -->
			<div dojoType="dijit.layout.ContentPane" region="center" style="margin:0;padding:0;" splitter="false">
				<!-- Instances grid-->
				<xsl:variable name="ident"><xsl:value-of select="Grid[@name='instances']/ident" /></xsl:variable>
				<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
				<xsl:apply-templates select="Grid[@name='instances']">
					<xsl:with-param name="ident" select="$ident" />
					<xsl:with-param name="grid_id" select="$grid_id" />
					<xsl:with-param name="store_id" select="$store_id" />
					<xsl:with-param name="default_sort_field">ident</xsl:with-param>
					<xsl:with-param name="store_prefs">StorePreferences</xsl:with-param>
					<xsl:with-param name="dojo_i18n_bundle">colbertsa</xsl:with-param>
				</xsl:apply-templates>
			</div>
		</div>
	</div>

	<div id="edit_instance" class="edit_instance_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Edit instance')"/></xsl:attribute>
		<div id="edit_instance_error_block">
		</div>
		<div id="tabbed_dialog" dojoType="mioga.TabbedDialog" onValidate="ValidateInstance ();" onCancel="CloseInstanceDialog ();">
			<div dojoType="dijit.form.Form" id="instance_attributes_form" class="dojo-form">
				<div dojoType="mioga.TabItem" id="instance-attributes-tab">
					<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Instance {ident} Attributes')"/></xsl:attribute>
					<div id="instance-attributes">
						<div class="instance-attributes-left-align">
							<input dojoType="dijit.form.TextBox" type="hidden" id="instance_attributes_rowid" name="rowid"/>

							<fieldset>
								<legend><xsl:value-of select="mioga:gettext('General Attributes')"/></legend>
								<div class="form-item">
									<label id="instance_attributes_ident_label" for="ident"><xsl:value-of select="mioga:gettext('Identifier')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-attributes-input" id="instance_attributes_ident" name="ident"/>
								</div>

								<div class="form-item">
									<label id="instance_attributes_comment_label" for="comment"><xsl:value-of select="mioga:gettext ('Comment')"/></label>
									<input dojoType="dijit.form.Textarea" class="instance-attributes-input" id="instance_attributes_comment" name="comment"/>
								</div>

								<div id="skel-chooser" class="form-item">
									<label id="instance_attributes_skeleton_label" for="skeleton"><xsl:value-of select="mioga:gettext ('Skeleton')"/></label>
									<div dojoType="mioga.data.ItemFileReadStore" jsId="skelStore" url="GetSkeletons.json" nodename="skeleton" identifier="file"></div>
									<input dojoType="dijit.form.FilteringSelect" class="instance-attributes-input" id="instance_attributes_skeleton" name="skeleton" store="skelStore" searchAttr="name" onChange="UpdateInstanceSkeletonDetails ();"/>
								</div>

								<div class="form-item">
									<label id="instance_attributes_lang_label" for="lang"><xsl:value-of select="mioga:gettext ('Language')"/></label>
									<div dojoType="mioga.data.ItemFileReadStore" jsId="langStore" url="GetLanguages.json" nodename="language" identifier="ident"></div>
									<input dojoType="dijit.form.FilteringSelect" class="instance-attributes-input" id="instance_attributes_lang" name="lang" store="langStore" searchAttr="label"/>
								</div>

								<div id="theme-chooser" class="form-item">
									<label id="instance_attributes_default_theme_id_label" for="default_theme_id"><xsl:value-of select="mioga:gettext ('Default Theme')"/></label>
									<input dojoType="dijit.form.FilteringSelect" class="instance-attributes-input" id="instance_attributes_default_theme_id" name="default_theme_id" searchAttr="name"/>
								</div>
							</fieldset>

							<xsl:if test="//miogacontext/shared_admin = 0">
								<fieldset id="admin-chooser">
									<legend><xsl:value-of select="mioga:gettext ('Admin Account')"/></legend>

									<xsl:if test="//miogacontext/centralized_users = 1">
										<p class="note">
											<xsl:value-of select="mioga:gettext ('The admin account will be linked to a centralized user base. Please fill the ident and email of an existing user you wish to be admin of the instance.')"/>
										</p>
									</xsl:if>

									<div class="form-item">
										<label id="instance_attributes_admin_ident_label" for="admin_ident"><xsl:value-of select="mioga:gettext('Identifier')"/></label>
										<input dojoType="dijit.form.TextBox" class="instance-attributes-input" id="instance_attributes_admin_ident" name="admin_ident"/>
									</div>

									<div class="form-item">
										<label id="instance_attributes_admin_email_label" for="admin_email"><xsl:value-of select="mioga:gettext('Email')"/></label>
										<input dojoType="dijit.form.TextBox" class="instance-attributes-input" id="instance_attributes_admin_email" name="admin_email"/>
									</div>

									<xsl:if test="//miogacontext/centralized_users = 0">
										<div class="form-item">
											<label id="instance_attributes_admin_password_label" for="admin_password"><xsl:value-of select="mioga:gettext('Password')"/></label>
											<input dojoType="dijit.form.TextBox" type="password" class="instance-attributes-input" id="instance_attributes_admin_password" name="admin_password"/>
										</div>

										<div class="form-item">
											<label id="instance_attributes_admin_password_confirm_label" for="admin_password_confirm"><xsl:value-of select="mioga:gettext('Password confirmation')"/></label>
											<input dojoType="dijit.form.TextBox" type="password" class="instance-attributes-input" id="instance_attributes_admin_password_confirm" name="admin_password_confirm"/>
										</div>
									</xsl:if>
								</fieldset>
							</xsl:if>

							<div id="referent-chooser">
								<fieldset>
									<legend><xsl:value-of select="mioga:gettext ('Instance Referent')"/></legend>
									<div class="form-item">
										<label id="instance_attributes_referent_id_label" for="referent"><xsl:value-of select="mioga:gettext ('Referent')"/></label>
										<input dojoType="dijit.form.FilteringSelect" class="instance-attributes-input" id="instance_attributes_referent_id" name="referent_id" searchAttr="label" autoComplete="false" required="false">
											<xsl:attribute name="queryExpr">*${0}*</xsl:attribute>
										</input>
									</div>
									<div class="form-item">
										<label id="instance_attributes_referent_comment_label" for="referent_comment"><xsl:value-of select="mioga:gettext ('Comment')"/></label>
										<input dojoType="dijit.form.TextBox" class="instance-attributes-input" id="instance_attributes_referent_comment" name="referent_comment"/>
									</div>
								</fieldset>
							</div>

							<div id="homepage-chooser">
								<fieldset>
									<legend><xsl:value-of select="mioga:gettext ('Instance Homepage')"/></legend>
				
									<!-- Information message -->
									<xsl:call-template name="InfoMessage">
										<xsl:with-param name="id">homepage_info</xsl:with-param>
										<xsl:with-param name="title">
											<xsl:value-of select="mioga:gettext('Instance Homepage')"/>
										</xsl:with-param>
										<xsl:with-param name="message">
											<p>
												<xsl:value-of select="mioga:gettext('When a user connects to the base URL of an instance he gets automatically redirected to homepage. Homepage can be either a group or a static page (that has to be within the disk space of the instance).')"/>
											</p>
											<p class="note">
												<xsl:value-of select="mioga:gettext('If a default group is defined, then the static homepage is ignored.')"/>
											</p>
										</xsl:with-param>
									</xsl:call-template>
									<div class="form-item">
										<label id="instance_attributes_default_group_id_label" for="default_group"><xsl:value-of select="mioga:gettext ('Default Group')"/></label>
										<input dojoType="dijit.form.FilteringSelect" class="instance-attributes-input" id="instance_attributes_default_group_id" name="default_group_id" searchAttr="ident" required="false"/>
									</div>
									<div class="form-item">
										<label id="instance_attributes_homepage_label" for="homepage"><xsl:value-of select="mioga:gettext ('Static Homepage')"/></label>
										<input dojoType="dijit.form.TextBox" class="instance-attributes-input" id="instance_attributes_homepage" name="homepage"/>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
				</div>
				<div dojoType="mioga.TabItem" id="security-attributes-tab">
					<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Instance {ident} Security Attributes')"/></xsl:attribute>
					<div id="instance-security-attributes">
						<div class="instance-attributes-left-align">
							<fieldset>
								<legend><xsl:value-of select="mioga:gettext ('Password Policy')"/></legend>
								<div class="form-item">
									<label id="instance_attributes_use_secret_question_label" for="use_secret_question"><xsl:value-of select="mioga:gettext ('Secure passwords with secret question')"/></label>
									<input dojoType="dijit.form.CheckBox" class="instance-security-attributes-input" id="instance_use_secret_question" name="use_secret_question"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_doris_can_set_secret_question" for="doris_can_set_secret_question"><xsl:value-of select="mioga:gettext ('Let users set their secret question through password assistance')"/></label>
									<input dojoType="dijit.form.CheckBox" class="instance-security-attributes-input" id="instance_doris_can_set_secret_question" name="doris_can_set_secret_question"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_pwd_min_length_label" for="pwd_min_length"><xsl:value-of select="mioga:gettext ('Minimum length')"/></label>
									<input dojoType="dijit.form.NumberSpinner" class="instance-security-attributes-input" id="instance_pwd_min_length" name="pwd_min_length">
										<xsl:attribute name="constraints">{min:0}</xsl:attribute>
									</input>
								</div>
								<div class="form-item">
									<label id="instance_attributes_pwd_min_letter_label" for="pwd_min_letter"><xsl:value-of select="mioga:gettext ('Minimum number of letters')"/></label>
									<input dojoType="dijit.form.NumberSpinner" class="instance-security-attributes-input" id="instance_pwd_min_letter" name="pwd_min_letter">
										<xsl:attribute name="constraints">{min:0}</xsl:attribute>
									</input>
								</div>
								<div class="form-item">
									<label id="instance_attributes_pwd_min_digit_label" for="pwd_min_digit"><xsl:value-of select="mioga:gettext ('Minimum number of digits')"/></label>
									<div dojoType="dijit.form.NumberSpinner" class="instance-security-attributes-input" id="instance_pwd_min_digit" name="pwd_min_digit">
										<xsl:attribute name="constraints">{min:0}</xsl:attribute>
									</div>
								</div>
								<div class="form-item">
									<label id="instance_attributes_pwd_min_special_label" for="pwd_min_special"><xsl:value-of select="mioga:gettext ('Minimum number of special characters')"/></label>
									<input dojoType="dijit.form.NumberSpinner" class="instance-security-attributes-input" id="instance_pwd_min_special" name="pwd_min_special">
										<xsl:attribute name="constraints">{min:0}</xsl:attribute>
									</input>
								</div>
								<div class="form-item">
									<label id="instance_attributes_pwd_min_chcase_label" for="pwd_min_chcase"><xsl:value-of select="mioga:gettext ('Minimum number of case changes')"/></label>
									<input dojoType="dijit.form.NumberSpinner" class="instance-security-attributes-input" id="instance_pwd_min_chcase" name="pwd_min_chcase">
										<xsl:attribute name="constraints">{min:0}</xsl:attribute>
									</input>
								</div>
							</fieldset>
							<fieldset>
								<legend><xsl:value-of select="mioga:gettext ('Other Attributes')"/></legend>
				
								<!-- Information message -->
								<xsl:call-template name="InfoMessage">
									<xsl:with-param name="id">auth_fail_timeout_info</xsl:with-param>
									<xsl:with-param name="title">
										<xsl:value-of select="mioga:gettext('Account lock timeout')"/>
									</xsl:with-param>
									<xsl:with-param name="message">
										<p>
											<xsl:value-of select="mioga:gettext('When a user account is used three times for authentication with a wrong password, it gets locked. This timeout is the period it remains locked before being automatically unlocked.')"/>
										</p>
									</xsl:with-param>
								</xsl:call-template>
								<div class="form-item">
									<label id="instance_attributes_auth_fail_timeout_label" for="auth_fail_timeout"><xsl:value-of select="mioga:gettext ('Account lock timeout in minutes')"/></label>
									<input dojoType="dijit.form.NumberSpinner" class="instance-security-attributes-input" id="instance_auth_fail_timeout" name="auth_fail_timeout">
										<xsl:attribute name="constraints">{min:0}</xsl:attribute>
									</input>
								</div>
								<div class="form-item">
									<label id="instance_attributes_password_crypt_method_label" for="password_crypt_method"><xsl:value-of select="mioga:gettext ('Password crypt method')"/></label>
									<select dojoType="dijit.form.FilteringSelect" class="instance-security-attributes-input" id="instance_password_crypt_method" name="password_crypt_method">
										<option value="md5">md5</option>
										<option value="sha">sha</option>
										<option value="crypt">crypt</option>
										<option value="clear">clear</option>
									</select>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
				<div dojoType="mioga.TabItem" id="ldap-attributes-tab">
					<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Instance {ident} LDAP Directory Attributes')"/></xsl:attribute>
					<div id="instance-ldap-attributes">
						<div class="instance-attributes-left-align">
							<fieldset>
								<legend><xsl:value-of select="mioga:gettext('LDAP Directory Attributes')"/></legend>
								<div class="form-item">
									<label id="instance_attributes_use_ldap_label" for="use_ldap"><xsl:value-of select="mioga:gettext ('Connect Mioga2 to a LDAP directory')"/></label>
									<input dojoType="dijit.form.CheckBox" class="instance-ldap-attributes-input" id="instance_use_ldap" name="use_ldap" onClick="ableLDAPAttributes ();"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_access_label" for="ldap_access"><xsl:value-of select="mioga:gettext ('Access to LDAP directory')"/></label>
									<select dojoType="dijit.form.FilteringSelect" class="instance-ldap-attributes-input" name="ldap_access" id="instance_ldap_access">
										<option value="2"><xsl:value-of select="mioga:gettext ('Full access')"/></option>
										<option value="1"><xsl:value-of select="mioga:gettext ('Write access to instance affectation')"/></option>
										<option value="0"><xsl:value-of select="mioga:gettext ('Read-only')"/></option>
									</select>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_host_label" for="ldap_host"><xsl:value-of select="mioga:gettext ('Hostname')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-ldap-attributes-input" id="instance_ldap_host" name="ldap_host"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_port_label" for="ldap_port"><xsl:value-of select="mioga:gettext ('Port')"/></label>
									<input dojoType="dijit.form.NumberSpinner" class="instance-ldap-attributes-input" id="instance_ldap_port" name="ldap_port">
										<xsl:attribute name="constraints">{min:1,max:65535}</xsl:attribute>
									</input>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_bind_dn_label" for="ldap_bind_dn"><xsl:value-of select="mioga:gettext ('Bind DN')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-ldap-attributes-input" id="instance_ldap_bind_dn" name="ldap_bind_dn"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_bind_pwd_label" for="ldap_bind_pwd"><xsl:value-of select="mioga:gettext ('Bind password')"/></label>
									<input dojoType="dijit.form.TextBox" type="password" class="instance-ldap-attributes-input" id="instance_ldap_bind_pwd" name="ldap_bind_pwd"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_user_base_dn_label" for="ldap_user_base_dn"><xsl:value-of select="mioga:gettext ('Base DN')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-ldap-attributes-input" id="instance_ldap_user_base_dn" name="ldap_user_base_dn"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_user_filter_label" for="ldap_user_filter"><xsl:value-of select="mioga:gettext ('Filter')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-ldap-attributes-input" id="instance_ldap_user_filter" name="ldap_user_filter"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_user_scope_label" for="ldap_user_scope"><xsl:value-of select="mioga:gettext ('Search Scope')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-ldap-attributes-input" id="instance_ldap_user_scope" name="ldap_user_scope"/>
								</div>
							</fieldset>
							<fieldset>
								<legend><xsl:value-of select="mioga:gettext ('LDAP Attributes Translation')"/></legend>
								<div class="form-item">
									<label id="instance_attributes_ldap_user_ident_attr_label" for="ldap_user_ident_attr"><xsl:value-of select="mioga:gettext ('Identifier')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-ldap-attributes-input" id="instance_ldap_user_ident_attr" name="ldap_user_ident_attr"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_user_firstname_attr_label" for="ldap_user_firstname_attr"><xsl:value-of select="mioga:gettext ('Firstname')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-ldap-attributes-input" id="instance_ldap_user_firstname_attr" name="ldap_user_firstname_attr"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_user_lastname_attr_label" for="ldap_user_lastname_attr"><xsl:value-of select="mioga:gettext ('Lastname')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-ldap-attributes-input" id="instance_ldap_user_lastname_attr" name="ldap_user_lastname_attr"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_user_email_attr_label" for="ldap_user_email_attr"><xsl:value-of select="mioga:gettext ('Email')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-ldap-attributes-input" id="instance_ldap_user_email_attr" name="ldap_user_email_attr"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_user_pwd_attr_label" for="ldap_user_pwd_attr"><xsl:value-of select="mioga:gettext ('Password')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-ldap-attributes-input" id="instance_ldap_user_pwd_attr" name="ldap_user_pwd_attr"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_user_desc_attr_label" for="ldap_user_desc_attr"><xsl:value-of select="mioga:gettext ('Description')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-ldap-attributes-input" id="instance_ldap_user_desc_attr" name="ldap_user_desc_attr"/>
								</div>
								<div class="form-item">
									<label id="instance_attributes_ldap_user_inst_attr_label" for="ldap_user_inst_attr"><xsl:value-of select="mioga:gettext ('Instance List')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-ldap-attributes-input" id="instance_ldap_user_inst_attr" name="ldap_user_inst_attr"/>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
				<div dojoType="mioga.TabItem" id="limits-attributes-tab">
					<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Instance {ident} Utilization Limits')"/></xsl:attribute>
					<div id="instance-limits-attributes">
						<div class="instance-attributes-left-align">
							<fieldset>
								<legend><xsl:value-of select="mioga:gettext('Utilization Limits')"/></legend>
								<div class="form-item">
									<label id="instance_attributes_max_allowed_group_label" for="max_allowed_group"><xsl:value-of select="mioga:gettext ('Maximum number of groups')"/></label>
									<input dojoType="dijit.form.NumberSpinner" class="instance-limits-attributes-input" id="instance_max_allowed_group" name="max_allowed_group">
										<xsl:attribute name="constraints">{min:0}</xsl:attribute>
									</input>
								</div>
								<div class="form-item">
									<label id="instance_attributes_quota_soft_label" for="quota_soft"><xsl:value-of select="mioga:gettext ('Soft limit of disk usage (MB)')"/></label>
									<input dojoType="dijit.form.NumberSpinner" class="instance-limits-attributes-input" id="instance_quota_soft" name="quota_soft">
										<xsl:attribute name="constraints">{min:0}</xsl:attribute>
									</input>
								</div>
								<div class="form-item">
									<label id="instance_attributes_quota_hard_label" for="quota_hard"><xsl:value-of select="mioga:gettext ('Hard limit of disk usage (MB)')"/></label>
									<input dojoType="dijit.form.NumberSpinner" class="instance-limits-attributes-input" id="instance_quota_hard" name="quota_hard">
										<xsl:attribute name="constraints">{min:0}</xsl:attribute>
									</input>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
				<div dojoType="mioga.TabItem" id="system-attributes-tab">
					<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Instance {ident} System Settings')"/></xsl:attribute>
					<div id="instance-system-attributes">
						<div class="instance-attributes-left-align">
							<fieldset>
								<legend><xsl:value-of select="mioga:gettext('System Settings')"/></legend>
								<div class="form-item">
									<label id="instance_attributes_domain_name_label" for="domain_name"><xsl:value-of select="mioga:gettext ('Domain name')"/></label>
									<input dojoType="dijit.form.TextBox" class="instance-system-attributes-input" id="instance_domain_name" name="domain_name"/>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
				<div dojoType="mioga.TabItem" id="instance-apps-tab">
					<xsl:attribute name="legend"><xsl:value-of select="mioga:gettext ('Instance {ident} Applications')"/></xsl:attribute>
					<div id="instance_applications_label"></div>
					<div id="instance_applications" dojoType="mioga.SelectList" label="label" sort="label">
						<xsl:attribute name="sort_fields">application_sort_fields</xsl:attribute>
						<xsl:attribute name="secondary_sort_field">"label"</xsl:attribute>
						<xsl:attribute name="sel_label"><xsl:value-of select="mioga:gettext ('Instance has application')"/></xsl:attribute>
						<xsl:attribute name="unsel_label"><xsl:value-of select="mioga:gettext ('Instance does not have application')"/></xsl:attribute>
					</div>
				</div>
				<div class="clear-both"></div>
			</div>
		</div>
	</div>

	<!-- IFrame to upload files -->
	<iframe id="form_submit_iframe" name="form_submit_iframe" style="width:0px;height:0px;display:none;">
	</iframe>

	<!-- Skeleton upload dialog -->
	<div id="upload_skeletons" class="upload_skeletons_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Upload skeletons')"/></xsl:attribute>
		<div id="upload_skeletons_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="upload_skeletons_error_block_content"></div>
		</div>
		<form dojoType="dijit.form.Form" target="form_submit_iframe" id="upload_skeletons_form" class="dojo-form" action="UploadSkeletons" method="post" enctype="multipart/form-data">
			<xsl:attribute name="onsubmit">ShowUnderlay ();</xsl:attribute>
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Select a skeletons archive')"/></legend>
				<xsl:call-template name="CSRF-input"/>
				<div class="form-item">
					<input type="hidden" name="type" value="instance"/>
				</div>
				<div class="form-item">
					<label for="file"><xsl:value-of select="mioga:gettext ('Skeletons ZIP archive')"/></label>
					<input type="file" id="file" name="skeletons_archive"/>
				</div>
				<ul class="button_list">
					<li>
						<button class="button" type="submit"><xsl:value-of select="mioga:gettext ('Upload')"/></button>
					</li>
				</ul>
			</fieldset>
		</form>
	</div>

	<!-- Login resources upload dialog -->
	<div id="upload_login_resources" class="upload_login_resources_dialog" dojoType="dijit.Dialog">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Upload login system resources')"/></xsl:attribute>
		<div id="upload_login_resources_error_block" class="errorbox">
			<h1><xsl:value-of select="mioga:gettext('Messages')"/></h1>
			<div id="upload_login_resources_error_block_content"></div>
		</div>
		<form dojoType="dijit.form.Form" target="form_submit_iframe" id="upload_login_resources_form" class="dojo-form" action="UploadLoginResources" method="post" enctype="multipart/form-data">
			<xsl:attribute name="onsubmit">ShowUnderlay ();</xsl:attribute>
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Select a login system resources archive')"/></legend>
				<xsl:call-template name="CSRF-input"/>
				<div class="form-item">
					<label for="file"><xsl:value-of select="mioga:gettext ('Login system resources ZIP archive')"/></label>
					<input type="file" id="file" name="login_resources_archive"/>
				</div>
				<ul class="button_list">
					<li>
						<button class="button" type="submit"><xsl:value-of select="mioga:gettext ('Upload')"/></button>
					</li>
				</ul>
			</fieldset>
		</form>
	</div>
</xsl:template>


<xsl:template match="UploadSkeletons" mode="head">
</xsl:template>

<xsl:template match="UploadSkeletons">
	<div id="upload-skeletons-errors">
		<xsl:if test="errors != ''">
			<ul>
				<xsl:for-each select="errors/err">
					<li><xsl:value-of select="mioga:gettext (@field)"/>: <xsl:value-of select="."/></li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</div>
	<script type="text/javascript">
		parent.UploadSkeletonsCallback (document.getElementById('upload-skeletons-errors').innerHTML);
	</script>
</xsl:template>


<xsl:template match="UploadLoginResources" mode="head">
</xsl:template>

<xsl:template match="UploadLoginResources">
	<div id="upload-login-resources-errors">
		<xsl:if test="errors != ''">
			<ul>
				<xsl:for-each select="errors/err">
					<li><xsl:value-of select="mioga:gettext (@field)"/>: <xsl:value-of select="."/></li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</div>
	<script type="text/javascript">
		parent.UploadLoginResourcesCallback (document.getElementById('upload-login-resources-errors').innerHTML);
	</script>
</xsl:template>

<!-- Info image and associated tooltip -->
<xsl:template name="InfoMessage">
	<xsl:param name="id"/>
	<xsl:param name="title"/>
	<xsl:param name="message"/>

	<div class="info">
		<img id="{$id}" src="{$theme_uri}/images/16x16/emblems/emblem-info.png"/>
		<div dojoType="dijit.Tooltip" connectId="{$id}">
			<h1><xsl:copy-of select="$title"/></h1>
			<div>
				<xsl:copy-of select="$message"/>
			</div>
		</div>
	</div>
</xsl:template>

</xsl:stylesheet>
