<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"/>
<xsl:include href="box.xsl"/>
<xsl:include href="grid.xsl"/>
<xsl:include href="jquery.xsl"/>
<xsl:include href="dojotoolkit.xsl"/>

<!-- ===============
	root document
	================ -->

<xsl:template name="Bottin-css">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/bottin.css" media="screen" />
</xsl:template>

<xsl:template name="Bottin-js">
	<script src="{$theme_uri}/javascript/bottin.js" type="text/javascript"></script>
</xsl:template>

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>


	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="dojo-css"/>
	<xsl:call-template name="dojo-js"/>
	<xsl:call-template name="mioga_dojo-js"/>

	<xsl:call-template name="Bottin-css"/>
	<xsl:call-template name="Bottin-js"/>
	
	<xsl:apply-templates mode="head"/>

	<xsl:call-template name="theme-css"/>
</head>
<body id="Bottin_body" class="Bottin mioga">
	<xsl:apply-templates />
</body>
</html>
</xsl:template>

<xsl:template match="DisplayMain" mode="head">
</xsl:template>

<xsl:template match="DisplayMain">
	<div class="form-container">
		<xsl:call-template name="box">
			<xsl:with-param name="title"><xsl:value-of select="mioga:gettext('Search users')"/></xsl:with-param>
			<xsl:with-param name="content">
				<form class="form" action="DisplayUsers" method="POST">
					<fieldset>
						<legend><xsl:value-of select="mioga:gettext ('Filter')"/></legend>
						<div class="count-container">
							<span class="counter-value"></span>&#160;<span class="matches-note" style="display:none"><xsl:value-of select="mioga:gettext ('match(es)')"/></span>
						</div>
						<div class="form-item">
							<label><xsl:value-of select="mioga:gettext ('Match type')"/></label>
							<select name="match" class="filter-field" dojoType="dijit.form.FilteringSelect" onkeypress="TriggerCount ();">
								<option value="begins"><xsl:value-of select="mioga:gettext ('Begins with')"/></option>
								<option value="contains"><xsl:value-of select="mioga:gettext ('Contains')"/></option>
								<option value="ends"><xsl:value-of select="mioga:gettext ('Ends with')"/></option>
							</select>
						</div>
						<div class="form-item">
							<label for="firstname"><xsl:value-of select="mioga:gettext ('Firstname')"/></label>
							<input type="text" class="" name="firstname" onkeypress="TriggerCount ();"/>
						</div>
						<div class="form-item">
							<label for="lastname"><xsl:value-of select="mioga:gettext ('Lastname')"/></label>
							<input type="text" name="lastname" onkeypress="TriggerCount ();"/>
						</div>
						<div class="form-item">
							<label for="email"><xsl:value-of select="mioga:gettext ('Email')"/></label>
							<input type="text" name="email" onkeypress="TriggerCount ();"/>
						</div>

						<ul class="button_list">
							<li><button class="button" type="submit"><xsl:value-of select="mioga:gettext ('OK')"/></button></li>
						</ul>
					</fieldset>
				</form>

				<div class="footnote">
					<xsl:value-of select="mioga:gettext ('To skip this step, click there:')"/>&#160;<a href="DisplayUsers">DisplayUsers</a>
				</div>
			</xsl:with-param>
		</xsl:call-template>
	</div>
</xsl:template>

<!-- ===============
	DisplayUsers
	================ -->

<xsl:template match="DisplayUsers" mode="head">
	<xsl:variable name="ident"><xsl:value-of select="Grid[@name='users']/ident" /></xsl:variable>
	<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:variable name="filter_form_id">filter_form_<xsl:value-of select="$ident" /></xsl:variable>
	<xsl:apply-templates select="Grid" mode="head"/>

	<script type="text/javascript">
		dojo.requireLocalization("mioga", "bottin");

		var grid_id = '<xsl:value-of select="$grid_id" />';
		var dataStoreUrl = '<xsl:value-of select="Grid/GetData" />';
		var dataStoreNodeName = '<xsl:value-of select="Grid/NodeName" />';
		var dataStoreIdentifier = '<xsl:value-of select="Grid/Identifier" />';
		var dataStoreLabel = '<xsl:value-of select="Grid/Label" />';
		var filter_form_id = '<xsl:value-of select="$filter_form_id" />';
	</script>
</xsl:template>

<xsl:template match="DisplayUsers">
		
	<div id="border-main" dojoType="dijit.layout.BorderContainer" style="margin:0;padding:0;height:100%; width:100%">
		<div dojoType="dijit.layout.ContentPane" region="top">
			<xsl:call-template name="DisplayAppTitle">
				<xsl:with-param name="help">applications.html#Bottin</xsl:with-param>
			</xsl:call-template>
		</div>

		<div dojoType="dijit.layout.ContentPane" region="left" style="margin:0;padding:0;height:100%;width:20%;" splitter="false">
			<div id="main_actions">
				<ul class="actions vmenu">
				<xsl:if test="@SuperAdmin=1">
					<xsl:call-template name="box">
						<xsl:with-param name="title"><xsl:value-of select="mioga:gettext('Instance')"/></xsl:with-param>
						<xsl:with-param name="content">
							<select dojoType="dijit.form.FilteringSelect" id="instance_selector" name="instance_selector">
								<xsl:attribute name="onChange">RefreshGrid ({instance: document.getElementById ('instance_selector').value}, AdaptUI)</xsl:attribute>
								<xsl:for-each select="Instance">
									<option>
										<xsl:attribute name="value"><xsl:value-of select="@access"/></xsl:attribute>
										<xsl:if test="@selected=1">
											<xsl:attribute name="selected">1</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="."/>
									</option>
								</xsl:for-each>
							</select>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
					<xsl:if test="//UserRights/Administration = 1 and ((@CanImport=1 or @CanModify=1) or @SuperAdmin=1)">
						<li id="add_user_menu_item">
							<a href="#">
								<xsl:attribute name="onclick"><xsl:value-of select="Grid/DefaultAction" />(); return false;</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Add User')"/>
							</a>
						</li>
						<li id="csv_import_users_menu_item">
							<a href="#" onclick="ShowImportWizard ();">
								<xsl:value-of select="mioga:gettext('Import Users')"/>
							</a>
						</li>
					</xsl:if>
					<li>
						<a href="#">
							<xsl:attribute name="onclick">ExportAll ();</xsl:attribute>
							<xsl:value-of select="mioga:gettext('Export Users')"/>
						</a>
					</li>
					<xsl:if test="@SuperAdmin=1">
						<li>
							<a href="#">
								<xsl:attribute name="onclick">SearchThroughDirectory ();</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Search through directory')"/>
							</a>
						</li>
					</xsl:if>
				</ul>
			</div>

			<xsl:if test="@SuperAdmin=1">
				<div id="directory_search_actions" style="display:none">
					<!-- Information message -->
					<xsl:call-template name="InfoMessage">
						<xsl:with-param name="id">search_mode_info</xsl:with-param>
						<xsl:with-param name="title">
							<xsl:value-of select="mioga:gettext('Search mode is enabled')"/>
						</xsl:with-param>
						<xsl:with-param name="message">
							<p>
								<xsl:value-of select="mioga:gettext ('Bottin is now in search mode. The list that you see corresponds to directory contents matching your search. You can not add users in this mode. If you wish to, you should exit search mode.')"/>
							</p>
						</xsl:with-param>
					</xsl:call-template>
					<ul class="actions vmenu">
						<li>
							<a href="#">
								<xsl:attribute name="onclick">SearchThroughDirectory ();</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Search through directory')"/>
							</a>
						</li>
						<li>
							<a href="#">
								<xsl:attribute name="onclick">ExitSearchMode ();</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Exit search mode')"/>
							</a>
						</li>
					</ul>
				</div>
			</xsl:if>
		</div>
	
		<!-- EditUser dialog -->
		<div id="edit_user" class="edit_user_dialog" dojoType="dijit.Dialog" title="Search params">
			<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Edit user')" /></xsl:attribute>
			<div dojoType="dijit.form.Form" id="edit_form" jsId="edit_form" class="form" encType="multipart/form-data" action="" method="">
				<script type="dojo/method" event="validate">
					ValidateUser();	
					dijit.byId("grid_users").update();
					return false;
				</script>
				
				<div id="error_block" class="error_block">
				</div>
				<div id="PasswordPolicy" style="display: none">
					<p class="error">
						<xsl:value-of select="mioga:gettext('Password does not match security policy.')"/>
						&#160;
						<xsl:value-of select="mioga:gettext('Passwords should contain at least:')"/>
					</p>
					<ul>
						<div class="list">
							<li><xsl:value-of select="mioga:gettext('%d characters,', string(PasswordPolicy/Length))"/></li>
							<li><xsl:value-of select="mioga:gettext('%d letters,', string(PasswordPolicy/Letters))"/></li>
							<li><xsl:value-of select="mioga:gettext('%d digits,', string(PasswordPolicy/Digits))"/></li>
							<li><xsl:value-of select="mioga:gettext('%d special characters,', string(PasswordPolicy/Special))"/></li>
							<li><xsl:value-of select="mioga:gettext('%d case changes.', string(PasswordPolicy/ChCase))"/></li>
						</div>
					</ul>
				</div>
				<div id="UserForm" name="UserForm">
					<fieldset>
						<legend><xsl:value-of select="mioga:gettext('User attributes')" /></legend>
						<input class="bottin-input" dojoType="dijit.form.TextBox" type="hidden" id="canmodify" name="canmodify" value="{@CanModify}"/>
						<input class="bottin-input" dojoType="dijit.form.TextBox" type="hidden" id="canimport" name="canimport" value="{@CanImport}"/>
						<input class="bottin-input" dojoType="dijit.form.TextBox" type="hidden" id="rowid"  name="rowid" value="" />
						<div class="form-item" id="user-firstname-cont">
							<label for="firstname" id="firstname_label"><xsl:value-of select="mioga:gettext('Firstname')" /></label>
							<input class="bottin-input" dojoType="dijit.form.TextBox" id="firstname"  name="firstname" value="" />
						</div>
						<div class="form-item" id="user-lastname-cont">
							<label for="lastname" id="lastname_label"><xsl:value-of select="mioga:gettext('Lastname')" /></label>
							<input class="bottin-input" dojoType="dijit.form.TextBox" id="lastname"  name="lastname" value="" />
						</div>
						<div class="form-item">
							<label for="email" id="email_label"><xsl:value-of select="mioga:gettext('Email')" /></label>
							<input class="bottin-input" dojoType="dijit.form.TextBox" id="email"  name="email" value="" />
						</div>
						<div class="form-item">
							<!-- <label for="ident"><xsl:value-of select="mioga:gettext('Identifier')" /></label> -->
							<input type="hidden" class="bottin-input" dojoType="dijit.form.TextBox" id="ident"  name="ident" value="" />
						</div>
						<div class="form-item" id="user-password-cont">
							<label for="password" id="password_label"><xsl:value-of select="mioga:gettext('Password')" /></label>
							<input class="bottin-input" dojoType="dijit.form.TextBox" type="password" id="password"  name="password" value="" />
						</div>
						<div class="form-item" id="user-password2-cont">
							<label for="password2" id="password2_label"><xsl:value-of select="mioga:gettext('Password (2)')" /></label>
							<input class="bottin-input" dojoType="dijit.form.TextBox" type="password" id="password2"  name="password2" value="" />
						</div>
						<div class="form-item" id="user-description-cont">
							<label for="description" id="description_label"><xsl:value-of select="mioga:gettext('Comment')" /></label>
							<input class="bottin-input" dojoType="dijit.form.Textarea" id="description"  name="description" value="" />
						</div>
						<xsl:if test="@SuperAdmin=1">
							<div class="form-item" id="instances_list_container">
								<label for="instances_list" id="instances_label"><xsl:value-of select="mioga:gettext('Instances')" /></label>
								<div id="instances_list"/>
							</div>
						</xsl:if>
					</fieldset>
				</div>
				<div id="EditImportBox" style="display: none">
					<fieldset>
						<legend><xsl:value-of select="mioga:gettext('Import from other instance')"/></legend>
						<div id="EditImportBoxContent" class="importbox">
						</div>
					</fieldset>
				</div>
				<div id="NoCreateMessage" style="display:none">
					<p><xsl:value-of select="mioga:gettext('Creation is not permitted. Please contact LDAP directory adminstrator.')"/></p>
				</div>
				<xsl:if test="(//UserRights/Administration = 1 or //UserRights/Write = 1) and ((@CanImport=1 or @CanModify=1) or @SuperAdmin=1)">
					<ul class="button_list">
						<li id="edit_user_validate_button"><button class="button" onClick="return false">OK</button></li>
						<xsl:if test="@SuperAdmin=1">
							<li id="edit_user_delete_button" style="display:none"><a class="button" onclick="ConfirmDeleteUserFromDirectory (edit_form); return (false);"><xsl:value-of select="mioga:gettext ('Delete user from all Mioga instances')"/></a></li>
						</xsl:if>
					</ul>
				</xsl:if>
			</div>
		</div>

		<div dojoType="dijit.layout.ContentPane" region="center" style="margin:0;padding:0;" splitter="false">
			<xsl:variable name="ident"><xsl:value-of select="Grid[@name='users']/ident" /></xsl:variable>
			<xsl:variable name="grid_id">grid_<xsl:value-of select="$ident" /></xsl:variable>
			<xsl:variable name="store_id">store_<xsl:value-of select="$ident" /></xsl:variable>

			<xsl:apply-templates select="Grid[@name='users']">
				<xsl:with-param name="ident" select="$ident" />
				<xsl:with-param name="grid_id" select="$grid_id" />
				<xsl:with-param name="store_id" select="$store_id" />
				<xsl:with-param name="default_sort_field">lastname</xsl:with-param>
				<xsl:with-param name="store_prefs">StorePreferences</xsl:with-param>
					<xsl:with-param name="dojo_i18n_bundle">bottin</xsl:with-param>
			</xsl:apply-templates>
		</div>
	</div>

	<!-- Directory search dialog -->
	<div dojoType="dijit.Dialog" id="DirectorySearchDialog" title="Search through directory">
		<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Search through directory')" /></xsl:attribute>
		<div dojoType="dijit.form.Form" id="directory_search_form" jsId="directory_search_form" class="form" encType="multipart/form-data" action="" method="">
			<script type="dojo/method" event="validate">
				ValidateDirectorySearch ();	
				dijit.byId("grid_users").update();
				return false;
			</script>

			<div id="DirectorySearchForm" name="DirectorySearchForm">
				<fieldset>
					<legend><xsl:value-of select="mioga:gettext('User attributes')" /></legend>
					<div class="form-item">
						<label for="firstname"><xsl:value-of select="mioga:gettext('Firstname')" /></label>
						<input class="miogaInput" dojoType="dijit.form.TextBox" id="directory_search_firstname"  name="firstname" value="" />
					</div>
					<div class="form-item">
						<label for="lastname"><xsl:value-of select="mioga:gettext('Lastname')" /></label>
						<input class="miogaInput" dojoType="dijit.form.TextBox" id="directory_search_lastname"  name="lastname" value="" />
					</div>
					<div class="form-item">
						<label for="email"><xsl:value-of select="mioga:gettext('Email')" /></label>
						<input class="miogaInput" dojoType="dijit.form.TextBox" id="directory_search_email"  name="email" value="" />
					</div>
				</fieldset>
			</div>

			<ul class="button_list">
				<li><a class="button" onclick="directory_search_form.validate();"><xsl:value-of select="mioga:gettext ('Search')"/></a></li>
			</ul>
		</div>
	</div>

	<xsl:if test="@SuperAdmin=1">
		<script type="text/javascript">
			var please_check_box = "<xsl:value-of select="mioga:gettext ('Please check the box to confirm you wish to delete the user.')"/>";
		</script>
		<!-- Deletion confirm dialog -->
		<div dojoType="dijit.Dialog" id="DeletionConfirmDialog">
			<xsl:attribute name="title"><xsl:value-of select="mioga:gettext ('Please confirm this deletion')"/></xsl:attribute>
			<p class="warning"><xsl:copy-of select="mioga:gettext ('This operation will delete the user from ALL Mioga instances. Is that really what you want ?')"/></p>
			<p>
				<input type="checkbox" id="checkbox_confirm_deletion" name="confirm_deletion"/><label for="checkbox_confirm_deletion"><xsl:value-of select="mioga:gettext ('Yes, I want to delete this user from ALL Mioga instances.')"/></label>
			</p>
			<ul class="button_list">
				<li><a class="button" id="decline_deletion"><xsl:value-of select="mioga:gettext ('No, do not delete this user')"/></a></li>
				<li><a class="button" id="confirm_deletion"><xsl:value-of select="mioga:gettext ('Yes, proceed deletion from ALL instances')"/></a></li>
			</ul>
		</div>
	</xsl:if>
</xsl:template>

<xsl:template match="ImportUsers" mode="head">
	<xsl:apply-templates select="UploadForm|Check|ProcessContents" mode="head"/>
</xsl:template>

<xsl:template match="ImportUsers">
	<xsl:apply-templates select="UploadForm|Check|ProcessContents"/>
</xsl:template>

<xsl:template match="ImportUsers/UploadForm" mode="head">
	<xsl:call-template name="jquery-js"/>
</xsl:template>

<xsl:template match="ImportUsers/UploadForm">
	<div class="dialog import-users">
		<div class="sbox import-users">
			<h2 class="title_bg_color"><xsl:value-of select="mioga:gettext ('Import Users')"/></h2>
			<div class="content">
				<form id="upload_form" jsId="upload_form" class="form" enctype="multipart/form-data" action="ImportUsers" method="POST">
					<input type="hidden" id="instance" name="instance"><xsl:attribute name="value"><xsl:value-of select="Instance"/></xsl:attribute></input>
					<div id="UploadForm">
						<fieldset>
							<legend><xsl:value-of select="mioga:gettext('Import parameters')"/></legend>
							<div dojoType="dijit.layout.ContentPane" class="form-item" id="UploadFileSelector">
								<label for="file"><xsl:value-of select="mioga:gettext('CSV File')"/></label>
								<input type="file" id="file" name="file"/>
							</div>
							<div class="form-item" id="ImportComboBox">
								<label for="mode"><xsl:value-of select="mioga:gettext('Import Mode')"/></label>
								<select id="mode" name="mode">
									<option value="add"><xsl:value-of select="mioga:gettext('Add Users')"/></option>
									<option value="update"><xsl:value-of select="mioga:gettext('Update Users')"/></option>
								</select>
							</div>
						</fieldset>
						<p class="processing-label"><xsl:value-of select="mioga:gettext ('Processing...')"/></p>
						<ul class="button_list">
							<li>
								<button type="submit" id="import" class="button">
									<xsl:attribute name="onclick">
										jQuery ('form').submit ();
										jQuery ('input, select, a').prop ('disabled', true);
										jQuery ('button').hide ();
										jQuery ('p.processing-label').show ();
										return (false);
									</xsl:attribute>
									<xsl:value-of select="mioga:gettext('Continue')"/>
								</button>
							</li>
							<li>
								<a href="DisplayUsers" class="button cancel">
									<xsl:value-of select="mioga:gettext('Cancel')"/>
								</a>
							</li>
						</ul>
					</div>
				</form>
			</div>
		</div>
	</div>
</xsl:template>

<xsl:template match="ImportUsers/Check" mode="head">
	<xsl:call-template name="jquery-js"/>
</xsl:template>

<xsl:template match="ImportUsers/Check">
	<div class="dialog import-users">
		<div class="sbox import-users">
			<h2 class="title_bg_color"><xsl:value-of select="mioga:gettext ('Import Users')"/></h2>
			<div class="content">
				<form id="upload_form" jsId="upload_form" class="form" enctype="multipart/form-data" action="ImportUsers" method="POST">
					<input type="hidden" id="import" name="import" value="1"/>
					<xsl:if test="Import">
						<fieldset>
							<legend><xsl:value-of select="mioga:gettext('Import from other instance')"/></legend>
							<div class="importbox">
								<ul class="import-helper">
									<li><a href="#" onclick="ImportSelectAll ();"><xsl:value-of select="mioga:gettext('All')"/></a></li>
									<li><a href="#" onclick="ImportSelectNone ();"><xsl:value-of select="mioga:gettext('None')"/></a></li>
								</ul>
								<xsl:apply-templates select="Import"/>
							</div>
						</fieldset>
					</xsl:if>
					<fieldset>
						<legend><xsl:value-of select="mioga:gettext('Messages')"/></legend>
						<div class="messagebox">
							<xsl:apply-templates select="*[not(self::Import)]"/>
						</div>
					</fieldset>
					<p class="processing-label"><xsl:value-of select="mioga:gettext ('Processing...')"/></p>
					<ul class="button_list">
						<li>
							<button type="submit" id="import" class="button">
								<xsl:attribute name="onclick">
									jQuery ('form').submit ();
									jQuery ('input, select, a').prop ('disabled', true);
									jQuery ('button').hide ();
									jQuery ('p.processing-label').show ();
									return (false);
								</xsl:attribute>
								<xsl:value-of select="mioga:gettext('Continue')"/>
							</button>
							</li>
							<li>
							<a href="DisplayUsers" class="button cancel">
								<xsl:value-of select="mioga:gettext('Cancel')"/>
							</a>
						</li>
					</ul>
				</form>
			</div>
		</div>
	</div>
</xsl:template>

<xsl:template match="ImportUsers/ProcessContents" mode="head">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/bottin-print.css" media="print" />
</xsl:template>

<xsl:template match="ImportUsers/ProcessContents">
	<div class="dialog import-users">
		<div class="sbox import-users">
			<h2 class="title_bg_color"><xsl:value-of select="mioga:gettext ('Import Users')"/></h2>
			<div class="content messagebox">
				<xsl:apply-templates/>
				<ul class="button_list">
					<li>
						<a class="button" href="DisplayUsers"><xsl:value-of select="mioga:gettext ('Finish')"/></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</xsl:template>

<xsl:template match="line">
	<div class="linenr">
		<xsl:value-of select="mioga:gettext ('Line')"/>&#160;<xsl:value-of select="."/>
	</div>
</xsl:template>

<xsl:template match="NotProcessed">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:value-of select="mioga:gettext('Has not been processed')" />
		</div>
	</div>
</xsl:template>

<xsl:template match="ProcessUpdate">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:value-of select="mioga:gettext('Has been updated')" />
		</div>
	</div>
</xsl:template>

<xsl:template match="ProcessCreation">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:if test="error">
				<p class="error"><xsl:value-of select="error"/></p>
			</xsl:if>
			<xsl:if test="success">
				<xsl:value-of select="mioga:gettext('Has been created')" />
			</xsl:if>
		</div>
	</div>
</xsl:template>

<xsl:template match="ProcessImport">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:if test="error">
				<p class="error"><xsl:value-of select="error"/></p>
			</xsl:if>
			<xsl:if test="success">
				<xsl:value-of select="mioga:gettext('Has been imported from another instance')" />
			</xsl:if>
		</div>
	</div>
</xsl:template>

<xsl:template match="Modify">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:value-of select="mioga:gettext('Will be updated')" />
		</div>
	</div>
</xsl:template>

<xsl:template match="Create">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:value-of select="mioga:gettext('Will be created')" />
		</div>
	</div>
</xsl:template>

<xsl:template match="CreationDenied">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:value-of select="mioga:gettext('Does not exist and can not be created. Please contact LDAP directory administrator.')" />
		</div>
	</div>
</xsl:template>

<xsl:template match="DoesNotExist">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:value-of select="mioga:gettext('Does not exist.')" />
		</div>
	</div>
</xsl:template>

<xsl:template match="IdentAlreadyExists">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:value-of select="mioga:gettext('Ident already exists in current instance.')" />
		</div>
	</div>
</xsl:template>

<xsl:template match="AlreadyExists">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:value-of select="mioga:gettext('Already exists in current instance as')" />&#160;<xsl:value-of select="match"/>
		</div>
	</div>
</xsl:template>

<xsl:template match="Import">
	<div class="importboxitem">
		<div class="importcheckbox">
			<input type="checkbox">
				<xsl:attribute name="id">import-line[<xsl:value-of select="./line"/>]</xsl:attribute>
				<xsl:attribute name="name">import-line[<xsl:value-of select="./line"/>]</xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="./dn"/></xsl:attribute>
			</input>
		</div>
		<div class="importtext">
			<xsl:attribute name="onClick">document.getElementById('import-line[<xsl:value-of select="./line"/>]').checked=!document.getElementById('import-line[<xsl:value-of select="./line"/>]').checked</xsl:attribute>
			<xsl:apply-templates select="line"/>
			<div class="original">
				<xsl:value-of select="user"/>
			</div>
			<div class="destination">
				<xsl:value-of select="mioga:gettext('Already exists in another instance as')" />&#160;<xsl:value-of select="match"/>
			</div>
			<xsl:if test="source_instance">
				<ul class="instance-list">
					<li><xsl:value-of select="mioga:gettext ('The user is already member of the following instances: ')"/></li>
					<xsl:for-each select="source_instance">
						<li><xsl:value-of select="."/></li>
					</xsl:for-each>
				</ul>
			</xsl:if>
		</div>
	</div>
</xsl:template>

<xsl:template match="WrongSyntax">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<p class="error"><xsl:value-of select="mioga:gettext('The syntax of this line is incorrect, it will be ignored.')" /></p>
		</div>
	</div>
</xsl:template>

<xsl:template match="IdentInUse">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<p class="error"><xsl:value-of select="mioga:gettext('This user can not be added as his ident is already used.')" /></p>
		</div>
	</div>
</xsl:template>

<xsl:template match="CannotAdd">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:value-of select="mioga:gettext('Can not add user to directory.')" />
		</div>
	</div>
</xsl:template>

<xsl:template match="CannotUpdate">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:value-of select="mioga:gettext('Can not update user into directory.')" />
		</div>
	</div>
</xsl:template>

<xsl:template match="EmailAlreadyExists">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:value-of select="mioga:gettext('Email already exists in current instance.')" />
		</div>
	</div>
</xsl:template>

<xsl:template match="CanBeProcessed">
	<div class="messageboxitem">
		<xsl:apply-templates select="line"/>
		<div class="element">
			<xsl:value-of select="user"/>
		</div>
		<div class="message">
			<xsl:value-of select="mioga:gettext('Can be processed')" />
		</div>
	</div>
</xsl:template>

<!-- Info image and associated tooltip -->
<xsl:template name="InfoMessage">
	<xsl:param name="id"/>
	<xsl:param name="title"/>
	<xsl:param name="message"/>

	<div class="info">
		<img id="{$id}" src="{$theme_uri}/images/16x16/emblems/emblem-info.png"/>
		<div dojoType="dijit.Tooltip" connectId="{$id}">
			<h1><xsl:copy-of select="$title"/></h1>
			<div>
				<xsl:copy-of select="$message"/>
			</div>
		</div>
	</div>
</xsl:template>

<xsl:template match="LdapAccessError" mode="head">
</xsl:template>

<xsl:template match="LdapAccessError">
	<div class="error-page">
		<div class="sbox border_color">
			<h2 class="title_bg_color"><xsl:value-of select="mioga:gettext ('Error')"/></h2>
			<p class="error"><xsl:value-of select="mioga:gettext ('LDAP connection failed. Please check instance configuration or contact LDAP directory administrator.')"/></p>
		</div>
	</div>
</xsl:template>

</xsl:stylesheet>
