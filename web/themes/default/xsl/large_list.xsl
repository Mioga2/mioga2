<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<!--
     LargeList templates.
     
     paramaters : 
     bgindex : pages index background color. (default $mioga-list-title-bg-color (base.xsl))

     fgindex : pages index foreground color. (default $mioga-list-title-text-color (base.xsl))

     topindex :    1 => display a page index on the top of the list.
                   0 => don't display a page index on the top of the list.
                   (default 1)

     bottomindex : 1 => display a page index on the bottom of the list.
                   0 => don't display a page index on the bottom of the list.
                   (default 1)

     bgcolor :     List background color.

     tablewidth :  width of the list. (default 100%)
     
     displaytitle : Display the liste title (default 1) 

     method_context : name of the calling method

     arg_context : additionnal arguments passed to method_context (& separated list)

     no_form : Don't create form (when the large list is already included in a form)
     -->

<xsl:template name="LargeList">
  <xsl:param name="bgindex" select="$mioga-list-title-bg-color"/>
  <xsl:param name="fgindex" select="$mioga-list-title-text-color"/>
  <xsl:param name="topindex">1</xsl:param>
  <xsl:param name="bottomindex">1</xsl:param>
  <xsl:param name="lppinbottomindex">0</xsl:param>
  <xsl:param name="bgcolor" select="$mioga-list-title-bg-color"/>
  <xsl:param name="tablewidth">100%</xsl:param>
  <xsl:param name="displaytitle">1</xsl:param>
  <xsl:param name="method_context"/>
  <xsl:param name="arg_context"></xsl:param>
  <xsl:param name="no_form">0</xsl:param>
  <xsl:param name="no-lpp">0</xsl:param>

  <!-- generate top index -->  
  
  <table cellpadding="0" cellspacing="0" class="ll-outer">
    <xsl:if test="count(LargeListIndex/index)!=0 and $topindex=1">
      <tr>
        <td colspan="2">
          <xsl:call-template name="ll_index">
            <xsl:with-param name="bgindex" select="$bgindex"/>
            <xsl:with-param name="fgindex" select="$fgindex"/>
            <xsl:with-param name="method_context" select="$method_context"/>
            <xsl:with-param name="arg_context" select="$arg_context"/>
         </xsl:call-template>
        </td>
      </tr>
    </xsl:if>
    <tr><td colspan="2">
    
    <!-- Display List -->
    <table border="0" cellpadding="0" cellspacing="0" class="large-list">
      <tr>
        <td>
          <table class="list" border="0" cellspacing="1" cellpadding="3">
            <xsl:if test="$displaytitle=1">
            <tr class="list-title">
              <xsl:apply-templates select="."  mode="list-title"/>
            </tr>
            </xsl:if>
            
            <xsl:apply-templates select="."  mode="list-body">
                <xsl:with-param name="no-lpp" select="$no-lpp"/>
            </xsl:apply-templates>
          </table>
        </td>
      </tr>
    </table>
    
    </td></tr>

    <!-- generate bottom index -->
    <xsl:if test="$bottomindex=1">
      <tr>
          <xsl:if test="$no_form=0">
              <xsl:text disable-output-escaping="yes">&lt;form action="</xsl:text><xsl:value-of select="$method_context"/><xsl:text disable-output-escaping="yes">" name="</xsl:text><xsl:value-of select="name(/*)"/>Sll<xsl:text disable-output-escaping="yes">" &gt;</xsl:text>
              <input type="hidden" name="pagenb_{./List/@name}"  value=""/>
          </xsl:if>
          
          <xsl:call-template name="gen_input_args">
              <xsl:with-param name="arg_context" select="$arg_context"/>
          </xsl:call-template>
          
          <input type="hidden" name="page" value="{LargeListIndex/@current_index}"/>

          <xsl:if test="$lppinbottomindex=1">
              <th align="left">
                  <xsl:call-template name="LinePerPage">
                      <xsl:with-param name="bgindex" select="$bgindex"/>
                      <xsl:with-param name="fgindex" select="$fgindex"/>
                      <xsl:with-param name="method_context" select="$method_context"/>
                      <xsl:with-param name="arg_context" select="$arg_context"/>
                      <xsl:with-param name="listname" select="List/@name"/>
                  </xsl:call-template>
              </th>
          </xsl:if>
          
       
          <xsl:if test="count(LargeListIndex/index)>0">
              <td align="left" width="100%">
                 <xsl:if test="$lppinbottomindex=0">
                     <xsl:attribute name="colspan">2</xsl:attribute>
                 </xsl:if>

                 <xsl:call-template name="ll_index">
                      <xsl:with-param name="bgindex">
                          <xsl:choose>
                              <xsl:when test="$lppinbottomindex=0"><xsl:value-of select="$mioga-list-even-row-bg-color"/></xsl:when>
                              <xsl:otherwise><xsl:value-of select="$bgindex"/></xsl:otherwise>
                          </xsl:choose>
                      </xsl:with-param>
                      <xsl:with-param name="fgindex" select="$fgindex"/>
                      <xsl:with-param name="method_context" select="$method_context"/>
                      <xsl:with-param name="arg_context" select="$arg_context"/>
                  </xsl:call-template>
              </td>
          </xsl:if>

          <xsl:if test="$no_form=0">
              <xsl:text disable-output-escaping="yes">
                  &lt;/form&gt;
              </xsl:text>
          </xsl:if>
          
      </tr>
     
    </xsl:if>
  </table>


</xsl:template>


<xsl:template name="LinePerPage">
    <xsl:param name="bgindex" select="$mioga-list-title-bg-color"/>
    <xsl:param name="fgindex" select="$mioga-list-title-text-color"/>
    <xsl:param name="method_context"/>
    <xsl:param name="arg_context"/>  
    <xsl:param name="curval" select="List/@nb_elem_per_page"/>
    <xsl:param name="listname" select="ancestor-or-self::List[1]/@name"/>
    
    <table>
        <tr>
            <td class="borderless" nowrap="1" >
                <b>
                    <span class="mioga-list-title">
                        <xsl:call-template name="line_per_page"/>
                    </span>
                </b>
            </td>
            
            <td class="borderless">
                <input type="text" size="3" maxlength="3" name="nb_elem_per_page_{$listname}" value="{$curval}"/>
            </td>
            
            <td class="borderless">
                <xsl:call-template name="ok-form-button">
                    <xsl:with-param name="name">act_change_elem_per_page</xsl:with-param>
                    <xsl:with-param name="formname" select="$method_context"/>
                </xsl:call-template>
            </td>
        </tr>
    </table>
</xsl:template>



<xsl:template name="gen_input_args">
  <xsl:param name="arg_context"/>

  <xsl:if test="$arg_context!=''">
    
    <xsl:variable name="var_name" select="substring-before($arg_context, '=')"/>
    <xsl:variable name="var_value">
      <xsl:choose>
        <xsl:when test="contains($arg_context, '&amp;')">
          <xsl:value-of select="substring-before(substring-after($arg_context, '='), '&amp;')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="substring-after($arg_context, '=')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:if test="starts-with($var_name, 'nb_elem_per_page') = false">
      <input type="hidden" name="{$var_name}" value="{$var_value}"/>
    </xsl:if>

    <xsl:call-template name="gen_input_args">
      <xsl:with-param name="arg_context" select="substring-after($arg_context, '&amp;')"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>


<!-- private templates -->

<!--
      Index
  -->

<xsl:template name="index-button">
    <xsl:param name="pagenb"/>
    <xsl:param name="value" select="$pagenb"/>
    <xsl:param name="callback"/>
    <xsl:param name="listname" select="../../List/@name"/>

    <xsl:variable name="onclick">submitForm("<xsl:value-of select="$callback"/>Sll", "pagenb_<xsl:value-of select="$listname"/>", "<xsl:value-of select="$pagenb"/>")</xsl:variable>
    <a href="#" onclick="{$onclick}">
        <b>
            <xsl:value-of select="$value"/>
        </b>
    </a>
</xsl:template>


<xsl:template name="ll_index">
  <xsl:param name="bgindex" select="$mioga-list-title-bg-color"/>
  <xsl:param name="fgindex" select="$mioga-list-title-text-color"/>
  <xsl:param name="method_context"/>
  <xsl:param name="arg_context"/>
            
  <xsl:choose>
  <xsl:when test="count(LargeListIndex/index)>0">
    
  <table class="ll-239" cellspacing="0" cellpadding="4">
    <tr>
      <td nowrap="1"><b><span class="mioga-list-title"><xsl:call-template name="pages"/></span></b></td>

      <xsl:if test="LargeListIndex/index[1] != 1">
        <td nowrap="1">
            <xsl:call-template name="index-button">
                <xsl:with-param name="pagenb" select="LargeListIndex/@current_index - LargeListIndex/@max_size"/>
                <xsl:with-param name="callback" select="name(/*)"/>
                <xsl:with-param name="value">
                    <xsl:call-template name="previous_page"/>                    
                </xsl:with-param>
                <xsl:with-param name="listname" select="List/@name"/>
            </xsl:call-template>
            
        </td>
      </xsl:if>
      
      <xsl:for-each select="LargeListIndex/index">
          <td nowrap="1">
              <xsl:if test=".=../@current_index">
                  <b><span class="mioga-list-title">[<xsl:value-of select="."/>]</span></b>
              </xsl:if>
              <xsl:if test=". != ../@current_index">
                  <xsl:call-template name="index-button">
                      <xsl:with-param name="callback" select="name(/*)"/>
                      <xsl:with-param name="pagenb" select="."/>
                  </xsl:call-template>
                  
              </xsl:if>
          </td>
      </xsl:for-each>
      
      <xsl:if test="LargeListIndex/index[position()=last()] != LargeListIndex/@size">
          <td nowrap="1">
              <xsl:call-template name="index-button">
                  <xsl:with-param name="callback" select="name(/*)"/>
                  <xsl:with-param name="pagenb" select="LargeListIndex/@current_index + LargeListIndex/@max_size"/>
                  <xsl:with-param name="value">
                      <xsl:call-template name="next_page"/>
                  </xsl:with-param>
                  <xsl:with-param name="listname" select="List/@name"/>
              </xsl:call-template>
         </td>
     </xsl:if>
     <td width="100%">&#160;</td>
      
    </tr>
  </table>

  </xsl:when>
  <xsl:otherwise>
    &#160;
  </xsl:otherwise>
  </xsl:choose>
</xsl:template>
  

  <!-- ====================
       == FR TRANSLATION ==   
       ==================== -->

  <xsl:template name="line_per_page"><xsl:value-of select="mioga:gettext('Lines per page:')"/>&#160;</xsl:template>
  <xsl:template name="previous_page">&lt;&lt;&#160;<xsl:value-of select="mioga:gettext('previous')"/></xsl:template>
  <xsl:template name="next_page"><xsl:value-of select="mioga:gettext('next')"/>&#160;&gt;&gt;</xsl:template>
  <xsl:template name="pages"><xsl:value-of select="mioga:gettext('Pages:')"/>&#160; </xsl:template>

</xsl:stylesheet>
