<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<xsl:output method="html"/>

<!-- *********************************************************************** -->
<!-- * Translation Templates                                               * -->
<!-- *********************************************************************** -->

<xsl:template name="organizerTranslation">
  <xsl:param name="type"/>

  <xsl:choose>
    <xsl:when test="$type='private-task'"><xsl:value-of select="mioga:gettext('Private task')"/></xsl:when>
    <xsl:when test="$type='contact'"><xsl:value-of select="mioga:gettext('Contact:')"/></xsl:when>
    <xsl:when test="$type='user-organizer'"><xsl:value-of select='mioga:gettext("User&apos;s schedule")'/>&#160;</xsl:when>
    <xsl:when test="$type='preferences'"><xsl:value-of select="mioga:gettext('Preferences')"/></xsl:when>
    <xsl:when test="$type='export'"><xsl:value-of select="mioga:gettext('Export tasks')"/></xsl:when>
    <xsl:when test="$type='goto'"><xsl:value-of select="mioga:gettext('Go to:')"/></xsl:when>
    <xsl:when test="$type='today'"><xsl:value-of select="mioga:gettext('Today')"/></xsl:when>
    <xsl:when test="$type='other-users'"><xsl:value-of select="mioga:gettext('Other users')"/></xsl:when>
    <xsl:when test="$type='other-groups'"><xsl:value-of select="mioga:gettext('Other groups')"/></xsl:when>
    <xsl:when test="$type='other-resources'"><xsl:value-of select="mioga:gettext('Other resources')"/></xsl:when>
    <xsl:when test="$type='none'"><xsl:value-of select="mioga:gettext('None')"/></xsl:when>
    <xsl:when test="$type='todo'"><xsl:value-of select="mioga:gettext('Todo')"/></xsl:when>
    <xsl:when test="$type='requested-meeting'"><xsl:value-of select="mioga:gettext('Appointment proposals')"/></xsl:when>
    <xsl:when test="$type='private-meeting'"><xsl:value-of select="mioga:gettext('Private appointment')"/></xsl:when>
    <xsl:when test="$type='label'"><xsl:value-of select="mioga:gettext('Title')"/></xsl:when>
    <xsl:when test="$type='category'"><xsl:value-of select="mioga:gettext('Category')"/></xsl:when>
    <xsl:when test="$type='undefined'"><xsl:value-of select="mioga:gettext('Undefined')"/></xsl:when>
    <xsl:when test="$type='date'"><xsl:value-of select="mioga:gettext('Date')"/></xsl:when>
    <xsl:when test="$type='schedule'"><xsl:value-of select="mioga:gettext('Timetable')"/></xsl:when>
    <xsl:when test="$type='description'"><xsl:value-of select="mioga:gettext('Description')"/></xsl:when>
    <xsl:when test="$type='location'"><xsl:value-of select="mioga:gettext('Location')"/></xsl:when>
    <xsl:when test="$type='location-place'"><xsl:value-of select="mioga:gettext('In the area')"/></xsl:when>
    <xsl:when test="$type='location-external'"><xsl:value-of select="mioga:gettext('On the move')"/></xsl:when>
    <xsl:when test="$type='repetitive-task'"><xsl:value-of select="mioga:gettext('Repetitive task')"/></xsl:when>
    <xsl:when test="$type='periodicity'"><xsl:value-of select="mioga:gettext('Periodicity')"/></xsl:when>
    <xsl:when test="$type='contact-label'"><xsl:value-of select="mioga:gettext('Contact')"/></xsl:when>
    <xsl:when test="$type='date-hour'"><xsl:value-of select="mioga:gettext('Date and time')"/></xsl:when>
    <xsl:when test="$type='priority'"><xsl:value-of select="mioga:gettext('Priority')"/></xsl:when>
    <xsl:when test="$type='task-params'"><xsl:value-of select="mioga:gettext('Task parameters')"/></xsl:when>
    <xsl:when test="$type='schedule-task'"><xsl:value-of select="mioga:gettext('Schedule task')"/></xsl:when>
    <xsl:when test="$type='fill-field'"><xsl:value-of select="mioga:gettext('Please fill in the following fields to note something to do')"/></xsl:when>
    <xsl:when test="$type='interval-to'"><xsl:value-of select="mioga:gettext('to')"/></xsl:when>
    <xsl:when test="$type='team'"><xsl:value-of select="mioga:gettext('Team')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="MiogaTitleTranslation">
  <xsl:choose>
    <xsl:when test="name(.)='CreateStrictOrPeriodicTask'"><xsl:value-of select="mioga:gettext('Creating task')"/></xsl:when>
    <xsl:when test="name(.)='CreateSimplePeriodicTask'"><xsl:value-of select="mioga:gettext('Creating simple periodic task')"/></xsl:when>
    <xsl:when test="name(.)='EditSimplePeriodicTask'"><xsl:value-of select="mioga:gettext('Editing simple periodic task')"/></xsl:when>
    <xsl:when test="name(.)='CreateStrictOrPeriodicTaskPeriodicity'"><xsl:value-of select="mioga:gettext('Creating task')"/></xsl:when>
    <xsl:when test="name(.)='EditStrictTask'"><xsl:value-of select="mioga:gettext('Modifying task')"/></xsl:when>
    <xsl:when test="name(.)='ViewStrictTask'"><xsl:value-of select="mioga:gettext('Viewing task')"/></xsl:when>
    <xsl:when test="name(.)='ViewTodoTask'"><xsl:value-of select="mioga:gettext('Viewing todo task')"/></xsl:when>
    <xsl:when test="name(.)='EditTodoTask'"><xsl:value-of select="mioga:gettext('Viewing todo task')"/></xsl:when>
    <xsl:when test="name(.)='CreateToDoTask'"><xsl:value-of select="mioga:gettext('Creating todo task')"/></xsl:when>
    <xsl:when test="name(.)='EditPeriodicTask'"><xsl:value-of select="mioga:gettext('Editing periodic task')"/></xsl:when>
    <xsl:when test="name(.)='ViewPeriodicTask'"><xsl:value-of select="mioga:gettext('Viewing periodic task')"/></xsl:when>
    <xsl:when test="name(.)='ChangePeriodicTask'"><xsl:value-of select="mioga:gettext('Modifying periodic task')"/></xsl:when>
    <xsl:when test="name(.)='ExportOrganizer'"><xsl:value-of select="mioga:gettext('Export tasks')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<!-- SuccessMessage -->
<xsl:template match="SuccessMessage">
  <xsl:variable name="message">
    <xsl:choose>
      <xsl:when test="name='user_pref'"><xsl:value-of select="mioga:gettext('Preferences modified successfully')"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="mioga:gettext('Category deleted successfully')"/></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
  <xsl:call-template name="SuccessMessage">
    <xsl:with-param name="message" select="$message"/>
    <xsl:with-param name="referer" select="referer"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="createTaskTranslation">
  <xsl:param name="element"/>

  <xsl:choose>
    <xsl:when test="$element = 'a'"> <xsl:value-of select="mioga:gettext('Create task')"/> </xsl:when>
    <xsl:otherwise> <xsl:value-of select="mioga:gettext('Free time')"/> </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- TaskResult -->
<xsl:template match="TaskResult">
    <xsl:variable name="message">
      <xsl:choose>
        <xsl:when test="@action = 'unknown_task'">
          <xsl:value-of select="mioga:gettext('Unknown task (or newly deleted)')"/>
        </xsl:when>
        <xsl:when test="@action = 'create'">
          <xsl:value-of select="mioga:gettext('Creation successfully done')"/>
        </xsl:when>
        <xsl:when test="@action = 'modify'">
          <xsl:value-of select="mioga:gettext('Modification successfully done')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="mioga:gettext('Deletion successfully done')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:call-template name="SuccessMessage">
      <xsl:with-param name="message" select="$message"/>
      <xsl:with-param name="referer" select="@link"/>
    </xsl:call-template>
</xsl:template>

<!-- ConfirmDeletion -->
<xsl:template match="ConfirmDeletion">
  <xsl:variable name="question">
    <xsl:value-of select="mioga:gettext('Are you sure')"/>
    <xsl:choose>
      <xsl:when test="@label = 'planned'"> <xsl:value-of select="mioga:gettext('you want to delete this simple task?')"/></xsl:when>
      <xsl:when test="@label = 'todo'"> <xsl:value-of select="mioga:gettext('to not have to do this anymore?')"/></xsl:when>
      <xsl:when test="@label = 'periodic'"> <xsl:value-of select="mioga:gettext('you want to delete this repetitive task?')"/></xsl:when>
      <xsl:when test="@label = 'occurrence'"> <xsl:value-of select="mioga:gettext('you want to delete this occurrence of repetitive task?')"/></xsl:when>
      <xsl:when test="@label = 'meeting'"> <xsl:value-of select="mioga:gettext('you want to delete this appointment?')"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="@label"/></xsl:otherwise>
    </xsl:choose>           
  </xsl:variable>

  <xsl:call-template name="Confirm">
    <xsl:with-param name="question" select="$question"/>
    <xsl:with-param name="valid_action_uri" select="@method"/>
    <xsl:with-param name="cancel_uri" select="@link"/>
  </xsl:call-template>

</xsl:template>


<xsl:template name="MiogaFormTranslation">

    <xsl:param name="label"/>

    <xsl:choose>
        <xsl:when test="$label='ExportOrganizer'"><xsl:value-of select="mioga:gettext('Export tasks')"/></xsl:when>
    </xsl:choose>
</xsl:template>

<xsl:template name="MiogaReportInputTranslation">
    <xsl:param name="label"/>
    <xsl:call-template name="MiogaFormInputTranslation">
        <xsl:with-param name="label" select="$label"/>
    </xsl:call-template>
</xsl:template>

<xsl:template name="MiogaFormInputTranslation">
    <xsl:param name="label"/>
    
    <xsl:choose>
        <xsl:when test="$label='create-task-field'">
          <xsl:value-of select="mioga:gettext('Please fill in the following fields to create a task')"/>
        </xsl:when>
        <xsl:when test="$label='name'"><xsl:value-of select="mioga:gettext('Title')"/></xsl:when>
        <xsl:when test="$label='category_id'"><xsl:value-of select="mioga:gettext('Category')"/></xsl:when>
        <xsl:when test="$label='date'"><xsl:value-of select="mioga:gettext('Date')"/></xsl:when>
        <xsl:when test="$label='schedule'"><xsl:value-of select="mioga:gettext('Timetable')"/></xsl:when>
        <xsl:when test="$label='description'"><xsl:value-of select="mioga:gettext('Description')"/></xsl:when>
        <xsl:when test="$label='outside'"><xsl:value-of select="mioga:gettext('On the move')"/></xsl:when>
        <xsl:when test="$label='repetitive-task'"><xsl:value-of select="mioga:gettext('Repetitive task')"/></xsl:when>
        <xsl:when test="$label='periodic'"><xsl:value-of select="mioga:gettext('Periodicity')"/></xsl:when>
        <xsl:when test="$label='contact-label'"><xsl:value-of select="mioga:gettext('Contact')"/></xsl:when>
        <xsl:when test="$label='externalcontact'"><xsl:value-of select="mioga:gettext('Potential contact')"/></xsl:when>
        <xsl:when test="$label='first_name'"><xsl:value-of select="mioga:gettext('Firstname')"/></xsl:when>
        <xsl:when test="$label='last_name'"><xsl:value-of select="mioga:gettext('Lastname')"/></xsl:when>
        <xsl:when test="$label='telephone'"><xsl:value-of select="mioga:gettext('Telephone')"/></xsl:when>
        <xsl:when test="$label='fax'"><xsl:value-of select="mioga:gettext('Fax')"/></xsl:when>
        <xsl:when test="$label='email'"><xsl:value-of select="mioga:gettext('Email')"/></xsl:when>
        <xsl:when test="$label='end-date'"><xsl:value-of select="mioga:gettext('Potential end date')"/></xsl:when>
        <xsl:when test="$label='format'"><xsl:value-of select="mioga:gettext('Format')"/></xsl:when>
        <xsl:when test="$label='start_date'"><xsl:value-of select="mioga:gettext('Export from')"/></xsl:when>
        <xsl:when test="$label='other_date'"><xsl:value-of select="mioga:gettext('Other')"/></xsl:when>
        <xsl:when test="$label='contact'"><xsl:value-of select="mioga:gettext('Contact')"/></xsl:when>
        <xsl:when test="$label='date-hour'"><xsl:value-of select="mioga:gettext('Date and time')"/></xsl:when>
        <xsl:when test="$label='convert-todo'"><xsl:value-of select="mioga:gettext('Change to todo task')"/></xsl:when>
        <xsl:when test="$label='convert-strict'"><xsl:value-of select="mioga:gettext('Schedule task')"/></xsl:when>
        <xsl:when test="$label='priority'"><xsl:value-of select="mioga:gettext('Priority')"/></xsl:when>
        <xsl:when test="$label='task-params'"><xsl:value-of select="mioga:gettext('Task parameters')"/></xsl:when>
        <xsl:when test="$label='planify'"><xsl:value-of select="mioga:gettext('Plan')"/></xsl:when>
        <xsl:when test="$label='actions'"><xsl:value-of select="mioga:gettext('Actions')"/></xsl:when>
        <xsl:when test="$label='delete-instance'"><xsl:value-of select="mioga:gettext('Delete this day occurrence')"/></xsl:when>
        <xsl:when test="$label='delete-task'"><xsl:value-of select="mioga:gettext('Delete all occurrences')"/></xsl:when>
        <xsl:when test="$label='edit-task'"><xsl:value-of select="mioga:gettext('Modify task parameters')"/></xsl:when>
        <xsl:when test="$label='startDate'"><xsl:value-of select="mioga:gettext('Start date')"/></xsl:when>
        <xsl:when test="$label='endDate'"><xsl:value-of select="mioga:gettext('End date')"/></xsl:when>
        <xsl:when test="$label='waiting_book'"><xsl:value-of select="mioga:gettext('Reservation')"/></xsl:when>
    </xsl:choose>

</xsl:template>

<xsl:template name="MiogaFormInputSelectTranslation">
    <xsl:param name="label"/>
    <xsl:param name="value"/>
    
    <xsl:choose>
        <xsl:when test="$label='format'">
            <xsl:value-of select="//available_format/format[@ident=$value]"/>
        </xsl:when>
        <xsl:when test="$label='start_date'">
            <xsl:choose>
                <xsl:when test="$value='last_synchro'"><xsl:value-of select="mioga:gettext('From last export')"/></xsl:when>
                <xsl:when test="$value='epoch'"><xsl:value-of select="mioga:gettext('All tasks')"/></xsl:when>
                <xsl:when test="$value='user_date'">
                    <xsl:call-template name="MiogaFormInputSelectComplexItem">
                        <xsl:with-param name="body">
                            <xsl:call-template name="MiogaFormInputDate">
                                <xsl:with-param name="label">other_date</xsl:with-param>
                            </xsl:call-template>
                        </xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:when>
        <xsl:when test="..='default'">
            <xsl:value-of select="mioga:gettext('Not defined')"/>
        </xsl:when>
        <xsl:when test="$label='category_id'"><xsl:value-of select=".."/></xsl:when>
        <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
