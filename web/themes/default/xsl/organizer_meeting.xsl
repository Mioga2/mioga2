<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:output method="html"/>

<xsl:include href="base.xsl" />
<xsl:include href="simple_large_list.xsl" />
<xsl:include href="organizer_base.xsl"/>
<xsl:include href="scriptaculous.xsl"/>
<xsl:include href="inline_message.xsl"/>

<!-- ===============
	root document
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="scriptaculous-js"/>
	<xsl:call-template name="theme-css"/>
</head>
	<xsl:apply-templates />
</html>
</xsl:template>
  
<!-- =================================
     Create Meeting Templates
     ================================= -->

<xsl:template match="CreateMeeting" mode="form-body">
  <tr valign="top">
    <td>
      <table>
        <tr>
          <td>
            <xsl:call-template name="FieldsName">
              <xsl:with-param name="name">name</xsl:with-param>
            </xsl:call-template>
          </td>
          <td><input type="text" name="name" value="{fields/name}"/></td>
        </tr>
        <tr>
          <td>
            <xsl:call-template name="FieldsName">
              <xsl:with-param name="name">category</xsl:with-param>
            </xsl:call-template>
          </td>
          <td>
            <select name="category_id">
              <xsl:for-each select="task_category">
                <option value="{@rowid}">
                  <xsl:if test="../fields/category_id = @rowid">
                    <xsl:attribute name="selected">selected</xsl:attribute>
                  </xsl:if>
                  <xsl:choose>
                    <xsl:when test=".='default'">
                      <xsl:call-template name="omTranslation">
                        <xsl:with-param name="type">undefined</xsl:with-param>
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </option>
              </xsl:for-each>
            </select>
          </td>
        </tr>
        <tr>
          <td>
            <xsl:call-template name="FieldsName">
              <xsl:with-param name="name">description</xsl:with-param>
            </xsl:call-template>
          </td>
          <td>
            <textarea name="description" cols="40" rows="6"><xsl:value-of select="fields/description"/></textarea>
          </td>
        </tr>
        <tr>
          <td>
            <xsl:call-template name="FieldsName">
              <xsl:with-param name="name">duration</xsl:with-param>
            </xsl:call-template>
          </td>
          <td>
            <xsl:call-template name="Task-time">
              <xsl:with-param name="param_hour">duration_hour</xsl:with-param>
              <xsl:with-param name="param_minute">duration_min</xsl:with-param>
              <xsl:with-param name="value_hour" select="fields/duration_hour"/>
              <xsl:with-param name="value_minute" select="fields/duration_min"/>
            </xsl:call-template>
          </td>
        </tr>
      </table>
    </td>
    <td align="left" width="10">
      <hr class="{$mioga-title-color} {$mioga-title-bg-color}" style="height: 100%; border: 0; width: 1px"/>
    </td>
    <td>
      <table class="{$mioga-title-color}" cellpadding="3">
        <tr>
          <td align="left" nowrap="nowrap">
            <xsl:call-template name="FieldsName">
              <xsl:with-param name="name">inv_user_list</xsl:with-param>
            </xsl:call-template>   
          </td>
          <td>
            <xsl:call-template name="add-form-button">
              <xsl:with-param name="name">edit_user_list</xsl:with-param>
            </xsl:call-template>
            <!--<xsl:if test="UserList/SimpleLargeList/List/Row">&#160;<xsl:call-template name="view-form-button">
                 <xsl:with-param name="name">view_user_list</xsl:with-param>
               </xsl:call-template></xsl:if>-->

          </td>
        </tr>
        <tr>
          <td align="center" colspan="2">
            <xsl:choose>
              <xsl:when test="./UserList/SimpleLargeList/List/Row">
                <xsl:apply-templates select="./UserList/SimpleLargeList">
                  <xsl:with-param name="displayaction">0</xsl:with-param>
                  <xsl:with-param name="tablewidth">100%</xsl:with-param>
                  <xsl:with-param name="bottomindex">
                    <xsl:choose>
                      <xsl:when test="./UserList/SimpleLargeList/List/@nb_elems &gt; ./UserList/SimpleLargeList/List/@nb_elem_per_page">1</xsl:when>
                      <xsl:otherwise>0</xsl:otherwise>
                    </xsl:choose>
                  </xsl:with-param>
                  <xsl:with-param name="no_form">1</xsl:with-param>
                </xsl:apply-templates>
              </xsl:when>
              <xsl:otherwise>
                <i>
                  <xsl:call-template name="FieldsName">
                    <xsl:with-param name="name">no_user</xsl:with-param>
                  </xsl:call-template>
                </i>
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </tr>
        <tr><td colspan="2"><img src="{$image_uri}/transparent_fill.gif" border="0" width="1" height="20"/></td></tr>
        <tr>
          <td align="left" nowrap="nowrap">
            <xsl:call-template name="FieldsName">
              <xsl:with-param name="name">inv_resource_list</xsl:with-param>
            </xsl:call-template>   
          </td>
          <td>
            <xsl:call-template name="add-form-button">
              <xsl:with-param name="name">edit_resource_list</xsl:with-param>
            </xsl:call-template>
            <!-- <xsl:if test="ResourceList/SimpleLargeList/List/Row">
                 &#160;<xsl:call-template name="view-form-button">
                 <xsl:with-param name="name">view_resource_list</xsl:with-param>
               </xsl:call-template></xsl:if>
               -->
          </td>
        </tr>
        <tr>
          <td align="center" colspan="2">
            <xsl:choose>
              <xsl:when test="./ResourceList/SimpleLargeList/List/Row">
                <xsl:apply-templates select="./ResourceList/SimpleLargeList">
                  <xsl:with-param name="displayaction">0</xsl:with-param>
                  <xsl:with-param name="tablewidth">100%</xsl:with-param>
                  <xsl:with-param name="bottomindex">
                    <xsl:choose>
                      <xsl:when test="./ResourceList/SimpleLargeList/List/@nb_elems &gt; ./ResourceList/SimpleLargeList/List/@nb_elem_per_page">1</xsl:when>
                      <xsl:otherwise>0</xsl:otherwise>
                    </xsl:choose>
                  </xsl:with-param>
                  <xsl:with-param name="no_form">1</xsl:with-param>
                </xsl:apply-templates>
              </xsl:when>
              <xsl:otherwise>
                <i>
                  <xsl:call-template name="FieldsName">
                    <xsl:with-param name="name">no_resource</xsl:with-param>
                  </xsl:call-template>
                </i>
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  
  <tr>
    <td colspan="3">
      <hr class="{$mioga-title-color} {$mioga-title-bg-color}" style="width: 100%; border: 0; height: 1px"/>
    </td>
  </tr>
  
  <tr class="{$mioga-title-color}">
    <td  colspan="3">
      <xsl:call-template name="omTranslation">
        <xsl:with-param name="type">suggested-dates</xsl:with-param>
      </xsl:call-template>
      
      <xsl:call-template name="add-form-button">
        <xsl:with-param name="name">add_date</xsl:with-param>
      </xsl:call-template>
      <xsl:if test="UserList/SimpleLargeList/List/Row">&#160;
        <xsl:call-template name="search-form-button">
          <xsl:with-param name="name">search_date</xsl:with-param>
        </xsl:call-template>
      </xsl:if>
    </td>
  </tr>

  <tr class="{$mioga-title-color}">
    <td colspan="3"><img src="{$image_uri}/transparent_fill.gif" border="0" width="1" height="10"/></td>
  </tr>
    

  <tr>
    <td align="center" colspan="3">
      <xsl:call-template name="DateList"/>
    </td>
  </tr>

    
    <xsl:if test="(count(fields/date) != 0 or count(fields/date) = 1) and count(UserList/SimpleLargeList/List/Row) != 0">
         
       <tr>
            <td colspan="3">
                <hr class="{$mioga-title-color} {$mioga-title-bg-color}" style="width: 100%; border: 0; height: 1px"/>
            </td>
        </tr>
        
        
        <tr>
            <td align="left" colspan="3">
                <table>
                    <xsl:if test="count(fields/date) != 0">
                        <tr>
                            <td>
                              <xsl:call-template name="omTranslation">
                                <xsl:with-param name="type">suggest-meeting</xsl:with-param>
                              </xsl:call-template>
                            </td>
                            <td>
                                <xsl:call-template name="propose-form-button">
                                    <xsl:with-param name="name">meeting_propose_act</xsl:with-param>
                                </xsl:call-template>
                            </td>
                        </tr>
                    </xsl:if>
                    
                    <xsl:if test="count(fields/date) = 1">
                        <tr>
                            <td>
                              <xsl:call-template name="omTranslation">
                                <xsl:with-param name="type">validate-meeting</xsl:with-param>
                              </xsl:call-template>
                            </td>
                            <td>
                                <xsl:call-template name="reserve-form-button">
                                    <xsl:with-param name="name">meeting_validate_act</xsl:with-param>
                                </xsl:call-template>
                            </td>
                        </tr>
                    </xsl:if>

                </table>
            </td>
        </tr>
    </xsl:if>

</xsl:template>

<xsl:template match="CreateMeeting" mode="form-button">
    <tr>
        <td colspan="3">
            <hr class="{$mioga-title-color} {$mioga-title-bg-color}" style="width: 100%; border: 0; height: 1px"/>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <xsl:call-template name="ok-form-button">
                <xsl:with-param name="name">meeting_ok_act</xsl:with-param>
            </xsl:call-template><xsl:if test="@creation=0">&#160;<xsl:call-template name="delete-form-button">
                <xsl:with-param name="name">delete_meeting</xsl:with-param>
            </xsl:call-template></xsl:if>&#160;<xsl:call-template name="cancel-button">
                <xsl:with-param name="href" select="referer"/>
            </xsl:call-template>
        </td>
    </tr>
</xsl:template>

<xsl:template match="CreateMeeting" mode="arg-checker-field-name">
    <xsl:param name="name"/>

    <xsl:call-template name="FieldsName">
        <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="CreateMeeting">
   <body xsl:use-attribute-sets="body_attr">
     <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>

	<!-- Status message -->
	<xsl:if test="@message != ''">
		<xsl:call-template name="InlineMessage">
			<xsl:with-param name="type" select="@type"/>
			<xsl:with-param name="message" select="@message"/>
		</xsl:call-template>
	</xsl:if>

     <br/>

     <xsl:call-template name="MiogaTitle"/>
        
     <br/>

     <center>
       <xsl:call-template name="arg_errors"/>
       
       <xsl:call-template name="simple-form">
         <xsl:with-param name="name"><xsl:call-template name="omFormTranslation"/></xsl:with-param>
         <xsl:with-param name="action">CreateMeeting</xsl:with-param>
         <xsl:with-param name="method">POST</xsl:with-param>           
         <xsl:with-param name="nbcols">3</xsl:with-param>           
       </xsl:call-template>
     </center>
   </body>
</xsl:template>

<!-- =================================
     Confirm Delete Meeting
     ================================= -->
<xsl:template match="ConfirmMeetingDelete">
  <xsl:call-template name="Confirm">
    <xsl:with-param name="question">
      <xsl:call-template name="confirm-meeting-delete"/>
    </xsl:with-param>

    <xsl:with-param name="valid_action_uri">CreateMeeting?<xsl:value-of select="type"/>=1</xsl:with-param>
    <xsl:with-param name="cancel_uri">ViewMeeting?back_confirm_delete=1</xsl:with-param>
  </xsl:call-template>
</xsl:template>


<!-- =================================
     View Meeting
     ================================= -->

<xsl:template match="ViewMeeting" mode="form-body">
    <tr valign="top">
        <td>
            <table>
                <tr>
                    <td>
                        <xsl:call-template name="FieldsName">
                            <xsl:with-param name="name">name</xsl:with-param>
                        </xsl:call-template>
                    </td>
                    <td><xsl:value-of select="fields/name"/></td>
                </tr>
                
                <tr>
                    <td>
                        <xsl:call-template name="FieldsName">
                            <xsl:with-param name="name">category</xsl:with-param>
                        </xsl:call-template>
                    </td>
                    <td>
                        <xsl:for-each select="task_category">
                            <xsl:if test="../fields/category_id = @rowid">
                                <xsl:choose>
                                    <xsl:when test=".='default'">
                                      <xsl:call-template name="omTranslation">
                                        <xsl:with-param name="type">undefined</xsl:with-param>
                                      </xsl:call-template>
                                    </xsl:when>
                                    <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </xsl:for-each>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <xsl:call-template name="FieldsName">
                            <xsl:with-param name="name">description</xsl:with-param>
                        </xsl:call-template>
                    </td>
                    <td>
                        <xsl:value-of select="fields/description"/>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <xsl:call-template name="FieldsName">
                            <xsl:with-param name="name">duration</xsl:with-param>
                        </xsl:call-template>
                    </td>
                    <td>
                        <xsl:value-of select="fields/duration_hour"/>&#160;h<xsl:if test="fields/duration_min != 0">&#160;<xsl:value-of select="fields/duration_min"/></xsl:if>
                    </td>
                </tr>
            </table>
        </td>

        <td align="left" width="10">
            <hr class="{$mioga-title-color} {$mioga-title-bg-color}" style="height: 100%; border: 0; width: 1px"/>
        </td>
        
        <td>
            <table class="{$mioga-title-color}" cellpadding="3">
                <tr>
                    <td align="left" nowrap="nowrap">
                        <xsl:call-template name="FieldsName">
                            <xsl:with-param name="name">inv_user_list</xsl:with-param>
                        </xsl:call-template>   
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <xsl:choose>
                            <xsl:when test="./UserList/SimpleLargeList/List/Row">
                                <xsl:apply-templates select="./UserList/SimpleLargeList">
                                    <xsl:with-param name="displayaction">0</xsl:with-param>
                                    <xsl:with-param name="tablewidth">100%</xsl:with-param>
                                    <xsl:with-param name="bottomindex">
                                        <xsl:choose>
                                            <xsl:when test="./UserList/SimpleLargeList/List/@nb_elems > ./UserList/SimpleLargeList/List/@nb_elem_per_page">1</xsl:when>
                                            <xsl:otherwise>0</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:with-param>
                                    <xsl:with-param name="no_form">1</xsl:with-param>
                                </xsl:apply-templates>
                            </xsl:when>

                            <xsl:otherwise>
                                <i>
                                    <xsl:call-template name="FieldsName">
                                        <xsl:with-param name="name">no_user</xsl:with-param>
                                    </xsl:call-template>
                                </i>
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                </tr>


                <tr><td colspan="2"><img src="{$image_uri}/transparent_fill.gif" border="0" width="1" height="20"/></td></tr>

                <tr>
                    <td align="left" nowrap="nowrap">
                        <xsl:call-template name="FieldsName">
                            <xsl:with-param name="name">inv_resource_list</xsl:with-param>
                        </xsl:call-template>   
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <xsl:choose>
                            <xsl:when test="./ResourceList/SimpleLargeList/List/Row">
                                <xsl:apply-templates select="./ResourceList/SimpleLargeList">
                                    <xsl:with-param name="displayaction">0</xsl:with-param>
                                    <xsl:with-param name="tablewidth">100%</xsl:with-param>
                                    <xsl:with-param name="bottomindex">
                                        <xsl:choose>
                                            <xsl:when test="./ResourceList/SimpleLargeList/List/@nb_elems > ./ResourceList/SimpleLargeList/List/@nb_elem_per_page">1</xsl:when>
                                            <xsl:otherwise>0</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:with-param>
                                    <xsl:with-param name="no_form">1</xsl:with-param>
                                </xsl:apply-templates>
                            </xsl:when>

                            <xsl:otherwise>
                                <i>
                                    <xsl:call-template name="FieldsName">
                                        <xsl:with-param name="name">no_resource</xsl:with-param>
                                    </xsl:call-template>
                                </i>
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td colspan="3">
            <hr class="{$mioga-title-color} {$mioga-title-bg-color}" style="width: 100%; border: 0; height: 1px"/>
        </td>
    </tr>

    <tr class="{$mioga-title-color}">
        <td  colspan="3">
          <xsl:call-template name="omTranslation">
            <xsl:with-param name="type">suggested-dates</xsl:with-param>
          </xsl:call-template>
        </td>
    </tr>

    <tr class="{$mioga-title-color}">
        <td colspan="3"><img src="{$image_uri}/transparent_fill.gif" border="0" width="1" height="10"/></td>
    </tr>
    

    <tr>
        <td align="center" colspan="3">
            <xsl:call-template name="ViewDateList"/>
        </td>
    </tr>

    
    <xsl:if test="canWrite">
        <tr>
            <td colspan="3">
                <hr class="{$mioga-title-color} {$mioga-title-bg-color}" style="width: 100%; border: 0; height: 1px"/>
            </td>
        </tr>
        
        <tr>
            <td align="left" colspan="3">
                <table>
                    <xsl:if test="count(UserList/SimpleLargeList/List/Row) != 0">
                        <tr>
                            <td>
                              <xsl:call-template name="omTranslation">
                                <xsl:with-param name="type">send-mail</xsl:with-param>
                              </xsl:call-template>
                            </td>
                            <td>
                                <xsl:call-template name="send-button">
                                    <xsl:with-param name="href">mailto:<xsl:for-each select="users/email"><xsl:if test="position() != 1">,</xsl:if><xsl:value-of select="."/></xsl:for-each></xsl:with-param>
                                </xsl:call-template>
                            </td>
                        </tr>
                    </xsl:if>
                    
                    <tr>
                        <td>
                          <xsl:call-template name="omTranslation">
                            <xsl:with-param name="type">create-meeting-params</xsl:with-param>
                          </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="create-form-button">
                                <xsl:with-param name="name">duplicate_act</xsl:with-param>
                            </xsl:call-template>
                        </td>
                    </tr>

                    <tr>
                        <td>
                          <xsl:call-template name="omTranslation">
                            <xsl:with-param name="type">cancel-meeting</xsl:with-param>
                          </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="cancel-button">
                                <xsl:with-param name="href">CreateMeeting?delete_meeting=1</xsl:with-param>
                            </xsl:call-template>
                        </td>
                    </tr>

                    <tr>
                      <td>
                        <xsl:call-template name="omTranslation">
                          <xsl:with-param name="type">cancel-meeting-keep-tasks</xsl:with-param>
                        </xsl:call-template>
                      </td>
                      <td>
                        <xsl:call-template name="delete-button">
                          <xsl:with-param name="href">CreateMeeting?delete_meeting_keep_task=1</xsl:with-param>
                        </xsl:call-template>
                      </td>
                    </tr>
                </table>
            </td>
        </tr>
    </xsl:if>

</xsl:template>

<xsl:template match="ViewMeeting" mode="form-button">
    <tr>
        <td colspan="3">
            <hr class="{$mioga-title-color} {$mioga-title-bg-color}" style="width: 100%; border: 0; height: 1px"/>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <xsl:call-template name="back-button">
                <xsl:with-param name="href" select="referer"/>
            </xsl:call-template>
        </td>
    </tr>
</xsl:template>


<xsl:template match="ViewMeeting">
   <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>

	<!-- Status message -->
	<xsl:if test="@message != ''">
		<xsl:call-template name="InlineMessage">
			<xsl:with-param name="type" select="@type"/>
			<xsl:with-param name="message" select="@message"/>
		</xsl:call-template>
	</xsl:if>

    <xsl:call-template name="MiogaTitle"/>
     
    <br/>
    
    <center>
      <xsl:call-template name="simple-form">
        <xsl:with-param name="name"><xsl:call-template name="omFormTranslation"/></xsl:with-param>
        <xsl:with-param name="action">CreateMeeting</xsl:with-param>
        <xsl:with-param name="method">GET</xsl:with-param>           
      </xsl:call-template>
    </center>
  </body>
</xsl:template>


<!-- =================================
     Selected Date List table
     ================================= -->

<xsl:template name="DateList">
    <xsl:param name="autocommit">0</xsl:param>

    <table>
        <xsl:for-each select="fields/date">
            <tr>
                <td align="center">
                    <xsl:call-template name="Task-day-month-year">
                        <xsl:with-param name="maybe_empty">0</xsl:with-param>
                        <xsl:with-param name="value_day" select="day"/>
                        <xsl:with-param name="param_day">day_<xsl:value-of select="@rowid"/></xsl:with-param>
                        <xsl:with-param name="value_month" select="month"/>
                        <xsl:with-param name="param_month">month_<xsl:value-of select="@rowid"/></xsl:with-param>
                        <xsl:with-param name="value_year" select="year"/>
                        <xsl:with-param name="param_year">year_<xsl:value-of select="@rowid"/></xsl:with-param>
                        <xsl:with-param name="value_startYear" select="year - 1"/>
                        <xsl:with-param name="value_stopYear" select="year + 7"/>
                    </xsl:call-template>
                </td>
                <td>
                    <xsl:call-template name="Task-time">
                        <xsl:with-param name="value_hour" select="hour"/>
                        <xsl:with-param name="param_hour">hour_<xsl:value-of select="@rowid"/></xsl:with-param>
                        <xsl:with-param name="value_minute" select="min"/>
                        <xsl:with-param name="param_minute">min_<xsl:value-of select="@rowid"/></xsl:with-param>
                        <xsl:with-param name="autocommit" select="$autocommit"/>
                    </xsl:call-template>
                </td>
                <td>
                    <input type="image" src="{$image_uri}/16x16/actions/trash-empty.png" border="0" name="del_date_{@rowid}"/>
                </td>
            </tr>
        </xsl:for-each>
    </table>
</xsl:template>


<xsl:template name="ViewDateList">
  <table>
    <xsl:for-each select="fields/date">
      <tr>
        <td align="center">
          <xsl:call-template name="LocalDateTime"/>
        </td>
      </tr>
    </xsl:for-each>
  </table>
</xsl:template>


<!-- =================================
     EditUserList
     ================================= -->

<xsl:template match="UserList|ResourceList|EditUserList|EditResourceList" mode="sll-field-name">
    <xsl:param name="name"/>

    <xsl:call-template name="FieldsName">
        <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>

</xsl:template>

<xsl:template match="UserList|ResourceList|EditUserList|EditResourceList" mode="sll-translate-values">
    <xsl:param name="name"/>
    <xsl:param name="value"/>
    
    <xsl:value-of select="$value"/>
</xsl:template>

<xsl:template match="EditUserList|EditResourceList">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>
    <br/>

    <xsl:call-template name="MiogaTitle" />
    
       <br/>

       <xsl:call-template name="MiogaAction"/>
       
       <br/>
       <br/>
       <br/>
       <br/>

       <center>
           
           <xsl:apply-templates select="SimpleLargeList">
               <xsl:with-param name="displayaction">0</xsl:with-param>
               <xsl:with-param name="tablewidth">90%</xsl:with-param>
           </xsl:apply-templates>
           
           <br/>

           <xsl:call-template name="back-button">
               <xsl:with-param name="href">CreateMeeting?edit_user_list_back=1</xsl:with-param>
           </xsl:call-template>

       </center>
       
   </body>    
</xsl:template>


<!-- =================================
     BuildDateList
     ================================= -->
<xsl:template match="BuildDateList">
  <body xsl:use-attribute-sets="body_attr">
    
    <xsl:variable name="next">
      <xsl:call-template name="omTranslation">
        <xsl:with-param name="type">next</xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="previous">
      <xsl:call-template name="omTranslation">
        <xsl:with-param name="type">previous</xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>
    <br />
      <xsl:call-template name="MiogaTitle"/>
        
      <br/>

        <center>
            <font class="{$mioga-title-color}">
              <xsl:call-template name="omTranslation">
                <xsl:with-param name="type">select-date</xsl:with-param>
              </xsl:call-template>
            </font>

            <br/>

            <table cellpadding="0" cellspacing="0" border="0" class="{$mioga-title-bg-color}">
                <tr>
                  <td bgclass="{$mioga-bg-color}">
                    <a href="BuildDateList?prev_week=1">
                      <img src="{$image_uri}/16x16/actions/go-previous.png" border="0" alt=" {$previous} " width="20" height="15"/>
                    </a>
                  </td>
                    
                    <!--
                    <td class="{$mioga-bg-color}">
                        <xsl:for-each select="Calendar">
                            <xsl:call-template name="table">
                                <xsl:with-param name="table_width">100%</xsl:with-param>
                                <xsl:with-param name="cellspacing">0</xsl:with-param>
                                <xsl:with-param name="cellpadding">1</xsl:with-param>
                            </xsl:call-template>
                        </xsl:for-each>
                    </td>
                    -->

                    <td align="right" class="{$mioga-bg-color}">
                      <a href="BuildDateList?next_week=1">
                        <img src="{$image_uri}/16x16/actions/go-next.png" border="0" alt=" {$next} " width="20" height="15"/>
                      </a>
                    </td>
                </tr>

                <tr>
                  <td colspan="2" class="{$mioga-bg-color}">&#160;</td>
                </tr>
                
                <tr><td colspan="2">
                <table cellpadding="5" cellspacing="1" border="0">
                  <tr class="{$mioga-list-even-row-bg-color}">
                    <td rowspan="2">&#160;</td>
                    <xsl:for-each select="day">
                      <td colspan="4" align="center">
                        <xsl:call-template name="LocalDateAbr"/>
                      </td>
                    </xsl:for-each>
                  </tr>
                  <tr class="{$mioga-title-color} {$mioga-list-odd-row-bg-color}">
                    <xsl:for-each select="day">
                      <xsl:variable name="url">BuildDateList?add_date=1&amp;day=<xsl:value-of select="day"/>&amp;month=<xsl:value-of select="month"/>&amp;year=<xsl:value-of select="year"/></xsl:variable>
                      <td width="30px" align="center"><font size="-2"><a class="{$mioga-title-color}" href="{$url}&amp;zone=1">8-10</a></font></td>
                      <td width="30px" align="center"><font size="-2"><a class="{$mioga-title-color}" href="{$url}&amp;zone=2">10-13</a></font></td>
                      <td width="30px" align="center"><font size="-2"><a class="{$mioga-title-color}" href="{$url}&amp;zone=3">13-16</a></font></td>
                      <td width="30px" align="center"><font size="-2"><a class="{$mioga-title-color}" href="{$url}&amp;zone=4">16-18</a></font></td>
                    </xsl:for-each>                    
                    </tr>


                    <xsl:for-each select="user">
                        <tr>
                            <xsl:choose>
                                <xsl:when test="position() mod 2 = 1">
                                    <xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color" /></xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="class"><xsl:value-of select="$mioga-list-even-row-bg-color" /></xsl:attribute>
                                </xsl:otherwise>
                            </xsl:choose>
                            
                            <td nowrap="nowrap"><xsl:value-of select="firstname"/>&#160;<xsl:value-of select="lastname"/></td>
                            
                            <xsl:call-template name="planning-table"/>
                            
                        </tr>
                    </xsl:for-each>
                    
                    <xsl:if test="resource">
                        <tr>
                            <td colspan="{count(day) * 4}"></td>
                        </tr>
                    </xsl:if>
                    
                    
                    <xsl:variable name="nb_user" select="count(user)"/>
                    
                    <xsl:for-each select="resource">
                        <tr>
                            <xsl:choose>
                                <xsl:when test="($nb_user + position()) mod 2 = 1">
                                    <xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color" /></xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="class"><xsl:value-of select="$mioga-list-even-row-bg-color" /></xsl:attribute>
                                </xsl:otherwise>
                            </xsl:choose>
                            
                            <td nowrap="nowrap"><xsl:value-of select="ident"/></td>
                            
                            <xsl:call-template name="planning-table"/>
                            
                        </tr>
                    </xsl:for-each>
                    
                </table>
            </td></tr>
        </table>
        
        <br/>
        
        <form method="GET" action="BuildDateList" name="BuildDateList">
            <xsl:call-template name="DateList"/>
        </form>
        
        <br/>
        
        <xsl:call-template name="back-button">
            <xsl:with-param name="href">CreateMeeting?search_date_list_back=1</xsl:with-param>
        </xsl:call-template>
     </center>
</body>
</xsl:template>


<xsl:template name="planning-table">

    <xsl:for-each select="task">
        <xsl:if test="@diff != 0">
            <xsl:call-template name="empty-loop-td">
                <xsl:with-param name="nb" select="@diff"/>
            </xsl:call-template>
        </xsl:if>
        
        <td colspan="{@size}" bgcolor="{$conflict_bgcolor}">
            <img src="{$image_uri}/transparent_fill.gif" border="0" width="1" height="1"/>
        </td>
    </xsl:for-each>
    
    <xsl:variable name="nb_zone" select="count(../day) * 4"/>
    <xsl:variable name="filled_zone" select="sum(./task/@diff) + sum(./task/@size)"/>
    
    <xsl:call-template name="empty-loop-td">
        <xsl:with-param name="nb" select="$nb_zone - $filled_zone"/>
    </xsl:call-template>
</xsl:template>

<xsl:template name="empty-loop-td">
    <xsl:param name="nb"/>

    <td><img src="{$image_uri}/transparent_fill.gif" border="0" width="1" height="1"/></td>

    <xsl:if test="$nb > 1">
        <xsl:call-template name="empty-loop-td">
            <xsl:with-param name="nb" select="$nb - 1"/>
        </xsl:call-template>
    </xsl:if>
</xsl:template>


<!-- =================================
     ProposeMeeting
     ================================= -->
<xsl:template match="ProposeMeeting" mode="form-body">
    <tr>
        <td>
            <xsl:call-template name="FieldsName">
                <xsl:with-param name="name">subject</xsl:with-param>
            </xsl:call-template>
        </td>

        <td>
            <input style="width: 100%" type="text" name="subject">
                <xsl:attribute name="value">
                    <xsl:call-template name="MSG-meeting_subject_proposal"/>
                </xsl:attribute>
            </input>
        </td>
    </tr>

    <tr>
        <td colspan="2">
            <textarea name="body" rows="20" cols="80">
                <xsl:call-template name="MSG-meeting_body_proposal"/>
            </textarea>
        </td>
    </tr>
</xsl:template>

<xsl:template match="ProposeMeeting" mode="form-button">
    <tr align="center">
        <td colspan="2">
            <xsl:call-template name="ok-cancel-form-button">
                <xsl:with-param name="name">proposal_send_act</xsl:with-param>
                <xsl:with-param name="referer">CreateMeeting?propose_meeting_back</xsl:with-param>
            </xsl:call-template>
        </td>
    </tr>

</xsl:template>


<xsl:template match="ProposeMeeting">
<body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>
   <br />
     <xsl:call-template name="MiogaTitle"/>
       
     <center class="alone-form">
      <br/>

      <xsl:call-template name="form">
        <xsl:with-param name="name"><xsl:call-template name="omFormTranslation"/></xsl:with-param>
        <xsl:with-param name="action">ProposeMeeting</xsl:with-param>
        <xsl:with-param name="method">POST</xsl:with-param>
      </xsl:call-template>

    </center>
</body>
</xsl:template>

<xsl:template match="EditUserList" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">addUser</xsl:with-param>
    <xsl:with-param name="href">EditUserList?add_user=1</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="EditResourceList" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">addResource</xsl:with-param>
    <xsl:with-param name="href">EditResourceList?add_resource=1</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<xsl:template name="FieldsName">
  <xsl:param name="name"/>
  
  <xsl:choose>
    <xsl:when test="$name = 'name'"><xsl:value-of select="mioga:gettext('Title')"/></xsl:when>
    <xsl:when test="$name = 'category'"><xsl:value-of select="mioga:gettext('Category')"/></xsl:when>
    <xsl:when test="$name = 'description'"><xsl:value-of select="mioga:gettext('Description')"/></xsl:when>
    <xsl:when test="$name = 'duration'"><xsl:value-of select="mioga:gettext('Duration')"/></xsl:when>
    <xsl:when test="$name = 'inv_user_list'"><xsl:value-of select="mioga:gettext('Invited users')"/></xsl:when>
    <xsl:when test="$name = 'inv_resource_list'"><xsl:value-of select="mioga:gettext('Used resources')"/></xsl:when>
    <xsl:when test="$name = 'no_user'"><xsl:value-of select="mioga:gettext('None')"/></xsl:when>
    <xsl:when test="$name = 'no_resource'"><xsl:value-of select="mioga:gettext('None')"/></xsl:when>
    <xsl:when test="$name = 'subject'"><xsl:value-of select="mioga:gettext('Subject')"/></xsl:when>
    <xsl:when test="$name = 'body'">&#160;</xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="mioga2-common-name-translation">
        <xsl:with-param name="name" select="$name"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- =================================
     SuccessMessage
     ================================= -->
<xsl:template match="SuccessMessage">
  <xsl:variable name="message">
    <xsl:choose>
      <xsl:when test="action='propose'"><xsl:value-of select="mioga:gettext('Message sent to invitees')"/></xsl:when>
      <xsl:when test="action='validate'"><xsl:value-of select="mioga:gettext('Appointment has been registered to attendees schedules.')"/></xsl:when>
      <xsl:when test="action='create'"><xsl:value-of select="mioga:gettext('Appointment created successfully')"/></xsl:when>
      <xsl:when test="action='delete'"><xsl:value-of select="mioga:gettext('Appointment deleted successfully')"/></xsl:when>
    </xsl:choose>
  </xsl:variable>
  <xsl:call-template name="SuccessMessage">
    <xsl:with-param name="message" select="$message"/>
    <xsl:with-param name="referer" select="referer"/>
  </xsl:call-template>
</xsl:template>

<!-- organizer- meeting translations -->
<xsl:template name="omTranslation">
  <xsl:param name="type"/>

  <xsl:choose>
    <xsl:when test="$type='undefined'"><xsl:value-of select="mioga:gettext('Undefined')"/></xsl:when>
    <xsl:when test="$type='suggested-dates'"><xsl:value-of select="mioga:gettext('Offered dates for appointment')"/>&#160;&#160;&#160;</xsl:when>
    <xsl:when test="$type='suggest-meeting'"><xsl:value-of select="mioga:gettext('Suggest appointment to various attendees:')"/></xsl:when>
    <xsl:when test="$type='validate-meeting'">
      <xsl:value-of select="mioga:gettext('Confirm date and register appointment in schedule of attendees and resources:')"/>
    </xsl:when>
    <xsl:when test="$type='send-mail'"><xsl:value-of select="mioga:gettext('Send email to attendees:')"/></xsl:when>
    <xsl:when test="$type='create-meeting-params'"><xsl:value-of select="mioga:gettext('Create a new appointment with the same parameters:')"/></xsl:when>
    <xsl:when test="$type='cancel-meeting'"><xsl:value-of select="mioga:gettext('Cancel appointment and destroy associated tasks in attendees schedules:')"/></xsl:when>
    <xsl:when test="$type='cancel-meeting-keep-tasks'">
      <xsl:value-of select="mioga:gettext('Delete appointment offer but keep associated tasks in attendees schedules:')"/>
    </xsl:when>
    <xsl:when test="$type='select-date'"><xsl:value-of select="mioga:gettext('Please select dates for your appointment.')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="CreateMeeting" mode="arg-checker-unknown-args">
  <xsl:param name="name"/>
  <xsl:param name="arg"/>
  
  <xsl:choose>
    <xsl:when test="$name = '__create__'"><xsl:value-of select="mioga:gettext('Error while creating appointment: %s.', string($arg))"/></xsl:when>
    <xsl:when test="$name = '__modify__'"><xsl:value-of select="mioga:gettext('Error while modifying appointment: %s.', string($arg))"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="confirm-meeting-delete">
  <xsl:choose>
    <xsl:when test="type='confirm_delete_meeting_keep_task'"><xsl:value-of select="mioga:gettext('Are you sure you want to delete this appointment offer?')"/></xsl:when>
    <xsl:otherwise><xsl:value-of select="mioga:gettext('Are you sure you want to cancel this appointment?')"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="MiogaTitleTranslation">
  <xsl:choose>
    <xsl:when test="name(.)='CreateMeeting'"><xsl:value-of select="mioga:gettext('Create appointment')"/></xsl:when>
    <xsl:when test="name(.)='ViewMeeting'"><xsl:value-of select="mioga:gettext('View appointment')"/></xsl:when>
    <xsl:when test="name(.)='EditUserList'"><xsl:value-of select="mioga:gettext('Editing users list')"/></xsl:when>
    <xsl:when test="name(.)='EditResourceList'"><xsl:value-of select="mioga:gettext('Editing resources list')"/></xsl:when>
    <xsl:when test="name(.)='BuildDateList'"><xsl:value-of select="mioga:gettext('Selecting appointment dates')"/></xsl:when>
    <xsl:when test="name(.)='ProposeMeeting'"><xsl:value-of select="mioga:gettext('Appointment offer')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="omFormTranslation">
  <xsl:choose>
    <xsl:when test="name(.)='CreateMeeting'"><xsl:value-of select="mioga:gettext('Create appointment:')"/></xsl:when>
    <xsl:when test="name(.)='ProposeMeeting'"></xsl:when>
    <xsl:when test="name(.)='ViewMeeting'"><xsl:value-of select="mioga:gettext('View appointment:')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="MiogaActionTranslation">
  <xsl:param name="label"/>
  <xsl:choose>
    <xsl:when test="$label='addUser'"><xsl:value-of select="mioga:gettext('Add users')"/></xsl:when>
    <xsl:when test="$label='addResource'"><xsl:value-of select="mioga:gettext('Add resources')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<!-- =================================
     MSG-meeting_subject_proposal
     ================================= -->
<xsl:template name="MSG-meeting_subject_proposal">
  <xsl:value-of select="mioga:gettext('[Mioga] Appointment proposal from %s %s', string(me/firstname), string(me/lastname))"/>
</xsl:template>

<!-- =================================
     MSG-meeting_body_proposal
     ================================= -->
<xsl:template name="MSG-meeting_body_proposal">
<xsl:value-of select="mioga:gettext('Hello,')"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:text>&#x0A;</xsl:text>
<xsl:value-of select="mioga:gettext('This is an autogenerated message from Mioga.')"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:value-of select="mioga:gettext('This is an appointment proposal from %s %s.', string(me/firstname), string(me/lastname))"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:text>&#x0A;</xsl:text>

<xsl:value-of select="mioga:gettext('Subject:')"/> <xsl:value-of select="name"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:value-of select="mioga:gettext('Duration: %sh', string(duration/hour))"/><xsl:if test="duration/min!=0"><xsl:value-of select="duration/min"/></xsl:if>
<xsl:text>&#x0A;</xsl:text>

<xsl:value-of select="mioga:gettext('Description:')"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:text>&#x09;</xsl:text>
<xsl:value-of select="description"/>
        
<xsl:text>&#x0A;</xsl:text>
<xsl:text>&#x0A;</xsl:text>
<xsl:value-of select="mioga:gettext('Offered dates:')"/>
    <xsl:for-each select="date">
		<xsl:text>&#x0A;</xsl:text>
		<xsl:text>&#x09;</xsl:text>
        <xsl:call-template name="LocalDateTime"/>
	<xsl:text disable-output-escaping="yes"></xsl:text>
    </xsl:for-each>

<xsl:text>&#x0A;</xsl:text>
<xsl:text>&#x0A;</xsl:text>
<xsl:value-of select="mioga:gettext('Attendees are:')"/>
        
    <xsl:for-each select="invited/user">
		<xsl:text>&#x0A;</xsl:text>
		<xsl:text>&#x09;</xsl:text>
        <xsl:value-of select="firstname"/><xsl:text disable-output-escaping="yes"> </xsl:text><xsl:value-of select="lastname"/>
        <xsl:text disable-output-escaping="yes"></xsl:text>
    </xsl:for-each>
        
<xsl:text>&#x0A;</xsl:text>
<xsl:text>&#x0A;</xsl:text>
<xsl:value-of select="mioga:gettext('Please confirm or undermine this proposal.')"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:text>&#x0A;</xsl:text>
<xsl:value-of select="mioga:gettext('Kind regards,')"/>
<xsl:text>&#x0A;</xsl:text>

<xsl:value-of select="me/firstname"/><xsl:text disable-output-escaping="yes"> </xsl:text><xsl:value-of select="me/lastname"/>
</xsl:template>

</xsl:stylesheet>
