<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
<!--  <xsl:output method="html"/> -->


  <!-- =================================
       MiogaAction
       ================================= -->
  
  <xsl:template name="MiogaBox">
      <xsl:param name="no-title">0</xsl:param>
      <xsl:param name="title"/>
      <xsl:param name="body"/>
      <xsl:param name="wrap">0</xsl:param>
      <xsl:param name="center">0</xsl:param>
      <xsl:param name="width"/>
      <xsl:param name="highlight">0</xsl:param>
      
      <table xsl:use-attribute-sets="mioga-border-table">
		<xsl:if test="$highlight = 1">
			<xsl:attribute name="class">highlight_bg_color</xsl:attribute>
		</xsl:if>
          <xsl:choose>
              <xsl:when test="$wrap=0">
                  <xsl:attribute name="align">left</xsl:attribute>
              </xsl:when>

              <xsl:when test="$center=1">
                  <xsl:attribute name="align">center</xsl:attribute>
              </xsl:when>
          </xsl:choose>
          
          <xsl:if test="$width != ''">
              <xsl:attribute name="width"><xsl:value-of select="$width"/></xsl:attribute>
          </xsl:if>

          <xsl:if test="$no-title != 1">
				<tr>
					<xsl:choose>
						<xsl:when test="$highlight != 1">
							<xsl:attribute name="class">title_bg_color</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="class">highlight_bg_color</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
					<td nowrap="nowrap">
						<p xsl:use-attribute-sets="mioga-box-title">
							<xsl:copy-of select="$title"/>
						</p>
					</td>
				</tr>
          </xsl:if>

          <tr>
              <td>
                  <table xsl:use-attribute-sets="mioga-content-table">
                      <tr class="mioga-box-background">
                          <td>
                            <xsl:choose>
                              <xsl:when test="$body">
                                <xsl:copy-of select="$body"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:apply-templates select="." mode="MiogaBoxBody"/>
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                      </tr>
                  </table>
              </td>
          </tr>
          
      </table>
      
  </xsl:template>


  <xsl:template name="MiogaTable">
      <table xsl:use-attribute-sets="mioga-border-table">
          <tr>
              <td>
                  <table xsl:use-attribute-sets="mioga-content-table">
                      <tr>
                          <xsl:apply-templates select="." mode="mioga-table-header"/>
                      </tr>
                      <xsl:apply-templates select="." mode="mioga-table-body"/>
                  </table>
              </td>
          </tr>
      </table>
  </xsl:template>

</xsl:stylesheet>
