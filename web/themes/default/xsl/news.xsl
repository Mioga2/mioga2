<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>
<xsl:import href="news-parts.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<xsl:include href="tiny_mce.xsl"/>
<xsl:include href="scriptaculous.xsl"/>

<!-- ===============
	Variables
	================ -->

<xsl:variable name="anim_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Animation">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="moder_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Moderation">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="write_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Write">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="read_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Read">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<!--
Headers
-->
<xsl:template name="mioga-css">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/mioga.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{$theme_uri}/css/news.css" media="screen" />
</xsl:template>


<xsl:template match="/">
	<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
    <xsl:call-template name="mioga-js"/>
    <xsl:call-template name="calendar-js"/>
    <xsl:call-template name="favicon"/>
    <xsl:call-template name="title"/>
    <xsl:call-template name="scriptaculous-js"/>
    <xsl:call-template name="tinymce-js"/>
    <xsl:call-template name="tinymce-init-js"/>
	<xsl:call-template name="theme-css"/>
</head>
<body class="news">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">news.html</xsl:with-param>
    </xsl:call-template>
    <xsl:apply-templates />
</body>
</html>
</xsl:template>

<!--
 DisplayMain
-->
<xsl:template match="DisplayMain">
    <xsl:call-template name="DisplayTemplate"/>
</xsl:template>

<!--
 DisplayWaitingNews
-->
<xsl:template match="DisplayWaitingNews">
    <xsl:call-template name="DisplayTemplate"/>
</xsl:template>

<!--
 DisplayDraftNews
-->
<xsl:template match="DisplayDraftNews">
    <xsl:call-template name="DisplayTemplate"/>
</xsl:template>


<xsl:template name="DisplayTemplate">
    <xsl:variable name="page" select="name()"/>
    
    <div id="disable-window" style="display: none;"></div>
    <div id="options-box" class="dialog" style="display: none;"></div>
    <div id="edit-box" class="dialog" style="visibility: hidden;"></div>
    
    <div class="news">
        <xsl:call-template name="Menu">
            <xsl:with-param name="page" select="$page"/>
        </xsl:call-template>
        <div id="news-box">
        <xsl:if test="count(News/NewsItem) = 0">
        	<p class="information">
                <xsl:value-of select="mioga:gettext('There is no news available at the moment.')"/>
                <xsl:if test="$write_right = 1"><xsl:value-of select="mioga:gettext(' To create a new one, click on &quot;Write a news item&quot;.')"/></xsl:if>
            </p>
        </xsl:if>
        
        <xsl:apply-templates select="News">
            <xsl:with-param name="page" select="$page"/>
        </xsl:apply-templates>
        </div>
    </div>

</xsl:template>

<!--
=======================================
 WriteNewsItem
=======================================
-->
<xsl:template match="WriteNewsItem">
    <xsl:call-template name="WriteOrEditNewsItem">
        <xsl:with-param name="action">CreateNews</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<!--
=======================================
 EditNewsItem
=======================================
-->
<xsl:template match="EditNewsItem">
    <xsl:call-template name="WriteOrEditNewsItem">
        <xsl:with-param name="action">UpdateNews</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<!--
==================
 UpdateNewsItem
==================
-->
<xsl:template match="UpdateNewsItem">
    <xsl:call-template name="WriteOrEditNewsItem">
        <xsl:with-param name="action" select="//EditAction"/>
    </xsl:call-template>
</xsl:template>

<!--
=======================================
 Menu template
=======================================
-->
<xsl:template name="Menu">
    <xsl:param name="page"/>
    
    <ul class="hmenu">
        <xsl:if test="count (appnav/group) != 0">
            <li>
                <xsl:call-template name="AppNav_Select"/>
            </li>
        </xsl:if>
        <xsl:if test="$anim_right=1">
            <li><a class="options" href="#"><xsl:attribute name="onclick">new Ajax.Request("DisplayOptions", { evalScripts:true, asynchronous:true }); return false;</xsl:attribute><xsl:value-of select="//Actions/Options"/></a></li>
        </xsl:if>
        <li>
        <xsl:choose>
            <xsl:when test="$page!='DisplayMain' or ($page='DisplayMain' and count(//Archives) &gt; 0)"><a class="documents" href="DisplayMain"><xsl:value-of select="//Actions/MainNews"/></a></xsl:when>
            <xsl:otherwise><span class="current documents"><xsl:value-of select="//Actions/MainNews"/></span></xsl:otherwise>
        </xsl:choose>
        </li>
        <li>
        <xsl:choose>
            <xsl:when test="count(//Archives) = 0"><a class="documents" href="DisplayMain?archive=1"><xsl:value-of select="//Actions/ArchivedNews"/></a></xsl:when>
            <xsl:otherwise><span class="current documents"><xsl:value-of select="//Actions/ArchivedNews"/></span></xsl:otherwise>
        </xsl:choose>
        </li>
        <xsl:if test="$moder_right=1">
            <li>
            <xsl:choose>
                <xsl:when test="$page!='DisplayWaitingNews'"><a class="documents" href="DisplayWaitingNews"><xsl:value-of select="//Actions/WaitingNews"/></a></xsl:when>
                <xsl:otherwise><span class="current documents"><xsl:value-of select="//Actions/WaitingNews"/></span></xsl:otherwise>
            </xsl:choose>
            </li>
        </xsl:if>
        <xsl:if test="$write_right=1">
            <li>
            <xsl:choose>
                <xsl:when test="$page!='DisplayDraftNews'"><a class="drafts" href="DisplayDraftNews"><xsl:value-of select="//Actions/DraftNews"/></a></xsl:when>
                <xsl:otherwise><span class="current drafts"><xsl:value-of select="//Actions/DraftNews"/></span></xsl:otherwise>
            </xsl:choose>
            </li>
            <li><a class="new_news" href="WriteNewsItem?page={$page}"><xsl:value-of select="//Actions/WriteNews"/></a></li>
        </xsl:if>
    </ul>
	<div class="clear-both"></div>
</xsl:template>

</xsl:stylesheet>
