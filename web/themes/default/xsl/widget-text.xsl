<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
<!--  <xsl:output method="html"/> -->

  
  <!-- ==========
       MiogaTitle
       ========== -->

  <xsl:template name="MiogaTitle">
      <xsl:param name="title"></xsl:param>
      <xsl:param name="referer"></xsl:param>
      
      <h4 xsl:use-attribute-sets="mioga-title-attr">
          <xsl:choose>
              <xsl:when test="$title = ''">
                  <xsl:call-template name="MiogaTitleTranslation"/>
              </xsl:when>
              <xsl:otherwise>
                  <xsl:value-of select="$title"/>
              </xsl:otherwise>
          </xsl:choose>
      </h4>
  </xsl:template>
  

  <!-- ============
       MiogaMessage
       ============ -->
  
  <xsl:template name="MiogaMessage">
      <xsl:param name="emphasis">0</xsl:param>
      <xsl:param name="message"/>
      <xsl:param name="align">center</xsl:param>
      
      <p xsl:use-attribute-sets="mioga_message_attr" align="{$align}">
          <xsl:choose>
              <xsl:when test="$emphasis=0">
                  <i><xsl:value-of select="$message"/></i>
              </xsl:when>
              <xsl:otherwise>
                  <b><xsl:value-of select="$message"/></b>
              </xsl:otherwise>
          </xsl:choose>
      </p>
  </xsl:template>

</xsl:stylesheet>
