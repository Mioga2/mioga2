<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
  <xsl:import href="anim_group.xsl"/>

  <xsl:output method="html" indent="yes"/>


  <!-- Tabs Menu description -->
  <xsl:template match="*" mode="MiogaMenuBody">

    <xsl:call-template name="MiogaMenuTab">
      <xsl:with-param name="href">DisplayUsers</xsl:with-param>
      <xsl:with-param name="label">DisplayUsers</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaMenuTab">
      <xsl:with-param name="href">DisplayTeams</xsl:with-param>
      <xsl:with-param name="label">DisplayTeams</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaMenuTab">
      <xsl:with-param name="href">DisplayThemes</xsl:with-param>
      <xsl:with-param name="label">DisplayThemes</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaMenuTab">
      <xsl:with-param name="href">DisplayProfiles</xsl:with-param>
      <xsl:with-param name="label">DisplayProfiles</xsl:with-param>
    </xsl:call-template>

    <xsl:if test="$mioga_context/user/is_autonomous">
      <xsl:call-template name="MiogaMenuTab">
        <xsl:with-param name="href">DisplayApplications</xsl:with-param>
        <xsl:with-param name="label">DisplayApplications</xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!-- DisplayProfiles -->
  <xsl:template match="DisplayProfiles" mode="MiogaActionBody">
    <xsl:if test="count(./is_autonomous) != 0">
      <xsl:call-template name="MiogaActionItem">
        <xsl:with-param name="label">CreateProfile</xsl:with-param>
        <xsl:with-param name="href">CreateProfile</xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>


  <xsl:template match="GroupList">
      <xsl:variable name="current-func" select="$mioga_method"/>
      
      <body xsl:use-attribute-sets="body_attr">
          
          <xsl:call-template name="DisplayAppTitle"/>
          
          <xsl:call-template name="MiogaTitle" />
          
          <div xsl:use-attribute-sets="app_body_attr">
          
              <xsl:apply-templates select="SimpleLargeList">
                  <xsl:with-param name="advancedsearch">1</xsl:with-param>
                  <xsl:with-param name="label" select="name(.)"/>
              </xsl:apply-templates>
              
              <xsl:call-template name="MiogaMessage">
                  <xsl:with-param name="message"><xsl:call-template name="searchResultTranslation" /></xsl:with-param>
              </xsl:call-template>
          </div>
          
      </body>
  </xsl:template>
  
  <xsl:template match="GroupList" mode="sll-translate-values">
      <xsl:param name="name"/>
      <xsl:param name="value"/> 
      <xsl:param name="row"/> 

      <xsl:variable name="href">
          <xsl:choose>
              <xsl:when test="count(link_path) = 0 or link_path = ''"><xsl:value-of select="$private_dav"/>/<xsl:value-of select="$value"/></xsl:when>
              <xsl:otherwise><xsl:value-of select="$bin_uri"/>/<xsl:value-of select="$value"/>/Portal/DisplayPortal?path=<xsl:value-of select="link_path"/></xsl:otherwise>
          </xsl:choose>
      </xsl:variable>
      
      <a href="{$href}" title="{$row/description}" alt="{$row/description}"><xsl:value-of select="$value"/></a>
  </xsl:template>

  <xsl:template match="GroupList" mode="advanced-search-field-content">
      <xsl:param name="name"/>
      <xsl:param name="value"/>
      
      <xsl:call-template name="SllAdvSearchFieldContent">
          <xsl:with-param name="name" select="$name"/>
          <xsl:with-param name="value" select="$value"/>
      </xsl:call-template>
  </xsl:template>
  


</xsl:stylesheet>
