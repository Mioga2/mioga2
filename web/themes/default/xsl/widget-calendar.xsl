<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<!--  <xsl:output method="html"/> -->

<!--
     MiogaCalendar : Display a calendar
     
     - label : identify the title that will be displayed
     -->
<xsl:template name="MiogaCalendar">
    <xsl:param name="label"/>
    
    <xsl:param name="year-value" select="$mioga_context/time/year"/>
    <xsl:param name="month-value" select="$mioga_context/time/month"/>
    <xsl:param name="day-value" select="$mioga_context/time/day"/>

    <xsl:param name="base_href" select="$private_bin_uri"/>

    <tr><td>
    <table xsl:use-attribute-sets="mioga-empty-table" style="border: 1px solid black" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center">
                <table border="0" cellpadding="2" cellspacing="0">
                    <tr>
                        <td>
                            <a href="{$base_href}/Organizer/ViewMonth?month={$year-value}-{$month-value}">
                                <b>
                                    <font class="{$mioga-list-title-text-color}">
                                        <xsl:call-template name="monthTranslation">
                                            <xsl:with-param name="month" select="$month-value - 1"/>
                                        </xsl:call-template>&#160;<xsl:value-of select="$year-value"/>
                                    </font>
                                </b>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td class="{$mioga-box-bg-color}">
                <table cellspacing="0" cellpadding="2" border="0">
                    <tr class="{$mioga-list-even-row-bg-color}">
                        <th>&#160;</th>
                        <xsl:call-template name="MiogaCalendarDayName"/>
                    </tr>

                    <xsl:for-each select="Calendar/Week">
                        <tr align="center">
                            
                            <td class="{$mioga-list-even-row-bg-color}">
                                <i>
                                    <a href="{$base_href}/Organizer/ViewWeek?week={$year-value}-{.}">
                                        <xsl:value-of select="."/>
                                    </a>
                                </i>
                            </td>

                            <xsl:if test="position()=1 and ../firstDay != 1">
                                <td colspan="{../firstDay - 1}">&#160;</td>
                            </xsl:if>

                            <xsl:variable name="firstDay">
                                <xsl:choose>
                                    <xsl:when test="position() = 1">1</xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="(position() - 2 ) * 7 + (7 - ../firstDay + 1) + 1"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                             
                            <xsl:call-template name="MiogaCalendarDayEntry">
                                <xsl:with-param name="year-value" select="$year-value"/>
                                <xsl:with-param name="month-value" select="$month-value"/>
                                <xsl:with-param name="day-value" select="$day-value"/>
                                <xsl:with-param name="base_href" select="$base_href"/>
                                <xsl:with-param name="firstDay" select="$firstDay"/>
                                <xsl:with-param name="nbDay">
                                    <xsl:choose>
                                        <xsl:when test="position() = 1"><xsl:value-of select="7 - ../firstDay + 1"/></xsl:when>
                                        <xsl:when test="position() = last()"><xsl:value-of select="../lastDay"/></xsl:when>
                                        <xsl:otherwise>7</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:with-param>
                            </xsl:call-template>
                            
                            <xsl:if test="position()=last() and ../lastDay != 7">
                                <td colspan="{7 - ../lastDay}">&#160;</td>
                            </xsl:if>
                        </tr>
                    </xsl:for-each>
                    
                </table>
            </td>
        </tr>

    </table>
    </td></tr>
</xsl:template>

<xsl:template name="MiogaCalendarDayEntry">
    <xsl:param name="firstDay"/>
    <xsl:param name="nbDay"/>
    <xsl:param name="base_href"/>

    <xsl:param name="year-value"/>
    <xsl:param name="month-value"/>
    <xsl:param name="day-value"/>

    <td>
        <xsl:if test="$firstDay = $day-value">
            <xsl:attribute name="style">border: 1px solid;</xsl:attribute>
			<xsl:attribute name="class"><xsl:value-of select="$mioga-error-color"/></xsl:attribute>
        </xsl:if>
        
        <a href="{$base_href}/Organizer/ViewDay?day={$year-value}-{$month-value}-{$firstDay}">
            <xsl:if test="../NotWorked = $firstDay">
                <xsl:attribute name="class"><xsl:value-of select="$mioga-error-color"/>;</xsl:attribute>
            </xsl:if>

            <xsl:if test="string-length($firstDay) = 1">0</xsl:if><xsl:value-of select="$firstDay"/>
        </a>
        
    </td>

    <xsl:if test="$nbDay > 1">
        <xsl:call-template name="MiogaCalendarDayEntry">
            <xsl:with-param name="firstDay" select="$firstDay + 1"/>
            <xsl:with-param name="nbDay" select="$nbDay - 1"/>
            <xsl:with-param name="base_href" select="$base_href"/>
            <xsl:with-param name="year-value" select="$year-value"/>
            <xsl:with-param name="month-value" select="$month-value"/>         
            <xsl:with-param name="day-value" select="$day-value"/>         
        </xsl:call-template>
    </xsl:if>
</xsl:template>


<xsl:template name="MiogaCalendarDayName">
    <th><xsl:value-of select="mioga:gettext('mo')"/></th>
    <th><xsl:value-of select="mioga:gettext('tu')"/></th>
    <th><xsl:value-of select="mioga:gettext('we')"/></th>
    <th><xsl:value-of select="mioga:gettext('th')"/></th>
    <th><xsl:value-of select="mioga:gettext('fr')"/></th>
    <th><xsl:value-of select="mioga:gettext('sa')"/></th>
    <th><xsl:value-of select="mioga:gettext('su')"/></th>
</xsl:template>

</xsl:stylesheet>
