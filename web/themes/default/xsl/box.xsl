<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<xsl:template name="box">
<xsl:param name="title"></xsl:param>
<xsl:param name="content"></xsl:param>
<div class="sbox">
	<h2 class="title_bg_color"><xsl:copy-of select="$title" /></h2>
	<xsl:copy-of select="$content" />
</div>
</xsl:template>

</xsl:stylesheet>
