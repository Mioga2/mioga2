<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
  
  <xsl:output method="html" indent="yes"/>

  <xsl:include href="base.xsl" />
  <xsl:include href="simple_large_list.xsl" />
  <xsl:include href="scriptaculous.xsl"/>
  <xsl:include href="inline_message.xsl"/>
    

<!-- ===============
	root document
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="scriptaculous-js"/>
	<xsl:call-template name="theme-css"/>
</head>
	<xsl:apply-templates />
</html>
</xsl:template>
  
<!-- =================================
     LeafIdentToDesc
     ================================= -->

<xsl:variable name="filename"><xsl:value-of select="$mioga_context/dir/langdir"/>/<xsl:value-of select="$mioga_context/group/@lang"/>.xml</xsl:variable>

<xsl:template name="LeafIdentToDesc">
    <xsl:param name="ident"/>
    <xsl:param name="leaf_ident"/>
    <xsl:param name="noicon">0</xsl:param>

    <xsl:variable name="app_node" select="$app_i18n/lang/application[@ident=$leaf_ident]"/>

    <xsl:choose>
        <xsl:when test="$app_node/@ident = $leaf_ident">
            <xsl:if test="$noicon = 0">
                <xsl:variable name="app_icon">
                    <xsl:choose>
                        <xsl:when test="$app_node/icon"><xsl:value-of select="$app_node/icon"/></xsl:when>
                        <xsl:otherwise>application.png</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <img src="{$image_uri}/16x16/apps/{$app_icon}" width="16" height="16" border="0" />
            </xsl:if>&#160;<xsl:value-of select="$app_node/name" />
        </xsl:when>


    	<xsl:when test="$leaf_ident='no_application'">
          <xsl:call-template name="workspaceTranslation">
            <xsl:with-param name="type">no-application</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
    	<xsl:when test="$leaf_ident='no_group'">
          <xsl:call-template name="workspaceTranslation">
            <xsl:with-param name="type">no-group</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
    	<xsl:when test="$leaf_ident='no_resource'">
          <xsl:call-template name="workspaceTranslation">
            <xsl:with-param name="type">no-resource</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
    	<xsl:when test="$leaf_ident='no_link'">
          <xsl:call-template name="workspaceTranslation">
            <xsl:with-param name="type">no-link</xsl:with-param>
          </xsl:call-template>
        </xsl:when>

        <xsl:when test="$leaf_ident='private_files'">
          <xsl:if test="$noicon = 0"><img src="{$image_uri}/16x16/places/folder-locked.png" />&#160;</xsl:if>
          <xsl:call-template name="workspaceTranslation">
            <xsl:with-param name="type">private-space</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
    	<xsl:when test="$leaf_ident='public_files'">
          <xsl:if test="$noicon = 0"><img src="{$image_uri}/16x16/places/folder.png" />&#160;</xsl:if>
          <xsl:call-template name="workspaceTranslation">
            <xsl:with-param name="type">public-space</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="$leaf_ident=''"><xsl:value-of select="$ident" /></xsl:when>
        <xsl:otherwise><img src="{$image_uri}/16x16/apps/application.png" />&#160;<xsl:value-of select="$ident" /></xsl:otherwise>
    </xsl:choose>

</xsl:template>

  <!-- =================================
       ident
       ================================= -->
  <xsl:template match="ident">
  </xsl:template>

  <!-- =================================
       user
       ================================= -->
  <xsl:template match="User">
  </xsl:template>

  <!-- =================================
       leaf
       ================================= -->
  <xsl:template match="leaf">
    <xsl:variable name="menu_reload_uri">
      <xsl:choose>
        <xsl:when test="count(app) != 0">
          <xsl:text>DisplayMenu?default_app=</xsl:text>
          <xsl:value-of select="app"/>
          <xsl:text>&amp;default_method=DisplayMain&amp;group_ident=</xsl:text>
          <xsl:value-of select="group_ident"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>DisplayMenu?default_link=</xsl:text>
          <xsl:value-of select="default_link"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    &#160;<a href="{./@uri}" target="mioga_main" class="highlight">
    <!--
    <xsl:attribute name="onclick">
        location='<xsl:value-of select="$menu_reload_uri"/>'; <xsl:if test="@ident='private_files' or @ident='public_files'">top.mioga_app_title.location='DisplayAppTitle?app_ident=<xsl:value-of select="@ident"/>'</xsl:if>
    </xsl:attribute>
    -->

    <xsl:call-template name="LeafIdentToDesc">
        <xsl:with-param name="ident"><xsl:value-of select="./@ident" /></xsl:with-param>
        <xsl:with-param name="leaf_ident"><xsl:value-of select="./@app_ident" /></xsl:with-param>
    </xsl:call-template>
	</a><br />
</xsl:template>

<!-- =================================
     branch
     ================================= -->
<xsl:template match="branch">
  <xsl:choose>
      <xsl:when test="./@ident = 'my_applications' and . = ''"></xsl:when>
      <xsl:when test="./@ident = 'my_files' and . = ''"></xsl:when>
    <xsl:when test="./@status='collapse'">
      <table border="0">
		<xsl:attribute name="class"><xsl:value-of select="@ident"/></xsl:attribute>
        <tr>
		  <xsl:attribute name="class"><xsl:value-of select="@ident"/></xsl:attribute>
          <td>
            <a name="{./@ident}" href="DisplayMenu?branch_path_0={./@path}#{./@ident}">
              <img src="{$image_uri}/16x16/actions/arrow-right.png" />
            </a>
          </td>
          <td>
            <a name="{./@ident}" href="DisplayMenu?branch_path_0={./@path}#{./@ident}" class="highlight-branch">
              <xsl:call-template name="IdentToDesc">
                <xsl:with-param name="ident"><xsl:value-of select="./@ident" /></xsl:with-param>
              </xsl:call-template>
            </a>
          </td>
        </tr>
      </table>
    </xsl:when>
    <xsl:when test="./@status='expand'">
      <table border="0">
		<xsl:attribute name="class"><xsl:value-of select="@ident"/></xsl:attribute>
        <xsl:choose>
          <xsl:when test="./@ident='group_applications' or ./@ident='group_files'"></xsl:when>
          <xsl:otherwise>
            <tr>
		      <xsl:attribute name="class"><xsl:value-of select="@ident"/></xsl:attribute>
              <td>
                <a name="{./@ident}" href="DisplayMenu?branch_path_1={./@path}#{./@ident}">
                  <img src="{$image_uri}/16x16/actions/arrow-down.png" />
                </a>
              </td>
              <td>
                <a name="{./@ident}" href="DisplayMenu?branch_path_1={./@path}#{./@ident}" class="highlight-branch">
                  <xsl:call-template name="IdentToDesc">
                    <xsl:with-param name="ident"><xsl:value-of select="./@ident" /></xsl:with-param>
                  </xsl:call-template>
                </a>
              </td>
            </tr>
          </xsl:otherwise>
        </xsl:choose>
        <tr>
          <td>&#160;</td>
          <td>
            <xsl:choose>
              <xsl:when test=". = '' and ./@ident = 'my_applications'">
                <i>
                  <xsl:call-template name="LeafIdentToDesc">
                    <xsl:with-param name="leaf_ident">no_application</xsl:with-param>
                  </xsl:call-template>
                </i>
              </xsl:when>
              <xsl:when test=". = '' and ./@ident = 'my_groups'">
                <i>
                  <xsl:call-template name="LeafIdentToDesc">
                    <xsl:with-param name="leaf_ident">no_group</xsl:with-param>
                  </xsl:call-template>
                </i>
              </xsl:when>
              <xsl:when test=". = '' and ./@ident = 'my_resources'">
                <i>
                  <xsl:call-template name="LeafIdentToDesc">
                    <xsl:with-param name="leaf_ident">no_resource</xsl:with-param>
                  </xsl:call-template>
                </i>
              </xsl:when>
              <xsl:when test=". = '' and ./@ident = 'my_links'">
                <i>
                  <xsl:call-template name="LeafIdentToDesc">
                    <xsl:with-param name="leaf_ident">no_link</xsl:with-param>
                  </xsl:call-template>
                </i>
              </xsl:when>
              <xsl:when test="./@ident = 'my_links'">
                <xsl:apply-templates/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:apply-templates>
                  <xsl:sort select="@ident" order="ascending"/>
                </xsl:apply-templates>
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </tr>
      </table>
    </xsl:when>
  </xsl:choose>
</xsl:template>

  <!-- =================================
       DisplayMain
       ================================= -->
  <xsl:template match="DisplayMain">
    <xsl:choose>
      <xsl:when test="count(resize) != 0">
        <frameset cols="250px,*" frameborder="yes">
          <frame scrolling="auto" src="{menu_uri}" name="mioga_menu"/>
          <frame scrolling="auto" src="{main_uri}" name="mioga_main"/>
        </frameset>
      </xsl:when>
      <xsl:otherwise>
        <frameset cols="250px,*" frameborder="no" framespacing="0" border="0">
          <frame scrolling="auto" src="{menu_uri}" name="mioga_menu"/>
          <frame scrolling="auto" src="{main_uri}" name="mioga_main"/>
        </frameset>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- =================================
       search
       ================================= -->
  <xsl:template match="search">
    <br />
    
    <form method="POST" action="{$private_bin_uri}/Search/DisplayMain" target="mioga_main" name="DisplayMain">
        <input type="hidden" name="advanced_search" value=""/>
        <input type="hidden" name="search_tags" value="on"/>
        <input type="hidden" name="search_description" value="on"/>
        <input type="hidden" name="search_text" value="on"/>
        <table border="0" xsl:use-attribute-sets="mioga-bordered">
            <tr>
                <td colspan="2" class="menu">
                    <xsl:call-template name="workspaceTranslation"><xsl:with-param name="type">search</xsl:with-param></xsl:call-template>
                </td>
            </tr>
            <tr>
          <td><input type="text" size="15" name="query_string" maxlength="64" /></td>
          <td>
            <xsl:call-template name="ok-form-button">
              <xsl:with-param name="name">act_search</xsl:with-param>
            </xsl:call-template>
          </td>
        </tr>
<!--
        <tr>
          <td colspan="2">
              <a href="#" class="highlight">
                <xsl:attribute name="onclick">
                    submitForm("DisplayResults", "adv_search", "1")
                </xsl:attribute>
                <xsl:call-template name="workspaceTranslation">
                    <xsl:with-param name="type">adv-search</xsl:with-param>
                </xsl:call-template>
            </a>
          </td>
        </tr>
-->

      </table>
    </form>
  </xsl:template>
  
  <!-- =================================
       DisplayMenu
       ================================= -->
  <xsl:template match="DisplayMenu">
    <body xsl:use-attribute-sets="workspace_menu_body_attr">
      
      <table border="0" width="100%">
        <tr>
          <td>
            <a href="{$private_bin_uri}/Workspace/DisplayMain?reload_menu=ok" target="_parent"><img src="{$image_uri}/logo_mioga2.gif" border="0" title="Mioga2 V{$mioga_version}"/></a>
          </td>
        </tr>
        <tr>
          <td><xsl:value-of select="User/firstname" />&#160;<xsl:value-of select="User/lastname" /></td>
        </tr>
        <tr>
          <td>
            <xsl:choose>
              <xsl:when test="count(User/nopref) = 0">
                <a href="MenuComponents" target="mioga_main" class="highlight">
                  <xsl:call-template name="IdentToDesc">
                    <xsl:with-param name="ident">my_infos</xsl:with-param>
                  </xsl:call-template>
                </a>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="User/firstname" />&#160;<xsl:value-of select="User/lastname" />
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </tr>
		<xsl:if test="//miogacontext/authen_web">
			<xsl:if test="$logout_uri != ''">
				<tr>
					<td>
						<img src="{$image_uri}/16x16/actions/logout.png" width="16" height="16" border="0" />&#160;<a href="{$logout_uri}?target={$private_bin_uri}/Workspace/DisplayMain" class="highlight" target="_parent"><xsl:value-of select="mioga:gettext ('Logout')"/></a>
					</td>
				</tr>
			</xsl:if>
		</xsl:if>
      </table>
      <xsl:apply-templates select="menu"/>
    </body>
  </xsl:template>

  <xsl:template match="ModifyUser|ModifyUserTimeParams" mode="arg-checker-field-name">
    <xsl:param name="name"/>
    
    <xsl:call-template name="FieldsName">
      <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="ModifyUser" mode="form-button">
    <xsl:if test="count(LocalUser) = 1 or count(LdapUser) = 1">
      <tr>
        <td colspan="2" align="center">
          <xsl:call-template name="ok-form-button">
            <xsl:with-param name="name">user_act_modify</xsl:with-param>
          </xsl:call-template>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>

  <xsl:template match="ModifyUser" mode="form-body">
    <tr>
      <td>
        <xsl:call-template name="FieldsName">
          <xsl:with-param name="name">user_ident</xsl:with-param>
        </xsl:call-template>&#160;:
      </td>
      
      <td>
        <xsl:value-of select="fields/ident"/>
      </td>
    </tr>
    
    <tr>
      <td>
        <xsl:call-template name="FieldsName">
          <xsl:with-param name="name">user_firstname</xsl:with-param>
        </xsl:call-template>&#160;:
      </td>
      
      <td>
        <xsl:choose>
          <xsl:when test="fields/type != 'local_user'">
            <xsl:value-of select="fields/firstname"/>
          </xsl:when>
          <xsl:otherwise>
            <input type="text" name="firstname" value="{fields/firstname}" maxsize="128" />
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </tr>
    
    <tr>
      <td>
        <xsl:call-template name="FieldsName">
          <xsl:with-param name="name">user_lastname</xsl:with-param>
        </xsl:call-template>&#160;:
      </td>

      <td>
        <xsl:choose>
          <xsl:when test="fields/type != 'local_user'">
            <xsl:value-of select="fields/lastname"/>
          </xsl:when>
          <xsl:otherwise>
            <input type="text" name="lastname" value="{fields/lastname}" maxsize="128" />
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </tr>
    
    <tr>
      <td>
        <xsl:call-template name="FieldsName">
          <xsl:with-param name="name">user_email</xsl:with-param>
        </xsl:call-template>&#160;:
      </td>
      
      <td>
        <xsl:choose>
          <xsl:when test="fields/type != 'local_user'">
            <xsl:value-of select="fields/email"/>
          </xsl:when>
          <xsl:otherwise>
            <input type="text" name="email" value="{fields/email}" maxsize="128" />
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </tr>
    
    <xsl:if test="count(LocalUser) = 1 or count(LdapUser) = 1">
      <tr>
        <td colspan="2">
          <xsl:call-template name="workspaceTranslation">
            <xsl:with-param name="type">info-password</xsl:with-param>
          </xsl:call-template>
        </td>
      </tr>
      <tr>
        <td>
          <xsl:call-template name="FieldsName">
            <xsl:with-param name="name">user_password</xsl:with-param>
          </xsl:call-template>&#160;:
        </td>
        
        <td>
          <input type="password" name="password" value="{fields/password}" maxsize="128" />
        </td>
      </tr>
      <tr>
        <td>
          <xsl:call-template name="FieldsName">
            <xsl:with-param name="name">new_password_1</xsl:with-param>
          </xsl:call-template>&#160;:
        </td>
        <td>
          <input type="password" name="new_password_1" value="{fields/new_password_1}" maxsize="128" />
        </td>
      </tr>
      <tr>
        <td>
          <xsl:call-template name="FieldsName">
            <xsl:with-param name="name">new_password_2</xsl:with-param>
          </xsl:call-template>&#160;:
        </td>
        
        <td>
          <input type="password" name="new_password_2" value="{fields/new_password_2}" maxsize="128" />
        </td>
      </tr>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="ModifyUser">
    <body xsl:use-attribute-sets="body_attr">
      <xsl:call-template name="DisplayAppTitle"/>

		<!-- Status message -->
		<xsl:if test="@message != ''">
			<xsl:call-template name="InlineMessage">
				<xsl:with-param name="type" select="@type"/>
				<xsl:with-param name="message" select="@message"/>
			</xsl:call-template>
		</xsl:if>

      <xsl:call-template name="MiogaMenu"/>
      
      <br />
      
      <center>
        <xsl:call-template name="arg_errors"/>
        
        <xsl:choose>
          <xsl:when test="count(LocalUser) = 1">
            <xsl:call-template name="form">
              <xsl:with-param name="name">
                <xsl:call-template name="workspaceTranslation">
                  <xsl:with-param name="type">modify-coord</xsl:with-param>
                </xsl:call-template>
              </xsl:with-param>
              <xsl:with-param name="action">ModifyUser</xsl:with-param>
              <xsl:with-param name="method">POST</xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="form">
              <xsl:with-param name="name">
                <xsl:call-template name="workspaceTranslation">
                  <xsl:with-param name="type">coord</xsl:with-param>
                </xsl:call-template>
              </xsl:with-param>
              <xsl:with-param name="action">ModifyUser</xsl:with-param>
              <xsl:with-param name="method">POST</xsl:with-param>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </center>
    </body>
  </xsl:template>


  <xsl:template match="ModifyUserTimeParams" mode="form-button">
    <tr>
      <td colspan="2" align="center">

          <xsl:call-template name="ok-form-button">
              <xsl:with-param name="name">user_act_modify</xsl:with-param>
          </xsl:call-template><xsl:if test="referer!='ModifyUserTimeParams'">&#160;<xsl:call-template name="back-button">
                  <xsl:with-param name="href" select="referer"/>
              </xsl:call-template>
          </xsl:if>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="ModifyUserTimeParams" mode="form-body">
    <tr>
      <td>
        <xsl:call-template name="FieldsName">
          <xsl:with-param name="name">timezone</xsl:with-param>
        </xsl:call-template>&#160;:
      </td>
      <td>
        <select name="timezone_id">
          <xsl:for-each select="Timezones/timezone">
            <option value="{./@rowid}">
              <xsl:if test="./@rowid=../../fields/timezone_id">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              
              <xsl:value-of select="." />
              
            </option>
          </xsl:for-each>
        </select>
      </td>
    </tr>
    <tr>
      <td colspan="2"><hr /></td>
    </tr>
    <tr>
        <td>
            <xsl:call-template name="FieldsName">
                <xsl:with-param name="name">time_per_day</xsl:with-param>
            </xsl:call-template>&#160;:
        </td>
        <td>
            <xsl:call-template name="LocalTimeInput">
                <xsl:with-param name="hour-value" select="fields/hour_per_day"/>
                <xsl:with-param name="min-value" select="fields/min_per_day"/>
                <xsl:with-param name="hour-name">hour_per_day</xsl:with-param>
                <xsl:with-param name="min-name">min_per_day</xsl:with-param>
            </xsl:call-template>
        </td>
    </tr>

    <tr>
        <td>
            <xsl:call-template name="FieldsName">
                <xsl:with-param name="name">working_days</xsl:with-param>
            </xsl:call-template>&#160;:
        </td>
        
        <td>
            <table cellpadding="0" cellspacing="0" border="0">
                <xsl:call-template name="DayOfWeekCheckBoxes"/>              
            </table>         
        </td>
    </tr>
      
    <tr>
      <td>
        <xsl:call-template name="FieldsName">
          <xsl:with-param name="name">monday_first</xsl:with-param>
        </xsl:call-template>&#160;:
      </td>
      
      <td>
        <select name="monday_first">
          <xsl:choose>
            <xsl:when test="fields/monday_first=1">
              <option value="1" selected="selected">
                <!-- Monday -->
                <xsl:call-template name="LocalDayOfWeek">
                  <xsl:with-param name="daynum"><xsl:value-of select="1" /></xsl:with-param>
                </xsl:call-template>
                
              </option>
              <option value="0">
                <!-- Sunday -->
                <xsl:call-template name="LocalDayOfWeek">
                  <xsl:with-param name="daynum"><xsl:value-of select="0" /></xsl:with-param>
                </xsl:call-template>
              </option>
            </xsl:when>
            <xsl:otherwise>
              <option value="1">
                <!-- Monday -->
                <xsl:call-template name="LocalDayOfWeek">
                  <xsl:with-param name="daynum"><xsl:value-of select="1" /></xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="0" selected="selected">
                <!-- Sunday -->
                <xsl:call-template name="LocalDayOfWeek">
                  <xsl:with-param name="daynum"><xsl:value-of select="0" /></xsl:with-param>
                </xsl:call-template>
              </option>
            </xsl:otherwise>
          </xsl:choose>
        </select>
      </td>
    </tr>

    <tr>
      <td colspan="2"><hr /></td>
    </tr>
    
    <tr>
        <td>
            <xsl:call-template name="FieldsName">
                <xsl:with-param name="name">start_time</xsl:with-param>
            </xsl:call-template>&#160;:
        </td>
        
        <td nowrap="nowrap">
            <xsl:call-template name="LocalTimeInput">
                <xsl:with-param name="hour-value" select="fields/start_time_hour"/>
                <xsl:with-param name="min-value" select="fields/start_time_min"/>
                <xsl:with-param name="hour-name">start_time_hour</xsl:with-param>
                <xsl:with-param name="min-name">start_time_min</xsl:with-param>
            </xsl:call-template>
        </td>
    </tr>
    
    <tr>
        <td>
            <xsl:call-template name="FieldsName">
                <xsl:with-param name="name">end_time</xsl:with-param>
            </xsl:call-template>&#160;:
        </td>
        
        <td nowrap="nowrap">
            <xsl:call-template name="LocalTimeInput">
                <xsl:with-param name="hour-value" select="fields/end_time_hour"/>
                <xsl:with-param name="min-value" select="fields/end_time_min"/>
                <xsl:with-param name="hour-name">end_time_hour</xsl:with-param>
                <xsl:with-param name="min-name">end_time_min</xsl:with-param>
            </xsl:call-template>
        </td>
    </tr>
    
    <tr>
        <td>
            <xsl:call-template name="FieldsName">
                <xsl:with-param name="name">interval</xsl:with-param>
            </xsl:call-template>&#160;:
        </td>
        
        <td>
            <input size="2" type="text" name="interval" value="{fields/interval}"/>&#160;
            <xsl:call-template name="workspaceTranslation">
                <xsl:with-param name="type">minutes</xsl:with-param>
            </xsl:call-template>
        </td>
    </tr>

  </xsl:template>

  <xsl:template name="DayOfWeekCheckBoxes">
      <xsl:param name="day">1</xsl:param>
      
      <tr>
          <td>
              <input type="checkbox" name="working_days" value="{$day}">
                  <xsl:if test="fields/working_days=$day">
                      <xsl:attribute name="checked">checked</xsl:attribute>
                  </xsl:if>
              </input>
          </td>
          <td>
              <xsl:call-template name="LocalDayOfWeek">
                  <xsl:with-param name="daynum"><xsl:value-of select="$day" /></xsl:with-param>
              </xsl:call-template>
          </td>
      </tr>

      <xsl:if test="$day != 0">
          <xsl:call-template name="DayOfWeekCheckBoxes">
              <xsl:with-param name="day" select="($day+1) mod 7"/>
          </xsl:call-template>
      </xsl:if>
      
  </xsl:template>

  
  <xsl:template match="ModifyUserTimeParams">
    <body xsl:use-attribute-sets="body_attr">
      <xsl:call-template name="DisplayAppTitle"/>

		<!-- Status message -->
		<xsl:if test="@message != ''">
			<xsl:call-template name="InlineMessage">
				<xsl:with-param name="type" select="@type"/>
				<xsl:with-param name="message" select="@message"/>
			</xsl:call-template>
		</xsl:if>

      <xsl:call-template name="MiogaMenu"/>
      <br />
      <center>
        <xsl:call-template name="arg_errors"/>
        
        <xsl:call-template name="form">
          <xsl:with-param name="name">
            <xsl:call-template name="wsFormLabel" />
          </xsl:with-param>
          <xsl:with-param name="action">ModifyUserTimeParams</xsl:with-param>
          <xsl:with-param name="method">POST</xsl:with-param>
        </xsl:call-template>
      </center>
    </body>
  </xsl:template>
  
  <!-- =================================
       DisplayHello
       ================================= -->
  <xsl:template match="DisplayHello">
    <body xsl:use-attribute-sets="body_attr" >
      <xsl:call-template name="DisplayAppTitle"/>
    </body>
  </xsl:template>
  
  <xsl:template match="DisplayDefaultURL" mode="form-button">
    
    <tr>
      <td colspan="2" align="center">
        <xsl:call-template name="ok-form-button">
          <xsl:with-param name="name">default_url_act_modify</xsl:with-param>
        </xsl:call-template>
      </td>
    </tr>
  </xsl:template>
  
  <xsl:template match="DisplayDefaultURL" mode="form-body">
    <tr>
      <td>
        <xsl:call-template name="workspaceTranslation">
          <xsl:with-param name="type">home-page</xsl:with-param>
        </xsl:call-template>
      </td>
      
      <td>
        <input type="text" name="default_url" value="{fields/default_url}" size="40"/>
      </td>
    </tr>

    <tr>
      <td>
        <xsl:call-template name="workspaceTranslation">
          <xsl:with-param name="type">portal-page</xsl:with-param>
        </xsl:call-template>
      </td>
      
      <td>
        <input type="text" name="home_url" value="{fields/home_url}" size="40"/>
      </td>
    </tr>

  </xsl:template>
  
  <xsl:template match="DisplayDefaultURL">
    <body xsl:use-attribute-sets="body_attr">
      <xsl:call-template name="DisplayAppTitle"/>

		<!-- Status message -->
		<xsl:if test="@message != ''">
			<xsl:call-template name="InlineMessage">
				<xsl:with-param name="type" select="@type"/>
				<xsl:with-param name="message" select="@message"/>
			</xsl:call-template>
		</xsl:if>

      <xsl:call-template name="MiogaMenu"/>
      <br/>
      <center>
        <xsl:call-template name="arg_errors"/>
        
        <xsl:call-template name="form">
          <xsl:with-param name="name">
            <xsl:call-template name="wsFormLabel"/>
          </xsl:with-param>
          <xsl:with-param name="action">DisplayDefaultURL</xsl:with-param>
          <xsl:with-param name="method">POST</xsl:with-param>
        </xsl:call-template>
        
      </center>
    </body>
  </xsl:template>

  
  <xsl:template match="MenuComponents" mode="form-button">
    
    <tr>
      <td colspan="3" align="center">
        <xsl:call-template name="ok-form-button">
          <xsl:with-param name="name">components_act_modify</xsl:with-param>
        </xsl:call-template>
      </td>
    </tr>
</xsl:template>

<xsl:template match="MenuComponents" mode="form-body">
<xsl:for-each select="entry">
    <tr>
        <td>
	   <xsl:call-template name="IdentToDesc">
	      <xsl:with-param name="ident"><xsl:value-of select="./@ident" /></xsl:with-param>
	   </xsl:call-template>
        </td>
	<td>
		<xsl:choose>
		<xsl:when test="./@display_ok='1'">
                    <input type="checkbox" name="{./@ident}" value="1" checked="checked" />
		</xsl:when>
		<xsl:otherwise>
                    <input type="checkbox" name="{./@ident}" value="1" />
		</xsl:otherwise>
		</xsl:choose>
	</td>
        <td nowrap="nowrap">
	<xsl:choose>
	   <xsl:when test="position() = last()">
	      <a href="MenuComponents?up={position()-1}"><img border="0" src="{$image_uri}/16x16/actions/arrow-up.png" width="16" height="19" /></a>&#160;<img border="0" src="{$image_uri}/transparent_fill.gif" width="16" height="19" />
	   </xsl:when>
	   <xsl:when test="position() =1">
	      <img border="0" src="{$image_uri}/transparent_fill.gif" width="16" height="19" />&#160;<a href="MenuComponents?down={position()-1}"><img border="0" src="{$image_uri}/16x16/actions/arrow-down.png" /></a>
	   </xsl:when>
	   <xsl:otherwise>
	      <a href="MenuComponents?up={position()-1}"><img border="0" src="{$image_uri}/16x16/actions/arrow-up.png" width="16" height="19" /></a>&#160;<a href="MenuComponents?down={position()-1}"><img border="0" src="{$image_uri}/16x16/actions/arrow-down.png" /></a>
	   </xsl:otherwise>
	</xsl:choose>
	</td>
    </tr>
</xsl:for-each>

    <tr>
        <td colspan="3"><hr /></td>
    </tr>

    <tr>
        <td colspan="2">
            <xsl:call-template name="FieldsName">
                <xsl:with-param name="name">resize</xsl:with-param>
            </xsl:call-template>&#160;:
        </td>

        <td>
	   <xsl:choose>
	      <xsl:when test="count(resize)=1"><input type="checkbox" name="resize" value="1" checked="checked" /></xsl:when>
	      <xsl:otherwise><input type="checkbox" name="resize" value="1" /></xsl:otherwise>
	   </xsl:choose>
        </td>
    </tr>
</xsl:template>

<xsl:template match="MenuComponents">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle"/>

	<!-- Status message -->
	<xsl:if test="@message != ''">
		<xsl:call-template name="InlineMessage">
			<xsl:with-param name="type" select="@type"/>
			<xsl:with-param name="message" select="@message"/>
		</xsl:call-template>
	</xsl:if>

    <xsl:call-template name="MiogaMenu"/>
    <br/>
    <center>
      <xsl:call-template name="arg_errors"/>
      
      <xsl:call-template name="form">
        <xsl:with-param name="name">
          <xsl:call-template name="wsFormLabel"/>
        </xsl:with-param>
        <xsl:with-param name="action">MenuComponents</xsl:with-param>
        <xsl:with-param name="method">POST</xsl:with-param>
        <xsl:with-param name="nbcols">3</xsl:with-param>
      </xsl:call-template>
    </center>
  </body>
</xsl:template>


<xsl:template match="DisplayLinks" mode="table-body">
  
  <xsl:for-each select="Row">
    <tr class="list-body" align="center">
      <xsl:choose>
        <xsl:when test="position() mod 2 = 1">
          <xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color" /></xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="class"><xsl:value-of select="$mioga-list-even-row-bg-color" /></xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <td nowrap="nowrap"><xsl:value-of select="ident" /></td>
      <td nowrap="nowrap">
          <a href="{link}" alt="{link}" title="{link}">
              <xsl:choose>
                  <xsl:when test="string-length(link) > 40">
                      <xsl:value-of select="substring(link, 0, 37)"/>...
                  </xsl:when>
                  <xsl:otherwise><xsl:value-of select="link"/></xsl:otherwise>
              </xsl:choose>      
          </a>
      </td>
      <td>
        <xsl:if test="position() !=1">
          <a href="DisplayLinks?up={position()-1}">
            <xsl:attribute name="onclick">top.mioga_menu.location='<xsl:value-of select="$bin_uri"/>/__MIOGA-USER__/Workspace/DisplayMenu'</xsl:attribute>
            <img border="0" src="{$image_uri}/16x16/actions/arrow-up.png"/>
          </a>
        </xsl:if>
        &#160;
        <xsl:if test="position() !=last()">
          <a href="DisplayLinks?down={position()-1}">
            <xsl:attribute name="onclick">top.mioga_menu.location='<xsl:value-of select="$bin_uri"/>/__MIOGA-USER__/Workspace/DisplayMenu'</xsl:attribute>
            <img border="0" src="{$image_uri}/16x16/actions/arrow-down.png"/>
          </a>
        </xsl:if>
      </td>
      <td nowrap="nowrap">
        <a href="ModifyLink?rowid={rowid}">
          <img src="{$image_uri}/16x16/actions/document-edit.png" alt="modifier le lien" name="modifier le lien"/>
        </a>
      </td>
      <td nowrap="nowrap">
        <a href="DeleteLink?rowid={rowid}">
          <img src="{$image_uri}/16x16/actions/trash-empty.png" border="0" alt="détruire le lien" name="détruire le lien"/>
        </a>
      </td>
    </tr>
  </xsl:for-each>
</xsl:template>

<xsl:template match="DisplayLinks">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle"/>

		<!-- Status message -->
		<xsl:if test="@message != ''">
			<xsl:call-template name="InlineMessage">
				<xsl:with-param name="type" select="@type"/>
				<xsl:with-param name="message" select="@message"/>
			</xsl:call-template>
		</xsl:if>

    <xsl:call-template name="MiogaMenu"/>

    <div xsl:use-attribute-sets="app_body_attr">

       <xsl:call-template name="MiogaAction">
           <xsl:with-param name="wrap">1</xsl:with-param>
       </xsl:call-template>
    
       <br/>

      <xsl:choose>
        <xsl:when test="count(Row) != 0">
         <table xsl:use-attribute-sets="mioga-border-table" align="center" width="50%">
            <tr>
                <td>
                    <table xsl:use-attribute-sets="mioga-content-table">
                        <tr>
                            <xsl:apply-templates select="." mode="table-title"/>
                        </tr>
                        <xsl:apply-templates select="." mode="table-body"/>
                    </table>
                </td>
            </tr>
        </table>
    </xsl:when>
    <xsl:otherwise>
        <p>
            <xsl:call-template name="workspaceTranslation">
                <xsl:with-param name="type">no-link</xsl:with-param>
            </xsl:call-template>
        </p>
    </xsl:otherwise>
</xsl:choose>
    </div>
  </body>
</xsl:template>

<xsl:template match="DisplayLinks" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">AddLink</xsl:with-param>
    <xsl:with-param name="href">AddLink</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- =================================
     DeleteLink
     ================================= -->

<xsl:template match="DeleteLink" mode="table-body">
  <xsl:for-each select="Row">
    <tr class="list-body" align="center">
      <xsl:choose>
        <xsl:when test="position() mod 2 = 1">
          <xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color" /></xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="class"><xsl:value-of select="$mioga-list-even-row-bg-color" /></xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <td nowrap="nowrap"><xsl:value-of select="ident" /></td>
      <td nowrap="nowrap"><xsl:value-of select="link" /></td>
    </tr>
  </xsl:for-each>
</xsl:template>

<xsl:template match="DeleteLink" mode="form-button">
  <tr>
    <td colspan="2" align="center">
      <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">act_delete_link</xsl:with-param>
        <xsl:with-param name="reload_menu">1</xsl:with-param>
        <xsl:with-param name="referer"><xsl:value-of select="referer" /></xsl:with-param>
      </xsl:call-template>
    </td>
  </tr>
</xsl:template>

<xsl:template match="DeleteLink" mode="form-body">
  <tr>
    <td colspan="2">
        <table xsl:use-attribute-sets="mioga-border-table" align="center" width="50%">
            <tr>
                <td>
                    <table xsl:use-attribute-sets="mioga-content-table">
                        <tr>
                            <xsl:apply-templates select="." mode="table-title"/>
                        </tr>                    
                        
                        <xsl:apply-templates select="." mode="table-body"/>
                    </table>
                </td>
            </tr>
        </table>
    </td>
  </tr>
</xsl:template>

<xsl:template match="DeleteLink">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle"/>
    
    <br/>

    <center>
      
      <xsl:call-template name="error" select="error"/>
      
      <xsl:call-template name="form">
	<xsl:with-param name="name">
          <xsl:call-template name="wsFormLabel"/>
        </xsl:with-param>
        <xsl:with-param name="action">DeleteLink</xsl:with-param>
        <xsl:with-param name="method">POST</xsl:with-param>
      </xsl:call-template>
    </center>
  </body>
</xsl:template>

<!-- =================================
     ModifyLink | AddLink
     ================================= -->
<xsl:template match="ModifyLink|AddLink" mode="form-button">
  <tr>
    <td colspan="2" align="center">
      <xsl:choose>
	<xsl:when test="local-name(.)='ModifyLink'">
          <xsl:call-template name="ok-cancel-form-button">
            <xsl:with-param name="name">link_act_modify</xsl:with-param>
            <xsl:with-param name="referer">DisplayLinks</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="local-name(.)='AddLink'">
          <xsl:call-template name="ok-cancel-form-button">
            <xsl:with-param name="name">link_act_add</xsl:with-param>
            <xsl:with-param name="referer">DisplayLinks</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </td>
  </tr>
</xsl:template>

<xsl:template match="ModifyLink|AddLink" mode="form-body">
  <tr>
    <td>
      <xsl:call-template name="workspaceTranslation">
        <xsl:with-param name="type">link-label</xsl:with-param>
      </xsl:call-template>
    </td>
    
    <td>
      <input type="text" name="ident" value="{fields/ident}" size="40"/>
    </td>
  </tr>
  <tr>
    <td>
      <xsl:value-of select="mioga:gettext('URL:')"/>
    </td>
    
    <td>
      <input type="text" name="link" value="{fields/link}" size="60"/>
    </td>
  </tr>
</xsl:template>

<xsl:template match="ModifyLink|AddLink">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle"/>

    <br/>

    <center>
      <xsl:call-template name="error" select="error"/>
      
      <xsl:choose>
        <xsl:when test="local-name(.)='ModifyLink'">
          <xsl:call-template name="form">
            <xsl:with-param name="name"><xsl:call-template name="wsFormLabel"/></xsl:with-param>
            <xsl:with-param name="action">ModifyLink</xsl:with-param>
            <xsl:with-param name="method">POST</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="local-name(.)='AddLink'">
          <xsl:call-template name="form">
            <xsl:with-param name="name"><xsl:call-template name="wsFormLabel"/></xsl:with-param>
            <xsl:with-param name="action">AddLink</xsl:with-param>
            <xsl:with-param name="method">POST</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </center>
  </body>
</xsl:template>

<!-- =================================
     AddAppToLinks
     ================================= -->
<xsl:template match="AddAppToLinks" mode="form-button">
  <tr>
    <td colspan="2" align="center">
      <xsl:choose>
        <xsl:when test="error = '' or count(error) = 0">
          <xsl:call-template name="ok-cancel-form-button">
            <xsl:with-param name="name">act_add_app_link</xsl:with-param>
            <xsl:with-param name="referer"><xsl:value-of select="fields/link" /></xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="cancel-noform-button">
            <xsl:with-param name="name">act_add_app_link</xsl:with-param>
            <xsl:with-param name="href"><xsl:value-of select="fields/link" /></xsl:with-param>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </td>
  </tr>
</xsl:template>

<xsl:template match="AddAppToLinks" mode="form-body">
  <xsl:choose>
    <xsl:when test="error = '' or count(error) = 0">
      <tr>
        <td>
          <xsl:call-template name="workspaceTranslation">
            <xsl:with-param name="type">link-label</xsl:with-param>
          </xsl:call-template>
        </td>
        <td><input type="text" name="ident" value="{fields/ident}" size="40" maxsize="128" /></td>
      </tr>
      <tr>
        <td>
          <xsl:value-of select="mioga:gettext('URL:')"/>
        </td>
        <td><xsl:value-of select="fields/link" /></td>
      </tr>
    </xsl:when>
    <xsl:otherwise>
      <tr>
        <td colspan="2">
          <font color="red">
            <xsl:call-template name="workspaceTranslation">
              <xsl:with-param name="type">no-link-add</xsl:with-param>
            </xsl:call-template>
          </font>
        </td>
      </tr>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="AddAppToLinks">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle"/>

    <br/>

    <center>
      <xsl:call-template name="arg_errors"/>
      
      <xsl:call-template name="form">
        <xsl:with-param name="name"><xsl:call-template name="wsFormLabel"/></xsl:with-param>
        <xsl:with-param name="action">AddAppToLinks</xsl:with-param>
        <xsl:with-param name="method">POST</xsl:with-param>
      </xsl:call-template>
    </center>
  </body>
</xsl:template>

<!-- =================================
     SuccessMessage
     ================================= -->

<xsl:template match="SuccessMessage">

    <xsl:call-template name="SuccessMessage">
        <xsl:with-param name="message">
          <xsl:call-template name="workspaceTranslation">
            <xsl:with-param name="type">success-modifications</xsl:with-param>
          </xsl:call-template>
        </xsl:with-param>
        <xsl:with-param name="referer" select="referer"/>
        <xsl:with-param name="reload_menu">1</xsl:with-param>
    </xsl:call-template>

</xsl:template>


  <!-- ====================
       == FR TRANSLATION ==
       ==================== -->

  <!-- Error messages -->
  <xsl:template name="error">
    <xsl:choose>
      <xsl:when test="./error = 'no_such_group'">
        <p class="error"><font color="red"><xsl:value-of select="mioga:gettext('Input group is unknown.')"/></font></p>
      </xsl:when>
      <xsl:when test="./error = 'all_fields'">
        <p class="error"><font color="red"><xsl:value-of select="mioga:gettext('All fields must be filled.')"/></font></p>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- common translation -->
  <xsl:template name="workspaceTranslation">
    <xsl:param name="type"/>
    <xsl:choose>
        <xsl:when test="$type='minutes'"><xsl:value-of select="mioga:gettext('minutes')"/></xsl:when>
        <xsl:when test="$type='back'"><xsl:value-of select="mioga:gettext('Back')"/></xsl:when>
        <xsl:when test="$type='search'"><xsl:value-of select="mioga:gettext('Search')"/></xsl:when>
        <xsl:when test="$type='adv-search'"><xsl:value-of select="mioga:gettext('Advanced search')"/></xsl:when>
        <xsl:when test="$type='info-password'">
          <xsl:value-of select="mioga:gettext('If you want to change your password, first you need to input your current password and then input your new password two times.')"/>
      </xsl:when>
      <xsl:when test="$type='modify-coord'"><xsl:value-of select="mioga:gettext('Modify my details:')"/></xsl:when>
      <xsl:when test="$type='coord'"><xsl:value-of select="mioga:gettext('My details:')"/></xsl:when>
      <xsl:when test="$type='no-application'"><xsl:value-of select="mioga:gettext('No available application')"/></xsl:when>
      <xsl:when test="$type='no-group'"><xsl:value-of select="mioga:gettext('No group')"/></xsl:when>
      <xsl:when test="$type='no-resource'"><xsl:value-of select="mioga:gettext('No resource')"/></xsl:when>
      <xsl:when test="$type='no-link'"><xsl:value-of select="mioga:gettext('No link')"/></xsl:when>
      <xsl:when test="$type='private-space'"><xsl:value-of select="mioga:gettext('Private space')"/></xsl:when>
      <xsl:when test="$type='public-space'"><xsl:value-of select="mioga:gettext('Public space')"/></xsl:when>
      <xsl:when test="$type='home-page'"><xsl:value-of select="mioga:gettext('My homepage:')"/></xsl:when>
      <xsl:when test="$type='portal-page'"><xsl:value-of select="mioga:gettext('My portal:')"/></xsl:when>
      <xsl:when test="$type='link-label'"><xsl:value-of select="mioga:gettext('Link title:')"/></xsl:when>
      <xsl:when test="$type='no-link-add'"><xsl:value-of select="mioga:gettext('Unable to add link')"/></xsl:when>
      <xsl:when test="$type='success-modifications'"><xsl:value-of select="mioga:gettext('Modifications applyed successfully.')"/></xsl:when>
    </xsl:choose>
  </xsl:template>
  
  <!-- ModifyUserTimeParams template -->
  <xsl:template match="ModifyUserTimeParams" mode="arg-checker-unknown-args">
    <xsl:param name="name"/>
    <xsl:param name="arg"/>
    
    <xsl:choose>
      <xsl:when test="$name = 'incorrect_time_per_day'"><xsl:value-of select="mioga:gettext('Invalid input time.')"/></xsl:when>
      <xsl:when test="$name = 'want_int' and $arg = 'hour_per_day'"><xsl:value-of select="mioga:gettext('Input number of hours is invalid.')"/></xsl:when>
      <xsl:when test="$name = 'want_int' and $arg = 'min_per_day'"><xsl:value-of select="mioga:gettext('Input number of minutes is invalid.')"/></xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- MiogaMenuTranslation -->
  <xsl:template name="MiogaMenuTranslation">
    <xsl:param name="label"/>
    <xsl:choose>
      <xsl:when test="$label='ModifyUser'"><xsl:value-of select="mioga:gettext('My details')"/></xsl:when>
      <xsl:when test="$label='ModifyUserTimeParams'"><xsl:value-of select="mioga:gettext('My schedule')"/></xsl:when>
      <xsl:when test="$label='MenuComponents'"><xsl:value-of select="mioga:gettext('My menu')"/></xsl:when>
      <xsl:when test="$label='DisplayDefaultURL'"><xsl:value-of select="mioga:gettext('My homepage')"/></xsl:when>
      <xsl:when test="$label='DisplayLinks'"><xsl:value-of select="mioga:gettext('My links')"/></xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="MiogaActionTranslation">
    <xsl:param name="label"/>
    <xsl:choose>
      <xsl:when test="$label='AddLink'"><xsl:value-of select="mioga:gettext('Add link')"/></xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="DisplayLinks" mode="table-title">
    <th nowrap="nowrap"><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Name')"/></font></th>
    <th nowrap="nowrap"><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Link')"/></font></th>
    <th nowrap="nowrap"><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Bracket')"/></font></th>
    <th nowrap="nowrap"><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Modify')"/></font></th>
    <th nowrap="nowrap"><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Delete')"/></font></th>
  </xsl:template>

  <xsl:template name="wsFormLabel">
    <xsl:choose>
      <xsl:when test="name(.) = 'ModifyUserTimeParams'"><xsl:value-of select="mioga:gettext('Modify my schedule parameters')"/></xsl:when>
      <xsl:when test="name(.) = 'DisplayDefaultURL'"><xsl:value-of select="mioga:gettext('Modify my homepage:')"/> </xsl:when>
      <xsl:when test="name(.) = 'MenuComponents'"><xsl:value-of select="mioga:gettext('Menu components')"/></xsl:when>
      <xsl:when test="name(.) = 'DeleteLink'"><xsl:value-of select="mioga:gettext('Deleting link')"/></xsl:when>
      <xsl:when test="name(.) = 'ModifyLink'"><xsl:value-of select="mioga:gettext('Modify link')"/></xsl:when>
      <xsl:when test="name(.) = 'AddLink'"><xsl:value-of select="mioga:gettext('Add link')"/></xsl:when>
      <xsl:when test="name(.) = 'AddAppToLinks'"><xsl:value-of select="mioga:gettext('Add link')"/></xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="DeleteLink" mode="table-title">
    <th nowrap="nowrap"><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Name')"/></font></th>
    <th nowrap="nowrap"><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Link')"/></font></th>
  </xsl:template>

  <xsl:template name="IdentToDesc">
    <xsl:param name="ident"/>
    <xsl:param name="noicon">0</xsl:param>
    
    <xsl:choose>
      <xsl:when test="$ident='my_applications'"><xsl:value-of select="mioga:gettext('My applications')"/></xsl:when>
      <xsl:when test="$ident='my_groups'"><xsl:value-of select="mioga:gettext('My groups')"/></xsl:when>
      <xsl:when test="$ident='my_resources'"><xsl:value-of select="mioga:gettext('My resources')"/></xsl:when>
      <xsl:when test="$ident='my_links'"><xsl:value-of select="mioga:gettext('My links')"/></xsl:when>
      <xsl:when test="$ident='my_files'"><xsl:value-of select="mioga:gettext('My documents')"/></xsl:when>
      <xsl:when test="$ident='group_files'"><xsl:value-of select="mioga:gettext('Documents')"/></xsl:when>
      <xsl:when test="$ident='group_applications'"><xsl:value-of select="mioga:gettext('Applications')"/></xsl:when>
      <xsl:when test="$ident='resource_applications'"><xsl:value-of select="mioga:gettext('Applications')"/></xsl:when>
      <xsl:when test="$ident='search'"><xsl:value-of select="mioga:gettext('Search')"/></xsl:when>
      <xsl:when test="$ident='my_infos'">
        <xsl:if test="$noicon = 0"><img src="{$image_uri}/16x16/places/user-identity.png" />&#160;</xsl:if>
        <xsl:value-of select="mioga:gettext('My informations')"/>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="$ident" /></xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- =================================
       MiogaMenuBody
       ================================= -->
  <xsl:template match="*" mode="MiogaMenuBody">
    
    <xsl:call-template name="MiogaMenuTab">
      <xsl:with-param name="value"><xsl:value-of select="mioga:gettext('My menu')"/></xsl:with-param>
      <xsl:with-param name="href">MenuComponents</xsl:with-param>
      <xsl:with-param name="label">MenuComponents</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaMenuTab">
      <xsl:with-param name="value"><xsl:value-of select="mioga:gettext('My homepage')"/></xsl:with-param>
      <xsl:with-param name="href">DisplayDefaultURL</xsl:with-param>
      <xsl:with-param name="label">DisplayDefaultURL</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaMenuTab">
      <xsl:with-param name="value"><xsl:value-of select="mioga:gettext('My links')"/></xsl:with-param>
      <xsl:with-param name="href">DisplayLinks</xsl:with-param>
      <xsl:with-param name="label">DisplayLinks</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="AddAppToLinks" mode="arg-checker-field-name">
    <xsl:param name="name"/>
    <xsl:choose>
      <xsl:when test="$name='ident'"><xsl:value-of select="mioga:gettext('link title')"/></xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- =================================
       DisplayUsers search field name
       ================================= -->
  
  <xsl:template match="ModifyUser" mode="sll-field-name" name="FieldsName">
      <xsl:param name="name"/>
      
      <xsl:choose>
          <xsl:when test="$name='start_time'"><xsl:value-of select="mioga:gettext('Start time')"/></xsl:when>
          <xsl:when test="$name='end_time'"><xsl:value-of select="mioga:gettext('End time')"/></xsl:when>
          <xsl:when test="$name='interval'"><xsl:value-of select="mioga:gettext('Schedule time interval')"/></xsl:when>
          <xsl:otherwise>
              <xsl:call-template name="mioga2-common-name-translation">
                  <xsl:with-param name="name" select="$name"/>
              </xsl:call-template>        
          </xsl:otherwise>
      </xsl:choose>
      
  </xsl:template>

</xsl:stylesheet>
