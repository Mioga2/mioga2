<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:news="http://www.mioga2.org/portal/mioglets/News" xmlns:mioga="urn:mioga">

<xsl:output method="html"/>

<!-- =================================
     Handle NewsList
     ================================= -->

<!-- Editor Form -->

<xsl:template match="news:NewsList" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorModeEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">news_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

	<xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">show_title</xsl:with-param>
        <xsl:with-param name="value" select="$node/show_title"/>
    </xsl:call-template>
    
   	<xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">show_date</xsl:with-param>
        <xsl:with-param name="value" select="$node/show_date"/>
    </xsl:call-template>
    
  	<xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">show_author</xsl:with-param>
        <xsl:with-param name="value" select="$node/show_author"/>
    </xsl:call-template>
    
  	<xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">show_content</xsl:with-param>
        <xsl:with-param name="value" select="$node/show_content"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">delay</xsl:with-param>
        <xsl:with-param name="value" select="$node/delay"/>
    </xsl:call-template>

</xsl:template>


<!-- main NewsList template -->

<xsl:template match="news:NewsList">
    <xsl:param name="node"/>
    
    <table class="newsNewsList" cellpadding="5" cellspacing="0">
        <xsl:if test="count(news) = 0">
            <tr>
                <td>
                    <i><xsl:call-template name="news-no-news"/></i>
                </td>
            </tr>
        </xsl:if>
        
        <xsl:apply-templates select="news">
            <xsl:with-param name="node" select="$node"/>
        </xsl:apply-templates>

        <xsl:if test="count(news) != 0">
            <tr>
                <td align="left">
                    <xsl:call-template name="news-more-news"/>&#160;<a href="{$private_bin_uri}/News/DisplayMain" style="font-size: tiny">[...]</a>
                </td>
            </tr>
        </xsl:if>
    </table>
</xsl:template>

<xsl:template match="news">
    <xsl:param name="node"/>

    <tr>
        <td>
            <table class="newsNewsItem" cellspacing="0" cellpadding="3">
                <tr class="newsNewsItem">
                    <td align="left" nowrap="nowrap" style="font-size: small">
                        <xsl:choose>
                            <xsl:when test="$node/mode = 'user'"><xsl:value-of select="group_ident"/></xsl:when>
                            <xsl:otherwise>&#160;</xsl:otherwise>
                        </xsl:choose>
                    </td>

                   	<td align="center" width="100%">
                   	    <strong>
                           	<xsl:value-of select="title"/>
                        </strong>
   	                </td>
                    
                    <td style="font-size: small" nowrap="nowrap">
                    <xsl:if test="count(created) != 0">
                        <xsl:call-template name="LocalDateShort">
                            <xsl:with-param name="node" select="created"/>
                        </xsl:call-template>&#160;<xsl:call-template name="LocalTime">
                            <xsl:with-param name="node" select="created"/>
                            <xsl:with-param name="nosec">1</xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:if test="count(created) != 0 and count(creator_name) != 0">
                    &#160;-&#160;
                    </xsl:if>
                    <xsl:if test="count(creator_name) != 0">
                        <a href="mailto:{creator_email}"><xsl:value-of select="creator_name"/></a>
                    </xsl:if>
                    </td>
                </tr>

                <tr class="text">
                    <td colspan="3">
                        <xsl:value-of disable-output-escaping="yes" select="text"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</xsl:template>

<xsl:template name="news-more-news">
  <xsl:value-of select="mioga:gettext('More news')"/>
</xsl:template>

<xsl:template name="news-no-news">
  <xsl:value-of select="mioga:gettext('No news')"/>
</xsl:template>


<!-- =================================
     Handle RSS Feed
     ================================= -->

<!-- Editor Form -->

<xsl:template match="news:RSSFeed" mode="editor">
    <xsl:param name="node"/>

	    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

   <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">news_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">mode</xsl:with-param>
        <xsl:with-param name="value">user</xsl:with-param>        
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">feed</xsl:with-param>
        <xsl:with-param name="value" select="$node/feed"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">size</xsl:with-param>
        <xsl:with-param name="value" select="$node/size"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="news:RSSFeed">
    <xsl:param name="node"/>
    
	<xsl:apply-templates select="error">
		<xsl:with-param name="node" select="$node"/>
    </xsl:apply-templates>
	
    <xsl:if test="count(item) != 0">
    <table class="newsRSSFeed" cellpadding="5" cellspacing="0">
    <xsl:for-each select="item">
	    <tr>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
					<xsl:otherwise>oddRow</xsl:otherwise>
				</xsl:choose>
		   </xsl:attribute>
		   <td>
			<a href="{@link}"><xsl:value-of select="."/></a>
		   </td>
		</tr>
    </xsl:for-each>
    </table>
        
    </xsl:if>
</xsl:template>

<xsl:template match="error">
	<span class="error"><xsl:value-of select="."/></span>
</xsl:template>

</xsl:stylesheet>
