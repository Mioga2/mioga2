<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
  <xsl:output method="html" indent="yes"/>

  <xsl:include href="base.xsl" />
  <xsl:include href="simple_large_list.xsl" />
  <xsl:include href="anim_group_appdesc.xsl" />
  <xsl:include href="scriptaculous.xsl"/>
  <xsl:include href="inline_message.xsl"/>
    
<!-- ===============
	root document
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="scriptaculous-js"/>
	<xsl:call-template name="theme-css"/>
</head>
	<xsl:apply-templates />
</html>
</xsl:template>
  
  <!-- =================================
       Tabs Menu description
       ================================= -->
  
  <xsl:template match="*" mode="MiogaMenuBody">
      
      <xsl:call-template name="MiogaMenuTab">
          <xsl:with-param name="href">DisplayUsers</xsl:with-param>
          <xsl:with-param name="label">DisplayUsers</xsl:with-param>
      </xsl:call-template>
    
      <xsl:call-template name="MiogaMenuTab">
          <xsl:with-param name="href">DisplayTeams</xsl:with-param>
          <xsl:with-param name="label">DisplayTeams</xsl:with-param>
      </xsl:call-template>
      
      <xsl:call-template name="MiogaMenuTab">
          <xsl:with-param name="href">DisplayResources</xsl:with-param>
          <xsl:with-param name="label">DisplayResources</xsl:with-param>
      </xsl:call-template>
      
      <xsl:call-template name="MiogaMenuTab">
          <xsl:with-param name="href">DisplayThemes</xsl:with-param>
          <xsl:with-param name="label">DisplayThemes</xsl:with-param>
      </xsl:call-template>
      
      <xsl:call-template name="MiogaMenuTab">
          <xsl:with-param name="href">DisplayApplications</xsl:with-param>
          <xsl:with-param name="label">DisplayApplications</xsl:with-param>
      </xsl:call-template>
      
      <xsl:call-template name="MiogaMenuTab">
          <xsl:with-param name="href">DisplayProfiles</xsl:with-param>
          <xsl:with-param name="label">DisplayProfiles</xsl:with-param>
      </xsl:call-template>
      
  </xsl:template>
  
  <!-- =================================
       DisplayUsers change profile button
       ================================= -->
  
  <xsl:template match="DisplayUsers|DisplayTeams|DisplayResources|DisplayApplications|DisplayProfiles|DisplayThemes|DisplayProfileUsers|DisplayProfileTeams|TeamsList|UsersList" mode="sll-checkboxes-button">
      
      <xsl:choose>
          <xsl:when test="local-name(.) ='DisplayApplications'">
              <xsl:call-template name="change-form-button">
                  <xsl:with-param name="name">change_application_act</xsl:with-param>
              </xsl:call-template>
          </xsl:when>
          
          <xsl:otherwise>
              <xsl:call-template name="change-form-button">
                  <xsl:with-param name="name">change_profile_act</xsl:with-param>
              </xsl:call-template>
          </xsl:otherwise>
      </xsl:choose>
      
  </xsl:template>
  
  <!-- =================================
       DisplayUsers search field name
       ================================= -->

  <xsl:template match="DisplayUsers|DisplayTeams|DisplayResources|DisplayApplications|DisplayProfiles|DisplayThemes|DisplayProfileUsers|DisplayProfileTeams|TeamsList|UsersList" mode="sll-field-name" name="FieldsName">
      <xsl:param name="name"/>
      
      <xsl:choose>
          <xsl:when test="$name = 'change_profile'">
              <select name="profile_id">
                  <option value=""><xsl:call-template name="ChangeProfileTranslation"/></option>
                  <xsl:for-each select="./ProfileList/profile">
                      <option value="{@rowid}">
                          <xsl:value-of select="."/>
                      </option>
                  </xsl:for-each>
              </select>
          </xsl:when>
          <xsl:when test="$name = 'nb_member'">
              <xsl:call-template name="fieldsNameTranslation">
                  <xsl:with-param name="type">nb_number</xsl:with-param>
              </xsl:call-template>
          </xsl:when>
          <xsl:when test="$name = 'ident' and local-name(.) = 'DisplayProfiles'">
              <xsl:call-template name="fieldsNameTranslation">
                  <xsl:with-param name="type">profile</xsl:with-param>
              </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
              <xsl:call-template name="mioga2-common-name-translation">
                  <xsl:with-param name="name" select="$name"/>
              </xsl:call-template>
          </xsl:otherwise>
      </xsl:choose>
  </xsl:template>
  
  <xsl:template match="DisplayUsers|DisplayTeams|DisplayResources">
      <xsl:variable name="current-func" select="$mioga_method"/>
      
      <body xsl:use-attribute-sets="body_attr">
          
          <xsl:call-template name="DisplayAppTitle">
              <xsl:with-param name="help">animgroupe.html#<xsl:choose>
                  <xsl:when test="name(.) = 'DisplayUsers'">user</xsl:when>
                  <xsl:when test="name(.) = 'DisplayTeams'">team</xsl:when>
                  <xsl:when test="name(.) = 'DisplayResources'">resource</xsl:when>
                  </xsl:choose></xsl:with-param>
          </xsl:call-template>

		<!-- Status message -->
		<xsl:if test="@message != ''">
			<xsl:call-template name="InlineMessage">
				<xsl:with-param name="type" select="@type"/>
				<xsl:with-param name="message" select="@message"/>
			</xsl:call-template>
		</xsl:if>

          <xsl:call-template name="MiogaMenu"/>
          
          <xsl:call-template name="MiogaTitle" />

          
          <div xsl:use-attribute-sets="app_body_attr">
          
              <xsl:if test="./SimpleLargeList/List">
                  <xsl:call-template name="MiogaAction"/>
              </xsl:if>
              
              <xsl:apply-templates select="SimpleLargeList">
                  <xsl:with-param name="advancedsearch">1</xsl:with-param>
                  <xsl:with-param name="label" select="name(.)"/>
                  <xsl:with-param name="nb-search-column">
                      <xsl:choose>
                          <xsl:when test="name(.) = 'DisplayUsers'">2</xsl:when>
                          <xsl:when test="name(.) ='DisplayResources'">2</xsl:when>
                          <xsl:otherwise>1</xsl:otherwise>
                      </xsl:choose>
                  </xsl:with-param>
              </xsl:apply-templates>
              
              <xsl:if test="./SimpleLargeList/List">
                  <xsl:call-template name="MiogaMessage">
                      <xsl:with-param name="message"><xsl:call-template name="searchResultTranslation" /></xsl:with-param>
                  </xsl:call-template>
              </xsl:if>
          </div>
          
      </body>
  </xsl:template>
  
  
  <!-- =================================
       DisplayApplications
       ================================= -->
  <xsl:template match="DisplayApplications">
      <xsl:variable name="current-func" select="local-name(.)"/>
      
      <body xsl:use-attribute-sets="body_attr">
          <xsl:call-template name="DisplayAppTitle">
              <xsl:with-param name="help">animgroupe.html#application</xsl:with-param>
          </xsl:call-template>

		<!-- Status message -->
		<xsl:if test="@message != ''">
			<xsl:call-template name="InlineMessage">
				<xsl:with-param name="type" select="@type"/>
				<xsl:with-param name="message" select="@message"/>
			</xsl:call-template>
		</xsl:if>

          <xsl:call-template name="MiogaMenu"/>
          
          <xsl:call-template name="MiogaTitle" />
          
          <div xsl:use-attribute-sets="app_body_attr">
              <xsl:apply-templates select="SimpleLargeList">
                  <xsl:with-param name="label" select="name(.)"/>
              </xsl:apply-templates>
          </div>
          
      </body>
  </xsl:template>
  
  
<!-- ===========
    RightsForUser
     =========== -->
  
<xsl:template match="RightsForUser">
    <body xsl:use-attribute-sets="body_attr">
        <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">animgroupe.html#users</xsl:with-param>
        </xsl:call-template>
        <h4 class="${mioga-title-color}" style="font-weight: bold"><xsl:value-of select="mioga:gettext('Specific rights for the %s user', CurrentUser/name)"/></h4>
        
        <p style="margin: 0.5em;"><a href="ModifyProfile?rowid={Profile}"><xsl:value-of select='mioga:gettext("Direct access to user&apos;s profile")'/></a></p>
        <div class="dialog form" style="margin: 1em;">
            <div class="sbox border_color">
                <h2 class="title_bg_color"><xsl:value-of select="mioga:gettext('Paths with specific rights')"/></h2>
                <div class="content" style="padding: 0.5em">
                    <p>
                        <xsl:value-of select="mioga:gettext('Legend:')"/>&#160;
                        <img style="vertical-align: middle;" src="{$image_uri}/16x16/status/security-high.png"/><xsl:value-of select="mioga:gettext(': read/write;')"/>&#160;
                        <img style="vertical-align: middle;" src="{$image_uri}/16x16/status/security-medium.png"/><xsl:value-of select="mioga:gettext(': read only;')"/>&#160;
                        <img style="vertical-align: middle;" src="{$image_uri}/16x16/status/security-low.png"/><xsl:value-of select="mioga:gettext(': no right.')"/>
                    </p>
                    <ul class="rights">
                        <xsl:for-each select="Paths/Path">
                            <li>
                                <xsl:attribute name="class">
                                    <xsl:choose>
                                        <xsl:when test="./access_right = 'rw'">read-write</xsl:when>
                                        <xsl:when test="./access_right = 'r'">read-only</xsl:when>
                                        <xsl:otherwise>no-right</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:attribute>
                                <xsl:value-of select="./uri"/><br/>
                            </li>
                        </xsl:for-each>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</xsl:template>
  
  <!-- =================================
       DisplayThemes
       ================================= -->
<xsl:template match="DisplayThemes">
<xsl:variable name="current-func" select="local-name(.)"/>

<body>
	<xsl:call-template name="DisplayAppTitle">
		<xsl:with-param name="help">animgroupe.html#theme</xsl:with-param>
	</xsl:call-template>

	<!-- Status message -->
	<xsl:if test="@message != ''">
		<xsl:call-template name="InlineMessage">
			<xsl:with-param name="type" select="@type"/>
			<xsl:with-param name="message" select="@message"/>
		</xsl:call-template>
	</xsl:if>

	<xsl:call-template name="MiogaMenu"/>
	<xsl:call-template name="MiogaTitle" />

	<form action="DisplayThemes" method="POST" name="DisplayThemes">
		<div>
			<table>
			<tr>
				<td>
					<fieldset>
						<legend><xsl:value-of select="mioga:gettext('Themes')"/></legend>
						<xsl:for-each select="./Themes/theme">
							<p><input type="radio" name="theme_id" value="{./@rowid}">
								<xsl:if test="//fields/theme_ident = ./@ident">
									<xsl:attribute name="checked">checked</xsl:attribute>
								</xsl:if>
							</input>&#160;<xsl:value-of select="./@name" /></p>
						</xsl:for-each>
					</fieldset>
				</td>
				<td>
					<fieldset>
						<legend><xsl:value-of select="mioga:gettext('Languages')"/></legend>
							<xsl:for-each select="./Languages/lang">
								<p><input type="radio" name="lang_id" value="{./@rowid}">
									<xsl:if test="//fields/lang_ident = ./@ident">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>&#160;<xsl:value-of select="./@locale" /></p>
							</xsl:for-each>
					</fieldset>
				</td>
			</tr>
			</table>
			<ul class="button_list">
				<li>
					<input class="button" type="submit" name="select_theme_act" value="OK"/>
				</li>
			</ul>
		</div>
	</form> 
</body>
</xsl:template>


  <!-- =================================
       DisplayProfiles
       ================================= -->  
  
  <xsl:template match="DisplayProfiles">
      <xsl:variable name="current-func" select="local-name(.)"/>
      
      <body xsl:use-attribute-sets="body_attr">
          <xsl:call-template name="DisplayAppTitle">
              <xsl:with-param name="help">animgroupe.html#profile</xsl:with-param>
          </xsl:call-template>

		<!-- Status message -->
		<xsl:if test="@message != ''">
			<xsl:call-template name="InlineMessage">
				<xsl:with-param name="type" select="@type"/>
				<xsl:with-param name="message" select="@message"/>
			</xsl:call-template>
		</xsl:if>

          <xsl:call-template name="MiogaMenu"/>
          
          <xsl:call-template name="MiogaTitle" />
          
          <div xsl:use-attribute-sets="app_body_attr">

              <xsl:if test="./SimpleLargeList/List">
                  <xsl:call-template name="MiogaAction"/>
              </xsl:if>
              
              <xsl:if test="./SimpleLargeList/List">
                  <form action="DisplayProfiles" method="GET" name="DisplayProfiles">
                      <table width="90%">
                          <tr>
                              <td nowrap="nowrap" class="{$mioga-title-color}"><xsl:call-template name="defaultProfileLabel" />&#160;:&#160;</td>
                              <td nowrap="nowrap">
                                  <select name="default_profile">
                                      <xsl:for-each select="ProfileList/profile">
                                          <option value="{@rowid}">
                                              <xsl:if test="@rowid = ../../default">
                                                  <xsl:attribute name="selected">selected</xsl:attribute>
                                              </xsl:if>
                                              
                                              <xsl:value-of select="."/>
                                          </option>
                                      </xsl:for-each>
                                  </select>
                              </td>
                              <td nowrap="nowrap">
                                  <xsl:call-template name="ok-form-button">
                                      <xsl:with-param name="name">act_change_profile</xsl:with-param>
                                  </xsl:call-template>
                              </td>
                              <td width="100%">&#160;</td>
                          </tr>
                      </table>
                  </form>
              </xsl:if>
              
              <xsl:apply-templates select="SimpleLargeList">
                  <xsl:with-param name="label" select="name(.)"/>
              </xsl:apply-templates>
          </div>
          
      </body>
  </xsl:template>


  <!-- =================================
       AppList
       ================================= -->

  <xsl:template match="AppList" mode="MiogaReportBody">

      <xsl:for-each select="app[position() mod 2 = 1]">
          <xsl:call-template name="MiogaReportInputAppDesc"/>
      </xsl:for-each>

      <xsl:call-template name="MiogaFormVertSep"/>

      <xsl:for-each select="app[position() mod 2 = 0]">
          <xsl:call-template name="MiogaReportInputAppDesc"/>
      </xsl:for-each>

  </xsl:template>

  <xsl:template match="AppList" mode="MiogaReportButton">
      <xsl:call-template name="back-button">
          <xsl:with-param name="href" select="referer"/>
      </xsl:call-template>
  </xsl:template>

  <xsl:template match="AppList">
      <body xsl:use-attribute-sets="body_attr">
          <xsl:call-template name="DisplayAppTitle"/>
          <xsl:call-template name="MiogaTitle"/>
          
          <div xsl:use-attribute-sets="app_form_body_attr">
              <xsl:call-template name="MiogaReport">
                  <xsl:with-param name="label">app_list</xsl:with-param>
                  <xsl:with-param name="no-border">1</xsl:with-param>
              </xsl:call-template>
          </div>
      </body>
  </xsl:template>
  
  
  <!-- =================================
       CreateProfile
     ================================= -->

  <xsl:template match="CreateProfile|ModifyProfile" mode="arg-checker-field-name">
    <xsl:param name="name"/>
    <xsl:call-template name="FieldsName">
      <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="CreateProfile|ModifyProfile" mode="form-body">
    <tr valign="top">
      <td>
        <table cellpadding="5" cellspacing="0" border="0" class="mioga-list-title">
          <tr>
            <td nowrap="nowrap">
              <xsl:call-template name="profileFormsTranslation">
                <xsl:with-param name="type">users</xsl:with-param>
              </xsl:call-template>
            </td>
            <td>
              <xsl:call-template name="add-form-button">
                <xsl:with-param name="name">act_add_user</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr valign="top">
            <td align="center" colspan="2">
              <xsl:choose>
                <xsl:when test="count(./UsersList/SimpleLargeList/List/Row) = 0">
                  <i>
                    <xsl:call-template name="profileFormsTranslation">
                      <xsl:with-param name="type">no_user</xsl:with-param>
                    </xsl:call-template>
                  </i>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:apply-templates select="UsersList/SimpleLargeList">
                      <xsl:with-param name="tablewidth">90%</xsl:with-param>
                      <xsl:with-param name="bottomindex">
                          <xsl:choose>
                              <xsl:when test="./UsersList/SimpleLargeList/List/@nb_elems &gt; ./UsersList/SimpleLargeList/List/@nb_elem_per_page">1</xsl:when>
                              <xsl:otherwise>0</xsl:otherwise>
                          </xsl:choose>
                      </xsl:with-param>
                      <xsl:with-param name="no_form">1</xsl:with-param>
                  </xsl:apply-templates>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr>
            <td>&#160;</td>
          </tr>
          <tr>
            <td nowrap="nowrap">
              <xsl:call-template name="profileFormsTranslation">
                <xsl:with-param name="type">teams</xsl:with-param>
              </xsl:call-template>
            </td>
            <td>
              <xsl:call-template name="add-form-button">
                <xsl:with-param name="name">act_add_team</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr valign="top">
            <td align="center" colspan="2">
              <xsl:choose>
                <xsl:when test="count(./TeamsList/SimpleLargeList/List/Row) = 0">
                  <i>
                    <xsl:call-template name="profileFormsTranslation">
                      <xsl:with-param name="type">no_team</xsl:with-param>
                    </xsl:call-template>
                  </i>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:apply-templates select="TeamsList/SimpleLargeList">
                    <xsl:with-param name="tablewidth">90%</xsl:with-param>
                    <xsl:with-param name="bottomindex">
                      <xsl:choose>
                        <xsl:when test="./TeamsList/SimpleLargeList/List/@nb_elems &gt; ./TeamsList/SimpleLargeList/List/@nb_elem_per_page">1</xsl:when>
                        <xsl:otherwise>0</xsl:otherwise>
                      </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="no_form">1</xsl:with-param>
                  </xsl:apply-templates>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </table>
      </td>
      <td class="mioga-title" width="1" style="padding: 0" >
        <img src="{$image_uri}/transparent_fill.gif" width="1" border="0"/>
      </td>
      <td>
        <table align="center">
          <tr>
            <td align="center">
              <xsl:call-template name="FieldsName">
                <xsl:with-param name="name">profile</xsl:with-param>
              </xsl:call-template>&#160;:
            </td>
            
            <td align="center">
              <xsl:choose>
                <xsl:when test="local-name(.) = 'CreateProfile'">
                  <input type="text" name="ident" value="{fields/ident}"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="fields/ident"/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          
          <tr>
            <td colspan="2" align="center">&#160;</td>
          </tr>
          
          <tr>
            <td colspan="2" align="center">
              <table>
                <xsl:for-each select="./Applications/app">
                  <xsl:sort select="count(func)" data-type="number" order="descending"/>
                  
                  <xsl:if test="position() mod 2 = 1">
                    <xsl:text disable-output-escaping="yes">&lt;tr valign="top" &gt;</xsl:text>
                  </xsl:if>
                  
                  <td>
                  <table xsl:use-attribute-sets="mioga-content-table" align="center">
                      <tr>
                        <td colspan="2">
                          <font class="{$mioga-list-title-text-color}">
                            <xsl:value-of select="@name"/>
                          </font>
                        </td>
                      </tr>
                      
                      <xsl:for-each select="func">
                        <tr align="center">
                          <xsl:choose>
                            <xsl:when test="position() mod 2 = 1">
							  <xsl:attribute name="class">odd</xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
							  <xsl:attribute name="class">even</xsl:attribute>
                            </xsl:otherwise>
                          </xsl:choose>
                          
                          <td><xsl:value-of select="."/></td>
                          <td>
                            <xsl:variable name="appid" select="../@rowid"/>
                            <xsl:variable name="funcname" select="./@ident"/>
                            
                            <input type="checkbox" name="select_function_{$appid}_{$funcname}">
                              <xsl:if test="../../../fields/Applications/app[attribute::rowid=$appid]/func = $funcname">
                                <xsl:attribute name="checked">checked</xsl:attribute>
                              </xsl:if>
                            </input>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </table>
                  </td> 
                  
                  <xsl:if test="position() mod 2 = 0">
                    <xsl:text disable-output-escaping="yes">&lt;/tr&gt;</xsl:text>
                  </xsl:if>
                  
                </xsl:for-each>
                
                <xsl:if test="count(./Applications/app) mod 2 = 1">
                  <xsl:text disable-output-escaping="yes">&lt;/tr&gt;</xsl:text>
                </xsl:if>
                
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </xsl:template>
  
  <xsl:template match="CreateProfile" mode="form-button">
    <tr>
      <td colspan="3" align="center">
        <xsl:call-template name="ok-cancel-form-button">
          <xsl:with-param name="name">profile_create_act</xsl:with-param>
          <xsl:with-param name="referer" select="referer"/>
        </xsl:call-template>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="ModifyProfile" mode="form-button">
    <tr>
      <td colspan="3" align="center">
        <xsl:call-template name="ok-cancel-form-button">
          <xsl:with-param name="name">profile_modify_act</xsl:with-param>
          <xsl:with-param name="referer" select="referer"/>
        </xsl:call-template>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="CreateProfile|ModifyProfile">
    <xsl:variable name="current-func" select="local-name(.)"/>
    
    <xsl:variable name="form-title">
      <xsl:apply-templates select="." mode="form-title"/>
    </xsl:variable>
    
    <body xsl:use-attribute-sets="body_attr">
      <xsl:call-template name="DisplayAppTitle">
          <xsl:with-param name="help">animgroupe.html#profile</xsl:with-param>
      </xsl:call-template>
      
      <xsl:call-template name="MiogaTitle" />
      
      <div xsl:use-attribute-sets="app_body_attr">
          <xsl:call-template name="arg_errors"/>
          
          <xsl:call-template name="form">
              <xsl:with-param name="name" select="$form-title"/>
              <xsl:with-param name="action" select="$current-func"/>
              <xsl:with-param name="nbcols">3</xsl:with-param>
              <xsl:with-param name="method">GET</xsl:with-param>
          </xsl:call-template>
      </div>
    </body>
  </xsl:template>

<xsl:template match="DisplayProfileUsers|DisplayProfileTeams">
    <xsl:variable name="current-func" select="local-name(.)"/>
    
    <body xsl:use-attribute-sets="body_attr">
      <xsl:call-template name="DisplayAppTitle">
          <xsl:with-param name="help">animgroupe.html#profile</xsl:with-param>
      </xsl:call-template>

		<!-- Status message -->
		<xsl:if test="@message != ''">
			<xsl:call-template name="InlineMessage">
				<xsl:with-param name="type" select="@type"/>
				<xsl:with-param name="message" select="@message"/>
			</xsl:call-template>
		</xsl:if>

      <xsl:call-template name="MiogaTitle" />
      
      <div xsl:use-attribute-sets="app_body_attr">
          <xsl:if test="./SimpleLargeList/List">
              <xsl:call-template name="MiogaAction"/>
          </xsl:if>
      
          <xsl:apply-templates select="SimpleLargeList">
              <xsl:with-param name="advancedsearch">1</xsl:with-param>
              <xsl:with-param name="label" select="name(.)"/>
          </xsl:apply-templates>
      </div>
  </body>
</xsl:template>

  <!-- =================================
       Search form title
       ================================= -->

  <xsl:template name="SllSearchFormTitle">
    <xsl:variable name="current-func" select="local-name(../..)"/>
  </xsl:template>

  <!-- =================================
       Search form body add-ons
       ================================= -->

  <xsl:template match="DisplayTeams|DisplayProfileTeams|DisplayProfileUsers" mode="advanced-search-field-content">
      <xsl:param name="name"/>
      <xsl:param name="value"/>
      
      <xsl:call-template name="SllAdvSearchFieldContent">
          <xsl:with-param name="name" select="$name"/>
          <xsl:with-param name="value" select="$value"/>
      </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="DisplayUsers" mode="advanced-search-field-content">
      <xsl:param name="name"/>
      <xsl:param name="value"/>
      
      <xsl:choose>
          <xsl:when test="$name='sll_search_status'">
              <xsl:call-template name="MiogaFormInputSelect">
                  <xsl:with-param name="label">sll_search_status</xsl:with-param>
                  <xsl:with-param name="value" select="$value"/>
                  <xsl:with-param name="choices" select="UserStatus/status"/>
                  <xsl:with-param name="empty-choice">1</xsl:with-param>
              </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
              <xsl:call-template name="SllAdvSearchFieldContent">
                  <xsl:with-param name="name" select="$name"/>
                  <xsl:with-param name="value" select="$value"/>
              </xsl:call-template>
          </xsl:otherwise>
      </xsl:choose>
  </xsl:template>
  
  <xsl:template match="DisplayResources" mode="advanced-search-field-content">
      <xsl:param name="name"/>
      <xsl:param name="value"/>
      
      <xsl:choose>
          <xsl:when test="$name='sll_search_status'">

              <xsl:call-template name="MiogaFormInputSelect">
                  <xsl:with-param name="label">sll_search_status</xsl:with-param>
                  <xsl:with-param name="value" select="$value"/>
                  <xsl:with-param name="choices" select="ResourceStatus/status"/>
                  <xsl:with-param name="empty-choice">1</xsl:with-param>
              </xsl:call-template>
              
          </xsl:when>
          
          <xsl:otherwise>
              <xsl:call-template name="SllAdvSearchFieldContent">
                  <xsl:with-param name="name" select="$name"/>
                  <xsl:with-param name="value" select="$value"/>
              </xsl:call-template>
          </xsl:otherwise>
      </xsl:choose>
  </xsl:template>
  

  <!-- =================================
       MemberList
       ================================= -->
  
  <xsl:template match="MemberList">
      <body xsl:use-attribute-sets="body_attr">
          
          <xsl:call-template name="DisplayAppTitle"/>
          
          <xsl:call-template name="MiogaTitle" />
          
          <div xsl:use-attribute-sets="app_body_attr">
          
              <xsl:apply-templates select="SimpleLargeList">
                  <xsl:with-param name="advancedsearch">1</xsl:with-param>
                  <xsl:with-param name="label" select="name(.)"/>
              </xsl:apply-templates>
              
              <xsl:call-template name="MiogaMessage">
                  <xsl:with-param name="message"><xsl:call-template name="searchResultTranslation" /></xsl:with-param>
              </xsl:call-template>
          </div>
          
      </body>
  </xsl:template>
  
  <xsl:template match="MemberList" mode="sll-field-name">
  	<xsl:param name="name"/>
	<xsl:call-template name="mioga2-common-name-translation">
	  <xsl:with-param name="name" select="$name"/>
	</xsl:call-template>
  </xsl:template>

  <xsl:template match="MemberList" mode="sll-translate-values">
      <xsl:param name="name"/>
      <xsl:param name="value"/> 
      <xsl:param name="row"/> 

      <xsl:value-of select="$value"/>
      
  </xsl:template>

  <xsl:template match="MemberList" mode="advanced-search-field-content">
      <xsl:param name="name"/>
      <xsl:param name="value"/>
      
      <xsl:call-template name="SllAdvSearchFieldContent">
          <xsl:with-param name="name" select="$name"/>
          <xsl:with-param name="value" select="$value"/>
      </xsl:call-template>
      
  </xsl:template>
  


  <!-- =================================
       SuccessMessage
       ================================= -->
  
  <xsl:template match="SuccessMessage">
      <xsl:variable name="message">
        <xsl:call-template name="successMessageTranslation" />
      </xsl:variable>

      <xsl:choose>
        <xsl:when test="name='theme' or name='applications'">
          <xsl:call-template name="SuccessMessage">
            <xsl:with-param name="message" select="$message"/>
            <xsl:with-param name="referer"><xsl:value-of select="referer"/></xsl:with-param>
            <xsl:with-param name="reload_menu">1</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="SuccessMessage">
            <xsl:with-param name="message" select="$message"/>
            <xsl:with-param name="referer"><xsl:value-of select="referer"/></xsl:with-param>
            <xsl:with-param name="reload_menu">1</xsl:with-param>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
  </xsl:template>

  <!-- Action box translation -->
  <xsl:template match="DisplayUsers" mode="MiogaActionBody">
    <xsl:call-template name="MiogaActionItem">
      <xsl:with-param name="label">InviteUsers</xsl:with-param>
      <xsl:with-param name="href">InviteUsers</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="DisplayProfiles" mode="MiogaActionBody">
    <xsl:call-template name="MiogaActionItem">
      <xsl:with-param name="label">CreateProfile</xsl:with-param>
      <xsl:with-param name="href">CreateProfile</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="DisplayTeams" mode="MiogaActionBody">
    <xsl:call-template name="MiogaActionItem">
      <xsl:with-param name="label">InviteTeams</xsl:with-param>
      <xsl:with-param name="href">InviteTeams</xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="DisplayResources" mode="MiogaActionBody">
    <xsl:call-template name="MiogaActionItem">
      <xsl:with-param name="label">AddResources</xsl:with-param>
      <xsl:with-param name="href">AddResources</xsl:with-param>
    </xsl:call-template>
  </xsl:template>
    
  <xsl:template match="DisplayProfileUsers" mode="MiogaActionBody">  
    <xsl:call-template name="MiogaActionItem">
      <xsl:with-param name="label">InviteProfileUsers</xsl:with-param>
      <xsl:with-param name="href">InviteProfileUsers</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaActionItem">
      <xsl:with-param name="label">Back</xsl:with-param>
      <xsl:with-param name="href" select="referer"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="DisplayProfileTeams" mode="MiogaActionBody">
    <xsl:call-template name="MiogaActionItem">
      <xsl:with-param name="label">InviteProfileTeams</xsl:with-param>
      <xsl:with-param name="href">InviteProfileTeams</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaActionItem">
      <xsl:with-param name="label">Back</xsl:with-param>
      <xsl:with-param name="href" select="referer"/>
    </xsl:call-template>
  </xsl:template>
  
  <!-- =======================
       == TRANSLATIONS - FR ==
       ======================= -->
  <xsl:template name="MiogaActionTranslation">
    <xsl:param name="label"/>

    <xsl:choose>
      <xsl:when test="$label='InviteUsers'"><xsl:value-of select="mioga:gettext('Invite users')"/></xsl:when>
      <xsl:when test="$label='CreateProfile'"><xsl:value-of select="mioga:gettext('Add profile')"/></xsl:when>
      <xsl:when test="$label='InviteTeams'"><xsl:value-of select="mioga:gettext('Invite teams')"/></xsl:when>
      <xsl:when test="$label='AddResources'"><xsl:value-of select="mioga:gettext('Add resource')"/></xsl:when>
      <xsl:when test="$label='InviteProfileUsers'"><xsl:value-of select="mioga:gettext('Add users')"/></xsl:when>
      <xsl:when test="$label='Back'"><xsl:value-of select="mioga:gettext('Return to previous page')"/></xsl:when>
      <xsl:when test="$label='InviteProfileTeams'"><xsl:value-of select="mioga:gettext('Add teams')"/></xsl:when>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="MiogaMenuTranslation">
    <xsl:param name="label" />
    <xsl:choose>
      <xsl:when test="$label='DisplayUsers'"><xsl:value-of select="mioga:gettext('Users')"/></xsl:when>
      <xsl:when test="$label='DisplayTeams'"><xsl:value-of select="mioga:gettext('Teams')"/></xsl:when>
      <xsl:when test="$label='DisplayResources'"><xsl:value-of select="mioga:gettext('Resources')"/></xsl:when>
      <xsl:when test="$label='DisplayThemes'"><xsl:value-of select="mioga:gettext('Themes and languages')"/></xsl:when>
      <xsl:when test="$label='DisplayProfiles'"><xsl:value-of select="mioga:gettext('Profiles')"/></xsl:when>
      <xsl:when test="$label='DisplayApplications'"><xsl:value-of select="mioga:gettext('Applications')"/></xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="MiogaTitleTranslation">
    <xsl:choose>
      <xsl:when test="local-name(.) = 'DisplayUsers'"><xsl:value-of select="mioga:gettext('Users management')"/></xsl:when>
      <xsl:when test="local-name(.) = 'DisplayTeams'"><xsl:value-of select="mioga:gettext('Teams management')"/></xsl:when>
      <xsl:when test="local-name(.) = 'DisplayResources'"><xsl:value-of select="mioga:gettext('Resources management')"/></xsl:when>
      <xsl:when test="local-name(.) = 'DisplayApplications'"><xsl:value-of select="mioga:gettext('Applications management')"/></xsl:when>
      <xsl:when test="local-name(.) = 'DisplayThemes'"><xsl:value-of select="mioga:gettext('Themes and languages management')"/></xsl:when>
      <xsl:when test="local-name(.) = 'DisplayProfiles'"><xsl:value-of select="mioga:gettext('Profiles management')"/></xsl:when>
      <xsl:when test="local-name(.) = 'ModifyProfile'"><xsl:value-of select="mioga:gettext('Modifying profile %s', string(fields/ident))"/></xsl:when>
      <xsl:when test="local-name(.) = 'CreateProfile'"><xsl:value-of select="mioga:gettext('Creating profile')"/></xsl:when>
      <xsl:when test="local-name(.) = 'DisplayProfileUsers'"><xsl:value-of select="mioga:gettext('Users profile management')"/></xsl:when>
      <xsl:when test="local-name(.) = 'DisplayProfileTeams'"><xsl:value-of select="mioga:gettext('Teams profile management')"/></xsl:when>
      <xsl:when test="local-name(.) = 'AppList'">&#160;</xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="successMessageTranslation">
    <xsl:choose>
      <xsl:when test="name='team' or name='anim_teams_sll' or name='anim_profiles_teams_sll'"><xsl:value-of select="mioga:gettext('Team')"/></xsl:when>
      <xsl:when test="name='user' or name='anim_users_sll' or name='anim_profiles_users_sll'"><xsl:value-of select="mioga:gettext('User')"/></xsl:when>
      <xsl:when test="name='theme'"><xsl:value-of select="mioga:gettext('Theme')"/></xsl:when>
      <xsl:when test="name='profile' or name='anim_profiles_sll'"><xsl:value-of select="mioga:gettext('Profile')"/></xsl:when>
      <xsl:when test="name='resource' or name='anim_resources_sll'"><xsl:value-of select="mioga:gettext('Resource')"/></xsl:when>
      <xsl:when test="name='application' or name='anim_application_sll'"><xsl:value-of select="mioga:gettext('Application')"/></xsl:when>
      <xsl:when test="name='applications'"><xsl:value-of select="mioga:gettext('Applications')"/></xsl:when>
    </xsl:choose>
    <xsl:text>&#160;</xsl:text>
    <xsl:choose>
      <xsl:when test="action='create'"><xsl:value-of select="mioga:gettext('has been created')"/></xsl:when>
      <xsl:when test="action='modify'"><xsl:value-of select="mioga:gettext('has been modified')"/></xsl:when>
      <xsl:when test="action='remove'"><xsl:value-of select="mioga:gettext('has been deleted')"/></xsl:when>
      <xsl:when test="action='invite'"><xsl:value-of select="mioga:gettext('has been invited')"/></xsl:when>
      <xsl:when test="action='allow'"><xsl:value-of select="mioga:gettext('has been enabled')"/></xsl:when>
      <xsl:when test="action='disallow'"><xsl:value-of select="mioga:gettext('has been disabled')"/></xsl:when>
      <xsl:when test="action='changestatus'"><xsl:value-of select="mioga:gettext('have been enabled/disabled')"/></xsl:when>
    </xsl:choose>
    <xsl:if test="name='resource' or name='anim_resources_sll' or name='team' or name='anim_teams_sll' or name='anim_profiles_teams_sll'"></xsl:if>
    &#160;<xsl:value-of select="mioga:gettext('successfully.')"/>
  </xsl:template>

  <xsl:template name="searchResultTranslation">
    <xsl:choose>
      <xsl:when test="name(.) ='DisplayUsers' and count(SimpleLargeList/AskConfirmForDelete) = 0 and count(SimpleLargeList/List/Row) = 0">
        <xsl:value-of select="mioga:gettext('No user to display')"/>
      </xsl:when>
      <xsl:when test="name(.)='DisplayTeams' and count(SimpleLargeList/AskConfirmForDelete) = 0 and count(SimpleLargeList/List/Row) = 0">
        <xsl:value-of select="mioga:gettext('No team to display')"/>
      </xsl:when>
      <xsl:when test="name(.)='DisplayResources' and count(SimpleLargeList/AskConfirmForDelete) = 0 and count(SimpleLargeList/List/Row) = 0">
        <xsl:value-of select="mioga:gettext('No resource to display')"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="themeNameTranslation">
    <xsl:value-of select="mioga:gettext('Theme: %s', string(@name))"/>
  </xsl:template>

  <xsl:template name="ChangeProfileTranslation">
    <xsl:value-of select="mioga:gettext('Change profile')"/>
  </xsl:template>

  <xsl:template name="fieldsNameTranslation">
    <xsl:param name="type" />
    <xsl:choose>
      <xsl:when test="$type='nb_number'"><xsl:value-of select="mioga:gettext('Members number')"/></xsl:when>
      <xsl:when test="$type='profile'"><xsl:value-of select="mioga:gettext('Profile')"/></xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="defaultProfileLabel">
    <xsl:value-of select="mioga:gettext('Initial profile')"/>
  </xsl:template>

  <xsl:template match="CreateProfile|ModifyProfile" mode="arg-checker-unknown-args">
    <xsl:param name="name"/>
    <xsl:param name="arg"/>
    
    <xsl:choose>
      <xsl:when test="$name = '__create__'"><xsl:value-of select="mioga:gettext('Error while creating profile: %s.', string($arg))"/></xsl:when>
      <xsl:when test="$name = '__modify__'"><xsl:value-of select="mioga:gettext('Error while modifying profile: %s.', string($arg))"/></xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="profileFormsTranslation">
    <xsl:param name="type"/>
    
    <xsl:choose>
      <xsl:when test="$type='users'"><xsl:value-of select="mioga:gettext('Users:')"/>&#160;</xsl:when>
      <xsl:when test="$type='no_user'"><xsl:value-of select="mioga:gettext('No user')"/></xsl:when>
      <xsl:when test="$type='teams'"><xsl:value-of select="mioga:gettext('Teams:')"/>&#160;</xsl:when>
      <xsl:when test="$type='no_team'"><xsl:value-of select="mioga:gettext('No team')"/></xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="CreateProfile" mode="form-title">
    <xsl:value-of select="mioga:gettext('Create profile:')"/>
  </xsl:template>
  
  <xsl:template match="ModifyProfile" mode="form-title">
    <xsl:value-of select="mioga:gettext('Modify profile:')"/>
  </xsl:template>

  <!-- forms translations -->
  <xsl:template name="MiogaFormTranslation">
      <xsl:param name="label"/>

      <xsl:choose>
          <xsl:when test="$label='DisplayProfileUsers'"><xsl:value-of select="mioga:gettext('Search for user:')"/></xsl:when>
          <xsl:when test="$label='DisplayProfileTeams'"><xsl:value-of select="mioga:gettext('Search for team:')"/></xsl:when>
          <xsl:when test="$label='DisplayUsers'"><xsl:value-of select="mioga:gettext('Search for user:')"/></xsl:when>
          <xsl:when test="$label='DisplayTeams'"><xsl:value-of select="mioga:gettext('Search for team:')"/></xsl:when>
          <xsl:when test="$label='DisplayResources'"><xsl:value-of select="mioga:gettext('Search for resource:')"/></xsl:when>
          <xsl:when test="$label='GroupList'"><xsl:value-of select="mioga:gettext('Search for group:')"/></xsl:when>
          <xsl:when test="$label='MemberList'"><xsl:value-of select="mioga:gettext('Search for member:')"/></xsl:when>
          <xsl:otherwise><xsl:value-of select="mioga:gettext('Search for:')"/></xsl:otherwise>
      </xsl:choose>&#160;
      
  </xsl:template>


  <!-- reports translations -->
  <xsl:template name="MiogaReportTranslation">
      <xsl:param name="label"/>

      <xsl:choose>
          <xsl:when test="$label='app_list'"><xsl:value-of select="mioga:gettext('Applications list:')"/></xsl:when>
      </xsl:choose>&#160;
      
  </xsl:template>



<!-- DisplayUsers field value translation -->  

  <xsl:template match="DisplayUsers|DisplayTeams|DisplayResources|DisplayApplications|DisplayProfiles|DisplayThemes|DisplayProfileUsers|DisplayProfileTeams|TeamsList|UsersList" mode="sll-translate-values" name="TranslateValues">
    <xsl:param name="name"/>
    <xsl:param name="value"/> 
    <xsl:param name="row"/> 
    
    <xsl:choose>
      <xsl:when test="local-name(.) = 'DisplayApplications' and $name = 'is_public'">
        <xsl:choose>
          <xsl:when test="$value!=0"><xsl:value-of select="mioga:gettext('Yes')"/></xsl:when>
          <xsl:otherwise><xsl:value-of select="mioga:gettext('No')"/></xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="mioga2-common-value-translation">
          <xsl:with-param name="name" select="$name"/>
          <xsl:with-param name="value" select="$value"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="MiogaFormInputSelectTranslation">
      <xsl:param name="label"/>
      <xsl:param name="value"/>

      <xsl:choose>
          <xsl:when test="starts-with($label, 'sll_search_')">
              <xsl:variable name="name" select="substring-after($label, 'sll_search_')"/>
              
              <xsl:call-template name="mioga2-common-value-translation">
                  <xsl:with-param name="name" select="$name"/>
                  <xsl:with-param name="value" select="$value"/>
              </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
              <xsl:value-of select="$value"/>            
          </xsl:otherwise>
    </xsl:choose>
      
  </xsl:template>

  <xsl:template name="MiogaFormInputTranslation">
      <xsl:param name="label"/>
      
      <xsl:choose>
        <xsl:when test="starts-with($label, 'sll_search_')">
            <xsl:variable name="name" select="substring-after($label, 'sll_search_')"/>
                
            <xsl:call-template name="mioga2-common-name-translation">
                <xsl:with-param name="name" select="$name"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="mioga2-common-name-translation">
                <xsl:with-param name="name" select="$label"/>
            </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
  </xsl:template>



</xsl:stylesheet>
