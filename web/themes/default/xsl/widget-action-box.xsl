<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
  
<!--  <xsl:output method="html"/> -->

  <!-- =================================
       Translation
       ================================= -->
  
  <xsl:template name="MiogaActionHeaderTranslation">
    <xsl:value-of select="mioga:gettext('Actions:')"/>&#160;
  </xsl:template>


  <!-- =================================
       MiogaAction
       ================================= -->  
  
  <xsl:template name="MiogaAction">
      <xsl:param name="name"/>
      <xsl:param name="wrap">0</xsl:param>

      <xsl:call-template name="MiogaBox">
          <xsl:with-param name="wrap" select="$wrap"/>
          <xsl:with-param name="title"><xsl:call-template name="MiogaActionHeaderTranslation"/></xsl:with-param>
          <xsl:with-param name="body">
              <table xsl:use-attribute-sets="mioga-empty-table-100" cellpadding="5">
                  <xsl:apply-templates select="." mode="MiogaActionBody">
                      <xsl:with-param name="name" select="$name"/>
                  </xsl:apply-templates>
              </table>
          </xsl:with-param>
      </xsl:call-template>

  </xsl:template>
  

  <!-- =================================
       MiogaActionItem
       ================================= -->  

  <xsl:template name="MiogaActionItem">
      <xsl:param name="label"/>
      <xsl:param name="href"/>
      
      <tr>
          <td><img src="{$image_uri}/16x16/actions/arrow-right.png"/></td>
          <td nowrap="nowrap">
              <a href="{$href}">
                  <xsl:call-template name="MiogaActionTranslation">
                      <xsl:with-param name="label" select="$label"/>
                  </xsl:call-template>
              </a>
          </td>
      </tr>
  </xsl:template>
  
</xsl:stylesheet>
