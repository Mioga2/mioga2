<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ext="urn:ext" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="text" indent="no" omit-xml-declaration="yes" encoding="UTF-8" media-type="text/javascript"/>

<xsl:variable name="total_mime" select="count(//IncludeMime/mimetype)"/>

<xsl:template match="/">
var mimetypes = {<xsl:apply-templates />};
</xsl:template>

<xsl:template match="IncludeMime"><xsl:apply-templates select="mimetype"/></xsl:template>

<xsl:template match="mimetype">'<xsl:value-of select="type"/>':{image:'<xsl:value-of select="class"/>',description:"<xsl:value-of select='description'/>"}<xsl:if test="position() &lt; $total_mime">,</xsl:if></xsl:template>

</xsl:stylesheet>
