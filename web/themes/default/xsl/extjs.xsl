<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="base.xsl"/>

<!-- ==========
  extjs
  to include extjs in HTML HEAD element
  ========== -->

<xsl:template name="extjs">
  <script type="text/javascript" src="{$jslib_uri}/extjs/adapter/ext/ext-base.js"></script>
  <script type="text/javascript" src="{$jslib_uri}/extjs/ext-all.js"></script>
  <script type="text/javascript" src="{$jslib_uri}/extjs/ext-base-addon.js"></script>
  <script type="text/javascript" src="{$jslib_uri}/extjs/build/locale/ext-lang-{//miogacontext/group/lang}.js"></script>
  <script type="text/javascript" src="{$jslib_uri}/extjs/Ext.ux.form.Checkbox.js"></script>
  <script type="text/javascript" src="{$jslib_uri}/extjs/Ext.ux.form.Radio.js"></script>
  <link rel="stylesheet" type="text/css" href="{$jslib_uri}/extjs/resources/css/ext-all.css" media="screen"/>
  <link rel="stylesheet" type="text/css" href="{$jslib_uri}/extjs/resources/css/checkbox.css" media="screen"/>
  <script type="text/javascript">
    var extjs_path = "<xsl:value-of select='$jslib_uri'/>/extjs";
    var image_path = "<xsl:value-of select='$theme_uri'/>/images";
  </script>
</xsl:template>

<xsl:template name="extjs-debug">
  <script type="text/javascript" src="{$jslib_uri}/extjs/adapter/ext/ext-base.js"></script>
  <script type="text/javascript" src="{$jslib_uri}/extjs/ext-all-debug.js"></script>
  <script type="text/javascript" src="{$jslib_uri}/extjs/build/locale/ext-lang-fr.js"></script>
  <link rel="stylesheet" type="text/css" href="{$jslib_uri}/extjs/resources/css/ext-all.css" media="screen"/>
  <script type="text/javascript">
    var extjs_path = "<xsl:value-of select='$jslib_uri'/>/extjs";
    var image_path = "<xsl:value-of select='$theme_uri'/>/images";
  </script>
</xsl:template>

</xsl:stylesheet>
