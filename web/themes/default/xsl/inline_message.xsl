<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- ===============================
	inline-message
	================================ -->

<xsl:template name="InlineMessage">
	<xsl:param name="message"/>
	<xsl:param name="type">info</xsl:param>

	<div position="relative" height="0px">
		<div id="InlineMessage" class="inline-message">
			<p class="{$type}"><xsl:value-of select="$message"/></p>
			<div class="close" onclick="hide_inline_message ();"></div>
		</div>
	</div>

	<script language="JavaScript">
		<xsl:if test="$type!='error'">
			setTimeout("hide_inline_message ()", 3000);
		</xsl:if>
	</script>

</xsl:template>



</xsl:stylesheet>
