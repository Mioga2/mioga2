<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
							xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>

<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" encoding="UTF-8"/>

<xsl:include href="scriptaculous.xsl"/>
<!-- ===============
	Variables
	================ -->

<xsl:variable name="anim_action"><xsl:value-of select="$private_bin_uri" />/Forum/ProcessAnimAction</xsl:variable>
<xsl:variable name="main_action"><xsl:value-of select="$private_bin_uri" />/Forum/ProcessMainAction</xsl:variable>
<xsl:variable name="view_action"><xsl:value-of select="$private_bin_uri" />/Forum/ProcessViewAction</xsl:variable>
<xsl:variable name="anim_right">
	<xsl:choose>
		<xsl:when test="//UserParams/anim_right">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="moder_right">
	<xsl:choose>
		<xsl:when test="//UserParams/moder_right">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="write_right">
	<xsl:choose>
		<xsl:when test="//UserParams/write_right">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<!-- ===============
	root match for AJAX response
	================ -->

<xsl:template match="/">
	<xsl:apply-templates />
</xsl:template>

<xsl:template match="AddCategory" >
	<xsl:apply-templates select="category" />
</xsl:template>

<xsl:template match="AddThematic" >
	<xsl:apply-templates select="thematic" />
</xsl:template>

<!-- ===============
	Box for one category
	================ -->

<xsl:template name="category_header">
	<xsl:variable name="category_rowid"><xsl:value-of select="./@rowid" /></xsl:variable>
	<xsl:variable name="edit_id">cat<xsl:value-of select="./@rowid" />_edit</xsl:variable>
	<xsl:variable name="thm_list_id">thm_list<xsl:value-of select="./@rowid" /></xsl:variable>
	<div class="menu_category">
		<ul class="hmenu">
		<xsl:if test="$anim_right='1'">
			<li>
				<a href="#" class="action" id="add_thematic" >
					<xsl:attribute name="onclick">AddThematic('<xsl:value-of select="$anim_action" />?category_id=<xsl:value-of select="$category_rowid" />', '<xsl:value-of select="$thm_list_id" />')</xsl:attribute>
					<xsl:value-of select="//action[@id = 'add_thematic']/@label" />
				</a>
			</li>
			<xsl:if test="count(thematic)='0'">
				<li>
					<a href="#" class="action" >
						<xsl:attribute name="onclick">ProcessAnimAction('<xsl:value-of select="$anim_action" />?action=suppress_category&amp;rowid=<xsl:value-of select="$category_rowid" />')</xsl:attribute>
						<img src="{$image_uri}/16x16/actions/trash-empty.png" >
							<xsl:attribute name="alt"><xsl:value-of select="//action[@id = 'suppress_category']/@label" /></xsl:attribute>
							<xsl:attribute name="title"><xsl:value-of select="//action[@id = 'suppress_category']/@label" /></xsl:attribute>
						</img>
					</a>
				</li>
			</xsl:if>
		</xsl:if>
		</ul>
	</div>
	<h2 id="{$edit_id}" class="title_bg_color"><xsl:value-of select="./@label" /></h2>
	<xsl:if test="$anim_right='1'">
		<xsl:call-template name="inplace-editor">
			<xsl:with-param name="element"><xsl:value-of select="$edit_id" /></xsl:with-param>
			<xsl:with-param name="url"><xsl:value-of select="$private_bin_uri" />/Forum/ProcessAnimAction?action=rename_category&amp;rowid=<xsl:value-of select="$category_rowid" /></xsl:with-param>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<!-- ===============
	category
	================ -->

<xsl:template match="category">
	<xsl:variable name="category_rowid"><xsl:value-of select="./@rowid" /></xsl:variable>
	<xsl:variable name="thm_list_id">thm_list<xsl:value-of select="./@rowid" /></xsl:variable>
	<div class="cat_view sbox border_color">
		<xsl:call-template name="category_header" />
		<table id="{$thm_list_id}" class="thematic_list">
		<tr>
			<th class="border_color"><xsl:value-of select="mioga:gettext('Thematic')" /></th>
			<th class="border_color"><xsl:value-of select="mioga:gettext('Nb subjects')" /></th>
			<th class="border_color"><xsl:value-of select="mioga:gettext('Nb messages')" /></th>
			<th class="border_color"><xsl:value-of select="mioga:gettext('Last post')" /></th>
		</tr>
		<xsl:apply-templates />
		</table>
	</div>
</xsl:template>

<!-- ===============
	thematic
	================ -->

<xsl:template match="thematic">
	<xsl:variable name="category_rowid"><xsl:value-of select="./category_id" /></xsl:variable>
	<xsl:variable name="thmedit_id">thm<xsl:value-of select="./rowid" />_edit</xsl:variable>
	<xsl:variable name="thmedit_id_ext"><xsl:value-of select="$thmedit_id" />_ext</xsl:variable>
	<xsl:variable name="thmdescedit_id">thmdesc<xsl:value-of select="./rowid" />_edit</xsl:variable>
	<tr>
		<xsl:attribute name="class">
			<xsl:choose>
				<xsl:when test='position() mod 2 = 0'>odd</xsl:when>
				<xsl:otherwise>even</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<td class="label border_color">
			<div class="menu_thematic">
				<ul class="hmenu">
					<xsl:if test="$anim_right='1'">
						<li>
							<img id="{$thmedit_id_ext}" src="{$image_uri}/16x16/actions/document-edit.png" title="Edit" alt="Edit" />
						</li>
						<xsl:if test="./nb_subjects='0'">
							<li>
								<a class="action" href="#">
									<xsl:attribute name="onclick">ProcessAnimAction('<xsl:value-of select="$anim_action" />?action=suppress_thematic&amp;rowid=<xsl:value-of select="./rowid" />&amp;category_id=<xsl:value-of select="$category_rowid" />')</xsl:attribute>
									<img src="{$image_uri}/16x16/actions/trash-empty.png" >
										<xsl:attribute name="alt"><xsl:value-of select="//action[@id = 'suppress_thematic']/@label" /></xsl:attribute>
										<xsl:attribute name="title"><xsl:value-of select="//action[@id = 'suppress_thematic']/@label" /></xsl:attribute>
									</img>
								</a>
							</li>
						</xsl:if>
					</xsl:if>
				</ul>
			</div>
			<h3>
				<a class="action" id="{$thmedit_id}" >
					<xsl:attribute name="href"><xsl:value-of select="$private_bin_uri" />/Forum/DisplayThematic?thematic_id=<xsl:value-of select="./rowid" />&amp;category_id=<xsl:value-of select="$category_rowid" /></xsl:attribute>
					<xsl:value-of select="./label" />
				</a>
			</h3>
			<xsl:if test="$anim_right='1'">
				<xsl:call-template name="inplace-editor-extcontrol">
					<xsl:with-param name="element"><xsl:value-of select="$thmedit_id" /></xsl:with-param>
					<xsl:with-param name="external_id"><xsl:value-of select="$thmedit_id_ext" /></xsl:with-param>
					<xsl:with-param name="url"><xsl:value-of select="$private_bin_uri" />/Forum/ProcessAnimAction?action=rename_thematic&amp;field=label&amp;rowid=<xsl:value-of select="./rowid" /></xsl:with-param>
				</xsl:call-template>
			</xsl:if>
			<p id="{$thmdescedit_id}" ><xsl:value-of select="./description" /></p>
			<xsl:if test="$anim_right='1'">
				<xsl:call-template name="inplace-editor-multi">
					<xsl:with-param name="element"><xsl:value-of select="$thmdescedit_id" /></xsl:with-param>
					<xsl:with-param name="url"><xsl:value-of select="$private_bin_uri" />/Forum/ProcessAnimAction?action=rename_thematic&amp;field=desc&amp;rowid=<xsl:value-of select="./rowid" /></xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</td>
		<td class="subjects border_color"><xsl:value-of select="./nb_subjects" /></td>
		<td class="messages border_color"><xsl:value-of select="./nb_messages" /></td>
		<td class="lastpost border_color">
			<xsl:if test="./lastpost != ''">
				<p><xsl:value-of select="./lastpost/year" />-<xsl:if test="./lastpost/month &lt; 10">0</xsl:if><xsl:value-of select="./lastpost/month" />-<xsl:if test="./lastpost/day &lt; 10">0</xsl:if><xsl:value-of select="./lastpost/day" />&#160;<xsl:if test="./lastpost/hour &lt; 10">0</xsl:if><xsl:value-of select="./lastpost/hour" />:<xsl:if test="./lastpost/min &lt; 10">0</xsl:if><xsl:value-of select="./lastpost/min" />:<xsl:if test="./lastpost/sec &lt; 10">0</xsl:if><xsl:value-of select="./lastpost/sec" /></p>
				<p><xsl:value-of select="./firstname" />&#160;<xsl:value-of select="./lastname" /></p>
			</xsl:if>
		</td>
		</tr>
</xsl:template>

<!-- ===============
	UpdateThematic
	================ -->

<xsl:template name="UpdateThematic" match="UpdateThematic">
<xsl:variable name="category_rowid"><xsl:value-of select="//Thematic/category/@rowid" /></xsl:variable>
<xsl:variable name="thematic_rowid"><xsl:value-of select="//Thematic/thematic/rowid" /></xsl:variable>
	<div id="header">
		<div class="menu_forum">
			<ul class="hmenu">
				<xsl:if test="$write_right='1'">
					<li>
						<a class="action" id="create_subject" >
							<xsl:attribute name="href"><xsl:value-of select="$view_action" />?action=create_subject&amp;category_id=<xsl:value-of select="$category_rowid" />&amp;thematic_id=<xsl:value-of select="$thematic_rowid" /></xsl:attribute>
							<xsl:value-of select="//action[@id = 'create_subject']/@label" />
						</a>
					</li>
				</xsl:if>
				<li>
					<a class="action" id="view_new_messages" >
							<xsl:attribute name="href"><xsl:value-of select="$view_action" />?action=view_new_messages&amp;subject_id=0&amp;category_id=<xsl:value-of select="$category_rowid" />&amp;thematic_id=<xsl:value-of select="$thematic_rowid" /></xsl:attribute>
						<xsl:value-of select="//action[@id = 'view_new_messages']/@label" />
					</a>
				</li>
			</ul>
		</div>
	</div>
	<xsl:if test='//Subjects/@nb_pages &gt; 1'>
	<div>
		<xsl:call-template name="change_page">
			<xsl:with-param name="cur_page"><xsl:value-of select="//Subjects/@page" /></xsl:with-param>
			<xsl:with-param name="nb_pages"><xsl:value-of select="//Subjects/@nb_pages" /></xsl:with-param>
			<xsl:with-param name="base_url"><xsl:value-of select="$private_bin_uri" />/Forum/DisplayThematic?category_id=<xsl:value-of select="$category_rowid" />&amp;thematic_id=<xsl:value-of select="$thematic_rowid" /></xsl:with-param>
		</xsl:call-template>
	</div>
	</xsl:if>
	<div id="subjects" class="sbox border_color">
		<h2 class="title_bg_color"><a href="{$private_bin_uri}/Forum/DisplayMain" ><xsl:value-of select="//Thematic/category/@label" /></a>&#160;/&#160;<xsl:value-of select="//Thematic/thematic/label" /></h2>
		<xsl:apply-templates select="Subjects" />
	</div>
	<xsl:if test='//Subjects/@nb_pages &gt; 1'>
	<div>
		<xsl:call-template name="change_page">
			<xsl:with-param name="cur_page"><xsl:value-of select="//Subjects/@page" /></xsl:with-param>
			<xsl:with-param name="nb_pages"><xsl:value-of select="//Subjects/@nb_pages" /></xsl:with-param>
			<xsl:with-param name="base_url"><xsl:value-of select="$private_bin_uri" />/Forum/DisplayThematic?category_id=<xsl:value-of select="$category_rowid" />&amp;thematic_id=<xsl:value-of select="$thematic_rowid" /></xsl:with-param>
		</xsl:call-template>
	</div>
	</xsl:if>
</xsl:template>

<!-- ===============
	Subjects
	================ -->

<xsl:template match="Subjects" >
<xsl:variable name="category_rowid"><xsl:value-of select="//Thematic/category/@rowid" /></xsl:variable>
<xsl:variable name="thematic_rowid"><xsl:value-of select="//Thematic/thematic/rowid" /></xsl:variable>
<table class="subjects">
<tr>
	<th class="subject"><xsl:value-of select="mioga:gettext('Subject')" /></th>
	<th class="messages"><xsl:value-of select="mioga:gettext('Nb messages')" /></th>
	<th class="lastpost"><xsl:value-of select="mioga:gettext('Last message')" /></th>
</tr>
<xsl:for-each select="subject">
	<tr>
		<xsl:attribute name="class">
			<xsl:choose>
				<xsl:when test='position() mod 2 = 0'>odd</xsl:when>
				<xsl:otherwise>even</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<td class="subject border_color">
			<div class="menu_subject">
				<ul class="hmenu">
					<xsl:if test="$moder_right='1'">
						<li>
							<a class="action" >
									<xsl:attribute name="href"><xsl:value-of select="$anim_action" />?action=edit_subject&amp;subject_id=<xsl:value-of select="./rowid" /></xsl:attribute>
								<img src="{$image_uri}/16x16/actions/document-edit.png" >
									<xsl:attribute name="alt"><xsl:value-of select="//action[@id = 'edit_subject']/@label" /></xsl:attribute>
									<xsl:attribute name="title"><xsl:value-of select="//action[@id = 'edit_subject']/@label" /></xsl:attribute>
								</img>
							</a>
						</li>
						<li>
							<a class="action" href="#">
								<xsl:attribute name="onclick">if (confirm('<xsl:value-of select="mioga:gettext('Delete subject and all messages ?')" />')) { ProcessAnimAction('<xsl:value-of select="$anim_action" />?action=suppress_subject&amp;subject_id=<xsl:value-of select="./rowid" />&amp;category_id=<xsl:value-of select="$category_rowid" />&amp;thematic_id=<xsl:value-of select="$thematic_rowid" />');} return false;</xsl:attribute>
								<img src="{$image_uri}/16x16/actions/trash-empty.png" >
									<xsl:attribute name="alt"><xsl:value-of select="//action[@id = 'suppress_subject']/@label" /></xsl:attribute>
									<xsl:attribute name="title"><xsl:value-of select="//action[@id = 'suppress_subject']/@label" /></xsl:attribute>
								</img>
							</a>
						</li>
					</xsl:if>
				</ul>
			</div>
			<a>
				<xsl:attribute name="class">
					<xsl:choose>
						<xsl:when test="./new_messages">new_subject</xsl:when>
						<xsl:otherwise>old_subject</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="href"><xsl:value-of select="$private_bin_uri" />/Forum/DisplaySubject?subject_id=<xsl:value-of select="./rowid" /></xsl:attribute>
				<xsl:value-of select="./label" />
			</a>
			<xsl:if test="./post_it = '1'">
				<img src="{$image_uri}/16x16/emblems/emblem-important.png" >
					<xsl:attribute name="alt"><xsl:value-of select="mioga:gettext('Post it subject')" /></xsl:attribute>
					<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Post it subject')" /></xsl:attribute>
				</img>
			</xsl:if>
			<xsl:if test="./open = '0'">
				<img src="{$image_uri}/edit-delete.gif" >
					<xsl:attribute name="alt"><xsl:value-of select="mioga:gettext('Closed subject')" /></xsl:attribute>
					<xsl:attribute name="title"><xsl:value-of select="mioga:gettext('Closed Subject')" /></xsl:attribute>
				</img>
			</xsl:if>
		</td>
		<td class="messages border_color"><xsl:value-of select="./nb_messages" /></td>
		<td class="lastpost border_color">
			<p><xsl:value-of select="./lastpost/year" />-<xsl:if test="./lastpost/month &lt; 10">0</xsl:if><xsl:value-of select="./lastpost/month" />-<xsl:if test="./lastpost/day &lt; 10">0</xsl:if><xsl:value-of select="./lastpost/day" />&#160;<xsl:if test="./lastpost/hour &lt; 10">0</xsl:if><xsl:value-of select="./lastpost/hour" />:<xsl:if test="./lastpost/min &lt; 10">0</xsl:if><xsl:value-of select="./lastpost/min" />:<xsl:if test="./lastpost/sec &lt; 10">0</xsl:if><xsl:value-of select="./lastpost/sec" /></p>
			<p><xsl:value-of select="./firstname" />&#160;<xsl:value-of select="./lastname" /></p>
		</td>
	</tr>
</xsl:for-each>
</table>
</xsl:template>

<!-- ===============================
	Pagination
	================================ -->

<xsl:template name="change_page">
	<xsl:param name="nb_pages">1</xsl:param>
	<xsl:param name="cur_page">1</xsl:param>
	<xsl:param name="base_url"></xsl:param>
	Pages : <xsl:value-of select="$nb_pages" />
	<ul class="change_page">
		<xsl:if test="$nb_pages &gt; 1">
			<xsl:if test="$cur_page &gt; 1">
				<li><a href="{$base_url}&amp;page=1"><img src="{$image_uri}/16x16/actions/go-first.png" title="go-first" alt="go-first"/></a></li>
				<li><a href="{$base_url}&amp;page={$cur_page}&amp;view=previous"><img src="{$image_uri}/16x16/actions/go-previous.png" title="go-previous" alt="go-previous"/></a></li>
			</xsl:if>
				<li><a href="{$base_url}&amp;page={$cur_page}"><xsl:value-of select="$cur_page" /></a></li>
			<xsl:if test="$cur_page &lt; $nb_pages">
				<li><a href="{$base_url}&amp;page={$cur_page}&amp;view=next"><img src="{$image_uri}/16x16/actions/go-next.png" title="go-next" alt="go-next"/></a></li>
				<li><a href="{$base_url}&amp;page={$nb_pages}"><img src="{$image_uri}/16x16/actions/go-last.png" title="go-last" alt="go-last"/></a></li>
			</xsl:if>
		</xsl:if>
	</ul>
</xsl:template>

</xsl:stylesheet>

