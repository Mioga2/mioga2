<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:output method="html"/>
<xsl:include href="base.xsl" />
<xsl:include href="scriptaculous.xsl"/>
<xsl:include href="inline_message.xsl"/>


<!-- ===============
	root document
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="scriptaculous-js"/>
	<xsl:call-template name="theme-css"/>
</head>
	<xsl:apply-templates />
</html>
</xsl:template>
  
<!-- =================================
     DisplayMain 
     ================================= -->

<xsl:template match="DisplayMain" mode="table-body">
<xsl:for-each select="YearMonthCount">
    <xsl:sort select="@year" data-type="number" order="ascending"/>
    <xsl:sort select="@month" data-type="number" order="ascending"/>
	<tr>
		<xsl:choose>
			<xsl:when test="position() mod 2 = 0">
				<xsl:attribute name="class"><xsl:value-of select="$mioga-list-even-row-bg-color" /></xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color" /></xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<td><xsl:value-of select="@year" /></td>
		<td><a href="DisplayYearMonthMsg?year={@year}&amp;month={@month}">
                <xsl:call-template name="mlMonthTranslation"/>
 		</a></td>
		<td><xsl:value-of select="@count" /></td>
	</tr>
</xsl:for-each>
</xsl:template>

<xsl:template match="DisplayMain">
<body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">applications.html#mailinglist</xsl:with-param>
    </xsl:call-template>

    <center class="alone-form">
        <xsl:choose>
            <xsl:when test="count(YearMonthCount) = 0">
                <font class="{$mioga-title-color}"><i>
                <xsl:call-template name="mlTranslation">
                  <xsl:with-param name="type">no-archived-message</xsl:with-param>
                </xsl:call-template>
                </i></font>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="table">
                    <xsl:with-param name="table_width">90%</xsl:with-param>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </center>
</body>
</xsl:template>


<!-- =================================
     DisplayYearMonthMsg 
     ================================= -->

<xsl:template match="DisplayYearMonthMsg" mode="table-title">
		<th><font class="{$mioga-list-title-text-color}">Date</font></th>
		<th><font class="{$mioga-list-title-text-color}">From</font></th>
		<th><font class="{$mioga-list-title-text-color}">Subject</font></th>
        <xsl:if test="count(can_anim) != 0">
            <th><font class="{$mioga-list-title-text-color}">&#160;</font></th>
        </xsl:if>
</xsl:template>

<xsl:template match="DisplayYearMonthMsg" mode="table-body">
<xsl:for-each select="messages/message">
	<tr>
		<xsl:choose>
			<xsl:when test="position() mod 2 = 0">
				<xsl:attribute name="class"><xsl:value-of select="$mioga-list-even-row-bg-color" /></xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color" /></xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<td><xsl:value-of select="date" /></td>
		<td><xsl:value-of select="from" /></td>
		<td>
                  <a href="DisplayMessage?rowid={@rowid}">
                    <xsl:choose>
                      <xsl:when test="subject=''">
                        <xsl:call-template name="mlTranslation">
                          <xsl:with-param name="type">no-subject</xsl:with-param>
                        </xsl:call-template>
                      </xsl:when>
                      <xsl:otherwise><xsl:value-of select="subject" /></xsl:otherwise>
                    </xsl:choose>
                  </a>
                </td>
        <xsl:if test="count(../../can_anim) != 0">
            <td><a href="DeleteMessage?rowid={@rowid}"><img border="0" src="{$image_uri}/16x16/actions/trash-empty.png" /></a></td>
        </xsl:if>
	</tr>
</xsl:for-each>
</xsl:template>


<xsl:template match="DisplayYearMonthMsg">
<body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">applications.html#mailinglist</xsl:with-param>
    </xsl:call-template>

	<!-- Status message -->
	<xsl:if test="@message != ''">
		<xsl:call-template name="InlineMessage">
			<xsl:with-param name="type" select="@type"/>
			<xsl:with-param name="message" select="@message"/>
		</xsl:call-template>
	</xsl:if>

<table border="0" cellpadding="2" cellspacing="0">
<tr>
	<td><img src="{$image_uri}/transparent_fill.gif" width="20" height="1" /></td>
	<td><img src="{$image_uri}/16x16/actions/arrow-right.png" /></td>
	<td>
		<a href="DisplayMain"><font class="{$mioga-title-color}">
                <xsl:call-template name="mlTranslation">
                  <xsl:with-param name="type">back</xsl:with-param>
                </xsl:call-template>
                </font></a>
	</td>
</tr>
</table>

<br />
<center>
	<xsl:call-template name="table">
		<xsl:with-param name="table_width">90%</xsl:with-param>
	</xsl:call-template>
</center>
</body>
</xsl:template>


<!-- =================================
     DisplayMessage 
     ================================= -->

<xsl:template match="DisplayMessage">
<body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">applications.html#mailinglist</xsl:with-param>
    </xsl:call-template>

<xsl:if test="not(referer='')">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
	<td><img src="{$image_uri}/transparent_fill.gif" width="20" height="1" /></td>
	<td><img src="{$image_uri}/16x16/actions/arrow-right.png" /></td>
	<td>
		<a href="{referer}"><font class="{$mioga-title-color}">
                <xsl:call-template name="mlTranslation">
                  <xsl:with-param name="type">back</xsl:with-param>
                </xsl:call-template>
                </font></a>
	</td>
</tr>
</table>
</xsl:if>

<br />
    
    <center>
        <xsl:apply-templates select="Mail"/>
    </center>
    

</body>
</xsl:template>

<!-- =================================
     Mail 
     ================================= -->

<xsl:template match="Mail">
  <table border="0" cellpadding="0" cellspacing="1" width="95%" class="{$mioga-list-title-bg-color}">
    <tr>
      <td>
        <table border="0" cellpadding="0" cellspacing="5" width="100%" class="{$mioga-list-title-bg-color}">
          <tr align="left">
            <th nowrap="nowrap" colspan="3">
              <font class="{$mioga-list-title-text-color}">
                <xsl:call-template name="mlTranslation">
                  <xsl:with-param name="type">subject</xsl:with-param>
                </xsl:call-template>&#160;:&#160;
                <xsl:value-of select="Subject"/></font>
            </th>
          </tr>
          <tr align="left">
            <th nowrap="nowrap">
              <font class="{$mioga-list-title-text-color}">
                <xsl:call-template name="mlTranslation">
                  <xsl:with-param name="type">from</xsl:with-param>
                </xsl:call-template>&#160;:&#160;
                <xsl:value-of select="From"/></font>
              
            </th>
            
            <th>
              <img src="{$image_uri}/transparent_fill.gif" width="10" height="1"/>
            </th>
            
            <th nowrap="nowrap">
              <font class="{$mioga-list-title-text-color}">
                <xsl:call-template name="mlTranslation">
                  <xsl:with-param name="type">to</xsl:with-param>
                </xsl:call-template>&#160;:&#160;
                <xsl:value-of select="To"/></font>
            </th>
            <th width="100%">&#160;</th>
          </tr>
          
          <xsl:if test="Cc!=''">
            <tr align="left">
              <th nowrap="nowrap" colspan="3">
                <font class="{$mioga-list-title-text-color}">
                <xsl:call-template name="mlTranslation">
                  <xsl:with-param name="type">cc</xsl:with-param>
                </xsl:call-template>&#160;:&#160;
                <xsl:value-of select="Cc"/></font>
              </th>
            </tr>
          </xsl:if>
          
          <tr align="left">
            <th nowrap="nowrap" colspan="3">
              <font class="{$mioga-list-title-text-color}">
                <xsl:call-template name="mlTranslation">
                  <xsl:with-param name="type">date</xsl:with-param>
                </xsl:call-template>&#160;:&#160;
                <xsl:for-each select="date">
                  <xsl:call-template name="LocalDate"/>
                </xsl:for-each>
              </font>
            </th>
          </tr>
        </table>
      </td>
    </tr>
    
    <xsl:if test="count(unknown_part) != 0">
      <tr>
        <td>
          <table border="0" cellpadding="0" cellspacing="5" width="100%" class="{$mioga-bg-color}">
            <tr>
              <td nowrap="nowrap"><font class="{$mioga-title-color}">
                <xsl:call-template name="mlTranslation">
                  <xsl:with-param name="type">attached-files</xsl:with-param>
                </xsl:call-template>&#160;:&#160;
              </font></td>
              <td colspan="3">&#160;</td>
            </tr>
            
            <xsl:for-each select="unknown_part">
                <tr>
                  <td>&#160;</td>
                  <td nowrap="nowrap" align="left">
                    <a href="DownloadJoined?name={name}">
                      <xsl:value-of select="name"/>
                    </a>
                  </td>
                  <td nowrap="nowrap" align="left">
                    (<xsl:value-of select="size"/>&#160;<xsl:call-template name="sizeUnit"/>)
                </td>
                <td nowrap="nowrap"  align="left" width="100%">
                  <xsl:value-of select="type"/>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </td>
      </tr>
    </xsl:if>
    
    <xsl:for-each select="printable_part">
      <tr>
        <td>
          <table border="0" cellpadding="20" cellspacing="0" width="100%" class="{$mioga-bg-color}">
            <tr>
              <td>
                <xsl:value-of select="." disable-output-escaping="yes"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </xsl:for-each>
    
    <xsl:for-each select="image_part">
      <tr>
        <td>
          <table border="0" cellpadding="0" cellspacing="5" width="100%" class="{$mioga-bg-color}">
            <tr>
              <td>
                <a href="DownloadJoined?name={.}">
                  <img src="DownloadJoined?name={.}" border="0"/>
                </a>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </xsl:for-each>
    
    <xsl:if test="count(Mail)!=0">
      <tr>
        <td>
          <table border="0" cellpadding="0" cellspacing="5" width="100%" class="{$mioga-bg-color}">
            <tr>
              <td nowrap="nowrap"><font class="{$mioga-title-color}">
              <xsl:call-template name="mlTranslation">
                <xsl:with-param name="type">attached-messages</xsl:with-param>
              </xsl:call-template>&#160;:&#160;
            </font></td>
              <td colspan="3">&#160;</td>
            </tr>
            <tr>
              <td>
                <xsl:apply-templates select="Mail"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </xsl:if>
    
  </table>  
</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<!-- SuccessMessage -->
<xsl:template match="SuccessMessage">
  <xsl:call-template name="SuccessMessage">
    <xsl:with-param name="message"><xsl:value-of select="mioga:gettext('Message successfully deleted.')"/></xsl:with-param>
    <xsl:with-param name="referer" select="referer"/>
  </xsl:call-template>
</xsl:template>

<xsl:template match="DisplayMain" mode="table-title">
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Year')"/></font></th>
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Month')"/></font></th>
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Number of messages')"/></font></th>
</xsl:template>

<xsl:template name="mlMonthTranslation">
    <xsl:message>
        <xsl:value-of select="@month"/>
    </xsl:message>

  <xsl:choose>
    <xsl:when test="./@month='1'"><xsl:value-of select="mioga:gettext('January')"/></xsl:when>
    <xsl:when test="./@month='2'"><xsl:value-of select="mioga:gettext('February')"/></xsl:when>
    <xsl:when test="./@month='3'"><xsl:value-of select="mioga:gettext('March')"/></xsl:when>
    <xsl:when test="./@month='4'"><xsl:value-of select="mioga:gettext('April')"/></xsl:when>
    <xsl:when test="./@month='5'"><xsl:value-of select="mioga:gettext('May')"/></xsl:when>
    <xsl:when test="./@month='6'"><xsl:value-of select="mioga:gettext('June')"/></xsl:when>
    <xsl:when test="./@month='7'"><xsl:value-of select="mioga:gettext('July')"/></xsl:when>
    <xsl:when test="./@month='8'"><xsl:value-of select="mioga:gettext('August')"/></xsl:when>
    <xsl:when test="./@month='9'"><xsl:value-of select="mioga:gettext('September')"/></xsl:when>
    <xsl:when test="./@month='10'"><xsl:value-of select="mioga:gettext('October')"/></xsl:when>
    <xsl:when test="./@month='11'"><xsl:value-of select="mioga:gettext('November')"/></xsl:when>
    <xsl:when test="./@month='12'"><xsl:value-of select="mioga:gettext('December')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="mlTranslation">
  <xsl:param name="type"/>
  
  <xsl:choose>
    <xsl:when test="$type='no-archived-message'"><xsl:value-of select="mioga:gettext('No message archived.')"/></xsl:when>
    <xsl:when test="$type='back'"><xsl:value-of select="mioga:gettext('Back')"/></xsl:when>
    <xsl:when test="$type='no-subject'"><xsl:value-of select="mioga:gettext('(no subject)')"/></xsl:when>
    <xsl:when test="$type='subject'"><xsl:value-of select="mioga:gettext('Subject')"/></xsl:when>
    <xsl:when test="$type='from'"><xsl:value-of select="mioga:gettext('From')"/></xsl:when>
    <xsl:when test="$type='to'"><xsl:value-of select="mioga:gettext('To')"/></xsl:when>
    <xsl:when test="$type='cc'"><xsl:value-of select="mioga:gettext('CC')"/></xsl:when>
    <xsl:when test="$type='date'"><xsl:value-of select="mioga:gettext('Date')"/></xsl:when>
    <xsl:when test="$type='attached-file'"><xsl:value-of select="mioga:gettext('Attached file')"/></xsl:when>
    <xsl:when test="$type='attached-messages'"><xsl:value-of select="mioga:gettext('Attached message')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="sizeUnit">
  <xsl:choose>
    <xsl:when test="unit='B'"><xsl:value-of select="mioga:gettext('b')"/></xsl:when>
    <xsl:when test="unit='MB'"><xsl:value-of select="mioga:gettext('Mb')"/></xsl:when>
    <xsl:when test="unit='KB'"><xsl:value-of select="mioga:gettext('kb')"/></xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
