<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:base="http://www.mioga2.org/portal/mioglets/Base" xmlns:mioga="urn:mioga">

<xsl:output method="html"/>

<!-- =================================
     Handle HTML content
     ================================= -->

<xsl:template match="base:html" mode="editor">
    <xsl:param name="node"/>
    
    <xsl:call-template name="EditorModeEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">base_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

    <xsl:call-template name="EditorURLEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

</xsl:template>

<xsl:template match="base:html">
    <xsl:value-of disable-output-escaping="yes" select="."/>
</xsl:template>

<!-- =================================
     Handle plain text content
     ================================= -->

<xsl:template match="base:text">
    <pre>
        <xsl:value-of disable-output-escaping="yes" select="."/>
    </pre>
</xsl:template>

<!-- =================================
     Handle images
     ================================= -->

<!-- Mioglet main template -->

<xsl:template match="base:image" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">base_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">mode</xsl:with-param>
        <xsl:with-param name="value">user</xsl:with-param>        
    </xsl:call-template>
    
    <xsl:call-template name="EditorURLEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>
</xsl:template>


<xsl:template match="base:image">
    <img src="{@href}"/>
</xsl:template>

<!-- =================================
     Handle iframe
     ================================= -->

<!-- Mioglet main template -->

<xsl:template match="base:iframe" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">base_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">mode</xsl:with-param>
        <xsl:with-param name="value">user</xsl:with-param>        
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">name</xsl:with-param>
        <xsl:with-param name="value" select="$node/name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">src</xsl:with-param>
        <xsl:with-param name="value" select="$node/src"/>
    </xsl:call-template>

</xsl:template>


<xsl:template match="base:iframe">
    <iframe src="{@src}" name="{@name}" />
</xsl:template>

<!-- =================================
     Handle Errors
     ================================= -->


<xsl:template match="base:error" mode="editor">
    <xsl:param name="node"/>

    <xsl:apply-templates select="./InitialTag/*" mode="editor">
        <xsl:with-param name="node" select="$node"/>
    </xsl:apply-templates>
</xsl:template>

<xsl:template match="base:error">
    <xsl:call-template name="MiogaTitle">
        <xsl:with-param name="title">
            Erreur : <xsl:value-of select="type"/>
        </xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaMessage">
        <xsl:with-param name="message">
            <xsl:value-of select="text"/>
        </xsl:with-param>
    </xsl:call-template>
</xsl:template>


<!-- =================================
     Handle GroupList
     ================================= -->

<!-- Editor Form -->

<xsl:template match="base:GroupList" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">link_path</xsl:with-param>
        <xsl:with-param name="value" select="$node/link_path"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">base_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">mode</xsl:with-param>
        <xsl:with-param name="value">user</xsl:with-param>        
    </xsl:call-template>

</xsl:template>


<!-- Mioglet main template -->

<xsl:template match="base:GroupList">
    <xsl:param name="node"/>

    <table class="baseGroupList" cellpadding="5" cellspacing="0">
        <xsl:if test="count(group) = 0">
            <tr>
                <td>
                    <i><xsl:call-template name="base-no-group"/></i>
                </td>
            </tr>
        </xsl:if>

        <xsl:for-each select="group">
            <tr>
                <xsl:if test="$node/boxed">
                    <xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
							<xsl:otherwise>oddRow</xsl:otherwise>
						</xsl:choose>
				   </xsl:attribute>
                </xsl:if>

                <xsl:variable name="href">
                    <xsl:choose>
                        <xsl:when test="count($node/link_path) = 0 or $node/link_path = ''"><xsl:value-of select="$private_dav"/>/<xsl:value-of select="@ident"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$bin_uri"/>/<xsl:value-of select="@ident"/>/Portal/DisplayPortal?path=<xsl:value-of select="$node/link_path"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <td>
                    <a href="{$href}" alt="{.}" title="{.}"><xsl:value-of select="@ident"/></a>
                </td>
            </tr>
        </xsl:for-each>
        
        
        <tr>
            <xsl:if test="$node/boxed">
				<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="count(group) != 0 and count(group) mod 2 = 0">evenRow</xsl:when>
							<xsl:otherwise>oddRow</xsl:otherwise>
						</xsl:choose>
				   </xsl:attribute>
			</xsl:if>
            
            <td>
                <xsl:variable name="href">
                    <xsl:value-of select="$bin_uri"/>/<xsl:value-of select="$mioga_context/user/@ident"/>/AnimUser/GroupList<xsl:if test="count($node/link_path) > 0">?link_path=<xsl:value-of select="$node/link_path"/></xsl:if>
                </xsl:variable>
                
                <xsl:call-template name="MiogletOtherLink">
                    <xsl:with-param name="href" select="$href"/>
                </xsl:call-template>
            </td>
        </tr>
        
    </table>

</xsl:template>


<!-- =================================
     Handle MemberList
     ================================= -->

<!-- Editor Form -->

<xsl:template match="base:MemberList" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">base_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">mode</xsl:with-param>
        <xsl:with-param name="value">group</xsl:with-param>        
    </xsl:call-template>

</xsl:template>


<!-- main memberlist template -->

<xsl:template match="base:MemberList">
    <xsl:param name="node"/>

    <table class="baseMemberList" cellpadding="5" cellspacing="0">
        <xsl:if test="count(user) = 0">
            <tr>
                <td>
                    <i><xsl:call-template name="base-no-member"/></i>
                </td>
            </tr>
        </xsl:if>

        <xsl:for-each select="user">
            <tr>
                <xsl:if test="$node/boxed">
					<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
							<xsl:otherwise>oddRow</xsl:otherwise>
						</xsl:choose>
				   </xsl:attribute>
                </xsl:if>
                
                <td>
                    <xsl:value-of select="."/>
                </td>
            </tr>
        </xsl:for-each>
        
        
        <tr>
            <xsl:if test="$node/boxed">
				<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="count(user) != 0 and count(user) mod 2 = 0">evenRow</xsl:when>
							<xsl:otherwise>oddRow</xsl:otherwise>
						</xsl:choose>
				   </xsl:attribute>
            </xsl:if>
            
            <td>
                <xsl:variable name="href">
                    <xsl:value-of select="$bin_uri"/>/<xsl:value-of select="$mioga_context/group/ident"/>/AnimGroup/MemberList
                </xsl:variable>
                
                <xsl:call-template name="MiogletOtherLink">
                    <xsl:with-param name="href" select="$href"/>
                </xsl:call-template>
            </td>
        </tr>
        
    </table>

</xsl:template>


<!-- =================================
     Handle AppList
     ================================= -->

<!-- Editor Form -->

<xsl:template match="base:AppList" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorModeEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">base_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>
</xsl:template>


<!-- main applist template -->

<xsl:template match="base:AppList">
    <xsl:param name="node"/>

    <xsl:variable name="group">
        <xsl:choose>
            <xsl:when test="@mode='group'"><xsl:value-of select="$mioga_context/group/ident"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="$mioga_context/user/ident"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <table class="baseAppList" cellpadding="5" cellspacing="0">
        <xsl:if test="count(app) = 0">
            <tr>
                <td>
                    <i><xsl:call-template name="base-no-app"/></i>
                </td>
            </tr>
        </xsl:if>
        
        <xsl:for-each select="app">
            <xsl:sort select="@name" order="ascending"/>
            
            <xsl:variable name="app_name">
                <xsl:call-template name="MiogaAppName">
                    <xsl:with-param name="app_ident" select="@ident"/>
                </xsl:call-template>
            </xsl:variable>
            
            <xsl:variable name="app_desc">
                <xsl:call-template name="MiogaAppDesc">
                    <xsl:with-param name="app_ident" select="@ident"/>
                </xsl:call-template>
            </xsl:variable>
            
            <xsl:variable name="app_icon">
                <xsl:call-template name="MiogaAppIcon">
                    <xsl:with-param name="app_ident" select="@ident"/>
                </xsl:call-template>
            </xsl:variable>
            
            <xsl:if test="$app_name != ''">
                
                <tr>
                    <xsl:if test="$node/boxed">
						<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
							<xsl:otherwise>oddRow</xsl:otherwise>
						</xsl:choose>
				   </xsl:attribute>
                  </xsl:if>
                    
                    <td>
                        <img src="{$image_uri}/16x16/apps/{$app_icon}" />
                    </td>
                    <td>
                        <a href="{$bin_uri}/{$group}/{@ident}/DisplayMain" alt="{$app_desc}" title="{$app_desc}">
                            <xsl:value-of select="$app_name"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>
        </xsl:for-each>
        
        
        <tr>
            <xsl:if test="$node/boxed">
				<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="count(app) != 0 and count(app) mod 2 = 0">evenRow</xsl:when>
							<xsl:otherwise>oddRow</xsl:otherwise>
						</xsl:choose>
				   </xsl:attribute>
            </xsl:if>
            
            <xsl:variable name="href">
                <xsl:value-of select="$bin_uri"/><xsl:choose>
                    <xsl:when test="$node/mode = 'group'">/<xsl:value-of select="$mioga_context/group/ident"/>/AnimGroup/AppList</xsl:when>
                    <xsl:otherwise>/<xsl:value-of select="$mioga_context/user/ident"/>/AnimUser/AppList</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            
            
            <td colspan="2">
                <xsl:call-template name="MiogletOtherLink">
                    <xsl:with-param name="href" select="$href"/>
                </xsl:call-template>
            </td>
        </tr>
        
    </table>
    
</xsl:template>


<!-- =================================
     Handle MiogletSelector
     ================================= -->

<!-- main applist template -->

<xsl:template match="base:MiogletSelector">
    <xsl:param name="node"/>

    <xsl:choose>
        <xsl:when test="$mode = 'edit'">

    <form method="GET">
        <table border="0" onclick="javascript:noclick(event)">
            <tr>
                <td>
                    <select name="mioglet_name">
                        <option name="select">
                            <xsl:call-template name="select-mioglet"/>
                        </option>
                        
                        <option value="--">----</option>
                        
                        <option value="vbox">vbox</option>
                        <option value="hbox">hbox</option>
                        
                        <xsl:if test="name($node/..) != 'portal'">
                            <option value="--">----</option>
                            
                            <option value="Base/image">image</option>
                            <option value="Base/html">html file</option>
                            <option value="Base/iframe">iframe</option>
                            <option value="inline-html">inline html</option>
                            
                            <option value="--">----</option>
                            
                            <xsl:for-each select="base:MiogletIdent">
                                <xsl:sort select="mioglet_ident"/>
                                <xsl:sort select="method_ident"/>
                                <option value="{mioglet_ident}/{method_ident}"><xsl:value-of select="mioglet_ident"/>/<xsl:value-of select="method_ident"/></option>
                            </xsl:for-each>
                        </xsl:if>
                    </select>
                    <input type="hidden" name="id" value="{$node/@id}"/>
                </td>
                <td>
                    <xsl:call-template name="ok-form-button">
                        <xsl:with-param name="name">set_mioglet</xsl:with-param>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </form>
            
        </xsl:when>
        
        <xsl:otherwise>
            <xsl:call-template name="unconfig-mioglet"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<!-- =================================
     Translations
     ================================= -->

<xsl:template name="select-mioglet">
  <xsl:value-of select="mioga:gettext('Please select mioglet to add')"/>
</xsl:template>

<xsl:template name="unconfig-mioglet">
  <xsl:value-of select="mioga:gettext('Mioglet not configured')"/>
</xsl:template>


<xsl:template name="base-no-app">
  <xsl:value-of select="mioga:gettext('No application')"/>
</xsl:template>

<xsl:template name="base-no-member">
  <xsl:value-of select="mioga:gettext('No member')"/>
</xsl:template>

<xsl:template name="base-no-group">
  <xsl:value-of select="mioga:gettext('No group')"/>
</xsl:template>

</xsl:stylesheet>
