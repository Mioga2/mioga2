<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl" />

<xsl:output method="html" indent="yes" encoding="UTF-8" />
<xsl:include href="simple_large_list.xsl" />

<!--
	Global variables
-->
<xsl:variable name="crop_length" select="24"/>

<!-- ===============
	root document
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>

	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="theme-css"/>
</head>
<body id="filebrowser" class="FileBrowser">
	<xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- ===============
	DisplayMain
	================ -->

<xsl:template match="DisplayMain">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">applications.html#filebrowser</xsl:with-param>
    </xsl:call-template>

<table class="main_view">
<tr>
	<td class="tree_col">
		<div id="tree_head">
		</div>
		<div id="tree">
		</div>
	</td>
	<td class="files_col">
		<div id="files_head">
		</div>
		<div id="files">
		</div>
	</td>
</tr>
</table>
</xsl:template>

<!--
================================================================================
DisplayURI
================================================================================
-->

<xsl:template match="DisplayURI">

    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">applications.html#filebrowser</xsl:with-param>
    </xsl:call-template>
    <xsl:variable name="rootUri"><xsl:if test="/DisplayURI/resource[normalize-space(@root)]">&amp;root=<xsl:value-of select="/DisplayURI/resource/@root"/></xsl:if></xsl:variable>
    <h1><img src="{$image_uri}/48x48/mimetypes/inode-directory.png" alt="Current directory"/>&#160;<xsl:for-each select="./resource/nav_uri/tag">/<xsl:choose>
            <xsl:when test="@uri"><a href="?uri={@uri}{$rootUri}"><xsl:value-of select="@text"/></a></xsl:when>
            <xsl:otherwise><xsl:value-of select="@text"/></xsl:otherwise>
        </xsl:choose>
    </xsl:for-each></h1>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    	<tr>
		<th>
            <a href="DisplayURI?uri={/DisplayURI/resource/@uri}&amp;sort=d{$rootUri}"><img src="{$image_uri}/16x16/mimetypes/inode-directory.png"/></a>
        </th>
	<xsl:for-each select="./headers/header">
		<th>
			<xsl:if test="@enabled = '1'">
				<xsl:attribute name="class">selected</xsl:attribute>
				<img src="{$image_uri}/16x16/actions/view-sort-descending.png" style="padding-right: 0.1em;"/>
			</xsl:if>
			<a href="DisplayURI?uri={/DisplayURI/resource/@uri}&amp;sort={@link}{$rootUri}"><xsl:call-template 
				name="FBTranslations"><xsl:with-param name="label" select="@name"/></xsl:call-template></a>
		</th>
	</xsl:for-each>
	</tr>
	<xsl:if test="./resource/@follow_parent = '1'">
	<tr class="odd">
		<td align="center"><img src="{$image_uri}/16x16/actions/go-previous.png" alt="Parent directory"/></td>
		<td><a href="DisplayURI?uri={./resource/@parent}{$rootUri}"><xsl:call-template name="FBTranslations"><xsl:with-param 
			name="label">Parent directory</xsl:with-param></xsl:call-template></a></td>
		<td>&#160;</td>
		<td align="right">-</td>
		<xsl:for-each select="./resource/item[1]/property">
			<td>&#160;</td>
		</xsl:for-each>
	</tr>
	</xsl:if>
	<xsl:for-each select="./resource/item">
        <xsl:variable name="even_or_odd">
            <xsl:choose>
                <xsl:when test="position() mod 2 = 0">odd</xsl:when>
                <xsl:otherwise>even</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
	<tr class="{$even_or_odd}">
		<td align="center">
			<xsl:call-template name="DisplayIcon">
				<xsl:with-param name="type" select="@type"/>
			</xsl:call-template>
		</td>
    		<td title="{uri/@short}">
            <div style="position: relative; overflow: hidden;">
			<xsl:choose>
				<xsl:when test="@type = 'directory'">
					<a href="DisplayURI?uri={uri/@long}{$rootUri}" title="{uri/@short}">
                        <xsl:value-of select="uri/@short" />
					</a>
				</xsl:when>
				<xsl:otherwise>
					<a href="{uri/@long}" title="{uri/@short}">
                       <xsl:value-of select="uri/@short" />
					</a>
				</xsl:otherwise>
			</xsl:choose>
            <img src="{$image_uri}/fade_{$even_or_odd}.png" class="fader"/>
            </div>
		</td>
		<td align="center">
            <div style="position: relative; overflow: hidden;">
			<xsl:call-template name="DisplayDate">
				<xsl:with-param name="origDate" select="date"/>
			</xsl:call-template>
            <img src="{$image_uri}/fade_{$even_or_odd}.png" class="fader"/>
            </div>
		</td>
		<td align="right">
			<xsl:call-template name="DisplaySize">
				<xsl:with-param name="size" select="@fancy_size"/>
			</xsl:call-template>
		</td>
		<xsl:for-each select="./property">
			<td title="{@value}">
            <div style="position: relative; overflow: hidden;">
                <xsl:value-of select="@value" />
                <img src="{$image_uri}/fade_{$even_or_odd}.png" class="fader"/>
            </div>
			</td>
		</xsl:for-each>
	</tr>
	</xsl:for-each>
    </table>
</xsl:template>

<!--
================================================================================
CropText template

return $text cropped at $length characters
================================================================================
-->
<xsl:template name="CropText">
	<xsl:param name="text"/>
	<xsl:param name="length"/>
	<xsl:choose>
		<xsl:when test="$text = ''">&#160;</xsl:when>
		<xsl:when test="string-length($text) > $length">
			<xsl:value-of select="substring($text,1,$length - 3)"/>...
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$text"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!--
================================================================================
DisplayIcon template

return correct image according to $type
================================================================================
-->
<xsl:template name="DisplayIcon">
	<xsl:param name="type"/>
	<xsl:variable name="img">
	<xsl:choose>
		<xsl:when test="$type = 'directory'">inode-directory.png</xsl:when>
		<xsl:when test="$type = 'audio'">audio-x-generic.png</xsl:when>
		<xsl:when test="$type = 'image'">image-x-generic.png</xsl:when>
		<xsl:when test="$type = 'video'">video-x-generic.png</xsl:when>
		<xsl:when test="$type = 'spreadsheet'">application-vnd.oasis.opendocument.spreadsheet.png</xsl:when>
		<xsl:when test="$type = 'wordproc'">application-vnd.oasis.opendocument.text.png</xsl:when>
		<xsl:when test="$type = 'presentation'">application-vnd.oasis.opendocument.presentation.png</xsl:when>
		<xsl:when test="$type = 'compressed'">application-zip.png</xsl:when>
		<xsl:when test="$type = 'css'">text-css.png</xsl:when>
		<xsl:when test="$type = 'html'">text-html.png</xsl:when>
		<xsl:when test="$type = 'pdf'">application-pdf.png</xsl:when>
		<xsl:when test="$type = 'ps'">application-postscript.png</xsl:when>
		<xsl:when test="$type = 'xml'">text-xml.png</xsl:when>
		<xsl:when test="$type = 'text'">text-plain.png</xsl:when>
		<xsl:otherwise>unknown.png</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<img src="{$image_uri}/16x16/mimetypes/{$img}" alt="{$type}"/>
</xsl:template>

<!--
================================================================================
DisplayDate
	
return date in FR
================================================================================
-->

<xsl:template name="DisplayDate">
	<xsl:param name="origDate"/>
	
	<xsl:call-template name="AddZero"><xsl:with-param name="str" select="$origDate/day"/></xsl:call-template>&#160;
	<xsl:choose>
		<xsl:when test="$origDate/month = '1'"><xsl:value-of select="mioga:gettext('Jan')"/></xsl:when>
		<xsl:when test="$origDate/month = '2'"><xsl:value-of select="mioga:gettext('Feb')"/></xsl:when>
		<xsl:when test="$origDate/month = '3'"><xsl:value-of select="mioga:gettext('Mar')"/></xsl:when>
		<xsl:when test="$origDate/month = '4'"><xsl:value-of select="mioga:gettext('Apr')"/></xsl:when>
		<xsl:when test="$origDate/month = '5'"><xsl:value-of select="mioga:gettext('May')"/></xsl:when>
		<xsl:when test="$origDate/month = '6'"><xsl:value-of select="mioga:gettext('Jun')"/></xsl:when>
		<xsl:when test="$origDate/month = '7'"><xsl:value-of select="mioga:gettext('Jul')"/></xsl:when>
		<xsl:when test="$origDate/month = '8'"><xsl:value-of select="mioga:gettext('Aug')"/></xsl:when>
		<xsl:when test="$origDate/month = '9'"><xsl:value-of select="mioga:gettext('Sep')"/></xsl:when>
		<xsl:when test="$origDate/month = '10'"><xsl:value-of select="mioga:gettext('Oct')"/></xsl:when>
		<xsl:when test="$origDate/month = '11'"><xsl:value-of select="mioga:gettext('Nov')"/></xsl:when>
		<xsl:when test="$origDate/month = '12'"><xsl:value-of select="mioga:gettext('Dec')"/></xsl:when>
	</xsl:choose>&#160;
	<xsl:value-of select="$origDate/year"/>,&#160;
	
	<xsl:call-template name="AddZero"><xsl:with-param name="str" select="$origDate/hour"/></xsl:call-template>:<xsl:call-template name="AddZero"><xsl:with-param name="str" select="$origDate/min"/></xsl:call-template>:<xsl:call-template name="AddZero"><xsl:with-param name="str" select="$origDate/sec"/></xsl:call-template>
</xsl:template>

<!--
================================================================================
AddZero template
	
add zero if needed before given number
================================================================================
-->
<xsl:template name="AddZero">
	<xsl:param name="str"/>
	<xsl:if test="$str &lt; 10">0</xsl:if><xsl:value-of select="$str"/>
</xsl:template>

<!--
================================================================================
DisplaySize template

return size in FR
================================================================================
-->
<xsl:template name="DisplaySize">
	<xsl:param name="size"/>
	<xsl:choose>
		<xsl:when test="$size = '-'"><xsl:value-of select="$size"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="$size"/><xsl:value-of select="mioga:gettext('b')"/></xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!--
================================================================================
FBTranslations template

Translate strings
== FR ==
================================================================================
-->
<xsl:template name="FBTranslations">
	<xsl:param name="label"/>
	<xsl:choose>
		<xsl:when test="$label = 'Parent directory'"><xsl:value-of select="mioga:gettext('Parent directory')"/></xsl:when>
		<xsl:when test="$label = 'Name'"><xsl:value-of select="mioga:gettext('Name')"/></xsl:when>
		<xsl:when test="$label = 'Last modified'"><xsl:value-of select="mioga:gettext('Last modified')"/></xsl:when>
		<xsl:when test="$label = 'Size'"><xsl:value-of select="mioga:gettext('Size')"/></xsl:when>
		<xsl:when test="$label = 'Index of'"><xsl:value-of select="mioga:gettext('Index of')"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="$label"/></xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>
