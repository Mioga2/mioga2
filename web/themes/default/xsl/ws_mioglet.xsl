<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ws="http://www.mioga2.org/portal/mioglets/Workspace" xmlns:mioga="urn:mioga">

<xsl:output method="html"/>

<!-- =================================
     Handle HTML content
     ================================= -->

<!-- Editor Form -->

<xsl:template match="ws:Links" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">ws_mioglet.xsl</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">mode</xsl:with-param>
        <xsl:with-param name="value">user</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">sec</xsl:with-param>
        <xsl:with-param name="value" select="$node/sec"/>
    </xsl:call-template>
</xsl:template>


<!-- main Links template -->


<xsl:template match="ws:Links">
    <xsl:param name="node"/>

    <table class="wsLinks" cellpadding="5" cellspacing="0">
        <xsl:if test="count(link) = 0">
            <tr>
                <td>
                    <i><xsl:call-template name="ws-no-link"/></i>
                </td>
            </tr>
        </xsl:if>
        
        <xsl:for-each select="link">

            <tr>
                <xsl:if test="$node/boxed">
                   <xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
							<xsl:otherwise>oddRow</xsl:otherwise>
						</xsl:choose>
				   </xsl:attribute>
                </xsl:if>
                
                <td>
                    <a href="{link}">
                        <xsl:value-of select="ident"/>
                    </a>
                </td>
            </tr>
            
        </xsl:for-each>
        
        
        <tr>
            <xsl:if test="$node/boxed">
					<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
							<xsl:otherwise>oddRow</xsl:otherwise>
						</xsl:choose>
				   </xsl:attribute>
            </xsl:if>
            
            <td colspan="2">
                <xsl:call-template name="ws-manage-link">
                    <xsl:with-param name="href">
                        <xsl:value-of select="$bin_uri"/>/<xsl:value-of select="$mioga_context/user/ident"/>/Workspace/DisplayLinks
                    </xsl:with-param>
                </xsl:call-template>
            </td>
        </tr>
        
    </table>

</xsl:template>

<xsl:template name="ws-manage-link">
    <xsl:param name="href"/>
    <xsl:value-of select="mioga:gettext('Manage links:')"/>&#160;<a href="{$href}" style="font-size: small">[...]</a>
</xsl:template>

<xsl:template name="ws-no-link">
    <xsl:value-of select="mioga:gettext('No link')"/>
</xsl:template>

<!-- =================================
     Handle TopBar
     ================================= -->

<!-- Editor Form -->

<xsl:template match="ws:TopBar" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">ws_mioglet.xsl</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">mode</xsl:with-param>
        <xsl:with-param name="value">user</xsl:with-param>
    </xsl:call-template>

</xsl:template>

<xsl:template match="ws:TopBar">
    <xsl:param name="node"/>

    <xsl:call-template name="DisplayAppTitle"/>
</xsl:template>

<!-- =================================
     Handle ConnectedUsers
     ================================= -->

<!-- Editor Form -->

<xsl:template match="ws:ConnectedUsers" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">ws_mioglet.xsl</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">mode</xsl:with-param>
        <xsl:with-param name="value">user</xsl:with-param>
    </xsl:call-template>

	<xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">sec</xsl:with-param>
        <xsl:with-param name="value" select="$node/sec"/>
    </xsl:call-template>
	
</xsl:template>

<xsl:template match="ws:ConnectedUsers">
    <xsl:param name="node"/>

	<xsl:if test="count(user) != 0">
    <table class="wsConnectedUsers" cellpadding="5" cellspacing="0">
    <xsl:for-each select="user">
	    <tr>
		    <xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
					<xsl:otherwise>oddRow</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<td>
				<span style="background: url({$image_uri}/16x16/places/user-identity.png); padding-left: 20px; background-repeat: no-repeat;"><xsl:value-of select="."/></span>
			</td>
      </tr>
    </xsl:for-each>
    </table>
	</xsl:if>
</xsl:template>

<!-- =================================
     Handle Weather
     ================================= -->

<!-- Editor Form -->

<xsl:template match="ws:Weather" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">ws_mioglet.xsl</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">mode</xsl:with-param>
        <xsl:with-param name="value">user</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">wcode</xsl:with-param>
        <xsl:with-param name="value" select="$node/wcode"/>
    </xsl:call-template>

</xsl:template>

<xsl:template match="ws:Weather">
    <xsl:param name="node"/>

	<xsl:apply-templates select="error">
		<xsl:with-param name="node" select="$node"/>
    </xsl:apply-templates>
	
	<xsl:if test="count(current) != 0">
    <table class="wsWeather" cellpadding="5" cellspacing="0">
	    <tr>
		    <xsl:attribute name="class">
  				<xsl:choose>
					<xsl:when test="position() mod 2 = 1">evenRow</xsl:when>
					<xsl:otherwise>oddRow</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<td class="bigIcon">
				<img src="{$image_uri}/weather/large/{./current/@icon}.png" alt="{./current}" />
			</td>
			<td class="temp">
				<strong><xsl:value-of select="./current/@temp"/>°C</strong>
			</td>
			<td class="location">
				<strong><xsl:value-of select="./location/."/></strong><br />
				<xsl:call-template name="MiogletWeatherTranslation">
					<xsl:with-param name="weather" select="./current"/>
			    </xsl:call-template>
			</td>
      </tr>
    </table>
    <xsl:if test="count(day) != 0">
    	<table class="wsWeather" cellpadding="5" cellspacing="0">
    	<xsl:for-each select="day">
	    <tr>
		    <xsl:attribute name="class">
  				<xsl:choose>
					<xsl:when test="position() mod 2 = 1">oddRow</xsl:when>
					<xsl:otherwise>evenRow</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<td class="smallIcon">
				<img src="{$image_uri}/weather/small/{@icon}.png" alt="{.}" />
			</td>
			<td class="temp">
				<small><xsl:value-of select="@temp"/>°C</small>
			</td>
			<td class="location">
				<xsl:call-template name="MiogletWeatherDateTranslation">
					<xsl:with-param name="dw" select="@dw"/>
					<xsl:with-param name="month" select="@month"/>
					<xsl:with-param name="day" select="@d"/>
			    </xsl:call-template>
				<br />
				<small>
					<xsl:call-template name="MiogletWeatherTranslation">
						<xsl:with-param name="weather" select="."/>
					</xsl:call-template>
				</small>
			</td>
      </tr>
      </xsl:for-each>
    </table>
    </xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template match="error">
	<span class="error"><xsl:value-of select="."/></span>
</xsl:template>

<xsl:template name="MiogletWeatherTranslation">
	<xsl:param name="weather"/>
	
	<xsl:choose>
    <xsl:when test="$weather = 'N/a'"><xsl:value-of select="mioga:gettext('N/a')"/></xsl:when>
    <xsl:when test="$weather = 'Partly cloudy'"><xsl:value-of select="mioga:gettext('Partly cloudy')"/></xsl:when>
    <xsl:when test="$weather = 'Sunny'"><xsl:value-of select="mioga:gettext('Sunny')"/></xsl:when>
    <xsl:when test="$weather = 'Cloudy / wind'"><xsl:value-of select="mioga:gettext('Cloudy / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Mostly cloudy'"><xsl:value-of select="mioga:gettext('Mostly cloudy')"/></xsl:when>
    <xsl:when test="$weather = 'Mostly sunny'"><xsl:value-of select="mioga:gettext('Mostly sunny')"/></xsl:when>
    <xsl:when test="$weather = 'Rain / wind'"><xsl:value-of select="mioga:gettext('Rain / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Rain'"><xsl:value-of select="mioga:gettext('Rain')"/></xsl:when>
    <xsl:when test="$weather = 'Am showers'"><xsl:value-of select="mioga:gettext('Am showers')"/></xsl:when>
    <xsl:when test="$weather = 'Mostly cloudy / wind'"><xsl:value-of select="mioga:gettext('Mostly cloudy / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Pm rain / snow showers'"><xsl:value-of select="mioga:gettext('Pm rain / snow showers')"/></xsl:when>
    <xsl:when test="$weather = 'Snow shower'"><xsl:value-of select="mioga:gettext('Snow shower')"/></xsl:when>
    <xsl:when test="$weather = 'Heavy rain / wind'"><xsl:value-of select="mioga:gettext('Heavy rain / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Heavy rain'"><xsl:value-of select="mioga:gettext('Heavy rain')"/></xsl:when>
    <xsl:when test="$weather = 'Mostly cloudy and windy'"><xsl:value-of select="mioga:gettext('Mostly cloudy and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Cloudy'"><xsl:value-of select="mioga:gettext('Cloudy')"/></xsl:when>
    <xsl:when test="$weather = 'Am snow showers'"><xsl:value-of select="mioga:gettext('Am snow showers')"/></xsl:when>
    <xsl:when test="$weather = 'Rain / snow'"><xsl:value-of select="mioga:gettext('Rain / snow')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain'"><xsl:value-of select="mioga:gettext('Light rain')"/></xsl:when>
    <xsl:when test="$weather = 'Pm showers'"><xsl:value-of select="mioga:gettext('Pm showers')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain / wind'"><xsl:value-of select="mioga:gettext('Light rain / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Partly cloudy / wind'"><xsl:value-of select="mioga:gettext('Partly cloudy / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Few showers / wind'"><xsl:value-of select="mioga:gettext('Few showers / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Showers / wind'"><xsl:value-of select="mioga:gettext('Showers / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Fair'"><xsl:value-of select="mioga:gettext('Fair')"/></xsl:when>
    <xsl:when test="$weather = 'Showers'"><xsl:value-of select="mioga:gettext('Showers')"/></xsl:when>
    <xsl:when test="$weather = 'T-storms'"><xsl:value-of select="mioga:gettext('T-storms')"/></xsl:when>
    <xsl:when test="$weather = 'Few showers'"><xsl:value-of select="mioga:gettext('Few showers')"/></xsl:when>
    <xsl:when test="$weather = 'Foggy'"><xsl:value-of select="mioga:gettext('Foggy')"/></xsl:when>
    <xsl:when test="$weather = 'Sunny intervals'"><xsl:value-of select="mioga:gettext('Sunny intervals')"/></xsl:when>
    <xsl:when test="$weather = 'Scattered showers'"><xsl:value-of select="mioga:gettext('Scattered showers')"/></xsl:when>
    <xsl:when test="$weather = 'Clear'"><xsl:value-of select="mioga:gettext('Clear')"/></xsl:when>
    <xsl:when test="$weather = 'Light snow'"><xsl:value-of select="mioga:gettext('Light snow')"/></xsl:when>
    <xsl:when test="$weather = 'Am snow'"><xsl:value-of select="mioga:gettext('Am snow')"/></xsl:when>
    <xsl:when test="$weather = 'Flurries'"><xsl:value-of select="mioga:gettext('Flurries')"/></xsl:when>
    <xsl:when test="$weather = 'Few snow showers'"><xsl:value-of select="mioga:gettext('Few snow showers')"/></xsl:when>
    <xsl:when test="$weather = 'Showers in the vicinity'"><xsl:value-of select="mioga:gettext('Showers in the vicinity')"/></xsl:when>
    <xsl:when test="$weather = 'Snow'"><xsl:value-of select="mioga:gettext('Snow')"/></xsl:when>
    <xsl:when test="$weather = 'Pm snow'"><xsl:value-of select="mioga:gettext('Pm snow')"/></xsl:when>
    <xsl:when test="$weather = 'Pm snow showers'"><xsl:value-of select="mioga:gettext('Pm snow showers')"/></xsl:when>
    <xsl:when test="$weather = 'Rain / snow / wind'"><xsl:value-of select="mioga:gettext('Rain / snow / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Scattered t-storms'"><xsl:value-of select="mioga:gettext('Scattered t-storms')"/></xsl:when>
    <xsl:when test="$weather = 'Isolated t-storms'"><xsl:value-of select="mioga:gettext('Isolated t-storms')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain shower'"><xsl:value-of select="mioga:gettext('Light rain shower')"/></xsl:when>
    <xsl:when test="$weather = 'Rain / snow showers'"><xsl:value-of select="mioga:gettext('Rain / snow showers')"/></xsl:when>
    <xsl:when test="$weather = 'Light showers'"><xsl:value-of select="mioga:gettext('Light showers')"/></xsl:when>
    <xsl:when test="$weather = 'Clear / wind'"><xsl:value-of select="mioga:gettext('Clear / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Snow / wind'"><xsl:value-of select="mioga:gettext('Snow / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Scattered t-storms'"><xsl:value-of select="mioga:gettext('Scattered t-storms')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain / freezing rain'"><xsl:value-of select="mioga:gettext('Light rain / freezing rain')"/></xsl:when>
    <xsl:when test="$weather = 'Wintry mix'"><xsl:value-of select="mioga:gettext('Wintry mix')"/></xsl:when>
    <xsl:when test="$weather = 'Sleet'"><xsl:value-of select="mioga:gettext('Sleet')"/></xsl:when>
    <xsl:when test="$weather = 'Thundery showers'"><xsl:value-of select="mioga:gettext('Thundery showers')"/></xsl:when>
    <xsl:when test="$weather = 'Light snow showers'"><xsl:value-of select="mioga:gettext('Light snow showers')"/></xsl:when>
    <xsl:when test="$weather = 'Heavy snow'"><xsl:value-of select="mioga:gettext('Heavy snow')"/></xsl:when>
    <xsl:when test="$weather = 'Haze'"><xsl:value-of select="mioga:gettext('Haze')"/></xsl:when>
    <xsl:when test="$weather = 'Heavy showers'"><xsl:value-of select="mioga:gettext('Heavy showers')"/></xsl:when>
    <xsl:when test="$weather = 'Misty'"><xsl:value-of select="mioga:gettext('Misty')"/></xsl:when>
    <xsl:when test="$weather = 'Light wintry mix'"><xsl:value-of select="mioga:gettext('Light wintry mix')"/></xsl:when>
    <xsl:when test="$weather = 'Heavy snow / wind'"><xsl:value-of select="mioga:gettext('Heavy snow / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Am rain / snow'"><xsl:value-of select="mioga:gettext('Am rain / snow')"/></xsl:when>
    <xsl:when test="$weather = 'Partly cloudy and windy'"><xsl:value-of select="mioga:gettext('Partly cloudy and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Light drizzle'"><xsl:value-of select="mioga:gettext('Light drizzle')"/></xsl:when>
    <xsl:when test="$weather = 'Am clouds / pm sun'"><xsl:value-of select="mioga:gettext('Am clouds / pm sun')"/></xsl:when>
    <xsl:when test="$weather = 'Cloudy and windy'"><xsl:value-of select="mioga:gettext('Cloudy and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Light snow and windy'"><xsl:value-of select="mioga:gettext('Light snow and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain shower and windy'"><xsl:value-of select="mioga:gettext('Light rain shower and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain and windy'"><xsl:value-of select="mioga:gettext('Light rain and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Am clouds / pm sun'"><xsl:value-of select="mioga:gettext('Am clouds / pm sun')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain and windy'"><xsl:value-of select="mioga:gettext('Light rain and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Heavy rain shower'"><xsl:value-of select="mioga:gettext('Heavy rain shower')"/></xsl:when>
    <xsl:when test="$weather = 'Rain / snow showers / wind'"><xsl:value-of select="mioga:gettext('Rain / snow showers / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Rain shower'"><xsl:value-of select="mioga:gettext('Rain shower')"/></xsl:when>
    <xsl:when test="$weather = 'Cloudy and windy'"><xsl:value-of select="mioga:gettext('Cloudy and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Partly cloudy and windy'"><xsl:value-of select="mioga:gettext('Partly cloudy and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Light snow and windy'"><xsl:value-of select="mioga:gettext('Light snow and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Rain / snow showers / wind'"><xsl:value-of select="mioga:gettext('Rain / snow showers / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Rain shower'"><xsl:value-of select="mioga:gettext('Rain shower')"/></xsl:when>
    <xsl:when test="$weather = 'Heavy rain shower'"><xsl:value-of select="mioga:gettext('Heavy rain shower')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain and windy'"><xsl:value-of select="mioga:gettext('Light rain and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Rain and snow'"><xsl:value-of select="mioga:gettext('Rain and snow')"/></xsl:when>
    <xsl:when test="$weather = 'Light drizzle and windy'"><xsl:value-of select="mioga:gettext('Light drizzle and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Drifting snow'"><xsl:value-of select="mioga:gettext('Drifting snow')"/></xsl:when>
    <xsl:when test="$weather = 'Flurries / wind'"><xsl:value-of select="mioga:gettext('Flurries / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Scattered flurries'"><xsl:value-of select="mioga:gettext('Scattered flurries')"/></xsl:when>
    <xsl:when test="$weather = 'Am rain / snow showers'"><xsl:value-of select="mioga:gettext('Am rain / snow showers')"/></xsl:when>
    <xsl:when test="$weather = 'Heavy snow showers'"><xsl:value-of select="mioga:gettext('Heavy snow showers')"/></xsl:when>
    <xsl:when test="$weather = 'Blowing snow and windy'"><xsl:value-of select="mioga:gettext('Blowing snow and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Fair and windy'"><xsl:value-of select="mioga:gettext('Fair and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Light snow and sleet'"><xsl:value-of select="mioga:gettext('Light snow and sleet')"/></xsl:when>
    <xsl:when test="$weather = 'Snow and fog'"><xsl:value-of select="mioga:gettext('Snow and fog')"/></xsl:when>
    <xsl:when test="$weather = 'Light freezing drizzle'"><xsl:value-of select="mioga:gettext('Light freezing drizzle')"/></xsl:when>
    <xsl:when test="$weather = 'Mostly clear'"><xsl:value-of select="mioga:gettext('Mostly clear')"/></xsl:when>
    <xsl:when test="$weather = 'Clear'"><xsl:value-of select="mioga:gettext('Clear')"/></xsl:when>
    <xsl:when test="$weather = 'Cloudy'"><xsl:value-of select="mioga:gettext('Cloudy')"/></xsl:when>
    <xsl:when test="$weather = 'Drizzle'"><xsl:value-of select="mioga:gettext('Drizzle')"/></xsl:when>
    <xsl:when test="$weather = 'Scattered snow showers'"><xsl:value-of select="mioga:gettext('Scattered snow showers')"/></xsl:when>
    <xsl:when test="$weather = 'T-showers'"><xsl:value-of select="mioga:gettext('T-showers')"/></xsl:when>
    <xsl:when test="$weather = 'Sprinkles'"><xsl:value-of select="mioga:gettext('Sprinkles')"/></xsl:when>
    <xsl:when test="$weather = 'Snow shower / wind'"><xsl:value-of select="mioga:gettext('Snow shower / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Mist'"><xsl:value-of select="mioga:gettext('Mist')"/></xsl:when>
    <xsl:when test="$weather = 'Light snow shower'"><xsl:value-of select="mioga:gettext('Light snow shower')"/></xsl:when>
    <xsl:when test="$weather = 'Light freezing rain'"><xsl:value-of select="mioga:gettext('Light freezing rain')"/></xsl:when>
    <xsl:when test="$weather = 'Wintry mix to snow'"><xsl:value-of select="mioga:gettext('Wintry mix to snow')"/></xsl:when>
    <xsl:when test="$weather = 'Light drizzle'"><xsl:value-of select="mioga:gettext('Light drizzle')"/></xsl:when>
    <xsl:when test="$weather = 'Pm snow showers / wind'"><xsl:value-of select="mioga:gettext('Pm snow showers / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Shallow fog'"><xsl:value-of select="mioga:gettext('Shallow fog')"/></xsl:when>
    <xsl:when test="$weather = 'Light freezing rain and fog'"><xsl:value-of select="mioga:gettext('Light freezing rain and fog')"/></xsl:when>
    <xsl:when test="$weather = 'Rain and windy'"><xsl:value-of select="mioga:gettext('Rain and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Fog'"><xsl:value-of select="mioga:gettext('Fog')"/></xsl:when>
    <xsl:when test="$weather = 'Heavy rain and windy'"><xsl:value-of select="mioga:gettext('Heavy rain and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Light freezing rain'"><xsl:value-of select="mioga:gettext('Light freezing rain')"/></xsl:when>
    <xsl:when test="$weather = 'Drifting snow'"><xsl:value-of select="mioga:gettext('Drifting snow')"/></xsl:when>
    <xsl:when test="$weather = 'Light snow shower'"><xsl:value-of select="mioga:gettext('Light snow shower')"/></xsl:when>
    <xsl:when test="$weather = 'Light snow grains'"><xsl:value-of select="mioga:gettext('Light snow grains')"/></xsl:when>
    <xsl:when test="$weather = 'Scattered snow showers'"><xsl:value-of select="mioga:gettext('Scattered snow showers')"/></xsl:when>
    <xsl:when test="$weather = 'Thunder'"><xsl:value-of select="mioga:gettext('Thunder')"/></xsl:when>
    <xsl:when test="$weather = 'Rain / thunder'"><xsl:value-of select="mioga:gettext('Rain / thunder')"/></xsl:when>
    <xsl:when test="$weather = 'Light snow and fog'"><xsl:value-of select="mioga:gettext('Light snow and fog')"/></xsl:when>
    <xsl:when test="$weather = 'Snow grains'"><xsl:value-of select="mioga:gettext('Snow grains')"/></xsl:when>
    <xsl:when test="$weather = 'Snow and thunder and fog'"><xsl:value-of select="mioga:gettext('Snow and thunder and fog')"/></xsl:when>
    <xsl:when test="$weather = 'Light freezing rain and fog'"><xsl:value-of select="mioga:gettext('Light freezing rain and fog')"/></xsl:when>
    <xsl:when test="$weather = 'Blowing snow'"><xsl:value-of select="mioga:gettext('Blowing snow')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain and freezing rain'"><xsl:value-of select="mioga:gettext('Light rain and freezing rain')"/></xsl:when>
    <xsl:when test="$weather = 'Rain and snow'"><xsl:value-of select="mioga:gettext('Rain and snow')"/></xsl:when>
    <xsl:when test="$weather = 'Light drizzle'"><xsl:value-of select="mioga:gettext('Light drizzle')"/></xsl:when>
    <xsl:when test="$weather = 'T-storm'"><xsl:value-of select="mioga:gettext('T-storm')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain with thunder'"><xsl:value-of select="mioga:gettext('Light rain with thunder')"/></xsl:when>
    <xsl:when test="$weather = 'Scattered-storms'"><xsl:value-of select="mioga:gettext('Scattered-storms')"/></xsl:when>
    <xsl:when test="$weather = 'Snow and sleet'"><xsl:value-of select="mioga:gettext('Snow and sleet')"/></xsl:when>
    <xsl:when test="$weather = 'Sleet showers'"><xsl:value-of select="mioga:gettext('Sleet showers')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain with thunder'"><xsl:value-of select="mioga:gettext('Light rain with thunder')"/></xsl:when>
    <xsl:when test="$weather = 'Light snow and fog'"><xsl:value-of select="mioga:gettext('Light snow and fog')"/></xsl:when>
    <xsl:when test="$weather = 'Heavy snow and windy'"><xsl:value-of select="mioga:gettext('Heavy snow and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain with thunder'"><xsl:value-of select="mioga:gettext('Light rain with thunder')"/></xsl:when>
    <xsl:when test="$weather = 'Light freezing rain'"><xsl:value-of select="mioga:gettext('Light freezing rain')"/></xsl:when>
    <xsl:when test="$weather = 'Am t-storms / wind'"><xsl:value-of select="mioga:gettext('Am t-storms / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Scattered snow showers / wind'"><xsl:value-of select="mioga:gettext('Scattered snow showers / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Pm light rain'"><xsl:value-of select="mioga:gettext('Pm light rain')"/></xsl:when>
    <xsl:when test="$weather = 'Am light rain'"><xsl:value-of select="mioga:gettext('Am light rain')"/></xsl:when>
    <xsl:when test="$weather = 'Light rain with thunder'"><xsl:value-of select="mioga:gettext('Light rain with thunder')"/></xsl:when>
    <xsl:when test="$weather = 'Rain and windy'"><xsl:value-of select="mioga:gettext('Rain and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Rain shower and windy'"><xsl:value-of select="mioga:gettext('Rain shower and windy')"/></xsl:when>
    <xsl:when test="$weather = 'Mist and light rain'"><xsl:value-of select="mioga:gettext('Mist and light rain')"/></xsl:when>
    <xsl:when test="$weather = 'Scattered t-storms / wind'"><xsl:value-of select="mioga:gettext('Scattered t-storms / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Sunny / wind'"><xsl:value-of select="mioga:gettext('Sunny / wind')"/></xsl:when>
    <xsl:when test="$weather = 'Rain and snow'"><xsl:value-of select="mioga:gettext('Rain and snow')"/></xsl:when>
    <xsl:when test="$weather = 'Pm light snow'"><xsl:value-of select="mioga:gettext('Pm light snow')"/></xsl:when>
    <xsl:when test="$weather = 'Am light rain'"><xsl:value-of select="mioga:gettext('Am light rain')"/></xsl:when>
    <xsl:when test="$weather = 'Mist'"><xsl:value-of select="mioga:gettext('Mist')"/></xsl:when>
    <xsl:when test="$weather = 'Drizzle'"><xsl:value-of select="mioga:gettext('Drizzle')"/></xsl:when>
    <xsl:when test="$weather = 'Variable'"><xsl:value-of select="mioga:gettext('Variable')"/></xsl:when>
    <xsl:when test="$weather = 'Rising'"><xsl:value-of select="mioga:gettext('Rising')"/></xsl:when>
    <xsl:when test="$weather = 'Steady'"><xsl:value-of select="mioga:gettext('Steady')"/></xsl:when>
    <xsl:when test="$weather = 'Falling'"><xsl:value-of select="mioga:gettext('Falling')"/></xsl:when>
    <xsl:when test="$weather = 'Unlimited'"><xsl:value-of select="mioga:gettext('Unlimited')"/></xsl:when>
    <xsl:when test="$weather = 'Snow to rain'"><xsl:value-of select="mioga:gettext('Snow to rain')"/></xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template name="MiogletWeatherDateTranslation">
	<xsl:param name="dw"/>
	<xsl:param name="day"/>
	<xsl:param name="month"/>
	
	<xsl:choose>
		<xsl:when test="$dw = 'Monday'"><xsl:value-of select="mioga:gettext('Monday')"/></xsl:when>
		<xsl:when test="$dw = 'Tuesday'"><xsl:value-of select="mioga:gettext('Tuesday')"/></xsl:when>
		<xsl:when test="$dw = 'Wednesday'"><xsl:value-of select="mioga:gettext('Wednesday')"/></xsl:when>
		<xsl:when test="$dw = 'Thursday'"><xsl:value-of select="mioga:gettext('Thursday')"/></xsl:when>
		<xsl:when test="$dw = 'Friday'"><xsl:value-of select="mioga:gettext('Friday')"/></xsl:when>
		<xsl:when test="$dw = 'Saturday'"><xsl:value-of select="mioga:gettext('Saturday')"/></xsl:when>
		<xsl:when test="$dw = 'Sunday'"><xsl:value-of select="mioga:gettext('Sunday')"/></xsl:when>
	</xsl:choose>
	&#160;<xsl:value-of select="$day"/>&#160;
	<xsl:choose>
		<xsl:when test="$month = 'January'"><xsl:value-of select="mioga:gettext('January')"/></xsl:when>
		<xsl:when test="$month = 'February'"><xsl:value-of select="mioga:gettext('February')"/></xsl:when>
		<xsl:when test="$month = 'March'"><xsl:value-of select="mioga:gettext('March')"/></xsl:when>
		<xsl:when test="$month = 'April'"><xsl:value-of select="mioga:gettext('April')"/></xsl:when>
		<xsl:when test="$month = 'May'"><xsl:value-of select="mioga:gettext('May')"/></xsl:when>
		<xsl:when test="$month = 'June'"><xsl:value-of select="mioga:gettext('June')"/></xsl:when>
		<xsl:when test="$month = 'July'"><xsl:value-of select="mioga:gettext('July')"/></xsl:when>
		<xsl:when test="$month = 'August'"><xsl:value-of select="mioga:gettext('August')"/></xsl:when>
		<xsl:when test="$month = 'September'"><xsl:value-of select="mioga:gettext('September')"/></xsl:when>
		<xsl:when test="$month = 'October'"><xsl:value-of select="mioga:gettext('October')"/></xsl:when>
		<xsl:when test="$month = 'November'"><xsl:value-of select="mioga:gettext('November')"/></xsl:when>
		<xsl:when test="$month = 'December'"><xsl:value-of select="mioga:gettext('December')"/></xsl:when>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>
