<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<!-- ===============================
     EditContact
     Main contacts edition page
     =============================== -->

<xsl:template match="DisplayContact">
  <xsl:variable name="function" select="Function" />
    
  <body xsl:use-attribute-sets="body_attr" >
    <xsl:call-template name="MiogaTitle"/>

    <table align="center" cellspacing="20">
      <tr>
        <td align="center" >
          <font>
            <xsl:attribute name="size">+2</xsl:attribute>
            <xsl:attribute name="class"><xsl:value-of select="$mioga-title-color" /></xsl:attribute>
            <img src="{$image_uri}/transparent_fill.gif" width="5" height="1" />
            <xsl:value-of select="surname" />
            <img src="{$image_uri}/transparent_fill.gif" width="5" height="1" />
            <xsl:value-of select="name" />
          </font>
        </td>
        
        <td align="right" colspan="2">
          <xsl:if test="photo_url!=''" >
            <img>
              <xsl:attribute name="src"><xsl:value-of select="photo_url" /></xsl:attribute>
            </img>
          </xsl:if>
          <xsl:if test="photo_data!=''" >
            <img>
              <xsl:attribute name="src"><xsl:value-of select="$private_bin_uri" />/Contact/GetPhoto?oid=<xsl:value-of select="photo_data"/></xsl:attribute>
            </img>
          </xsl:if>
        </td>
      </tr>
      <tr>        
        <td valign="top">
          <xsl:call-template name="visu-zone" >
            <xsl:with-param name="zone">visu-identity</xsl:with-param>
            <xsl:with-param name="title">identity</xsl:with-param>
            <xsl:with-param name="icon">identity.gif</xsl:with-param>
          </xsl:call-template>
        </td>

        <td rowspan="3">
            <hr class="{$mioga-title-color} {$mioga-title-bg-color}" style="height: 100%; border: 0; width: 1px; margin-right: 10px; margin-left: 10px;"/>
        </td>
        
        <td  valign="top">
          <xsl:call-template name="visu-zone" >
            <xsl:with-param name="zone">visu-telephone</xsl:with-param>
            <xsl:with-param name="title">phone</xsl:with-param>
            <xsl:with-param name="icon">telephone.gif</xsl:with-param>
          </xsl:call-template>
        </td>
      </tr>
      <tr>
        <td valign="top">
          <xsl:call-template name="visu-zone" >
            <xsl:with-param name="zone">visu-work-address</xsl:with-param>
            <xsl:with-param name="title">professional-address</xsl:with-param>
            <xsl:with-param name="icon">work.gif</xsl:with-param>
          </xsl:call-template>
        </td>
        <td  valign="top">
          <xsl:call-template name="visu-zone" >
            <xsl:with-param name="zone">visu-home-address</xsl:with-param>
            <xsl:with-param name="title">personal-address</xsl:with-param>
            <xsl:with-param name="icon">16x16/actions/go-home.png</xsl:with-param>
          </xsl:call-template>
        </td>
      </tr>
      <tr>
        <td  valign="top">
          <xsl:call-template name="visu-zone" >
            <xsl:with-param name="zone">visu-email</xsl:with-param>
            <xsl:with-param name="title">mail</xsl:with-param>
            <xsl:with-param name="icon">email.gif</xsl:with-param>
          </xsl:call-template>
        </td>
        <td valign="top">
          <xsl:call-template name="visu-zone" >
            <xsl:with-param name="zone">visu-comments</xsl:with-param>
            <xsl:with-param name="title">comments</xsl:with-param>
            <xsl:with-param name="icon">comments.gif</xsl:with-param>
          </xsl:call-template>
        </td>
      </tr>
    </table>

    <br/>

    <center>
        <xsl:call-template name="back-button">
            <xsl:with-param name="href" select="referer"/>
        </xsl:call-template>
    </center>

  </body>
</xsl:template>

<!-- ===============================
     visu-zone

     Visualisation zone includes a title 
     and a body
     =============================== -->
<xsl:template name="visu-zone">
    <xsl:param name="title"/>
    <xsl:param name="zone"/>
    
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <th colspan="3" align="left" valign="top" nowrap="1">
        <img src="{$image_uri}/16x16/actions/arrow-right.png" />
        <font>
          <xsl:attribute name="class"><xsl:value-of select="$mioga-title-color" /></xsl:attribute>
          <img src="{$image_uri}/transparent_fill.gif" width="5" height="1" />
            <xsl:call-template name="cvTranslation">
              <xsl:with-param name="type" select="$title"/>
            </xsl:call-template> : 
        </font>
      </th>
    </tr>
    <tr>
      <!--
      <td align="center" valign="top">
        <img>
          <xsl:attribute name="src"><xsl:value-of select="$image_uri" />/<xsl:value-of select="$icon" /></xsl:attribute>
        </img>
      </td>
      -->
      <td><img src="{$image_uri}/transparent_fill.gif" width="20" height="1" /></td>
      <td align="left">
        <xsl:choose>
          <xsl:when test="$zone='visu-identity'"><xsl:call-template name="visu-identity" /></xsl:when>
          <xsl:when test="$zone='visu-telephone'"><xsl:call-template name="visu-telephone" /></xsl:when>
          <xsl:when test="$zone='visu-email'"><xsl:call-template name="visu-email" /></xsl:when>
          <xsl:when test="$zone='visu-work-address'"><xsl:call-template name="visu-work-address" /></xsl:when>
          <xsl:when test="$zone='visu-home-address'"><xsl:call-template name="visu-home-address" /></xsl:when>
          <xsl:when test="$zone='visu-comments'"><xsl:call-template name="visu-comments" /></xsl:when>
        </xsl:choose>
      </td>
    </tr>
  </table>
</xsl:template>


<!-- ===============================
     visu-zone-field

     It's a table row containing 
     an edition field with  
     a label and a text.
     =============================== -->
<xsl:template name="visu-zone-field">
    <xsl:param name="field-label"/>
    <xsl:param name="field-value"/>
    
  <tr>
    <td nowrap="1">                
      <font>
        <xsl:attribute name="class"><xsl:value-of select="$mioga-title-color" /></xsl:attribute>
        <xsl:call-template name="cvTranslation">
          <xsl:with-param name="type" select="$field-label"/>
        </xsl:call-template> :
      </font>
    </td>
    <td>
      <font class="{$mioga-title-color}"><xsl:value-of select="$field-value" /></font>
    </td>
  </tr>
</xsl:template>

<!-- ===============================
     visu-url-zone-field

     It's a table row containing 
     an edition field with  
     a label and an url.
     =============================== -->
<xsl:template name="visu-url-zone-field">
    <xsl:param name="field-label"/>
    <xsl:param name="field-url"/>
    <xsl:param name="field-value"/>
    
  <tr>
    <td nowrap="1">                
      <font>
        <xsl:attribute name="class"><xsl:value-of select="$mioga-title-color" /></xsl:attribute>
        <xsl:call-template name="cvTranslation">
          <xsl:with-param name="type" select="$field-label"/>
        </xsl:call-template> :
      </font>
    </td>
    <td>
      <xsl:element name="a">
        <xsl:attribute name="href"><xsl:value-of select="$field-url" /></xsl:attribute>
        <xsl:attribute name="target">_blank</xsl:attribute>
        <xsl:value-of select="$field-value" />
      </xsl:element>
    </td>
  </tr>
</xsl:template>

<!-- ===============================
     visu-mail-zone-field

     It's a table row containing 
     an edition field with  
     a label and an url.
     =============================== -->
<xsl:template name="visu-mail-zone-field">
    <xsl:param name="field-label"/>
    <xsl:param name="field-url"/>
    <xsl:param name="field-value"/>
    
  <tr>
    <td nowrap="1">                
      <font>
        <xsl:attribute name="class"><xsl:value-of select="$mioga-title-color" /></xsl:attribute>
        <xsl:call-template name="cvTranslation">
          <xsl:with-param name="type" select="$field-label"/>
        </xsl:call-template> :
      </font>
    </td>
    <td>
      <xsl:element name="a">
        <xsl:attribute name="href"><xsl:value-of select="$field-url" /></xsl:attribute>
        <xsl:value-of select="$field-value" />
      </xsl:element>
    </td>
  </tr>
</xsl:template>



<!-- ===============================
     visu-zone-field-cells

     It's two <td>  containing 
     a label and a text .
     =============================== -->
<xsl:template name="visu-zone-field-cells">
  <td nowrap="1">                
    <font>
      <xsl:attribute name="class"><xsl:value-of select="$mioga-title-color" /></xsl:attribute>
      <xsl:value-of select="$field-label"/> :
    </font>
  </td>
  <td>
    <xsl:value-of select="$field-value" />
  </td>
</xsl:template>


<!-- ===============================
     identity
     =============================== -->
<xsl:template name="visu-identity" >
  <table border="0" cellspacing="0" cellpadding="2">
    <xsl:call-template name="visu-zone-field">
      <xsl:with-param name="field-label">title</xsl:with-param>
      <xsl:with-param name="field-value"><xsl:value-of select="title" /></xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="visu-zone-field">
      <xsl:with-param name="field-label">company</xsl:with-param>
      <xsl:with-param name="field-value"><xsl:value-of select="orgname" /></xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="visu-url-zone-field">
      <xsl:with-param name="field-label">website</xsl:with-param>
      <xsl:with-param name="field-value"><xsl:value-of select="url"/></xsl:with-param>
      <xsl:with-param name="field-url"><xsl:value-of select="url"/></xsl:with-param>
    </xsl:call-template>

  </table>
</xsl:template>
    
<!-- ===============================
     telephone
     =============================== -->
<xsl:template name="visu-telephone" >
  <table border="0" cellspacing="0" cellpadding="2">
    <xsl:call-template name="visu-zone-field">
      <xsl:with-param name="field-label">work</xsl:with-param>
      <xsl:with-param name="field-value"><xsl:value-of select="tel_work" /></xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="visu-zone-field">
      <xsl:with-param name="field-label">mobile</xsl:with-param>
      <xsl:with-param name="field-value"><xsl:value-of select="tel_mobile" /></xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="visu-zone-field">
      <xsl:with-param name="field-label">fax</xsl:with-param>
      <xsl:with-param name="field-value"><xsl:value-of select="tel_fax" /></xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="visu-zone-field">
      <xsl:with-param name="field-label">home</xsl:with-param>
      <xsl:with-param name="field-value"><xsl:value-of select="tel_home" /></xsl:with-param>
    </xsl:call-template>
  </table>
</xsl:template>
    

<!-- ===============================
     email
     =============================== -->
<xsl:template name="visu-email" >
  <table border="0" cellspacing="0" cellpadding="2">
    <xsl:call-template name="visu-mail-zone-field">
      <xsl:with-param name="field-label">professional</xsl:with-param>
      <xsl:with-param name="field-value"><xsl:value-of select="email_work" /></xsl:with-param>
      <xsl:with-param name="field-url">mailto:<xsl:value-of select="email_work" /></xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="visu-mail-zone-field">
      <xsl:with-param name="field-label">personal</xsl:with-param>
      <xsl:with-param name="field-value"><xsl:value-of select="email_home" /></xsl:with-param>
      <xsl:with-param name="field-url">mailto:<xsl:value-of select="email_home" /></xsl:with-param>
    </xsl:call-template>
    
  </table>
</xsl:template>



<!-- ===============================
     work adresse
     =============================== -->
<xsl:template name="visu-work-address">
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <table border="0" cellspacing="0" cellpadding="2">
          <xsl:call-template name="visu-zone-field">
            <xsl:with-param name="field-label">street</xsl:with-param>
            <xsl:with-param name="field-value"><xsl:value-of select="addr_work" /></xsl:with-param>
          </xsl:call-template>
          
          <xsl:call-template name="visu-zone-field">
            <xsl:with-param name="field-label">postal-code</xsl:with-param>
            <xsl:with-param name="field-value"><xsl:value-of select="postal_code_work" /></xsl:with-param>
          </xsl:call-template>
          
          <xsl:call-template name="visu-zone-field">
            <xsl:with-param name="field-label">state</xsl:with-param>
            <xsl:with-param name="field-value"><xsl:value-of select="state_work" /></xsl:with-param>
          </xsl:call-template>
        </table>
      </td>            
      <td>
        <table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td><img src="{$image_uri}/transparent_fill.gif" width="20" height="20" /></td>
          </tr>
          
          <xsl:call-template name="visu-zone-field">
            <xsl:with-param name="field-label">city</xsl:with-param>
            <xsl:with-param name="field-value"><xsl:value-of select="city_work" /></xsl:with-param>
          </xsl:call-template>
          
          <xsl:call-template name="visu-zone-field">
            <xsl:with-param name="field-label">country</xsl:with-param>
            <xsl:with-param name="field-value"><xsl:value-of select="country_work" /></xsl:with-param>
          </xsl:call-template>
        </table>
      </td>
    </tr>
  </table>
</xsl:template>

<!-- ===============================
     home adresse
     =============================== -->
<xsl:template name="visu-home-address" >
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <table border="0" cellspacing="0" cellpadding="2">
          <xsl:call-template name="visu-zone-field">
            <xsl:with-param name="field-label">street</xsl:with-param>
            <xsl:with-param name="field-value"><xsl:value-of select="addr_home" /></xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="visu-zone-field">
            <xsl:with-param name="field-label">postal-code</xsl:with-param>
            <xsl:with-param name="field-value"><xsl:value-of select="postal_code_home" /></xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="visu-zone-field">
            <xsl:with-param name="field-label">state</xsl:with-param>
            <xsl:with-param name="field-value"><xsl:value-of select="state_home" /></xsl:with-param>
          </xsl:call-template>

        </table>
      </td>
      <td>
        <table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td><img src="{$image_uri}/transparent_fill.gif" width="20" height="20" /></td>
          </tr>
          <xsl:call-template name="visu-zone-field">
            <xsl:with-param name="field-label">city</xsl:with-param>
            <xsl:with-param name="field-value"><xsl:value-of select="city_home" /></xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="visu-zone-field">
            <xsl:with-param name="field-label">country</xsl:with-param>
            <xsl:with-param name="field-value"><xsl:value-of select="country_home" /></xsl:with-param>
          </xsl:call-template>
        </table>
      </td>
    </tr>
  </table>
</xsl:template>

<!-- ===============================
     comments
     =============================== -->
<xsl:template name="visu-comments">
  <xsl:value-of select="comment" disable-output-escaping="yes"/>
</xsl:template>

<!-- contact-visu translation -->
<xsl:template name="cvTranslation">
  <xsl:param name="type"/>

  <xsl:choose>
    <xsl:when test="$type='identity'"><xsl:value-of select="mioga:gettext('Identity')"/></xsl:when>
    <xsl:when test="$type='phone'"><xsl:value-of select="mioga:gettext('Telephone')"/></xsl:when>
    <xsl:when test="$type='professional-address'"><xsl:value-of select="mioga:gettext('Professional address')"/></xsl:when>
    <xsl:when test="$type='personal-address'"><xsl:value-of select="mioga:gettext('Personal address')"/></xsl:when>
    <xsl:when test="$type='mail'"><xsl:value-of select="mioga:gettext('Email')"/></xsl:when>
    <xsl:when test="$type='comments'"><xsl:value-of select="mioga:gettext('Comments')"/></xsl:when>
    <xsl:when test="$type='street'"><xsl:value-of select="mioga:gettext('Street')"/></xsl:when>
    <xsl:when test="$type='postal-code'"><xsl:value-of select="mioga:gettext('Zip code')"/></xsl:when>
    <xsl:when test="$type='state'"><xsl:value-of select="mioga:gettext('Status')"/></xsl:when>
    <xsl:when test="$type='city'"><xsl:value-of select="mioga:gettext('City')"/></xsl:when>
    <xsl:when test="$type='country'"><xsl:value-of select="mioga:gettext('Country')"/></xsl:when>
    <xsl:when test="$type='title'"><xsl:value-of select="mioga:gettext('Title')"/></xsl:when>
    <xsl:when test="$type='company'"><xsl:value-of select="mioga:gettext('Company')"/></xsl:when>
    <xsl:when test="$type='website'"><xsl:value-of select="mioga:gettext('Web site')"/></xsl:when>
    <xsl:when test="$type='work'"><xsl:value-of select="mioga:gettext('Work')"/></xsl:when>
    <xsl:when test="$type='mobile'"><xsl:value-of select="mioga:gettext('Mobile')"/></xsl:when>
    <xsl:when test="$type='fax'"><xsl:value-of select="mioga:gettext('Fax')"/></xsl:when>
    <xsl:when test="$type='home'"><xsl:value-of select="mioga:gettext('Domicile')"/></xsl:when>
    <xsl:when test="$type='professional'"><xsl:value-of select="mioga:gettext('Professional')"/></xsl:when>
    <xsl:when test="$type='personal'"><xsl:value-of select="mioga:gettext('Personal')"/></xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
