<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<xsl:output method="html"/>

<xsl:include href="base.xsl"/>
<xsl:include href="portal_report.xsl"/>
<xsl:include href="scriptaculous.xsl"/>
<xsl:include href="inline_message.xsl"/>

<!-- ===============
	root document
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="scriptaculous-js"/>
	<xsl:call-template name="theme-css"/>
</head>
	<xsl:apply-templates />
</html>
</xsl:template>
  
<!-- ================================
     Internationalization templates
     ================================ -->

<xsl:template name="MiogaTitleTranslation">
    <xsl:choose>
        <xsl:when test="name(.)='DisplayMain'"><xsl:value-of select="mioga:gettext('Access to Mioga2 portals')"/></xsl:when>
        <xsl:when test="name(.)='CreatePortal'"><xsl:value-of select="mioga:gettext('Create a new portal')"/></xsl:when>
    </xsl:choose>
</xsl:template>


<xsl:template name="MiogaReportTranslation">
    <xsl:param name="label"/>

    <xsl:choose>
        <xsl:when test="$label = 'select_portal'"></xsl:when>
    </xsl:choose>
</xsl:template>

<xsl:template name="MiogaFormTranslation">
    <xsl:param name="label"/>

    <xsl:value-of select="mioga:gettext('Creates a new portal')"/>
</xsl:template>


<xsl:template name="MiogaFormInputTranslation" match="CreatePortal" mode="arg-checker-field-name">
    <xsl:param name="name"/>
    <xsl:param name="label" select="$name"/>

    <xsl:choose>
        <xsl:when test="$label='name'"><xsl:value-of select="mioga:gettext('Title')"/></xsl:when>
        <xsl:when test="$label='filename'"><xsl:value-of select="mioga:gettext('Filename')"/></xsl:when>
        <xsl:when test="$label='description'"><xsl:value-of select="mioga:gettext('Description')"/></xsl:when>
    </xsl:choose>
</xsl:template>

<!-- =================================
     SuccessMessage templates
     ================================= -->

<xsl:template match="SuccessMessage">
  <xsl:call-template name="SuccessMessage">
    <xsl:with-param name="message">
        <xsl:choose>
            <xsl:when test="action='saved'"><xsl:value-of select="mioga:gettext('Portal saved successfully.')"/></xsl:when>
            <xsl:when test="action='created'"><xsl:value-of select="mioga:gettext('Portal created successfully')"/></xsl:when>
        </xsl:choose>
        
    </xsl:with-param>
    <xsl:with-param name="referer" select="referer"/>
  </xsl:call-template>    
</xsl:template>


<xsl:template name="no-portal">
  <xsl:value-of select="mioga:gettext('No portal.')"/>
</xsl:template>

<xsl:template name="MiogaActionTranslation">
    <xsl:param name="label"/>

    <xsl:choose>
        <xsl:when test="$label='new_portal'"><xsl:value-of select="mioga:gettext('Create a new portal')"/></xsl:when>
    </xsl:choose>
</xsl:template>
  


<!-- ================================
     Other templates
     ================================ -->



<!-- =================================
     DisplayMain templates
     ================================= -->

<xsl:template match="DisplayMain" mode="MiogaReportBody">

    <xsl:for-each select="PortalFiles/portal">

        <xsl:call-template name="MiogaReportPortalDesc"/>

    </xsl:for-each>

</xsl:template>


<xsl:template match="DisplayMain" mode="MiogaActionBody">
    <xsl:call-template name="MiogaActionItem">
        <xsl:with-param name="label">new_portal</xsl:with-param>
        <xsl:with-param name="href">CreatePortal</xsl:with-param>
    </xsl:call-template>    
</xsl:template>

<xsl:template match="DisplayMain">
    <body>
        <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">portal.html</xsl:with-param>
        </xsl:call-template>

		<!-- Status message -->
		<xsl:if test="@message != ''">
			<xsl:call-template name="InlineMessage">
				<xsl:with-param name="type" select="@type"/>
				<xsl:with-param name="message" select="@message"/>
			</xsl:call-template>
		</xsl:if>

        <div xsl:use-attribute-sets="app_body_attr">
            <xsl:call-template name="MiogaTitle"/>
            
            <xsl:if test="canCreatePortal">
                <xsl:call-template name="MiogaAction"/>
            </xsl:if>
            
            <xsl:call-template name="MiogaReport">
                <xsl:with-param name="label">select_portal</xsl:with-param>
                <xsl:with-param name="no-border">1</xsl:with-param>
                <xsl:with-param name="no-button">1</xsl:with-param>
                <xsl:with-param name="no-title">1</xsl:with-param>
            </xsl:call-template>

            <xsl:if test="count(PortalFiles/portal) = 0">
                <xsl:call-template name="MiogaMessage">
                    <xsl:with-param name="message">
                        <xsl:call-template name="no-portal"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:if>
        </div>
    </body>
</xsl:template>

<!-- =================================
     CreatePortal templates
     ================================= -->

<xsl:template match="CreatePortal" mode="MiogaFormBody">
    <xsl:call-template name="CSRF-input"/>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">filename</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">name</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormVertRule"/>

    <xsl:call-template name="MiogaFormInputTextArea">
        <xsl:with-param name="label">description</xsl:with-param>
        
        <xsl:with-param name="value">
            <xsl:choose>
                <xsl:when test="count(fields/description/*) != 0">
                    <xsl:copy-of select="fields/description/*"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="fields/description"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:with-param>

        <xsl:with-param name="copy" select="count(fields/description/*)"/>

    </xsl:call-template>

</xsl:template>

<xsl:template match="CreatePortal" mode="MiogaFormButton">
    <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">portal_act_create</xsl:with-param>
        <xsl:with-param name="referer">DisplayMain</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template match="CreatePortal">
    <body>
        <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">portal.html</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="MiogaTitle"/>
        
        <div xsl:use-attribute-sets="app_form_body_attr">
            <xsl:call-template name="arg_errors"/>
            
            <xsl:call-template name="MiogaForm">
                <xsl:with-param name="label">CreatePortal</xsl:with-param>
                <xsl:with-param name="action">CreatePortal</xsl:with-param>
                <xsl:with-param name="method">POST</xsl:with-param>
            </xsl:call-template>
            
        </div>
    </body>
</xsl:template>

</xsl:stylesheet>
