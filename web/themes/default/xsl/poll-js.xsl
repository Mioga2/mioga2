<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>
<xsl:import href="poll-parts.xsl"/>

<xsl:output method="xml" indent="no" omit-xml-declaration="yes" encoding="UTF-8" media-type="text/javascript"/>

<xsl:include href="scriptaculous.xsl"/>


<!--
===============
 Templates
===============
-->

<xsl:template match="/">
    <xsl:apply-templates/>
</xsl:template>

<!--
=======================================
 UpdateOptions JS
=======================================
-->
<xsl:template match="UpdateOptions">
    <xsl:choose>
        <xsl:when test="//errors">
            <xsl:variable name="options">
                <xsl:apply-templates select="Options"/>
            </xsl:variable>
Element.update("options-box", '<xsl:copy-of select="$options"/>');
$("disable-window").show();
$("options-box").show();

calsd = new CalendarPopup("div_calsd");
calsd.setReturnFunction("setValueSD");

caled = new CalendarPopup("div_caled");
caled.setReturnFunction("setValueED");
        </xsl:when>
        <xsl:otherwise>
$("disable-window").hide();
$("options-box").hide();
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<!--
=======================================
 DisplayOptions JS
=======================================
-->
<xsl:template match="DisplayOptions">
    <xsl:variable name="options">
        <xsl:apply-templates select="Options"/>
    </xsl:variable>
    
Element.update("options-box", '<xsl:copy-of select="$options"/>');

$("disable-window").show();
$("options-box").show();

calsd = new CalendarPopup("div_calsd");
calsd.setReturnFunction("setValueSD");

caled = new CalendarPopup("div_caled");
caled.setReturnFunction("setValueED");
</xsl:template>

<!--
=======================================
 DeleteQuestion
=======================================
-->
<xsl:template match="DeleteQuestion">
new Effect.Fade("q-<xsl:value-of select="DeletedQuestion"/>", { duration: 0.3 });
</xsl:template>

<!--
=======================================
 DeletePoll
=======================================
-->
<xsl:template match="DeletePoll">
new Effect.Fade("poll-<xsl:value-of select="DeletedPoll"/>", { duration: 0.3 });
</xsl:template>

</xsl:stylesheet>
