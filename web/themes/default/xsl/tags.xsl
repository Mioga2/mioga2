<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ext="urn:ext" xmlns:mioga="urn:mioga">

<xsl:include href="jquery.xsl"/>

<xsl:template name="tags-js">
	<xsl:call-template name="jquery-js"/>
	<xsl:call-template name="jquery-form-js"/>
	<xsl:call-template name="jquery-form-serializeobject"/>
	<script src="{$theme_uri}/javascript/jquery.tags.js" type="text/javascript"></script>
</xsl:template>

<xsl:template name="tags-css">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/tags.css" media="screen" />
</xsl:template>

</xsl:stylesheet>
