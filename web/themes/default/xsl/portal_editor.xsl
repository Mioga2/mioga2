<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="portal.xsl"/>

<xsl:output method="html"/>

<!-- ===============
	root document
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/portal.css" media="screen" />
	<script type="text/javascript" src="{$theme_uri}/javascript/portal_editor.js"></script>
	<xsl:if test="/Portal/Mioga2Portal/global/css_file">
		<link rel="stylesheet" type="text/css" href="{$private_dav_uri}/{/Portal/Mioga2Portal/global/css_file}" media="screen" />
	</xsl:if>

	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="theme-css"/>
</head>
<body id="portal_body" class="portal">
	<xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- =================================
	 Mioga2Portal menu
	 ================================= -->

<xsl:template match="Mioga2Portal" mode="MiogaActionBody">
    
	<xsl:call-template name="MiogaActionItem">
		<xsl:with-param name="label">SavePortal</xsl:with-param>
		<xsl:with-param name="href">EditPortal?save_portal=1</xsl:with-param>
	</xsl:call-template>
	<xsl:call-template name="MiogaActionItem">
		<xsl:with-param name="label">ViewPortal</xsl:with-param>
		<xsl:with-param name="href">EditPortal?view_cache=1</xsl:with-param>
	</xsl:call-template>
	<xsl:call-template name="MiogaActionItem">
		<xsl:with-param name="label">GotoReferer</xsl:with-param>
		<xsl:with-param name="href" select="$referer"/>
	</xsl:call-template>
</xsl:template>

<!-- =================================
	 Mioga2Portal
	 ================================= -->

<xsl:template match="Mioga2Portal">
<body class="portal_editor">
	<xsl:call-template name="DisplayAppTitle">
		<xsl:with-param name="help">portal.html</xsl:with-param>
	</xsl:call-template>

	<div id="menuEditor">
		<xsl:call-template name="MiogaAction"/>
		<div class="vspace">
			<xsl:choose>
				<xsl:when test="$select_id != '' and (count(//*[@id=$select_id]/@type) = 0 or //*[@id=$select_id]/@type != 'Base/MiogletSelector')">
					<xsl:for-each select="//*[@id=$select_id]">
						<xsl:apply-templates select="." mode="editor">
							<xsl:with-param name="node" select="."/>
						</xsl:apply-templates>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="DefaultForm"/>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<div>
			<form action="EditPortal" method="GET">
				<xsl:call-template name="AddRootForm"/>
			</form>
			<table class="editorBox" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<xsl:for-each select="portal/*">
                        <xsl:variable name="tagname" select="local-name(.)"/>
                        <xsl:variable name="id" select="./@id"/>
                    
                        <xsl:variable name="class">
                            <xsl:choose>
                                <xsl:when test="$select_id = $id and $tagname = 'vbox'">selectedVBox</xsl:when>
                                <xsl:when test="$select_id = $id and $tagname = 'hbox'">selectedHBox</xsl:when>
                                <xsl:when test="$tagname = 'vbox'">VBox</xsl:when>
                                <xsl:when test="$tagname = 'hbox'">HBox</xsl:when>
                                <xsl:when test="$select_id = $id">selectedBox</xsl:when>
                                <xsl:otherwise>stdBox</xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        
                        <table cellspacing="0" cellpadding="0" onclick="javascript:selectId('{$id}', event);" class="{$class}">
                            <xsl:if test="./@bgcolor">
                                <xsl:attribute name="bgcolor"><xsl:value-of select="./@bgcolor"/></xsl:attribute>
                            </xsl:if>
                            <xsl:call-template name="boxify">
                                <xsl:with-param name="tagname" select="$tagname"/>
                                <xsl:with-param name="id" select="$id"/>
                            </xsl:call-template>
                            
                                <tr class="miogletBox">
                                    <td colspan="3" class="miogletBox">
                                        <table class="miogletBox">
                                            <tr>
                                                <td>
                                                    <xsl:apply-templates select="."/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            
                        </table>
					</xsl:for-each>
				</td>
			</tr>
			</table>
		</div>
	</div>
</body>
</xsl:template>

<!-- =================================
	 DefaultForm : Display an info box at form position.
	 ================================= -->
<xsl:template name="DefaultForm">
	<div id="none" style="margin: 2em;">
		<xsl:call-template name="MiogaBox">
			<xsl:with-param name="title"><xsl:value-of select="mioga:gettext('Portal editor')"/></xsl:with-param>
			<xsl:with-param name="center">1</xsl:with-param>				
			<xsl:with-param name="wrap">1</xsl:with-param>				
			<xsl:with-param name="body">
				<p style="padding: 1em">
					<xsl:value-of select="mioga:gettext('Please select component to edit')"/>
				</p>
			</xsl:with-param>
		</xsl:call-template>
	</div>
</xsl:template>


<!-- =================================
	 EditorForms : Create the forms for component edition.
	 ================================= -->

<xsl:template match="*" mode="MiogaFormBody">
	<input type="hidden" name="mioglet_id" value="{@id}"/>

	<xsl:choose>
		<xsl:when test="local-name(.) = 'mioglet'">
			<xsl:variable name="mioglet_ref" select="key('mioglet', @ref)"/>
			<xsl:variable name="node" select="."/>
   
			<xsl:for-each select="$mioglet_ref/*">
				<xsl:apply-templates select="." mode="editor">
					<xsl:with-param name="node" select="$node"/>
				</xsl:apply-templates>
			</xsl:for-each>
			
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates select="." mode="editorform"/>
		</xsl:otherwise>
	</xsl:choose>

	<xsl:variable name="ancestorTagName">
		<xsl:call-template name="getAncestorTagName"/>
	</xsl:variable>
	
	<xsl:if test="$ancestorTagName != 'portal'">
		<xsl:call-template name="MiogaFormVertRule"/>

		<xsl:call-template name="EditorBasicBoxEntry"/>
	</xsl:if>

</xsl:template>

<xsl:template match="*" mode="MiogaFormButton">
	<xsl:call-template name="ok-form-button">
		<xsl:with-param name="name">change_mioglet_param</xsl:with-param>
	</xsl:call-template>
</xsl:template>

<!-- =================================
	 EditorForm
	 ================================= -->

<xsl:template name="EditorForm">
	<div id="{@id}" class="editorForm">
		<xsl:call-template name="MiogaForm">
			<xsl:with-param name="label">portal_editor</xsl:with-param>
			<xsl:with-param name="action" select="EditPortal"/>
			<xsl:with-param name="method">POST</xsl:with-param>
		</xsl:call-template>
	</div>
</xsl:template>


<!-- =================================
	 boxify : Add contents in a titled box
	 ================================= -->

<xsl:template name="getAncestorTagName">
	<xsl:choose>
		<xsl:when test="local-name(..) = 'content'">
			<xsl:variable name="contentid" select="../@id"/>
			<xsl:value-of select="local-name(//*[@ref=$contentid][1]/..)"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="local-name(..)"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="getAncestorParam">
	<xsl:param name="param_name"/>
	<xsl:param name="node" select="."/>

	<xsl:choose>
		<xsl:when test="local-name(..) = 'content'">
			<xsl:variable name="contentid" select="../@id"/>
			<xsl:value-of select="//*[@ref=$contentid][1]/attribute::*[local-name(.)=$param_name]"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="../attribute::*[local-name(.)=$param_name]"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template name="getBoxPosition">
	<xsl:choose>
		<xsl:when test="local-name(..) = 'content'">
			<xsl:variable name="contentid" select="../@id"/>
			<xsl:value-of select="count(//*[@ref=$contentid][1]/preceding-sibling::*) + 1"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="count(./preceding-sibling::*) + 1"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template name="getLastBox">
	<xsl:choose>
		<xsl:when test="local-name(..) = 'content'">
			<xsl:variable name="contentid" select="../@id"/>
			<xsl:value-of select="count(//*[@ref=$contentid][1]/../*)"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="count(../*)"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- ==============
	boxify
	================ -->

<xsl:template name="boxify">
	<xsl:param name="tagname" select="local-name(.)"/>
	<xsl:param name="id" select="./@id"/>

		<tr>
			<td class="tagName">
					<xsl:value-of select="$tagname"/>
			</td>
			<td width="100%">
				&#160;
			</td>
			<td>
				<xsl:choose>
					<xsl:when test="$select_id = $id">
						<xsl:variable name="position">
							<xsl:call-template name="getBoxPosition">
								<xsl:with-param name="id" select="$id"/>
							</xsl:call-template>
						</xsl:variable>
						
						<xsl:variable name="last">
							<xsl:call-template name="getLastBox">
								<xsl:with-param name="id" select="$id"/>
							</xsl:call-template>
						</xsl:variable>

						<xsl:variable name="ancestorTagName">
							<xsl:call-template name="getAncestorTagName">
								<xsl:with-param name="id" select="$id"/>
							</xsl:call-template>
						</xsl:variable>

						<xsl:variable name="addBefore">
							<xsl:call-template name="MiogaFormInputTranslation">
								<xsl:with-param name="label">add_before</xsl:with-param>
							</xsl:call-template>
						</xsl:variable>

						<xsl:variable name="addAfter">
							<xsl:call-template name="MiogaFormInputTranslation">
								<xsl:with-param name="label">add_after</xsl:with-param>
							</xsl:call-template>
						</xsl:variable>

						<xsl:variable name="moveLeft">
							<xsl:call-template name="MiogaFormInputTranslation">
								<xsl:with-param name="label">move_left</xsl:with-param>
							</xsl:call-template>
						</xsl:variable>

						<xsl:variable name="moveRight">
							<xsl:call-template name="MiogaFormInputTranslation">
								<xsl:with-param name="label">move_right</xsl:with-param>
							</xsl:call-template>
						</xsl:variable>

						<xsl:variable name="moveDown">
							<xsl:call-template name="MiogaFormInputTranslation">
								<xsl:with-param name="label">move_down</xsl:with-param>
							</xsl:call-template>
						</xsl:variable>

						<xsl:variable name="moveUp">
							<xsl:call-template name="MiogaFormInputTranslation">
								<xsl:with-param name="label">move_up</xsl:with-param>
							</xsl:call-template>
						</xsl:variable>

						<xsl:variable name="deleteBox">
							<xsl:call-template name="MiogaFormInputTranslation">
								<xsl:with-param name="label">delete_box</xsl:with-param>
							</xsl:call-template>
						</xsl:variable>


						<xsl:if test="$ancestorTagName != 'portal'">
					
							<div class="iconBox">
								<table>
									<tr>
										<xsl:if test="$ancestorTagName = 'vbox'">
											<td>
												<img src="{$image_uri}/16x16/actions/arrow-up-double.png" 
													onclick="javascript:addBeforeId('{$id}', event);"
													alt="{$addBefore}" title="{$addBefore}"/>
											</td>
											<td>
												<img src="{$image_uri}/16x16/actions/arrow-down-double.png" 
													onclick="javascript:addAfterId('{$id}', event);"
													alt="{$addAfter}" title="{$addAfter}"/>
											</td>
										</xsl:if>
										
										<xsl:if test="$ancestorTagName = 'hbox'">
											<td>
												<img src="{$image_uri}/16x16/actions/arrow-left-double.png" 
													onclick="javascript:addBeforeId('{$id}', event);"
													alt="{$addBefore}" title="{$addBefore}"/>
											</td>
											<td>
												<img src="{$image_uri}/16x16/actions/arrow-right-double.png" 
													onclick="javascript:addAfterId('{$id}', event);"
													alt="{$addAfter}" title="{$addAfter}"/>
											</td>
										</xsl:if>
										
										<xsl:if test="$position > 1 and $ancestorTagName = 'vbox'">
											<td>
												<img src="{$image_uri}/16x16/actions/arrow-down.png" 
													onclick="javascript:downId('{$id}', event);"
													alt="{$moveUp}" title="{$moveUp}"/>
											</td>
										</xsl:if>
										<xsl:if test="$position != $last and $ancestorTagName = 'vbox'">
											<td>
												<img src="{$image_uri}/16x16/actions/arrow-up.png" 
													onclick="javascript:upId('{$id}', event);"
													alt="{$moveDown}" title="{$moveDown}"/>
											</td>
										</xsl:if>
										<xsl:if test="$position > 1 and $ancestorTagName = 'hbox'">
											<td>
												<img src="{$image_uri}/16x16/actions/arrow-left.png" 
													onclick="javascript:downId('{$id}', event);"
													alt="{$moveLeft}" title="{$moveLeft}"/>
											</td>
										</xsl:if>
										<xsl:if test="$position != $last and $ancestorTagName = 'hbox'">
											<td>
												<img src="{$image_uri}/16x16/actions/arrow-right.png" 
													onclick="javascript:upId('{$id}', event);"
													alt="{$moveRight}" title="{$moveRight}"/>
											</td>
										</xsl:if>
										<xsl:if test="$last > 1">
											<td>
												<img src="{$image_uri}/16x16/actions/dialog-close.png" 
													onclick="javascript:deleteId('{$id}', event);"
													alt="{$deleteBox}" title="{$deleteBox}"/>
											</td>
										</xsl:if>
									</tr>
								</table>
							</div>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						&#160; 
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>

</xsl:template>

<!-- =================================
	 hbox : Handle horizontal box declaration
	 ================================= -->

<xsl:template match="hbox|vbox" mode="editor">
	<xsl:call-template name="EditorForm"/>
</xsl:template>


<!-- =================================
	 box : Handle box declaration
	 ================================= -->

<xsl:template match="box" mode="editor">
	<xsl:choose>
		<xsl:when test="@ref">
			<xsl:variable name="content_ref" select="key('content', @ref)"/>
			
			<xsl:choose>
				<xsl:when test="$content_ref">
					<xsl:for-each select="$content_ref/*">
						<xsl:apply-templates select="." mode="editor">
							<xsl:with-param name="node" select="."/>
						</xsl:apply-templates>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
          <xsl:value-of select="mioga:gettext('Invalid reference: %s', string(@ref))"/>
				</xsl:otherwise>
			</xsl:choose>
			
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates select="*" mode="editor">
				<xsl:with-param name="node" select="."/>
			</xsl:apply-templates>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- =================================
	 html : Handle inline html
	 ================================= -->

<xsl:template match="html" mode="editor">
	<xsl:call-template name="EditorForm"/>
</xsl:template>



<!-- =================================
	 TitledBox : Handle titled box
	 ================================= -->

<xsl:template match="TitledBox" mode="editor">
	<xsl:call-template name="EditorForm"/>
</xsl:template>


<!-- =================================
	 mioglet : Handle mioglets
	 ================================= -->

<xsl:template match="mioglet" mode="editor">
	<xsl:call-template name="EditorForm"/>
</xsl:template>


<!-- =================================
	 box : Handle box declaration
	 ================================= -->

<xsl:template match="box">  
	<xsl:if test="./@width">
		<xsl:attribute name="width"><xsl:value-of select="./@width"/></xsl:attribute>
	</xsl:if>	

	<xsl:variable name="tagname">
		<xsl:choose>
			<xsl:when test="@ref">
				<xsl:variable name="content_ref" select="key('content', @ref)"/>
				<xsl:value-of select="local-name($content_ref[1]/child::*[1])"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="local-name(child::*[1])"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	 <xsl:variable name="tagid">
		<xsl:choose>
			<xsl:when test="@ref">
				<xsl:variable name="content_ref" select="key('content', @ref)"/>
				<xsl:value-of select="$content_ref[1]/child::*[1]/@id"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="child::*[1]/@id"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>


    <xsl:variable name="class">
        <xsl:choose>
            <xsl:when test="$select_id = $tagid and $tagname = 'vbox'">selectedVBox</xsl:when>
            <xsl:when test="$select_id = $tagid and $tagname = 'hbox'">selectedHBox</xsl:when>
            <xsl:when test="$tagname = 'vbox'">VBox</xsl:when>
            <xsl:when test="$tagname = 'hbox'">HBox</xsl:when>
            <xsl:when test="$select_id = $tagid">selectedBox</xsl:when>
            <xsl:otherwise>stdBox</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <table cellspacing="0" cellpadding="0" onclick="javascript:selectId('{$tagid}', event);" class="{$class}">
        <xsl:if test="./@bgcolor">
            <xsl:attribute name="bgcolor"><xsl:value-of select="./@bgcolor"/></xsl:attribute>
        </xsl:if>
        <xsl:call-template name="boxify">
            <xsl:with-param name="tagname" select="$tagname"/>
            <xsl:with-param name="id" select="$tagid"/>
        </xsl:call-template>
        
            <tr class="miogletBox">
                <td colspan="3" class="miogletBox">
                    <table class="miogletBox">
                        <tr>
                            <td>
                                <xsl:apply-imports/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        
    </table>

</xsl:template>

<!-- =================================
	 Forms for basic objects .
	 ================================= -->

<xsl:template match="none" mode="editorform">
	<xsl:call-template name="MiogaFormDisplayText">
		<xsl:with-param name="label">text</xsl:with-param>
		<xsl:with-param name="value">
      <xsl:value-of select="mioga:gettext('Please select component to edit')"/>
		</xsl:with-param>		
	</xsl:call-template>
</xsl:template>


<xsl:template match="vbox|hbox" mode="editorform">
	<xsl:variable name="ancestorTagName">
		<xsl:call-template name="getAncestorTagName"/>
	</xsl:variable>

	<xsl:choose>
		<xsl:when test="$ancestorTagName='portal'">
			<xsl:call-template name="EditorRootBoxEntry"/> 
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="EditorBasicContainerEntry"/>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>


<xsl:template match="html" mode="editorform">
	<xsl:param name="node" select="."/>

	<xsl:call-template name="EditorBasicEntry"/>

	<xsl:call-template name="MiogaFormVertRule"/>

	<xsl:call-template name="MiogaFormInputTextArea">
		<xsl:with-param name="label">content</xsl:with-param>
		<xsl:with-param name="value">
			<xsl:choose>
				<xsl:when test="count(content/*) != 0">
					<xsl:apply-templates select="content/*"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="content"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:with-param>
		<xsl:with-param name="copy" select="count(content/*)"/>
		<xsl:with-param name="height">5</xsl:with-param>
		<xsl:with-param name="width">50</xsl:with-param>
   </xsl:call-template>

</xsl:template>

<xsl:template match="html/content//* | html/content//@* | description//* | description//@*">
	<xsl:copy>
	  <xsl:apply-templates select="@* | node()"/>
	</xsl:copy>
</xsl:template>


<!-- =================================
	 Editor*Entry : Misc Portal Editor form component
	 ================================= -->

<xsl:template name="EditorBasicEntry">
	<xsl:param name="node" select="."/>

	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">title</xsl:with-param>
		<xsl:with-param name="value" select="$node/title"/>
	</xsl:call-template>

	<xsl:call-template name="EditorBoxedEntry">
		<xsl:with-param name="node" select="$node"/>
	</xsl:call-template>

	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">css_id</xsl:with-param>
		<xsl:with-param name="value" select="$node/css_id"/>
	</xsl:call-template>

</xsl:template>

<xsl:template name="EditorBasicContainerEntry">
	<xsl:param name="node" select="."/>

	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">param_spacing</xsl:with-param>
		<xsl:with-param name="value" select="$node/@spacing"/>
	</xsl:call-template>
 
	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">param_padding</xsl:with-param>
		<xsl:with-param name="value" select="$node/@padding"/>
	</xsl:call-template>

	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">param_width</xsl:with-param>
		<xsl:with-param name="value">
			<xsl:call-template name="getAncestorParam">
				<xsl:with-param name="param_name">width</xsl:with-param>
				<xsl:with-param name="node" select="$node"/>
			</xsl:call-template>
		</xsl:with-param>
	</xsl:call-template>
	
</xsl:template>

<xsl:template name="EditorRootBoxEntry">
	<xsl:param name="node" select="."/>
		
	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">param_spacing</xsl:with-param>
		<xsl:with-param name="value" select="$node/@spacing"/>
	</xsl:call-template>
 
	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">param_padding</xsl:with-param>
		<xsl:with-param name="value" select="$node/@padding"/>
	</xsl:call-template>
 
	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">param_bgcolor</xsl:with-param>
		<xsl:with-param name="value" select="//global/background/@color"/>
	</xsl:call-template>
	
	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">param_bgimage</xsl:with-param>
		<xsl:with-param name="value" select="//global/background/@image"/>
	</xsl:call-template>	
	
	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">css_file</xsl:with-param>
		<xsl:with-param name="value" select="//global/css_file"/>
	</xsl:call-template>	
	
	<xsl:call-template name="MiogaFormVertRule"/>

	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">param_name</xsl:with-param>
		<xsl:with-param name="value" select="//Mioga2Portal/@name"/>
	</xsl:call-template>
	
	<xsl:call-template name="MiogaFormInputTextArea">
		<xsl:with-param name="label">description</xsl:with-param>
		<xsl:with-param name="value">
			<xsl:choose>
				<xsl:when test="count(//global/description/*) != 0"><xsl:apply-templates select="//global/description/*"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="//global/description"/></xsl:otherwise>
			</xsl:choose>
		</xsl:with-param>
		<xsl:with-param name="width">40</xsl:with-param>
		<xsl:with-param name="height">5</xsl:with-param>
		<xsl:with-param name="copy" select="count(//global/description/*)"/>
	</xsl:call-template>	
	
</xsl:template>

<xsl:template name="MiogaFormInputSelectTranslation">
	<xsl:param name="label"/>
	<xsl:param name="value"/>

	<xsl:value-of select=".."/>
	
</xsl:template>
	
<xsl:template name="EditorBasicBoxEntry">
	<xsl:param name="node" select="."/>

	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">param_bgcolor</xsl:with-param>
		<xsl:with-param name="value">
			<xsl:call-template name="getAncestorParam">
				<xsl:with-param name="param_name">bgcolor</xsl:with-param>
				<xsl:with-param name="node" select="$node"/>
			</xsl:call-template>
		</xsl:with-param>
	</xsl:call-template>
	
	<xsl:variable name="border_value">
		<xsl:call-template name="getAncestorParam">
			<xsl:with-param name="param_name">border</xsl:with-param>
			<xsl:with-param name="node" select="$node"/>
		</xsl:call-template>
	</xsl:variable>

	<xsl:call-template name="MiogaFormInputMisc">
		<xsl:with-param name="label">param_border</xsl:with-param>
		<xsl:with-param name="value" select="$border_value"/> 
		<xsl:with-param name="content">
			<table>
				<tr>
					<td>
						<xsl:call-template name="MiogaFormInputTranslation">
							<xsl:with-param name="label">right</xsl:with-param>
						</xsl:call-template> : 
					</td>
					<td>
						<input type="checkbox" name="param_border_right">
						<xsl:if test="contains($border_value, 'right')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
						</input>
					</td>
					<td>&#160;</td>
					<td>
						<xsl:call-template name="MiogaFormInputTranslation">
							<xsl:with-param name="label">left</xsl:with-param>
						</xsl:call-template> : 
					</td>
					<td>
						<input type="checkbox" name="param_border_left">
						<xsl:if test="contains($border_value, 'left')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
						</input>
					</td>
				</tr>
				<tr>

					<td>
						<xsl:call-template name="MiogaFormInputTranslation">
							<xsl:with-param name="label">top</xsl:with-param>
						</xsl:call-template> : 
					</td>
					<td>
						<input type="checkbox" name="param_border_top">
						<xsl:if test="contains($border_value, 'top')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
						</input>
					</td>

					<td>&#160;</td>

					<td>
						<xsl:call-template name="MiogaFormInputTranslation">
							<xsl:with-param name="label">bottom</xsl:with-param>
						</xsl:call-template> : 
					</td>
					<td>
						<input type="checkbox" name="param_border_bottom">
						<xsl:if test="contains($border_value, 'bottom')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
						</input>
					</td>
				</tr>
			</table>
		</xsl:with-param>
	</xsl:call-template>

</xsl:template>


<xsl:template name="EditorBoxedEntry">
	<xsl:param name="node" select="."/>

	<xsl:call-template name="MiogaFormInputCheckbox">
		<xsl:with-param name="label">boxed</xsl:with-param>
		<xsl:with-param name="value" select="count($node/boxed)"/>
	</xsl:call-template>

</xsl:template>


<xsl:template name="EditorURLEntry">
	<xsl:param name="node" select="."/>

	<xsl:variable name="href">
		<xsl:choose>
			<xsl:when test="starts-with($node/href, $private_dav_uri)">
				<xsl:value-of select="substring-after($node/href, $private_dav_uri)"/>
			</xsl:when>
			<xsl:when test="starts-with($node/href, $public_dav_uri)">
				<xsl:value-of select="substring-after($node/href, $public_dav_uri)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$node/href"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:call-template name="MiogaFormInputText">
		<xsl:with-param name="label">href</xsl:with-param>
		<xsl:with-param name="value" select="$href"/>
	</xsl:call-template>

</xsl:template>


<xsl:template name="EditorModeEntry">
	<xsl:param name="node" select="."/>

	<xsl:call-template name="MiogaFormInputMisc">
		<xsl:with-param name="label">mode</xsl:with-param>
		<xsl:with-param name="value" select="$node/mode"/>
		<xsl:with-param name="content">
			<select name="mode">
				<option value="user">
					<xsl:if test="$node/mode = 'user'">
						<xsl:attribute name="selected">selected</xsl:attribute>
					</xsl:if>
					<xsl:call-template name="MiogaFormInputTranslation">
						<xsl:with-param name="label">user</xsl:with-param>
					</xsl:call-template>
				</option>
				<option value="group">
					<xsl:if test="$node/mode = 'group'">
						<xsl:attribute name="selected">selected</xsl:attribute>
					</xsl:if>
					<xsl:call-template name="MiogaFormInputTranslation">
						<xsl:with-param name="label">group</xsl:with-param>
					</xsl:call-template>
				</option>
			</select>
		</xsl:with-param>
	</xsl:call-template>
</xsl:template>


<!-- =================================
	 Forms translation.
	 ================================= -->

<xsl:template name="MiogaFormTranslation">
	<xsl:param name="label"/>

	<xsl:variable name="name">
		<xsl:choose>
			<xsl:when test="local-name(.) = 'mioglet'">
				<xsl:variable name="mioglet_ref" select="key('mioglet', @ref)"/>
				<xsl:for-each select="$mioglet_ref/*">
					<xsl:value-of select="local-name(.)"/>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="local-name(.)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
		
	<xsl:choose>
		<xsl:when test="$label='portal_editor'"><xsl:value-of select="mioga:gettext('Edit a %s component', string($name))"/></xsl:when>
	</xsl:choose>

</xsl:template>


<xsl:template name="MiogaFormInputTranslation">
	<xsl:param name="label"/>
	
	<xsl:choose>
		<xsl:when test="$label='test'"><xsl:value-of select="mioga:gettext('Portal editor')"/></xsl:when>
		<xsl:when test="$label='title'"><xsl:value-of select="mioga:gettext('Title')"/></xsl:when>
		<xsl:when test="$label='css_id'"><xsl:value-of select="mioga:gettext('CSS identifier')"/></xsl:when>
		<xsl:when test="$label='mode'"><xsl:value-of select="mioga:gettext('Mode')"/></xsl:when>
		<xsl:when test="$label='boxed'"><xsl:value-of select="mioga:gettext('Box')"/></xsl:when>
		<xsl:when test="$label='user'"><xsl:value-of select="mioga:gettext('User')"/></xsl:when>
		<xsl:when test="$label='group'"><xsl:value-of select="mioga:gettext('Group')"/></xsl:when>
		<xsl:when test="$label='param_padding'"><xsl:value-of select="mioga:gettext('Padding')"/></xsl:when>
		<xsl:when test="$label='param_spacing'"><xsl:value-of select="mioga:gettext('Spacing')"/></xsl:when>
		<xsl:when test="$label='param_bgcolor'"><xsl:value-of select="mioga:gettext('Background color')"/></xsl:when>
		<xsl:when test="$label='param_bgimage'"><xsl:value-of select="mioga:gettext('Background image')"/></xsl:when>
		<xsl:when test="$label='css_file'"><xsl:value-of select="mioga:gettext('CSS file')"/></xsl:when>
		<xsl:when test="$label='param_border'"><xsl:value-of select="mioga:gettext('Borders')"/></xsl:when>
		<xsl:when test="$label='param_width'"><xsl:value-of select="mioga:gettext('Width')"/></xsl:when>
		<xsl:when test="$label='add_after'"><xsl:value-of select="mioga:gettext('Insert after')"/></xsl:when>
		<xsl:when test="$label='add_before'"><xsl:value-of select="mioga:gettext('Insert before')"/></xsl:when>
		<xsl:when test="$label='move_up'"><xsl:value-of select="mioga:gettext('Move to top')"/></xsl:when>
		<xsl:when test="$label='move_down'"><xsl:value-of select="mioga:gettext('Move to bottom')"/></xsl:when>
		<xsl:when test="$label='move_left'"><xsl:value-of select="mioga:gettext('Move to left')"/></xsl:when>
		<xsl:when test="$label='move_right'"><xsl:value-of select="mioga:gettext('Move to right')"/></xsl:when>
		<xsl:when test="$label='delete_box'"><xsl:value-of select="mioga:gettext('Delete')"/></xsl:when>
		<xsl:when test="$label='param_href'"><xsl:value-of select="mioga:gettext('Address')"/></xsl:when>
		<xsl:when test="$label='left'"><xsl:value-of select="mioga:gettext('left')"/></xsl:when>
		<xsl:when test="$label='right'"><xsl:value-of select="mioga:gettext('right')"/></xsl:when>
		<xsl:when test="$label='top'"><xsl:value-of select="mioga:gettext('above')"/></xsl:when>
		<xsl:when test="$label='bottom'"><xsl:value-of select="mioga:gettext('below')"/></xsl:when>
		<xsl:when test="$label='content'"><xsl:value-of select="mioga:gettext('Content')"/></xsl:when>
		<xsl:when test="$label='param_name'"><xsl:value-of select="mioga:gettext('Title')"/></xsl:when>
		<xsl:when test="$label='description'"><xsl:value-of select="mioga:gettext('Description')"/></xsl:when>
		<xsl:when test="$label='link_path'"><xsl:value-of select="mioga:gettext('Links path')"/></xsl:when>
		<xsl:when test="$label='folder'"><xsl:value-of select="mioga:gettext('Path')"/></xsl:when>
		<xsl:when test="$label='show_files'"><xsl:value-of select="mioga:gettext('List files')"/></xsl:when>
		<xsl:when test="$label='use_filebrowser'"><xsl:value-of select="mioga:gettext('Use filebrowser to follow links')"/></xsl:when>
		<xsl:when test="$label='feed'"><xsl:value-of select="mioga:gettext('RSS feed')"/></xsl:when>
		<xsl:when test="$label='size'"><xsl:value-of select="mioga:gettext('Size')"/></xsl:when>
		<xsl:when test="$label='wcode'"><xsl:value-of select="mioga:gettext('Weather.com code of city')"/></xsl:when>
		<xsl:when test="$label='sec'"><xsl:value-of select="mioga:gettext('number of seconds')"/></xsl:when>
		<xsl:when test="$label='show_title'"><xsl:value-of select="mioga:gettext('Display title')"/></xsl:when>
		<xsl:when test="$label='show_date'"><xsl:value-of select="mioga:gettext('Display date')"/></xsl:when>
		<xsl:when test="$label='show_author'"><xsl:value-of select="mioga:gettext('Display author')"/></xsl:when>
		<xsl:when test="$label='show_content'"><xsl:value-of select="mioga:gettext('Display content')"/></xsl:when>
        <xsl:when test="$label='delay'"><xsl:value-of select="mioga:gettext('Number of days')"/></xsl:when>
		<xsl:when test="$label='chroot'"><xsl:value-of select="mioga:gettext('Root directory (chroot)')"/></xsl:when>
		<xsl:when test="$label='category'"><xsl:value-of select="mioga:gettext('Category')"/></xsl:when>
		<xsl:when test="$label='number'"><xsl:value-of select="mioga:gettext('Number')"/></xsl:when>
		<xsl:when test="$label='src'"><xsl:value-of select="mioga:gettext('Source')"/></xsl:when>
		<xsl:when test="$label='name'"><xsl:value-of select="mioga:gettext('Name')"/></xsl:when>
		<xsl:when test="$label='limit'"><xsl:value-of select="mioga:gettext('Limit')"/></xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$label"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="MiogaActionTranslation">
	<xsl:param name="label"/>

	<xsl:choose>
		<xsl:when test="$label='SavePortal'"><xsl:value-of select="mioga:gettext('Save portal')"/></xsl:when>
		<xsl:when test="$label='GotoReferer'"><xsl:value-of select="mioga:gettext('Back to previous page')"/></xsl:when>
		<xsl:when test="$label='ViewPortal'"><xsl:value-of select="mioga:gettext('View current portal')"/></xsl:when>
	</xsl:choose>
</xsl:template>
  
<xsl:template name="AddRootForm">
	
	<table class="rootForm" cellspacing="2" cellpadding="2">
		<tr>
			<td><img src="{$image_uri}/16x16/actions/arrow-right.png"/></td>
			<td><xsl:value-of select="mioga:gettext('Add a')"/> </td>
			<td>
				<select name="root_container">
					<option>vbox</option>
					<option>hbox</option>
				</select>
			</td>
			<td>
				<xsl:value-of select="mioga:gettext('to root:')"/>
			</td>
			<td>
				<xsl:call-template name="add-form-button">
					<xsl:with-param name="name">insert_root</xsl:with-param>
				</xsl:call-template>
			</td>
		</tr>
	</table>
	
</xsl:template>

</xsl:stylesheet>
