<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- ==========
	dojo-js
	to include dojo in HTML HEAD element
	========== -->

<xsl:template name="dojo-js">
	<script type="text/javascript" src="{$jslib_uri}/dojotoolkit/dojo/dojo.js">
		<xsl:attribute name="djConfig">parseOnLoad:true, locale: '<xsl:value-of select="substring (//miogacontext/group/lang, 0, 3)"/>'</xsl:attribute>
	</script>
	<!-- <script type="text/javascript" src="{$jslib_uri}/dojotoolkit/dojo/mioga-dojo.js" djConfig="parseOnLoad:true"></script> -->
</xsl:template>

<xsl:template name="dojo-js-debug">
	<script type="text/javascript" src="{$jslib_uri}/dojotoolkit/dojo/dojo.js.uncompressed.js">
		<xsl:attribute name="djConfig">parseOnLoad:true, locale: '<xsl:value-of select="substring (//miogacontext/group/lang, 0, 3)"/>'</xsl:attribute>
	</script>
	<!-- <script type="text/javascript" src="{$jslib_uri}/dojotoolkit/dojo/mioga-dojo.js.uncompressed.js" djConfig="parseOnLoad:true"></script> -->
</xsl:template>

<xsl:template name="mioga_dojo-js">
	<script type="text/javascript" src="{$jslib_uri}/dojotoolkit/mioga/layers/base.js"></script>  

	<xsl:call-template name="mioga_dojo_widgets-css"/>
</xsl:template>

<xsl:template name="mioga_dojo-js-dev">
	<script type="text/javascript" src="{$jslib_uri}/dojotoolkit/mioga/layers/base.dev.js" djConfig="parseOnLoad:true"></script>

	<xsl:call-template name="mioga_dojo_widgets-css"/>
</xsl:template>

<xsl:template name="mioga_dojo_widgets-css">
	<!-- The Mioga Widgets stylesheets -->
	<link rel="stylesheet" type="text/css" href="{$jslib_uri}/dojotoolkit/mioga/css/tabbeddialog.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$jslib_uri}/dojotoolkit/mioga/css/tabitem.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$jslib_uri}/dojotoolkit/mioga/css/minilist.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$jslib_uri}/dojotoolkit/mioga/css/userattributes.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$jslib_uri}/dojotoolkit/mioga/css/groupattributes.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$jslib_uri}/dojotoolkit/mioga/css/teamattributes.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{$jslib_uri}/dojotoolkit/mioga/css/widgets.css" media="screen" />
</xsl:template>

<xsl:template name="dojo-css">
   	<link rel="stylesheet" type="text/css" href="{$jslib_uri}/dojotoolkit/dijit/themes/mioga/mioga.css" media="screen" />
   	<link rel="stylesheet" type="text/css" href="{$jslib_uri}/dojotoolkit/dojo/resources/dojo.css" media="screen" />    
</xsl:template>

<xsl:template name="dojo-grid-css">
   	<link rel="stylesheet" type="text/css" href="{$jslib_uri}/dojotoolkit/dojox/grid/resources/Grid.css" media="screen" />
   	<link rel="stylesheet" type="text/css" href="{$jslib_uri}/dojotoolkit/dijit/themes/mioga/miogaGrid.css" media="screen" />
</xsl:template>

</xsl:stylesheet>
