<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<!--  <xsl:output method="html"/> -->

<!--
     MiogaForm : Create a new form
     
     - label: identify the title of the form, used by MiogaFormTranslation
     - no-title: (0|1) Display or not the title (default:0)
     - no-button: (0|1) Display or not the submit button (default:0)
     - action: target script for the form submission
     - method: form submission method (default: POST)
     - enctype: enctype for form submission
     - param: parameters form template with mode MiogaFormBody and MiogaFormButton

     -->

<xsl:template name="MiogaForm">
  <xsl:param name="label"/>
  <xsl:param name="no-title">0</xsl:param>
  <xsl:param name="no-button">0</xsl:param>
  <xsl:param name="highlight">0</xsl:param>
  
  <xsl:param name="action"/>
  <xsl:param name="method">post</xsl:param>
  <xsl:param name="enctype"/>

  <xsl:param name="param"></xsl:param>
  
  <form method="{$method}">
    <!-- Form enctype -->
    <xsl:if test="$enctype">
      <xsl:attribute name="enctype"><xsl:value-of select="$enctype"/></xsl:attribute>
    </xsl:if>
    <!-- Form action -->
    <xsl:if test="$action">
      <xsl:attribute name="action"><xsl:value-of select="$action"/></xsl:attribute>
    </xsl:if>

    <xsl:call-template name="MiogaBox">
      <xsl:with-param name="no-title" select="$no-title"/>
      <xsl:with-param name="highlight" select="$highlight"/>
      <xsl:with-param name="wrap">1</xsl:with-param>
      <xsl:with-param name="center">1</xsl:with-param>
      <xsl:with-param name="title">
          <xsl:if test="$no-title != 1">
              <xsl:call-template name="MiogaFormTranslation">
                  <xsl:with-param name="label" select="$label"/>
              </xsl:call-template>
          </xsl:if>
      </xsl:with-param>
      
      <xsl:with-param name="body">
        <table xsl:use-attribute-sets="mioga-empty-table">
          <tr valign="top">
            <td>
              <table xsl:use-attribute-sets="mioga-empty-table">
                <tr valign="top">
                  <td>
                    <xsl:call-template name="MiogaFormOpenZone1"/>
                    <xsl:call-template name="MiogaFormOpenZone2"/>
                    
                    <xsl:apply-templates select="."  mode="MiogaFormBody">
                      <xsl:with-param name="param" select="$param"/>
                    </xsl:apply-templates>
                    
                    <xsl:call-template name="MiogaFormCloseZone2"/>
                    <xsl:call-template name="MiogaFormCloseZone1"/>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          
          
          <xsl:if test="$no-button != 1">
            <tr>
              <td>&#160;</td>
            </tr>
            
            <tr>
              <td align="center">
                <xsl:apply-templates select="."  mode="MiogaFormButton">
                  <xsl:with-param name="param" select="$param"/>
                </xsl:apply-templates>
              </td>
            </tr>
          </xsl:if>
          
        </table>
       </xsl:with-param>
    </xsl:call-template>
    
  </form>
</xsl:template>

<xsl:template name="MiogaFormMini">
  <xsl:param name="label"/>
  <xsl:param name="button"></xsl:param>
  <xsl:param name="body"></xsl:param>

  <xsl:param name="action"/>
  <xsl:param name="method">post</xsl:param>
  <xsl:param name="enctype"></xsl:param>

  <xsl:call-template name="MiogaBox">
    <xsl:with-param name="no-title">1</xsl:with-param>
    <xsl:with-param name="body">
      <form action="{$action}" method="{$method}" enctype="{$enctype}">
        <table>
          <tr>
            <td>
              <table>
                <!-- body -->
                <xsl:choose>
                  <xsl:when test="$body">
                    <xsl:copy-of select="$body"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:apply-templates select="." mode="MiogaFormMiniBody"/>
                  </xsl:otherwise>
                </xsl:choose>
              </table>
            </td>
            <td valign="bottom">
              <!-- button -->
              <xsl:choose>
                <xsl:when test="$button">
                  <xsl:copy-of select="$button"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:apply-templates select="." mode="MiogaFormMiniButton"/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </table>
      </form>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>


<!--
     MiogaFormTitle - Display form titles
     
     - label : identify the title that will be displayed
     -->

<xsl:template name="MiogaFormTitle">
  <xsl:param name="label"/>
  
  <tr>
      <td colspan="2">
          <h2 style="margin-bottom: 0.2em;">
              <xsl:call-template name="MiogaFormInputTranslation">
                  <xsl:with-param name="label" select="$label"/>
              </xsl:call-template>
          </h2>
      </td>
  </tr>
</xsl:template>

<!-- ==================================
     == Horizontal And Vertical Rule ==
     ================================== -->

<!-- Display a main horizontal rule (e.g as large as the form is) -->
<xsl:template name="MiogaFormHorizRule">
  <xsl:call-template name="MiogaFormCloseZone2"/>
  <xsl:call-template name="MiogaFormCloseZone1"/>
  
  <xsl:call-template name="MiogaFormInternalHorizRule"/>
  
  <xsl:call-template name="MiogaFormOpenZone1"/>
  <xsl:call-template name="MiogaFormOpenZone2"/>
</xsl:template>

<!-- Display an horizontal rule as a classic form component: it should be called
     in a main vertical zone -->
<xsl:template name="MiogaFormTinyHorizRule">
  <xsl:call-template name="MiogaFormCloseZone2"/>
  <xsl:call-template name="MiogaFormInternalHorizRule"/>
  <xsl:call-template name="MiogaFormOpenZone2"/>
</xsl:template>

<!-- Display a main vertical rule, as higher as the form is -->
<xsl:template name="MiogaFormVertRule">
  <xsl:call-template name="MiogaFormCloseZone2"/>
  <xsl:call-template name="MiogaFormCloseZone1"/>
  
  <xsl:call-template name="MiogaFormInternalVertRule"/>

  <xsl:call-template name="MiogaFormOpenZone1"/>
  <xsl:call-template name="MiogaFormOpenZone2"/>
</xsl:template>

<!-- Display a vertical rule in a main horizontal zone -->
<xsl:template name="MiogaFormTinyVertRule">
  <xsl:call-template name="MiogaFormCloseZone2"/>
  
  <xsl:call-template name="MiogaFormInternalVertRule"/>
  
  <xsl:call-template name="MiogaFormOpenZone2"/>
</xsl:template>

<!-- Implementation for Vertical Rule -->
<xsl:template name="MiogaFormInternalVertRule">
  <xsl:text disable-output-escaping="yes">
    <![CDATA[
    </td>
    ]]>
    </xsl:text>    
    <td>&#160;</td>
    <td valign="middle" bgcolor="{$mioga-rules-color}"><img src="{$image_uri}/transparent_fill.gif" border="0" width="1" height="1" alt=""/></td>
    <td>&#160;</td>
    <xsl:text disable-output-escaping="yes">
    <![CDATA[
    <td>
      ]]>
    </xsl:text>    
</xsl:template>    

<!-- Implementation of horizontal rule -->
<xsl:template name="MiogaFormInternalHorizRule">
  <xsl:text disable-output-escaping="yes">
    <![CDATA[
      </td></tr><tr><td>
      ]]>
  </xsl:text>
      <hr bgcolor="{$mioga-rules-color}" color="{$mioga-rules-color}" width="90%" size="1"/>
  <xsl:text disable-output-escaping="yes">
      <![CDATA[
      </td></tr><tr><td>
      ]]>
  </xsl:text>
</xsl:template>



<!-- ==================================
     Horizontal And Vertical Separator 

     Same templates as *Rule, but a Separator is transparent
     ================================== -->

<!-- Display a main horizontal separator -->
<xsl:template name="MiogaFormHorizSep">
  <xsl:call-template name="MiogaFormCloseZone2"/>
  <xsl:call-template name="MiogaFormCloseZone1"/>
  
  <xsl:call-template name="MiogaFormInternalHorizSep"/>
  
  <xsl:call-template name="MiogaFormOpenZone1"/>
  <xsl:call-template name="MiogaFormOpenZone2"/>
</xsl:template>

<!-- Display an horizontal separator as a classic component, in a main vertical zone -->
<xsl:template name="MiogaFormTinyHorizSep">
  <xsl:call-template name="MiogaFormCloseZone2"/>
  <xsl:call-template name="MiogaFormInternalHorizSep"/>
  <xsl:call-template name="MiogaFormOpenZone2"/>
</xsl:template>

<!-- Display a main vertical separator -->
<xsl:template name="MiogaFormVertSep">
  <xsl:call-template name="MiogaFormCloseZone2"/>
  <xsl:call-template name="MiogaFormCloseZone1"/>
  
  <xsl:call-template name="MiogaFormInternalVertSep"/>

  <xsl:call-template name="MiogaFormOpenZone1"/>
  <xsl:call-template name="MiogaFormOpenZone2"/>
</xsl:template>

<!-- Display a small vertical rule in a main horizontal zone -->
<xsl:template name="MiogaFormTinyVertSep">
  <xsl:call-template name="MiogaFormCloseZone2"/>
  
  <xsl:call-template name="MiogaFormInternalVertSep"/>
  
  <xsl:call-template name="MiogaFormOpenZone2"/>
</xsl:template>

<!-- Vertical rule implementation -->
<xsl:template name="MiogaFormInternalVertSep">
    <xsl:text disable-output-escaping="yes">
    <![CDATA[
    </td>
    ]]>
    </xsl:text>    
    <td>&#160;</td>
    <td valign="middle"><img src="{$image_uri}/transparent_fill.gif" border="0" width="1" height="1" alt=""/></td>
    <td>&#160;</td>
    <xsl:text disable-output-escaping="yes">
    <![CDATA[
    <td>
      ]]>
    </xsl:text>    
</xsl:template>    

<!-- Horizontal rule implemenation -->
<xsl:template name="MiogaFormInternalHorizSep">
  <xsl:text disable-output-escaping="yes">
      <![CDATA[
      </td></tr><tr><td>
      ]]>
  </xsl:text>
  <img src="{$image_uri}/transparent_fill.gif" border="0" width="1" height="1" alt=""/>
  <xsl:text disable-output-escaping="yes">
      <![CDATA[
      </td></tr><tr><td>
      ]]>
  </xsl:text>
</xsl:template>

<!-- ===============
     == Form Zone ==
     =============== -->

<xsl:template name="MiogaFormOpenZone1">
  <xsl:text disable-output-escaping="yes"><![CDATA[<table border="0" cellspacing="5" cellpadding="0"><tr valign="top"><td>]]></xsl:text>
</xsl:template>

<xsl:template name="MiogaFormOpenZone2">
  <xsl:text disable-output-escaping="yes"><![CDATA[<table border="0" cellspacing="2" cellpadding="2">]]></xsl:text>
</xsl:template>
    
<xsl:template name="MiogaFormCloseZone1">
  <xsl:text disable-output-escaping="yes"><![CDATA[</td></tr></table>]]></xsl:text>
</xsl:template>

<xsl:template name="MiogaFormCloseZone2">
  <xsl:text disable-output-escaping="yes"><![CDATA[</table>]]></xsl:text>
</xsl:template>

<!-- ================
     Form Input
     
     - label : name of the text input, used for translation
     - is-required : indicates if the field is required
     - has-error : indicates if the field is erroneous
     - tabindex : indicates input identifier for tabs navigation
     - value: default value
     - disposition: 0 -> inline; 1 -> label on top of input
     
     ================ -->

<xsl:template name="MiogaFormInputInternal">
  <xsl:param name="label"/>
  <xsl:param name="body"/>
  <xsl:param name="is-required"/>
  <xsl:param name="has-error"/>
  <xsl:param name="disposition"/>

  <tr>
      <xsl:choose>
          <xsl:when test="$has-error >= 1">
              <td xsl:use-attribute-sets="mioga-form-input-error-label-attr">
                  <xsl:call-template name="MiogaFormInputTranslation">
                      <xsl:with-param name="label" select="$label"/>
                  </xsl:call-template>&#160;:&#160;
              </td>

          </xsl:when>
          
          <xsl:otherwise>
              
              <td xsl:use-attribute-sets="mioga-form-input-label-attr">
                  <xsl:call-template name="MiogaFormInputTranslation">
                      <xsl:with-param name="label" select="$label"/>
                  </xsl:call-template>&#160;:&#160;
              </td>
              
          </xsl:otherwise>
      </xsl:choose>

    <xsl:if test="$disposition=1">
      <xsl:text disable-output-escaping="yes"><![CDATA[</tr><tr>]]></xsl:text>
    </xsl:if>
    <td>
      <xsl:copy-of select="$body"/>
    </td>
  </tr>
</xsl:template>


<!-- Simple text input 
     - size: width of the text box
     - maxlength: max number character you can fill in
     -->
<xsl:template name="MiogaFormInputText">
  <xsl:param name="label"/>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="size">20</xsl:param>
  <xsl:param name="maxlength">256</xsl:param>
  <xsl:param name="disposition">0</xsl:param>
  <xsl:param name="disabled">0</xsl:param>

  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required" select="$is-required"/>
    <xsl:with-param name="has-error" select="$has-error"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <input type="text" name="{$label}" maxlength="{$maxlength}" size="{$size}" value="{$value}">
        <xsl:if test="$tabindex">
          <xsl:attribute name="tabindex">
            <xsl:value-of select="$tabindex"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="$disabled = 1">
            <xsl:attribute name="disabled">disabled</xsl:attribute>
            <xsl:attribute name="class">disabled</xsl:attribute>
        </xsl:if>
      </input>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Display a fixed value (no input) -->
<xsl:template name="MiogaFormDisplayText">
  <xsl:param name="label"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>

  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required">0</xsl:with-param>
    <xsl:with-param name="has-error">0</xsl:with-param>
    <xsl:with-param name="body">
      <xsl:copy-of select="$value"/>
    </xsl:with-param>
  </xsl:call-template>

</xsl:template>

<!-- Password input, with stars
     - size: width of the text box
     - maxlength: max number character you can fill in
     -->
<xsl:template name="MiogaFormInputPassword">
  <xsl:param name="label"/>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="size">20</xsl:param>
  <xsl:param name="maxlength">255</xsl:param>
  <xsl:param name="disposition">0</xsl:param>

  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required" select="$is-required"/>
    <xsl:with-param name="has-error" select="$has-error"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <input type="password" name="{$label}" maxlength="{$maxlength}" size="{$size}" value="{$value}">
        <xsl:if test="$tabindex">
          <xsl:attribute name="tabindex">
            <xsl:value-of select="$tabindex"/>
          </xsl:attribute>
        </xsl:if>
      </input>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Large text input (textarea)
     - width : box width
     - length : box height
     -->
<xsl:template name="MiogaFormInputTextArea">
  <xsl:param name="label"/>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="width">30</xsl:param>
  <xsl:param name="height">7</xsl:param>
  <xsl:param name="disposition">1</xsl:param>
  <xsl:param name="copy">0</xsl:param>


  <xsl:variable name="body">
      <textarea name="{$label}" rows="{$height}" cols="{$width}">
          <xsl:if test="$tabindex">
              <xsl:attribute name="tabindex">
                  <xsl:value-of select="$tabindex"/>
              </xsl:attribute>
          </xsl:if>
          
          <xsl:choose>
              <xsl:when test="$copy != 0">
                  <xsl:copy-of select="$value"/>                  
              </xsl:when>
              <xsl:otherwise>
                  <xsl:value-of disable-output-escaping="yes" select="$value"/>                  
              </xsl:otherwise>
          </xsl:choose>
      </textarea>
  </xsl:variable>

  <xsl:choose>
      <xsl:when test="$disposition=0">
          <xsl:call-template name="MiogaFormInputInternal">
              <xsl:with-param name="label" select="$label"/>
              <xsl:with-param name="is-required" select="$is-required"/>
              <xsl:with-param name="has-error" select="$has-error"/>
              <xsl:with-param name="disposition" select="$disposition"/>
              <xsl:with-param name="body" select="$body"/>
          </xsl:call-template>          
      </xsl:when>

      <xsl:otherwise>
          <tr>
              <td colspan="2">
                  <table xsl:use-attribute-sets="mioga-empty-table">
                      <tr>
                          <td>
                              <p xsl:use-attribute-sets="mioga-form-input-label-attr">
                                  <xsl:call-template name="MiogaFormInputTranslation">
                                      <xsl:with-param name="label" select="$label"/>
                                  </xsl:call-template>&#160;:&#160;
                              </p>
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <xsl:copy-of select="$body"/>
                          </td>
                      </tr>
                  </table>
              </td>
          </tr>
      </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- Hidden value
     -->
<xsl:template name="MiogaFormInputHidden">
  <xsl:param name="label"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>

  <input type="hidden" name="{$label}" value="{$value}"/>
</xsl:template>



<!-- Display a single checkbox with a label 
     - value : (0|1) indicates if the box is checked
     -->
<xsl:template name="MiogaFormInputCheckbox">
  <xsl:param name="label"/>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="disposition"></xsl:param>

  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required" select="$is-required"/>
    <xsl:with-param name="has-error" select="$has-error"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <input type="checkbox" name="{$label}">
        <xsl:if test="$tabindex">
          <xsl:attribute name="tabindex">
            <xsl:value-of select="$tabindex"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="$value = 1 or $value='on'">
          <xsl:attribute name="checked">checked</xsl:attribute>
        </xsl:if>
      </input>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Display a selection form between several values
     - choices: nodeset with all values, also used for label translation
     - multiple: indicates if multiple values can be selected
     - type: if multiple = 0, you can choose between radio button (0) or classic select (1)
     - align: for radio or multiple select, indicates a vertical display (1) or horizontal (0)
     -->
<xsl:template name="MiogaFormInputSelect">
  <xsl:param name="label"/>
  <xsl:param name="choices"/>
  <xsl:param name="empty-choice">0</xsl:param>
  <xsl:param name="empty-choice-label">0</xsl:param>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="disposition">0</xsl:param>

  <!-- select type -->
  <xsl:param name="multiple">0</xsl:param>
  <xsl:param name="type">1</xsl:param>
  <xsl:param name="align">0</xsl:param>

  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required" select="$is-required"/>
    <xsl:with-param name="has-error" select="$has-error"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <xsl:choose>
        <xsl:when test="$multiple=1">
          <!-- checkbox input -->
          <xsl:call-template name="MiogaFormInputRadioCheckboxInternal">
            <xsl:with-param name="label" select="$label"/>
            <xsl:with-param name="tabindex" select="$tabindex"/>
            <xsl:with-param name="value" select="$value"/>
            <xsl:with-param name="choices" select="$choices"/>
            <xsl:with-param name="align" select="$align"/>
            <xsl:with-param name="type">1</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$type=0">
              <!-- radio input -->
              <xsl:call-template name="MiogaFormInputRadioCheckboxInternal">
                <xsl:with-param name="label" select="$label"/>
                <xsl:with-param name="tabindex" select="$tabindex"/>
                <xsl:with-param name="value" select="$value"/>
                <xsl:with-param name="choices" select="$choices"/>
                <xsl:with-param name="align" select="$align"/>
                <xsl:with-param name="type">0</xsl:with-param>
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
              <!-- select -->
              <xsl:call-template name="MiogaFormInputSelectInternal">
                <xsl:with-param name="label" select="$label"/>
                <xsl:with-param name="tabindex" select="$tabindex"/>
                <xsl:with-param name="value" select="$value"/>
                <xsl:with-param name="choices" select="$choices"/>
                <xsl:with-param name="empty-choice" select="$empty-choice"/>
                <xsl:with-param name="empty-choice-label" select="$empty-choice-label"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- internal select implemantation -->
<xsl:template name="MiogaFormInputSelectInternal">
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="value" />
  <xsl:param name="choices"/>
  <xsl:param name="label"/>
  <xsl:param name="empty-choice"/>
  <xsl:param name="empty-choice-label"/>

  <select name="{$label}">
    <xsl:if test="$tabindex">
      <xsl:attribute name="tabindex">
        <xsl:value-of select="$tabindex"/>
      </xsl:attribute>
    </xsl:if>

    <xsl:if test="$empty-choice!=0">
        <option value="">
            <xsl:call-template name="MiogaFormInputSelectTranslation">
                <xsl:with-param name="label" select="$label"/>
                <xsl:with-param name="value" select="$empty-choice-label"/>
            </xsl:call-template>
        </option>
    </xsl:if>

    <xsl:for-each select="$choices">
      <option value="{.}">
        <xsl:if test=". = $value">
          <xsl:attribute name="selected">selected</xsl:attribute>
        </xsl:if>
        <xsl:call-template name="MiogaFormInputSelectTranslation">
            <xsl:with-param name="label" select="$label"/>
            <xsl:with-param name="value" select="."/>
        </xsl:call-template>
      </option>
    </xsl:for-each>
  </select>
</xsl:template>

<!-- internal checkbox or radio button selection implementation -->
<xsl:template name="MiogaFormInputRadioCheckboxInternal">
  <xsl:param name="label"/>
  <xsl:param name="value"/>
  <xsl:param name="tabindex"/>
  <xsl:param name="align"/>
  <xsl:param name="type"/>
  <xsl:param name="choices"/>
  
  <table border="0" cellpadding="0" cellspacing="2">
      <xsl:if test="$align=1">
          <xsl:text disable-output-escaping="yes">
              <![CDATA[<tr>]]>
          </xsl:text>
      </xsl:if>
      <xsl:for-each select="$choices">
          <xsl:if test="$align=0">
              <xsl:text disable-output-escaping="yes">
                  <![CDATA[<tr>]]>
              </xsl:text>
          </xsl:if>

          <td align="right">
              <input name="{$label}" value="{.}">
                  <xsl:attribute name="tabindex">
                      <xsl:value-of select="$tabindex - 1 + position()"/>
                  </xsl:attribute>
                  <xsl:choose>
                      <xsl:when test="$type = '0'">
                          <xsl:attribute name="type">radio</xsl:attribute>
                      </xsl:when>
                      <xsl:otherwise>
                          <xsl:attribute name="type">checkbox</xsl:attribute>
                      </xsl:otherwise>
                  </xsl:choose>
                  <xsl:if test=". = $value">
                      <xsl:attribute name="checked">checked</xsl:attribute>
                  </xsl:if>
              </input> 
          </td>
          <td xsl:use-attribute-sets="mioga-form-input-value-attr">
              <xsl:call-template name="MiogaFormInputSelectTranslation">
                  <xsl:with-param name="label" select="$label"/>
                  <xsl:with-param name="value" select="."/>
              </xsl:call-template>
          </td>
          <xsl:if test="$align=0">
              <xsl:text disable-output-escaping="yes">
                  <![CDATA[</tr>]]>
              </xsl:text>
          </xsl:if>
          <xsl:if test="$align=1">
               <td>&#160;&#160;</td>
          </xsl:if>
     </xsl:for-each>
      <xsl:if test="$align=1">
          <xsl:text disable-output-escaping="yes">
              <![CDATA[</tr>]]>
          </xsl:text>
      </xsl:if>
  </table>
</xsl:template>

<!-- Used to add complex Select item
     - body : must contains one or several MiogaFormInput*
   
     -->
<xsl:template name="MiogaFormInputSelectComplexItem">
    <xsl:param name="body"/>

    <table xsl:use-attribute-sets="mioga-empty-table">
        <tr>
            <td>
                <xsl:copy-of select="$body"/>
            </td>
        </tr>
    </table>
</xsl:template>

<!-- Date Input
     - first-year: first year in the year select
     - nb-year: year count to display in the year select
     - year-value, month-value, day-value: default value for year, month and day
     - year-name, month-name, day-name: default value for year, month and day
     -->
<xsl:template name="MiogaFormInputDate">
  <xsl:param name="label"/>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="disposition">0</xsl:param>

  <xsl:param name="can-be-empty">0</xsl:param>

  <xsl:param name="first-year" select="$mioga_context/time/year - 1"/>
  <xsl:param name="nb-year">50</xsl:param>

  <!-- default values -->

  <xsl:param name="year-value" select="fields/*[name(.)=concat($label, '_year')]"/>
  <xsl:param name="month-value" select="fields/*[name(.)=concat($label, '_month')]"/>
  <xsl:param name="day-value" select="fields/*[name(.)=concat($label, '_day')]"/>
  
  <!-- default input names -->
  <xsl:param name="year-name"><xsl:value-of select="$label"/>_year</xsl:param>
  <xsl:param name="month-name"><xsl:value-of select="$label"/>_month</xsl:param>
  <xsl:param name="day-name"><xsl:value-of select="$label"/>_day</xsl:param>

  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required" select="$is-required"/>
    <xsl:with-param name="has-error" select="$has-error"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <xsl:call-template name="MiogaFormInputDateInternal">
        <xsl:with-param name="tabindex" select="$tabindex"/>
        <xsl:with-param name="day-value" select="$day-value"/>
        <xsl:with-param name="month-value" select="$month-value -1"/>
        <xsl:with-param name="year-value" select="$year-value"/>
        <xsl:with-param name="day-name" select="$day-name"/>
        <xsl:with-param name="month-name" select="$month-name"/>
        <xsl:with-param name="year-name" select="$year-name"/>
        <xsl:with-param name="nb-year" select="$nb-year"/>
        <xsl:with-param name="first-year" select="$first-year"/>
        <xsl:with-param name="can-be-empty" select="$can-be-empty"/>
      </xsl:call-template>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Flexible Date Input (date with flexible constraint)
     - first-year: first year in the year select
     - nb-year: year count to display in the year select
     - year-value, month-value, day-value: default value for year, month and day
     - year-name, month-name, day-name: default value for year, month and day
     -->
<xsl:template name="MiogaFormInputFlexibleDate">
  <xsl:param name="label"/>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="disposition">0</xsl:param>

  <xsl:param name="can-be-empty">0</xsl:param>

  <xsl:param name="first-year" select="$mioga_context/time/year - 1"/>
  <xsl:param name="nb-year">50</xsl:param>

  <!-- default values -->

  <xsl:param name="year-value" select="fields/*[name(.)=concat($label, '_year')]"/>
  <xsl:param name="month-value" select="fields/*[name(.)=concat($label, '_month')]"/>
  <xsl:param name="day-value" select="fields/*[name(.)=concat($label, '_day')]"/>
  <xsl:param name="constraint-value" select="fields/*[name(.)=concat($label, '_constraint')]"/>
  
  <!-- default input names -->
  <xsl:param name="year-name"><xsl:value-of select="$label"/>_year</xsl:param>
  <xsl:param name="month-name"><xsl:value-of select="$label"/>_month</xsl:param>
  <xsl:param name="day-name"><xsl:value-of select="$label"/>_day</xsl:param>
  <xsl:param name="constraint-name"><xsl:value-of select="$label"/>_constraint</xsl:param>

  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required" select="$is-required"/>
    <xsl:with-param name="has-error" select="$has-error"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <xsl:call-template name="MiogaFormInputDateInternal">
          <xsl:with-param name="tabindex" select="$tabindex"/>
          <xsl:with-param name="day-value" select="$day-value"/>
          <xsl:with-param name="month-value" select="$month-value -1"/>
          <xsl:with-param name="year-value" select="$year-value"/>
          <xsl:with-param name="day-name" select="$day-name"/>
          <xsl:with-param name="month-name" select="$month-name"/>
          <xsl:with-param name="year-name" select="$year-name"/>
          <xsl:with-param name="nb-year" select="$nb-year"/>
          <xsl:with-param name="first-year" select="$first-year"/>
          <xsl:with-param name="can-be-empty" select="$can-be-empty"/>
      </xsl:call-template>&#160;<xsl:call-template name="MiogaFormInputFlexibleContraintInternal">
          <xsl:with-param name="constraint-name" select="$constraint-name"/>
          <xsl:with-param name="constraint-value" select="$constraint-value"/>
      </xsl:call-template>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>


<!-- internal implementation for flexible constraint -->
<xsl:template name="MiogaFormInputFlexibleContraintInternal">
    <xsl:param name="constraint-name"/>
    <xsl:param name="constraint-value"/>
    
    <select name="{$constraint-name}">
        <xsl:call-template name="MiogaFormInputFlexibleContraintInternalAddConstraint">
            <xsl:with-param name="selected_value" select="$constraint-value"/>
            <xsl:with-param name="value">0</xsl:with-param>
            <xsl:with-param name="name">none</xsl:with-param>
        </xsl:call-template>
        
        <xsl:call-template name="MiogaFormInputFlexibleContraintInternalAddConstraint">
            <xsl:with-param name="selected_value" select="$constraint-value"/>
            <xsl:with-param name="value">1</xsl:with-param>
            <xsl:with-param name="name">fixe</xsl:with-param>
        </xsl:call-template>
        
        <xsl:call-template name="MiogaFormInputFlexibleContraintInternalAddConstraint">
            <xsl:with-param name="selected_value" select="$constraint-value"/>
            <xsl:with-param name="value">2</xsl:with-param>
            <xsl:with-param name="name">asap</xsl:with-param>
        </xsl:call-template>
        
        <xsl:call-template name="MiogaFormInputFlexibleContraintInternalAddConstraint">
            <xsl:with-param name="selected_value" select="$constraint-value"/>
            <xsl:with-param name="value">3</xsl:with-param>
            <xsl:with-param name="name">alap</xsl:with-param>
        </xsl:call-template>
    </select>

</xsl:template>

<!-- internal implementation for flexible constraint -->
<xsl:template name="MiogaFormInputFlexibleContraintInternalAddConstraint">
    <xsl:param name="selected_value"/>
    <xsl:param name="value"/>
    <xsl:param name="name"/>
    
    <option value="{$value}">
        <xsl:if test="$selected_value = $value">
            <xsl:attribute name="selected">selected</xsl:attribute>
        </xsl:if>
        
        <xsl:call-template name="flexibleConstraintTranslation">
            <xsl:with-param name="name" select="$name"/>
        </xsl:call-template>
    </option>
    
</xsl:template>


<!-- internal implementation for date -->
<xsl:template name="MiogaFormInputDateInternal">
  <xsl:param name="tabindex"/>
  <xsl:param name="first-year"/>
  <xsl:param name="nb-year"/>
  <xsl:param name="can-be-empty">0</xsl:param>

  <!-- default values -->
  <xsl:param name="year-value"/>
  <xsl:param name="month-value"/>
  <xsl:param name="day-value"/>

  <!-- default input names -->
  <xsl:param name="year-name"/>
  <xsl:param name="month-name"/>
  <xsl:param name="day-name"/>

  <select name="{$day-name}">
    <xsl:if test="$tabindex">
      <xsl:attribute name="tabindex">
        <xsl:value-of select="$tabindex"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:call-template name="generateNumberSelect">
        <xsl:with-param name="start">1</xsl:with-param>
        <xsl:with-param name="end">31</xsl:with-param>
        <xsl:with-param name="selected" select="$day-value"/>
        <xsl:with-param name="can-be-empty" select="$can-be-empty"/>
    </xsl:call-template>
  </select>
  
  <select name="{$month-name}">
    <xsl:if test="$tabindex">
      <xsl:attribute name="tabindex">
        <xsl:value-of select="$tabindex+1"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:call-template name="generateMonthSelect">
      <xsl:with-param name="selected" select="$month-value"/>
      <xsl:with-param name="can-be-empty" select="$can-be-empty"/>
    </xsl:call-template>
  </select>
<!--
  <select name="{$year-name}">
    <xsl:if test="$tabindex">
      <xsl:attribute name="tabindex">
        <xsl:value-of select="$tabindex+2"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:call-template name="generateNumberSelect">
      <xsl:with-param name="start" select="$first-year"/>
      <xsl:with-param name="end" select="$first-year+$nb-year"/>
      <xsl:with-param name="selected" select="$year-value"/>
      <xsl:with-param name="can-be-empty" select="$can-be-empty"/>
   </xsl:call-template>
  </select>
-->
	<input type="text" name="{$year-name}" value="{$year-value}" maxlength="4" size="4" />
	<xsl:variable name="cal_name">cal<xsl:value-of select="$year-name" /></xsl:variable>
	<script language="JavaScript" id="calendar_script">
		var <xsl:value-of select="$cal_name" /> = new CalendarPopup('div_<xsl:value-of select="$cal_name" />');
		<xsl:value-of select="$cal_name" />.setReturnFunction("setValues<xsl:value-of select="$year-name" />");
		function setValues<xsl:value-of select="$year-name" />(y,m,d) {
			document.forms[0].<xsl:value-of select="$year-name" />.value=y;
			document.forms[0].<xsl:value-of select="$month-name" />.selectedIndex=m-1 + <xsl:value-of select="$can-be-empty" />;
			document.forms[0].<xsl:value-of select="$day-name" />.selectedIndex=d-1 + <xsl:value-of select="$can-be-empty" />;
			document.getElementById('calendar_link').onclick = function(evt) { 
			 <xsl:value-of select="$cal_name" />.showCalendar('calendar_link', (document.forms[0].<xsl:value-of select="$month-name" />.selectedIndex + 1)+"-"+(document.forms[0].<xsl:value-of select="$day-name" />.selectedIndex + 1)+"-"+document.forms[0].<xsl:value-of select="$year-name" />.value); return false; 
			};
		}
	</script>
	 <a href="#" title="Cliquer pour choisir une date" name="calendar_link" id="calendar_link">
	 	<xsl:attribute name="onclick"><xsl:value-of select="$cal_name" />.showCalendar('calendar_link', '<xsl:value-of select="$month-value + 1"/>-<xsl:value-of select="$day-value"/>-<xsl:value-of select="$year-value"/>'); return false;</xsl:attribute>
	 	<img src="{$image_uri}/calendar.gif" border="0" width="16" height="16" />
	 </a>
	 <div id="div_{$cal_name}" class="calendar_popup" style="position:absolute;visibility:hidden;"></div>
</xsl:template>

<!-- Generate all options tags for a select with a number list 
     - start : first number
     - end: last number
     - step: step between each option
     - selected: default selected number
     -->
<xsl:template name="generateNumberSelect">
  <xsl:param name="start"/>
  <xsl:param name="end"/>
  <xsl:param name="step">1</xsl:param>
  <xsl:param name="selected">0</xsl:param>
  <xsl:param name="can-be-empty">0</xsl:param>

  <xsl:if test="$start &lt; $end+1">
      <xsl:if test="$can-be-empty=1">
          <option value="">
              <xsl:if test="$selected = ''">
                  <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
          </option>
      </xsl:if>
      <option>
          <xsl:if test="format-number($selected,00) = format-number($start,00)">
              <xsl:attribute name="selected">selected</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="format-number($start,'00')"/>
      </option>
      <xsl:call-template name="generateNumberSelect">
          <xsl:with-param name="start" select="$start+$step"/>
          <xsl:with-param name="end" select="$end"/>
          <xsl:with-param name="step" select="$step"/>
          <xsl:with-param name="selected" select="$selected"/>
      </xsl:call-template>
  </xsl:if>
</xsl:template>

<!-- Generate all options tags for a month select 
     - month : first month
     - selectd: selected month

     Use monthTranslation template
     -->
<xsl:template name="generateMonthSelect">
  <xsl:param name="month">0</xsl:param>
  <xsl:param name="selected">0</xsl:param>
  <xsl:param name="can-be-empty">0</xsl:param>

  <xsl:if test="$can-be-empty=1">
      <option value="">
          <xsl:if test="$selected = ''">
              <xsl:attribute name="selected">selected</xsl:attribute>
          </xsl:if>
      </option>
  </xsl:if>
  <option value="{$month+1}">
    <xsl:if test="$selected = $month">
      <xsl:attribute name="selected">selected</xsl:attribute>
    </xsl:if>
    <xsl:call-template name="monthTranslation">
      <xsl:with-param name="month" select="$month"/>
    </xsl:call-template>
  </option>
  <xsl:if test="$month+1 &lt; 12">
    <xsl:call-template name="generateMonthSelect">
      <xsl:with-param name="month" select="$month + 1"/>
      <xsl:with-param name="selected" select="$selected"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>

<!-- Time Input
     - minute-step: step between each available minutes (15 for instance)
     - use-second: Display secondes input or not
     - hour-value, min-value, sec-value: default value for year, month and day
     - hour-name, min-name, sec-name: default value for year, month and day
     -->
<xsl:template name="MiogaFormInputTime">
  <xsl:param name="label"/>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="disposition">0</xsl:param>

  <!-- default values -->
  <xsl:param name="hour-value"></xsl:param>
  <xsl:param name="min-value"></xsl:param>
  <xsl:param name="sec-value"></xsl:param>

  <!-- default input names -->
  <xsl:param name="hour-name"><xsl:value-of select="$label"/>_hour</xsl:param>
  <xsl:param name="min-name"><xsl:value-of select="$label"/>_min</xsl:param>
  <xsl:param name="sec-name"><xsl:value-of select="$label"/>_sec</xsl:param>

  <!-- misc -->
  <xsl:param name="use-second">1</xsl:param>
  <xsl:param name="minute-step">1</xsl:param>

  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required" select="$is-required"/>
    <xsl:with-param name="has-error" select="$has-error"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <xsl:call-template name="MiogaFormInputTimeInternal">
        <xsl:with-param name="tabindex" select="$tabindex"/>
        <xsl:with-param name="hour-value" select="$hour-value"/>
        <xsl:with-param name="min-value" select="$min-value"/>
        <xsl:with-param name="sec-value" select="$sec-value"/>
        <xsl:with-param name="hour-name" select="$hour-name"/>
        <xsl:with-param name="min-name" select="$min-name"/>
        <xsl:with-param name="sec-name" select="$sec-name"/>
        <xsl:with-param name="use-second" select="$use-second"/>
        <xsl:with-param name="minute-step" select="$minute-step"/>
      </xsl:call-template>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- internal input time implementation -->
<xsl:template name="MiogaFormInputTimeInternal">
  <xsl:param name="tabindex"/>

  <!-- default values -->
  <xsl:param name="hour-value"/>
  <xsl:param name="min-value"/>
  <xsl:param name="sec-value"/>

  <!-- default input names -->
  <xsl:param name="hour-name"/>
  <xsl:param name="min-name"/>
  <xsl:param name="sec-name"/>

  <!-- misc -->
  <xsl:param name="use-second">1</xsl:param>
  <xsl:param name="minute-step">1</xsl:param>
  <xsl:param name="can-be-empty">0</xsl:param>

  <select name="{$hour-name}">
    <xsl:if test="$tabindex">
      <xsl:attribute name="tabindex">
        <xsl:value-of select="$tabindex"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="$can-be-empty=1">
        <option value=""></option>
    </xsl:if>
    <xsl:call-template name="generateNumberSelect">
      <xsl:with-param name="start" select="0"/>
      <xsl:with-param name="end" select="23"/>
      <xsl:with-param name="selected" select="$hour-value"/>
    </xsl:call-template>
  </select>
  <xsl:text>:</xsl:text>
  <select name="{$min-name}">
    <xsl:if test="$tabindex">
      <xsl:attribute name="tabindex">
        <xsl:value-of select="$tabindex+1"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="$can-be-empty=1">
        <option value=""></option>
    </xsl:if>
    <xsl:call-template name="generateNumberSelect">
      <xsl:with-param name="start" select="0"/>
      <xsl:with-param name="end" select="59"/>
      <xsl:with-param name="step" select="$minute-step"/>
      <xsl:with-param name="selected" select="$min-value"/>
    </xsl:call-template>
  </select>
  <xsl:if test="$use-second=1">
    <xsl:text>:</xsl:text>
    <select name="{$sec-name}">
      <xsl:if test="$tabindex">
        <xsl:attribute name="tabindex">
          <xsl:value-of select="$tabindex+2"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:call-template name="generateNumberSelect">
        <xsl:with-param name="start" select="0"/>
        <xsl:with-param name="end" select="59"/>
        <xsl:with-param name="selected" select="$sec-value"/>
      </xsl:call-template>
    </select>
  </xsl:if>
</xsl:template>

<!-- Complete datetime input
     Same parameters as MiogaFormInputDate and MiogaFormInputTime
     -->
<xsl:template name="MiogaFormInputDateTime">
  <xsl:param name="label"/>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="disposition">0</xsl:param>

  <!-- default values -->
  <xsl:param name="year-value"></xsl:param>
  <xsl:param name="month-value"></xsl:param>
  <xsl:param name="day-value"></xsl:param>
  <xsl:param name="hour-value"></xsl:param>
  <xsl:param name="min-value"></xsl:param>
  <xsl:param name="sec-value"></xsl:param>

  <!-- default input names -->
  <xsl:param name="year-name"><xsl:value-of select="$label"/>_year</xsl:param>
  <xsl:param name="month-name"><xsl:value-of select="$label"/>_month</xsl:param>
  <xsl:param name="day-name"><xsl:value-of select="$label"/>_day</xsl:param>
  <xsl:param name="hour-name"><xsl:value-of select="$label"/>_hour</xsl:param>
  <xsl:param name="min-name"><xsl:value-of select="$label"/>_min</xsl:param>
  <xsl:param name="sec-name"><xsl:value-of select="$label"/>_sec</xsl:param>

  <!-- misc -->
  <xsl:param name="use-second">1</xsl:param>
  <xsl:param name="minute-step">1</xsl:param>
  <xsl:param name="first-year">1900</xsl:param><!-- à prendre ds miogacontext -->
  <xsl:param name="nb-year">50</xsl:param>

  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required" select="$is-required"/>
    <xsl:with-param name="has-error" select="$has-error"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <xsl:call-template name="MiogaFormInputDateInternal">
        <xsl:with-param name="tabindex" select="$tabindex"/>
        <xsl:with-param name="day-value" select="$day-value"/>
        <xsl:with-param name="month-value" select="$month-value -1"/>
        <xsl:with-param name="year-value" select="$year-value"/>
        <xsl:with-param name="day-name" select="$day-name"/>
        <xsl:with-param name="month-name" select="$month-name"/>
        <xsl:with-param name="year-name" select="$year-name"/>
        <xsl:with-param name="nb-year" select="$nb-year"/>
        <xsl:with-param name="first-year" select="$first-year"/>
      </xsl:call-template>
      &#160;
      <xsl:call-template name="MiogaFormInputTimeInternal">
        <xsl:with-param name="tabindex" select="$tabindex+3"/>
        <xsl:with-param name="hour-value" select="$hour-value"/>
        <xsl:with-param name="min-value" select="$min-value"/>
        <xsl:with-param name="sec-value" select="$sec-value"/>
        <xsl:with-param name="hour-name" select="$hour-name"/>
        <xsl:with-param name="min-name" select="$min-name"/>
        <xsl:with-param name="sec-name" select="$sec-name"/>
        <xsl:with-param name="use-second" select="$use-second"/>
        <xsl:with-param name="minute-step" select="$minute-step"/>
      </xsl:call-template>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>


<xsl:template name="MiogaFormInputTimeInterval">
  <xsl:param name="label"/>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="tabindex"></xsl:param>

  <!-- default values -->
  <xsl:param name="hour-value-1"></xsl:param>
  <xsl:param name="min-value-1"></xsl:param>
  <xsl:param name="sec-value-1"></xsl:param>
  <xsl:param name="hour-value-2"></xsl:param>
  <xsl:param name="min-value-2"></xsl:param>
  <xsl:param name="sec-value-2"></xsl:param>

  <!-- default input names -->
  <xsl:param name="hour-name-1"><xsl:value-of select="$label"/>_hour_1</xsl:param>
  <xsl:param name="min-name-1"><xsl:value-of select="$label"/>_min_1</xsl:param>
  <xsl:param name="sec-name-1"><xsl:value-of select="$label"/>_sec_1</xsl:param>
  <xsl:param name="hour-name-2"><xsl:value-of select="$label"/>_hour_2</xsl:param>
  <xsl:param name="min-name-2"><xsl:value-of select="$label"/>_min_2</xsl:param>
  <xsl:param name="sec-name-2"><xsl:value-of select="$label"/>_sec_2</xsl:param>

  <!-- misc -->
  <xsl:param name="use-second">1</xsl:param>
  <xsl:param name="minute-step">1</xsl:param>
  <xsl:param name="can-be-empty">0</xsl:param>

  <tr>
    <td xsl:use-attribute-sets="mioga-form-input-label-attr">
      <xsl:call-template name="MiogaFormInputTranslation">
        <xsl:with-param name="label" select="$label"/>
      </xsl:call-template>&#160;:&#160;
    </td>
    <td>
        <table xsl:use-attribute-sets="mioga-empty-table">
            <tr>
                <td>
                    <xsl:call-template name="MiogaFormInputTimeInternal">
                        <xsl:with-param name="tabindex" select="$tabindex"/>
                        <xsl:with-param name="hour-value" select="$hour-value-1"/>
                        <xsl:with-param name="min-value" select="$min-value-1"/>
                        <xsl:with-param name="sec-value" select="$sec-value-1"/>
                        <xsl:with-param name="hour-name" select="$hour-name-1"/>
                        <xsl:with-param name="min-name" select="$min-name-1"/>
                        <xsl:with-param name="sec-name" select="$sec-name-1"/>
                        <xsl:with-param name="use-second" select="$use-second"/>
                        <xsl:with-param name="minute-step" select="$minute-step"/>
                        <xsl:with-param name="can-be-empty" select="$can-be-empty"/>
                    </xsl:call-template>
                </td>
                <td>
                    &#160;<xsl:call-template name="WidgetFormTranslation">
                        <xsl:with-param name="label">to</xsl:with-param>
                    </xsl:call-template>&#160;
                </td>
                <td>
                    <xsl:call-template name="MiogaFormInputTimeInternal">
                        <xsl:with-param name="hour-value" select="$hour-value-2"/>
                        <xsl:with-param name="min-value" select="$min-value-2"/>
                        <xsl:with-param name="sec-value" select="$sec-value-2"/>
                        <xsl:with-param name="hour-name" select="$hour-name-2"/>
                        <xsl:with-param name="min-name" select="$min-name-2"/>
                        <xsl:with-param name="sec-name" select="$sec-name-2"/>
                        <xsl:with-param name="use-second" select="$use-second"/>
                        <xsl:with-param name="minute-step" select="$minute-step"/>
                        <xsl:with-param name="can-be-empty" select="$can-be-empty"/>
                    </xsl:call-template>                
                </td>
            </tr>
        </table>
    </td>
  </tr>
</xsl:template>


<!-- Display a button wich call an external screen to select a single value -->
<xsl:template name="MiogaFormInputExternalSelectSingle">
  <xsl:param name="label"/>
  <xsl:param name="action"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>
  <xsl:param name="disp_value" select="$value"/>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="disposition">0</xsl:param>

  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required" select="$is-required"/>
    <xsl:with-param name="has-error" select="$has-error"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <table xsl:use-attribute-sets="mioga-empty-table">
        <tr>
          <td>
            <xsl:value-of select="$disp_value"/>&#160;
            <input type="hidden" name="{$label}" value="{$value}"/>
          </td>
          <td>
            <xsl:call-template name="choose-form-button">
              <xsl:with-param name="name" select="$action"/>
            </xsl:call-template>
          </td>
        </tr>
      </table>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Display a button wich call an external screen to select a multiple value -->
<xsl:template name="MiogaFormInputExternalSelectMultiple">
  <xsl:param name="label"/>
  <xsl:param name="list-name" select="$label"/>
  <xsl:param name="action"/>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="tabindex"></xsl:param>
  
  <tr>
    <td colspan="2">
      <table xsl:use-attribute-sets="mioga-empty-table">
        <tr>
          <td>
            <p xsl:use-attribute-sets="mioga-form-input-label-attr">
              <xsl:call-template name="MiogaFormInputTranslation">
                <xsl:with-param name="label" select="$label"/>
              </xsl:call-template>&#160;:&#160;
            </p>
          </td>
          <td>
            <xsl:call-template name="affect-form-button">
              <xsl:with-param name="name" select="$action"/>
            </xsl:call-template>
          </td>
        </tr>
        <tr align="center">
          <td colspan="2">
            <xsl:variable name="sll" select="*[name(.)=$list-name]"/>
           
            <xsl:choose>
              <xsl:when test="count($sll/SimpleLargeList/List/Row) = 0">
                <br/>
                <i>
                  <xsl:call-template name="formLabel">
                    <xsl:with-param name="label">no-element</xsl:with-param>
                  </xsl:call-template>&#160;
                </i>
              </xsl:when>
              <xsl:otherwise>
                <xsl:apply-templates select="$sll/SimpleLargeList">
                  <xsl:with-param name="bottomindex">
                    <xsl:choose>
                      <xsl:when test="$sll/SimpleLargeList/List/@nb_elems &gt; $sll/SimpleLargeList/List/@nb_elem_per_page">1</xsl:when>
                      <xsl:otherwise>0</xsl:otherwise>
                    </xsl:choose>
                  </xsl:with-param>
                  <xsl:with-param name="no_form">1</xsl:with-param>
                </xsl:apply-templates>
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</xsl:template>

<!-- Display an input to upload a file -->
<xsl:template name="MiogaFormInputUploadFile">
  <xsl:param name="label"/>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="disposition">0</xsl:param>

  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="body">
      <input type="file" name="{$label}"/>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Input with anything you want, "content" parameter is paste with xsl:copy-of -->
<xsl:template name="MiogaFormInputMisc">
  <xsl:param name="label"/>
  <xsl:param name="value" select="fields/*[name(.)=$label]"/>
  <xsl:param name="content"></xsl:param>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="disposition">0</xsl:param>
  
  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required" select="$is-required"/>
    <xsl:with-param name="has-error" select="$has-error"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <xsl:copy-of select="$content"/>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template name="MiogaFormInputDuration">
  <xsl:param name="label"/>
  <xsl:param name="value" select="fields/*[name(.)=concat($label,'_value')]"/>
  <xsl:param name="value_unit" select="fields/*[name(.)=concat($label,'_unit')]"/>
  <xsl:param name="use-seconds">1</xsl:param>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="size">4</xsl:param>
  <xsl:param name="maxlength">16</xsl:param>
  <xsl:param name="disposition">0</xsl:param>

   <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required" select="$is-required"/>
    <xsl:with-param name="has-error" select="$has-error"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <table xsl:use-attribute-sets="mioga-empty-table">
        <tr>
          <td>
            <input type="text" name="{$label}_value" maxlength="{$maxlength}" size="{$size}" value="{$value}">
              <xsl:if test="$tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="$tabindex"/>
                </xsl:attribute>
              </xsl:if>
            </input>
          </td>
          <td>
            <select name="{$label}_unit">
                <xsl:if test="$use-seconds!=0">
                    <xsl:call-template name="MiogaFormInputDurationOption">
                        <xsl:with-param name="name">second</xsl:with-param>
                        <xsl:with-param name="value">0</xsl:with-param>
                        <xsl:with-param name="selected_value" select="$value_unit"/>
                    </xsl:call-template>
                </xsl:if>
                
                <xsl:call-template name="MiogaFormInputDurationOption">
                    <xsl:with-param name="name">minute</xsl:with-param>
                    <xsl:with-param name="value">1</xsl:with-param>
                    <xsl:with-param name="selected_value" select="$value_unit"/>
                </xsl:call-template>
                
                <xsl:call-template name="MiogaFormInputDurationOption">
                    <xsl:with-param name="name">hour</xsl:with-param>
                    <xsl:with-param name="value">2</xsl:with-param>
                    <xsl:with-param name="selected_value" select="$value_unit"/>
                </xsl:call-template>
                
                <xsl:call-template name="MiogaFormInputDurationOption">
                    <xsl:with-param name="name">day</xsl:with-param>
                    <xsl:with-param name="value">3</xsl:with-param>
                    <xsl:with-param name="selected_value" select="$value_unit"/>
                </xsl:call-template>

            </select>
          </td>
        </tr>
      </table>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template name="MiogaFormInputDurationOption">
     <xsl:param name="name"/>
     <xsl:param name="value"/>
     <xsl:param name="selected_value"/>
     
     <option value="{$value}">
         <xsl:if test="$selected_value = $value">
             <xsl:attribute name="selected">selected</xsl:attribute>
         </xsl:if>
         
         <xsl:call-template name="durationTranslation">
             <xsl:with-param name="label" select="$name"/>
         </xsl:call-template>
     </option>
</xsl:template>

<xsl:template name="MiogaFormInputSimpleColorSelect">
  <xsl:param name="label"/>
  <xsl:param name="value"><xsl:value-of select="fields/*[name(.)=$label]"/></xsl:param>
  <xsl:param name="is-required">0</xsl:param>
  <xsl:param name="has-error" select="count(//errors/err[@arg = $label])"/>
  <xsl:param name="tabindex"></xsl:param>
  <xsl:param name="adv-color-select-action"></xsl:param>
  <xsl:param name="disposition">0</xsl:param>

  <xsl:call-template name="MiogaFormInputInternal">
    <xsl:with-param name="label" select="$label"/>
    <xsl:with-param name="is-required" select="$is-required"/>
    <xsl:with-param name="has-error" select="$has-error"/>
    <xsl:with-param name="disposition" select="$disposition"/>
    <xsl:with-param name="body">
      <table xsl:use-attribute-sets="mioga-empty-table">
        <tr>
          <td>
            <select name="{$label}">
              <xsl:if test="$tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="$tabindex"/>
                </xsl:attribute>
              </xsl:if>
              
              <option value="{$value}" selected="selected">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color" select="$value"/>
                </xsl:call-template>
              </option>
              <option value="#FFFFFF">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">white</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#000000">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">black</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#006699">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">dark-blue</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#00CCCC">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">light-blue</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#009966">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">dark-green</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#00FF99">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">light-green</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#FF9933">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">orange</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#FFFF66">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">yellow</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#CC99CC">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">purple</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#FF6666">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">dark-pink</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#FF9999">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">light-pink</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#CC9900">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">brown</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#C0C0C0">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">silver</xsl:with-param>
                </xsl:call-template>
              </option>
              <option value="#008080">
                <xsl:call-template name="ColorNameTranslation">
                  <xsl:with-param name="color">dark-cyan</xsl:with-param>
                </xsl:call-template>
              </option>
            </select>
              
          </td>
          <xsl:if test="$adv-color-select-action != ''">
            <td>
              <xsl:call-template name="other-form-button">
                <xsl:with-param name="name" select="$adv-color-select-action"/>
              </xsl:call-template>
            </td>
          </xsl:if>
        </tr>
      </table>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<xsl:template name="durationTranslation">
  <xsl:param name="label"/>
  
  <xsl:choose>
    <xsl:when test="$label='second'"><xsl:value-of select="mioga:gettext('seconds')"/></xsl:when>
    <xsl:when test="$label='minute'"><xsl:value-of select="mioga:gettext('minutes')"/></xsl:when>
    <xsl:when test="$label='hour'"><xsl:value-of select="mioga:gettext('hours')"/></xsl:when>
    <xsl:when test="$label='day'"><xsl:value-of select="mioga:gettext('days')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="monthTranslation">
  <xsl:param name="month"/>
  
  <xsl:choose>
    <xsl:when test="$month=0"><xsl:value-of select="mioga:gettext('January')"/></xsl:when>
    <xsl:when test="$month=1"><xsl:value-of select="mioga:gettext('February')"/></xsl:when>
    <xsl:when test="$month=2"><xsl:value-of select="mioga:gettext('March')"/></xsl:when>
    <xsl:when test="$month=3"><xsl:value-of select="mioga:gettext('April')"/></xsl:when>
    <xsl:when test="$month=4"><xsl:value-of select="mioga:gettext('May')"/></xsl:when>
    <xsl:when test="$month=5"><xsl:value-of select="mioga:gettext('June')"/></xsl:when>
    <xsl:when test="$month=6"><xsl:value-of select="mioga:gettext('July')"/></xsl:when>
    <xsl:when test="$month=7"><xsl:value-of select="mioga:gettext('August')"/></xsl:when>
    <xsl:when test="$month=8"><xsl:value-of select="mioga:gettext('September')"/></xsl:when>
    <xsl:when test="$month=9"><xsl:value-of select="mioga:gettext('October')"/></xsl:when>
    <xsl:when test="$month=10"><xsl:value-of select="mioga:gettext('November')"/></xsl:when>
    <xsl:when test="$month=11"><xsl:value-of select="mioga:gettext('December')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="formLabel">
    <xsl:param name="label"/>
    
    <xsl:choose>
        <xsl:when test="$label='no-element'"><xsl:value-of select="mioga:gettext('None')"/></xsl:when>
    </xsl:choose>
</xsl:template>

<xsl:template name="ColorNameTranslation">
  <xsl:param name="color"/>

  <xsl:choose>
    <xsl:when test="$color='black'       or $color='#000000'"><xsl:value-of select="mioga:gettext('Black')"/></xsl:when>
    <xsl:when test="$color='white'       or $color='#FFFFFF'"><xsl:value-of select="mioga:gettext('White')"/></xsl:when>
    <xsl:when test="$color='dark-blue'   or $color='#006699'"><xsl:value-of select="mioga:gettext('Dark blue')"/></xsl:when>
    <xsl:when test="$color='light-blue'  or $color='#00CCCC'"><xsl:value-of select="mioga:gettext('Light blue')"/></xsl:when>
    <xsl:when test="$color='dark-green'  or $color='#009966'"><xsl:value-of select="mioga:gettext('Dark green')"/></xsl:when>
    <xsl:when test="$color='light-green' or $color='#00FF99'"><xsl:value-of select="mioga:gettext('Light green')"/></xsl:when>
    <xsl:when test="$color='orange'      or $color='#FF9933'"><xsl:value-of select="mioga:gettext('Orange')"/></xsl:when>
    <xsl:when test="$color='yellow'      or $color='#FFFF66'"><xsl:value-of select="mioga:gettext('Yellow')"/></xsl:when>
    <xsl:when test="$color='purple'      or $color='#CC99CC'"><xsl:value-of select="mioga:gettext('Purple')"/></xsl:when>
    <xsl:when test="$color='dark-pink'   or $color='#FF6666'"><xsl:value-of select="mioga:gettext('Dark pink')"/></xsl:when>
    <xsl:when test="$color='light-pink'  or $color='#FF9999'"><xsl:value-of select="mioga:gettext('Light pink')"/></xsl:when>
    <xsl:when test="$color='brown'       or $color='#CC9900'"><xsl:value-of select="mioga:gettext('Brown')"/></xsl:when>
    <xsl:when test="$color='silver'      or $color='#C0C0C0'"><xsl:value-of select="mioga:gettext('Silver')"/></xsl:when>
    <xsl:when test="$color='dark-cyan'   or $color='#008080'"><xsl:value-of select="mioga:gettext('Dark Cyan')"/></xsl:when>
    <xsl:when test="$color='red'         or $color='#FF0000'"><xsl:value-of select="mioga:gettext('Red')"/></xsl:when>
    <xsl:when test="$color='green'       or $color='#00FF00'"><xsl:value-of select="mioga:gettext('Green')"/></xsl:when>
    <xsl:when test="$color='blue'        or $color='#0000FF'"><xsl:value-of select="mioga:gettext('Blue')"/></xsl:when>
    <xsl:otherwise><xsl:value-of select="$color"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>


<xsl:template name="WidgetFormTranslation">
    <xsl:param name="label"/>
    
    <xsl:choose>
        <xsl:when test="$label='to'"><xsl:value-of select="mioga:gettext('to')"/></xsl:when>
    </xsl:choose>
</xsl:template>


<xsl:template name="flexibleConstraintTranslation">
  <xsl:param name="name"/>
  <xsl:choose>
    <xsl:when test="$name='none'"></xsl:when>
    <xsl:when test="$name='fixe'"><xsl:value-of select="mioga:gettext('fixed')"/></xsl:when>
    <xsl:when test="$name='asap'"><xsl:value-of select="mioga:gettext('sooner')"/></xsl:when>
    <xsl:when test="$name='alap'"><xsl:value-of select="mioga:gettext('later')"/></xsl:when>
  </xsl:choose>
</xsl:template>


</xsl:stylesheet>
