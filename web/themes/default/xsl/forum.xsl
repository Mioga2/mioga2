<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
						xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>
<xsl:import href="forum_parts.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<xsl:include href="tiny_mce.xsl"/>
<xsl:include href="scriptaculous.xsl"/>


<!-- ===============
	root document
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="scriptaculous-js"/>
  <xsl:call-template name="tinymce-js"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/forum.css" media="screen" />
	<script type="text/javascript" src="{$theme_uri}/javascript/forum.js"></script>
	<xsl:call-template name="tinymce-init-js"/>

	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="theme-css"/>
</head>
<body id="forum_body" class="forum">
	<xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- ===============
	DisplayMain
	================ -->

<xsl:template match="DisplayMain">
<xsl:call-template name="DisplayAppTitle">
	<xsl:with-param name="help">applications.html#forum</xsl:with-param>
</xsl:call-template>
<div id="modal-box" class="modal-box" style="display:none;"></div>

<div id="forum_main" class="mioga_main">
	<xsl:call-template name="DialogSetParameters" />
	<xsl:call-template name="DialogSetUserParameters" />
	<div class="menu_forum">
		<ul class="hmenu">
            <xsl:if test="count (appnav/group) != 0">
                <li>
                    <xsl:call-template name="AppNav_Select"/>
                </li>
            </xsl:if>
			<xsl:if test="$anim_right='1'">
				<li>
					<a href="#" class="action" id="add_category" >
						<xsl:attribute name="onclick">AddCategory('<xsl:value-of select="$anim_action" />')</xsl:attribute>
						<xsl:value-of select="//action[@id = 'add_category']/@label" />
					</a>
				</li>
				<li>
					<a href="#" class="action" id="set_parameters" >
						<xsl:attribute name="onclick">SetParameters('<xsl:value-of select="$anim_action" />')</xsl:attribute>
						<xsl:value-of select="//action[@id = 'set_parameters']/@label" />
					</a>
				</li>
			</xsl:if>
			<xsl:if test="//subscription='1'">
				<li>
					<a href="#" class="action" id="subscribe" >
						<xsl:attribute name="onclick">SetUserParameters()</xsl:attribute>
						<xsl:value-of select="//action[@id = 'subscribe']/@label" />
					</a>
				</li>
			</xsl:if>
		</ul>
	</div>
	<div class="clear-both"></div>
	<div id="categories">
		<xsl:apply-templates select="Categories" />
	</div>
</div>
</xsl:template>

<!-- ===============
	DisplayThematic
	================ -->

<xsl:template match="DisplayThematic">
<xsl:call-template name="DisplayAppTitle">
	<xsl:with-param name="help">applications.html#forum</xsl:with-param>
</xsl:call-template>
<!-- <div id="modal-box" class="modal-box" style="display:none;"></div>
<div id="subject_dialog" class="dialog" style="display:none; visibility:hidden;"></div>
-->
<div id="forum_main" class="mioga_main">
	<xsl:call-template name="UpdateThematic" />
</div>
</xsl:template>

<!-- ===============
	DisplaySubject
	================ -->

<!-- header -->

<xsl:template name="DisplaySubject_header">
<xsl:variable name="category_rowid"><xsl:value-of select="//subject_env/category_id" /></xsl:variable>
<xsl:variable name="thematic_rowid"><xsl:value-of select="//subject_env/thematic_id" /></xsl:variable>
<div id="header">
	<h2><a href="{$private_bin_uri}/Forum/DisplayMain" ><xsl:value-of select="//subject_env/category_label" /></a>&#160;/&#160;<a href="{$private_bin_uri}/Forum/DisplayThematic?category_id={$category_rowid}&amp;thematic_id={$thematic_rowid}"><xsl:value-of select="//subject_env/thematic_label" /></a>&#160;/&#160;<xsl:value-of select="//subject/label" /></h2>
	<div class="menu_forum">
		<ul class="hmenu">
			<xsl:if test="$write_right='1' and //subject/open='1'">
				<li>
					<a class="action" id="create_message" >
							<xsl:attribute name="href"><xsl:value-of select="$view_action" />?action=create_message&amp;subject_id=<xsl:value-of select="//subject/rowid" />&amp;category_id=<xsl:value-of select="$category_rowid" />&amp;thematic_id=<xsl:value-of select="$thematic_rowid" /></xsl:attribute>
						<xsl:value-of select="//action[@id = 'create_message']/@label" />
					</a>
				</li>
			</xsl:if>
		</ul>
	</div>
	<xsl:if test='//Messages/@nb_pages &gt; 1'>
	<div>
		<xsl:call-template name="change_page">
			<xsl:with-param name="cur_page"><xsl:value-of select="//Messages/@page" /></xsl:with-param>
			<xsl:with-param name="nb_pages"><xsl:value-of select="//Messages/@nb_pages" /></xsl:with-param>
			<xsl:with-param name="base_url"><xsl:value-of select="$private_bin_uri" />/Forum/DisplaySubject?subject_id=<xsl:value-of select="//subject/rowid" /></xsl:with-param>
		</xsl:call-template>
	</div>
	</xsl:if>
</div>
</xsl:template>

<!-- body -->

<xsl:template match="DisplaySubject">
<xsl:call-template name="DisplayAppTitle">
	<xsl:with-param name="help">applications.html#forum</xsl:with-param>
</xsl:call-template>
<div id="modal-box" class="modal-box" style="display:none;"></div>

<div id="forum_main" class="mioga_main">
	<xsl:call-template name="DisplaySubject_header" />
	<div id="messages">
		<xsl:apply-templates select="Messages" />
	</div>
	<xsl:call-template name="DisplaySubject_header" />
</div>
</xsl:template>

<!-- ===============
	DisplayNewMessages
	================ -->

<xsl:template match="DisplayNewMessages">
<xsl:call-template name="DisplayAppTitle">
	<xsl:with-param name="help">applications.html#forum</xsl:with-param>
</xsl:call-template>

<div id="forum_main" class="mioga_main">
	<xsl:choose>
		<xsl:when test="//miogacontext/referer != '' and not (contains (//miogacontext/referer, $view_action))">
			<p><a href="{//miogacontext/referer}" ><xsl:value-of select="mioga:gettext('Forum return')" /></a></p>
		</xsl:when>
		<xsl:otherwise>
			<p><a href="DisplayMain" ><xsl:value-of select="mioga:gettext('Forum return')" /></a></p>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:for-each select="Subject">
		<div class="subject_messages">
			<h2><xsl:value-of select="subject//label" /></h2>
			<div>
				<xsl:apply-templates select="Messages" />
			</div>
		</div>
	</xsl:for-each>
	<xsl:if test="count(//Subject/Messages/message) &gt; 10" >
		<p><a href="{//miogacontext/referer}" ><xsl:value-of select="mioga:gettext('Forum return')" /></a></p>
	</xsl:if>
	<xsl:if test="count(//Subject/Messages/message) = 0" >
		<p><xsl:value-of select="mioga:gettext('No new messages')" /></p>
	</xsl:if>
</div>
</xsl:template>

<!-- ===============
	Categories
	================ -->

<xsl:template match="Categories" >
	<xsl:apply-templates />
</xsl:template>

<!-- ===============
	Messages
	================ -->

<xsl:template match="Messages" >
<xsl:for-each select="message">
	<div class="forum_message sbox border_color">
		<div class="message_info title_bg_color">
			<h2><xsl:call-template name="LocalDateTime"><xsl:with-param name="node" select="./created" /></xsl:call-template> by <xsl:value-of select="./firstname" />&#160;<xsl:value-of select="./lastname" /></h2>
		</div>
		<div class="message_body"><xsl:value-of select="./message_text" disable-output-escaping="yes" /></div>
		<xsl:if test="./display_footer">
			<table class="message_footer">
			<tr>
				<td>
					<xsl:if test="./have_been_modified">
						<p class="message_note"><xsl:value-of select="mioga:gettext('modified by')" />&#160;<xsl:value-of select="./modifier_firstname" />&#160;<xsl:value-of select="./modifier_lastname" />&#160;<xsl:call-template name="LocalDateTime"><xsl:with-param name="node" select="./created" /></xsl:call-template></p>
					</xsl:if>
				</td>
				<td class="menu">
				<xsl:choose>
					<xsl:when test='./no_menu'>&#160;</xsl:when>
					<xsl:otherwise>
						<ul class="hmenu">
							<xsl:if test="$moder_right='1' or ./author">
								<li>
									<a class="action" id="edit_message" >
											<xsl:attribute name="href"><xsl:value-of select="$view_action" />?action=edit_message&amp;message_id=<xsl:value-of select="./rowid" /></xsl:attribute>
										<xsl:value-of select="//action[@id = 'edit_message']/@label" />
									</a>
								</li>
							</xsl:if>
							<xsl:if test="//moderated='1'">
								<li>
									<a href="#" class="action" id="signal_message" >
										<xsl:attribute name="onclick">if (confirm('<xsl:value-of select="mioga:gettext('Signal this message to moderators ?')" />')) { SignalMessage('<xsl:value-of select="$view_action" />', '<xsl:value-of select="./rowid" />')} return false;</xsl:attribute>
										<xsl:value-of select="//action[@id = 'signal_message']/@label" />
									</a>
								</li>
							</xsl:if>
							<xsl:if test="$moder_right='1'">
								<li>
									<a class="action" href="#">
										<xsl:attribute name="onclick">if (confirm('<xsl:value-of select="mioga:gettext('Delete message ?')" />')) { ProcessAnimAction('<xsl:value-of select="$anim_action" />?action=suppress_message&amp;message_id=<xsl:value-of select="./rowid" />');} return false;</xsl:attribute>
										<img src="{$image_uri}/16x16/actions/trash-empty.png" >
											<xsl:attribute name="alt"><xsl:value-of select="//action[@id = 'suppress_message']/@label" /></xsl:attribute>
											<xsl:attribute name="title"><xsl:value-of select="//action[@id = 'suppress_message']/@label" /></xsl:attribute>
										</img>
									</a>
								</li>
							</xsl:if>
						</ul>
					</xsl:otherwise>
				</xsl:choose>
				</td>
			</tr>
			</table>
		</xsl:if>
	</div>
</xsl:for-each>
</xsl:template>

<!-- ===============
	DisplaySubjectDialog
	================ -->

<xsl:template match="DisplaySubjectDialog">
<xsl:call-template name="DisplayAppTitle">
	<xsl:with-param name="help">applications.html#forum</xsl:with-param>
</xsl:call-template>
<div id="forum_main" class="mioga_main">
	<div id="subject_dialog_content" class="sbox border_color">
		<h2 class="title_bg_color"><xsl:value-of select="mioga:gettext('Edit subject')" /></h2>
		<div class="content">
		<form id="form_subject" method="post" action="ProcessViewAction">
			<input type="hidden" name="action" value="update_subject" />
			<input type="hidden" name="category_id" value="{EditSubject/category_id}" />
			<input type="hidden" name="thematic_id" value="{EditSubject/thematic_id}" />
			<input type="hidden" name="subject_id" value="{EditSubject/subject_id}" />
			<xsl:if test="EditSubject/show_message">
				<input type="hidden" name="show_message" value="1" />
			</xsl:if>
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext('Subject')" /></legend>
				<input class="text border_color" size="64" type="text" name="subject" id="subject" value="{EditSubject/subject}" />
				<xsl:if test="//errors/err[@arg = 'subject']">
					<xsl:call-template name="error-message">
						<xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'subject']/type" /></xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			</fieldset>
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext('Parameters')" /></legend>
				<p><input type="checkbox" name="post_it" >
					<xsl:if test="EditSubject/post_it='1'">
						<xsl:attribute name="checked">on</xsl:attribute>
					</xsl:if>
				</input>&#160;<xsl:value-of select="mioga:gettext('Post It subject')" /></p>
				<xsl:if test="//errors/err[@arg = 'post_it']">
					<xsl:call-template name="error-message">
						<xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'post_it']/type" /></xsl:with-param>
					</xsl:call-template>
				</xsl:if>

				<p><input type="checkbox" name="open" >
					<xsl:if test="EditSubject/open='1'">
						<xsl:attribute name="checked">on</xsl:attribute>
					</xsl:if>
				</input>&#160;<xsl:value-of select="mioga:gettext('Open subject')" /></p>
				<xsl:if test="//errors/err[@arg = 'open']">
					<xsl:call-template name="error-message">
						<xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'open']/type" /></xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			</fieldset>
			<xsl:if test="EditSubject/show_message">
				<fieldset>
					<legend><xsl:value-of select="mioga:gettext('Message')" /></legend>
					<textarea class="mceEditor border_color" name="message" id="message"><xsl:value-of select="EditSubject/message" /></textarea>
					<xsl:if test="//errors/err[@arg = 'message']">
						<xsl:call-template name="error-message">
							<xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'message']/type" /></xsl:with-param>
						</xsl:call-template>
					</xsl:if>
				</fieldset>
			</xsl:if>
			<ul class="button_list">
				<li>
					<input class="button" type="submit">
						<xsl:attribute name="value"><xsl:value-of select="mioga:gettext('OK')" /></xsl:attribute>
					</input>
				</li>
				<li>
					<a class="button" href="{//cancel_url}"><xsl:value-of select="mioga:gettext('Cancel')" /></a>
				</li>
			</ul>
		</form>
		</div>
	</div>
</div>
</xsl:template>

<!-- ===============
	DisplayMessageDialog
	================ -->

<xsl:template match="DisplayMessageDialog">
<xsl:call-template name="DisplayAppTitle">
	<xsl:with-param name="help">applications.html#forum</xsl:with-param>
</xsl:call-template>
<div id="forum_main" class="mioga_main">
	<div id="message_dialog_content" class="sbox border_color">
		<h2 class="title_bg_color"><xsl:value-of select="mioga:gettext('Edit message')" /></h2>
		<div class="content">
		<form id="form_message" method="post" action="ProcessViewAction">
			<input type="hidden" name="action" value="update_message" />
			<input type="hidden" name="category_id" value="{EditMessage/category_id}" />
			<input type="hidden" name="thematic_id" value="{EditMessage/thematic_id}" />
			<input type="hidden" name="subject_id" value="{EditMessage/subject_id}" />
			<input type="hidden" name="message_id" value="{EditMessage/message_id}" />
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext('Message')" /></legend>
				<textarea class="mceEditor border_color" name="message" id="message"><xsl:value-of select="EditMessage/message" /></textarea>
				<xsl:if test="//errors/err[@arg = 'message']">
					<xsl:call-template name="error-message">
						<xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'message']/type" /></xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			</fieldset>
			<ul class="button_list">
				<li>
					<input class="button" type="submit">
						<xsl:attribute name="value"><xsl:value-of select="mioga:gettext('OK')" /></xsl:attribute>
					</input>
				</li>
				<li>
					<a class="button" href="{//cancel_url}"><xsl:value-of select="mioga:gettext('Cancel')" /></a>
				</li>
			</ul>
		</form>
		</div>
	</div>
	<div id="messages">
		<p><xsl:value-of select="mioga:gettext('Previous messages')" /></p>
		<xsl:apply-templates select="Messages" />
	</div>
</div>
</xsl:template>

<!-- ===============
	DialogSetParameters
	================ -->

<xsl:template name="DialogSetParameters">
<div id="dialog_parameters" class="sbox border_color dialog" style="display:none;">
	<div class="sbox border_color" id="parameters">
		<h2 class="title_bg_color"><xsl:value-of select="mioga:gettext('Parameters')" /></h2>
		<div class="content">
			<form name="forum_parameters" method="POST" action="{$anim_action}">
				<input type="hidden" name="action" value="set_parameters" />
				<xsl:call-template name="CSRF-input"/>
				<input type="hidden" name="rowid" >
					<xsl:attribute name="value"><xsl:value-of select="rowid" /></xsl:attribute>
				</input>
				<p><input type="checkbox" name="simple_ihm" >
					<xsl:if test="simple_ihm='1'">
						<xsl:attribute name="checked">on</xsl:attribute>
					</xsl:if>
				</input>&#160;<xsl:value-of select="mioga:gettext('Simple IHM')" /></p>
				<p><input type="checkbox" name="moderated" >
					<xsl:if test="moderated='1'">
						<xsl:attribute name="checked">on</xsl:attribute>
					</xsl:if>
				</input>&#160;<xsl:value-of select="mioga:gettext('Moderated forum')" /></p>

				<p><input type="checkbox" name="subscription" >
					<xsl:if test="subscription='1'">
						<xsl:attribute name="checked">on</xsl:attribute>
					</xsl:if>
				</input>&#160;<xsl:value-of select="mioga:gettext('Authorize subscription')" /></p>
				<p><input type="checkbox" name="mail_alert" >
					<xsl:if test="mail_alert='1'">
						<xsl:attribute name="checked">on</xsl:attribute>
					</xsl:if>
				</input>&#160;<xsl:value-of select="mioga:gettext('Send mail alerts')" /></p>
				<ul class="button_list">
					<li><a class="button" href="#" onclick="ValidateParameters();"><xsl:value-of select="mioga:gettext('OK')" /></a></li>
					<li><a class="button"  href="#" onclick="CloseDialog('dialog_parameters'); Element.hide('modal-box');"><xsl:value-of select="mioga:gettext('Cancel')" /></a></li>
				</ul>
			</form>
		</div>
	</div>
</div>
</xsl:template>

<!-- ===============
	DialogSetUsersParameters
	================ -->

<xsl:template name="DialogSetUserParameters">
<div id="dialog_user_params" class="sbox border_color dialog" style="display:none;">
	<div class="sbox border_color" id="parameters">
		<h2 class="title_bg_color"><xsl:value-of select="mioga:gettext('User parameters')" /></h2>
		<div class="content">
			<form name="forum_user_params" method="GET" action="{$main_action}">
				<input type="hidden" name="action" value="set_user_params" />
				<input type="hidden" name="rowid" >
					<xsl:attribute name="value"><xsl:value-of select="rowid" /></xsl:attribute>
				</input>
				<p><input type="checkbox" name="subscribe" >
					<xsl:if test="//UserParams/subscribe='1'">
						<xsl:attribute name="checked">on</xsl:attribute>
					</xsl:if>
				</input>&#160;<xsl:value-of select="//UserParams/subscribe/@label"/></p>
				<ul class="button_list">
					<li><a class="button" href="#" onclick="ValidateUserParameters();"><xsl:value-of select="mioga:gettext('OK')" /></a></li>
					<li><a class="button" href="#" onclick="CloseDialog('dialog_user_params'); Element.hide('modal-box');"><xsl:value-of select="mioga:gettext('Cancel')" /></a></li>
				</ul>
			</form>
		</div>
	</div>
</div>
</xsl:template>

</xsl:stylesheet>

