<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:poll="http://www.mioga2.org/portal/mioglets/Poll" xmlns:mioga="urn:mioga">

<xsl:output method="html"/>

<!-- =================================
     Handle Polls
     ================================= -->

<!-- Editor Form -->

<xsl:template match="poll:DisplayPolls" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">poll_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

	<xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>
    
</xsl:template>


<!-- =======================
	DisplayPolls
	======================= -->

<xsl:template match="poll:DisplayPolls">
<xsl:param name="node"/>
<div class="poll_mioglet">
    <xsl:choose>
    	<xsl:when test="count(Poll) = 0"><p style="font-style: italic; text-align: center;"><xsl:value-of select="mioga:gettext('No poll currently available')"/></p></xsl:when>
    	<xsl:otherwise>
            <ul>
            <xsl:for-each select="Poll">
                <xsl:variable name="odd_or_even">
                    <xsl:choose>
                        <xsl:when test="position() mod 2 = 0">even</xsl:when>
                        <xsl:otherwise>odd</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                
                <li class="{$odd_or_even}"><a href="{@link}"><xsl:value-of select="."/></a></li>
            </xsl:for-each>
            </ul>
        </xsl:otherwise>
    </xsl:choose>
</div>
</xsl:template>

</xsl:stylesheet>
