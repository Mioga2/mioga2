<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ext="urn:ext" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="text" indent="no" omit-xml-declaration="yes" encoding="UTF-8" media-type="text/css"/>

<xsl:template match="/">

.mimetype {
  background-position: center left;
  background-repeat: no-repeat;
}
.mime-128-multiple {
  background-image: url('<xsl:value-of select="$theme_uri"/>/images/128x128/places/document-multiple.png');
}
.mime-128-folder-image {
  background-image: url('<xsl:value-of select="$theme_uri"/>/images/128x128/places/folder-image.png');
}
<xsl:apply-templates />
</xsl:template>

<xsl:template match="IncludeMime">
  <xsl:apply-templates select="mimetype" />
</xsl:template>

<xsl:template match="mimetype">
<xsl:variable name="section">
	<xsl:choose>
		<xsl:when test="class='folder'">places</xsl:when>
		<xsl:otherwise>mimetypes</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<!-- Initialize class name (replacing dots by dashes) -->
<xsl:variable name="classname">
	<xsl:call-template name="ConvertClass">
		<xsl:with-param name="string">
			<xsl:value-of select="class"/>
		</xsl:with-param>
	</xsl:call-template>
</xsl:variable>

.mime-16-<xsl:value-of select="$classname"/> {
  background-image: url('<xsl:value-of select="$theme_uri"/>/images/16x16/<xsl:value-of select="$section"/>/<xsl:value-of select="class"/>.png');
}
.mime-48-<xsl:value-of select="$classname"/> {
  background-image: url('<xsl:value-of select="$theme_uri"/>/images/48x48/<xsl:value-of select="$section"/>/<xsl:value-of select="class"/>.png');
}
.mime-128-<xsl:value-of select="$classname"/> {
  background-image: url('<xsl:value-of select="$theme_uri"/>/images/128x128/<xsl:value-of select="$section"/>/<xsl:value-of select="class"/>.png');
}
</xsl:template>

<!-- Convert class to a CSS-compliant class name -->
<xsl:template name="ConvertClass">
	<xsl:param name="string"/>
	<xsl:param name="from">.</xsl:param>
	<xsl:param name="to">-</xsl:param>
	<xsl:choose>
		<xsl:when test="contains($string, $from)">
			<xsl:value-of select="substring-before($string, $from)"/>
			<xsl:copy-of select="$to"/>
			<xsl:call-template name="ConvertClass">
				<xsl:with-param name="string" select="substring-after($string, $from)" />
				<xsl:with-param name="from" select="$from" />
				<xsl:with-param name="to" select="$to" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$string" />
		</xsl:otherwise>
	</xsl:choose>
 </xsl:template>

</xsl:stylesheet>
