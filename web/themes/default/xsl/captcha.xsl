<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<!-- ===============================
	Captcha
	================================ -->

<xsl:template name="Captcha">
	<xsl:param name="id"/>
	<xsl:param name="image"/>
	<xsl:param name="error">0</xsl:param>
		<div class="captcha">
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext ('Verification code')"/></legend>
				<p>
					<xsl:if test="$error=1">
						<xsl:attribute name="class">error</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="mioga:gettext ('Please type the verification code in the box below.')"/>
				</p>
				<input type="hidden" name="captcha_id">
					<xsl:attribute name="value">
						<xsl:value-of select="$id"/>
					</xsl:attribute>
				</input>
				<div class="form-item">
					<label for="captcha">
						<img>
							<xsl:attribute name="src">GetCaptcha?hash=<xsl:value-of select="$id"/></xsl:attribute>
						</img>
					</label>
					<input name="captcha" class="miogaInput"/>
				</div>
			</fieldset>
		</div>
</xsl:template>

</xsl:stylesheet>
