<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:output method="html"/>

<xsl:include href="organizer_color.xsl"/>


<!-- *********************************************************************** -->
<!-- * Day-name                                                            * -->
<!-- *********************************************************************** -->
<xsl:template name="Day-name">
   <xsl:param name="day_number"/>
   <xsl:param name="short" select="0"/>
   <xsl:variable name="day_name">
     <xsl:call-template name="dayName">
       <xsl:with-param name="no" select="$day_number"/>
     </xsl:call-template>
   </xsl:variable>
   <xsl:variable name="day_print">
      <xsl:choose>
         <xsl:when test="number($short) != 0">
            <xsl:value-of select="substring($day_name,1,$short)"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$day_name"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
   <xsl:value-of select="$day_print"/>
</xsl:template>


<!-- *********************************************************************** -->
<!-- * Task-* for Create* / Edit*                                          * -->
<!-- *********************************************************************** -->
<!-- ======================================================================= -->
<!-- * Task-contact                                                        * -->
<!-- ======================================================================= -->
<xsl:template name="Task-contact">
   <xsl:param name="param_firstname" />
   <xsl:param name="value_firstname" />
   <xsl:param name="param_lastname" />
   <xsl:param name="value_lastname" />
   <xsl:param name="param_phone" />
   <xsl:param name="value_phone" />
   <xsl:param name="param_fax" />
   <xsl:param name="value_fax" />
   <xsl:param name="param_email" />
   <xsl:param name="value_email" />
   <xsl:param name="display_icon">0</xsl:param>
   <xsl:param name="read_only">0</xsl:param>
   <table border="0" width="100%"  class="{$mioga-title-color}"><tr>
   <td align="center" width="40%"><font class="{$mioga-title-color}">
   <xsl:call-template name="obTranslation">
     <xsl:with-param name="type">possible-contact</xsl:with-param>
   </xsl:call-template>
   </font></td>
   <td align="left"   width="60%">
     <xsl:if test="$display_icon = 1">
       <xsl:call-template name="search-form-button">
         <xsl:with-param name="name">contact</xsl:with-param>
       </xsl:call-template>
     </xsl:if>
   </td>
      </tr><tr>
         <td align="center"><font class="{$mioga-title-color}">
         <xsl:call-template name="obTranslation">
           <xsl:with-param name="type">first-name</xsl:with-param>
         </xsl:call-template>
         </font></td>
         <td>
            <xsl:choose>
               <xsl:when test="$read_only = 1">
                  <xsl:value-of select="$value_firstname"/>
               </xsl:when>
               <xsl:otherwise>
                  <input type="text" name="{$param_firstname}" value="{$value_firstname}" size="20" maxlength="30"/>
               </xsl:otherwise>
            </xsl:choose>
         </td>
      </tr><tr>
         <td align="center"><font class="{$mioga-title-color}">
         <xsl:call-template name="obTranslation">
           <xsl:with-param name="type">last-name</xsl:with-param>
         </xsl:call-template>
         </font></td>
         <td>
            <xsl:choose>
               <xsl:when test="$read_only = 1">
                  <xsl:value-of select="$value_lastname"/>
               </xsl:when>
               <xsl:otherwise>
                  <input type="text" name="{$param_lastname}" value="{$value_lastname}" size="20" maxlength="30"/>
               </xsl:otherwise>
            </xsl:choose>
         </td>
      </tr><tr>
         <td align="center"><font class="{$mioga-title-color}">
         <xsl:call-template name="obTranslation">
           <xsl:with-param name="type">phone</xsl:with-param>
         </xsl:call-template>
         </font></td>
         <td>
            <xsl:choose>
               <xsl:when test="$read_only = 1">
                  <xsl:value-of select="$value_phone"/>
               </xsl:when>
               <xsl:otherwise>
                  <input type="text" name="{$param_phone}" value="{$value_phone}" size="20" maxlength="30"/>
               </xsl:otherwise>
            </xsl:choose>
         </td>
      </tr><tr>
         <td align="center"><font class="{$mioga-title-color}">
         <xsl:call-template name="obTranslation">
           <xsl:with-param name="type">fax</xsl:with-param>
         </xsl:call-template>
         </font></td>
         <td>
            <xsl:choose>
               <xsl:when test="$read_only = 1">
                  <xsl:value-of select="$value_fax"/>
               </xsl:when>
               <xsl:otherwise>
                  <input type="text" name="{$param_fax}" value="{$value_fax}" size="20" maxlength="30"/>
               </xsl:otherwise>
            </xsl:choose>
         </td>
      </tr><tr>
         <td align="center"><font class="{$mioga-title-color}">
         <xsl:call-template name="obTranslation">
           <xsl:with-param name="type">mail</xsl:with-param>
         </xsl:call-template>
         </font></td>
         <td>
            <xsl:choose>
               <xsl:when test="$read_only = 1">
                  <xsl:value-of select="$value_email"/>
               </xsl:when>
               <xsl:otherwise>
                  <input type="text" name="{$param_email}" value="{$value_email}" size="20" maxlength="40"/>
               </xsl:otherwise>
            </xsl:choose>
         </td>
   </tr></table>
</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-time                                                           * -->
<!-- ======================================================================= -->
<xsl:template name="Task-time">
   <xsl:param name="value_hour"/>
   <xsl:param name="param_hour"/>
   <xsl:param name="value_minute"/>
   <xsl:param name="param_minute"/>
   <xsl:param name="autocommit"/>
   <xsl:param name="can_be_empty">0</xsl:param>

   <select name="{$param_hour}">
       <xsl:if test="$autocommit">
           <xsl:attribute name="onchange">submit('<xsl:value-of select="$autocommit"/>')</xsl:attribute>
       </xsl:if>

       <xsl:if test="$can_be_empty=1">
           <option value="">
               <xsl:if test="$value_hour=''">
                   <xsl:attribute name="selected">selected</xsl:attribute>
               </xsl:if>
           </option>
       </xsl:if>

       <xsl:call-template name="for-loop">
           <xsl:with-param name="start"      select="0"/>
           <xsl:with-param name="stop"       select="23"/>
           <xsl:with-param name="incr"       select="1"/>
           <xsl:with-param name="traitement" select="'time-select-option'"/>
           <xsl:with-param name="argument"   select="$value_hour"/>
       </xsl:call-template>
   </select>&#160;
   <xsl:call-template name="obTranslation">
     <xsl:with-param name="type">hour-abbr</xsl:with-param>
   </xsl:call-template>&#160;
   <select name="{$param_minute}" width="2">
       <xsl:if test="$autocommit">
           <xsl:attribute name="onchange">submit('<xsl:value-of select="$autocommit"/>')</xsl:attribute>
       </xsl:if>

         <xsl:if test="$can_be_empty=1">
           <option value="">
               <xsl:if test="$value_minute=''">
                   <xsl:attribute name="selected">selected</xsl:attribute>
               </xsl:if>
           </option>
       </xsl:if>

       <xsl:call-template name="for-loop">
         <xsl:with-param name="start"      select="0"/>
         <xsl:with-param name="stop"       select="55"/>
         <xsl:with-param name="incr"       select="5"/>
         <xsl:with-param name="traitement" select="'time-select-option'"/>
         <xsl:with-param name="argument"   select="$value_minute"/>
      </xsl:call-template>
   </select>
</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-day-month-year                                                 * -->
<!-- ======================================================================= -->
<xsl:template name="Task-day-month-year">
   <xsl:param name="maybe_empty" select="0"/>
   <xsl:param name="value_day"/>
   <xsl:param name="param_day"/>
   <xsl:param name="value_month"/>
   <xsl:param name="param_month"/>
   <xsl:param name="value_year"/>
   <xsl:param name="param_year"/>
   <xsl:param name="value_startYear"/>
   <xsl:param name="value_stopYear"/>

   <xsl:call-template name="Task-day">
      <xsl:with-param name="value_day"       select="$value_day"/>
      <xsl:with-param name="param_day"       select="$param_day"/>
      <xsl:with-param name="maybe_empty"     select="$maybe_empty"/>
   </xsl:call-template>
   <xsl:call-template name="Task-month">
      <xsl:with-param name="value_month"     select="$value_month"/>
      <xsl:with-param name="param_month"     select="$param_month"/>
      <xsl:with-param name="maybe_empty"     select="$maybe_empty"/>
   </xsl:call-template>
   <xsl:call-template name="Task-year">
      <xsl:with-param name="value_year"      select="$value_year"/>
      <xsl:with-param name="param_year"      select="$param_year"/>
      <xsl:with-param name="value_startYear" select="$value_startYear"/>
      <xsl:with-param name="value_stopYear"  select="$value_stopYear"/>
      <xsl:with-param name="maybe_empty"     select="$maybe_empty"/>
   </xsl:call-template>
</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-day-month-year-string                                          * -->
<!-- ======================================================================= -->
<xsl:template name="Task-day-month-year-string">
   <xsl:param name="value_day"/>
   <xsl:param name="value_month"/>
   <xsl:param name="value_year"/>

   <xsl:choose>
      <xsl:when test="$value_day = 0 or $value_month = 0 or $value_year = 0">
         <xsl:call-template name="obTranslation">
           <xsl:with-param name="type">no-limit</xsl:with-param>
         </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
         <xsl:value-of select="$value_day" />
         &#160;
         <xsl:call-template name="Month-name">
            <xsl:with-param name="month_number" select="$value_month"/>
         </xsl:call-template>
         &#160;
         <xsl:value-of select="$value_year" />
      </xsl:otherwise>
  </xsl:choose>

</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-day-month                                                      * -->
<!-- ======================================================================= -->
<xsl:template name="Task-day-month">
   <xsl:param name="value_day"/>
   <xsl:param name="param_day"/>
   <xsl:param name="value_month"/>
   <xsl:param name="param_month"/>
   <xsl:call-template name="Task-day">
      <xsl:with-param name="value_day"     select="$value_day"/>
      <xsl:with-param name="param_day"     select="$param_day"/>
   </xsl:call-template>
   <xsl:call-template name="Task-month">
      <xsl:with-param name="value_month"     select="$value_month"/>
      <xsl:with-param name="param_month"     select="$param_month"/>
   </xsl:call-template>
</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-day                                                            * -->
<!-- ======================================================================= -->
<xsl:template name="Task-day">
   <xsl:param name="value_day"/>
   <xsl:param name="param_day"/>
   <xsl:param name="maybe_empty" select="0" />

   <select name="{$param_day}">
      <xsl:if test="$maybe_empty = 1">
          <xsl:element name="option">
             <xsl:attribute name="value">0</xsl:attribute>
             <xsl:if test="$value_day = 0"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
          </xsl:element>
      </xsl:if>
      <xsl:call-template name="for-loop">
         <xsl:with-param name="start"      select="1"/>
         <xsl:with-param name="stop"       select="31"/>
         <xsl:with-param name="incr"       select="1"/>
         <xsl:with-param name="traitement" select="'time-select-option'"/>
         <xsl:with-param name="argument"   select="$value_day"/>
      </xsl:call-template>
   </select>
</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-year                                                           * -->
<!-- ======================================================================= -->
<xsl:template name="Task-year">
   <xsl:param name="value_year"/>
   <xsl:param name="param_year"/>
   <xsl:param name="value_startYear"/>
   <xsl:param name="value_stopYear"/>
   <xsl:param name="maybe_empty" select="0" />

   <select name="{$param_year}">
      <xsl:if test="$maybe_empty = 1">
          <xsl:element name="option">
             <xsl:attribute name="value">0</xsl:attribute>
             <xsl:if test="$value_year = 0"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
          </xsl:element>
      </xsl:if>
      <xsl:call-template name="for-loop">
         <xsl:with-param name="start"      select="$value_startYear"/>
         <xsl:with-param name="stop"       select="$value_stopYear"/>
         <xsl:with-param name="incr"       select="1"/>
         <xsl:with-param name="traitement" select="'time-select-option'"/>
         <xsl:with-param name="argument"   select="$value_year"/>
      </xsl:call-template>
   </select>
</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-month                                                          * -->
<!-- ======================================================================= -->
<xsl:template name="Task-month">
   <xsl:param name="value_month"/>
   <xsl:param name="param_month"/>
   <xsl:param name="maybe_empty" select="0" />

   <select name="{$param_month}">
      <xsl:if test="$maybe_empty = 1">
          <xsl:element name="option">
             <xsl:attribute name="value">0</xsl:attribute>
             <xsl:if test="$value_month = 0"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
          </xsl:element>
      </xsl:if>
      <xsl:call-template name="Task-month-loop">
         <xsl:with-param name="argument"   select="$value_month"/>
      </xsl:call-template>
   </select>
</xsl:template>

<xsl:template name="Task-month-loop">
   <xsl:param name="start"      select="1"/>
   <xsl:param name="stop"       select="12"/>
   <xsl:param name="argument"/>
   <xsl:if test="$start &lt;= $stop">
      <option value="{$start}">
         <xsl:if test="$start = $argument"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
         <xsl:call-template name="Month-name"><xsl:with-param name="month_number" select="$start"/></xsl:call-template>
      </option>
      <xsl:call-template name="Task-month-loop">
         <xsl:with-param name="start"      select="$start + 1"/>
         <xsl:with-param name="stop"       select="$stop"/>
         <xsl:with-param name="argument"   select="$argument"/>
      </xsl:call-template>
   </xsl:if>
</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-priority                                                       * -->
<!-- ======================================================================= -->
<xsl:template name="Task-priority">
   <xsl:param name="value_priority"/>
   <xsl:param name="param_priority"/>

   <select name="{$param_priority}">
      <xsl:element name="option">
         <xsl:attribute name="value">very urgent</xsl:attribute>
         <xsl:if test="$value_priority = 'very urgent'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
         <xsl:call-template name="Task-priority-name"><xsl:with-param name="task_priority">very urgent</xsl:with-param></xsl:call-template>
      </xsl:element>
      <xsl:element name="option">
         <xsl:attribute name="value">urgent</xsl:attribute>
         <xsl:if test="$value_priority = 'urgent'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
         <xsl:call-template name="Task-priority-name"><xsl:with-param name="task_priority">urgent</xsl:with-param></xsl:call-template>
      </xsl:element>
      <xsl:element name="option">
         <xsl:attribute name="value">normal</xsl:attribute>
         <xsl:if test="$value_priority = 'normal'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
         <xsl:call-template name="Task-priority-name"><xsl:with-param name="task_priority">normal</xsl:with-param></xsl:call-template>
      </xsl:element>
      <xsl:element name="option">
         <xsl:attribute name="value">not urgent</xsl:attribute>
         <xsl:if test="$value_priority = 'not urgent'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
         <xsl:call-template name="Task-priority-name"><xsl:with-param name="task_priority">not urgent</xsl:with-param></xsl:call-template>
      </xsl:element>
   </select>
</xsl:template>


<!-- ======================================================================= -->
<!-- * Task-Priority-Form                                                   * -->
<!-- ======================================================================= -->
<xsl:template name="Task-Priority-Form">
    <xsl:param name="label"/>
    <xsl:param name="value"/>
    <xsl:param name="action"/>

    <xsl:call-template name="MiogaFormInputMisc">
        <xsl:with-param name="label" select="$label"/>
        <xsl:with-param name="content">
                
            <xsl:call-template name="Task-priority">
                <xsl:with-param name="value_priority" select="$value"/>
                <xsl:with-param name="param_priority">priority</xsl:with-param>
            </xsl:call-template>
        </xsl:with-param>
    </xsl:call-template>

</xsl:template>


<xsl:template name="Task-Change-Priority-Form">
    <xsl:param name="label"/>
    <xsl:param name="value"/>
    <xsl:param name="action"/>

    <xsl:call-template name="MiogaFormInputMisc">
        <xsl:with-param name="label" select="$label"/>
        <xsl:with-param name="content">
                
            <table xsl:use-attribute-sets="mioga-empty-table">
                <tr>
                    <td>
                        <xsl:call-template name="Task-priority">
                            <xsl:with-param name="value_priority" select="$value"/>
                            <xsl:with-param name="param_priority">priority</xsl:with-param>
                        </xsl:call-template>
                    </td>
                    <td>&#160;</td>
                    <td>
                        <xsl:call-template name="change-form-button">
                            <xsl:with-param name="name" select="$action"/>
                        </xsl:call-template>
                    </td>
                </tr>
            </table>
        </xsl:with-param>
    </xsl:call-template>

</xsl:template>


<!-- ======================================================================= -->
<!-- * Task-show-date                                                      * -->
<!-- ======================================================================= -->
<xsl:template name="Task-show-date" >
    <xsl:param name="day" />
    <xsl:param name="month" />
    <xsl:param name="year" />

    <xsl:value-of select="format-number($day, '00')" />
    &#160;
    <xsl:call-template name="Month-name">
        <xsl:with-param name="month_number" select="$month"/>
    </xsl:call-template>
    &#160;
    <xsl:value-of select="format-number($year, '0000')" />
</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-show-time                                                      * -->
<!-- ======================================================================= -->
<xsl:template name="Task-show-time" >
  <xsl:param name="hour"/>
  <xsl:param name="minute"/>
  
  <xsl:value-of select="format-number($hour, '00')"/>
  <xsl:call-template name="obTranslation">
    <xsl:with-param name="type">short-hour-label</xsl:with-param>
  </xsl:call-template>
  <xsl:value-of select="format-number($minute, '00')"/>            
</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-Periodic-Form                                                  * -->
<!-- ======================================================================= -->
<xsl:template name="Task-Periodic-Form-Widget-Body">
    <xsl:variable name="nbcol">4</xsl:variable>

    <table border="0" cellspacing="2" cellpadding="2">
        <tr>
            <td>
                <input type="radio" name="periodType" value="planned">
                    <xsl:if test="@periodType = 'planned'">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                    </xsl:if>
                </input>
            </td>
            
            <td colspan="{$nbcol - 2}">
                <xsl:call-template name="obTranslation">
                    <xsl:with-param name="type">unique</xsl:with-param>
                </xsl:call-template>
            </td>
        </tr>

        <tr>
            <td>
                <input type="radio" name="periodType" value="day">
                    <xsl:if test="@periodType = 'day'">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                    </xsl:if>
                </input>
            </td>
            
            <td>
                <xsl:call-template name="obTranslation">
                    <xsl:with-param name="type">every-dayly</xsl:with-param>
                </xsl:call-template>
            </td>
            
            <td>
                <select name="dayFreq">
                    <xsl:call-template name="for-loop">
                        <xsl:with-param name="start">1</xsl:with-param>
                        <xsl:with-param name="stop">6</xsl:with-param>
                        <xsl:with-param name="incr">1</xsl:with-param>
                        <xsl:with-param name="traitement">time-select-option</xsl:with-param>
                        <xsl:with-param name="argument"   select="@dayFreq"/>
                    </xsl:call-template>
                </select>
            </td>

            <td>
                <xsl:call-template name="obTranslation">
                    <xsl:with-param name="type">days</xsl:with-param>
                </xsl:call-template>
            </td>
        </tr>

        <tr>
            <td>
                <input type="radio" name="periodType" value="week">
                    <xsl:if test="@periodType = 'week'">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                    </xsl:if>
                </input>
            </td>
            
            <td>
                <xsl:call-template name="obTranslation">
                    <xsl:with-param name="type">every-weekly</xsl:with-param>
                </xsl:call-template>
            </td>
            
            <td>
                <select name="weekFreq">
                    <xsl:call-template name="for-loop">
                        <xsl:with-param name="start">1</xsl:with-param>
                        <xsl:with-param name="stop">10</xsl:with-param>
                        <xsl:with-param name="incr">1</xsl:with-param>
                        <xsl:with-param name="traitement">time-select-option</xsl:with-param>
                        <xsl:with-param name="argument"   select="@weekFreq"/>
                    </xsl:call-template>
                </select>
            </td>

            <td>
                <table cellpadding="3" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <xsl:call-template name="obTranslation">
                                <xsl:with-param name="type">weekly-day-transition</xsl:with-param>
                            </xsl:call-template>
                        </td>
                        
                        <td>
                            <xsl:call-template name="Task-weekday">
                                <xsl:with-param name="param_weekday">weekWeekDay</xsl:with-param>
                                <xsl:with-param name="value_weekday" select="@weekWeekDay"/>
                            </xsl:call-template>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <input type="radio" name="periodType" value="month">
                    <xsl:if test="@periodType = 'month'">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                    </xsl:if>
                </input>
            </td>
            
            <td>
                <xsl:call-template name="obTranslation">
                    <xsl:with-param name="type">every-monthly</xsl:with-param>
                </xsl:call-template>
            </td>
            
            <td>
                <select name="monthFreq">
                    <xsl:call-template name="for-loop">
                        <xsl:with-param name="start">1</xsl:with-param>
                        <xsl:with-param name="stop">11</xsl:with-param>
                        <xsl:with-param name="incr">1</xsl:with-param>
                        <xsl:with-param name="traitement">time-select-option</xsl:with-param>
                        <xsl:with-param name="argument"   select="@monthFreq"/>
                    </xsl:call-template>
                </select>
            </td>

            <td>
                <xsl:call-template name="obTranslation">
                    <xsl:with-param name="type">months</xsl:with-param>
                </xsl:call-template>
            </td>
        </tr>
        <tr>
            <td colspan="{$nbcol}">
                <table cellpadding="0" cellspacing="2" border="0">
                    <tr>
                        <td><img src="{$image_uri}/transparent_fill.gif" width="30" height="1" /></td>
                        <td>
                            <input type="radio" name="monthPeriodType" value="day">
                                <xsl:if test="@monthPeriodType = 'day'">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input>
                        </td>
                        <td>
                            <xsl:call-template name="obTranslation">
                                <xsl:with-param name="type">the</xsl:with-param>
                            </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="Task-day">
                                <xsl:with-param name="param_day">monthDay</xsl:with-param>
                                <xsl:with-param name="value_day" select="@monthDay" />
                            </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="obTranslation">
                                <xsl:with-param name="type">numbered-day-monthly</xsl:with-param>
                            </xsl:call-template>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="{$nbcol}">
                <table cellpadding="0" cellspacing="2" border="0">
                    <tr>
                        <td><img src="{$image_uri}/transparent_fill.gif" width="30" height="1" /></td>
                        <td>
                            <input type="radio" name="monthPeriodType" value="week">
                                <xsl:if test="@monthPeriodType = 'week'">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                </xsl:if>
                            </input>
                        </td>
                        <td>
                            <xsl:call-template name="obTranslation">
                                <xsl:with-param name="type">the</xsl:with-param>
                            </xsl:call-template>
                        </td>
                        <td>
                            <select name="monthWeek">
                                <option value="1">
                                    <xsl:if test="@monthWeek = 1"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
                                    <xsl:call-template name="obTranslation">
                                        <xsl:with-param name="type">first</xsl:with-param>
                                    </xsl:call-template>
                                </option>
                                
                                <option value="2">
                                    <xsl:if test="@monthWeek = 2"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
                                    <xsl:call-template name="obTranslation">
                                        <xsl:with-param name="type">second</xsl:with-param>
                                    </xsl:call-template>
                                </option>
                                
                                <option value="3">
                                    <xsl:if test="@monthWeek = 3"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
                                    <xsl:call-template name="obTranslation">
                                        <xsl:with-param name="type">third</xsl:with-param>
                                    </xsl:call-template>
                                </option>   
                                
                                <option value="4">
                                    <xsl:if test="@monthWeek = 4"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
                                    <xsl:call-template name="obTranslation">
                                        <xsl:with-param name="type">fourth</xsl:with-param>
                                    </xsl:call-template>
                                </option>
                                
                                <option value="-1">
                                    <xsl:if test="@monthWeek = -1"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
                                    <xsl:call-template name="obTranslation">
                                        <xsl:with-param name="type">last</xsl:with-param>
                                    </xsl:call-template>
                                </option>
                            </select>
                        </td>
                        <td>
                            <xsl:call-template name="Task-weekday">
                                <xsl:with-param name="param_weekday">monthWeekDay</xsl:with-param>
                                <xsl:with-param name="value_weekday" select="@monthWeekDay" />
                            </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="obTranslation">
                                <xsl:with-param name="type">named-day-monthly</xsl:with-param>
                            </xsl:call-template>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        

        <tr>
            <td>
                <input type="radio" name="periodType" value="year">
                    <xsl:if test="@periodType = 'year'">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                    </xsl:if>
                </input>
            </td>
            
            <td>
                <xsl:call-template name="obTranslation">
                    <xsl:with-param name="type">every-year</xsl:with-param>
                </xsl:call-template>
            </td>
            
            <td>
                <select name="yearFreq">
                    <xsl:call-template name="for-loop">
                        <xsl:with-param name="start">1</xsl:with-param>
                        <xsl:with-param name="stop">5</xsl:with-param>
                        <xsl:with-param name="incr">1</xsl:with-param>
                        <xsl:with-param name="traitement">time-select-option</xsl:with-param>
                        <xsl:with-param name="argument"   select="@yearFreq"/>
                    </xsl:call-template>
                </select>
            </td>
            <td>
                <table cellpadding="3" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <xsl:call-template name="obTranslation">
                                <xsl:with-param name="type">yearly-transition</xsl:with-param>
                            </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="Task-day-month">
                                <xsl:with-param name="param_day">yearDay</xsl:with-param>
                                <xsl:with-param name="value_day" select="@yearDay" />
                                <xsl:with-param name="param_month">yearMonth</xsl:with-param>
                                <xsl:with-param name="value_month" select="@yearMonth" />
                            </xsl:call-template>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</xsl:template>

<xsl:template name="Task-Periodic-Form">
    <xsl:call-template name="MiogaFormInputMisc">
        <xsl:with-param name="label">periodic</xsl:with-param>
        <xsl:with-param name="content">
            <xsl:call-template name="Task-Periodic-Form-Widget-Body"/>
        </xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormTinyHorizSep"/>

    <xsl:call-template name="MiogaFormInputDate">
        <xsl:with-param name="label">end-date</xsl:with-param>

        <xsl:with-param name="year-value"  select="@endYear"/>
        <xsl:with-param name="month-value" select="@endMonth"/>
        <xsl:with-param name="day-value"   select="@endDay"/>
        <xsl:with-param name="year-name">endYear</xsl:with-param>
        <xsl:with-param name="month-name">endMonth</xsl:with-param>
        <xsl:with-param name="day-name">endDay</xsl:with-param>
        <xsl:with-param name="first-year"  select="@startYear"/>
        <xsl:with-param name="nb-year"     select="@stopYear - @startYear"/>
        <xsl:with-param name="can-be-empty">1</xsl:with-param>
        
    </xsl:call-template>
</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-periodic                                                       * -->
<!-- ======================================================================= -->
<xsl:template name="Task-periodic" >
    <xsl:param name="param_periodType"      select = "'periodType'" />
    <xsl:param name="value_periodType" />
    <xsl:param name="param_endDay"          select = "'endDay'" />
    <xsl:param name="value_endDay" />
    <xsl:param name="param_endMonth"        select = "'endMonth'" />
    <xsl:param name="value_endMonth" />
    <xsl:param name="param_endYear"         select = "'endYear'" />
    <xsl:param name="value_endYear" />
    <xsl:param name="param_dayFreq"         select = "'dayFreq'" />
    <xsl:param name="value_dayFreq" />
    <xsl:param name="param_weekFreq"        select = "'weekFreq'" />
    <xsl:param name="value_weekFreq" />
    <xsl:param name="param_weekWeekDay"     select = "'weekWeekDay'" />
    <xsl:param name="value_weekWeekDay" />
    <xsl:param name="param_monthFreq"       select = "'monthFreq'" />
    <xsl:param name="value_monthFreq" />
    <xsl:param name="param_monthPeriodType" select = "'monthPeriodType'" />
    <xsl:param name="value_monthPeriodType" />
    <xsl:param name="param_monthDay"        select = "'monthDay'" />
    <xsl:param name="value_monthDay" />
    <xsl:param name="param_monthWeek"       select = "'monthWeek'" />
    <xsl:param name="value_monthWeek" />
    <xsl:param name="param_monthWeekDay"    select = "'monthWeekDay'" />
    <xsl:param name="value_monthWeekDay" />
    <xsl:param name="param_yearFreq"        select = "'yearFreq'" />
    <xsl:param name="value_yearFreq" />
    <xsl:param name="param_yearDay"         select = "'yearDay'" />
    <xsl:param name="value_yearDay" />
    <xsl:param name="param_yearMonth"       select = "'yearMonth'" />
    <xsl:param name="value_yearMonth" />
    <xsl:param name="strictBool"            select = "0"/>

    <table border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td align="center" valign="top" >
      <br/>
      <font class="{$mioga-title-color}">
         <xsl:call-template name="obTranslation">
           <xsl:with-param name="type">periodicity</xsl:with-param>
         </xsl:call-template>
      </font>
    </td>
    <td>
      <xsl:if test="$strictBool = 1">
        <table border="0"><tr>
        <td>
          <table><tr>
          <td>
            <xsl:element name="input">
              <xsl:attribute name="type">radio</xsl:attribute>
              <xsl:attribute name="name"><xsl:value-of select="$param_periodType" /></xsl:attribute>
              <xsl:attribute name="value">planned</xsl:attribute>
              <xsl:if test="$value_periodType='planned'"><xsl:attribute name="checked">1</xsl:attribute></xsl:if>
            </xsl:element>
          </td>
          <td class="{$mioga-title-color}">
         <xsl:call-template name="obTranslation">
           <xsl:with-param name="type">unique</xsl:with-param>
         </xsl:call-template>
       </td>
        </tr></table>
             </td>
          </tr></table>
       </xsl:if>
       <table border="0"><tr>
          <td>
            <table><tr>
              <td>
                 <xsl:element name="input">
                    <xsl:attribute name="type">radio</xsl:attribute>
                    <xsl:attribute name="name"><xsl:value-of select="$param_periodType" /></xsl:attribute>
                    <xsl:attribute name="value">day</xsl:attribute>
                    <xsl:if test="$value_periodType='day'"><xsl:attribute name="checked">1</xsl:attribute></xsl:if>
                 </xsl:element>
              </td>
              <td class="{$mioga-title-color}">
                <xsl:call-template name="obTranslation">
                  <xsl:with-param name="type">every-dayly</xsl:with-param>
                </xsl:call-template>
              </td>
              <td>
              <select name="{$param_dayFreq}">
                  <xsl:call-template name="for-loop">
                     <xsl:with-param name="start"      select="1"/>
                     <xsl:with-param name="stop"       select="6"/>
                     <xsl:with-param name="incr"       select="1"/>
                     <xsl:with-param name="traitement" select="'time-select-option'"/>
                     <xsl:with-param name="argument"   select="$value_dayFreq"/>
                  </xsl:call-template>
              </select>
              </td>
              <td class="{$mioga-title-color}">
                <xsl:call-template name="obTranslation">
                  <xsl:with-param name="type">days</xsl:with-param>
                </xsl:call-template>
              </td>
           </tr></table>
          </td>
       </tr></table>
       <table border="0"><tr>
          <td>
            <table><tr>
              <td>
                 <xsl:element name="input">
                    <xsl:attribute name="type">radio</xsl:attribute>
                    <xsl:attribute name="name"><xsl:value-of select="$param_periodType" /></xsl:attribute>
                    <xsl:attribute name="value">week</xsl:attribute>
                    <xsl:if test="$value_periodType = 'week'"><xsl:attribute name="checked">1</xsl:attribute></xsl:if>
                 </xsl:element>
              </td>
              <td class="{$mioga-title-color}">
                <xsl:call-template name="obTranslation">
                  <xsl:with-param name="type">every-weekly</xsl:with-param>
                </xsl:call-template>
              </td>
              <td>
                  <select name="{$param_weekFreq}">
                      <xsl:call-template name="for-loop">
                         <xsl:with-param name="start"      select="1"/>
                         <xsl:with-param name="stop"       select="52"/>
                         <xsl:with-param name="incr"       select="1"/>
                         <xsl:with-param name="traitement" select="'time-select-option'"/>
                         <xsl:with-param name="argument"   select="$value_weekFreq"/>
                      </xsl:call-template>
                  </select>
              </td>
              <td class="{$mioga-title-color}">
                <xsl:call-template name="obTranslation">
                  <xsl:with-param name="type">weekly-day-transition</xsl:with-param>
                </xsl:call-template>
              </td>
              <td>
                <xsl:call-template name="Task-weekday">
                  <xsl:with-param name="param_weekday" select="$param_weekWeekDay"/>
                  <xsl:with-param name="value_weekday" select="$value_weekWeekDay"/>
                </xsl:call-template>
               </td>
            </tr>
          </table>
          </td>
       </tr></table>
       <table border="0"><tr>
         <td>
         <table><tr>
         <td>
           <xsl:element name="input">
              <xsl:attribute name="type">radio</xsl:attribute>
              <xsl:attribute name="name"><xsl:value-of select="$param_periodType" /></xsl:attribute>
              <xsl:attribute name="value">month</xsl:attribute>
              <xsl:if test="$value_periodType = 'month'"><xsl:attribute name="checked">1</xsl:attribute></xsl:if>
           </xsl:element>
         </td>
         <td class="{$mioga-title-color}">
           <xsl:call-template name="obTranslation">
             <xsl:with-param name="type">every-monthly</xsl:with-param>
           </xsl:call-template>
         </td>
         <td>
           <select name="{$param_monthFreq}">
               <xsl:call-template name="for-loop">
                  <xsl:with-param name="start"      select="1"/>
                  <xsl:with-param name="stop"       select="11"/>
                  <xsl:with-param name="incr"       select="1"/>
                  <xsl:with-param name="traitement" select="'time-select-option'"/>
                  <xsl:with-param name="argument"   select="$value_monthFreq"/>
               </xsl:call-template>
           </select>
         </td>
         <td class="{$mioga-title-color}">
           <xsl:call-template name="obTranslation">
             <xsl:with-param name="type">months</xsl:with-param>
           </xsl:call-template>
         </td>
         </tr></table>
         </td>
         </tr>
         <tr>
             <td colspan="3">
                 <table border="0"><tr>
                 <td><img src="{$image_uri}/transparent_fill.gif" width="30" height="1" /></td>
                   <td>
                     <xsl:element name="input">
                       <xsl:attribute name="type">radio</xsl:attribute>
                       <xsl:attribute name="name"><xsl:value-of select="$param_monthPeriodType" /></xsl:attribute>
                       <xsl:attribute name="value">day</xsl:attribute>
                       <xsl:if test="$value_monthPeriodType = 'day'"><xsl:attribute name="checked">1</xsl:attribute></xsl:if>
                     </xsl:element>
                    &#160;
                   </td>
                   <td class="{$mioga-title-color}">
                     <xsl:call-template name="obTranslation">
                       <xsl:with-param name="type">the</xsl:with-param>
                     </xsl:call-template>
                   </td>
                   <td>
                      <xsl:call-template name="Task-day">
                        <xsl:with-param name="param_day" select="$param_monthDay" />
                        <xsl:with-param name="value_day" select="$value_monthDay" />
                      </xsl:call-template>
                   </td>
                   <td class="{$mioga-title-color}">
                     <xsl:call-template name="obTranslation">
                       <xsl:with-param name="type">numbered-day-monthly</xsl:with-param>
                     </xsl:call-template>
                   </td>
                 </tr></table>
             </td>
         </tr>
         <tr>
             <td colspan="3">
                 <table border="0"><tr>
                 <td><img src="{$image_uri}/transparent_fill.gif" width="30" height="1" /></td>
                   <td>
                    <xsl:element name="input">
                       <xsl:attribute name="type">radio</xsl:attribute>
                       <xsl:attribute name="name"><xsl:value-of select="$param_monthPeriodType" /></xsl:attribute>
                       <xsl:attribute name="value">week</xsl:attribute>
                       <xsl:if test="$value_monthPeriodType='week'"><xsl:attribute name="checked">1</xsl:attribute></xsl:if>
                    </xsl:element>
                    &#160;
                   </td>
                   <td class="{$mioga-title-color}">
                     <xsl:call-template name="obTranslation">
                       <xsl:with-param name="type">the</xsl:with-param>
                     </xsl:call-template>
                   </td>
                   <td>
                     <select name="{$param_monthWeek}">
   
                      <xsl:element name="option">
                         <xsl:attribute name="value">1</xsl:attribute>
                         <xsl:if test="$value_monthWeek = 1"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
                         <xsl:call-template name="obTranslation">
                           <xsl:with-param name="type">first</xsl:with-param>
                         </xsl:call-template>
                      </xsl:element>
   
                      <xsl:element name="option">
                         <xsl:attribute name="value">2</xsl:attribute>
                         <xsl:if test="$value_monthWeek = 2"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
                         <xsl:call-template name="obTranslation">
                           <xsl:with-param name="type">second</xsl:with-param>
                         </xsl:call-template>
                      </xsl:element>
   
                      <xsl:element name="option">
                         <xsl:attribute name="value">3</xsl:attribute>
                         <xsl:if test="$value_monthWeek = 3"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
                         <xsl:call-template name="obTranslation">
                           <xsl:with-param name="type">third</xsl:with-param>
                         </xsl:call-template>
                      </xsl:element>
   
                      <xsl:element name="option">
                         <xsl:attribute name="value">4</xsl:attribute>
                         <xsl:if test="$value_monthWeek = 4"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
                         <xsl:call-template name="obTranslation">
                           <xsl:with-param name="type">fourth</xsl:with-param>
                         </xsl:call-template>
                      </xsl:element>
   
                      <xsl:element name="option">
                         <xsl:attribute name="value">-1</xsl:attribute>
                         <xsl:if test="$value_monthWeek = -1"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
                         <xsl:call-template name="obTranslation">
                           <xsl:with-param name="type">last</xsl:with-param>
                         </xsl:call-template>
                      </xsl:element>
                     </select>
                   </td>
                   <td>
                     <xsl:call-template name="Task-weekday">
                         <xsl:with-param name="param_weekday" select="$param_monthWeekDay" />
                         <xsl:with-param name="value_weekday" select="$value_monthWeekDay" />
                     </xsl:call-template>
                   </td>
                   <td class="{$mioga-title-color}">
                     <xsl:call-template name="obTranslation">
                       <xsl:with-param name="type">named-day-monthly</xsl:with-param>
                     </xsl:call-template>
                   </td>
                 </tr></table>
             </td>
       </tr></table>
       <table border="0"><tr>
         <td>
          <table><tr>
              <td>
                 <xsl:element name="input">
                    <xsl:attribute name="type">radio</xsl:attribute>
                    <xsl:attribute name="name"><xsl:value-of select="$param_periodType" /></xsl:attribute>
                    <xsl:attribute name="value">year</xsl:attribute>
                    <xsl:if test="$value_periodType='year'"><xsl:attribute name="checked">1</xsl:attribute></xsl:if>
                 </xsl:element>
              </td>
              <td class="{$mioga-title-color}">
                <xsl:call-template name="obTranslation">
                  <xsl:with-param name="type">every-year</xsl:with-param>
                </xsl:call-template>
              </td>
              <td>
                  <select name="{$param_yearFreq}">
                      <xsl:call-template name="for-loop">
                         <xsl:with-param name="start"      select="1"/>
                         <xsl:with-param name="stop"       select="5"/>
                         <xsl:with-param name="incr"       select="1"/>
                         <xsl:with-param name="traitement" select="'time-select-option'"/>
                         <xsl:with-param name="argument"   select="$value_yearFreq"/>
                      </xsl:call-template>
                  </select>
              </td>
              <td class="{$mioga-title-color}">
                <xsl:call-template name="obTranslation">
                  <xsl:with-param name="type">yearly-transition</xsl:with-param>
                </xsl:call-template>
              </td>
              <td>
                  <xsl:call-template name="Task-day-month">
                    <xsl:with-param name="param_day" select="$param_yearDay" />
                    <xsl:with-param name="value_day" select="$value_yearDay" />
                    <xsl:with-param name="param_month" select="$param_yearMonth" />
                    <xsl:with-param name="value_month" select="$value_yearMonth" />
                  </xsl:call-template>
               </td>
            </tr></table>
          </td>
       </tr></table>

    </td></tr>   

    <tr><td colspan="2"><img src="{$image_uri}/transparent_fill.gif" border="0" alt="" width="1" height="3"/></td></tr>

    <tr>
       <td align="center"><font class="{$mioga-title-color}">
       <xsl:call-template name="obTranslation">
         <xsl:with-param name="type">end-date</xsl:with-param>
       </xsl:call-template>
       </font>&#160;</td>
       <td>
          <xsl:call-template name="Task-day-month-year">
             <xsl:with-param name="value_day"       select="@endDay"/>
             <xsl:with-param name="param_day"       select="'endDay'"/>
             <xsl:with-param name="value_month"     select="@endMonth"/>
             <xsl:with-param name="param_month"     select="'endMonth'"/>
             <xsl:with-param name="value_year"      select="@endYear"/>
             <xsl:with-param name="param_year"      select="'endYear'"/>
             <xsl:with-param name="value_startYear" select="@startYear"/>
             <xsl:with-param name="value_stopYear"  select="@stopYear"/>
             <xsl:with-param name="maybe_empty"     select="1"/>
          </xsl:call-template>
       </td>
    </tr>

    </table>
</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-weekday                                                        * -->
<!-- ======================================================================= -->
<xsl:template name="Task-weekday">
  <xsl:param name="param_weekday" />
  <xsl:param name="value_weekday" select="1" />
  <xsl:param name="mondayfirst" select="1" />

  <xsl:element name="select">
  <xsl:attribute name="name"><xsl:value-of select="$param_weekday" /></xsl:attribute>
    <xsl:if test="$mondayfirst != 1">
      <xsl:element name="option">
        <xsl:attribute name="value">7</xsl:attribute>
        <xsl:if test="$value_weekday = 7"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
        <xsl:call-template name="Day-name"><xsl:with-param name="day_number" select="7"/></xsl:call-template>
      </xsl:element>
    </xsl:if>
    <xsl:element name="option">
      <xsl:attribute name="value">1</xsl:attribute>
      <xsl:if test="$value_weekday = 1"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
      <xsl:call-template name="Day-name"><xsl:with-param name="day_number" select="1"/></xsl:call-template>
    </xsl:element>
    <xsl:element name="option">
      <xsl:attribute name="value">2</xsl:attribute>
      <xsl:if test="$value_weekday = 2"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
      <xsl:call-template name="Day-name"><xsl:with-param name="day_number" select="2"/></xsl:call-template>
    </xsl:element>
    <xsl:element name="option">
      <xsl:attribute name="value">3</xsl:attribute>
      <xsl:if test="$value_weekday = 3"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
      <xsl:call-template name="Day-name"><xsl:with-param name="day_number" select="3"/></xsl:call-template>
    </xsl:element>
    <xsl:element name="option">
      <xsl:attribute name="value">4</xsl:attribute>
      <xsl:if test="$value_weekday = 4"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
      <xsl:call-template name="Day-name"><xsl:with-param name="day_number" select="4"/></xsl:call-template>
    </xsl:element>
    <xsl:element name="option">
      <xsl:attribute name="value">5</xsl:attribute>
      <xsl:if test="$value_weekday = 5"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
      <xsl:call-template name="Day-name"><xsl:with-param name="day_number" select="5"/></xsl:call-template>
    </xsl:element>
    <xsl:element name="option">
      <xsl:attribute name="value">6</xsl:attribute>
      <xsl:if test="$value_weekday = 6"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
      <xsl:call-template name="Day-name"><xsl:with-param name="day_number" select="6"/></xsl:call-template>
    </xsl:element>
    <xsl:if test="$mondayfirst = 1">
      <xsl:element name="option">
        <xsl:attribute name="value">7</xsl:attribute>
        <xsl:if test="$value_weekday = 7"><xsl:attribute name="selected">1</xsl:attribute></xsl:if>
        <xsl:call-template name="Day-name"><xsl:with-param name="day_number" select="7"/></xsl:call-template>
      </xsl:element>
    </xsl:if>
  </xsl:element>
</xsl:template>


<!-- ======================================================================= -->
<!-- * integer loops                                                       * -->
<!-- ======================================================================= -->
<xsl:template name="body-loop" mode="time-select-option">
   <xsl:param name="value"/>
   <xsl:param name="argument"/>
   <xsl:variable name="strvalue">
      <xsl:choose>
         <xsl:when test="string-length($value) = 1">
            <xsl:text>0</xsl:text><xsl:value-of select="$value"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$value"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
   <xsl:choose>   
      <xsl:when test="($value = $argument) or (concat('0', $value) = $argument)">
         <option value="{$strvalue}" selected="selected"><xsl:value-of select="$strvalue"/></option>
      </xsl:when>
      <xsl:otherwise>
         <option value="{$strvalue}"><xsl:value-of select="$strvalue"/></option>
      </xsl:otherwise>
   </xsl:choose>
</xsl:template>

<xsl:template name="for-loop">
   <xsl:param name="start"      select="1"/>
   <xsl:param name="stop"       select="10"/>
   <xsl:param name="incr"       select="1"/>
   <xsl:param name="traitement" select="'default'"/>
   <xsl:param name="argument"/>
   <xsl:if test="$start &lt;= $stop">
      <xsl:call-template name="body-loop" mode="$traitement">
         <xsl:with-param name="value"    select="$start"/>
         <xsl:with-param name="argument" select="$argument"/>
      </xsl:call-template>
      <xsl:call-template name="for-loop">
         <xsl:with-param name="start"      select="$start + $incr"/>
         <xsl:with-param name="stop"       select="$stop"/>
         <xsl:with-param name="incr"       select="$incr"/>
         <xsl:with-param name="traitement" select="$traitement"/>
         <xsl:with-param name="argument"   select="$argument"/>
      </xsl:call-template>
   </xsl:if>
</xsl:template>


<!-- *********************************************************************** -->
<!-- * Calendar for View*                                                  * -->
<!-- *********************************************************************** -->
<xsl:template match="Calendar" mode="table-title">
  <xsl:variable name="next">
    <xsl:call-template name="obTranslation">
      <xsl:with-param name="type">next</xsl:with-param>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="previous">
    <xsl:call-template name="obTranslation">
      <xsl:with-param name="type">previous</xsl:with-param>
    </xsl:call-template>
  </xsl:variable>
   <td>
      <table border="0" cellspacing="0" width="100%"><tr>
         <td align="left">
            <a href="{$private_bin_uri}/Organizer/{../Calendar-previous/@method}?{../Calendar-previous/@attribute}={../Calendar-previous/@value}">
               <img src="{$image_uri}/16x16/actions/go-previous.png" border="0" alt=" {$previous} "/>
            </a>
         </td>
         <td align="center">
            <a href="{$private_bin_uri}/Organizer/ViewMonth?month={@year}-{@month}">
               <b><font class="{$mioga-list-title-text-color}">
                  <xsl:call-template name="Month-name">
                     <xsl:with-param name="month_number" select="@month"/>
                  </xsl:call-template>
                  <xsl:text disable-output-escaping="yes">&amp;nbsp&#59;</xsl:text>
                  <xsl:value-of select="@year"/>
               </font></b>
            </a>
         </td>
         <td align="right">
            <a href="{$private_bin_uri}/Organizer/{../Calendar-next/@method}?{../Calendar-next/@attribute}={../Calendar-next/@value}">
               <img src="{$image_uri}/16x16/actions/go-next.png" border="0" alt=" {$next} "/>
            </a>
         </td>
      </tr></table>
   </td>
</xsl:template>

<xsl:template match="Calendar" mode="table-body">
   <xsl:variable name="firstDay" select="@firstDay"/>
   <xsl:variable name="lastDay" select="@lastDay"/>
   <xsl:variable name="month" select="@month"/>
   <xsl:variable name="year" select="@year"/>
   <td class="{$mioga-bg-color}">
      <table cellspacing="0" cellpadding="2">
         <tr>
            <th class="{$mioga-list-even-row-bg-color}"><xsl:text disable-output-escaping="yes">&amp;nbsp&#59;</xsl:text></th>
            <xsl:for-each select="DayNames/DayName">
               <th class="{$mioga-list-even-row-bg-color}">
                  <xsl:call-template name="Day-name">
                     <xsl:with-param name="day_number" select="@dayInWeek"/>
                     <xsl:with-param name="short" select="2"/>
                  </xsl:call-template>
               </th>
            </xsl:for-each>
         </tr>
         <xsl:for-each select="Week">
            <tr>
               <td class="{$mioga-list-even-row-bg-color}" align="center">
                  <a href="{$private_bin_uri}/Organizer/ViewWeek?week={@year}-{@number}">
                     <i><xsl:value-of select="@number"/></i>
                  </a>
                  <xsl:text disable-output-escaping="yes">&amp;nbsp&#59; </xsl:text>
               </td>
               <xsl:for-each select="Day">
                  <xsl:element name="td">
                     <xsl:attribute name="align">center</xsl:attribute>
                     <xsl:choose>
                        <xsl:when test="@number &gt;= $firstDay and @number &lt;= $lastDay">
                           <xsl:attribute name="class"><xsl:value-of select="$mioga-list-title-bg-color"/></xsl:attribute>
                           <a href="{$private_bin_uri}/Organizer/ViewDay?day={$year}-{$month}-{@number}">
                              <font>
                                 <xsl:choose>
                                    <xsl:when test="@isWorkingDay = 0">
                                       <xsl:attribute name="class"><xsl:value-of select="$nonworking_bgcolor"/></xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise>
                                       <xsl:attribute name="class"><xsl:value-of select="$mioga-list-title-text-color"/></xsl:attribute>
                                    </xsl:otherwise>
                                 </xsl:choose>
                                 <xsl:value-of select="@number"/>
                              </font>
                           </a>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color"/></xsl:attribute>
                           <a href="{$private_bin_uri}/Organizer/ViewDay?day={$year}-{$month}-{@number}">
                              <font>
                                 <xsl:choose>
                                    <xsl:when test="@isWorkingDay = 0">
                                       <xsl:attribute name="class"><xsl:value-of select="$nonworking_bgcolor"/></xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise/>
                                 </xsl:choose>
                                 <xsl:value-of select="@number"/>
                              </font>
                           </a>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:element>
               </xsl:for-each>
            </tr>
         </xsl:for-each>
      </table>
   </td>
</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<xsl:template name="obTranslation">
  <xsl:param name="type"/>
  
  <xsl:choose>
    <xsl:when test="$type='possible-contact'"><xsl:value-of select="mioga:gettext('Potential contact')"/></xsl:when>
    <xsl:when test="$type='first-name'"><xsl:value-of select="mioga:gettext('Firstname')"/></xsl:when>
    <xsl:when test="$type='last-name'"><xsl:value-of select="mioga:gettext('Lastname')"/></xsl:when>
    <xsl:when test="$type='phone'"><xsl:value-of select="mioga:gettext('Telephone')"/></xsl:when>
    <xsl:when test="$type='fax'"><xsl:value-of select="mioga:gettext('Fax')"/></xsl:when>
    <xsl:when test="$type='mail'"><xsl:value-of select="mioga:gettext('Email')"/></xsl:when>
    <xsl:when test="$type='no-limit'"><xsl:value-of select="mioga:gettext('No date limit')"/></xsl:when>
    <xsl:when test="$type='periodicity'"><xsl:value-of select="mioga:gettext('Periodicity')"/></xsl:when>
    <xsl:when test="$type='unique'"><xsl:value-of select="mioga:gettext('Unique')"/></xsl:when>
    <xsl:when test="$type='short-hour-label'">&#160;<xsl:value-of select="mioga:gettext('h')"/>&#160;</xsl:when>
    <xsl:when test="$type='days'"><xsl:value-of select="mioga:gettext('day')"/></xsl:when>
    <xsl:when test="$type='every-monthly'"><xsl:value-of select="mioga:gettext('Every')"/></xsl:when>
    <xsl:when test="$type='every-dayly'"><xsl:value-of select="mioga:gettext('Every')"/></xsl:when>
    <xsl:when test="$type='every-weekly'"><xsl:value-of select="mioga:gettext('Every')"/></xsl:when>
    <xsl:when test="$type='weekly-day-transition'"><xsl:value-of select="mioga:gettext('weeks on')"/></xsl:when>
    <xsl:when test="$type='months'"><xsl:value-of select="mioga:gettext('month')"/></xsl:when>
    <xsl:when test="$type='the'"><xsl:value-of select="mioga:gettext('The')"/>&#160;</xsl:when>
    <xsl:when test="$type='numbered-day-monthly'"><xsl:value-of select="mioga:gettext('of month')"/></xsl:when>
    <xsl:when test="$type='first'"><xsl:value-of select="mioga:gettext('first')"/></xsl:when>
    <xsl:when test="$type='second'"><xsl:value-of select="mioga:gettext('second')"/></xsl:when>
    <xsl:when test="$type='third'"><xsl:value-of select="mioga:gettext('third')"/></xsl:when>
    <xsl:when test="$type='fourth'"><xsl:value-of select="mioga:gettext('fourth')"/></xsl:when>
    <xsl:when test="$type='last'"><xsl:value-of select="mioga:gettext('last')"/></xsl:when>
    <xsl:when test="$type='named-day-monthly'"><xsl:value-of select="mioga:gettext('of month')"/></xsl:when>
    <xsl:when test="$type='every-year'"><xsl:value-of select="mioga:gettext('All')"/></xsl:when>
    <xsl:when test="$type='yearly-transition'"><xsl:value-of select="mioga:gettext('year on')"/></xsl:when>
    <xsl:when test="$type='end-date'"><xsl:value-of select="mioga:gettext('Potential end date')"/></xsl:when>
    <xsl:when test="$type='next'"><xsl:value-of select="mioga:gettext('Next')"/></xsl:when>
    <xsl:when test="$type='previous'"><xsl:value-of select="mioga:gettext('Previous')"/></xsl:when>
    <xsl:when test="$type='hour-abbr'"><xsl:value-of select="mioga:gettext('h')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<!-- Month-name -->
<xsl:template name="Month-name">
   <xsl:param name="month_number"/>
   <xsl:choose>
      <xsl:when test="number($month_number) = 1" ><xsl:value-of select="mioga:gettext('january')"/></xsl:when>
      <xsl:when test="number($month_number) = 2" ><xsl:value-of select="mioga:gettext('february')"/></xsl:when>
      <xsl:when test="number($month_number) = 3" ><xsl:value-of select="mioga:gettext('march')"/></xsl:when>
      <xsl:when test="number($month_number) = 4" ><xsl:value-of select="mioga:gettext('april')"/></xsl:when>
      <xsl:when test="number($month_number) = 5" ><xsl:value-of select="mioga:gettext('may')"/></xsl:when>
      <xsl:when test="number($month_number) = 6" ><xsl:value-of select="mioga:gettext('june')"/></xsl:when>
      <xsl:when test="number($month_number) = 7" ><xsl:value-of select="mioga:gettext('july')"/></xsl:when>
      <xsl:when test="number($month_number) = 8" ><xsl:value-of select="mioga:gettext('august')"/></xsl:when>
      <xsl:when test="number($month_number) = 9" ><xsl:value-of select="mioga:gettext('september')"/></xsl:when>
      <xsl:when test="number($month_number) = 10"><xsl:value-of select="mioga:gettext('october')"/></xsl:when>
      <xsl:when test="number($month_number) = 11"><xsl:value-of select="mioga:gettext('november')"/></xsl:when>
      <xsl:when test="number($month_number) = 12"><xsl:value-of select="mioga:gettext('december')"/></xsl:when>
   </xsl:choose>
</xsl:template>

<!-- Error-string -->
<xsl:template name="Error-string">
   <xsl:param name="error"/>
   <xsl:choose>
      <xsl:when test="$error='OK'">&#160;</xsl:when>

      <xsl:when test="$error='NAME_EMPTY'"            ><xsl:value-of select="mioga:gettext('You have to input a title')"/></xsl:when>
      <xsl:when test="$error='BAD_DAY_FOR_MONTH'"     ><xsl:value-of select="mioga:gettext('There are not so days for this month')"/></xsl:when>
      <xsl:when test="$error='START_TIME_AFTER'"      ><xsl:value-of select="mioga:gettext('End time have to be higher than start one')"/></xsl:when>
      <xsl:when test="$error='START_DATE_AFTER'"      ><xsl:value-of select="mioga:gettext('End date have to be at least higher than start one')"/></xsl:when>

      <xsl:when test="$error='NAME_INCORRECT'"        ><xsl:value-of select="mioga:gettext('Invalid title')"/></xsl:when>
      <xsl:when test="$error='DESCRIPTION_INCORRECT'" ><xsl:value-of select="mioga:gettext('Invalid description')"/></xsl:when>
      <xsl:when test="$error='FIRST_NAME_INCORRECT'"  ><xsl:value-of select="mioga:gettext('Invalid firstname')"/></xsl:when>
      <xsl:when test="$error='LAST_NAME_INCORRECT'"   ><xsl:value-of select="mioga:gettext('Invalid lastname')"/></xsl:when>
      <xsl:when test="$error='TELEPHONE_INCORRECT'"   ><xsl:value-of select="mioga:gettext('Invalid telephone')"/></xsl:when>
      <xsl:when test="$error='FAX_INCORRECT'"         ><xsl:value-of select="mioga:gettext('Invalid fax')"/></xsl:when>
      <xsl:when test="$error='EMAIL_INCORRECT'"       ><xsl:value-of select="mioga:gettext('Invalid email')"/></xsl:when>
      <xsl:when test="$error='START_DATE_INCORRECT'"  ><xsl:value-of select="mioga:gettext('Invalid start date')"/></xsl:when>
      <xsl:when test="$error='STOP_DATE_INCORRECT'"   ><xsl:value-of select="mioga:gettext('Invalid end date')"/></xsl:when>
      <xsl:when test="$error='CREATION_ERROR'"        ><xsl:value-of select="mioga:gettext('Creation error')"/></xsl:when>
      <xsl:when test="$error='DELETION_ERROR'"        ><xsl:value-of select="mioga:gettext('Deletion error')"/></xsl:when>
      <xsl:when test="$error='CHANGING_ERROR'"        ><xsl:value-of select="mioga:gettext('Modification error')"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="mioga:gettext('Unknown error: %s', string($error))"/></xsl:otherwise>
   </xsl:choose>
</xsl:template>

<xsl:template name="dayName">
  <xsl:param name="no"/>
  <xsl:choose>
    <xsl:when test="number($no) = 1"><xsl:value-of select="mioga:gettext('monday')"/></xsl:when>
    <xsl:when test="number($no) = 2"><xsl:value-of select="mioga:gettext('tuesday')"/></xsl:when>
    <xsl:when test="number($no) = 3"><xsl:value-of select="mioga:gettext('wednesday')"/></xsl:when>
    <xsl:when test="number($no) = 4"><xsl:value-of select="mioga:gettext('thursday')"/></xsl:when>
    <xsl:when test="number($no) = 5"><xsl:value-of select="mioga:gettext('friday')"/></xsl:when>
    <xsl:when test="number($no) = 6"><xsl:value-of select="mioga:gettext('saturday')"/></xsl:when>
    <xsl:when test="number($no) = 7"><xsl:value-of select="mioga:gettext('sunday')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<!-- *********************************************************************** -->
<!-- * Task-priority-name                                                  * -->
<!-- *********************************************************************** -->
<xsl:template name="Task-priority-name">
   <xsl:param name="task_priority"/>
   <xsl:choose>
      <xsl:when test="$task_priority = 'very urgent'" ><xsl:value-of select="mioga:gettext('Maximal')"/></xsl:when>
      <xsl:when test="$task_priority = 'urgent'" ><xsl:value-of select="mioga:gettext('High')"/></xsl:when>
      <xsl:when test="$task_priority = 'normal'" ><xsl:value-of select="mioga:gettext('Normal')"/></xsl:when>
      <xsl:when test="$task_priority = 'not urgent'" ><xsl:value-of select="mioga:gettext('Low')"/></xsl:when>
      <xsl:otherwise>!!</xsl:otherwise>
   </xsl:choose>
</xsl:template>

<!-- ======================================================================= -->
<!-- * Task-periodic-string                                                * -->
<!-- ======================================================================= -->
<xsl:template name="Task-periodic-string" >
    <xsl:param name="value_periodType" />
    <xsl:param name="value_endDay" />
    <xsl:param name="value_endMonth" />
    <xsl:param name="value_endYear" />
    <xsl:param name="value_dayFreq" />
    <xsl:param name="value_weekFreq" />
    <xsl:param name="value_weekWeekDay" />
    <xsl:param name="value_monthFreq" />
    <xsl:param name="value_monthPeriodType" />
    <xsl:param name="value_monthDay" />
    <xsl:param name="value_monthWeek" />
    <xsl:param name="value_monthWeekDay" />
    <xsl:param name="value_yearFreq" />
    <xsl:param name="value_yearDay" />
    <xsl:param name="value_yearMonth" />
    <xsl:choose>
      <xsl:when test="$value_periodType='planned'">
         <xsl:value-of select="mioga:gettext('Unique')"/>
      </xsl:when>
      <xsl:when test="$value_periodType='day'">
         <xsl:value-of select="mioga:gettext('Every')"/>
         <xsl:if test="$value_dayFreq != 1">
            <xsl:value-of select="$value_dayFreq"/>
         </xsl:if>
         <xsl:value-of select="mioga:gettext('days')"/>
      </xsl:when>
      <xsl:when test="$value_periodType='week'">
         <xsl:value-of select="mioga:gettext('Every')"/>
         <xsl:if test="$value_weekFreq != 1">
            <xsl:value-of select="$value_weekFreq"/>
         </xsl:if>
         <xsl:value-of select="mioga:gettext('weeks on')"/>
         <xsl:call-template name="Day-name">
            <xsl:with-param name="day_number" select="$value_weekWeekDay"/>
         </xsl:call-template>
      </xsl:when>
      <xsl:when test="$value_periodType='month'">
         <xsl:value-of select="mioga:gettext('Every')"/>
         <xsl:if test="$value_monthFreq != 1">
            <xsl:value-of select="$value_monthFreq"/>
         </xsl:if>
         <xsl:value-of select="mioga:gettext('months on')"/>
         <xsl:choose>
            <xsl:when test="$value_monthPeriodType='day'">
               <xsl:value-of select="$value_monthDay"/>
            </xsl:when>
            <xsl:when test="$value_monthPeriodType='week'">
               <xsl:choose>
                  <xsl:when test="$value_monthWeek = 1"><xsl:value-of select="mioga:gettext('first')"/></xsl:when>
                  <xsl:when test="$value_monthWeek = 2"><xsl:value-of select="mioga:gettext('second')"/></xsl:when>
                  <xsl:when test="$value_monthWeek = 3"><xsl:value-of select="mioga:gettext('third')"/></xsl:when>
                  <xsl:when test="$value_monthWeek = 4"><xsl:value-of select="mioga:gettext('fourth')"/></xsl:when>
                  <xsl:when test="$value_monthWeek = -1"><xsl:value-of select="mioga:gettext('last')"/></xsl:when>
                  <xsl:otherwise>??1??</xsl:otherwise>
               </xsl:choose>
               <xsl:call-template name="Day-name">
                  <xsl:with-param name="day_number" select="$value_monthWeekDay"/>
               </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
               ??2??
            </xsl:otherwise>
         </xsl:choose>
         <xsl:value-of select="mioga:gettext('of month')"/>
      </xsl:when>
      <xsl:when test="$value_periodType='year'">
         <xsl:value-of select="mioga:gettext('Every')"/>
         <xsl:if test="$value_yearFreq != 1">
            <xsl:value-of select="$value_yearFreq"/>
         </xsl:if>
         <xsl:value-of select="mioga:gettext('years on')"/>
         <xsl:value-of select="$value_yearDay"/>&#160;

         <xsl:call-template name="Month-name">
            <xsl:with-param name="month_number" select="$value_yearMonth"/>
         </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
         ??3??
      </xsl:otherwise>
    </xsl:choose>
</xsl:template>


</xsl:stylesheet>
