<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>
<xsl:import href="jquery.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<!-- ===============
	root node
	================ -->

<xsl:template match="/">
	<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="console-js"/>
	<xsl:call-template name="objectkeys-js"/>
	<xsl:call-template name="jquery-js"/>
	<xsl:call-template name="jquery-form-js"/>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery-ui-mioga2.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.tableScrollableSortable.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.itemSelect.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.userSelect.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/cantor.css"/>
	<script type="text/javascript" src="{$jslib_uri}/jquery/jquery-ui-mioga2.js"></script>
	<script type="text/javascript" src="{$theme_uri}/javascript/cantor.js"></script>
	<xsl:apply-templates mode="head"/>
	<xsl:call-template name="theme-css"/>
</head>
<body id="cantor_body" class="cantor mioga fluid">
	<xsl:call-template name="DisplayAppTitle">
	</xsl:call-template>

	<div class="cantor-main">
		<xsl:apply-templates />
	</div>
</body>
</html>
</xsl:template>

<!-- ===============
	DisplayMain
	================ -->

<xsl:template match="/DisplayMain" mode="head">
	<script type="text/javascript">
		$(document).ready(function(){
			$('.cantor-main').cantor ({
				i18n: {
					<!-- UI translation -->
					<xsl:apply-templates select="document('')/*/mioga:strings/i18nString"/>
				}
			});
		 });
	</script>
</xsl:template>

<xsl:template match="/DisplayMain">
	<!-- NONE -->
</xsl:template>

<mioga:strings>
	<i18nString>progressBarText</i18nString>
	<i18nString>btn_create</i18nString>
	<i18nString>btn_save</i18nString>
	<i18nString>btn_cancel</i18nString>
	<i18nString>btn_add_user</i18nString>
	<i18nString>my_surveys</i18nString>
	<i18nString>title</i18nString>
	<i18nString>description</i18nString>
	<i18nString>modified</i18nString>
	<i18nString>progress</i18nString>
	<i18nString>duration</i18nString>
	<i18nString>open_attendee_list</i18nString>
	<i18nString>open_list</i18nString>
	<i18nString>closed_list</i18nString>
	<i18nString>url</i18nString>
	<i18nString>list_solicitation</i18nString>
	<i18nString>status</i18nString>
	<i18nString>confirm_cancel_survey_edit</i18nString>
	<i18nString>survey_update_success</i18nString>
	<i18nString>msg_error_attendee_name_empty</i18nString>
	<i18nString>msg_error_attendee_email</i18nString>
	<i18nString>msg_error_attendee_alreadyexist</i18nString>
	<i18nString>msg_error_resource_alreadyexist</i18nString>
	<i18nString>label_date</i18nString>
	<i18nString>btn_ok</i18nString>
	<i18nString>msg_error_date_invalid</i18nString>
	<i18nString>add_date</i18nString>
	<i18nString>select_date</i18nString>
	<i18nString>label_add_attendee</i18nString>
	<i18nString>fullname_placeholder</i18nString>
	<i18nString>email_placeholder</i18nString>
	<i18nString>add_from_mioga</i18nString>
	<i18nString>select_from_mioga</i18nString>
	<i18nString>btn_add_resource</i18nString>
	<i18nString>label_time</i18nString>
	<i18nString>select_all</i18nString>
	<i18nString>deselect_all</i18nString>
	<i18nString>all</i18nString>
	<i18nString>add</i18nString>
	<i18nString>firstname</i18nString>
	<i18nString>lastname</i18nString>
	<i18nString>email</i18nString>
	<i18nString>select_mioga_resource</i18nString>
	<i18nString>no_ui</i18nString>
	<i18nString>short_url</i18nString>
	<i18nString>enable_short_url</i18nString>
	<i18nString>short_url_enabled_later</i18nString>
	<i18nString>short_url_disabled_later</i18nString>
	<i18nString>confirm_survey_deletion_title</i18nString>
	<i18nString>confirm_survey_deletion</i18nString>
	<i18nString>btn_delete</i18nString>
	<i18nString>btn_cancel</i18nString>
	<i18nString>identify_yourself</i18nString>
	<i18nString>identify_yourself_email_prompt</i18nString>
	<i18nString>email_address</i18nString>
	<i18nString>identify_yourself_fullname_prompt</i18nString>
	<i18nString>fullname</i18nString>
	<i18nString>duration_placeholder</i18nString>
	<i18nString>duration_format_error</i18nString>
	<i18nString>btn_back_to_cantor</i18nString>
	<i18nString>search_users</i18nString>
	<i18nString>select_users</i18nString>
	<i18nString>select_teams</i18nString>
	<i18nString>select_resources</i18nString>
	<i18nString>search</i18nString>
	<i18nString>no_user_found</i18nString>
	<i18nString>close</i18nString>
</mioga:strings>


<xsl:template match="i18nString">
	<xsl:variable name="translation"><xsl:call-template name="escapeString"/></xsl:variable>
	<xsl:if test="$translation!=.">
		"<xsl:value-of select="."/>": "<xsl:value-of select="$translation"/>"<xsl:if test="position() != last()">,</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="escapeString">
	<!-- Escape newlines -->
	<xsl:call-template name="escapeNewline">
		<xsl:with-param name="pText">
			<!-- Escape quotes -->
			<xsl:call-template name="escapeQuote">
				<xsl:with-param name="pText">
					<xsl:value-of select="mioga:gettext (.)"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:with-param>
	</xsl:call-template>
</xsl:template>

<xsl:template name="escapeQuote">
	<xsl:param name="pText"/>

	<xsl:if test="string-length($pText) >0">
		<xsl:value-of select="substring-before(concat($pText, '&quot;'), '&quot;')"/>

		<xsl:if test="contains($pText, '&quot;')">
			<xsl:text>\"</xsl:text>

			<xsl:call-template name="escapeQuote">
				<xsl:with-param name="pText" select="substring-after($pText, '&quot;')"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="escapeNewline">
	<xsl:param name="pText"/>

	<xsl:if test="string-length($pText) >0">
		<xsl:value-of select= "substring-before(concat($pText, '&#10;'), '&#10;')"/>

		<xsl:if test="contains($pText, '&#10;')">
			<xsl:text>\n</xsl:text>

			<xsl:call-template name="escapeNewline">
				<xsl:with-param name="pText" select="substring-after($pText, '&#10;')"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>
