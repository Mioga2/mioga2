<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >

<xsl:output method="text"/>

<!-- User to groups report -->
<xsl:template match="DisplayUserGroupReport">
	<!-- Column headers for user attributes -->
	<xsl:text>"</xsl:text>
	<xsl:value-of select="mioga:gettext ('Firstname')"/>
	<xsl:text>","</xsl:text>
	<xsl:value-of select="mioga:gettext ('Lastname')"/>
	<xsl:text>","</xsl:text>
	<xsl:value-of select="mioga:gettext ('Email')"/>
	<xsl:text>"</xsl:text>

	<!-- Column headers for groups -->
	<xsl:for-each select="Report/user[1]/group">
		<xsl:text>,"</xsl:text>
		<xsl:value-of select="group_ident"/>
		<xsl:text>"</xsl:text>
	</xsl:for-each>
	<xsl:text>&#xa;</xsl:text>
	<xsl:apply-templates select="*"/>
</xsl:template>

<!-- Team to groups report -->
<xsl:template match="DisplayTeamGroupReport">
	<!-- Column header for team attributes -->
	<xsl:text>"</xsl:text>
	<xsl:value-of select="mioga:gettext ('Team')"/>
	<xsl:text>"</xsl:text>

	<!-- Column headers for groups -->
	<xsl:for-each select="Report/team[1]/group">
		<xsl:text>,"</xsl:text>
		<xsl:value-of select="group_ident"/>
		<xsl:text>"</xsl:text>
	</xsl:for-each>
	<xsl:text>&#xa;</xsl:text>
	<xsl:apply-templates select="*"/>
</xsl:template>

<!-- User to teams report -->
<xsl:template match="DisplayUserTeamReport">
	<!-- Column headers for user attributes -->
	<xsl:text>"</xsl:text>
	<xsl:value-of select="mioga:gettext ('Firstname')"/>
	<xsl:text>","</xsl:text>
	<xsl:value-of select="mioga:gettext ('Lastname')"/>
	<xsl:text>","</xsl:text>
	<xsl:value-of select="mioga:gettext ('Email')"/>
	<xsl:text>"</xsl:text>

	<!-- Column headers for teams -->
	<xsl:for-each select="Report/user[1]/team">
		<xsl:text>,"</xsl:text>
		<xsl:value-of select="team_ident"/>
		<xsl:text>"</xsl:text>
	</xsl:for-each>
	<xsl:text>&#xa;</xsl:text>
	<xsl:apply-templates select="*"/>
</xsl:template>

<!-- User list -->
<xsl:template match="user">
	<xsl:text>"</xsl:text>
	<xsl:value-of select="firstname"/>
	<xsl:text>","</xsl:text>
	<xsl:value-of select="lastname"/>
	<xsl:text>","</xsl:text>
	<xsl:value-of select="email"/>
	<xsl:text>"</xsl:text>
	<xsl:apply-templates select="group"/>
	<xsl:apply-templates select="team"/>
	<xsl:text>&#xa;</xsl:text>
</xsl:template>

<!-- Team list -->
<xsl:template match="team">
	<xsl:choose>
		<xsl:when test="name(/*) = 'DisplayTeamGroupReport'">
			<xsl:text>"</xsl:text>
			<xsl:value-of select="ident"/>
			<xsl:text>"</xsl:text>
			<xsl:apply-templates select="group"/>
			<xsl:text>&#xa;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>,"</xsl:text>
			<xsl:if test="is_member = 1">X</xsl:if>
			<xsl:text>"</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- Group list -->
<xsl:template match="group">
	<xsl:text>,"</xsl:text>
	<xsl:apply-templates select="profile"/>
	<xsl:text>"</xsl:text>
</xsl:template>

<!-- Profile list -->
<xsl:template match="profile">
	<!-- Add separator if preceding profile was defined -->
	<xsl:if test="preceding-sibling::profile/profile_ident != '' and profile_ident != ''">
		<xsl:text>;</xsl:text>
	</xsl:if>
	<xsl:value-of select="profile_ident"/>
</xsl:template>

</xsl:stylesheet>
