<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="base.xsl"/>

<!-- ==========
	tinymce-js
	to include tinymce in HTML HEAD element
	Caution : you must load this before scriptaculous
	========== -->

<!-- ==========
	tinymce-init-news-js
	========== -->
<xsl:template name="tinymce-init-news-js">
	<script language="javascript" type="text/javascript">

    tinyMCE.init({
    elements : "mce-news-content",
  
    <!-- Options who force a content of mceEditor to save without <p> tag. -->
    force_br_newlines : true,
    force_p_newlines : false,
    forced_root_block : '', 
    
    <!-- ***************************************************************** -->

	language : "fr",
	mode : "textareas",
	editor_selector : "mceEditor",
	skin : "o2k7",
	theme : "advanced",
	plugins : "directionality,layer,media,spellchecker,style,advimage,advlink,emotions,media,searchreplace,contextmenu,paste,fullscreen",
    
    theme_advanced_disable :"",
	theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,cut,copy,paste,|,search,replace,|,bullist,numlist,|,undo,redo,|,link,unlink,|,forecolor,backcolor,|,image,charmap,emotions,iespell,media,|,fullscreen,|,newdocument",
	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_resizing : false,
	
	apply_source_formatting : true,
	
	width : "780px",
	height : "270px",
			
		});
	</script>
</xsl:template>


<xsl:template name="tinymce-js">
	<script language="javascript" type="text/javascript" src="{$jslib_uri}/tiny_mce/tiny_mce.js"></script>
</xsl:template>

<!-- ==========
	tinymce-init-js
	========== -->

<xsl:template name="tinymce-init-js">
	<script language="javascript" type="text/javascript">
    tinyMCE.init({
			language : "fr",
			mode : "textareas",
			editor_selector : "mceEditor",
			theme : "advanced",
			plugins : "style,emotions,contextmenu,table,searchreplace,save,inlinepopups,spellchecker",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_buttons1 : "newdocument,save,|,undo,redo,|,search,replace,|,spellchecker",
			theme_advanced_buttons2 : "formatselect,fontselect,fontsizeselect,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,outdent,indent,|,forecolor,backcolor",
			theme_advanced_buttons3 : "bullist,numlist,|,link,unlink,anchor,|,charmap,image,media,emotions,cleanup,|,tablecontrols",
			convert_urls: false
		});
		</script>
</xsl:template>

<!-- ==========
	tinymce-init-complete-js
	========== -->

<xsl:template name="tinymce-init-complete-js">
	<script language="javascript" type="text/javascript">
    tinyMCE.init({
      mode : "textareas",
      theme : "advanced",
      editor_selector : "mceEditor",
      language : "fr",
      plugins : "safari,spellchecker,layer,table,advhr,advimage,advlink,emotions,insertdatetime,preview,media,searchreplace,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,pagebreak",
      theme_advanced_disable: "styleselect,code",
      theme_advanced_buttons1_add : "fontselect,fontsizeselect",
      theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator,insertdate,inserttime,preview,separator,forecolor,backcolor",
      theme_advanced_buttons3_add_before : "tablecontrols,separator",
      theme_advanced_buttons3_add : "emotions,media,advhr,separator,ltr,rtl",
      theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,blockquote,pagebreak",
      theme_advanced_toolbar_location : "top",
      theme_advanced_toolbar_align : "left",
      theme_advanced_statusbar_location : "bottom",
      plugin_insertdate_dateFormat : "%d/%m/%Y",
      plugin_insertdate_timeFormat : "%H:%M:%S",
      theme_advanced_resize_horizontal : false,
      theme_advanced_resizing : true,
      apply_source_formatting : true,
      extended_valid_elements : "hr[class|width|size|noshade]"
    });
		</script>
</xsl:template>
	
</xsl:stylesheet>
