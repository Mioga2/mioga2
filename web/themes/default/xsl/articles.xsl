<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>
<xsl:import href="articles-parts.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<xsl:include href="tiny_mce.xsl"/>
<xsl:include href="scriptaculous.xsl"/>

<!-- ===============
	Variables
	================ -->

<xsl:variable name="anim_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Animation">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="moder_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Moderation">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="write_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Write">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:variable name="read_right">
	<xsl:choose>
		<xsl:when test="//AccessRights/Read">1</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<!--
Headers
-->
<xsl:template name="mioga-css">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/mioga.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{$theme_uri}/css/articles.css" media="screen" />
</xsl:template>

<xsl:template name="articles-js">
    <script src="{$theme_uri}/javascript/articles.js" type="text/javascript">
    </script>
</xsl:template>

<xsl:template match="/">
	<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
    <xsl:call-template name="mioga-js"/>
    <xsl:call-template name="calendar-js"/>
    <xsl:call-template name="favicon"/>
    <xsl:call-template name="title"/>
    <xsl:call-template name="articles-js"/>
    <xsl:call-template name="scriptaculous-js"/>
    <xsl:call-template name="tinymce-js"/>
    
	<xsl:choose>
		<xsl:when test="//Options/Option[@field='extended_ui'] = 1">
			<xsl:call-template name="tinymce-init-complete-js"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="tinymce-init-js"/>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:call-template name="theme-css"/>
</head>
<body class="articles">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">articles.html</xsl:with-param>
    </xsl:call-template>
    <xsl:apply-templates />
</body>
</html>
</xsl:template>

<!--
 DisplayMain
-->
<xsl:template match="DisplayMain">
    <xsl:call-template name="DisplayTemplate"/>
</xsl:template>

<!--
 DisplayWaitingArticles
-->
<xsl:template match="DisplayWaitingArticles">
    <xsl:call-template name="DisplayTemplate"/>
</xsl:template>

<!--
 DisplayDraftArticles
-->
<xsl:template match="DisplayDraftArticles">
    <xsl:call-template name="DisplayTemplate"/>
</xsl:template>

<!--
=======================================
 Article
=======================================
-->
<xsl:template match="Article">
	<div class="article">
        <h2><xsl:value-of select="Title"/></h2>
        <pre class="chapo"><xsl:value-of select="Header" disable-output-escaping="yes"/></pre>
        <xsl:value-of select="Contents" disable-output-escaping="yes"/>
    </div>
</xsl:template>


<!--
=======================================
 DisplayArticle
=======================================
-->
<xsl:template match="DisplayArticle">
    <xsl:call-template name="Menu">
        <xsl:with-param name="page" select="name()"/>
    </xsl:call-template>
    
    <div id="disable-window" style="display: none;"></div>
    <div id="download-box" class="dialog" style="display: none;">
        <div class="sbox border_color">
            <h2 class="title_bg_color"><xsl:value-of select="mioga:gettext('Choose a path to save this article')"/></h2>
            <div class="content">
                <form method="post" action="SaveArticle">
                    <xsl:attribute name="onsubmit">if (checkEmpty(this.uri, "<xsl:value-of select="mioga:gettext('Please provide a path')"/>")) { new Ajax.Request("SaveArticle", { asynchronous: true, evalScripts: true, parameters: encodeURI(Form.serialize(this)), onComplete: complete, onLoading: loading }) }; return false;</xsl:attribute>
                    
                    <p class="tip"><xsl:value-of select="mioga:gettext('Please just provide the relative path from Mioga2 with the filename. Ex: /home/admin/folder/article.html')"/><br/>
                    <xsl:value-of select="mioga:gettext('CAUTION: if you provide an already existing filename, it will be overwritten.')"/></p>
                    <p class="error" id="dl-error" style="display: none;"></p>
                    <span><input class="text" type="text" name="uri" style="width: 95%;"/></span>
                    
                    <ul class="button_list">
                        <li>
                            <input class="button" type="submit" value="{mioga:gettext('Save')}"/>
                        </li>
                        <li>
                            <a class="button" href="#"><xsl:attribute name="onclick">$("disable-window").hide(); $("download-box").hide(); return false;</xsl:attribute><xsl:value-of select="mioga:gettext('Cancel')"/></a>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
    
	<xsl:apply-templates select="Article"/>
</xsl:template>

<!--
=======================================
 ViewArticle/CommentArticle
=======================================
-->
<xsl:template match="ViewArticle|CommentArticle">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td width="20%">
                <div class="categories">
                    <div id="catg-box" class="sbox border_color">
                        <h2 class="title_bg_color"><xsl:value-of select="mioga:gettext('Categories')"/></h2>
                        <ul>
                            <xsl:for-each select="Categories/Category">
                                <!-- js variables -->
                                <xsl:variable name="category_name">
                                    <xsl:value-of select="label"/>
                                </xsl:variable>
                                <!-- end of js variables -->
                                
                                <xsl:variable name="ctgedit_id">ctg<xsl:value-of select="id" />_edit</xsl:variable>
                                <xsl:variable name="ctgedit_id_ext"><xsl:value-of select="$ctgedit_id" />_ext</xsl:variable>
                                
                                <xsl:variable name="even_or_odd">
                                    <xsl:choose>
                                        <xsl:when test="position() mod 2 = 0">even</xsl:when>
                                        <xsl:otherwise>odd</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                
                                <xsl:variable name="params">
                                    <xsl:if test="id">?category_id=<xsl:value-of select="id"/></xsl:if>
                                </xsl:variable>
                                
                                <li class="{$even_or_odd}">
                                    <a id="{$ctgedit_id}" href="{action}{$params}"><xsl:value-of select="$category_name"/></a>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </div>
                </div>
            </td>
    
            <td width="80%">
                <div style="margin-right: 0.5em" id="article-{Article/id}" class="article sbox border_color">
                    <h2 class="title_bg_color"><a style="color: #0332B2;" href="ViewArticle?article_id={Article/id}"><xsl:value-of select="Article/Title"/></a></h2>
                    <h3 class="article"><xsl:value-of select="Article/Infos"/>
                        <xsl:for-each select="Article/Categories/Category">
                            <!-- js variables -->
                            <xsl:variable name="category_name">
                                <xsl:value-of select="name"/>
                            </xsl:variable>
                            <!-- end of js variables -->
                            
                            <xsl:if test="position() != 1">,</xsl:if>&#160;<a href="{action}?id={id}"><xsl:value-of select="$category_name"/></a>
                        </xsl:for-each>
                        <span class="article_id">#<xsl:value-of select="Article/id"/></span>
                    </h3>
                    <pre class="chapo"><xsl:value-of select="Article/Header" disable-output-escaping="yes"/></pre>
                    <div class="content">
                        <xsl:value-of select="Article/Contents" disable-output-escaping="yes"/>
                    </div>
                    <xsl:if test="//AccessRights/ReadComment">
                        <p class="comments_number"><xsl:choose>
                            <xsl:when test="Article/CommentsNumber &gt; 0">
                                <xsl:value-of select="mioga:gettext('%d comment(s)', Article/CommentsNumber)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="mioga:gettext('No comment')"/>
                            </xsl:otherwise>
                        </xsl:choose></p>
                    </xsl:if>
                </div>
                <xsl:if test="local-name(.)='ViewArticle' and //AccessRights/WriteComment">
                    <ul class="button_list">
                        <li><a href="CommentArticle?article_id={Article/id}" class="button"><xsl:value-of select="mioga:gettext('Send a comment')"/></a></li>
                    </ul>
                </xsl:if>
                <xsl:if test="//AccessRights/ReadComment">
                    <div id="comments">
                        <xsl:choose>
                            <xsl:when test="local-name(.)='ViewArticle'">
                                <xsl:call-template name="RenderComments">
                                    <xsl:with-param name="comments" select="Article/Comments"/>
                                    <xsl:with-param name="article_id" select="Article/id"/>
                                </xsl:call-template>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:if test="count(Comment) &gt; 0">
                                    <xsl:call-template name="RenderComments">
                                        <xsl:with-param name="comments" select="Comment"/>
                                        <xsl:with-param name="article_id" select="Article/id"/>
                                    </xsl:call-template>
                                </xsl:if>
                                <form action="CommentArticle" method="POST">
                                    <input type="hidden" name="article_id" value="{Article/id}"/>
                                    <input type="hidden" name="parent_id" value="{Comment/rowid}"/>
                                    <xsl:if test="Error">
                                        <p class="error"><xsl:value-of select="mioga:gettext('An error has occurred:')"/>&#160;<xsl:value-of select="Error"/></p>
                                    </xsl:if>
                                    <p>
                                        <label for="subject"><xsl:value-of select="mioga:gettext('Subject')"/></label>&#160;<br/>
                                        <input class="text border_color" type="text" name="subject" id="subject" value="{Reply/subject}"/>
                                    </p>
                                    <p>
                                        <label for="comment"><xsl:value-of select="mioga:gettext('Comment')"/></label><br/>
                                        <textarea class="border_color" rows="5" name="comment" id="comment"><xsl:value-of select="Reply/comment"/></textarea>
                                    </p>
                                    <ul class="button_list">
                                        <li><input class="button" type="submit" name="send_comment" value="{mioga:gettext('Send')}"/></li>
                                    </ul> 
                                </form>
                            </xsl:otherwise>
                        </xsl:choose>
                    </div>
                </xsl:if>
            </td>
        </tr>
    </table>
</xsl:template>

<!--
=======================================
 RenderComments
=======================================
-->
<xsl:template name="RenderComments">
    <xsl:param name="comments"/>
    <xsl:param name="article_id"/>
    
    <xsl:for-each select="$comments">
        <xsl:sort select="rowid" data-type="number"/>
        <div class="comment" id="comment-{rowid}">
            <h2><xsl:value-of select="subject"/></h2>
            <p class="infos"><xsl:value-of select="mioga:gettext('Written by %s on %s at %s.', author, date, time)"/></p>
            <div class="contents"><xsl:value-of select="comment"/></div>
            <xsl:if test="//AccessRights/WriteComment">
                <p class="reply">[ <a href="CommentArticle?article_id={$article_id}&amp;parent_id={rowid}"><xsl:value-of select="mioga:gettext('Reply')"/></a> ]</p>
            </xsl:if>
            <xsl:call-template name="RenderComments">
                <xsl:with-param name="comments" select="./Comments"/>
                <xsl:with-param name="article_id" select="$article_id"/>
            </xsl:call-template>
        </div>
    </xsl:for-each>
</xsl:template>

<!--
=======================================
 DisplayTemplate
=======================================
-->
<xsl:template name="DisplayTemplate">
    <xsl:variable name="page" select="name()"/>
    
    <div id="disable-window" style="display: none;"></div>
    <div id="options-box" class="dialog" style="display: none;"></div>
    
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td width="20%">
                <div class="categories">
                    <xsl:if test="$anim_right=1">
                        <ul class="hmenu" style="margin-bottom: 0.3em; text-align: left;">
                            <li><a class="add_category" href="#"><xsl:attribute name="onclick">new Ajax.Request("CreateCategory", {evalScripts:true, onLoading: loading, onComplete: complete, asynchronous:true, parameters:encodeURI("label=<xsl:value-of select="Categories/New"/>&amp;page=<xsl:value-of select="$page"/>")}); return false;</xsl:attribute><xsl:value-of select="Categories/Create"/></a></li>
                        </ul>
						<div class="clear-both"></div>
                    </xsl:if>
                    <div id="catg-box" class="sbox border_color">
                        <xsl:apply-templates select="Categories">
                            <xsl:with-param name="page" select="$page"/>
                        </xsl:apply-templates>
                    </div>
                </div>
            </td>
    
            <td width="80%">
                <div class="articles">
                    <xsl:call-template name="Menu">
                        <xsl:with-param name="page" select="$page"/>
                    </xsl:call-template>
                    <div id="articles-box">
                    <xsl:apply-templates select="Articles">
                        <xsl:with-param name="page" select="$page"/>
                    </xsl:apply-templates>
                    </div>
                </div>
            </td>
        </tr>
    </table>

</xsl:template>

<xsl:template name="Menu">
    <xsl:param name="page"/>
    
	<div class="articles-menu">
		<ul class="hmenu">
			<xsl:choose>
				<xsl:when test="$page = 'DisplayArticle'">
					<li>
						<a class="back" href="DisplayMain"><xsl:value-of select="mioga:gettext('Back to the main page')"/></a>
					</li>
					<xsl:if test="$write_right = 1">
						<li>
							<a class="edition" href="EditArticle?rowid={//Article/rowid}&amp;page={$page}%3Farticle_id%3D{//Article/id}"><xsl:value-of select="mioga:gettext('Edit this article')"/></a>
						</li>
					</xsl:if>
					<li>
						<a class="download" href="#">
							<xsl:attribute name="onclick">$("disable-window").show(); $("download-box").show(); return false;</xsl:attribute>
							<xsl:value-of select="mioga:gettext('Save this article to your Mioga2 home')"/></a>
					</li>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$anim_right=1">
						<li><a class="options" href="#"><xsl:attribute name="onclick">new Ajax.Request("DisplayOptions", { evalScripts:true, asynchronous:true }); return false;</xsl:attribute><xsl:value-of select="//Actions/Options"/></a></li>
					</xsl:if>
					<li>
					<xsl:choose>
						<xsl:when test="$page!='DisplayMain'"><a class="documents" href="DisplayMain"><xsl:value-of select="//Actions/MainArticles"/></a></xsl:when>
						<xsl:otherwise><span class="current documents"><xsl:value-of select="//Actions/MainArticles"/></span></xsl:otherwise>
					</xsl:choose>
					</li>
					<xsl:if test="$moder_right=1">
						<li>
						<xsl:choose>
							<xsl:when test="$page!='DisplayWaitingArticles'"><a class="documents" href="DisplayWaitingArticles"><xsl:value-of select="//Actions/WaitingArticles"/></a></xsl:when>
							<xsl:otherwise><span class="current documents"><xsl:value-of select="//Actions/WaitingArticles"/></span></xsl:otherwise>
						</xsl:choose>
						</li>
					</xsl:if>
					<xsl:if test="$write_right=1">
						<li>
						<xsl:choose>
							<xsl:when test="$page!='DisplayDraftArticles'"><a class="drafts" href="DisplayDraftArticles"><xsl:value-of select="//Actions/DraftArticles"/></a></xsl:when>
							<xsl:otherwise><span class="current drafts"><xsl:value-of select="//Actions/DraftArticles"/></span></xsl:otherwise>
						</xsl:choose>
						</li>
						<li id="new_article">
						<xsl:choose>
							<xsl:when test="//NoCategory"><xsl:call-template name="DisabledNewArticle"/></xsl:when>
							<xsl:otherwise><xsl:call-template name="NewArticle"><xsl:with-param name="page" select="$page"/></xsl:call-template></xsl:otherwise>
						</xsl:choose>
						</li>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</ul>
	</div>
	<div class="clear-both"></div>
</xsl:template>

<!--
=======================================
 WriteArticle
=======================================
-->
<xsl:template match="WriteArticle">
    <xsl:call-template name="WriteOrEditArticle">
        <xsl:with-param name="action">WriteArticle</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<!--
=======================================
 EditArticle
=======================================
-->
<xsl:template match="EditArticle">
    <xsl:call-template name="WriteOrEditArticle">
        <xsl:with-param name="action">UpdateArticle</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<!--
==================
 UpdateArticle
==================
-->
<xsl:template match="UpdateArticle">
    <xsl:call-template name="WriteOrEditArticle">
        <xsl:with-param name="action" select="//EditAction"/>
    </xsl:call-template>
</xsl:template>

</xsl:stylesheet>
