<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>
<xsl:import href="poll-parts.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<xsl:include href="scriptaculous.xsl"/>

<!--
Headers
-->
<xsl:template name="poll-css">
    <link rel="stylesheet" type="text/css" href="{$theme_uri}/css/poll.css" media="screen" />
</xsl:template>

<xsl:template name="poll-js">
	<script src="{$theme_uri}/javascript/poll.js" type="text/javascript">
    </script>
</xsl:template>

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
    <xsl:call-template name="poll-css"/>
    <xsl:call-template name="mioga-js"/>
    <xsl:call-template name="calendar-js"/>
    <xsl:call-template name="favicon"/>
    <xsl:call-template name="title"/>
    
    <xsl:call-template name="scriptaculous-js"/>
    <xsl:call-template name="poll-js"/>
	<xsl:call-template name="theme-css"/>
</head>
<body class="poll">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">poll.html</xsl:with-param>
    </xsl:call-template>
    <xsl:apply-templates />
</body>
</html>
</xsl:template>

<!--
=======================================
 DisplayMain
=======================================
-->
<xsl:template match="DisplayMain">
    <xsl:call-template name="DisplayTemplate"/>
</xsl:template>

<!--
=======================================
 DisplayAnim
=======================================
-->
<xsl:template match="DisplayAnim">
	<xsl:call-template name="DisplayTemplate"/>
</xsl:template>

<!--
=======================================
 ViewResults
=======================================
-->
<xsl:template match="ViewResults">
	 <xsl:call-template name="Menu">
        <xsl:with-param name="page" select="name()"/>
    </xsl:call-template>
    
    <xsl:for-each select="Results/Question">
        <table class="results sbox border_color" width="80%" cellspacing="0" cellpadding="0">
            <tr>
                <xsl:for-each select="Headers/Header">
                	<th class="title_bg_color"><xsl:value-of select="."/></th>
                </xsl:for-each>
            </tr>
            <xsl:for-each select="Row">
            	<tr>
                    <xsl:attribute name="class">
                        <xsl:choose>
                            <xsl:when test="position() mod 2 = 0">even</xsl:when>
                            <xsl:otherwise>odd</xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:for-each select="Col">
                    	<td class="border_color"><xsl:value-of select="."/></td>
                    </xsl:for-each>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:for-each>
</xsl:template>

<!--
=======================================
 DisplayTemplate
=======================================
-->
<xsl:template name="DisplayTemplate">
    <xsl:variable name="page" select="name()"/>
    
    <div id="disable-window" style="display: none;"></div>
    <div id="options-box" class="dialog" style="display: none;"></div>
    <div id="edit-box" class="dialog" style="visibility: hidden;"></div>
    
    <xsl:call-template name="Menu">
        <xsl:with-param name="page" select="$page"/>
    </xsl:call-template>
    <div id="polls">
        <xsl:if test="count(Polls/Poll) = 0">
        	<p class="information">
            <xsl:if test="$page = 'DisplayMain'">
                <xsl:value-of select="mioga:gettext('There is no poll available at this time.')"/>
                <xsl:if test="//CanAnim"><xsl:value-of select="mioga:gettext(' You can create one in the animation interface.')"/></xsl:if>
            </xsl:if>
            <xsl:if test="$page = 'DisplayAnim'">
                <xsl:value-of select="mioga:gettext('There is no poll in a writing state. You can create a new one by clicking on &quot;Create a new poll&quot;.')"/>
            </xsl:if>
            </p>
        </xsl:if>
        
        <xsl:apply-templates select="Polls">
            <xsl:with-param name="page" select="$page"/>
        </xsl:apply-templates>
    </div>

</xsl:template>

<!--
=======================================
 AnswerPoll
=======================================
-->
<xsl:template match="AnswerPoll">
    <xsl:variable name="page" select="name()"/>
    
    <xsl:call-template name="AnswerUI">
        <xsl:with-param name="page" select="$page"/>
    </xsl:call-template>
</xsl:template>

<!--
=======================================
 UpdateAnswers
=======================================
-->
<xsl:template match="UpdateAnswers">
	<xsl:call-template name="AnswerUI">
        <xsl:with-param name="page">AnswerPoll</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<!--
=======================================
 AnswerUI
=======================================
-->
<xsl:template name="AnswerUI">
    <xsl:param name="page"/>
    
	<xsl:variable name="next_or_finish">
        <xsl:choose>
        	<xsl:when test="CurrentQuestion = Poll/@questions"><xsl:value-of select="mioga:gettext('Validate')"/></xsl:when>
        	<xsl:otherwise><xsl:value-of select="mioga:gettext('Next')"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
	<xsl:call-template name="Menu">
        <xsl:with-param name="page" select="$page"/>
    </xsl:call-template>
    
    <div class="poll sbox border_color">
        <h2 class="title_bg_color"><xsl:value-of select="Poll/Title"/>
            <div class="progress-box" style="position: absolute; right: 10px; top: 0px;">
                <div class="progress-bar">
                    <xsl:attribute name="style">width: <xsl:value-of select="AnswersPercent"/>%;</xsl:attribute>
                    <img src="{$image_uri}/pb_shadow.png"/>
                </div>
            </div>
        </h2>
        <div class="content">
            <p><xsl:value-of select="Poll/Description"/></p>
            <form method="post" action="UpdateAnswers" id="answer-form">
            <xsl:attribute name="onsubmit">return checkForm(this, "<xsl:value-of select="mioga:gettext('This input field requires a numerical integer value.')"/>", "<xsl:value-of select="mioga:gettext('This input field requires a numerical float value.')"/>", "<xsl:value-of select='mioga:gettext("Your answer can&apos;t be blank.")'/>");</xsl:attribute>
            <input type="hidden" name="poll_id" value="{Poll/@rowid}"/>
            <input type="hidden" name="current_question" value="{CurrentQuestion}"/>
            <input type="hidden" id="q_type" name="type" value="{Question/@type}"/>
			<xsl:call-template name="CSRF-input"/>
            <xsl:apply-templates select="Question">
                <xsl:with-param name="page" select="$page"/>
            </xsl:apply-templates>
            
                <ul class="button_list">
                    <xsl:if test="CurrentQuestion != 1">
                    	<li><a class="button" href="AnswerPoll?rowid={Poll/@rowid}&amp;page={CurrentQuestion - 2}"><xsl:value-of select="mioga:gettext('Previous')"/></a></li>
                    </xsl:if>
                    <li><input class="button" type="submit" value="{mioga:gettext('Save and continue later')}" onclick="this.name='continue'"/></li>
                    <li><input class="button" type="submit" value="{$next_or_finish}"/></li>
                </ul>
            </form>
        </div>
    </div>
</xsl:template>

<!--
=======================================
 EditPoll
=======================================
-->
<xsl:template match="EditPoll">
    <xsl:call-template name="CreateOrEditPoll">
        <xsl:with-param name="page" select="name()"/>
    </xsl:call-template>
</xsl:template>

<!--
=======================================
 CreatePoll
=======================================
-->
<xsl:template match="CreatePoll">
	<xsl:call-template name="CreateOrEditPoll">
        <xsl:with-param name="page" select="name()"/>
    </xsl:call-template>
</xsl:template>

<!--
=======================================
 UpdatePoll
=======================================
-->
<xsl:template match="UpdatePoll">
	<xsl:call-template name="CreateOrEditPoll">
        <xsl:with-param name="page" select="Page"/>
    </xsl:call-template>
</xsl:template>

<!--
=======================================
 AddQuestion
=======================================
-->
<xsl:template match="AddQuestion">
    <xsl:call-template name="AddOrEditQuestion">
        <xsl:with-param name="page">AddQuestion</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<!--
=======================================
 EditQuestion
=======================================
-->
<xsl:template match="EditQuestion">
	<xsl:call-template name="AddOrEditQuestion">
        <xsl:with-param name="page">EditQuestion</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<!--
=======================================
 UpdateQuestion
=======================================
-->
<xsl:template match="UpdateQuestion">
    <xsl:apply-templates select="Question" mode="edition">
        <xsl:with-param name="page" select="Page"/>
    </xsl:apply-templates>
    
    <script type="text/javascript">
        answer_count = <xsl:value-of select="count(Question/Choices/Choice)"/>;
    </script>
</xsl:template>

<!--
=======================================
 CreateOrEditPoll
=======================================
-->
<xsl:template name="CreateOrEditPoll">
    <xsl:param name="page"/>
    
    <xsl:variable name="title">
        <xsl:choose>
        	<xsl:when test="$page = 'EditPoll'"><xsl:value-of select="mioga:gettext('Editing poll')"/></xsl:when>
        	<xsl:otherwise><xsl:value-of select="mioga:gettext('Creating new poll')"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
	<div id="disable-window" style="display: none;"></div>
    <div id="options-box" class="dialog" style="display: none;"></div>
    
	<xsl:call-template name="Menu">
        <xsl:with-param name="page" select="$page"/>
    </xsl:call-template>
    
    <h1><xsl:value-of select="$title"/></h1>
    
    <form action="UpdatePoll" method="post" id="poll-form">
        <input type="hidden" name="page" value="{$page}"/>
		<xsl:call-template name="CSRF-input"/>
        <fieldset>
            <legend><xsl:value-of select="mioga:gettext('Poll informations')"/></legend>
            
            <label for="poll_title">
                <p><xsl:value-of select="mioga:gettext('Title:')"/></p>
                <input id="poll_title" name="poll_title" type="text" maxsize="128" value="{Poll/Title}"/>
                <xsl:if test="//errors/err[@arg = 'poll_title']">
                    <xsl:call-template name="error-message">
                        <xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'poll_title']/type" /></xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
            </label><br/>
            <label for="poll_description">
                <p><xsl:value-of select="mioga:gettext('Description:')"/></p>
                <textarea id="poll_description" name="poll_description"><xsl:value-of select="Poll/Description"/></textarea>
                <xsl:if test="//errors/err[@arg = 'poll_description']">
                	<xsl:call-template name="error-message">
                        	<xsl:with-param name="type"><xsl:value-of select="//errors/err[@arg = 'poll_description']/type" /></xsl:with-param>
                	</xsl:call-template>
                </xsl:if>
            </label>
        </fieldset>
        
        <fieldset id="questions-list">
            <legend><xsl:value-of select="mioga:gettext('Questions')"/></legend>
            <ul class="hmenu" style="margin-bottom: 1em;">
                <li>
                    <a class="new_question" href="#"><xsl:attribute name="onclick">location.href="AddQuestion?"+Form.serialize("poll-form");</xsl:attribute>
                    <xsl:value-of select="mioga:gettext('Add a new question')"/></a>
                </li>
            </ul>
            <xsl:for-each select="Poll/Questions/Question">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </fieldset>
        
        <ul class="button_list">
            <li><input class="button" type="submit" value="{mioga:gettext('Save')}"/></li>
            <li><a class="button" href="DisplayAnim"><xsl:value-of select="mioga:gettext('Cancel')"/></a></li>
        </ul>
    </form>
</xsl:template>

<!--
=======================================
 Menu
=======================================
-->
<xsl:template name="Menu">
    <xsl:param name="page"/>
    
    <ul class="hmenu">
        <xsl:if test="$page = 'DisplayMain'">
        	<xsl:if test="//CanAnim">
                <li>
                    <a class="anim_poll" href="DisplayAnim"><xsl:value-of select="mioga:gettext('Animation interface (create or edit a poll)')"/></a>
                </li>
            </xsl:if>
        </xsl:if>
        <xsl:if test="$page = 'DisplayAnim' or $page = 'ViewResults'">
        	<li>
                <a class="back" href="DisplayMain"><xsl:value-of select="mioga:gettext('Back to main page')"/></a>
            </li>
        </xsl:if>
        <xsl:if test="$page = 'ViewResults'">
        	<li>
                <a class="csv" href="ViewResults?rowid={//Results/@poll_id}&amp;format=csv"><xsl:value-of select="mioga:gettext('Get results in CSV format')"/></a>
            </li>
        </xsl:if>
        <xsl:if test="$page = 'DisplayAnim'">
        	<li>
                <a class="create_poll" href="CreatePoll"><xsl:value-of select="mioga:gettext('Create a new poll')"/></a>
            </li>
        </xsl:if>
        <xsl:if test="$page = 'CreatePoll' or $page = 'EditPoll'">
            <li>
                <a class="back" href="DisplayAnim"><xsl:value-of select="mioga:gettext('Back to animation page')"/></a>
            </li>
        	<li>
                <a class="options" href="#"><xsl:attribute name="onclick">new Ajax.Request("DisplayOptions", { evalScripts: true, asynchronous: true } ); return false;</xsl:attribute><xsl:value-of select="mioga:gettext('Modify poll options')"/></a>
            </li>
        </xsl:if>
    </ul>
	<div class="clear-both"></div>
</xsl:template>

</xsl:stylesheet>
