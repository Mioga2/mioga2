<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<!--  <xsl:output method="html"/> -->

<xsl:template name="translate_button">
  <xsl:param name="string"/>
  
  <xsl:choose>
    <xsl:when test="$string = 'ok'"><xsl:value-of select="mioga:gettext('Ok')"/></xsl:when>
    <xsl:when test="$string = 'search'"><xsl:value-of select="mioga:gettext('Search')"/></xsl:when>
    <xsl:when test="$string = 'delete'"><xsl:value-of select="mioga:gettext('Delete')"/></xsl:when>
    <xsl:when test="$string = 'affect'"><xsl:value-of select="mioga:gettext('Assign')"/></xsl:when>
    <xsl:when test="$string = 'cancel'"><xsl:value-of select="mioga:gettext('Cancel')"/></xsl:when>
    <xsl:when test="$string = 'choose'"><xsl:value-of select="mioga:gettext('Choose')"/></xsl:when>
    <xsl:when test="$string = 'show_all'"><xsl:value-of select="mioga:gettext('Show all')"/></xsl:when>
    <xsl:when test="$string = 'back'"><xsl:value-of select="mioga:gettext('Back')"/></xsl:when>
    <xsl:when test="$string = 'add'"><xsl:value-of select="mioga:gettext('Add')"/></xsl:when>
    <xsl:when test="$string = 'change'"><xsl:value-of select="mioga:gettext('Change')"/></xsl:when>
    <xsl:when test="$string = 'create'"><xsl:value-of select="mioga:gettext('Create')"/></xsl:when>
    <xsl:when test="$string = 'edit'"><xsl:value-of select="mioga:gettext('Edit')"/></xsl:when>
    <xsl:when test="$string = 'extract'"><xsl:value-of select="mioga:gettext('Extract')"/></xsl:when>
    <xsl:when test="$string = 'modify'"><xsl:value-of select="mioga:gettext('Modify')"/></xsl:when>
    <xsl:when test="$string = 'next'"><xsl:value-of select="mioga:gettext('Next')"/></xsl:when>
    <xsl:when test="$string = 'no'"><xsl:value-of select="mioga:gettext('No')"/></xsl:when>
    <xsl:when test="$string = 'other'"><xsl:value-of select="mioga:gettext('Other')"/></xsl:when>
    <xsl:when test="$string = 'propose'"><xsl:value-of select="mioga:gettext('Propose')"/></xsl:when>
    <xsl:when test="$string = 'previous'"><xsl:value-of select="mioga:gettext('Previous')"/></xsl:when>
    <xsl:when test="$string = 'reload'"><xsl:value-of select="mioga:gettext('Reload')"/></xsl:when>
    <xsl:when test="$string = 'reply'"><xsl:value-of select="mioga:gettext('Reply')"/></xsl:when>
    <xsl:when test="$string = 'reserve'"><xsl:value-of select="mioga:gettext('Reserve')"/></xsl:when>
    <xsl:when test="$string = 'save'"><xsl:value-of select="mioga:gettext('Save')"/></xsl:when>
    <xsl:when test="$string = 'send'"><xsl:value-of select="mioga:gettext('Send')"/></xsl:when>
    <xsl:when test="$string = 'view'"><xsl:value-of select="mioga:gettext('View')"/></xsl:when>
    <xsl:when test="$string = 'yes'"><xsl:value-of select="mioga:gettext('Yes')"/></xsl:when>
    <xsl:otherwise><xsl:value-of select="mioga:gettext('Submit')"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

  <!-- =================================
       button
       ================================= -->
  
  <xsl:template name="button">
    <xsl:param name="name"/>
    <xsl:param name="href"/>
    <xsl:param name="reload_menu">0</xsl:param>

    <a href="{$href}" class="button" id="{$name}-button">
        <xsl:if test="$reload_menu=1">
            <xsl:attribute name="onclick">top.mioga_menu.location='<xsl:value-of select="$bin_uri"/>/__MIOGA-USER__/Workspace/DisplayMenu'</xsl:attribute>
        </xsl:if>
        <xsl:call-template name="translate_button">
          <xsl:with-param name="string" select="$name"/>
        </xsl:call-template>
    </a>
  </xsl:template>

  <!-- =================================
       form button
       ================================= -->

  <xsl:template name="form-button">
    <xsl:param name="name"/>
    <xsl:param name="value"/>
    <xsl:param name="input-value"/>
    <xsl:param name="reload_menu">0</xsl:param>
    
    <!--<ul class="button_list">
      <li>-->
        <input type="submit" name="{$name}" class="button">
          <xsl:attribute name="value"><xsl:call-template name="translate_button">
            <xsl:with-param name="string" select="$value"/>
          </xsl:call-template></xsl:attribute>
          <xsl:if test="$reload_menu=1">
            <xsl:attribute name="onclick">top.mioga_menu.location='<xsl:value-of select="$bin_uri"/>/__MIOGA-USER__/Workspace/DisplayMenu'</xsl:attribute>
          </xsl:if>
        </input>
      <!--</li>
    </ul>-->
  </xsl:template>

  <xsl:template name="reload-button">
    <xsl:param name="href"/>

    <xsl:call-template name="button">
      <xsl:with-param name="name">reload</xsl:with-param>
      <xsl:with-param name="href" select="$href"/>
    </xsl:call-template>
  </xsl:template>
 
<!-- =================================
     ok cancel form buttons
     ================================= -->
  
  <xsl:template name="ok-cancel-form-button">
    <xsl:param name="name"/>
    <xsl:param name="referer"/>
    <xsl:param name="reload_menu">0</xsl:param>


    <xsl:call-template name="ok-form-button">
      <xsl:with-param name="reload_menu" select="$reload_menu"/>
      <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>&#160;<xsl:call-template name="cancel-button">
    <xsl:with-param name="href" select="$referer"/>
  </xsl:call-template>            
  
</xsl:template>

  <!-- =================================
       yes no form buttons
       ================================= -->
  
  <xsl:template name="yes-no-form-button">
    <xsl:param name="name"/>
    <xsl:param name="referer"/>
    <xsl:param name="reload_menu">0</xsl:param>


    <xsl:call-template name="yes-form-button">
      <xsl:with-param name="reload_menu" select="$reload_menu"/>
      <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>&#160;<xsl:call-template name="no-button">
    <xsl:with-param name="href" select="$referer"/>
  </xsl:call-template>            
  
</xsl:template>


<!-- =================================
     ok cancel buttons
     ================================= -->

<xsl:template name="ok-cancel-button">
    <xsl:param name="ok-href"/>
    <xsl:param name="cancel-href"/>
    

    <xsl:call-template name="ok-button">
        <xsl:with-param name="href" select="$ok-href"/>
    </xsl:call-template>&#160;<xsl:call-template name="cancel-button">
        <xsl:with-param name="href" select="$cancel-href"/>
    </xsl:call-template>

</xsl:template>


<!-- =================================
     yes no buttons
     ================================= -->

<xsl:template name="yes-no-button">
    <xsl:param name="yes-href"/>
    <xsl:param name="no-href"/>
    

    <xsl:call-template name="yes-button">
        <xsl:with-param name="href" select="$ok-href"/>
    </xsl:call-template>&#160;<xsl:call-template name="no-button">
        <xsl:with-param name="href" select="$cancel-href"/>
    </xsl:call-template>

</xsl:template>

<!-- 
     ====================
     == FR TRANSLATION ==
     ====================
     -->

<!-- =================================
     validate form button
     ================================= -->

<xsl:template name="create-form-button">
    <xsl:param name="name"/>


    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">create</xsl:with-param>
    </xsl:call-template>

</xsl:template>



<!-- =================================
     next form button
     ================================= -->

<xsl:template name="next-form-button">
    <xsl:param name="name"/>


    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">next</xsl:with-param>
    </xsl:call-template>

</xsl:template>

<!-- =================================
     prev form button
     ================================= -->

<xsl:template name="prev-form-button">
    <xsl:param name="name"/>


    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">previous</xsl:with-param>
    </xsl:call-template>

</xsl:template>



<!-- =================================
     other form button
     ================================= -->

<xsl:template name="other-form-button">
    <xsl:param name="name"/>


    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">other</xsl:with-param>
    </xsl:call-template>

</xsl:template>

 

<!-- =================================
     affect form button
     ================================= -->

<xsl:template name="affect-form-button">
  <xsl:param name="name"/>

  <xsl:call-template name="form-button">
    <xsl:with-param name="name" select="$name"/>
    <xsl:with-param name="value">affect</xsl:with-param>
  </xsl:call-template>
</xsl:template>


<!-- =================================
     save form button
     ================================= -->

<xsl:template name="save-form-button">
    <xsl:param name="name"/>


    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">save</xsl:with-param>
    </xsl:call-template>

</xsl:template>



<!-- =================================
     edit form button
     ================================= -->

<xsl:template name="edit-form-button">
    <xsl:param name="name"/>


    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">edit</xsl:with-param>
    </xsl:call-template>

</xsl:template>


<!-- =================================
     view form button
     ================================= -->

<xsl:template name="view-form-button">
    <xsl:param name="name"/>


    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">view</xsl:with-param>
    </xsl:call-template>

</xsl:template>


<!-- =================================
     send form button
     ================================= -->

<xsl:template name="send-form-button">
    <xsl:param name="name"/>


    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">send</xsl:with-param>
    </xsl:call-template>

</xsl:template>


<!-- =================================
     send button
     ================================= -->

<xsl:template name="send-button">
    <xsl:param name="href"/>

    <xsl:call-template name="button">
        <xsl:with-param name="name">send</xsl:with-param>
        <xsl:with-param name="href" select="$href"/>
    </xsl:call-template>

</xsl:template>

<!-- =================================
     delete button
     ================================= -->

<xsl:template name="delete-button">
    <xsl:param name="href"/>

    <xsl:call-template name="button">
        <xsl:with-param name="name">delete</xsl:with-param>
        <xsl:with-param name="href" select="$href"/>
    </xsl:call-template>

</xsl:template>



<!-- =================================
     edit button
     ================================= -->

<xsl:template name="edit-button">
    <xsl:param name="href"/>

    <xsl:call-template name="button">
        <xsl:with-param name="name">edit</xsl:with-param>
        <xsl:with-param name="href" select="$href"/>
    </xsl:call-template>

</xsl:template>

<!-- =================================
     back button
     ================================= -->

<xsl:template name="back-button">
    <xsl:param name="href"/>
    <xsl:param name="reload_menu">0</xsl:param>
    <xsl:param name="target"></xsl:param>

    <xsl:call-template name="button">
        <xsl:with-param name="name">back</xsl:with-param>
        <xsl:with-param name="href" select="$href"/>
        <xsl:with-param name="reload_menu" select="$reload_menu"/>
        <xsl:with-param name="target" select="$target"/>
    </xsl:call-template>

</xsl:template>


<!-- =================================
     modify button
     ================================= -->

<xsl:template name="modify-button">
    <xsl:param name="href"/>

    <xsl:call-template name="button">
        <xsl:with-param name="name">modify</xsl:with-param>
        <xsl:with-param name="href" select="$href"/>
    </xsl:call-template>


</xsl:template>




<!-- =================================
     reply button
     ================================= -->

<xsl:template name="reply-button">
    <xsl:param name="href"/>
    <xsl:param name="reload_menu">0</xsl:param>

    <xsl:call-template name="button">
        <xsl:with-param name="name">reply</xsl:with-param>
        <xsl:with-param name="href" select="$href"/>
        <xsl:with-param name="reload_menu" select="$reload_menu"/>
    </xsl:call-template>

</xsl:template>


<!-- =================================
     modify form button
     ================================= -->

<xsl:template name="modify-form-button">
    <xsl:param name="name"/>

    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">modify</xsl:with-param>
    </xsl:call-template>

</xsl:template>

<!-- =================================
     choose form button
     ================================= -->

<xsl:template name="choose-form-button">
    <xsl:param name="name"/>

    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">choose</xsl:with-param>
    </xsl:call-template>

</xsl:template>




<!-- =================================
     yes form button
     ================================= -->

<xsl:template name="yes-form-button">
    <xsl:param name="name"/>
    <xsl:param name="reload_menu">0</xsl:param>

    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">yes</xsl:with-param>
        <xsl:with-param name="reload_menu"><xsl:value-of select="$reload_menu" /></xsl:with-param>
    </xsl:call-template>

</xsl:template>



<!-- =================================
     ok form button
     ================================= -->

<xsl:template name="ok-form-button">
    <xsl:param name="name"/>
    <xsl:param name="reload_menu">0</xsl:param>

    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">ok</xsl:with-param>
        <xsl:with-param name="reload_menu"><xsl:value-of select="$reload_menu" /></xsl:with-param>
    </xsl:call-template>

</xsl:template>


<!-- =================================
     yes button
     ================================= -->

<xsl:template name="yes-button">
    <xsl:param name="href"/>

    <xsl:call-template name="button">
        <xsl:with-param name="name">yes</xsl:with-param>
        <xsl:with-param name="href" select="$href"/>
    </xsl:call-template>

</xsl:template>


<!-- =================================
     ok button
     ================================= -->

<xsl:template name="ok-button">
    <xsl:param name="href"/>

    <xsl:call-template name="button">
        <xsl:with-param name="name">ok</xsl:with-param>
        <xsl:with-param name="href" select="$href"/>
    </xsl:call-template>

</xsl:template>



  <!-- ====================
       == FR TRANSLATION ==
       ==================== -->

  <xsl:template name="actionBoxTitleTranslation">
    <xsl:text>Actions&#160;:&#160;</xsl:text>
  </xsl:template>

  <!-- search form button -->
  <xsl:template name="search-form-button">
    <xsl:param name="name"/>

    <xsl:call-template name="form-button">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value">search</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- change form button -->
  <xsl:template name="change-form-button">
    <xsl:param name="name"/>
    
    <xsl:call-template name="form-button">
      <xsl:with-param name="name" select="$name"/>
      <xsl:with-param name="value">change</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- delete form button -->
  <xsl:template name="delete-form-button">
    <xsl:param name="name"/>
    <xsl:param name="value"></xsl:param>

    <xsl:call-template name="form-button">
      <xsl:with-param name="name" select="$name"/>
      <xsl:with-param name="value">delete</xsl:with-param>
      <xsl:with-param name="input-value" select="$value"/>
    </xsl:call-template>
  </xsl:template>

  <!-- add form button -->
  <xsl:template name="add-form-button">
    <xsl:param name="name"/>
    <xsl:call-template name="form-button">
      <xsl:with-param name="name" select="$name"/>
      <xsl:with-param name="value">add</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- no form buttons -->
  <xsl:template name="no-form-button">
    <xsl:param name="name"/>
    <xsl:call-template name="form-button">
      <xsl:with-param name="name" select="$name"/>
      <xsl:with-param name="value">no</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- cancel form buttons -->
  <xsl:template name="cancel-form-button">
    <xsl:param name="name"/>
    <xsl:call-template name="form-button">
      <xsl:with-param name="name" select="$name"/>
      <xsl:with-param name="value">cancel</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- cancel button -->
  <xsl:template name="cancel-button">
    <xsl:param name="href"/>
    
    <xsl:call-template name="button">
      <xsl:with-param name="name">cancel</xsl:with-param>
      <xsl:with-param name="href" select="$href"/>
    </xsl:call-template>
  </xsl:template>

  <!-- no button -->
  <xsl:template name="no-button">
    <xsl:param name="href"/>
    
    <xsl:call-template name="button">
      <xsl:with-param name="name">no</xsl:with-param>
      <xsl:with-param name="href" select="$href"/>
    </xsl:call-template>
  </xsl:template>

  
  <!-- propose button  -->
  <xsl:template name="propose-form-button">
      <xsl:param name="name"/>
      <xsl:call-template name="form-button">
          <xsl:with-param name="name" select="$name"/>
          <xsl:with-param name="value">propose</xsl:with-param>
      </xsl:call-template>
  </xsl:template>

  <!-- reserve button -->
  <xsl:template name="reserve-form-button">
      <xsl:param name="name"/>
      <xsl:call-template name="form-button">
          <xsl:with-param name="name" select="$name"/>
          <xsl:with-param name="value">reserve</xsl:with-param>
      </xsl:call-template>
  </xsl:template>
  
</xsl:stylesheet>
