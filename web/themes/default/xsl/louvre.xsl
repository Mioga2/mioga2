<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<xsl:include href="jquery.xsl"/>
<xsl:include href="inline_message.xsl"/>

<!-- ===============
	root document
	================ -->

<xsl:template name="Louvre-css">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/louvre.css" media="screen" />
</xsl:template>

<xsl:template name="Louvre-js">
 	<script src="{$theme_uri}/javascript/louvre.js" type="text/javascript"></script>
 	<script src="{$theme_uri}/javascript/locale/louvre-lang-{//miogacontext/group/lang}.js" type="text/javascript"></script>
</xsl:template>

<xsl:template match="/">
	<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
    <xsl:call-template name="console-js"/>

	<xsl:call-template name="jquery-js"/>
	<xsl:call-template name="jquery-ui-1.12.1"/>
	<xsl:call-template name="jquery-layout"/>
	<xsl:call-template name="jquery-treeview"/>
	<xsl:call-template name="jquery-form-serializeobject"/>

	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>

	<xsl:call-template name="Louvre-css"/>
	<xsl:call-template name="Louvre-js"/>

	<xsl:apply-templates mode="head"/>
	<xsl:call-template name="theme-css"/>
</head>
<body id="Louvre_body" class="Louvre body">
	<xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- ===============
	ContainsGalleries
	================ -->

<xsl:template match="ContainsGalleries" mode="head">
</xsl:template>

<xsl:template match="ContainsGalleries">
	<div class="message-container">
		&#160;
		<p class="center"><xsl:value-of select="mioga:gettext ('This folder contains one or more galleries and can not be converted itself to a gallery.')"/></p>
	</div>
</xsl:template>

<!-- ===============
	CreateGallery_Config
	================ -->

<xsl:template match="CreateGallery_Config" mode="head">
</xsl:template>

<xsl:template match="CreateGallery_Config">
  <form class="form" method="POST" action="CreateGallery" id="the_form">
    <xsl:if test="regenerate = 1">
		<input type="hidden" name="regenerate" value="1"/>
	</xsl:if>
    <table border="0" cellspacing="5">
	  <tr>
	    <td class="right-align">
          <xsl:value-of select="mioga:gettext('Maximum thumbnail size:')"/>
        </td><td>
          <label for="maxwidth" class="left-align">
            <xsl:value-of select="mioga:gettext('width')"/>
          </label>
          <xsl:text disable-output-escaping="yes">&amp;nbsp&#59;</xsl:text>
          <input type="text" name="maxwidth" size="5" value="100"/>
          <xsl:text disable-output-escaping="yes">&amp;nbsp&#59;</xsl:text>
          <xsl:text disable-output-escaping="yes">&amp;nbsp&#59;</xsl:text>
          <xsl:text disable-output-escaping="yes">&amp;nbsp&#59;</xsl:text>
          <label for="maxheight" class="left-align">
            <xsl:value-of select="mioga:gettext('height')"/>
          </label>
          <xsl:text disable-output-escaping="yes">&amp;nbsp&#59;</xsl:text>
          <input type="text" name="maxheight" size="5" value="100"/>
        </td>
	  </tr>
	  <tr>
		  <td class="right-align">
			  <label for="locale"><xsl:value-of select="mioga:gettext ('Gallery language')"/></label>
		  </td>
		  <td>
			  <select name="locale">
				  <xsl:for-each select="locale">
					  <option>
						  <xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
						  <xsl:value-of select="label"/>
					  </option>
				  </xsl:for-each>
			  </select>
		  </td>
	  </tr>
	  <tr>
		  <td class="right-align">
			  <label for="background"><xsl:value-of select="mioga:gettext ('Background color')"/></label>
		  </td>
		  <td>
			  <select name="background">
				  <option value="black"><xsl:value-of select="mioga:gettext ('Black')"/></option>
				  <option value="white"><xsl:value-of select="mioga:gettext ('White')"/></option>
			  </select>
		  </td>
	  </tr>
	  <tr>
		  <td class="right-align">
			  <label for="options"><xsl:value-of select="mioga:gettext ('Display options')"/></label>
		  </td>
		  <td>
		  	<table>
				<tr>
					<td>
						<input type="checkbox" name="popup_display" id="popup_display" checked="1" onclick="ToggleCheckBoxes ();"/>
						<label class="left-align" for="popup_display"><xsl:value-of select="mioga:gettext ('Show preview as popup')"/></label>
					</td>
					<td>
						<input type="checkbox" name="show_filename" id="show_filename" checked="1" disabled="1"/>
						<label class="left-align" for="show_filename"><xsl:value-of select="mioga:gettext ('Show filename')"/></label>
					</td>
				</tr>
				<tr>
					<td>
						<input type="checkbox" name="show_description" id="show_description" checked="1" disabled="1"/>
						<label class="left-align" for="show_description"><xsl:value-of select="mioga:gettext ('Show description')"/></label>
					</td>
					<td>
						<input type="checkbox" name="show_dimensions" id="show_dimensions" checked="1" disabled="1"/>
						<label class="left-align" for="show_dimensions"><xsl:value-of select="mioga:gettext ('Show dimensions')"/></label>
					</td>
				</tr>
				<tr>
					<td>
						<input type="checkbox" name="show_tags" id="show_tags" checked="1" disabled="1"/>
						<label class="left-align" for="show_tags"><xsl:value-of select="mioga:gettext ('Show tags')"/></label>
					</td>
					<td>
						<input type="checkbox" name="show_weight" id="show_weight" checked="1" disabled="1"/>
						<label class="left-align" for="show_weight"><xsl:value-of select="mioga:gettext ('Show weight')"/></label>
					</td>
				</tr>
			</table>
		  </td>
	  </tr>
	  <tr><td colspan="2" class="button_cell">
		<button class="button" id="button_submit">
			<xsl:choose>
				<xsl:when test="regenerate = 1">
					<xsl:value-of select="mioga:gettext ('Modify the gallery')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="mioga:gettext ('Create the gallery')"/>
				</xsl:otherwise>
			</xsl:choose>
		</button>
	  </td></tr>
	</table>
	<input type="hidden" name="dir">
	  <xsl:attribute name="value">
	    <xsl:value-of select="directory"/>
	  </xsl:attribute>
	</input>
	<p class="invisible centered" id="p_show_loading">
	  <img src="{$theme_uri}/images/16x16/actions/view-refresh.png"/>
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="mioga:gettext('Creating gallery...')"/>
	</p>
  </form>
</xsl:template>


<!-- ===============
	Operation_Result
	================ -->

<xsl:template match="Operation_Result" mode="head">
</xsl:template>

<xsl:template match="Operation_Result">
  <br/><br/>
  <p class="centered">
    <xsl:value-of select="result"/>
  </p>
  <xsl:if test="status=1">
	  <script type="text/javascript">
		document.has_finished = true;
	  </script>
  </xsl:if>
</xsl:template>


<!-- ===============
	DisplayMain
	================ -->

<xsl:template match="DisplayMain" mode="head">
	<script type="text/javascript">
		var search_uri = '<xsl:value-of select="$user_bin_uri"/>/Search/SearchWS.json';
		var no_thumbnail = '<xsl:value-of select="mioga:gettext ('Thumbnail not available')"/>';
		var louvre_dir = '<xsl:value-of select="louvre_dir"/>';
		jQuery (document).ready (function () {
			InitializeUI ();
		});
	</script>
</xsl:template>

<xsl:template match="DisplayMain">
	<div id="ui-container">
		<div class="ui-layout-west">
			<ul id="browser" class="filetree treeview-default">
				<xsl:apply-templates select="galleries/folder"/>
			</ul>
		</div>
		<div class="ui-layout-center">
			<div id="search-toggler" onclick="ShowSearchBar ();">
				<xsl:attribute name="title"><xsl:value-of select="mioga:gettext ('Show search bar')"/></xsl:attribute>
				&#160;
			</div>
			<div id="search-bar">
				<div class="close-box" onclick="HideSearchBar ();">&#160;</div>
				<form class="form" onsubmit="return (false);">
					<input type="text" name="query_string" id="query_string" onkeypress="HandleKeyPress (event);"/>
					<a class="button" href="#" onclick="Search ();"><xsl:value-of select="mioga:gettext ('Search')"/></a>
					<div class="search-parameters">
						<span class="label"><xsl:value-of select="mioga:gettext ('Search parameters')"/></span>
						<div class="search-parameter">
							<input type="checkbox" class="checkbox" name="search_text" id="search_text" checked="1"/>
							<label for="search_text"><xsl:value-of select="mioga:gettext ('Filename and comments')"/></label>
						</div>
						<div class="search-parameter">
							<input type="checkbox" class="checkbox" name="search_description" id="search_description" checked="1"/>
							<label for="search_description"><xsl:value-of select="mioga:gettext ('Description')"/></label>
						</div>
						<div class="search-parameter">
							<input type="checkbox" class="checkbox" name="search_tags" id="search_tags" checked="1"/>
							<label for="search_tags"><xsl:value-of select="mioga:gettext ('Tags')"/></label>
						</div>
						<div class="search-parameter">
							<a id="advanced-search-parameters-link" href="#"><xsl:value-of select="mioga:gettext ('Advanced parameters')"/></a>
						</div>
						<div id="advanced-search-parameters" style="display: none;">
							<input type="hidden" name="advanced_search" value="1" disabled="true"/>
							<div class="search-parameter">
								<label for="group_id"><xsl:value-of select="mioga:gettext ('Search in group')"/></label>
								&#160;
								<select name="group_id" id="group_id" disabled="true">
									<option value=""><xsl:value-of select="mioga:gettext ('All groups')"/></option>
									<xsl:for-each select="group">
										<option>
											<xsl:attribute name="value"><xsl:value-of select="rowid"/></xsl:attribute>
											<xsl:value-of select="ident"/>
										</option>
									</xsl:for-each>
								</select>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div id="viewer-container">
				<iframe id="viewer" src=""></iframe>
			</div>
			<div id="search-results">
				<p><xsl:value-of select="mioga:gettext ('Results for:')"/>&#160;<span id="search-query"></span><a id="download-selection" class="button" href="#" onclick="DownloadSelectedImages ();" style="display: none;"><xsl:value-of select="mioga:gettext ('Download selected images')"/></a></p>
				<ul class="result-list"></ul>
			</div>
		</div>
	</div>
</xsl:template>

<xsl:template match="folder">
	<li>
		<xsl:choose>
			<xsl:when test="path != ''">
				<a href="#">
					<xsl:attribute name="onclick">DisplayGallery ('<xsl:value-of select="path"/>/index.html');</xsl:attribute>
					<span class="folder gallery"><xsl:value-of select="name"/></span>
				</a>
			</xsl:when>
			<xsl:otherwise>
				<span class="folder"><xsl:value-of select="name"/></span>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="folder != ''">
			<ul>
				<xsl:apply-templates select="folder"/>
			</ul>
		</xsl:if>
	</li>
</xsl:template>


</xsl:stylesheet>

