<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:output method="html" indent="yes"/>

<xsl:include href="base.xsl" />
<xsl:include href="simple_large_list.xsl"/>


<!-- =================================
     Select Single User
     ================================= -->

<xsl:template match="SelectSingleUser|SelectSingleTeam|SelectSingleGroup|SelectSingleResource|SelectSingleExternalMioga|SelectMultipleUsers|SelectMultipleTeams|SelectMultipleGroups|SelectMultipleResources|SelectMultipleResourcesAllowedInGroup|SelectMultipleResourcesNotInGroup|SelectMultipleTeamsNotInGroup|SelectMultipleUsersNotInGroup|SelectSingleUserMemberOfGroup|SelectSingleExtendedUserMemberOfGroup|SelectSingleGroupForUserWithAppliAndFunc|SelectSingleUserMemberOfTeam|SelectSingleGroupForUser|SelectMultipleUsersMemberOfGroup|SelectMultipleUsersMemberOfTeam|SelectMultipleTeamsMemberOfGroup|SelectMultipleExternalMiogaUser|ValidateSelectMultipleUsers|ValidateSelectMultipleGroups|ValidateSelectMultipleTeams|ValidateSelectMultipleResources|ValidateSelectMultipleUsersMemberOfGroup|ValidateSelectMultipleUsersMemberOfTeam|ValidateSelectMultipleTeamsMemberOfGroup|ValidateSelectMultipleResourcesAllowedInGroup|ValidateSelectMultipleResourcesNotInGroup|ValidateSelectMultipleUsersNotInGroup|ValidateSelectMultipleTeamsNotInGroup|ValidateSelectMultipleExternalMiogaUsers" mode="advanced-search-field-content">
    <xsl:param name="name"/>
    <xsl:param name="value"/>

    <xsl:call-template name="SllAdvSearchFieldContent">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value" select="$value"/>
    </xsl:call-template>
   
</xsl:template>

<xsl:template match="SelectSingleUser|SelectSingleTeam|SelectSingleGroup|SelectSingleResource|SelectSingleExternalMioga|SelectMultipleUsers|SelectMultipleTeams|SelectMultipleGroups|SelectMultipleResources|SelectMultipleResourcesAllowedInGroup|SelectMultipleResourcesNotInGroup|SelectMultipleTeamsNotInGroup|SelectMultipleUsersNotInGroup|SelectSingleUserMemberOfGroup|SelectSingleExtendedUserMemberOfGroup|SelectSingleGroupForUserWithAppliAndFunc|SelectSingleUserMemberOfTeam|SelectSingleGroupForUser|SelectMultipleUsersMemberOfGroup|SelectMultipleUsersMemberOfTeam|SelectMultipleTeamsMemberOfGroup|SelectMultipleExternalMiogaUser|ValidateSelectMultipleUsers|ValidateSelectMultipleGroups|ValidateSelectMultipleTeams|ValidateSelectMultipleResources|ValidateSelectMultipleUsersMemberOfGroup|ValidateSelectMultipleUsersMemberOfTeam|ValidateSelectMultipleTeamsMemberOfGroup|ValidateSelectMultipleResourcesAllowedInGroup|ValidateSelectMultipleResourcesNotInGroup|ValidateSelectMultipleUsersNotInGroup|ValidateSelectMultipleTeamsNotInGroup|ValidateSelectMultipleExternalMiogaUsers" mode="sll-field-name" name="MiogaFormInputTranslation">
    <xsl:param name="label"/>
    <xsl:param name="name">
        <xsl:choose>
            <xsl:when test="starts-with($label, 'sll_search_')"><xsl:value-of select="substring-after($label, 'sll_search_')"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="$label"/></xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    
    <xsl:call-template name="mioga2-common-name-translation">
        <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>

</xsl:template>


<xsl:template match="SelectSingleUser|SelectSingleTeam|SelectSingleGroup|SelectSingleResource|SelectSingleExternalMioga|SelectMultipleUsers|SelectMultipleTeams|SelectMultipleGroups|SelectMultipleResources|SelectMultipleResourcesAllowedInGroup|SelectMultipleResourcesNotInGroup|SelectMultipleTeamsNotInGroup|SelectMultipleUsersNotInGroup|SelectSingleUserMemberOfGroup|SelectSingleExtendedUserMemberOfGroup|SelectSingleGroupForUserWithAppliAndFunc|SelectSingleUserMemberOfTeam|SelectSingleGroupForUser|SelectMultipleUsersMemberOfGroup|SelectMultipleUsersMemberOfTeam|SelectMultipleTeamsMemberOfGroup|SelectMultipleExternalMiogaUser|ValidateSelectMultipleUsers|ValidateSelectMultipleGroups|ValidateSelectMultipleTeams|ValidateSelectMultipleResources|ValidateSelectMultipleUsersMemberOfGroup|ValidateSelectMultipleUsersMemberOfTeam|ValidateSelectMultipleTeamsMemberOfGroup|ValidateSelectMultipleResourcesAllowedInGroup|ValidateSelectMultipleResourcesNotInGroup|ValidateSelectMultipleUsersNotInGroup|ValidateSelectMultipleTeamsNotInGroup|ValidateSelectMultipleExternalMiogaUsers" mode="sll-translate-values">
    <xsl:param name="name"/>
    <xsl:param name="value"/>

    <xsl:call-template name="mioga2-common-value-translation">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value" select="$value"/>
    </xsl:call-template>

</xsl:template>


<xsl:template match="SelectSingleUser|SelectSingleTeam|SelectSingleUserMemberOfGroup|SelectSingleExtendedUserMemberOfGroup|SelectSingleUserMemberOfTeam|SelectSingleGroup|SelectSingleGroupForUser|SelectSingleGroupForUserWithAppliAndFunc|SelectSingleResource|SelectSingleExternalMioga">
    <body xsl:use-attribute-sets="body_attr">
        <xsl:call-template name="DisplayAppTitle"/>
        
        <xsl:call-template name="MiogaTitle" />


        <div xsl:use-attribute-sets="app_body_attr">

            <xsl:apply-templates select="SimpleLargeList">
                <xsl:with-param name="advancedsearch">1</xsl:with-param>
                <xsl:with-param name="displayaction">0</xsl:with-param>
                <xsl:with-param name="label" select="name(.)"/>
            </xsl:apply-templates>
            
        </div>

        <div xsl:use-attribute-sets="app_footer_attr">
            <xsl:call-template name="cancel-button">
                <xsl:with-param name="href"><xsl:value-of select="local-name(.)"/>?select_cancel_act=1</xsl:with-param>
            </xsl:call-template>            
        </div>

    </body>
</xsl:template>

<xsl:template name="MiogaTitleTranslation">
  <xsl:value-of select="title"/>
</xsl:template>

<xsl:template match="SelectMultipleUsers|SelectMultipleTeams|SelectMultipleGroups|SelectMultipleResources|SelectMultipleResourcesAllowedInGroup|SelectMultipleUsersMemberOfGroup|SelectMultipleUsersMemberOfTeam|SelectMultipleTeamsMemberOfGroup|SelectMultipleUsersNotInGroup|SelectMultipleTeamsNotInGroup|SelectMultipleResourcesNotInGroup|SelectMultipleExternalMiogaUsers">
  <xsl:variable name="callback" select="local-name(.)"/>
  
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle"/>
    
    <xsl:call-template name="MiogaTitle"/>
    
    <div xsl:use-attribute-sets="app_body_attr">

      <xsl:apply-templates select="SimpleLargeList">
        <xsl:with-param name="advancedsearch">1</xsl:with-param>
        <xsl:with-param name="displayaction">0</xsl:with-param>
        <xsl:with-param name="select-menu">1</xsl:with-param>
        <xsl:with-param name="label" select="name(.)"/>
      </xsl:apply-templates>
            
  </div>

  <div xsl:use-attribute-sets="app_footer_attr">
      <xsl:choose>
        <xsl:when test="count(SimpleLargeList/List/Row) != 0">
          
          <xsl:call-template name="ok-cancel-button">
            <xsl:with-param name="ok-href">Validate<xsl:value-of select="$callback"/>?select_valid_act=1</xsl:with-param>
            <xsl:with-param name="cancel-href"><xsl:value-of select="$callback"/>?select_cancel_act=1</xsl:with-param>
          </xsl:call-template>
          
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="local-name(.) = 'SelectMultipleTeamsMemberOfGroup'">
              <xsl:call-template name="MiogaMessage">
                <xsl:with-param name="message">
                  <xsl:call-template name="noTeam"/>
                </xsl:with-param>
              </xsl:call-template>
            </xsl:when>
          </xsl:choose>
          <xsl:call-template name="cancel-button">
            <xsl:with-param name="href"><xsl:value-of select="local-name(.)"/>?select_cancel_act=1</xsl:with-param>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </div>
    
    <script type="text/javascript" src="{$jslib_uri}/scriptaculous/prototype.js"></script>
    <script src="{$theme_uri}/javascript/select.js" type="text/javascript"></script>
    <script type="text/javascript">
        init_select_validate('<xsl:value-of select="$callback"/>Sll');
    </script>
  </body>
</xsl:template>

<xsl:template match="ValidateSelectMultipleUsers|ValidateSelectMultipleTeams|ValidateSelectMultipleResources|ValidateSelectMultipleResourcesAllowedInGroup|ValidateSelectMultipleGroups|ValidateSelectMultipleUsersMemberOfGroup|ValidateSelectMultipleUsersMemberOfTeam|ValidateSelectMultipleTeamsMemberOfGroup|ValidateSelectMultipleUsersNotInGroup|ValidateSelectMultipleTeamsNotInGroup|ValidateSelectMultipleResourcesNotInGroup|ValidateSelectMultipleExternalMiogaUsers">
    <body xsl:use-attribute-sets="body_attr">
        <xsl:call-template name="DisplayAppTitle"/>

        <xsl:call-template name="MiogaTitle" />

        <div xsl:use-attribute-sets="app_body_attr">

            <xsl:call-template name="ValidateTitle"/>

            <xsl:apply-templates select="SimpleLargeList">
                <xsl:with-param name="displayaction">0</xsl:with-param>
            </xsl:apply-templates>
            
        </div>

        <div xsl:use-attribute-sets="app_footer_attr">
            <xsl:call-template name="ok-button">
                <xsl:with-param name="href"><xsl:value-of select="./name"/>?select_ok_act=1</xsl:with-param>
            </xsl:call-template>&#160;<xsl:call-template name="modify-button">
                <xsl:with-param name="href"><xsl:value-of select="./name"/>?select_modify_act=1</xsl:with-param>
            </xsl:call-template>&#160;<xsl:call-template name="cancel-button">
                <xsl:with-param name="href"><xsl:value-of select="./name"/>?select_cancel_act=1</xsl:with-param>
            </xsl:call-template>            

        </div>

    </body>
</xsl:template>


  <xsl:template match="SelectSingleUser/SimpleLargeList|SelectSingleTeam/SimpleLargeList|SelectSingleGroup/SimpleLargeList|SelectSingleResource/SimpleLargeList|SelectSingleExternalMioga/SimpleLargeList|SelectMultipleUsers/SimpleLargeList|SelectMultipleTeams/SimpleLargeList|SelectMultipleGroups/SimpleLargeList|SelectMultipleResources/SimpleLargeList|SelectMultipleResourcesAllowedInGroup/SimpleLargeList|SelectMultipleResourcesNotInGroup/SimpleLargeList|SelectMultipleTeamsNotInGroup/SimpleLargeList|SelectMultipleUsersNotInGroup/SimpleLargeList|SelectSingleUserMemberOfGroup/SimpleLargeList|SelectSingleExtendedUserMemberOfGroup/SimpleLargeList|SelectSingleGroupForUserWithAppliAndFunc/SimpleLargeList|SelectSingleUserMemberOfTeam/SimpleLargeList|SelectSingleGroupForUser/SimpleLargeList|SelectMultipleUsersMemberOfGroup/SimpleLargeList|SelectMultipleUsersMemberOfTeam/SimpleLargeList|SelectMultipleTeamsMemberOfGroup/SimpleLargeList|SelectMultipleExternalMiogaUser/SimpleLargeList" mode="list-body" name="SelectSllListBody">
    <xsl:param name="disp_action">1</xsl:param>
    <xsl:param name="callback" select=".."/>
    <xsl:param name="callback-name"><xsl:value-of select="local-name($callback)"/></xsl:param>
    
    <xsl:for-each select="List/Row">
      
      <tr align="center">
        <xsl:choose>
          <xsl:when test="./rowid = ../../../notselectable/rowid">
			<xsl:attribute name="class">list-body disabled</xsl:attribute>
          </xsl:when>
          <xsl:when test="position() mod 2 = 1">
			<xsl:attribute name="class">list-body odd</xsl:attribute>
            <xsl:if test="./rowid = ../../../selected/rowid"><xsl:attribute name="class">list-body odd selected</xsl:attribute></xsl:if>
          </xsl:when>
          <xsl:otherwise>
			<xsl:attribute name="class">list-body even</xsl:attribute>
            <xsl:if test="./rowid = ../../../selected/rowid"><xsl:attribute name="class">list-body even selected</xsl:attribute></xsl:if>
          </xsl:otherwise>
        </xsl:choose>
        

        <xsl:call-template name="SllListRow">
          <xsl:with-param name="callback" select="$callback"/>
          <xsl:with-param name="callback-name" select="$callback-name"/>
          <xsl:with-param name="disp_action" select="$disp_action"/>
        </xsl:call-template>
      </tr>
    </xsl:for-each>
    
    <xsl:if test="$disp_action=1">
      <xsl:call-template name="SllListActions">
        <xsl:with-param name="callback" select="$callback"/>
        <xsl:with-param name="callback-name" select="$callback-name"/>
        <xsl:with-param name="disp_action" select="$disp_action"/>                
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <!-- ====================
       == FR TRANSLATION ==
       ==================== -->

  <xsl:template name="noTeam">
    <xsl:value-of select="mioga:gettext('No team invited in group')"/>
  </xsl:template>
  
  <xsl:template name="MiogaFormTranslation">
      <xsl:variable name="label"/>
    
      <xsl:value-of select="mioga:gettext('Search for')"/>&#160;
      <xsl:choose>
          <xsl:when test="contains($label, 'User')"><xsl:value-of select="mioga:gettext('user')"/></xsl:when>
          <xsl:when test="contains($label, 'Team')"><xsl:value-of select="mioga:gettext('team')"/></xsl:when>
          <xsl:when test="contains($label, 'Group')"><xsl:value-of select="mioga:gettext('group')"/></xsl:when>
          <xsl:when test="contains($label, 'Resource')"><xsl:value-of select="mioga:gettext('resource')"/></xsl:when>
      </xsl:choose>
  </xsl:template>

  <xsl:template name="ValidateTitle">
      <xsl:call-template name="MiogaMessage">
          <xsl:with-param name="message">
            <xsl:value-of select="mioga:gettext('Are you sure you want to select these')"/>&#160;
              <xsl:choose>
                  <xsl:when test="starts-with(./name, 'SelectMultipleTeams')"><xsl:value-of select="mioga:gettext('teams')"/></xsl:when>
                  <xsl:when test="starts-with(./name, 'SelectMultipleUsers') or ./name = 'SelectMultipleExternalMiogaUsers'"><xsl:value-of select="mioga:gettext('users')"/></xsl:when>
                  <xsl:when test="starts-with(./name, 'SelectMultipleGroups')"><xsl:value-of select="mioga:gettext('groups')"/></xsl:when>
                  <xsl:when test="starts-with(./name, 'SelectMultipleResources')"><xsl:value-of select="mioga:gettext('resources')"/></xsl:when>
              </xsl:choose> ?
          </xsl:with-param>
      </xsl:call-template>
  </xsl:template>

</xsl:stylesheet>
