<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>
<xsl:import href="jquery.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<!-- ===============
	root node
	================ -->

<xsl:template match="/">
	<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="title"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="console-js"/>
	<xsl:call-template name="objectkeys-js"/>
	<xsl:call-template name="jquery-js"/>
	<xsl:call-template name="jquery-ui"/>
	<xsl:call-template name="jquery-form-js"/>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>

	<!-- Mioga jQuery modules -->
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.foldable.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.tableScrollableSortable.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.itemSelect.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.userSelect.css" media="screen" />

	<!-- Fouquet-specific -->
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/fouquet.css"/>
	<script src="{$theme_uri}/javascript/fouquet.js" type="text/javascript"></script>
	<xsl:call-template name="theme-css"/>
	<xsl:apply-templates mode="head"/>
</head>
<body class="fouquet mioga fluid">
	<xsl:call-template name="DisplayAppTitle">
	</xsl:call-template>

	<xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- ===============
	DisplayMain
	================ -->

<mioga:strings>
	<i18nString>label</i18nString>
	<i18nString>firstname</i18nString>
	<i18nString>lastname</i18nString>
	<i18nString>email</i18nString>
	<i18nString>last_connection</i18nString>
	<i18nString>never_connected</i18nString>
	<i18nString>ident</i18nString>
	<i18nString>profile</i18nString>
	<i18nString>identity_title</i18nString>
	<i18nString>filesystem_title</i18nString>
	<i18nString>applications_title</i18nString>
	<i18nString>read_write</i18nString>
	<i18nString>read_only</i18nString>
	<i18nString>no_right</i18nString>
	<i18nString>profile_details</i18nString>
	<i18nString>team</i18nString>
	<i18nString>synthesis_view</i18nString>
	<i18nString>no_profile</i18nString>
	<i18nString>invite_user</i18nString>
	<i18nString>progressBarText</i18nString>
	<i18nString>group_attributes_title</i18nString>
	<i18nString>group_attributes_ident</i18nString>
	<i18nString>group_attributes_animator</i18nString>
	<i18nString>group_attributes_disk_space_used</i18nString>
	<i18nString>group_attributes_default_profile</i18nString>
	<i18nString>group_attributes_theme</i18nString>
	<i18nString>group_attributes_lang</i18nString>
	<i18nString>group_attributes_default_app</i18nString>
	<i18nString>B</i18nString>
	<i18nString>kB</i18nString>
	<i18nString>MB</i18nString>
	<i18nString>GB</i18nString>
	<i18nString>TB</i18nString>
	<i18nString>en_US</i18nString>
	<i18nString>fr_FR</i18nString>
	<i18nString>group_users_title</i18nString>
	<i18nString>group_teams_title</i18nString>
	<i18nString>group_profiles_title</i18nString>
	<i18nString>animation</i18nString>
	<i18nString>user_synthesis</i18nString>
	<i18nString>team_synthesis</i18nString>
	<i18nString>profile_synthesis</i18nString>
	<i18nString>nb_users</i18nString>
	<i18nString>nb_teams</i18nString>
	<i18nString>users_title</i18nString>
	<i18nString>teams_title</i18nString>
	<i18nString>affected_users_title</i18nString>
	<i18nString>non_affected_users_title</i18nString>
	<i18nString>affected_teams_title</i18nString>
	<i18nString>non_affected_teams_title</i18nString>
	<i18nString>no_ui</i18nString>
	<i18nString>user_specific</i18nString>
	<i18nString>users</i18nString>
	<i18nString>teams</i18nString>
	<i18nString>team_specific</i18nString>
	<i18nString>internal_error</i18nString>
	<i18nString>no_user_for_profile</i18nString>
	<i18nString>no_team_for_profile</i18nString>
	<i18nString>no_profile_for_user</i18nString>
	<i18nString>edit</i18nString>
	<i18nString>save</i18nString>
	<i18nString>cancel</i18nString>
	<i18nString>confirm_cancel_profile_edit</i18nString>
	<i18nString>filesystem</i18nString>
	<i18nString>applications</i18nString>
	<i18nString>drag_users_notice</i18nString>
	<i18nString>drag_teams_notice</i18nString>
	<i18nString>group_applications_title</i18nString>
	<i18nString>confirm_application_deletion</i18nString>
	<i18nString>enable_application</i18nString>
	<i18nString>user_tabs_help</i18nString>
	<i18nString>team_tabs_help</i18nString>
	<i18nString>team_members</i18nString>
	<i18nString>no_user_for_team</i18nString>
	<i18nString>group_resources_title</i18nString>
	<i18nString>enable_resource</i18nString>
	<i18nString>confirm_resource_deletion</i18nString>
	<i18nString>user_edit_title</i18nString>
	<i18nString>team_edit_title</i18nString>
	<i18nString>profile_edit_title</i18nString>
	<i18nString>profile_change_success</i18nString>
	<i18nString>profile_update_success</i18nString>
	<i18nString>theme_change_success</i18nString>
	<i18nString>lang_change_success</i18nString>
	<i18nString>application_enable_success</i18nString>
	<i18nString>application_disable_success</i18nString>
	<i18nString>default_app_change_success</i18nString>
	<i18nString>resource_enable_success</i18nString>
	<i18nString>resource_disable_success</i18nString>
	<i18nString>invite_users</i18nString>
	<i18nString>users_invite_success</i18nString>
	<i18nString>invite_teams</i18nString>
	<i18nString>teams_invite_success</i18nString>
	<i18nString>revoke_users</i18nString>
	<i18nString>revoke_users_success</i18nString>
	<i18nString>revoke_teams</i18nString>
	<i18nString>revoke_teams_success</i18nString>
	<i18nString>select_all</i18nString>
	<i18nString>deselect_all</i18nString>
	<i18nString>add</i18nString>
	<i18nString>firstname</i18nString>
	<i18nString>lastname</i18nString>
	<i18nString>email</i18nString>
	<i18nString>revoke</i18nString>
	<i18nString>revoke_users_confirm_message</i18nString>
	<i18nString>revoke_teams_confirm_message</i18nString>
	<i18nString>new_profile</i18nString>
	<i18nString>create_profile</i18nString>
	<i18nString>close</i18nString>
	<i18nString>view_user_tooltip</i18nString>
	<i18nString>view_team_tooltip</i18nString>
	<i18nString>view_profile_tooltip</i18nString>
	<i18nString>top_of_page</i18nString>
	<i18nString>home_page</i18nString>
	<i18nString>no_more_users_to_invite</i18nString>
	<i18nString>no_more_users_to_revoke</i18nString>
	<i18nString>no_more_teams_to_invite</i18nString>
	<i18nString>no_more_teams_to_revoke</i18nString>
	<i18nString>change_profile_users</i18nString>
	<i18nString>change_profile_users_confirm_message</i18nString>
	<i18nString>select_users</i18nString>
	<i18nString>select_teams</i18nString>
	<i18nString>change_profile</i18nString>
	<i18nString>multiple_select</i18nString>
	<i18nString>change_profile_teams</i18nString>
	<i18nString>change_profile_teams_confirm_message</i18nString>
	<i18nString>is_public</i18nString>
	<i18nString>is_private</i18nString>
	<i18nString>application_public</i18nString>
	<i18nString>application_private</i18nString>
	<i18nString>public_mode_toggle_failed</i18nString>
	<i18nString>delete_profile</i18nString>
	<i18nString>all</i18nString>
</mioga:strings>

<xsl:template match="DisplayMain" mode="head">
	<script type="text/javascript">
		$(document).ready (function () {
			$('#fouquet').fouquet ({
				i18n: {
					<!-- UI translation -->
					<xsl:apply-templates select="document('')/*/mioga:strings/i18nString"/>
				}
			});
		});
	</script>
</xsl:template>

<xsl:template match="DisplayMain">
	<div id="fouquet"></div>
</xsl:template>

<xsl:template match="i18nString">
	<xsl:variable name="translation"><xsl:call-template name="escapeString"/></xsl:variable>
	<xsl:if test="$translation!=.">
		"<xsl:value-of select="."/>": "<xsl:value-of select="$translation"/>"<xsl:if test="position() != last()">,</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="escapeString">
	<!-- Escape newlines -->
	<xsl:call-template name="escapeNewline">
		<xsl:with-param name="pText">
			<!-- Escape quotes -->
			<xsl:call-template name="escapeQuote">
				<xsl:with-param name="pText">
					<xsl:value-of select="mioga:gettext (.)"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:with-param>
	</xsl:call-template>
</xsl:template>

<xsl:template name="escapeQuote">
	<xsl:param name="pText"/>

	<xsl:if test="string-length($pText) >0">
		<xsl:value-of select="substring-before(concat($pText, '&quot;'), '&quot;')"/>

		<xsl:if test="contains($pText, '&quot;')">
			<xsl:text>\"</xsl:text>

			<xsl:call-template name="escapeQuote">
				<xsl:with-param name="pText" select="substring-after($pText, '&quot;')"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="escapeNewline">
	<xsl:param name="pText"/>

	<xsl:if test="string-length($pText) >0">
		<xsl:value-of select= "substring-before(concat($pText, '&#10;'), '&#10;')"/>

		<xsl:if test="contains($pText, '&#10;')">
			<xsl:text>\n</xsl:text>

			<xsl:call-template name="escapeNewline">
				<xsl:with-param name="pText" select="substring-after($pText, '&#10;')"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>
