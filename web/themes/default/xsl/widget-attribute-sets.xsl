<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
<!--  <xsl:output method="html"/> -->


  <!-- =================================
       body_attr
       
       Attributes set for <body> html tags
       of main mioga window.
       
       ================================= -->
  
  <xsl:attribute-set name="body_attr">
	  <xsl:attribute name="class">mioga-body</xsl:attribute>
  </xsl:attribute-set>


  <!-- =================================
       app_body_attr
       
       Attributes set for application body div.
       
       ================================= -->

  <xsl:attribute-set name="app_body_attr">
      <xsl:attribute name="class">mioga-app-body</xsl:attribute>
  </xsl:attribute-set>


  <!-- =================================
       app_form_body_attr
       
       Attributes set for application form body div.
       
       ================================= -->

  <xsl:attribute-set name="app_form_body_attr">
	  <xsl:attribute name="class">mioga-app-form-body</xsl:attribute>
  </xsl:attribute-set>



  <!-- =================================
       app_message_body_attr
       
       Attributes set for message pages body div.
       
       ================================= -->

  <xsl:attribute-set name="app_message_body_attr">
	  <xsl:attribute name="class">mioga-app-message-body</xsl:attribute>
  </xsl:attribute-set>
  
  <!-- =================================
       app_footer_attr
       
       Attributes set for application footer div.
       
       ================================= -->
  
  <xsl:attribute-set name="app_footer_attr">
	  <xsl:attribute name="class">mioga-app-footer</xsl:attribute>
  </xsl:attribute-set>



  <!-- =================================
       workspace_menu_body_attr

       Attributes for workspace menu body

       ================================= -->

  <xsl:attribute-set name="workspace_menu_body_attr">
	  <xsl:attribute name="class">workspace-menu-body</xsl:attribute>
  </xsl:attribute-set>

  <!-- =================================
       mioga-empty-table-100
       
       Attributes set for table without 
       border with 100% width.
       
       ================================= -->

  <xsl:attribute-set name="mioga-empty-table-100">
	  <xsl:attribute name="class">mioga-empty-table-100</xsl:attribute>
      <xsl:attribute name="cellspacing">0</xsl:attribute>
      <xsl:attribute name="cellpadding">0</xsl:attribute>
  </xsl:attribute-set>
  
  <!-- =================================
       mioga-empty-table-100
       
       Attributes set for table without 
       border.
       
       ================================= -->

  <xsl:attribute-set name="mioga-empty-table">
	  <xsl:attribute name="class">mioga-empty-table</xsl:attribute>
      <xsl:attribute name="cellspacing">0</xsl:attribute>
      <xsl:attribute name="cellpadding">0</xsl:attribute>
  </xsl:attribute-set>
  

  <!-- =================================
       mioga-border-table
       
       Attributes set for bordered table.
       
       ================================= -->

  <xsl:attribute-set name="mioga-border-table">
	  <xsl:attribute name="class">mioga-border-table</xsl:attribute>
      <xsl:attribute name="cellspacing">0</xsl:attribute>
      <xsl:attribute name="cellpadding">0</xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:attribute-set name="mioga-content-table">
  	<xsl:attribute name="class">mioga-content-table</xsl:attribute>
    <xsl:attribute name="cellspacing">1</xsl:attribute>
    <xsl:attribute name="cellpadding">3</xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:attribute-set name="mioga-box-title">
	<xsl:attribute name="class">mioga-box-title</xsl:attribute>
  </xsl:attribute-set>


  <!-- =================================
       mioga-form-input-label-attr
       
       form inputs label styles.
       
       ================================= -->

  <xsl:attribute-set name="mioga-form-input-label-attr">
	  <xsl:attribute name="class">mioga-form-input-label</xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:attribute-set name="mioga-form-input-error-label-attr">
	  <xsl:attribute name="class">mioga-form-input-error-label</xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:attribute-set name="mioga-form-input-value-attr">
	  <xsl:attribute name="class">mioga-form-input-value</xsl:attribute>
  </xsl:attribute-set>


  <!-- obsolete attribute set -->
  <xsl:attribute-set name="mioga-form-table">
	  <xsl:attribute name="class">mioga-form-table</xsl:attribute>
      <xsl:attribute name="cellspacing">0</xsl:attribute>
      <xsl:attribute name="cellpadding">5</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="mioga-form-table-title">
	  <xsl:attribute name="class">mioga-form-table-title</xsl:attribute>
  </xsl:attribute-set>


  <!-- =================================
       mioga-title-attr
       
       Style of mioga titles
       
       ================================= -->
  
  <xsl:attribute-set name="mioga-title-attr">
	  <xsl:attribute name="class">mioga-title</xsl:attribute>
  </xsl:attribute-set>
  
  <!-- =================================
       mioga_message_attr
       
       Style of mioga messages
       
       ================================= -->
  
  <xsl:attribute-set name="mioga_message_attr">
	  <xsl:attribute name="class">mioga-message</xsl:attribute>
  </xsl:attribute-set>
  

  <!-- =================================
       mioga-arg-error
       
       Style of mioga error messages
       
       ================================= -->
  
  <xsl:attribute-set name="mioga-arg-error">
	  <xsl:attribute name="class">mioga-arg-error</xsl:attribute>
  </xsl:attribute-set>
  


  <!-- =================================
       mioga-blue-border
       
       Mioga Bordered table
       
       ================================= -->
  
  <xsl:attribute-set name="mioga-bordered">
	  <xsl:attribute name="class">mioga-bordered</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>
