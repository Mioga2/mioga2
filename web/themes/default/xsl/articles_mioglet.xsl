<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:articles="http://www.mioga2.org/portal/mioglets/Articles">

<xsl:output method="html"/>

<!-- =================================
     Handle NewsList
     ================================= -->

<!-- Editor Form -->

<xsl:template match="articles:DisplayArticle" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">articles_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

	<xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">category</xsl:with-param>
        <xsl:with-param name="value" select="$node/category"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">number</xsl:with-param>
        <xsl:with-param name="value" select="$node/number"/>
    </xsl:call-template>
    
</xsl:template>


<!-- =======================
	DisplayArticle
	======================= -->

<xsl:template match="articles:DisplayArticle">
<xsl:param name="node"/>
<div class="article_mioglet">
	<h2><xsl:value-of select="title" /></h2>
	<p class="header"><xsl:value-of select="header" /></p>
	<div class="content">
	<xsl:value-of select="content" disable-output-escaping="yes" />
	</div>
</div>
</xsl:template>

<!-- error template -->
<xsl:template match="error">
	<span class="error"><xsl:value-of select="."/></span>
</xsl:template>

</xsl:stylesheet>
