<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>
<xsl:import href="jquery.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<!-- ===============
	root node
	================ -->

<xsl:template match="/">
	<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="console-js"/>
	<xsl:call-template name="objectkeys-js"/>
	<xsl:call-template name="jquery-js"/>
	<xsl:call-template name="jquery-form-js"/>
	<xsl:call-template name="jquery-ui-1.12.1"/>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.miniColors.css"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/fullcalendar.css"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery-ui-mioga2.css"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.extensibleList.css"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/deming.css"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/deming-print.css" media="print"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.dropDown.css"/>
	<xsl:apply-templates mode="head"/>
	<xsl:call-template name="theme-css"/>
</head>
<body class="deming mioga">
	<xsl:call-template name="DisplayAppTitle">
	</xsl:call-template>

	<xsl:apply-templates/>
</body>
</html>
</xsl:template>

<mioga:strings>
	<i18nString>progressBarText</i18nString>
	<i18nString>no_ui</i18nString>
	<i18nString>main_page</i18nString>
	<i18nString>manage_activities</i18nString>
	<i18nString>save</i18nString>
	<i18nString>validate</i18nString>
	<i18nString>cancel</i18nString>
	<i18nString>cancel_confirm</i18nString>
	<i18nString>add_activity</i18nString>
	<i18nString>add_external</i18nString>
	<i18nString>delete_activity</i18nString>
	<i18nString>create_subactivity</i18nString>
	<i18nString>foreground_color</i18nString>
	<i18nString>background_color</i18nString>
	<i18nString>change_colors</i18nString>
	<i18nString>close</i18nString>
	<i18nString>btn_ok</i18nString>
	<i18nString>btn_close</i18nString>
	<i18nString>activity</i18nString>
	<i18nString>sub_activity</i18nString>
	<i18nString>notes</i18nString>
	<i18nString>new_entry_title</i18nString>
	<i18nString>edit_entry_title</i18nString>
	<i18nString>recent_entries</i18nString>
	<i18nString>actions</i18nString>
	<i18nString>delete_entry</i18nString>
	<i18nString>copy_entry</i18nString>
	<i18nString>edit_entry</i18nString>
	<i18nString>confirm_entry_deletion</i18nString>
	<i18nString>copy_and_edit_entry</i18nString>
	<i18nString>slot10min</i18nString>
	<i18nString>slot15min</i18nString>
	<i18nString>slot30min</i18nString>
	<i18nString>time_range</i18nString>
	<i18nString>preferences</i18nString>
	<i18nString>first_hour</i18nString>
	<i18nString>day_length</i18nString>
	<i18nString>hourText</i18nString>
	<i18nString>minuteText</i18nString>
	<i18nString>default_activity</i18nString>
	<i18nString>apply_filter</i18nString>
	<i18nString>report_column_user_label</i18nString>
	<i18nString>report_column_activity_label</i18nString>
	<i18nString>report_column_sub_activity_label</i18nString>
	<i18nString>report_column_sub_sub_activity_label</i18nString>
	<i18nString>report_column_duration_label</i18nString>
	<i18nString>total_duration</i18nString>
	<i18nString>days</i18nString>
	<i18nString>csv_raw_export</i18nString>
	<i18nString>csv_synthesis_export</i18nString>
	<i18nString>user_report_view</i18nString>
	<i18nString>calendar_view</i18nString>
	<i18nString>monday</i18nString>
	<i18nString>tuesday</i18nString>
	<i18nString>wednesday</i18nString>
	<i18nString>thursday</i18nString>
	<i18nString>friday</i18nString>
	<i18nString>chronos_import</i18nString>
	<i18nString>my_calendars</i18nString>
	<i18nString>my_groups_calendars</i18nString>
	<i18nString>click_here_to_import</i18nString>
	<i18nString>prev_day</i18nString>
	<i18nString>next_day</i18nString>
	<i18nString>show_weekends</i18nString>
	<i18nString>weekly_total</i18nString>
	<i18nString>all_day_event</i18nString>
	<i18nString>overlaps_event</i18nString>
	<i18nString>weekly_total_unit</i18nString>
	<i18nString>all_sub_activities</i18nString>
	<i18nString>report_column_dtstart_label</i18nString>
	<i18nString>report_column_dtend_label</i18nString>
	<i18nString>report_column_notes_label</i18nString>
	<i18nString>view</i18nString>
	<i18nString>categoryLegend</i18nString>
	<i18nString>reattachWindow</i18nString>
</mioga:strings>

<!-- ===============
	DisplayMain
	================ -->

<xsl:template match="/DisplayMain" mode="head">
	<script src="{$theme_uri}/javascript/deming.js" type="text/javascript"></script>
	<script type="text/javascript">
		<!-- MIOGA.debug.Deming = 1; -->
		<!-- MIOGA.debug.DemingMainPage = 1; -->
		<!-- MIOGA.debug.DemingActivities = 1; -->
		<!-- MIOGA.debug.DemingActivitiesEdit = 1; -->
		<!-- MIOGA.debug.DemingEntry = 1; -->
		<!-- MIOGA.debug.DemingPreferencesEdit = 1; -->
		$(document).ready (function () {
			$('.deming-main-cont').deming ({
				i18n: {
					<!-- UI translation -->
					<xsl:apply-templates select="document('')/*/mioga:strings/i18nString"/>
				},
				calendar: {
					dayNamesShort : ["<xsl:value-of select="mioga:gettext('Sun')"/>",
								   "<xsl:value-of select="mioga:gettext('Mon')"/>",
								   "<xsl:value-of select="mioga:gettext('Tue')"/>",
								   "<xsl:value-of select="mioga:gettext('Wed')"/>",
								   "<xsl:value-of select="mioga:gettext('Thu')"/>",
								   "<xsl:value-of select="mioga:gettext('Fri')"/>",
								   "<xsl:value-of select="mioga:gettext('Sat')"/>" ],
					dayNames : ["<xsl:value-of select="mioga:gettext('Sunday')"/>",
								   "<xsl:value-of select="mioga:gettext('Monday')"/>",
								   "<xsl:value-of select="mioga:gettext('Tuesday')"/>",
								   "<xsl:value-of select="mioga:gettext('Wednesday')"/>",
								   "<xsl:value-of select="mioga:gettext('Thursday')"/>",
								   "<xsl:value-of select="mioga:gettext('Friday')"/>",
								   "<xsl:value-of select="mioga:gettext('Saturday')"/>" ],
					monthNames:[ "<xsl:value-of select="mioga:gettext('January')"/>",
								 "<xsl:value-of select="mioga:gettext('February')"/>",
								 "<xsl:value-of select="mioga:gettext('March')"/>",
								 "<xsl:value-of select="mioga:gettext('April')"/>",
								 "<xsl:value-of select="mioga:gettext('May')"/>",
								 "<xsl:value-of select="mioga:gettext('June')"/>",
								 "<xsl:value-of select="mioga:gettext('July')"/>",
								 "<xsl:value-of select="mioga:gettext('August')"/>",
								 "<xsl:value-of select="mioga:gettext('September')"/>",
								 "<xsl:value-of select="mioga:gettext('October')"/>",
								 "<xsl:value-of select="mioga:gettext('November')"/>",
								 "<xsl:value-of select="mioga:gettext('December')"/>" ],
					monthNamesShort:[ "<xsl:value-of select="mioga:gettext('Jan')"/>",
								 "<xsl:value-of select="mioga:gettext('Feb')"/>",
								 "<xsl:value-of select="mioga:gettext('Mar')"/>",
								 "<xsl:value-of select="mioga:gettext('Apr')"/>",
								 "<xsl:value-of select="mioga:gettext('May')"/>",
								 "<xsl:value-of select="mioga:gettext('Jun')"/>",
								 "<xsl:value-of select="mioga:gettext('Jul')"/>",
								 "<xsl:value-of select="mioga:gettext('Aug')"/>",
								 "<xsl:value-of select="mioga:gettext('Sep')"/>",
								 "<xsl:value-of select="mioga:gettext('Oct')"/>",
								 "<xsl:value-of select="mioga:gettext('Nov')"/>",
								 "<xsl:value-of select="mioga:gettext('Dec')"/>" ],
					buttonText : { today : "<xsl:value-of select="mioga:gettext('today')"/>" },
					columnFormat : { week: "<xsl:value-of select="mioga:gettext('ddd M/d')"/>" },
					titleFormat : { week: "d[ MMMM yyyy]{ '&#8212; 'd MMMM yyyy}" },
					"axisFormat":"HH:mm",
					<!-- This only handles numeric values, some more work to be done if something else is needed -->
					<xsl:apply-templates select="/DisplayMain/UserPrefs/*"/>
				},
				sources: {
					<xsl:apply-templates select="/DisplayMain/ExternalSource"/>
				}
			});
		});
	</script>
</xsl:template>

<xsl:template match="/DisplayMain">
	<div class="deming-main-cont"></div>
</xsl:template>

<!-- This only handles numeric values, some more work to be done if something else is needed -->
<xsl:template match="/DisplayMain/UserPrefs/*">
	<xsl:choose>
		<xsl:when test="name(.) = 'dayLength'">
			"<xsl:value-of select="name(.)"/>": "<xsl:value-of select="."/>"<xsl:if test="position() != last()">,</xsl:if>
		</xsl:when>
		<xsl:otherwise>
			"<xsl:value-of select="name(.)"/>": <xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="/DisplayReport/ExternalSource">
	<xsl:value-of select="rowid"/>: "<xsl:value-of select="source_ident"/>"<xsl:if test="position() != last()">,</xsl:if>
</xsl:template>

<xsl:template match="/DisplayMain/ExternalSource">
	<xsl:value-of select="rowid"/>: "<xsl:value-of select="source_ident"/>"<xsl:if test="position() != last()">,</xsl:if>
</xsl:template>


<!-- ===============
	DisplayReport
	================ -->

<xsl:template match="/DisplayReport" mode="head">
	<script src="{$theme_uri}/javascript/deming-report.js" type="text/javascript"></script>
	<script type="text/javascript">
		<!-- MIOGA.debug.Deming = 1; -->
		<!-- MIOGA.debug.DemingMainPage = 1; -->
		<!-- MIOGA.debug.DemingActivities = 1; -->
		<!-- MIOGA.debug.DemingActivitiesEdit = 1; -->
		<!-- MIOGA.debug.DemingEntry = 1; -->
		<!-- MIOGA.debug.DemingPreferencesEdit = 1; -->
		<!-- MIOGA.debug.DemingReportMainPage = 1; -->
		$(document).ready (function () {
			var $activities = $('#activity-filter');

			var $groups = $('#group-filter');

			var $dates = $('div.dates');

			var $grouping = $('div.grouping');

			$('.deming-main-cont').deming ({
				i18n: {
					<!-- UI translation -->
					<xsl:apply-templates select="document('')/*/mioga:strings/i18nString"/>
				},
				filter: {
					activities: $activities,
					groups: $groups,
					dates: $dates,
					grouping: $grouping
				},
				sources: {
					<xsl:apply-templates select="/DisplayReport/ExternalSource"/>
				}
			});
		});
	</script>
</xsl:template>

<!-- For case-insensitive sorting -->
<xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />

<xsl:template match="/DisplayReport">
	<div class="deming-report">
		<div class="filter form">
			<ul class="button_list"></ul>

			<div id="date-filter" class="dates form">
				<fieldset>
					<legend><xsl:value-of select="mioga:gettext ('Dates')"/></legend>
					<div class="form-item">
						<label for="start"><xsl:value-of select="mioga:gettext ('Start')"/></label>
						<input type="text" name="start"/>
					</div>
					<div class="form-item">
						<label for="end"><xsl:value-of select="mioga:gettext ('End')"/></label>
						<input type="text" name="end"/>
					</div>
				</fieldset>
			</div>

			<div id="activity-filter">
				<fieldset>
					<legend><xsl:value-of select="mioga:gettext ('Activities')"/></legend>
					<div class="form-item">
						<label for="all_activities"><xsl:value-of select="mioga:gettext ('All activities')"/></label>
						<input type="radio" id="all_activities" name="activity" value="all" checked="checked"/>
					</div>
					<div class="form-item">
						<label for="select_activity"><xsl:value-of select="mioga:gettext ('Select an activity')"/></label>
						<input type="radio" id="select_activity" name="activity" value="single"/>
					</div>
					<div class="form-item activity-selector" style="display: none">
						<label for="activity"><xsl:value-of select="mioga:gettext ('Activity')"/></label>
						<select name="activity" id="activity"></select>
					</div>
					<div class="form-item sub-activity-selector" style="display: none">
						<label for="sub_activity"><span></span></label>
						<select name="sub_activity" id="sub_activity"></select>
					</div>
				</fieldset>
			</div>

			<!--
			<div id="group-filter">
				<fieldset>
					<legend><xsl:value-of select="mioga:gettext ('Groups')"/></legend>
					<div class="form-item">
						<label for="all_groups"><xsl:value-of select="mioga:gettext ('All groups')"/></label>
						<input type="radio" id="all_groups" name="group" value="all" checked="checked"/>
					</div>
					<div class="form-item">
						<label for="select_group"><xsl:value-of select="mioga:gettext ('Select a group')"/></label>
						<input type="radio" id="select_group" name="group" value="single"/>
					</div>
					<div class="form-item group-selector" style="display: none">
						<label for="group"><xsl:value-of select="mioga:gettext ('Group')"/></label>
						<select name="group" id="group">
							<xsl:apply-templates select="Groups">
								<xsl:sort select="ident"/>
							</xsl:apply-templates>
						</select>
					</div>
				</fieldset>
			</div>
			-->
		</div>
		<div class="deming-main-cont">
		</div>
	</div>
</xsl:template>

<xsl:template match="Groups">
	<option>
		<xsl:attribute name="value"><xsl:value-of select="rowid"/></xsl:attribute>
		<xsl:value-of select="ident"/>
	</option>
</xsl:template>

<xsl:template match="i18nString">
	<xsl:variable name="translation"><xsl:call-template name="escapeString"/></xsl:variable>
	<xsl:if test="$translation!=.">
		"<xsl:value-of select="."/>": "<xsl:value-of select="$translation"/>"<xsl:if test="position() != last()">,</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="escapeString">
	<!-- Escape newlines -->
	<xsl:call-template name="escapeNewline">
		<xsl:with-param name="pText">
			<!-- Escape quotes -->
			<xsl:call-template name="escapeQuote">
				<xsl:with-param name="pText">
					<xsl:value-of select="mioga:gettext (.)"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:with-param>
	</xsl:call-template>
</xsl:template>

<xsl:template name="escapeQuote">
	<xsl:param name="pText"/>

	<xsl:if test="string-length($pText) >0">
		<xsl:value-of select="substring-before(concat($pText, '&quot;'), '&quot;')"/>

		<xsl:if test="contains($pText, '&quot;')">
			<xsl:text>\"</xsl:text>

			<xsl:call-template name="escapeQuote">
				<xsl:with-param name="pText" select="substring-after($pText, '&quot;')"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:if>
</xsl:template>

<xsl:template name="escapeNewline">
	<xsl:param name="pText"/>

	<xsl:if test="string-length($pText) >0">
		<xsl:value-of select= "substring-before(concat($pText, '&#10;'), '&#10;')"/>

		<xsl:if test="contains($pText, '&#10;')">
			<xsl:text>\n</xsl:text>

			<xsl:call-template name="escapeNewline">
				<xsl:with-param name="pText" select="substring-after($pText, '&#10;')"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>
