<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >

<!-- ===============
	Grid
	================ -->

<xsl:template match="Grid" mode="head">
	<xsl:call-template name="dojo-grid-css"/>

	<script src="{$theme_uri}/javascript/grid.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/grid.css" media="screen" />

	<script type="text/javascript">
		dojo.requireLocalization("mioga", "grid");
		var transl = dojo.i18n.getLocalization("mioga", "grid");
		dojo.require ("dojo.io.iframe");
		dojo.require ("mioga.data.ItemFileReadStore");
		<!--
		dojo.require("dijit.Toolbar");
		dojo.require("dijit.form.DropDownButton");
		dojo.require("dijit.TooltipDialog");
		dojo.require("dijit.ColorPalette");
		dojo.require("dijit.form.TextBox");
		-->
		var grid_underlay;

		var filter_message = '<xsl:value-of select="mioga:gettext ('A filter is active')"/>';
		var store_url = '<xsl:value-of select="GetData" />';

		dojo.addOnLoad (function () {
			if (dijit.byId (filter_form_id) === undefined) {
				// No filter at all, just refresh grid
				RefreshGrid ();
			}
		});

		function ActionCallback(gridId) {
			console.debug('ActionCallback');
			var grid = dijit.byId(gridId);
        	console.debug('grid ' + grid);
			var item = grid.selection.getFirstSelected();
			if (item) {
				console.debug('item ' + item);
				//console.dir(item);
				<xsl:value-of select="DefaultAction" />(item.rowid);
			}
			else {
				var transl = dojo.i18n.getLocalization("mioga", "grid");
				alert(transl.NoSelection);
			}
		}

		function ShowGridUnderlay () {
			if (!grid_underlay) {
				grid_underlay = new dijit.DialogUnderlay ({'class': 'loading', 'id': 'grid_underlay'});
			}
			grid_underlay.show ();
		}

		function ProcessSelectionAction(gridId, method, type, func, post_var, error_function) {
			console.debug('ProcessSelectionAction  method : ' + method);
			if (post_var == '') post_var = 'rowids';
			var grid = dojo.getObject(gridId);
        	console.debug('grid ' + grid);
			var items = grid.selection.getSelected();
        	console.debug('items ' + items);
			if (items.length) {
        		console.debug('length '+items.length);
				var rowids = new Array();
				dojo.forEach(items, function(selItem, index, array) {
        			console.debug('selItem ' + selItem + ' index ' + index + ' array ' + array[index]);
					if (selItem) {
						var val = grid.store.getValues(selItem, 'rowid');
						console.debug('val ' + val);
						rowids.push(val);
					}
				});
        		console.debug('rowids ' + rowids);

				var content = new Object ();
				content[post_var] = rowids;

				// Get additional data
				var args = eval (func);
				for (key in args) {
					content[key] = args[key];
				}

				if (type == 'file') {
				console.log ("Type file");
					dojo.io.iframe.send ({
						form: dojo.byId ('fake-form'),
						timeout: 3000,
						url: method,
						method: 'post',
						content: content
					});
				}
				else {
					var handleAs = '';
					if (type == 'json') handleAs = 'json';
					ShowGridUnderlay ();
					dojo.xhrPost({
						url: method,
						//timeout: 30000, // give up after 3 seconds
						content: content,
						handleAs: handleAs,
						load: function(response, ioArgs){
							console.debug("load function response : " + response);
							if ((response == 'OK') || (type == 'json' &amp;&amp; !response.errors)) {
								// Get additional data
								var getargs = '';
								var args = eval (func);
								for (key in args) {
									if (getargs == '') {
										getargs += '?';
									}
									else {
										getargs += '&amp;';
									}
									getargs += key + '=' + args[key];
								}
								<!-- The two commented lines below are too specific and can not be compatible with Colbert (or other applications) WebServices,
									 it has been replaced by a JS call to RefreshGrid function that each application that needs to use a grid should provide -->
								<!-- grid.setStore(new mioga.data.ItemFileReadStore({url:'<xsl:value-of select="GetData" />'+getargs})); -->
								<!-- grid.update(); -->
								grid_underlay.hide ();
								DeselectAll (grid_id);
								RefreshGrid ();
							}
							else {
								grid_underlay.hide ();
								if (error_function != '') {
									window [error_function] (response.errors);
								}
								else {
									var transl = dojo.i18n.getLocalization("mioga", "grid");
									alert(trans.UnhandledError + response);
								}
							}
						},
						error: function(err, ioArgs){
							grid_underlay.hide ();
							var transl = dojo.i18n.getLocalization("mioga", "grid");
							var dialog = new dijit.Dialog({
								title: transl['generic_error_title'],
								content: '&lt;div style="width: 100%;"&gt;&lt;p&gt;&lt;b&gt;' + transl['generic_error'] + '&lt;/b&gt;&lt;/p&gt;' + ioArgs.xhr.responseText + '&lt;/div&gt;'
							});
							dialog.show ();
							// The two lines below are because of a bad rendering with Chromium
							dojo.style(dialog.titleBar, { width: '100%' });
							dojo.style(dialog.containerNode, { width: '100%' });
							console.error(err);
						}
					});
				}
			}
			else {
				var transl = dojo.i18n.getLocalization("mioga", "grid");
				alert(transl.NoSelection);
			}
		}

		function DeselectAll(gridId) {
			var grid = dojo.getObject(gridId);
			grid.selection.deselectAll();
		}

		function loadItems(widget){
			widget.store.fetch({
				start: 0,
				count: widget.rowCount,
				query: widget.query,
				sort: widget.getSortProps(),
				queryOptions: widget.queryOptions,
				isRender: widget.isRender,
				onBegin: dojo.hitch(widget, "_onFetchBegin"),
				onComplete: dojo.hitch(widget, "_onFetchComplete"),
				onError: dojo.hitch(widget, "_onFetchError")
			});
		}


		function SelectAll(gridId) {
        	console.debug("SelectAll");
			var grid = dojo.getObject(gridId);
			loadItems(grid);
        	console.debug("rowCount" + grid.rowCount);
			var i;
			for (i=0;i&lt;grid.rowCount;i++) grid.selection.addToSelection(i);
		}

		function FormatMailto(value, rowIdx, cell) {
			SetClass (value, rowIdx, cell);
			return '&lt;a href="mailto:'+ value + '"&gt;'+ value + '&lt;/a&gt;';
		}

		function FormatBoolean(value, rowIdx, cell) {
			SetClass (value, rowIdx, cell);
			var classname = "grid-bool-false";
			if (value == '1') {
				classname = "grid-bool-true";
			}
			return '&lt;div class="' + classname + '" &gt;&lt;/div&gt;';
		}

		function FormatFilesize(value, rowIdx, cell) {
			SetClass (value, rowIdx, cell);
			var new_val = value;
			// TODO translation
			if (value > 1048576) {
				new_val = Math.ceil((value / 1048576) + 0.5) + ' Mo';
			}
			else if (value > 1024) {
				new_val = Math.ceil((value / 1024) + 0.5) + ' Ko';
			}
			return new_val;
		}

		function FormatDate (value, rowIdx, cell) {
			SetClass (value, rowIdx, cell);
			var date = '';
			if (value &amp;&amp; value != '') {
				var fields = value.match (/(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/);
				var date = 'YYYY-MM-DD hh:mm:ss';
				date = date.replace (/YYYY/g, fields[1]);
				date = date.replace (/MM/g, fields[2]);
				date = date.replace (/DD/g, fields[3]);
				date = date.replace (/hh/g, fields[4]);
				date = date.replace (/mm/g, fields[5]);
				date = date.replace (/ss/g, fields[6]);
			}

			return (date);
		}

		function UnescapeHTML (value, rowIdx, cell) {
			SetClass (value, rowIdx, cell);
			value = value.replace (/&amp;lt;/g, '&lt;');
			value = value.replace (/&amp;gt;/g, '&gt;');

			return (value);
		}

		function UnescapeJSON (value, rowIdx, cell) {
			SetClass (value, rowIdx, cell);
			if (typeof (value) === 'string') {
				value = value.replace (/\\\'/g, "'");
			}

			return (value);
		}

		function SetClass (value, rowIdx, cell) {
			cell.customClasses.push(cell.id);
			return (value);
		}
	</script>
</xsl:template>

<xsl:template match="Grid">
	<xsl:param name="ident" />
	<xsl:param name="grid_id" />
	<xsl:param name="store_id" />
	<xsl:param name="read_only" />
	<xsl:param name="default_sort_field" />
	<xsl:param name="store_prefs" />
	<xsl:param name="dojo_i18n_bundle" />
	<!--
	<xsl:variable name="ident"><xsl:value-of select="ident" /></xsl:variable>
	<xsl:variable name="grid_id">grid_<xsl:value-of select="ident" /></xsl:variable>
	<xsl:variable name="store_id">store_<xsl:value-of select="ident" /></xsl:variable>
	-->
	<xsl:variable name="dialog_id">filter_dialog_<xsl:value-of select="ident" /></xsl:variable>
	<xsl:variable name="form_id">filter_form_<xsl:value-of select="$ident" /></xsl:variable>

	<!-- Select correct sort field -->
	<xsl:variable name="sort_field">
		<xsl:choose>
			<xsl:when test="SortField != ''">
				<xsl:value-of select="SortField"/>
			</xsl:when>
			<xsl:when test="$default_sort_field != ''">
				<xsl:value-of select="$default_sort_field"/>
			</xsl:when>
		</xsl:choose>
	</xsl:variable>

	<!-- Sort by first column unless sort_field is specified -->
	<xsl:if test="$sort_field=''">
		<script text="text/javascript">
			dojo.addOnLoad (function () {
				var handle = dojo.connect (<xsl:value-of select="$grid_id"/>, '_onFetchComplete', function () {
					dojo.disconnect (handle);
					<xsl:value-of select="$grid_id"/>.setSortIndex (0);
				});
			});
		</script>
	</xsl:if>

		<div id="border_{$ident}" dojoType="dijit.layout.BorderContainer" class="gridContainer" style="height:100%; width:100%" gutters="false">

			<!-- Fake form to have dojo.io.iframe.send finally POSTing instead of GETting -->
			<form id="fake-form" method="POST" style="display: none;"></form>

			<div id="tb_{$ident}" dojoType="dijit.Toolbar" region="top" style="margin:0;padding:0;" splitter="false">
				<div id="selection_dropdown" dojoType="dijit.form.DropDownButton" showLabel="true">
					<span><xsl:value-of select="mioga:gettext('Selection')" /></span>
					<div dojoType="dijit.Menu" id="windowContextMenu" style="display: none;">
    					<div dojoType="dijit.MenuItem" onClick="SelectAll('{$grid_id}'); return false;">
							<xsl:value-of select="mioga:gettext('Select All')"/>
						</div>
    					<div dojoType="dijit.MenuItem" onClick="DeselectAll('{$grid_id}');">
							<xsl:value-of select="mioga:gettext('Unselect All')"/>
						</div>
						<xsl:if test="$read_only != 1">
							<xsl:if test="SelectAction">
								<div dojoType="dijit.MenuSeparator">
								</div>
								<xsl:for-each select="SelectAction">
									<div dojoType="dijit.MenuItem">
										<xsl:attribute name="onClick">if (<xsl:value-of select="./@check_function"/>) { ConfirmSelectionAction ("\"<xsl:value-of select="."/>\". "+ transl.ConfirmMsg, Array (<xsl:value-of select="//ConfirmFields"/>), '<xsl:value-of select="$dojo_i18n_bundle"/>', function () { return ProcessSelectionAction('<xsl:value-of select="$grid_id" />', '<xsl:value-of select="./@function" />', '<xsl:value-of select="./@type"/>', '<xsl:value-of select="./@func"/>', '<xsl:value-of select="./@post_var"/>', '<xsl:value-of select="./@error_function"/>');}) }</xsl:attribute>
										<xsl:value-of select="." />
									</div>
								</xsl:for-each>
							</xsl:if>
						</xsl:if>
					</div>
				</div>
				<xsl:if test="Filter/Field!=''">
					<div id="filter_dropdown" dojoType="dijit.form.DropDownButton" showLabel="true">
						<span>
							<xsl:value-of select="mioga:gettext('Filter')"/>
						</span>
						<div id="{$dialog_id}" dojoType="dijit.TooltipDialog" execute="Filter('{$form_id}', '{$grid_id}');" class="filter-form" >
							<xsl:apply-templates select="Filter">
								<xsl:with-param name="ident" select="$ident" />
								<xsl:with-param name="grid_id" select="$grid_id" />
								<xsl:with-param name="dialog_id" select="$dialog_id" />
								<xsl:with-param name="form_id" select="$form_id" />
							</xsl:apply-templates>
						</div>
					</div>
				</xsl:if>
			</div>

			<div dojoType="dijit.layout.ContentPane" region="center">
				<span dojoType="mioga.data.ItemFileReadStore" jsId="{$store_id}">
					<xsl:attribute name="nodename"><xsl:value-of select="NodeName"/></xsl:attribute>
					<xsl:attribute name="identifier"><xsl:value-of select="Identifier"/></xsl:attribute>
					<xsl:attribute name="label"><xsl:value-of select="Label"/></xsl:attribute>
					<xsl:attribute name="data">{}</xsl:attribute>
				</span>
				<table id="{$grid_id}" jsId="{$grid_id}" dojoType="dojox.grid.DataGrid" store="{$store_id}" clientSort="true" columnReordering="true">
					<!-- <xsl:attribute name="onRowDblClick"><xsl:value-of select="DefaultAction" /></xsl:attribute> -->
					<xsl:choose>
						<xsl:when test="$read_only != 1">
							<xsl:attribute name="onRowDblClick">ActionCallback('<xsl:value-of select="$grid_id" />');</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="onRowDblClick">alert ("<xsl:value-of select="mioga:gettext ('You do not have sufficient rights to edit such an object.')"/>");</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="$store_prefs != ''">
						<xsl:attribute name="onHeaderClick"><xsl:value-of select="$store_prefs"/> ();</xsl:attribute>
						<xsl:attribute name="onResizeColumn"><xsl:value-of select="$store_prefs"/> ();</xsl:attribute>
					</xsl:if>
					<thead>
						<tr>
							<script type="text/javascript">
								dojo.addOnLoad (function () {
									<xsl:value-of select="$store_id"/>.comparatorMap = {};
								});
							</script>
							<xsl:for-each select="Field">
								<xsl:variable name="field_ident"><xsl:value-of select="ident" /></xsl:variable>
								<xsl:variable name="field_label"><xsl:value-of select="label" /></xsl:variable>
								<xsl:if test="$sort_field = $field_ident">
									<!-- Current field is default sorting column, process it. setSortIndex to position-2 because index is 1-based and first column (rowid) is not displayed -->
									<script type="text/javascript">
										dojo.addOnLoad (function () {
											var handle = dojo.connect (<xsl:value-of select="$grid_id"/>, '_onFetchComplete', function () {
												dojo.disconnect (handle);
												<xsl:value-of select="$grid_id"/>.setSortIndex (<xsl:value-of select="position () - 2"/>);
											});
										});
									</script>
								</xsl:if>
								<xsl:choose>
									<xsl:when test="type='normal'">
										<th id="{$field_ident}" field="{$field_ident}" formatter="UnescapeJSON" width="{@width}"><xsl:value-of select="$field_label" /></th>
										<script type="text/javascript">
											dojo.addOnLoad (function () {
												<xsl:value-of select="$store_id"/>.comparatorMap['<xsl:value-of select="$field_ident"/>'] = function (a, b) {
													if ((typeof (a) == 'string') &amp;&amp; (typeof (b) == 'string')) {
														var a = unaccent (a.toLowerCase());
														var b = unaccent (b.toLowerCase());
													}

													if (a&gt;b) return 1;
													if (a&lt;b) return -1;
													return 0;
												};
											});
										</script>
									</xsl:when>
									<xsl:when test="type='asis'">
										<th id="{$field_ident}" field="{$field_ident}" formatter="UnescapeHTML" width="{@width}"><xsl:value-of select="$field_label" /></th>
									</xsl:when>
									<xsl:when test="type='date'">
										<th id="{$field_ident}" field="{$field_ident}" formatter="FormatDate" width="{@width}"><xsl:value-of select="$field_label" /></th>
									</xsl:when>
									<xsl:when test="type='email'">
										<th id="{$field_ident}" field="{$field_ident}" formatter="FormatMailto" width="{@width}"><xsl:value-of select="$field_label" /></th>
									</xsl:when>
									<xsl:when test="type='boolean'">
										<th id="{$field_ident}" field="{$field_ident}" formatter="FormatBoolean" width="{@width}"><xsl:value-of select="$field_label" /></th>
									</xsl:when>
									<xsl:when test="type='filesize'">
										<th id="{$field_ident}" field="{$field_ident}" formatter="FormatFilesize" width="{@width}"><xsl:value-of select="$field_label" /></th>
									</xsl:when>
								</xsl:choose>
							</xsl:for-each>
						</tr>
					</thead>
				</table>
			</div>
		</div>

		<!-- Confirmation dialog for selection actions -->
		<div id="ConfirmSelectionActionDialog" dojoType="dijit.Dialog" onCancel="CloseConfirmSelectionActionDialog ();">
			<div class="contents"></div>
			<ul class="button_list">
				<li><a class="button"><xsl:value-of select="mioga:gettext ('Confirm')"/></a></li>
				<li><a class="button cancel" onClick="CloseConfirmSelectionActionDialog ();"><xsl:value-of select="mioga:gettext ('Cancel')"/></a></li>
			</ul>
		</div>
</xsl:template>

<!-- ===============
	filter-dialog
	================ -->

<xsl:template match="Filter" >
	<xsl:param name="ident" />
	<xsl:param name="grid_id" />
	<xsl:param name="dialog_id" />
	<xsl:param name="form_id" />
		<script type="text/javascript">
			var field_counter = 0;
			<xsl:choose>
				<xsl:when test="@mode = 'javascript'">
					var js_filter = 1;
				</xsl:when>
				<xsl:otherwise>
					var js_filter = 0;
				</xsl:otherwise>
			</xsl:choose>
		</script>
		<div dojoType="dijit.form.Form" id="{$form_id}" jsId="{$form_id}" class="form" encType="multipart/form-data" action="" method="">
			<xsl:attribute name="onsubmit">return (false);</xsl:attribute>
			<fieldset>
				<legend><xsl:value-of select="mioga:gettext('Parameters')" /></legend>
				<div class="form-item">
					<label><xsl:value-of select="mioga:gettext ('Match type')"/></label>
					<select name="match" class="filter-field" dojoType="dijit.form.FilteringSelect">
						<option value="begins">
							<xsl:if test="@match = 'begins'">
								<xsl:attribute name="selected">1</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="mioga:gettext ('Begins with')"/>
						</option>
						<option value="contains">
							<xsl:if test="@match = 'contains'">
								<xsl:attribute name="selected">1</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="mioga:gettext ('Contains')"/>
						</option>
						<option value="ends">
							<xsl:if test="@match = 'ends'">
								<xsl:attribute name="selected">1</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="mioga:gettext ('Ends with')"/>
						</option>
					</select>
				</div>
				<xsl:for-each select="Field">
					<div class="form-item">
						<xsl:variable name="field_id"><xsl:value-of select="$form_id" />_<xsl:value-of select="ident" /></xsl:variable>
						<label for="{$field_id}"><xsl:value-of select="label" /></label>
						<xsl:choose>
							<xsl:when test="./@type='list'">
								<div dojoType="mioga.data.ItemFileReadStore" jsId="{$field_id}Store" label="label">
									<xsl:attribute name="url"><xsl:value-of select="url"/></xsl:attribute>
									<xsl:attribute name="nodename"><xsl:value-of select="nodename"/></xsl:attribute>
									<xsl:attribute name="identifier"><xsl:value-of select="identifier"/></xsl:attribute>
								</div>
								<input id="{$field_id}" class="filter-field" dojoType="dijit.form.FilteringSelect" store="{$field_id}Store" required="false">
									<xsl:attribute name="searchAttr">label</xsl:attribute>
									<xsl:attribute name="name"><xsl:value-of select="ident" /></xsl:attribute>
								</input>
								<button class="clear-filter-list" onclick="dijit.byId ('{$field_id}').attr ('value', '');">
									<xsl:attribute name="title"><xsl:value-of select="mioga:gettext ('Clear this value')"/></xsl:attribute>
								</button>
								<script type="text/javascript">
									dojo.addOnLoad (function () {
										field_counter++;
										<xsl:value-of select="$field_id"/>Store.fetch ({ onComplete: function () {
											dijit.byId ('<xsl:value-of select="$field_id"/>').attr ('value', '<xsl:value-of select="./value"/>');
											field_counter--;
											if (field_counter == 0) {
												Filter ("<xsl:value-of select="$form_id"/>", "<xsl:value-of select="$grid_id"/>");
											}
										}});
									})
								</script>
							</xsl:when>
							<xsl:otherwise>
								<input dojoType="dijit.form.TextBox" id="{$field_id}" class="filter-field" value="">
									<xsl:if test="@focus=1">
										<xsl:attribute name="tabindex">1</xsl:attribute>
									</xsl:if>
									<xsl:attribute name="name"><xsl:value-of select="ident" /></xsl:attribute>
									<xsl:attribute name="onkeypress">filter_list</xsl:attribute>
								</input>
								<script type="text/javascript">
									dojo.addOnLoad (function () {
										dijit.byId ('<xsl:value-of select="$field_id"/>').attr ('value', '<xsl:value-of select="./value"/>');
									})
								</script>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</xsl:for-each>
			</fieldset>
		</div>

		<ul class="button_list">
			<li>
				<a class="button">
					<xsl:attribute name="onClick">Filter ("<xsl:value-of select="$form_id"/>", "<xsl:value-of select="$grid_id"/>");</xsl:attribute>
					<!-- <xsl:attribute name="onClick"><xsl:value-of select="$form_id"/>.submit ();</xsl:attribute> -->
					<xsl:value-of select="mioga:gettext('OK')" />
				</a>
			</li>
			<li>
				<a class="button cancel">
					<xsl:attribute name="onClick">ClearFilter ("<xsl:value-of select="$form_id"/>", "<xsl:value-of select="$grid_id"/>");</xsl:attribute>
					<xsl:value-of select="mioga:gettext('Clear')" />
				</a>
			</li>
		</ul>

		<script type="text/javascript">
			dojo.ready (function () {
				if (field_counter == 0) {
					if (js_filter) {
						RefreshGrid ();
					}
					else {
						Filter ("<xsl:value-of select="$form_id"/>", "<xsl:value-of select="$grid_id"/>");
					}
				}
			});
		</script>
</xsl:template>

</xsl:stylesheet>

