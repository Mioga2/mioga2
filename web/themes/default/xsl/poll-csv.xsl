<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="text" indent="no" omit-xml-declaration="yes" encoding="UTF-8" media-type="text/comma-separated-values"/>

<xsl:template match="/">
    <xsl:apply-templates/>
</xsl:template>

<!--
=======================================
 ViewResults
=======================================
-->
<xsl:template match="ViewResults">
<xsl:for-each select="Results/Question">
<xsl:for-each select="Headers/Header"><xsl:if test="position() != 1">;</xsl:if>"<xsl:value-of select="."/>"</xsl:for-each>
<xsl:text>
</xsl:text>

<xsl:for-each select="Row">
<xsl:for-each select="Col"><xsl:if test="position() != 1">;</xsl:if>"<xsl:value-of select="."/>"</xsl:for-each>
<xsl:text>
</xsl:text></xsl:for-each>
<xsl:text>
</xsl:text></xsl:for-each>
</xsl:template>

</xsl:stylesheet>
