<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" encoding="UTF-8" media-type="text/html"/>

<xsl:template match="/">
<xsl:call-template name="doctype"/>
	<html>
		<head>
			<xsl:apply-templates mode="head"/>
		</head>
		<body>
			<xsl:apply-templates/>
		</body>
	</html>
</xsl:template>

<xsl:template match="SetTagList" mode="head">
	<script type="text/javascript">
		window.onload = function () {
			console.dir (document.getElementById ('import-taglist-errors'));
			parent.TaglistImportCallback (document.getElementById ('import-taglist-errors'));
		}
	</script>
</xsl:template>

<xsl:template match="SetTagList">
	<ul id="import-taglist-errors">
		<xsl:for-each select="errors">
			<li>
				<xsl:value-of select="mioga:gettext ('Line')"/>&#160;
				<xsl:value-of select="./line"/>
				:&#160;
				<xsl:value-of select="./error"/>
			</li>
		</xsl:for-each>
	</ul>
</xsl:template>


<xsl:template match="ImportUsers" mode="head">
</xsl:template>

<xsl:template match="ImportUsers">
	<div id="import-users-errors">
		<xsl:if test="Import != ''">
			<ul>
				<xsl:for-each select="Import">
					<li>
						<xsl:value-of select="mioga:gettext ('Line')"/>&#160;
						<xsl:value-of select="./line"/>
						:&#160;
						<xsl:value-of select="./error"/>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</div>
	<script type="text/javascript">
		window.onload = function () {
			parent.RefreshGrid ();
			parent.UserImportCallback (document.getElementById('import-users-errors'));
		}
	</script>
</xsl:template>

</xsl:stylesheet>
