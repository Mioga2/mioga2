<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:search="http://www.mioga2.org/portal/mioglets/Search">

<xsl:output method="html" encoding="UTF-8" />

<!-- =================================
     Handle Calendar
     ================================= -->

<!-- Editor Form -->

<xsl:template match="search:SearchForm" mode="editor">
    <xsl:param name="node"/>

    <xsl:call-template name="EditorBasicEntry">
        <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputHidden">
        <xsl:with-param name="label">stylesheet</xsl:with-param>
        <xsl:with-param name="value">search_mioglet.xsl</xsl:with-param>        
    </xsl:call-template>

</xsl:template>


<!-- main SearchForm template -->

<xsl:template match="search:SearchForm">
    <xsl:param name="node"/>

    <form method="POST" action="{$private_bin_uri}/Search/DisplayMain" name="DisplayMain">
        <input type="hidden" name="advances_search" value=""/>
        <input type="hidden" name="search_tags" value="on"/>
        <input type="hidden" name="search_description" value="on"/>
        <input type="hidden" name="search_text" value="on"/>
        <table class="searchSearchForm">
            <tr>
                <td><input type="text" name="query_string" maxlength="64" /></td>
                <td>
                    <xsl:call-template name="ok-form-button">
                        <xsl:with-param name="name">act_search</xsl:with-param>
                    </xsl:call-template>
                </td>
            </tr>
        </table>
    </form>
</xsl:template>

</xsl:stylesheet>
