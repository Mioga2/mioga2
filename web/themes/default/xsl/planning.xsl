<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
  <xsl:output method="html" indent="yes"/>
  <xsl:include href="base.xsl"/>
<xsl:include href="scriptaculous.xsl"/>
<xsl:include href="inline_message.xsl"/>

  <xsl:variable name="projectcolor">#bbffbb</xsl:variable>
  <xsl:variable name="defaultcolor">#aaaaaa</xsl:variable>
  <xsl:variable name="cell_width">50</xsl:variable>
  <xsl:variable name="begin_day_width">5</xsl:variable>
  <xsl:variable name="end_day_width">5</xsl:variable>
  <xsl:variable name="day_width">40</xsl:variable>

<xsl:template name="planning-js">
    <script src="{$theme_uri}/javascript/planning.js" type="text/javascript">
    </script>
</xsl:template>

<!-- Root document
     ========== -->
<xsl:template match="/">

<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/planning.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="{$theme_uri}/css/planning-print.css" media="print" />
	<xsl:call-template name="mioga-js" />
    <xsl:call-template name="calendar-js" />
    <xsl:call-template name="scriptaculous-js"/>
    <xsl:call-template name="planning-js"/>
    <xsl:call-template name="favicon" />
    <title>Mioga - Planning</title>
    <script type="text/javascript">
			function dynamicElts() {
			  createCopy();
			  Element.observe(window, 'scroll', adjustCopy);
			  Element.observe(window, 'scroll', positionTime);
			  Element.observe(window, 'resize', adjustCopy);
			  Element.observe(window, 'resize', positionTime);
			  Element.observe('planning', 'mousedown', beginSelection);
			  range_link = '<xsl:value-of select="$private_bin_uri"/><xsl:value-of select="//ViewType/@app"/>';
			  positionTime();
			}
    </script>
	<xsl:call-template name="theme-css"/>
</head>
    <xsl:apply-templates />
</html>
</xsl:template>


<!-- DisplayMain
     ========== -->

<xsl:template match="DisplayMain">
	<body xsl:use-attribute-sets="body_attr">
	  <xsl:if test="ViewType/@type != 'week'">
	   <xsl:attribute name="onload">dynamicElts();</xsl:attribute>
	  </xsl:if>
		<xsl:call-template name="DisplayAppTitle">
			<xsl:with-param name="help">gestiontemps.html#planning</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="MiogaTitle"/>
		<p>
		  <font color="red">
			<b>
			  <xsl:call-template name="errorCode"/>
			</b>
		  </font>
		</p>



<form action="DisplayMain" method="GET">
<input type="hidden" name="view_type">
	<xsl:attribute name="value"><xsl:value-of select="ViewType/@type" /></xsl:attribute>
</input>
<div id="form" class="{$mioga-border-color}" style="border:1px solid;">
	<table style="width:100%;">
	<tr>
		<td>
			<a href="DisplayMain?year">
				<xsl:attribute name="href">
					DisplayMain?year=<xsl:value-of select="miogacontext/time/year" />&amp;month=<xsl:value-of select="miogacontext/time/month" />&amp;day=<xsl:value-of select="miogacontext/time/day" />&amp;nb_week=<xsl:value-of select="Params/@nb_week" />&amp;view_type=<xsl:value-of select="ViewType/@type" />
				</xsl:attribute>
        <xsl:value-of select="mioga:gettext('Today')"/>
			</a>
		</td>
		<td>
			<table>
			<xsl:call-template name="MiogaFormInputDate">
				<xsl:with-param name="label">date</xsl:with-param>
				<xsl:with-param name="year-value" select="Params/@year"/>
				<xsl:with-param name="month-value" select="Params/@month"/>
				<xsl:with-param name="day-value" select="Params/@day"/>
				<xsl:with-param name="year-name">year</xsl:with-param>
				<xsl:with-param name="month-name">month</xsl:with-param>
				<xsl:with-param name="day-name">day</xsl:with-param>
				<xsl:with-param name="first-year" select="Params/@year - 2"/>
				<xsl:with-param name="nb-year" select="10"/>
			</xsl:call-template>
			</table>
		</td>
	<xsl:if test="ViewType/@type='month'">
		<td>
			<table>
			<xsl:call-template name="MiogaFormInputText">
				<xsl:with-param name="label">nb_week</xsl:with-param>
				<xsl:with-param name="value" select="Params/@nb_week"/>
				<xsl:with-param name="maxlength">2</xsl:with-param>
				<xsl:with-param name="size">2</xsl:with-param>
			</xsl:call-template>
			</table>
		</td>
	</xsl:if>
		<td>
			<input class="button" type="submit" name="reload" value="{mioga:gettext('Ok')}"/>
		</td>
		<td>
			&#160;
		</td>
		<xsl:if test="actions/action = 'ViewPrefs'">
			<td>
				<a href="ViewPrefs"><xsl:value-of select="mioga:gettext('Preferences')"/></a>
			</td>
		</xsl:if>
	</tr>
	</table>
</div>
</form>
    
<div id="table-copy"></div>
<center>
<table id="planning" class="planning" style="padding:0px; background-color: #000000;">
<xsl:if test="/DisplayMain/ViewType/@type='month'">
<tr>
  <th class="{$mioga-title-bg-color}">&#160;</th>
  <th class="{$mioga-title-bg-color}" style="text-align: left;" colspan="{count(//division)}">
    <p id="time-arrows">
      <a href="{$private_bin_uri}/Planning/DisplayMain?previous_step=1&amp;view_type=month">
        <img src="{$image_uri}/16x16/actions/go-first.png" alt="{mioga:gettext('Go back to %s weeks ago', string(//Params/@nb_week))}" title="{mioga:gettext('Go back to %s weeks ago', string(//Params/@nb_week))}" border="0"/>
      </a>&#160;
      <a href="{$private_bin_uri}/Planning/DisplayMain?previous_month=1&amp;view_type=month">
        <img src="{$image_uri}/16x16/actions/go-previous.png" alt="{mioga:gettext('Go back to previous month')}" title="{mioga:gettext('Go back to previous month')}" border="0"/>
      </a>&#160;&#160;
      <span class="week"><xsl:value-of select="mioga:gettext('Weeks %s to %s', string(//division[1]/@wofy), string(//division[count(//division)]/@wofy))"/></span>
      &#160;&#160;<a href="{$private_bin_uri}/Planning/DisplayMain?next_month=1&amp;view_type=month">
        <img src="{$image_uri}/16x16/actions/go-next.png" alt="{mioga:gettext('Go to next month')}" title="{mioga:gettext('Go to next month')}" border="0"/>
      </a>&#160;
      <a href="{$private_bin_uri}/Planning/DisplayMain?next_step=1&amp;view_type=month">
        <img src="{$image_uri}/16x16/actions/go-last.png" alt="{mioga:gettext('Go to %s weeks later', string(//Params/@nb_week))}" title="{mioga:gettext('Go to %s weeks later', string(//Params/@nb_week))}" border="0"/>
      </a>
    </p>
    &#160;
  </th>
</tr>
</xsl:if>
<tr>
	<th class="{$mioga-title-bg-color}">&#160;</th>
	<xsl:for-each select="/DisplayMain/main_div">
		<th class="{$mioga-title-bg-color}">
			<xsl:attribute name="colspan"><xsl:value-of select="./@count" /></xsl:attribute>
			<xsl:if test="/DisplayMain/ViewType/@type='month'">
				<p><span class="month">
				<xsl:choose>
					<xsl:when test="./@month=1"><xsl:value-of select="mioga:gettext('January')"/></xsl:when>
					<xsl:when test="./@month=2"><xsl:value-of select="mioga:gettext('February')"/></xsl:when>
					<xsl:when test="./@month=3"><xsl:value-of select="mioga:gettext('March')"/></xsl:when>
					<xsl:when test="./@month=4"><xsl:value-of select="mioga:gettext('April')"/></xsl:when>
					<xsl:when test="./@month=5"><xsl:value-of select="mioga:gettext('May')"/></xsl:when>
					<xsl:when test="./@month=6"><xsl:value-of select="mioga:gettext('June')"/></xsl:when>
					<xsl:when test="./@month=7"><xsl:value-of select="mioga:gettext('July')"/></xsl:when>
					<xsl:when test="./@month=8"><xsl:value-of select="mioga:gettext('August')"/></xsl:when>
					<xsl:when test="./@month=9"><xsl:value-of select="mioga:gettext('September')"/></xsl:when>
					<xsl:when test="./@month=10"><xsl:value-of select="mioga:gettext('October')"/></xsl:when>
					<xsl:when test="./@month=11"><xsl:value-of select="mioga:gettext('November')"/></xsl:when>
					<xsl:when test="./@month=12"><xsl:value-of select="mioga:gettext('December')"/></xsl:when>
				</xsl:choose>
				</span></p>
				<a href="{$private_bin_uri}/Planning/DisplayMain?view_type=week&amp;day={./@begin_day}&amp;month={./@begin_month}&amp;year={./@begin_year}"><span class="week"><xsl:value-of select="mioga:gettext('Week #%s', string(./@wofy))"/></span></a>
			</xsl:if>
			<xsl:if test="/DisplayMain/ViewType/@type='week'">
				<p><a href="{$private_bin_uri}/Planning/DisplayMain?previous_week=1&amp;view_type=week"><img src="{$image_uri}/16x16/actions/go-previous.png" alt="" border="0"/></a>
				<span class="week"><xsl:value-of select="mioga:gettext('Week #%s from %3$s/%2$s/%4$s to %6$s/%5$s/%7$s', string(./@wofy), string(./@begin_day), string(./@begin_month), string(./@begin_year), string(./@end_day), string(./@end_month), string(./@end_year))"/></span>
				<a href="{$private_bin_uri}/Planning/DisplayMain?next_week=1&amp;view_type=week"><img src="{$image_uri}/16x16/actions/go-next.png" alt="" border="0"/></a>
				</p>
			</xsl:if>
		</th>
	</xsl:for-each>
</tr>

<xsl:call-template name="division" />

<xsl:choose>
	<xsl:when test="ViewType/@type='month'">
		<xsl:call-template name="view-month" />
	</xsl:when>
	<xsl:otherwise>
		<xsl:call-template name="view-week" />
	</xsl:otherwise>
</xsl:choose>

<xsl:call-template name="division" />

</table>
</center>
</body>
</xsl:template>

<!-- ViewPrefs
     ========== -->

<xsl:template match="ViewPrefs">
<body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
         <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>
    <xsl:call-template name="MiogaBox">
        <xsl:with-param name="wrap">1</xsl:with-param>
        <xsl:with-param name="center">1</xsl:with-param>
        <xsl:with-param name="title"><xsl:value-of select="mioga:gettext('Preferences')"/></xsl:with-param>
        <xsl:with-param name="body">
			<form action="DisplayMain" method="POST">
			 <xsl:attribute name="onsubmit">
			   if ($('app').value.strip() == '') { alert("<xsl:value-of select="mioga:gettext('You must input an application')"/>"); return false; }
			   else { serializeUsers(); }
			 </xsl:attribute>
			 <div style="display: none;" id="users_infos">&#160;</div>
				<table style="width:100%;">
				<tr>
          <td><xsl:value-of select="mioga:gettext('Application to use when clicking on a cell')"/></td>
          <td>
             <input type="text" id="app" name="app" value="{app/@value}" />
          </td>
        </tr>
				<tr>
					<td><xsl:value-of select="mioga:gettext('Show weekends')"/></td>
					<td>
						<xsl:choose>
							<xsl:when test="show_we/@value='0'">
								<input type="radio" name="show_we" value="1" /><xsl:value-of select="mioga:gettext('Yes')"/><br />
								<input type="radio" name="show_we" value="0" checked="yes" /><xsl:value-of select="mioga:gettext('No')"/>
							</xsl:when>
							<xsl:otherwise>
								<input type="radio" name="show_we" value="1" checked="yes" /><xsl:value-of select="mioga:gettext('Yes')"/><br />
								<input type="radio" name="show_we" value="0" /><xsl:value-of select="mioga:gettext('No')"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				<tr>
					<td><xsl:value-of select="mioga:gettext('View type')"/></td>
					<td>
						<xsl:choose>
							<xsl:when test="view_type/@value='month'">
								<input type="radio" name="view_type" value="week" /><xsl:value-of select="mioga:gettext('Week')"/><br />
								<input type="radio" name="view_type" value="month" checked="yes" /><xsl:value-of select="mioga:gettext('Month')"/>
							</xsl:when>
							<xsl:otherwise>
								<input type="radio" name="view_type" value="week" checked="yes" /><xsl:value-of select="mioga:gettext('Week')"/><br />
								<input type="radio" name="view_type" value="month" /><xsl:value-of select="mioga:gettext('Month')"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				<tr>
					<td><xsl:value-of select="mioga:gettext('Global tasks')"/></td>
					<td>
						<xsl:choose>
							<xsl:when test="show_global/@value='0'">
								<input type="radio" name="show_global" value="1" /><xsl:value-of select="mioga:gettext('Yes')"/><br />
								<input type="radio" name="show_global" value="0" checked="yes" /><xsl:value-of select="mioga:gettext('No')"/>
							</xsl:when>
							<xsl:otherwise>
								<input type="radio" name="show_global" value="1" checked="yes" /><xsl:value-of select="mioga:gettext('Yes')"/><br />
								<input type="radio" name="show_global" value="0" /><xsl:value-of select="mioga:gettext('No')"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				<tr>
          <td><xsl:value-of select="mioga:gettext('Custom users order')"/></td>
          <td>
            <xsl:choose>
              <xsl:when test="use_order/@value='0'">
                <input type="radio" name="use_order" onclick="$('user_list').show()" value="1" /><xsl:value-of select="mioga:gettext('Yes')"/><br />
                <input type="radio" name="use_order" onclick="$('user_list').hide()" value="0" checked="yes" /><xsl:value-of select="mioga:gettext('No')"/>
              </xsl:when>
              <xsl:otherwise>
                <input type="radio" name="use_order" onclick="$('user_list').show()" value="1" checked="yes" /><xsl:value-of select="mioga:gettext('Yes')"/><br />
                <input type="radio" name="use_order" onclick="$('user_list').hide()" value="0" /><xsl:value-of select="mioga:gettext('No')"/>
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </tr>
        <xsl:variable name="list_style">
          <xsl:if test="use_order/@value='0'">display: none;</xsl:if>
        </xsl:variable>
        <tr id="user_list" style="{$list_style}">
          <td colspan="2" class="sortable_list">
             <ul class="hmenu" style="margin-bottom: 1em;">
                <li>
                    <a class="add" href="#">
                    <xsl:attribute name="onclick">addSeparator(); return false;</xsl:attribute>
                    <xsl:value-of select="mioga:gettext('Add separator')"/>
                    </a>
                </li>
             </ul>
             <table cellpadding="0" cellspacing="2" border="0" width="100%">
               <tbody id="users">
                <xsl:variable name="sortby"><xsl:choose>
                <xsl:when test="user[1]/order">order</xsl:when>
                <xsl:otherwise>lastname</xsl:otherwise>
                </xsl:choose></xsl:variable>
                
                <xsl:variable name="datatype"><xsl:choose>
                <xsl:when test="user[1]/order">number</xsl:when>
                <xsl:otherwise>text</xsl:otherwise>
                </xsl:choose></xsl:variable>
              
                <xsl:for-each select="user">
                  <xsl:sort select="*[local-name() = string($sortby)]" data-type="{$datatype}"/>
                  <tr id="users_{@rowid}">
                    <td>
                      <xsl:choose>
                        <xsl:when test="@rowid = 0">
                          <xsl:attribute name="class">separator</xsl:attribute>
                          <a href='#' title='{mioga:gettext("Delete this separator")}' onclick='removeSeparator(this); return false;'>
                            <img src="{$image_uri}/16x16/actions/trash-empty.png"/>
                          </a>
                          <hr/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:attribute name="class">user</xsl:attribute>
                          <xsl:value-of select="lastname" />&#160;<xsl:value-of select="firstname" />
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                  </tr>
                </xsl:for-each>
               </tbody>
             </table>
            <script type="text/javascript">
              Sortable.create("users", {tag: 'tr', scroll: window});
              image_uri = '<xsl:value-of select="$image_uri"/>';
            </script>
          </td>
        </tr>
				<tr>
					<td colspan="2">
            <ul class="button_list">
              <li>
                <input class="button" type="submit" name="set_pref" value="{mioga:gettext('Ok')}"/>
              </li>
              <li>
                <a href="DisplayMain" class="button"><xsl:value-of select="mioga:gettext('Cancel')"/></a>
              </li>
            </ul>
					</td>
				</tr>
				</table>
			</form>
        </xsl:with-param>
    </xsl:call-template>

</body>
</xsl:template>

<!-- division
     ======== -->

<xsl:template name="division">
<tr>
	<th class="{$mioga-title-bg-color}">&#160;</th>
	<xsl:for-each select="/DisplayMain/division">
		<xsl:sort select="./@order" data-type="number" order="ascending"/>
		<xsl:variable name="th-color">
			<xsl:choose>
				<xsl:when test="./@current='yes'">#ef1412</xsl:when>
				<xsl:when test="./@holiday='yes' or ./@wday='6' or ./@wday='0'">#f95c5c</xsl:when>
				<xsl:otherwise><xsl:value-of select="$mioga-title-color" /></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="/DisplayMain/ViewType/@type='month'">
				<!-- TODO -->
				<th class="day_month {$mioga-title-bg-color}" style="color:{$th-color};">
				<xsl:choose>
					<xsl:when test="./@wday=1"><xsl:value-of select="mioga:gettext('Mo')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
					<xsl:when test="./@wday=2"><xsl:value-of select="mioga:gettext('Tu')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
					<xsl:when test="./@wday=3"><xsl:value-of select="mioga:gettext('We')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
					<xsl:when test="./@wday=4"><xsl:value-of select="mioga:gettext('Th')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
					<xsl:when test="./@wday=5"><xsl:value-of select="mioga:gettext('Fr')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
					<xsl:when test="./@wday=6"><xsl:value-of select="mioga:gettext('Sa')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
					<xsl:when test="./@wday=0"><xsl:value-of select="mioga:gettext('Su')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
				</xsl:choose>
				</th>
			</xsl:when>
			<xsl:otherwise>
				<!-- TODO -->
				<th class="day_week {$mioga-title-bg-color}" style="color:{$th-color};">
				<xsl:choose>
					<xsl:when test="./@wday=1"><xsl:value-of select="mioga:gettext('Monday')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
					<xsl:when test="./@wday=2"><xsl:value-of select="mioga:gettext('Tuesday')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
					<xsl:when test="./@wday=3"><xsl:value-of select="mioga:gettext('Wednesday')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
					<xsl:when test="./@wday=4"><xsl:value-of select="mioga:gettext('Thursday')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
					<xsl:when test="./@wday=5"><xsl:value-of select="mioga:gettext('Friday')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
					<xsl:when test="./@wday=6"><xsl:value-of select="mioga:gettext('Saturday')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
					<xsl:when test="./@wday=0"><xsl:value-of select="mioga:gettext('Sunday')"/>&#160;<xsl:value-of select="./@day"/></xsl:when>
				</xsl:choose>
				</th>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
</tr>
</xsl:template>

<!-- view-month
     ========== -->

<xsl:template name="view-month">

<xsl:variable name="sortby"><xsl:choose>
<xsl:when test="user[1]/order">order</xsl:when>
<xsl:otherwise>lastname</xsl:otherwise>
</xsl:choose></xsl:variable>

<xsl:variable name="datatype"><xsl:choose>
<xsl:when test="user[1]/order">number</xsl:when>
<xsl:otherwise>text</xsl:otherwise>
</xsl:choose></xsl:variable>

<xsl:for-each select="user">
  <xsl:sort select="*[local-name() = string($sortby)]" data-type="{$datatype}"/>
	
	<xsl:variable name="trcolor">
	<xsl:choose>
		<xsl:when test="position() mod 2 = 0"><xsl:value-of select="$mioga-list-odd-row-bg-color" /></xsl:when>
		<xsl:otherwise><xsl:value-of select="$mioga-list-even-row-bg-color" /></xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<xsl:variable name="ident"><xsl:value-of select="./ident"/></xsl:variable>
	<tr class="{$trcolor}">
	  <xsl:choose>
	    <xsl:when test="@id = 0">
	      <td class="sep {$mioga-title-bg-color}" colspan="{count(/DisplayMain/division)+1}">
	       &#160;
	      </td>
	    </xsl:when>
	    <xsl:otherwise>
		    <td class="header">
		      <a href="../../{$ident}/Organizer/DisplayMain"><xsl:value-of select="lastname" />&#160;<xsl:value-of select="substring(firstname, 1, 1)" /></a>
		    </td>
		    <xsl:for-each select="/DisplayMain/division">
		      <xsl:sort select="./@order" data-type="number" order="ascending"/>
		      <xsl:variable name="task_url">
		        <xsl:value-of select="$private_bin_uri"/><xsl:value-of select="//ViewType/@app"/>?day=<xsl:value-of select="@day"/>&amp;month=<xsl:value-of select="@month"/>&amp;year=<xsl:value-of select="@year"/>&amp;ident=<xsl:value-of select="$ident"/>
		      </xsl:variable>
		      <xsl:variable name="tdcolor">
		        <xsl:if test="./@holiday='yes' or ./@wday='6' or ./@wday='0'">
		          holidays
		        </xsl:if>
		      </xsl:variable>
		      <td class="date {$tdcolor}" onclick="location='{$task_url}'">
		        <xsl:attribute name="ident"><xsl:value-of select="$ident"/></xsl:attribute>
		        <xsl:attribute name="year"><xsl:value-of select="@year"/></xsl:attribute>
		        <xsl:attribute name="month"><xsl:value-of select="@month"/></xsl:attribute>
		        <xsl:attribute name="day"><xsl:value-of select="@day"/></xsl:attribute>
		        <xsl:call-template name="display-task-graph">
		          <xsl:with-param name="ident"><xsl:value-of select="$ident"/></xsl:with-param>
		          <xsl:with-param name="order"><xsl:value-of select="@order"/></xsl:with-param>
		          <xsl:with-param name="user" select="/DisplayMain/user"/>
		        </xsl:call-template>
		      </td>
		    </xsl:for-each>
	    </xsl:otherwise>
	  </xsl:choose>
	</tr>
</xsl:for-each>
</xsl:template>


<!-- view-week
     ========== -->

<xsl:template name="view-week">

<xsl:variable name="sortby"><xsl:choose>
<xsl:when test="user[1]/order">order</xsl:when>
<xsl:otherwise>lastname</xsl:otherwise>
</xsl:choose></xsl:variable>

<xsl:variable name="datatype"><xsl:choose>
<xsl:when test="user[1]/order">number</xsl:when>
<xsl:otherwise>text</xsl:otherwise>
</xsl:choose></xsl:variable>

<xsl:for-each select="user">
  <xsl:sort select="*[local-name() = string($sortby)]" data-type="{$datatype}"/>
	<xsl:variable name="row_parity"><xsl:value-of select="position() mod 2" /></xsl:variable>
	<xsl:variable name="trcolor">
	  <xsl:choose>
	    <xsl:when test="$row_parity = 0"><xsl:value-of select="$mioga-list-odd-row-bg-color" /></xsl:when>
	    <xsl:otherwise><xsl:value-of select="$mioga-list-even-row-bg-color" /></xsl:otherwise>
	  </xsl:choose>
  </xsl:variable>
	<xsl:variable name="ident"><xsl:value-of select="./ident"/></xsl:variable>
	<tr class="{$trcolor}">
	 <xsl:choose>
      <xsl:when test="@id = 0">
        <td class="sep {$mioga-title-bg-color}" colspan="{count(/DisplayMain/division)+1}">
         &#160;
        </td>
      </xsl:when>
      <xsl:otherwise>
        <td class="header">
		      <a href="../../{$ident}/Organizer/DisplayMain"><xsl:value-of select="lastname" />&#160;<xsl:value-of select="substring(firstname, 1, 1)" /></a>
		    </td>
		    <xsl:for-each select="/DisplayMain/division">
		      <xsl:sort select="./@order" data-type="number" order="ascending"/>
		      <xsl:variable name="tdcolor">
		        <!-- <xsl:if test="./@holiday='yes' or ./@wday='6' or ./@wday='0'">
		          holidays
		        </xsl:if> -->
		      </xsl:variable>
		      <xsl:variable name="task_url">ViewTasks?day=<xsl:value-of select="@day"/>&amp;month=<xsl:value-of select="@month"/>&amp;year=<xsl:value-of select="@year"/>&amp;ident=<xsl:value-of select="$ident"/></xsl:variable>
		      <td class="planning_date {$tdcolor}" onclick="location='{$task_url}'">
		        <xsl:call-template name="display-task">
		          <xsl:with-param name="ident"><xsl:value-of select="$ident"/></xsl:with-param>
		          <xsl:with-param name="bgcolor"><xsl:value-of select="$tdcolor"/></xsl:with-param>
		          <xsl:with-param name="row_type"><xsl:value-of select="$row_parity"/></xsl:with-param>
		          <xsl:with-param name="order"><xsl:value-of select="@order"/></xsl:with-param>
		          <xsl:with-param name="day"><xsl:value-of select="@day"/></xsl:with-param>
		          <xsl:with-param name="month"><xsl:value-of select="@month"/></xsl:with-param>
		          <xsl:with-param name="year"><xsl:value-of select="@year"/></xsl:with-param>
		          <xsl:with-param name="user" select="/DisplayMain/user"/>
		        </xsl:call-template>
		      </td>
	      </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
	</tr>
</xsl:for-each>
</xsl:template>

<!-- display-task
     ============ -->

<xsl:template name="display-task">
    <xsl:param name="user"/>
    <xsl:param name="ident"/>
    <xsl:param name="order"/>
    <xsl:param name="row_type"/>
    <xsl:param name="bgcolor"/>
    
<div class="week_day">
	<xsl:choose>
		<xsl:when test="$user[ident=$ident]/div_user[@order=$order]">
			<xsl:for-each select="$user[ident=$ident]/div_user[@order=$order]/day_task">
  				<xsl:variable name="text"><xsl:value-of select="./@text"/></xsl:variable>
  				<xsl:variable name="color"><xsl:value-of select="./@color"/></xsl:variable>
  				<xsl:variable name="bclass">
					<xsl:choose>
						<xsl:when test="./@outside='1'">ident outside</xsl:when>
						<xsl:otherwise>ident</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<div class="task">
					<p>
					<xsl:attribute name="title"><xsl:value-of select="$text"/></xsl:attribute>
					<span class="nohour" style="color:{$bgcolor};" >00:00-00:00</span>
					<span class="color">
					<xsl:attribute name="style">color:<xsl:value-of select="$color"/>;</xsl:attribute>
					&#160;&#9830;&#160;</span>
						<span class="{$bclass}"><xsl:value-of select="$text"/></span>
					</p>
				</div>
			</xsl:for-each>
			<xsl:for-each select="$user[ident=$ident]/div_user[@order=$order]/task">
  				<xsl:variable name="bclass">
					<xsl:choose>
						<xsl:when test="./@outside='1'">ident outside</xsl:when>
						<xsl:otherwise>ident</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<div class="task">
					<p>
					<xsl:attribute name="title"><xsl:value-of select="./@text"/></xsl:attribute>
					<span class="hour"><xsl:value-of select="./@begin_hour"/>:<xsl:value-of select="./@begin_min"/>-<xsl:value-of select="./@end_hour"/>:<xsl:value-of select="./@end_min"/></span>
					<span class="color">
					<xsl:attribute name="style">color:<xsl:value-of select="./@color"/>;</xsl:attribute>
					&#160;&#9830;&#160;</span>
						<span class="{$bclass}"><xsl:value-of select="./@text"/></span>
					</p>
				</div>
			</xsl:for-each>
		</xsl:when>
		<xsl:otherwise>
			<div class="desk">
			</div>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:choose>
		<xsl:when test='$row_type=1'>
			<img class="fader" src="{$image_uri}/fade_even.png" />
		</xsl:when>
		<xsl:otherwise>
			<img class="fader" src="{$image_uri}/fade_odd.png" />
		</xsl:otherwise>
	</xsl:choose>
</div>
</xsl:template>

<!-- display-task-graph
     ================== -->

<xsl:template name="display-task-graph">
  <xsl:param name="user"/>
  <xsl:param name="order"/>
  <xsl:param name="ident"/>
    
  
  <xsl:if test="$user[ident=$ident]/div_user[@order=$order]/day_task or $user[ident=$ident]/div_user[@order=$order]/task or (/DisplayMain/ViewType/@show_global='1' and $user[ident=$ident]/div_user[@order=$order]/globaltask)">
    <xsl:attribute name="onclick">window.location.href='ViewTasks?day=<xsl:value-of select="@day"/>&amp;month=<xsl:value-of select="@month"/>&amp;year=<xsl:value-of select="@year"/>&amp;ident=<xsl:value-of select="$ident"/>'</xsl:attribute>
  </xsl:if>
	<xsl:choose>
		<xsl:when test="$user[ident=$ident]/div_user[@order=$order]">
			<div class="task_graph" style="width:{$cell_width}px;">
			<xsl:choose>
				<xsl:when test="$user[ident=$ident]/div_user[@order=$order]/day_task">
					<xsl:variable name="text"><xsl:value-of select="$user[ident=$ident]/div_user[@order=$order]/day_task/@text" /></xsl:variable>
					<xsl:variable name="color"><xsl:value-of select="$user[ident=$ident]/div_user[@order=$order]/day_task/@color" /></xsl:variable>
					<div class="day_task" style="background-color:{$color};" title="{$text}">&#160;</div>
				</xsl:when>
				<xsl:otherwise>
					<div class="day_task" style="background-color:transparent;">&#160;</div>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="count($user[ident=$ident]/div_user[@order=$order]/task) = 0">
					<div class="graph">&#160;</div>
				</xsl:when>
				<xsl:otherwise>
					<div class="graph">
						<xsl:for-each select="$user[ident=$ident]/div_user[@order=$order]/task">
							<xsl:variable name="text"><xsl:value-of select="./@begin_hour" />:<xsl:value-of select="./@begin_min" />-<xsl:value-of select="./@end_hour" />:<xsl:value-of select="./@end_min" />&#160;<xsl:value-of select="./@text" /></xsl:variable>
							<xsl:variable name="width"><xsl:value-of select="ceiling((./@wz1 * $begin_day_width div 100) + (./@wz2 * $day_width div 100) + (./@wz3 * $end_day_width div 100))" /></xsl:variable>
							<xsl:variable name="bgcolor"><xsl:value-of select="./@color" /></xsl:variable>
							<xsl:choose>
								<xsl:when test="./@wz1 &gt; 0">
									<xsl:variable name="pos"><xsl:value-of select="ceiling(./@position * $begin_day_width div 100)" /></xsl:variable>
									<div class="graph_task" title="{$text}" style="background-color:{$bgcolor}; position:absolute; top:0; left:{$pos}px;width:{$width}px;"><xsl:if test="./@outside='1'"><div class="outside"></div></xsl:if></div>
								</xsl:when>
								<xsl:when test="./@wz2 &gt; 0">
									<xsl:variable name="pos"><xsl:value-of select="ceiling(./@position * $day_width div 100 + $begin_day_width)" /></xsl:variable>
									<div class="graph_task" title="{$text}" style="background-color:{$bgcolor}; position:absolute; top:0; left:{$pos}px;width:{$width}px;"><xsl:if test="./@outside='1'"><div class="outside"></div></xsl:if></div>
								</xsl:when>
								<xsl:otherwise>
									<xsl:variable name="pos"><xsl:value-of select="ceiling(./@position * $end_day_width div 100 + $begin_day_width + $day_width)" /></xsl:variable>
									<div class="graph_task" title="{$text}" style="background-color:{$bgcolor}; position:absolute; top:0; left:{$pos}px;width:{$width}px;"><xsl:if test="./@outside='1'"><div class="outside"></div></xsl:if></div>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</div>
				</xsl:otherwise>
			</xsl:choose>

			<xsl:if test="/DisplayMain/ViewType/@show_global='1'">
				<xsl:for-each select="$user[ident=$ident]/div_user[@order=$order]/globaltask">
					<xsl:variable name="text"><xsl:value-of select="./@text" /></xsl:variable>
					<xsl:variable name="bgcolor"><xsl:value-of select="./@color" /></xsl:variable>
					<xsl:choose>
						<xsl:when test="./@value = 1">
							<div class="global_std" style="background-color:{$bgcolor};" title="{$text}">&#160;</div>
						</xsl:when>
						<xsl:when test="./@value = 2">
							<div class="global_delayed" style="background-color:{$bgcolor};" title="{$text}">&#160;</div>
						</xsl:when>
						<xsl:when test="./@value = 3">
							<div class="global_empty" style="background-color:transparent;"><div class="global_margin" style="background-color:#c0c0c0;" title="{$text}">&#160;</div></div>
						</xsl:when>
						<xsl:otherwise>
							<div class="global_empty" style="background-color:transparent;">&#160;</div>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:if>
			</div>
		</xsl:when>
		<xsl:otherwise>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- ViewTasks
	=========== -->

<xsl:template match="ViewTasks">
    <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">gestiontemps.html#planning</xsl:with-param>
    </xsl:call-template>

      <!-- Status message -->
      <xsl:if test="@message != ''">
         <xsl:call-template name="InlineMessage">
            <xsl:with-param name="type" select="@type"/>
            <xsl:with-param name="message" select="@message"/>
         </xsl:call-template>
      </xsl:if>

    <xsl:call-template name="MiogaTitle"/>
        <p>
            <img src="{$image_uri}/16x16/actions/arrow-right.png" /><font size="+1" class="{$mioga-title-color}">&#160;
              <xsl:call-template name="planningTranslation">
                <xsl:with-param name="type">organizer</xsl:with-param>
              </xsl:call-template>
            </font>
        </p>
        
        <xsl:choose>
            <xsl:when test="Agenda/@accessright != 0">
                <xsl:apply-templates select="Agenda"/>
            </xsl:when>
            <xsl:otherwise>
              <i>
                <xsl:call-template name="planningTranslation">
                  <xsl:with-param name="type">no-access-right</xsl:with-param>
                </xsl:call-template>
              </i>
            </xsl:otherwise>
        </xsl:choose>
  
        
        <xsl:if test="count(resource) = 0">

        <p>
            <img src="{$image_uri}/16x16/actions/arrow-right.png" /><font size="+1" class="{$mioga-title-color}">&#160;
              <xsl:call-template name="planningTranslation">
                <xsl:with-param name="type">task</xsl:with-param>
              </xsl:call-template>
            </font>
        </p>
        
        <xsl:apply-templates select="GlobalTasks"/>

            
        </xsl:if>

        <br/>
        
        <center>
            <xsl:call-template name="back-button">
                <xsl:with-param name="href"><xsl:value-of select="$private_bin_uri"/>/Planning/DisplayMain</xsl:with-param>
            </xsl:call-template>
        </center>

    </body>
</xsl:template>

<xsl:template match="Agenda" mode="table-body">
    <xsl:variable name="accessright" select="@accessright"/>

        <xsl:for-each select="org_task">
                <tr>
                    <xsl:choose>
                        <xsl:when test="position() mod 2 = 0">
                            <xsl:attribute name="class"><xsl:value-of select="$mioga-list-even-row-bg-color" /></xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color" /></xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    
                    <td width="20%" bgcolor="{color}">
                        <xsl:choose>
                            <xsl:when test="private=0">
                                <a>
                                    <xsl:if test="id/@is_strict=1">
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="/ViewTasks/desturi/@private_bin"/>/Organizer/ViewStrictTask?taskrowid=<xsl:value-of select="id"/>&amp;mode=initial
                                        </xsl:attribute>
                                    </xsl:if>
                                    <xsl:if test="id/@is_strict=0">
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="/ViewTasks/desturi/@private_bin"/>/Organizer/ViewPeriodicTask?taskrowid=<xsl:value-of select="id"/>&amp;mode=initial&amp;day=<xsl:value-of select="start/@day"/>&amp;month=<xsl:value-of select="start/@month"/>&amp;year=<xsl:value-of select="start/@year"/>
                                        </xsl:attribute>
                                    </xsl:if>
                                    <xsl:value-of select="name"/>
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                <i>
                                  <xsl:call-template name="planningTranslation">
                                    <xsl:with-param name="type">private-task</xsl:with-param>
                                  </xsl:call-template>

                                </i>
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                    <td width="50%">
                      <xsl:call-template name="disp_datetime">
                        <xsl:with-param name="day"><xsl:value-of select="start/@day"/></xsl:with-param>
                        <xsl:with-param name="month"><xsl:value-of select="start/@month"/></xsl:with-param>
                        <xsl:with-param name="year"><xsl:value-of select="start/@year"/></xsl:with-param>
                        <xsl:with-param name="dow"><xsl:value-of select="start/@dow"/></xsl:with-param>
                        <xsl:with-param name="hour"><xsl:value-of select="start/@hour"/></xsl:with-param>
                        <xsl:with-param name="minute"><xsl:value-of select="start/@minute"/></xsl:with-param>
                        <xsl:with-param name="second"><xsl:value-of select="start/@second"/></xsl:with-param>
                      </xsl:call-template>
                        </td>

                        <td width="30%">
                          <xsl:call-template name="hourLabel"/>
                        </td>
                        
  

                </tr>
        </xsl:for-each>
</xsl:template>

<xsl:template match="Agenda">
        <xsl:if test="count(org_task)=0">
                <p>
                        <img src="{$image_uri}/transparent_fill.gif" width="35" height="1"/> --  <xsl:call-template name="planningTranslation"><xsl:with-param name="type">none</xsl:with-param></xsl:call-template> --
                </p>
        </xsl:if>
        <xsl:if test="count(org_task)!=0">
                <center>
                        <xsl:call-template name="table">
                                <xsl:with-param name="table_width">90%</xsl:with-param>
                        </xsl:call-template>
                </center>
        </xsl:if>
</xsl:template>

<xsl:template match="ProjectTasks" mode="table-body">
        <xsl:for-each select="project_task">
                <tr>
                        <xsl:choose>
                                <xsl:when test="position() mod 2 = 0">
                                        <xsl:attribute name="class"><xsl:value-of select="$mioga-list-even-row-bg-color" /></xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise>
                                        <xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color" /></xsl:attribute>
                                </xsl:otherwise>
                        </xsl:choose>
                        <td width="20%">
                          <a href="{/ViewTasks/desturi/@private_bin}/Project/ProjectDisplayTask?taskid={id}">
                                        <xsl:value-of select="name"/>
                                </a>
                        </td>
                        <td width="25%">
                                <xsl:call-template name="disp_date">
                                        <xsl:with-param name="day"><xsl:value-of select="start/@day"/></xsl:with-param>
                                        <xsl:with-param name="month"><xsl:value-of select="start/@month"/></xsl:with-param>
                                        <xsl:with-param name="year"><xsl:value-of select="start/@year"/></xsl:with-param>
                                        <xsl:with-param name="dow"><xsl:value-of select="start/@dow"/></xsl:with-param>
                                </xsl:call-template>
                        </td>

                        <td width="25%">
                                <xsl:call-template name="disp_date">
                                        <xsl:with-param name="day"><xsl:value-of select="stop/@day"/></xsl:with-param>
                                        <xsl:with-param name="month"><xsl:value-of select="stop/@month"/></xsl:with-param>
                                        <xsl:with-param name="year"><xsl:value-of select="stop/@year"/></xsl:with-param>
                                        <xsl:with-param name="dow"><xsl:value-of select="stop/@dow"/></xsl:with-param>
                                </xsl:call-template>
                        </td>


                        <td width="20%">
                          <xsl:call-template name="dateLabel"/>
                        </td>

                        <td width="10%"><xsl:value-of select="progress"/> %</td>
                </tr>
        </xsl:for-each>
</xsl:template>

<xsl:template match="ProjectTasks">
  
  <xsl:if test="count(project_task)=0">
    <p>
      <img src="{$image_uri}/transparent_fill.gif" width="35" height="1"/> -- <xsl:call-template name="planningTranslation"><xsl:with-param name="type">none</xsl:with-param></xsl:call-template> --
    </p>
  </xsl:if>
  <xsl:if test="count(project_task)!=0">
    <center>
      <xsl:call-template name="table">
        <xsl:with-param name="table_width">90%</xsl:with-param>
      </xsl:call-template>
    </center>
  </xsl:if>

</xsl:template>

<xsl:template match="GlobalTasks" mode="table-body">
    <xsl:variable name="accessright" select="@accessright"/>

        <xsl:for-each select="global_task">
                <tr>
                        <xsl:choose>
                                <xsl:when test="position() mod 2 = 0">
                                        <xsl:attribute name="class"><xsl:value-of select="$mioga-list-even-row-bg-color" /></xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise>
                                        <xsl:attribute name="class"><xsl:value-of select="$mioga-list-odd-row-bg-color" /></xsl:attribute>
                                </xsl:otherwise>
                        </xsl:choose>
                        <td width="20%" bgcolor="{color}">
                            <xsl:choose>
                                <xsl:when test="private=0">
                                    <a href="{$bin_uri}/{group_ident}/Tasks/DisplayTask?rowid={id}">
                                        <xsl:value-of select="name"/>
                                    </a>
                                </xsl:when>
                                <xsl:otherwise>
                                  <i>
                                    <xsl:call-template name="planningTranslation">
                                      <xsl:with-param name="type">private-task</xsl:with-param>
                                    </xsl:call-template>
                                    </i>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td width="25%">
                                <xsl:call-template name="disp_date">
                                        <xsl:with-param name="day"><xsl:value-of select="start/@day"/></xsl:with-param>
                                        <xsl:with-param name="month"><xsl:value-of select="start/@month"/></xsl:with-param>
                                        <xsl:with-param name="year"><xsl:value-of select="start/@year"/></xsl:with-param>
                                        <xsl:with-param name="dow"><xsl:value-of select="start/@dow"/></xsl:with-param>
                                </xsl:call-template>
                        </td>

                        <td width="25%">
                                <xsl:call-template name="disp_date">
                                        <xsl:with-param name="day"><xsl:value-of select="stop/@day"/></xsl:with-param>
                                        <xsl:with-param name="month"><xsl:value-of select="stop/@month"/></xsl:with-param>
                                        <xsl:with-param name="year"><xsl:value-of select="stop/@year"/></xsl:with-param>
                                        <xsl:with-param name="dow"><xsl:value-of select="stop/@dow"/></xsl:with-param>
                                </xsl:call-template>
                        </td>
                        <td width="20%">
                          <xsl:call-template name="dateLabel"/>
                        </td>
                        <td width="10%"><xsl:value-of select="progress"/> %</td>
                </tr>
        </xsl:for-each>        
</xsl:template>

<xsl:template match="GlobalTasks">
  
        <xsl:if test="count(global_task)=0">
                <p>
                  <img src="{$image_uri}/transparent_fill.gif" width="35" height="1"/> -- <xsl:call-template name="planningTranslation"><xsl:with-param name="type">no-task-no-right</xsl:with-param></xsl:call-template> --
                </p>
        </xsl:if>
        <xsl:if test="count(global_task)!=0">
                <center>
                        <xsl:call-template name="table">
                                <xsl:with-param name="table_width">90%</xsl:with-param>
                        </xsl:call-template>
                </center>
        </xsl:if>
        
</xsl:template>

<xsl:template name="disp_datetime">
    <xsl:param name="day"/>
    <xsl:param name="month"/>
    <xsl:param name="year"/>
    <xsl:param name="dow"/>
    <xsl:param name="hour"/>
    <xsl:param name="minute"/>
    
  <xsl:call-template name="disp_date">
    <xsl:with-param name="day"><xsl:value-of select="$day"/></xsl:with-param>
    <xsl:with-param name="month"><xsl:value-of select="$month"/></xsl:with-param>
    <xsl:with-param name="year"><xsl:value-of select="$year"/></xsl:with-param>
    <xsl:with-param name="dow"><xsl:value-of select="$dow"/></xsl:with-param>
  </xsl:call-template>
  &#160;
  <xsl:call-template name="LocalTime">
    <xsl:with-param name="hour" select="$hour"/>
    <xsl:with-param name="min" select="$minute"/>
  </xsl:call-template>
</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<xsl:template name="MiogaTitleTranslation">
  <xsl:choose>
    <xsl:when test="name(.)='DisplayMain'"></xsl:when>
    <xsl:when test="name(.)='ViewTasks'">
      <xsl:value-of select="mioga:gettext('Tasks of')"/>
      <xsl:for-each select="date">
        <xsl:call-template name="LocalDate"/>
      </xsl:for-each> <xsl:value-of select="mioga:gettext('for')"/>
      <xsl:choose>
        <xsl:when test="user"><xsl:value-of select="mioga:gettext('user %s %s', string(user/@fn), string(user/@ln))"/></xsl:when>
        <xsl:when test="resource"><xsl:value-of select="mioga:gettext('resource %s', string(resource/@ident))"/></xsl:when>
      </xsl:choose> 
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="errorCode">
  <xsl:choose>
    <xsl:when test="error/@id='101'"><xsl:value-of select="mioga:gettext('Invalid date')"/></xsl:when>
    <xsl:when test="error/@id='102'"><xsl:value-of select="mioga:gettext('Days, weeks and months have to be numeric')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="planningTranslation">
  <xsl:param name="type"/>

  <xsl:choose>
    <xsl:when test="$type='see'"><xsl:value-of select="mioga:gettext('View')"/></xsl:when>
    <xsl:when test="$type='modify-task'"><xsl:value-of select="mioga:gettext('Modify this task')"/></xsl:when>
    <xsl:when test="$type='back-to-planning'"><xsl:value-of select="mioga:gettext('Back to schedule')"/></xsl:when>
    <xsl:when test="$type='no-access-right'"><xsl:value-of select="mioga:gettext('No access right')"/></xsl:when>
    <xsl:when test="$type='organizer'"><xsl:value-of select="mioga:gettext('Organizer')"/></xsl:when>
    <xsl:when test="$type='task'"><xsl:value-of select="mioga:gettext('Tasks')"/></xsl:when>
    <xsl:when test="$type='private-task'"><xsl:value-of select="mioga:gettext('Private task')"/></xsl:when>
    <xsl:when test="$type='none'"><xsl:value-of select="mioga:gettext('None')"/></xsl:when>
    <xsl:when test="$type='no-task-no-right'"><xsl:value-of select="mioga:gettext('No task or no right on tasks')"/></xsl:when>
    <xsl:when test="$type='hour-abbr'">&#160;<xsl:value-of select="mioga:gettext('h')"/>&#160;</xsl:when>
    <xsl:when test="$type='starting-at'"><xsl:value-of select="mioga:gettext('starting at')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="timeElement">
  <xsl:param name="type"/>
  <xsl:choose>
    <xsl:when test="$type=1"><xsl:value-of select="mioga:gettext('Day')"/></xsl:when>
    <xsl:when test="$type=2"><xsl:value-of select="mioga:gettext('Week')"/></xsl:when>
    <xsl:when test="$type=3"><xsl:value-of select="mioga:gettext('Month')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="planningDate">
  <xsl:if test="@subdiv='week'">
    <xsl:value-of select="mioga:gettext('Week #%s', string(@wofy))"/>
  </xsl:if>
  <xsl:if test="@subdiv='day'">
    <xsl:value-of select="mioga:gettext('%2$s/%1$s/%3$s', string(@day), string(@month), string(@year))"/>
  </xsl:if>  
</xsl:template>

<xsl:template match="Agenda" mode="table-title">
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Name')"/></font></th>
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Date')"/></font></th>
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Duration')"/></font></th>
</xsl:template>

<xsl:template name="hourLabel">
  <xsl:if test="duration_value/@hour!=0">
    <xsl:value-of select="duration_value/@hour"/> <xsl:value-of select="mioga:gettext('hour')"/><xsl:if test="duration_value/@hour&gt;1">s</xsl:if>
    &#160;
  </xsl:if>
  <xsl:if test="duration_value/@minute!=0">
    <xsl:value-of select="duration_value/@minute"/> <xsl:value-of select="mioga:gettext('minute')"/><xsl:if test="duration_value/@minute&gt;1">s</xsl:if>
  </xsl:if>
</xsl:template>

<xsl:template name="dateLabel">
  <xsl:if test="load_value/@day!=0">
    <xsl:value-of select="load_value/@day"/> <xsl:value-of select="mioga:gettext('day')"/><xsl:if test="load_value/@day&gt;1">s</xsl:if>
    &#160;
  </xsl:if>
  <xsl:if test="load_value/@hour!=0">
    <xsl:value-of select="load_value/@hour"/> <xsl:value-of select="mioga:gettext('hour')"/><xsl:if test="load_value/@hour&gt;1">s</xsl:if>
    &#160;
  </xsl:if>
  <xsl:if test="load_value/@minute!=0">
    <xsl:value-of select="load_value/@minute"/> <xsl:value-of select="mioga:gettext('minute')"/><xsl:if test="load_value/@minute&gt;1">s</xsl:if>
  </xsl:if>
</xsl:template>

<xsl:template match="ProjectTasks" mode="table-title">
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Name')"/></font></th>
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Start date')"/></font></th>
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('End date')"/></font></th>
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Workload')"/></font></th>
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Scheduled')"/></font></th>
</xsl:template>

<xsl:template match="GlobalTasks" mode="table-title">
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Name')"/></font></th>
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Start date')"/></font></th>
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('End date')"/></font></th>
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Workload')"/></font></th>
  <th><font class="{$mioga-list-title-text-color}"><xsl:value-of select="mioga:gettext('Scheduled')"/></font></th>
</xsl:template>

<xsl:template name="disp_date">
  <xsl:param name="dow"/>
  <xsl:param name="month"/>
  <xsl:param name="year"/>
  <xsl:param name="day"/>

  <xsl:if test="$dow=1"><xsl:value-of select="mioga:gettext('Monday')"/></xsl:if>
  <xsl:if test="$dow=2"><xsl:value-of select="mioga:gettext('Tuesday')"/></xsl:if>
  <xsl:if test="$dow=3"><xsl:value-of select="mioga:gettext('Wednesday')"/></xsl:if>
  <xsl:if test="$dow=4"><xsl:value-of select="mioga:gettext('Thursday')"/></xsl:if>
  <xsl:if test="$dow=5"><xsl:value-of select="mioga:gettext('Friday')"/></xsl:if>
  <xsl:if test="$dow=6"><xsl:value-of select="mioga:gettext('Saturday')"/></xsl:if>
  <xsl:if test="$dow=7"><xsl:value-of select="mioga:gettext('Sunday')"/></xsl:if>
  &#160;
  <xsl:value-of select="$day"/>
  &#160;        
  <xsl:if test="$month='01'"><xsl:value-of select="mioga:gettext('january')"/></xsl:if>
  <xsl:if test="$month='02'"><xsl:value-of select="mioga:gettext('february')"/></xsl:if>
  <xsl:if test="$month='03'"><xsl:value-of select="mioga:gettext('march')"/></xsl:if>
  <xsl:if test="$month='04'"><xsl:value-of select="mioga:gettext('april')"/></xsl:if>
  <xsl:if test="$month='05'"><xsl:value-of select="mioga:gettext('may')"/></xsl:if>
  <xsl:if test="$month='06'"><xsl:value-of select="mioga:gettext('june')"/></xsl:if>
  <xsl:if test="$month='07'"><xsl:value-of select="mioga:gettext('july')"/></xsl:if>
  <xsl:if test="$month='08'"><xsl:value-of select="mioga:gettext('august')"/></xsl:if>
  <xsl:if test="$month='09'"><xsl:value-of select="mioga:gettext('september')"/></xsl:if>
  <xsl:if test="$month='10'"><xsl:value-of select="mioga:gettext('october')"/></xsl:if>
  <xsl:if test="$month='11'"><xsl:value-of select="mioga:gettext('november')"/></xsl:if>
  <xsl:if test="$month='12'"><xsl:value-of select="mioga:gettext('december')"/></xsl:if>
  &#160;
  <xsl:value-of select="$year"/>
</xsl:template>

<xsl:template name="MiogaFormInputTranslation">
    <xsl:param name="label"/>
    
    <xsl:choose>
        <xsl:when test="$label='nb_week'"><xsl:value-of select="mioga:gettext('Number of weeks')"/></xsl:when>

        <xsl:when test="$label='category_id'"><xsl:value-of select="mioga:gettext('Category')"/></xsl:when>
        <xsl:when test="$label='date'"><xsl:value-of select="mioga:gettext('Date')"/></xsl:when>
        <xsl:when test="$label='schedule'"><xsl:value-of select="mioga:gettext('Timetable')"/></xsl:when>
        <xsl:when test="$label='description'"><xsl:value-of select="mioga:gettext('Description')"/></xsl:when>
        <xsl:when test="$label='outside'"><xsl:value-of select="mioga:gettext('On the move')"/></xsl:when>
        <xsl:when test="$label='repetitive-task'"><xsl:value-of select="mioga:gettext('Repetitive task')"/></xsl:when>
        <xsl:when test="$label='periodic'"><xsl:value-of select="mioga:gettext('Periodicity')"/></xsl:when>
        <xsl:when test="$label='contact-label'"><xsl:value-of select="mioga:gettext('Contact')"/></xsl:when>
        <xsl:when test="$label='externalcontact'"><xsl:value-of select="mioga:gettext('Potential contact')"/></xsl:when>
        <xsl:when test="$label='first_name'"><xsl:value-of select="mioga:gettext('Firstname')"/></xsl:when>
        <xsl:when test="$label='last_name'"><xsl:value-of select="mioga:gettext('Lastname')"/></xsl:when>
        <xsl:when test="$label='telephone'"><xsl:value-of select="mioga:gettext('Telephone')"/></xsl:when>
        <xsl:when test="$label='fax'"><xsl:value-of select="mioga:gettext('Fax')"/></xsl:when>
        <xsl:when test="$label='email'"><xsl:value-of select="mioga:gettext('Email')"/></xsl:when>
        <xsl:when test="$label='end-date'"><xsl:value-of select="mioga:gettext('Potential end date')"/></xsl:when>
        <xsl:when test="$label='format'"><xsl:value-of select="mioga:gettext('Format')"/></xsl:when>
        <xsl:when test="$label='start_date'"><xsl:value-of select="mioga:gettext('Export from')"/></xsl:when>
        <xsl:when test="$label='other_date'"><xsl:value-of select="mioga:gettext('Other')"/></xsl:when>
        <xsl:when test="$label='contact'"><xsl:value-of select="mioga:gettext('Contact')"/></xsl:when>
        <xsl:when test="$label='date-hour'"><xsl:value-of select="mioga:gettext('Date and time')"/></xsl:when>
        <xsl:when test="$label='convert-todo'"><xsl:value-of select="mioga:gettext('Change to todo task')"/></xsl:when>
        <xsl:when test="$label='convert-strict'"><xsl:value-of select="mioga:gettext('Schedule task')"/></xsl:when>
        <xsl:when test="$label='priority'"><xsl:value-of select="mioga:gettext('Priority')"/></xsl:when>
        <xsl:when test="$label='task-params'"><xsl:value-of select="mioga:gettext('Task parameters')"/></xsl:when>
        <xsl:when test="$label='planify'"><xsl:value-of select="mioga:gettext('Plan')"/></xsl:when>
        <xsl:when test="$label='actions'"><xsl:value-of select="mioga:gettext('Actions')"/></xsl:when>
        <xsl:when test="$label='delete-instance'"><xsl:value-of select="mioga:gettext('Delete this day occurrence')"/></xsl:when>
        <xsl:when test="$label='delete-task'"><xsl:value-of select="mioga:gettext('Delete all occurrences')"/></xsl:when>
        <xsl:when test="$label='edit-task'"><xsl:value-of select="mioga:gettext('Modify task parameters')"/></xsl:when>
    </xsl:choose>

</xsl:template>

</xsl:stylesheet>

