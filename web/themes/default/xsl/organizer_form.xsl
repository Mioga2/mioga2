<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<xsl:output method="html"/>

<!-- *********************************************************************** -->
<!-- * Form Main template                                                  * -->
<!-- *********************************************************************** -->

<xsl:template match="CreateStrictOrPeriodicTask|CreateSimplePeriodicTask|EditSimplePeriodicTask|EditStrictTask|EditTodoTask|CreateToDoTask|EditPeriodicTask|ChangePeriodicTask">
    <body xsl:use-attribute-sets="body_attr">
        <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="MiogaTitle"/>
        
        <center>
            <b>
                <font color="FF0000">
                    <xsl:call-template name="Error-string">
                        <xsl:with-param name="error" select="@message"/>
                    </xsl:call-template>
                </font>
            </b>
        </center>
        
        <div xsl:use-attribute-sets="app_form_body_attr">
            
            <xsl:call-template name="MiogaForm">
                <xsl:with-param name="label" select="name(.)"/>
                <xsl:with-param name="action" select="name(.)"/>
                <xsl:with-param name="method">POST</xsl:with-param>
                <xsl:with-param name="no-title">1</xsl:with-param>
            </xsl:call-template>
            
        </div>
        
    </body>
</xsl:template>


<!-- *********************************************************************** -->
<!-- * Reports Main template                                               * -->
<!-- *********************************************************************** -->

<xsl:template match="ViewStrictTask|ViewTodoTask|ViewPeriodicTask">
    <body xsl:use-attribute-sets="body_attr">
        <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="MiogaTitle"/>
        
        <center>
            <b>
                <font color="FF0000">
                    <xsl:call-template name="Error-string">
                        <xsl:with-param name="error" select="@message"/>
                    </xsl:call-template>
                </font>
            </b>
        </center>

        <div xsl:use-attribute-sets="app_form_body_attr">
            
            <xsl:call-template name="MiogaReport">
                <xsl:with-param name="label" select="name(.)"/>
                <xsl:with-param name="no-title">1</xsl:with-param>
            </xsl:call-template>
            
        </div>
        
    </body>
</xsl:template>

<!-- *********************************************************************** -->
<!-- * CreateSimplePeriodicTask                                            * -->
<!-- *********************************************************************** -->

<xsl:template match="CreateSimplePeriodicTask" mode="MiogaFormBody">
    <xsl:variable name="disabled"><xsl:choose>
    	<xsl:when test="@mode = 'booking'">1</xsl:when>
    	<xsl:otherwise>0</xsl:otherwise>
    </xsl:choose></xsl:variable>
    
    <input type="hidden" name="taskrowid" value="{@taskrowid}"/>

    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">task-params</xsl:with-param>
    </xsl:call-template>

    <tr>
        <td>&#160;</td>
        <td>
            <p class="advanced"><a href="CreateStrictOrPeriodicTask"><xsl:value-of select="mioga:gettext('Advanced form')"/></a></p>
        </td>
    </tr>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">name</xsl:with-param>
        <xsl:with-param name="value" select="@name"/>
        <xsl:with-param name="maxlength">128</xsl:with-param>
        <xsl:with-param name="disabled" select="$disabled"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputSelect">
        <xsl:with-param name="label">category_id</xsl:with-param>
        <xsl:with-param name="value" select="@category_id"/>
        <xsl:with-param name="choices" select="task_category/@rowid"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputDate">
        <xsl:with-param name="label">startDate</xsl:with-param>
        <xsl:with-param name="year-value" select="@year"/>
        <xsl:with-param name="month-value" select="@month"/>
        <xsl:with-param name="day-value" select="@day"/>
        <xsl:with-param name="year-name">year</xsl:with-param>
        <xsl:with-param name="month-name">month</xsl:with-param>
        <xsl:with-param name="day-name">day</xsl:with-param>
        <xsl:with-param name="first-year" select="@startYear"/>
        <xsl:with-param name="nb-year" select="50"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputDate">
        <xsl:with-param name="label">endDate</xsl:with-param>
        <xsl:with-param name="year-value" select="@endYear"/>
        <xsl:with-param name="month-value" select="@endMonth"/>
        <xsl:with-param name="day-value" select="@endDay"/>
        <xsl:with-param name="year-name">endYear</xsl:with-param>
        <xsl:with-param name="month-name">endMonth</xsl:with-param>
        <xsl:with-param name="day-name">endDay</xsl:with-param>
        <xsl:with-param name="first-year" select="@startYear"/>
        <xsl:with-param name="nb-year" select="50"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputTextArea">
        <xsl:with-param name="label">description</xsl:with-param>
        <xsl:with-param name="value" select="description"/>
        <xsl:with-param name="cols">50</xsl:with-param>
        <xsl:with-param name="rows">6</xsl:with-param>
        <xsl:with-param name="disposition">0</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">outside</xsl:with-param>
        <xsl:with-param name="value" select="@outside"/>
    </xsl:call-template>
    
    <xsl:choose>
    	<xsl:when test="@mode = 'booking'">
            <xsl:call-template name="MiogaFormInputHidden">
                <xsl:with-param name="label">waiting_book</xsl:with-param>
                <xsl:with-param name="value">1</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
    	<xsl:otherwise>
            <xsl:call-template name="MiogaFormInputCheckbox">
                <xsl:with-param name="label">waiting_book</xsl:with-param>
                <xsl:with-param name="value" select="@waiting_book"/>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>

</xsl:template>

<xsl:template match="CreateSimplePeriodicTask" mode="MiogaFormButton">
    <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">create</xsl:with-param>
        <xsl:with-param name="referer" select="@referer"/>
    </xsl:call-template>    
</xsl:template>

<!-- *********************************************************************** -->
<!-- * EditSimplePeriodicTask                                            * -->
<!-- *********************************************************************** -->

<xsl:template match="EditSimplePeriodicTask" mode="MiogaFormBody">
    <xsl:variable name="disabled"><xsl:choose>
    	<xsl:when test="@mode = 'booking'">1</xsl:when>
    	<xsl:otherwise>0</xsl:otherwise>
    </xsl:choose></xsl:variable>

    <input type="hidden" name="taskrowid" value="{@taskrowid}"/>

    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">task-params</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormTinyHorizSep"/>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">name</xsl:with-param>
        <xsl:with-param name="value" select="@name"/>
        <xsl:with-param name="maxlength">128</xsl:with-param>
        <xsl:with-param name="disabled" select="$disabled"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputSelect">
        <xsl:with-param name="label">category_id</xsl:with-param>
        <xsl:with-param name="value" select="@category_id"/>
        <xsl:with-param name="choices" select="task_category/@rowid"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputDate">
        <xsl:with-param name="label">startDate</xsl:with-param>
        <xsl:with-param name="year-value" select="@year"/>
        <xsl:with-param name="month-value" select="@month"/>
        <xsl:with-param name="day-value" select="@day"/>
        <xsl:with-param name="year-name">year</xsl:with-param>
        <xsl:with-param name="month-name">month</xsl:with-param>
        <xsl:with-param name="day-name">day</xsl:with-param>
        <xsl:with-param name="first-year" select="@startYear"/>
        <xsl:with-param name="nb-year" select="50"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputDate">
        <xsl:with-param name="label">endDate</xsl:with-param>
        <xsl:with-param name="year-value" select="@endYear"/>
        <xsl:with-param name="month-value" select="@endMonth"/>
        <xsl:with-param name="day-value" select="@endDay"/>
        <xsl:with-param name="year-name">endYear</xsl:with-param>
        <xsl:with-param name="month-name">endMonth</xsl:with-param>
        <xsl:with-param name="day-name">endDay</xsl:with-param>
        <xsl:with-param name="first-year" select="@startYear"/>
        <xsl:with-param name="nb-year" select="50"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputTextArea">
        <xsl:with-param name="label">description</xsl:with-param>
        <xsl:with-param name="value" select="description"/>
        <xsl:with-param name="cols">50</xsl:with-param>
        <xsl:with-param name="rows">6</xsl:with-param>
        <xsl:with-param name="disposition">0</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">outside</xsl:with-param>
        <xsl:with-param name="value" select="@outside"/>
    </xsl:call-template>
    
    <xsl:choose>
    	<xsl:when test="@mode = 'booking'">
            <xsl:call-template name="MiogaFormInputHidden">
                <xsl:with-param name="label">waiting_book</xsl:with-param>
                <xsl:with-param name="value">1</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
    	<xsl:otherwise>
            <xsl:call-template name="MiogaFormInputCheckbox">
                <xsl:with-param name="label">waiting_book</xsl:with-param>
                <xsl:with-param name="value" select="@waiting_book"/>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>

</xsl:template>

<xsl:template match="EditSimplePeriodicTask" mode="MiogaFormButton">
    <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">change</xsl:with-param>
        <xsl:with-param name="referer" select="@referer"/>
    </xsl:call-template>    
</xsl:template>

<!-- *********************************************************************** -->
<!-- * CreateStrictOrPeriodicTask                                          * -->
<!-- *********************************************************************** -->

<xsl:template match="CreateStrictOrPeriodicTask" mode="MiogaFormBody">
    <xsl:variable name="disabled"><xsl:choose>
    	<xsl:when test="@mode = 'booking'">1</xsl:when>
    	<xsl:otherwise>0</xsl:otherwise>
    </xsl:choose></xsl:variable>
    
    <input type="hidden" name="taskrowid" value="{@taskrowid}"/>

    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">task-params</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormTinyHorizSep"/>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">name</xsl:with-param>
        <xsl:with-param name="value" select="@name"/>
        <xsl:with-param name="maxlength">128</xsl:with-param>
        <xsl:with-param name="disabled" select="$disabled"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputSelect">
        <xsl:with-param name="label">category_id</xsl:with-param>
        <xsl:with-param name="value" select="@category_id"/>
        <xsl:with-param name="choices" select="task_category/@rowid"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputDate">
        <xsl:with-param name="label">date</xsl:with-param>
        <xsl:with-param name="year-value" select="@year"/>
        <xsl:with-param name="month-value" select="@month"/>
        <xsl:with-param name="day-value" select="@day"/>
        <xsl:with-param name="year-name">year</xsl:with-param>
        <xsl:with-param name="month-name">month</xsl:with-param>
        <xsl:with-param name="day-name">day</xsl:with-param>
        <xsl:with-param name="first-year" select="@startYear"/>
        <xsl:with-param name="nb-year" select="50"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputTimeInterval">
        <xsl:with-param name="label">schedule</xsl:with-param>
        <xsl:with-param name="use-second">0</xsl:with-param>
        <xsl:with-param name="hour-value-1" select="@startHour"/>
        <xsl:with-param name="min-value-1" select="@startMin"/>
        <xsl:with-param name="hour-value-2" select="@stopHour"/>
        <xsl:with-param name="min-value-2" select="@stopMin"/>
        <xsl:with-param name="hour-name-1">startHour</xsl:with-param>
        <xsl:with-param name="min-name-1">startMin</xsl:with-param>
        <xsl:with-param name="hour-name-2">stopHour</xsl:with-param>
        <xsl:with-param name="min-name-2">stopMin</xsl:with-param>
        <xsl:with-param name="minute-step">5</xsl:with-param>
        <xsl:with-param name="can-be-empty">1</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputTextArea">
        <xsl:with-param name="label">description</xsl:with-param>
        <xsl:with-param name="value" select="description"/>
        <xsl:with-param name="cols">50</xsl:with-param>
        <xsl:with-param name="rows">6</xsl:with-param>
        <xsl:with-param name="disposition">0</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">outside</xsl:with-param>
        <xsl:with-param name="value" select="@outside"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormVertRule"/>

    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">repetitive-task</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormTinyHorizSep"/>

    <xsl:call-template name="MiogaFormInputExternalSelectSingle">
        <xsl:with-param name="label">periodic</xsl:with-param>
        <xsl:with-param name="value">
            <xsl:call-template name="Task-periodic-string">
                <xsl:with-param name="value_periodType"  select="@periodType"/>
                <xsl:with-param name="value_endDay"      select="@endDay"/>
                <xsl:with-param name="value_endMonth"    select="@endMonth"/>
                <xsl:with-param name="value_endYear"     select="@endYear"/>
                <xsl:with-param name="value_dayFreq"     select="@dayFreq"/>
                <xsl:with-param name="value_weekFreq"    select="@weekFreq"/>
                <xsl:with-param name="value_weekWeekDay" select="@weekWeekDay"/>
                <xsl:with-param name="value_monthFreq"   select="@monthFreq"/>
                <xsl:with-param name="value_monthPeriodType"   select="@monthPeriodType"/>
                <xsl:with-param name="value_monthDay"    select="@monthDay"/>
                <xsl:with-param name="value_monthWeek"   select="@monthWeek"/>
                <xsl:with-param name="value_monthWeekDay"      select="@monthWeekDay"/>
                <xsl:with-param name="value_yearFreq"    select="@yearFreq"/>
                <xsl:with-param name="value_yearDay"     select="@yearDay"/>
                <xsl:with-param name="value_yearMonth"   select="@yearMonth"/>
            </xsl:call-template>&#160;<xsl:if test="@periodType != 'planned'">
                <xsl:call-template name="Task-day-month-year-string">
                    <xsl:with-param name="value_day"     select="@endDay"/>
                    <xsl:with-param name="value_month"   select="@endMonth"/>
                    <xsl:with-param name="value_year"    select="@endYear"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:with-param>
        <xsl:with-param name="action">periodicity</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormTinyHorizRule"/>

    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">contact-label</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormTinyHorizSep"/>

    <xsl:choose>
        <xsl:when test="@display_icon = 1">
            <xsl:call-template name="MiogaFormInputExternalSelectSingle">
                <xsl:with-param name="label">externalcontact</xsl:with-param>
                <xsl:with-param name="value"></xsl:with-param>
                <xsl:with-param name="action">contact</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">contact</xsl:with-param>
                <xsl:with-param name="value"></xsl:with-param>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">first_name</xsl:with-param>
        <xsl:with-param name="value" select="@first_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">last_name</xsl:with-param>
        <xsl:with-param name="value" select="@last_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">telephone</xsl:with-param>
        <xsl:with-param name="value" select="@telephone"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">fax</xsl:with-param>
        <xsl:with-param name="value" select="@fax"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">email</xsl:with-param>
        <xsl:with-param name="value" select="@email"/>
    </xsl:call-template>

    <xsl:if test="@mode = 'booking'">
        <xsl:call-template name="MiogaFormInputHidden">
            <xsl:with-param name="label">waiting_book</xsl:with-param>
            <xsl:with-param name="value">1</xsl:with-param>
        </xsl:call-template>
    </xsl:if>
</xsl:template>

<xsl:template match="CreateStrictOrPeriodicTask" mode="MiogaFormButton">
    <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">create</xsl:with-param>
        <xsl:with-param name="referer" select="@referer"/>
    </xsl:call-template>    
</xsl:template>

<!-- *********************************************************************** -->
<!-- * CreateStrictOrPeriodicTaskPeriodicity                               * -->
<!-- *********************************************************************** -->

<xsl:template match="CreateStrictOrPeriodicTaskPeriodicity" mode="MiogaFormButton">
    <xsl:call-template name="ok-form-button">
        <xsl:with-param name="name">ok_periodicity</xsl:with-param>
    </xsl:call-template>&#160;<xsl:call-template name="cancel-form-button">
        <xsl:with-param name="name">cancel_periodicity</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template match="CreateStrictOrPeriodicTaskPeriodicity" mode="MiogaFormBody">
    <input type="hidden" name="taskrowid" value="{@taskrowid}"/>

    <xsl:call-template name="Task-Periodic-Form">
        <xsl:with-param name="param_periodType"        select="'periodType'"/>
        <xsl:with-param name="value_periodType"        select="@periodType"/>
        <xsl:with-param name="value_endDay"            select="@endDay"/>
        <xsl:with-param name="value_endMonth"          select="@endMonth"/>
        <xsl:with-param name="value_endYear"           select="@endYear"/>
        <xsl:with-param name="value_dayFreq"           select="@dayFreq"/>
        <xsl:with-param name="value_weekFreq"          select="@weekFreq"/>
        <xsl:with-param name="value_weekWeekDay"       select="@weekWeekDay"/>
        <xsl:with-param name="value_monthFreq"         select="@monthFreq"/>
        <xsl:with-param name="value_monthPeriodType"   select="@monthPeriodType"/>
        <xsl:with-param name="value_monthDay"          select="@monthDay"/>
        <xsl:with-param name="value_monthWeek"         select="@monthWeek"/>
        <xsl:with-param name="value_monthWeekDay"      select="@monthWeekDay"/>
        <xsl:with-param name="value_yearFreq"          select="@yearFreq"/>
        <xsl:with-param name="value_yearDay"           select="@yearDay"/>
        <xsl:with-param name="value_yearMonth"         select="@yearMonth"/>
        <xsl:with-param name="strictBool"              select="1"/>
    </xsl:call-template>

</xsl:template>

<xsl:template match="CreateStrictOrPeriodicTaskPeriodicity">
   <body xsl:use-attribute-sets="body_attr">
     <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">gestiontemps.html#organizer</xsl:with-param>
        </xsl:call-template>
     <xsl:call-template name="MiogaTitle"/>
     <br/>

     <div xsl:use-attribute-sets="app_form_body_attr">
     
         <xsl:call-template name="MiogaForm">
             <xsl:with-param name="label">CreateStrictOrPeriodicTask</xsl:with-param>
             <xsl:with-param name="action">CreateStrictOrPeriodicTask</xsl:with-param>
             <xsl:with-param name="method">POST</xsl:with-param>
             <xsl:with-param name="no-title">1</xsl:with-param>
         </xsl:call-template>
         
     </div>
</body>
</xsl:template>

<!-- *********************************************************************** -->
<!-- * EditStrictTask                                                      * -->
<!-- *********************************************************************** -->
<xsl:template match="EditStrictTask" mode="MiogaFormBody">
    <xsl:variable name="disabled"><xsl:choose>
    	<xsl:when test="@waiting_book = 1">1</xsl:when>
    	<xsl:otherwise>0</xsl:otherwise>
    </xsl:choose></xsl:variable>
    
    <input type="hidden" name="taskrowid" value="{@taskrowid}"/>
    
    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">task-params</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormTinyHorizSep"/>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">name</xsl:with-param>
        <xsl:with-param name="value" select="@name"/>
        <xsl:with-param name="maxlength">128</xsl:with-param>
        <xsl:with-param name="disabled" select="$disabled"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputSelect">
        <xsl:with-param name="label">category_id</xsl:with-param>
        <xsl:with-param name="value" select="@category_id"/>
        <xsl:with-param name="choices" select="task_category/@rowid"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputDate">
        <xsl:with-param name="label">date</xsl:with-param>
        <xsl:with-param name="year-value" select="@year"/>
        <xsl:with-param name="month-value" select="@month"/>
        <xsl:with-param name="day-value" select="@day"/>
        <xsl:with-param name="year-name">year</xsl:with-param>
        <xsl:with-param name="month-name">month</xsl:with-param>
        <xsl:with-param name="day-name">day</xsl:with-param>
        <xsl:with-param name="first-year" select="@startYear"/>
        <xsl:with-param name="nb-year" select="50"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputTimeInterval">
        <xsl:with-param name="label">schedule</xsl:with-param>
        <xsl:with-param name="use-second">0</xsl:with-param>
        <xsl:with-param name="hour-value-1" select="@startHour"/>
        <xsl:with-param name="min-value-1" select="@startMin"/>
        <xsl:with-param name="hour-value-2" select="@stopHour"/>
        <xsl:with-param name="min-value-2" select="@stopMin"/>
        <xsl:with-param name="hour-name-1">startHour</xsl:with-param>
        <xsl:with-param name="min-name-1">startMin</xsl:with-param>
        <xsl:with-param name="hour-name-2">stopHour</xsl:with-param>
        <xsl:with-param name="min-name-2">stopMin</xsl:with-param>
        <xsl:with-param name="minute-step">5</xsl:with-param>
        <xsl:with-param name="can-be-empty">1</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputTextArea">
        <xsl:with-param name="label">description</xsl:with-param>
        <xsl:with-param name="value" select="description"/>
        <xsl:with-param name="cols">50</xsl:with-param>
        <xsl:with-param name="rows">6</xsl:with-param>
        <xsl:with-param name="disposition">0</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">outside</xsl:with-param>
        <xsl:with-param name="value" select="@outside"/>
    </xsl:call-template>
    

    <xsl:call-template name="MiogaFormVertRule"/>

    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">convert-todo</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormTinyHorizSep"/>
    
    <xsl:call-template name="Task-Change-Priority-Form">
        <xsl:with-param name="label">priority</xsl:with-param>
        <xsl:with-param name="value"  select="@priority"/>
        <xsl:with-param name="action">transform_todo</xsl:with-param>
    </xsl:call-template>


    <xsl:call-template name="MiogaFormTinyHorizRule"/>

    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">contact</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormTinyHorizSep"/>
       
    <xsl:choose>
        <xsl:when test="@display_icon = 1">
            <xsl:call-template name="MiogaFormInputExternalSelectSingle">
                <xsl:with-param name="label">externalcontact</xsl:with-param>
                <xsl:with-param name="value"></xsl:with-param>
                <xsl:with-param name="action">contact</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">contact</xsl:with-param>
                <xsl:with-param name="value"></xsl:with-param>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>  

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">first_name</xsl:with-param>
        <xsl:with-param name="value" select="@first_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">last_name</xsl:with-param>
        <xsl:with-param name="value" select="@last_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">telephone</xsl:with-param>
        <xsl:with-param name="value" select="@telephone"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">fax</xsl:with-param>
        <xsl:with-param name="value" select="@fax"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">email</xsl:with-param>
        <xsl:with-param name="value" select="@email"/>
    </xsl:call-template>

    <xsl:if test="@waiting_book = 1">
        <xsl:call-template name="MiogaFormInputHidden">
            <xsl:with-param name="label">waiting_book</xsl:with-param>
            <xsl:with-param name="value">1</xsl:with-param>
        </xsl:call-template>
    </xsl:if>
</xsl:template>

<xsl:template match="EditStrictTask" mode="MiogaFormButton">
    <xsl:call-template name="ok-form-button">
        <xsl:with-param name="name">change</xsl:with-param>
    </xsl:call-template>&#160;<xsl:call-template name="delete-form-button">
        <xsl:with-param name="name">delete</xsl:with-param>
    </xsl:call-template>&#160;<xsl:call-template name="cancel-button">
        <xsl:with-param name="href" select="@referer"/>
    </xsl:call-template>
</xsl:template>


<!-- *********************************************************************** -->
<!-- * ViewStrictTask                                                      * -->
<!-- *********************************************************************** -->

<xsl:template match="ViewStrictTask" mode="MiogaReportBody">

    <xsl:call-template name="MiogaReportTitle">
        <xsl:with-param name="label">task-params</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormTinyHorizSep"/>
    
    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">name</xsl:with-param>
        <xsl:with-param name="value" select="@name"/>
        <xsl:with-param name="maxlength">128</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">category_id</xsl:with-param>
        <xsl:with-param name="value" select="task_category[@rowid = current()/@category_id]"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportDate">
        <xsl:with-param name="label">date</xsl:with-param>
        <xsl:with-param name="year-value" select="@year"/>
        <xsl:with-param name="month-value" select="@month"/>
        <xsl:with-param name="day-value" select="@day"/>
        <xsl:with-param name="year-name">year</xsl:with-param>
        <xsl:with-param name="month-name">month</xsl:with-param>
        <xsl:with-param name="day-name">day</xsl:with-param>
        <xsl:with-param name="first-year" select="@startYear"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaReportTimeInterval">
        <xsl:with-param name="label">schedule</xsl:with-param>
        <xsl:with-param name="use-second">0</xsl:with-param>
        <xsl:with-param name="hour-value-1" select="@startHour"/>
        <xsl:with-param name="min-value-1" select="@startMin"/>
        <xsl:with-param name="hour-value-2" select="@stopHour"/>
        <xsl:with-param name="min-value-2" select="@stopMin"/>
        <xsl:with-param name="hour-name-1">startHour</xsl:with-param>
        <xsl:with-param name="min-name-1">startMin</xsl:with-param>
        <xsl:with-param name="hour-name-2">stopHour</xsl:with-param>
        <xsl:with-param name="min-name-2">stopMin</xsl:with-param>
    </xsl:call-template>
    

    <xsl:call-template name="MiogaReportTextArea">
        <xsl:with-param name="label">description</xsl:with-param>
        <xsl:with-param name="value" select="description"/>
        <xsl:with-param name="width">50</xsl:with-param>
        <xsl:with-param name="disposition">0</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportCheckbox">
        <xsl:with-param name="label">outside</xsl:with-param>
        <xsl:with-param name="value" select="task_category[@rowid = current()/@category_id]"/>
    </xsl:call-template>


    <xsl:call-template name="MiogaFormVertRule"/>

    <xsl:call-template name="MiogaReportTitle">
        <xsl:with-param name="label">contact</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormTinyHorizSep"/>
     
    <xsl:choose>
        <xsl:when test="@display_icon = 1">
            <xsl:call-template name="MiogaFormInputExternalSelectSingle">
                <xsl:with-param name="label">externalcontact</xsl:with-param>
                <xsl:with-param name="value"></xsl:with-param>
                <xsl:with-param name="action">contact</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">contact</xsl:with-param>
                <xsl:with-param name="value"></xsl:with-param>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>  

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">first_name</xsl:with-param>
        <xsl:with-param name="value" select="@first_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">last_name</xsl:with-param>
        <xsl:with-param name="value" select="@last_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">telephone</xsl:with-param>
        <xsl:with-param name="value" select="@telephone"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">fax</xsl:with-param>
        <xsl:with-param name="value" select="@fax"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">email</xsl:with-param>
        <xsl:with-param name="value" select="@email"/>
    </xsl:call-template>

</xsl:template>

<xsl:template match="ViewStrictTask" mode="MiogaReportButton">
    <xsl:if test="@canwrite=1">
        <xsl:call-template name="edit-button">
            <xsl:with-param name="href">EditStrictTask?taskrowid=<xsl:value-of select="@rowid"/>&amp;mode=initial&amp;referer=<xsl:value-of select="@escreferer"/></xsl:with-param>
        </xsl:call-template>&#160;</xsl:if><xsl:call-template name="back-button">
        <xsl:with-param name="href" select="@referer"/>
    </xsl:call-template>

</xsl:template>


<!-- *********************************************************************** -->
<!-- * EditTodoTask and CreateToDoTask                                     * -->
<!-- *********************************************************************** -->

<xsl:template match="EditTodoTask|CreateToDoTask" mode="MiogaFormBody">
    <input type="hidden" name="taskrowid" value="{@taskrowid}"/>
    
    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">task-params</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormTinyHorizSep"/>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">name</xsl:with-param>
        <xsl:with-param name="value" select="@name"/>
        <xsl:with-param name="maxlength">128</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="Task-Priority-Form">
        <xsl:with-param name="label">priority</xsl:with-param>
        <xsl:with-param name="value"  select="@priority"/>
    </xsl:call-template>
  
    <xsl:call-template name="MiogaFormInputSelect">
        <xsl:with-param name="label">category_id</xsl:with-param>
        <xsl:with-param name="value" select="@category_id"/>
        <xsl:with-param name="choices" select="task_category/@rowid"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">outside</xsl:with-param>
        <xsl:with-param name="value" select="@outside"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputTextArea">
        <xsl:with-param name="label">description</xsl:with-param>
        <xsl:with-param name="value" select="description"/>
        <xsl:with-param name="cols">50</xsl:with-param>
        <xsl:with-param name="rows">6</xsl:with-param>
        <xsl:with-param name="disposition">0</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormVertRule"/>

    <xsl:if test="name(.) = 'EditTodoTask'">
        <xsl:call-template name="MiogaFormTitle">
            <xsl:with-param name="label">convert-strict</xsl:with-param>
        </xsl:call-template>
        
        
        <xsl:call-template name="MiogaFormTinyHorizSep"/>
        

        <xsl:call-template name="MiogaFormInputDate">
            <xsl:with-param name="label">date</xsl:with-param>
            <xsl:with-param name="year-value" select="@year"/>
            <xsl:with-param name="month-value" select="@month"/>
            <xsl:with-param name="day-value" select="@day"/>
            <xsl:with-param name="year-name">year</xsl:with-param>
            <xsl:with-param name="month-name">month</xsl:with-param>
            <xsl:with-param name="day-name">day</xsl:with-param>
            <xsl:with-param name="first-year" select="@startYear"/>
            <xsl:with-param name="nb-year" select="50"/>
        </xsl:call-template>
        
        <xsl:call-template name="MiogaFormInputTimeInterval">
            <xsl:with-param name="label">schedule</xsl:with-param>
            <xsl:with-param name="use-second">0</xsl:with-param>
            <xsl:with-param name="hour-value-1" select="@startHour"/>
            <xsl:with-param name="min-value-1" select="@startMin"/>
            <xsl:with-param name="hour-value-2" select="@stopHour"/>
            <xsl:with-param name="min-value-2" select="@stopMin"/>
            <xsl:with-param name="hour-name-1">startHour</xsl:with-param>
            <xsl:with-param name="min-name-1">startMin</xsl:with-param>
            <xsl:with-param name="hour-name-2">stopHour</xsl:with-param>
            <xsl:with-param name="min-name-2">stopMin</xsl:with-param>
            <xsl:with-param name="minute-step">5</xsl:with-param>
        </xsl:call-template>
        
        <xsl:call-template name="MiogaFormInputMisc">
            <xsl:with-param name="label">planify</xsl:with-param>
            <xsl:with-param name="content">
                <xsl:call-template name="ok-form-button">
                    <xsl:with-param name="name">transform_strict</xsl:with-param>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
        
        <xsl:call-template name="MiogaFormTinyHorizRule"/>
    </xsl:if>

    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">contact</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormTinyHorizSep"/>
   
    <xsl:choose>
        <xsl:when test="@display_icon = 1">
            <xsl:call-template name="MiogaFormInputExternalSelectSingle">
                <xsl:with-param name="label">externalcontact</xsl:with-param>
                <xsl:with-param name="value"></xsl:with-param>
                <xsl:with-param name="action">contact</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">contact</xsl:with-param>
                <xsl:with-param name="value"></xsl:with-param>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>  

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">first_name</xsl:with-param>
        <xsl:with-param name="value" select="@first_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">last_name</xsl:with-param>
        <xsl:with-param name="value" select="@last_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">telephone</xsl:with-param>
        <xsl:with-param name="value" select="@telephone"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">fax</xsl:with-param>
        <xsl:with-param name="value" select="@fax"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">email</xsl:with-param>
        <xsl:with-param name="value" select="@email"/>
    </xsl:call-template>    
   
</xsl:template>

<xsl:template match="EditTodoTask" mode="MiogaFormButton">
    <xsl:call-template name="ok-form-button">
        <xsl:with-param name="name">change</xsl:with-param>
    </xsl:call-template>&#160;<xsl:call-template name="delete-form-button">
        <xsl:with-param name="name">delete</xsl:with-param>
    </xsl:call-template>&#160;<xsl:call-template name="cancel-button">
        <xsl:with-param name="href" select="@referer"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="CreateToDoTask" mode="MiogaFormButton">
    
    <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">create</xsl:with-param>
        <xsl:with-param name="referer" select="@referer"/>
    </xsl:call-template>

</xsl:template>



<!-- *********************************************************************** -->
<!-- * ViewTodoTask                                                        * -->
<!-- *********************************************************************** -->

<xsl:template match="ViewTodoTask" mode="MiogaReportBody">

     <xsl:call-template name="MiogaReportTitle">
        <xsl:with-param name="label">task-params</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormTinyHorizSep"/>
    
    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">name</xsl:with-param>
        <xsl:with-param name="value" select="@name"/>
        <xsl:with-param name="maxlength">128</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">priority</xsl:with-param>
        <xsl:with-param name="value">
            <xsl:call-template name="Task-priority-name">
                <xsl:with-param name="task_priority" select="@priority"/>
            </xsl:call-template>
        </xsl:with-param>
    </xsl:call-template>
  
    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">category_id</xsl:with-param>
        <xsl:with-param name="value" select="task_category[@rowid = current()/@category_id]"/>
    </xsl:call-template>
   

    <xsl:call-template name="MiogaReportTextArea">
        <xsl:with-param name="label">description</xsl:with-param>
        <xsl:with-param name="value" select="description"/>
        <xsl:with-param name="width">50</xsl:with-param>
        <xsl:with-param name="disposition">0</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportCheckbox">
        <xsl:with-param name="label">outside</xsl:with-param>
        <xsl:with-param name="value" select="task_category[@rowid = current()/@category_id]"/>
    </xsl:call-template>


    <xsl:call-template name="MiogaFormVertRule"/>

    <xsl:call-template name="MiogaReportTitle">
        <xsl:with-param name="label">contact</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormTinyHorizSep"/>
    
    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">first_name</xsl:with-param>
        <xsl:with-param name="value" select="@first_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">last_name</xsl:with-param>
        <xsl:with-param name="value" select="@last_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">telephone</xsl:with-param>
        <xsl:with-param name="value" select="@telephone"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">fax</xsl:with-param>
        <xsl:with-param name="value" select="@fax"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">email</xsl:with-param>
        <xsl:with-param name="value" select="@email"/>
    </xsl:call-template>

</xsl:template>


<xsl:template match="ViewTodoTask" mode="MiogaReportButton">

    <xsl:call-template name="back-button">
        <xsl:with-param name="href" select="@referer"/>
    </xsl:call-template>

</xsl:template>



<!-- *********************************************************************** -->
<!-- * EditPeriodicTask                                                    * -->
<!-- *********************************************************************** -->
<xsl:template match="ViewPeriodicTask" mode="MiogaReportBody">
    <xsl:call-template name="PeriodicTaskBody"/>
</xsl:template>

<xsl:template match="EditPeriodicTask" mode="MiogaFormBody" name="PeriodicTaskBody">
    <input type="hidden" name="taskrowid" value="{@taskrowid}"/>

    <xsl:call-template name="MiogaReportTitle">
        <xsl:with-param name="label">task-params</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">name</xsl:with-param>
        <xsl:with-param name="value" select="@name"/>
        <xsl:with-param name="maxlength">128</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaReportDate">
        <xsl:with-param name="label">date</xsl:with-param>
        <xsl:with-param name="year-value" select="@year"/>
        <xsl:with-param name="month-value" select="@month"/>
        <xsl:with-param name="day-value" select="@day"/>
        <xsl:with-param name="year-name">year</xsl:with-param>
        <xsl:with-param name="month-name">month</xsl:with-param>
        <xsl:with-param name="day-name">day</xsl:with-param>
        <xsl:with-param name="first-year" select="@startYear"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaReportTimeInterval">
        <xsl:with-param name="label">schedule</xsl:with-param>
        <xsl:with-param name="use-second">0</xsl:with-param>
        <xsl:with-param name="hour-value-1" select="@startHour"/>
        <xsl:with-param name="min-value-1" select="@startMin"/>
        <xsl:with-param name="hour-value-2" select="@stopHour"/>
        <xsl:with-param name="min-value-2" select="@stopMin"/>
        <xsl:with-param name="hour-name-1">startHour</xsl:with-param>
        <xsl:with-param name="min-name-1">startMin</xsl:with-param>
        <xsl:with-param name="hour-name-2">stopHour</xsl:with-param>
        <xsl:with-param name="min-name-2">stopMin</xsl:with-param>
        <xsl:with-param name="minute-step">5</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaReportTextArea">
        <xsl:with-param name="label">description</xsl:with-param>
        <xsl:with-param name="value" select="description"/>
        <xsl:with-param name="width">50</xsl:with-param>
        <xsl:with-param name="disposition">0</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportCheckbox">
        <xsl:with-param name="label">outside</xsl:with-param>
        <xsl:with-param name="value" select="task_category[@rowid = current()/@category_id]"/>
    </xsl:call-template>

    
    <xsl:call-template name="MiogaFormTinyVertRule"/>

    <xsl:call-template name="MiogaReportTitle">
        <xsl:with-param name="label">contact</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">first_name</xsl:with-param>
        <xsl:with-param name="value" select="@first_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">last_name</xsl:with-param>
        <xsl:with-param name="value" select="@last_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">telephone</xsl:with-param>
        <xsl:with-param name="value" select="@telephone"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">fax</xsl:with-param>
        <xsl:with-param name="value" select="@fax"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">email</xsl:with-param>
        <xsl:with-param name="value" select="@email"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormHorizRule"/>

    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">repetitive-task</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputMisc">
        <xsl:with-param name="label">periodic</xsl:with-param>
        <xsl:with-param name="content">
            <xsl:call-template name="Task-periodic-string">
                <xsl:with-param name="value_periodType"  select="@periodType"/>
                <xsl:with-param name="value_endDay"      select="@endDay"/>
                <xsl:with-param name="value_endMonth"    select="@endMonth"/>
                <xsl:with-param name="value_endYear"     select="@endYear"/>
                <xsl:with-param name="value_dayFreq"     select="@dayFreq"/>
                <xsl:with-param name="value_weekFreq"    select="@weekFreq"/>
                <xsl:with-param name="value_weekWeekDay" select="@weekWeekDay"/>
                <xsl:with-param name="value_monthFreq"   select="@monthFreq"/>
                <xsl:with-param name="value_monthPeriodType"   select="@monthPeriodType"/>
                <xsl:with-param name="value_monthDay"    select="@monthDay"/>
                <xsl:with-param name="value_monthWeek"   select="@monthWeek"/>
                <xsl:with-param name="value_monthWeekDay"      select="@monthWeekDay"/>
                <xsl:with-param name="value_yearFreq"    select="@yearFreq"/>
                <xsl:with-param name="value_yearDay"     select="@yearDay"/>
                <xsl:with-param name="value_yearMonth"   select="@yearMonth"/>
            </xsl:call-template>&#160;<xsl:if test="@periodType != 'planned'">
                <xsl:call-template name="Task-day-month-year-string">
                    <xsl:with-param name="value_day"     select="@endDay"/>
                    <xsl:with-param name="value_month"   select="@endMonth"/>
                    <xsl:with-param name="value_year"    select="@endYear"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:with-param>
    </xsl:call-template>

    <xsl:if test="name(.) = 'EditPeriodicTask'">
        <xsl:call-template name="MiogaFormHorizRule"/>
        
        <xsl:call-template name="MiogaReportTitle">
            <xsl:with-param name="label">actions</xsl:with-param>
        </xsl:call-template>
        
        <xsl:call-template name="MiogaReportMisc">
            <xsl:with-param name="label">delete-instance</xsl:with-param>
            <xsl:with-param name="content">
                <xsl:call-template name="delete-form-button">
                    <xsl:with-param name="name">delete</xsl:with-param>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
        
        <xsl:call-template name="MiogaReportMisc">
            <xsl:with-param name="label">delete-task</xsl:with-param>
            <xsl:with-param name="content">
                <xsl:call-template name="delete-button">
                    <xsl:with-param name="href">ChangePeriodicTask?taskrowid=<xsl:value-of select="@taskrowid"/>&amp;delete=1</xsl:with-param>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
        
        <xsl:call-template name="MiogaReportMisc">
            <xsl:with-param name="label">edit-task</xsl:with-param>
            <xsl:with-param name="content">
                <xsl:call-template name="change-form-button">
                    <xsl:with-param name="name">change</xsl:with-param>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:if>
</xsl:template>

<xsl:template match="EditPeriodicTask" mode="MiogaFormButton">
    <xsl:call-template name="back-button">
        <xsl:with-param name="href" select="@referer"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="ViewPeriodicTask" mode="MiogaReportButton">
    <xsl:call-template name="back-button">
        <xsl:with-param name="href" select="@referer"/>
    </xsl:call-template>
</xsl:template>

<!-- *********************************************************************** -->
<!-- * ChangePeriodicTask                                                  * -->
<!-- *********************************************************************** -->

<xsl:template match="ChangePeriodicTask" mode="MiogaFormBody">
    
    <input type="hidden" name="taskrowid" value="{@taskrowid}"/>

    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">task-params</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormTinyHorizSep"/>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">name</xsl:with-param>
        <xsl:with-param name="value" select="@name"/>
        <xsl:with-param name="maxlength">128</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputSelect">
        <xsl:with-param name="label">category_id</xsl:with-param>
        <xsl:with-param name="value" select="@category_id"/>
        <xsl:with-param name="choices" select="task_category/@rowid"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputDate">
        <xsl:with-param name="label">date</xsl:with-param>
        <xsl:with-param name="year-value" select="@year"/>
        <xsl:with-param name="month-value" select="@month"/>
        <xsl:with-param name="day-value" select="@day"/>
        <xsl:with-param name="year-name">year</xsl:with-param>
        <xsl:with-param name="month-name">month</xsl:with-param>
        <xsl:with-param name="day-name">day</xsl:with-param>
        <xsl:with-param name="first-year" select="@startYear"/>
        <xsl:with-param name="nb-year" select="50"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputTimeInterval">
        <xsl:with-param name="label">schedule</xsl:with-param>
        <xsl:with-param name="use-second">0</xsl:with-param>
        <xsl:with-param name="hour-value-1" select="@startHour"/>
        <xsl:with-param name="min-value-1" select="@startMin"/>
        <xsl:with-param name="hour-value-2" select="@stopHour"/>
        <xsl:with-param name="min-value-2" select="@stopMin"/>
        <xsl:with-param name="hour-name-1">startHour</xsl:with-param>
        <xsl:with-param name="min-name-1">startMin</xsl:with-param>
        <xsl:with-param name="hour-name-2">stopHour</xsl:with-param>
        <xsl:with-param name="min-name-2">stopMin</xsl:with-param>
        <xsl:with-param name="minute-step">5</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputTextArea">
        <xsl:with-param name="label">description</xsl:with-param>
        <xsl:with-param name="value" select="description"/>
        <xsl:with-param name="cols">50</xsl:with-param>
        <xsl:with-param name="rows">6</xsl:with-param>
        <xsl:with-param name="disposition">0</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">outside</xsl:with-param>
        <xsl:with-param name="value" select="@outside"/>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormVertRule"/>

    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">repetitive-task</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormTinyHorizSep"/>
   
    <xsl:call-template name="Task-Periodic-Form">
        <xsl:with-param name="param_periodType"        select="'periodType'"/>
        <xsl:with-param name="value_periodType"        select="@periodType"/>
        <xsl:with-param name="value_endDay"            select="@endDay"/>
        <xsl:with-param name="value_endMonth"          select="@endMonth"/>
        <xsl:with-param name="value_endYear"           select="@endYear"/>
        <xsl:with-param name="value_dayFreq"           select="@dayFreq"/>
        <xsl:with-param name="value_weekFreq"          select="@weekFreq"/>
        <xsl:with-param name="value_weekWeekDay"       select="@weekWeekDay"/>
        <xsl:with-param name="value_monthFreq"         select="@monthFreq"/>
        <xsl:with-param name="value_monthPeriodType"   select="@monthPeriodType"/>
        <xsl:with-param name="value_monthDay"          select="@monthDay"/>
        <xsl:with-param name="value_monthWeek"         select="@monthWeek"/>
        <xsl:with-param name="value_monthWeekDay"      select="@monthWeekDay"/>
        <xsl:with-param name="value_yearFreq"          select="@yearFreq"/>
        <xsl:with-param name="value_yearDay"           select="@yearDay"/>
        <xsl:with-param name="value_yearMonth"         select="@yearMonth"/>
        <xsl:with-param name="strictBool"              select="1"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormTinyHorizRule"/>

    <xsl:call-template name="MiogaFormTitle">
        <xsl:with-param name="label">contact-label</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormTinyHorizSep"/>
   
    <xsl:choose>
        <xsl:when test="@display_icon = 1">
            <xsl:call-template name="MiogaFormInputExternalSelectSingle">
                <xsl:with-param name="label">externalcontact</xsl:with-param>
                <xsl:with-param name="value"></xsl:with-param>
                <xsl:with-param name="action">contact</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">contact</xsl:with-param>
                <xsl:with-param name="value"></xsl:with-param>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>  
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">first_name</xsl:with-param>
        <xsl:with-param name="value" select="@first_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">last_name</xsl:with-param>
        <xsl:with-param name="value" select="@last_name"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">telephone</xsl:with-param>
        <xsl:with-param name="value" select="@telephone"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">fax</xsl:with-param>
        <xsl:with-param name="value" select="@fax"/>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">email</xsl:with-param>
        <xsl:with-param name="value" select="@email"/>
    </xsl:call-template>

</xsl:template>

<xsl:template match="ChangePeriodicTask" mode="MiogaFormButton">
    <xsl:call-template name="ok-form-button">
        <xsl:with-param name="name">change</xsl:with-param>
    </xsl:call-template>&#160;<xsl:call-template name="delete-form-button">
    <xsl:with-param name="name">delete</xsl:with-param>
        </xsl:call-template>&#160;<xsl:call-template name="cancel-button">
        <xsl:with-param name="href" select="@referer"/>
    </xsl:call-template>
</xsl:template>

</xsl:stylesheet>
