<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:output method="html" indent="yes"/>

<xsl:include href="base.xsl" />
<xsl:include href="simple_large_list.xsl" />
<xsl:include href="scriptaculous.xsl"/>
<xsl:include href="inline_message.xsl"/>

<!-- ===============
	root document
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="scriptaculous-js"/>
	<xsl:call-template name="theme-css"/>
</head>
	<xsl:apply-templates />
</html>
</xsl:template>
  
<!-- =================================
     DisplayMain
     ================================= -->

<xsl:template name="ConvertDate">
  <xsl:param name="date"/>
  
  <xsl:variable name="year"  select="substring-before($date, '-')"/>
  <xsl:variable name="month" select="substring-before(substring-after($date, '-'), '-')"/>
  <xsl:variable name="day"   select="substring-before(substring-after(substring-after($date, '-'), '-'), ' ')"/>
  
    <xsl:if test="$year!='' and $month !='' and $day != ''">
        <xsl:value-of select="$day"/>/<xsl:value-of select="$month"/>/<xsl:value-of select="$year"/>
    </xsl:if>        
</xsl:template>

<xsl:template match="*" mode="sll-translate-values" name="TranslateValues">
    <xsl:param name="name"/>
    <xsl:param name="value"/>
    <xsl:param name="row"/>

    <xsl:choose>
        <xsl:when test="$name='load_unit'">
          <xsl:call-template name="timeUnit">
            <xsl:with-param name="value" select="$value"/>
          </xsl:call-template>
        </xsl:when>

        <xsl:when test="$name='start_constraint' or $name='stop_constraint'">
          <xsl:call-template name="timeConstraint">
            <xsl:with-param name="value" select="$value"/>
          </xsl:call-template>
        </xsl:when>

        <xsl:when test="$name='start'">
            <xsl:call-template name="ConvertDate">
                <xsl:with-param name="date" select="$value"/>
            </xsl:call-template>
            <xsl:if test="$row/start_constraint != 0">
                &#160;
                (<xsl:call-template name="TranslateValues">
                    <xsl:with-param name="name">start_constraint</xsl:with-param>
                    <xsl:with-param name="value" select="$row/start_constraint"/>
                </xsl:call-template>)
            </xsl:if>
        </xsl:when>

        <xsl:when test="$name='stop'">
            <xsl:call-template name="ConvertDate">
                <xsl:with-param name="date" select="$value"/>
            </xsl:call-template>
            <xsl:if test="$row/stop_constraint != 0">
                &#160;
                (<xsl:call-template name="TranslateValues">
                    <xsl:with-param name="name">stop_constraint</xsl:with-param>
                    <xsl:with-param name="value" select="$row/stop_constraint"/>
                </xsl:call-template>)
            </xsl:if>
        </xsl:when>


        <xsl:when test="$name='load_value' and $value != ''">
            <xsl:value-of select="$value"/>&#160;<xsl:call-template name="TranslateValues">
                <xsl:with-param name="name">load_unit</xsl:with-param>                
                <xsl:with-param name="value" select="$row/load_unit"/>
            </xsl:call-template>
        </xsl:when>

        <xsl:when test="$name='progress'">
            <xsl:value-of select="$value"/>&#160;%
        </xsl:when>

        <xsl:when test="$name='group' and $value='__none__'">
          <xsl:call-template name="taskTranslation">
            <xsl:with-param name="type">none</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
            
        <xsl:when test="$name='name'">
            <xsl:attribute name="style">background-color: <xsl:value-of select="$row/bgcolor"/>; color: <xsl:value-of select="$row/fgcolor"/></xsl:attribute>
            <a href="DisplayTask?rowid={$row/rowid}" style="color: {$row/fgcolor}"><xsl:value-of select="$value"/></a>
        </xsl:when>

        <xsl:otherwise>
            <xsl:value-of select="$value"/>
        </xsl:otherwise>

    </xsl:choose>
</xsl:template>

<xsl:template match="DisplayMain" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">addTask</xsl:with-param>
    <xsl:with-param name="href">EditTask</xsl:with-param>
  </xsl:call-template>
</xsl:template>


<xsl:template match="DisplayMain">
<body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">gestiontemps.html#task</xsl:with-param>
    </xsl:call-template>

	<!-- Status message -->
	<xsl:if test="@message != ''">
		<xsl:call-template name="InlineMessage">
			<xsl:with-param name="type" select="@type"/>
			<xsl:with-param name="message" select="@message"/>
		</xsl:call-template>
	</xsl:if>

    <xsl:call-template name="MiogaTitle"/>

    <xsl:if test="UserTasks or DelegatedTasks or GroupTasks">
      <xsl:call-template name="MiogaAction">
        <xsl:with-param name="wrap">1</xsl:with-param>
      </xsl:call-template>
    </xsl:if>


    <div xsl:use-attribute-sets="app_body_attr">

        <xsl:for-each select="UserTasks">
            <xsl:call-template name="MiogaTitle">
                <xsl:with-param name="title">
                    <xsl:call-template name="taskTranslation">
                        <xsl:with-param name="type">todo-task</xsl:with-param>
                    </xsl:call-template>
                </xsl:with-param>
            </xsl:call-template>
            
            <xsl:apply-templates name="SimpleLargeList">
                <xsl:with-param name="displayaction">0</xsl:with-param>
                <xsl:with-param name="label">todo-task</xsl:with-param>
            </xsl:apply-templates>
            
        </xsl:for-each>

        <xsl:for-each select="DelegatedTasks">
            <xsl:call-template name="MiogaTitle">
                <xsl:with-param name="title">
                    <xsl:call-template name="taskTranslation">
                        <xsl:with-param name="type">delegated-task</xsl:with-param>
                    </xsl:call-template>
                </xsl:with-param>
            </xsl:call-template>
            
            <xsl:apply-templates name="SimpleLargeList">
                <xsl:with-param name="displayaction">0</xsl:with-param>
                <xsl:with-param name="label">delegated-task</xsl:with-param>
            </xsl:apply-templates>

        </xsl:for-each>

        <xsl:for-each select="GroupTasks">
             <xsl:call-template name="MiogaTitle">
                <xsl:with-param name="title">
                    <xsl:call-template name="taskTranslation">
                        <xsl:with-param name="type">group-task</xsl:with-param>
                    </xsl:call-template>
                </xsl:with-param>
            </xsl:call-template>
            
            <xsl:apply-templates name="SimpleLargeList">
                <xsl:with-param name="displayaction">0</xsl:with-param>
                <xsl:with-param name="label">group-task</xsl:with-param>
            </xsl:apply-templates>
            
        </xsl:for-each>

        <xsl:if test="SimpleLargeList">
            <xsl:apply-templates name="SimpleLargeList">
                <xsl:with-param name="displayaction">0</xsl:with-param>
                <xsl:with-param name="label">list</xsl:with-param>
            </xsl:apply-templates>
        </xsl:if>

    </div>
</body>
</xsl:template>


<!-- =================================
     EditTask
     ================================= -->

<xsl:template match="EditTask" mode="MiogaFormBody">
	<xsl:call-template name="CSRF-input"/>
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">name</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputExternalSelectSingle">
        <xsl:with-param name="label">group_id</xsl:with-param>
        <xsl:with-param name="disp_value">
            <xsl:choose>
                <xsl:when test="fields/group_ident = '__none__'">
                    <xsl:call-template name="TranslateValues">
                        <xsl:with-param name="name">group</xsl:with-param>
                        <xsl:with-param name="value">__none__</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="fields/group_ident"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:with-param>
        <xsl:with-param name="action">select_group_act</xsl:with-param>                          
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputSelect">
        <xsl:with-param name="label">category_id</xsl:with-param>
        <xsl:with-param name="choices" select="task_category/@rowid"/>
    </xsl:call-template>
        

    <xsl:call-template name="MiogaFormInputDuration">
        <xsl:with-param name="label">load</xsl:with-param>
        <xsl:with-param name="use-seconds">0</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">progress</xsl:with-param>
        <xsl:with-param name="size">4</xsl:with-param>
    </xsl:call-template>  
    
    <xsl:if test="IsUser=0">
        <xsl:call-template name="MiogaFormInputExternalSelectSingle">
            <xsl:with-param name="label">delegate_id</xsl:with-param>
            <xsl:with-param name="disp_value" select="fields/delegate_name"/>
            <xsl:with-param name="action">select_delegate_act</xsl:with-param>
        </xsl:call-template>
    </xsl:if>
  
    <xsl:call-template name="MiogaFormInputFlexibleDate">
        <xsl:with-param name="label">start</xsl:with-param>
        <xsl:with-param name="can-be-empty">1</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputFlexibleDate">
        <xsl:with-param name="label">stop</xsl:with-param>
        <xsl:with-param name="can-be-empty">1</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormVertRule"/>

    <xsl:call-template name="MiogaFormInputTextArea">
        <xsl:with-param name="label">description</xsl:with-param>
    </xsl:call-template>

</xsl:template>

<xsl:template match="EditTask" mode="MiogaFormButton">
    <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">create_task_act</xsl:with-param>
        <xsl:with-param name="referer" select="referer"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="*" mode="arg-checker-field-name">
    <xsl:param name="name"/>

    <xsl:call-template name="FieldsName">
        <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="EditTask">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">gestiontemps.html#task</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaTitle"/>
    
    <div xsl:use-attribute-sets="app_form_body_attr">
      <xsl:call-template name="arg_errors"/>

      <xsl:call-template name="MiogaForm">
          <xsl:with-param name="label">
              <xsl:choose>
                  <xsl:when test="fields/rowid != ''">modify-task</xsl:when>
                  <xsl:otherwise>create-task</xsl:otherwise>
              </xsl:choose>
          </xsl:with-param>

          <xsl:with-param name="action">EditTask</xsl:with-param>
          <xsl:with-param name="method">POST</xsl:with-param>

      </xsl:call-template>
    </div>
  </body>
</xsl:template>

<!-- =================================
     DisplayTask
     ================================= -->

<xsl:template match="DisplayTask" mode="MiogaReportBody">
    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">name</xsl:with-param>        
    </xsl:call-template>

    <xsl:if test="fields/group_ident != '__none__'">
        <xsl:call-template name="MiogaReportText">
            <xsl:with-param name="label">group_ident</xsl:with-param>
        </xsl:call-template>
    </xsl:if>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">category</xsl:with-param>
        <xsl:with-param name="value">
            <xsl:variable name="category" select="task_category[@rowid=../fields/category_id]"/>

            <xsl:choose>
                <xsl:when test="$category='default'">
                    <xsl:call-template name="taskTranslation">
                        <xsl:with-param name="type">undefined</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="$category"/></xsl:otherwise>
            </xsl:choose>

        </xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportDuration">
        <xsl:with-param name="label">load</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">progress</xsl:with-param>
        <xsl:with-param name="value"><xsl:value-of select="fields/progress"/>&#160;%</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportText">
        <xsl:with-param name="label">delegate_name</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportFlexibleDate">
        <xsl:with-param name="label">start</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaReportFlexibleDate">
        <xsl:with-param name="label">stop</xsl:with-param>
    </xsl:call-template>


    <xsl:call-template name="MiogaFormVertRule"/>

    <xsl:call-template name="MiogaReportTextArea">
        <xsl:with-param name="label">description</xsl:with-param>
    </xsl:call-template>

</xsl:template>

<xsl:template match="DisplayTask" mode="MiogaReportButton">
    <xsl:if test="count(CanWrite) > 0"><xsl:call-template name="edit-button">
        <xsl:with-param name="href">EditTask?rowid=<xsl:value-of select="fields/rowid"/>&amp;referer=<xsl:value-of select="escreferer"/></xsl:with-param>
        </xsl:call-template>&#160;</xsl:if><xsl:call-template name="back-button">
        <xsl:with-param name="href" select="referer"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="DisplayTask">
  <body xsl:use-attribute-sets="body_attr">
      <xsl:call-template name="DisplayAppTitle">
          <xsl:with-param name="help">gestiontemps.html#task</xsl:with-param>
      </xsl:call-template>

      <xsl:call-template name="MiogaTitle"/>
      
      <div xsl:use-attribute-sets="app_form_body_attr">
          <xsl:call-template name="arg_errors"/>
          
          <xsl:call-template name="MiogaReport">
              <xsl:with-param name="label">display-task</xsl:with-param>
          </xsl:call-template>
      </div>
  </body>
</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<xsl:template name="timeUnit">
  <xsl:param name="value"/>

  <xsl:choose>
    <xsl:when test="$value=1"><xsl:value-of select="mioga:gettext('minutes')"/></xsl:when>
    <xsl:when test="$value=2"><xsl:value-of select="mioga:gettext('hours')"/></xsl:when>
    <xsl:when test="$value=3"><xsl:value-of select="mioga:gettext('days')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<!-- SuccessMessage -->
<xsl:template match="SuccessMessage">
  <xsl:variable name="message">
    <xsl:choose>
      <xsl:when test="action='create'"><xsl:value-of select="mioga:gettext('Task created successfully.')"/></xsl:when>
      <xsl:when test="action='modify'"><xsl:value-of select="mioga:gettext('Task modified successfully.')"/></xsl:when>
      <xsl:when test="action='remove'"><xsl:value-of select="mioga:gettext('Task deleted successfully.')"/></xsl:when>
    </xsl:choose>
  </xsl:variable>

  <xsl:call-template name="SuccessMessage">
    <xsl:with-param name="message" select="$message"/>
    <xsl:with-param name="referer" select="referer"/>
  </xsl:call-template>
  
</xsl:template>

<xsl:template name="taskTranslation">
  <xsl:param name="type"/>

  <xsl:choose>
    <xsl:when test="$type='none'"><xsl:value-of select="mioga:gettext('None')"/></xsl:when>
    <xsl:when test="$type='todo-task'"><xsl:value-of select="mioga:gettext('Todo tasks')"/></xsl:when>
    <xsl:when test="$type='delegated-task'"><xsl:value-of select="mioga:gettext('Delegated task')"/></xsl:when>
    <xsl:when test="$type='group-task'"><xsl:value-of select="mioga:gettext('Group tasks')"/></xsl:when>
    <xsl:when test="$type='task-search'"><xsl:value-of select="mioga:gettext('Search for task')"/></xsl:when>
    <xsl:when test="$type='undefined'"><xsl:value-of select="mioga:gettext('Undefined')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="MiogaFormTranslation">
    <xsl:param name="label"/>
    <xsl:choose>
        <xsl:when test="$label='todo-task'"><xsl:value-of select="mioga:gettext('Search for todo task')"/></xsl:when>
        <xsl:when test="$label='delegated-task'"><xsl:value-of select="mioga:gettext('Search for delegated task')"/></xsl:when>
        <xsl:when test="$label='group-task'"><xsl:value-of select="mioga:gettext('Search for delegated task')"/></xsl:when>
        <xsl:when test="$label='modify-task'"><xsl:value-of select="mioga:gettext('Modify task')"/></xsl:when>
        <xsl:when test="$label='create-task'"><xsl:value-of select="mioga:gettext('Create task')"/></xsl:when>
    </xsl:choose>
</xsl:template>

<xsl:template name="MiogaReportTranslation">
    <xsl:param name="label"/>
    <xsl:choose>
        <xsl:when test="$label='display-task'"><xsl:value-of select="mioga:gettext('Display task')"/></xsl:when>
    </xsl:choose>
</xsl:template>

<xsl:template name="MiogaReportInputTranslation">
    <xsl:param name="label"/>
    <xsl:call-template name="FieldsName">
        <xsl:with-param name="name" select="$label"/>
    </xsl:call-template>
</xsl:template>

<xsl:template name="MiogaFormInputTranslation">
    <xsl:param name="label"/>
    <xsl:call-template name="FieldsName">
        <xsl:with-param name="name" select="$label"/>
    </xsl:call-template>
</xsl:template>

<xsl:template name="MiogaFormInputSelectTranslation">
    <xsl:param name="label"/>
    <xsl:param name="value"/>

    <xsl:value-of select="//task_category[@rowid=$value]"/>
</xsl:template>

<xsl:template name="MiogaTitleTranslation">
  <xsl:choose>
    <xsl:when test="name(.)='DisplayMain'"></xsl:when>
    <xsl:when test="name(.)='EditTask'"></xsl:when>
    <xsl:when test="name(.)='DisplayTask'"></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="*" mode="sll-field-name" name="FieldsName">
  <xsl:param name="name"/>
  <xsl:choose>
    <xsl:when test="$name='name'"><xsl:value-of select="mioga:gettext('Name')"/></xsl:when>
    <xsl:when test="$name='group' or $name='group_id' or $name = 'group_ident'"><xsl:value-of select="mioga:gettext('Group')"/></xsl:when>
    <xsl:when test="$name='category' or $name='category_id'"><xsl:value-of select="mioga:gettext('Category')"/></xsl:when>
    <xsl:when test="$name='creator'"><xsl:value-of select="mioga:gettext('Created by')"/></xsl:when>
    <xsl:when test="$name='delegate'  or $name = 'delegate_name' or $name = 'delegate_id'"><xsl:value-of select="mioga:gettext('Delegated to')"/></xsl:when>
    <xsl:when test="$name='start'"><xsl:value-of select="mioga:gettext('Start')"/></xsl:when>
    <xsl:when test="$name='stop'"><xsl:value-of select="mioga:gettext('End')"/></xsl:when>
    <xsl:when test="$name='progress'"><xsl:value-of select="mioga:gettext('Progress')"/></xsl:when>
    <xsl:when test="$name='description'"><xsl:value-of select="mioga:gettext('Description')"/></xsl:when>
    <xsl:when test="$name='load' or $name='load_value' or $name='load_unit'"><xsl:value-of select="mioga:gettext('Workload')"/></xsl:when>
    <xsl:otherwise><xsl:value-of select="$name"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="MiogaActionTranslation">
  <xsl:param name="label"/>
  <xsl:choose>
    <xsl:when test="$label='addTask'"><xsl:value-of select="mioga:gettext('Add task')"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="*" mode="arg-checker-unknown-args">
  <xsl:param name="name"/>
  <xsl:param name="arg"/>
  <xsl:choose>
    <xsl:when test="$name='__create__'"><xsl:value-of select="mioga:gettext('Error while creating task: %s', string($arg))"/></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="timeConstraint">
  <xsl:param name="value"/>
  <xsl:choose>
    <xsl:when test="$value=0"></xsl:when>
    <xsl:when test="$value=1"><xsl:value-of select="mioga:gettext('Fixed')"/></xsl:when>
    <xsl:when test="$value=2"><xsl:value-of select="mioga:gettext('Sooner')"/></xsl:when>
    <xsl:when test="$value=3"><xsl:value-of select="mioga:gettext('Later')"/></xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
