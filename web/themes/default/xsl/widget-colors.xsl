<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
<!--  <xsl:output method="html"/> -->

  <!-- =================================
       Colors definition.              
       ================================= -->

  <!-- 
       base colors
       -->

  <xsl:variable name="mioga-text-color">mioga-text-color</xsl:variable>
  <xsl:variable name="mioga-bg-color">mioga-bg-color</xsl:variable>
  <xsl:variable name="mioga-border-color">mioga-border-color</xsl:variable>
  <xsl:variable name="mioga-title-bg-color">mioga-title-bg-color</xsl:variable>


  <!-- 
       other colors
       -->
  
  <!-- misc text -->
  <xsl:variable name="mioga-title-color" select="$mioga-text-color"/>
  <xsl:variable name="mioga-subtitle-color">mioga-subtitle-color</xsl:variable>
  <xsl:variable name="mioga-link-color">mioga-link-color</xsl:variable>
  <xsl:variable name="mioga-active-link-color" select="$mioga-link-color"/>
  <xsl:variable name="mioga-visited-link-color" select="$mioga-link-color"/>
  <xsl:variable name="mioga-error-color">mioga-error-color</xsl:variable>

  <!-- box -->
  <xsl:variable name="mioga-box-bg-color" select="$mioga-bg-color"/>
  <xsl:variable name="mioga-box-title-bg-color" select="$mioga-title-bg-color"/>
  <xsl:variable name="mioga-box-title-color" select="$mioga-title-color"/>

  <!-- tab menu -->
  <xsl:variable name="mioga-tab-border-color" select="$mioga-border-color"/>
  <xsl:variable name="mioga-active-tab-bg-color" select="$mioga-title-bg-color"/>
  <xsl:variable name="mioga-inactive-tab-bg-color">mioga-inactive-tab-bg-color</xsl:variable>
  <xsl:variable name="mioga-active-tab-text-color">mioga-active-tab-text-color</xsl:variable>
  <xsl:variable name="mioga-inactive-tab-text-color">mioga-inactive-tab-text-color</xsl:variable>

  <!-- lists -->
  <xsl:variable name="mioga-list-title-bg-color" select="$mioga-title-bg-color"/>
  <xsl:variable name="mioga-list-title-text-color" select="$mioga-text-color"/>
  <xsl:variable name="mioga-list-even-row-bg-color">mioga-list-even-row-bg-color</xsl:variable>
  <xsl:variable name="mioga-list-odd-row-bg-color" select="$mioga-bg-color"/>
  <xsl:variable name="mioga-list-selected-row-bg-color">mioga-list-selected-row-bg-color</xsl:variable>
  <xsl:variable name="mioga-list-disabled-row-bg-color">mioga-list-disabled-row-bg-color</xsl:variable>

  <!-- forms -->
  <xsl:variable name="mioga-rules-color" select="$mioga-title-bg-color"/>

</xsl:stylesheet>
