<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">
<xsl:import href="base.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"/>
<xsl:include href="jquery.xsl"/>

<!-- ==============================
	Root document
	=============================== -->

<xsl:template name="search-css">
    <link rel="stylesheet" type="text/css" href="{$theme_uri}/css/search.css" media="screen" />
</xsl:template>

<xsl:template name="search-js">
	<!-- <script src="{$theme_uri}/javascript/search.js" type="text/javascript"> -->
	<script type="text/javascript">
		function AddToQuery(value) {
			var val = $('#query_string').attr('value');
			val += ' ' + value;
			$('#query_string').attr('value', val);
		}
		function SetParam(n, value) {
			console.debug('SetParam ' + name + ' ' + value);
			$('input[name="'+n+'"]').attr('value', value)
		}
		function SubmitMainForm() {
			console.debug('SubmitMainForm');
			$('#search_form').submit();
		}
    </script>
</xsl:template>

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="mioga-css"/>
    <xsl:call-template name="favicon"/>
    <xsl:call-template name="title"/>
    <xsl:call-template name="mioga-js"/>
	<xsl:call-template name="jquery-js"/>

    <xsl:call-template name="search-css"/>
    <xsl:call-template name="search-js"/>
    
	<xsl:apply-templates mode="head"/>
	<xsl:call-template name="theme-css"/>
</head>
<body class="search">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">search.html</xsl:with-param>
    </xsl:call-template>
    <xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- =======================================
	DisplayMain
	======================================= -->

<xsl:template match="DisplayMain" mode="head">
</xsl:template>

<xsl:template match="DisplayMain">
<xsl:variable name="query_string"><xsl:value-of select="SearchInfos/args/query_string" /></xsl:variable>
<xsl:variable name="esc_query_string"><xsl:value-of select="esc_query_string" /></xsl:variable>
<xsl:variable name="offset"><xsl:value-of select="SearchInfos/offset" /></xsl:variable>
<xsl:variable name="current_page"><xsl:value-of select="SearchInfos/current_page" /></xsl:variable>

<div id="form">
<form id="search_form" action="DisplayMain" name="search_form" method="POST">
	<table class="tb_form">
		<tr>
			<td><img src="{$image_uri}/logo_mioga2.gif" /></td>
			<td>
				<input type="hidden" name="offset" value="{$offset}" />
				<input type="hidden" name="force_terms" value="0" />
				<input id="query_string" type="text" name="query_string" value="{$query_string}" />&#160;<input type="submit" name="act_search" onclick="SetParam('offset', '0');">
					<xsl:attribute name="value">
						<xsl:value-of select="mioga:gettext('Search')" />
					</xsl:attribute>
				</input>
			</td>
			<td>
				<p class="action"><a href="#" onclick="$('#terms').fadeIn(); return false;"><xsl:value-of select="mioga:gettext('Suggested terms')" /></a></p>
				<p class="action"><a href="#" onclick="$('#adv_search').fadeIn(); return false;"><xsl:value-of select="mioga:gettext('Advanced search')" /></a></p>
			</td>
		</tr>
	</table>

	<div id="terms" style="display:none">
		<p>
		<span class="accent"><xsl:value-of select="mioga:gettext('Suggested terms')" />&#160;:&#160;</span>
		<xsl:for-each select="SearchInfos/term">
			<span><a href="#" onclick="AddToQuery('{.}'); SubmitMainForm(); return false;"><xsl:value-of select="." /></a>,&#160;</span>
		</xsl:for-each>
		</p>
	</div>

	<div id="adv_search">
		<xsl:attribute name="style">
			<xsl:choose>
				<xsl:when test="SearchInfos/args/advanced_search">display:block;</xsl:when>
				<xsl:otherwise>display:none;</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<p>
			<input type="checkbox" name="advanced_search" >
				<xsl:if test="SearchInfos/args/advanced_search">
					<xsl:attribute name="checked">on</xsl:attribute>
				</xsl:if>
			</input>

		<span class="accent"><xsl:value-of select="mioga:gettext('Advanced parameters')" /></span><xsl:text> </xsl:text>
		<xsl:value-of select="mioga:gettext('Limit to group')" /> : <select id="group_id" name="group_id">
			<xsl:for-each select="group">
				<option value="{rowid}">
					<xsl:if test="current = 'yes'">
						<xsl:attribute name="selected">yes</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="name" />
				</option>
			</xsl:for-each>
		</select>
		<xsl:text> </xsl:text><xsl:value-of select="mioga:gettext('Limit to application')" /> : <select id="app_code" name="app_code">
			<xsl:for-each select="application">
				<xsl:sort select="app_code" data-type="number"/>
				<option value="{app_code}">
					<xsl:if test="current = 'yes'">
						<xsl:attribute name="selected">yes</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="name" />
				</option>
			</xsl:for-each>
		</select>
		</p>
		<p>
			<xsl:value-of select="mioga:gettext('Search in')" />&#160;
			<input type="checkbox" name="search_text">
				<xsl:if test="SearchInfos/args/search_text">
					<xsl:attribute name="checked">on</xsl:attribute>
				</xsl:if>
			</input> <xsl:value-of select="mioga:gettext('Text')" />
			<xsl:text disable-output-escaping="yes">&amp;nbsp&#59;</xsl:text>
			<input type="checkbox" name="search_tags">
				<xsl:if test="SearchInfos/args/search_tags">
					<xsl:attribute name="checked">on</xsl:attribute>
				</xsl:if>
			</input> <xsl:value-of select="mioga:gettext('Tags')" />
			<xsl:text disable-output-escaping="yes">&amp;nbsp&#59;</xsl:text>
			<input type="checkbox" name="search_description">
				<xsl:if test="SearchInfos/args/search_description">
					<xsl:attribute name="checked">on</xsl:attribute>
				</xsl:if>
			</input> <xsl:value-of select="mioga:gettext('Description')" />
		</p>
	</div>
</form>
</div>
<xsl:if test='SearchInfos/corrected'>
		<p id="corrected"><xsl:value-of select="mioga:gettext('Search done with: ')" />
			<xsl:for-each select="SearchInfos/query_terms">
				<span>
					<xsl:if test="changed='1'">
						<xsl:attribute name="class">changed</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="term" />
				</span>&#160;
			</xsl:for-each>
			(<xsl:value-of select="mioga:gettext('Force search with: ')" /><a href="#" onclick="SetParam('force_terms', 1); SubmitMainForm(); return false;"><xsl:value-of select="$query_string" /></a>);
		</p>
</xsl:if>
<xsl:choose>
	<xsl:when test='SearchInfos/total_count &gt; 0'>
		<div id="count">
			<p class="info"><xsl:value-of select="mioga:gettext('Results')" />&#160;<xsl:value-of select="SearchInfos/offset"/>&#160;<xsl:value-of select="mioga:gettext ('range_to')"/>&#160;<xsl:value-of select="SearchInfos/last"/><xsl:text> </xsl:text><xsl:value-of select="mioga:gettext('for an estimated count of')" /><xsl:text> </xsl:text><xsl:value-of select="SearchInfos/total_count"/>
			</p>
		</div>
		<div id="result">
			<xsl:apply-templates select="Result" />
		</div>
		<div id="pages" class="clear-both">
			<p class="info"><xsl:value-of select="mioga:gettext('Result pages')" /> :
			<xsl:if test="SearchInfos/previous_page">
				<xsl:variable name="previous_offset"><xsl:value-of select="SearchInfos/previous_page/offset" /></xsl:variable>
				<a href="#" onclick="SetParam('offset', '{$previous_offset}'); SubmitMainForm(); return false;"><xsl:value-of select="mioga:gettext('Previous')" /></a>&#160;
			</xsl:if>
			<xsl:for-each select="SearchInfos/page">
				<xsl:sort select="number" data-type="number"/>
				<xsl:variable name="new_offset"><xsl:value-of select="offset" /></xsl:variable>
				<xsl:choose>
					<xsl:when test="number=$current_page">
						&#160;<span class="current_page"><xsl:value-of select="number" /></span>
					</xsl:when>
					<xsl:otherwise>
						&#160;<a href="#" onclick="SetParam('offset', '{$new_offset}'); SubmitMainForm(); return false;"><xsl:value-of select="number" /></a>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			<xsl:if test="SearchInfos/next_page">
				<xsl:variable name="next_offset"><xsl:value-of select="SearchInfos/next_page/offset" /></xsl:variable>
				&#160;<a href="#" onclick="SetParam('offset', '{$next_offset}'); SubmitMainForm(); return false;"><xsl:value-of select="mioga:gettext('Next')" /></a>
			</xsl:if>
			</p>
		</div>
	</xsl:when>
	<xsl:otherwise>
		<div id="result">
			<p><xsl:value-of select="mioga:gettext('No result')" /></p>
		</div>
	</xsl:otherwise>
</xsl:choose>
</xsl:template>

<!-- ========================
	app_type => icon
	========================= -->

<xsl:template match="Result">
<xsl:variable name="mime_icon">
	<xsl:choose>
		<xsl:when test="mime = 'text/plain'" >mimetypes/text-plain.png</xsl:when>
		<xsl:when test="mime = 'text/html'" >mimetypes/text-html.png</xsl:when>
		<xsl:when test="mime = 'application/vnd.oasis.opendocument.text'" >mimetypes/application-vnd.oasis.opendocument.text.png</xsl:when>
		<xsl:when test="mime = 'application/msword'" >mimetypes/application-msword.png</xsl:when>
		<xsl:when test="mime = 'application/vnd.oasis.opendocument.spreadsheet'" >mimetypes/application-vnd.oasis.opendocument.spreadsheet.png</xsl:when>
		<xsl:when test="mime = 'application/vnd.ms-excel'" >mimetypes/application-vnd.ms-excel.png</xsl:when>
		<xsl:when test="mime = 'application/pdf'" >mimetypes/application-pdf.png</xsl:when>
		<xsl:when test="mime = 'application/postscript'" >mimetypes/application-postscript.png</xsl:when>
		<xsl:when test="mime = 'application/mioga2-articles'" >apps/articles.png</xsl:when>
		<xsl:when test="mime = 'application/mioga2-forum'" >apps/forum.png</xsl:when>
		<xsl:when test="mime = 'application/mioga2-news'" >apps/news.png</xsl:when>
		<xsl:when test="mime = 'application/mioga2-diderot'" >apps/diderot.png</xsl:when>
		<xsl:when test="mime = 'image/jpeg'" >mimetypes/image-x-generic.png</xsl:when>
		<xsl:when test="mime = 'image/png'" >mimetypes/image-x-generic.png</xsl:when>
		<xsl:when test="mime = 'image/tiff'" >mimetypes/image-x-generic.png</xsl:when>
		<xsl:when test="mime = 'image/gif'" >mimetypes/image-x-generic.png</xsl:when>
		<xsl:when test="mime = 'image/bmp'" >mimetypes/image-x-generic.png</xsl:when>
		<xsl:otherwise>mimetypes/unknown.png</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<div class="search_result">
	<div class="clear-both separator"></div>
	<div class="icon_mime"><img src="{$image_uri}/48x48/{$mime_icon}" /></div>
	<div class="result">
		<p><b><a href="{url}"><xsl:value-of select="description" /></a></b>&#160;<xsl:value-of select="mioga:gettext('Group')" /> : <b><xsl:value-of select="group" /></b></p>
		<xsl:if test="sup_data/gallery/thumbnail_uri">
				<div class="thumbnail"><img src="{sup_data/gallery/thumbnail_uri}" /></div>
		</xsl:if>
		<p class="highlight"><xsl:value-of select="highlight" disable-output-escaping="yes" /></p>
		<p class="path"><a href="{url}"><xsl:value-of select="url" /></a>&#160;<span class="date"><xsl:value-of select="date" /></span> </p>
	</div>
</div>
</xsl:template>

</xsl:stylesheet>
