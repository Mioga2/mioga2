<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<xsl:include href="scriptaculous.xsl"/>
<xsl:include href="inline_message.xsl"/>

<!-- ===============
	root document
	================ -->

<xsl:template name="narkissos-css">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/narkissos.css" media="screen" />
</xsl:template>

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
	<xsl:call-template name="scriptaculous-js"/>

	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>

	<xsl:call-template name="narkissos-css"/>

	<xsl:apply-templates mode="head"/>
	<xsl:call-template name="theme-css"/>
</head>
<body id="Narkissos_body" class="Narkissos mioga">
	<xsl:apply-templates />
</body>
</html>
</xsl:template>


<xsl:template match="DisplayMain" mode="arg-checker-field-name">
    <xsl:param name="name"/>

    <xsl:call-template name="FieldsName">
        <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>
</xsl:template>

<!-- ===============
	DisplayMain
	================ -->

<xsl:template match="DisplayMain" mode="head">
</xsl:template>

<xsl:template match="DisplayMain">
	<xsl:call-template name="DisplayAppTitle">
		<xsl:with-param name="help">applications.html#narkissos</xsl:with-param>
	</xsl:call-template>

	<!-- Status message -->
	<xsl:if test="@message != ''">
		<xsl:call-template name="InlineMessage">
			<xsl:with-param name="type" select="@type"/>
			<xsl:with-param name="message" select="@message"/>
		</xsl:call-template>
	</xsl:if>

	<div id="main_dialog" class="dialog">
		<div class="sbox border_color">
			<h2 class="title_bg_color">
				<xsl:choose>
					<xsl:when test="@CanModify=1">
						<xsl:value-of select="mioga:gettext('Modify my personal data')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="mioga:gettext('View my personal data')"/>
					</xsl:otherwise>
				</xsl:choose>
			</h2>
			<div class="content">
		<xsl:choose>
			<xsl:when test="@Autonomous=1">
				<form id="narkissos-form" class="form" method="post">
					<xsl:call-template name="CSRF-input"/>
					<xsl:if test="@PasswordMatchFail=1">
						<p class="error">
							<xsl:value-of select="mioga:gettext('Passwords are different.')"/>
						</p>
					</xsl:if>
					<xsl:if test="@WrongPassword=1">
						<p class="error">
							<xsl:value-of select="mioga:gettext('Wrong password.')"/>
						</p>
					</xsl:if>

					<xsl:call-template name="arg_errors"/>

					<fieldset>
						<legend>
							<xsl:value-of select="mioga:gettext('My personal data')" />
						</legend>
						<div class="form-item">
							<label for="firstname"><xsl:value-of select="mioga:gettext('First Name')"/></label>
							<input type="text" class="miogaInput" id="firstname" name="firstname" value="{firstname}">
								<xsl:if test="@CanModify!=1">
									<xsl:attribute name="disabled" value="disabled"/>
								</xsl:if>
							</input>
						</div>
						<div class="form-item">
							<label for="lastname"><xsl:value-of select="mioga:gettext('Last Name')"/></label>
							<input type="text" class="miogaInput" id="lastname" name="lastname" value="{lastname}">
								<xsl:if test="@CanModify!=1">
									<xsl:attribute name="disabled" value="disabled"/>
								</xsl:if>
							</input>
						</div>
						<div class="form-item">
							<label for="email"><xsl:value-of select="mioga:gettext('E-mail')"/></label>
							<input type="text" class="miogaInput" id="email" name="email" value="{email}">
								<xsl:if test="@CanModify!=1">
									<xsl:attribute name="disabled" value="disabled"/>
								</xsl:if>
							</input>
						</div>
						<xsl:if test="instances != ''">
							<div class="form-item">
								<label><xsl:value-of select="mioga:gettext('Instances')"/></label>
								<table>
									<tr><td>
										<xsl:for-each select="instances">
											<li>
												<xsl:value-of select="."/>
											</li>
										</xsl:for-each>
									</td></tr>
								</table>
							</div>
						</xsl:if>
					</fieldset>
					<fieldset>
						<legend>
							<xsl:value-of select="mioga:gettext('Password update')" />
						</legend>
						<div class="form-item">
							<label for="password"><xsl:value-of select="mioga:gettext('Password')"/></label>
							<input type="password" class="miogaInput" id="password" name="password">
								<xsl:if test="@CanModify!=1">
									<xsl:attribute name="disabled" value="disabled"/>
								</xsl:if>
							</input>
						</div>
						<div class="form-item">
							<label for="new_password_1"><xsl:value-of select="mioga:gettext('New password')"/></label>
							<input type="password" class="miogaInput" id="new_password_1" name="new_password_1">
								<xsl:if test="@CanModify!=1">
									<xsl:attribute name="disabled" value="disabled"/>
								</xsl:if>
							</input>
						</div>
						<div class="form-item">
							<label for="new_password_2"><xsl:value-of select="mioga:gettext('Confirm password')"/></label>
							<input type="password" class="miogaInput" id="new_password_2" name="new_password_2">
								<xsl:if test="@CanModify!=1">
									<xsl:attribute name="disabled" value="disabled"/>
								</xsl:if>
							</input>
						</div>
					</fieldset>
					<xsl:if test="@UseSecretQuestion = 1">
						<fieldset>
							<legend>
								<xsl:value-of select="mioga:gettext('Password protection')" />
							</legend>
							<div class="form-item">
								<label for="secret_question"><xsl:value-of select="mioga:gettext('Secret question')"/></label>
								<input type="text" class="miogaInput" id="secret_question" name="secret_question" value="{question}" />
							</div>
							<div class="form-item">
								<label for="secret_question_answer"><xsl:value-of select="mioga:gettext('Answer')"/></label>
								<input type="text" class="miogaInput" id="secret_question_answer" name="secret_question_answer" value="{answer}" />
							</div>
						</fieldset>
					</xsl:if>
					<xsl:if test="@CanModify=1">
						<input type="hidden" name="user_act_modify" value="OK"/>
						<ul class="button_list">
							<li>
								<a class="button" onClick="document.getElementById('narkissos-form').submit();">OK</a>
							</li>
						</ul>
					</xsl:if>
				</form>
			</xsl:when>
			<xsl:otherwise>
				<p>
					<xsl:value-of select="mioga:gettext('Your user account is restricted, personal information modification is not allowed. Please contact your Mioga administrator.')"/>
				</p>
			</xsl:otherwise>
		</xsl:choose>
			</div>
		</div>
	</div>
		
</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<xsl:template name="FieldsName">
  <xsl:param name="name"/>
  
  <xsl:choose>
    <xsl:when test="$name = 'secret_question'"><xsl:value-of select="mioga:gettext('Secret question')"/></xsl:when>
    <xsl:when test="$name = 'secret_question_answer'"><xsl:value-of select="mioga:gettext('Answer')"/></xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="mioga2-common-name-translation">
        <xsl:with-param name="name" select="$name"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>

