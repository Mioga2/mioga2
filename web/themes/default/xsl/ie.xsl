<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga">

<xsl:template name="ie-hacks">
	<xsl:text disable-output-escaping="yes">&lt;!--[if IE]&gt;</xsl:text>
	<xsl:call-template name="ie-compat"/>
	<xsl:call-template name="ie-css"/>
	<xsl:call-template name="ie-contextual-fragment"/>
	<xsl:text disable-output-escaping="yes">&lt;![endif]--&gt;</xsl:text>
</xsl:template>

<xsl:template name="ie-compat">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
</xsl:template>

<xsl:template name="ie-css">
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/ie.css" media="screen" />
</xsl:template>

<!-- Fix for bug #1328 -->
<!-- This can be removed once Magellan doesn't use ExtJS anymore -->
<xsl:template name="ie-contextual-fragment">
	<script type="text/javascript">
		if ((typeof Range !== "undefined") &amp;&amp; (typeof Range.prototype.createContextualFragment == "undefined")) {
			Range.prototype.createContextualFragment = function(html) {
				var startNode = this.startContainer;
				var doc = startNode.nodeType == 9 ? startNode : startNode.ownerDocument;
				var container = doc.createElement("div");
				container.innerHTML = html;
				var frag = doc.createDocumentFragment(), n;
				while ( (n = container.firstChild) ) {
					frag.appendChild(n);
				}
				return frag;
			};
		}
	</script>
</xsl:template>

</xsl:stylesheet>
