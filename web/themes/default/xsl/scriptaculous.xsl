<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="base.xsl"/>

<!-- ==========
	scriptaculous-js
	to include scriptaculous in HTML HEAD element
	========== -->

<xsl:template name="scriptaculous-js">
	<script type="text/javascript" src="{$jslib_uri}/scriptaculous/prototype.js"></script>
	<script type="text/javascript" src="{$jslib_uri}/scriptaculous/scriptaculous.js"></script>
	<script type="text/javascript" src="{$jslib_uri}/scriptaculous/scriptaculous-mioga.js"></script>
</xsl:template>

<!-- ==========
	inplace-editor
	params :
		element : id of element to edit
		url : complete url to call (like : /Mioga2/Mioga/bin/group/Forum/ProcessAnimAction)
	optionnal params :
		size : size of text edit zone
	========== -->
<xsl:template name="inplace-editor">
	<xsl:param name="element">none</xsl:param>
	<xsl:param name="url">none</xsl:param>
	<xsl:param name="size">80</xsl:param>
    <script language="javascript" type="text/javascript">
		new Ajax.InPlaceEditor($("<xsl:value-of select="$element" />"), "<xsl:value-of select="$url" />", {size:<xsl:value-of select="$size" />, highlightDuration: 0.5, okImage: "<xsl:value-of select="$image_uri"/>/16x16/actions/dialog-ok-apply.png", cancelImage: "<xsl:value-of select="$image_uri"/>/16x16/actions/dialog-cancel.png"});
    </script>
</xsl:template>

<!-- ==========
	inplace-editor-multi
	params :
		element : id of element to edit
		url : complete url to call (like : /Mioga2/Mioga/bin/group/Forum/ProcessAnimAction)
	optionnal params :
		cols : size of text edit zone
		rows : size of text edit zone
	========== -->
<xsl:template name="inplace-editor-multi">
	<xsl:param name="element">none</xsl:param>
	<xsl:param name="url">none</xsl:param>
	<xsl:param name="cols">80</xsl:param>
	<xsl:param name="rows">2</xsl:param>
    <script language="javascript" type="text/javascript">
		new Ajax.InPlaceEditor($("<xsl:value-of select="$element" />"), "<xsl:value-of select="$url" />", {cols:<xsl:value-of select="$cols" />, rows:<xsl:value-of select="$rows" />, highlightDuration: 0.5, okImage: "<xsl:value-of select="$image_uri"/>/16x16/actions/dialog-ok-apply.png", cancelImage: "<xsl:value-of select="$image_uri"/>/16x16/actions/dialog-cancel.png"});
    </script>
</xsl:template>

<!-- ==========
	inplace-editor-extcontrol
	params :
		element : id of element to edit
		url : complete url to call (like : /Mioga2/Mioga/bin/group/Forum/ProcessAnimAction)
	optionnal params :
		size : size of text edit zone
	========== -->
<xsl:template name="inplace-editor-extcontrol">
	<xsl:param name="element">none</xsl:param>
	<xsl:param name="url">none</xsl:param>
	<xsl:param name="size">80</xsl:param>
	<xsl:param name="external_id">none</xsl:param>
    <script language="javascript" type="text/javascript">new Ajax.InPlaceEditor($("<xsl:value-of select="$element" />"), "<xsl:value-of select="$url" />", {size:<xsl:value-of select="$size" />, externalControl:$("<xsl:value-of select="$external_id" />"), highlightDuration: 0.5, okImage: "<xsl:value-of select="$image_uri"/>/16x16/actions/dialog-ok-apply.png", cancelImage: "<xsl:value-of select="$image_uri"/>/16x16/actions/dialog-cancel.png"});</script>
</xsl:template>
</xsl:stylesheet>
