<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<xsl:template name="MiogaReportInputAppDesc">
    <xsl:param name="node" select="."/>
    <xsl:param name="base_href" select="$private_bin_uri"/>
    
    <xsl:variable name="href"><xsl:value-of select="$base_href"/>/<xsl:value-of select="$node/@ident"/>/DisplayMain</xsl:variable>

    <tr>
        <td>
            <table cellpadding="3" cellspacing="3" border="0">
                <tr>
                    <td rowspan="2" valign="top">
                        <img border="0">
                            <xsl:attribute name="src">
                                <xsl:value-of select="$image_uri"/>/16x16/apps/<xsl:call-template name="MiogaAppIcon">
                                    <xsl:with-param name="app_ident" select="$node/@ident"/>
                                </xsl:call-template>
                            </xsl:attribute>
                        </img>
                    </td>
                    <td valign="top">
                        <a href="{$href}" class="{$mioga-title-color}" style="font-weight: bold;">
                            <xsl:call-template name="MiogaAppName">
                                <xsl:with-param name="app_ident" select="$node/@ident"/>                       
                            </xsl:call-template>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="{$mioga-subtitle-color}" style="align: justify; width: 300px;">
                        <xsl:call-template name="MiogaAppDesc">
                            <xsl:with-param name="app_ident" select="$node/@ident"/>
                        </xsl:call-template>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</xsl:template>


</xsl:stylesheet>

