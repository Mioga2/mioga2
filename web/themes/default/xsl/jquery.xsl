<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- ==========
	jquery-js
	to include jquery in HTML HEAD element
	========== -->

<xsl:template name="jquery-js">
	<script type="text/javascript" src="{$jslib_uri}/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="{$jslib_uri}/jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript">
		jQuery.curCSS = function(element, prop, val) {
			return jQuery(element).css(prop, val);
		};
	</script>
</xsl:template>

<xsl:template name="jquery-js-debug">
	<script type="text/javascript" src="{$jslib_uri}/jquery/jquery.js"></script>
</xsl:template>

<!-- ==========
	jquery-form-js
	to include jquery.form in HTML HEAD element
	========== -->

<xsl:template name="jquery-form-js">
	<script type="text/javascript" src="{$jslib_uri}/jquery/jquery.form.js"></script>
</xsl:template>

<xsl:template name="jquery-ui">
	<script type="text/javascript" src="{$jslib_uri}/jquery/jquery-ui-mioga2.js"></script>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery-ui-mioga2.css" media="screen" />
</xsl:template>

<xsl:template name="jquery-ui-1.12.1">
	<script type="text/javascript" src="{$jslib_uri}/jquery/jquery-ui-1.12.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery-ui-1.12.1.min.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery-ui-1.12.1.theme.min.css" media="screen" />
</xsl:template>

<xsl:template name="jquery-layout">
	<script type="text/javascript" src="{$jslib_uri}/jquery/jquery.layout.min-1.2.0.js"></script>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.layout.css" media="screen" />
</xsl:template>

<xsl:template name="jquery-treeview">
	<script type="text/javascript" src="{$jslib_uri}/jquery/jquery.treeview.js"></script>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.treeview.css" media="screen" />
</xsl:template>

<xsl:template name="jquery-form-serializeobject">
	<script type="text/javascript" src="{$theme_uri}/javascript/jquery.form.serializeObject.js"></script>
</xsl:template>

</xsl:stylesheet>
