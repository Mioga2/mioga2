<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html"/>

<xsl:template name="MiogaReportPortalDesc">
    <xsl:param name="node" select="."/>
        
    <xsl:variable name="href"><xsl:value-of select="$bin_uri"/>/<xsl:value-of select="$node/@group"/>/Portal/DisplayPortal?path=/<xsl:value-of select="@file"/></xsl:variable>

    <tr>
        <td>
            <table cellpadding="3" cellspacing="3" class="portalBox">
                <tr>
                    <td class="title">
                        <a href="{$href}">
                            <xsl:value-of select="@name"/>
                        </a>
                    </td>
                    <td class="edit">
                        <xsl:if test="@canwrite = 1">
                            <a href="EditPortal?path=/{@file}&amp;nocache=1">
                                <img src="{$image_uri}/16x16/actions/document-edit.png"/>
                            </a>
                        </xsl:if>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="desc">
                        <xsl:copy-of select="."/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&#160;</td>
    </tr>
</xsl:template>


</xsl:stylesheet>

