<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mioga="urn:mioga" >
<xsl:import href="base.xsl"/>
<xsl:import href="jquery.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<!-- ===============
	root node
	================ -->

<xsl:template match="/">
<xsl:call-template name="doctype"/>
<html>
<head>
	<xsl:call-template name="title"/>
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="jquery-js"/>
	<xsl:call-template name="jquery-ui"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery-ui-mioga2.css"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/jquery.dropDown.css"/>

	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	
	
	
	<script src="{$theme_uri}/javascript/eiffel.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/eiffel.css"/>
	<xsl:call-template name="theme-css"/>
	<xsl:apply-templates mode="head"/>
</head>
<body class="eiffel mioga">
	<xsl:call-template name="DisplayAppTitle">
	</xsl:call-template>

	<xsl:apply-templates />
</body>
</html>
</xsl:template>

<!-- ===============
	DisplayMain
	================ -->

<xsl:template match="/DisplayMain" mode="head">
	<script type="text/javascript">
	$(document).ready(function(){
		$( "#progressbar" ).progressbar({ value: 0 });
		$('#eiffel').eiffel({
								i18n : {
										monthNames:[ "<xsl:value-of select="mioga:gettext('January')"/>",
													 "<xsl:value-of select="mioga:gettext('February')"/>",
													 "<xsl:value-of select="mioga:gettext('March')"/>",
													 "<xsl:value-of select="mioga:gettext('April')"/>",
													 "<xsl:value-of select="mioga:gettext('May')"/>",
													 "<xsl:value-of select="mioga:gettext('June')"/>",
													 "<xsl:value-of select="mioga:gettext('July')"/>",
													 "<xsl:value-of select="mioga:gettext('August')"/>",
													 "<xsl:value-of select="mioga:gettext('September')"/>",
													 "<xsl:value-of select="mioga:gettext('October')"/>",
													 "<xsl:value-of select="mioga:gettext('November')"/>",
													 "<xsl:value-of select="mioga:gettext('December')"/>" ],
										returnToMain : "<xsl:value-of select="mioga:gettext('Return to main')"/>",
										applyLabel :  "<xsl:value-of select="mioga:gettext('Apply')"/>",
										backwardAllLabel : "<xsl:value-of select="mioga:gettext('backward N weeks')"/>",
										backwardLabel : "<xsl:value-of select="mioga:gettext('backward one week')"/>",
										forwardLabel : "<xsl:value-of select="mioga:gettext('forward one week')"/>",
										forwardAllLabel : "<xsl:value-of select="mioga:gettext('forward N weeks')"/>",
										zoomInLabel : "<xsl:value-of select="mioga:gettext('Zoom in')"/>",
										zoomOutLabel : "<xsl:value-of select="mioga:gettext('Zoom out')"/>",
										todayLabel : "<xsl:value-of select="mioga:gettext('Today')"/>",
										weekCountLabel : "<xsl:value-of select="mioga:gettext('Week count')"/>",
										evtFromGroupsLabel : "<xsl:value-of select="mioga:gettext('Show events from groups')"/>",
										paramsLabel : "<xsl:value-of select="mioga:gettext('Parameters')"/>",
										itemSeqLabel : "<xsl:value-of select="mioga:gettext('Display sequence')"/>",
										addSepLabel : "<xsl:value-of select="mioga:gettext('Add a separator')"/>",
										destroySeparator : "<xsl:value-of select="mioga:gettext('Destroy')"/>",
										configurationText : "<xsl:value-of select="mioga:gettext('Configuration')"/>",
										showDetailsTitle: "<xsl:value-of select="mioga:gettext('Details for')"/>",
										fromLabel: "<xsl:value-of select="mioga:gettext('From')"/>",
										toLabel: "<xsl:value-of select="mioga:gettext('To')"/>",
										closeLabel: "<xsl:value-of select="mioga:gettext('Close')"/>",
										noTasksText: "<xsl:value-of select="mioga:gettext('No tasks for this day')"/>",
										weekLabel: "<xsl:value-of select="mioga:gettext('Week')"/>",
										weeksLabel: "<xsl:value-of select="mioga:gettext('Weeks')"/>",
										progressBarText: "<xsl:value-of select="mioga:gettext('Eiffel initializing, please wait ...')"/>",
										categoryLegend: "<xsl:value-of select="mioga:gettext('Category legend')"/>",
										reattachWindow: "<xsl:value-of select="mioga:gettext('Reattach the window')"/>"
									}
							});
	 });
	</script>
</xsl:template>

<xsl:template match="/DisplayMain">
	<div id="eiffel"></div>
</xsl:template>

</xsl:stylesheet>
