function submitForm (formName, fieldName, value) {
    var i = 0;
    var form;
    var oldval;
    var nbforms = document.forms.length;
        
    while(i < nbforms  && document.forms[i].name != formName) { i++; };
 
    if(i >= nbforms) {
       i = 0;
    }

    form = document.forms[i];
    i=0;
    while(form.elements[i].name != fieldName) { i++; }; 
    oldval = form.elements[i].value;
    form.elements[i].value=value;
    form.submit();
    form.elements[i].value=oldval;
}
// ----------------------------------------------------------------------
// CloseDialog
//  displays off the given dialog box
// ----------------------------------------------------------------------
function CloseDialog(dialog_id) {
	var dialog = $(dialog_id);
	if (dialog) {
		dialog.style.display = 'none';
	}
	else {
		alert('CloseDialog - No such dialog : ' + dialog_id);
	}
}

// ----------------------------------------------------------------------
// Utility functions to change visibility of an element
// Different from prototype hide() and show() methods since these ones
// use the display attribute of the element.
// ----------------------------------------------------------------------
function setVisible(elm) {
    elm.style.visibility = 'visible';
}
    
function setHidden(elm) {
    elm.style.visibility = 'hidden';
}


// ----------------------------------------------------------------------
// Utility functions to hide InlineMessage
// ----------------------------------------------------------------------
function hide_inline_message () {
	if ($('InlineMessage').getOpacity ()) {
		new Effect.Opacity ('InlineMessage', {duration:2, fps:25, from:1.0, to:0.0});
		setTimeout("$('InlineMessage').style.top = -2000", 2000);
	}
}


// ----------------------------------------------------------------------
// Remove accents from a string
// ----------------------------------------------------------------------
function unaccent (str) {
	str = str.replace (/[áâãäå]/g, "a")
	str = str.replace (/[ÀÁÂÃÄÅ]/g, "A")
	str = str.replace (/[èéêë]/g,"e");
	str = str.replace (/[ÈÉÊË]/g,"E");
	str = str.replace (/[ìíîï]/g,"i");
	str = str.replace (/[ÌÍÎÏ]/g,"I");
	str = str.replace (/[òóôõöø]/g,"o");
	str = str.replace (/[ÒÓÔÕÖØ]/g,"O");
	str = str.replace (/[ùúûü]/g,"u");
	str = str.replace (/[ÙÚÛÜ]/g,"U");
	str = str.replace (/[ñ]/g,"n");
	str = str.replace (/[Ñ]/g,"N");
	str = str.replace (/[ç]/g,"c");
	str = str.replace (/[Ç]/g,"C");

	return (str);
}

// To navigate between groups, see <SELECT id="navigation_groups">
var navigation_groups_timer = null;
function navigation_groups() {
	// Timeout for avoiding multiple events (scrolling)
	window.clearTimeout(navigation_groups_timer);
	navigation_groups_timer = window.setTimeout(function() {
			document.location.href = document.getElementById("navigation_groups").value;
	}, 500);
}


// ----------------------------------------------------------------------
// Extensions to Date API
// ----------------------------------------------------------------------
// first_day = 0 for Sunday, 1 for monday
Date.prototype.firstDayOfWeek = function(first_day) {
	if (first_day !== 0 && first_day !== 1) {
		first_day = 0;
	}
	var ret_date = new Date(this);
	var d = this.getDay() - first_day;
	ret_date.setDate(ret_date.getDate() - d);
	ret_date.setHours(0);
	ret_date.setMinutes(0);
	ret_date.setSeconds(0);
	return ret_date;
}
// From Wikipedia
Date.prototype.getWeek = function() {
	var d_of_w = this.getDay();
	var d = new Date(this);
	d.setDate(this.getDate() - (d_of_w + 6) % 7 + 3); // Nearest Thu
	var milli = d.getTime();
	d.setMonth(0);
	d.setDate(4);
	return Math.round((milli - d.getTime()) / (7 * 86400000)) + 1
}
Date.prototype.getLastDayOfMonth = function() {
	var end_date = new Date();
	end_date.setDate(1);
	if (this.getMonth() === 11) {
		end_date.setMonth(0);
		end_date.setFullYear(this.getFullYear() + 1);
	}
	else {
		end_date.setMonth(this.getMonth() + 1);
		end_date.setFullYear(this.getFullYear());
	}
	end_date.setDate(end_date.getDate() - 1);
	return end_date;
}
Date.prototype.parseMiogaDate = function(val, date_only) {
	var re;
	if (date_only) {
		re = /^(\d+)-(\d+)-(\d+)/;
	} else {
		re = /^(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/;
	}
	var res = re.exec(val);
	this.setUTCFullYear(res[1]);
	this.setUTCDate(1);	// This line is mandatory. If this.getDay is not valid in MiogaDate month, then this.getMonth gets increased
	this.setUTCMonth(res[2] - 1);
	this.setUTCDate(res[3]);
	if (date_only) {
		this.setUTCHours(0);
		this.setUTCMinutes(0);
		this.setUTCSeconds(0);
		this.setUTCMilliseconds(0);
	} else {
		this.setUTCHours(res[4]);
		this.setUTCMinutes(res[5]);
		this.setUTCSeconds(res[6]);
		this.setUTCMilliseconds(0);
	}
}
Date.prototype.formatHour = function(fl_seconds, fl_utc) {
	function zeroPad(n) {
		return (n < 10 ? '0' : '') + n;
	}

	var c;
	if (fl_utc === true) {
		c = zeroPad(this.getUTCHours()) + ':' + zeroPad(this.getUTCMinutes());
		if (fl_seconds) {
			c += ':' + zeroPad(this.getUTCSeconds());
		}
	}
	else {
		c = zeroPad(this.getHours()) + ':' + zeroPad(this.getMinutes());
		if (fl_seconds) {
			c += ':' + zeroPad(this.getSeconds());
		}
	}

	return c;
}
Date.prototype.formatMioga = function(fl_hour, fl_utc) {
	function zeroPad(n) {
		return (n < 10 ? '0' : '') + n;
	}

	var c;
	if (fl_utc === true) {
		c = this.getUTCFullYear() + '-' + zeroPad(this.getUTCMonth() + 1) + '-' + zeroPad(this.getUTCDate());
		if (fl_hour) {
			c += ' ' + zeroPad(this.getUTCHours()) + ':' + zeroPad(this.getUTCMinutes()) + ':' + zeroPad(this.getUTCSeconds());
		}
	}
	else {
		c = this.getFullYear() + '-' + zeroPad(this.getMonth() + 1) + '-' + zeroPad(this.getDate());
		if (fl_hour) {
			c += ' ' + zeroPad(this.getHours()) + ':' + zeroPad(this.getMinutes()) + ':' + zeroPad(this.getSeconds());
		}
	}

	return c;
}


// =============================================================================
/**
Utilities objects and methods for Mioga2 javascript development

Note : JQuery $ function must be defined

@module MIOGA
**/
// =============================================================================

var MIOGA = {};

// ----------------------------------------------------------------------------
/**
Log an error on the server. Must be used for non user errors, to detect bugs immediatly.

@method logError
@param string Message to display
@param boolean Reload window.location if true
**/
// --------------------------------------------------------
MIOGA.logError = function (message, reload) {
	//TODO must call a Mioga2 web service
	alert(message);
	if (reload === true) {
		location.reload(true);
	}
}
// ----------------------------------------------------------------------------
/**
Generate an unique id for DOM object with an optionnal prefix. This method uses the id_count attribute
of MIOGA object and increment for each call.

@method generateID
@param string prefix (optionnal)
**/
// --------------------------------------------------------
MIOGA.id_count = 1;
MIOGA.generateID = function (prefix) {
	var p = "";
	if (typeof prefix === "string") {
		p = prefix;
	}
	return p + String(this.id_count++);
}


// ----------------------------------------------------------------------------
/**
Print a debug message on console with optionnals arguments. It uses an associative array depending on module names
to show messages or not. Set MIOGA.debug[module name] to 2 to log messages from level 1 to 2.

@method logDebug
@param string module Name of source module
@param int level Level of debug message to compare with Mioga.debug option for module
@param string message Message to display
@param object obj an optionnal object to dump
**/
// --------------------------------------------------------
MIOGA.debug = {};
MIOGA.logDebug = function (module, level, message, obj) {
	if (MIOGA.debug[module] && MIOGA.debug[module] >= level) {
		console.log(module + " : " + message);
		if (typeof obj === "object") {
			if (window.console.dir) {
				console.dir(obj);
			}
		}
	}
}
//----------------------------------------------------------------------------
/**
Initialize Mioga dialog.

@method dialog
@param object to configure the dialog
	@option String title dialog title
	@option Object content Jquery object that is a content of dialog
	@option buttons Array of object buttons
		@option String i18n button translation
		@option Array classes containing each CSS classes to this button
		@option Function cb is a call back called on button click. It contains two parameters: event click on button and dialog as Jquery selector.

@example

MIOGA.dialog({
	title : "My title of dialog",
	content : $(<div>This is a content of my dialog</div>),
	buttons : [
		{
			i18n : "OK",
			classes : ["button","confirm-btn"];
			cb : function (evClick,$dialog) {
				console.log('OK button is clicked!');
				$dialog.dialog('destroy');
			}
		},
		{
			i18n : "Cancel",
			classes : ["button","cancel"];
			cb : function (evClick,$dialog) {
				console.log('cancel button is clicked!');
				$dialog.dialog('destroy');
			}
		}
	]
});
**/
// --------------------------------------------------------
MIOGA.dialog = function (args) {
	var $dialog = $('<div class="mioga-ui-dialog"></div>').append(args.content);
	var $button_list = $('<ul class="button_list"></ul>').appendTo($dialog);
	$.each(args.buttons, function (i,e) {
		var $btn = $('<button class="button">' + e.i18n + '</button>')
			.click(function (ev) {
				e.cb(ev, $dialog);
			})
			.appendTo($('<li></li>').appendTo ($button_list));
		if ((e.classes !== undefined) && (e.classes.length)) {
			$btn.addClass(e.classes.join(' '))
		}
	});
	$dialog.dialog({
		autoOpen : true,
		modal : true,
		resizable: false,
		width : "auto",
		title : args.title || "",
		closeOnEscape : true
	});
	return ($dialog);
};
//----------------------------------------------------------------------------
/**
Initialize Mioga confirm dialog.

@method MIOGA.confirm
@param object to configure the dialog
	@option String title is dialog title
	@option String i18n_OK is translation to OK button
	@option Function cb_OK callback that is called on OK button click
	@option String i18n_cancel is translation to cancel button
	@option Function cb_cancel callback that is called on cancel button click
	@option String message is HTML message to confirmation

@example

MIOGA.confirm({
	title : "delete : My item",
	i18n_OK : "OK",
	cb_OK : function (evClick,$dialog) {
			console.log('OK button is clicked!');
			$dialog.dialog('destroy');
	},
	i18n_cancel : "Cancel",
	cb_cancel : function (evClick,$dialog) {
		console.log('cancel button is clicked!');
		$dialog.dialog('destroy');
	},
	message : "<p class="error">Are you sure ?</p>"
});
**/
// --------------------------------------------------------
MIOGA.confirm = function (args) {
	var $dialog = $('<div class="mioga-ui-dialog"></div>').append(args.message);
	var $button_list = $('<ul class="button_list"></ul>').appendTo($dialog);
	$('<button class="button">' + args.i18n_OK + '</button>')
		.click(function (ev) {
			args.cb_OK(ev, $dialog);
		})
		.appendTo($('<li></li>').appendTo ($button_list));
	$('<button class="button cancel">' + args.i18n_cancel + '</button>')
	.click(function (ev) {
		args.cb_cancel(ev, $dialog);
	})
	.appendTo($('<li></li>').appendTo ($button_list));
	$dialog.dialog({
		autoOpen : true,
		modal : true,
		resizable: false,
		width : "auto",
		title : args.title || "",
		closeOnEscape : true
	});
	return ($dialog);
};
//----------------------------------------------------------------------------
/**
Initialize delete confirm dialog.

@method MIOGA.deleteConfirm
@param object to configure the dialog
	@option String title is dialog title
	@option String i18n_OK is translation to OK button
	@option Function cb_OK callback that is called on OK button click
	@option String i18n_cancel is translation to cancel button
	@option Function cb_cancel callback that is called on cancel button click
	@option String message is HTML message to delete confirmation

@example

MIOGA.deleteConfirm({
	title : "delete : My item",
	i18n_OK : "OK",
	cb_OK : function (evClick,$dialog) {
			console.log('OK button is clicked!');
			$dialog.dialog('destroy');
	},
	i18n_cancel : "Cancel",
	cb_cancel : function (evClick,$dialog) {
		console.log('cancel button is clicked!');
		$dialog.dialog('destroy');
	},
	message : "<p class="error">Are you sure ?</p>"
});
**/
// --------------------------------------------------------
MIOGA.deleteConfirm = function (args) {
	var $dialog = $('<div class="mioga-ui-dialog-delete"></div>').append(args.message);
	var $button_list = $('<ul class="button-list"></ul>').appendTo($dialog);
	$('<button type="button" class="button">' + args.i18n_OK + '</button>')
		.click(function (ev) {
			args.cb_OK(ev, $dialog);
		})
		.appendTo($button_list);
	$('<button type="button" class="button cancel">' + args.i18n_cancel + '</button>')
	.click(function (ev) {
		args.cb_cancel(ev, $dialog);
	})
	.appendTo($button_list);
	$dialog.dialog({
		autoOpen : true,
		modal : true,
		width : "auto",
		title : args.title || "",
		closeOnEscape : true
	});
};



// ----------------------------------------------------------------------
// Add X-CSRF-Token header from cookie value to each XHR request
// ----------------------------------------------------------------------
(function() {
	// Read cookie value
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
		}
		return "";
	}

	// Overload XHR send
	var xhrsend = XMLHttpRequest.prototype.send;
	XMLHttpRequest.prototype.send = function(data) {
		if (this.csrf_header_set === undefined) {
			this.setRequestHeader('X-CSRF-Token', getCookie("X-CSRF-Token"));
			this.csrf_header_set = true;
		}
		return xhrsend.apply(this, arguments);
	};
})();
