// Ugly global vars
var answer_count = 0;
var calsd = null;
var caled = null;

function setValueSD(y,m,d) {
    var date    = new Date(y, m-1, d);
    var day     = date.getDate();
    var month   = date.getMonth() + 1;
    var year    = date.getFullYear();
    
    if (month < 10)
        month   = "0" + month;
    if (day < 10)
        day     = "0" + day;
    
    $("s_date_formatted").innerHTML = day + "/" + month + "/" + year;
    $("s_fixed_date").value = year + "-" + month + "-" + day + " 00:00:00";
}

function setValueED(y,m,d) {
    var date    = new Date(y, m-1, d);
    var day     = date.getDate();
    var month   = date.getMonth() + 1;
    var year    = date.getFullYear();
    
    if (month < 10)
        month   = "0" + month;
    if (day < 10)
        day     = "0" + day;
    
    $("e_date_formatted").innerHTML = day + "/" + month + "/" + year;
    $("e_fixed_date").value = year + "-" + month + "-" + day + " 00:00:00";
}

function addAnswer(delete_confirm, image_uri, delete_str) {
    var new_answer = Builder.node("li", {id: "a-" + answer_count}, [
        Builder.node("input", {type: "text", className: "text", name: "answers", value: ""}),
        Builder.node("a", {href: "#", onclick: "if (confirm('"+delete_confirm+"')) { new Effect.Fade(this.parentNode, { duration: 0.5}) }; return false;"}, [
            Builder.node("img", {src: image_uri+"/16x16/actions/trash-empty.png", alt: delete_str, title: delete_str})
            ])
        ]);
    
    $("answers-list").appendChild(new_answer);
    answer_count++;
    return false;
}

function checkForm(form, err_int_msg, err_float_msg, err_empty_msg) {
    var element = form.elements[form.elements.length - 3];
    
    if ($F("q_type").match(/FREE/)) {
        if ($F("q_type").match(/INTEGER/))
            return checkInt(element, err_int_msg);
        else if ($F("q_type").match(/FLOAT/))
            return checkFloat(element, err_float_msg);
        else
            return checkEmpty(element, err_empty_msg);
    }
    else if ($F("q_type").match(/F_/)) {
        if ($F("text-answer")) {
            if ($F("q_type").match(/INTEGER/))
                return checkInt(element, err_int_msg);
            else if ($F("q_type").match(/FLOAT/))
                return checkFloat(element, err_float_msg);
            else
                return checkEmpty(element, err_empty_msg);
        }
    }
    return true;
}

function checkInt(input, err_msg) {
    if (input.value.match(/^\d+$/))
        return true;
    alert(err_msg);
    input.focus();
    return false;
}

function checkFloat(input, err_msg) {
    if (input.value.match(/^\d+\.?\d*$/))
        return true;
    alert(err_msg);
    input.focus();
    return false;
}

function checkEmpty(input, err_msg) {
    if (!input.value.match(/^\s*$/))
        return true;
    alert(err_msg);
    input.focus();
    return false;
}
