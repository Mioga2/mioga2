function RefreshGrid(data, callback) {
	var grid = dojo.getObject(grid_id);
	var compmap = grid.store.comparatorMap;

	var filter;
	var filter_form = dijit.byId (filter_form_id);
	if (filter_form) {
		filter = filter_form.attr ('value');
	}

	// Check if filter data is defined
	var filtered = false;
	for (key in filter){
		if (key !== 'match' && filter[key] !== '') {
			filtered = true;
		}
	}
	if (filtered === false) {
		filter = {};
	}

	for (var key in data) {
		filter[key] = data[key];
	}

	console.log ('Filter:');
	console.dir (filter);

	// Show tooltip
	if (filtered) {
		dijit.showTooltip (filter_message, dojo.byId ('filter_dropdown'), "");
	}
	else {
		dijit.hideTooltip (dojo.byId ('filter_dropdown'));
	}

	grid.setStore(new mioga.data.ItemFileReadStore({sync:true,url:dataStoreUrl,args:filter,nodename:dataStoreNodeName,identifier:dataStoreIdentifier,label:dataStoreLabel}));
	grid.store.comparatorMap = compmap;
	grid.store.fetch ();
	
	if (callback) {
		callback ();
	}
}

// Shows dialog to confirm selection action.
// Dialog summarizes elements on which the operation will be performed.
function ConfirmSelectionAction (title, fields, dojo_i18n_bundle, callback) {
	var dialog = dijit.byId ('ConfirmSelectionActionDialog');
	var grid = dijit.byId (grid_id);
	var container = dojo.query(".contents", dojo.byId ('ConfirmSelectionActionDialog'))[0];

	dojo.requireLocalization("mioga", dojo_i18n_bundle);
	var transl = dojo.i18n.getLocalization("mioga", dojo_i18n_bundle);

	// Set title
	dialog.attr ('title', title);

	// Set list of items to be processed
	var contents = '<table class="list border_color">';
	contents += '<tr>';
	dojo.forEach (fields, function (field) {
		contents += '<th>' + transl[field] + '</th>';
	});
	contents += '</tr>';
	var items = grid.selection.getSelected();
	if (items.length) {
		dojo.forEach (items, function (selItem, index, array) {
			if (selItem) {
				contents += '<tr>';
				dojo.forEach (fields, function (field) {
					var val = grid.store.getValues(selItem, field);
					contents += '<td>' + val + '</td>';
				});
				contents += '</tr>';
			}
		});
	}
	contents += "</table>";
	console.log ('Contents: ' + contents);

	container.innerHTML = contents;

	// Set callback to button click
	var button = dojo.query("a.button", dojo.byId ('ConfirmSelectionActionDialog'))[0];
	button.onclick = function () { callback () ; dialog.hide (); };

	// Show dialog
	dialog.show ();
}

function CloseConfirmSelectionActionDialog () {
	dijit.byId ('ConfirmSelectionActionDialog').hide ();
}

function Filter (id, gridId) {
	if (js_filter) {
		console.debug('id = ' + id + ' gridId = ' + gridId);
		var f = dojo.byId(id);
		var grid = dojo.getObject(gridId);
		//console.debug('f = ' + f + ' length = ' + f.elements.length + ' grid ' + grid + ' grid.class  ' + grid.class);
		console.dir (f.elements);
		var match = f.elements.namedItem ('match').value;
		var s = new Object();
		var showtooltip = false;
		for(var i = 0; i < f.elements.length; i++){
			var elem = f.elements[i];
			console.debug('i = ' + i + ' elem = ' + elem + ' elem.name = ' + elem.name);
			if ((elem.name) && (elem.name != 'match') && (elem.value) && (elem.value.length > 0)) {
				if (match === 'contains') {
					s[elem.name] = '*' + elem.value + '*';
				}
				else if (match === 'begins') {
					s[elem.name] = elem.value + '*';
				}
				else if (match === 'ends') {
					s[elem.name] = '*' + elem.value;
				}
				showtooltip = true;
			}
		}

		console.debug('s = ' + s);
		for (var nom in s) console.debug(nom + ' = ' + s[nom]);
		grid.queryOptions = {ignoreCase: true};
		grid.filter(s);
		if (showtooltip) {
			dijit.showTooltip (filter_message, dojo.byId ('filter_dropdown'), "");
		}
		else {
			dijit.hideTooltip (dojo.byId ('filter_dropdown'));
		}
	}
	else {
		RefreshGrid ();
	}
	var gridNode = dojo.byId (gridId);
	dijit.focus (gridNode);

}

function ClearFilter (id, gridId) {
	var f = dojo.byId (id);
	f.reset ();
	Filter (id, gridId);
}

// Handle "return" press in filter form
function filter_list (event) {
	if (event.keyCode == 13) {
		Filter (filter_form_id, grid_id);
	}
}
