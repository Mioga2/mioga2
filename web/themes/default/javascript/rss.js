function init_rss() {
    var feeds       = $$('ul.feeds').first();
    var event_type  = (navigator.userAgent.match(/msie/i)) ? 'click' : 'change';
    feeds.observe(event_type, update_feeds);
    feeds.observe('click', toggle_groups);
}

function toggle_groups(event) {
    element = Event.element(event);
    if (element.tagName.toLowerCase() != 'img' || !element.id.match(/^toggle\-group/)) { return; }
    Element.up(element, 'li.group').getElementsBySelector('ul').each(function(u) {  Element.toggle(u); });
    toggle_fold(element);
}

function toggle_fold(element) {
    if (element.src.match(/unfold\.gif$/))
        element.src = image_path + "/fold.gif";
    else
        element.src = image_path + "/unfold.gif";
}

function update_feeds(event) {
    element = Event.element(event);
    if (element.type != 'checkbox') { return; }
    
    if (element.name.match(/group-(\w+)/)) {
        Element.up(element).getElementsBySelector('input[type=checkbox]').each(function(c) { c.checked = element.checked });
    }
    else {
        if (element.checked == false) { Element.up(element, 'li.group').down('input').checked = false; }
    }
}