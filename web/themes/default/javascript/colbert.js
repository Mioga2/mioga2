var skel_block_toggler;
var app_block_toggler;
var error_block_toggler;
var underlay;

dojo.requireLocalization("mioga", "colbert");
var transl = dojo.i18n.getLocalization("mioga", "colbert");

var user_sort_fields = {
	firstname: transl.firstname,
	lastname: transl.lastname,
	email: transl.email,
	selected: transl.membership
};

var group_sort_fields = {
	ident: transl.ident,
	selected: transl.membership
};

var team_sort_fields = {
	ident: transl.ident,
	selected: transl.membership
};

var application_sort_fields = {
	label: transl.label,
	selected: transl.authorization
};


/* -------------------------------------- *
 *           Generic functions            *
 * -------------------------------------- */

function ShowUnderlay () {
	if (!underlay) {
		underlay = dijit.byId ('underlay');
		if (!underlay) {
			underlay = new dijit.DialogUnderlay ({'class': 'loading', 'id': 'underlay'});
		}
	}
	underlay.show ();
}


/* -------------------------------------- *
 *            User functions              *
 * -------------------------------------- */

function CloseUserDialog() {
	dijit.byId ('edit_user').hide ();
}

function ClearUserErrors () {
	// Hide error block
	dojo.byId ('edit_user_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('edit_user')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayUserEdit (rowid) {
	console.debug('rowid ' + rowid);

	// Clear errors
	ClearUserErrors ();

	// Enable user attribute fields
	dojo.forEach (Array ('firstname', 'lastname', 'email', 'password', 'password2'), function (elm) {
		dijit.byId ('user_attributes_'+elm).set ('disabled', false);
	});

	// Reset form
	dijit.byId('user_attributes_form').reset ();
	dijit.byId('user_attributes_autonomous').set ('value', true);
	dijit.byId('user-status-active').set ('value', true);

	// Replace {ident} macro with span in titles
	dojo.query ("h1.tabtitle").forEach (function (node) { node.innerHTML = node.innerHTML.replace (/{ident}/, '<span class="user-ident"></span>'); });
	dojo.query ("h1.tabtitle span.user-ident").forEach (function (node) { node.innerHTML = ''; });
	dojo.query ("div.tab").forEach (function (node) { node.title = node.title.replace (/ {ident}/, ''); });

	if (rowid) {
		ShowUnderlay ();

		// Hide skeleton chooser
		dojo.byId ('user-skel-chooser').style.display = 'none';

		// Hide ident input field and note
		dojo.byId ('user-ident-field').style.display = 'none';
		dojo.byId ('identifier_info').style.display = 'none';

		// Reset password input values
		dijit.byId ('user_attributes_password').set ('value', '');
		dijit.byId ('user_attributes_password2').set ('value', '');

		dojo.xhrGet({
			url: "GetUserDetails.json",
			timeout: 30000, // give up after 3 seconds
			content: { rowid:rowid,full_list:1 },
			handleAs: "json",
			load: function(response, ioArgs){
				console.dir (response);
				delete (response.user.password);

				// Add user label to titles
				dojo.query ("h1.tabtitle span.user-ident").forEach (function (node) { node.innerHTML = response.user.label; });

				// Set list of groups, teams and applications
				var sel = new Array ();
				var all = new Array ();
				dojo.forEach (Array ('group', 'team', 'application'), function (elm) {
					sel[elm] = new Array ();
					all[elm] = new Array ();
					if (response[elm+'s'] && response[elm+'s'][elm]) {
						dojo.forEach (response[elm+'s'][elm], function (item) {
							if (item.selected) {
								sel[elm].push (item);
							}
							all[elm].push (item);
						});
					}
					dijit.byId ('user_attributes_'+elm+'s').createStore (all[elm], sel[elm]);
				});
				underlay.hide ();

				dojo.forEach (all.application, function (item) {
					if (item['protected']) {
						dijit.byId ('user_attributes_applications').lockItem (item.ident, 'ident');
					}
				});

				// Set form values
				dijit.byId('user_attributes_form').set ('value', response.user);
				if (response.user.autonomous != 0) dijit.byId('user_attributes_autonomous').set ('value', response.user.autonomous);
				if (response.user.shared != 0) dijit.byId('user_attributes_shared').set ('value', response.user.shared);

				// Disable user attribute fields in case of LDAP user
				if (response.user.type == 'ldap_user') {
					dojo.forEach (Array ('firstname', 'lastname', 'email', 'password', 'password2'), function (elm) {
						dijit.byId ('user_attributes_'+elm).set ('disabled', true);
					});
				}
			},
			error: function(err, ioArgs){
				var transl = dojo.i18n.getLocalization("mioga", "colbert");
				var dialog = new dijit.Dialog({
					title: transl['generic_error_title'],
					content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
				});
				dialog.show ();
				// The two lines below are because of a bad rendering with Chromium
				dojo.style(dialog.titleBar, { width: '100%' });
				dojo.style(dialog.containerNode, { width: '100%' });
				underlay.hide ();
			}
		});
	}
	else {
		// Show skeleton chooser
		dojo.byId ('user-skel-chooser').style.display = 'block';

		// Show ident input field and note
		dojo.byId ('user-ident-field').style.display = 'block';
		dojo.byId ('identifier_info').style.display = 'block';

		// Set FilteringSelect initial values
		dijit.byId ('user_attributes_lang').set ('value', lang);
		dijit.byId ('user_attributes_skeleton').store.fetch ({onComplete: function () { var store_data = dijit.byId ('user_attributes_skeleton').store._arrayOfAllItems; console.dir (store_data); dijit.byId ('user_attributes_skeleton').set ('value', store_data[0].file[0]);}});
	}

	// Show dialog
	dijit.byId ('edit_user').show ();

	// Set first tab as visible
	dijit.byId ('tabbed_dialog').showFirst ();

	// Ensure dialog is centered
	dijit.byId ('edit_user')._position ();
}

function UpdateUserSkeletonDetails () {
	var lang = dijit.byId ('user_attributes_lang').get ('value');
	var file = dijit.byId ('user_attributes_skeleton').get ('value');

	if (file && lang) {
		ShowUnderlay ();
		dojo.xhrGet({
			url: "GetSkeletonDetails.json",
			timeout: 30000, // give up after 3 seconds
			content: { type:'user',lang:lang,file:file,full_list:1 },
			handleAs: "json",
			load: function(response, ioArgs){
				console.dir (response);
				dijit.byId('user_attributes_form').setValues (response.attributes);
				if (response.attributes.autonomous != 0) dijit.byId('user_attributes_autonomous').set ('value', response.attributes.autonomous);
				if (response.attributes.shared != 0) dijit.byId('user_attributes_shared').set ('value', response.attributes.shared);

				// Set list of groups, teams and applications
				var sel = new Array ();
				var all = new Array ();
				dojo.forEach (Array ('group', 'team', 'application'), function (elm) {
					sel[elm] = new Array ();
					all[elm] = new Array ();
					if (response[elm+'s'] && response[elm+'s'][elm]) {
						dojo.forEach (response[elm+'s'][elm], function (item) {
							if (item.selected) {
								sel[elm].push (item);
							}
							all[elm].push (item);
						});
					}
					dijit.byId ('user_attributes_'+elm+'s').createStore (all[elm], sel[elm]);
				});
				underlay.hide ();

				dojo.forEach (all.application, function (item) {
					if (item['protected']) {
						dijit.byId ('user_attributes_applications').lockItem (item.ident, 'ident');
					}
				});
			},
			error: function(err, ioArgs){
				underlay.hide ();
				var transl = dojo.i18n.getLocalization("mioga", "colbert");
				var dialog = new dijit.Dialog({
					title: transl['generic_error_title'],
					content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
				});
				dialog.show ();
				// The two lines below are because of a bad rendering with Chromium
				dojo.style(dialog.titleBar, { width: '100%' });
				dojo.style(dialog.containerNode, { width: '100%' });
			}
		});
	}
	else {
		// FilteringSelect values not initialized, plan a new call to function
		setTimeout (UpdateUserSkeletonDetails, 1000);
	}
}

function ValidateUser() {
	console.debug('Validate called');
	var transl = dojo.i18n.getLocalization("mioga", "colbert");

	// Clear errors
	ClearUserErrors ();

	// Enable user attribute fields
	var disable = dijit.byId ('user_attributes_firstname').get ('disabled');
	dojo.forEach (Array ('firstname', 'lastname', 'email', 'password', 'password2'), function (elm) {
		dijit.byId ('user_attributes_'+elm).set ('disabled', false);
	});

	// Get form data
	var form = dijit.byId('user_attributes_form');
	console.debug("form " + form);
	var form_data = form.get ("value");

	if (disable) {
		dojo.forEach (Array ('firstname', 'lastname', 'email', 'password', 'password2'), function (elm) {
			dijit.byId ('user_attributes_'+elm).set ('disabled', true);
		});
	}

	// Delete rowid if empty string (adding)
	if (form_data.rowid == "") delete form_data.rowid;

	// Delete passwords if empty strings (updating)
	if (form_data.password == "") {
		delete form_data.password;
		delete form_data.password2;
	}

	console.debug("form_data " + form_data);

	// Transform checkboxes values from array to flag
	if (form_data.shared[0]) {
		form_data.shared = 1;
	}
	else {
		form_data.shared = 0;
	}
	if (form_data.autonomous[0]) {
		form_data.autonomous = 1;
	}
	else {
		form_data.autonomous = 0;
	}

	// Get groups, teams and applications
	dojo.forEach (Array ('groups', 'teams', 'applications'), function (elm) {
		form_data[elm] = new Array ();
		dojo.forEach (dijit.byId ('user_attributes_' + elm).getList (), function (item) {
			form_data[elm].push (item.ident);
		});
	});

	console.dir(form_data);

	// Display loading underlay
	ShowUnderlay ();

	dojo.xhrPost({
		url: "SetUser.json",
		timeout: 30000, // give up after 3 seconds
		content: form_data,
		handleAs:"json",
		load: function(response, ioArgs){
			console.debug("load function  response " + response);
			console.dir(response);
			if (response.errors) {
				var errors = '<ul>';
				var displayed_policy = false;
				dojo.forEach(response.errors, function(item, index, array) {
					for (var i in item) {
						console.log (i + ": " + item[i]);
						dojo.byId ('user_attributes_'+i+'_label').className = 'error';
						if ((item[i] !== 'password_policy') || displayed_policy === false) {
							errors += '<li>' + transl[i] + ': ' + transl[item[i]];
						}
						if (item[i] == 'password_policy' && displayed_policy == false) {
							errors += '&#160;<span id="password-policy-notice">' + transl.show_password_policy + '.</span>';
							displayed_policy = true;
						}
						errors += '</li>';
					}
				});
				errors += '</ul>';
				dojo.byId ('edit_user_error_block').innerHTML = errors;
				dojo.byId ('edit_user_error_block').style.display = 'block';
				new dijit.Tooltip ({
					connectId: ['password-policy-notice'],
					label: GetAndTranslatePasswordPolicy ()
				});
				underlay.hide ();
			}
			else if (!response.success && response.journal) {
				var journal = '<ul class="journal">';
				dojo.forEach (response.journal, function (item, index, array) {
					var classname = item.status ? 'success' : 'failure';
					if (item.module != 'Mioga2::UserList') {
						return (0);
					}
					if (item.stage) {
						classname += ' ' + item.stage;
					}
					journal += '<li class="' + classname + '">' + item.step + '</li>';
				});
				journal += '</ul>';
				var journal_dialog = new dijit.Dialog({
					title: transl.journal,
					content: journal
				});
				dojo.byId ('edit_user_error_block').innerHTML = '<p>' + transl.operation_rollback + '&#160;<a href="#">' + transl.show_journal + '</a>';
				dojo.query ('a', dojo.byId ('edit_user_error_block')).connect ('onclick', function (evt) {
					journal_dialog.show ();
				});
				dojo.byId ('edit_user_error_block').style.display = 'block';
				underlay.hide ();
			}
			else {
				underlay.hide ();
				dijit.byId("edit_user").hide();
				RefreshGrid();
			}

		},
		error: function(err, ioArgs){
			underlay.hide ();
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
			RefreshGrid();
		}
	});
}

function GetAndTranslatePasswordPolicy () {
	var transl = dojo.i18n.getLocalization("mioga", "colbert");
	var str = '<ul class="password-policy">';

	dojo.xhrGet({
		url: "GetPasswordPolicy.json",
		timeout: 30000, // give up after 3 seconds
		handleAs: "json",
		sync: true,
		load: function(response, ioArgs){
			// console.dir (response);
			dojo.forEach (Array ('pwd_min_length', 'pwd_min_letter', 'pwd_min_digit', 'pwd_min_special', 'pwd_min_chcase'), function (key) {
				str += '<li>' + transl[key] + (lang == 'fr_FR' ? '&#160;' : '') + ': &#160;' + response[key] + '</li>';
			});
		},
		error: function(err, ioArgs){
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
		}
	});

	str += '</ul>';

	return (str);
}

function HandleDeleteUserErrors (errors) {
	var transl = dojo.i18n.getLocalization("mioga", "colbert");
	var str = transl['DeleteUserErrorHeader'] + "\n";
	dojo.forEach (errors, function (err) {
		for (var i in err) {
			str += err[i] + " " + transl[i] + "\n";
		}
	});
	RefreshGrid ();
	alert (str);
}


function CheckUserForDelete (gridId) {
	var grid = dojo.getObject(gridId);
	var ok = true;
	dojo.forEach (grid.selection.getSelected(), function (item) {
		if (item.is_anim_of && item.is_anim_of != 0) {
			ok = false;
		}
	});

	if (!ok) {
		var transl = dojo.i18n.getLocalization("mioga", "colbert");
		alert (transl.UserIsAnimator);
	}

	return (ok);
}


/* -------------------------------------- *
 *            Group functions              *
 * -------------------------------------- */
function compareLabels (a, b) {
	a = unaccent (a.label.toLowerCase ());
	b = unaccent (b.label.toLowerCase ());

	if (a > b) return (1);
	if (a < b) return (-1);
	return (0);
}

function CloseGroupDialog() {
	dijit.byId ('edit_group').hide ();
}

function ClearGroupErrors () {
	// Hide error block
	dojo.byId ('edit_group_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('edit_group')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayGroupEdit (rowid) {
	console.debug('rowid ' + rowid);

	// Show dialog
	dijit.byId ('edit_group').show ();

	// Clear errors
	ClearGroupErrors ();

	// Reset form
	dijit.byId('group_attributes_form').reset ();

	var current_animator_id;
	var current_default_app;

	// Replace {ident} macro with span in titles
	dojo.query ("h1.tabtitle").forEach (function (node) { node.innerHTML = node.innerHTML.replace (/{ident}/, '<span class="group-ident"></span>'); });
	dojo.query ("h1.tabtitle span.group-ident").forEach (function (node) { node.innerHTML = ''; });
	dojo.query ("div.tab").forEach (function (node) { node.title = node.title.replace (/ {ident}/, ''); });

	if (rowid) {
		ShowUnderlay ();

		// Hide skeleton chooser
		dojo.byId ('group-skel-chooser').style.display = 'none';
		// Show default-application chooser
		dojo.byId ('group-default-app-chooser').style.display = 'block';

		dojo.xhrGet({
			url: "GetGroupDetails.json",
			timeout: 30000, // give up after 3 seconds
			content: { rowid:rowid,full_list:1 },
			handleAs: "json",
			load: function(response, ioArgs){
				console.dir (response);

				// Add group name to tab titles
				dojo.query ("h1.tabtitle span.group-ident").forEach (function (node) {
					node.innerHTML = response.group.ident;
				});

				// Set list of users, teams and applications
				var sel = new Array ();
				var all = new Array ();
				dojo.forEach (Array ('user', 'team', 'application'), function (elm) {
					sel[elm] = new Array ();
					all[elm] = new Array ();
					if (response[elm+'s'] && response[elm+'s'][elm]) {
						dojo.forEach (response[elm+'s'][elm], function (item) {
							if (item.selected) {
								sel[elm].push (item);
							}
							all[elm].push (item);
						});
					}
					dijit.byId ('group_'+elm+'s').createStore (all[elm], sel[elm]);
				});

				// Initialize default application chooser datastore
				var defaultAppStore = new dojo.data.ItemFileWriteStore({ data:{identifier:'ident', label:'label', items:[]} });
				// Could not have ItemFileWriteStore / FilteringSelect sort items correctly, so do it by my own...
				all.application.sort (compareLabels);
				sel.application.sort (compareLabels);
				dojo.forEach (all.application, function (item) {
					if (item.selected) {
						defaultAppStore.newItem (item);
					}
					if (item['protected']) {
						dijit.byId ('group_applications').lockItem (item.ident, 'ident');
					}
				});
				dijit.byId ('group_attributes_default_app').set ('store', defaultAppStore);

				// Set form values
				dijit.byId('group_attributes_form').setValues (response.group);
				if (response.group.public_part != 0) dijit.byId('group_attributes_public_part').set ('value', response.group.public_part ? true:false); // This line should not be, but Perl-Generated JSON don't provide true/false but "1"/"0" strings
				if (response.group.history != 0) dijit.byId('group_attributes_history').set ('value', response.group.history ? true:false); // This line should not be, but Perl-Generated JSON don't provide true/false but "1"/"0" strings

				underlay.hide ();
			},
			error: function(err, ioArgs){
				var transl = dojo.i18n.getLocalization("mioga", "colbert");
				var dialog = new dijit.Dialog({
					title: transl['generic_error_title'],
					content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
				});
				dialog.show ();
				// The two lines below are because of a bad rendering with Chromium
				dojo.style(dialog.titleBar, { width: '100%' });
				dojo.style(dialog.containerNode, { width: '100%' });
				underlay.hide ();
			}
		});
	}
	else {
		// Show skeleton chooser
		dojo.byId ('group-skel-chooser').style.display = 'block';

		// Set FilteringSelect initial values
		dijit.byId ('group_attributes_lang').set ('value', lang);
		dijit.byId ('group_attributes_skeleton').store.fetch ({onComplete: function () { var store_data = dijit.byId ('group_attributes_skeleton').store._arrayOfAllItems; console.dir (store_data); dijit.byId ('group_attributes_skeleton').set ('value', store_data[0].file[0]);}});
	}

	dojo.connect (dijit.byId('group_attributes_animator_id'), 'onChange', function () {
		dijit.byId ('group_users').unlockItem (current_animator_id, 'rowid');
		current_animator_id = dijit.byId('group_attributes_animator_id').attr ('value');
		dijit.byId ('group_users').lockItem (current_animator_id, 'rowid');
	});
	dojo.connect (dijit.byId('group_attributes_default_app'), 'onChange', function () {
		dijit.byId ('group_applications').unlockItem (current_default_app, 'ident');
		current_default_app = dijit.byId('group_attributes_default_app').attr ('value');
		dijit.byId ('group_applications').lockItem (current_default_app, 'ident');
	});

	// Set first tab as visible
	dijit.byId ('tabbed_dialog').showFirst ();

	// Ensure dialog is centered
	dijit.byId ('edit_group')._position ();
}

function UpdateGroupSkeletonDetails () {
	var lang = dijit.byId ('group_attributes_lang').get ('value');
	var file = dijit.byId ('group_attributes_skeleton').get ('value');

	if (file && lang) {
		ShowUnderlay ();
		dojo.xhrGet({
			url: "GetSkeletonDetails.json",
			timeout: 30000, // give up after 3 seconds
			content: { type:'group',lang:lang,file:file,full_list:1 },
			handleAs: "json",
			load: function(response, ioArgs){
				console.dir (response);
				dijit.byId('group_attributes_form').setValues (response.attributes);
				if (response.attributes.public_part != 0) dijit.byId('group_attributes_public_part').set ('value', response.attributes.public_part ? true:false); // This line should not be, but Perl-Generated JSON don't provide true/false but "1"/"0" strings
				if (response.attributes.history != 0) dijit.byId('group_attributes_history').set ('value', response.attributes.history ? true:false); // This line should not be, but Perl-Generated JSON don't provide true/false but "1"/"0" strings

				// Set list of groups, teams and applications
				var sel = new Array ();
				var all = new Array ();
				dojo.forEach (Array ('user', 'team', 'application'), function (elm) {
					sel[elm] = new Array ();
					all[elm] = new Array ();
					if (response[elm+'s'] && response[elm+'s'][elm]) {
						dojo.forEach (response[elm+'s'][elm], function (item) {
							if (item.selected) {
								sel[elm].push (item);
							}
							all[elm].push (item);
						});
					}
					dijit.byId ('group_'+elm+'s').createStore (all[elm], sel[elm]);
				});

				// Initialize default application chooser datastore
				var defaultAppStore = new dojo.data.ItemFileWriteStore({ data:{identifier:'ident', label:'label', items:[]} });
				// Could not have ItemFileWriteStore / FilteringSelect sort items correctly, so do it by my own...
				all.application.sort (compareLabels);
				sel.application.sort (compareLabels);
				dojo.forEach (all.application, function (item) {
					if (item.selected) {
						defaultAppStore.newItem (item);
					}
					if (item['protected']) {
						dijit.byId ('group_applications').lockItem (item.ident, 'ident');
					}
				});
				dijit.byId ('group_attributes_default_app').set ('store', defaultAppStore);
				dijit.byId ('group_attributes_default_app').set ('value', response.attributes.default_app);

				// Set default animator
				dijit.byId ('group_attributes_animator_id').set ('value', response.attributes.animator_id);

				underlay.hide ();
			},
			error: function(err, ioArgs){
				underlay.hide ();
				var transl = dojo.i18n.getLocalization("mioga", "colbert");
				var dialog = new dijit.Dialog({
					title: transl['generic_error_title'],
					content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
				});
				dialog.show ();
				// The two lines below are because of a bad rendering with Chromium
				dojo.style(dialog.titleBar, { width: '100%' });
				dojo.style(dialog.containerNode, { width: '100%' });
			}
		});
	}
	else {
		// FilteringSelect values not initialized, plan a new call to function
		setTimeout (UpdateGroupSkeletonDetails, 1000);
	}
}

function ValidateGroup() {
	console.debug('Validate called');
	var transl = dojo.i18n.getLocalization("mioga", "colbert");

	// Clear errors
	ClearGroupErrors ();

	// Get form data
	var form = dijit.byId('group_attributes_form');
	console.debug("form " + form);
	var form_data = form.get ("value");

	// Delete rowid if empty string (adding)
	if (form_data.rowid == "") delete form_data.rowid;

	console.debug("form_data " + form_data);

	// Get groups, teams and applications
	dojo.forEach (Array ('users', 'teams', 'applications'), function (elm) {
		form_data[elm] = new Array ();
		dojo.forEach (dijit.byId ('group_' + elm).getList (), function (item) {
			form_data[elm].push (item.ident);
		});
	});

	// Translate checkbox values
	if (form_data.public_part[0]) {
		form_data.public_part = 1;
	}
	else {
		form_data.public_part = 0;
	}
	if (form_data.history[0]) {
		form_data.history = 1;
	}
	else {
		form_data.history = 0;
	}

	console.dir(form_data);

	// Display loading underlay
	ShowUnderlay ();

	dojo.xhrPost({
		url: "SetGroup.json",
		timeout: 30000, // give up after 3 seconds
		content: form_data,
		handleAs:"json",
		load: function(response, ioArgs){
			console.debug("load function  response " + response);
			console.dir(response);
			if (response.errors) {
				var errors = '<ul>';
				dojo.forEach(response.errors, function(item, index, array) {
					for (var i in item) {
						console.log (i + ": " + item[i]);
						if (i != 'global') {
							dojo.byId ('group_attributes_'+i+'_label').className = 'error';
							errors += '<li>' + transl[i] + ': ' + transl[item[i]] + '</li>';
						}
						else {
							var msg = (transl[item[i]] ? transl[item[i]] : item[i]);
							errors += '<li>' + msg + '</li>';
						}
					}
				});
				errors += '</ul>';
				dojo.byId ('edit_group_error_block').innerHTML = errors;
				dojo.byId ('edit_group_error_block').style.display = 'block';
				underlay.hide ();
			}
			else if (!response.success && response.journal) {
				var journal = '<ul class="journal">';
				dojo.forEach (response.journal, function (item, index, array) {
					var classname = item.status ? 'success' : 'failure';
					if (item.module != 'Mioga2::GroupList') {
						return (0);
					}
					if (item.stage) {
						classname += ' ' + item.stage;
					}
					journal += '<li class="' + classname + '">' + item.step + '</li>';
				});
				journal += '</ul>';
				var journal_dialog = new dijit.Dialog({
					title: transl.journal,
					content: journal
				});
				dojo.byId ('edit_group_error_block').innerHTML = '<p>' + transl.operation_rollback + '&#160;<a href="#">' + transl.show_journal + '</a>';
				dojo.query ('a', dojo.byId ('edit_group_error_block')).connect ('onclick', function (evt) {
					journal_dialog.show ();
				});
				dojo.byId ('edit_group_error_block').style.display = 'block';
				underlay.hide ();
			}
			else {
				RefreshGrid();
				dijit.byId("edit_group").hide();
				underlay.hide ();
			}

		},
		error: function(err, ioArgs){
			underlay.hide ();
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
			RefreshGrid();
		}
	});
}

// Check instance default group is not being deleted
function CheckGroupForDelete (gridId) {
	var grid = dojo.getObject(gridId);
	var ok = true;
	dojo.forEach (grid.selection.getSelected(), function (item) {
		if (item.rowid == default_group_id) {
			ok = false;
		}
	});

	if (!ok) {
		var transl = dojo.i18n.getLocalization("mioga", "colbert");
		alert (transl.GroupDelMsg);
	}

	return (ok);
}

function GroupDeleteErrorCallback (errors) {
	var msg = '';
	for (i=0; i<errors.length; i++) {
		for (key in errors[i]) {
			msg += errors[i][key];
		}
	}
	alert (msg);
}


/* -------------------------------------- *
 *            Team functions              *
 * -------------------------------------- */

function CloseTeamDialog() {
	dijit.byId ('edit_team').hide ();
}

function ClearTeamErrors () {
	// Hide error block
	dojo.byId ('edit_team_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('edit_team')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayTeamEdit (rowid) {
	console.debug('rowid ' + rowid);

	// Clear errors
	ClearTeamErrors ();

	// Reset form
	dijit.byId('team_attributes_form').reset ();

	// Replace {ident} macro with span in titles
	dojo.query ("h1.tabtitle").forEach (function (node) { node.innerHTML = node.innerHTML.replace (/{ident}/, '<span class="team-ident"></span>'); });
	dojo.query ("h1.tabtitle span.team-ident").forEach (function (node) { node.innerHTML = ''; });
	dojo.query ("div.tab").forEach (function (node) { node.title = node.title.replace (/ {ident}/, ''); });

	if (rowid) {
		ShowUnderlay ();

		dojo.xhrGet({
			url: "GetTeamDetails.json",
			timeout: 30000, // give up after 3 seconds
			content: { rowid:rowid,full_list:1 },
			handleAs: "json",
			load: function(response, ioArgs){
				console.dir (response);

				// Add team ident to titles
				dojo.query ("h1.tabtitle span.team-ident").forEach (function (node) { node.innerHTML = response.team.ident; });

				// Set list of users, teams and applications
				var sel = new Array ();
				var all = new Array ();
				dojo.forEach (Array ('user', 'group'), function (elm) {
					sel[elm] = new Array ();
					all[elm] = new Array ();
					if (response[elm+'s'] && response[elm+'s'][elm]) {
						dojo.forEach (response[elm+'s'][elm], function (item) {
							if (item.selected) {
								sel[elm].push (item);
							}
							all[elm].push (item);
						});
					}
					dijit.byId ('team_'+elm+'s').createStore (all[elm], sel[elm]);
				});
				underlay.hide ();

				// Set form values
				dijit.byId('team_attributes_form').set ('value', response.team);
			},
			error: function(err, ioArgs){
				var transl = dojo.i18n.getLocalization("mioga", "colbert");
				var dialog = new dijit.Dialog({
					title: transl['generic_error_title'],
					content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
				});
				dialog.show ();
				// The two lines below are because of a bad rendering with Chromium
				dojo.style(dialog.titleBar, { width: '100%' });
				dojo.style(dialog.containerNode, { width: '100%' });
				underlay.hide ();
			}
		});
	}
	else {
		// Initialize SelectLists content
		dojo.forEach (Array ('user', 'group'), function (elm) {
			console.log (elm);
			var url = 'Get' + elm.substr (0, 1).toUpperCase () + elm.substr (1) + 's.json';
			console.log (url);
			dojo.xhrGet({
				url: url,
				timeout: 30000, // give up after 3 seconds
				content: {  },
				handleAs: "json",
				load: function(response, ioArgs){
					dijit.byId ('team_' + elm + 's').createStore (response[elm], Array ());
				},
				error: function(err, ioArgs){
					var transl = dojo.i18n.getLocalization("mioga", "colbert");
					var dialog = new dijit.Dialog({
						title: transl['generic_error_title'],
						content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
					});
					dialog.show ();
					// The two lines below are because of a bad rendering with Chromium
					dojo.style(dialog.titleBar, { width: '100%' });
					dojo.style(dialog.containerNode, { width: '100%' });
				}
			});
		});
	}

	// Show dialog
	dijit.byId ('edit_team').show ();

	// Set first tab as visible
	dijit.byId ('tabbed_dialog').showFirst ();

	// Ensure dialog is centered
	dijit.byId ('edit_team')._position ();
}

function ValidateTeam() {
	console.debug('Validate called');
	var transl = dojo.i18n.getLocalization("mioga", "colbert");

	// Clear errors
	ClearTeamErrors ();

	// Get form data
	var form = dijit.byId('team_attributes_form');
	console.debug("form " + form);
	var form_data = form.get ("value");

	// Delete rowid if empty string (adding)
	if (form_data.rowid == "") delete form_data.rowid;

	console.debug("form_data " + form_data);

	// Get users and groups
	dojo.forEach (Array ('users', 'groups'), function (elm) {
		form_data[elm] = new Array ();
		dojo.forEach (dijit.byId ('team_' + elm).getList (), function (item) {
			form_data[elm].push (item.ident);
		});
	});

	console.dir(form_data);

	// Display loading underlay
	ShowUnderlay ();

	dojo.xhrPost({
		url: "SetTeam.json",
		timeout: 30000, // give up after 3 seconds
		content: form_data,
		handleAs:"json",
		load: function(response, ioArgs){
			console.debug("load function  response " + response);
			console.dir(response);
			if (response.errors) {
				var errors = '<ul>';
				dojo.forEach(response.errors, function(item, index, array) {
					for (var i in item) {
						console.log (i + ": " + item[i]);
						dojo.byId ('team_attributes_'+i+'_label').className = 'error';
						errors += '<li>' + transl[i] + ': ' + transl[item[i]] + '</li>';
					}
				});
				errors += '</ul>';
				dojo.byId ('edit_team_error_block').innerHTML = errors;
				dojo.byId ('edit_team_error_block').style.display = 'block';
				underlay.hide ();
			}
			else if (!response.success && response.journal) {
				var journal = '<ul class="journal">';
				dojo.forEach (response.journal, function (item, index, array) {
					var classname = item.status ? 'success' : 'failure';
					if (item.module != 'Mioga2::TeamList') {
						return (0);
					}
					if (item.stage) {
						classname += ' ' + item.stage;
					}
					journal += '<li class="' + classname + '">' + item.step + '</li>';
				});
				journal += '</ul>';
				var journal_dialog = new dijit.Dialog({
					title: transl.journal,
					content: journal
				});
				dojo.byId ('edit_team_error_block').innerHTML = '<p>' + transl.operation_rollback + '&#160;<a href="#">' + transl.show_journal + '</a>';
				dojo.query ('a', dojo.byId ('edit_team_error_block')).connect ('onclick', function (evt) {
					journal_dialog.show ();
				});
				dojo.byId ('edit_team_error_block').style.display = 'block';
				underlay.hide ();
			}
			else {
				underlay.hide ();
				dijit.byId("edit_team").hide();
				RefreshGrid();
			}

		},
		error: function(err, ioArgs){
			underlay.hide ();
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
			RefreshGrid();
		}
	});
}

function ClearUserTeamImportErrors () {
	// Hide error block
	dojo.byId ('import_user_teams_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('import_user_teams')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayUserTeamImportDialog () {
	// Clear errors
	ClearUserTeamImportErrors ();

	// Show dialog
	dijit.byId ('import_user_teams').show ();
}

function UserTeamImportCallback (err) {
	var underlay = dijit.byId ('underlay');
	if (underlay) {
		underlay.hide ();
	}
	if (err) {
		dojo.byId ('import_user_teams_error_block').style.display = 'block';
		dojo.byId ('import_user_teams_error_block_content').innerHTML = err;
	}
	else {
		dijit.byId ('import_user_teams').hide ();
		RefreshGrid ();
	}
}

function ClearTeamGroupImportErrors () {
	// Hide error block
	dojo.byId ('import_team_groups_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('import_team_groups')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayTeamGroupImportDialog () {
	// Clear errors
	ClearTeamGroupImportErrors ();

	// Show dialog
	dijit.byId ('import_team_groups').show ();
}

function TeamGroupImportCallback (err) {
	var underlay = dijit.byId ('underlay');
	if (underlay) {
		underlay.hide ();
	}
	if (err) {
		dojo.byId ('import_team_groups_error_block').style.display = 'block';
		dojo.byId ('import_team_groups_error_block_content').innerHTML = err;
	}
	else {
		dijit.byId ('import_team_groups').hide ();
		RefreshGrid ();
	}
}


/* -------------------------------------- *
 *       External Mioga functions         *
 * -------------------------------------- */

function CloseExtMiogaDialog () {
	dijit.byId ('add_ext_mioga').hide ();
}

function ClearExtMiogaErrors () {
	// Hide error block
	dojo.byId ('add_ext_mioga_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('add_ext_mioga')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayExtMiogaDialog () {
	// Clear errors
	ClearExtMiogaErrors ();

	// Show dialog
	dijit.byId ('add_ext_mioga').show ();
}

// Check no users come from this external Mioga
function CheckExtMiogaForDelete (gridId) {
	var grid = dojo.getObject(gridId);
	var ok = true;
	dojo.forEach (grid.selection.getSelected(), function (item) {
		dojo.xhrGet({
			url: "GetExternalMiogaDetails.json",
			timeout: 30000, // give up after 3 seconds
			content: { rowid:item.rowid },
			handleAs: "json",
			sync: true,
			load: function(response, ioArgs){
				console.dir (response);
				if (response.external_mioga.nb_users != 0) {
					ok = false;
				}
			},
			error: function(err, ioArgs){
				var transl = dojo.i18n.getLocalization("mioga", "colbert");
				var dialog = new dijit.Dialog({
					title: transl['generic_error_title'],
					content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
				});
				dialog.show ();
				// The two lines below are because of a bad rendering with Chromium
				dojo.style(dialog.titleBar, { width: '100%' });
				dojo.style(dialog.containerNode, { width: '100%' });
			}
		});
	});

	if (!ok) {
		var transl = dojo.i18n.getLocalization("mioga", "colbert");
		alert (transl.ExtMiogaDelMsg);
	}

	return (ok);
}

function ExtMiogaCreateCallback (err) {
	if (err) {
		dojo.byId ('add_ext_mioga_error_block').style.display = 'block';
		dojo.byId ('add_ext_mioga_error_block_content').innerHTML = err;
	}
	else {
		CloseExtMiogaDialog ();
		RefreshGrid ();
	}
}

function DisplayExternalUserInviteDialog () {
	dijit.byId ('external_user_server').set ('value', '');
	dijit.byId ('external_user_skeleton').set ('value', '50-standard.xml');
	dojo.byId ('external-users-container').style.display = 'none';
	dojo.byId ('external_user_invite_error_block').style.display = 'none';
	dijit.byId ('external_user_invite').show ();
}

function CloseExternalUserInviteDialog () {
	dijit.byId ('external_user_invite').hide ();
}

function UpdateExternalUserList () {
	var ext_mioga_id = dijit.byId ('external_user_server').value;

	ShowUnderlay ();
	dojo.xhrGet({
		url: "GetExternalMiogaUsers.json",
		timeout: 30000, // give up after 3 seconds
		content: { rowid:ext_mioga_id },
		handleAs: "json",
		load: function(response, ioArgs){
			// Display SelectList
			dojo.byId ('external-users-container').style.display = 'block';
			console.dir (response);

			var sel = new Array ();
			var all = new Array ();
			dojo.forEach (response.user, function (user) {
				if (!user.selected) {
					all.push (user);
				}
			});

			// Populate SelectList if data, hide it otherwise
			if (all.length) {
				dijit.byId ('external_users').createStore (all, sel);
			}
			else {
				dojo.byId ('external_users').style.display = 'none';
			}
			underlay.hide ();
		},
		error: function(err, ioArgs){
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
			underlay.hide ();
		}
	});
}

function ValidateExternalUserInvite () {
	console.log ("validate ext invite");
	var transl = dojo.i18n.getLocalization("mioga", "colbert");

	var form = dijit.byId('external_user_invite_form');
	var form_data = form.get ("value");

	form_data.rowid = new Array ();
	dojo.forEach (dijit.byId ('external_users').getList (), function (item) {
		form_data.rowid.push (item.rowid);
	});

	console.dir (form_data);

	// Display loading underlay
	ShowUnderlay ();

	dojo.xhrPost({
		url: "InviteExternalUsers.json",
		timeout: 30000, // give up after 3 seconds
		content: form_data,
		handleAs:"json",
		load: function(response, ioArgs){
			console.debug("load function  response " + response);
			console.dir(response);
			if (response.errors) {
				var errors = '<ul>';
				dojo.forEach(response.errors, function(item, index, array) {
					for (var i in item) {
						console.log (i + ": " + item[i]);
						errors += '<li>' + transl[i] + ': ' + transl[item[i]] + '</li>';
					}
				});
				errors += '</ul>';
				dojo.byId ('external_user_invite_error_block').innerHTML = errors;
				dojo.byId ('external_user_invite_error_block').style.display = 'block';
				underlay.hide ();
			}
			else {
				underlay.hide ();
				dijit.byId("external_user_invite").hide();
				RefreshGrid();
			}

		},
		error: function(err, ioArgs){
			underlay.hide ();
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
			RefreshGrid();
		}
	});
}


/* -------------------------------------- *
 *         User import functions          *
 * -------------------------------------- */

function CloseUserImportDialog () {
	dijit.byId ('import_users').hide ();
}

function ClearUserImportErrors () {
	// Hide error block
	dojo.byId ('import_users_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('import_users')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayUserImportDialog () {
	// Clear errors
	ClearUserImportErrors ();

	// Show dialog
	dijit.byId ('import_users').show ();
}

function UserImportCallback (err) {
	var underlay = dijit.byId ('underlay');
	if (underlay) {
		underlay.hide ();
	}
	if (dojo.query ('ul', err).length) {
		dojo.byId ('import_users_error_block').style.display = 'block';
		dojo.byId ('import_users_error_block_content').innerHTML = err.innerHTML;
	}
	else {
		CloseUserImportDialog ();
		RefreshGrid ();
	}
}


/* -------------------------------------- *
 *        Task categories functions       *
 * -------------------------------------- */

function CloseTaskCategoryDialog () {
	dijit.byId ('task_category').hide ();
}

function ClearTaskCategoryErrors () {
	// Hide error block
	dojo.byId ('task_category_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('task_category')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayTaskCategoryDialog (rowid) {
	// Clear errors
	ClearTaskCategoryErrors ();

	if (rowid) {
		dojo.xhrGet({
			url: "GetTaskCategoryDetails.json",
			timeout: 30000, // give up after 3 seconds
			content: { rowid:rowid },
			handleAs: "json",
			load: function(response, ioArgs){
				console.dir (response);
				dijit.byId('task_category_form').setValues (response.category);
				dijit.byId ('bgcolor').set ('value', response.category.bgcolor);
				dijit.byId ('fgcolor').set ('value', response.category.fgcolor);
				if (response.category.private != 0) dijit.byId('task_category_private').set ('value', response.category.private);
			},
			error: function(err, ioArgs){
				var transl = dojo.i18n.getLocalization("mioga", "colbert");
				var dialog = new dijit.Dialog({
					title: transl['generic_error_title'],
					content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
				});
				dialog.show ();
				// The two lines below are because of a bad rendering with Chromium
				dojo.style(dialog.titleBar, { width: '100%' });
				dojo.style(dialog.containerNode, { width: '100%' });
			}
		});
	}
	else {
		// Reset form
		dijit.byId('task_category_form').reset ();
		dijit.byId ('bgcolor').set ('value', '');
		dijit.byId ('fgcolor').set ('value', '');
	}


	// Show dialog
	dijit.byId ('task_category').show ();
}

function ValidateTaskCategory () {
	console.debug('Validate called');
	var transl = dojo.i18n.getLocalization("mioga", "colbert");

	// Clear errors
	ClearTaskCategoryErrors ();

	// Get form data
	var form = dijit.byId('task_category_form');
	console.debug("form " + form);
	var form_data = form.get ("value");

	// Delete rowid if empty string (adding)
	if (form_data.rowid == "") delete form_data.rowid;

	// Transform checkboxes values from array to flag
	if (form_data.private[0]) {
		form_data.private = 1;
	}
	else {
		form_data.private = 0;
	}

	form_data.bgcolor = dijit.byId ('bgcolor').get ('value');
	form_data.fgcolor = dijit.byId ('fgcolor').get ('value');

	console.dir(form_data);

	// Display loading underlay
	ShowUnderlay ();

	dojo.xhrPost({
		url: "SetTaskCategory.json",
		timeout: 30000, // give up after 3 seconds
		content: form_data,
		handleAs:"json",
		load: function(response, ioArgs){
			console.debug("load function  response " + response);
			console.dir(response);
			if (response.errors) {
				var errors = '<ul>';
				dojo.forEach(response.errors, function(item, index, array) {
					for (var i in item) {
						console.log (i + ": " + item[i]);
						dojo.byId ('task_category_'+i+'_label').className = 'error';
						errors += '<li>' + transl[i] + ': ' + transl[item[i]] + '</li>';
					}
				});
				errors += '</ul>';
				dojo.byId ('task_category_error_block').innerHTML = errors;
				dojo.byId ('task_category_error_block').style.display = 'block';
				underlay.hide ();
			}
			else {
				underlay.hide ();
				dijit.byId("task_category").hide();
				RefreshGrid();
			}

		},
		error: function(err, ioArgs){
			underlay.hide ();
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
			RefreshGrid();
		}
	});
}

// Check no task is associated to categories do delete
function CheckTaskCategoryForDelete (gridId) {
	var grid = dojo.getObject(gridId);
	var ok = true;
	dojo.forEach (grid.selection.getSelected(), function (item) {
		console.log (item.nb_tasks[0]);
		if (item.nb_tasks[0] != 0) {
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			alert (transl.TaskCatDelMsg);
			ok = false;
			return (ok);
		}
	});

	return (ok);
}


/* -------------------------------------- *
 *           Resources functions          *
 * -------------------------------------- */

function CloseResourceDialog () {
	dijit.byId ('resource').hide ();
}

function ClearResourceErrors () {
	// Hide error block
	dojo.byId ('resource_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('resource')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayResourceDialog (rowid) {
	// Clear errors
	ClearResourceErrors ();

	if (rowid) {
		dojo.xhrGet({
			url: "GetResourceDetails.json",
			timeout: 30000, // give up after 3 seconds
			content: { rowid:rowid },
			handleAs: "json",
			load: function(response, ioArgs){
				dijit.byId('resource_form').setValues (response.resource);
				dijit.byId ('resource_ident').set ('disabled', true);
			},
			error: function(err, ioArgs){
				var transl = dojo.i18n.getLocalization("mioga", "colbert");
				var dialog = new dijit.Dialog({
					title: transl['generic_error_title'],
					content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
				});
				dialog.show ();
				// The two lines below are because of a bad rendering with Chromium
				dojo.style(dialog.titleBar, { width: '100%' });
				dojo.style(dialog.containerNode, { width: '100%' });
			}
		});
	}
	else {
		// Reset form
		dijit.byId('resource_form').reset ();
		dijit.byId ('resource_ident').set ('disabled', false);
	}


	// Show dialog
	dijit.byId ('resource').show ();
}

function ValidateResource () {
	console.debug('Validate called');
	var transl = dojo.i18n.getLocalization("mioga", "colbert");

	// Clear errors
	ClearResourceErrors ();

	// Get form data
	var form = dijit.byId('resource_form');
	console.debug("form " + form);
	var form_data = form.get ("value");

	// Delete rowid if empty string (adding)
	if (form_data.rowid == "") delete form_data.rowid;

	// Force value of ident (at update time, ident is disabled, so not parsed by form.get ('value'))
	form_data.ident = dijit.byId ('resource_ident').get ('value');

	console.dir(form_data);

	// Display loading underlay
	ShowUnderlay ();

	dojo.xhrPost({
		url: "SetResource.json",
		timeout: 30000, // give up after 3 seconds
		content: form_data,
		handleAs:"json",
		load: function(response, ioArgs){
			console.debug("load function  response " + response);
			console.dir(response);
			if (response.errors) {
				var errors = '<ul>';
				dojo.forEach(response.errors, function(item, index, array) {
					for (var i in item) {
						console.log (i + ": " + item[i]);
						dojo.byId ('resource_'+i+'_label').className = 'error';
						errors += '<li>' + transl[i] + ': ' + transl[item[i]] + '</li>';
					}
				});
				errors += '</ul>';
				dojo.byId ('resource_error_block').innerHTML = errors;
				dojo.byId ('resource_error_block').style.display = 'block';
				underlay.hide ();
			}
			else if (!response.success && response.journal) {
				var journal = '<ul class="journal">';
				dojo.forEach (response.journal, function (item, index, array) {
					var classname = item.status ? 'success' : 'failure';
					if (item.module != 'Mioga2::ResourceList') {
						return (0);
					}
					if (item.stage) {
						classname += ' ' + item.stage;
					}
					journal += '<li class="' + classname + '">' + item.step + '</li>';
				});
				journal += '</ul>';
				var journal_dialog = new dijit.Dialog({
					title: transl.journal,
					content: journal
				});
				dojo.byId ('edit_resource_error_block').innerHTML = '<p>' + transl.operation_rollback + '&#160;<a href="#">' + transl.show_journal + '</a>';
				dojo.query ('a', dojo.byId ('edit_resource_error_block')).connect ('onclick', function (evt) {
					journal_dialog.show ();
				});
				dojo.byId ('edit_resource_error_block').style.display = 'block';
				underlay.hide ();
			}
			else {
				underlay.hide ();
				dijit.byId("resource").hide();
				RefreshGrid();
			}

		},
		error: function(err, ioArgs){
			underlay.hide ();
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
			RefreshGrid();
		}
	});
}


/* -------------------------------------- *
 *           Themes functions          *
 * -------------------------------------- */

function CloseThemeDialog () {
	dijit.byId ('theme').hide ();
}

function ClearThemeErrors () {
	// Hide error block
	dojo.byId ('theme_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('theme')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayThemeDialog (rowid) {
	// Clear errors
	ClearThemeErrors ();

	if (rowid) {
		dojo.xhrGet({
			url: "GetThemeDetails.json",
			timeout: 30000, // give up after 3 seconds
			content: { rowid:rowid },
			handleAs: "json",
			load: function(response, ioArgs){
				dijit.byId('theme_form').setValues (response.theme);
				dijit.byId ('theme_ident').set ('disabled', true);
			},
			error: function(err, ioArgs){
				var transl = dojo.i18n.getLocalization("mioga", "colbert");
				var dialog = new dijit.Dialog({
					title: transl['generic_error_title'],
					content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
				});
				dialog.show ();
				// The two lines below are because of a bad rendering with Chromium
				dojo.style(dialog.titleBar, { width: '100%' });
				dojo.style(dialog.containerNode, { width: '100%' });
			}
		});
	}
	else {
		// Reset form
		dijit.byId('theme_form').reset ();
		dijit.byId ('theme_ident').set ('disabled', false);
	}


	// Show dialog
	dijit.byId ('theme').show ();
}

function ValidateTheme () {
	console.debug('Validate called');
	var transl = dojo.i18n.getLocalization("mioga", "colbert");

	// Clear errors
	ClearThemeErrors ();

	// Get form data
	var form = dijit.byId('theme_form');
	console.debug("form " + form);
	var form_data = form.get ("value");

	// Delete rowid if empty string (adding)
	if (form_data.rowid == "") delete form_data.rowid;

	// Force value of ident (at update time, ident is disabled, so not parsed by form.get ('value'))
	form_data.ident = dijit.byId ('theme_ident').get ('value');

	console.dir(form_data);

	// Display loading underlay
	ShowUnderlay ();

	dojo.xhrPost({
		url: "SetTheme.json",
		content: form_data,
		handleAs:"json",
		load: function(response, ioArgs){
			console.debug("load function  response " + response);
			console.dir(response);
			if (response.errors) {
				var errors = '<ul>';
				dojo.forEach(response.errors, function(item, index, array) {
					for (var i in item) {
						console.log (i + ": " + item[i]);
						dojo.byId ('theme_'+i+'_label').className = 'error';
						errors += '<li>' + transl[i] + ': ' + transl[item[i]] + '</li>';
					}
				});
				errors += '</ul>';
				dojo.byId ('theme_error_block').innerHTML = errors;
				dojo.byId ('theme_error_block').style.display = 'block';
				underlay.hide ();
			}
			else {
				underlay.hide ();
				dijit.byId("theme").hide();
				RefreshGrid();
			}

		},
		error: function(err, ioArgs){
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			underlay.hide ();
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
			RefreshGrid();
		}
	});
}

function CheckThemeForDelete (gridId) {
	var grid = dojo.getObject(gridId);
	var ok = true;
	var reason = '';
	dojo.forEach (grid.selection.getSelected(), function (item) {
		dojo.xhrGet({
			url: "GetThemeDetails.json",
			timeout: 30000, // give up after 3 seconds
			content: { rowid:item.rowid },
			handleAs: "json",
			sync: true,
			load: function(response, ioArgs){
				console.dir (response);
				if (response.theme.nb_groups != 0) {
					ok = false;
					reason = 'used';
				}
				else if (response.theme.is_default) {
					ok = false;
					reason = 'default';
				}
			},
			error: function(err, ioArgs){
				var transl = dojo.i18n.getLocalization("mioga", "colbert");
				var dialog = new dijit.Dialog({
					title: transl['generic_error_title'],
					content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
				});
				dialog.show ();
				// The two lines below are because of a bad rendering with Chromium
				dojo.style(dialog.titleBar, { width: '100%' });
				dojo.style(dialog.containerNode, { width: '100%' });
			}
		});
	});

	if (!ok) {
		var transl = dojo.i18n.getLocalization("mioga", "colbert");
		if (reason == 'used') {
			alert (transl.ThemeUsedMsg);
		}
		else if (reason == 'default') {
			alert (transl.ThemeDefaultMsg);
		}
	}

	return (ok);
}


/* -------------------------------------- *
 *           Skeletons functions          *
 * -------------------------------------- */

function CloseSkeletonsUploadDialog () {
	dijit.byId ('upload_skeletons').hide ();
}

function ClearSkeletonsUploadErrors () {
	// Hide error block
	dojo.byId ('upload_skeletons_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('skeletons_dialog')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplaySkeletonsUploadDialog () {
	// Clear errors
	ClearSkeletonsUploadErrors ();

	// Show dialog
	dijit.byId ('upload_skeletons').show ();
}

function UploadSkeletonsCallback (err) {
	var underlay = dijit.byId ('underlay');
	if (underlay) {
		underlay.hide ();
	}
	if (err) {
		dojo.byId ('upload_skeletons_error_block').style.display = 'block';
		dojo.byId ('upload_skeletons_error_block_content').innerHTML = err;
	}
	else {
		CloseSkeletonsUploadDialog ();
	}
}


/* -------------------------------------- *
 *           PasswordPolicy functions          *
 * -------------------------------------- */

function ClosePasswordPolicyDialog () {
	dijit.byId ('password_policy_dialog').hide ();
}

function ClearPasswordPolicyErrors () {
	// Hide error block
	dojo.byId ('password_policy_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('password_policy_dialog')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayPasswordPolicyDialog () {
	// Clear errors
	ClearPasswordPolicyErrors ();

	dojo.xhrGet({
		url: "GetPasswordPolicy.json",
		timeout: 30000, // give up after 3 seconds
		handleAs: "json",
		sync: true,
		load: function(response, ioArgs){
			// console.dir (response);
			dijit.byId('password_policy_form').setValues (response);
			if (response.use_secret_question != 0) dijit.byId('instance_use_secret_question').set ('value', response.use_secret_question);

			// Show dialog
			dijit.byId ('password_policy_dialog').show ();
		},
		error: function(err, ioArgs){
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
		}
	});
}

function ValidatePasswordPolicy () {
	var form_data = dijit.byId('password_policy_form').get ('value');
	if (form_data.use_secret_question[0]) {
		form_data.use_secret_question = 1;
	}
	else {
		form_data.use_secret_question = 0;
	}

	// Display loading underlay
	ShowUnderlay ();

	dojo.xhrPost({
		url: "SetPasswordPolicy.json",
		timeout: 30000, // give up after 3 seconds
		content: form_data,
		handleAs:"json",
		load: function(response, ioArgs){
			if (response.errors) {
				var transl = dojo.i18n.getLocalization("mioga", "colbert");
				var errors = '<ul>';
				var displayed_policy = false;
				dojo.forEach(response.errors, function(item, index, array) {
					for (var i in item) {
						console.log (i + ": " + item[i]);
						dojo.byId (i+'_label').className = 'error';
						errors += '<li>' + transl[i] + ': ' + transl[item[i]] + '</li>';
					}
				});
				errors += '</ul>';
				dojo.byId ('password_policy_error_block').innerHTML = errors;
				dojo.byId ('password_policy_error_block').style.display = 'block';
				underlay.hide ();
			}
			else {
				underlay.hide ();
				dijit.byId("password_policy_dialog").hide();
			}

		},
		error: function(err, ioArgs){
			underlay.hide ();
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
		}
	});
}


/* -------------------------------------- *
 *         Applications functions         *
 * -------------------------------------- */

function CloseApplicationDialog () {
	dijit.byId ('edit_application').hide ();
}

function DisplayApplicationDialog (rowid) {

	// Replace {ident} macro with span in titles
	dojo.query ("h1.tabtitle").forEach (function (node) { node.innerHTML = node.innerHTML.replace (/{ident}/, '<span class="application-ident"></span>'); });
	dojo.query ("h1.tabtitle span.application-ident").forEach (function (node) { node.innerHTML = ''; });
	dojo.query ("div.tab").forEach (function (node) { node.title = node.title.replace (/ {ident}/, ''); });

	dojo.xhrGet({
		url: "GetApplicationDetails.json",
		timeout: 30000, // give up after 3 seconds
		content: { rowid:rowid,full_list:1 },
		handleAs: "json",
		sync: true,
		load: function(response, ioArgs){
		console.log ("response: ");
		console.dir (response);
			dojo.query ("h1.tabtitle span.application-ident").forEach (function (node) { node.innerHTML = response.application.label; });
			dijit.byId('application_attributes_form').setValues (response.application);
			// Set list of groups and users
			var sel = new Array ();
			var all = new Array ();
			dojo.forEach (Array ('group', 'user'), function (elm) {
				sel[elm] = new Array ();
				all[elm] = new Array ();
				if (response[elm+'s'] && response[elm+'s'][elm]) {
					dojo.forEach (response[elm+'s'][elm], function (item) {
						if (item.selected) {
							sel[elm].push (item);
						}
						all[elm].push (item);
					});
				}
				else {
					console.log ('Hide application-' + elm + 's-tab');
					dijit.byId ('tabbed_dialog').disable ('application-' + elm + 's-tab');
				}
				dijit.byId ('application_attributes_'+elm+'s').createStore (all[elm], sel[elm]);
			});
		},
		error: function(err, ioArgs){
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
		}
	});

	// Show dialog
	dijit.byId ('edit_application').show ();

	// Set first tab as visible
	dijit.byId ('tabbed_dialog').showFirst ();

	// Ensure dialog is centered
	dijit.byId ('edit_application')._position ();
}

function ValidateApplication () {
	console.debug('Validate called');
	var transl = dojo.i18n.getLocalization("mioga", "colbert");

	// Get form data
	var form = dijit.byId('application_attributes_form');
	var form_data = form.get ("value");

	if (form_data.rowid) {
		// Get groups, teams and applications
		dojo.forEach (Array ('groups', 'users'), function (elm) {
			form_data[elm] = new Array ();
			dojo.forEach (dijit.byId ('application_attributes_' + elm).getList (), function (item) {
				form_data[elm].push (item.ident);
			});
		});
	}

	console.debug("form_data ");
	console.dir(form_data);

	// Display loading underlay
	ShowUnderlay ();

	dojo.xhrPost({
		url: "SetApplication.json",
		timeout: 30000, // give up after 3 seconds
		content: form_data,
		handleAs:"json",
		load: function(response, ioArgs){
			console.debug("load function  response " + response);
			console.dir(response);
			if (response.errors) {
				var errors = '<ul>';
				dojo.forEach(response.errors, function(item, index, array) {
					for (var i in item) {
						console.log (i + ": " + item[i]);
						dojo.byId ('application_attributes_'+i+'_label').className = 'error';
						errors += '<li>' + transl[i] + ': ' + transl[item[i]] + '</li>';
					}
				});
				errors += '</ul>';
				underlay.hide ();
			}
			else {
				underlay.hide ();
				dijit.byId("edit_application").hide();
				RefreshGrid();
			}

		},
		error: function(err, ioArgs){
			underlay.hide ();
			var transl = dojo.i18n.getLocalization("mioga", "colbert");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
			RefreshGrid();
		}
	});
}

function SelectAllUsersOrGroups (listId) {
	console.log ("list ID: " + listId);
	var list = dijit.byId (listId);
	console.dir (list);
	dojo.forEach (list.cont.childNodes, function (item) {
		console.log ("node: " + item.id)
		var event = Array ();
		event.target = Array ();
		if (item.className.match (/unselect/)) {
			event.target.id = item.id;
			list.selectItem (event);
		}
	});
}

// Function to store grid preferences
function StorePreferences () {
	var prefs = new Object ();

	var grid = dijit.byId (grid_id);

	// Indicate the method name
	prefs.method = methodName;

	// Get current sort field
	prefs.sort_field = grid.getSortProps()[0].attribute;

	// Get width of each column
	prefs.columns = new Array ();
	prefs.widths = new Array ();
	var cell_idx = 0;
	dojo.forEach (grid.structure[0].cells[0], function (elm) {
		var field_name = elm.field;
		prefs.columns.push (field_name);
		prefs.widths.push (grid.getCell (cell_idx).unitWidth);
		cell_idx++;
	});

	// SelectLists
	switch (grid_id) {
		case 'grid_users':
			prefs.group_sort_field = dojo.query("#user_attributes_groups .sort-selector li.active a").attr ('field')[0];
			prefs.group_sort_order = dojo.query("#user_attributes_groups .sort-selector li.active.reverse").length ? -1 : 1;
			prefs.team_sort_field = dojo.query("#user_attributes_teams .sort-selector li.active a").attr ('field');
			prefs.team_sort_order = dojo.query("#user_attributes_teams .sort-selector li.active.reverse").length ? -1 : 1;
			prefs.application_sort_field = dojo.query("#user_attributes_applications .sort-selector li.active a").attr ('field');
			prefs.application_sort_order = dojo.query("#user_attributes_applications .sort-selector li.active.reverse").length ? -1 : 1;
			break;
		case 'grid_groups':
			prefs.user_sort_field = dojo.query("#group_users .sort-selector li.active a").attr ('field')[0];
			prefs.user_sort_order = dojo.query("#group_users .sort-selector li.active.reverse").length ? -1 : 1;
			prefs.team_sort_field = dojo.query("#group_teams .sort-selector li.active a").attr ('field');
			prefs.team_sort_order = dojo.query("#group_teams .sort-selector li.active.reverse").length ? -1 : 1;
			prefs.application_sort_field = dojo.query("#group_applications .sort-selector li.active a").attr ('field');
			prefs.application_sort_order = dojo.query("#group_applications .sort-selector li.active.reverse").length ? -1 : 1;
			break;
		case 'grid_teams':
			prefs.user_sort_field = dojo.query("#team_users .sort-selector li.active a").attr ('field')[0];
			prefs.user_sort_order = dojo.query("#team_users .sort-selector li.active.reverse").length ? -1 : 1;
			prefs.group_sort_field = dojo.query("#team_groups .sort-selector li.active a").attr ('field');
			prefs.group_sort_order = dojo.query("#team_groups .sort-selector li.active.reverse").length ? -1 : 1;
			break;
		case 'grid_applications':
			prefs.user_sort_field = dojo.query("#application_attributes_users .sort-selector li.active a").attr ('field')[0];
			prefs.user_sort_order = dojo.query("#application_attributes_users .sort-selector li.active.reverse").length ? -1 : 1;
			prefs.group_sort_field = dojo.query("#application_attributes_groups .sort-selector li.active a").attr ('field');
			prefs.group_sort_order = dojo.query("#application_attributes_groups .sort-selector li.active.reverse").length ? -1 : 1;
			break;
	}

	console.log ("Storing the following preferences: ");
	console.dir (prefs);

	// Store preferences
	dojo.xhrPost({
		url: "StorePreferences.json",
		timeout: 30000, // give up after 30 seconds
		content: prefs,
		handleAs: "json",
		load: function(response, ioArgs) {
			console.debug("Store preferences response:");
			console.dir(response);
		}
	});
}

var timer;
var delay = 3000;
function TriggerCount (dialog_id, method) {
	// Drop any existing timer
	if (timer) {
		clearTimeout (timer);
	}

	// Start new timer
	timer = setTimeout (function () {
		var form_data = {};
		dojo.query ('#' + dialog_id + ' form input').forEach (function (item){
				if (item.name && item.value) {
					form_data[item.name] = item.value;
				}
			});
		form_data.mode = 'count';
		console.dir (form_data);

		dojo.xhrPost({
			url: method + ".json",
			timeout: 30000, // give up after 3 seconds
			content: form_data,
			handleAs:"json",
			load: function(response, ioArgs){
				var count = response.count ? response.count : 0;
				console.log ('Matches: ' + count)
				dojo.query ('#' + dialog_id + ' span.counter-value').forEach (function (item) {
						item.innerHTML = count;
					});
				dojo.query ('#' + dialog_id + ' span.matches-note').forEach (function (item) {
						item.style.display = 'inline';
					});
			},
			error: function(err, ioArgs){
				// Just ignore errors
			}
		});

		delay = 500;
	}, delay);
}


//
// Tags-related functions
//

function ClearTaglistImportErrors () {
	// Hide error block
	dojo.byId ('import_taglist_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('import_taglist')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayTaglistImportDialog () {
	// Clear errors
	ClearTaglistImportErrors ();

	// Show dialog
	dijit.byId ('import_taglist').show ();
}

function TaglistImportCallback (err) {
	var underlay = dijit.byId ('underlay');
	if (underlay) {
		underlay.hide ();
	}
	if (err.children.length) {
		dojo.byId ('import_taglist_error_block').style.display = 'block';
		dojo.byId ('import_taglist_error_block_content').innerHTML = '<ul>' + err.innerHTML + '</ul>';
	}
	else {
		dijit.byId ('import_taglist').hide ();
		location.reload ();
	}
}

function DeleteTagsCatalog (rowid) {
	ShowUnderlay ();
	var data = {
		catalog_id: rowid
	};
	dojo.xhrPost({
		url: "DeleteTagsCatalog.json",
		timeout: 30000, // give up after 3 seconds
		content: data,
		handleAs: "json",
		load: function(response, ioArgs){
			window.location.reload ();
		},
		error: function(err, ioArgs){
			window.location.reload ();
		}
	});
}

function ToggleFreeTags () {
	ShowUnderlay ();
	dojo.xhrPost({
		url: "ToggleFreeTags.json",
		timeout: 30000, // give up after 3 seconds
		handleAs: "json",
		load: function(response, ioArgs){
			window.location.reload ();
		},
		error: function(err, ioArgs){
			window.location.reload ();
		}
	});
}
