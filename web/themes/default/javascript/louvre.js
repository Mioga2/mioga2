var Louvre = {};

window.onload = function () {
	var button_submit = document.getElementById('button_submit');
	if (button_submit) {
		button_submit.onclick = function() {
			var form = document.getElementById('the_form');
			if (form.maxwidth.value < 10 || form.maxwidth.value > 999) {
				alert(Louvre.messages.maxwidth);
				return false;
			} else if (form.maxheight.value < 10 || form.maxheight.value > 999) {
				alert(Louvre.messages.maxheight);
				return false;
			}
				
			jQuery ('#button_submit').hide ();
			jQuery ('#p_show_loading').removeClass('invisible');
			
			form.submit();
			jQuery ('input, select').prop ('disabled', true);
			return false;
		};
	}
};

function InitializeUI () {
	$('#ui-container').layout ();
	$('#browser').treeview ();

	$('#advanced-search-parameters-link').toggle (
		function () {
			$('#advanced-search-parameters').show ();
			$('#advanced-search-parameters input, #advanced-search-parameters select').removeAttr ('disabled');
		},
		function () {
			$('#advanced-search-parameters').hide ();
			$('#advanced-search-parameters input, #advanced-search-parameters select').attr ('disabled', true);
		}
	);
}

// Display a gallery in the viewer
function DisplayGallery (uri) {
	$('#viewer').attr ('src', uri);
	$('#search-results').hide ();
	HideSearchBar ();
	$('#viewer-container').show ();
}

// Fetch and display search results
function Search () {
	var data = $('form').serializeObject ();

	// Process search and format results
	$.post (search_uri, data, function (response) {
		$('#search-query').html (data.query_string);
		$('#search-results').find ('ul.result-list').html ('');
		$('#download-selection').hide ();
		$.each (response.Result, function () {

			// Only keep results that are contained in a gallery
			if (!this.sup_data || !this.sup_data.gallery) {
				return (0);
			}

			// Skip thumbnails
			if (this.url.match (/_tn\.[^\.]*$/)) {
				return (0);
			}

			// Get thumbnail and (sub-)gallery URLs
			var thumbnail_url = escape (this.sup_data.gallery.thumbnail_uri);
			var re = new RegExp(this.sup_data.gallery.gallery_uri + '/' + louvre_dir + '(.*)/[^/]*$');
			this.sup_data.gallery.thumbnail_uri.match (re);
			var subdirs = RegExp.$1;
			var gallery_url = escape (this.sup_data.gallery.gallery_uri + '/' + louvre_dir + subdirs + '/index.html');

			var $result = $('<li class="result"></li>');
			var $link = $('<a href="#"></a>').append ($('<div class="thumbnail"></div>').append ('<img src="' + thumbnail_url + '" alt="' + no_thumbnail + '"/>'));
			$link.find ('img').hover (
				function () {
					$('#preview').fadeOut ('fast').remove ();
					Preview ($result);
				},
				function () {
				}
			);
			$link.click (function () {
				// Click on result switches to corresponding gallery
				$('#search-results').hide ();
				$('#viewer-container').show ();
				$('#viewer').attr ('src', gallery_url);
				
				// Add close box
				$('<div class="close-box">&#160;</div>').appendTo ('#viewer-container').click (function () {
					$('#search-results').show ();
					$('#viewer-container').hide ();
					$(this).remove ();
				});
			});
			$result.append ($link);
			var $cb = $('<input type="checkbox" name="' + this.url + '"/>').appendTo ($result);
			$cb.change (function () {
				if ($('li.result input:checked').length) {
					$('#download-selection').show ();
				}
				else {
					$('#download-selection').hide ();
				}
			});
			$('#search-results').find ('ul.result-list').append ($result);
		});

		// Show correct container
		$('#search-results').show ();
		$('#viewer-container').hide ();
		$('div.close-box').hide ();
	}, 'json');
}

// Trigger search when user presses ENTER
function HandleKeyPress (event) {
	if (event.keyCode == 13) {
		Search ();
	}
}

function ShowSearchBar () {
	$('#search-bar').slideDown ("slow");
	$('div.close-box').show ();
}

function HideSearchBar () {
	$('#search-bar').slideUp ("slow");
}

function DownloadSelectedImages () {
	var data = {
		path: mioga_context.private_dav,
		entries: $('#search-results input:checked').map (function () {
			var $this = $(this);
			return ($this.attr ('name'));
		}).get ()
	};
	var url = '../Magellan/DownloadFiles?' + $.param (data);
	url = url.replace (/%5B%5D/g, '');
	console.log ('Downloading files: ' + url);

	// Start download
	$('#viewer').attr ('src', url);
}

function Preview ($item) {
	var image_url = $item.find ('input[type$=checkbox]').attr ('name');
	var image_width = $item.find ('img').css ('width').replace ('px', '');
	var image_height = $item.find ('img').css ('height').replace ('px', '');
	var position = $item.find ('img').position ();

	// Create preview
	var $preview = $('<div id="preview"><img style="width: 100%; height: 100%;" src="' + image_url + '"/></div>').appendTo ($('#search-query')).css ({
		'position': 'absolute',
		'top': position.top + 'px',
		'left': (position.left - (image_width / 2)) + 'px'
	});
	$preview.find ('img').css ({
		'height': 2 * image_height + 'px',
		'width': 2 * image_width + 'px'
	});

	// Get details
	if (!$item.find ('div.details').length) {
		$.getJSON ('../Magellan/GetProperties.json', { uri: image_url }, function (response) {
			console.log ("Data fetch complete: ");
			console.dir (response);
			var $details = $('<div class="details"></div>').appendTo ($item).hide ();
			var $description = $('<div class="description">' + response.description + '</div>').appendTo ($details);
			var $tags = $('<ul class="taglist"></ul>').appendTo ($details);
			$.each (response.tag, function (index, tag) {
				$('<li class="tag">' + tag + '</li>').appendTo ($tags);
			});
			$details.clone ().show ().appendTo ($preview);
		});
	}
	else {
		// Details already loaded
		$item.find ('div.details').clone ().show ().appendTo ($preview);
	}

	// Trigger link click on preview click
	$preview.click (function () {
		$item.find ('a').trigger ('click');
	});

	// Display preview
	$preview.fadeIn ('fast');

	// Hide preview when mouse leaves
	$preview.mouseleave (function () {
		$(this).fadeOut ('fast').remove ();
	});
}

function ToggleCheckBoxes () {
	if ($('#popup_display').attr ('checked')) {
		$('input[type$=checkbox]').each (function () {
			if ($(this).attr ('id') === 'popup_display') {
				return (0);
			}
			$(this).attr ('disabled', 'true');
			$(this).attr ('checked', 'true');
		});
	}
	else {
		$('input[type$=checkbox]').removeAttr ('disabled');
	}
}
