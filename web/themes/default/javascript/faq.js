// The global variable "faqVars" must be defined in a <script> section.

dojo.require("mioga.InlineEditBox");
dojo.requireLocalization("mioga", "faq");

dojo.addOnLoad(function() {
	dojo.connect(dojo.byId("open_entries"), "onclick", null, function() {
		dojo.query(".faq_entry").forEach(function (div) { 
			dijit.byNode(div).setAttribute("open", true);
		});
	});
	dojo.connect(dojo.byId("close_entries"), "onclick", null, function() {
		dojo.query(".faq_entry").forEach(function (div) { 
			dijit.byNode(div).setAttribute("open", false); 
		});
	});
	
	if (faqVars.adminSession) {
		// console.log("hello Console.JS");
		// Add modification links
		// dojo.query(".category", "categories").style("position", "relative");
		var categories_width = dojo.marginBox("categories").w;

		var found = false;
		dojo.query("div.icon_delete", "categories").forEach(function(div) {
			dojo.connect(div, "onclick", null, deleteCategory);
			dojo.style(div, "float", "right");
			dojo.style(div, "marginLeft", (dojo.style(div, "marginLeft") + 5) + "px");
			// dojo.style(div, { position: "absolute", top: "0px", right: "0px" });
			// if (right_pos === 0) { right_pos = dojo.marginBox("div").w + 5; }
			if (!found) { 
				categories_width += dojo.marginBox(div).w; 
				found = true;
			}
		});

		dojo.query("div.deletion_not_possible", "categories").forEach(function(div) {
			dojo.addClass(div, "icon_delete");
			dojo.style(div, "backgroundImage", "url()"); // just consume enough space for the icon
			dojo.style(div, "float", "right");
			dojo.style(div, "marginLeft", (dojo.style(div, "marginLeft") + 5) + "px");
			// dojo.style(div, { position: "absolute", top: "0px", right: "0px" });
			// if (right_pos === 0) { right_pos = dojo.marginBox("div").w + 5; }
			if (!found) {
				categories_width += dojo.marginBox(div).w;
				found = true;
			} 
		});

		dojo.query("div.icon_edit", "categories").forEach(function(div, i) {
			dojo.connect(div, "onclick", null, editCategory);
			dojo.style(div, "float", "right");
			// console.log("setting");
			// console.log(div);
			// console.log("to right: "+ right_pos + "px");
			// dojo.style(div, { position: "absolute", top: "0px", right: right_pos + "px" });
			if (i === 0) { categories_width += dojo.marginBox(div).w; }
		});

		dojo.connect(dojo.byId("btn_cat_new"), "onclick", null, newCategory);
		var dijit_newFAQ = dijit.byId("faq_entry_new");
		dojo.connect(dojo.byId("btn_entry_new"), "onclick", 
				dijit_newFAQ, dijit_newFAQ.editFaq);
		// Make FAQ entries draggable and droppable
		fieldBeingEdited._entryDndList = make_draggables("entries");
		dojo.place(dijit_newFAQ.domNode, "faq_entries", "last");
		fieldBeingEdited._categoryDndList = make_draggables("categories");
		// Hook FAQ entries to the "fieldBeingEdited" mechanism
		var faqpanes = returnInDOMOrder("entries").concat(dijit_newFAQ.domNode);
		dojo.forEach(faqpanes, function(faqpane_node) {
			var faqpane = dijit.byNode(faqpane_node);
			faqpane.callBeforeEdit     = function()     { return ! fieldBeingEdited.get() };
			faqpane.callAfterEditStart = function(node) { return fieldBeingEdited.set(node); };
			faqpane.callAfterEditEnd   = function()     { return fieldBeingEdited.clear() };
			faqpane.callbackResponse   = function(data) { reloadOrShowError(data, false); };
		});
		
		// IE needs help
		if (dojo.isIE) {
			dojo.style("categories", "width", categories_width);
		}
		// WebKit needs some extra pixels to keep the icons in the same line
		dojo.style("categories", "width",
				(dojo.style("categories", "width") + 6) + "px");
	}
	
	// Layout, after creating everything else
	if (dojo.byId("category_box") !== null) {
		var cB_mB = dojo.marginBox("category_box"); 
		dojo.style("faq_content", "marginLeft",
					(cB_mB.l + cB_mB.w + dojo.style("faq_content", "marginLeft")) + "px");
	}
});

function make_draggables(mode) { // domList, containerNode, moveCallback) {
	var domList = returnInDOMOrder(mode);
	var containerNode = (mode === "categories" ? "categories" : "faq_entries");
	var wrappers = new Array();
	var originalOrder = new Array();
	dojo.forEach(domList, function(div_node) {
		// DnD items get their own IDs, so we use a wrapper node
		var wrapperNode = dojo.create("div", { type: mode }, div_node, "before");
		dojo.addClass(wrapperNode, "draggable");
		dojo.place(div_node, wrapperNode, "first");
		wrappers.push(wrapperNode);
		originalOrder.push(div_node.id);
	});
	var dndSource = new dojo.dnd.Source(containerNode, { accept: mode });
	dndSource.insertNodes(false, wrappers);
	dojo.connect(dndSource, "onDrop", function() { 
		movedItem(mode, originalOrder);
	});
	return dndSource;
}

var fieldBeingEdited = {
	_field: null,
	_entryDndList: undefined,
	_categoryDndList: undefined,
	_savedHandlers: {},
	get: function (field_in_edit) {
		if (this._field === null) {
			return false;
		} else {
			dijit.focus(this._field);
			return true;
		}
	},
	set: function (field) {
		this._field = field;
		this._enableDnd(this._entryDndList, false);
		this._enableDnd(this._categoryDndList, false);
	},
	clear: function() {
		this._field = null;
		this._enableDnd(this._entryDndList, true);
		this._enableDnd(this._categoryDndList, true);
	},
	_enableDnd: function(dndList, value) {
		dndList.isSource = value;
		var nid = dndList.node.id;
		if (this._savedHandlers[nid] === undefined) {
			this._savedHandlers[nid] = {};
		}
		if (value) {
			if (this._savedHandlers[nid]["onMouseOver"] !== undefined) {
				// dojo.connect(dndList, "onMouseOver", 
				//		dndList, this._savedHandlers[nid]["onMouseOver"]);
				dndList.onMouseOver = this._savedHandlers[nid]["onMouseOver"];
				this._savedHandlers[nid]["onMouseOver"] = undefined;
			}
		} else {
			if (this._savedHandlers[nid]["onMouseOver"] === undefined) {
				this._savedHandlers[nid]["onMouseOver"] = dndList.onMouseOver;
				dndList.onMouseOver = function() {};
			}
		}
	}
};


function editCategory(evt) {
	if ( fieldBeingEdited.get() ) { return; }
	var cat_id = evt.currentTarget.parentNode.id;
	var cat_link_list = dojo.query("#" + cat_id + " a.cat_link");
	if (cat_link_list.length !== 1) { return; }
	var cat_link = cat_link_list[0];
	dojo.removeClass(cat_link, "cat_link");
	
	dojo.query("#" + cat_id + " .icon_edit"  ).style("display", "none");
	dojo.query("#" + cat_id + " .icon_delete").style("display", "none");
	
	var ieb = new mioga.InlineEditBox({}, cat_link);
	var oldValue = dojo.attr(cat_link, "innerHTML"); 
	dojo.connect(ieb, 'onCancel', function() { 
		finishedEditingCategory(this, cat_id, cat_link, "cancel"); });
	dojo.connect(ieb, 'onChange', function(newValue) { 
		finishedEditingCategory(this, cat_id, cat_link, "change", oldValue, newValue); });
	ieb.edit();
	
	fieldBeingEdited.set(ieb.domNode);
}

function movedItem(mode, original_order) {
	if ( fieldBeingEdited.get() ) { return; }
	var current_order = returnInDOMOrder(mode);
	if (original_order.length === current_order.length) {
		var hasChanged = false;
		hasChanged = dojo.some(original_order, function(original_id, index) {
			return current_order[index].id !== original_id;
		});
	}
	if (hasChanged) {
		fieldBeingEdited.set(dojo.body());
		var id_position = { mode: mode };
		dojo.forEach( current_order, function(item, index) {
			var regex = (mode === "categories" ? /^cat_/ : /^faq_entry_/ );
			var rowid = item.id.replace(regex, "");
			id_position["row"+rowid] = index+1;
		});
		dojo.xhrPost({
			url: "SetOrder.json",
			handleAs: "json",
			sync: true,
			content: id_position,
			load: function(data) { reloadOrShowError(data, true); }
		});
	}
}

function newCategory(evt) {
	if ( fieldBeingEdited.get() ) { return; }
	dojo.create("form", {id:"form_cat_edit"}, "div_btn_cat_new", "before");
	var form = new dijit.form.Form({}, "form_cat_edit");

	var span_cat_new = dojo.create("span", {id: "span_cat_new"}, "form_cat_edit");
	
	var ieb = new mioga.InlineEditBox({}, span_cat_new);
	dojo.connect(ieb, 'onCancel', function() { 
		finishedCreatingCategory(this, "cancel"); });
	dojo.connect(ieb, 'onChange', function(newValue) { 
		finishedCreatingCategory(this, "change", faqVars.idcat, newValue); });

	dojo.connect(form, 'onSubmit', ieb, function(evt) {
		this.save(true);
		evt.stopPropagation();
	});
	ieb.edit();
	fieldBeingEdited.set( dojo.query("#"+ieb.domNode.id + " :first-child")[0] );
	
}

function finishedEditingCategory(obj, cat_id, cat_link, status, oldVal, newVal) {
	if (status === "change") {
		if (newVal.length > 0) {
			var rowid = cat_id.replace(/^cat_/, "");
			dojo.xhrPost({
				url: document.location.pathname + ".json",
				handleAs: "json",
				sync: true,
				content: { 
					title: newVal,
					act_insert_category: "Modifier",
					dbaction: "update",
					rowid: rowid
				},
				load: reloadOrShowError
			});
		} else {
			dojo.attr(cat_link, "innerHTML", oldVal);
			fieldBeingEdited.clear();
		}
	} else {
		fieldBeingEdited.clear();
	}
	// Remove InlineEditBox, remove FORM, restore link
	obj.destroy(true);
	dojo.removeClass(cat_link);
	dojo.addClass(cat_link, "cat_link");
	dojo.query("#" + cat_id + " .icon_edit"  ).style("display", "block");
	dojo.query("#" + cat_id + " .icon_delete").style("display", "block");
}

function finishedCreatingCategory(obj, status, idcat, title) {
	if (status === "change" && title.length > 0) {
		dojo.xhrPost({
			url: document.location.pathname + ".json",
			handleAs: "json",
			sync: true,
			content: { 
				title: title,
				act_insert_category: "Modifier",
				dbaction: "insert",
				idcat: idcat
			},
			load: function(data) { reloadOrShowError(data, false); }
		});
	} else {
		fieldBeingEdited.clear();
	}
	dijit.byId("form_cat_edit").destroyRecursive();
}

function reloadOrShowError(data, clear) {
	if (data.success === true) {
		document.location.reload(); // to mitigate old values in browser cache
	} else {
		var transl = dojo.i18n.getLocalization("mioga", "faq");
        var dlg = new dijit.Dialog({
            style: "width: 300px",
            title: transl.errorLabel,
            content: data.error
        });
        dlg.show();
        if (clear) { fieldBeingEdited.clear(); }
	}
}

function deleteCategory(evt) {
	if ( fieldBeingEdited.get() ) { return; }
	var cat_id = evt.currentTarget.parentNode.id;
	var cat_link = dojo.query("#" + cat_id + " .cat_link")[0];
	var cat_name = dojo.attr(cat_link, "innerHTML");
	var transl = dojo.i18n.getLocalization("mioga", "faq");
	
	if ( window.confirm(transl.deleteCategory.replace("%CATEGORY%", cat_name)) ) {
		var cat_rowid = cat_id.replace(/^cat_/, "");
		dojo.xhrPost({
			url: "DeleteCategory.json",
			handleAs: "json",
			sync: true,
			content: { cat_id: cat_rowid },
			load: function(data) { reloadOrShowError(data, false); }
		});
	}
}


function returnInDOMOrder(mode) {
	var parentID, childClass;
	if (mode === "categories") {
		parentID = "categories";
		childClass = "category";
	} else {
		parentID = "faq_entries";
		childClass = "faq_entry";		
	}
	// We cannot trust Dojo to get the right order in all browsers
	var div_faq_entries = dojo.byId(parentID);
	var result_list = new Array();
	var current_entry = div_faq_entries.firstChild;
	while (current_entry !== null) {
		if (dojo.hasClass(current_entry, childClass)) {
			result_list.push(current_entry);
		} else if (dojo.hasClass(current_entry, "draggable")) {
			result_list.push(current_entry.firstChild);
		}
		current_entry = current_entry.nextSibling;
	}
	return result_list;
}


// dojo.provide("mioga.FaqPane");
dojo.require("dijit.Editor");
dojo.require("dijit._editor.plugins.AlwaysShowToolbar");
dojo.require("dijit._editor.plugins.LinkDialog");

/* Widget for a FAQPane. 
 * 
 * (1) Only if called with the parameter "adminSession: true", it provides editing facilities.
 * By default, it behaves like its ancestor dijit.TitlePane.
 * 
 * (2) An existing FAQ entry must be declared on a DIV node that has 
 * two subnodes: one of class "question" and one of class "answer",
 * each with the corresponding data. Further mandatory attributes are 
 * "title" and (if adminSession is true) "entry_id".
 * 
 * (3) A new FAQPane (not corresponding to an existing entry) can be created from an empty
 * DIV node, with the attributes "newEntry: true" and a numerical value for "category_id".
 * This entry will be visible only in editing mode.
 * 
 * (4) There are four callback functions that can be overwritten for more control.
 * Editing is only allowed when "callBeforeEdit" returns "true" (default behaviour).
 * When editing has started, "callAfterEditStart" is called, with the DOM node of
 * the title editing widget as a parameter. When editing is finished (i.e. cancelled),
 * "callAfterEditEnd" is called. Finally, callbackResponse() should handle the
 * JSON response one gets after an edit/creation or deletion request.
 * All callback functions are used within the Mioga2 FAQ application. 
 */

// Prevent TinyMCE toolbar items from getting focus with tab key
function setTinyMCETabIndex () {
	$(".mceToolbar a").attr("tabIndex", "-1");
}

dojo.declare("mioga.FaqPane", dijit.TitlePane, {
	entry_id: "",
	newEntry: false,
	category_id: "",
	adminSession: false,
	former_onTitleKey: undefined,
	callBeforeEdit:     function() { return true; },
	callAfterEditStart: function() {},
	callAfterEditEnd:   function() {},
	callbackResponse:   function() {},
	
	postCreate: function() {
		this.inherited(arguments);
		if (this.newEntry === true) { this.newEntry = true; }
		if (this.adminSession) {	
			var deleteImgNode = dojo.create("div", { className: "icon_delete", title: faqVars.i18n.delete_tooltip },
					this.focusNode, "first");
			dojo.style(deleteImgNode, "float", "right");
			dojo.style(deleteImgNode, "marginLeft", 
					(dojo.style(deleteImgNode, "marginLeft") + 5) + "px");
			dojo.connect(deleteImgNode, "onclick", this, this.deleteFaq);
			
			var editImgNode = dojo.create("div", { className: "icon_edit", title: faqVars.i18n.edit_tooltip }, 
					deleteImgNode, "after");
			dojo.style(editImgNode, "float", "right");
			dojo.connect(editImgNode, "onclick", this, this.editFaq);
		
		} else if (this.newEntry) {
			this.old_title = "";
			this.old_question = "";
			this.old_answer = "";
			dojo.style(this.domNode, "display", "none");
		}
	},
	
	editFaq: function(evt) {
		if ( this.callBeforeEdit() ) {
			var transl = dojo.i18n.getLocalization("mioga", "faq");
			
			if (!this.newEntry) {
				this.old_title = dojo.attr(this.titleNode, "innerHTML");
				this.old_question = dojo.attr(dojo.query("div.question", this.domNode)[0], "innerHTML");
				this.old_answer   = dojo.attr(dojo.query("div.answer",   this.domNode)[0], "innerHTML");
			}

			// Wrap the whole DOM node into the form
			var dom_editForm = dojo.create("form", {}, this.domNode, "before");
			this.edit_form = new dijit.form.Form({}, dom_editForm);
			dojo.place(this.domNode, this.edit_form.domNode, "first");
			
			dom_label_title = dojo.create("label", 
					{ "for":"title", innerHTML: transl.titleLabel },
					this.titleNode, "only");
			dojo.style(dom_label_title, "marginRight", "0.7em");
			var dom_title = dojo.create("input", 
					{ type: "text", "name": "title" }, 
					this.titleNode, "last");
			this.edit_title = new dijit.form.TextBox({ name: "title", value: this.old_title },
					dom_title);
			this.former_onTitleKey = this._onTitleKey;
			this._onTitleKey = function() {};
			var cn = this.containerNode;
			
			var fieldset_question = dojo.create("fieldset", {}, cn, "only");
			dojo.create("legend", { innerHTML: transl.questionLabel },
					fieldset_question, "last");
			this.edit_question = dojo.create("div", {}, fieldset_question, "last");
			this.edit_question.innerHTML = '<textarea id="faq_edit_question" class="faq_edit_question">' + this.old_question + '</textarea>';
			
			var fieldset_answer = dojo.create("fieldset", {}, cn, "last");
			dojo.style(fieldset_answer, "marginTop",
					(dojo.style(fieldset_answer, "marginTop") + 7) + "px");
			dojo.create("legend", { innerHTML: transl.answerLabel },
					fieldset_answer, "last");
			this.edit_answer = dojo.create("div", {}, fieldset_answer, "last");
			this.edit_answer.innerHTML = '<textarea id="faq_edit_answer" class="faq_edit_answer">' + this.old_answer + '</textarea>';
			
			// Initialize question editor
			tinyMCE.init({
					force_br_newlines : false,
					force_p_newlines : true,
					forced_root_block : '', 

					language : "fr",
					mode : "textareas",
					editor_selector: "faq_edit_question",
					skin : "o2k7",
					theme : "advanced",
					plugins : "directionality,layer,media,spellchecker,style,advimage,advlink,media,searchreplace,contextmenu,paste,fullscreen,tabfocus,inlinepopups",
					elements: "tabfocus",
					tabfocus_elements: "faq_edit_answer",
					init_instance_callback: "setTinyMCETabIndex",

					theme_advanced_disable :"",
					theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,cut,copy,paste,|,search,replace,|,bullist,numlist,|,undo,redo,|,link,unlink,|,image,charmap,emotions,iespell,media,|,fullscreen,|,newdocument",
					theme_advanced_buttons2 : "",
					theme_advanced_buttons3 : "",
					theme_advanced_toolbar_location : "top",
					theme_advanced_resizing : false,

					apply_source_formatting : true,

					width : "100%",
					height : "10em",
			});
			
			var dom_buttons = dojo.create("ul", {}, cn, "last");
			var dom_button_submit = dojo.create("button", { 
					id: "faq_store", className: "button", innerHTML: transl.saveEntryLabel
					}, dom_buttons, "last");
			dojo.style(dom_button_submit, "marginRight", "3em");
			dojo.connect(dom_button_submit, "onclick", this, this.saveFaq);
			var dom_button_cancel = dojo.create("a", { 
					className: "button", innerHTML: transl.cancelEntryLabel
					}, dom_buttons, "last");
			dojo.connect(dom_button_cancel, "onclick", this, this.cancelEditing);

			// Initialize answer editor
			tinyMCE.init({
					force_br_newlines : false,
					force_p_newlines : true,
					forced_root_block : '', 

					language : "fr",
					mode : "textareas",
					editor_selector: "faq_edit_answer",
					skin : "o2k7",
					theme : "advanced",
					plugins : "directionality,layer,media,spellchecker,style,advimage,advlink,media,searchreplace,contextmenu,paste,fullscreen,tabfocus,inlinepopups",
					elements: "tabfocus",
					tabfocus_elements: "faq_store",
					init_instance_callback: "setTinyMCETabIndex",

					theme_advanced_disable :"",
					theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,cut,copy,paste,|,search,replace,|,bullist,numlist,|,undo,redo,|,link,unlink,|,image,charmap,emotions,iespell,media,|,fullscreen,|,newdocument",
					theme_advanced_buttons2 : "",
					theme_advanced_buttons3 : "",
					theme_advanced_toolbar_location : "top",
					theme_advanced_resizing : false,

					apply_source_formatting : true,

					width : "100%",
					height : "10em",
			});

			this.dom_error = dojo.create("p", { className: "error" }, cn, "last");			
			if (!this.attr("open")) {
				this.attr("open", true);
			}
			this.toggleable = false;

			if (this.newEntry) {
				dojo.style(this.domNode, { opacity:0, display:"block" });
				dojo.fadeIn({ node: this.domNode }).play();
			}
			// This calculation is only possible when the object is visible
			dojo.style(this.edit_title.domNode, "width",
					(dojo.style(this.domNode, "width") - 150) + "px" );		
			
			var node_to_focus = dojo.query("input", this.edit_title.domNode)[0];
			// var node_to_focus = this.titleNode;
			dijit.focus(node_to_focus);
			this.callAfterEditStart(node_to_focus);
		}
		evt.stopPropagation();
	},
	
	deleteFaq: function(evt) {
		if ( this.callBeforeEdit() ) { 
			var entry_title = dojo.attr(this.titleNode, "innerHTML");
			var transl = dojo.i18n.getLocalization("mioga", "faq");
			if ( window.confirm(transl.deleteEntry.replace("%TITLE%", entry_title)) ) {
				dojo.xhrPost({
					url: "DeleteEntry.json",
					handleAs: "json",
					sync: true,
					content: { entry_id: this.entry_id },
					load: this.callbackResponse
				});
			}
		}
		evt.stopPropagation();
	},
	
	deleteFaqConfirm: function(data) {
		var confirmDlg = new dijit.Dialog({
			content: data
		});
		confirmDlg.show();
	},
	
	cancelEditing: function() {
		// Reverse all the edit widgets
		dojo.place(this.domNode, this.edit_form.domNode, "replace");
		this.edit_form.destroy();
		this.edit_title.destroy();
		dojo.destroy (this.edit_question);
		dojo.destroy (this.edit_answer);
		this.dom_error = undefined;
		
		if (this.newEntry) {
			dojo.style(this.domNode, "display", "none");
			dojo.empty(this.titleNode);
			dojo.empty(this.containerNode);
		} else {
			dojo.place(document.createTextNode(this.old_title), this.titleNode, "only");
			dojo.create("div", { className: "question", innerHTML: this.old_question },
					this.containerNode, "only");
			dojo.create("div", { className: "answer",   innerHTML: this.old_answer },
					this.containerNode, "last");
		}
		this._onTitleKey = this.former_onTitleKey;
		this.former_onTitleKey = undefined;
		this.toggleable = true;
		this.callAfterEditEnd();
	},
	
	saveFaq: function() {
		// We cannot gray out or in the button using onBlur, onChange or onKey* events
		// as there is always a possible situation where the refresh has not yet happened. 
		var question = tinyMCE.editors.faq_edit_question.getContent ();
		var answer = tinyMCE.editors.faq_edit_answer.getContent ();
		if (this.edit_title.attr("value").length === 0
				|| question === ''
				|| answer === '') {
			dojo.style(this.dom_error, "opacity", "0");
			var transl = dojo.i18n.getLocalization("mioga", "faq");
			dojo.attr(this.dom_error, "innerHTML", transl.fillAllFields);
			dojo.fadeIn({ node: this.dom_error }).play();
		} else {
			var content = {
				act_insert_entry: "yes",
				title:    this.edit_title.attr("value"),
				question: question,
				answer:   answer
			};
			if (this.newEntry) {
				content["idcat"] = this.category_id;
				content["dbaction"] = "insert";
			} else {
				content["rowid"] = this.entry_id;
				content["dbaction"] = "update";
			}

			dojo.xhrPost({
				url: document.location.pathname + ".json",
				handleAs: "json",
				sync: true,
				content: content,
				load: this.callbackResponse
			});
		}
	}
});
