(function ($) {
	$.fn.ellipsis = function (options) {
		var settings = $.extend ({
			lines: 2
		}, options);

		return (this.each (function () {
			var $this = $(this);

			$this.height ('auto');

			// Get line height
			var $dummy = $('<span>l</span>');
			$dummy.appendTo ($this);
			var line_height = $dummy.height ();
			$dummy.remove ();
			
			// Set height
			var height = settings.lines * line_height;
			if ($this.height () > height) {
				var $clone = $this.clone ();
				$clone.removeAttr ('id');

				$this.html ('');
				$this.append ($clone);
				$this.append ('<div class="ellipsis">\u2026</div>');

				$clone.height (height);
				$clone.css ('overflow', 'hidden');
			}
		}));
	};

})(jQuery);
