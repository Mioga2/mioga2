/**
 * @fileOverview The Mioga2 files explorer written with the ExtJS toolkit
 * @name Magellan
 */

/** @ignore */
function areArraysEqual(array1, array2) {
   var temp = [];
   if ( (!array1[0]) || (!array2[0]) ) {
      return false;
   }
   if (array1.length != array2.length) {
      return false;
   }
   for (var i=0; i<array1.length; i++) {
      key = (typeof array1[i]) + "~" + array1[i];
      if (temp[key]) { temp[key]++; } else { temp[key] = 1; }
   }
   for (var i=0; i<array2.length; i++) {
      key = (typeof array2[i]) + "~" + array2[i];
      if (temp[key]) {
         if (temp[key] === 0) { return false; } else { temp[key]--; }
      } else {
         return false;
      }
   }
   return true;
}




/** @ignore */
Ext.DataView.DragSelector = function(cfg){
    cfg = cfg || {};
    var view, regions, proxy, tracker;
    var rs, bodyRegion, dragRegion = new Ext.lib.Region(0,0,0,0);
    var dragSafe = cfg.dragSafe === true;

    /** @ignore */
    this.init = function(dataView){
        view = dataView;
        view.on('render', onRender);
    };

    function fillRegions(){
        rs = [];
        view.all.each(function(el){
            rs[rs.length] = el.getRegion();
        });
        bodyRegion = view.el.getRegion();
    }

    function cancelClick(){
        return false;
    }

    function onBeforeStart(e){
        return !dragSafe || e.target == view.el.dom;
    }

    function onStart(e){
        view.on('containerclick', cancelClick, view, {single:true});
        if(!proxy || !Ext.get(proxy.id)){
            proxy = view.el.createChild({cls:'x-view-selector'});
        }else{
            proxy.setDisplayed('block');
        }
        fillRegions();
        view.clearSelections();
    }

    function onDrag(e){
        var startXY = tracker.startXY;
        var xy = tracker.getXY();

        var x = Math.min(startXY[0], xy[0]);
        var y = Math.min(startXY[1], xy[1]);
        var w = Math.abs(startXY[0] - xy[0]);
        var h = Math.abs(startXY[1] - xy[1]);

        dragRegion.left = x;
        dragRegion.top = y;
        dragRegion.right = x+w;
        dragRegion.bottom = y+h;

        dragRegion.constrainTo(bodyRegion);
        proxy.setRegion(dragRegion);

        for(var i = 0, len = rs.length; i < len; i++){
            var r = rs[i], sel = dragRegion.intersect(r);
            if(sel && !r.selected){
                r.selected = true;
                view.select(i, true);
            }else if(!sel && r.selected){
                r.selected = false;
                view.deselect(i);
            }
        }
    }

    function onEnd(e){
        if(proxy){
            proxy.setDisplayed(false);
        }
    }

    function onRender(view){
        tracker = new Ext.dd.DragTracker({
            onBeforeStart: onBeforeStart,
            onStart: onStart,
            onDrag: onDrag,
            onEnd: onEnd
        });
        tracker.initEl(view.el);
    }
};

// Adding some more message boxes
/** @ignore */
Ext.MessageBox.error = function(title, msg, handler) {
  var config  = {
    buttons: Ext.MessageBox.OK,
    icon: Ext.MessageBox.ERROR,
    msg: msg,
    title: title
  };
  if (handler) { config.fn  = handler; }

  Ext.MessageBox.show(config);
};

// reference local blank image
Ext.BLANK_IMAGE_URL = String.format('{0}/resources/images/default/s.gif', extjs_path);

// create namespace
Ext.namespace('Magellan');

// create application
/**
 * The Magellan application object
 */
Magellan.app = function() {
  // private variables
  var viewport, rootString, basePath, homePath, pathElts, ddMenu, canSendMail, canSetProps;
  var version, codename;
  var contextMenu     = new Ext.menu.Menu();
  var history         = [];
  var history_pos     = 1;
  var walking_history = false;
  var walking_tree    = false;
  var drag_nodes      = [];
  var drop_nodes      = [];
  var list_drop_nodes = [];
  var clipboard       = {state: 'empty', items: []};
  var extracting      = false;
  var upload_win, rights_win, edit_win, history_win, extract_progress, comment_win, louvre_win, props_win;
  var addItem, renameItem, deleteItem, cutItem, copyItem, pasteItem, historyItem, historyEnableItem, editItem, lockItem,
      rightsItem, uploadItem, downloadItem, addGalleryItem, removeGalleryItem, recreateGalleryItem, showGalleryItem, propsItem;
  var iconView, treeView, listView;
  var properties, prop_extra_infos, prop_name, prop_mimetype, prop_size, prop_modified, prop_rights,
      prop_author, prop_locked, prop_lock_owner, prop_lock_owner_val, prop_comments, prop_tags, prop_description;
  var metrics;
  var viewMode;
  var timer;

  // private methods
  function reloadCurrentFolder() {
    var node  = treeView.getSelectionModel().getSelectedNode() || treeView.getRootNode();
    treeView.getLoader().load(node, function() { node.expand(); });
    displayFullNode(null, node);
  }

  // Navigation in the file tree (left pane)
  function displayFullNode(dsm, node, refresh) {
	iconView.store.load({params: {files: 1, node: node.id, refresh: refresh} });
    node.expand();
    setCurrentPath(node.id);
  }

  function toggleNavigationButtons(dsm, node) {
    var upBtn   = Ext.getCmp('up');
    var nextBtn = Ext.getCmp('next');
    var prevBtn = Ext.getCmp('previous');
    var homeBtn = Ext.getCmp('home');

    upBtn.setDisabled(node.id == basePath);
    nextBtn.setDisabled(history_pos == history.length);
    prevBtn.setDisabled(history_pos == 1);
    homeBtn.setDisabled(node.id == homePath);
  }

  function toggleHistoryButtons(sm) {
    var dlBtn   = Ext.getCmp('history-download');
    var delBtn  = Ext.getCmp('history-delete');
    var record  = sm.getSelected();

    [dlBtn, delBtn].invoke('disable');
    if (!record) { return; }

    if (record.data.type == 'edit') {
      dlBtn.enable();

      if (record.data.can_manage) {
        delBtn.enable();
      }
    }
  }

  function toggleContextMenu(e) {
    var records = getSelectedRecords();
    var node    = treeView.getSelectionModel().getSelectedNode();
    var items   = 0;

    contextMenu.removeAll();

    if (!records.length && node.attributes.rights == 'rw') {
      contextMenu.addMenuItem(addItem);
    }
    if (records.length == 1 && records.first().data.rights == 'rw') {
      contextMenu.addMenuItem(renameItem);
    }
    if (records.length && records.pluck('data').pluck('rights').all(function(rights) { return rights == 'rw'; })) {
      contextMenu.addMenuItem(deleteItem);
    }
    items = contextMenu.items.length;

    if (items) {
      contextMenu.addSeparator();
      items = contextMenu.items.length;
    }

    if (records.length) {
      if (records.pluck('data').pluck('rights').all(function(rights) { return rights == 'rw'; })) {
        contextMenu.addMenuItem(cutItem);
      }
      contextMenu.addMenuItem(copyItem);
    }
    if (node.attributes.rights == 'rw' && clipboard.state != 'empty') {
      var path  = clipboard.items[0].split('/');
      path.pop();
      path  = path.join('/');

	  if (node.id != path) { contextMenu.addMenuItem(pasteItem); }
    }

    if (items != contextMenu.items.length) {
      contextMenu.addSeparator();
      items = contextMenu.items.length;
    }

    if (records.length == 1) {
      if (records.first().data.history_enabled) {
	  	contextMenu.addMenuItem(historyItem);
	}
	else if ((records.first().data.group_has_history) && (records.first().data.can_set_history)) {
	  	contextMenu.addMenuItem(historyEnableItem);
	}
      if (records.first().data.rights == 'rw') {
        if (records.first().data.type.match(/(text\/)|(xml)|(xsl)|(javascript)|(perl)|(ruby)|(python)/i )) { contextMenu.addMenuItem(editItem); }

        if (records.first().data.type != 'directory') {
          if (records.first().data.locked) {
          	if (records.first().data.unlockable) {
				lockItem.text = Magellan.app.messages.unlock;
				lockItem.icon = String.format('{0}/16x16/actions/edit-unlock.png', image_path);
				contextMenu.addMenuItem(lockItem);
			}
          }
          else {
            lockItem.text = Magellan.app.messages.lock;
            lockItem.icon = String.format('{0}/16x16/actions/edit-lock.png', image_path);
			contextMenu.addMenuItem(lockItem);
          }
        }
      }
    }
    if (records.length <= 1) {
        if (records.length && records.length && records.first().get('manage') === true) {
            contextMenu.addMenuItem(rightsItem);
        }
        else if(node.attributes.manage === true) {
            contextMenu.addMenuItem(rightsItem);
        }
    }

	if (records.length && records.first().data.setprops) {
		contextMenu.addMenuItem(propsItem);
	}
    if (items != contextMenu.items.length) {
      contextMenu.addSeparator();
      items = contextMenu.items.length;
    }

    if (!records.length && node.attributes.rights == 'rw') {
      contextMenu.addMenuItem(uploadItem);
    }
    if (records.length) {
      contextMenu.addMenuItem(downloadItem);
	}
	if (records.length && records.first().get('email') == true) {
      contextMenu.addMenuItem(mailItem);
    }
	if (records.length && records.first().get('diderot') == true) {
      contextMenu.addMenuItem(attachToNoticeItem);
    }

	// LOUVRE START
	var louvre;
	if ((!records.length) && typeof(node.attributes.louvre) !== 'undefined') {
	  louvre = node.attributes.louvre;
	} else if (records.length === 1) {
	  louvre = records.first().get('louvre');
	}
	if (louvre !== undefined && louvre !== null) {
	  if (louvre.type === 'n' && louvre.perm === true) {
		contextMenu.addMenuItem(addGalleryItem);
	  } else if (louvre.type === 'g') {
		contextMenu.addMenuItem(showGalleryItem);
		if (louvre.perm === true) {
		  contextMenu.addMenuItem(recreateGalleryItem);
		  contextMenu.addMenuItem(removeGalleryItem);
		}
	  }
	}
	// LOUVRE END

    if (contextMenu.items.length) {
        contextMenu.showAt(e.getXY());
    }
  }

  function updateHistory(dsm, node) {
    if (!walking_history) {
      if (!(history.length == history_pos || history.length === 0)) {
        history.splice(history_pos);
      }
      history.push(node);
      history_pos = history.length;
    }
    updateHistoryButtons();
    walking_history = false;
  }

  function updateHistoryButtons() {
    var prevMenu  = Ext.getCmp('previous').menu;
    var nextMenu  = Ext.getCmp('next').menu;

    prevMenu.removeAll();
    nextMenu.removeAll();
    for (var i = 0; i < history_pos - 1; i++) {
      prevMenu.insert(0, new Ext.menu.Item({text: history[i].id,
                                            history_pos: i + 1,
                                            handler: goToHistory}));
    }

    for (var i = history_pos; i < history.length; i++) {
      nextMenu.add(new Ext.menu.Item({text: history[i].id,
                                      history_pos: i + 1,
                                      handler: goToHistory}));
    }
  }

  function goToParentNode() {
    var node  = treeView.getSelectionModel().getSelectedNode();

    expandTreeTo(node.parentNode.id);
  }

  function goToPrevious() {
    walking_history = true;
    history_pos--;
    expandTreeTo(history[history_pos - 1].id);
  }

  function goToNext() {
    walking_history = true;
    history_pos++;
    expandTreeTo(history[history_pos - 1].id);
  }

  function goToHome() {
    expandTreeTo(homePath);
  }

  function goToHistory(item) {
    walking_history = true;
    history_pos     = item.history_pos;
    expandTreeTo(history[history_pos - 1].id);
    updateHistoryButtons();
  }

  function expandTreeTo(path) {
    if (walking_tree) { return false; }

    walking_tree  = true;
    pathElts  = path.split('/').grep(/./).inject([], function(array, value) {
      array.push((array.last() || '') + '/' + value);
      return array;
    });

    pathElts  = pathElts.grep(treeView.getRootNode().id);
    treeView.getNodeById(pathElts.shift()).expand(false, true, walkToPath);
  }

  function walkToPath(node) {
    var curnode = treeView.getNodeById(pathElts.shift());

    if (curnode != null) {
      curnode.expand(false, true, walkToPath);
    }
    else {
      walking_tree = false;
      node.select();
    }
  }

  function actionForIcon(record) {
    if (record.get('type') == 'directory') {
      expandTreeTo(record.get('path'));
    } else {
      window.open(record.get('path'), '_blank');
    }
  }

  function gridActionDblClick(grid, idx, e) {
    actionForIcon(grid.getStore().getAt(idx));
  }

  function dataViewActionDblClick(dv, idx, node, e) {
    actionForIcon(dv.getRecord(node));
  }

  function checkConsistency (store) {
	if (store.reader.jsonData.inconsistent) {
		var json = new Array ();
		json.errors = new Array ();
		var elm = new Array ();
		elm.uri = store.lastOptions.params.node;
		elm.code = 'DBProblem';
		json.errors.push (elm);
		detailedError (json, '<div class="error-contents"><p>' + Magellan.app.messages.dbProblemHeader + '</p>');
	}
  }

  function addComment(e) {
    if (!comment_win) {
      var store = new Ext.data.JsonStore({
          url: 'GetComments.json',
          root: 'comments',
          fields: [
              {name: 'modified', type: 'date', dateFormat: 'Y-m-d H:i:s'},
              'comment',
              'author'
          ],
          sortInfo: {field: 'modified', direction: 'DESC'}
      });

      var tpl = new Ext.XTemplate(
          '<tpl for=".">',
              '<div class="comment">',
                '<h3>{[this.getHeader(values)]}</h3>',
                '<p>{comment}</p>',
              '</div>',
          '</tpl>', {
            getHeader: function(values) {
              return String.format(Magellan.app.messages.commentHeader, values.author, values.modified.format(Magellan.app.messages.dateFormat));
            }
          }
      );

      var dataview  = new Ext.DataView({
        id: 'comment-view',
        cls: 'comment-view',
        store: store,
        tpl: tpl.compile(),
        anchor: '100%',
        height: 205,
        multiSelect: false,
        itemSelector:'div.comment',
        emptyText: Magellan.app.messages.noComment,
        loadingText: Magellan.app.messages.loading
      });
      comment_win = new Ext.Window({
          layout: 'fit',
          width: 800,
          minWidth: 800,
          height: 400,
          minHeight: 400,
          closeAction: 'hide',
          shim: false,
          id: 'comment_window',
          constrainHeader: true,
          modal: true,

          items: [
            new Ext.form.FormPanel({
              frame: true,
              labelSeparator: '&nbsp;:',
              labelWidth: 150,
              labelAlign: 'top',
              autoScroll: true,
              url: 'AddComment.json',
              id: 'comment_form',
              items: [ dataview, {
                xtype: 'textarea',
                id: 'comment_content',
                name: 'comment',
                anchor: '99%',
                height: 100,
                fieldLabel: Magellan.app.messages.comment
              }, {
                xtype: 'hidden',
                id: 'comment_node_id',
                name: 'node'
              } ]
            }) ],

          buttons: [ {
            id: 'comment_submit',
            text: Magellan.app.messages.send,
            handler: commentSubmit
          }, {
            text: Magellan.app.messages.close,
            handler: function() {
              comment_win.hide();
              Ext.getCmp('comment_content').reset();
            }
          } ]
      });
      comment_win.render(document.body);
    }
    var record = getSelectedRecords().first();
    comment_win.record = record;
    comment_win.setTitle(String.format(Magellan.app.messages.commentFileTitle, record.get('name')));
    Ext.getCmp('comment-view').store.load({params: {node: record.get('path')} });
    if (record.get('rights') == 'rw') {
        Ext.getCmp('comment_submit').enable();
        Ext.getCmp('comment_content').enable();
    }
    else {
        Ext.getCmp('comment_submit').disable();
        Ext.getCmp('comment_content').disable();
    }
    comment_win.show();
  }

  function commentSubmit() {
    Ext.getCmp('comment_node_id').setValue(comment_win.record.get('path'));
    Ext.getCmp('comment_form').form.submit({
      waitMsg: Magellan.app.messages.creating,
      waitTitle: Magellan.app.messages.waitMessage,
      reset: false,
      failure: function(form, action) {
		genericError (action.result.error);
      },
      success: function(form, action) {
        var success = action.result.success;
        if (success === false) {
			genericError (action.result.error);
        }
        else {
          Ext.getCmp('comment-view').store.reload();
          Ext.getCmp('comment_content').reset();
          reloadCurrentFolder();
        }
      }
    });
  }

  function updateProperties(records) {
    var old_class   = properties.dom.className;
    var record_is_treeview = false;
    if (records.length === 0) {
      var node  = treeView.getSelectionModel().getSelectedNode();
      if (node) {
        var date  = node.attributes.modified ? Date.parseDate(node.attributes.modified, 'Y-m-d H:i:s') : null;
        records = [{ data: {
            type: node.attributes.mimetype,
            name: node.attributes.text,
            modified: date,
            size: node.attributes.size,
            rights: node.attributes.rights,
            author: node.attributes.author,
            locked: node.attributes.locked,
            unlockable: node.attributes.unlockable,
            lock_owner: node.attributes.lock_owner,
            user_is_lock_owner: node.attributes.user_is_lock_owner,
            setprops: node.attributes.setprops,
            path: node.attributes.id,
            elements: node.attributes.elements,
            louvre: node.attributes.louvre
          }
        }];
        record_is_treeview = true;

		jQuery ('label.properties-label').removeClass ('clickable');
		jQuery ('label.comments-label').removeClass ('clickable');
      }
    }
	else {
		console.dir (records);
		if (records.first () && (records.pluck('data').pluck('setprops').all(function(setprops) { return setprops == true; }))) {
			jQuery ('label.properties-label').addClass ('clickable');
			jQuery ('label.comments-label').addClass ('clickable');
		}
	}

    if (records.length <= 1) {
      var record  = records.first();

      if (record) {
   	    $('prop_extra_infos').setStyle({ display: "none" });
   	    var newClass = getClassForMime(record.data.type, 128).replace (/\./g, '-');
   	    // LOUVRE START
   	    if (newClass === 'mime-128-folder' && typeof(record.data.louvre) !== 'undefined' &&
   	    	(record.data.louvre.type === 'g' || record.data.louvre.type === 's')) {
   	      var path = (record.data.louvre.type === 'g' ? record.data.path : record.data.louvre.path);
  	      $('louvre_link').setAttribute('href', galleryForPath(path));
   	      newClass = 'mime-128-folder-image';
   	      $('louvre_infos').setStyle({ display: "block" });
   	      if (record_is_treeview) {
   	    	Ext.get('views-panel').child('div div.x-panel-body').addClass('gallery');
   	      }
   	    } else {
   	      $('louvre_infos').setStyle({ display: "none" });
   	      if (record_is_treeview) {
   	    	Ext.get('views-panel').child('div div.x-panel-body').removeClass('gallery');
   	      }
   	    }
   	    // LOUVRE END
        properties.replaceClass(old_class, newClass);
        prop_name.update(record.data.path ? String.format('<a href="{0}">{1}</a>', record.data.path, record.data.name): record.data.name);
        prop_mimetype.update((mimetypes[record.data.type] || mimetypes['file']).description);
        if (record.data.type == 'directory' && record.data.elements) {
        	// update size of current node
        	var total_size = 0;
        	var nodes = iconView.getNodes();
        	for (var i=0; i < nodes.length; i++) {
        		  if (nodes[i] !== undefined) {
        			  var record_tmp  = iconView.getRecord(nodes[i]);
        			  total_size += record_tmp.data.size;
            	  }
              }
        	prop_size.update(String.format("{0} ({1} éléments)", renderFileSize(total_size), nodes.length));
        }
        else {
          prop_size.update(renderFileSize(record.data.size));
        }
        prop_modified.update(record.data.modified ? record.data.modified.format(Magellan.app.messages.dateFormat) : '-');
        prop_rights.update(renderRights(record.data.rights));
        prop_author.update(record.data.author || '-');
        prop_locked.setVisible(record.data.locked);
        prop_lock_owner.setVisible(record.data.locked);
        prop_lock_owner_val.update(record.data.lock_owner || '-');

        if (!(record.data.comments >= 0)) {
            var ac    = Ext.get('add_comment');
            if (ac) { ac.removeListener('click', addComment); }
            prop_comments.update("-");
        }
        else {
			if (record.data.comments) {
				prop_comments.update(String.format("{0} {1}", record.data.comments, Magellan.app.messages.comments));
			}
			else {
				prop_comments.update(Magellan.app.messages.no_comment);
			}
        }

		if (record.data.properties && record.data.properties.tag.length) {
			jQuery ('#prop_tags').tags ({
				tags: record.data.properties.tag,
				readonly: 1
			});
			jQuery ('#prop_tags').ellipsis ({lines: 3});
		}
		else {
			prop_tags.update ('-');
		}

		if (record.data.properties && record.data.properties.description) {
			var description = record.data.properties.description.replace(/\\n/g, "<br/>").replace (/\\r/g, "");
			prop_description.update (description);
			jQuery ('#prop_description').ellipsis ();
		}
		else {
			prop_description.update ('-');
			jQuery ('#prop_description').removeAttr ('title');
		}
      }

	  if ((record && record.data.manage) || (node && node.attributes.manage)) {
		jQuery ('label.rights-label').addClass ('clickable');
	  }
    }
    else {
      var total       = records.length;
      var types       = records.pluck('data').pluck('type');
      var folders     = types.select(function(t) {
        return t == 'directory';
      }).length;
      var files       = types.select(function(t) {
        return t != 'directory';
      }).length;
      var total_size  = records.pluck('data').pluck('size').inject(0, function(acc, s) {
        return acc + s;
      });

      prop_locked.hide();
      properties.replaceClass(old_class, 'mime-128-multiple');
      prop_name.update("-");
      prop_mimetype.update("-");
      prop_size.update(renderFileSize(total_size));
      prop_modified.update("-");
      prop_rights.update("-");
      prop_author.update("-");
      prop_lock_owner_val.update("-");

      var ac    = Ext.get('add_comment');
      if (ac) { ac.removeListener('click', addComment); }
      prop_comments.update("-");
      prop_description.update("-");

      prop_extra_infos.update(String.format(Magellan.app.messages.extraInfos,
                                            total,
                                            folders,
                                            files));

		jQuery ('label.rights-label').removeClass ('clickable');
		jQuery ('label.comments-label').removeClass ('clickable');
      $('prop_extra_infos').setStyle({ display: "block" });
    }
  }

  function gridActionSC(sm) {
    updateProperties(sm.getSelections());
  }

  function dataViewActionSC(dv, selections) {
    updateProperties(dv.getRecords(selections));
  }

  function setIconView() {
    if (Ext.getCmp('views-panel').layout.activeItem.id == 'icon-view') { return; }

	// Set preferences if changed
	if (viewMode != 'icon') {
		viewMode = 'icon';
		setPreferences ();
	}

    var store   = listView.getStore();
    var records = listView.getSelectionModel().getSelections();

    store.sort('name', 'DESC');
    sortByFolders(store);
    var lv  = Ext.state.Manager.get('list-view', null);
    if (lv) {
      lv.sort.field     = 'name';
      lv.sort.direction = 'DESC';
      Ext.state.Manager.set('list-view', lv);
    }
    iconView.select(records.collect(function (r) { return r.store.indexOf(r); }));

    Ext.getCmp('views-panel').layout.setActiveItem(0);
    Ext.state.Manager.set('current-view', 0);
  }

  function setListView() {
    if (Ext.getCmp('views-panel').layout.activeItem.id == 'list-view') { return; }

	// Set preferences if changed
	if (viewMode != 'list') {
		viewMode = 'list';
		setPreferences ();
	}

    var records = iconView.getSelectedRecords();
    listView.getSelectionModel().selectRecords(records);

    Ext.getCmp('views-panel').layout.setActiveItem(1);
    Ext.state.Manager.set('current-view', 1);
  }

  function setPreferences () {
	Ext.Ajax.request({
	  url: 'SetPreferences.json',
	  params: { view_mode: viewMode },
	  scope: this
	});
  }

  function getImageForMime(type) {
    var mime  = mimetypes[type];
    if (!mime) { return mimetypes['file'].image; }

    return mime.image;
  }

  function getNameForThumb(name) {
    if (metrics.getWidth(name) < 100) { return name; }

    while (metrics.getWidth(name) > 100) {
      name = name.substr(0, name.length - 1);
    }
    return String.format("{0}...", name.substr(0, name.length - 3));
  }

  function getClassForMime(type, size) {
    var mime  = mimetypes[type];
    if (!mime) { mime  = mimetypes['file']; }

    return String.format('mime-{0}-{1}', size, mime.image);
  }

  function renderName(value, metadata, record) {
    var mime      = getImageForMime(record.data.type);
    metadata.css  = "mimetype mime-16-"+mime;
	metadata.css  = metadata.css.replace (/\./g, '-');
    metadata.attr = 'style="padding-left: 20px;"';

    return value;
  }

  function renderModification(value, metadata, record) {
    var msg;

    metadata.attr = 'style="padding-left: 20px;"';

    if (value == 'edit') {
      msg           = Magellan.app.messages.historyEdit;
      metadata.css  = "history-edit";
    }
    else {
      msg           = String.format(Magellan.app.messages.historyRename, record.data.old_name);
      metadata.css  = "history-rename";
    }

    return msg;
  }

  function renderFileSize(value) {
    if (!(value > 0)) { value = 0; }
    var size  = Ext.util.Format.fileSize(value);
    return size.replace('B',Magellan.app.messages.byteSymbol).replace('bytes',Magellan.app.messages.bytes);
  }

  function renderRights(value) {
    if (value == 'r')   { return Magellan.app.messages.read; }
    if (value == 'rw')  { return Magellan.app.messages.readWrite; }
    return Magellan.app.messages.none;
  }

  function renderLock(value) {
    if (!value) { return; }

    return String.format('<img src="{0}/16x16/status/object-locked.png"/>', image_path);
  }

  function gridAddDnD() {
    list_drop_nodes.invoke('unreg');
    list_drop_nodes.clear();

    var dirs  = [];

    listView.store.data.items.each(function(item, idx) {
      if (item.data.type == 'directory') { dirs.push(idx); }
    });
    dirs.each(function(dir) {
      var elm = listView.view.getRow(dir);
      Ext.dd.Registry.register(elm, {record: listView.store.data.items[dir]});
      var drop  = new Ext.dd.DropTarget(elm, {ddGroup: 'TreeDD'});
      drop.notifyDrop = processDrop.createDelegate(drop);
      list_drop_nodes.push(drop);
    });
  }

  function dataViewAddDnD() {
    drag_nodes.invoke('unreg');
    drop_nodes.invoke('unreg');
    drag_nodes.clear();
    drop_nodes.clear();

    var nodes   = iconView.getNodes();

    if (nodes.any()) {
      drag_nodes  = nodes.collect(function(node) {
        var record  = iconView.getRecord(node);

        Ext.dd.Registry.register(node.id, {record: record});
        var drag  = new Ext.dd.DragSource(node.id, {ddGroup: 'TreeDD'});
        drag.onStartDrag = processDrag.createDelegate(drag);
        return drag;
      });

      nodes.each(function(node) {
        var record  = iconView.getRecord(node);

        if (record.data.type == 'directory' && record.data.rights == 'rw') {
          var drop  = new Ext.dd.DropTarget(node.id, {ddGroup: 'TreeDD'});
          drop.notifyDrop = processDrop.createDelegate(drop);
          drop_nodes.push(drop);
        }
      });
    }
  }

  function processDrag() {
    var items = getSelectedRecords();
    if (items.length === 0) { items = [Ext.dd.Registry.getTarget(this).record]; }
    this.dragData.selections  = items;
    if (items.length <= 1) { return; }

    var ghost = this.proxy.getGhost();
    ghost.select('img').set({src: String.format('{0}/48x48/places/document-multiple.png', image_path)});
    ghost.select('span').update(String.format(Magellan.app.messages.elements, items.length));
  }

  // used for DropTarget
  function processDrop(source, e, data) {
    var nodes       = data.selections.collect(function(r) { return {record: r}; });
    var targetNode  = Ext.dd.Registry.getTarget(this.id);

    return menuDrop(nodes, targetNode, e);
  }

  // Start a timer that will alert the user a long operation is running
  function launchLongOperationTimer () {
	timer = setTimeout (function () {alert (Magellan.app.messages.long_operation_text); launchLongOperationTimer ();}, 30000);
  }

  function transferResources(srcPathes, destPath, mode) {
    var msg = mode == 'cut' ? Magellan.app.messages.moving : Magellan.app.messages.copying;

	// Launch timer to alert user if operation is long
	launchLongOperationTimer ();
    Ext.get(document.body).mask(msg, 'x-mask-loading');
    Ext.Ajax.request({
      url: 'PasteResources.json',
      params: { entries: srcPathes, mode: mode, destination: destPath },
      scope: this,
      timeout: 3600000,
      success: fileOpSuccess,
      failure: ajaxFailure
    });
  }

  function menuDrop(nodes, dest, e) {
    var pathes      = nodes.pluck('record').pluck('data').pluck('path');
    if (dest.record) {
      dest  = dest.record.data;
    }
    else {
      dest.attributes.path  = dest.id;
      dest  = dest.attributes;
    }

    if (dest.rights != 'rw' ||
        pathes.any(function(path) {
          return path == dest.path || path.replace(/(.*)\/.*/, '$1') == dest.path;
        })) { return false; }

    ddMenu.items.items[0].setHandler(transferResources.createCallback(pathes, dest.path, 'copy'));
    ddMenu.items.items[1].setHandler(transferResources.createCallback(pathes, dest.path, 'cut'));
    ddMenu.items.items[1].setDisabled(nodes.any(function(node) {
      return node.record.data.rights == 'r';
    }));
    ddMenu.showAt(e.getXY());
    return true;
  }

  function createFolder(btn, text) {
    if (btn != 'ok') { return; }

    createResource(text, 'folder');
  }

  function createNewFolder() {
    Ext.Msg.prompt(Magellan.app.messages.creatingFolder,
                   Magellan.app.messages.nameForFolder,
                   createFolder);
  }

  function createFile(btn, text) {
    if (btn != 'ok') { return; }

    createResource(text, 'file');
  }

  function createResource(name, type) {
    var currentNode = treeView.getSelectionModel().getSelectedNode();
    var nodes       = iconView.getNodes();

    name  = name.trim();

    if ((nodes.size () > 0) && nodes.any (function (node) { return (node && node.id == name); })) {
      return Ext.MessageBox.error(Magellan.app.messages.error, Magellan.app.messages.fileAlreadyExists);
    }
    Ext.get(document.body).mask(Magellan.app.messages.creating, 'x-mask-loading');
    Ext.Ajax.request({
      url: 'CreateResource.json',
      params: { path: currentNode.id, name: name, type: type },
      scope: this,
      success: fileOpSuccess,
      failure: ajaxFailure
    });
  }

  function ajaxFailure(response, options) {
    Ext.get(document.body).unmask();

	if (timer) {
		clearInterval (timer);
	}

    Ext.MessageBox.error(Magellan.app.messages.error, Magellan.app.messages.serverError);
  }

  function createNewFile() {
    Ext.Msg.prompt(Magellan.app.messages.creatingFile,
                   Magellan.app.messages.nameForFile,
                   createFile);
  }

  function renameEntry(btn, name) {
    if (btn != 'ok') { return; }
	if (name === '') { return; }

    var curNode   = getSelectedRecords().first();
    var curFolder = treeView.getSelectionModel().getSelectedNode();
    var nodes     = iconView.getNodes();
    name          = name.trim();

    if (nodes.any(function(node) { return node.id == name; })) {
      return Ext.MessageBox.error(Magellan.app.messages.error, Magellan.app.messages.fileAlreadyExists);
    }

    Ext.get(document.body).mask(Magellan.app.messages.renaming, 'x-mask-loading');
    Ext.Ajax.request({
      url: 'MoveResource.json',
      params: { source: curNode.data.path,
                destination: String.format("{0}/{1}",
                                           curFolder.id,
                                           name)
              },
      scope: this,
      success: fileOpSuccess,
      failure: ajaxFailure
    });
  }

  function fileOpSuccess(response, options) {
    Ext.get(document.body).unmask();
    var json    = Ext.decode(response.responseText);
    var reload  = true;

	if (timer) {
		clearInterval (timer);
	}

    if (json.success === false) {
	  var errorstr = '<div class="error-contents">';
	  if (options.params.clipboard) {
	  	// Initialize clipboard error
		errorstr += "<p>" + (options.params.mode == 'cut') ? Magellan.app.messages.cutErrorHeader : Magellan.app.messages.copyErrorHeader + "</p>";
		errorstr += "<p>" + Magellan.app.messages.clipboardSource + "<ul class='dav-source-list'>";
		options.params.entries.forEach (function (item) {errorstr += "<li>" + item + "</li>"});
		errorstr += "</ul></p>";
		errorstr += "<p>" + Magellan.app.messages.clipboardDestination + "<ul class='dav-source-list'><li>" + options.params.destination + "</li></ul></p>";
	  }
	  else if (options.params.source && options.params.destination) {
	  	// Initialize rename error
		errorstr += "<p>" + Magellan.app.messages.renameErrorHeader + "</p>";
		errorstr += "<p>" + Magellan.app.messages.clipboardSource + "<ul class='dav-source-list'><li>" + options.params.source + "</li></ul>";
		errorstr += "<p>" + Magellan.app.messages.clipboardDestination + "<ul class='dav-source-list'><li>" + options.params.destination + "</li></ul></p>";
	  }
	  else {
	  	// Default error layout
		errorstr += "<p>" + Magellan.app.messages.genericErrorHeader + "</p>";

	  }
	  detailedError (json, errorstr, function () { reloadCurrentFolder (); });
    }
    if (options && options.params && options.params.destination && options.params.entries) {
      var source  = options.params.entries[0].replace(/(.*)\/.*/, '$1');
      var dest    = options.params.destination;
      var current = treeView.getSelectionModel().getSelectedNode();

      if (options.params.clipboard && options.params.mode == 'cut') {
		clipboard.state = 'empty';
      }

      if (current.id == source && !current.id.match(dest)) {
        treeView.getNodeById(dest).reload(function () { reloadCurrentFolder (); });
        reload  = false;
      }
      else if (current.id == source && options.params.mode == 'copy') {
        treeView.getNodeById(dest).select();
        treeView.getNodeById(dest).reload(expandTreeTo.createCallback(current.id));
        reload  = false;
      }
      else if (current.id == dest && options.params.mode == 'cut') {
        options.params.entries.collect(function(e) { return treeView.getNodeById(e); }).without(null).invoke('remove');
      }
    }

    if ((json.success === true) && (reload == true)) {
		reloadCurrentFolder ();
	}
  }

  function getSelectedRecords() {
    var activeItem  = Ext.getCmp('views-panel').layout.activeItem;
    return (activeItem.getSelectionModel && activeItem.getSelectionModel ().getSelections) ? activeItem.getSelectionModel ().getSelections() : activeItem.getSelectedRecords();
  }

  function deselectAll() {
    var activeItem  = Ext.getCmp('views-panel').layout.activeItem;
    activeItem.select ? activeItem.select() : activeItem.getSelectionModel().clearSelections();
  }

  function revisionSuccess(response, options) {
    Ext.get(document.body).unmask();
	if (response.responseText.match (/^{/)) {
		// Process JSON response
		var json = Ext.decode(response.responseText);
		if (json.success === false) {
		  return Ext.MessageBox.error(Magellan.app.messages.error, Magellan.app.messages.errorOccured);
		}
		Ext.getCmp('history-grid').getStore().reload();
	}
	else {
		// Process HTML (probably exception page) response
		genericError (response.responseText);
	}
  }

  function historyEnableSuccess(response, options) {
    Ext.get(document.body).unmask();
	if (response.responseText.match (/^{/)) {
		// Process JSON response
		var json = Ext.decode(response.responseText);
		if (json.success === false) {
		  return Ext.MessageBox.error(Magellan.app.messages.error, Magellan.app.messages.errorOccured);
		}
		Ext.getCmp('history-window').hide ();
		reloadCurrentFolder();
	}
	else {
		// Process HTML (probably exception page) response
		genericError (response.responseText);
	}
  }

  function historyDisableSuccess(response, options) {
    Ext.get(document.body).unmask();
	if (response.responseText.match (/^{/)) {
		// Process JSON response
		var json = Ext.decode(response.responseText);
		if (json.success === false) {
		  return Ext.MessageBox.error(Magellan.app.messages.error, Magellan.app.messages.errorOccured);
		}
		Ext.getCmp('history-window').hide ();
		reloadCurrentFolder();
	}
	else {
		// Process HTML (probably exception page) response
		genericError (response.responseText);
	}
  }

  function deleteEntries(btn) {
    if (btn != 'yes') { return; }

	// Launch timer to alert user if operation is long
	launchLongOperationTimer ();
    var entries = getSelectedRecords();
    Ext.get(document.body).mask(Magellan.app.messages.deleting, 'x-mask-loading');
    Ext.Ajax.request({
      url: 'DeleteResources.json',
      params: { entries: entries.pluck('data').pluck('path') },
      scope: this,
      timeout: 3600000,
      success: fileOpSuccess,
      failure: ajaxFailure
    });
  }

  function copyOrCutResources(mode) {
    var entries = getSelectedRecords();
    var node    = treeView.getSelectionModel().getSelectedNode();

    clipboard.state = mode;
    clipboard.items = entries.pluck('data').pluck('path');
  }

  function pasteResources() {
    var msg   = (clipboard.state == 'cut') ? Magellan.app.messages.moving : Magellan.app.messages.copying;
    var node  = treeView.getSelectionModel().getSelectedNode();

	// Launch timer to alert user if operation is long
	launchLongOperationTimer ();
    Ext.get(document.body).mask(msg, 'x-mask-loading');
    Ext.Ajax.request({
      url: 'PasteResources.json',
      params: { entries: clipboard.items, mode: clipboard.state, destination: node.id, clipboard: true },
      timeout: 3600000,
      scope: this,
      success: fileOpSuccess,
      failure: ajaxFailure
    });
  }

  function downloadFiles() {
    var selectedNodes = getSelectedRecords();
    var currentNode   = treeView.getSelectionModel().getSelectedNode();
    var params        = {
                          entries: selectedNodes.pluck('data').pluck('path'),
                          path: currentNode.id
                        };

    window.location.href  = String.format("DownloadFiles?{0}", Ext.urlEncode(params));
  }

  function manageRights() {
    if (!rights_win) {
      rights_win = new Ext.Window({
          layout: 'fit',
          width: 800,
          height: 600,
          closeAction: 'hide',
          plain: false,
          title: Magellan.app.messages.manageRights,
          constrainHeader: true,

          items: [
            new Ext.Panel({
              applyTo: 'rights_mgt',
              height: 600,
              id: 'rights_panel'
            })
          ],

          buttons: [ {
            text: Magellan.app.messages.close,
            handler: function() {
              rights_win.hide();
              Ext.get('rights_iframe').dom.src = '';
              reloadCurrentFolder();
            }
          } ]
      });
      Ext.get('rights_mgt').setDisplayed(true);
    }

    var records = getSelectedRecords();
    var path    = records.length ? records.first().data.path : treeView.getSelectionModel().getSelectedNode().id;
	var data = records.length ? records.first().data : treeView.getSelectionModel().getSelectedNode().attributes;
    Ext.get('rights_iframe').dom.src  = String.format("../../" + data.group + "/Magellan/AccessRights?uri={0}&mode=edit_acl&no_header=1", path.replace('&', '%26').replace('#', '%23'));
    rights_win.show();
  }

  function setProperties () {
    if (!props_win) {

      props_win = new Ext.Window({
          layout: 'fit',
          width: Ext.getBody().getViewSize().width * 0.8,
          height: Ext.getBody().getViewSize().height * 0.6,
          closeAction: 'hide',
          plain: false,
          title: Magellan.app.messages.setProperties,
          constrainHeader: true,

          items: [
            new Ext.Panel({
              applyTo: 'props_mgt',
              height: 200,
              id: 'props_panel'
            })
          ],

          buttons: [ {
            text: Magellan.app.messages.apply,
            handler: function() {
				var records = getSelectedRecords();
				var data = jQuery ('#file-props-form').serializeObject ();

				// Add URIs to data for POST
				data.uri = new Array ();
				var group = records.first().data.group;
				records.each (function (rec) {
					data.uri.push (rec.data.path);
				});

				// Post data
				Ext.Ajax.request({
				  url: '../../' + group + '/Magellan/SetProperties.json',
				  params: data,
				  timeout: 3600000,
				  scope: this,
				  success: function () {
					  props_win.hide();
					  reloadCurrentFolder();
				  },
				  failure: ajaxFailure
				});
            }},
			{
            text: Magellan.app.messages.close,
            handler: function() {
              props_win.hide();
              reloadCurrentFolder();
            }
          } ]
      });
      Ext.get('props_mgt').setDisplayed(true);
    }
    props_win.show();

	var records = getSelectedRecords();
	var multiple = (records.length > 1);

	if (multiple) {
		props_win.setTitle(Magellan.app.messages.setProperties);
	}
	else {
		props_win.setTitle(String.format(Magellan.app.messages.setPropertiesForSingleFile, records[0].data.name));
	}

	// Initialize tags / description
	//     - if a single element is selected, get data from element properties
	//     - if multiple elements are selected, use empty data
	var tags = (!multiple && records.first().data.properties) ? records.first().data.properties.tag : new Array ();
	var description = (!multiple && records.first().data.properties) ? records.first ().data.properties.description : '';
	if (description) {
		description = description.replace(/\\n/g, "\n").replace (/\\r/g, "");
	}

	if (multiple) {
		// If multiple elements are selected, disable description field and show right pane
		jQuery ('#props_mgt').find ('textarea[name=description]').attr ('disabled', true);
		jQuery ('#props_mgt hr').show ();
		jQuery ('#override-description').removeAttr ('checked');
		jQuery ('td.properties-notice').show ();
		jQuery ('#properties-multiple').show ();
		jQuery ('#properties-multiple ul').html ('');
		records.each (function (rec) {
			jQuery ('#properties-multiple ul').append ('<li>' + rec.data.name + '</li>');
		});
		jQuery ('#props_mgt').find ('textarea[name=description]').attr ('disabled', '');
		jQuery ('#props_mgt').find ('textarea[name=description]').val ('');
	}
	else {
		// If single element is selected, enable description field and hide right pane
		jQuery ('#props_mgt').find ('textarea[name=description]').removeAttr ('disabled');
		jQuery ('#props_mgt hr').hide ();
		jQuery ('td.properties-notice').hide ();
		jQuery ('#properties-multiple').hide ();
		jQuery ('#props_mgt').find ('textarea[name=description]').removeAttr ('disabled');
		jQuery ('#props_mgt').find ('textarea[name=description]').val ('');
		jQuery ('#mode-replace-tag').attr ('checked', true);
	}

	// Initialize tag picker
	jQuery ('#props_mgt').find ('textarea[name=description]').val (description);
	var obj = jQuery('#tag-container').tags ({
		available: available_tags,
		free: free_tags,
		tags: tags,
		warning: tag_does_not_exist
	});
  }

  function editResource() {
    if (!edit_win) {
      edit_win = new Ext.Window({
          layout: 'fit',
          width: Ext.getBody().getViewSize().width * 0.6,
          height: Ext.getBody().getViewSize().height * 0.8,
          closeAction: 'hide',
          shim: false,
          id: 'edit_window',
          constrainHeader: true,

          items: [
            new Ext.form.FormPanel({
              frame: true,
              labelSeparator: '&nbsp;:',
              labelWidth: 150,
              labelAlign: 'top',
              autoScroll: false,
              url: 'EditResource.json',
              id: 'edit_form',
              items: [ {
                xtype: 'textarea',
                id: 'content',
                name: 'content',
                fieldLabel: Magellan.app.messages.content,
                anchor: '-20 -50'
              }, {
                xtype: 'hidden',
                id: 'edit_resource',
                name: 'resource'
              } ]
            }) ],

          buttons: [ {
            text: Magellan.app.messages.save,
            handler: editSubmit
          }, {
            text: Magellan.app.messages.close,
            handler: function() {
              edit_win.hide();
              Ext.getCmp('content').reset();
            }
          } ]
      });
      edit_win.render(document.body);
    }
    var record      = getSelectedRecords().first();
    edit_win.record = record;
    edit_win.setTitle(String.format(Magellan.app.messages.editingFile, record.data.name));
    edit_win.items.first().load({
      url: 'GetResource.json',
      params: { resource: record.data.path },
      scope: this,
      method: 'POST',
      success: function() {
          edit_win.show();
      },
      failure: function(frm, action) {
		genericError (action.result.error);
      }
    });
  }

  function uploadFile() {
    upload_win.show();
  }

  function downloadRevision() {
    var record  = Ext.getCmp('history-grid').getSelectionModel().getSelected();

    window.location.href  = String.format("DownloadRevision?{0}",
                                          Ext.urlEncode({
                                            resource: history_win.record.data.path,
                                            revision: record.data.rowid
                                          }));
  }

  function deleteRevision() {
    var record  = Ext.getCmp('history-grid').getSelectionModel().getSelected();

    Ext.get(document.body).mask(Magellan.app.messages.deleting, 'x-mask-loading');
    Ext.Ajax.request({
      url: 'DeleteRevision.json',
      params: { resource: history_win.record.data.path, revision: record.data.rowid },
      scope: this,
      success: revisionSuccess,
      failure: ajaxFailure
    });
  }

  function viewRevisions() {
    if (!history_win) {
      history_win = new Ext.Window({
          layout: 'fit',
          width: 750,
          height: 400,
          closeAction: 'hide',
          id: 'history-window',
          constrainHeader: true,

          items: new Ext.grid.GridPanel({
            id: 'history-grid',
            border: false,
            autoScroll: true,
            loadMask: true,
            viewConfig: {
              forceFit: true
            },
            stripeRows: true,
            sm: new Ext.grid.RowSelectionModel({
              singleSelect: true
            }),
            ds: new Ext.data.JsonStore({
              url: 'GetHistory.json',
              root: 'history',
              fields: [
                  {name: 'modified', mapping: 'datetime', type: 'date', dateFormat: 'Y-m-d H:i:s'},
                  {name: 'can_manage', type: 'boolean'},
                  'username',
                  'ident',
                  'rowid',
                  'old_name',
                  'type'
              ],
              sortInfo: {field: 'modified', direction: 'DESC'}
            }),
            cm: new Ext.grid.ColumnModel([
              {header: Magellan.app.messages.modificationType, width: 300, dataIndex: 'type', renderer: renderModification},
              {header: Magellan.app.messages.modifiedOn, width: 120, dataIndex: 'modified', renderer: Ext.util.Format.dateRenderer(Magellan.app.messages.dateFormat)},
              {header: Magellan.app.messages.modifiedBy, width: 150, dataIndex: 'username'}
            ]),
            tbar: [ {
              id: 'history-download',
              text: Magellan.app.messages.download,
              tooltip: { title: Magellan.app.messages.download,
                         text: Magellan.app.messages.downloadRevision
                       },
              icon: String.format("{0}/16x16/actions/download.png", image_path),
              disabled: true,
              scope: this,
              cls: 'x-btn-text-icon',
              handler: downloadRevision
            }, '-', {
              id: 'history-delete',
              text: Magellan.app.messages.Delete,
              tooltip: { title: Magellan.app.messages.Delete,
                         text: Magellan.app.messages.deleteRevision
                       },
              icon: String.format("{0}/16x16/actions/edit-delete.png", image_path),
              disabled: true,
              scope: this,
              cls: 'x-btn-text-icon',
              handler: function () {
                Ext.MessageBox.confirm(Magellan.app.messages.deleteRevisionTitle,
                                       Magellan.app.messages.confirmRevisionDeletion,
                                       function (btn) {
                                        if (btn == 'yes') { deleteRevision(); }
                                       });
              }
            }, '-', {
				id: 'history-disable',
				text: Magellan.app.messages.DisableHistory,
				tooltip: {
					title: Magellan.app.messages.DisableHistory,
					text: Magellan.app.messages.DisableHistoryText
				},
				icon: String.format("{0}/16x16/actions/dialog-cancel.png", image_path),
				disabled: true,
				scope: this,
				cls: 'x-btn-text-icon',
				handler: function () {
					Ext.MessageBox.confirm(Magellan.app.messages.disableHistoryTitle,
										   Magellan.app.messages.confirmHistoryDisable,
										   function (btn) {
											if (btn == 'yes') { disableHistory(); }
										   });
				}
			} ]
          }),

          buttons: [ {
            text: Magellan.app.messages.close,
            handler: function() { history_win.hide(); }
          } ]
      });
      var history_grid  = Ext.getCmp('history-grid');
      history_grid.getColumnModel().defaultSortable = true;
      history_grid.getSelectionModel().on('selectionchange', toggleHistoryButtons);
    }
    var record  = getSelectedRecords().first();
    history_win.setTitle(String.format(Magellan.app.messages.fileHistory, record.data.name));
    history_win.record  = record;
    history_win.show();
    var disBtn  = Ext.getCmp('history-disable');
	if (record.data.can_set_history) {
		disBtn.enable ();
	}
	else {
		disBtn.disable ();
	}
    Ext.getCmp('history-grid').store.load({params: {resource: record.data.path} });
  }

  function enableHistory () {
    var record  = getSelectedRecords().first();
    Ext.Ajax.request({
      url: 'EnableHistory.json',
      params: { resource: record.data.path },
      scope: this,
      success: historyEnableSuccess,
      failure: ajaxFailure
    });
  }

  function disableHistory () {
    var record  = getSelectedRecords().first();
    Ext.Ajax.request({
      url: 'DisableHistory.json',
      params: { resource: record.data.path },
      scope: this,
      success: historyDisableSuccess,
      failure: ajaxFailure
    });
  }

  function doUpload(btn) {
	var decompress  = (btn == 'yes') ? true : false;
    var file        = Ext.getCmp('file_upload');

    var currentPath = treeView.getSelectionModel().getSelectedNode().id;
    Ext.getCmp('destination').setValue(currentPath);
    Ext.getCmp('upload_form').form.submit({
      waitMsg: Magellan.app.messages.uploadingFile,
      waitTitle: Magellan.app.messages.waitMessage,
      reset: false,
      failure: function(form, action) {
        Ext.MessageBox.error(Magellan.app.messages.error, String.format(Magellan.app.messages.fileError,
                                                     action.result.errors));
      },
      success: function(form, action) {
        var success = action.result.success;
        if (success === false) {
		  detailedError (action.result);
        }
        else {
			var filename = Ext.getCmp('file_upload').getValue ().replace (/^.*[\/\\]/, '');
          upload_win.hide();
          file.reset();
          if (decompress) {
			var disk_path = action.result.disk_path.replace (/[^\/]*$/, filename);
            extractArchive(action.result.path, action.result.filename, disk_path);
          }
          else {
            reloadCurrentFolder();
          }
        }
      }
    });
  }

  function editSubmit() {
    Ext.getCmp('edit_resource').setValue(edit_win.record.data.path);
    Ext.getCmp('edit_form').form.submit({
      waitMsg: Magellan.app.messages.modifyingFile,
      waitTitle: Magellan.app.messages.waitMessage,
      reset: false,
      failure: function(form, action) {
		Ext.MessageBox.show ({
			title: Magellan.app.messages.error,
			msg: String.format(Magellan.app.messages.errorDetails, action.result.error),
			buttons: Ext.Msg.OK,
			icon: Ext.MessageBox.ERROR,
			minWidth: 500	// Workaround so that the box is wide enough in Chromium
		});
      },
      success: function(form, action) {
        var success = action.result.success;
        if (success === false) {
			detailedError (action.result);
        }
        else {
          edit_win.hide();
          Ext.getCmp('content').reset();
          reloadCurrentFolder();
        }
      }
    });
  }

  function uploadSubmit() {
    var file        = Ext.getCmp('file_upload');
    
    if (file.getValue().match(/(zip)$/i)) {
      Ext.MessageBox.confirm(Magellan.app.messages.decompressArchive,
                             Magellan.app.messages.autoDecompress,
                             doUpload);
    }
    else {
        var currentNode = treeView.getSelectionModel().getSelectedNode();
        var nodes       = iconView.getNodes();
        name  = file.getValue().trim();
    	if ((nodes.size () > 0) && nodes.any (function (node) { return (node && node.id == name); })) {
    		return Ext.Msg.show({
    			title: Magellan.app.messages.fileAlreadyExists,
	    		msg: Magellan.app.messages.confirmreplaceFileByThis,
	    		width: 500,
	    		prompt : false,
	    		buttons: Ext.MessageBox.OKCANCEL,
	    		fn: function (btn) {
	    			if (btn == "ok") {
	    				doUpload ('no');
	    			}
	    			else {
	    			}
	    		}
	    	});
	    }
    	else {
    		doUpload('no');
    	}
    }
  }

  function extractArchive(path, filename, disk_path) {
    extract_progress  = Ext.MessageBox.progress(String.format(Magellan.app.messages.fileDecompression, filename),
                                                '',
                                                Magellan.app.messages.waitingServer);

    var timeout = Ext.Ajax.timeout;
    Ext.Ajax.timeout  = 3600000; // Set to 1 hour
    Ext.Ajax.request({
      url: 'Extract.json',
      params: { arch_dir: path, arch_path: disk_path },
      scope: this,
      success: function (response, options) {
        var json    = Ext.decode(response.responseText);
        var success = json.success;

        extracting  = false;
        extract_progress.hide();
        extract_progress.updateProgress(0,' ',' ');
        Ext.Ajax.timeout  = timeout;
        if (!success) {
		  var filelist = '';
		  if (json.files) {
			  filelist += '<ul class="dav-errors">';
			  json.files.forEach (function (file) {
				filelist += '<li>' + file + '</li>';
			  });
			  filelist += '</ul>';
		  }
          return Ext.MessageBox.error(Magellan.app.messages.error,
                                      String.format('<div class="error-contents">' + Magellan.app.messages.decompressError + filelist + '</div>',
                                                    json.error),
                                      function () { reloadCurrentFolder (); });
        }
        reloadCurrentFolder();
      },
      failure: function (response, options) {
        extracting  = false;
        Ext.Ajax.timeout  = timeout;
        Ext.MessageBox.error(Magellan.app.messages.error, Magellan.app.messages.serverError);
      }
    });
    extracting  = true;
    updateExtractProgress.defer(1500);
  }

  function updateExtractProgress() {
    Ext.Ajax.request({
      url: 'MonitorDecompress.json',
      scope: this,
      success: function (response, options) {
        var json                   = Ext.decode(response.responseText);
        if (!json || (json.progress_infos && (json.progress_infos.error === true))) {
          extracting  = false;
          return;
        }
        var infos = json.progress_infos;

        if (extracting) {
          if (infos) {
            extract_progress.updateProgress(infos.percent_size / 100,
                                            String.format(Magellan.app.messages.fileProgress,
                                                          renderFileSize(infos.current_size),
                                                          renderFileSize(infos.total_size)),
                                            String.format(Magellan.app.messages.decompressCopy, infos.current_filename));
          }
          updateExtractProgress.defer(2500);
        }
        else { extract_progress.updateProgress(0,' ',' '); }
      },
      failure: function (response, options) {
        extracting  = false;
        Ext.Ajax.timeout  = timeout;
        Ext.MessageBox.error(Magellan.app.messages.error, Magellan.app.messages.serverError);
      }
    });
  }

  function sortByFolders(store, records, options) {
    if (store.sortInfo.field != 'name') { return; }

    store.data.sort(store.sortInfo.direction, function(r1, r2) {
      var t1  = r1.data['type'], t2 = r2.data['type'];
      var v1  = r1.data['name'].toLowerCase(), v2 = r2.data['name'].toLowerCase();
      var dir = store.sortInfo.direction == 'ASC' ? -1 : 1;

      if (t1 == 'directory' && t2 != 'directory') {
        return 1 * dir;
      }
      if (t1 != 'directory' && t2 == 'directory') {
        return -1 * dir;
      }
      return v1 > v2 ? -1 : (v1 < v2 ? 1 : 0);
    });
    store.fireEvent('datachanged', store);
  }

  function toggleLock() {
    var record  = getSelectedRecords().first();
    var msg, action;
    if (record.data.locked && !record.data.user_is_lock_owner) {
        Ext.MessageBox.confirm(Magellan.app.messages.forceUnlockTitle,
                               Magellan.app.messages.forceUnlockMsg,
							   doToggleLock
                               );
    }
    else {
        doToggleLock ('yes');
    }
  }

  function doToggleLock (btn) {
	if (btn == 'yes') {
        var record  = getSelectedRecords().first();
        var msg, action;
        if (record.data.locked) {
          msg     = Magellan.app.messages.unlocking;
          action  = "UnlockResource.json";
        }
        else {
          msg     = Magellan.app.messages.locking;
          action  = "LockResource.json";
        }

        Ext.get(document.body).mask(msg, 'x-mask-loading');
        Ext.Ajax.request({
          url: action,
          params: { resource: record.data.path },
          scope: this,
          success: fileOpSuccess,
          failure: ajaxFailure
        });
	}
  }

  function showVersionInformations() {
    Ext.MessageBox.show({
      buttons: Ext.MessageBox.OK,
      icon: 'mb-magellan',
      msg: String.format(Magellan.app.messages.version, version, codename),
      title: 'Magellan'
    });
  }

  function setCurrentPath(path) {
    var instances = Ext.state.Manager.get('instances', {});

    if (!instances[rootString]) {
      instances[rootString] = {};
    }
    instances[rootString].currentPath = path;
    Ext.state.Manager.set('instances', instances);
  }

  function getCurrentPath(isUserApp) {
    var instances         = Ext.state.Manager.get('instances', {});
    var current_instance  = instances[rootString];
    var path              = homePath;

    if (current_instance && current_instance.currentPath && (current_instance.currentPath.match (path) || isUserApp)) {
      path  = current_instance.currentPath;
    }
    return path;
  }

  function mailFiles() {
    var selectedNodes = getSelectedRecords();
    var currentNode   = treeView.getSelectionModel().getSelectedNode();
    var params        = {
                          entries: selectedNodes.pluck('data').pluck('path'),
                          path: currentNode.id,
                          from_magellan: 1,
						  no_header: 1
                        };
    var nodeGroup = getSelectedRecords().first ().data.group;

    window.location.href  = String.format("../../" + nodeGroup + "/Mermoz/DisplayMain?{0}", Ext.urlEncode(params));
  }

  // LOUVRE START
  function addGallery(regenerate) {
    var currentNode   = treeView.getSelectionModel().getSelectedNode();
    var selectedNodes = getSelectedRecords();
    var path;
    var need_full_reload;
    if (selectedNodes.length === 0) {
      path = currentNode.id;
      need_full_reload = true; // because called by context menu
    } else {
      path = selectedNodes.pluck('data').pluck('path');
      need_full_reload = false;
    }
	// Bug #1473 callback to menu item click now passes an argument, force "regenerate" value to something consistent
    if (typeof (regenerate) !== 'number') {
		regenerate = 0;
    }
    var params        = { dir: path, regenerate: regenerate };
	var nodeGroup     = currentNode.attributes.group;
	if (regenerate === 1) {
		louvre_win.setTitle(Magellan.app.messages.recreateGallery);
	}
	else {
		louvre_win.setTitle(Magellan.app.messages.addGallery);
	}
    louvre_win.showURL(String.format("../../" + nodeGroup + "/Louvre/CreateGallery?{0}", Ext.urlEncode(params)),
    	need_full_reload);
  }

  function showGallery() {
	var selectedNodes = getSelectedRecords();
	var path = (selectedNodes.length === 0
		? treeView.getSelectionModel().getSelectedNode().id
		: selectedNodes.first().data.path);
	window.open(galleryForPath(path));
  }

  function galleryForPath(path) {
    return path+"/mioga2-louvre/index.html";
  }

  function removeGallery() {
	var currentNode   = treeView.getSelectionModel().getSelectedNode();
    var selectedNodes = getSelectedRecords();
    var path;
    var need_full_reload;
    if (selectedNodes.length === 0) {
      path = currentNode.id;
      need_full_reload = true; // called by context menu
    } else {
      path = selectedNodes.pluck('data').pluck('path');
      need_full_reload = false;
    }
    var params        = { dir: path };
	var nodeGroup = currentNode.attributes.group;
	louvre_win.setTitle(Magellan.app.messages.removeGallery);
	louvre_win.showURL(String.format("../../" + nodeGroup + "/Louvre/RemoveGallery?{0}", Ext.urlEncode(params)),
		need_full_reload);
  }

  function recreateGallery () {
  	addGallery (1);
  }
  // LOUVRE END

	// Diderot start
	function switchToDiderot () {
    	var selectedNodes = getSelectedRecords();
		var nodes = selectedNodes.pluck('data').pluck('path').toArray ();
		var url = '../Diderot/DisplayMain#Diderot-attach?files=' + nodes.join ('&files=');
		window.location.href = url;
	}
	// Diderot end

  /***************
   * Error boxes *
   ***************/

  // Generic error
  function genericError (error, func) {
	var errorstr = '<div class="error-contents"><p>' + Magellan.app.messages.genericErrorHeader + '</p><p class="error-message">' + error + '</p><p>' + Magellan.app.messages.reportErrorFooter + '</p></div>';

	// Initialize empty callback if none given
	if (!func) {
	  func = function () {};
	}

	// Create message box
	Ext.MessageBox.show ({
		title: Magellan.app.messages.error,
		msg: errorstr,
		buttons: Ext.Msg.OK,
		icon: Ext.MessageBox.ERROR,
		fn: func,
		minWidth: 500	// Workaround so that the box is wide enough in Chromium
	});
  }

  // Detailed error
  function detailedError (json, header, func) {
  	  var errorstr = (header ? header : '<div class="error-contents">' + Magellan.app.messages.genericErrorHeader);

	  // Initialize error string
	  errorstr += '<p>' + Magellan.app.messages.errors + '</p>';
	  if (json.errors) {
		  // Create string from a list of file => HTTP code elements
		  errorstr += '<ul class="dav-errors">';
		  json.errors.each (function (item) {
			errorstr += '<li>';
			errorstr += item.uri + " " + '<span class="dav-error-reason">' + Magellan.app.messages['davError'+item.code] + '</span>';
			errorstr += '</li>';
		  });
		  errorstr += '</ul>';
	  }
	  else {
	  	// Place string from server as-is
		errorstr += '<p>' + json.error + '</p>';
	  }
	  errorstr += "</div>";

	  // Initialize empty callback if none given
	  if (!func) {
	  	func = function () {};
	  }

	  // Create message box
	  Ext.MessageBox.show ({
	  	title: Magellan.app.messages.error,
		msg: errorstr,
		buttons: Ext.Msg.OK,
		icon: Ext.MessageBox.ERROR,
		fn: func,
		minWidth: 500	// Workaround so that the box is wide enough in Chromium
	  });
  }

  // public space
  /** @scope Magellan.app */
  return {
    // public properties, e.g. strings to translate
    messages: {
      lock: "Lock",
      unlock: "Unlock",
      dateFormat: "m/d/Y h:i a",
      extraInfos: "{0} elements - {1} folder(s) - {2} file(s)",
      historyEdit: "File modification",
      historyRename: "Name modification (previously was {0})",
      byteSymbol: "B",
      bytes: "bytes",
      read: "Read",
      readWrite: "Read/Write",
      none: "None",
      elements: "{0} elements",
      moving: "Moving...",
      copying: "Copying...",
      creatingFolder: "Creating new folder",
      nameForFolder: "Please provide a name for the new folder",
      error: "Error",
      fileAlreadyExists: "A file or folder with this name already exists.",
      confirmreplaceFileByThis: "Do you want replaces the existing file or folder by this ?",
      creating: "Creating...",
      serverError: "Communication error with server occured.",
      creatingFile: "Creating new file",
      nameForFile: "Please provide a name for the new file",
      renaming: "Renaming...",
      errorDetails: "<p>The following error occured:</p><p class='error-message'>{0}</p><p>Please report this message to your Mioga2 administrator.</p>",
      errorOccured: "Error has occured.",
	  genericErrorHeader: "Operation failed.",
	  cutErrorHeader: "Moving failed.",
	  copyErrorHeader: "Copying failed.",
	  renameErrorHeader: "Renaming failed.",
	  dbProblemHeader: "An internal inconsistency has been fixed.",
	  reportErrorFooter: "Please report this message to your Mioga2 administrator.",
	  clipboardSource: "Source: ",
	  clipboardDestination: "Destination: ",
	  errors: "Errors: ",
	  davError400: " is already in target folder",
	  davError403: " is not writeable",
	  davError423: " is locked",
	  davError500: " caused an internal server error, please report this to your Mioga2 administrator",
	  davErrorDBProblem: " was containing elements that were not correctly registered into Mioga2 database, which has been fixed, please report this to your Mioga2 administrator so he can check the access rights.",
	  serverMessage: "Server message: ",
      deleting: "Deleting...",
      manageRights: "Manage rights",
      close: 'Close',
      save: 'Save',
      editingFile: "Editing {0}",
      modificationType: "Modification type",
      modifiedOn: "Modified on",
      modifiedBy: "Modified by",
      download: "Download",
      downloadRevision: "Download selected revision",
      Delete: "Delete",
      deleteRevision: "Delete selected revision",
      deleteRevisionTitle: "Delete revision",
      confirmRevisionDeletion: "Are you sure you want to delete this revision?",
      fileHistory: "History for {0} file",
      uploadingFile: "Uploading file...",
      waitMessage: "Please wait...",
      fileError: "Error occured while uploading file:<br/>{0}",
      modifyingFile: "Modifying file...",
      decompressArchive: "Decompress archive",
      autoDecompress: "File that you are about to upload seems to be a compressed archive. Do you want this file to be automatically decompressed when it arrives on server?",
      fileDecompression: "Decompressing {0}",
      waitingServer: "Waiting server",
      decompressError: "Error while decompressing archive:<br/>{0}",
      fileProgress: "{0} / {1} done",
      decompressCopy: "Copying {0}",
      locking: "Locking...",
      unlocking: "Unlocking...",
      version: "Version: {0} ({1})",
      previous: "Previous",
      previousTip: "Go to previous folder",
      next: "Next",
      nextTip: "Go to next folder",
      parentFolder: "Parent folder",
      parentFolderTip: "Go to parent folder",
      homeFolder: "Home folder",
      homeFolderTip: "Go to home folder",
      reload: "Reload",
      reloadTip: "Reload current folder",
      display: "Display",
      displayTip: "Change display mode",
      icons: "Icons",
      detailedList: "Detailed list",
      about: "About",
      noFile: "No file",
      loading: "Loading...",
      selectedElements: "{0} selected element{1}",
      name: "Name",
      size: "Size",
      accessRights: "Access rights",
      copy: "Copy",
      cut: "Move",
      paste: "Paste",
      cancel: "Cancel",
      createNew: "Create a new",
      folder: "Folder",
      file: "File",
      rename: "Rename",
      renameTip: "Please provide a new name",
      confirmDelete: "Are you sure you want to delete these elements?",
      history: "History",
      edit: "Edit",
      uploadFile: "Upload file",
      downloadSelection: "Download current selection",
      separator: ":",
      fileToUpload: "File to upload",
      send: "Send",
      content: "Content",
      mailSelection: "Mail link to current selection",
      addComment: "Add a comment",
      viewComments: "View comments",
      commentHeader: "From {0} on {1}:",
      noComment: "No comment",
      commentFileTitle: "Comment(s) of file {0}",
      comment: "Comment",
	  forceUnlockTitle: "Unlock a file you didn't lock",
	  forceUnlockMsg: "The file you are trying to unlock was locked by someone else. If another user is currently writing to this file, it may get corrupted. Are you sure you want to unlock it ?",
	  addGallery: "Convert into gallery",
	  showGallery: "Show gallery",
	  removeGallery: "Remove gallery",
	  long_operation_text: "The operation you requested is long but is still running. Please do not close your Web browser, which would stop the operation.",
	  setProperties: "Set properties",
	  setPropertiesForSingleFile: "Set properties for file {0}",
	  apply: "Apply",
	  recreateGallery: "Modify gallery parameters",
	  comments: "comment(s)",
	  no_comment: "No comment",
	  attachSelectionToNotice: "Switch to Diderot and attach selection to a notice",
	disableHistoryTitle: "Disable history ?",
	confirmHistoryDisable: "Are you sure you want to disable history for this element (and its children) ?",
	DisableHistory: "Disable history",
	DisableHistoryText: "Disable history for this element (and its children)",
	enableHistory: "Enable history"
    },

    // public methods
    /**
	 * Initialize Magellan
	 *
	 * @constructor
	 * @param {object}
	 *            options Options needed to properly initialize application
	 * @config {string} rootString Name for the root node. Usually the current
	 *         instance name.
	 * @config {string} homePath Path of user's home.
	 * @config {string} basePath Path for the root node.
	 * @config {string} version Version of the application.
	 * @config {string} codename Codename of the application.
	 * @config {string} [forcedPath] Path to display when loading application
	 *         instead of default path.
	 */
    init: function(options) {
      Ext.MessageBox.minProgressWidth = 800;

      rootString      = options.rootString;
      homePath        = options.homePath;
      basePath        = options.basePath;
      version         = options.version;
      codename        = options.codename;
      canSendMail     = options.canSendMail;
	  viewMode        = (options.viewMode != '') ? options.viewMode : 'icon';
      var forcedPath  = options.forcedPath;
      var isUserApp   = options.isUserApp;

      Ext.state.Manager.setProvider(new Ext.state.CookieProvider({secure: Ext.isSecure}));
      Ext.QuickTips.init();

      var toolbar = new Ext.Toolbar({
        id: 'toolbar',
        region:'north',
        border: false,
        margins: {top: no_header ? 0 : 28},
		height: 24,
        items: [
          {
            xtype: 'splitbutton',
            icon: String.format("{0}/16x16/actions/go-previous.png", image_path),
            cls: "x-btn-icon",
            id: 'previous',
            tooltip: { title: this.messages.previous,
                       text: this.messages.previousTip
                     },
            scope: this,
            disabled: true,
            handler: goToPrevious,
            menu: {
              id: 'previousMenu',
              items: []
            }
          }, {
            xtype: 'splitbutton',
            icon: String.format("{0}/16x16/actions/go-next.png", image_path),
            cls: 'x-btn-icon',
            id: 'next',
            tooltip: { title: this.messages.next,
                       text: this.messages.nextTip
                     },
            scope: this,
            disabled: true,
            handler: goToNext,
            menu: {
              id: 'nextMenu',
              items: []
            }
          }, {
            xtype: 'button',
            icon: String.format("{0}/16x16/actions/go-up.png", image_path),
            cls: 'x-btn-icon',
            id: 'up',
            tooltip: { title: this.messages.parentFolder,
                       text: this.messages.parentFolderTip
                     },
            scope: this,
            disabled: true,
            handler: goToParentNode
          }, {
            xtype: 'button',
            icon: String.format("{0}/16x16/places/user-home.png", image_path),
            cls: 'x-btn-icon',
            id: 'home',
            tooltip: { title: this.messages.homeFolder,
                       text: this.messages.homeFolderTip
                     },
            scope: this,
            handler: goToHome
          }, {
            xtype: 'button',
            icon: String.format("{0}/16x16/actions/view-refresh.png", image_path),
            cls: 'x-btn-icon',
            id: 'reload',
            tooltip: { title: this.messages.reload,
                       text: this.messages.reloadTip
                     },
            scope: this,
            handler: function () { reloadCurrentFolder (); }
          }, '-', {
            xtype: 'button',
            icon: String.format("{0}/16x16/actions/view-choose.png", image_path),
            cls: 'x-btn-icon',
            id: 'views',
            scope: this,
            tooltip: { title: this.messages.display,
                       text: this.messages.displayTip
                     },
            menu: {
              id: 'viewsMenu',
              items: [ {
                text: this.messages.icons,
                icon: String.format("{0}/16x16/actions/view-list-icons.png", image_path),
                handler: setIconView
              }, {
                text: this.messages.detailedList,
                icon: String.format("{0}/16x16/actions/view-list-details.png", image_path),
                handler: setListView
              } ]
            }
          }, '-', {
            xtype: 'button',
            icon: String.format("{0}/16x16/emblems/emblem-info.png", image_path),
            cls: 'x-btn-icon',
            id: 'infos',
            tooltip: this.messages.about,
            scope: this,
            handler: showVersionInformations
          }
        ]
      });

      var store = new Ext.data.JsonStore({
          url: 'GetFullNodes.json',
          root: 'resources',
          fields: [
              {name: 'name', mapping: 'text'},
              {name: 'type', mapping: 'mimetype'},
              {name: 'size', type: 'int'},
              {name: 'path', mapping: 'id'},
              {name: 'modified', type: 'date', dateFormat: 'Y-m-d H:i:s'},
              {name: 'locked', type: 'boolean'},
              {name: 'can_set_history', type: 'boolean'},
              {name: 'group_has_history', type: 'boolean'},
              {name: 'history_enabled', type: 'boolean'},
              {name: 'history', type: 'boolean'},
              {name: 'manage', type: 'boolean'},
              {name: 'email', type: 'boolean'},
              {name: 'unlockable', type: 'boolean'},
              {name: 'user_is_lock_owner', type: 'boolean'},
			  {name: 'inconsistent', type: 'boolean'},
			  {name: 'setprops', type: 'boolean'},
              'louvre',
              'lock_owner',
              'author',
              'ident',
			  'group',
              'rights',
              'elements',
              'comments',
			  'properties'
          ],
          sortInfo: {field: 'name', direction: 'DESC'}
      });
      store.load({params: {files: 1, node: basePath} });

      metrics = Ext.util.TextMetrics.createInstance(Ext.get('metrics'));
      var tpl = new Ext.XTemplate(
          '<tpl for=".">',
              '<div class="thumb-wrap" id="{name}">',
                '<div class="thumb">',
                  '<img src="'+image_path+'/48x48/{[this.getSection(values)]}/{[this.getImage(values)]}.png" title="{name}">',
                  '{[this.displayLock(values)]}',
                '</div>',
              '<span class="x-editable">{[this.getName(values)]}</span></div>',
          '</tpl>',
          '<div class="x-clear"></div>', {
            getImage: function(values) {
        	  var image_for_mime = getImageForMime(values.type);
        	  if (image_for_mime === 'folder' &&
        		  typeof(values.louvre) !== 'undefined' && values.louvre.type === 'g') {
        		image_for_mime = 'folder-image';
        	  };
              return image_for_mime;
            },
            displayLock: function(values) {
              var html  = "";
              if (values.locked) {
                html  = String.format('<img src="{0}/16x16/status/object-locked.png" class="lock"/>', image_path);
              }
              return html;
            },
            getName: function(values) {
              return getNameForThumb(values.name);
            },
		    getSection: function (values) {
		      if (values.type === 'directory') {
			 	return 'places';
			  }
			  else {
			 	return 'mimetypes';
			  }
		   }
          }
      );
      var dataview  = new Ext.DataView({
        id: 'icon-view',
        cls: 'data-view',
        store: store,
        tpl: tpl.compile(),
        anchor: '100% 100%',
        multiSelect: true,
        overClass:'x-view-over',
        itemSelector:'div.thumb-wrap',
        emptyText: this.messages.noFile,
        loadingText: this.messages.loading,
        plugins: [
          new Ext.DataView.DragSelector()
        ]
      });
      var grid  = new Ext.grid.GridPanel({
        border: false,
        store: store,
        autoScroll: true,
        loadMask: true,
        id: 'list-view',
        enableDragDrop: true,
        ddText: this.messages.selectedElements,
        ddGroup: "TreeDD",
        columns: [
          {header: '', width: 30, dataIndex: 'locked', renderer: renderLock},
          {header: this.messages.name, width: 300, dataIndex: 'name', renderer: renderName},
          {header: this.messages.size, width: 90, dataIndex: 'size', renderer: renderFileSize},
          {header: this.messages.accessRights, width: 120, dataIndex: 'rights', renderer: renderRights},
          {header: this.messages.modifiedOn, width: 120, dataIndex: 'modified', renderer: Ext.util.Format.dateRenderer(this.messages.dateFormat)},
          {header: this.messages.modifiedBy, width: 150, dataIndex: 'author'}
        ]
      });
      grid.getColumnModel().defaultSortable = true;
      viewport  = new Ext.Viewport({
  	    layout: 'border',
  	    items: [
          toolbar, new Ext.Panel({
            region: 'center',
            layout: 'card',
            id: 'views-panel',
            activeItem: 0,
            items: [
              dataview, grid
            ]
          }), new Ext.Panel({
            contentEl: 'properties',
            height: 150,
            minSize: 150,
            collapsible: true,
            collapseMode: 'mini',
            split: true,
            region: 'south'
          }), {
            id: 'tree',
            xtype: 'treepanel',
            collapsible: true,
            collapseMode: 'mini',
            region: 'west',
            width: 200,
            split: true,
            autoScroll: true,
            enableDrop: true,
            animate: true,
            containerScroll: true,
            loader: new Ext.tree.TreeLoader({
              dataUrl: 'GetNodes.json'
            }),
            dropConfig: {
              allowParentInsert: false
            },
            root: new Ext.tree.AsyncTreeNode({
              draggable: false,
              id: basePath,
              text: rootString,
              expanded: true,
              rights: 'r',
              mimetype: 'directory'
            })
          }]
      });

      ddMenu  = new Ext.menu.Menu({
        items: [
          { text: this.messages.copy, icon: String.format("{0}/16x16/actions/edit-copy.png", image_path) },
          { text: this.messages.cut, icon: String.format("{0}/16x16/actions/edit-cut.png", image_path) },
          '-',
          { text: this.messages.cancel, icon: String.format("{0}/16x16/actions/dialog-cancel.png", image_path) }
        ]
      });

      /*
		 * Definition of elements for context menu
		 */

      addItem = {
        icon: String.format("{0}/16x16/actions/list-add.png", image_path),
        text: this.messages.createNew,
        scope: this,
        menu: {
          id: 'newMenu',
          items: [ {
            text: this.messages.folder,
            icon: String.format("{0}/16x16/mimetypes/inode-directory.png", image_path),
            handler: createNewFolder
          }, {
            text: this.messages.file,
            icon: String.format("{0}/16x16/mimetypes/unknown.png", image_path),
            handler: createNewFile
          } ]
        }
      };
      renameItem  = {
        icon: String.format("{0}/16x16/actions/edit-rename.png", image_path),
        text: this.messages.rename,
        scope: this,
        handler: function() {
            var elem = getSelectedRecords();
            var old_name = elem.pluck('data')[0].name || "";
	    	Ext.Msg.show({
	    		title: this.messages.rename,
	    		msg: this.messages.renameTip,
	    		width: 300,
	    		prompt : true,
	    		value : old_name,
	    		buttons: Ext.MessageBox.OKCANCEL,
	    		multiline: false,
	    		fn: renameEntry
	    	});
        }
      };
      deleteItem  = {
        icon: String.format("{0}/16x16/actions/edit-delete.png", image_path),
        text: this.messages.Delete,
        scope: this,
        handler: function() {
          Ext.MessageBox.confirm(this.messages.Delete,
                                 this.messages.confirmDelete,
                                 deleteEntries);
        }
      };
      // '-'
      cutItem = {
        icon: String.format("{0}/16x16/actions/edit-cut.png", image_path),
        text: this.messages.cut,
        scope: this,
        handler: copyOrCutResources.createCallback('cut')
      };
      copyItem  = {
        icon: String.format("{0}/16x16/actions/edit-copy.png", image_path),
        text: this.messages.copy,
        scope: this,
        handler: copyOrCutResources.createCallback('copy')
      };
      pasteItem = {
        icon: String.format("{0}/16x16/actions/edit-paste.png", image_path),
        text: this.messages.paste,
        scope: this,
        handler: pasteResources
      };
      // '-'
      historyItem = {
        icon: String.format("{0}/16x16/actions/view-history.png", image_path),
        text: this.messages.history,
        scope: this,
        handler: viewRevisions
      };
      historyEnableItem = {
        icon: String.format("{0}/16x16/actions/view-history.png", image_path),
        text: this.messages.enableHistory,
        scope: this,
        handler: enableHistory
      };
      editItem  = {
        icon: String.format("{0}/16x16/actions/document-edit.png", image_path),
        text: this.messages.edit,
        scope: this,
        handler: editResource
      };
      lockItem  = {
        scope: this,
        handler: toggleLock
      };
      rightsItem  = {
        icon: String.format("{0}/16x16/actions/edit-security.png", image_path),
        text: this.messages.manageRights,
        scope: this,
        handler: manageRights
      };
      propsItem  = {
        icon: String.format("{0}/16x16/actions/document-properties.png", image_path),
        text: this.messages.setProperties,
        scope: this,
        handler: setProperties
      };
      // '-'
      uploadItem  = {
        icon: String.format("{0}/16x16/actions/upload.png", image_path),
        text: this.messages.uploadFile,
        scope: this,
        handler: uploadFile
      };
      downloadItem  = {
        icon: String.format("{0}/16x16/actions/download.png", image_path),
        text: this.messages.downloadSelection,
        scope: this,
        handler: downloadFiles
      };
	  attachToNoticeItem = {
	  	icon: String.format("{0}/16x16/actions/documentation.png", image_path),
        text: this.messages.attachSelectionToNotice,
        scope: this,
        handler: switchToDiderot
	  };
      // '-'
      mailItem = {
        icon: String.format("{0}/16x16/actions/mail-send.png", image_path),
        text: this.messages.mailSelection,
        scope: this,
        handler: mailFiles
      };
      addGalleryItem = {
        icon: String.format("{0}/16x16/mimetypes/image-x-generic.png", image_path),
        text: this.messages.addGallery,
        scope: this,
        handler: addGallery
      };
      showGalleryItem = {
    	icon: String.format("{0}/16x16/mimetypes/image-x-generic.png", image_path),
    	text: this.messages.showGallery,
    	scope: this,
    	handler: showGallery
      };
      removeGalleryItem = {
    	icon: String.format("{0}/16x16/mimetypes/image-x-generic.png", image_path),
    	text: this.messages.removeGallery,
    	scope: this,
    	handler: removeGallery
      };
	  recreateGalleryItem = {
    	icon: String.format("{0}/16x16/mimetypes/image-x-generic.png", image_path),
    	text: this.messages.recreateGallery,
    	scope: this,
    	handler: recreateGallery
	  };

      /*
		 * End of context menu elements
		 */

      // get objects that'll be reused

      iconView    = Ext.getCmp('icon-view');
      treeView    = Ext.getCmp('tree');
      listView    = Ext.getCmp('list-view');

      properties          = Ext.get('properties');
      prop_extra_infos    = Ext.get('prop_extra_infos');
      prop_name           = Ext.get('prop_name');
      prop_mimetype       = Ext.get('prop_type');
      prop_size           = Ext.get('prop_size');
      prop_modified       = Ext.get('prop_modified');
      prop_rights         = Ext.get('prop_rights');
      prop_author         = Ext.get('prop_author');
      prop_locked         = Ext.get('prop_lock');
      prop_lock_owner     = Ext.get('prop_lock_owner');
      prop_lock_owner_val = Ext.get('prop_lock_owner_val');
      prop_comments       = Ext.get('prop_comments');
      prop_tags           = Ext.get('prop_tags');
      prop_description    = Ext.get('prop_description');

      treeView.dropZone.onNodeDrop  = function(nodeData, source, e, data) {
        var nodes   = data.selections.collect(function(r) { return {record: r}; });
        targetNode  = nodeData.node;

        return menuDrop(nodes, targetNode, e);
      };

      upload_win = new Ext.Window({
          layout: 'fit',
          width: 450,
          height: 140,
          closeAction: 'hide',
          plain: false,
          title: this.messages.uploadFile,
          constrainHeader: true,

          items: [
            new Ext.form.FormPanel({
              frame: true,
              fileUpload: true,
              labelSeparator: this.messages.separator,
              labelWidth: 120,
              autoScroll: true,
              url: 'UploadFile.json',
              id: 'upload_form',
              items: [ {
                xtype: 'textfield',
                id: 'file_upload',
                name: 'file',
                inputType: 'file',
                fieldLabel: this.messages.fileToUpload
              }, {
                xtype: 'hidden',
                id: 'destination',
                name: 'destination'
              }, {
                xtype: 'hidden',
                id: 'X-CSRF-Token',
                name: 'X-CSRF-Token',
                value: getCookie("X-CSRF-Token")
              }]
            }) ],

          buttons: [ {
            text: this.messages.send,
            handler: uploadSubmit
          }, {
            text: this.messages.close,
            handler: function() { upload_win.hide(); }
          } ]
      });

      var sm  = treeView.getSelectionModel();
      sm.on('selectionchange', displayFullNode);
      sm.on('selectionchange', updateHistory);
      sm.on('selectionchange', toggleNavigationButtons);

      dataview.on('contextmenu', function(dv, idx, node, e) {
        e.preventDefault();
        e.stopEvent();

        if (!dv.isSelected(node)) {
          dv.select(node);
        }
        toggleContextMenu(e);
      });
      dataview.on('dblclick', dataViewActionDblClick);
      dataview.on('selectionchange', dataViewActionSC);
      dataview.store.on('load', sortByFolders);
      dataview.store.on('load', dataViewAddDnD);
      dataview.store.on('load', checkConsistency);

      grid.on('rowdblclick', gridActionDblClick);
      grid.on('headerclick', sortByFolders.createCallback(grid.store));
      grid.getSelectionModel().on('selectionchange', gridActionSC);
      grid.on('rowcontextmenu', function(grid, idx, e) {
        e.preventDefault();
        e.stopEvent();

        if (!grid.getSelectionModel().hasSelection()) {
          grid.getSelectionModel().selectRow(idx);
        }
        toggleContextMenu(e);
      });
      grid.store.on('load', gridAddDnD);

      Ext.getCmp('views-panel').layout.setActiveItem(Ext.state.Manager.get('current-view', 0));
      Ext.getCmp('views-panel').getEl().on('contextmenu', function(e) {
        e.preventDefault();
        e.stopEvent();
        deselectAll();
        toggleContextMenu(e);
      });
      expandTreeTo(forcedPath ? forcedPath : getCurrentPath(isUserApp));

      louvre_win = new Ext.Window({
        layout: 'fit',
        width: Ext.getBody().getViewSize().width * 0.8,
        height: Ext.getBody().getViewSize().height * 0.6,
        closeAction: 'hide',
        plain: false,
        title: this.messages.addGallery,
        constrainHeader: true,
        need_full_reload: false,

        finished_observer_task: {
    	  run: function() {
    	    var ifr = document.getElementById('iframe_louvre');
    	    if (typeof(ifr.contentWindow.document.has_finished) !== 'undefined') {
    	   	  var ilOK = Ext.getCmp('iframe_louvre_OK');
    	   	  var il = ilOK.ownerCt;
			  il.hide ();
    	    }
      	  },
      	  interval: 1000
        },
        finished_observer_task_is_running: 0,

        timeout_close_task: null,

      	listeners: {
       	  'hide' : function() {
        	if (this.finished_observer_task_is_running) {
        	  Ext.TaskMgr.stop(this.finished_observer_task);
        	  this.finished_observer_task_is_running = 0;
        	}
       	    if (this.timeout_close_task) {
       	      this.timeout_close_task.cancel();
       	      this.timeout_close_task = null;
       	    }
       	    if (this.need_full_reload) {
       		  window.location.reload();
       		} else {
       		  reloadCurrentFolder();
       		}
       		var ifr = document.getElementById('iframe_louvre');
       		if (ifr) {
       		  ifr.contentWindow.location.href = "about:blank";
       	    }
       	  }
       	},

        buttons: [ {
          id: 'iframe_louvre_OK',
          text: 'OK',
		  hidden: true,
          disabled: true,
          listeners: {
        	'click': function() {
        	  this.ownerCt.hide();
            }
          }
        } ],

        showURL: function(url, need_full_reload) {
       	  if (this.finished_observer_task_is_running === 0) {
       	    this.finished_observer_task_is_running = 1;
    	    Ext.TaskMgr.start(this.finished_observer_task);
       	  }
    	  if (need_full_reload !== undefined) {
    	    this.need_full_reload = need_full_reload;
    	  }
    	  var ifr = document.getElementById('iframe_louvre');
    	  if (ifr) {
    	    ifr.contentWindow.location.href = url;
    	    this.show();
    	  } else {
    	    this.html = '<iframe src="' + url + '" id="iframe_louvre" name="louvre" height="250" width="600"></iframe>';
    	    this.show();
    	  }
      	}

      });

	  // Set view mode
	  if (viewMode == 'list') {
	  	setListView ();
	  }
	  else {
	  	setIconView ();
	  }
    },

  showPropertiesDialog: function () {
	var records = getSelectedRecords ();
	if (records.first () && (records.pluck('data').pluck('setprops').all(function(setprops) { return setprops == true; }))) {
		setProperties ();
	}
  },

  showRightsDialog: function () {
	var records = getSelectedRecords ();
    var node    = treeView.getSelectionModel().getSelectedNode();
	if ((records.length <= 1) && (((records.first () && records.first ().data.manage)) || (node.attributes.manage))) {
		manageRights ();
	}
  },

  showCommentsDialog: function () {
	var records = getSelectedRecords ();
	if (records.length === 1) {
		addComment ();
	}
  }
  };
}();

function ToggleOverrideDescription () {
	if (jQuery ('#props_mgt').find ('#override-description').is (':checked')) {
		jQuery ('#props_mgt').find ('textarea[name=description]').removeAttr ('disabled');
		jQuery ('#props_mgt').find ('textarea[name=description]').val ('');
	}
	else {
		jQuery ('#props_mgt').find ('textarea[name=description]').attr ('disabled', '');
		jQuery ('#props_mgt').find ('textarea[name=description]').val ('');
	}
}

// Read cookie value
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
	}
	return "";
}
Ext.Ajax.defaultHeaders = {
	'X-CSRF-Token': getCookie("X-CSRF-Token")
};
