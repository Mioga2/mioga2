(function ($) {
	$.fn.tags = function (options) {

		options = $.extend(true, {
			available: new Array (),
			readonly: 0,
			free: 0,
			tags: new Array (),
			warning: 'This tag does not exist and will be ignored.'
		}, options);

		var $this = $(this);

		// Add a tag to the list
		var addTag = function (tag) {
			if (tag === '') {
				return (0);
			}

			// Clear timer so the suggestions box does not popup after the tag has been added
			if (timer) {
				clearTimeout (timer);
			}

			var $delete_icon = $('<span class="delete">&#160;</span>');
			var $item = $('<li class="tag"><span class="tag">' + tag + '</span></li>').append ($delete_icon);

			if (!options.readonly) {
				$item.insertBefore ($list.find ('li.add-tag-container'));
				var $record = $('<option selected="1">' + tag + '</option>').appendTo ($tags);

				// Create click handler for delete icon
				$delete_icon.on ('click', function (evt) {
					$(this).parent ().fadeOut ('slow', function () {
						$record.remove ();
						$(this).remove ();
					});
				});

				// Trigger display of remove icon
				$item.hover (
					function () {
						$(this).find ('span.delete').addClass ('with-icon');
					},
					function () {
						$(this).find ('span.delete').removeClass ('with-icon');
					}
				);
			}
			else {
				$list.append ($item);
			}

			if (!options.readonly && !free_tags && !tagExists (tag)) {
				$item.addClass ('warning');
				$item.attr ('title', options.warning);
			}
		};

		// Check if a tag exists in database
		var tagExists = function (tag) {
			var res = $.grep (options.available, function (existing_tag) {
				return (existing_tag === tag);
			});
			return ((res != undefined) && (res.length === 1));
		};

		// Gets tags matching a value and presents them into a list
		var suggestTags = function ($suggestion_box, value) {
			var re = new RegExp ('^' + value, 'i');
			var suggestions = $.grep (options.available, function (tag) {
				return (tag.match (re));
			});

			if (suggestions.length) {
				$suggestion_box.fadeIn ('slow');
			}
			else {
				$suggestion_box.fadeOut ('slow');
			}

			$suggestion_box.html ('');
			$.each (suggestions, function (index, item) {
				$('<li>' + item + '</li>').appendTo ($suggestion_box).click (function () {
					addTag (item);
					$suggestion_box.parent ().parent ().remove ();
					$add.show ();
				}).hover (
					function () {
						$(this).addClass ('selected');
					},
					function () {
						$(this).removeClass ('selected');
					}
				);
			});
		};

		var timer = undefined;

		// Initialize UI
		$this.html ('');
		var $list = $('<ul name="tags" class="taglist"></ul>').appendTo ($this);
		if (!options.readonly) {
			$list.addClass ('readwrite');
			var $tags = $('<select name="tags" class="tag-list" multiple="1"></select>').appendTo ($this);

			// Button to add new tags to list
			var $add = $('<li class="add-tag-container"><span class="add-tag">&#160;</span></li>').appendTo ($list);
			$add.on ('click', function () {
				var $this = $(this);
				$this.hide ();

				// Add input to type new tag
				var $container = $('<li></li>').appendTo ($(this).parent ());
				var $input = $('<input type="text" name="tag" class="tag-picker"/>').appendTo ($container);
				var $apply = $('<span class="apply"></span>').appendTo ($container);
				var $suggestion_box = $('<ul class="suggestions" style="display:none"></ul>').appendTo ($('<div class="suggestions-container"></div>').appendTo ($container));
				$apply.click (function () {
					if ($input.attr ('value') != '') {
						addTag ($input.attr ('value'));
						$container.remove ();
						$add.show ();
					}
				});
				$input.select ();
				$input.on ('keypress', function (evt) {
					if (evt.keyCode == 13 && $(this).attr ('value')) {
						// User pressed ENTER, create node in list
						addTag ($(this).attr ('value'));
						$container.remove ();
						$add.show ();
					}
				});

				// Add a timer before showing suggestions
				if (options.available) {
					timer = setTimeout (function () {
						suggestTags ($suggestion_box, $input.val ());
						clearTimeout (timer);
						$input.keyup (function () {
							suggestTags ($suggestion_box, $input.val ());
						});
					}, 2000);
				}
			});
		}

		// Set initial tag list
		$.each (options.tags, function (index, tag) {
			addTag (tag);
		});

		return ($this);
	};
}) (jQuery);
