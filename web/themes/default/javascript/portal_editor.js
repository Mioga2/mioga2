function selectId(id, e) {
  location = "EditPortal?select_id=" + id;

  if(document.all) {
    window.event.cancelBubble = 1;
  }
  else {
    e.stopPropagation();
  }
}

function deleteId(id, e) {
  location = "EditPortal?delete_id=" + id;

  if(document.all) {
    window.event.cancelBubble = 1;
  }
  else {
    e.stopPropagation();
  }
}


function upId(id, e) {
  location = "EditPortal?up_id=" + id;

  if(document.all) {
    window.event.cancelBubble = 1;
  }
  else {
    e.stopPropagation();
  }
}


function downId(id, e) {
  location = "EditPortal?down_id=" + id;

  if(document.all) {
    window.event.cancelBubble = 1;
  }
  else {
    e.stopPropagation();
  }
}

function addBeforeId(id, e) {
  location = "EditPortal?add_before_id=" + id;

  if(document.all) {
    window.event.cancelBubble = 1;
  }
  else {
    e.stopPropagation();
  }
}


function addAfterId(id, e) {
  location = "EditPortal?add_after_id=" + id;

  if(document.all) {
    window.event.cancelBubble = 1;
  }
  else {
    e.stopPropagation();
  }
}


function noclick(e) {
  if(document.all) {
    window.event.cancelBubble = 1;
  }
  else {
    e.stopPropagation();
  }
}
