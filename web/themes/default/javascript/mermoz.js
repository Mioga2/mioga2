// Recipient count
var recipient_count = 0;
var max_recipients = 0;

// Messages
var Mermoz = { messages: {
    check_recipients: "You have to select at least one recipient.",
    check_subject: "You have to write a subject.",
    check_content: "You have to write a message.",
    check_signature: "You have to write a signature"
} };

function init_mermoz(options) {
    (options.signature_enabled == '1') ? enable_signature() : disable_signature();
    $('subject').activate();
    
    var contacts    = $$('div.contacts').first();
    var event_type  = (navigator.userAgent.match(/msie/i)) ? 'click' : 'change';
    contacts.observe(event_type, update_recipients);
    contacts.observe('click', toggle_groups);
    $('toggle-signature').observe(event_type, toggle_signature);
}

function toggle_groups(event) {
    element = Event.element(event);
    if (element.tagName.toLowerCase() != 'img' || !element.id.match(/^toggle\-group/)) { return; }
    Element.up(element, 'li.group').getElementsBySelector('ul').each(function(u) {  Element.toggle(u); });
    toggle_fold(element);
}

function toggle_fold(element) {
    if (element.src.match(/unfold\.gif$/))
        element.src = image_path + "/fold.gif";
    else
        element.src = image_path + "/unfold.gif";
}

function check_data() {
    serialize_recipients();

    if ($('recipients').value.match(/^\s*$/)) {
        alert(Mermoz.messages.check_recipients);
        return false;
    }
    if ($('subject').value.match(/^\s*$/)) {
        alert(Mermoz.messages.check_subject);
        return false;
    }
    if (tinyMCE.activeEditor.getContent().match(/^\s*$/)) {
        alert(Mermoz.messages.check_content);
        return false;
    }
    if ($('toggle-signature').checked && $('signature').value.match(/^\s*$/)) {
        alert(Mermoz.messages.check_signature);
        return false;
    }
    return true;
}

function toggle_signature() {
    var signature = $('signature');
    if (signature.disabled)
        enable_signature();
    else
        disable_signature();
}

function enable_signature() {
    var signature = $('signature');
    signature.removeClassName('disabled');
    signature.enable();
    signature.activate();
}

function disable_signature() {
    var signature = $('signature');
    signature.addClassName('disabled');
    signature.disable();
}

function update_recipients(event) {
    element = Event.element(event);
    if (element.type != 'checkbox') { return; }
    
    if (element.name.match(/group-(\d+)/)) {
        Element.up(element).getElementsBySelector('input[type=checkbox]').each(function(c) { c.checked = element.checked });
    }
    else {
        if (element.checked == false) { Element.up(element, 'li.group').down('input').checked = false; }
    }
    
    serialize_recipients();
}

function serialize_recipients() {
    $('recipients').value = $$('div.contacts').first().getElementsBySelector('input[type=checkbox]').select(function(c) { return c.checked }).map(function(c) { return c.value }).join(';');
}

function updateCount (event, recipients) {
	if (event.currentTarget.checked) {
		recipient_count += recipients;
	}
	else {
		recipient_count -= recipients;
	}

	if (recipient_count > max_recipients) {
		document.getElementById ('detail-recipients').checked = false;
		document.getElementById ('detail-recipients').disabled = true;
	}
	else {
		document.getElementById ('detail-recipients').checked = true;
		document.getElementById ('detail-recipients').disabled = false;
	}
}
