var skel_block_toggler;
var app_block_toggler;
var error_block_toggler;
var underlay;

dojo.requireLocalization("mioga", "colbertsa");
var transl = dojo.i18n.getLocalization("mioga", "colbertsa");

var application_sort_fields = {
	label: transl.label,
	selected: transl.authorization
};

/* -------------------------------------- *
 *           Generic functions            *
 * -------------------------------------- */

function ShowUnderlay () {
	if (!underlay) {
		underlay = new dijit.DialogUnderlay ({'class': 'loading', 'id': 'underlay'});
	}
	underlay.show ();
}


/* -------------------------------------- *
 *            Instance functions              *
 * -------------------------------------- */

function CloseInstanceDialog() {
	dijit.byId ('edit_instance').hide ();
}

function ClearInstanceErrors () {
	// Hide error block
	dojo.byId ('edit_instance_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('edit_instance')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayInstanceEdit (rowid) {
	console.debug('rowid ' + rowid);

	// Replace {ident} macro with span in titles
	dojo.query ("h1.tabtitle").forEach (function (node) { node.innerHTML = node.innerHTML.replace (/{ident}/, '<span class="instance-ident"></span>'); });
	dojo.query ("h1.tabtitle span.instance-ident").forEach (function (node) { node.innerHTML = ''; });
	dojo.query ("div.tab").forEach (function (node) { node.title = node.title.replace (/ {ident}/, ''); });

	// Clear errors
	ClearInstanceErrors ();

	if (rowid) {
		// Disable ident field
		dijit.byId('instance_attributes_ident').attr ('disabled', true);

		// Hide skeleton chooser
		dojo.byId ('skel-chooser').style.display = 'none';
		// Show referent, theme and homepage chooser
		var admin_chooser = dojo.byId ('admin-chooser');
		if (admin_chooser) {
			admin_chooser.style.display = 'none';
		}
		dojo.byId ('referent-chooser').style.display = 'block';
		dojo.byId ('theme-chooser').style.display = 'block';
		dojo.byId ('homepage-chooser').style.display = 'block';

		dojo.xhrGet({
			url: "GetInstanceDetails.json",
			timeout: 30000, // give up after 3 seconds
			content: { rowid:rowid,full_list:1 },
			handleAs: "json",
			load: function(response, ioArgs){
				console.dir (response);

				// Add ident to tab titles
				dojo.query ("h1.tabtitle span.instance-ident").forEach (function (node) { node.innerHTML = response.instance.ident; });

				var referentStore = new mioga.data.ItemFileReadStore({
						nodename: 'user',
						identifier: 'rowid',
						url: 'GetUsers.json?instance='+response.instance.ident
					});
				referentStore.fetch ();
				dijit.byId ('instance_attributes_referent_id').set ('store', referentStore);
				var defaultgroupStore = new mioga.data.ItemFileReadStore({
						nodename: 'group',
						identifier: 'rowid',
						url: 'GetGroups.json?instance='+response.instance.ident
					});
				defaultgroupStore.fetch ();
				var themeStore = new mioga.data.ItemFileReadStore({
						nodename: 'theme',
						identifier: 'rowid',
						url: 'GetThemes.json?instance='+response.instance.ident
					});
				themeStore.fetch ();
				dijit.byId ('instance_attributes_default_theme_id').set ('store', themeStore);
				dijit.byId ('instance_attributes_default_group_id').set ('store', defaultgroupStore);
				dijit.byId('instance_attributes_form').reset ();
				dijit.byId('instance_attributes_form').setValues (response.instance);
				if (response.instance.use_secret_question != 0) dijit.byId('instance_use_secret_question').setValue (response.instance.use_secret_question);
				if (response.instance.doris_can_set_secret_question != 0) dijit.byId('instance_doris_can_set_secret_question').setValue (response.instance.doris_can_set_secret_question);
				if (response.instance.use_ldap != 0) dijit.byId('instance_use_ldap').setValue (response.instance.use_ldap);
				ableLDAPAttributes ();
				dijit.byId ('instance_auth_fail_timeout').setValue (Math.round (response.instance.auth_fail_timeout / 60));
				dijit.byId ('instance_quota_soft').setValue (Math.round (response.instance.quota_soft));
				dijit.byId ('instance_quota_hard').setValue (Math.round (response.instance.quota_hard));

				// Set list of groups, teams and applications
				var sel = new Array ();
				var all = new Array ();
				dojo.forEach (Array ('application'), function (elm) {
					sel[elm] = new Array ();
					all[elm] = new Array ();
					if (response[elm+'s'] && response[elm+'s'][elm]) {
						dojo.forEach (response[elm+'s'][elm], function (item) {
							if (item.selected) {
								sel[elm].push (item);
							}
							all[elm].push (item);
						});
					}
					dijit.byId ('instance_'+elm+'s').createStore (all[elm], sel[elm]);
				});
			},
			error: function(err, ioArgs){
				var transl = dojo.i18n.getLocalization("mioga", "colbertsa");
				var dialog = new dijit.Dialog({
					title: transl['generic_error_title'],
					content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
				});
				dialog.show ();
				// The two lines below are because of a bad rendering with Chromium
				dojo.style(dialog.titleBar, { width: '100%' });
				dojo.style(dialog.containerNode, { width: '100%' });
			}
		});
	}
	else {
		// Enable ident field
		dijit.byId('instance_attributes_ident').attr ('disabled', false);

		// Reset form
		dijit.byId('instance_attributes_form').reset ();

		// Set FilteringSelect initial values
		dijit.byId ('instance_attributes_lang').setValue (lang);
		dijit.byId ('instance_attributes_skeleton').store.fetch ({onComplete: function () { var store_data = dijit.byId ('instance_attributes_skeleton').store._arrayOfAllItems; console.dir (store_data); dijit.byId ('instance_attributes_skeleton').set ('value', store_data[0].file[0]);}});

		// Show skeleton chooser
		dojo.byId ('skel-chooser').style.display = 'block';
		// Hide referent, theme and homepage chooser
		var admin_chooser = dojo.byId ('admin-chooser');
		if (admin_chooser) {
			admin_chooser.style.display = 'block';
		}
		dojo.byId ('referent-chooser').style.display = 'none';
		dojo.byId ('theme-chooser').style.display = 'none';
		dojo.byId ('homepage-chooser').style.display = 'none';
	}

	// Show dialog
	dijit.byId ('edit_instance').show ();

	// Set first tab as visible
	dijit.byId ('tabbed_dialog').showFirst ();

	// Ensure dialog is centered
	dijit.byId ('edit_instance')._position ();
}

function UpdateInstanceSkeletonDetails () {
	var lang = dijit.byId ('instance_attributes_lang').attr ('value');
	var file = dijit.byId ('instance_attributes_skeleton').attr ('value');

	if (file && lang) {
		ShowUnderlay ();
		dojo.xhrGet({
			url: "GetSkeletonDetails.json",
			timeout: 30000, // give up after 3 seconds
			content: { type:'instance',lang:lang,file:file,full_list:1 },
			handleAs: "json",
			load: function(response, ioArgs){
				console.dir (response);

				dijit.byId('instance_attributes_form').setValues (response.attributes);
				if (response.attributes.use_secret_question != 0) dijit.byId('instance_use_secret_question').setValue (response.attributes.use_secret_question);
				if (response.attributes.doris_can_set_secret_question != 0) dijit.byId('instance_doris_can_set_secret_question').setValue (response.attributes.doris_can_set_secret_question);
				if (response.attributes.use_ldap != 0) dijit.byId('instance_use_ldap').setValue (response.attributes.use_ldap);
				ableLDAPAttributes ();
				dijit.byId ('instance_auth_fail_timeout').setValue (Math.round (response.attributes.auth_fail_timeout / 60));
				dijit.byId ('instance_quota_soft').setValue (Math.round (response.attributes.quota_soft));
				dijit.byId ('instance_quota_hard').setValue (Math.round (response.attributes.quota_hard));

				// Set list of applications
				var sel = new Array ();
				var all = new Array ();
				dojo.forEach (Array ('application'), function (elm) {
					sel[elm] = new Array ();
					all[elm] = new Array ();
					if (response[elm+'s'] && response[elm+'s'][elm]) {
						dojo.forEach (response[elm+'s'][elm], function (item) {
							if (item.selected) {
								sel[elm].push (item);
							}
							all[elm].push (item);
						});
					}
					dijit.byId ('instance_'+elm+'s').createStore (all[elm], sel[elm]);
				});
				underlay.hide ();
			},
			error: function(err, ioArgs){
				underlay.hide ();
				var transl = dojo.i18n.getLocalization("mioga", "colbertsa");
				var dialog = new dijit.Dialog({
					title: transl['generic_error_title'],
					content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
				});
				dialog.show ();
				// The two lines below are because of a bad rendering with Chromium
				dojo.style(dialog.titleBar, { width: '100%' });
				dojo.style(dialog.containerNode, { width: '100%' });
			}
		});
	}
	else {
		// FilteringSelect values not initialized, plan a new call to function
		setTimeout (UpdateInstanceSkeletonDetails, 1000);
	}
}

function ValidateInstance() {
	console.debug('Validate called');
	var transl = dojo.i18n.getLocalization("mioga", "colbertsa");

	// Clear errors
	ClearInstanceErrors ();

	// Get form data
	var form = dijit.byId('instance_attributes_form');
	console.debug("form " + form);
	var form_data = form.attr("value");

	// Delete useless data for creation
	if (form_data.rowid == "") {
		delete form_data.rowid;
		delete form_data.default_theme_id;
		delete form_data.homepage;
		delete form_data.default_group_id;
	}
	else {
		delete form_data.admin_ident;
		delete form_data.admin_email;
	}

	// Transform checkboxes values from array to flag
	if (form_data.use_secret_question[0]) {
		form_data.use_secret_question = 1;
	}
	else {
		form_data.use_secret_question = 0;
	}
	if (form_data.doris_can_set_secret_question[0]) {
		form_data.doris_can_set_secret_question = 1;
	}
	else {
		form_data.doris_can_set_secret_question = 0;
	}
	form_data.use_ldap = form_data.use_ldap[0] ? 1 : 0;

	// Convert account lock timeout to seconds
	form_data.auth_fail_timeout *= 60;

	if (form_data.admin_password == "") {
		delete form_data.admin_password;
		delete form_data.admin_password_confirm;
	}

	// Transform numbers to numbers
	if (dijit.byId ('instance_use_ldap').getValue ()) {
		form_data.ldap_access = parseInt (form_data.ldap_access, 10);
	}

	// Get applications
	dojo.forEach (Array ('applications'), function (elm) {
		form_data[elm] = new Array ();
		dojo.forEach (dijit.byId ('instance_' + elm).getList (), function (item) {
			form_data[elm].push (item.ident);
		});
	});

	console.debug("form_data " + form_data);

	console.dir(form_data);

	ShowUnderlay ();

	dojo.xhrPost({
		url: "SetInstance.json",
		timeout: 60000, // give up after 30 seconds
		content: form_data,
		handleAs:"json",
		load: function(response, ioArgs){
			console.debug("load function  response " + response);
			console.dir(response);
			if (response.errors) {
				var errors = '<ul>';
				dojo.forEach(response.errors, function(item, index, array) {
					for (var i in item) {
						console.log (i + ": " + item[i]);
						dojo.byId ('instance_attributes_'+i+'_label').className = 'error';
						errors += '<li>' + transl[i] + ': ' + transl[item[i]] + '</li>';
					}
				});
				errors += '</ul>';
				dojo.byId ('edit_instance_error_block').innerHTML = errors;
				dojo.byId ('edit_instance_error_block').style.display = 'block';
				underlay.hide ();
			}
			else if (!response.success && response.journal) {
				var journal = '<ul class="journal">';
				dojo.forEach (response.journal, function (item, index, array) {
					var classname = item.status ? 'success' : 'failure';
					if (item.module != 'Mioga2::InstanceList') {
						return (0);
					}
					if (item.stage) {
						classname += ' ' + item.stage;
					}
					journal += '<li class="' + classname + '">' + item.step + '</li>';
				});
				journal += '</ul>';
				var journal_dialog = new dijit.Dialog({
					title: transl.journal,
					content: journal
				});
				dojo.byId ('edit_instance_error_block').innerHTML = '<p>' + transl.operation_rollback + '&#160;<a href="#">' + transl.show_journal + '</a>';
				dojo.query ('a', dojo.byId ('edit_instance_error_block')).connect ('onclick', function (evt) {
					journal_dialog.show ();
				});
				dojo.byId ('edit_instance_error_block').style.display = 'block';
				underlay.hide ();
			}
			else {
				underlay.hide ();
				dijit.byId("edit_instance").hide ();
				RefreshGrid();
			}

		},
		error: function(err, ioArgs){
			underlay.hide ();
			var transl = dojo.i18n.getLocalization("mioga", "colbertsa");
			var dialog = new dijit.Dialog({
				title: transl['generic_error_title'],
				content: '<div style="width: 100%;"><p><b>' + transl['generic_error'] + '</b></p>' + ioArgs.xhr.responseText + '</div>'
			});
			dialog.show ();
			// The two lines below are because of a bad rendering with Chromium
			dojo.style(dialog.titleBar, { width: '100%' });
			dojo.style(dialog.containerNode, { width: '100%' });
		}
	});
}

// Check function before instance deletion
function CheckInstanceForDelete (gridId) {
	var grid = dojo.getObject(gridId);
	var ok = true;
	dojo.forEach (grid.selection.getSelected(), function (item) {
		if (item.current_instance) {
			ok = false;
		}
	});

	if (!ok) {
		var transl = dojo.i18n.getLocalization("mioga", "colbertsa");
		alert (transl.DefaultInstDelMsg);
	}

	return (ok);
}

// En- or Dis- able LDAP attributes according to use_ldap flag
function ableLDAPAttributes () {
	state = !dijit.byId ('instance_use_ldap').getValue ();
	dojo.forEach (Array ('ldap_access', 'ldap_host', 'ldap_port', 'ldap_bind_dn', 'ldap_bind_pwd', 'ldap_user_base_dn', 'ldap_user_filter', 'ldap_user_scope', 'ldap_user_ident_attr', 'ldap_user_firstname_attr', 'ldap_user_lastname_attr', 'ldap_user_email_attr', 'ldap_user_pwd_attr', 'ldap_user_desc_attr', 'ldap_user_inst_attr'), function (elm) {
		dijit.byId ('instance_'+elm).attr ('disabled', state);
		dojo.byId ('instance_attributes_'+elm+'_label').className = (state) ? 'disabled' : '';
	});

	// Set default port value if not active
	if (state) {
		dijit.byId ('instance_ldap_port').attr ('value', 389);
	}
}


/* -------------------------------------- *
 *           Skeletons functions          *
 * -------------------------------------- */

function CloseSkeletonsUploadDialog () {
	dijit.byId ('upload_skeletons').hide ();
}

function ClearSkeletonsUploadErrors () {
	// Hide error block
	dojo.byId ('upload_skeletons_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('skeletons_dialog')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplaySkeletonsUploadDialog () {
	// Clear errors
	ClearSkeletonsUploadErrors ();

	// Show dialog
	dijit.byId ('upload_skeletons').show ();
}

function UploadSkeletonsCallback (err) {
	dijit.byId ('underlay').hide ();
	if (err) {
		dojo.byId ('upload_skeletons_error_block').style.display = 'block';
		dojo.byId ('upload_skeletons_error_block_content').innerHTML = err;
	}
	else {
		CloseSkeletonsUploadDialog ();
	}
}

// Function to store grid preferences
function StorePreferences () {
	var prefs = new Object ();

	var grid = dijit.byId (grid_id);

	// Get current sort field
	prefs.sort_field = grid.getSortProps()[0].attribute;

	// Get width of each column
	prefs.columns = new Array ();
	prefs.widths = new Array ();
	var cell_idx = 0;
	dojo.forEach (grid.structure[0].cells[0], function (elm) {
		var field_name = elm.field;
		prefs.columns.push (field_name);
		prefs.widths.push (grid.getCell (cell_idx).unitWidth);
		cell_idx++;
	});

	console.log ("Storing the following preferences: ");
	console.dir (prefs);

	// Store preferences
	dojo.xhrPost({
		url: "StorePreferences.json",
		timeout: 30000, // give up after 30 seconds
		content: prefs,
		handleAs: "json",
		load: function(response, ioArgs) {
			console.debug("Store preferences response:");
			console.dir(response);
		}
	});
}


/* -------------------------------------- *
 *          Login system functions        *
 * -------------------------------------- */

function CloseLoginResourcesUploadDialog () {
	dijit.byId ('upload_login_resources').hide ();
}

function ClearLoginResourcesUploadErrors () {
	// Hide error block
	dojo.byId ('upload_login_resources_error_block').style.display = 'none';

	// Reset labels class
	dojo.query ('.error', dojo.byId ('upload_login_resources')).forEach(function(node, index, arr){
		node.className = '';
	});
}

function DisplayLoginResourcesUploadDialog () {
	// Clear errors
	ClearLoginResourcesUploadErrors ();

	// Show dialog
	dijit.byId ('upload_login_resources').show ();
}

function UploadLoginResourcesCallback (err) {
	dijit.byId ('underlay').hide ();
	if (err) {
		dojo.byId ('upload_login_resources_error_block').style.display = 'block';
		dojo.byId ('upload_login_resources_error_block_content').innerHTML = err;
	}
	else {
		CloseLoginResourcesUploadDialog ();
	}
}
