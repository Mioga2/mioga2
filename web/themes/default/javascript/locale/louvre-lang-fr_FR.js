/**
 * @fileOverview Translation file for Louvre to French (fr_FR)
 * @author Alixen (technique@alixen.fr)
 */
 
Louvre.messages = {
	maxwidth: "La largeur des vignettes doit se situer entre 10 et 999.",
	maxheight:"La hauteur des vignettes doit se situer entre 10 et 999."
};