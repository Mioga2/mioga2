/**
 * @fileOverview Translation file for Mermoz to french (fr_FR)
 * @author Loic Guitaut, Alixen (info@alixen.fr)
 */
 
 Mermoz.messages = {
    check_recipients: 'Vous devez sélectionner au moins un destinataire.',
    check_subject: 'Vous devez saisir un sujet.',
    check_content: 'Vous devez saisir un message.',
    check_signature: 'Vous devez saisir une signature.'
 };