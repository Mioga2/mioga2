/**
 * @fileOverview Translation file for Louvre to English (en_US)
 * @author Alixen (technique@alixen.fr)
 */

Louvre.messages = {
	maxwidth: "The thumbnail width must be between 10 and 999.",
	maxheight: "The thumbnail height must be between 10 and 999."
};