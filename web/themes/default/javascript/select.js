function init_select_validate(form_name) {
    if (!$$('form[name='+form_name+']')[0] || !$('ok-button')) { return; }
    $('ok-button').observe('click', validate_form.bindAsEventListener(this, form_name));
    $$('form[name='+form_name+']')[0].observe('click', toggle_selection);
    
    var select_all      = $('select_all');
    var unselect_all    = $('unselect_all');
    select_all.href     = '#';
    unselect_all.href   = '#';
    select_all.observe('click', select_all_hook);
    unselect_all.observe('click', unselect_all_hook);
}

function validate_form(event, form_name) {
    var button  = $('ok-button');
    var url     = button.href.gsub(/\?.*/, '');
    var params  = $H(button.href.toQueryParams());
    var sllform = $$('form[name='+form_name+']')[0];
    params = params.merge($H(sllform.serialize().toQueryParams()));
    button.href = url + "?" + params.map(function(pair) { return encodeURIComponent(pair.key) + "=" + encodeURIComponent(pair.value || ''); }).join('&');
}

function toggle_selection(event) {
    var element = Event.element(event);
    if (!(element.tagName == 'INPUT' && element.type == 'checkbox')) { return; }
    
    var tr  = element.up('tr');
    (element.checked) ? tr.addClassName('selected') : tr.removeClassName('selected');
}

function select_all_hook(event) {
    var checkboxes  = $$('input[type=checkbox]').select(function(input) { return input.name.match(/rowid_\d+/) && !input.checked; });
    checkboxes.each(function(c) { var tr = c.up('tr'); tr.addClassName('selected'); c.checked = true; });
}

function unselect_all_hook(event) {
    var checkboxes  = $$('input[type=checkbox]').select(function(input) { return input.name.match(/rowid_\d+/) && input.checked; });
    checkboxes.each(function(c) { var tr = c.up('tr'); tr.removeClassName('selected'); c.checked = false; });
}