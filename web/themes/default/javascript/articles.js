function checkEmpty(obj, err_msg) {
    if (!obj.value.match(/^\s*$/))
        return true;
    alert(err_msg);
    obj.focus();
    return false;
}

function loading() {
    document.body.style.cursor = "wait";
}

function complete() {
    document.body.style.cursor = "auto";
}
