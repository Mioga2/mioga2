window.onload = function () {
	if (popup_display) {
		$$('div.preview').invoke ('hide');
		$$('div.thumbnail').invoke ('observe', 'mouseover', function () {
			// Get dimensions
			var dimensions = this.down ('.thumbnail_img_inner img').getDimensions ();
			var width = dimensions.width;
			var height = dimensions.height;

			// Get position
			var position = this.down ('.thumbnail_img_inner img').cumulativeOffset ();
			var top = position.top;
			var left = ((position.left - (width / 2)) > 0) ? position.left - (width / 2) : 5;

			// Set style for container
			this.down ('.preview').setStyle ({
				'position': 'absolute',
				'top': top + 'px',
				'left': left + 'px'
			});

			// Set style for image
			this.down ('.preview img').setStyle ({
				'height': height * 2 + 'px',
				'width': width * 2 + 'px'
			});
			this.down ('.preview').show ();
		});

		// Hide preview when mouse leaves
		$$('div.thumbnail').invoke ('observe', 'mouseout', function () {
			this.down ('.preview').hide ();
		});
	}
}
