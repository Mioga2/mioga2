var underlay;
var form_button_click_handler;
var canmodify;
var canimport;

function ShowUnderlay () {
	if (!underlay) {
		underlay = new dijit.DialogUnderlay ({'class': 'loading', 'id': 'underlay'});
	}
	underlay.show ();
}

function callback (messages) {
	// Display upload parameters and hide progress bar
	dojo.byId('UploadForm').style.display = "block";
	dojo.byId('UploadProgressBar').style.display = "none";

	// Disable file and mode selector
	dojo.byId('FileToUpload').disabled = true;
	dijit.byId('ImportMode').setDisabled(true);

	// Add dynamic HTML from server response
	if (messages != '') {
		dojo.byId('MessageBox').style.display = "block";
		dojo.byId('MessageBox').innerHTML = messages;
	}

	RefreshGrid ();
	
	dijit.byId('UploadDialog').hide ();
	if (messages != '') {
		dijit.byId('UploadDialog').show ();
	}
}

function ShowUploadDialog () {
	// Import and message boxes
	dojo.byId('MessageBox').style.display = "none";
	dojo.byId('MessageBox').innerHTML = '';

	dojo.byId('UploadFileSelector').style.display = "block";
	dojo.byId('UploadProgressBar').style.display = "none";
	dojo.byId('UploadMessageBlock').style.display = "none";
	dojo.byId('UploadMessageBlock').innerHTML = '';
	// dijit.byId('UploadButton').setDisabled(false);
	dijit.byId('ImportMode').setDisabled(false);
	dojo.byId('FileToUpload').disabled = false;
	document.getElementById('FileToUpload').disabled = false;

	// Show dialog
	dijit.byId('UploadDialog').show ();
}

function ValidateUser() {
	console.debug('Validate called');
	EraseErrors();
	var form = dijit.byId('edit_form');
	console.debug("form " + form);
	var form_data = form.get ("value");
	var importcb = dojo.query (".importcheckbox :checked");
	importcb.forEach(function(node, index, nodeList){
		form_data.importdn = node.title;
	});
	var instance = dojo.byId ('instance_selector');
	if (instance) {
		form_data.instance = instance.value;
	}
	console.debug("form_data " + form_data);
	console.dir(form_data);
	ShowUnderlay ();
	dojo.xhrPost({
		url: "SetUser.json",
		timeout: 30000, // give up after 3 seconds
		content: form_data,
		handleAs:"json",
		load: function(response, ioArgs){
			console.debug("load function  response " + response);
			console.dir(response);
			if (response.errors.length > 0) {
				var importflag = 0;
				var createflag = false;
				var errormsg = '';
				dojo.forEach(response.errors, function(item, index, array) {
												var transl = dojo.i18n.getLocalization("mioga", "bottin");
												  console.debug('0 ' + item[0]);
												  console.debug('1 ' + item[1]);
												  if (item[0] == 'import') {
													importflag = 1;
												  }
												  else if (item[0] == 'create') {
												  	createflag = true;
												  }
												  else if (item[0] != '') {
													  if (dojo.byId (item[0]+'_label')) {
														  dojo.addClass (dojo.byId (item[0]+'_label'), "error");
													  }
													  // dojo.query('#'+item[0]+'_label').addContent('<p class="error">'+item[1]+'</p>', "after");
													  if (item[1][0] == 'password_policy') {
														errormsg += dojo.byId ('PasswordPolicy').innerHTML;
													  }
													  else if (transl[item[0]] && transl[item[1]]) {
													  	errormsg += '<li>' + transl[item[0]] + ': ' + transl[item[1]] + '</li>';
													  }
													  else {
													  	errormsg += '<li>' + item[1] + '</li>';
													  }
												  }
												  else {
													errormsg += '<p class="error">' + item[1] + '</p>';
												  }
											  }
								);
				dojo.byId ('error_block').style.display = 'block';
				if (importflag) {
					showImport (response.errors);
				}
				else if (createflag) {
					// Show user-creation form elements
					if (canmodify) {
						// User import failed, create user
						["firstname", "lastname", "password", "password2", "description", "ident"].map (function (name) {
							if (dojo.byId ('user-' + name + '-cont')) {
								dojo.byId ('user-' + name + '-cont').style.display = "block";
							}
							dijit.byId (name).attr ('disabled', false);
						});
						dijit.byId ('ident').attr ('value', response.user.ident);
					}
					else {
						// User import failed but can't modify LDAP, abort.
						dojo.byId ('UserForm').style.display = 'none';
						dojo.byId ('EditImportBox').style.display = 'none';
						dojo.byId ('NoCreateMessage').style.display = 'block';

						// Replace click handler with dialog close
						form_button_click_handler && form_button_click_handler.remove ();
						form_button_click_handler = dojo.query ('#edit_user_validate_button button').on ('click', function () {
							dojo.byId ('NoCreateMessage').style.display = 'none';
							dijit.byId("edit_user").hide();
						});
					}
				}
				else {
					dojo.byId('error_block').innerHTML = "<h1>Error</h1>" + '<div class="errorbox">' + errormsg + '</div>';
				}
				underlay.hide ();
			}
			else {
				dijit.byId("edit_user").hide();
				underlay.hide ();
				RefreshGrid();
			}

		},
		error: function(err, ioArgs){
			alert('Error DisplayUserEdit ' + err);
			underlay.hide ();
		}
	})
}

function showImport (list) {
	dojo.byId ('UserForm').style.display = 'none';
	dojo.byId ('EditImportBox').style.display = 'block';
	dojo.byId ('NoCreateMessage').style.display = 'none';
	var content = '';
	dojo.forEach(list, function(item, index, array) {
										content += '<div class="form-item">';
										content += '<div class="importcheckbox"><input type="checkbox" id="' + item[1][0] + '" title="' + item[1][1] + '"/></div><div class="importtext" onClick="document.getElementById(\'' + item[1][0] + '\').checked=!document.getElementById(\'' + item[1][0] + '\').checked"><div class="original">' + item[1][1] + '</div><div class="destination">' + item[1][2] + '</div></div>';
										content += '</div>';
								  }
					);
	dojo.byId ('EditImportBoxContent').innerHTML = content;
	dojo.byId('edit_user').style.width = 'auto';
}

function ExportAll () {
	dojo.require ("dojo.io.iframe");
	var instance_name = '';
	var instance = dojo.byId ('instance');
	if (instance) {
		instance_name = instance.value;
	}

	dojo.io.iframe.send ({
		timeout: 30000,
		method: 'POST',
		url: 'ExportUsers',
		content: {instance: instance_name},
		load: function (data) {console.log(data);}
	});
}

function GetInstance () {
	var data = Object ();
	var instance = dojo.byId ('instance_selector');
	if (instance) {
		data.instance = instance.value;
	}
	return (data);
}

//
// Directory-wide search functions
//

function SearchThroughDirectory () {
	dijit.byId ('DirectorySearchDialog').show ();
}

function ValidateDirectorySearch () {
	console.debug('Validate called');
	var form = dijit.byId('directory_search_form');
	var form_data = form.get ("value");
	console.debug("form_data " + form_data);
	console.dir(form_data);
	dojo.xhrPost({
		url: "SearchThroughDirectory.json",
		content: form_data,
		handleAs:"json",
		load: function(response, ioArgs){
			console.debug("load function  response " + response);
			console.dir(response);

			// Hide search dialog
			dijit.byId("DirectorySearchDialog").hide();

			// Refresh grid contents
			var grid = dojo.getObject(grid_id);
			grid.setStore(new dojo.data.ItemFileReadStore({data: response}));
			grid.update();

			// Hide actions box
			dojo.query ("div#main_actions").forEach (function (elm) { elm.style.display = 'none'; });

			// Disable selection actions dropdown
			dijit.byId ('selection_dropdown').disabled = true;

			// Show explanation box
			dojo.query ("div#directory_search_actions").forEach (function (elm) { elm.style.display = 'block'; });

			// Set buttons to their initial state
			if (dojo.byId ('edit_user_validate_button')) {
				dojo.byId ('edit_user_validate_button').style.display = 'none';
				dojo.byId ('edit_user_delete_button').style.display = 'block';
			}
		},
		error: function(err, ioArgs){
			alert('Error ValidateDirectorySearch ' + err);
		}
	})
}

function ExitSearchMode () {
	// Refresh grid
	RefreshGrid ();

	// Show actions box
	dojo.query ("div#main_actions").forEach (function (elm) { elm.style.display = 'block'; });

	// Enable selection actions dropdown
	dijit.byId ('selection_dropdown').disabled = false;

	// Hide explanation box
	dojo.query ("div#directory_search_actions").forEach (function (elm) { elm.style.display = 'none'; });

	// Set buttons to their initial state
	if (dojo.byId ('edit_user_validate_button')) {
		dojo.byId ('edit_user_validate_button').style.display = 'block';
		dojo.byId ('edit_user_delete_button').style.display = 'none';
	}
}

var confirm_handles = [];
function ConfirmDeleteUserFromDirectory (form) {
	var form_data = form.get ('value');
	dojo.byId ('checkbox_confirm_deletion').checked = false;
	dijit.byId ('DeletionConfirmDialog').show ();
	confirm_handles.push (dojo.connect(dojo.byId ('decline_deletion'), 'onclick', function () { DeclineDeletion (); return (false); }));
	confirm_handles.push (dojo.connect(dijit.byId ('DeletionConfirmDialog'), 'hide', function () { DeclineDeletion (); return (false); }));
	confirm_handles.push (dojo.connect(dojo.byId ('confirm_deletion'), 'onclick', function () { DeleteUserFromDirectory (form_data.rowid); return (false); }));
}

function DeclineDeletion () {
	dojo.forEach(confirm_handles, dojo.disconnect);
	dijit.byId ('DeletionConfirmDialog').hide ();
	dijit.byId ('edit_user').hide ();
}

function DeleteUserFromDirectory (dn) {
	if (dojo.byId ('checkbox_confirm_deletion').checked) {
		dojo.forEach(confirm_handles, dojo.disconnect);
		console.log ("Deleting from directory: " + dn);

		// Initialize form POST data
		var rowids = new Array();
		rowids.push (dn);
		var content = new Object ();
		content.rowids = rowids;
		content.instance = '*';

		// Post deletion request
		dojo.xhrPost({
			url: 'DeleteUser',
			content: content,
			load: function(response, ioArgs){
				dijit.byId ('DeletionConfirmDialog').hide ();
				dijit.byId ('edit_user').hide ();

				// Update search results
				ValidateDirectorySearch ();
			}
		});
	}
	else {
		alert (please_check_box);
	}
}

// Function to store grid preferences
function StorePreferences () {
	var prefs = new Object ();

	// Get current sort field
	prefs.sort_field = grid_users.getSortProps()[0].attribute;

	// Get width of each column
	prefs.columns = new Array ();
	prefs.widths = new Array ();
	var cell_idx = 0;
	dojo.forEach (grid_users.structure[0].cells[0], function (elm) {
		var field_name = elm.field;
		prefs.columns.push (field_name);
		prefs.widths.push (grid_users.getCell (cell_idx).unitWidth);
		cell_idx++;
	});

	console.log ("Storing the following preferences: ");
	console.dir (prefs);

	// Store preferences
	dojo.xhrPost({
		url: "StorePreferences.json",
		timeout: 30000, // give up after 30 seconds
		content: prefs,
		handleAs: "json",
		load: function(response, ioArgs) {
			console.debug("Store preferences response:");
			console.dir(response);
		}
	});
}

// Show import wizard, passing the correct instance name
function ShowImportWizard () {
	var url = 'ImportUsers';
	if (dijit.byId ('instance_selector')) {
		url += '?instance=' + document.getElementById ('instance_selector').value;
	}
	window.location = url;
}

// Select all checkboxes
function ImportSelectAll () {
	dojo.query ('div.importcheckbox input').forEach (function (elm) {
		elm.checked = 'true';
	});
}

// Unselect all checkboxes
function ImportSelectNone () {
	dojo.query ('div.importcheckbox input').forEach (function (elm) {
		elm.checked = false;
	});
}

var timer;
var delay = 3000;
function TriggerCount () {
	// Drop any existing timer
	if (timer) {
		clearTimeout (timer);
	}

	// Start new timer
	timer = setTimeout (function () {
		var form_data = {};
		dojo.query ('form input').forEach (function (item){
				if (item.name && item.value) {
					form_data[item.name] = item.value;
				}
			});
		form_data.mode = 'count';
		console.dir (form_data);

		dojo.xhrPost({
			url: "GetUserData",
			timeout: 30000, // give up after 3 seconds
			content: form_data,
			handleAs:"json",
			load: function(response, ioArgs){
				var count = response.count ? response.count : 0;
				console.log ('Matches: ' + count)
				dojo.query ('span.counter-value').forEach (function (item) {
						item.innerHTML = count;
					});
				dojo.query ('span.matches-note').forEach (function (item) {
						item.style.display = 'inline';
					});
			},
			error: function(err, ioArgs){
				// Just ignore errors
			}
		});

		delay = 500;
	}, delay);
}

function DisplayUserEdit (rowid) {
	console.debug('rowid ' + rowid);

	EraseErrors ();

	// Default action is to post form
	form_button_click_handler && form_button_click_handler.remove ();
	form_button_click_handler = dojo.query ('#edit_user_validate_button button').on ('click', function () {
		edit_form.validate();
	});

	if (rowid !== undefined) {
		dojo.xhrGet({
			url: "GetUser.json",
			timeout: 30000, // give up after 3 seconds
			content: { rowid:rowid  },
			handleAs:"json",
			load: function(response, ioArgs){
				console.debug("load function  response " + response);
				var form = dijit.byId('edit_form');
				form.reset ();
				console.debug("form " + form);
				form.setValues(response.user);
				dojo.byId ('error_block').style.display = 'none';

				if (dojo.byId ('instances_list_container')) {
					dojo.byId ('instances_list_container').setAttribute ('style', 'display: block');
				}

				// Show create dialog (ie enable and show all fields)
				var disable_fields = (canmodify === false) ? true : false;
				["firstname", "lastname", "password", "password2", "description"].map (function (name) {
					dojo.byId ('user-' + name + '-cont').style.display = "block";
					dijit.byId (name).attr ('disabled', disable_fields);
				});
				if (disable_fields) {
					dijit.byId ('rowid').attr ('disabled', true);
					dijit.byId ('ident').attr ('disabled', true);
					dijit.byId ('email').attr ('disabled', true);
				}
				else {
					dijit.byId ('rowid').attr ('disabled', false);
					dijit.byId ('ident').attr ('disabled', false);
					dijit.byId ('email').attr ('disabled', false);
				}

				if (dojo.byId ('instances')) {
					dojo.byId ('instances_list').innerHTML = '';
					var instances_list = dojo.create ('ul', null, dojo.byId ('instances_list'));
					dojo.forEach (
						response.user.instances,
						function (item) {
							dojo.create ('li', { innerHTML: item }, instances_list);
						}
					);
				}

				dojo.byId("UserForm").style.display = "block";
				dojo.byId("PasswordPolicy").style.display = "none";
				dojo.byId("EditImportBox").style.display = "none";
				dijit.byId('edit_user').show();
			},
			error: function(err, ioArgs){
				alert('Error DisplayUserEdit ' + err);
			}
		});
	}
	else {
		dijit.byId('ident').attr('disabled', false);
		dojo.byId ('UserForm').setAttribute ('style', 'display: block');
		dojo.byId ('EditImportBox').setAttribute ('style', 'display: none');
		dojo.byId ('EditImportBoxContent').innerHTML = '';
		if (dojo.byId ('instances_list_container')) {
			dojo.byId ('instances_list_container').setAttribute ('style', 'display: none');
		}

		// Show import dialog (ie disable and hide all fields but email)
		["rowid", "ident", "firstname", "lastname", "password", "password2", "description"].map (function (name) {
			if (dojo.byId ('user-' + name + '-cont')) { dojo.byId ('user-' + name + '-cont').style.display = "none"; }
			dijit.byId (name).attr ('disabled', true);
		});
		dijit.byId ('email').attr ('value', "");
		dijit.byId ('email').attr ('disabled', false);

		dojo.byId("UserForm").style.display = "block";
		dojo.byId("PasswordPolicy").style.display = "none";
		dojo.byId("EditImportBox").style.display = "none";
		dijit.byId('edit_user').show();
	}
}

function EraseErrors() {
	console.debug('EraseErrors');
	dojo.byId ('error_block').style.display = 'none';
	dojo.byId('error_block').innerHTML= "";
	dojo.query('.form .form-item p.error').orphan();
	dojo.query('.form .form-item label.error').removeClass ('error');
}

function CancelUserDialog() {
	console.debug('Cancel called');
	EraseErrors();
}

function AdaptUI () {
	switch (parseInt (dijit.byId ('instance_selector').attr ('value'), 10)) {
		case 2:
			canmodify = true;
			canimport = true;
			break;
		case 1:
			canmodify = false;
			canimport = true;
			break;
		default:
			canmodify = false;
			canimport = false;
			break;
	}

	if (canmodify || canimport) {
		dojo.byId ("add_user_menu_item").style.display = "block";
		dojo.byId ("csv_import_users_menu_item").style.display = "block";
		dojo.query ('#edit_form ul.button_list').style ('display', 'block');
	}
	else {
		dojo.byId ("add_user_menu_item").style.display = "none";
		dojo.byId ("csv_import_users_menu_item").style.display = "none";
		dojo.query ('#edit_form ul.button_list').style ('display', 'none');
	}
}

dojo.addOnLoad (function () {
	// In multi-instance mode, overload 'RefreshGrid' function to add instance to WebService arguments
	if (document.getElementById ('instance_selector')) {
		window['RefreshGrid.orig'] = window['RefreshGrid'];

		window['RefreshGrid'] = function () {
			window['RefreshGrid.orig'] ({instance: document.getElementById ('instance_selector').value}, AdaptUI);
		}
	} else {
		canmodify = +(dijit.byId('canmodify').attr('value'));
		canimport = +(dijit.byId('canimport').attr('value'));
	}
});
