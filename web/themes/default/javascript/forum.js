// ----------------------------------------------------------------------
// SetParameters
//  displays the set parameters dialog
// ----------------------------------------------------------------------
function SetParameters() {
	Element.show('modal-box');
	Element.show('dialog_parameters');
}
function ValidateParameters() {
	Element.hide('dialog_parameters');
	Element.hide('modal-box');
	document.forum_parameters.submit();
}
// ----------------------------------------------------------------------
// SetUserParameters
//  displays the user parameters dialog
// ----------------------------------------------------------------------
function SetUserParameters() {
	Element.show('modal-box');
	Element.show('dialog_user_params');
}
function ValidateUserParameters() {
	Element.hide('dialog_user_params');
	Element.hide('modal-box');
	document.forum_user_params.submit();
}
// ----------------------------------------------------------------------
// AddCategory (url)
//   url : URL to call for Ajax resquest
// This function make an Ajax request to add a category.
// On success, it insert the result at the end of categories list
// ----------------------------------------------------------------------
function AddCategory(url) {
	var params = 'action=add_category';
	var handlerFunc = function(t) {
		new Insertion.Bottom('categories', t.responseText);
	}

	var errFunc = function(t) {
		alert('Error ' + t.status + ' -- ' + t.statusText);
	}

	new Ajax.Request(url, {method:'post', parameters:params, onSuccess:handlerFunc, onFailure:errFunc});
}
// ----------------------------------------------------------------------
// AddThematic (url)
//   url : URL to call for Ajax resquest
//   list_id : element's ID for insertion
// This function make an Ajax request to add a thematic.
// On success, it insert the result at the end of thematic list
// ----------------------------------------------------------------------
function AddThematic(url, list_id) {
	var params = 'action=add_thematic';
	var list_elem = $(list_id);
	var handlerFunc = function(t) {
		new Insertion.Bottom(list_elem, t.responseText);
	}

	var errFunc = function(t) {
		alert('Error ' + t.status + ' -- ' + t.statusText);
	}

	new Ajax.Request(url, {method:'post', parameters:params, onSuccess:handlerFunc, onFailure:errFunc});
}
// ----------------------------------------------------------------------
// ProcessAnimAction (url)
//   url : URL to call for Ajax resquest
// This function makes an Ajax request to "ProcessAnimAction".
// On success, it reloads the current page as it was the original behavior of the application
// ----------------------------------------------------------------------
function ProcessAnimAction(url) {
	var handlerFunc = function(t) {
		window.location.reload();
	}

	var errFunc = function(t) {
		alert('Error ' + t.status + ' -- ' + t.statusText);
	}

	new Ajax.Request(url, {method:'post', onSuccess: handlerFunc, onFailure: errFunc});
}
// ----------------------------------------------------------------------
// CreateSubject(thematic_rowid)
//  displays the create subject dialog
//  and initialise the thematic_id hidden input
// ----------------------------------------------------------------------
function CreateSubject(url, category_rowid, thematic_rowid) {
	var params = 'action=create_subject&category_id=' + category_rowid + '&thematic_id=' + thematic_rowid;

	var handlerFunc = function(t) {
		Element.update('subject_dialog', t.responseText);
		Element.show('subject_dialog');
		tinyMCE.idCounter=0;
		tinyMCE.execCommand("mceAddControl", false, "message");
		Element.show('modal-box');
		setVisible($('subject_dialog'));
	}

	var errFunc = function(t) {
		alert('Error ' + t.status + ' -- ' + t.statusText);
	}

	new Ajax.Request(url, {method:'get', parameters:params, onSuccess:handlerFunc, onFailure:errFunc});
	alert('avant return');
	return false;
}
function SubmitSubjectDialog(url) {
	$('message').value = tinyMCE.getContent();
	var params = Form.serialize($('form_subject'));

	var handlerFunc = function(t) {
		var s = new String(t.responseText);
		if (s.search(/id="subject_dialog_content"/) != -1) {
			Element.update('subject_dialog', t.responseText);
			tinyMCE.idCounter=0;
			tinyMCE.execCommand("mceAddControl", false, "message");
		}
		else {
			Element.update('forum_main', t.responseText);
			Element.hide('modal-box');
			setHidden($('subject_dialog'));
			Element.hide('subject_dialog');
		}
	}

	var errFunc = function(t) {
		alert('Error ' + t.status + ' -- ' + t.statusText);
	}

	new Ajax.Request(url, {method:'post', parameters:params, asynchronous:true, onSuccess:handlerFunc, onFailure:errFunc});
}
// ----------------------------------------------------------------------
// SignalMessage
// ----------------------------------------------------------------------
function SignalMessage(url, message_id) {
	var params = 'action=signal_message&message_id=' + message_id;
	var handlerFunc = function(t) {
		alert(t.responseText);
	}

	var errFunc = function(t) {
		alert('Error ' + t.status + ' -- ' + t.statusText);
	}

	new Ajax.Request(url, {method:'get', parameters:params, asynchronous:true, onSuccess:handlerFunc, onFailure:errFunc});
}
