var range_link;
var cells;
var selected_elts;
var image_uri;

function createCopy() {
  var planning      = $('planning');
  var planning_copy = $('table-copy');
  var header_dim    = Element.getDimensions(planning.down('th'));
  var table_dim     = Element.getDimensions(planning);
  header_width      = header_dim.width + 1;
  header_height     = table_dim.height;
  
  planning_copy.setStyle({top: planning.offsetTop + 'px',
                          left: planning.offsetLeft + 'px',
                          width: header_width + 'px'});
  
  var pcopy = planning.cloneNode(false);
  pcopy.removeAttribute('id');
  var tbody = Builder.node('tbody');
  var trs   = planning.getElementsBySelector('tr');
  trs.each( function(elt) {
    var tr        = elt.cloneNode(false);
    var cell      = Element.down(elt, 'td') || Element.down(elt, 'th');
    var cell_dim  = Element.getDimensions(cell);
    var copy      = cell.cloneNode(true);
    copy.removeAttribute('colspan');
    
    copy.setStyle({height: cell_dim.height});
    if (! (copy.match('td.header') || copy.match('th')))
      copy.setStyle({border: 'none'});
    tr.appendChild(copy);
    tbody.appendChild(tr);
  });
  
  pcopy.appendChild(tbody);
  planning_copy.appendChild(pcopy);
}

function adjustCopy(event) {
  var copy        = $('table-copy');
  var planning    = $('planning');
  var widthOffset = Math.abs(Position.page(document.body)[0]);
  var poffset;
  
  if ((poffset = Position.page(planning)[0]) >= 0)
    widthOffset += poffset;
  copy.setStyle({left: widthOffset + 'px'});
}

function positionTime() {
  var p   = $('time-arrows');
  var dim = Element.getDimensions(p);
  
  var doc_width   = Element.getDimensions(document.body).width;
  var planning    = $('planning');
  var widthOffset = Math.abs(Position.page(document.body)[0]);
  var poffset;
  
  if ((poffset = Position.page(planning)[0]) >= 0)
    widthOffset += poffset;
  p.setStyle({left: (Math.ceil((doc_width - dim.width) / 2) + widthOffset) + "px"});
}

function beginSelection(evt) {
  if (!Event.isLeftClick(evt))
    return;
    
  var x = Event.pointerX(evt);
  var y = Event.pointerY(evt);
  var elem  = Event.element(evt);
  selected_elts = new Array();
  cells = null;
  
	if (Element.match(elem, 'td.date')) {
	  var tr = Element.up(elem, 'tr');
	  cells  = tr.getElementsBySelector('td.date');
	 	
	  selected_elts.push(elem);
	  Element.addClassName(elem, 'selected');
	  Event.observe(document, 'mousemove', moveHandler);
	  Event.observe(document, 'mouseup', upHandler);
	}
}

function moveHandler(event) {
  var elt = Event.element(event);
  
  if (cells.include(elt)) {
    var sidx  = cells.indexOf(selected_elts[0]);
    var eidx  = cells.indexOf(elt);
    
    selected_elts.invoke('removeClassName', 'selected');
    selected_elts.clear();
    (sidx < eidx ? $R(sidx, eidx) : $R(eidx, sidx)).each(function(idx) {
      selected_elts.push(cells[idx]);
      cells[idx].addClassName('selected');
    });
    if (eidx < sidx)
      selected_elts.reverse();
    selected_elts.uniq();
  }
}

function upHandler(event) {
  Event.stopObserving(document, 'mouseup', upHandler);
  Event.stopObserving(document, 'mousemove', moveHandler);
  
  var sidx  = cells.indexOf(selected_elts.first());
  var eidx  = cells.indexOf(selected_elts.last());
  
  if (eidx < sidx)
    selected_elts.reverse();
  
  var stag = selected_elts.first();
  var etag = selected_elts.last();
  
  var loc_tpl  = new Template("#{link}?day=#{sday}&month=#{smon}&year=#{syear}&ident=#{ident}&eday=#{eday}&emon=#{emon}&eyear=#{eyear}");
  selected_elts.invoke('removeClassName', 'selected');
  window.location = loc_tpl.evaluate({ link: range_link,
                                sday: stag.readAttribute('day'),
                                smon: stag.readAttribute('month'),
                                syear: stag.readAttribute('year'),
                                ident: stag.readAttribute('ident'),
                                eday: etag.readAttribute('day'),
                                emon: etag.readAttribute('month'),
                                eyear: etag.readAttribute('year')
                              });
}

function serializeUsers() {
  var users_div   = $('users_infos');
  var users_list  = $('users');
  var input_tpl   = new Template("<input type=\"hidden\" name=\"users\" value=\"#{val}\"/>");
  
  users_div.update(
    users_list.getElementsBySelector('tr').collect( function(tr) {
        return tr.id.match(/users_(.*)/)[1]
      } ).collect( function(id) {
        return input_tpl.evaluate({val: id});
      } ).join('')
  );
}

function addSeparator() {
  var hr = Builder.node('tr', {id: 'users_0'}, [ 
    Builder.node('td', {className: 'separator'}, [
      Builder.node('a', {href: '#', 
                         title: 'Supprimer ce séparateur', 
                         onclick: 'removeSeparator(this); return false;'}, [
        Builder.node('img', {src: image_uri+'/16x16/actions/trash-empty.png'})
      ]),
      Builder.node('hr')
    ])
  ]);
  
  $('users').appendChild(hr);
  Sortable.create("users", {tag: 'tr', scroll: window});
}

function removeSeparator(elt) {
  var tr = Element.up(elt, 'tr');
  tr.remove();
  
  Sortable.create("users", {tag: 'tr', scroll: window});
}
