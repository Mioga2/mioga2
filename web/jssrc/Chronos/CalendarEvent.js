// =============================================================================
/**
Create a calendar event object.

    var params = {
        title : "title of event",
		....
    };

@class CalendarEvent
@constructor
@param {Object} params
**/
// =============================================================================
function CalendarEvent(params) {
	// initialization from parameters or default values
	this.title       = params.title || "";
	this.desc        = params.desc || "";
	this.allDay      = params.allDay || false;
	this.category_id = params.category_id || undefined;
	this.rowid       = params.rowid || undefined;
	this.calendar    = params.calendar || undefined;	
	// TODO get a real unique identifier
	this.uid      = params.uid || new Date().getTime();

	MIOGA.debug.CalendarEvent = 0;

	if (params.start > params.end) {
		throw new Error("Bad start and end date");
	}
	if (params.start) {
		this.start    = params.start;
	}
	else {
		this.start = new Date();
		this.start.setMinutes(0);
	}
	if (params.end) {
		this.end      = params.end;
	}
	else {
		this.end = new Date();
		this.end.setHours(this.end.getHours() + 1);
		this.end.setMinutes(0);
	}

	// Recurrence
	this.fl_rrule          = params.fl_rrule || false;
	if (params.rrule) {
		MIOGA.logDebug('CalendarEvent', 1, 'params.rrule defined');
		this.rrule = params.rrule;
		this.rrule.base_start = params.base_start || params.start;
		this.rrule.base_end = params.base_end || params.end;
	}
	else {
		MIOGA.logDebug('CalendarEvent', 1, 'params.rrule not set => default values');
		this.rrule = { "period"       : 1,
						"repeat"      : "daily", // daily, weekly, monthly, yearly
						"endChoice"   : 1,  // 1 = no end, 2 = count, 3 = date
						"endCount"    : 0, // 0 = infinity
						"endDate"     : "", // "" = infinity
						"weekdays"    : [ false, false, false, false, false, false, false ],  // 7 days
						"monthmode"   : 1, // 1 : fixed day, 2 : relative day, 3 : last day of month
						"fixmonthday" : this.start.getDate(),
						"monthdaypos" : 1,  // 1 = first, 2 = second, 3 = third, 4 = fourth, 5 = last
						"monthday"    : 1,  // 0 = sunday, 1 = monday, etc...
						"fixyearday" : this.start.getDate(),
						"fixyearmonth" : this.start.getMonth() + 1
					};
		//this.rrule.exceptions  = [];
		this.rrule.base_start = params.start;
		this.rrule.base_end = params.end;
	}

	
	// --------------------------------------------------------
	// modify change date parameters for given event
	// --------------------------------------------------------
	this.modify = modify;
	function modify(params, revertFunc) {
		MIOGA.logDebug('CalendarEvent', 1, "MiogaCalendar modify");
		this.allDay   = params.allDay;
		this.start    = params.start;
		this.end      = params.end;
	}
	// --------------------------------------------------------
	// serializeICAL
	// --------------------------------------------------------
	this.serializeICAL = serializeICAL;
	function serializeICAL() {
		MIOGA.logDebug('CalendarEvent', 1, "MiogaCalendar serializeICAL");
		var limit75 = function(text) {
			var out = '';
			while (text.length > 75) {
				out += text.substr(0, 75) + '\n';
				text = ' ' + text.substr(75);
			}
			out += text;
			return out;
		};

		var ical_event = "BEGIN:VEVENT\n"
						+ "UID:" + this.uid + "\n"
						+ "DTSTAMP:" + this.formatICALDateTime(new Date()) + "\n"
						+ limit75("TITLE:" + this.title) + "\n"
						+ "DTSTART:" + this.formatICALDateTime(this.start) + "\n"
						+ "DTEND:" + this.formatICALDateTime(this.end) + "\n"
						+ (this.desc ? limit75("DESCRIPTION:" + this.desc) + "\n" : "")
						+ (this.fl_rrule ? this.makeRecurrence() + "\n" : "")
						+ "END:VEVENT\n";

		return ical_event;
	}
	// --------------------------------------------------------
	// formatICALDateTime
	// Format date like yyyymmddThhmmss[Z]
	// Z is function of fl_local boolean flag.
	// --------------------------------------------------------
	this.formatICALDateTime = formatICALDateTime;
	function formatICALDateTime(dateTime, fl_local) {
		var ical_date = "",
		    fl_utc = true;

		var ensureTwo = function(value) {
			return (value < 10 ? '0' : '') + value;
		};

		if (fl_local) { fl_utc = false; }

		if (fl_utc) {
			ical_date = dateTime.getFullYear()  + ensureTwo(dateTime.getMonth() + 1) + ensureTwo(dateTime.getDate())
						+ 'T' + ensureTwo(dateTime.getHours()) + ensureTwo(dateTime.getMinutes()) + ensureTwo(dateTime.getSeconds());
		}
		else {
			ical_date = dateTime.getUTCFullYear()  + ensureTwo(dateTime.getUTCMonth() + 1) + ensureTwo(dateTime.getUTCDate())
						+ 'T' + ensureTwo(dateTime.getUTCHours()) + ensureTwo(dateTime.getUTCMinutes()) + ensureTwo(dateTime.getUTCSeconds()) + 'Z';
		}
		return ical_date;
	}
	// --------------------------------------------------------
	// makeRecurrence
	// Generate RRULE line
	// --------------------------------------------------------
	this.makeRecurrence = makeRecurrence;
	function makeRecurrence() {
		var rrule = "RRULE:";

		var wdays = ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'];
		rrule += "FREQ=";
		if (this.rrule.repeat === 1) {
			rrule += "DAILY;"
			rrule += "INTERVAL=" + this.rrule.period + ";";
		}
		else if (this.rrule.repeat === 2) {
			rrule += "WEEKLY;"
			rrule += "INTERVAL=" + this.rrule.period + ";";
			var wd = new Array();
			for (var i = 0; i < 7; i++) {
				if (this.rrule.weekdays[i]) {
					wd.push(wdays[i]);
				}
			}
			if (wd.length > 0) {
				rrule += "BYDAY=" + wd.join() + ";"
			}
			else {
				alert("ERROR ! No weekdays in a rrule line");
			}
		}
		else if (this.rrule.repeat === 3) {
			rrule += "MONTHLY;"
			rrule += "INTERVAL=" + this.rrule.period + ";";
		}
		else {
			rrule += "YEARLY;"
			rrule += "INTERVAL=" + this.rrule.period + ";";
		}

		if (this.rrule.endChoice === 2) {
			rrule += "UNTIL=" + this.formatICALDateTime(this.rrule.endDate);
		}
		else if (this.rrule.endChoice === 3) {
			rrule += "COUNT=" + this.rrule.endCount
		}
		return rrule;
	}
	// --------------------------------------------------------
	// stringify
	// Format event for JSON POST
	// --------------------------------------------------------
	this.stringify = stringify;
	function stringify() {
		var data;

		// Use UTC Time only for allday event
		// Other events are set on UTC time
		if (this.allDay) {
			data = { "start_year"    :this.start.getFullYear(),
			 		 "start_month"   :this.start.getMonth() + 1,
			 		 "start_day"     :this.start.getDate(),
			 		 "start_hours"   :"00",
			 		 "start_minutes" :"00",
			 		 "start_seconds" :"00",
			         "end_year"      :this.end.getFullYear(),
			 		 "end_month"     :this.end.getMonth() + 1,
			 		 "end_day"       :this.end.getDate(),
			 		 "end_hours"     :"23",
			 		 "end_minutes"   :"59",
			 		 "end_seconds"   :"59"
					};
		} else {
			data = { "start_year"    :this.start.getUTCFullYear(),
			 		 "start_month"   :this.start.getUTCMonth() + 1,
			 		 "start_day"     :this.start.getUTCDate(),
			 		 "start_hours"   :this.start.getUTCHours(),
			 		 "start_minutes" :this.start.getUTCMinutes(),
			 		 "start_seconds" :this.start.getUTCSeconds(),
			         "end_year"      :this.end.getUTCFullYear(),
			 		 "end_month"     :this.end.getUTCMonth() + 1,
			 		 "end_day"       :this.end.getUTCDate(),
			 		 "end_hours"     :this.end.getUTCHours(),
			 		 "end_minutes"   :this.end.getUTCMinutes(),
			 		 "end_seconds"   :this.end.getUTCSeconds()
					};
		}
		// Other parameters
		data.subject     = this.title;
	 	data.desc        = this.desc;
		data.all_day     = this.allDay;
		data.fl_recur    = this.fl_rrule;
		data.rrule       = this.rrule;
		//data.icalendar   = this.serializeICAL();
		data.category_id = this.category_id;

		return JSON.stringify(data);
	}

}

