// =============================================================================
/**
Display the main Chronos window.

    var options = {
        i18n : { ... }
    };

@class ChronosMain
@constructor
@param {Object} elem JQuery element parent of progress bar
@param {Object} options
**/
// =============================================================================
function ChronosMain(elem, options) {
	this.elem           = elem;
	this.mode           = options.mode;
	this.firsthour      = options.firsthour;
	this.showweekend    = options.showweekend;
	this.chronos        = options.chronos;
	this.categories     = options.categories;
	this.i18n           = options.i18n;
	this.currentView    = options.currentView;
	this.CL             = options.CL;
	this.PL             = options.PL;
	this.configChangeCB = options.configChangeCB;

	var that = this; // For callback reference to current object
	// debug level
	MIOGA.debug.ChronosMain = 0;

	MIOGA.logDebug("ChronosMain", 1, "ChronosMain constructor begin >>>>>>>>>>>>>>>>>>>>>");
	// --------------------------------------------------------
	// Pre declare utility functions
	// --------------------------------------------------------
	this.refreshCL = refreshCL;

	this.base_id = $(this.elem).prop('id');
	MIOGA.logDebug("ChronosMain", 3, "    base_id = " + this.base_id);

	this.$cont = $('<div class="chronos-main"></div>').appendTo(this.elem);
	// ------------------------------------------------------------------------------------------------
	// Sidebar and its content
	// ------------------------------------------------------------------------------------------------
	var $sb = $('<div class="chronos-sb"></div>').appendTo(this.$cont);

	// ------------------------------------------------------------------------------------------------
	// Actions menu
	// ------------------------------------------------------------------------------------------------
	var $action_list = $('<ul class="action vmenu"></ul>').appendTo($sb);

	// if in group mode and calendar nowritable, do not display Create button
	var canCreate = true;
	if (this.mode === 'group' && this.CL.currentCal.access !== 2) {
		canCreate = false;
	}
	// Create button
	if (canCreate) {
		var create_id = MIOGA.generateID("chronos");
		$('<li><a id="'+create_id+'" href="#create">'+this.i18n.createText+'</a></li>').appendTo($action_list);
		$('#'+create_id).click(function(event) {
								MIOGA.logDebug("ChronosMain", 1, "Create new event");
								var ev = new CalendarEvent({ });
								that.chronos.initEventEditor(ev, 'create', null);
							});
	}
	// Configuration button
	if (this.chronos.animRight) {
		var config_id = MIOGA.generateID("chronos");
		$('<li><a id="'+config_id+'" href="#config">'+this.i18n.configurationText+'</a></li>').appendTo($action_list);
	}
	// ProjectTasks button
	var prj_task_id = MIOGA.generateID("chronos");
	if (this.mode === 'user') {
		$('<li><a id="'+prj_task_id+'" href="#project_tasks">'+this.i18n.projectTasksText+'</a></li>').appendTo($action_list);
	}

	// Tasks button
	// TODO implement it
	//var task_id = MIOGA.generateID("chronos");
	//$('<li><a id="'+task_id+'" href="#tasks">'+this.i18n.tasksText+'</a></li>').appendTo($action_list);

	// minical
	MIOGA.logDebug("ChronosMain", 2, "set minical defaultView to " + this.currentView);
	var mcal_opts = $.extend({
						"view" : this.currentView,
						"titleCB" : function(event, y, m) {
										MIOGA.logDebug("ChronosMain", 1, "minical titleCB y = " + y + "  m = " + m);
										var date = that.$fc.fullCalendar('getDate');
										MIOGA.logDebug("ChronosMain", 1, "current date = "+date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate());
										that.$fc.fullCalendar('changeView', "month");
										that.$fc.fullCalendar('gotoDate', y, m);
									},
						"weekCB" : function(event, y, m, d) {
										MIOGA.logDebug("ChronosMain", 1, "minical weekCB");
										var date = that.$fc.fullCalendar('getDate');
										MIOGA.logDebug("ChronosMain", 1, "current date = "+date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate());
										that.$fc.fullCalendar('changeView', "agendaWeek");
										that.$fc.fullCalendar('gotoDate', y, m, d);
									},
						"dayCB": function(event, y, m, d) {
										MIOGA.logDebug("ChronosMain", 1, "minical dayCB  date = " + y + "-" + m + "-" + d);
										that.$fc.fullCalendar('changeView', "agendaDay");
										that.$fc.fullCalendar('gotoDate', y, m, d);
									}
					}, options);
	MIOGA.logDebug("ChronosMain", 1, "mcal_opts.firstDay = " + mcal_opts.firstDay + "  option.firstDay = " + options.firstDay);
	this.$mcal = $('<div class="chronos-minical"></div>').appendTo($sb).minical(mcal_opts);
	//this.$mcal = $('<div class="chronos-minical"></div>').appendTo($sb);
	// Calendar List
	if (this.mode === 'user') {
		var $list_cont = $('<div class="chronos-box"></div>').appendTo($sb);
		$('<h2 class="calchoice-list-title">'+this.i18n.calendarListTitle+'</span></h2>').appendTo($list_cont);
		this.$callist = $('<ul class="calchoice-list"></ul>').appendTo($list_cont);
	}
	// ------------------------------------------------------------------------------------------------
	// Full Calendar
	// ------------------------------------------------------------------------------------------------
	MIOGA.logDebug("ChronosMain", 2, "set FC defaultView to " + this.currentView);
	var fc_opts = $.extend({
							"weekMode" : "liquid",
							"lazyFetching":false,
							"height" : 600,
							"firstHour" : this.firsthour,
							"weekends" : this.showweekend,
							//"aspectRatio" : 2,
							"defaultView" : this.currentView,
							"theme":true,
							"viewDisplay": function(view) {
												fcChangeViewCB.call(that, view);
											},
							"selectable": true,
							"editable": true,
							"selectHelper": false,
							"select": function(start, end, allDay,jsEvent,view) {
											// TODO test write access
											if (that.CL.currentCal) {
												if (that.CL.currentCal.access === 2) {
													that.createEventDialog.init(start, end, allDay);
													that.createEventDialog.$cont.dialog('open');
												} else {
													alert(that.i18n.errorCalendarReadOnly);
												}
											}
											else {
												// TODO translate
												alert(that.i18n.errorNoCalendar);
											}
										},
							"eventClick": function(fc_event, jsEvent, view) {
												MIOGA.logDebug("ChronosMain", 1, "event click  event = " + $.type(fc_event.ev));
												if (fc_event.ev instanceof CalendarEvent) {
													if (fc_event.ev.calendar.access != 2) {
														alert(that.i18n.errorCalendarReadOnly);
													} else {
														that.chronos.initEventEditor(fc_event.ev, 'edit', null);
														window.location.hash = "#create";
													}
												} else {
													window.location.hash = "#project_tasks";
													//alert(that.i18n.errorCannotModifyProjectTask);
												}
							              },
							"eventDrop": function(fc_event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
												MIOGA.logDebug("ChronosMain", 2, "event drop");
												// CalendarEvent
												if (fc_event.ev instanceof CalendarEvent) {
													if (fc_event.ev.calendar.access != 2) {
														alert(that.i18n.errorCalendarReadOnly);
														revertFunc();
													}
													if (fc_event.ev.fl_rrule) {
														alert(that.i18n.errorCannotModifyRecurEvent);
														revertFunc();
													} else {
														fc_event.ev.modify(fc_event);
														fc_event.ev.calendar.modifyEvent({ "calEvent" : fc_event.ev, "error" : revertFunc });
													}
												} else {
												// ProjectTask
													alert(that.i18n.errorCannotModifyProjectTask);
													revertFunc();
												}
											},
							"eventResize": function(fc_event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
												MIOGA.logDebug("ChronosMain", 2, "event resize");
												// CalendarEvent
												if (fc_event.ev instanceof CalendarEvent) {
													if (fc_event.ev.calendar.access != 2) {
														alert(that.i18n.errorCalendarReadOnly);
														revertFunc();
													}
													if (fc_event.ev.fl_rrule) {
														alert(that.i18n.errorCannotModifyRecurEvent);
														revertFunc();
													} else {
														fc_event.ev.modify(fc_event);
														fc_event.ev.calendar.modifyEvent({ "calEvent" : fc_event.ev, "error" : revertFunc });
													}
												} else {
												// ProjectTask
													alert(that.i18n.errorCannotModifyProjectTask);
													revertFunc();
												}
											},
							"eventRender": function(fc_event, elem, view) {
												MIOGA.logDebug("ChronosMain", 1, "event render", fc_event);
												if (fc_event.hasOwnProperty('borderColor')) {
													$('<div class="fc-color-block" style="background-color:'+fc_event.borderColor+';"></div>').appendTo(elem);
												}
												if (fc_event.hasOwnProperty('allDay')) {
													if(fc_event.allDay) {
													}
													else {
														$(elem).find('.fc-event-content').append('<div class="fc-event-desc">'+fc_event.ev.desc+'</div>');;
													}
												}
											},
							"timeFormat":"HH:mm",
							"axisFormat":"HH:mm",
							"events": function(start, end, cb) {
									MIOGA.logDebug("ChronosMain", 2, 'ChronosMain, get events for ' + start + ' to ' + end);
									getEvents.call(that, start, end, cb);
							}
						}, options);
	this.fullcall_id = this.base_id+"-fc";
	this.$fc = $('<div id="'+this.fullcal_id+'" class="chronos-fc"></div>').appendTo(this.$cont).fullCalendar(fc_opts);

	// categories legend
	this.$button_legend = $('<div class="legend-title-cont"><div class="legend-title">' + that.i18n.categoryLegend + '</div><div class="icon-down"></div></div>');
	this.$cont_legend = $('<div class="legend-list"></div>');
	this.$legend_action = $('<div class="legend-action"><button class="legend-back">' + that.i18n.reattachWindow + '</button></div>').appendTo(that.$cont_legend);
	
	this.$legend_cont = $('<div class="legend-cont toolbar-item"></div>').prependTo(this.$fc.find('.fc-header-right'));
	this.$legend_cont.after('<span class="fc-header-space"></span>');
	this.$legend_action.find('.legend-back').hide();	
	this.$legend_cont.dropDown({
		button : that.$button_legend,
		content : that.$cont_legend,
		openDirection : "left"
	});
	that.$cont_legend.parent().draggable({
		start : function (event, ui) {
			if (that.$legend_cont.parent().data('origin_position') === undefined) {
				that.$legend_cont.parent().data('origin_position', ui.position);
			}
			that.$legend_action.find('.legend-back').show();
		}
	}).delegate('.legend-back', 'click', function () {
		that.$cont_legend.parent().draggable('disable');
		that.$cont_legend.parent().animate(that.$legend_cont.parent().data('origin_position'),"slow", function () {
			that.$legend_action.find('.legend-back').hide();
			that.$cont_legend.parent().draggable('enable');
		});
	});

	//this.$fc = $('<div id="'+this.fullcal_id+'" class="chronos-fc"></div>').appendTo(this.$cont);

	// ------------------------------------------------------------------------------------------------
	// Dialog to create simple event
	// ------------------------------------------------------------------------------------------------
	var dialog_opts = $.extend({
						calendarList : this.CL,
						categories : this.categories,
						fc : this.$fc,
						chronos : this
					}, options);

	this.createEventDialog = new CreateEventDialog(elem, dialog_opts);

	this.CL.addRefreshCB(this.refreshCL, this);
	this.refreshCL();
	
	MIOGA.logDebug("ChronosMain", 1, "that ", that);
	MIOGA.logDebug("ChronosMain", 1, "constructor end <<<<<<<<<<<<<<<< ");
	// End constructor
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	// -------------------------------------------------------------
	// refreshCL function
	// redraw list content function of calendarList
	// set event sources for fullcalendar
	// -------------------------------------------------------------
	function refreshCL() {
		MIOGA.logDebug("ChronosMain", 1, 'refreshCL  >>>>>>>');
		var that = this;
		MIOGA.logDebug("ChronosMain", 3, 'refreshCL  currentCalendar', this.CL.currentCal);
		if (this.$callist) {
			this.$callist.find('li').remove();
			var cur_type;
			$.each(this.CL.calendars.sort(function(a,b) {
										if (a.type < b.type) { return -1 }
										else if (a.type > b.type) { return 1; }
										return 0;
								}),
								function(num, item) {
									if (cur_type === undefined || cur_type !== item.type) {
										if (item.type === item.CONSTANT.CAL_TYPE.USER) {
											$('<li><h2 class="calchoice-list-title">'+that.i18n.ownerCalendars+'</h2></li>').appendTo(that.$callist);
										}
										else if (item.type ===  item.CONSTANT.CAL_TYPE.GROUP) {
											$('<li><h2 class="calchoice-list-title">'+that.i18n.groupCalendars+'</h2></li>').appendTo(that.$callist);
										}
										else {
											$('<li><h2 class="calchoice-list-title">'+that.i18n.otherCalendars+'</h2></li>').appendTo(that.$callist);
										}
										cur_type = item.type;
									}
									drawCalListItem.call(that, num, item);
			});
		}

		MIOGA.logDebug("ChronosMain", 2, ' refetchEvents');
		this.$fc.fullCalendar('refetchEvents');
	}
	// --------------------------------------------------------
	// showWindow display and init window
	// --------------------------------------------------------
	this.showWindow = showWindow;
	function showWindow() {
		MIOGA.logDebug("ChronosMain", 1, 'ChronosMain  showWindow ===');
		if (this.currentDialog) {
			this.currentDialog.hide();
		}
		this.$cont.show();
		//TODO not necessary all times
		MIOGA.logDebug("ChronosMain", 1, "refetchEvents");
		this.$fc.fullCalendar('refetchEvents');
	}
	// --------------------------------------------------------
	// hideWindow hides the window
	// --------------------------------------------------------
	this.hideWindow = hideWindow;
	function hideWindow() {
		MIOGA.logDebug("ChronosMain", 1, 'ChronosMain  hideWindow ===');
		this.$cont.hide();
	}


	// ========================================================================================
	// Private functions (must not be added to "this")
	// ========================================================================================
	// --------------------------------------------------------
	// FullCalendar change view callback
	// --------------------------------------------------------
	function fcChangeViewCB(view) {
		MIOGA.logDebug("ChronosMain", 1, 'fcChangeViewCB view =', view);
		this.$mcal.minical('setView', { "viewType" : view.name, "currentDate" : view.start });
		this.currentView = view.name;
		// TODO test if view change and write access to current calendar to set prefrences

		if (typeof this.configChangeCB === 'function') {
			this.configChangeCB('setView', { "viewType" : view.name, "currentDate" : view.start });
		}
	}
	// -------------------------------------------------------------
	// drawCalListItem
	//    draw an entry in calendar list
	// -------------------------------------------------------------
	function drawCalListItem(num, item) {
		var that = this;
		MIOGA.logDebug("ChronosMain", 1, 'ChronosMain.drawCalListItem num = ' + num);
		var $li = $('<li class="calchoice-item"></li>').appendTo(this.$callist);

		$li.click(function(event) {
			MIOGA.logDebug("ChronosMain", 1, "change current calendar  newId = " + item.rowid);
			if (item.access === 2) {
				if (item.rowid !== that.CL.currentCal.rowid) {
					MIOGA.logDebug("ChronosMain", 1, "    not current");
					that.CL.changeCurrentCalendar(num);
					that.$callist.find('.cal-selected').removeClass('cal-selected');
					$li.addClass('cal-selected');
					if (typeof that.configChangeCB === 'function') {
						that.configChangeCB('setCurCal', { "num" : num });
					}
				}
			} else {
				alert(that.i18n.errorCalendarReadOnly);
			}
		});

		if (that.CL.currentCal && (item.rowid === that.CL.currentCal.rowid)) {
			MIOGA.logDebug("ChronosMain", 1, "    current found");
			$li.addClass('cal-selected');
		}
		var visibility;
		var style;
		if (item.visible) {
			visibility = "cal-visible";
			style = 'style="background-color:'+item.bgcolor+';color:'+item.color+';"';
		}
		else {
			visibility = "cal-hidden";
			style = '';
		}
		$('<div class="'+visibility+'">'+this.i18n.visibleCal+'</div>')
								.appendTo($li)
								.click(function(event) {
									MIOGA.logDebug("ChronosMain", 2, "change visibility for calendar");
									if (item.visible) {
										item.setVisibility(false);
									}
									else {
										item.setVisibility(true);
									}
									that.CL.callRefreshCB();
									return false;  // do not propagate
								});
		if (item.access === 2) {
			$('<div class="read-write">'+this.i18n.readWriteLabel+'</div>').appendTo($li);
		} else {
			$('<div class="read-only">'+this.i18n.readOnlyLabel+'</div>').appendTo($li);
		}
		var $p = $('<p '+style+'></p>').appendTo($li)
		$('<span class="cal-ident" title="'+item.ident+'" >'+item.ident+'</span>').appendTo($p)

	}
	// --------------------------------------------------------
	// getEvents for each calendar of the CalendarList
	// --------------------------------------------------------
	function getEvents(start, end, cb) {
		MIOGA.logDebug("ChronosMain", 2, "ChronosMain.getEvent");
		var that = this;
		var funcs = new Array();
		var events = new Array();
		//var done = false;

		// Get calendars events
		$.each(this.CL.calendars, function(num, calendar) {
			if (calendar.visible) {
				// This function transform CalendarEvent into object usable by FullCalendar
				// It puts the CalendarEvent in attribute for FullCalendar event
				funcs.push(function(callback) {
					MIOGA.logDebug("ChronosMain", 3, 'async function num = '+ num);
					calendar.getEvents(start, end, function(err, result) {
						var events = new Array();
						if (result) {
							MIOGA.logDebug("ChronosMain", 3, ' result count= '+result.length);
							var category;
							var category_list = {};
							for (var i = 0; i < result.length; i++) {
								var evt = new Object();
								evt.title = result[i].title;
								evt.allDay = result[i].allDay;
								evt.start = result[i].start;
								evt.end = result[i].end;
								evt.ev = result[i];

								category = that.chronos.getCategoryByID(result[i].category_id);
								if (category === undefined) {
									category = {};
									category.fgcolor = "#000000";
									category.bgcolor = "#ffffff";
								}
								if (that.chronos.colorscheme === 1) {
									evt.textColor = category.fgcolor;
									evt.borderColor = calendar.bgcolor;
									evt.backgroundColor = category.bgcolor;
									
									if (category_list[category.category_id] === undefined) {
										category_list[category.category_id] = {
											cat_name : category.name,
											bgcolor : category.bgcolor
										};
									}
								}
								else {
									evt.textColor = calendar.color;
									evt.backgroundColor = calendar.bgcolor;
									evt.borderColor = category.bgcolor;
								}
								events.push(evt);
							}
						}
						// HTML categories list
						that.$cont_legend.children().not('.legend-action').remove();
						for (var i in category_list) {
							that.$cont_legend.append('<div class="cat"><span class="col" style="background-color:' + category_list[i].bgcolor + '"></span><p>' + category_list[i].cat_name + '</p></div>')
						}
						callback(err, events);
					});
				});
			}
		});
		// Get ProjectTasks events
		if (that.PL) {
			funcs.push(function(callback) {
				callback(null, that.PL.getEvents());
			});
		}
		MIOGA.logDebug("ChronosMain", 3, 'funcs = ' + funcs, funcs);
		// Process events returns
		if (funcs.length > 0) {
			async.parallel(funcs, function(err, results){
				MIOGA.logDebug("ChronosMain", 2, 'async final callback err = ' + err);
				//if (! done) {
					if (cb) {
						for (var i = 0; i < results.length; i++) {
							events = events.concat(results[i]);
						}
						// resfresh events
						cb(events);
					}
					//done = true;
				//}
				//else {
					//MIOGA.logDebug("ChronosMain", 1, 'async already done');
				//}
			});
		}
	}

};

