// =============================================================================
/**
This module display the create event dialog.

    var options = {
        i18n : { .... }
    };

@class CreateEventDialog
@constructor
@param {Object} elem JQuery element parent of progress bar
@param {Object} options
**/
// =============================================================================
function CreateEventDialog(elem, options) {

	this.elem       = elem;
	this.CL         = options.calendarList;  // to access create methods
	this.categories = options.categories;
	this.$fc        = options.fc;            // to refresh calendar
	this.chronos    = options.chronos;      // to edit details of events
	this.i18n       = options.i18n;
	this.options    = options;

	MIOGA.debug.CreateEventDialog = 0;

	// Constructor, create dialog
	var that = this; // For callback reference to current object
	this.$cont = $('<div class="chronos-dialog"></div>').appendTo(this.elem);
	this.form_id = MIOGA.generateID("chronos");
	this.eventDate_id = MIOGA.generateID("chronos");
	this.subject_id = MIOGA.generateID("chronos");
	this.start_id = MIOGA.generateID("chronos");
	this.end_id = MIOGA.generateID("chronos");
	this.allday_id = MIOGA.generateID("chronos");
	this.categories_id = MIOGA.generateID("chronos");
	this.btnok_id = MIOGA.generateID("chronos");
	this.details_id = MIOGA.generateID("chronos");
	this.$cont.append('<div><form class="form" id="'+this.form_id+'">'
		                +'<p id="'+this.eventDate_id+'" class="eventDate"></p>'
						+'<div class="form_item"><label for="'+this.subject_id+'">'+this.i18n.evSubject+'</label><input id="'+this.subject_id+'" type="text" name="subject" /></div>'
						+'<div class="form-item"><label for="'+this.categories_id+'">'+this.i18n.evCategory+'</label><select id="'+this.categories_id+'" name="category" ></select></div>'
						+'<input id="'+this.start_id+'" type="hidden" name="start" />'
						+'<input id="'+this.end_id+'" type="hidden" name="end" />'
						+'<input id="'+this.allday_id+'" type="hidden" name="allday" />'
					 +'</form></div>'
					 +'<div>'
				        +'<p><button id="'+this.btnok_id+'">'+this.i18n.OKLabel+'</button><a id="'+this.details_id+'" href="#create">'+this.i18n.detailText+'</a></p>'
					 +'</div>');

	$.each(this.categories, function(num, item) {
		MIOGA.logDebug("CreateEventDialog", 1, "category num = " + num);
		$('<option value="'+item.category_id+'" >'+item.name+'</option>').appendTo('#'+that.categories_id);
	});

	this.$cont.dialog({ title: this.i18n.createEventTitle,
			                            autoOpen:false,
										modal:true,
										width:'25em',
										closeOnEscape:true
									});

	this.$cont.find('#' + that.subject_id).on('keypress',function(evt) {
		if ((evt.keyCode == 13) && (evt.target.type !== "input")) {
			createEvent ();
			return (false);
		}
	});

	$('#'+this.btnok_id).click(function(event) {
							createEvent ();
	});
	$('#'+this.details_id).click(function(event) {
							//MIOGA.logDebug("CreateEventDialog", 1, "details event");
							var subject = $('#'+that.subject_id).val();
							$.trim(subject);
							//MIOGA.logDebug("CreateEventDialog", 1, "subject = " + subject);
							try {
								var data = that.$cont.data();
								var ev = new CalendarEvent({"title" : subject,
														"allDay": data.allDay,
														"start": data.start,
														"end": data.end
													 });
								that.chronos.initEventEditor(ev, 'create', function() {
																// TODO check error from Webservice
																//MIOGA.logDebug("CreateEventDialog", 1, "return from create event with code = " + code);
																that.$fc.fullCalendar('refetchEvents');
															});
								that.$cont.dialog('close');
							} catch (e) {  
								//MIOGA.logDebug("CreateEventDialog", 1, e.name);
								//MIOGA.logDebug("CreateEventDialog", 1, e.message);
								$('#'+that.subject_id).addClass('error');
								return false;
							}
	});
	// end constructor
	// ----------------------------------------------------------------
	// init function prepare form elements
	// ----------------------------------------------------------------
	this.init = init;
	function init(start, end, allDay) {
		this.$cont.find('#'+this.eventDate_id).html(that.i18n.dateText +' : ' + that.formatEventDateString(start, end, allDay));
		$('#'+that.subject_id).removeClass('error');
		this.$cont.find('#'+this.subject_id).attr('value', "");
		//TODO format start and end
		this.$cont.find('#'+this.start_id).attr('value', $.fullCalendar.formatDate(start, "yyyy-MM-dd HH:mm"));
		this.$cont.find('#'+this.end_id).attr('value',  $.fullCalendar.formatDate(end, "yyyy-MM-dd HH:mm"));
		this.$cont.find('#'+this.allday_id).attr('value', allDay);
		this.$cont.data({"start" : start, "end":end, "allDay": allDay});
	}
	// -----------------------------------------------------------------------------------------
	// private function that creates event
	// -----------------------------------------------------------------------------------------
	function createEvent () {
		//MIOGA.logDebug("CreateEventDialog", 1, "create event");
		var subject = $('#'+that.subject_id).val();
		$.trim(subject);
		//MIOGA.logDebug("CreateEventDialog", 1, "subject = " + subject);
		try {
			if (subject == "") {
				 throw "Event object can't be empty";
			}
			var data = that.$cont.data();
			var ev = new CalendarEvent({"title" : subject,
									"category_id": $('#'+that.categories_id).val(),
									"allDay": data.allDay,
									"start": data.start,
									"end": data.end
								 });
			var curcal = that.CL.getCurrentCalendar();
			if (curcal) {
				curcal.addNewEvent({"calEvent": ev,
				                    "success": function(code) {
											// TODO check error from Webservice
									  		//MIOGA.logDebug("CreateEventDialog", 1, "return from create event with code = " + code);
											that.$fc.fullCalendar('refetchEvents');
											that.$cont.dialog('close');
										}
									});
			} else {
				alert(that.i18n.errorNoCurrentCalendar);
			}
		} catch (e) {  
			MIOGA.logDebug("CreateEventDialog", 1, e.name);
			MIOGA.logDebug("CreateEventDialog", 1, e.message);
			$('#'+that.subject_id).addClass('error');
		}
	}
	// -----------------------------------------------------------------------------------------
	// formatEventDateString present single line with start and end dates for event
	// -----------------------------------------------------------------------------------------
	this.formatEventDateString = formatEventDateString;
	function formatEventDateString(start, end, allDay) {
		var evDate = "";
		//TODO get format from i18n
		var opt = {
					dayNamesMin  : that.options.dayNamesMin,
					dayNames     : that.options.dayNames,
					monthNames   : that.options.monthNames,
					dayNamesShort   : that.options.dayNamesShort,
					monthNamesShort : that.options.monthNamesShort
				};
		if (allDay) {
			evDate = $.fullCalendar.formatDate(start, this.i18n.dateFormatLong, opt) + ' - ' + $.fullCalendar.formatDate(end, this.i18n.dateFormatLong, opt);
		}
		else {
			evDate = $.fullCalendar.formatDate(start, this.i18n.dateFormatLong, opt) + ' ' + $.fullCalendar.formatDate(start, "HH:mm") + ' - ' + $.fullCalendar.formatDate(end, "HH:mm");
		}
		return evDate;
	}
} // End of CreateEventDialog

