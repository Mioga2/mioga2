// =============================================================================
/**
This module display the task/todo list 

    var options = {
        i18n : { .... }
    };

@class TaskEditor
@constructor
@param {Object} elem JQuery element parent of progress bar
@param {Object} options
**/
// =============================================================================
function TaskEditor(elem, options) {

	this.elem     = elem;                  // parent DOM node
	this.i18n     = options.i18n;

	MIOGA.debug.TaskEditor = 0;

	// Constructor, create dialog
	var that = this; // For callback reference to current object
	// main window
	this.$cont = $('<div class="task-editor"></div>').appendTo(this.elem);

	// navigation bar
	var $nav = $('<div class="app-nav"></div>').appendTo(this.$cont);
	this.$btn_ok = $('<button class="button" >'+this.i18n.OKLabel+'</button>').appendTo($nav);
	this.$btn_ok.click(function(event) {
							MIOGA.logDebug("TaskEditor", 1, "TaskEditor OK button");
							if (that.validateForm()) {
								MIOGA.logDebug("TaskEditor", 1, "return");
								history.back();
							}
						});
	this.$btn_cancel = $('<button class="button cancel" >'+this.i18n.cancelLabel+'</button>').appendTo($nav);
	this.$btn_cancel.click(function(event) {
							history.back();
						});


	// end constructor
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	// --------------------------------------------------------
	// showWindow display and init window
	// --------------------------------------------------------
	this.showWindow = showWindow;
	function showWindow() {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor  showWindow ===');
		this.$cont.show();
	}
	// --------------------------------------------------------
	// hideWindow hides the window
	// --------------------------------------------------------
	this.hideWindow = hideWindow;
	function hideWindow() {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor  hideWindow ===');
		this.$cont.hide();
	}

} // End of TaskEditor

