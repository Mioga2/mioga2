// -----------------------------------------------------------------------------
// Chronos header file to buils effective javascript file
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// Chronos is the calendar application for Mioga2 Project
//     It is construct as a jQuery plugin with advanced functions to acces
//     different calendar formats
//
//     Documentation for methods is inserted as comment
// -----------------------------------------------------------------------------


(function ($) {


var defaults = {
			dayNamesMin : ["Su","Mo","Tu","We","Th","Fr","Sa"],
			dayNames : ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
			dayNamesShort :  ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
			monthNames:["January","February","March","April","May","June", "July","August","September","October","November","December"],
			monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec"],
			firstDay : "0",   // 0 = sunday, 1 = monday
			buttonText : { "today":"today", "month":"month", "week":"week", "day":"day" },
			titleFormat: { "month": "MMMM yyyy", "week": "d[ MMMM yyyy]{ '&#8212; 'd MMMM yyyy}", "day": "dddd, d MMMM yyyy" },
			header : { "left":"title", "center":"", "right":"today prev,next month,agendaWeek,agendaDay" },
			columnFormat : {month: 'ddd', week: 'ddd M/d', day: 'dddd M/d' },
			allDayText : "all day",
			i18n : { 	
						"progressBarText":"Chronos initializing, please wait ...",
						"nextText" : "Next",
						"previousText" : "Previous",
						"createText" : "Create event",
						"configurationText" : "Configuration",
						"projectTasksText" : "Project Tasks",
						"tasksText" : "Tasks / Todos",
						"dateText" : "Date",
						"evInformations":"Informations",
						"evOptions":"Options",
						"evSubject":"Subject",
						"evDescription":"Description",
						"evStart":"From",
						"evEnd":"to",
						"evCalendar":"Calendar",
						"evAllDay":"All day",
						"evCategory":"Category",
						"evRRule":"Recurrence",
						"evRepeat":"Repeat every",
						"evRDay":"days",
						"evRDayOfWeek":"Day of week",
						"evRWeek":"weeks",
						"evRMonth":"months",
						"evRYear":"years",
						"evRPeriod":"Period",
						"evRend":"End",
						"evRthe":"the",
						"evRendNever":"Never",
						"evRendCount":"Count",
						"evRendDate":"Date",
						"evRweekDay":"Day of week",
						"evRmonthDay":"Day of month",
						"evRlastMonthDay":"Last day of month",
						"evRfirst":"First",
						"evRsecond":"Second",
						"evRthird":"Third",
						"evRfourth":"Fourth",
						"evRlast":"Last",
						"dateFormat":"dd/mm/yy",
						"createEventTitle":"Create an event",
						"editEventTitle":"Edit an event",
						"addCalendarTitle":"Add a calendar",
						"editCalendarTitle":"Modify the calendar",
						"viewCalendar":"Calendar view",
						"viewList":"List view",
						"calendarListTitle":"Calendars",
						"otherParamsTitle":"Other parameters",
						"showWeekend" : "Show weekends",
						"firstHour" : "First displayed hour",
						"colorscheme" : "Color mode",
						"colorModeActivity" : "Background color for activity, border for calendar",
						"colorModeCalendar" : "Background color for calendar, border for activity",
						"visibleCal": "Visible",
						"hiddenCal": "Hidden",
						"calUserEdit":"Agenda personnel",
						"ownerCalendars" : "My calendars",
						"groupCalendars" : "Group calendars",
						"otherCalendars" : "Other calendars",
						"addNewCalendar" : "Add a calendar",
						"addAccessRight" : "Add an access right",
						"accessRightTitle" : "Add access right",
						"readOnlyLabel" : "Read only",
						"readWriteLabel" : "Read / Write",
						"userAccessLabel" : "User",
						"suppressAccess" : "Suppress access",
						"teamAccessLabel" : "Team",
						"addLabel" : "Add",
						"searchLabel" : "Search",
						"firstnameLabel" : "firstname",
						"lastnameLabel" : "lastname",
						"emailLabel" : "Email",
						"notFoundLabel" : "User not found ...",
						"selectAllLabel" : "Select all",
						"selectNoneLabel" : "Select none",
						"allLabel" : "All",
						"exampleEvent" : "Event",
						"suppressCalendar" : "Suppress calendar",
						"exportICS" : "Export to iCalendar format (ICS)",
						"returnToMain" : "Return to calendar",
						"saveButton" : "Save",
						"calName" : "Name",
						"calColor" : "Text color",
						"calBGColor" : "Background color",
						"calColor" : "Text color",
						"calBGColor" : "Background color",
						"groupColor" : "Calendar text color",
						"groupBGColor" : "Calendar background color",
						"userSearchLabel" : "Search user",
						"userFromGroupLabel" : "Users from groups",
						"teamLabel" : "Teams",
						"detailText":"detail ...",
						"OKLabel":"OK",
						"applyLabel":"Apply",
						"suppressLabel":"Suppress",
						"supOccurence":"Suppress all occurences",
						"confirmSuppressUserCal":"Delete calendar with all events ?",
						"confirmDetachCal":"Detach from calendar ?",
						"closeLabel":"Close",
						"cancelLabel":"Cancel",
						noProjectTask : "No project tasks to display",
						infoProjectTask : "Project task detail",
						infoUserPlanning : "User planning",
						eventLabel : "Visible",
						roleLabel : "Role",
						responsableLabel : "R",
						attendeeLabel : "A",
						ptEventSet : "Show event",
						ptEventUnset : "Do not show event",
						projectLabel : "Project",
						groupLabel : "Group",
						taskLabel : "Task",
						progressLabel : "Progress (%)",
						workloadLabel : "Workload (h)",
						pstartLabel : "Planned begin",
						pendLabel : "Planned end",
						dstartLabel : "User start",
						dendLabel : "User end",
						editProjectTaskTitle:"Edit a project task",
						"errorProjectTaskIsAllday" : "Project task is only allday",
						"errorCannotModifyProjectTask" : "Cannot modify project task",
						"errorCannotModifyRecurEvent" : "Cannot modify recurrent event",
						"errorCalendarReadOnly" : "This calendar is read only",
						"errorNoCalendar" : "There is no calendar, create one",
						"errorNoCategories" : "There is no defined categories, ask administrator to create one",
						"errorNoCurrentCalendar" : "There is no current calendar selected",
						"categoryLegend" : "Category legend",
						"reattachWindow" : "Reattach the window"
					  }
	};

// ========================================================
// Effective plugin declaration with methods abstraction
// Usage :
//     <div id="chronos"></div>
//
//     $('#chronos').chronos(options);
// ========================================================
$.fn.chronos = function(options) {
	// method call, get existing Chronos Object and apply method
	// if method not returns result we return this for chainability
	if (typeof options === 'string') {
		var args = Array.prototype.slice.call(arguments, 1);
		this.each(function() {
			var chronos = $.data(this, 'chronos');
			if (chronos && $.isFunction(chronos[options])) {
				var res = chronos[options].apply(chronos, args);
				if (res !== undefined) {
					return res;
				}
			}
			else {
				$.error( 'Method ' +  options + ' does not exist on jQuery.chronos' );
			}
		});
	}
	else if (typeof options === 'object') {
		options =  $.extend(true, {}, defaults, options);

		this.each(function(i, item) {
			var $elem = $(item);
			var chronos = new Chronos(item, options);
			$(item).data('chronos', chronos);
		});
	}
	else {
		$.error( 'Chronos must not be there ... ');
	}
	return this;
};

