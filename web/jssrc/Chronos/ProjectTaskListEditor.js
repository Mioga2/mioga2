// =============================================================================
/**
This module display the project tasks list 

    var options = {
        i18n : { .... }
    };

@class ProjectTaskListEditor
@constructor
@param {Object} elem JQuery element parent of progress bar
@param {Object} options
**/
// =============================================================================
function ProjectTaskListEditor(elem, options) {
	this.elem     = elem;                  // parent DOM node
	this.i18n     = options.i18n;
	this.chronos  = options.chronos;
	this.PTL      = options.projectTaskList;

	this.current_task = undefined;
	this.current_elem = undefined;

	MIOGA.debug.ProjectTaskListEditor = 0;

	this.refresh = refresh;
	// Constructor, create dialog
	var that = this; // For callback reference to current object
	// main window
	this.$cont = $('<div class="prj-task-list-editor"></div>').appendTo(this.elem);

	// navigation bar
	var $nav = $('<div class="app-nav"></div>').appendTo(this.$cont);
	$('<button class="button cancel">'+this.i18n.returnToMain+'</button>')
	           .appendTo($nav)
			   .click(function(event) {
							history.back();
	           });

	// list container
	this.$no_task = $('<p>'+that.i18n.noProjectTask+'</p>').appendTo(this.$cont);
	this.$task_tbl = $('<table class="task-table list tablesorter" ></div>').appendTo(this.$cont);
	$('<thead>'
			+ '<tr>'
				+ '<th colspan="8">' + that.i18n.infoProjectTask + '</th>'
				+ '<th class="spacer"></th>'
				+ '<th colspan="3">' + that.i18n.infoUserPlanning + '</th>'
		 	+ '</tr>'
			+ '<tr>'
				+ '<th class="pt-group">' + that.i18n.groupLabel + '</th>'
				+ '<th class="pt-project">' + that.i18n.projectLabel + '</th>'
				+ '<th class="pt-label">' + that.i18n.taskLabel + '</th>'
				+ '<th class="pt-workload">' + that.i18n.workloadLabel + '</th>'
				+ '<th class="pt-current_progress">' + that.i18n.progressLabel + '</th>'
				+ '<th class="pt-pstart">' + that.i18n.pstartLabel + '</th>'
				+ '<th class="pt-pend">' + that.i18n.pendLabel + '</th>'
				+ '<th class="pt-role">' + that.i18n.roleLabel + '</th>'
				+ '<th class="spacer"></th>'
				+ '<th class="pt-dtart">' + that.i18n.dstartLabel + '</th>'
				+ '<th class="pt-dend">' + that.i18n.dendLabel + '</th>'
				+ '<th class="pt-event">' + that.i18n.eventLabel + '</th>'
		 	+ '</tr>'
		 + '</thead>'
		 + '<tbody></tbody>').appendTo(this.$task_tbl)

	this.$task_tbl.tablesorter({sortList: [[5,0]]});
	this.$task_tbl.on("click", "td.user-info", function(ev) {
		MIOGA.logDebug("ProjectTaskListEditor", 1, "click for task ");
		var $elem = $(ev.currentTarget).parent();
		var task = $elem.data('task');
		MIOGA.logDebug("ProjectTaskListEditor", 1, "task ", task);
		editTask.call(that, $elem, task);
	});
	this.$task_tbl.on("click", ".pt-link", function(ev) {
		ev.stopPropagation();
	});
	// Project task edit dialog
	this.$dialog = $('<div class="chronos-dialog"></div>').appendTo(elem);
	var idForm = MIOGA.generateID("chronos");
	var idOK = MIOGA.generateID("chronos");
	var idCancel = MIOGA.generateID("chronos");

	this.task_id = MIOGA.generateID("chronos");
	this.show_event_id = MIOGA.generateID("chronos");
	this.dstart_id = MIOGA.generateID("chronos");
	this.dend_id = MIOGA.generateID("chronos");

	this.$dialog.append('<form id="'+idForm+'" class="form">'
								+'<input type="hidden" name="rowid" value="" />'
								+'<fieldset>'
								+'<div id="'+this.task_id+'" class="form-item">'
								+'</div>'
								+'<div class="form-item">'
									+'<label>'+this.i18n.eventLabel+'</label><input id="'+this.show_event_id+'" type="checkbox" name="event" />'
								+'</div>'
								+'<div class="form-item">'
									+'<label>'+this.i18n.dstartLabel+'</label><input id="'+this.dstart_id+'" type="text" name="dstart" />'
								+'</div>'
								+'<div class="form-item">'
									+'<label>'+this.i18n.dendLabel+'</label><input id="'+this.dend_id+'" type="text" name="dend" />'
								+'</div>'
								+'</fieldset>'
				        		+'<p><button id="'+idOK+'" >'+this.i18n.OKLabel+'</button>'
								+'<button id="'+idCancel+'" >'+this.i18n.cancelLabel+'</button></p>'
								+'</form>');

	this.$dialog.dialog({ title: this.i18n.editProjectTaskTitle,
			                            autoOpen:false,
										modal:true,
										width:'40em',
										closeOnEscape:true
									});

	var dpicker_opts = {
		dateFormat : that.i18n.dateFormat,
		dayNames : options.dayNames,
		dayNamesMin :options.dayNamesMin,
		dayNamesShort :options.dayNamesShort,
		monthNames :options.monthNames,
		monthNamesShort :options.monthNamesShort
	};
	$('#'+this.dstart_id).datepicker(dpicker_opts);
	$('#'+this.dend_id).datepicker(dpicker_opts); 

	$('#'+idForm).submit(function(event) {
								return false;
							});
	$('#'+idOK).click(function(event) {
								validateTask.call(that);
								return false;
							});
	$('#'+idCancel).click(function(event) {
								that.$dialog.dialog('close');
								return false;
							});

	this.PTL.addRefreshCB(this.refresh, this);
	this.refresh();

	// end constructor ----------------------------------------
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	// --------------------------------------------------------
	// showWindow display and init window
	// --------------------------------------------------------
	this.showWindow = showWindow;
	function showWindow() {
		MIOGA.logDebug("ProjectTaskListEditor", 1, 'showWindow ===');
		this.$cont.show();
	}
	// --------------------------------------------------------
	// hideWindow hides the window
	// --------------------------------------------------------
	this.hideWindow = hideWindow;
	function hideWindow() {
		MIOGA.logDebug("ProjectTaskListEditor", 1, 'hideWindow ===');
		this.$cont.hide();
	}
	// -------------------------------------------------------------
	// refresh function
	// redraw list content function of calendarList
	// -------------------------------------------------------------
	function refresh() {
		MIOGA.logDebug("ProjectTaskListEditor", 1, ' ============= refresh');
		var that = this;
		var $tbody = this.$task_tbl.find('tbody');
		$tbody.children().remove();


		MIOGA.logDebug("ProjectTaskListEditor", 2, 'PTL.tasks = ',that.PTL.tasks);
		if (that.PTL.tasks.length === 0) {
			this.$task_tbl.hide();
			this.$no_task.show();
		} else {
			this.$no_task.hide();
			this.$task_tbl.show();
			$.each(that.PTL.tasks, function (num, item) {
				MIOGA.logDebug("ProjectTaskListEditor", 3, 'num = '+num+' itel = ',item);
				var $elem = $('<tr></tr>').appendTo($tbody)
					.data("task", item);
					/*
					.click(function(event) {
						MIOGA.logDebug("ProjectTaskListEditor", 1, "click for task " +item.label);
						editTask.call(that, this, item);
					});
					*/
				drawLine.call(that, $elem, item);
			});
			this.$task_tbl.trigger("update");
		}
		MIOGA.logDebug("ProjectTaskListEditor", 1, ' ============= refresh end');
	}
	// ============================================================================
	// Private methods
	// ============================================================================
	// -------------------------------------------------------------
	// drawLine
	// redraw a line of task representation
	// -------------------------------------------------------------
	function drawLine($elem, task) {
		MIOGA.logDebug("ProjectTaskListEditor", 2, 'drawLine ' + task.label);
		var that = this;
		var haussmann_href = mioga_context.bin_uri+'/'+task.group_ident+'/Haussmann/DisplayMain';
		var span_event;
		var user_classes = "user-info";
		var role_label;
		if (task.show_event) {
			span_event = '<span class="pt-event-set">'+this.i18n.ptEventSet+'</span>';
		} else {
			span_event = '<span class="pt-event-unset">'+this.i18n.ptEventUnset+'</span>';
		}
		if (task.type === 'r') {
			user_classes += " responsable";
			role_label = this.i18n.responsableLabel;
		}
		else {
			user_classes += " attendee";
			role_label = this.i18n.attendeeLabel;
		}

		$('<td class="project-info pt-group" >'+task.group_ident+'</td>'
			+'<td class="project-info pt-project" ><a href ="'+haussmann_href+'#show-project-'+task.project_id+'">'+task.project_label+'</a></td>'
			+'<td class="project-info pt-label" ><a href="'+haussmann_href+'#edit-task-'+task.rowid+'" >'+task.label+'</a></td>'
			+'<td class="project-info pt-workload" >'+task.workload+'</td>'
			+'<td class="project-info pt-progress" >'+task.current_progress+'</td>'
			+'<td class="project-info pt-pstart" >'+formatDate.call(that, task.pstart)+'</td>'
			+'<td class="project-info pt-pend" >'+formatDate.call(that, task.pend)+'</td>'
			+'<td class="project-info pt-role" >'+role_label+'</td>'
			+'<td class="spacer"></th>'
			+'<td class="'+user_classes+' pt-dstart" >'+formatDate.call(that, task.dstart)+'</td>'
			+'<td class="'+user_classes+' pt-dend" >'+formatDate.call(that, task.dend)+'</td>'
			+'<td class="'+user_classes+' pt-event" >'+span_event+'</td>').appendTo($elem);
	}
	// -------------------------------------------------------------
	// editTask
	// Initialize dialog to edit task
	// -------------------------------------------------------------
	function editTask(elem, task) {
		MIOGA.logDebug("ProjectTaskListEditor", 1, "edit task ",task);
		MIOGA.logDebug("ProjectTaskListEditor", 1, "arguments " ,arguments);
		MIOGA.logDebug("ProjectTaskListEditor", 1, "task.label " + task.label);
		//$(elem).children().remove();
		$('#'+this.task_id).html('<p>'+task.label+'</p>');
		if (task.show_event) {
			$('#'+this.show_event_id).prop('checked', 1);
		} else {
			$('#'+this.show_event_id).prop('checked', 0);
		}
		$("#"+this.dstart_id).removeClass( "error");
		$("#"+this.dend_id).removeClass( "error");

		$('#'+this.dstart_id).datepicker('setDate', task.dstart);
		$('#'+this.dend_id).datepicker('setDate', task.dend);
		if (task.pstart) {
			$('#'+this.dstart_id).datepicker('option', "minDate", task.pstart);
			$('#'+this.dend_id).datepicker('option', "minDate", task.pstart);
		}
		if (task.pend) {
			$('#'+this.dstart_id).datepicker('option', "maxDate", task.pend);
			$('#'+this.dend_id).datepicker('option', "maxDate", task.pend);
		}
		if (task.type === 'r') {
			$('#'+this.dstart_id).prop("disabled", false);
			$('#'+this.dend_id).prop("disabled", false);
		}
		else {
			$('#'+this.dstart_id).prop("disabled", true);
			$('#'+this.dend_id).prop("disabled", true);
		}

		this.current_elem = elem;
		this.current_task = task;
		this.$dialog.dialog('open');
	}
	// -------------------------------------------------------------
	// validateTask
	// Set new parameters for task
	// -------------------------------------------------------------
	function validateTask() {
		MIOGA.logDebug("ProjectTaskListEditor", 1, "validate task " +this.current_task.label);
		var fl_error = false;
		var show_event;
		if ($('#'+this.show_event_id).is(':checked')) {
			show_event = true;
		} else {
			show_event = false;
		}
		MIOGA.logDebug("ProjectTaskListEditor", 1, "show_event = " +show_event);
		var start = $("#"+this.dstart_id).datepicker( "getDate");
		var end = $("#"+this.dend_id).datepicker( "getDate");
		MIOGA.logDebug("ProjectTaskListEditor", 1, "start = " + start + " end = " + end);
		if (start === null) {
			end = null;
		}
		else if (end === null) {
			fl_error = true;
			$("#"+this.dend_id).addClass( "error");
		}
		else  {
			if (start > end) {
				$("#"+this.dstart_id).addClass( "error");
				$("#"+this.dend_id).addClass( "error");
				fl_error = true;
			}
		}
		// Push values if no errors
		if (!fl_error) {
			$("#"+this.dstart_id).removeClass( "error");
			$("#"+this.dend_id).removeClass( "error");
			this.current_task.setValues( { show_event : show_event, dstart : start, dend : end }, that.current_elem,
										function(elem, task) {
											MIOGA.logDebug("ProjectTaskListEditor", 1, "setValues cb call");
											$(elem).children().remove();
											drawLine.call(that, elem, task);
										});
			this.$dialog.dialog('close');
			this.current_elem = undefined;
			this.current_task = undefined;
		}
	}
	// -------------------------------------------------------------
	// formatDate
	// -------------------------------------------------------------
	function formatDate(date) {
		MIOGA.logDebug("ProjectTaskListEditor", 1, "formatDate " +date);
		var sdate = " ";
		if (date) {
			//sdate = date.toISOString().substring(0, 10);
			sdate = date.formatMioga(false, false);
		}

		return sdate;
	}
} // End of ProjectTaskListEditor

