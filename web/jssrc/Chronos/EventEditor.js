// =============================================================================
/**
This module display the edit event dialog

    var options = {
        i18n : { .... }
    };

@class EventEditor
@constructor
@param {Object} elem JQuery element parent of progress bar
@param {Object} options
**/
// =============================================================================

// ========================================================================================================
// EventEditor
// ========================================================================================================
function EventEditor(elem, options) {
	"use strict";

	this.elem     = elem;                  // parent DOM node
	this.i18n     = options.i18n;
	this.config     = options.config;
	this.chronos     = options.chronos;
	this.categories = options.categories;
	this.dayNames = options.dayNames;
	this.monthNames = options.monthNames;
	this.dayNamesMin = options.dayNamesMin;
	this.CL       = options.calendarList;

	this.editEvent = undefined;    // CalendarEvent object to edit
	this.mode = "";    // create or edit function of initialisation

	MIOGA.debug.EventEditor = 0;

	// Constructor, create dialog
	var that = this; // For callback reference to current object
	// main window
	this.$cont = $('<div class="event-editor"></div>').appendTo(this.elem);

	// navigation bar
	var $nav = $('<div class="app-nav"></div>').appendTo(this.$cont);
	this.$btn_ok = $('<button class="button" >'+this.i18n.OKLabel+'</button>').appendTo($nav);
	this.$btn_ok.click(function(event) {
							MIOGA.logDebug("EventEditor", 1, "EventEditor OK button");
							if (that.validateForm()) {
								MIOGA.logDebug("EventEditor", 1, "return");
								if (this.cb instanceof Function) {
									this.cb();
								}
								history.back();
							}
						});
	this.$btn_cancel = $('<button class="button cancel" >'+this.i18n.cancelLabel+'</button>').appendTo($nav);
	this.$btn_cancel.click(function(event) {
							history.back();
						});

	this.$btn_suppress = $('<button class="button" >'+this.i18n.suppressLabel+'</button>').appendTo($nav);
	this.$btn_suppress.click(function(event) {
							MIOGA.logDebug("EventEditor", 1, "EventEditor Suppress button");
							if (that.suppressEvent()) {
								MIOGA.logDebug("EventEditor", 1, "return");
								history.back();
							}
						});

	this.$btn_supOccurence = $('<button class="button" >'+this.i18n.supOccurence+'</button>').appendTo($nav);
	this.$btn_supOccurence.click(function(event) {
							MIOGA.logDebug("EventEditor", 1, "EventEditor Suppress occurence button");
							if (that.suppressEvent()) {
								MIOGA.logDebug("EventEditor", 1, "return");
								history.back();
							}
						});


	this.subject_id   = MIOGA.generateID("chronos");
	this.desc_id      = MIOGA.generateID("chronos");
	this.startdate_id = MIOGA.generateID("chronos");
	this.starthour_id = MIOGA.generateID("chronos");
	this.enddate_id   = MIOGA.generateID("chronos");
	this.endhour_id   = MIOGA.generateID("chronos");
	this.allday_id    = MIOGA.generateID("chronos");
	this.category_id  = MIOGA.generateID("chronos");
	this.categoryColor_id  = MIOGA.generateID("chronos");
	this.callist_id   = MIOGA.generateID("chronos");
	this.rrulechk_id  = MIOGA.generateID("chronos");

	var $form = $('<form class="form"></form>').appendTo(this.$cont);

	$form.append( '<fieldset><legend>'+this.i18n.evInformations+'</legend>'
						+ '<div class="form-item"><label for="'+this.subject_id+'">'+this.i18n.evSubject+'</label><input id="'+this.subject_id+'" type="text" name="subject" /></div>'
						+ '<div class="form-item"><label for="'+this.desc_id+'">'+this.i18n.evDescription+'</label><textarea id="'+this.desc_id+'" name="desc" /></textarea></div>'
						+ '<div class="form-item"><label>'+this.i18n.evStart+'</label><input id="'+this.startdate_id+'" type="text" name="startdate" />'
							+'<input id="'+this.starthour_id+'" type="text" name="starthour" /></div>'
						+ '<div class="form-item"><label>'+this.i18n.evEnd+'</label><input id="'+this.enddate_id+'" type="text" name="enddate" />'
							+'<input id="'+this.endhour_id+'" type="text" name="endhour" /></div>'
						+ '<div class="form-item"><label for="'+this.allday_id+'">'+this.i18n.evAllDay+'</label><input id="'+this.allday_id+'" type="checkbox" name="allday" /></div>'
						+ '<div class="form-item"><label for="'+this.category_id+'">'+this.i18n.evCategory+'</label><select id="'+this.category_id+'" name="category"></select><span id="'+this.categoryColor_id+'" class="category-color"></span></div>'
						+ '<div class="form-item"><label for="'+this.rrulechk_id+'">'+this.i18n.evRRule+'</label><input id="'+this.rrulechk_id+'" type="checkbox" name="rrule" /></div>'
				 +'</fieldset>');

	// Options fieldset
	var $f2 = $('<fieldset><legend>'+this.i18n.evOptions+'</legend></fieldset>').appendTo($form);
	var $fitem = $('<div class="form-item"><label>'+this.i18n.evCalendar+'</label></div>').appendTo($f2);

	this.$callist = $('<select id="'+this.callist_id+'" name="calendar"></select>').appendTo($fitem);

	// Initialize date and hour objects
	$('#'+this.startdate_id).datepicker({"dateFormat":this.i18n.dateFormat,
											dayNames : options.dayNames,
											dayNamesMin :options.dayNamesMin,
											dayNamesShort :options.dayNamesShort,
											monthNames :options.monthNames,
											monthNamesShort :options.monthNamesShort,
											"onSelect" : function(dateText, inst) {
														 	that.checkDatesAndHours('startdate');
											             }
	                                    });
	$('#'+this.starthour_id).timepicker({
											hourText : that.i18n.hourText,
											minuteText : that.i18n.minuteText,
											"onSelect" : function() {
														 	that.checkDatesAndHours('starthour');
											             }
	                                  });
	$('#'+this.enddate_id).datepicker({"dateFormat":this.i18n.dateFormat,
											dayNames : options.dayNames,
											dayNamesMin :options.dayNamesMin,
											dayNamesShort :options.dayNamesShort,
											monthNames :options.monthNames,
											monthNamesShort :options.monthNamesShort,
											"onSelect" : function(dateText, inst) {
														 	that.checkDatesAndHours('enddate');
											             }
	                                 });
	$('#'+this.endhour_id).timepicker({
											hourText : that.i18n.hourText,
											minuteText : that.i18n.minuteText,
											"onSelect" : function() {
														 	that.checkDatesAndHours('endhour');
											             }
	                                  });

	$('#'+this.allday_id).click(function(event) {
									MIOGA.logDebug("EventEditor", 1, "allday clicked");
									if ($('#'+that.allday_id).is(':checked')) {
										$('#'+that.starthour_id).hide();
										$('#'+that.endhour_id).hide();
									}
									else {
										$('#'+that.starthour_id).show();
										$('#'+that.endhour_id).show();
									}
								 	that.checkDatesAndHours('allday');
								});

	// Initialize category list
	$.each(this.categories, function(num, item) {
		$('<option value="'+item.category_id+'" >'+item.name+'</option>').appendTo('#'+that.category_id);
	});
	$('#'+this.category_id).change(function (event) {
		MIOGA.logDebug("EventEditor", 1, 'category change');
		var val = $(this).val();
		MIOGA.logDebug("EventEditor", 1, 'val = ', val);
		that.setCategoryColor(val);
	});
	// -----------------------------------------------------------------
	// Recurence fieldset
	// -----------------------------------------------------------------
	this.period_id         = MIOGA.generateID("chronos");
	this.repeat_id         = MIOGA.generateID("chronos");
	this.rendchoice_id     = MIOGA.generateID("chronos");
	this.rendcount_id      = MIOGA.generateID("chronos");
	this.renddate_id       = MIOGA.generateID("chronos");
	this.rweekopt_id       = MIOGA.generateID("chronos");
	this.rmonday_id        = MIOGA.generateID("chronos");
	this.rtuesday_id       = MIOGA.generateID("chronos");
	this.rwednesday_id     = MIOGA.generateID("chronos");
	this.rthursday_id      = MIOGA.generateID("chronos");
	this.rfriday_id        = MIOGA.generateID("chronos");
	this.rsaturday_id      = MIOGA.generateID("chronos");
	this.rsunday_id        = MIOGA.generateID("chronos");
	this.rmonthopt_id      = MIOGA.generateID("chronos");
	this.rfixmonthday_id   = MIOGA.generateID("chronos");
	this.rmonthday_id      = MIOGA.generateID("chronos");
	this.rmonthdaypos_id   = MIOGA.generateID("chronos");
	this.ryearopt_id       = MIOGA.generateID("chronos");
	this.rfixyearday_id    = MIOGA.generateID("chronos");
	this.rfixyearmonth_id  = MIOGA.generateID("chronos");

	var rruleHTML = '<fieldset><legend>'+this.i18n.evRRule+'</legend>'
						+ '<div class="form-item"><label for="'+this.repeat_id+'">'+this.i18n.evRepeat+'</label>'
							+ '<input id="'+this.period_id+'" class="number3" type="text" name="rperiod" />'
							+ '&nbsp;<select id="'+this.repeat_id+'" name="repeat" >'
								+ '<option value="daily">'+this.i18n.evRDay+'</option>'
								+ '<option value="weekly">'+this.i18n.evRWeek+'</option>'
								+ '<option value="monthly">'+this.i18n.evRMonth+'</option>'
								+ '<option value="yearly">'+this.i18n.evRYear+'</option>'
							+ '</select>'
						+ '</div>'
						+ '<div id="'+this.rweekopt_id+'" class="form-item"><label>'+this.i18n.evRweekDay+'</label>'
							+ '<div class="input-group" >'
								+ '<input id="'+this.rmonday_id+'" type="checkbox" name="rmonday" value="1">'+this.dayNamesMin[1]
								+ '<input id="'+this.rtuesday_id+'" type="checkbox" name="rtuesday" value="2">'+this.dayNamesMin[2]
								+ '<input id="'+this.rwednesday_id+'" type="checkbox" name="rwednesday" value="3">'+this.dayNamesMin[3]
								+ '<input id="'+this.rthursday_id+'" type="checkbox" name="rthursday" value="4">'+this.dayNamesMin[4]
								+ '<input id="'+this.rfriday_id+'" type="checkbox" name="rfriday" value="5">'+this.dayNamesMin[5]
								+ '<input id="'+this.rsaturday_id+'" type="checkbox" name="rsaturday" value="6">'+this.dayNamesMin[6]
								+ '<input id="'+this.rsunday_id+'" type="checkbox" name="rsunday" value="0">'+this.dayNamesMin[0]
							+ '</div>'
						+ '</div>'
						+ '<div id="'+this.rmonthopt_id+'" class="form-item"><label for="'+this.rmonthday_id+'">'+this.i18n.evRmonthDay+'</label>'
							+ '<div class="input-group" >'
								+ '<div><input type="radio" name="rmonthmode" value="1"/>'
									+ this.i18n.evRthe + '&nbsp;'
									+ '<input type="text class="number3" class="number3" name="rfixmonthday" id="'+this.rfixmonthday_id+'" />'
								+ '</div>'
								+ '<div><input type="radio" name="rmonthmode" value="2"/>'
									+ '<select id="'+this.rmonthdaypos_id+'" name="rmonthdaypos" >'
										+ '<option value="1">'+this.i18n.evRfirst+'</option>'
										+ '<option value="2">'+this.i18n.evRsecond+'</option>'
										+ '<option value="3">'+this.i18n.evRthird+'</option>'
										+ '<option value="4">'+this.i18n.evRfourth+'</option>'
										+ '<option value="5">'+this.i18n.evRlast+'</option>'
									+ '</select>'
									+ '&nbsp;'
									+ '<select id="'+this.rmonthday_id+'" name="rmonthday" >'
										+ '<option value="0">'+this.dayNames[0]+'</option>'
										+ '<option value="1">'+this.dayNames[1]+'</option>'
										+ '<option value="2">'+this.dayNames[2]+'</option>'
										+ '<option value="3">'+this.dayNames[3]+'</option>'
										+ '<option value="4">'+this.dayNames[4]+'</option>'
										+ '<option value="5">'+this.dayNames[5]+'</option>'
										+ '<option value="6">'+this.dayNames[6]+'</option>'
									+ '</select>'
								+ '</div>'
								+ '<div><input type="radio" name="rmonthmode" value="3"/>' + this.i18n.evRlastMonthDay
								+ '</div>'
							+ '</div>'
						+ '</div>'
						+ '<div id="'+this.ryearopt_id+'" class="form-item"><label>'+this.i18n.evRthe+'</label>'
							+ '<div class="input-group" >'
								+ '<div>'
									+ '<input type="text" class="number3" name="rfixyearday" id="'+this.rfixyearday_id+'" />'
									+ '&nbsp;'
									+ '<select id="'+this.rfixyearmonth_id+'" name="rfixyearmonth" >'
										+ '<option value="0">'+this.monthNames[0]+'</option>'
										+ '<option value="1">'+this.monthNames[1]+'</option>'
										+ '<option value="2">'+this.monthNames[2]+'</option>'
										+ '<option value="3">'+this.monthNames[3]+'</option>'
										+ '<option value="4">'+this.monthNames[4]+'</option>'
										+ '<option value="5">'+this.monthNames[5]+'</option>'
										+ '<option value="6">'+this.monthNames[6]+'</option>'
										+ '<option value="7">'+this.monthNames[7]+'</option>'
										+ '<option value="8">'+this.monthNames[8]+'</option>'
										+ '<option value="9">'+this.monthNames[9]+'</option>'
										+ '<option value="10">'+this.monthNames[10]+'</option>'
										+ '<option value="11">'+this.monthNames[11]+'</option>'
									+ '</select>'
								+ '</div>'
							+ '</div>'
						+ '</div>'
						+ '<div class="form-item"><label for="'+this.rendchoice_id+'">'+this.i18n.evRend+'</label>'
							+ '<div class="input-group" >'
								+ '<div><input type="radio" name="rendchoice" value="1"/>'+this.i18n.evRendNever+'</div>'
								+ '<div><input type="radio" name="rendchoice" value="2"/>'+this.i18n.evRendCount+'&nbsp;<input class="number3" type="text" id="'+this.rendcount_id+'" name="rendcount" /></div>'
								+ '<div><input type="radio" name="rendchoice" value="3"/>'+this.i18n.evRendDate+'&nbsp;<input type="text" id="'+this.renddate_id+'" name="renddate" /></div>'
							+ '</div>'
						+ '</div>'
				 +'</fieldset>';
	this.$rrule = $(rruleHTML).appendTo($form);

	// Recurrence rules
	$('#'+this.rrulechk_id).click(function(event) {
									MIOGA.logDebug("EventEditor", 1, "rrulechk clicked");
									if ($('#'+that.rrulechk_id).is(':checked')) {
									MIOGA.logDebug("EventEditor", 1, "show");
										that.$rrule.show();
									}
									else {
									MIOGA.logDebug("EventEditor", 1, "hide");
										that.$rrule.hide();
									}
								});
	// Repeat choice
	$('#'+this.repeat_id).change(function(event) {
									MIOGA.logDebug("EventEditor", 1, "repeat change ");
									MIOGA.logDebug("EventEditor", 1, 'value = '+ $('#'+that.repeat_id).val());
									that.showRepeatOptions($('#'+that.repeat_id).val());
								});
	// end choice
	$("input[name='rendchoice']").change(function() {
										MIOGA.logDebug("EventEditor", 1, "end choice change");
										MIOGA.logDebug("EventEditor", 1, 'value = '+ $("input[name='rendchoice']:checked").val());
										that.setRRuleEndChoice($("input[name='rendchoice']:checked").val());
									});
	// end date
	$('#'+this.renddate_id).datepicker({"dateFormat":this.i18n.dateFormat,
											dayNames : options.dayNames,
											dayNamesMin :options.dayNamesMin,
											dayNamesShort :options.dayNamesShort,
											monthNames :options.monthNames,
											monthNamesShort :options.monthNamesShort,
											"onSelect" : function(dateText, inst) {
														 	MIOGA.logDebug("EventEditor", 1, "change rrule end date");
											             }
	                                    });



	// end constructor
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	// --------------------------------------------------------
	// showWindow display and init window
	// --------------------------------------------------------
	this.showWindow = showWindow;
	function showWindow() {
		MIOGA.logDebug("EventEditor", 1, 'EventEditor  showWindow ===');
		this.$cont.show();
	}
	// --------------------------------------------------------
	// hideWindow hides the window
	// --------------------------------------------------------
	this.hideWindow = hideWindow;
	function hideWindow() {
		MIOGA.logDebug("EventEditor", 1, 'EventEditor  hideWindow ===');
		this.$cont.hide();
	}

	// ----------------------------------------------------------------
	// create a new event
	// ----------------------------------------------------------------
	this.create = create;
	function create(calEvent, cb) {
		MIOGA.logDebug("EventEditor", 1, "EventEditor.create ==========================");
		this.editEvent = calEvent;
		this.mode = "create";
		this.init(cb);
	}
	// ----------------------------------------------------------------
	// edit an existing event
	// ----------------------------------------------------------------
	this.edit = edit;
	function edit(calEvent, cb) {
		MIOGA.logDebug("EventEditor", 1, "EventEditor.edit ==========================");
		this.editEvent = calEvent;
		this.mode = "edit";
		this.init(cb);
	}
	// ----------------------------------------------------------------
	// init function prepare form elements
	// ----------------------------------------------------------------
	this.init = init;
	function init(cb) {
		MIOGA.logDebug("EventEditor", 1, "EventEditor.init ==========================");
		var that = this;
		// suppress button and calendar list function of mode
		this.$callist.find('option').remove();
		this.$cont.find().removeClass('error');
		var html = "";
		if (cb instanceof Function) {
			this.cb = cb;
		} else {
			this.cb = null;
		}
		if (this.mode === "edit") {
			if (this.editEvent.fl_rrule) {
				this.$btn_supOccurence.show();
				this.$btn_suppress.hide();
			} else {
				this.$btn_supOccurence.hide();
				this.$btn_suppress.show();
			}
			$.each(this.CL.calendars, function(num, item) {
				var selected;
				if (item === that.editEvent.calendar) {
					selected = 'selected="selected"';
				}
				else {
					selected = "";
				}
				$('<option value="'+num+'" '+selected+'>'+item.ident+'</option>').appendTo(that.$callist);
			});
		} else {
			this.$btn_suppress.hide();
			this.$btn_supOccurence.hide();
			$.each(this.CL.calendars, function(num, item) {
				var selected;
				if (that.CL.currentCal && (item.rowid === that.CL.currentCal.rowid)) {
					selected = 'selected="selected"';
				}
				else {
					selected = "";
				}
				$('<option value="'+num+'" '+selected+'>'+item.ident+'</option>').appendTo(that.$callist);
			});
		}
		// Calendar list
		this.$callist.show();
		MIOGA.logDebug("EventEditor", 1, "editEvent", this.editEvent);
		// subject
		$('#'+this.subject_id).val(this.editEvent.title);
		// description
		$('#'+this.desc_id).val(this.editEvent.desc);
		//
		// init en date if needed
		if (this.editEvent.end == null) {   // allDay = true and one day event
			MIOGA.logDebug("EventEditor", 1, "end is null");
			this.editEvent.end = new Date(this.editEvent.start);
		}
		// Set start and end dates
		// if recurrent event, set the date with the first date of sequence
		var start = this.editEvent.start;
		var end = this.editEvent.end;
		if (this.editEvent.fl_rrule) {
			MIOGA.logDebug("EventEditor", 1, "base_start" + this.editEvent.rrule.base_start);
			MIOGA.logDebug("EventEditor", 1, "base_end" + this.editEvent.rrule.base_end);
			start = this.editEvent.rrule.base_start;
			end = this.editEvent.rrule.base_end;
		}
		// start date
		$('#'+this.startdate_id).datepicker('setDate', start);
		$('#'+this.starthour_id).timepicker('setTime', $.fullCalendar.formatDate(start, "HH:mm"));
		// end date
		$('#'+this.enddate_id).datepicker('setDate', end);
		$('#'+this.endhour_id).timepicker('setTime', $.fullCalendar.formatDate(end, "HH:mm"));
		//allDay
		if (this.editEvent.allDay) {
			$('#'+this.allday_id).prop('checked', 1);
			$('#'+this.starthour_id).hide();
			$('#'+this.endhour_id).hide();
		}
		else {
			$('#'+this.allday_id).prop('checked', 0);
			$('#'+this.starthour_id).show();
			$('#'+this.endhour_id).show();
		}
		// category
		$('#'+this.category_id).val(this.editEvent.category_id);
		this.setCategoryColor(this.editEvent.category_id);
		// recurrence fieldset animation
		this.setRecurrenceOptions();
		if (this.editEvent.fl_rrule) {
			$('#'+this.rrulechk_id).prop('checked', 1);
			this.$rrule.show();
		}
		else {
			$('#'+this.rrulechk_id).prop('checked', 0);
			this.$rrule.hide();
		}

		this.checkDatesAndHours('init');
	}
	// ----------------------------------------------------------------
	// setRecurrenceOptions
	// ----------------------------------------------------------------
	this.setRecurrenceOptions = setRecurrenceOptions;
	function setRecurrenceOptions() {
		MIOGA.logDebug("EventEditor", 1, "EventEditor.setRecurrenceOptions ==========================");
		$('#'+this.period_id).prop('value', this.editEvent.rrule.period)
		$('#'+this.repeat_id).val(this.editEvent.rrule.repeat);
		// week repeat mode
//		==============================================================================================================================================
//		
//		lu - ma - me - je - ve - sa - di
//		
//		Le jour de la semaine indiqué par l'indice dans le tableau correspond au jour de l'événement exprimé en UTC.
//		Il faut transcrire ce temps en local pour savoir quel jour de la semaine activer.
//		Par exemple, si mardi est enregistré mais que l'événement, traduit en temps local est mercredi, alors la checkbox "mercredi" doit être cochée.
//		==============================================================================================================================================
//		
//		mo - tu - we - th - fr - sa - su
//		
//		The day of the week showed by array index that is event day in UTC.
//		It must translate this time in local to know what is the week day to activate.
//		For example, if tuesday is actived and the event in local time is wednesday, so the checkbox "wednesday" must be checked.
//		==============================================================================================================================================

		var offsetDay = this.editEvent.start.getDate() - this.editEvent.start.getUTCDate();
		$('#'+this.rmonday_id).prop('checked', this.editEvent.rrule.weekdays[(1-offsetDay)]);
		$('#'+this.rtuesday_id).prop('checked', this.editEvent.rrule.weekdays[(2-offsetDay)]);
		$('#'+this.rwednesday_id).prop('checked', this.editEvent.rrule.weekdays[(3-offsetDay)]);
		$('#'+this.rthursday_id).prop('checked', this.editEvent.rrule.weekdays[(4-offsetDay)]);
		$('#'+this.rfriday_id).prop('checked', this.editEvent.rrule.weekdays[(5-offsetDay)]);
		if ((6-offsetDay) > 6) {
			$('#'+this.rsaturday_id).prop('checked', this.editEvent.rrule.weekdays[0]);
		}
		else {
			$('#'+this.rsaturday_id).prop('checked', this.editEvent.rrule.weekdays[6]);
		}
		if ((0-offsetDay) < 0) {
			$('#'+this.rsunday_id).prop('checked', this.editEvent.rrule.weekdays[6]);
		}
		else {
			$('#'+this.rsunday_id).prop('checked', this.editEvent.rrule.weekdays[0]);
		}
		// month repeat mode
		var $radios = $('input[name="rmonthmode"]');
		$radios.prop('checked', false);  // set all false
		$radios.filter('[value='+this.editEvent.rrule.monthmode+']').prop('checked', true);
		$('#'+this.rfixmonthday_id).prop('value', this.editEvent.rrule.fixmonthday)
		$('#'+this.rmonthdaypos_id).val(this.editEvent.rrule.monthdaypos);
		$('#'+this.rmonthday_id).val(this.editEvent.rrule.monthday);
		// year repat mode
		$('#'+this.rfixyearday_id).prop('value', this.editEvent.rrule.fixyearday)
		$('#'+this.rfixyearmonth_id).val(this.editEvent.rrule.fixyearmonth);
		// end choice
		this.setRRuleEndChoice(this.editEvent.rrule.endChoice);
		$('#'+this.rendcount_id).prop('value', this.editEvent.rrule.endCount);
		var renddate_obj = new Date (that.editEvent.rrule.endDate);
		$('#'+this.renddate_id).datepicker('setDate', renddate_obj);
		this.showRepeatOptions(this.editEvent.rrule.repeat);
	}
	// ----------------------------------------------------------------
	// setCategoryColor
	// ----------------------------------------------------------------
	this.setCategoryColor = setCategoryColor;
	function setCategoryColor(cat_id) {
		MIOGA.logDebug("EventEditor", 1, "EventEditor.setCategoryColor cat_id = " + cat_id);
		var category;
		if (cat_id) {
			category = this.chronos.getCategoryByID(cat_id);
		}
		else {
			if (this.categories.length > 0) {
				category = this.categories[0];
			}
			else {
				category = {fgcolor: '#000000', bgcolor : '#ffffff' };
			}
		}

		$('#' + this.categoryColor_id).css("color", category.fgcolor);
		$('#' + this.categoryColor_id).css("background-color", category.bgcolor);
	}
	// ----------------------------------------------------------------
	// showRepeatOptions
	// ----------------------------------------------------------------
	this.showRepeatOptions = showRepeatOptions;
	function showRepeatOptions(val) {
		MIOGA.logDebug("EventEditor", 1, "EventEditor.showRepeatOptions val = "+ val);
		MIOGA.logDebug("EventEditor", 1, "rweekopt_id = " + this.rweekopt_id);
		MIOGA.logDebug("EventEditor", 1, "rmonthopt_id = " + this.rmonthopt_id);

		// Day
		if (val == 'daily') {
			MIOGA.logDebug("EventEditor", 1, "option day");
			$('#'+this.rweekopt_id).hide();
			$('#'+this.rmonthopt_id).hide();
			$('#'+this.ryearopt_id).hide();
		}
		// week
		else if (val == 'weekly') {
			MIOGA.logDebug("EventEditor", 1, "option week");
			$('#'+this.rweekopt_id).show();
			$('#'+this.rmonthopt_id).hide();
			$('#'+this.ryearopt_id).hide();
		}
		// month
		else if (val == 'monthly') {
			MIOGA.logDebug("EventEditor", 1, "option month");
			$('#'+this.rweekopt_id).hide();
			$('#'+this.rmonthopt_id).show();
			$('#'+this.ryearopt_id).hide();
		}
		// Year
		else {
			MIOGA.logDebug("EventEditor", 1, "option year");
			$('#'+this.rweekopt_id).hide();
			$('#'+this.rmonthopt_id).hide();
			$('#'+this.ryearopt_id).show();
		}
	}
	// ----------------------------------------------------------------
	// setRRuleEndChoice
	// ----------------------------------------------------------------
	this.setRRuleEndChoice = setRRuleEndChoice;
	function setRRuleEndChoice(val) {
		MIOGA.logDebug("EventEditor", 1, "EventEditor.setRRuleEndChoice val = "+ val);
		var $radios = $('input[name="rendchoice"]');
		$radios.prop('checked', false);  // set all false
		$radios.filter('[value='+val+']').prop('checked', true);

		// Count
		if (val == 2) {
			MIOGA.logDebug("EventEditor", 1, "option count");
			$('#'+this.rendcount_id).prop('disabled', false)
			$('#'+this.renddate_id).prop('disabled', true)
		}
		// Date
		else if (val == 3) {
			MIOGA.logDebug("EventEditor", 1, "option date");
			$('#'+this.rendcount_id).prop('disabled', true)
			$('#'+this.renddate_id).prop('disabled', false)
		}
		// Never
		else {
			MIOGA.logDebug("EventEditor", 1, "option never");
			$('#'+this.rendcount_id).prop('disabled', true)
			$('#'+this.renddate_id).prop('disabled', true)
		}
	}
	// ----------------------------------------------------------------
	// checkDatesAndHours
	// ----------------------------------------------------------------
	this.checkDatesAndHours = checkDatesAndHours;
	function checkDatesAndHours(origin) {
		MIOGA.logDebug("EventEditor", 1, "EventEditor.checkDatesAndHours ==========================");
		var error = true;
		var start = $("#"+this.startdate_id).datepicker( "getDate");
		var end = $("#"+this.enddate_id).datepicker( "getDate");
		var fl_allday = $('#'+this.allday_id).is(':checked');
		MIOGA.logDebug("EventEditor", 1, "origin = " + origin + " start = " + start + "   end = " + end + " fl_allday = " + fl_allday);
		// call from startdate change
		if (origin === 'startdate') {
			// if allday the keep at less one day between start and end
			if ( (! fl_allday) || (start > end) ) {
				MIOGA.logDebug("EventEditor", 1, "not all day or start > end");
				$('#'+this.enddate_id).datepicker('setDate', start);
				end = start;
			}
		}
		if (origin === 'enddate') {
			if (start > end) {
				$('#'+this.startdate_id).datepicker('setDate', end);
				start = end;
			}
		}
		// checks for errors
		if (start > end) {
			$("#"+this.startdate_id).addClass( "error");
			$("#"+this.enddate_id).addClass( "error");
			error = false;
		}
		else {
			MIOGA.logDebug("EventEditor", 1, "remove class error");
			$("#"+this.startdate_id).removeClass( "error");
			$("#"+this.enddate_id).removeClass( "error");
			// not all day event, check hours
			if (! fl_allday) {
				MIOGA.logDebug("EventEditor", 1, "all day no set check hours");
				var starthour = $('#'+this.starthour_id).timepicker('getHour');
				var startmin = $('#'+this.starthour_id).timepicker('getMinute');
				MIOGA.logDebug("EventEditor", 1, "starthour = " + starthour);
				MIOGA.logDebug("EventEditor", 1, "startmin = " + startmin);
				start.setHours(starthour);
				start.setMinutes(startmin);
				var endhour = $('#'+this.endhour_id).timepicker('getHour');
				var endmin = $('#'+this.endhour_id).timepicker('getMinute');
				MIOGA.logDebug("EventEditor", 1, "endhour = " + endhour);
				MIOGA.logDebug("EventEditor", 1, "endmin = " + endmin);
				end.setHours(endhour);
				end.setMinutes(endmin);
				if ( start > end ) {
					$("#"+this.starthour_id).addClass( "error");
					$("#"+this.endhour_id).addClass( "error");
					error = false;
				}
				else {
					$("#"+this.starthour_id).removeClass( "error");
					$("#"+this.endhour_id).removeClass( "error");
				}
			}
		}
		MIOGA.logDebug("EventEditor", 1, "return value : " + error);

		return error;
	}
	// ----------------------------------------------------------------
	// validateForm
	//    validate form fields and create event
	// ----------------------------------------------------------------
	this.validateForm = validateForm;
	function validateForm() {
		MIOGA.logDebug("EventEditor", 1, "validateForm ==========================");
		var error = 0;
		var rrule = {};

		// TODO suppress all error classes

		var subject = $('#'+that.subject_id).val();
		$.trim(subject);
		MIOGA.logDebug("EventEditor", 1, "subject = " + subject);
		if (subject.length <= 0) {
			$('#'+this.subject_id).addClass('error');
			error++;
		}
		else {
			$('#'+this.subject_id).removeClass('error');
		}
		// TODO check for malicious javascript
		var desc = $('#'+that.desc_id).val();
		$.trim(desc);
		MIOGA.logDebug("EventEditor", 1, "desc = " + desc);

		var allday = $('#'+this.allday_id).is(':checked');
		MIOGA.logDebug("EventEditor", 1, "allday = " + allday);

		var start, end;
		if (this.checkDatesAndHours('validate')) {
			MIOGA.logDebug("EventEditor", 1, "get dates");

			start = new Date($("#"+this.startdate_id).datepicker( "getDate"));
			end = new Date($("#"+this.enddate_id).datepicker( "getDate"));
			if (! allday) {
				start.setHours($('#'+this.starthour_id).timepicker('getHour'));
				start.setMinutes($('#'+this.starthour_id).timepicker('getMinute'));
				end.setHours($('#'+this.endhour_id).timepicker('getHour'));
				end.setMinutes($('#'+this.endhour_id).timepicker('getMinute'));
			}
		}
		else {
			MIOGA.logDebug("EventEditor", 1, "dates error");
			error++;
		}
		MIOGA.logDebug("EventEditor", 1, "start = " + start);
		MIOGA.logDebug("EventEditor", 1, "end = " + end);
		// Category
		var category_id = $('#'+this.category_id).val();

		var calendar_num = this.$callist.val();
		MIOGA.logDebug("EventEditor", 1, "calendar_num = " + calendar_num);
		var calendar = that.CL.getCalendarByNum(calendar_num);
		if (! calendar) {
			alert("EventEditor.validateForm bad calendar number =" + calendar_num);
			error += 1;
		}
		var fl_rrule = $('#'+this.rrulechk_id).is(':checked');
		MIOGA.logDebug("EventEditor", 1, "fl_rrule = " + fl_rrule);

		// Recurrence check
		if (fl_rrule) {
			// period
			rrule.period = $('#'+this.period_id).val();
			$.trim(rrule.period);
			if (rrule.period.length <= 0) {
				$('#'+this.period_id).addClass('error');
				error++;
			}
			else {
				$('#'+this.period_id).removeClass('error');
			}
			// repeat mode
			rrule.repeat = $('#'+that.repeat_id).val();

			// end choice
			rrule.endChoice = $("input[name='rendchoice']:checked").val();
			// end count
			if (rrule.endChoice == 2) {
				rrule.endCount = $('#'+that.rendcount_id).val();
				$.trim(rrule.endCount);
				if (rrule.endCount.length <= 0) {
					$('#'+this.rendcount_id).addClass('error');
					error++;
				}
				else {
					$('#'+this.rendcount_id).removeClass('error');
				}
			}
			else {
				rrule.endCount = 1;
			}
			// end date
			if (rrule.endChoice == 3) {
				rrule.endDate = $("#"+this.renddate_id).datepicker("getDate");
				MIOGA.logDebug("EventEditor", 1, "rrule.enddate = " + rrule.endDate);
				$.trim(rrule.endDate);
				var difference = (rrule.endDate- start ) / (86400000 * 7);

				if (rrule.endDate.length <= 0 || difference < 0) {
					$('#'+this.renddate_id).addClass('error');
					error++;
				}
				else {
					$('#'+this.renddate_id).removeClass('error');
				}
			}
			else {
				rrule.endDate = "";
			}
			// weekdays
			rrule.weekdays = new Array();
			if (rrule.repeat === "weekly") {
				var checked = false;
				$("#" + this.rweekopt_id + ' label').removeClass('error');
				$("#" + this.rweekopt_id + ' [type="checkbox"]').each(function (i,e) {
					if ($(e)[0].checked) {
						checked = true
					}
				});
				if (checked === true) {
//					==========================================================================================================================
//					il faut générer un tableau de 7 booléans indiquant les jours de la semaine pour lesquels l'événement sera reproduit.
//					Il faut considérer la date UTC de l'événement dans la détermination des jours de la semaine.
//					Si mardi est coché, mais qu'en temps UTC il s'agit de lundi, il faut enregistrer lundi.
//					On calcul donc la diférence, en nombre de jour, entre le jour de l'événement en temps local et le jour de l'événement en temps UTC.
//					Elle sera de 1 , 0 ou -1;
//					Pour chaque checkbox, l'information true ou false sera répercutée dans le tableau à l'indice correspondant incluant la différence calculée.
//					==========================================================================================================================
//					It must make array of 7 booleans that meaning week day for which event will be remakes.
//					It must to get UTC time of event in determination of week days.
//					If tuesday is checked but in UTC time it is monday, this is monday that must be saved.
//					We calculate the diference, in number of day, between the day of event in local time and day of event in UTC time.
//					It will be 1, 0 or -1;
//					Foreach checkbox, the boolean information will be reflected in array at the index with this diference.
//					
//					==========================================================================================================================
					var offsetDay = start.getDate() - start.getUTCDate();
					rrule.weekdays[(1-offsetDay)] = $('#'+this.rmonday_id).prop('checked');
					rrule.weekdays[(2-offsetDay)] = $('#'+this.rtuesday_id).prop('checked');
					rrule.weekdays[(3-offsetDay)] = $('#'+this.rwednesday_id).prop('checked');
					rrule.weekdays[(4-offsetDay)] = $('#'+this.rthursday_id).prop('checked');
					rrule.weekdays[(5-offsetDay)] = $('#'+this.rfriday_id).prop('checked');
					
					if ((6-offsetDay) > 6) {
						rrule.weekdays[0] = $('#'+this.rsaturday_id).prop('checked');
					}
					else {
						rrule.weekdays[(6-offsetDay)] = $('#'+this.rsaturday_id).prop('checked');
					}
					
					if ((0-offsetDay) < 0) {
						rrule.weekdays[6] = $('#'+this.rsunday_id).prop('checked');
					}
					else {
						rrule.weekdays[(0-offsetDay)] = $('#'+this.rsunday_id).prop('checked');
					}
				}
				else {
					$("#" + this.rweekopt_id + ' label').addClass('error');
					error++;
				}
			}
			else {
				for (var i = 0; i < 7; i++) {
					rrule.weekdays[i] = false;
				}
			}
			// monthmode
			if (rrule.repeat === "monthly") {
				rrule.monthmode = $("input[name='rmonthmode']:checked").val();
				if (rrule.monthmode == 1) {
					rrule.fixmonthday = $("#"+this.rfixmonthday_id).val();
					$.trim(rrule.fixmonthday);
					if (rrule.fixmonthday.length <= 0) {
						$('#'+this.rfixmonthday_id).addClass('error');
						error++;
					}
					else {
						$('#'+this.rfixmonthday_id).removeClass('error');
					}
					rrule.monthdaypos = 1;
					rrule.monthday = 0;
				}
				else if (rrule.monthmode == 2) {
					rrule.fixmonthday = 1;
					rrule.monthdaypos =  $('#'+that.rmonthdaypos_id).val();
					rrule.monthday = $('#'+that.rmonthday_id).val();
				}
				else {
					rrule.fixmonthday = 1;
					rrule.monthdaypos = 1;
					rrule.monthday = 0;
				}
			}
			else {
				rrule.fixmonthday = 1;
				rrule.monthdaypos = 1;
				rrule.monthday = 0;
			}
			// yearmode
			if (rrule.repeat === "yearly") {
				rrule.fixyearday = $('#'+this.rfixyearday_id).val();
				$.trim(rrule.fixyearday);
				if (rrule.fixyearday.length <= 0) {
					$('#'+this.rfixyearday_id).addClass('error');
					error++;
				}
				else {
					$('#'+this.rfixyearday_id).removeClass('error');
				}
				rrule.fixyearmonth = $('#'+this.rfixyearmonth_id).val();
			}
			else {
				rrule.fixyearday = 1;
				rrule.fixyearmonth = 1;
			}
			MIOGA.logDebug("EventEditor", 1, "rrule = ");
		}
		MIOGA.logDebug("EventEditor", 1, "error = " + error);
		if (error === 0) {
			this.editEvent.title  = subject + "";
			this.editEvent.desc   = desc;
			this.editEvent.allDay = allday;
			this.editEvent.start  = start;
			this.editEvent.end    = end;
			this.editEvent.category_id    = category_id;
			this.editEvent.fl_rrule    = fl_rrule;
			if (fl_rrule) {
				this.editEvent.rrule = $.extend(true, {}, rrule);
			}
			MIOGA.logDebug("EventEditor", 1, "editEvent = ");
			
			if (this.mode === "edit") {
				calendar.modifyEvent({ "calEvent" : this.editEvent,
										"success" : function(code) {
														MIOGA.logDebug("EventEditor", 1, "return from modify event code : " + code);
														//that.$fc.fullCalendar('refetchEvents');
													}
									});
			}
			else { // default to create
				calendar.addNewEvent({ "calEvent" : this.editEvent,
										"success" : function(code) {
														MIOGA.logDebug("EventEditor", 1, "return from create event code : " + code);
													}
									});
			}
		}
		else {
			return false;
		}
		return true;
	}
	// ----------------------------------------------------------------
	// suppressEvent
	//    suppress event in calendar
	// ----------------------------------------------------------------
	this.suppressEvent = suppressEvent;
	function suppressEvent() {
		MIOGA.logDebug("EventEditor", 1, "EventEditor.suppressEvent ==========================");
		var error = 0;

		var cal = this.editEvent.calendar;
		if (cal) {
			MIOGA.logDebug("EventEditor", 1, "effective suppress");
			cal.suppressEvent(this.editEvent);
		}

		return true;
	}
} // End of EventEditor

