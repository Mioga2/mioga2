// =============================================================================
/**
This module permits to set access rights for user calendar

    var options = {
        i18n : {
				userSearchLabel: "Search"
				}
    };

	this.appPB = new CalAccesRightEditor(this.elem, options);

@class CalAccessRightEditor
@constructor
@param {Object} elem JQuery parent element
@param {Object} options

**/
// =============================================================================
function CalAccessRightEditor(elem, options) {

	"use strict";

	this.elem     = elem;
	this.i18n     = options.i18n;
	this.returnCB = options.returnCB;
	this.CL       = options.calendarList;

	this.curCalendar = undefined; // current calendar to process
	this.actionCB    = undefined; // Callback to call on change

	MIOGA.debug.CalAccessRightEditor = 0;

	this.$dialog = $('<div class="chronos-dialog"></div>').appendTo(elem);
	var tabs_id = MIOGA.generateID("chronos");
	var tab_search_id = MIOGA.generateID("chronos");
	var tab_user_id = MIOGA.generateID("chronos");
	var tab_team_id = MIOGA.generateID("chronos");
	var close_btn_id = MIOGA.generateID("chronos");
	var search_btn_id = MIOGA.generateID("chronos");
	var add_btn_id = MIOGA.generateID("chronos");

	this.team_select_id = MIOGA.generateID("chronos");
	this.user_select_id = MIOGA.generateID("chronos");
	this.user_found_id = MIOGA.generateID("chronos");
	this.user_not_found_id = MIOGA.generateID("chronos");
	this.user_label_id = MIOGA.generateID("chronos");
	this.email_txt_id = MIOGA.generateID("chronos");
	this.rowid_txt_id = MIOGA.generateID("chronos");

	var that = this;

	var html = '<div class="access-right-dialog" id="'+tabs_id+'">'
					+ '<ul>'
        			     +'<li><a href="#'+tab_search_id+'">'+this.i18n.userSearchLabel+'</a></li>'
        			     +'<li><a href="#'+tab_user_id+'">'+this.i18n.userFromGroupLabel+'</a></li>'
        			     +'<li><a href="#'+tab_team_id+'">'+this.i18n.teamLabel+'</a></li>'
					+ '</ul>'
					+ '<div id="'+tab_search_id+'">'
						+ '<p class="search-email-form" >'
							+ '<label>'+this.i18n.emailLabel+'</label>'
							+ '<input id="'+this.email_txt_id+'" type="text" name="email" value="" placeholder="'+this.i18n.emailLabel+'" />'
							+ '<button class="button" id="'+search_btn_id+'">'+this.i18n.searchLabel+'</button>'
						+ '</p>'
						+ '<p id="'+this.user_not_found_id+'" >'+this.i18n.notFoundLabel+'</p>'
						+ '<p id="'+this.user_found_id+'" >'
							+ '<span id="'+this.user_label_id+'" ></span>'
							+ '<span><input id="'+this.rowid_txt_id+'" type="hidden" name="user_rowid" value="" /><button class="button" id="'+add_btn_id+'">'+this.i18n.addLabel+'</button></span>'
						+ '</p>'
					+ '</div>'
					+ '<div id="'+tab_user_id+'">'
						+ '<div class="user-select" id="'+this.user_select_id+'"></div>'
					+ '</div>'
					+ '<div id="'+tab_team_id+'">'
						+ '<div class="item-select" id="'+this.team_select_id+'"></div>'
					+ '</div>'
				+ '</div>'
				+'<button id="'+close_btn_id+'" >'+this.i18n.closeLabel+'</button></p>';

	this.$dialog.append(html);

	// First tab : User search
	// on search button, demand user to the calendar. On success
	$('#'+this.user_found_id).css("visibility", "hidden");
	$('#'+this.user_not_found_id).css("visibility", "hidden");
	$('#'+search_btn_id).click(function(event) {
					$('#'+that.user_found_id).css("visibility", "hidden");
					$('#'+that.user_not_found_id).css("visibility", "hidden");
					if (that.curCalendar) {
						// read and verify email
						var email = $('#' + that.email_txt_id).val();
						$.trim(email);
						if (email.length <= 0) {
							$('#'+that.email_txt_id).addClass('error');
						}
						else {
							// if correct email, search for user
							$('#'+that.email_txt_id).removeClass('error');
							that.curCalendar.searchUserFromMail(email, function(user_data) {
								if (user_data === null) {
									$('#'+that.user_not_found_id).css("visibility", "visible");
								} else {
									$('#'+that.user_label_id).html(user_data.firstname + ' ' + user_data.lastname + '(' + user_data.email + ')');
									$('#'+that.rowid_txt_id).val(user_data.rowid);
									$('#'+that.user_found_id).css("visibility", "visible");
								}
							});
						}
					} else {
						alert("Chronos.CalAccessRightEditor no current calendar error");
					}
				});
	// on add button add access right to calendar if rowid defined
	$('#'+add_btn_id).click(function(event) {
					if (that.curCalendar) {
						that.curCalendar.changeAccessForObject($('#'+that.rowid_txt_id).val(),
						                                       that.curCalendar.CONSTANT.ACCESS_TYPE.USER,
						                                       that.curCalendar.CONSTANT.ACCESS_ACTION_TYPE.ADD,
															   1, // read only
															   false,
															   function() {
																	$('#'+that.user_found_id).css("visibility", "hidden");
																	$('#'+that.user_not_found_id).css("visibility", "hidden");
																	$('#'+that.rowid_txt_id).val("");
																	if ($.isFunction(that.actionCB)) {
																		that.actionCB();
																	}
																});
					} else {
						alert("Chronos.CalAccessRightEditor no current calendar error");
					}
				});

	// Second tab : User select
	$('#'+this.user_select_id).userSelect({ i18n : { select_all : that.i18n.selectAllLabel,
									          deselect_all: that.i18n.selectNoneLabel,
									          add_btn 	: that.i18n.addLabel,
									          all 	: that.i18n.allLabel,
											  columns : [
															{
																field: 'firstname',
																label: that.i18n.firstnameLabel
															},
															{
																field: 'lastname',
																label: that.i18n.lastnameLabel,
																Default: true
															},
															{
																field: 'email',
																label: that.i18n.emailLabel
															}
													 ]
											},
									 select_add : function(rowids) {
									 					MIOGA.logDebug("CalAccessRightEditor", 2, "Add given rowids to access rights");
														that.addSelectedUsers(rowids);
									              }
	});
	// Third tab : Team select
	$('#'+this.team_select_id).itemSelect({ i18n : { select_all : that.i18n.selectAllLabel,
									          deselect_all: that.i18n.selectNoneLabel,
									          add_btn 	: that.i18n.addLabel
											},
									 select_add : function(rowids) {
									 					MIOGA.logDebug("CalAccessRightEditor", 2, "Add given rowids to access rights");
														that.addSelectedTeams(rowids);
									              }
	});
	// Close dialog
	$('#'+close_btn_id).click(function(event) {
								MIOGA.logDebug("CalAccessRightEditor", 2, 'close');
								that.$dialog.dialog('close');
								return false;
							});
	
	// Create dialog
	this.$dialog.dialog({ title: this.i18n.accessRightTitle,
			                            autoOpen:false,
										modal:true,
										width: '780',
										height:'450',
										closeOnEscape:true
									});
	$('#'+tabs_id).tabs();
	// ----------------------------------------------------------------------------
	/**
	Launch edit dialog

	@method edit 
	@param {Object} calendar
	@param {Function} actionCB
	**/
	// ----------------------------------------------------------------------------
	this.edit = edit;
	function edit(calendar, actionCB) {

		this.curCalendar = calendar;
		this.actionCB    = actionCB;

		// reinit dialog
		$('#'+this.user_found_id).css("visibility", "hidden");
		$('#'+this.user_not_found_id).css("visibility", "hidden");
		$('#'+that.rowid_txt_id).val("");

		// Get team list and initialize plugin
		calendar.getTeamList(function(result) {
				//TODO if result.length is 0 => write a message "no teams"
				$('#'+that.team_select_id).itemSelect('replace_list', result);
		});

		that.curCalendar.getUserList(function(result) {
				//TODO if result.length is 0 => write a message "no users"
				$('#'+that.user_select_id).userSelect('replace_list', result);
		});

		this.$dialog.dialog('open');
	}
	// ----------------------------------------------------------------------------
	/**
	Add selected users

	@method addSelectedUsers
	@param {Array} rowids
	**/
	// ----------------------------------------------------------------------------
	this.addSelectedUsers = addSelectedUsers;
	function addSelectedUsers(rowids) {
		MIOGA.logDebug("CalAccessRightEditor", 1, ' addSelectedUsers');
		for (var i = 0; i < rowids.length; i++) {
			that.curCalendar.changeAccessForObject(rowids[i],
												   that.curCalendar.CONSTANT.ACCESS_TYPE.USER,
												   that.curCalendar.CONSTANT.ACCESS_ACTION_TYPE.ADD,
												   1, //read only
												   false,
												   function() {
													});
		}
		// Re-init team list
		that.curCalendar.getUserList(function(result) {
				//TODO if result.length is 0 => write a message "no users"
				$('#'+that.user_select_id).userSelect('replace_list', result);
		});
		if ($.isFunction(that.actionCB)) {
			that.actionCB();
		}
	}
	// ----------------------------------------------------------------------------
	/**
	Add selected teams

	@method addSelectedTeams 
	@param {Array} rowids
	**/
	// ----------------------------------------------------------------------------
	this.addSelectedTeams = addSelectedTeams;
	function addSelectedTeams(rowids) {
		MIOGA.logDebug("CalAccessRightEditor", 1, ' addSelectedTeams');
		for (var i = 0; i < rowids.length; i++) {
			that.curCalendar.changeAccessForObject(rowids[i],
												   that.curCalendar.CONSTANT.ACCESS_TYPE.TEAM,
												   that.curCalendar.CONSTANT.ACCESS_ACTION_TYPE.ADD,
												   1, //read only
												   false,
												   function() {
													});
		}
		// Re-init team list
		that.curCalendar.getTeamList(function(result) {
				//TODO if result.length is 0 => write a message "no teams"
				$('#'+that.team_select_id).itemSelect('replace_list', result);
		});
		if ($.isFunction(that.actionCB)) {
			that.actionCB();
		}
	}


} // End of CalAccessRightEditor

