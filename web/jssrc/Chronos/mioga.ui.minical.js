(function( $ ) {

	var options = {
				dayNamesMin : ['Su','Mo','Tu','We','Th','Fr','Sa'],
				dayNames : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
				dayNamesShort :  ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
				monthNames:['January','February','March','April','May','June', 'July','August','September','October','November','December'],
				monthNamesShort:['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
				firstDay : 0,
				nextText : "Next",
				prevText : "Previous"
	};

	function getMonthString(params) {
		var m = params.currentDate.getMonth();
		var y = params.currentDate.getFullYear();
		var res = params.monthNames[m] + ' ' + y;
		return res;
	}

	function selectDay(day) {
	}

	function modifyMonth(params) {
		params.$title.html(getMonthString(params));

		// Get first day of month
		var date = new Date(params.currentDate.getFullYear(), params.currentDate.getMonth(), 1, 0, 0, 0, 0 );

		// Get last day of month
		var end_date = date.getLastDayOfMonth();


		// Calculate last day to display
		end_date.setDate(end_date.getDate() + 1);
		while (end_date.getDay() !== parseInt(params.firstDay, 10)) {
			end_date.setDate(end_date.getDate() + 1);
		}
		end_date.setDate(end_date.getDate() - 1);


		// Calculate first day to display
		while (date.getDay() !== parseInt(params.firstDay, 10)) {
			date.setDate(date.getDate() - 1);
		}

		// Prepare elements for highliting current view and today;
		var today = new Date();

		// Generate HTML table
		params.$month.children().remove();
		var $table = $('<table></table>').appendTo(params.$month);
		// Header
		var $thead = $('<thead></thead>').appendTo($table);
		var $firstRow = $('<tr></tr>').appendTo($thead);
		$('<th></th>').appendTo($firstRow);
		var iday = parseInt(params.firstDay, 10);
		for (var i = 0; i < 7; i++) {
			if (iday > 6) { iday = 0; }
			$('<th>'+params.dayNamesMin[iday]+'</th>').appendTo($firstRow);
			iday++;
		}
		// Body with weeks and days
		var $tbody = $('<tbody></tbody>').appendTo($table);
		var needClose = false;
		var $currentRow;
		var flCurWeek = false;
		while (date.getTime() < end_date.getTime()) {
			iday = date.getDay();
			if  (iday === parseInt(params.firstDay, 10)) {
				var $currentRow = $('<tr></tr>').appendTo($tbody);
				var week_class = "minical-week";
				if (date.getWeek() === today.getWeek()) {
					week_class += " minical-today";
				}
				if (params.view === 'agendaWeek' && date.getWeek() === params.currentDate.getWeek()) {
					week_class += " minical-view";
					flCurWeek = true;
				}
				else {
					flCurWeek = false;
				}
				$('<th><a class="'+week_class+'">'+date.getWeek()+'</a></th>')
								.appendTo($currentRow)
								.click({ year:date.getFullYear(), month:date.getMonth(), day:date.getDate()}, function(event) {
											if (typeof params.weekCB === 'function') {
												params.weekCB(event, event.data.year, event.data.month, event.data.day);
											}
								       });
			}
			var day_class = "minical-day";
			if (date.getMonth() === params.currentDate.getMonth()) {
				day_class += " ui-state-default minical-indate";
			}
			else {
				day_class += " minical-outdate";
			}
			if (date.getMonth() === today.getMonth() && date.getFullYear() == today.getFullYear() && date.getDate() == today.getDate() ) {
				day_class += " minical-today";
			}
			if (flCurWeek) {
				day_class += " minical-view";
			}
			if (params.view === 'agendaDay' && date.getDate() === params.currentDate.getDate() && date.getMonth() === params.currentDate.getMonth()) {
				day_class += " minical-view";
			}
			var $day = $('<td><a class="'+day_class+'">'+date.getDate()+'</a></td>')
							.appendTo($currentRow)
							.click({ year:date.getFullYear(), month:date.getMonth(), day:date.getDate()}, function(event) {
										if (typeof params.dayCB === 'function') {
											params.dayCB(event, event.data.year, event.data.month, event.data.day);
										}
							   });
			date.setDate(date.getDate() + 1);
		}
	}

	function nextMonth(event) {
		var params = event.data.params;
		if (!params.currentDate) {
			return;
		}
		var m = params.currentDate.getMonth() + 1;
		var y = params.currentDate.getFullYear();
		if (m > 11) {
			y += 1;
			m = 0;
		}
		params.currentDate.setMonth(m);
		params.currentDate.setFullYear(y);
		modifyMonth(params);
	}

	function previousMonth(event) {
		var params = event.data.params;
		if (!params.currentDate) {
			return;
		}
		var m = params.currentDate.getMonth() - 1;
		var y = params.currentDate.getFullYear();
		if (m < 0) {
			y -= 1;
			m = 11;
		}
		params.currentDate.setMonth(m);
		params.currentDate.setFullYear(y);
		modifyMonth(params);
	}

	function callCB(event) {
		var params = event.data.params;
		var cb = event.data.cb;
		if (typeof params[cb] === 'function') {
			if (cb === 'titleCB') {
				params[cb](event.data.elem, params.currentDate.getFullYear(), params.currentDate.getMonth());
			}
		}
	}

	function draw(elem, params) {
		params.$cont = $(elem);
		$(elem).addClass('hasMinical');
		params.$main = $('<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>').appendTo(elem);
		params.$header = $('<div class="minical-header ui-widget-header ui-helper-clearfix ui-corner-all"></div>').appendTo(params.$main);

		$('<a title="'+params.prevText+'" class="minical-prev ui-corner-all"><span class="ui-icon ui-icon-circle-triangle-w">' + params.prevText + '</span></a>').appendTo(params.$header).click({"params": params} , previousMonth);
		$('<a title="'+params.nextText+'" class="minical-next ui-corner-all"><span class="ui-icon ui-icon-circle-triangle-e">' + params.nextText+'</span></a>').appendTo(params.$header).click({"params": params}, nextMonth);
		params.$title = $('<span class="minical-title"></span>').appendTo(params.$header).click({"params": params, "cb" : "titleCB"}, callCB);

		params.$month = $('<div class="minical-month"></div>').appendTo(params.$main);

		modifyMonth(params);
	}

	var methods = {
		init : function(opt) { 
			var params = $.extend({}, options, opt);
			if (!params.currentDate) {
				params.currentDate = new Date();
			}
			draw(this, params);
			$(this).data('params', params);
		},
		setView : function(opt) {
			var params = $(this).data('params');
			params.currentDate = opt.currentDate;
			params.view =  opt.viewType;
			modifyMonth(params);
		}
	};
	// --------------------------------------------------------
	// Effective plugin declaration with methods abstraction
	// --------------------------------------------------------
	$.fn.minical = function(method) {
		var args = arguments;
		return this.each(function() {
			if ( methods[method] ) {
				methods[method].apply( this, Array.prototype.slice.call(args, 1));
			}
			else if ( typeof method === 'object' || ! method ) {
				return methods.init.call(this, method);
			}
			else {
				$.error( 'Method ' +  method + ' does not exist on jQuery.minical' );
			}    
		});
	};
})( jQuery );

