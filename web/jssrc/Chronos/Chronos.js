// =============================================================================
/**
Create a Chronos object.

    var options = {
        i18n : { .... }
    };

	this.appPB = new Chronos(this.elem, options);

@class Chronos
@constructor
@param {Object} elem JQuery element parent of progress bar
@param {Object} options
**/
// =============================================================================
function Chronos(elem, options) {
	
	// Attributs
	this.i18n       = options.i18n;
	this.elem       = elem;

	// calendar preferences
	this.currentCalId;  // current selected calendar
	this.currentView;  // current selected type of view
	this.colorscheme;
	this.firsthour;
	this.showweekend;
	this.color;  // group calendar default color
	this.bgcolor;  // group calendar default background color
	this.mode;          // calendar in user mode, group mode or resource mode

	this.currentWindow;  // Used by hashchange system to hide the current window

	var that = this; // For callback reference to current object
	MIOGA.debug.Chronos = 0;

	MIOGA.object = this;

	// --------------------------------------------------------
	// Create and activate progress bar
	// --------------------------------------------------------
	var pb_opt = { i18n : { "displayText" : this.i18n.progressBarText } };
	this.appPB = new AppProgressBar(this.elem, pb_opt);
	this.appPB.setValue(0);

	// --------------------------------------------------------
	// Read configuration on server (synchronous call) and initialize object
	// --------------------------------------------------------
	if (!options.config) {
		MIOGA.logDebug("Chronos", 2, 'must read configuration');
		this.config = readConfiguration.apply(this);
	}
	else {
		this.config = options.config;
	}
	MIOGA.logDebug("Chronos", 1, 'Config ......');
	this.currentCalId = this.config.currentCalId;
	this.currentView =  this.config.currentView;
	this.colorscheme = this.config.colorscheme;
	this.firsthour = this.config.firsthour;
	this.showweekend = true;
	if (parseInt(this.config.showweekend, 10) === 0) {
		this.showweekend = false;
	}

	this.mode = this.config.mode;
	this.color = this.config.color;
	this.bgcolor = this.config.bgcolor;

	if (parseInt(this.config.anim_right, 10) === 0) {
		this.animRight = false;
	} else {
		this.animRight = true;
	}

	if (this.config.categories.length <= 0) {
		alert(options.i18n.errorNoCategories);
	}

	MIOGA.logDebug("Chronos", 1, "currentCalId = " + this.currentCalId + " currentView = " + this.currentView + " colorscheme = " + this.colorscheme + " firsthour = " + this.firsthour + " showweekend = " + this.showweekend + " animRight = " + this.animRight);

	this.appPB.setValue(10);

	// --------------------------------------------------------
	// Create all calendars
	// --------------------------------------------------------
	this.calendarList = new MiogaCalendarList({ chronos:this});
	if (this.mode === 'user') {
		this.calendarList.setLists(this.config.calendars, this.config.currentCalId);
	} else {
		this.calendarList.setLists(this.config.calendars, 0);
	}
	this.calendarList.setAuthorizedGroups(this.config.groupCals);
	this.calendarList.setAuthorizedOthers(this.config.otherCals);
	this.appPB.setValue(20);
	// ------------------------------------------------------------------------------------------------
	// Configuration window
	// ------------------------------------------------------------------------------------------------
	this.configEditor = new ChronosConfigEditor(elem, {"i18n"            : options.i18n,
														"config"         : this.config,
														"calendarList"   : this.calendarList,
														"configChangeCB" : function(action, args) {
																				MIOGA.logDebug("Chronos", 1, 'configChangeCB called');
																				configChange.call(that, action, args);
																			}
													});
	this.configEditor.hideWindow();
	this.appPB.setValue(30);
	// ------------------------------------------------------------------------------------------------
	// Event Editor
	// ------------------------------------------------------------------------------------------------
	this.eventEditor = new EventEditor(elem, {"i18n"           : options.i18n,
												"config"       : this.config,
												"categories"   : this.config.categories,
												"chronos"      : this,
												"dayNamesMin"  : options.dayNamesMin,
												"dayNames"     : options.dayNames,
												"monthNames"   : options.monthNames,
												dayNamesShort   : options.dayNamesShort,
												monthNamesShort : options.monthNamesShort,
												"calendarList" : this.calendarList
										});
	this.eventEditor.hideWindow();

	this.appPB.setValue(40);
	// --------------------------------------------------------
	// Project tasks only in user mode
	// --------------------------------------------------------
	this.projectTaskList = null;
	if (this.mode === 'user') {
		// --------------------------------------------------------
		// Create project task list
		// --------------------------------------------------------
		this.projectTaskList = new ProjectTaskList({ chronos:this});
		// ------------------------------------------------------------------------------------------------
		// Project tasks list  editor
		// ------------------------------------------------------------------------------------------------
		this.projectTaskListEditor = new ProjectTaskListEditor(elem, {"i18n" : options.i18n,
																		dayNamesMin     : options.dayNamesMin,
																		dayNames        : options.dayNames,
																		dayNamesShort   : options.dayNamesShort,
																		monthNames      : options.monthNames,
																		monthNamesShort : options.monthNamesShort,
																	   chronos : this,
																	   projectTaskList : this.projectTaskList
																	});
		this.projectTaskListEditor.hideWindow();
	}
	this.appPB.setValue(50);
	// ------------------------------------------------------------------------------------------------
	// Task editor
	// ------------------------------------------------------------------------------------------------
	this.taskEditor = new TaskEditor(elem, {"i18n" : options.i18n
										});
	this.taskEditor.hideWindow();

	this.appPB.setValue(60);

	// ------------------------------------------------------------------------------------------------
	// Create main window,views and tools
	// ------------------------------------------------------------------------------------------------
	this.chronosMain = new ChronosMain(elem, {
											mode            : this.mode,
											currentView     : this.currentView,
											firsthour       : this.firsthour,
											showweekend     : this.showweekend,
											categories      : this.config.categories,
											CL              : this.calendarList,
											PL              : this.projectTaskList,
											chronos         : this,
											i18n            : options.i18n,
											dayNamesMin     : options.dayNamesMin,
											dayNames        : options.dayNames,
											dayNamesShort   : options.dayNamesShort,
											monthNames      : options.monthNames,
											monthNamesShort : options.monthNamesShort,
											firstDay        : options.firstDay,
											buttonText      : options.buttonText,
											titleFormat     : options.titleFormat,
											header          : options.header,
											allDayText      : options.allDayText,
											columnFormat    : options.columnFormat,
											configChangeCB  : function(action, args) {
																	MIOGA.logDebug("Chronos", 1, 'configChangeCB called');
																	configChange.call(that, action, args);
																}
	                                   });

	this.chronosMain.hideWindow();
	this.appPB.setValue(100);
	this.appPB.$pbcont.hide();

	//this.calendarList.addRefreshCB(this.refreshCL, this);
	//this.refreshCL();
	// ------------------------------------------------------------------------------------------------
	// Prepare callback for navigation on hashchange
	// ------------------------------------------------------------------------------------------------
	$(window).hashchange( function(){
		MIOGA.logDebug("Chronos", 1,  'new hashchange ' + location.hash );
		var hash = location.hash;
		var newWindow;
		if (hash === '#create') {
			MIOGA.logDebug("Chronos", 1, 'Show create event window');
			newWindow = that.eventEditor;
		}
		else if (hash === '#config') {
			MIOGA.logDebug("Chronos", 1, 'Show config window');
			newWindow = that.configEditor;
		}
		else if (hash === '#project_tasks') {
			MIOGA.logDebug("Chronos", 1, 'Show project task window');
			newWindow = that.projectTaskListEditor;
		}
		else if (hash === '#tasks') {
			MIOGA.logDebug("Chronos", 1, 'Show task window');
			newWindow = that.taskEditor;
		}
		else {
			MIOGA.logDebug("Chronos", 1, 'Draw main window');
			newWindow = that.chronosMain;
		}
		if ( (that.currentWindow === undefined) || (that.currentWindow !== newWindow)) {
			MIOGA.logDebug("Chronos", 1, 'currentView not defined or change of view');
			if (that.currentWindow !== undefined) {
				that.currentWindow.hideWindow();
			}
			newWindow.showWindow();
			that.currentWindow = newWindow;
		}
	});
	$(window).hashchange();
	// End of constructor
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	// --------------------------------------------------------
	// initEventEditor set the mode for EventEditor
	// --------------------------------------------------------
	this.initEventEditor = initEventEditor;
	function initEventEditor(ev, mode, cb) {
		MIOGA.logDebug("Chronos", 1, "Chronos.initEventEditor");
		if (ev !== undefined) {
			if (mode && mode === 'edit') {
				this.eventEditor.edit(ev, cb);
			} else {
				this.eventEditor.create(ev, cb);
			}
		}
	}
	// --------------------------------------------------------
	// initEventEditor set the mode for EventEditor
	// --------------------------------------------------------
	this.getCategoryByID = getCategoryByID;
	function getCategoryByID(cat_id) {
		MIOGA.logDebug("Chronos", 1, "Chronos.getCategoryByID count = " + this.config.categories.length);
		if (cat_id === undefined) {
			return undefined;
		}
		for (var i = 0; i < this.config.categories.length; i++) {
			if (parseInt(cat_id, 10) === this.config.categories[i].category_id) {
				return this.config.categories[i];
			}
		}
		// MUST NOT BE HERE !
		// TODO LOG ERROR
		alert("Error Cannot find category for ID = " + cat_id);
		return undefined;
	}
	// ========================================================================================
	// Private functions must not be added to this
	// ========================================================================================
	// --------------------------------------------------------
	// readConfiguration from server
	// --------------------------------------------------------
	function readConfiguration() {
		MIOGA.logDebug("Chronos", 1, "readConfiguration");
		var config = new Object();
		$.ajax({
				 url: mioga_context.private_bin_uri + "/Chronos/GetConfiguration.json",
				 async:false,
				 dataType:"json",
				 success : function(data) {
				 			if (data.status === "OK") {
								config = data.config;
							}
							else {
								MIOGA.logError("Chronos.readConfiguration Error in web service", false);
							}
					  },
					  error: function(jqXHR, textStatus, errorThrown) {
						MIOGA.logError("Chronos.readConfiguration Error in web service  textStatus = " + textStatus, false);
					  }
				});
		return config;
	}
	// --------------------------------------------------------
	// configChange
	// --------------------------------------------------------
	function configChange(action, args) {
		MIOGA.logDebug("Chronos", 1, "configChange");
		MIOGA.logDebug("Chronos", 1, 'action = ' + action + " args = ", args);
		var reload = false;
		if (action === 'setView') {
			this.currentView = args.viewType;
		}
		else if (action === 'setCurCal') {
			this.calendarList.changeCurrentCalendar(args.num);
		}
		else if (action === 'setParams') {
			this.colorscheme = args.colorscheme;
			this.firsthour   = args.firsthour;
			if (this.showweekend !== args.showweekend) {
				reload = true;
			}
			this.showweekend   = args.showweekend;
			if (this.mode !== 'user') {
				this.color = args.color;
				this.bgcolor = args.bgcolor;
			}
		}
		else {
			MIOGA.logDebug("Chronos", 1, "Chronos.configChange unknown action = [" + action + "]");
			return;
		}
		writePreferences.call(this, reload);
	}
	// --------------------------------------------------------
	// writePreferences to server
	// --------------------------------------------------------
	function writePreferences(reload) {
		MIOGA.logDebug("Chronos", 1, "writePreferences");
		var config = new Object();
		var currentCalId;
		// Do nothing if user hasn't anim right
		if (!that.animRight) {
			return;
		}
		if (that.calendarList.currentCal !== undefined) {
			currentCalId = that.calendarList.currentCal.rowid;
		} else {
			currentCalId = 0;
		}
		var showwe = 0;
		if (that.showweekend) {
			showwe = 1;
		}
		var data = {
					"currentCalId" : currentCalId,
					"currentView" : that.currentView,
					"colorscheme" : that.colorscheme,
					"firsthour" : that.firsthour,
					"showweekend" : showwe
				};
		if (this.mode === 'user') {
			data.pt_color = this.pt_color;
			data.pt_bgcolor = this.pt_bgcolor;
		}
		$.ajax({
				 url: mioga_context.private_bin_uri + "/Chronos/SetPreferences.json",
				 dataType:"json",
				 data: data,
				 type: "POST",
				 success : function(data) {
						MIOGA.logDebug("Chronos", 1, "preferences saved");
						if (reload) {
							var oldloc = window.location.href;
							MIOGA.logDebug("Chronos", 1, " oldloc = " + oldloc);
							var loc = oldloc.replace( /#.*$/,'');
							MIOGA.logDebug("Chronos", 1, " loc = " + loc);
							window.location = loc;
						}
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
					  	alert("SetPreferences AJAX error : " + textStatus);
				  }
		});
	}
} // End of Chronos object
