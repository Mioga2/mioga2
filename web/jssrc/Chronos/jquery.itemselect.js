(function( $ ){
	// declaration of plugin itemSelect
	// options can be : 
	
	//	{
	//		data : "URL of WS that return array of item" or array of item or function that return array of item,
	//		i18n : {
	//			select_all	: "Select all",
	//			deselect_all: "Deselect all",
	//			add_btn 	: "Add",
	//		},
	//		******* optionals arguments *******
	//		single : boolean,
	//		select_add		: CB called when the user clicks on the submit button,
	//		select_change	: CB called each selection or deselection
	//	}
	
	// ******  usage :  ********************** 
	//		$('#id-cont').itemSelect(options);
	// ***************************************
	var defaults = {
		single 	: false,
		i18n	: {
				select_all	: "Select all",
				deselect_all: "Unselect all",
				add_btn 	: "Add"
		},
		data			: undefined,
		select_add		: undefined,
		select_change	: undefined
    };
	// ==================================================
	// ========= privates methods =======================
	// ==================================================
	function getDrawItem(item) {
		var $item = $('<li class="is-item">' + item.ident + '</li>').data({
			"rowid" 	: item.rowid,
			"ident" 	: item.ident
		});
		return $item;
	}
	function activateBtn ($elem) {
		if ($elem.find('.is-item').length === 0) {
			$elem.find('.is-select_all, .is-deselect_all').attr('disabled', true).removeClass('disabled').addClass('disabled');
		}
		else {
			$elem.find('.is-select_all, .is-deselect_all').removeAttr('disabled').removeClass('disabled');
		}
		if ($elem.find('.is-selected').length === 0) {
			$elem.find('.is-add-btn').attr('disabled', true).removeClass('disabled').addClass('disabled');
		}
		else {
			$elem.find('.is-add-btn').removeAttr('disabled').removeClass('disabled');
		}
	}
	// ========================================================
	// Methods declaration
	// ========================================================
	var methods = {
	    init : function(args) {
	    	var base_obj = $.extend({}, defaults, args);
	    	return $(this).each(function() {
		        
			       var $this = $(this);
			        if (typeof base_obj.data === 'string') {
						// TODO verify result of AJX call
			        	base_obj.list = $.get(base_obj.data);
			        }
			        // array
			        else if (Object.prototype.toString.apply(base_obj.data) === '[object Array]') {
			        	base_obj.list = base_obj.data;
			        }
			        // function
			        else if (base_obj.data instanceof Function) {
			        	base_obj.list = base_obj.data();
			        }
			        else {
			        	base_obj.list	= [];
			        }
			        $this.data("params", base_obj);
				    // Draw list container
			        $this.children().remove();
			        var $glob_selection = $(
			        	'<div class="is-link-cont">' + 
			        		'<input type="button" class="button is-select_all" value="' + base_obj.i18n.select_all   + '"/>' +
			        		'<input type="button" class="button is-deselect_all" value="' + base_obj.i18n.deselect_all + '"/>' +
			        	'</div>'
			        );
			        $this.append(
			        	'<div class="is-cont">' + 
				        	'<div class="is-list-cont"><ul class="is-list"></ul></div>' + 
					    	'<div class="is-actions-cont">' + 
					    		'<input type="button" class="button is-add-btn" value="' + base_obj.i18n.add_btn + '"/>' + 
					    	'</div>' + 
					    '</div>'
				    );
			        // Make dimensions
			        var $cont = $this.find('.is-cont'); 
			        var cont_h = parseInt($this.outerHeight(true)) - parseInt($cont.css('padding-top')) - parseInt($cont.css('padding-bottom'));
			        var cont_w = parseInt($this.outerWidth(true)) - parseInt($cont.css('padding-left')) - parseInt($cont.css('padding-right'));
			        $cont.height(cont_h + "px");
			        $cont.width(cont_w + "px");
			        var link_cont_h = 0;
			        var action_cont_h = parseInt($this.find('.is-actions-cont').outerHeight(true));
			        
			        if (base_obj.single === false) {
			        	$this.find('.is-cont').prepend($glob_selection);
			        	link_cont_h = parseInt($this.find('.is-link-cont').outerHeight(true));
				        // behavior to select or deselect all items
				        $this.find('.is-select_all').click(function () {
				        	$this.find('li').removeClass('is-selected').addClass('is-selected');
				        	activateBtn($this);
				        });
				        $this.find('.is-deselect_all').click(function () {
				        	$this.find('li').removeClass('is-selected');
				        	activateBtn($this);
				        });
			        }
			        $this.find('.is-list-cont').height(cont_h - link_cont_h - action_cont_h + "px");
			        
					// behavior to select or deselect item, and disabled or not if item selected
			        $this.find('.is-list').delegate('li', 'click',function(event) {
			        	if(event.target != this) return;
						var id			= $(this).parent().data("rowid");
						var selected 	= false;
						if ($(this).hasClass('is-selected')) {
							$(this).removeClass('is-selected');
						}
						else {
							if (base_obj.single === true) {
								$this.find('.is-selected').removeClass('is-selected');
							}
							$(this).addClass('is-selected');
							selected = true;
						}
						activateBtn($this);
						if (base_obj.select_change instanceof Function) {
							base_obj.select_change(id, selected);	
						}
					});
					// behavior to add selection
			        $this.find('.is-add-btn').click(function(event) {
						if (base_obj.select_add instanceof Function) {
				    		var rowids = new Array();
				    		$.each($this.find('.is-selected'), function (i,e) {
				    			rowids.push($(e).data("rowid"));
				    		});
				    		base_obj.select_add(rowids);
						}
					});
			        $.each(base_obj.list, function (i, e) {
			        	$this.find('.is-list').append(getDrawItem(e));
					});
			        activateBtn($this);
		    	});
		    },
	    // current list to new list
	    replace_list	: function (args) {
	    	return $(this).each(function() {
	    		var $this 	= $(this);
	    		var newList = args;
		    	// -----------------------------------------------------
		    	// Generation of list according to source. 
		    	// This is can be URL of WS, array of data or function that retrieves data.
		    	// -----------------------------------------------------
			    // URL of WS
			    if (typeof new_list === 'string') {
			    	newList = $.get(args);
			    }
			    // array
			    else if (Object.prototype.toString.apply(args) === '[object Array]') {
			    	newList = args;
			    }
			    // function
			    else if (args instanceof Function) {
			    	newList = args();
			    }
			    $this.find('.is-list').children().remove();
				$.each(newList, function (i,e) {
					$this.find('.is-list').append(getDrawItem(e));
				});
				activateBtn($this);
	    	});
	    },
	    // Refreshes the current list, delete items already selected
	    refresh_list	: function  () {
	    	return $(this).each(function() {
	    		var $this = $(this);
	    		$this.find('.is-selected').remove();
	    		activateBtn($this);
	    	});
	    },
	    // Inserts annotations on the elements of the list
	    annotate_list	: function  (args) {
	    	var msg_list = args;
	    	return $(this).each(function () {
	    		var $this = $(this);
	    		$.each(msg_list, function (i,e) {
	        		$.each($this.find('li'), function (ind,elm) {
	        			if ($(elm).data("rowid") === e.rowid) {
	        				$(elm).find('.is-annoted').remove();
	        				$(elm).append('<span class="is-annoted">' + e.message + '</span>');
	        				return;
	        			}
	        		});
	        	});
	    	});
	    },
	    // Retrieves the list and the current selection state of its components
	    get_list	: function () {
	    	var $this = $(this);
	    	var current_list = new Array();
	    	$.each($this.find('.is-item'), function (i,e) {
	    		current_list.push(
	    			{
	    				rowid	: $(e).data("rowid"),
	    				selected: $(e).hasClass('is-selected')
	    			}
	    		);
	    	});
	    	return current_list;
	    }
	};

	$.fn.itemSelect = function(method) {
	    var args = arguments;
	    if ( methods[method] ) {
	    	return methods[ method ].apply( this, Array.prototype.slice.call( args, 1 ));
	    }
	    else if ( typeof method === 'object' || ! method ) {
	    	return methods.init.apply( this, args );
	    }
	    else {
	        $.error( 'Method ' +  method + ' does not exist on jQuery.itemSelect' );
	    }
	};
})( jQuery );
