// =============================================================================
/**
Manage a calendar list for Chronos.

Note : JQuery $ function must be defined

@class MiogaCalendarList
@constructor
**/
// =============================================================================

function MiogaCalendarList(options) {
	/**
	Array of MiogaCalendar object
	@attribute calendars
	@type array
	**/
	this.calendars  = [];
	/**
	Index of calendars for direct access by rowid
	@attribute idx_by_rowid
	@type object
	**/
	this.idx_by_rowid = {};
	/**
	Array or authtorized group's calendar definition not used by user
	@attribute authorizedGroups
	@type array
	**/
	this.authorizedGroups  = [];
	/**
	Array or authtorized calendars definition from other users and not used by user
	@attribute authorizedOthers
	@type array
	**/
	this.authorizedOthers  = [];
	/**
	List of callback to call on refresh event
	@attribute refreshCB
	@type array
	@private
	**/
	this.refreshCB  = [];
	/**
	Current  selected calendar
	@attribute currentCal
	@type integer
	@private
	**/
	this.currentCal = undefined;

	MIOGA.debug.MiogaCalendarList = 0;
	MIOGA.Chronos = {};
	MIOGA.Chronos.CL = this;
	// End of constructor ==========================================================
	// ----------------------------------------------------------------------------
	// Public methods
	// ----------------------------------------------------------------------------
	/**
	Push calendar in array and rebuild index

	@method pushCalendar
	@param Object A MiogaCalendar object
	**/
	// --------------------------------------------------------
	this.pushCalendar = pushCalendar;
	function pushCalendar(calendar) {
		this.calendars.push(calendar);
		if (calendar.rowid) {
			this.idx_by_rowid[calendar.rowid] = calendar;
		}
		else {
			MIOGA.logError("MiogaCalendarList.pushCalendar calendar.rowid undefined", false);
		}
	}
	// --------------------------------------------------------
	// Create and initialize calendars
	// --------------------------------------------------------
	this.setLists = setLists;
	function setLists(calendars, currentCalId) {
		var that = this;
		MIOGA.logDebug("MiogaCalendarList", 1, "CalendarList setLists currentCalId = "+ currentCalId+" typeof " + typeof currentCalId, calendars);
		// purge this.calendars
		this.calendars = [];
		if ($.isArray(calendars)) {
			$.each(calendars, function(i, item) {
				var cal = new MiogaCalendar(item);
				that.pushCalendar(cal);
				MIOGA.logDebug("MiogaCalendarList", 1, "i = " +i+" cal rowid = "+cal.rowid + " typeof " + typeof cal.rowid);
				if (currentCalId === cal.rowid) {
					that.currentCal = cal;
				}
			});
		}
		MIOGA.logDebug("MiogaCalendarList", 1, "length = " +this.calendars.length+" currentCal = "+this.currentCal);
		if (this.calendars.length > 0) {
			if (this.currentCal === undefined) {
				MIOGA.logDebug("MiogaCalendarList", 1, "force first calendar of list");
				this.currentCal = this.calendars[0];
			}
		}

		MIOGA.logDebug("MiogaCalendarList", 3, "current calendar = ", this.currentCal);

		this.callRefreshCB();
	}
	// --------------------------------------------------------
	// Set the potential list with authorized group's calendars
	// --------------------------------------------------------
	this.setAuthorizedGroups = setAuthorizedGroups;
	function setAuthorizedGroups(list) {
		var that = this;
		MIOGA.logDebug("MiogaCalendarList", 1, "CalendarList AuthorizedGroups");
		that.authorizedGroups = [];
		if ($.isArray(list)) {
			$.each(list, function(i, item) {
				that.authorizedGroups.push(item);
			});
		}
	}
	// --------------------------------------------------------
	// Set the potential list with authorized other's calendars
	// --------------------------------------------------------
	this.setAuthorizedOthers = setAuthorizedOthers;
	function setAuthorizedOthers(list) {
		var that = this;
		MIOGA.logDebug("MiogaCalendarList", 1, "CalendarList AuthorizedGroups");
		that.authorizedOthers = [];
		if ($.isArray(list)) {
			$.each(list, function(i, item) {
				that.authorizedOthers.push(item);
			});
		}
	}
	// --------------------------------------------------------
	// Add a new refresh callback called when calendar list change
	// --------------------------------------------------------
	this.addRefreshCB = addRefreshCB;
	function addRefreshCB(cb, context) {
		var that = this;
		this.refreshCB.push( { cb : cb, context : context });
	}
	// --------------------------------------------------------
	// Call all refresh callback
	// --------------------------------------------------------
	this.callRefreshCB = callRefreshCB;
	function callRefreshCB() {
		//MIOGA.logDebug("MiogaCalendarList", 1, "CalendarList  ==== callRefreshCB ======");
		var that = this;
		$.each(this.refreshCB, function(i, item) {
				item.cb.call(item.context);
			});
	}
	// --------------------------------------------------------
	// isValidIdent
	//     check if given string is a valid calendar ident
	// --------------------------------------------------------
	this.isValidIdent = isValidIdent;
	function isValidIdent(ident) {
		if (ident.length <= 0) {
			return false;
		}
		return true;
	}
	// --------------------------------------------------------
	// Add a new owner calendar
	// --------------------------------------------------------
	this.addOwnerCalendar = addOwnerCalendar;
	function addOwnerCalendar(params) {
		var that = this;
		$.ajax({
			url: mioga_context.private_bin_uri + "/Chronos/CreateUserCalendar.json",
			type: "POST",
			data: { "ident":params.ident,
			        "color":params.color,
					"bgcolor":params.bgcolor
			       },
			dataType:"json",
			success : function(data) {
				MIOGA.logDebug("MiogaCalendarList", 1, "data = " +data);
				if (data.status === "OK") {
					var newCal = new MiogaCalendar(data.calendar);
					that.pushCalendar(newCal);
				} else {
					MIOGA.logError("MiogaCalendarList.addOwnerCalendar Error in web service", false);
				}
				that.callRefreshCB();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				MIOGA.logError("MiogaCalendarList.addOwnerCalendar Error in web service textStatus = " + textStatus, false);
			}
		});
	}
	// ----------------------------------------------------------------------------
	/**
	Connect to a group calendar

	@method connectToGroupCalendar
	@param integer rowid rowid of calendar to connect to
	**/
	// ----------------------------------------------------------------------------
	this.connectToGroupCalendar = connectToGroupCalendar;
	function connectToGroupCalendar(rowid) {
		MIOGA.logDebug("MiogaCalendarList", 1, 'connectToGroupCalendar rowid = ' + rowid);
		var that = this;
		for (var i = 0; i < this.authorizedGroups.length; i++) {
			if (this.authorizedGroups[i].rowid === rowid) {
				MIOGA.logDebug("MiogaCalendarList", 1, 'found group calendar');
				connectToCalendar.call(this, 2, this.authorizedGroups[i].ident, rowid, function() {
											MIOGA.logDebug("MiogaCalendarList", 1, 'connect to group calendar success');
											that.authorizedGroups.splice(i, 1);
										});

				return;
			}
		}
		MIOGA.logError("MiogaCalendarList.connectToGroupCalendar calendar not found", false);
	}
	// ----------------------------------------------------------------------------
	/**
	Connect to an other calendar

	@method connectToOtherCalendar
	@param integer rowid rowid of calendar to connect to
	**/
	// ----------------------------------------------------------------------------
	this.connectToOtherCalendar = connectToOtherCalendar;
	function connectToOtherCalendar(rowid) {
		MIOGA.logDebug("MiogaCalendarList", 1, 'connectToOtherCalendar rowid = ' + rowid);
		var that = this;
		for (var i = 0; i < this.authorizedOthers.length; i++) {
			MIOGA.logDebug("MiogaCalendarList", 1, '  other i = ' +  i + ' rowid = '+this.authorizedOthers[i].rowid);
			if (this.authorizedOthers[i].rowid === rowid) {
				MIOGA.logDebug("MiogaCalendarList", 1, 'found other calendar');
				connectToCalendar.call(this, 1, this.authorizedOthers[i].ident, rowid, function() {
											MIOGA.logDebug("MiogaCalendarList", 1, 'connect to other calendar success');
											that.authorizedOthers.splice(i, 1);
										});

				return;
			}
		}
		MIOGA.logError("MiogaCalendarList.connectToOtherCalendar calendar not found", false);
	}
	// ----------------------------------------------------------------------------
	/**
	Detach from calendar

	@method detachFromCalendar
	@param integer rowid rowid of calendar to detach from
	**/
	// ----------------------------------------------------------------------------
	this.detachFromCalendar = detachFromCalendar;
	function detachFromCalendar(rowid) {
		MIOGA.logDebug("MiogaCalendarList", 1, 'detachFromCalendar rowid = ' + rowid);
		var calendar = this.idx_by_rowid[rowid];
		if (calendar) {
			MIOGA.logDebug("MiogaCalendarList", 1, ' ident = ' + calendar.ident + '  rowid = ' + calendar.rowid);
			var that = this;
			$.ajax({
				url: mioga_context.private_bin_uri + "/Chronos/DetachFromCalendar.json",
				type: "POST",
				data: { "calendar_id" : calendar.rowid } ,
				dataType:"json",
				success : function(data) {
					//MIOGA.logDebug("MiogaCalendarList", 1, "data = " +data);
					if (data.status === "OK") {
						// suppress in array
						for (var num = 0; num < that.calendars.length; num++) {
							if (that.calendars[num] === calendar) {
								that.calendars.splice(num, 1);
								break;
							}
						}
						// suppress in index
						delete that.idx_by_rowid[calendar.rowid];
						// change current calendar if needed
						if (calendar.currentCal && (calendar.rowid === that.currentCal.rowid)) {
							if (that.calendars.length > 0) {
								that.currentCal = that.calendars[0];
							} else {
								that.currentCal = undefined;
							}
						}
						// Refresh calendar's list
						that.setAuthorizedGroups(data.config.groupCals);
						that.setAuthorizedOthers(data.config.otherCals);
					} else {
						MIOGA.logError("MiogaCalendarList.detachFromCalendar Error in web service", false);
					}
					that.callRefreshCB();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					MIOGA.logError("MiogaCalendarList.detachFromCalendar Error in web service textStatus = " + textStatus, false);
				}
			});
		} else {
			MIOGA.logError("MiogaCalendarList.detachFromCalendar No calendar found for rowid = " + rowid, false);
		}
	}
	// --------------------------------------------------------
	// suppressCalendar
	// --------------------------------------------------------
	this.suppressCalendar = suppressCalendar;
	function suppressCalendar(rowid) {
		MIOGA.logDebug("MiogaCalendarList", 1, 'MiogaCalendarList.suppressCalendar  rowid =' + rowid);
		var calendar = this.idx_by_rowid[rowid];
		if (calendar) {
			MIOGA.logDebug("MiogaCalendarList", 1, ' ident = ' + calendar.ident + '  rowid = ' + calendar.rowid);
			var that = this;
			$.ajax({
				url: mioga_context.private_bin_uri + "/Chronos/DestroyUserCalendar.json",
				type: "POST",
				data: { "calendar_id" : calendar.rowid } ,
				dataType:"json",
				success : function(data) {
					//MIOGA.logDebug("MiogaCalendarList", 1, "data = " +data);
					if (data.status === "OK") {
						// suppress in array
						for (var num = 0; num < that.calendars.length; num++) {
							if (that.calendars[num] === calendar) {
								that.calendars.splice(num, 1);
								break;
							}
						}
						// suppress in index
						delete that.idx_by_rowid[calendar.rowid];
						// change current calendar if needed
						if (calendar.rowid === that.currentCal.rowid) {
							if (that.calendars.length > 0) {
								that.currentCal = that.calendars[0];
							} else {
								that.currentCal = undefined;
							}
						}
					} else {
						MIOGA.logError("MiogaCalendarList.suppressCalendar Error in web service", false);
					}
					that.callRefreshCB();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					MIOGA.logError("MiogaCalendarList.suppressCalendar Error in web service textStatus = " + textStatus, false);
				}
			});
		} else {
			MIOGA.logError("MiogaCalendarList.suppressCalendar No calendar found for rowid = " + rowid, false);
		}
	}
	// --------------------------------------------------------
	// getCalendar
	// --------------------------------------------------------
	this.getCalendarByNum = getCalendarByNum;
	function getCalendarByNum(num) {
		if (num >=0 && num  < this.calendars.length) {
			return this.calendars[num];
		} else {
			MIOGA.logError("MiogaCalendarList.getCalendarByNum num out of limits", false);
			return undefined;
		}
	}
	// --------------------------------------------------------
	// getCurrentCalendar
	// --------------------------------------------------------
	this.getCurrentCalendar = getCurrentCalendar;
	function getCurrentCalendar(num) {
		return this.currentCal;
	}
	// --------------------------------------------------------
	// changeCurrentCalendar
	// --------------------------------------------------------
	this.changeCurrentCalendar = changeCurrentCalendar;
	function changeCurrentCalendar(num) {
		MIOGA.logDebug("MiogaCalendarList", 1, "changeCurrentCalendar num = " + num);
		if (num >=0 && num  < this.calendars.length) {
			this.currentCal = this.calendars[num];
		} else {
			MIOGA.logError("MiogaCalendarList.changeCurrentCalendar num out of limits", false);
		}
	}
/*
	// --------------------------------------------------------
	// getCalendarById
	// --------------------------------------------------------
	this.getCalendarById = getCalendarById;
	function getCalendarById(id) {
		//MIOGA.logDebug("MiogaCalendarList", 1, "getCalendarById   id = " + id);
		for (var i = 0; i < this.calendars.length; i++) {
			//MIOGA.logDebug("MiogaCalendarList", 1, "  i = " + i);
			//MIOGA.logDebug("MiogaCalendarList", 1, this.calendars[i]);
			if (this.calendars[i].rowid === id) {
				//MIOGA.logDebug("MiogaCalendarList", 1, " found : " + i);
				return this.calendars[i];
			}
		}
		return undefined;
	}
*/
	// ----------------------------------------------------------------------------
	// Private methods
	// ----------------------------------------------------------------------------
	// ----------------------------------------------------------------------------
	/**
	Connect to an authorized calendar (private method)

	@method connectToCalendar
	@param Integer type 1 for other and 2 for group
	@param String ident ident for calendar
	@param integer rowid rowid of calendar to connect to
	**/
	// ----------------------------------------------------------------------------
	function connectToCalendar(type, ident, rowid, cb) {
		MIOGA.logDebug("MiogaCalendarList", 1, 'connectToCalendar type = ' + type + ' ident = ' + ident + ' rowid = ' + rowid);
		var that = this;
		var params = {  "type" : type,
						"color" : "#000000",
						"bgcolor" : "#ffffff",
						"calendar_id" : rowid,
						"ident" : ident
					};
		$.ajax({
			url: mioga_context.private_bin_uri + "/Chronos/ConnectCalendar.json",
			type: "POST",
			data: params,
			dataType:"json",
			success : function(data) {
				MIOGA.logDebug("MiogaCalendarList", 1, "connectToCalendar data = " +data);
				if (data.status === "OK") {
					var newCal = new MiogaCalendar(data.calendar);
					that.pushCalendar(newCal);
					cb();
				} else {
					MIOGA.logError("MiogaCalendarList.connectToCalendar Error in web service", false);
				}
				that.callRefreshCB();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				MIOGA.logError("MiogaCalendarList.connectToCalendar Error in web service textStatus = " + textStatus, false);
			}
		});
	}
}
