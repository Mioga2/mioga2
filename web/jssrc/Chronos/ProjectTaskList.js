// =============================================================================
/**
Manage a project task list for Chronos.

Note : JQuery $ function must be defined

@class ProjectTaskList
@constructor
**/
// =============================================================================

function ProjectTaskList(options) {
	/**
	Array of ProjectTask object
	@attribute calendars
	@type array
	**/
	this.tasks  = new Array();
	/**
	Index of calendars for direct access by rowid
	@attribute idx_by_rowid
	@type object
	**/
	this.idx_by_rowid = {};
	/**
	List of callback to call on refresh event
	@attribute refreshCB
	@type array
	@private
	**/
	this.refreshCB  = [];
	// pre declare method
	this.getList = getList;
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;

	MIOGA.debug.ProjectTaskList = 0;
	MIOGA.Chronos = {};
	MIOGA.Chronos.PL = this;

	this.getList(false);
	// End of constructor ==========================================================
	// ============================================================================
	// Public methods
	// ============================================================================
	// --------------------------------------------------------
	// Add a new refresh callback called when calendar list change
	// --------------------------------------------------------
	function addRefreshCB(cb, context) {
		var that = this;
		this.refreshCB.push( { cb : cb, context : context });
	}
	// --------------------------------------------------------
	// Call all refresh callback
	// --------------------------------------------------------
	function callRefreshCB() {
		MIOGA.logDebug("ProjectTaskList", 2, "callRefreshCB ======");
		var that = this;
		$.each(this.refreshCB, function(i, item) {
				item.cb.call(item.context);
			});
	}
	// --------------------------------------------------------
	// Get and initialize tasks
	// --------------------------------------------------------
	function getList(fl_async) {
		MIOGA.logDebug("ProjectTaskList", 1, "getList");
		var that = this;
		$.ajax({
			 url : mioga_context.private_bin_uri + "/Chronos/GetProjectTasks.json",
			 dataType : "json",
			 async : fl_async,
			 success : function(data) {
				MIOGA.logDebug("ProjectTaskList", 1, " getList success");
				that.tasks = [];
			 	if ($.isArray(data.projectTasks)) {
					$.each(data.projectTasks, function(i, item) {
						MIOGA.logDebug("ProjectTaskList", 3, " item ", item);
						var task = new ProjectTask(item);
						that.tasks.push(task);
					});
				}
				initIndex.call(that);
				that.callRefreshCB();
			 },
			 error: function(jqXHR, textStatus, errorThrown) {
				MIOGA.logError("Chronos.readConfiguration Error in web service  textStatus = " + textStatus, false);
			 }
		});
	}
	// --------------------------------------------------------
	// get event associated to project tasks
	// --------------------------------------------------------
	this.getEvents = getEvents;
	function getEvents() {
		MIOGA.logDebug("ProjectTaskList", 2, "getEvents ======");
		var that = this;
		var events = new Array();
		$.each(this.tasks, function(i, item) {
			if (item.show_event) {
				var evt = new Object();
				evt.title = item.project_label + ' : ' + item.label;
				evt.allDay = true;
				evt.start = item.dstart;
				evt.end = item.dend;
				evt.ev = item;
				evt.project = true;
				evt.textColor = item.color;
				evt.backgroundColor = item.bgcolor;
				events.push(evt);
			}
		});

		return events;
	}
	// ============================================================================
	// Private methods
	// ============================================================================
	function initIndex() {
		MIOGA.logDebug("ProjectTaskList", 2, 'initIndex');

		this.idx_by_rowid = {};
		for( var i = 0; i < this.tasks.length; i++) {
			MIOGA.logDebug("ProjectTaskList", 2, 'i = ' + i);
			this.idx_by_rowid[this.tasks.rowid] = i;
		}
	}
}
