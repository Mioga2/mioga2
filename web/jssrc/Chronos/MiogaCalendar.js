// =============================================================================
/**
Mioga Calendar object

@class MiogaCalendar
@constructor
**/
// =============================================================================


// MiogaCalendar
function MiogaCalendar(options) {
	"use strict";
	// Constants
	this.CONSTANT = {
		/**
		Calendar owner types
		1 : user calendar, 2 : group calendar, 3 : resource calendar
		@attribute CONSTANT.OWNER_TYPE
		@readOnly
		@type integer
		**/
		OWNER_TYPE : { USER  : 1, GROUP : 2, RESOURCE : 3 },
		/**
		Calendar types
		0 : user calendar, 1 : other calendar, 2 : group calendar
		@attribute CONSTANT.CAL_TYPE
		@readOnly
		@type integer
		**/
		CAL_TYPE : { USER  : 0, OTHER : 1, GROUP : 2 },
		/**
		Access type types
		1 : team access , 2 : user access
		@attribute CONSTANT.ACCESS_TYPE
		@readOnly
		@type integer
		**/
		ACCESS_TYPE : { TEAM : 1, USER : 2 },
		/**
		Action types types
		1 : add access , 2 : change access
		@attribute CONSTANT.ACCESS_ACTION_TYPE
		@readOnly
		@type integer
		**/
		ACCESS_ACTION_TYPE : { ADD : 1, CHANGE : 2 }
	}; // End of CONSTANT
	// Attributes
	this.ident      = options.ident;
	this.owner_type = options.owner_type;
	this.owner_id   = options.owner_id;

	this.type       = options.type; // One of CONSTANT.CAL_TYPE

	this.bgcolor    = options.bgcolor;
	this.color      = options.color;
	this.visible    = true;
	//TODO options.visible must be boolean => to be corrected
	if (options.visible === 0) this.visible   = false;

	this.rowid     = options.rowid;
	this.access    = options.access;

	MIOGA.debug.MiogaCalendar = 0;

	// For user calendars list of access right
	this.setAccessRights = setAccessRights;  // predeclare
	this.accessRightList = [];
	if ($.isPlainObject(options.access_rights)) {
		this.setAccessRights(options.access_rights);
	}
	// --------- End of constructor
	// --------------------------------------------------------
	// Initialize access rights
	// --------------------------------------------------------
	function setAccessRights(access_rights) {
		MIOGA.logDebug("MiogaCalendar", 1, "MiogaCalendar setAccessRights ==================");
		this.accessRightList = [];
		if ($.isArray(access_rights.teams)) {
			for (var i = 0; i < access_rights.teams.length; i++) {
				this.accessRightList.push({ type : this.CONSTANT.ACCESS_TYPE.TEAM,
											ident : access_rights.teams[i].ident,
											object_id : access_rights.teams[i].team_id,
											access : access_rights.teams[i].access,
											id : access_rights.teams[i].rowid
										});
			}
		}
		if ($.isArray(access_rights.users)) {
			for (var i = 0; i < access_rights.users.length; i++) {
				if (access_rights.users[i].user_id !== this.owner_id) {
					this.accessRightList.push({ type : this.CONSTANT.ACCESS_TYPE.USER,
												ident : access_rights.users[i].firstname + ' ' + access_rights.users[i].lastname,
												object_id : access_rights.users[i].user_id,
												access : access_rights.users[i].access,
												id : access_rights.users[i].rowid
											});
				}
			}
		}
		MIOGA.logDebug("MiogaCalendar", 1, ' accessRightList  ====> ');
	}
	// --------------------------------------------------------
	// Modify calendar
	// --------------------------------------------------------
	this.modify = modify;
	function modify(params) {
		MIOGA.logDebug("MiogaCalendar", 1, "MiogaCalendar modify ==================");
		var that = this;
		var error = "OK";
		// MIOGA.logDebug("MiogaCalendar", 1, "params");
		$.ajax({
			 url: mioga_context.private_bin_uri + "/Chronos/SetCalendar.json",
			 type: "POST",
			 async: false,
			 data: { "rowid" : that.rowid,
			         "ident" : params.ident || this.ident,
					 "color" : params.color || this.color,
					 "bgcolor" : params.bgcolor || this.bgcolor
			       },
			 dataType:"json",
			 success : function(data) {
						that.ident = params.ident;
						that.color = params.color;
						that.bgcolor = params.bgcolor;
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
				  		alert("SetCalendar AJAX error : " + textStatus);
				  }
		});
		return error;
	}
	// --------------------------------------------------------
	// setVisibility
	// --------------------------------------------------------
	this.setVisibility = setVisibility;
	function setVisibility(visible) {
		MIOGA.logDebug("MiogaCalendar", 1, "MiogaCalendar setVisibility ==================");
		var that = this;
		var error = "OK";
		$.ajax({
			 url: mioga_context.private_bin_uri + "/Chronos/SetVisibility.json",
			 type: "POST",
			 async: false,
			 data: { "calendar_id" : that.rowid,
					 "visible" : visible
			       },
			 dataType:"json",
			 success : function(data) {
						that.visible = visible;
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
				  		alert("SetVisibility AJAX error : " + textStatus);
				  }
		});
		return error;
	}
	// --------------------------------------------------------
	// addNewEvent create an event from params
	// --------------------------------------------------------
	this.addNewEvent = addNewEvent;
	function addNewEvent(params) {
		MIOGA.logDebug("MiogaCalendar", 1, "MiogaCalendar addNewEvent ==================");
		var calEvent = params.calEvent;
		var that = this;
		var data = calEvent.stringify();
		// MIOGA.logDebug("MiogaCalendar", 1, "data = " + data);
		$.ajax({
			 url: mioga_context.private_bin_uri + "/Chronos/CreateEvent.json",
			 type: "POST",
			 data: {"event": data, "calendar_id":that.rowid },
			 dataType:"json",
			 // TODO check errors from web service
			 success : function(data) {
						if (typeof params.success === 'function') {
							params.success(data.status);
						}
			 },
			 error: function(jqXHR, textStatus, errorThrown) {
			  		alert("CreateEvent AJAX error : " + textStatus);
					if (params.returnCB) {
						params.returnCB('KO');
					}
			  }
		});
	}
	// --------------------------------------------------------
	// modifyEvent modify event parameters from params
	// --------------------------------------------------------
	this.modifyEvent = modifyEvent;
	function modifyEvent(params) {
		MIOGA.logDebug("MiogaCalendar", 1, "MiogaCalendar modifyEvent ==================");
		var calEvent = params.calEvent;
		var that = this;
		// Case of event dropping from allday position
		if (calEvent.end === null) {
			calEvent.end = new Date(calEvent.start);
			calEvent.end.setHours(calEvent.end.getHours() + 1);
		}
		// Do update event
		var data = calEvent.stringify();
		$.ajax({
			 url: mioga_context.private_bin_uri + "/Chronos/UpdateEvent.json",
			 type: "POST",
			 data: {"event":data, "rowid":calEvent.rowid, "calendar_id":that.rowid },
			 dataType:"json",
			 success : function(data) {
						if (typeof params.success === 'function') {
							params.success(data.status);
						}
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
				  		alert("UpdateEvent AJAX error : " + textStatus);
						if (params.error) {
							params.error('KO');
						}
				  }
			});
	}
	// --------------------------------------------------------
	// suppressEvent suppress the given event from calendar
	// --------------------------------------------------------
	this.suppressEvent = suppressEvent;
	function suppressEvent(calEvent) {
		MIOGA.logDebug("MiogaCalendar", 1, "MiogaCalendar suppressEvent ==================");
		var that = this;
		// TODO : must call Mioga to modify event
		$.ajax({
			 url: mioga_context.private_bin_uri + "/Chronos/SuppressEvent.json",
			 type: "POST",
			 data: { 
					 "rowid":calEvent.rowid
			       },
			 dataType:"json",
			 success : function(data) {
						if (typeof returnCB === 'function') {
							returnCB(data.status);
						}
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
				  		alert("SuppressEvent AJAX error : " + textStatus);
						if (params.returnCB) {
							params.returnCB('KO');
						}
				  }
			});
	}
	// --------------------------------------------------------
	// suppressOccurence suppress the given event from calendar
	// --------------------------------------------------------
	this.suppressOccurence = suppressOccurence;
	function suppressOccurence(calEvent) {
		MIOGA.logDebug("MiogaCalendar", 1, "suppressOccurence ==================");
		var that = this;
		// TODO : must call Mioga to modify event
		$.ajax({
			 url: mioga_context.private_bin_uri + "/Chronos/SuppressOccurence.json",
			 type: "POST",
			 data: { 
					 "rowid":calEvent.rowid
			       },
			 dataType:"json",
			 success : function(data) {
						if (typeof returnCB === 'function') {
							returnCB(data.status);
						}
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
				  		alert("SuppressEvent AJAX error : " + textStatus);
						if (params.returnCB) {
							params.returnCB('KO');
						}
				  }
			});
	}
	// --------------------------------------------------------
	// getEvents
	// --------------------------------------------------------
	this.getEvents = getEvents;
	//function getEvents(start, end, colorscheme, callback) {
	function getEvents(start, end, callback) {
		MIOGA.logDebug("MiogaCalendar", 1, "MiogaCalendar getEvents("+this.ident+") ==== start = " + start + "  end = " + end);
		var that = this;
		// Get events from source
		$.ajax({
			url: mioga_context.private_bin_uri + "/Chronos/GetEvents.json",
			type: "POST",
			async:"false",
			data: { "start_year":start.getUTCFullYear(),
			 		 "start_month":start.getUTCMonth() + 1,
			 		 "start_day":start.getUTCDate(),
			         "end_year":end.getUTCFullYear(),
			 		 "end_month":end.getUTCMonth() + 1,
			 		 "end_day":end.getUTCDate(),
					 "calendar_list":'['+ that.rowid + ']'
			       },
			dataType:"json",
			success : function(data) {
				// MIOGA.logDebug("MiogaCalendar", 1, "getEvents for calendar "+this.ident+" success function");
				var events = new Array();
				if (data.tasks.length > 0) {
					$.each(data.tasks, function(num, item) {
						MIOGA.logDebug("MiogaCalendar", 3, "getEvents item", item);
						var subject = item.subject + "" || "";
						var desc = item.description + "" || "";
						var allday = false;
						if (item.allday === 1) {
							allday = true;
						}
						var start = new Date();
						start.parseMiogaDate(item.dstart, allday);
						var end = new Date();
						end.parseMiogaDate(item.dend, allday);

						// Handle DST changes in recurrent events:
						// 		- events created in summer time and displayed in winter time need to be moved 1 hour later
						// 		- events created in winter time and displayed in summer time need to be moved 1 hour earlier
						if (item.base_start !== undefined) {
							var base_start = new Date ();
							base_start.parseMiogaDate (item.base_start, allday);
							var start_offset = start.getTimezoneOffset () - base_start.getTimezoneOffset ();
							var base_end = new Date ();
							base_end.parseMiogaDate (item.base_end, allday);
							var end_offset = end.getTimezoneOffset () - base_end.getTimezoneOffset ();
							MIOGA.logDebug ('MiogaCalendar', 2, "Offset for event is " + start_offset + ' / ' + end_offset);
							if (start_offset || end_offset) {
								start.setMinutes (start.getMinutes () + start_offset);
								end.setMinutes (end.getMinutes () + end_offset);
							}
						}

						var evdesc = { "title" : subject,
									  "desc" : desc,
									  "start" : start,
									  "end"   : end,
									  "allDay" : allday,
									  "category_id" : item.category_id,
									  "rowid" : item.rowid,
									  "fl_rrule": false,
									  "calendar" : that
									};
						if (parseInt(item.fl_recur, 10) === 1) {
							evdesc.fl_rrule = true;
							var base_start = new Date();
							base_start.parseMiogaDate(item.base_start, allday);
							evdesc.base_start = base_start;
							var base_end = new Date();
							base_end.parseMiogaDate(item.base_end, allday);
							evdesc.base_end = base_end;
							evdesc.rrule = $.parseJSON(item.rrule);
						}
						var ev = new CalendarEvent(evdesc);
						MIOGA.logDebug("MiogaCalendar", 2, "getEvents ev", ev);
						events.push(ev);
					});
				}
				// MIOGA.logDebug("MiogaCalendar", 1, 'calendar ' + that.ident + ' got ' + events.length + ' events. Calling callback ...');
				if (callback) {
					//console.debug('callback events' + cb);
					callback(null, events);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert("GetEvents AJAX error : " + textStatus);
				if (callback) {
					//console.debug('callback events' + cb);
					callback("GetEvents AJAX error : " + textStatus, null);
				}
			}
		});
	}
	// ----------------------------------------------------------------------------
	/**
	Read user list with no rights for calendar. Returns only users invited in current user's groups

	@method getUserList
	@param {Function} callback to call with array of users
	**/
	// ----------------------------------------------------------------------------
	this.getUserList = getUserList;
	function getUserList(cb) {
		MIOGA.logDebug("MiogaCalendar", 1, "MiogaCalendar getUserList for calendar : " + this.ident);
		var that = this;
		$.ajax({
			url: mioga_context.private_bin_uri + "/Chronos/GetUserList.json",
			type: "GET",
			data: { calendar_id : that.rowid },
			dataType:"json",
			success : function(data) {
							MIOGA.logDebug("MiogaCalendar", 1, 'data = ');
							if (data.status === "OK") {
								if ($.isFunction(cb)) {
									cb(data.users);
								}
							} else {
								MIOGA.logError("MiogaCalendar.getUserList Error in web service", false);
							}
						},
			error: function(jqXHR, textStatus, errorThrown) {
				MIOGA.logError("MiogaCalendar.getUserList Error in web service  textStatus = " + textStatus, false);
			}
		});
	}
	// ----------------------------------------------------------------------------
	/**
	Read team list with no rights for calendar

	@method getTeamList
	@param {Function} callback to call with array of teams
	**/
	// ----------------------------------------------------------------------------
	this.getTeamList = getTeamList;
	function getTeamList(cb) {
		MIOGA.logDebug("MiogaCalendar", 1, "MiogaCalendar getTeamList for calendar : " + this.ident);
		var that = this;
		$.ajax({
			url: mioga_context.private_bin_uri + "/Chronos/GetTeamList.json",
			type: "GET",
			data: { calendar_id : that.rowid },
			dataType:"json",
			success : function(data) {
							MIOGA.logDebug("MiogaCalendar", 1, 'data = ');
							if (data.status === "OK") {
								if ($.isFunction(cb)) {
									cb(data.teams);
								}
							} else {
								MIOGA.logError("MiogaCalendar.getTeamList Error in web service", false);
							}
						},
			error: function(jqXHR, textStatus, errorThrown) {
				MIOGA.logError("MiogaCalendar.getTeamList Error in web service  textStatus = " + textStatus, false);
			}
		});
	}
	// ----------------------------------------------------------------------------
	/**
	Search for user in Mioga Database. Call callback with user informations or "KO"

	@method searchUserFromMail
	@param {String} email to search
	@param {Function} callback to call with response
	**/
	// ----------------------------------------------------------------------------
	this.searchUserFromMail = searchUserFromMail;
	function searchUserFromMail(email, cb) {
		MIOGA.logDebug("MiogaCalendar", 1, "MiogaCalendar searchUserFromMail for calendar : " + this.ident);
		var that = this;
		$.ajax({
			url: mioga_context.private_bin_uri + "/Chronos/SearchUserFromMail.json",
			type: "GET",
			data: { email: email },
			dataType:"json",
			success : function(data) {
							MIOGA.logDebug("MiogaCalendar", 1, "search email success");
							if (data.status === "OK") {
								if ($.isFunction(cb)) {
									cb(data.user);
								}
							} else {
								MIOGA.logError("MiogaCalendar.searchUserFromMail Error in web service", false);
							}
						},
			error: function(jqXHR, textStatus, errorThrown) {
				MIOGA.logError("MiogaCalendar.searchUserFromMail Error in web service  textStatus = " + textStatus, false);
			}
		});

	}
	// ----------------------------------------------------------------------------
	/**
	Add or change an access right for the given object

	@method changeAccessForObject
	@param {Integer} rowid for user
	@param {Integer} object_type (see CONSTANT.ACCESS_TYPE)
	@param {Integer} action (see CONSTANT.ACCESS_ACTION_TYPE)
	@param {Integer} access (1 : read, 2 : read/write)
	@param {Function} callback to call with response
	**/
	// ----------------------------------------------------------------------------
	this.changeAccessForObject = changeAccessForObject;
	function changeAccessForObject(rowid, object_type, action, access, async, cb) {
		MIOGA.logDebug("MiogaCalendar", 1, "MiogaCalendar changeAccessForObject for calendar : " + this.ident);
		var that = this;
		$.ajax({
			url: mioga_context.private_bin_uri + "/Chronos/ChangeAccessToCalendar.json",
			type: "POST",
			data: { object_type : object_type,
			        calendar_id : that.rowid,
					object_id : rowid,
					access : access,
					action : action  },
			dataType:"json",
			async : async,
			success : function(data) {
							MIOGA.logDebug("MiogaCalendar", 1, "data = ");
							if (data.status === "OK") {
								that.setAccessRights(data.access_list);
								if ($.isFunction(cb)) {
									cb(data.status);
								}
							} else {
								MIOGA.logError("MiogaCalendar.changeAccessForObject Error in web service", false);
							}
						},
			error: function(jqXHR, textStatus, errorThrown) {
				MIOGA.logError("MiogaCalendar.changeAccessForObject Error in web service  textStatus = " + textStatus, false);
			}
		});

	}
	// ----------------------------------------------------------------------------
	/**
	Suppress an access right for the given access id

	@method suppressAccessRight
	@param {Integer} id for access
	@param {Function} callback to call with response
	**/
	// ----------------------------------------------------------------------------
	this.suppressAccessRight = suppressAccessRight;
	function suppressAccessRight(id, cb) {
		MIOGA.logDebug("MiogaCalendar", 1, "MiogaCalendar suppressAccesRight for calendar : " + this.ident);
		var that = this;
		//TODO find object
		var access_found = undefined;
		for (var i = 0; i < this.accessRightList.length; i++) {
			if (this.accessRightList[i].id === parseInt(id, 10)) {
				MIOGA.logDebug("MiogaCalendar", 1, " found access right i = " + i);
				access_found = this.accessRightList[i];
			}
		}
		if (access_found === undefined) {
			MIOGA.logError("MiogaCalendar.suppressAccessRight cannot found access with id", false);
		} else {
			$.ajax({
				url: mioga_context.private_bin_uri + "/Chronos/SuppressAccessToCalendar.json",
				type: "POST",
				data: { object_type : access_found.type,
						object_id : access_found.object_id,
						access_id : id,
						calendar_id : that.rowid },
				dataType:"json",
				success : function(data) {
								MIOGA.logDebug("MiogaCalendar", 1, "data = ");
								if (data.status === "OK") {
									that.setAccessRights(data.access_list);
									if ($.isFunction(cb)) {
										cb(data.status);
									}
								} else {
									MIOGA.logError("MiogaCalendar.suppressAccessForUser Error in web service", false);
								}
							},
				error: function(jqXHR, textStatus, errorThrown) {
					MIOGA.logError("MiogaCalendar.suppressAccessForUser Error in web service  textStatus = " + textStatus, false);
				}
			});
		}
	}
}	// =========================================================
