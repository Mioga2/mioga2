// =============================================================================
/**
This module permits to modify Chronos configuration function of mode (user or group). It work on the Chronos object.

    var options = {
        i18n : { ... }
    };

@class ChronosConfigEditor
@constructor
@param {Object} elem JQuery element parent of progress bar
@param {Object} options
**/
// =============================================================================
function ChronosConfigEditor(elem, options) {

	this.elem           = elem;
	this.i18n           = options.i18n;
	this.CL             = options.calendarList;
	this.config         = options.config;
	this.mode           = this.config.mode;
	this.configChangeCB = options.configChangeCB;

	MIOGA.debug.ChronosConfigEditor = 0;

	MIOGA.logDebug("ChronosConfigEditor", 1, "ChronosConfigEditor constructor begin >>>>>>>>>>>>>>>> ");

	this.refresh = refresh;
	// ----------------------------------------------------------------
	// Create interface
	// ----------------------------------------------------------------
	var that = this; // For callback reference to current object
	var i18n = this.i18n;
	// main window
	this.$cont = $('<div class="chronos-config"></div>').appendTo(this.elem);

	// navigation bar
	var $nav = $('<div class="app-nav"></div>').appendTo(this.$cont);
	$('<button class="button cancel">'+i18n.returnToMain+'</button>')
	           .appendTo($nav)
			   .click(function(event) {
			   				MIOGA.logDebug("ChronosConfigEditor", 1, 'ChronosConfigEditor main-return event');
							history.back();
	           });

	// -----------------------------------------------------------
	// Calendar lists if in user mode
	// -----------------------------------------------------------
	if (this.mode === 'user') {
		this.$clist = $('<div class="sbox"></div>').appendTo(this.$cont);
		$('<h2 class="title_bg_color">'+i18n.calendarListTitle+'</h2>').appendTo(this.$clist);
		// user calendar list
		this.$ownerblock = $('<div class="cal-block"></div>').appendTo(this.$clist);
		$('<h2>'+i18n.ownerCalendars+'</h2>').appendTo(this.$ownerblock);
		this.$ownerList = $('<ul class="cal-list"></ul>').appendTo(this.$ownerblock);
		var $add = $('<div class="add-new-cont"></div>').appendTo(this.$ownerblock);
		$('<span class="add-new">'+i18n.addNewCalendar+'</span>')
						.appendTo($add)
						.click(function(event) {
									MIOGA.logDebug("ChronosConfigEditor", 1, 'Must create a calendar');
									var ident = $add.find('input[name="ident"]').prop('value');
									$.trim(ident);
									if (that.CL.isValidIdent(ident)) {
										that.CL.addOwnerCalendar( { "ident" : ident, "color" : "#000000", bgcolor : "#ffffff"} );
										$add.find('input[name="ident"]').prop('value', "").removeClass("error");
									}
									else {
										$add.find('input[name="ident"]').addClass('error');
									}
							});
		$('<input type="text" name="ident" value="" />').appendTo($add);

		// other calendar list
		this.$otherblock = $('<div class="cal-block"></div>').appendTo(this.$clist);
		$('<h2>'+i18n.otherCalendars+'</h2>').appendTo(this.$otherblock);
		this.$otherList = $('<ul class="cal-list"></ul>').appendTo(this.$otherblock);
		this.$addOther = $('<div class="add-new-cont"></div>').appendTo(this.$otherblock);

		$('<span class="add-new">'+i18n.addNewCalendar+'</span>')
							.appendTo(this.$addOther)
							.click(function(event) {
									MIOGA.logDebug("ChronosConfigEditor", 1, 'Subscribe to a new calendar');
									var val = that.$selOther.attr('value');
									MIOGA.logDebug("ChronosConfigEditor", 1, 'val =  ' + val);
									that.CL.connectToOtherCalendar(parseInt(val, 10));
							});
		this.$selOther = $('<select name="other"></select>').appendTo(this.$addOther);

		// group calendar list
		this.$groupblock = $('<div class="cal-block"></div>').appendTo(this.$clist);
		$('<h2>'+i18n.groupCalendars+'</h2>').appendTo(this.$groupblock);
		this.$groupList = $('<ul class="cal-list"></ul>').appendTo(this.$groupblock);
		this.$addGroup = $('<div class="add-new-cont"></div>').appendTo(this.$groupblock);
		$('<span class="add-new">'+i18n.addNewCalendar+'</span>')
							.appendTo(this.$addGroup)
							.click(function(event) {
									MIOGA.logDebug("ChronosConfigEditor", 1, 'Subscribe to a new calendar');
									var val = that.$selGroup.prop('value');
									MIOGA.logDebug("ChronosConfigEditor", 1, 'val =  ' + val);
									that.CL.connectToGroupCalendar(parseInt(val, 10));
							});
		this.$selGroup = $('<select name="group"></select>').appendTo(this.$addGroup);

		// create calendar editor
		this.caledit = new CalendarEditor(this.$cont, options);
		// create access right editor
		this.accessedit = new CalAccessRightEditor(this.$cont, options);
		// fill in lists
		this.CL.addRefreshCB(this.refresh, this);
		this.refresh();
	}
	// -----------------------------------------------------------
	// Other parameters
	// -----------------------------------------------------------
	this.$oparams = $('<div class="sbox oparams"></div>').appendTo(this.$cont);
	$('<h2 class="title_bg_color">'+i18n.otherParamsTitle+'</h2>').appendTo(this.$oparams);

	this.$oform = $('<form class="form"></form>').appendTo(this.$oparams);
	this.$oform.submit(function() {
						MIOGA.logDebug("ChronosConfigEditor", 1, "oform submit");
						return false;
					});

	// Calendar colors if in group or resource mode
	if (this.mode !== 'user') {
		this.bgcolor_id = MIOGA.generateID("chronos");
		this.fgcolor_id = MIOGA.generateID("chronos");
		color_label = this.i18n.groupColor;
		bgcolor_label = this.i18n.groupBGColor;
		$('<div class="form-item"><label>'+color_label+'</label><input id="'+this.fgcolor_id+'" type="text" name="fgcolor" class="color-picker" size="7"/></div>').appendTo(this.$oform);
		$('<div class="form-item"><label>'+bgcolor_label+'</label><input id="'+this.bgcolor_id+'" type="text" name="bgcolor" class="color-picker" size="7"/></div>').appendTo(this.$oform);
		$('.color-picker').miniColors();
	}
	// first hour displayed
	this.firsthour_id = MIOGA.generateID("chronos");
	$('<div class="form-item"><label>'+this.i18n.firstHour+'</label><input id="'+this.firsthour_id+'" type="text" name="firsthour" /></div>').appendTo(this.$oform);
	$('#'+this.firsthour_id).timepicker({
											hourText : that.i18n.hourText,
											minuteText : that.i18n.minuteText,
											showMinutes: false
	                                  });
	// Show week ends
	$('<div class="form-item"><label for="'+this.showweekend_id+'">'+this.i18n.showWeekend+'</label><input id="'+this.showweekend_id+'" type="checkbox" name="showweekend" /></div>').appendTo(this.$oform);
	// color Scheme
	this.colorscheme_id = MIOGA.generateID("chronos");
	$('<div class="form-item"><label>'+this.i18n.colorscheme+'</label><select id="'+this.colorscheme_id+'" class="color-mode" name="colorscheme" >'
											+ '<option value="1">'+this.i18n.colorModeActivity+'</option>'
											+ '<option value="2">'+this.i18n.colorModeCalendar+'</option>'
											+ '</select></div>').appendTo(this.$oform);
	// validate button
	this.applybtn_id = MIOGA.generateID("chronos");
	$('<div class="form-buttons"><button id="'+this.applybtn_id+'" class="button">'+i18n.applyLabel+'</button></div>').appendTo(this.$oform);

	//Validate method
	$('#'+this.applybtn_id).click(function(event) {
				MIOGA.logDebug("ChronosConfigEditor", 1, 'ChronosConfigEditor validate other params');
				event.preventDefault();
				// TODO check parameters values
				var fgcolor = $('#'+that.fgcolor_id).val();
				var bgcolor = $('#'+that.bgcolor_id).val();
				var params = {};
				if (that.mode !== 'user') {
					// In group mode, only one calendar
					that.CL.calendars[0].modify({color : fgcolor, bgcolor:bgcolor });
				}
				if ($('#'+that.showweekend_id).is(':checked')) {
					params.showweekend = true;
				}
				else {
					params.showweekend = false;
				}
				params.firsthour = $('#'+that.firsthour_id).timepicker('getHour'); 
				params.colorscheme = $('#'+that.colorscheme_id).val(); 
				that.configChangeCB('setParams', params);
	});

	MIOGA.logDebug("ChronosConfigEditor", 1, "ChronosConfigEditor constructor end <<<<<<<<<<<<<<<< ");
	// == end constructor ===========================================
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	// --------------------------------------------------------
	// showWindow display and init window
	// --------------------------------------------------------
	this.showWindow = showWindow;
	function showWindow() {
		MIOGA.logDebug("ChronosConfigEditor", 1, 'ChronosConfigEditor  showWindow ===');
		if (this.mode === 'user') {
			this.refresh();
		}
		this.initOtherParams();
		this.$cont.show();
	}
	// --------------------------------------------------------
	// hideWindow hides the window
	// --------------------------------------------------------
	this.hideWindow = hideWindow;
	function hideWindow() {
		MIOGA.logDebug("ChronosConfigEditor", 1, 'ChronosConfigEditor  hideWindow ===');
		this.$cont.hide();
	}
	// --------------------------------------------------------
	// initOtherParams intialize fields od other params form
	// --------------------------------------------------------
	this.initOtherParams = initOtherParams;
	function initOtherParams() {
		MIOGA.logDebug("ChronosConfigEditor", 1, 'ChronosConfigEditor  initOtherParams ===');

		if (this.mode !== 'user') {
			$('#'+this.fgcolor_id).miniColors("value", this.CL.calendars[0].color);
			$('#'+this.bgcolor_id).miniColors("value", this.CL.calendars[0].bgcolor);
		}
		$('#'+this.showweekend_id).prop('checked', this.config.showweekend);
		$('#'+this.firsthour_id).timepicker('setTime', this.config.firsthour + ':00');
		$('#'+this.colorscheme_id).val(this.config.colorscheme);
	}
	// -------------------------------------------------------------
	// refresh function
	// redraw list content function of calendarList
	// -------------------------------------------------------------
	function refresh() {
		MIOGA.logDebug("ChronosConfigEditor", 1, ' ============= ChronosConfigEditor refresh');
		var that = this;
		this.$ownerList.find('.cal-list-item').remove();
		this.$groupList.find('.cal-list-item').remove();
		this.$otherList.find('.cal-list-item').remove();
		this.$selGroup.find('option').remove();
		this.$selOther.find('option').remove();
		var other_count = 0;
		var group_count = 0;

		$.each(this.CL.calendars, function(num, item) {
			if (item.type === item.CONSTANT.CAL_TYPE.USER) {
				drawElement.call(that, that.$ownerList, item);
			}
			else if (item.type === item.CONSTANT.CAL_TYPE.GROUP) {
				drawElement.call(that, that.$groupList, item);
				group_count++;
			}
			else if (item.type === item.CONSTANT.CAL_TYPE.OTHER) {
				drawElement.call(that, that.$otherList, item);
				other_count++;
			}
			else {
				alert('ChronosConfigEditor::refresh() Bad calendar type : ' + item.type);
			}
		});
		// show or hide other block
		if ((other_count > 0) || (this.CL.authorizedOthers.length > 0)) {
			this.$otherblock.show();
		}
		else  {
			this.$otherblock.hide();
		}
		// show or hide groups block
		if ((group_count > 0) || (this.CL.authorizedGroups.length > 0)) {
			this.$groupblock.show();
		}
		else  {
			this.$groupblock.hide();
		}

		// show or hide list of groups
		if (this.CL.authorizedGroups.length === 0) {
			this.$addGroup.hide();
		}
		else {
			this.$addGroup.show();
			$.each(this.CL.authorizedGroups, function(num, item) {
				that.$selGroup.append('<option value="'+item.rowid+'">'+item.ident+'</option>');
			});
		}
		// show or hide list of other calendars
		if (this.CL.authorizedOthers.length === 0) {
			this.$addOther.hide();
		}
		else {
			this.$addOther.show();
			$.each(this.CL.authorizedOthers, function(num, item) {
				that.$selOther.append('<option value="'+item.rowid+'">'+item.ident+'</option>');
			});
		}
	}
	// ===============================================================
	// Private methods
	// ===============================================================
	// -------------------------------------------------------------
	// drawElement
	//    draw an entry in calendar list
	// -------------------------------------------------------------
	function drawElement($list, item) {
		MIOGA.logDebug("ChronosConfigEditor", 1, 'draw element');

		var access_span = "";
		// For group and other calendars, show access rights status
		if (item.type !== item.CONSTANT.CAL_TYPE.USER) {
			if (item.access === 2) {
				access_span = '<span class="read-write">'+this.i18n.readWriteLabel+'</span>';
			} else {
				access_span = '<span class="read-only">'+this.i18n.readOnlyLabel+'</span>';
			}
		}
		var $el = $('<li class="cal-list-item"></li>').appendTo($list);
		var $el_content = $('<div class="calendar-item">'
				+ '<span class="cal-suppress">'+this.i18n.suppressCalendar+'</span>'
				+ '<span class="cal-ident" style="background-color:'+item.bgcolor+';color:'+item.color+';" title="'+item.ident+'" >'+item.ident+'</span>'
				+ access_span
				+ '<span class="cal-export"><a class="export-ics" href="GetICSCalendar?calendar_id='+item.rowid+'" title="'+this.i18n.exportICS+'" >'+this.i18n.exportICS+'</a></span>'
				+ '</div>').appendTo($el);

		// For owner calendar's show access rights granted to other
		if (item.type === item.CONSTANT.CAL_TYPE.USER) {
			MIOGA.logDebug("ChronosConfigEditor", 1, "owner calendar add right access");
			drawAccessRights.call(that, $el, item);
		}

		/*
		$el_content.hover(function(event) {
							$el.find('.cal-suppress').css("visibility", "visible");
						},
					function(event) {
							$el.find('.cal-suppress').css("visibility", "hidden");
						});
		*/
		$el.find('.cal-suppress').click(function(event) {
									MIOGA.logDebug("ChronosConfigEditor", 1, 'suppress calendar ' + item.ident + '  type = ' + item.type);
									if (item.type === item.CONSTANT.CAL_TYPE.USER) {
										if (confirm(that.i18n.confirmSuppressUserCal)) {
											that.CL.suppressCalendar(item.rowid);
										}
									} else {
										if (confirm(that.i18n.confirmDetachCal)) {
											that.CL.detachFromCalendar(item.rowid);
										}
									}
									event.stopPropagation();
								});
		$el_content.click(function(event) {
						MIOGA.logDebug("ChronosConfigEditor", 1, 'edit calendar ' + item.ident);
						that.caledit.edit(item);
					});
	}
	// -------------------------------------------------------------
	// drawAccesRights
	//    draw the access right list for user calendars
	// -------------------------------------------------------------
	function drawAccessRights($el, calendar) {
		MIOGA.logDebug("ChronosConfigEditor", 1, "drawAccesRights ");
		var listId = MIOGA.generateID("chronos");
		var btnId = MIOGA.generateID("chronos");
		var suppressId = MIOGA.generateID("chronos");
		var rightId = MIOGA.generateID("chronos");
		$el.find('div.calendar-access-rights').detach();
		var $el_rightaccess = $('<div class="calendar-access-rights">'
									+ '<div class="access-label">'
										+ '<button id="'+btnId+'" >'+this.i18n.addAccessRight+'</button>'
									+ '</div>'
									+ '<ul id="'+listId+'" class="access-right-list"></ul>'
								+ '</div>').appendTo($el);

		$('#' + btnId).click(function(event) {
					MIOGA.logDebug("ChronosConfigEditor", 1, 'add access right for calendar ' + calendar.ident);
					that.accessedit.edit(calendar, function() {
											MIOGA.logDebug("ChronosConfigEditor", 1, ' CB to refresh access ');
											drawAccessRights.call(that, $el, calendar);
										});
				});
		$.each(calendar.accessRightList, function(num, item) {
				var html;
				var $ar = $('<li></li>').appendTo('#' + listId);

				html = '<span class="access-suppress" title="'+that.i18n.suppressAccess+'" >'+that.i18n.suppressAccess+'</span>';
				$(html).appendTo($ar)
					   .click(function(event) {
							MIOGA.logDebug("ChronosConfigEditor", 1, 'suppress access ' + item.id);
							calendar.suppressAccessRight(item.id, function(status) {
																		MIOGA.logDebug("ChronosConfigEditor", 1, ' suppress access status for item ' + item.id + ' status ' + status);
																		drawAccessRights.call(that, $el, calendar);
																	});
							return false;
						});
				// access right icon
				if (item.access === 1) {
					html = '<span class="read-only" title="'+that.i18n.readOnlyLabel+'" >'+that.i18n.readOnlyLabel+'</span>';
				} else {
					html = '<span class="read-write" title="'+that.i18n.readWriteLabel+'" >'+that.i18n.readWriteLabel+'</span>';
				}
				$(html).appendTo($ar)
					   .click(function(event) {
							MIOGA.logDebug("ChronosConfigEditor", 1, 'change access right ' + item.id);
							var access;
							if (item.access === 2) {
								access = 1;
							} else {
								access = 2;
							}
							calendar.changeAccessForObject(item.object_id,
														   item.type,
														   calendar.CONSTANT.ACCESS_ACTION_TYPE.CHANGE,
														   access,
														   false,
														   function() {
																		drawAccessRights.call(that, $el, calendar);
																	}
														);
							return false;
						});
				// access granted to type
				if (item.type === calendar.CONSTANT.ACCESS_TYPE.USER) {
					html = '<span class="user-access" title="'+that.i18n.userAccessLabel+'" >'+that.i18n.userAccessLabel+'</span>';
				} else {
					html = '<span class="team-access" title="'+that.i18n.teamAccessLabel+'" >'+that.i18n.teamAccessLabel+'</span>';
				}
				$(html).appendTo($ar);
				// comment
				html = '<span class="access-comment">'+item.ident+'</span>';
				$(html).appendTo($ar);

				$ar.hover(function(event) {
							$ar.find('.access-suppress').css("visibility", "visible");
						},
					function(event) {
							$ar.find('.access-suppress').css("visibility", "hidden");
						});
		});
	}

} // End of ChronosConfigEditor

