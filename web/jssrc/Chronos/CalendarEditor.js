// =============================================================================
/**
This module display the modify calendar form.

@class CalendarEditor
@constructor
**/
// =============================================================================
function CalendarEditor(elem, options) {

	this.elem     = elem;
	this.i18n     = options.i18n;
	this.returnCB = options.returnCB;
	this.CL       = options.calendarList;

	MIOGA.debug.CalendarEditor = 0;

	this.$dialog = $('<div class="chronos-dialog"></div>').appendTo(elem);
	var idForm = MIOGA.generateID("chronos");
	var idOK = MIOGA.generateID("chronos");
	var idCancel = MIOGA.generateID("chronos");

	var that = this;
	this.$dialog.append('<form id="'+idForm+'" class="form">'
								+'<input type="hidden" name="rowid" value="" />'
								+'<input type="hidden" name="cal_num" value="" />'
								+'<fieldset>'
								+'<div class="form-item">'
									+'<label>'+this.i18n.calName+'</label><input type="text" name="ident" />'
								+'</div>'
								+'<div class="form-item">'
									+'<label>'+this.i18n.calColor+'</label><input type="text" name="color" class="color-picker" size="7" />'
								+'</div>'
								+'<div class="form-item">'
									+'<label>'+this.i18n.calBGColor+'</label><input type="text" name="bgcolor" class="color-picker" size="7" />'
								+'</div>'
								+'</fieldset>'
				        		+'<p><button id="'+idOK+'" >'+this.i18n.OKLabel+'</button>'
								+'<button id="'+idCancel+'" >'+this.i18n.cancelLabel+'</button></p>'
								+'</form>');

	$('.color-picker').miniColors();
	this.$dialog.dialog({ title: this.i18n.editCalendarTitle,
			                            autoOpen:false,
										modal:true,
										width:'25em',
										closeOnEscape:true
									});
	$('#'+idForm).submit(function(event) {
								MIOGA.logDebug('CalendarEditor', 1, 'submit');
								return false;
							});
	$('#'+idCancel).click(function(event) {
								MIOGA.logDebug('CalendarEditor', 1, 'cancel');
								that.$dialog.dialog('close');
								return false;
							});
	$('#'+idOK).click(function(event) {
								MIOGA.logDebug('CalendarEditor', 1, 'OK');
								that.validateCalendar();
								return false;
							});
	// ----------------------------------------------------------------------------
	/**
	Launch edit dialog

	@method edit
	**/
	// ----------------------------------------------------------------------------
	this.edit = edit;
	function edit(item) {
		this.$dialog.find('input[name="ident"]').prop('value', item.ident);
		var $color = this.$dialog.find('input[name="color"]');
		$color.miniColors('value', item.color);
		var $bgcolor = this.$dialog.find('input[name="bgcolor"]')
		$bgcolor.miniColors('value', item.bgcolor);
		this.$dialog.data('calendar', item);
		this.$dialog.dialog('open');
	}
	// ---------------------------------------------------------------------------
	// lauch edit dialog
	// ---------------------------------------------------------------------------
	this.validateCalendar = validateCalendar;
	function validateCalendar() {
		MIOGA.logDebug('CalendarEditor', 1, ' validateCalendar');
		//TODO check values, call WS and refresh calendar list
		var params = { 
						ident : this.$dialog.find('input[name="ident"]').prop('value'),
						color : this.$dialog.find('input[name="color"]').prop('value'),
						bgcolor : this.$dialog.find('input[name="bgcolor"]').prop('value')
					};
		MIOGA.logDebug('CalendarEditor', 1, "params");
		var calendar = this.$dialog.data('calendar');
		MIOGA.logDebug('CalendarEditor', 1, "calendar");
		var error = calendar.modify(params);
		MIOGA.logDebug('CalendarEditor', 1, "error");

		this.CL.callRefreshCB();
		this.$dialog.dialog('close');
	}


} // End of CalendarEditor

