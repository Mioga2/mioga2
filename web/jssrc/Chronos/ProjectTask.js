// =============================================================================
/**
ProjectTask object.

    var options = {
        i18n : { .... }
    };

@class ProjectTask
@constructor
**/
// =============================================================================

function ProjectTask (args) {
	"use strict";
	MIOGA.debug.ProjectTask = 0;
	var that = this;
	// ---------------------------------------------------------------------
	// Attributes
	// ---------------------------------------------------------------------
	/**
	Label of task

	@attribute label
	@type String
	**/
	this.label = undefined;
	/**
	Description od task

	@attribute description
	@type String
	**/
	this.description = undefined;
	/**
	DB identifier for task

	@attribute rowid
	@type integer
	**/
	this.rowid = undefined;
	/**
	DB identifier for project

	@attribute project_id
	@type integer
	**/
	this.project_id = undefined;
	/**
	Name of project

	@attribute project_label
	@type string
	**/
	this.project_label = undefined;
	/**
	DB identifier of user the task is delegated to

	@attribute user_id
	@type integer
	**/
	this.user_id = undefined;
	/**
	DB identifier for group of project

	@attribute group_id
	@type integer
	**/
	this.group_id = undefined;
	/**
	Ident of group of project

	@attribute group_ident
	@type string
	**/
	this.group_ident = undefined;
	/**
	Current progress percent for the task

	@attribute current_progress
	@type real
	**/
	this.current_progress = undefined;
	/**
	Workload for task in hour

	@attribute workload
	@type integer
	**/
	this.workload = undefined;
	/**
	Planned beginning date

	@attribute pstart
	@type Date
	**/
	this.pstart = null;
	/**
	Planned ending date

	@attribute pend
	@type Date
	**/
	this.pend = null;
	/**
	User begin date

	@attribute dstart
	@type Date
	**/
	this.dstart = null;
	/**
	User ending date

	@attribute dend
	@type Date
	**/
	this.dend = null;
	/**
	Show event flag

	@attribute show_event
	@type Boolean
	**/
	this.show_event = false;
	/**
	Type of relation for user. 'r' for responsable and 'a' for attendee

	@attribute type
	@type String
	**/
	this.type = undefined;
	/**
	Text color for event

	@attribute color
	@type String
	**/
	this.color = undefined;
	/**
	Bakground color for event

	@attribute bgcolor
	@type String
	**/
	this.bgcolor = undefined;
	//
	// Initialize all attributes
	initAttributes.call(that, args);
	// end constructor ----------------------------------------
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	// --------------------------------------------------------
	// setValues
	// Record values for task
	// --------------------------------------------------------
	this.setValues = setValues;
	function setValues(params, elem, cb) {
		MIOGA.logDebug("ProjectTask", 1, "setValues params = ", params);
		var that = this;
		var start;
		var end;
		if (params.dstart === null) {
			start = "";
			end = "";
		}
		else {
			start = params.dstart.getFullYear()+'-'+(params.dstart.getMonth() + 1)+'-'+params.dstart.getDate();
			end = params.dend.getFullYear()+'-'+(params.dend.getMonth() + 1)+'-'+params.dend.getDate();
		}
		var show_event;
		if (params.hasOwnProperty(show_event)) {
			show_event = false;
		} else {
			show_event = params.show_event;
		}
		MIOGA.logDebug("ProjectTask", 1, "start = " + start + "  end = " + end);
		var data =  { "rowid" : that.rowid,
			         "show_event" : show_event,
			         "type" : that.type,
			         "dstart" : start,
			         "dend" : end
			       };

		// MIOGA.logDebug("MiogaCalendar", 1, "params");
		$.ajax({
			 url: mioga_context.private_bin_uri + "/Chronos/SetProjectTask.json",
			 type: "POST",
			 data: data,
			 dataType:"json",
			 success : function(data) {
			 			if (data.status === "OK") {
							initAttributes.call(that, data.task);
							if ($.isFunction(cb)) {
								cb(elem, that);
							}
						} else {
							MIOGA.logError("ProjectTask.setValues Error in web service", false);
						}
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
				  		alert("SetCalendar AJAX error : " + textStatus);
						MIOGA.logError("ProjectTask.setValues Error in web service textStatus " + textStatus, false);
				  }
		});
	}
	// ============================================================================
	// Private methods
	// ============================================================================
	// --------------------------------------------------------
	// initAttributes
	// init values from server
	// --------------------------------------------------------
	function initAttributes(args) {
		MIOGA.logDebug("ProjectTask", 3, "initAttributes args = ", args);
		var that = this;
		this.label = args.label || "";
		this.description = args.description || "";
		this.rowid = args.rowid || 0;
		this.project_id = args.project_id || undefined;
		this.project_label = args.project_label || undefined;
		this.user_id = args.user_id || undefined;
		this.group_id = args.group_id || undefined;
		this.group_ident = args.group_ident || undefined;
		this.current_progress = args.current_progress || 0;
		this.workload = args.workload || 1;
		this.pstart = null;
		if (args.pstart) {
			this.pstart = new Date();
			this.pstart.parseMiogaDate(args.pstart, true);
		}
		this.pend = null;
		if (args.pend) {
			this.pend = new Date();
			this.pend.parseMiogaDate(args.pend, true);
		}
		this.dstart = null;
		if (args.dstart) {
			this.dstart = new Date();
			this.dstart.parseMiogaDate(args.dstart, true);
		}
		this.dend = null;
		if (args.dend) {
			this.dend = new Date();
			this.dend.parseMiogaDate(args.dend, true);
		}
		this.show_event = false;
		if ( (args.show_event) && (args.show_event === 1) ) {
			this.show_event = true;
		}
		this.type = args.type || undefined;
		this.color = args.color || undefined;
		this.bgcolor = args.bgcolor || undefined;
	}
}
