// -----------------------------------------------------------------------------
// Fouquet is Mioga2 group animation application. It is constructed as a jQuery
// plugin.
// -----------------------------------------------------------------------------
(function ($) {
	var defaults = {
		i18n: {
			"label": "Identity",
			"firstname": "Firstname",
			"lastname": "Lastname",
			"email": "Email",
			"last_connection": "Last connection to group",
			"never_connected": "Never connected",
			"ident": "Identifier",
			"profile": "Profile",
			"identity_title": "Identity",
			"filesystem_title": "Filesystem access rights",
			"applications_title": "Applications access rights",
			"read_write": "Read/Write",
			"read_only": "Read-only",
			"no_right": "No right",
			"profile_details": "Profile details",
			"team": "Team",
			"synthesis_view": "Synthesis",
			"no_profile": "This user is not directly invited into this group.",
			"invite_user": "Invite this user.",
			"progressBarText": "Fouquet is loading...",
			"group_attributes_title": "Group attributes",
			"group_attributes_ident": "Identifier",
			"group_attributes_animator": "Animator",
			"group_attributes_disk_space_used": "Disk space",
			"group_attributes_default_profile": "Default profile",
			"group_attributes_theme": "Theme",
			"group_attributes_lang": "Language",
			"group_attributes_default_app": "Default application",
			"B": "B",
			"kB": "kB",
			"MB": "MB",
			"GB": "GB",
			"TB": "TB",
			"en_US": "English",
			"fr_FR": "Français",
			"group_users_title": "Users",
			"group_teams_title": "Teams",
			"group_profiles_title": "Profiles",
			"animation": "Animation",
			"user_synthesis": "User synthesis",
			"team_synthesis": "Team synthesis",
			"profile_synthesis": "Profile synthesis",
			"nb_users": "User(s)",
			"nb_teams": "Team(s)",
			"users_title": "Users",
			"teams_title": "Teams",
			"affected_users_title": "Users affected to profile",
			"non_affected_users_title": "Users non-affected to profile",
			"affected_teams_title": "Teams affected to profile",
			"non_affected_teams_title": "Teams non-affected to profile",
			"no_ui": "No user interface for ",
			"user_specific": "User-specific",
			"users": "Users",
			"teams": "Teams",
			"team_specific": "Team-specific",
			"internal_error": "Internal error: ",
			"no_user_for_profile": "No user is affected to this profile",
			"no_team_for_profile": "No team is affected to this profile",
			"no_profile_for_user": "This user is invited in group through a team and has no profile.",
			"edit": "Edit",
			"save": "Save",
			"cancel": "Cancel",
			"confirm_cancel_profile_edit": "Are you sure you want to cancel ?",
			"filesystem": "Filesystem",
			"applications": "Applications",
			"drag_users_notice": "To change user affectation, drag users from one list to the other.",
			"drag_teams_notice": "To change team affectation, drag teams from one list to the other.",
			"group_applications_title": "Applications",
			"confirm_application_deletion": "If you remove application $name$ from your group, all its data will be lost. This is unrecoverable.\nAre you sure you want to remove application $name$ from your group ?",
			"enable_application": "Enable an application",
			"user_tabs_help": "With the menu items above, you change the display of access rights to filesystem and applications. These rights can come from the user's profile, from the teams the user is member of or from user-specific rights (filesystem-only). Each menu item corresponds to a source of access rights applying to the current user. The \"Synthesis\" gives you an overall view on these access rights, once merged.",
			"team_tabs_help": "With the menu items above, you change the display of access rights to filesystem and applications. These rights can come from the team's profile or from team-specific rights (filesystem-only). Each menu item corresponds to a source of access rights applying to the current team. The \"Synthesis\" gives you an overall view on these access rights, once merged.",
			"team_members": "Team members",
			"no_user_for_team": "No user is member of this team",
			"group_resources_title": "Resources",
			"enable_resource": "Enable a resource",
			"confirm_resource_deletion": "If you remove resource $ident$ from your group, you won't be able to book it anymore.\nAre you sure you want to remove resource $ident$ from your group ?",
			"user_edit_title": "User \"$label$\"",
			"team_edit_title": "Team \"$ident$\"",
			"profile_edit_title": "Profile \"$ident$\"",
			"profile_change_success": "Profile successfully changed.",
			"profile_update_success": "Profile successfully updated.",
			"theme_change_success": "Theme successfully changed, you should reload current page to get it to current theme.",
			"lang_change_success": "Language successfully changed, you should reload current page to get it to current language.",
			"application_enable_success": "Application successfully enabled.",
			"application_disable_success": "Application successfully disabled.",
			"default_app_change_success": "Default application successfully changed.",
			"resource_enable_success": "Resource successfully enabled.",
			"resource_disable_success": "Resource successfully disabled.",
			"invite_users": "Invite users",
			"users_invite_success": "Users successfully invited",
			"invite_teams": "Invite teams",
			"teams_invite_success": "Teams successfully invited",
			"revoke_users": "Revoke users",
			"revoke_users_success": "Users successfully revoked.",
			"revoke_teams": "Revoke teams",
			"revoke_teams_success": "Teams successfully revoked.",
			"select_all": "Select all",
			"deselect_all": "Select none",
			"add": "Add",
			"revoke": "Revoke",
			"revoke_users_confirm_message": "The following users will be revoked from your group.\nAre you sure you want to revoke them ?",
			"revoke_teams_confirm_message": "The following teams will be revoked from your group.\nAre you sure you want to revoke them ?",
			"new_profile": "New profile",
			"create_profile": "Create profile",
			"close": "Close",
			"view_user_tooltip": "View details for user $firstname$ $lastname$ ($email$)",
			"view_team_tooltip": "View details for team $ident$",
			"view_profile_tooltip": "View details for profile $ident$",
			"top_of_page": "Top",
			"home_page": "Home page",
			"no_more_users_to_invite": "There are no more users to invite.",
			"no_more_users_to_revoke": "There are no more users to revoke.",
			"no_more_teams_to_invite": "There are no more teams to invite.",
			"no_more_teams_to_revoke": "There are no more teams to revoke.",
			"change_profile_users": "Change profile",
			"change_profile_users_confirm_message": "To change the following users' profile, please choose the profile below and validate.",
			"select_users": "Select users",
			"select_teams": "Select teams",
			"change_profile": "Change profile",
			"multiple_select": "Multiple select",
			"change_profile_teams": "Change profile",
			"change_profile_teams_confirm_message": "To change the following teams' profile, please choose the profile below and validate.",
			"no_more_users_to_change_profile": "There are no more users to change profile for.",
			"no_more_teams_to_change_profile": "There are no more teams to change profile for.",
			"is_public": "Public",
			"is_private": "Private",
			"application_public": "Application is now public",
			"application_private": "Application is now private",
			"public_mode_toggle_failed": "This application can not be set public.",
			"delete_profile": "Delete",
			"all": "All"
		}
	};

	// ========================================================
	// Effective plugin declaration with methods abstraction
	// Usage :
	//     <div id="fouquet"></div>
	//
	//     $('#fouquet').fouquet(options);
	// ========================================================
	$.fn.fouquet = function(options) {
		// method call, get existing Fouquet Object and apply method
		// if method not returns result we return this for chainability
		if (typeof options === 'string') {
			var args = Array.prototype.slice.call(arguments, 1);
			this.each(function() {
				var fouquet = $.data(this, 'fouquet');
				if (fouquet && $.isFunction(fouquet[options])) {
					var res = fouquet[options].apply(fouquet, args);
					if (res !== undefined) {
						return res;
					}
				}
				else {
					$.error( 'Method ' +  options + ' does not exist on jQuery.fouquet' );
				}
			});
		}
		else if (typeof options === 'object') {
			options =  $.extend(true, {}, defaults, options);

			this.each(function(i, item) {
				var $elem = $(item);
				var fouquet = new Fouquet(item, options);
				$(item).data('fouquet', fouquet);
			});
		}
		else {
			$.error( 'Fouquet must not be there ... ');
		}
		return this;
	};
