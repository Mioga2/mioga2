// ========================================================
// FouquetProfile OO-interface to profiles
// ========================================================
function FouquetProfile (options) {
	// ========================================================
	// Default values
	// ========================================================
	defaults = {
		rowid: undefined,
		ident: undefined
	},

	// ========================================================
	// Public methods
	// ========================================================

	// --------------------------------------------------------
	// fetch, fetch profile from Mioga2
	// The "full" flag (defaults to false) indicates whether the profile should
	// be loaded for a display purpose (only its members are loaded) or for an
	// edit purpose (even non-members are loaded)
	// --------------------------------------------------------
	this.fetch = fetch;
	function fetch () {
		MIOGA.logDebug ('FouquetProfile', 2, '[fetch] Entering, rowid: ' + this.attr.rowid);
		var that = this;

		// Profile to load
		var profile_attr = { };

		if (this.attr.rowid) {
			profile_attr.rowid = this.attr.rowid
		}

		if (this.full) {
			profile_attr.full = 1;
		}

		$.ajax ({
			url: 'GetProfile.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			data: profile_attr,
			success: function (data) {
				MIOGA.logDebug ('FouquetProfile', 3, '[fetch] GetProfile.json returned, data: ', data);
				that.attr = data;
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('FouquetProfile', 2, '[fetch] Leaving, data: ', this.attr);
	};

	// --------------------------------------------------------
	// store, store profile to Mioga2
	// --------------------------------------------------------
	this.store = store;
	function store () {
		MIOGA.logDebug ('FouquetProfile', 1, '[store] Entering');

		// Basic attributes
		var profile_attr = {
			rowid: this.attr.rowid,
			ident: this.attr.ident,
			functions: [ ]
		};

		// Populate function list
		for (app_idx = 0; app_idx < this.attr.rights.applications.length; app_idx++) {
			var app = this.attr.rights.applications[app_idx];
			for (funct_idx = 0; funct_idx < app.functions.length; funct_idx++) {
				if (app.functions[funct_idx].selected) {
					profile_attr.functions.push (app.functions[funct_idx].rowid);
				}
			}
		}

		MIOGA.logDebug ('FouquetProfile', 2, '[store] Data to be posted: ', profile_attr);

		// Post data to Mioga2
		var success = false;
		var message = "[FouquetProfile::store] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		var profile = {rowid: this.attr.rowid};	// For further loading from WebService response
		$.ajax ({
			url: 'SetProfile.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: profile_attr,
			success: function (data) {
				MIOGA.logDebug ('FouquetProfile', 2, '[store] SetProfile.json POST returned, data: ', data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.profile) {
					profile = data.profile;
				}
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('FouquetProfile', 1, '[store] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, profile: profile});
	};

	// --------------------------------------------------------
	// deleteProfile, delete a profile from Mioga2
	// --------------------------------------------------------
	this.deleteProfile = deleteProfile;
	function deleteProfile () {
		MIOGA.logDebug ('FouquetProfile', 1, '[deleteProfile] Entering');

		var post_data = {
			rowid: this.attr.rowid
		};

		MIOGA.logDebug ('FouquetProfile', 2, '[deleteProfile] Data to be posted: ', post_data);

		// Post data to Mioga2
		var success = false;
		var message = "[FouquetProfile::deleteProfile] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		$.ajax ({
			url: 'DeleteProfile.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: post_data,
			success: function (data) {
				MIOGA.logDebug ('FouquetProfile', 2, '[deleteProfile] DeleteProfile.json POST returned, data: ', data);
				success = data.success;
				message = data.message;
				errors = data.errors;
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('FouquetProfile', 1, '[deleteProfile] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors});
	}


	// ========================================================
	// Constructor
	// ========================================================
	MIOGA.logDebug ('FouquetProfile', 1, '[new] Entering, options: ', options);
	var that = this;

	this.attr = $.extend (true, {}, this.defaults, options);

	// Should we load full profile (with extra-data) ?
	this.full = (this.attr.full !== undefined) ? delete this.attr.full : false;

	// Fetch profile data from Mioga2 if required
	if ((Object.keys (this.attr).length === 1) && (this.attr.rowid !== undefined)) {
		// Object attributes has a single key (rowid), fetch data from Mioga2
		this.fetch ();
	}

	MIOGA.logDebug ('FouquetProfile', 1, '[new] Leaving, data: ', that.attr);
}
