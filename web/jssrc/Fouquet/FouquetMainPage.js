	// ========================================================================================================
	// FouquetMainPage object, UI entry point
	// ========================================================================================================
	function FouquetMainPage (elem, options) {


		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data, formatter) {
			MIOGA.logDebug ('FouquetMainPage', 1, '[refresh] Entering', data);
			var that = this; // For callback reference to current object

			that.data = data;

			// Remove error messages
			$('span.error-message').remove ();

			// Attributes
			$.each (Array ('ident', 'animator', 'disk_space_used', 'default_profile', 'lang', 'theme', 'default_app'), function (index, field) {
				that.fields[field].empty ();
				that.fields[field].append (formatValue.call (that, data.attr, field, 'group'));
			});

			// Users
			that.fields.$users.empty ();
			$.each (data.attr.users, function (index, user) {
				// Generate label string
				user.label = user.firstname + ' ' + user.lastname + ' (' + user.email + ')';
				if (user.profile !== undefined) {
					var $line = $('<tr></tr>').appendTo (that.fields.$users);
					$.each (that.options.columns.users, function (index, field) {
						$('<td></td>').append (formatValue.call (that, user, field, 'user')).addClass (field).appendTo ($line);
					});
				}
			});
			that.$cont.find ('.group-users .table-container').css ({overflow: 'hidden'});	// Dirty hack, otherwise normally-hidden table content will be visible and mess the UI
			that.$cont.find ('.group-users table').trigger ('update');
			that.$cont.find ('.group-users .table-container').css ({overflow: 'inherit'});	// Dirty hack, otherwise normally-hidden table content will be visible and mess the UI

			// Toggle users invite button visibility
			var non_member  = $.grep (that.data.attr.users, function (user) { return (user.profile === undefined); });
			if (non_member.length || data.attr.non_member_users) {
				that.$invite_users_btn.show ();
			}
			else {
				that.$invite_users_btn.hide ();
			}
			if (non_member.length === (that.data.attr.users.length - 1)) {	// Only one member in group (the animator), revokation is not possible
				that.$multi_select_users_btn.hide ();
			}
			else {
				that.$multi_select_users_btn.show ();
			}

			// Teams
			that.fields.$teams.empty ();
			$.each (data.attr.teams, function (index, team) {
				if (team.profile !== undefined) {
					var $line = $('<tr></tr>').appendTo (that.fields.$teams);
					$.each (that.options.columns.teams, function (index, field) {
						$('<td></td>').append (formatValue.call (that, team, field, 'team')).addClass (field).appendTo ($line);
					});
				}
			});
			that.$cont.find ('.group-teams .table-container').css ({overflow: 'hidden'});	// Dirty hack, otherwise normally-hidden table content will be visible and mess the UI
			that.$cont.find ('.group-teams table').trigger ('update');
			that.$cont.find ('.group-teams .table-container').css ({overflow: 'inherit'});	// Dirty hack, otherwise normally-hidden table content will be visible and mess the UI

			// Toggle teams invite button visibility
			var non_member  = $.grep (that.data.attr.teams, function (team) { return (team.profile === undefined); });
			if (non_member.length || data.attr.non_member_teams) {
				that.$invite_teams_btn.show ();
			}
			else {
				that.$invite_teams_btn.hide ();
			}
			if (non_member.length === that.data.attr.teams.length) {
				that.$multi_select_teams_btn.hide ();
				that.fields.$teams.parent ().find ('thead').hide ();
			}
			else {
				that.$multi_select_teams_btn.show ();
				that.fields.$teams.parent ().find ('thead').show ();
			}

			// Profiles
			that.fields.$profiles.empty ();
			$.each (data.attr.profiles, function (index, profile) {
				var $line = $('<tr></tr>').appendTo (that.fields.$profiles);
				$.each (that.options.columns.profiles, function (index, field) {
					$('<td></td>').append (formatValue.call (that, profile, field, 'profile')).addClass (field).appendTo ($line);
				});
			});

			// Resources
			that.fields.$resources.empty ();
			that.fields.$inactive_resources.empty ().hide ();
			// "Enable an resource" placeholder
			$('<option></option>').append (i18n.enable_resource).val (0).attr ('disabled', true).attr ('selected', true).appendTo (that.fields.$inactive_resources);
			data.attr.resources.sort (function (a, b) {
				return ((a.ident < b.ident) ? -1 : 1);
			});
			$.each (data.attr.resources, function (index, resource) {
				if (resource.selected) {
					var $res = $('<li class="resource"></li>').append (resource.ident).appendTo (that.fields.$resources);
					$('<div class="note"></div>').append (resource.description).appendTo ($res);
					$('<div></div>').addClass ('delete').appendTo ($res).data ('rowid', resource.rowid).click (function () {
						var message = i18n.confirm_resource_deletion.replace (/\$ident\$/g, resource.ident);
						if (confirm (message)) {
							disableResource.call (that, $(this).data ('rowid'));
						}
					});
				}
				else {
					var $res = $('<option></option>').append (resource.ident).val (resource.rowid);
					that.fields.$inactive_resources.append ($res);
					that.fields.$inactive_resources.show ();
				}
			});

			// Applications
			that.fields.$applications.empty ();
			that.fields.$inactive_applications.empty ().hide ();
			var $toggle = $('<div class="enable-application-list closed"></div>').append ($('<div class="list-placeholder"></div>').append (i18n.enable_application).append ($('<span class="list-open"></span>').append ('&#x25B2;')).append ($('<span class="list-closed"></span>').append ('&#x25BC;'))).appendTo (that.fields.$inactive_applications);
			var $inactive_applications_list = $('<ul class="list applications inactive-applications"></ul>').appendTo ($toggle).hide ();
			$toggle.click (function () {
				$toggle.toggleClass ('open').toggleClass ('closed');
				$inactive_applications_list.toggle ();
			});
			data.attr.applications.sort (function (a, b) {
				return ((a.label < b.label) ? -1 : 1);
			});
			$.each (data.attr.applications, function (index, application) {
				var $app = $('<li class="application"></li>').addClass ('rowid-' + application.rowid).append (application.label);
				$('<div class="note"></div>').append (application.description).appendTo ($app);
				$('<div></div>').addClass ('icon').append ('<img src="' + mioga_context.image_uri + '/32x32/apps/' + application.ident.toLowerCase () + '.png" alt="icon"/>').appendTo ($app);
				if (application.selected) {
					if (application.can_be_public) {
						var $public_mode = $('<div class="public-mode-toggle"></div>').appendTo ($app);
						var $private_input = $('<input type="radio" value="0"/>').attr ('name', 'application-' + application.rowid + '-public-mode').attr ('id', 'application-' + application.rowid + '-private').appendTo ($public_mode).change (function () { togglePublicMode.call (that, application.rowid, 'private') });
						$('<label></label>').attr ('for', 'application-' + application.rowid + '-private').append (i18n.is_private).appendTo ($public_mode);
						var $public_input = $('<input type="radio" value="1"/>').attr ('name', 'application-' + application.rowid + '-public-mode').attr ('id', 'application-' + application.rowid + '-public').appendTo ($public_mode).change (function () { togglePublicMode.call (that, application.rowid, 'public') });
						$('<label></label>').attr ('for', 'application-' + application.rowid + '-public').append (i18n.is_public).appendTo ($public_mode);
						if (application.is_public) {
							$public_input.attr ('checked', true);
						}
						else {
							$private_input.attr ('checked', true);
						}
						$public_mode.buttonset ();
					}
					$app.appendTo (that.fields.$applications);
					if (application.rowid !== that.data.attr.default_app_id && $.inArray(application.ident, that.data.attr.protected_apps) == -1) {
						$('<div></div>').addClass ('delete').appendTo ($app).data ('rowid', application.rowid).click (function () {
							var message = i18n.confirm_application_deletion.replace (/\$name\$/g, application.label);
							if (confirm (message)) {
								disableApplication.call (that, $(this).data ('rowid'));
							}
						});
					}
				}
				else {
					$app.appendTo ($inactive_applications_list).click (function () {
						enableApplication.call (that, application.rowid);
						return (false);
					});
					that.fields.$inactive_applications.show ();
				}
			});
			if ((formatter !== undefined) && (formatter.application !== undefined)) {
				// Scroll to newly-added application
				$('div.group-applications div.list-container').scrollTop ($('li.application.rowid-' + formatter.application).position ().top);
				// Blink newly-added application
				$('li.application.rowid-' + formatter.application).animate({opacity:0}, 200, "linear", function () { $(this).animate ({opacity:1}, 200);});
			}

			MIOGA.logDebug ('FouquetMainPage', 1, '[refresh] Leaving.');
		};


		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// formatValue
		// Given an object (hash) the name of a field and an object type,
		// format inner data for field display an interaction (click events, etc.)
		// --------------------------------------------------------
		function formatValue (object, field, object_type) {
			var that = this;

			// Default behavior is to return the field as-is
			var value = object[field];

			// Some fields need a specific contruction rule
			if ((field === 'ident') && (object_type !== 'group')) {
				var tooltip = '';
				if (object_type === 'team') {
					tooltip = i18n.view_team_tooltip.replace (/\$ident\$/, object.ident);
				}
				else if (object_type === 'profile') {
					tooltip = i18n.view_profile_tooltip.replace (/\$ident\$/, object.ident);
				}
				value = $('<a></a>').attr ('href', fouquet.generateNavigationHash (object_type, object.rowid)).attr ('title', tooltip).append (object.ident);
			}
			else if (field === 'lastname') {
				// User label formatter, link with click handler
				// User label is made of firstname, lastname and e-mail
				var tooltip = i18n.view_user_tooltip.replace (/\$firstname\$/, object.firstname).replace (/\$lastname\$/, object.lastname).replace (/\$email\$/, object.email);
				value = $('<a></a>').attr ('href', fouquet.generateNavigationHash (object_type, object.rowid)).attr ('title', tooltip).append (object.lastname);
			}
			else if (field === 'animator') {
				// Animator: user label formatter, link with click handler
				// User label is made of firstname, lastname and e-mail
				var tooltip = i18n.view_user_tooltip.replace (/\$firstname\$/, object.animator.firstname).replace (/\$lastname\$/, object.animator.lastname).replace (/\$email\$/, object.animator.email);
				value = $('<a></a>').attr ('href', fouquet.generateNavigationHash ('user', object.animator.rowid)).attr ('title', tooltip).append (object.animator.firstname + ' ' + object.animator.lastname + ' (' + object.animator.email + ')');
			}
			else if ((field === 'profile') || (field === 'default_profile')) {
				// Profile formatter, link to profile
				var tooltip = i18n.view_profile_tooltip.replace (/\$ident\$/, object[field].ident);
				value = $('<div></div>').addClass ('inline-chooser').append ($('<a></a>').attr ('href', fouquet.generateNavigationHash ('profile', object[field].rowid)).attr ('title', tooltip).append (object[field].ident));

				if (object.rowid !== that.data.attr.animator.rowid) {
					// Dropdown menu to change profile
					var $dropdown = $('<ul class="dropdown"></ul>').appendTo (value);
					var $entry = $('<li class="dir"></li>').append ('&#x25BC;').appendTo ($dropdown);
					var $submenu = $('<ul></ul>').appendTo ($entry);

					$.each (that.data.attr.profiles, function (index, profile) {
						if (profile.ident !== object[field].ident) {
							var $link = $('<a></a>').append (profile.ident);
							var $entry = $('<li></li>').append ($link).appendTo ($submenu).click (function () {
								// Update object's profile and store to Mioga2
								setProfile.call (that, object_type, object, profile);
								that.refresh (that.data);
							});
						}
					});
				}
			}
			else if ((field === 'lang') || (field === 'theme')) {
				// Lang and theme formatter, link with click handler
				value = $('<div></div>').addClass ('inline-chooser').append ($('<span></span>').append (object[field].name));

				// Dropdown menu to change value
				var $dropdown = $('<ul class="dropdown"></ul>').appendTo (value);
				var $entry = $('<li class="dir"></li>').append ('&#x25BC;').appendTo ($dropdown);
				var $submenu = $('<ul></ul>').appendTo ($entry);

				$.each (that.data.attr[field + 's'], function (index, item) {
					if (item.name !== object[field].name) {
						var $link = $('<a></a>').append (item.name);
						var $entry = $('<li></li>').append ($link).appendTo ($submenu).click (function () {
							// Update group attributes
							var group = new FouquetGroup (object);
							switch (field) {
								case 'lang':
									group.attr.lang = item;
									object.lang = item;	// For further use in UI
									break;
								case 'theme':
									group.attr.theme = item;
									object.theme = item;	// For further use in UI
									break;
							}
							var res = group.store ();
							if (res.success) {
								var message = (field === 'lang') ? i18n.lang_change_success : i18n.theme_change_success;
								message = (res.message !== undefined) ? res.message : message;
								that.$message_box.inlineMessage ('info', message);
								that.refresh (that.data);
							}
							else {
								displayError.call (that, res);
							}
						});
					}
				});
			}
			else if ((field === 'last_connection') && (object.last_connection === undefined)) {
				value = $('<span class="note"></span>').append (i18n.never_connected);
			}
			else if (field === 'last_connection') {
				var d = new Date ();
				d.parseMiogaDate (object[field]);
				value = d.formatMioga (true);
			}
			else if (field === 'default_app') {
				// Default application formatter, application label in a link pointing to application's DisplayMain and click handler to change default application
				var app = $.grep (object.applications, function (item) { return (item.rowid === object.default_app_id); })[0];
				value = $('<div></div>').addClass ('inline-chooser').append ($('<span></span>').append (app.label));

				// Dropdown menu to change application
				var $dropdown = $('<ul class="dropdown"></ul>').appendTo (value);
				var $entry = $('<li class="dir"></li>').append ('&#x25BC;').appendTo ($dropdown);
				var $submenu = $('<ul></ul>').appendTo ($entry);

				var applications = $.grep (that.data.attr.applications, function (app) { return (app.selected === true); });
				$.each (applications, function (index, app) {
					if (app.rowid !== object[field]) {
						var $link = $('<a></a>').append (app.label);
						var $entry = $('<li></li>').append ($link).appendTo ($submenu).click (function () {
							// Update object's default_app and store to Mioga2
							setDefaultApp.call (that, app.rowid);
						});
					}
				});
			}
			else if (field === 'disk_space_used') {
				var units = ['B', 'kB', 'MB', 'GB', 'TB'];
				var mult = 0;
				while (value >= 1024) {
					value /= 1024;
					mult++;
				}
				value = Math.ceil (value * 100) / 100;
				value += i18n[units[mult]];
			}

			return (value);
		};

		// --------------------------------------------------------
		// setProfile
		// Given an object type and an object attributes (hash),
		// instanciate object, update profile and store object
		// --------------------------------------------------------
		function setProfile (object_type, data, profile) {
			MIOGA.logDebug ('FouquetMainPage', 1, '[setProfile] Entering, object type: ' + object_type + ', object rowid: ' + data.rowid + ', profile ident: ' + profile.ident);

			var object = undefined;
			var key = 'profile';	// In case of a group, the key is "default_profile"
			switch (object_type) {
				case 'group':
					object = new FouquetGroup (data);
					key = 'default_profile';
					break;
				case 'user':
					object = new FouquetUser (data);
					break;
				case 'team':
					object = new FouquetTeam (data);
					break;
				default:
					alert (i18n.internal_error + 'Object type "' + object_type + '" is not supported by FouquetMainPage::setProfile');
					break;
			}

			// Change profile and store data to Mioga2
			object.attr[key] = {
				rowid: profile.rowid,
				ident: profile.ident
			};
			var res = object.store ();
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.profile_change_success;
				that.$message_box.inlineMessage ('info', message);

				// For further use in UI
				data[key] = {
					rowid: profile.rowid,
					ident: profile.ident
				};

				// Update profile members count
				for (var i=0; i<that.data.attr.profiles.length; i++) {
					var prof = that.data.attr.profiles[i];
					if (prof.rowid === profile.rowid) {
						switch (object_type) {
							case 'user':
								prof.nb_users++;
								break;
							case 'team':
								prof.nb_teams++;
								break;
						}
					}
					else if (prof.rowid === data.profile_id) {
						switch (object_type) {
							case 'user':
								prof.nb_users--;
								break;
							case 'team':
								prof.nb_teams--;
								break;
						}
					}
				}
				data.profile_id = profile.rowid;

				that.refresh (that.data);
			}
			else {
				displayError.call (that, res);
			}

			MIOGA.logDebug ('FouquetMainPage', 1, '[setProfile] Leaving.');
		}

		// --------------------------------------------------------
		// enableApplication
		// Given an application rowid, enable it for group
		// --------------------------------------------------------
		function enableApplication (rowid) {
			MIOGA.logDebug ('FouquetMainPage', 1, '[enableApplication] Entering, rowid: ' + rowid);

			// Update group
			var group = new FouquetGroup (that.data.attr);
			var app = $.grep (group.attr.applications, function (item) { return (item.rowid === rowid); })[0];
			app.selected = true;
			var res = group.store ();
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.application_enable_success;
				that.$message_box.inlineMessage ('info', message);

				// Refresh UI to match changes
				that.data = group;
				this.refresh (that.data, {application: app.rowid});
			}
			else {
				displayError.call (that, res);
			}

			MIOGA.logDebug ('FouquetMainPage', 1, '[enableApplication] Leaving.');
		}

		// --------------------------------------------------------
		// disableApplication
		// Given an application rowid, disable it for group
		// --------------------------------------------------------
		function disableApplication (rowid) {
			MIOGA.logDebug ('FouquetMainPage', 1, '[disableApplication] Entering, rowid: ' + rowid);

			// Update group
			var group = new FouquetGroup (that.data.attr);
			var app = $.grep (group.attr.applications, function (item) { return (item.rowid === rowid); })[0];
			app.selected = false;
			var res = group.store ();
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.application_disable_success;
				that.$message_box.inlineMessage ('info', message);

				// Refresh UI to match changes
				that.data = group;
				this.refresh (that.data);
			}
			else {
				displayError.call (that, res);
			}

			MIOGA.logDebug ('FouquetMainPage', 1, '[disableApplication] Leaving.');
		}

		// --------------------------------------------------------
		// setDefaultApp
		// Given an application rowid, disable it for group
		// --------------------------------------------------------
		function setDefaultApp (rowid) {
			MIOGA.logDebug ('FouquetMainPage', 1, '[setDefaultApp] Entering, rowid: ' + rowid);

			// Update group
			var group = new FouquetGroup (that.data.attr);
			var app = $.grep (group.attr.applications, function (item) { return (item.rowid === rowid); })[0];
			group.attr.default_app_id = app.rowid;
			var res = group.store ();
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.default_app_change_success;
				that.$message_box.inlineMessage ('info', message);

				// Refresh UI to match changes
				that.data = group;
				this.refresh (that.data);
			}
			else {
				displayError.call (that, res);
			}

			MIOGA.logDebug ('FouquetMainPage', 1, '[setDefaultApp] Leaving.');
		}

		// --------------------------------------------------------
		// enableResource
		// Given an resource rowid, enable it for group
		// --------------------------------------------------------
		function enableResource (rowid) {
			MIOGA.logDebug ('FouquetMainPage', 1, '[enableResource] Entering, rowid: ' + rowid);

			// Update group
			var group = new FouquetGroup (that.data.attr);
			var res = $.grep (group.attr.resources, function (item) { return (item.rowid === rowid); })[0];
			res.selected = true;
			var res = group.store ();
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.resource_enable_success;
				that.$message_box.inlineMessage ('info', message);

				// Refresh UI to match changes
				that.data = group;
				this.refresh (that.data);
			}
			else {
				displayError.call (that, res);
			}

			MIOGA.logDebug ('FouquetMainPage', 1, '[enableResource] Leaving.');
		}

		// --------------------------------------------------------
		// disableResource
		// Given an resource rowid, disable it for group
		// --------------------------------------------------------
		function disableResource (rowid) {
			MIOGA.logDebug ('FouquetMainPage', 1, '[disableResource] Entering, rowid: ' + rowid);

			// Update group
			var group = new FouquetGroup (that.data.attr);
			var app = $.grep (group.attr.resources, function (item) { return (item.rowid === rowid); })[0];
			app.selected = false;
			var res = group.store ();
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.resource_disable_success;
				that.$message_box.inlineMessage ('info', message);

				// Refresh UI to match changes
				that.data = group;
				this.refresh (that.data);
			}
			else {
				displayError.call (that, res);
			}

			// Refresh UI to match changes
			that.data = group;
			this.refresh (that.data);

			MIOGA.logDebug ('FouquetMainPage', 1, '[disableResource] Leaving.');
		}

		// --------------------------------------------------------
		// inviteUsers
		// Show non-members list and invite new users
		// --------------------------------------------------------
		function inviteUsers () {
			var open_dialog = true;
			var args = {
				app_name: 'Fouquet',
				single: false,
				i18n: {
					select_all: i18n.select_all,
					deselect_all: i18n.deselect_all,
					add_btn: i18n.add,
					all: i18n.all,
					columns: [
						{
							field: 'firstname',
							label: i18n.firstname
						},
						{
							field: 'lastname',
							label: i18n.lastname,
							Default: true
						},
						{
							field: 'email',
							label: i18n.email
						}
					]
				},
				data: that.data.getNonMemberUsers (),
				select_add: function (rowids) {
					MIOGA.logDebug ('FouquetMainPage', 1, '[inviteUsers] Adding user rowids: ', rowids);
					for (i=0; i<rowids.length; i++) {
						that.data.attr.users.push ({rowid: rowids[i], profile: that.data.attr.default_profile});
					}
					var res = that.data.store ();
					if (res.success) {
						message = i18n.users_invite_success;
						that.$message_box.inlineMessage ('info', message);
						var group = new FouquetGroup (res.group);
						that.refresh (group);
						that.$userSelect.userSelect ('refresh_list');
						// Query server for non-member count
						var users = that.data.getNonMemberUsers ({
							max: 0
						});
						if (isNaN (parseInt (users, 10))) {
							that.$confirm_message = $('<div class="confirm-dialog"></div>').append ($('<p class="note"></p>').append (i18n.no_more_users_to_invite)).appendTo (that.$selector);
							that.$confirm_message.show ();
							that.$userSelect.hide ();
						}
						else {
							if (that.$confirm_message !== undefined) {
								that.$confirm_message.hide ();
							}
							that.$userSelect.show ();
						}
					}
					else {
						displayError.call (that, res);
					}
				}
			};
			if (open_dialog) {
				// Hide useless batch-actions buttons
				that.$revoke_teams_btn_cont.hide ();
				that.$change_profile_teams_btn_cont.hide ();
				that.$revoke_users_btn_cont.hide ();
				that.$change_profile_users_btn_cont.hide ();

				// Open dialog
				that.$selector_dialog.dialog ({
					modal: true,
					width: Math.round( (0.8 * $(window).width () ) / 10 ) * 10,
					height: Math.round( (0.8 * $(window).height () ) / 10 ) * 10,
					resizable: true,
					title: i18n.invite_users,
					open: function () {
						that.$selector.empty ();
						that.$userSelect = $('<div class="invite-dialog"></div>').appendTo (that.$selector).userSelect (args);
					}
				});
			}
		}

		// --------------------------------------------------------
		// multipleUserSelect
		// Show members list and give access to batch actions
		// --------------------------------------------------------
		function multipleUserSelect () {
			var open_dialog = true;
			var args = {
				app_name: 'Fouquet',
				single: false,
				i18n: {
					select_all: i18n.select_all,
					deselect_all: i18n.deselect_all,
					all: i18n.all,
					// add_btn: i18n.revoke,
					columns: [
						{
							field: 'firstname',
							label: i18n.firstname
						},
						{
							field: 'lastname',
							label: i18n.lastname,
							Default: true
						},
						{
							field: 'profile',
							label: i18n.profile
						},
						{
							field: 'email',
							label: i18n.email
						}
					]
				},
				data: $.map ($.grep (that.data.attr.users, function (user) { return ((user.profile !== undefined) && (user.rowid !== that.data.attr.animator.rowid)); }), function (user) { var tmp = {rowid: user.rowid, ident: user.ident, email: user.email, firstname: user.firstname, lastname: user.lastname, profile: user.profile.ident}; return (tmp); })
			};
			if (open_dialog) {
				that.$selector_dialog.dialog ({
					modal: true,
					width: 0.8 * $(window).width (),
					height: 0.8 * $(window).height (),
					resizable: true,
					title: i18n.select_users,
					open: function () {
						that.$selector.empty ();
						that.$userSelect = $('<div class="revoke-dialog"></div>').appendTo (that.$selector).userSelect (args);
					}
				});
				that.$revoke_teams_btn_cont.hide ();
				that.$change_profile_teams_btn_cont.hide ();
				that.$revoke_users_btn_cont.show ();
				that.$change_profile_users_btn_cont.show ();
			}
		}

		// --------------------------------------------------------
		// processUserSelection
		// Perform an action (revoke, change profile) on a list of users
		// --------------------------------------------------------
		function processUserSelection (rowids, command) {
			MIOGA.logDebug ('FouquetMainPage', 1, '[processUserSelection] Performing ' + command + ' on user rowids: ', rowids);
			this.$revoke_users_btn_cont.hide ();
			this.$change_profile_users_btn_cont.hide ();
			this.$dialog_close_btn_cont.hide ();
			var $btn_list = $('<ul class="button_list"></ul>');
			if (command === 'revoke') {
				that.$confirm_message = $('<div class="confirm-dialog"></div>').append ($('<p class="note"></p>').append (i18n.revoke_users_confirm_message));
				$('<button class="button"></button>').append (i18n.revoke).appendTo ($('<li></li>').appendTo ($btn_list)).click (function () {
					revokeUsers.call (that, rowids);
					return (false);
				});
			}
			else if (command === 'change_profile') {
				that.$confirm_message = $('<div class="confirm-dialog"></div>').append ($('<p class="note"></p>').append (i18n.change_profile_users_confirm_message));
				var $profile = $('<select class="profile-select"></select>').appendTo (that.$confirm_message);
				for (i=0; i<that.data.attr.profiles.length; i++) {
					var profile = that.data.attr.profiles[i];
					$profile.append ($('<option></option>').attr ('value', profile.rowid).append (profile.ident));
				}
				$('<button class="button"></button>').append (i18n.change_profile).appendTo ($('<li></li>').appendTo ($btn_list)).click (function () {
					var profile = {
						rowid: parseInt ($profile.val (), 10),
						ident: $profile.find ('option:selected').html ()
					};
					changeUsersProfile.call (that, rowids, profile);
					var users = $.map ($.grep (that.data.attr.users, function (user) { return ((user.profile !== undefined) && (user.rowid !== that.data.attr.animator.rowid)); }), function (user) { var tmp = {rowid: user.rowid, ident: user.ident, email: user.email, firstname: user.firstname, lastname: user.lastname, profile: user.profile.ident}; return (tmp); });
					that.$userSelect.userSelect ('replace_list', users);
					return (false);
				});
			}

			// Append Cancel button and button list
			$('<button class="button cancel"></button>').append (i18n.cancel).appendTo ($('<li></li>').appendTo ($btn_list)).click (function () {
				that.$confirm_message.hide ();
				that.$userSelect.show ();
				that.$revoke_users_btn_cont.show ();
				that.$change_profile_users_btn_cont.show ();
				that.$dialog_close_btn_cont.show ();
				return (false);
			});
			$btn_list.appendTo (that.$confirm_message)

			// Add list of users to confirm message
			var $list = $('<ul class="item-list"></ul>').appendTo (that.$confirm_message);
			for (i=0; i<that.data.attr.users.length; i++) {
				if ($.grep (rowids, function (rowid) { return (rowid === that.data.attr.users[i].rowid); }).length !== 0) {
					$('<li></li>').append (that.data.attr.users[i].label).appendTo ($list);
				}
			}

			// Show confirm message and hide team selector
			that.$selector.append (that.$confirm_message);
			that.$userSelect.hide ();
			that.$confirm_message.show ();
		}


		// --------------------------------------------------------
		// revokeUsers
		// Batch-revoke users from a list of rowids
		// --------------------------------------------------------
		function revokeUsers (rowids) {
			// Remove profile information from selected users
			for (i=0; i<that.data.attr.users.length; i++) {
				if ($.grep (rowids, function (rowid) { return (rowid === that.data.attr.users[i].rowid); }).length !== 0) {
					delete (that.data.attr.users[i].profile);
				}
			}
			var res = that.data.store ();
			if (res.success) {
				message = i18n.revoke_users_success;
				that.$message_box.inlineMessage ('info', message);
				var group = new FouquetGroup (res.group);
				that.refresh (group);
				that.$userSelect.userSelect ('refresh_list');
				var users = that.$userSelect.userSelect ('get_list');
				if (users.length === 0) {
					that.$confirm_message.replaceWith ($('<div class="confirm-dialog"></div>').append ($('<p class="note"></p>').append (i18n.no_more_users_to_revoke)));
					that.$confirm_message.show ();
					that.$userSelect.hide ();
				}
				else {
					that.$confirm_message.hide ();
					that.$userSelect.show ();
					that.$revoke_users_btn_cont.show ();
					that.$change_profile_users_btn_cont.show ();
				}
			}
			else {
				displayError.call (that, res);
			}
		}

		// --------------------------------------------------------
		// changeUsersProfile
		// Batch-change users profile from a list of rowids and a profile information hash
		// --------------------------------------------------------
		function changeUsersProfile (rowids, profile) {
			// Update profile information to selected users
			for (i=0; i<that.data.attr.users.length; i++) {
				if ($.grep (rowids, function (rowid) { return (rowid === that.data.attr.users[i].rowid); }).length !== 0) {
					setProfile.call (that, 'user', that.data.attr.users[i], profile);
				}
			}
			that.$userSelect.userSelect ('refresh_list');
			var users = that.$userSelect.userSelect ('get_list');
			if (users.length === 0) {
				that.$confirm_message.replaceWith ($('<div class="confirm-dialog"></div>').append ($('<p class="note"></p>').append (i18n.no_more_users_to_change_profile)));
				that.$confirm_message.show ();
				that.$userSelect.hide ();
			}
			else {
				that.$confirm_message.hide ();
				that.$userSelect.show ();
				that.$revoke_users_btn_cont.show ();
				that.$change_profile_users_btn_cont.show ();
				that.$dialog_close_btn_cont.show ();
			}
		}


		// --------------------------------------------------------
		// inviteTeams
		// Show non-members list and invite new teams
		// --------------------------------------------------------
		function inviteTeams () {
			var open_dialog = true;
			var args = {
				app_name: 'Fouquet',
				single: false,
				i18n: {
					select_all: i18n.select_all,
					deselect_all: i18n.deselect_all,
					add_btn: i18n.add
				},
				data: function (arg) {
					var teams = $.grep (that.data.attr.teams, function (team) { return (team.profile === undefined); });
					if (!teams.length) {
						teams = that.data.getNonMemberTeams ();
					}
					return (teams);
				},
				select_add: function (rowids) {
					MIOGA.logDebug ('FouquetMainPage', 1, '[inviteTeams] Adding team rowids: ', rowids);
					for (i=0; i<rowids.length; i++) {
						that.data.attr.teams.push ({rowid: rowids[i], profile: that.data.attr.default_profile});
					}
					var res = that.data.store ();
					if (res.success) {
						message = i18n.teams_invite_success;
						that.$message_box.inlineMessage ('info', message);
						var group = new FouquetGroup (res.group);
						that.refresh (group);
						that.$teamselect.itemSelect ('refresh_list');
						var teams = that.$teamselect.itemSelect ('get_list');
						if (teams.length === 0) {
							that.$confirm_message = $('<div class="confirm-dialog"></div>').append ($('<p class="note"></p>').append (i18n.no_more_teams_to_invite)).appendTo (that.$selector);
							that.$confirm_message.show ();
							that.$teamselect.hide ();
						}
						else {
							that.$confirm_message.hide ();
							that.$teamselect.show ();
						}
					}
					else {
						displayError.call (that, res);
					}
				}
			};
			if (open_dialog) {
				// Hide useless batch-actions buttons
				that.$revoke_teams_btn_cont.hide ();
				that.$change_profile_teams_btn_cont.hide ();
				that.$revoke_users_btn_cont.hide ();
				that.$change_profile_users_btn_cont.hide ();

				// Open dialog
				that.$selector_dialog.dialog ({
					modal: true,
					width: 0.8 * $(window).width (),
					height: 0.8 * $(window).height (),
					resizable: true,
					title: i18n.invite_teams,
					open: function () {
						that.$selector.empty ();
						that.$teamselect = $('<div></div>').appendTo (that.$selector).itemSelect (args);
					}
				});
			}
		}


		// --------------------------------------------------------
		// multipleTeamSelect
		// Show members list and give access to batch actions
		// --------------------------------------------------------
		function multipleTeamSelect () {
			var open_dialog = true;
			var args = {
				app_name: 'Fouquet',
				single: false,
				i18n: {
					select_all: i18n.select_all,
					deselect_all: i18n.deselect_all,
					add_btn: i18n.revoke
				},
				data: function (arg) {
					var teams = $.map ($.grep (that.data.attr.teams, function (team) { return (team.profile !== undefined); }), function (team) { var tmp = { rowid: team.rowid, ident: team.ident + ' <span class="team-profile">' + team.profile.ident + '</span>', profile: team.profile }; return (tmp); });
					return (teams);
				}
			};
			if (open_dialog) {
				that.$selector_dialog.dialog ({
					modal: true,
					width: 0.8 * $(window).width (),
					height: 0.8 * $(window).height (),
					resizable: true,
					title: i18n.select_teams,
					open: function () {
						that.$selector.empty ();
						that.$teamselect = $('<div class="revoke-dialog"></div>').appendTo (that.$selector).itemSelect (args);
					}
				});
				that.$revoke_teams_btn_cont.show ();
				that.$change_profile_teams_btn_cont.show ();
				that.$revoke_users_btn_cont.hide ();
				that.$change_profile_users_btn_cont.hide ();
			}
		}


		// --------------------------------------------------------
		// processTeamSelection
		// Perform an action (revoke, change profile) on a list of teams
		// --------------------------------------------------------
		function processTeamSelection (rowids, command) {
			MIOGA.logDebug ('FouquetMainPage', 1, '[processTeamSelection] Performing ' + command + ' on team rowids: ', rowids);
			this.$revoke_teams_btn_cont.hide ();
			this.$change_profile_teams_btn_cont.hide ();
			this.$dialog_close_btn_cont.hide ();
			var $btn_list = $('<ul class="button_list"></ul>');
			if (command === 'revoke') {
				that.$confirm_message = $('<div class="confirm-dialog"></div>').append ($('<p class="note"></p>').append (i18n.revoke_teams_confirm_message));
				$('<button class="button"></button>').append (i18n.revoke).appendTo ($('<li></li>').appendTo ($btn_list)).click (function () {
					revokeTeams.call (that, rowids);
					return (false);
				});
			}
			else if (command === 'change_profile') {
				that.$confirm_message = $('<div class="confirm-dialog"></div>').append ($('<p class="note"></p>').append (i18n.change_profile_teams_confirm_message));
				var $profile = $('<select class="profile-select"></select>').appendTo (that.$confirm_message);
				for (i=0; i<that.data.attr.profiles.length; i++) {
					var profile = that.data.attr.profiles[i];
					$profile.append ($('<option></option>').attr ('value', profile.rowid).append (profile.ident));
				}
				$('<button class="button"></button>').append (i18n.change_profile).appendTo ($('<li></li>').appendTo ($btn_list)).click (function () {
					var profile = {
						rowid: parseInt ($profile.val (), 10),
						ident: $profile.find ('option:selected').html ()
					};
					changeTeamsProfile.call (that, rowids, profile);
					var teams = $.map ($.grep (that.data.attr.teams, function (team) { return (team.profile !== undefined); }), function (team) { var tmp = { rowid: team.rowid, ident: team.ident + ' <span class="team-profile">' + team.profile.ident + '</span>', profile: team.profile }; return (tmp); });
					that.$teamselect.itemSelect ('replace_list', teams);
					return (false);
				});
			}

			// Append Cancel button and button list
			$('<button class="button cancel"></button>').append (i18n.cancel).appendTo ($('<li></li>').appendTo ($btn_list)).click (function () {
				that.$confirm_message.hide ();
				that.$teamselect.show ();
				that.$revoke_teams_btn_cont.show ();
				that.$change_profile_teams_btn_cont.show ();
				that.$dialog_close_btn_cont.show ();
				return (false);
			});
			$btn_list.appendTo (that.$confirm_message);

			// Add list of teams to confirm message
			var $list = $('<ul class="item-list"></ul>').appendTo (that.$confirm_message);
			for (i=0; i<that.data.attr.teams.length; i++) {
				if ($.grep (rowids, function (rowid) { return (rowid === that.data.attr.teams[i].rowid); }).length !== 0) {
					$('<li></li>').append (that.data.attr.teams[i].ident).appendTo ($list);
				}
			}

			// Show confirm message and hide team selector
			that.$selector.append (that.$confirm_message);
			that.$confirm_message.show ();
			that.$teamselect.hide ();
		}


		// --------------------------------------------------------
		// revokeTeams
		// Batch-revoke teams from a list of rowids
		// --------------------------------------------------------
		function revokeTeams (rowids) {
			// Remove profile information from selected teams
			for (i=0; i<that.data.attr.teams.length; i++) {
				if ($.grep (rowids, function (rowid) { return (rowid === that.data.attr.teams[i].rowid); }).length !== 0) {
					delete (that.data.attr.teams[i].profile);
				}
			}
			var res = that.data.store ();
			if (res.success) {
				message = i18n.revoke_teams_success;
				that.$message_box.inlineMessage ('info', message);
				var group = new FouquetGroup (res.group);
				that.refresh (group);
				that.$teamselect.itemSelect ('refresh_list');
				var teams = that.$teamselect.itemSelect ('get_list');
				if (teams.length === 0) {
					that.$confirm_message.replaceWith ($('<div class="confirm-dialog"></div>').append ($('<p class="note"></p>').append (i18n.no_more_teams_to_revoke)));
					that.$confirm_message.show ();
					that.$teamselect.hide ();
				}
				else {
					that.$confirm_message.hide ();
					that.$teamselect.show ();
				}
			}
			else {
				displayError.call (that, res);
			}
			that.$revoke_teams_btn_cont.show ();
			that.$change_profile_teams_btn_cont.show ();
		}


		// --------------------------------------------------------
		// revokeTeams
		// Batch-change teams profile from a list of rowids and a profile information hash
		// --------------------------------------------------------
		function changeTeamsProfile (rowids, profile) {
			// Remove profile information from selected teams
			for (i=0; i<that.data.attr.teams.length; i++) {
				if ($.grep (rowids, function (rowid) { return (rowid === that.data.attr.teams[i].rowid); }).length !== 0) {
					setProfile.call (that, 'team', that.data.attr.teams[i], profile);
				}
			}
			that.$teamselect.itemSelect ('refresh_list');
			var teams = that.$teamselect.itemSelect ('get_list');
			that.$confirm_message.hide ();
			that.$teamselect.show ();
			that.$revoke_teams_btn_cont.show ();
			that.$dialog_close_btn_cont.show ();
			that.$change_profile_teams_btn_cont.show ();
		}

		function displayError (res) {
			that.$message_box.inlineMessage ('error', res.message);
			$('span.error-message').remove ();
			$.each (res.errors, function (index, error) {
				MIOGA.logDebug ('FouquetMainPage', 2, '[displayError] Server returned error for field ' + error.field);
				var field = error.field.replace (/_id/, '');
				that.fields[field].append ('<span class="error-message">' + error.message + '</span>');
			});
		}


		// --------------------------------------------------------
		// togglePublicMode
		// Change public mode for an application
		// --------------------------------------------------------
		function togglePublicMode (rowid, mode) {
			MIOGA.logDebug ('FouquetMainPage', 1, '[togglePublicMode] Setting application ' + rowid + ' to ' + mode);

			// Call equivalent function from FouquetGroup
			var res = that.data.togglePublicMode (rowid, mode);
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : ((mode === 'public') ? i18n.application_public : i18n.application_private);
				that.$message_box.inlineMessage ('info', message);
			}
			else {
				displayError.call (that, res);
			}
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('FouquetMainPage', 1, '[new] Entering.');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };

		var i18n = options.i18n;

		var fouquet = options.fouquet;

		var defaults = {
			data: undefined,
			columns: {
				users: ['firstname', 'lastname', 'email', 'profile', 'last_connection'],
				teams: ['ident', 'profile', 'nb_users'],
				profiles: ['ident', 'nb_users', 'nb_teams']
			}
		};

		this.options =  $.extend (true, {}, defaults, options);

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="fouquet-summary"></div>').appendTo (this.elem);

		// Message box
		this.$message_box = $('<div></div>').inlineMessage ().appendTo (this.$cont);

		var $row = $('<div class="row"></div>').appendTo (this.$cont);

		// -------------------------------------------------------------
		// First column
		// -------------------------------------------------------------
		var $cell = $('<div class="span8"></div>').appendTo ($row);

		// Group details box
		var $content = $('<div class="content group-attributes"></div>').appendTo ($cell);
		var $table = $('<table class="group-attributes"></table>').appendTo ($content);
		// Place each field into a table line through a formatter
		$.each (Array ('ident', 'animator', 'disk_space_used', 'default_profile', 'lang', 'theme', 'default_app'), function (index, value) {
			var $line = $('<tr><th>' + i18n['group_attributes_' + value] + '</th></tr>').appendTo ($table);
			that.fields[value] = $('<td></td>').appendTo ($line);
		});

		// User list box
		var $box = $('<div class="sbox group-users"><h2 class="title_bg_color">' + i18n.group_users_title + '</h2></div>').foldable ().appendTo ($cell);
		var $contents = $('<div class="content"></div>').appendTo ($box);
		var $table_container = $('<div class="table-container"></div>').appendTo ($contents);
		var $table = $('<table class="highlight"></table>').appendTo ($table_container);
		var $head = $('<tr></tr>').appendTo ($('<thead></thead>').appendTo ($table));
		$.each (this.options.columns.users, function (index, value) {
			$('<th>' + i18n[value] + '</th>').addClass (value).append ($('<div class="sort-order-placeholder"></div>')).appendTo ($head);
		});
		this.fields.$users = $('<tbody></tbody>').appendTo ($table);
		$table_container.tableScrollableSortable ({
			sortable: {
				sorterOpt: {
					sortList: [[1,0]],
					widgets: ["resizable"],
					widgetOptions: { },
					widthFixed: false
				}
			}
		});
		// User invite button
		var $btn_bar = $('<ul class="button_list"></ul>').appendTo ($contents);
		this.$invite_users_btn = $('<button class="button"></button>').append (i18n.invite_users).appendTo ($('<li></li>').appendTo ($btn_bar)).click (function () {
			inviteUsers.call (that);
			return (false);
		});
		// User revoke button
		this.$multi_select_users_btn = $('<button class="button"></button>').append (i18n.multiple_select).appendTo ($('<li></li>').appendTo ($btn_bar)).click (function () {
			multipleUserSelect.call (that);
			return (false);
		});

		// Team list box
		var $box = $('<div class="sbox group-teams"><h2 class="title_bg_color">' + i18n.group_teams_title + '</h2></div>').foldable ().appendTo ($cell);
		var $contents = $('<div class="content"></div>').appendTo ($box);
		var $table_container = $('<div class="table-container"></div>').appendTo ($contents);
		var $table = $('<table class="highlight"></table>').appendTo ($table_container);
		var $head = $('<tr></tr>').appendTo ($('<thead></thead>').appendTo ($table));
		$.each (this.options.columns.teams, function (index, value) {
			$('<th>' + i18n[value] + '</th>').addClass (value).append ($('<div class="sort-order-placeholder"></div>')).appendTo ($head);
		});
		this.fields.$teams = $('<tbody></tbody>').appendTo ($table);
		$table_container.tableScrollableSortable ({
			sortable: {
				sorterOpt: {
					sortList: [[0,0]],
					widgets: ["resizable"],
					widgetOptions: { },
					widthFixed: false
				}
			}
		});
		// Team invite button
		var $btn_bar = $('<ul class="button_list"></ul>').appendTo ($contents);
		this.$invite_teams_btn = $('<button class="button"></button>').append (i18n.invite_teams).appendTo ($('<li></li>').appendTo ($btn_bar)).click (function () {
			inviteTeams.call (that);
			return (false);
		});
		// Team revoke button
		this.$multi_select_teams_btn = $('<button class="button"></button>').append (i18n.multiple_select).appendTo ($('<li></li>').appendTo ($btn_bar)).click (function () {
			multipleTeamSelect.call (that);
			return (false);
		});

		// -------------------------------------------------------------
		// Second column
		// -------------------------------------------------------------
		$cell = $('<div class="span4"></div>').appendTo ($row);

		// Profile list box
		var $box = $('<div class="sbox group-profiles"><h2 class="title_bg_color">' + i18n.group_profiles_title + '</h2></div>').foldable ().appendTo ($cell);
		var $contents = $('<div class="content"></div>').appendTo ($box);
		var $table = $('<table class="highlight"></table>').appendTo ($contents);
		$.each (this.options.columns.profiles, function (index, value) {
			$('<th>' + i18n[value] + '</th>').addClass (value).appendTo ($table);
		});
		this.fields.$profiles = $('<tbody></tbody>').appendTo ($table);
		$('<a class="button"></a>').attr ('href', fouquet.generateNavigationHash ('profile')).append (i18n.create_profile).appendTo ($('<li></li>').appendTo ($('<ul class="button_list"></ul>').appendTo ($contents)));

		// Resources list box
		var $box = $('<div class="sbox group-resources"><h2 class="title_bg_color">' + i18n.group_resources_title + '</h2></div>').foldable ().appendTo ($cell);
		var $contents = $('<div class="content"></div>').appendTo ($box);
		var $container = $('<div class="enable-resource"></div>').appendTo ($contents);
		this.fields.$resources = $('<ul class="list resources highlight"></ul>').appendTo ($contents);
		this.fields.$inactive_resources = $('<select></select>').appendTo ($container).change (function () {
			enableResource.call (that, parseInt ($(this).val (), 10));
		});

		// Applications list box
		var $box = $('<div class="sbox group-applications"><h2 class="title_bg_color">' + i18n.group_applications_title + '</h2></div>').foldable ().appendTo ($cell);
		var $contents = $('<div class="content"></div>').appendTo ($box);
		var $container = $('<div class="enable-application"></div>').appendTo ($contents);
		this.fields.$inactive_applications = $('<div class="inactive-applications"></div>').appendTo ($container);
		var $container = $('<div class="list-container"></div>').appendTo ($contents);
		this.fields.$applications = $('<ul class="list applications highlight"></ul>').appendTo ($container);


		// User and team selector
		this.$selector_dialog = $('<div></div>').appendTo (this.$cont).hide ();
		this.$selector = $('<div class="invite-dialog"></div>').appendTo (this.$selector_dialog);
		var $dialog_buttons = $('<ul class="button_list"></ul>').appendTo (this.$selector_dialog);
		this.$change_profile_users_btn_cont = $('<li></li>').appendTo ($dialog_buttons).hide ();
		$('<button class="button change_profile-users-btn"></button>').append (i18n.change_profile_users).appendTo (this.$change_profile_users_btn_cont).click (function () {
			var rowids = $.map ($.grep (that.$userSelect.userSelect ('get_list'), function (user) { return (user.selected); }), function (user) { return (user.rowid); });
			if (rowids.length > 0) {
				processUserSelection.call (that, rowids, 'change_profile');
			}
			return (false);
		});
		this.$revoke_users_btn_cont = $('<li></li>').appendTo ($dialog_buttons).hide ();
		$('<button class="button revoke-users-btn"></button>').append (i18n.revoke_users).appendTo (this.$revoke_users_btn_cont).click (function () {
			var rowids = $.map ($.grep (that.$userSelect.userSelect ('get_list'), function (user) { return (user.selected); }), function (user) { return (user.rowid); });
			if (rowids.length > 0) {
				processUserSelection.call (that, rowids, 'revoke');
			}
			return (false);
		});
		this.$change_profile_teams_btn_cont = $('<li></li>').appendTo ($dialog_buttons).hide ();
		$('<button class="button change_profile-teams-btn"></button>').append (i18n.change_profile_teams).appendTo (this.$change_profile_teams_btn_cont).click (function () {
			var rowids = $.map ($.grep (that.$teamselect.itemSelect ('get_list'), function (team) { return (team.selected); }), function (team) { return (team.rowid); });
			if (rowids.length > 0) {
				processTeamSelection.call (that, rowids, 'change_profile');
			}
			return (false);
		});
		this.$revoke_teams_btn_cont = $('<li></li>').appendTo ($dialog_buttons).hide ();
		$('<button class="button revoke-teams-btn"></button>').append (i18n.revoke_teams).appendTo (this.$revoke_teams_btn_cont).click (function () {
			var rowids = $.map ($.grep (that.$teamselect.itemSelect ('get_list'), function (team) { return (team.selected); }), function (team) { return (team.rowid); });
			if (rowids.length > 0) {
				processTeamSelection.call (that, rowids, 'revoke');
			}
			return (false);
		});
		this.$dialog_close_btn_cont = $('<li></li>').appendTo ($dialog_buttons);
		$('<button class="button cancel"></button>').append (i18n.close).appendTo (this.$dialog_close_btn_cont).click (function () {
			that.$selector_dialog.parents ('.ui-dialog').find (".ui-dialog-titlebar-close").trigger('click');
			return (false);
		});


		// Initialize FouquetGroup object and display it
		if (this.options.data !== undefined) {
			this.options.data = new FouquetGroup (this.options.data);
			this.refresh (this.options.data);
		}

		MIOGA.logDebug ('FouquetMainPage', 1, '[new] Leaving.');
	};
