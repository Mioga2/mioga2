// ========================================================
// FouquetGroup OO-interface to groups
// ========================================================
function FouquetGroup (options) {
	// ========================================================
	// Default values
	// ========================================================
	defaults = {
		ident: undefined,
		animator: undefined,
		disk_space_used: undefined,
		lang: undefined,
		theme: undefined,
		default_profile: undefined,
		users: undefined,
		teams: undefined,
		profiles: undefined
	},

	// ========================================================
	// Public methods
	// ========================================================

	// --------------------------------------------------------
	// fetch, fetch group from Mioga2
	// --------------------------------------------------------
	this.fetch = fetch;
	function fetch () {
		MIOGA.logDebug ('FouquetGroup', 2, '[fetch] Entering');

		this.attr = { };
		$.ajax ({
			url: 'GetGroup.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			success: function (data) {
				MIOGA.logDebug ('FouquetGroup', 3, '[fetch] GetGroup.json returned, data: ', data);
				that.attr = data;
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('FouquetGroup', 2, '[fetch] Leaving, data: ', that.attr);
	};

	// --------------------------------------------------------
	// store, store group to Mioga2
	// --------------------------------------------------------
	this.store = store;
	function store () {
		MIOGA.logDebug ('FouquetGroup', 1, '[store] Entering');

		// Basic attributes
		var group_attr = {
			rowid: this.attr.rowid,
			ident: this.attr.ident,
			default_profile_id: this.attr.default_profile.rowid,
			lang_id: this.attr.lang.rowid,
			theme_id: this.attr.theme.rowid,
			default_app_id: this.attr.default_app_id,
			users: [],
			teams: [],
			applications: [],
			resources: []
		};

		// Users
		for (i=0; i<this.attr.users.length; i++) {
			var user = this.attr.users[i];
			if (user.profile !== undefined) {
				group_attr.users.push (user.rowid);
			}
		}

		// Teams
		for (i=0; i<this.attr.teams.length; i++) {
			var team = this.attr.teams[i];
			if (team.profile !== undefined) {
				group_attr.teams.push (team.rowid);
			}
		}

		// Resources
		for (i=0; i<this.attr.resources.length; i++) {
			var app = this.attr.resources[i];
			if (app.selected) {
				group_attr.resources.push (app.rowid);
			}
		}

		// Applications
		for (i=0; i<this.attr.applications.length; i++) {
			var app = this.attr.applications[i];
			if (app.selected) {
				group_attr.applications.push (app.rowid);
			}
		}

		MIOGA.logDebug ('FouquetGroup', 2, '[store] Data to be posted: ', group_attr);

		// Post data to Mioga2
		var success = false;
		var message = "[FouquetGroup::store] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		var group = { rowid: this.attr.rowid };
		$.ajax ({
			url: 'SetGroup.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: group_attr,
			success: function (data) {
				MIOGA.logDebug ('FouquetGroup', 2, '[store] SetGroup.json POST returned, data: ', data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				group = data.group;
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('FouquetGroup', 1, '[store] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, group: group});
	};

	// --------------------------------------------------------
	// getNonMemberUsers, get list of non-member users from Mioga2
	// --------------------------------------------------------
	this.getNonMemberUsers = getNonMemberUsers;
	function getNonMemberUsers (args) {
		MIOGA.logDebug ('FouquetGroup', 2, '[getNonMemberUsers] Entering, args: ', args);

		var users = [ ];
		$.ajax ({
			url: 'GetNonMemberUsers.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			data: args,
			success: function (data) {
				MIOGA.logDebug ('FouquetGroup', 3, '[getNonMemberUsers] GetNonMemberUsers.json returned, data: ', data);
				users = data;
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('FouquetGroup', 2, '[getNonMemberUsers] Leaving, data: ', users);
		return (users);
	};

	// --------------------------------------------------------
	// getNonMemberTeams, get list of non-member teams from Mioga2
	// --------------------------------------------------------
	this.getNonMemberTeams = getNonMemberTeams;
	function getNonMemberTeams () {
		MIOGA.logDebug ('FouquetGroup', 2, '[getNonMemberTeams] Entering, args: ');

		var teams = [ ];
		$.ajax ({
			url: 'GetNonMemberTeams.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			success: function (data) {
				MIOGA.logDebug ('FouquetGroup', 3, '[getNonMemberTeams] GetNonMemberTeams.json returned, data: ', data);
				teams = data;
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('FouquetGroup', 2, '[getNonMemberTeams] Leaving, data: ', teams);
		return (teams);
	};


	// --------------------------------------------------------
	// togglePublicMode, set application to private or public
	// --------------------------------------------------------
	this.togglePublicMode = togglePublicMode;
	function togglePublicMode (rowid, mode) {
		MIOGA.logDebug ('FouquetGroup', 1, '[togglePublicMode] Entering for application ' + rowid + ', mode: ' + mode);

		var app = $.grep (that.attr.applications, function (a) { return (a.rowid === rowid); })[0];

		var post_data = {
			rowid: rowid,
			mode: mode
		};

		// Post data to Mioga2
		var success = false;
		var message = "[FouquetGroup::togglePublicMode] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];

		// Ensure application can be set to public
		if (app.can_be_public) {
			$.ajax ({
				url: 'SetApplicationPublic.json',
				type: 'POST',
				async: false,
				traditional: true,
				dataType: 'json',
				data: post_data,
				success: function (data) {
					MIOGA.logDebug ('FouquetGroup', 2, '[togglePublicMode] SetApplicationPublic.json POST returned, data: ', data);
					success = data.success;
					message = data.message;
					errors = data.errors;

					// Update application access mode
					if (success) {
						app.is_public = (mode === 'public') ? true : false;
					}
				},
				error: function () {
					MIOGA.logError ('Internal error', true);
				}
			});
		}

		MIOGA.logDebug ('FouquetGroup', 1, '[togglePublicMode] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors});
	};


	// ========================================================
	// Constructor
	// ========================================================
	MIOGA.logDebug ('FouquetGroup', 1, '[new] Entering, options: ', options);
	var that = this;

	this.attr = $.extend (true, {}, this.defaults, options);

	if ((options === undefined) || (Object.keys (options).length === 0)) {
		this.fetch ();
	}

	MIOGA.logDebug ('FouquetGroup', 1, '[new] Leaving, data: ', that.attr);
}
