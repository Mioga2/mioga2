	// ========================================================================================================
	// FouquetProfileEdit object, allows to manipulate fouquetProfileEdit UI
	// ========================================================================================================
	function FouquetProfileEdit (elem, options) {


		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data, mode) {
			MIOGA.logDebug ('FouquetProfileEdit', 1, '[refresh] Entering, data: ', data);
			var that = this; // For callback reference to current object
			this.profile = data;

			if (!data.attr.rowid) {
				mode = 'edit';
			}

			// Main title
			that.$title.empty ().append (data.attr.ident);

			// Identity
			$.each (Array ('ident'), function (index, value) {
				that.fields[value].empty ();
				that.fields[value].append (formatValue.call (that, data.attr, value, 'profile'));
			});

			if ((mode === undefined) || (mode === 'view')) {
				// View mode
				synthesisFillMembers.call (that, that.profile.attr);
				synthesisFillRights.call (that, that.profile.attr.rights);

				// Show "edit" button and hide "save" and "cancel" buttons
				this.$edit_btn_cont.show ();
				this.$save_btn_cont.hide ();
				this.$cancel_btn_cont.hide ();

				// Show "delete" button if no user / team is affected to profile
				if ((data.attr.users.length === 0) && (data.attr.teams.length === 0)) {
					this.$delete_btn_cont.show ();
				}
				else {
					this.$delete_btn_cont.hide ();
				}

				// Remove class to container node
				this.$cont.removeClass ('editor');

				this.editor.$cont.hide ();
				this.synthesis.$cont.show ();
			}
			else {
				// Edit mode
				editorFillRights.call (that, that.profile.attr.rights);

				// Hide "edit" and "delete" buttons and show "save" and "cancel" buttons
				this.$edit_btn_cont.hide ();
				this.$delete_btn_cont.hide ();
				this.$save_btn_cont.show ();
				this.$cancel_btn_cont.show ();

				// Add class to container node
				this.$cont.addClass ('editor');

				this.synthesis.$cont.hide ();
				this.editor.$cont.show ();
			}

			MIOGA.logDebug ('FouquetProfileEdit', 1, '[refresh] Leaving');
		};


		// --------------------------------------------------------
		// Fill members for profile in view mode
		// --------------------------------------------------------
		function synthesisFillMembers (data) {
			var that = this;

			MIOGA.logDebug ('FouquetProfileEdit', 1, '[synthesisFillMembers] Entering, data: ', data);

			// User list
			that.synthesis.$users.empty ();
			var $none = $('<span class="note"></span>').append (i18n.no_user_for_profile).appendTo (that.synthesis.$users);
			if ((data.users !== undefined) && (data.users.length)) {
				$.each (data.users, function (index, user) {
					user.label = user.firstname + ' ' + user.lastname + ' (' + user.email + ')';
					var $elem = $('<li></li>').append (formatValue.call (that, user, 'label', 'user'));
					that.synthesis.$users.append ($elem);
					if ($none) {
						$none.remove ();
						$none = undefined;
					}
				});
			}

			// Team list
			that.synthesis.$teams.empty ();
			var $none = $('<span class="note"></span>').append (i18n.no_team_for_profile).appendTo (that.synthesis.$teams);
			if ((data.teams !== undefined) && (data.teams.length)) {
				$.each (data.teams, function (index, team) {
					var $elem = $('<li></li>').append (formatValue.call (that, team, 'ident', 'team'));
					that.synthesis.$teams.append ($elem);
					if ($none) {
						$none.remove ();
						$none = undefined;
					}
				});
			}

			MIOGA.logDebug ('FouquetProfileEdit', 1, '[synthesisFillMembers] Leaving');
		};


		// --------------------------------------------------------
		// Fill filesystem and applications rights in view mode
		// --------------------------------------------------------
		function synthesisFillRights (data) {
			MIOGA.logDebug ('FouquetProfileEdit', 1, '[synthesisFillRights] Entering, data: ', data);
			var that = this;

			// Filesystem rights
			this.synthesis.$filesystem.empty ();
			if ((data.filesystem !== undefined) && (data.filesystem.length)) {
				data.filesystem.sort (function (a, b) {
					return ((a.path < b.path) ? -1 : 1);
				});
				$.each (data.filesystem, function (index, value) {
					// Set classname from access right
					var className = undefined;
					switch (value.profile) {
						case 2:
							className = 'read-write';
							break;
						case 1:
							className = 'read-only';
							break;
						case 0:
							className = 'no-right';
							break;
					}

					// Create and append DOM
					var $item = $('<li></li>').append (value.path).addClass (className).appendTo (that.synthesis.$filesystem);
				});
			}

			// Applications rights
			this.synthesis.$applications.empty ();
			// Create and append DOM node
			if ((data.applications !== undefined) && (data.applications.length)) {
				data.applications.sort (function (a, b) {
					return ((a.label < b.label) ? -1 : 1);
				});
				$.each (data.applications, function (index, app) {
					var $app = $('<li></li>').addClass ('application').append (app.label);
					$('<div class="top-link"></div>').appendTo ($app).click (function () {
						$(document).scrollTop (0);
						return (false);
					});
					$('<div></div>').addClass ('icon').append ('<img src="' + mioga_context.image_uri + '/48x48/apps/' + app.ident.toLowerCase () + '.png" alt="icon"/>').appendTo ($app);
					var $functions = $('<ul class="highlight"></ul>').addClass ('functions').appendTo ($app);
					app.functions.sort (function (a, b) {
						return ((a.label < b.label) ? -1 : 1);
					});
					var has_funct = false;
					$.each (app.functions, function (index, funct) {
						if (funct.selected) {
							has_funct = true;
							$('<li></li>').append (funct.label).appendTo ($functions);
						}
					});
					if (has_funct) {
						$app.appendTo (that.synthesis.$applications);
					}
				});
			}

			this.synthesis.$applications.parent ().parent ().show ();

			MIOGA.logDebug ('FouquetProfileEdit', 1, '[synthesisFillRights] Leaving');
		};


		// --------------------------------------------------------
		// Fill filesystem and applications rights in edit mode
		// --------------------------------------------------------
		function editorFillRights (data) {
			MIOGA.logDebug ('FouquetProfileEdit', 1, '[editorFillRights] Entering, data: ', data);
			var that = this;

			// Applications rights
			this.editor.$applications.empty ();
			this.editor.$applications.find ('input:checked').attr ('checked', false);
			// Create and append DOM node
			if ((data.applications !== undefined) && (data.applications.length)) {
				data.applications.sort (function (a, b) {
					return ((a.label < b.label) ? -1 : 1);
				});
				$.each (data.applications, function (app_index, app) {
					var $app = $('<li></li>').addClass ('application').append (app.label).appendTo (that.editor.$applications);
					$('<div class="top-link"></div>').appendTo ($app).click (function () {
						$(document).scrollTop (0);
						return (false);
					});
					$('<div></div>').addClass ('icon').append ('<img src="' + mioga_context.image_uri + '/48x48/apps/' + app.ident.toLowerCase () + '.png" alt="icon"/>').appendTo ($app);
					var $functions = $('<ul class="highlight"></ul>').addClass ('functions').appendTo ($app);
					if ((app.functions !== undefined) && (app.functions.length)) {
						app.functions.sort (function (a, b) {
							return ((a.label < b.label) ? -1 : 1);
						});
						$.each (app.functions, function (funct_index, funct) {
							var $cb = $('<input type="checkbox"></input>').data ('data_pos', [app_index, funct_index]);	// data_pos is used in "change" callback below for direct access to function in nested data structure
							if (funct.selected) {
								$cb.attr ('checked', true);
							}
							var $label = $('<label></label>').append ($cb.data ('rowid', funct.rowid)).append (funct.label);
							$label.addClass (funct.selected ? 'checked' : '');
							$('<li></li>').append ($label).appendTo ($functions);
						});
					}
				});
			}

			// Register callback to checkbox change
			this.editor.$applications.find ('input').change (function () {
				var $this = $(this);
				var data_pos = $this.data ('data_pos');
				var app_index = data_pos[0];
				var funct_index = data_pos[1];
				var funct = that.profile.attr.rights.applications[app_index].functions[funct_index];
				funct.selected = $this.attr ('checked') ? true : false;
				$(this).parent ().toggleClass ('changed').toggleClass ('checked');
			});

			this.editor.$applications.parent ().parent ().show ();

			MIOGA.logDebug ('FouquetProfileEdit', 1, '[editorFillRights] Leaving');
		};

		// --------------------------------------------------------
		// formatValue
		// Given an object (hash) the name of a field and an object type,
		// format inner data for field display an interaction (click events, etc.)
		// --------------------------------------------------------
		function formatValue (object, field, object_type) {
			var that = this;

			// Default behavior is to return the field as-is
			var value = object[field];

			// Some fields need a specific contruction rule
			if ((field === 'ident') && (object_type !== 'profile')) {
				value = $('<a></a>').attr ('href', fouquet.generateNavigationHash (object_type, object.rowid)).append (object.ident);
			}
			else if ((field === 'ident') && (object_type === 'profile')) {
				value = $('<span class="inline-edit"></span>').append (object.ident).click (function () {
					if ($.find ('div.fouquet-profile-synthesis.editor').length) {
						var $cont = $('<div class="inline-edit-form-cont"></div>');
						var $input = $('<input type="text"></input>').attr ('value', object.ident).appendTo ($cont);
						$('<div class="save-icon"></div>').appendTo ($cont).click (function () {
							that.profile.attr.ident = $input.val ();
							$cont.replaceWith (formatValue.call (that, that.profile.attr, 'ident', 'profile'));
						});
						$('<div class="cancel-icon"></div>').appendTo ($cont).click (function () {
							$cont.replaceWith (formatValue.call (that, that.profile.attr, 'ident', 'profile'));
						});
						$(this).replaceWith ($cont);
						$input.focus ();
					}
				});
			}
			else if (field === 'label') {
				// User label formatter, link with click handler
				// User label is made of firstname, lastname and e-mail
				value = $('<a></a>').attr ('href', fouquet.generateNavigationHash (object_type, object.rowid)).append (object.firstname + ' ' + object.lastname + ' (' + object.email + ')');
			}

			return (value);
		};

		// --------------------------------------------------------
		// editProfile
		// Switch to profile edition mode
		// --------------------------------------------------------
		function editProfile () {
			MIOGA.logDebug ('FouquetProfileEdit', 1, '[editProfile] Entering');

			// Load profile for edition and refresh view
			this.profile = new FouquetProfile ({rowid: this.profile.attr.rowid, full: true});
			this.refresh (this.profile, 'edit');

			// Hide "edit" button and show "save" and "cancel" buttons
			this.$edit_btn_cont.hide ();
			this.$save_btn_cont.show ();
			this.$cancel_btn_cont.show ();

			// Hide synthesis view and show editor
			this.synthesis.$cont.hide ();
			this.editor.$cont.show ();

			this.$cont.addClass ('editor');

			MIOGA.logDebug ('FouquetProfileEdit', 1, '[editProfile] Leaving');
		};

		// --------------------------------------------------------
		// deleteProfile
		// Delete current profile
		// --------------------------------------------------------
		function deleteProfile () {
			MIOGA.logDebug ('FouquetProfileEdit', 1, '[deleteProfile] Entering');

			var res = this.profile.deleteProfile ();
			if (res.success) {
				// Profile doesn't exist anymore, redirect to home page
				window.location.href = 'DisplayMain';
			}
			else {
				displayError.call (that, res);
			}

			MIOGA.logDebug ('FouquetProfileEdit', 1, '[deleteProfile] Leaving');
		};

		// --------------------------------------------------------
		// cancelEdit
		// Cancel profile edition and display synthesis
		// --------------------------------------------------------
		function cancelEdit () {
			MIOGA.logDebug ('FouquetProfileEdit', 1, '[cancelEdit] Entering');

			if (confirm (i18n.confirm_cancel_profile_edit)) {
				this.refresh (this.profile, 'view');
			}

			MIOGA.logDebug ('FouquetProfileEdit', 1, '[cancelEdit] Leaving');
		}

		// --------------------------------------------------------
		// displayError
		// display error message from WebService response
		// --------------------------------------------------------
		function displayError (res) {
			that.$message_box.inlineMessage ('error', res.message);
			$('span.error-message').remove ();
			$.each (res.errors, function (index, error) {
				MIOGA.logDebug ('FouquetProfileEdit', 2, '[displayError] Server returned error for field ' + error.field);
				var field = error.field.replace (/_id/, '');
				that.fields[field].append ('<span class="error-message">' + error.message + '</span>');
			});
		}

		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('FouquetProfileEdit', 1, '[new] Entering');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };

		var fouquet = options.fouquet;

		options =  $.extend (true, {}, defaults, options);
		this.options    = options;

		this.data = options.data;

		// Stack
		this.stack = options.stack;

		var i18n = options.i18n;

		// --------------------------------------------------------
		// Internally-used containers
		// --------------------------------------------------------
		this.synthesis = { };
		this.editor = { };

		// --------------------------------------------------------
		// Create empty UI
		// --------------------------------------------------------
		this.$cont = $('<div class="fouquet-profile-synthesis"></div>').appendTo (this.elem);

		// Message box
		this.$message_box = $('<div></div>').inlineMessage ().appendTo (this.$cont);

		var $row = $('<div class="row"></div>').appendTo (this.$cont);

		// Left pane
		$cell = $('<div class="span4"></div>').appendTo ($row);

		// Identity box
		var $container = $('<div class="attribute-box"></div>').appendTo ($cell);
		var $title_box = $('<div class="profile-title"></div>').appendTo ($container);
		this.$title = $('<h1></h1>').appendTo ($title_box);
		var $content = $('<div class="content identity-box"></div>').appendTo ($container);
		var $table = $('<table class="profile-identity"></table>').appendTo ($content);
		// Place each field into a table line through a formatter
		$.each (Array ('ident'), function (index, value) {
			var $line = $('<tr><th>' + i18n[value] + '</th></tr>').appendTo ($table);
			that.fields[value] = $('<td></td>').appendTo ($line);
		});

		// Users box
		var $box = $('<div class="sbox"><h2 class="title_bg_color">' + i18n.users_title + '</h2></div>').foldable ().appendTo ($cell);
		var $content = $('<div class="content"></div>').appendTo ($box);
		this.synthesis.$users = $('<ul class="list highlight"></ul>').appendTo ($content);

		// Teams box
		var $box = $('<div class="sbox"><h2 class="title_bg_color">' + i18n.teams_title + '</h2></div>').foldable ().appendTo ($cell);
		var $content = $('<div class="content"></div>').appendTo ($box);
		this.synthesis.$teams = $('<ul class="list highlight"></ul>').appendTo ($content);

		// Filesystem rights box
		var $box = $('<div class="sbox"><h2 class="title_bg_color">' + i18n.filesystem_title + '</h2></div>').foldable ().appendTo ($cell);
		var $content = $('<div class="content"></div>').appendTo ($box);
		$content.append ($('<ul class="legend rights"><li class="read-write">' + i18n.read_write + '</li><li class="read-only">' + i18n.read_only + '</li><li class="no-right">' + i18n.no_right + '</li></ul>'));
		this.synthesis.$filesystem = $('<ul class="rights list highlight"></ul>').appendTo ($content);

		// --------------------------------------------------------
		// Synthesis
		// --------------------------------------------------------

		var $cell = $('<div class="span8"></div>').appendTo ($row);

		// Edit button
		var $button_bar = $('<ul class="button_list"></ul>').appendTo ($cell);
		this.$edit_btn_cont = $('<li></li>').appendTo ($button_bar);
		this.$edit_btn = $('<button class="button"></button>').append (i18n.edit).appendTo (this.$edit_btn_cont).click (function () {
			editProfile.call (that);
		});

		// Delete button
		this.$delete_btn_cont = $('<li></li>').appendTo ($button_bar);
		this.$delete_btn = $('<button class="button"></button>').append (i18n.delete_profile).appendTo (this.$delete_btn_cont).click (function () {
			deleteProfile.call (that);
		});

		// Save and cancel buttons
		this.$save_btn_cont = $('<li></li>').appendTo ($button_bar);
		this.$save_btn = $('<button class="button"></button>').append (i18n.save).appendTo (this.$save_btn_cont).click (function () {
			var profile_update = (that.profile.attr.rowid !== undefined) ? true : false;
			var res = that.profile.store ();
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.profile_update_success;
				that.$message_box.inlineMessage ('info', message);
				var profile = new FouquetProfile (res.profile);
				if (profile_update) {
					that.refresh (profile);
				}
				else {
					var target = fouquet.generateNavigationHash ('profile', profile.attr.rowid);
					window.location.href = target;
				}
			}
			else {
				displayError.call (that, res);
			}
		});
		this.$cancel_btn_cont = $('<li></li>').appendTo ($button_bar);
		this.$cancel_btn = $('<button class="button cancel"></button>').append (i18n.cancel).appendTo (this.$cancel_btn_cont).click (function () {
			cancelEdit.call (that);
		});

		this.synthesis.$cont = $('<div></div>').appendTo ($cell);

		// Applications rights box
		var $box = $('<div class="sbox"><h2 class="title_bg_color">' + i18n.applications_title + '</h2></div>').foldable ().appendTo (this.synthesis.$cont);
		var $content = $('<div class="content"></div>').appendTo ($box);
		this.synthesis.$applications = $('<ul></ul>').addClass ('applications-rights').appendTo ($content);


		// --------------------------------------------------------
		// Editor
		// --------------------------------------------------------
		this.editor.$cont = $('<div></div>').appendTo ($cell).hide ();

		// Application rights list
		this.editor.$applications_tab = $('<div></div>').appendTo (this.editor.$cont);
		var $box = $('<div class="sbox"><h2 class="title_bg_color">' + i18n.applications_title + '</h2></div>').appendTo (this.editor.$applications_tab);
		var $content = $('<div class="content"></div>').appendTo ($box);
		this.editor.$applications = $('<ul></ul>').addClass ('applications-rights').appendTo ($content);

		// --------------------------------------------------------
		// If the profile to display is already known, then refresh view
		// --------------------------------------------------------
		if (options.profile !== undefined) {
			this.refresh (options.profile);
		}

		MIOGA.logDebug ('FouquetProfileEdit', 1, '[new] Leaving');
	};
