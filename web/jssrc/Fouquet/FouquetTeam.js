// ========================================================
// FouquetTeam OO-interface to teams
// ========================================================
function FouquetTeam (options) {
	// ========================================================
	// Default values
	// ========================================================
	defaults = {
		rowid: undefined,
		ident: undefined,
		profile: undefined,
		users: undefined
	},

	// ========================================================
	// Public methods
	// ========================================================

	// --------------------------------------------------------
	// fetch, fetch team from Mioga2
	// --------------------------------------------------------
	this.fetch = fetch;
	function fetch () {
		MIOGA.logDebug ('FouquetTeam', 2, '[fetch] Entering, rowid: ' + this.attr.rowid);
		var that = this;

		$.ajax ({
			url: 'GetTeam.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			data: { rowid: this.attr.rowid },
			success: function (data) {
				MIOGA.logDebug ('FouquetTeam', 3, '[fetch] GetUser.json returned, data: ', data);
				that.attr = data;
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('FouquetTeam', 2, '[fetch] Leaving, data: ', this.attr);
	};

	// --------------------------------------------------------
	// store, store team to Mioga2
	// --------------------------------------------------------
	this.store = store;
	function store () {
		MIOGA.logDebug ('FouquetTeam', 1, '[store] Entering');

		// Basic attributes
		var team_attr = {
			rowid: this.attr.rowid,
			profile: this.attr.profile.rowid
		};

		MIOGA.logDebug ('FouquetTeam', 2, '[store] Data to be posted: ', team_attr);

		// Post data to Mioga2
		var success = false;
		var message = "[FouquetTeam::store] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		var team = {rowid: this.attr.rowid};	// For further loading from WebService response
		$.ajax ({
			url: 'SetTeam.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: team_attr,
			success: function (data) {
				MIOGA.logDebug ('FouquetTeam', 2, '[store] SetTeam.json POST returned, data: ', data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.team) {
					team = data.team;
				}
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('FouquetTeam', 1, '[store] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, team: team});
	};


	// ========================================================
	// Constructor
	// ========================================================
	MIOGA.logDebug ('FouquetTeam', 1, '[new] Entering, options: ', options);
	var that = this;

	this.attr = $.extend (true, {}, this.defaults, options);

	// Fetch team data from Mioga2 if required
	if ((Object.keys (this.attr).length === 1) && (this.attr.rowid !== undefined)) {
		// Object attributes has a single key (rowid), fetch data from Mioga2
		this.fetch ();
	}
	else {
		// Object attribute is supposed to a complete team description, no need to fetch data
		this.attr = options;
	}

	MIOGA.logDebug ('FouquetTeam', 1, '[new] Leaving, data: ', that.attr);
}
