	// ========================================================================================================
	// FouquetTeamEdit object, allows to manipulate fouquetTeamEdit UI
	// ========================================================================================================
	function FouquetTeamEdit (elem, options) {

		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data) {
			MIOGA.logDebug ('FouquetTeamEdit', 1, '[refresh] Entering, data: ', data);
			var that = this; // For callback reference to current object

			that.data = data;

			// Main title
			that.$title.empty ().append (data.attr.ident);

			// Identity
			$.each (Array ('ident', 'profile'), function (index, field) {
				that.fields[field].empty ();
				that.fields[field].append (formatValue.call (that, data.attr, field, 'team'));
			});

			// --------------------------------------------------------
			// Menu to select view mode
			// --------------------------------------------------------
			this.$actions.empty ();

			// Synthesis view
			var $synthesis = $('<li><a>' + i18n.synthesis_view + '</a></li>').appendTo (this.$actions).click (function () {
				$(this).addClass ('selected').siblings ().removeClass ('selected');
				fillRights.call (that, data.attr);
			});

			// Member list
			that.$members.empty ();
			var $none = $('<span class="note"></span>').append (i18n.no_user_for_team).appendTo (that.$members);
			if ((data.attr.users !== undefined) && (data.attr.users.length)) {
				$.each (data.attr.users, function (index, user) {
					user.label = user.firstname + ' ' + user.lastname + ' (' + user.email + ')';
					var $elem = $('<li></li>').append (formatValue.call (that, user, 'label', 'user'));
					that.$members.append ($elem);
					if ($none) {
						$none.remove ();
						$none = undefined;
					}
				});
			}

			// Team-specific rights, if applicable
			var specific_rights = $.grep (data.attr.filesystem, function (item) { return (item.team !== undefined); });
			if (specific_rights.length) {
				$('<li><a>' + i18n.team_specific + '</a></li>').appendTo (this.$actions).click (function () {
					$(this).addClass ('selected').siblings ().removeClass ('selected');
					var rights = {
						filesystem: specific_rights
					};
					fillRights.call (that, rights);
				});
			}

			// Profile details
			if (data.attr.profile !== undefined) {
				$('<li><a>' + i18n.profile + ' &quot;' + data.attr.profile.ident + '&quot;</a></li>').appendTo (this.$actions).click (function () {
					$(this).addClass ('selected').siblings ().removeClass ('selected');

					// Produce some kind of grep on data.attr.filesystem to filter non-profile keys
					var rights = {
						filesystem: [],
						applications: []
					};
					$.each (data.attr.filesystem, function (index, value) {
						if (value.profile !== undefined) {
							var entry = {
								path: value.path,
								profile: value.profile
							};
							rights.filesystem.push (entry);
						}
					});
					rights.applications = data.attr.applications;
					fillRights.call (that, rights);
				});
			}

			// Default view is synthesis
			$synthesis.trigger ('click');

			MIOGA.logDebug ('FouquetTeamEdit', 1, '[refresh] Leaving.');
		}


		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// formatValue
		// Given an object (hash) the name of a field and an object type,
		// format inner data for field display an interaction (click events, etc.)
		// --------------------------------------------------------
		function formatValue (object, field, object_type) {
			var that = this;

			// Default behavior is to return the field as-is
			var value = object[field];

			// Some fields need a specific contruction rule
			if (field === 'profile') {
				// Profile formatter, link to profile
				value = $('<div></div>').addClass ('inline-chooser').append ($('<a></a>').attr ('href', fouquet.generateNavigationHash ('profile', object[field].rowid)).append (object[field].ident));

				// Dropdown menu to change profile
				var $dropdown = $('<ul class="dropdown"></ul>').appendTo (value);
				var $entry = $('<li class="dir"></li>').append ('&#x25BC;').appendTo ($dropdown);
				var $submenu = $('<ul></ul>').appendTo ($entry);

				$.each (that.data.attr.profiles, function (index, profile) {
					if (profile.ident !== object[field].ident) {
						var $link = $('<a></a>').append (profile.ident);
						var $entry = $('<li></li>').append ($link).appendTo ($submenu).click (function () {
							// Update team's profile and store to Mioga2
							that.data.attr.profile.rowid = profile.rowid;
							that.data.attr.profile.ident = profile.ident;
							var res = that.data.store ();
							if (res.success) {
								var message = (res.message !== undefined) ? res.message : i18n.profile_change_success;
								that.$message_box.inlineMessage ('info', message);

								// Refresh data from new team description returned by WebService
								var team = new FouquetTeam (res.team);
								that.refresh (team);
							}
							else {
								that.$message_box.inlineMessage ('error', res.message);
							}
						});
					}
				});
			}
			else if (field === 'label') {
				// User label formatter, link with click handler
				// User label is made of firstname, lastname and e-mail
				value = $('<a></a>').attr ('href', fouquet.generateNavigationHash (object_type, object.rowid)).append (object.firstname + ' ' + object.lastname + ' (' + object.email + ')');
			}

			return (value);
		};


		// --------------------------------------------------------
		// Fill filesystem and applications rights
		// --------------------------------------------------------
		function fillRights (data) {
			MIOGA.logDebug ('FouquetTeamEdit', 1, '[fillRights] Entering, data: ', data);
			var that = this;

			// Filesystem rights
			this.$filesystem.empty ();
			if ((data.filesystem !== undefined) && data.filesystem.length) {
				data.filesystem.sort (function (a, b) {
					return ((a.path < b.path) ? -1 : 1);
				});
				$.each (data.filesystem, function (index, value) {
					// Get max access right
					var access = (value.team !== undefined) ? value.team : value.profile;

					// Do not record path with undefined access rights, for example when drawing rights for a team on a path that has no specific right for this team profile
					if (access !== undefined) {
						// Set classname from access right
						var className = undefined;
						switch (access) {
							case 2:
								className = 'read-write';
								break;
							case 1:
								className = 'read-only';
								break;
							case 0:
								className = 'no-right';
								break;
						}

						// Create and append DOM
						var $item = $('<li></li>').append (value.path).addClass (className).appendTo (that.$filesystem);
					}
				});

				this.$filesystem.parent ().parent ().show ();
			}
			else {
				this.$filesystem.parent ().parent ().hide ();
			}

			// Applications rights
			this.$applications.empty ();
			if ((data.applications !== null) && (data.applications !== undefined) && (Object.keys (data.applications).length)) {
				// Create full list (from profile and teams)
				var full_list = [];
				$.each (data.applications, function (index, team) {
					$.each (team.rights, function (index, app) {
						full_list.push (app);
					});
				});

				// Create and append DOM node
				full_list.sort (function (a, b) {
					return ((a.label < b.label) ? -1 : 1);
				});
				$.each (full_list, function (index, app) {
					var $app = $('<li></li>').addClass ('application').append (app.label).appendTo (that.$applications);
					$('<div></div>').addClass ('icon').append ('<img src="' + mioga_context.image_uri + '/48x48/apps/' + app.ident.toLowerCase () + '.png" alt="icon"/>').appendTo ($app);
					var $functions = $('<ul class="highlight"></ul>').addClass ('functions').appendTo ($app);
					app.functions.sort (function (a, b) {
						return ((a.label < b.label) ? -1 : 1);
					});
					$.each (app.functions, function (index, funct) {
						$('<li></li>').append (funct.label).appendTo ($functions);
					});
				});

				this.$applications.parent ().parent ().show ();
			}
			else {
				this.$applications.parent ().parent ().hide ();
			}

			MIOGA.logDebug ('FouquetTeamEdit', 1, '[fillRights] Leaving.');
		};

		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('FouquetTeamEdit', 1, '[new] Entering.');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };

		var fouquet = options.fouquet;

		options =  $.extend (true, {}, defaults, options);
		this.options    = options;

		this.data = options.data;

		// Stack
		this.stack = options.stack;

		var i18n = options.i18n;

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="fouquet-team-synthesis"></div>').appendTo (this.elem);

		// Message box
		this.$message_box = $('<div></div>').inlineMessage ().appendTo (this.$cont);

		var $row = $('<div class="row"></div>').appendTo (this.$cont);

		// Identity box
		$cell = $('<div class="span4"></div>').appendTo ($row);
		var $container = $('<div class="attribute-box"></div>').appendTo ($cell);
		var $title_box = $('<div class="team-title"></div>').appendTo ($container);
		this.$title = $('<h1></h1>').appendTo ($('<div class="title"></div>').appendTo ($title_box));
		var $content = $('<div class="content identity-box"></div>').appendTo ($container);
		var $table = $('<table class="team-identity"></table>').appendTo ($content);
		// Place each field into a table line through a formatter
		$.each (Array ('ident', 'profile'), function (index, value) {
			var $line = $('<tr><th>' + i18n[value] + '</th></tr>').appendTo ($table);
			that.fields[value] = $('<td></td>').appendTo ($line);
		});

		this.$actions = $('<ul class="actions vmenu"></ul>').appendTo ($cell);
		$('<div></div>').append (i18n.team_tabs_help).addClass ('help-notice').appendTo ($cell);

		// Members box
		var $box = $('<div class="sbox"><h2 class="title_bg_color">' + i18n.team_members + '</h2></div>').foldable ().appendTo ($cell);
		var $content = $('<div class="content"></div>').appendTo ($box);
		this.$members = $('<ul class="list highlight"></ul>').appendTo ($content);

		// Access rights
		var $cell = $('<div class="span8"></div>').appendTo ($row);

		// Filesystem rights box
		var $box = $('<div class="sbox"><h2 class="title_bg_color">' + i18n.filesystem_title + '</h2></div>').foldable ().appendTo ($cell);
		var $content = $('<div class="content"></div>').appendTo ($box);
		$content.append ($('<ul class="legend rights"><li class="read-write">' + i18n.read_write + '</li><li class="read-only">' + i18n.read_only + '</li><li class="no-right">' + i18n.no_right + '</li></ul>'));
		this.$filesystem = $('<ul class="rights list highlight"></ul>').appendTo ($content);

		// Applications rights box
		var $box = $('<div class="sbox"><h2 class="title_bg_color">' + i18n.applications_title + '</h2></div>').foldable ().appendTo ($cell);
		var $content = $('<div class="content"></div>').appendTo ($box);
		this.$applications = $('<ul></ul>').addClass ('applications-rights').appendTo ($content);

		// If the team to display is already known, then refresh view
		if (options.team !== undefined) {
			this.refresh (options.team);
		}

		MIOGA.logDebug ('FouquetTeamEdit', 1, '[new] Leaving.');
	};
