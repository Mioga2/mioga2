// ==============================================================
//
// AppProgressBar
//    Create element for masquing application initialisation
//    and give information to process advance.
//
// ==============================================================

function AppProgressBar(elem, options) {

	this.elem = elem; // parent element
	this.i18n = options.i18n;

	// Create the DOM elements
	var that = this; // For callback reference to current object
	this.$pbcont = $('<div class="app-progressbar-cont"></div>').appendTo(this.elem);
	$('<h1>'+this.i18n.displayText+'</h1>').appendTo(this.$pbcont);
	this.$pb = $('<div class="app-progressbar"></div>').progressbar({value : 0}).appendTo(this.$pbcont);

	// Set the value for the progress bar
	this.setValue = setValue;
	function setValue(val) {
		this.$pb.progressbar("value", val);
	}
};
