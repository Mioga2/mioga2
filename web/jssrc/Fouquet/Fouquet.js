	// ========================================================================================================
	// Main Fouquet object
	// ========================================================================================================
	function Fouquet (elem, options) {
		var defaults = {
			page: undefined,
			max_user_count: 10,
			max_team_count: 10
		};


		// ========================================================
		// Public attributes
		// ========================================================
		this.applicationName = 'Fouquet';


		// ========================================================
		// Public methods
		// ========================================================


		// ------------------------------------------------------------------------------------------------
		// Browser navigation hash generator
		// ------------------------------------------------------------------------------------------------
		this.generateNavigationHash = generateNavigationHash;
		function generateNavigationHash (type, rowid) {
			if (rowid) {
				return ('#' + this.applicationName +  '-' + type + '-' + rowid);
			}
			else {
				return ('#' + this.applicationName +  '-' + type);
			}
		};


		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// show, show an object according to its type (user, team, etc.) and rowid
		// --------------------------------------------------------
		function show (type, rowid) {
			MIOGA.logDebug ('Fouquet', 1, '[show] Entering for ' + type + ' ' + rowid);

			// Get editor for object
			var page = undefined;
			var object = undefined;
			switch (type) {
				case undefined:
					page = this.mainPage;
					object = new FouquetGroup (this.data);
					break;
				case 'user':
					page = this.userEdit;
					object = new FouquetUser ({rowid: rowid});
					break;
				case 'team':
					page = this.teamEdit;
					object = new FouquetTeam ({rowid: rowid});
					break;
				case 'profile':
					page = this.profileEdit;
					object = new FouquetProfile ({rowid: rowid, full: true});
					if (!rowid) {
						delete (object.attr.rowid);
						object.attr.ident = that.i18n.new_profile
					}
					break;
			}

			// Show / hide main menu if needed
			if (page === this.mainPage) {
				this.$home_btn.hide ();
			}
			else {
				this.$home_btn.show ();
			}

			// Check editor exists before trying to display it
			if ((page !== undefined) && (object !== undefined)) {
				page.refresh (object);
				if (this.currentPage !== undefined) {
					this.currentPage.hide ();
				}
				page.show ();
				this.currentPage = page;
			}
			else {
				alert (this.i18n.no_ui + type);
			}

			MIOGA.logDebug ('Fouquet', 1, '[show] Leaving.');
		};


		// ------------------------------------------------------------------------------------------------
		// Browser navigation (back and next buttons) handler
		// ------------------------------------------------------------------------------------------------
		function navigationClick () {
			MIOGA.logDebug ('Fouquet', 1, '[navigationClick] Entering.');

			// load_page will be set to 'false' if page is part of Fouquet and needs to be handled in JavaScript
			var load_page = true;

			// Extract parts of hash being in the form #Fouquet-<type>-<rowid>, type being 'user', 'team', etc.
			var hash = window.location.hash;
			var parts = hash.split ('-');

			if ((parts[0] === '#' + that.applicationName) || ((parts[0] === '') && (window.location === that.appLocation))) {
				// Target URL belongs to application, handle it here
				MIOGA.logDebug ('Fouquet', 1, '[navigationClick] Target URL ' + hash + ' belongs to application');
				show.call (that, parts[1], parseInt (parts[2], 10));
				load_page = false;
			}
			else {
				MIOGA.logDebug ('Fouquet', 1, '[navigationClick] Target URL does not belong to application');
			}

			MIOGA.logDebug ('Fouquet', 1, '[navigationClick] Leaving');
			return (load_page);
		};


		// ========================================================
		// Constructor
		// ========================================================
		var that = this;

		// Attributs
		this.options    = $.extend (true, {}, defaults, options);
		this.elem       = elem;
		this.i18n       = options.i18n;

		// Max values
		this.max_user_count = this.options.max_user_count;
		this.max_team_count = this.options.max_team_count;

		this.data = options.data;
		this.currentPage = undefined;
		this.appLocation = window.location;

		// Create and activate progress bar
		var pb_opt = { i18n : { "displayText" : this.i18n.progressBarText } };
		this.appPB = new AppProgressBar(this.elem, pb_opt);
		this.appPB.setValue(0);

		$menu = $('<ul class="main-menu actions hmenu"></ul>').appendTo ($(that.elem));
		this.$home_btn = $('<a class="home-page" href="DisplayMain"></a>').append (this.i18n.home_page).appendTo ($('<li></li>').appendTo ($menu));

		// ------------------------------------------------------------------------------------------------
		// Main page
		// ------------------------------------------------------------------------------------------------
		this.mainPage = new FouquetMainPage (that.elem, {
			fouquet: this,
			i18n: options.i18n
		});
		this.mainPage.hide ();

		this.appPB.setValue (25);

		// ------------------------------------------------------------------------------------------------
		// User editor
		// ------------------------------------------------------------------------------------------------
		this.userEdit = new FouquetUserEdit (that.elem, {
			fouquet: this,
			i18n: options.i18n
		});
		this.userEdit.hide ();

		this.appPB.setValue (50);

		// ------------------------------------------------------------------------------------------------
		// Team editor
		// ------------------------------------------------------------------------------------------------
		this.teamEdit = new FouquetTeamEdit (that.elem, {
			fouquet: this,
			i18n: options.i18n
		});
		this.teamEdit.hide ();

		this.appPB.setValue (75);

		// ------------------------------------------------------------------------------------------------
		// Profile editor
		// ------------------------------------------------------------------------------------------------
		this.profileEdit = new FouquetProfileEdit (that.elem, {
			fouquet: this,
			i18n: options.i18n
		});
		this.profileEdit.hide ();

		// ------------------------------------------------------------------------------------------------
		// Trap browser's navigation buttons
		// ------------------------------------------------------------------------------------------------
		if (window.addEventListener) {
			window.addEventListener ("hashchange", navigationClick, false);
		}
		else if (window.attachEvent) {
			window.attachEvent ("onhashchange", navigationClick);
		}
		else {
			alert (i18n.internal_error + "Can't attach event listener to hashchange.");
		}
		navigationClick.call (this);

		this.appPB.setValue (100);
		this.appPB.$pbcont.fadeOut ('slow');
	};
