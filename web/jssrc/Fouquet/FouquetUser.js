// ========================================================
// FouquetUser OO-interface to users
// ========================================================
function FouquetUser (options) {
	// ========================================================
	// Default values
	// ========================================================
	defaults = {
		rowid: undefined,
		ident: undefined,
		firstname: undefined,
		lastname: undefined,
		email: undefined,
		label: undefined,
		last_connection: undefined,
		profile: undefined,
		teams: undefined
	},

	// ========================================================
	// Public methods
	// ========================================================

	// --------------------------------------------------------
	// fetch, fetch user from Mioga2
	// --------------------------------------------------------
	this.fetch = fetch;
	function fetch () {
		MIOGA.logDebug ('FouquetUser', 2, '[fetch] Entering, rowid: ' + this.attr.rowid);
		var that = this;

		$.ajax ({
			url: 'GetUser.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			data: { rowid: this.attr.rowid },
			success: function (data) {
				MIOGA.logDebug ('FouquetUser', 3, '[fetch] GetUser.json returned, data: ', data);
				that.attr = data;
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('FouquetUser', 2, '[fetch] Leaving, data: ', this.attr);
	};

	// --------------------------------------------------------
	// store, store user to Mioga2
	// --------------------------------------------------------
	this.store = store;
	function store () {
		MIOGA.logDebug ('FouquetUser', 1, '[store] Entering');

		// Basic attributes
		var user_attr = {
			rowid: this.attr.rowid,
			profile: this.attr.profile.rowid
		};

		MIOGA.logDebug ('FouquetUser', 2, '[store] Data to be posted:', user_attr);

		// Post data to Mioga2
		var success = false;
		var message = "[FouquetUser::store] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		var user = {rowid: this.attr.rowid};	// For further loading from WebService response
		$.ajax ({
			url: 'SetUser.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: user_attr,
			success: function (data) {
				MIOGA.logDebug ('FouquetUser', 2, '[store] SetUser.json POST returned, data:', data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.user) {
					user = data.user;
				}
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('FouquetUser', 1, '[store] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, user: user});
	};


	// ========================================================
	// Private methods
	// ========================================================


	// ========================================================
	// Constructor
	// ========================================================
	MIOGA.logDebug ('FouquetUser', 1, '[new] Entering, options: ', options);
	var that = this;

	this.attr = $.extend (true, {}, this.defaults, options);

	// Fetch user data from Mioga2 if required
	if ((Object.keys (this.attr).length === 1) && (this.attr.rowid !== undefined)) {
		// Object attributes has a single key (rowid), fetch data from Mioga2
		this.fetch ();
	}
	else {
		// Object attribute is supposed to a complete user description, no need to fetch data
		this.attr = options;
	}

	MIOGA.logDebug ('FouquetUser', 1, '[new] Leaving, data: ', that.attr);
}
