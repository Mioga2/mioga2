	// ========================================================================================================
	// FouquetUserEdit object, allows to manipulate fouquetUserEdit UI
	// ========================================================================================================
	function FouquetUserEdit (elem, options) {


		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data) {
			MIOGA.logDebug ('FouquetUserEdit', 1, '[refresh] Entering, data: ', data);
			var that = this; // For callback reference to current object

			that.data = data;

			// Main title
			that.$title.empty ();
			that.$title.append ($('<div></div>').append (data.attr.firstname + ' ' + data.attr.lastname));
			that.$title.append ($('<div></div>').append (data.attr.email));

			// Identity
			$.each (Array ('firstname', 'lastname', 'email', 'profile', 'last_connection', 'teams'), function (index, field) {
				that.fields[field].empty ();
				that.fields[field].append (formatValue.call (that, data.attr, field, 'user'));
			});

			// --------------------------------------------------------
			// Menu to select view mode
			// --------------------------------------------------------
			this.$actions.empty ();

			// Synthesis view
			var $synthesis = $('<li><a>' + i18n.synthesis_view + '</a></li>').appendTo (this.$actions).click (function () {
				$(this).addClass ('selected').siblings ().removeClass ('selected');
				fillRights.call (that, data.attr);
			});

			// User-specific rights, if applicable
			var specific_rights = $.grep (data.attr.filesystem, function (item) { return (item.user !== undefined); });
			if (specific_rights.length) {
				$('<li><a>' + i18n.user_specific + '</a></li>').appendTo (this.$actions).click (function () {
					$(this).addClass ('selected').siblings ().removeClass ('selected');
					var rights = {
						filesystem: specific_rights
					};
					fillRights.call (that, rights);
				});
			}

			// Profile details, if applicable
			if (data.attr.profile !== undefined) {
				$('<li><a>' + i18n.profile + ' &quot;' + data.attr.profile.ident + '&quot;</a></li>').appendTo (this.$actions).click (function () {
					$(this).addClass ('selected').siblings ().removeClass ('selected');

					// Produce some kind of grep on data.attr.filesystem to filter non-profile keys
					var rights = {
						filesystem: [],
						applications: []
					};
					$.each (data.attr.filesystem, function (index, value) {
						if (value.profile !== undefined) {
							var entry = {
								path: value.path,
								profile: value.profile
							};
							rights.filesystem.push (entry);
						}
					});
					rights.applications = {
						profile: data.attr.applications.profile,
						teams: []
					};
					fillRights.call (that, rights);
				});
			}

			// Teams details, if applicable
			if ((data.attr.teams !== undefined) && (data.attr.teams.length)) {
				$.each (data.attr.teams, function (index, team) {
					// User is invited through a team, add an entry to show details
					$team_details = $('<li><a>' + i18n.team + ' &quot;' + team.ident + '&quot;' + '</a></li>').appendTo (that.$actions).click (function () {
						$(this).addClass ('selected').siblings ().removeClass ('selected');

						// Produce some kind of grep on data.attr.filesystem to filter non-team keys
						var rights = {
							filesystem: [],
							applications: []
						};
						if ((data.attr.filesystem !== undefined) && data.attr.filesystem.length) {
							$.each (data.attr.filesystem, function (index, value) {
								if ((value.teams !== undefined) && (value.teams.length)) {
									var team_rights = $.grep (value.teams, function (item) { return (item.rowid === team.rowid); })[0];
									if (team_rights !== undefined) {
										var entry = {
											path: value.path,
											teams: [team_rights]
										};
										rights.filesystem.push (entry);
									}
								}
							});
						}

						// Get application rights for current team
						if ((data.attr.applications.teams !== undefined) && data.attr.applications.teams.length) {
							var team_rights = $.grep (data.attr.applications.teams, function (item) { return (item.rowid === team.rowid)})[0];
							if (team_rights !== undefined) {
								rights.applications = {
									teams: [team_rights]
								};
							}
						}
						fillRights.call (that, rights);
					});
				});
			}

			// Default view is synthesis
			$synthesis.trigger ('click');

			MIOGA.logDebug ('FouquetUserEdit', 1, '[refresh] Leaving.');
		};


		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// formatValue
		// Given an object (hash) the name of a field and an object type,
		// format inner data for field display an interaction (click events, etc.)
		// --------------------------------------------------------
		function formatValue (object, field, object_type) {
			var that = this;

			// Default behavior is to return the field as-is
			var value = object[field];

			// Some fields need a specific contruction rule
			if (field === 'profile') {
				if (object[field] !== undefined) {
					// Profile formatter, link to profile
					value = $('<div></div>').addClass ('inline-chooser').append ($('<a></a>').attr ('href', fouquet.generateNavigationHash ('profile', object[field].rowid)).append (object[field].ident));

					if (!that.data.attr.is_animator) {
						// Dropdown menu to change profile
						var $dropdown = $('<ul class="dropdown"></ul>').appendTo (value);
						var $entry = $('<li class="dir"></li>').append ('&#x25BC;').appendTo ($dropdown);
						var $submenu = $('<ul></ul>').appendTo ($entry);

						$.each (that.data.attr.profiles, function (index, profile) {
							if (profile.ident !== object[field].ident) {
								var $link = $('<a></a>').append (profile.ident);
								var $entry = $('<li></li>').append ($link).appendTo ($submenu).click (function () {
									// Update user's profile and store to Mioga2
									that.data.attr.profile.rowid = profile.rowid;
									that.data.attr.profile.ident = profile.ident;
									var res = that.data.store ();
									if (res.success) {
										var message = (res.message !== undefined) ? res.message : i18n.profile_change_success;
										that.$message_box.inlineMessage ('info', message);

										// Refresh data from new user description returned by WebService
										var user = new FouquetUser (res.user);
										that.refresh (user);
									}
									else {
										that.$message_box.inlineMessage ('error', res.message);
									}
								});
							}
						});
					}
				}
				else {
					value = $('<span class="note"></span>').append (i18n.no_profile_for_user);
				}
			}
			else if (field === 'teams') {
				value = $('<ul></ul>').addClass ('team-list');
				if ((object.teams !== undefined) && (object.teams.length)) {
					object.teams.sort (function (a, b) {
						return ((a.ident < b.ident) ? -1 : 1);
					});
					$.each (object.teams, function (index, team) {
						var $elem = $('<li></li>').appendTo (value);
						$('<a></a>').attr ('href', fouquet.generateNavigationHash ('team', team.rowid)).append (team.ident).appendTo ($elem);
					});
				}
			}
			else if ((field === 'last_connection') && (object.last_connection === undefined)) {
				value = $('<span class="note"></span>').append (i18n.never_connected);
			}
			else if (field === 'label') {
				value = object.firstname + ' ' + object.lastname + ' (' + object.email + ')';
			}

			return (value);
		};


		// --------------------------------------------------------
		// Fill filesystem and applications rights
		// --------------------------------------------------------
		function fillRights (data) {
			MIOGA.logDebug ('FouquetUserEdit', 1, '[fillRights] Entering, data: ', data);
			var that = this;

			// Filesystem rights
			this.$filesystem.empty ();
			if ((data.filesystem !== undefined) && (data.filesystem.length)) {
				data.filesystem.sort (function (a, b) {
					return ((a.path < b.path) ? -1 : 1);
				});
				$.each (data.filesystem, function (index, value) {
					var access = 0;

					// Get access right from teams
					if (value.teams !== undefined) {
						// Team-specific access right is stronger than profile access right
						var team_specific = undefined;
						var team_profile = 0;
						$.each (value.teams, function (index, team) {
							team_profile = Math.max (team.profile, team_profile);
							if (team.team !== undefined) {
								if (team_specific === undefined) {
									team_specific = 0;
								}
								team_specific = Math.max (team.team, team_specific);
							}
						});
						access = Math.max (team_profile, access);
						if (team_specific !== undefined) {
							access = team_specific;
						}
					}

					// Get access right from user profile
					access = (value.profile !== undefined) ? value.profile : access;

					// Get access right from user specific
					access = (value.user !== undefined) ? value.user : access;

					// Do not record path with undefined access rights, for example when drawing rights for a team on a path that has no specific right for this team profile
					if (access !== undefined) {
						// Set classname from access right
						var className = undefined;
						switch (access) {
							case 2:
								className = 'read-write';
								break;
							case 1:
								className = 'read-only';
								break;
							case 0:
								className = 'no-right';
								break;
						}

						// Create and append DOM
						var $item = $('<li></li>').append (value.path).addClass (className).appendTo (that.$filesystem);
					}
				});

				this.$filesystem.parent ().parent ().show ();
			}
			else {
				this.$filesystem.parent ().parent ().hide ();
			}

			// Applications rights
			this.$applications.empty ();
			if ((data.applications !== null) && (data.applications !== undefined) && (Object.keys (data.applications).length)) {
				// Create full list (from profile and teams)
				var full_list = [];
				if (data.applications.profile !== undefined) {
					$.each (data.applications.profile, function (index, app) {
						full_list.push (app);
					});
				}
				if (data.applications.teams !== undefined) {
					$.each (data.applications.teams, function (index, team) {
						$.each (team.rights, function (index, app) {
							full_list.push (app);
						});
					});
				}

				// Merge records of list
				var merge = [];
				$.each (full_list, function (index, app) {
					var existing_app = $.grep (merge, function (item) { return (item.label === app.label); })[0];
					if (existing_app === undefined) {
						// Application not in list, create it
						existing_app = {
							label: app.label,
							ident: app.ident,
							functions: [ ]
						};
						merge.push (existing_app);
					}
					// Loop through each application functions to check if already registered from previous iteration
					$.each (app.functions, function (index, func) {
						var existing_func = $.grep (existing_app.functions, function (item) { return (item.ident === func.ident)})[0];
						if (existing_func === undefined) {
							existing_app.functions.push (func);
						}
					});
				});

				// Create and append DOM node
				merge.sort (function (a, b) {
					return ((a.label < b.label) ? -1 : 1);
				});
				$.each (merge, function (index, app) {
					var $app = $('<li></li>').addClass ('application').append (app.label).appendTo (that.$applications);
					$('<div class="top-link"></div>').appendTo ($app).click (function () {
						$(document).scrollTop (0);
						return (false);
					});
					$('<div></div>').addClass ('icon').append ('<img src="' + mioga_context.image_uri + '/48x48/apps/' + app.ident.toLowerCase () + '.png" alt="icon"/>').appendTo ($app);
					var $functions = $('<ul class="highlight"></ul>').addClass ('functions').appendTo ($app);
					app.functions.sort (function (a, b) {
						return ((a.label < b.label) ? -1 : 1);
					});
					$.each (app.functions, function (index, funct) {
						$('<li></li>').append (funct.label).appendTo ($functions);
					});
				});

				this.$applications.parent ().parent ().show ();
			}
			else {
				this.$applications.parent ().parent ().hide ();
			}

			MIOGA.logDebug ('FouquetUserEdit', 1, '[fillRights] Leaving.');
		};


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('FouquetUserEdit', 1, '[new] Entering.');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };

		var fouquet = options.fouquet;

		options =  $.extend (true, {}, defaults, options);
		this.options    = options;

		this.data = options.data;

		// Stack
		this.stack = options.stack;

		var i18n = options.i18n;


		// --------------------------------------------------------
		// Create empty UI
		// --------------------------------------------------------
		this.$cont = $('<div class="fouquet-user-synthesis"></div>').appendTo (this.elem);

		// Message box
		this.$message_box = $('<div></div>').inlineMessage ().appendTo (this.$cont);

		var $row = $('<div class="row"></div>').appendTo (this.$cont);

		// Identity box
		$cell = $('<div class="span4"></div>').appendTo ($row);

		var $container = $('<div class="attribute-box"></div>').appendTo ($cell);
		var $title_box = $('<div class="user-title"></div>').appendTo ($container);
		this.$title = $('<h1></h1>').appendTo ($title_box);
		var $content = $('<div class="content identity-box"></div>').appendTo ($container);
		var $table = $('<table class="user-identity"></table>').appendTo ($content);
		// Place each field into a table line through a formatter
		$.each (Array ('firstname', 'lastname', 'email', 'profile', 'teams', 'last_connection'), function (index, value) {
			var $line = $('<tr><th>' + i18n[value] + '</th></tr>').appendTo ($table);
			that.fields[value] = $('<td></td>').appendTo ($line);
		});

		this.$actions = $('<ul class="actions vmenu"></ul>').appendTo ($cell);
		$('<div></div>').append (i18n.user_tabs_help).addClass ('help-notice').appendTo ($cell);

		// Access rights
		var $cell = $('<div class="span8"></div>').appendTo ($row);

		// Filesystem rights box
		var $box = $('<div class="sbox"><h2 class="title_bg_color">' + i18n.filesystem_title + '</h2></div>').foldable ().appendTo ($cell);
		var $content = $('<div class="content"></div>').appendTo ($box);
		$content.append ($('<ul class="legend rights"><li class="read-write">' + i18n.read_write + '</li><li class="read-only">' + i18n.read_only + '</li><li class="no-right">' + i18n.no_right + '</li></ul>'));
		this.$filesystem = $('<ul class="rights list highlight"></ul>').appendTo ($content);

		// Applications rights box
		var $box = $('<div class="sbox"><h2 class="title_bg_color">' + i18n.applications_title + '</h2></div>').foldable ().appendTo ($cell);
		var $content = $('<div class="content"></div>').appendTo ($box);
		this.$applications = $('<ul></ul>').addClass ('applications-rights').appendTo ($content);

		// If the user to display is already known, then refresh view
		if (options.user !== undefined) {
			this.refresh (options.user);
		}

		MIOGA.logDebug ('FouquetUserEdit', 1, '[new] Leaving.');
	};
