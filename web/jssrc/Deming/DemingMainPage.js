	// ========================================================================================================
	// DemingMainPage object, UI entry point
	// ========================================================================================================
	function DemingMainPage (elem, options) {

		MIOGA.debug.DemingMainPage = 0;

		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();

			// Force fullCalendar refresh
			that.data.total = 0;
			that.$cal.fullCalendar ('rerenderEvents');
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data) {
			MIOGA.logDebug ('DemingMainPage', 1, '[refresh] Entering', data);
			var that = this; // For callback reference to current object

			that.data = data;
			that.data.total = 0;

			// Menu
			var $menu = deming.generateMenu ();
			that.$chronos_opt = $('<a href="#"></a>').append (i18n.chronos_import).appendTo ($('<li></li>').appendTo ($menu)).click (function () { showChronosImport.call (that); return (false); });
			var $cont = $('<li></li>').appendTo ($menu);
			that.$table_switch = $('<a href="#"></a>').append (i18n.user_report_view).appendTo ($cont).click (function () { showUserReportView.call (that); return (false); });
			that.$calendar_switch = $('<a href="#"></a>').append (i18n.calendar_view).appendTo ($cont).click (function () { showCalendarView.call (that); return (false); }).hide ();
			that.$prefs_action = $('<a href="#"></a>').append (i18n.preferences).appendTo ($('<li></li>').appendTo ($menu)).click (function () { showPreferences.call (that); return (false); });

			// Get colors for activities
			that.data.colors = that.data.activities.getColorHash ();

			// Data changed, fetch events
			that.$cal.fullCalendar ('refetchEvents');

			// Init preference editor
			that.preferences_editor.refresh ({
				prefs: that.data.user_prefs
			});

			MIOGA.logDebug ('DemingMainPage', 1, '[refresh] Leaving.');
		};

		// --------------------------------------------------------
		// Push an entry into list and trigger calendar update
		// --------------------------------------------------------
		this.pushEntry = pushEntry;
		function pushEntry (entry) {
			that.data.entries.push (entry);
			that.$cal.fullCalendar( 'refetchEvents');
		}


		// ========================================================
		// Private methods
		// ========================================================


		// --------------------------------------------------------
		// showChronosImport, show Chronos import dialog
		// --------------------------------------------------------
		function showChronosImport () {
			if (that.$chronos_dialog === undefined) {
				var $elem = $('<div></div>');
				that.$chronos_import = new DemingChronosImport ($elem, {
					deming: deming,
					i18n: i18n,
					activities: that.data.activities,
					firstHour: that.options.calendar.firstHour,
					dayLength: that.options.calendar.dayLength
					, calendar : that.options.calendar
				});
				// No dialog created yet, create it
				that.$chronos_dialog = MIOGA.dialog ({
					title: i18n.chronos_import,
					content: $elem,
					buttons: [
						{
							i18n: i18n.close,
							classes: ['button', 'cancel'],
							cb: function (evClick,$dialog) {
								$dialog.dialog ('close');
							}
						}
					]
				});
			}

			// Fetch today events
			that.$chronos_import.refresh (new Date ());
			that.$chronos_dialog.dialog ('open');
		}


		// --------------------------------------------------------
		// showUserReportView, switch from calendar to table
		// --------------------------------------------------------
		function showUserReportView () {
			MIOGA.logDebug ('DemingMainPage', 1, '[showUserReportView] Entering');
			// Update table contents
			that.$table.find ('tbody td').empty ();

			// Set view title
			that.$table_view_title.html (that.$cal.find ('td.fc-header-center').html ());

			// Sorter function to have entries in the correct order
			var entry_sorter = function (a, b) {
				if (a.attr.dtstart < b.attr.dtstart) return (-1);
				if (a.attr.dtstart > b.attr.dtstart) return (1);
				return (0);
			};

			if ((that.data !== undefined) && (that.data.entries !== undefined) && that.data.entries.length) {
				// Get entries for current fullCalendar week
				var current = that.$cal.fullCalendar ('getDate');
				var week = current.getWeek ();
				var year = current.getFullYear ();
				var entries = $.grep (that.data.entries, function (entry) { if ((entry.attr.dtstart.getFullYear () === year) && (entry.attr.dtstart.getWeek () === week)) { return (entry); } }).sort (entry_sorter);

				var day_count = 7;
				if ((that.data.user_prefs !== undefined) && (that.data.user_prefs.weekends !== undefined) && (that.data.user_prefs.weekends === 1)) {
					that.$table.removeClass ('no-weekends').addClass ('weekends');
					that.$table.find ('th.Saturday, th.Sunday, td.Saturday, td.Sunday').show ();
				}
				else {
					day_count = 5;
					that.$table.removeClass ('weekends').addClass ('no-weekends');
					that.$table.find ('th.Saturday, th.Sunday, td.Saturday, td.Sunday').hide ();
				}

				// Duplicate fullCalendat column headers to table
				var $headings = that.$table.find ('thead th');
				for (i=0; i<day_count; i++) {
					$($headings[i]).empty ().append ($(that.$cal.find ('th')[i+1]).html ());
				}

				that.data.total = 0;
				// Render entries
				$.each (entries, function (index, entry) {
					var day_number = entry.attr.dtstart.getDay () - 1;
					if (day_number === -1) {
						day_number = 6;
					}
					MIOGA.logDebug ('DemingMainPage', 1, ' day_number = ' + day_number);
					var $day = that.$table.find ('td')[day_number];
					var $block = $('<div class="entry"><div class="time"></div><div class="fc-event-title"></div></div>').appendTo ($day);
					var attr = {
						title: entry.attr.notes,
						start: entry.attr.dtstart,
						end: entry.attr.dtend,
						allDay: false,
						textColor: that.data.colors[entry.attr.activity]['fgcolor'],
						color: that.data.colors[entry.attr.activity]['bgcolor'],
						activity: that.data.activities.getDetails (entry.attr.activity, entry.attr.sub_activity, entry.attr.sub_sub_activity),
						DemingEntry: entry
					};

					// Set box title
					var $box_title = entry.attr.dtstart.formatMioga (true).replace (/^([\d]+)-([\d]+)-([\d]+)/, '').replace (/:([\d]+)$/, '') + ' - ' + entry.attr.dtend.formatMioga (true).replace (/^([\d]+)-([\d]+)-([\d]+)/, '').replace (/:([\d]+)$/, '');
					$block.find ('div.time').append ($box_title);

					// Set box contents
					that.options.calendar.eventRender (attr, $block);

					// Drop click callbacks added into eventRender
					that.$table.find ('*').unbind ('click');

					// Set box color
					if ((attr.textColor !== undefined) && (attr.color !== undefined)) {
						$block.css ({
							color: attr.textColor,
							'background-color': attr.color
						});
					}
				});
			}

			that.$cal.hide ();
			that.$table.show ();

			that.$table_switch.hide ();
			that.$calendar_switch.show ();
			that.$prefs_action.hide ();
			that.$chronos_opt.hide ();
		}

		// --------------------------------------------------------
		// showCalendarView, switch from table to calendar
		// --------------------------------------------------------
		function showCalendarView () {
			that.$table.hide ();
			that.$cal.show ();

			that.data.total = 0;

			that.$calendar_switch.hide ();
			that.$table_switch.show ();
			that.$prefs_action.show ();
			that.$chronos_opt.show ();
		}


		// --------------------------------------------------------
		// populateEntryForm, add recent entries
		// --------------------------------------------------------
		function populateEntryForm () {
			MIOGA.logDebug ('DemingMainPage', 1, '[populateEntryForm] Entering.');

			// Recent entries
			if (that.data.recent === undefined) {
				that.data.recent = [ ];
				if ((that.data.entries !== undefined) && (that.data.entries.length)) {
					for (i=that.data.entries.length-1; i>=0; i--) {
						var entry = that.data.entries[i];
						if ((entry.attr.notes !== undefined) && (entry.attr.notes !== '')) {
							var existing = $.grep (that.data.recent, function (recent) { return ((recent.attr.activity === entry.attr.activity) && (recent.attr.notes === entry.attr.notes)) });
							if (!existing.length) {
								// Entry is not already into recent list, push it
								that.data.recent.push (entry);
								if (that.data.recent.length >= that.options.max_recent) {
									// Got enough recent entries, stop iterating
									break;
								}
							}
						}
					}
				}
			}

			MIOGA.logDebug ('DemingMainPage', 1, '[populateEntryForm] Leaving.');
		}


		// --------------------------------------------------------
		// Entry edition
		// --------------------------------------------------------
		function editEntry (entry, event_id) {
			MIOGA.logDebug ('DemingMainPage', 1, 'editEntry event_id '+event_id + ' entry = ', entry);
			that.edit_entry = entry;
			var create = (entry.attr.rowid === undefined) ? true : false;
			MIOGA.logDebug ('DemingMainPage', 1, 'create = ' + create);
			// Populate form
			populateEntryForm.call (that);

			// Set form title
			that.$form_title.empty ().append (create ? i18n.new_entry_title : i18n.edit_entry_title);

			// Show / hide form elements before creating dialog so it places correctly
			if (create && ((Object.keys (that.edit_entry.attr).length == 2)) && (that.edit_entry.attr.dtstart !== undefined) && (that.edit_entry.attr.dtend !== undefined)) {
				// Show recent entries container
				that.$recent_cont.show ();
			}
			else {
				// Hide recent entries container
				that.$recent_cont.hide ();
			}

			// Show dialog
			var $dialog = MIOGA.dialog ({
				title: i18n.edit_entry,
				content: that.$add_entry_dialog_contents,
				buttons: [
					{
						i18n: i18n.btn_ok,
						cb: function (ev, $dialog) {
							that.edit_entry.attr.notes = that.$notes.val ();
							var res = that.edit_entry.store ();
							if (res.success === true) {
								var stored_entry = new DemingEntry (res.entry);
								if (create) {
									that.data.entries.push (stored_entry);
									if (that.data.recent.length == that.options.max_recent) {
										that.data.recent.splice (that.data.recent.length - 1, 1);
									}
									that.data.recent.unshift (stored_entry);
								}
								that.$cal.fullCalendar( 'refetchEvents');
								$dialog.dialog ('destroy');
							}
						}
					},
					{
						i18n: i18n.btn_close,
						classes: ['button', 'cancel'],
						cb: function (ev, $dialog) {
							$dialog.dialog ('destroy');
						}
					}
				]
			});

			// Set start and end dates
			that.$start.val (zeroPad (that.edit_entry.attr.dtstart.getHours ()) + ':' + zeroPad (that.edit_entry.attr.dtstart.getMinutes ()));
			MIOGA.logDebug ('DemingMainPage', 1, '$start.val = ' +that.$start.val());
			that.$start.change (function () {
				MIOGA.logDebug ('DemingMainPage', 1, '$start.change = ' +that.$start.val() + ' entry = ',that.edit_entry);
				var orig_date = new Date (that.edit_entry.attr.dtstart.getTime ());
				var parts = $(this).val ().split (/:/);
				that.edit_entry.attr.dtstart.setHours (parts[0]);
				that.edit_entry.attr.dtstart.setMinutes (parts[1]);
				if (isOverlapping.call (that, that.edit_entry.attr.dtstart, that.edit_entry.attr.dtend, that.edit_entry.attr.rowid)) {
					alert (i18n.overlaps_event);
					that.edit_entry.attr.dtstart = orig_date;
					that.$start.val (zeroPad (that.edit_entry.attr.dtstart.getHours ()) + ':' + zeroPad (that.edit_entry.attr.dtstart.getMinutes ()));
				}
			});
			that.$end.val (zeroPad (that.edit_entry.attr.dtend.getHours ()) + ':' + zeroPad (that.edit_entry.attr.dtend.getMinutes ()));
			MIOGA.logDebug ('DemingMainPage', 1, '$end.val = ' +that.$end.val());
			that.$end.change (function () {
				MIOGA.logDebug ('DemingMainPage', 1, '$end.change = ' +that.$end.val() + ' entry = ',that.edit_entry);
				var orig_date = new Date (that.edit_entry.attr.dtend.getTime ());
				var parts = $(this).val ().split (/:/);
				that.edit_entry.attr.dtend.setHours (parts[0]);
				that.edit_entry.attr.dtend.setMinutes (parts[1]);
				if (isOverlapping.call (that, that.edit_entry.attr.dtstart, that.edit_entry.attr.dtend, that.edit_entry.attr.rowid)) {
					alert (i18n.overlaps_event);
					that.edit_entry.attr.dtend = orig_date;
					that.$end.val (zeroPad (that.edit_entry.attr.dtend.getHours ()) + ':' + zeroPad (that.edit_entry.attr.dtend.getMinutes ()));
				}
			});

			// Initialize activity:
			// 	- from given entry,
			// 	- from user prefs
			if (that.edit_entry.attr.activity !== undefined) {
				// Set activity from given entry
				that.activity_selector.refresh (that.data.activities, {
					activity: that.edit_entry.attr.activity,
					sub_activity: (that.edit_entry.attr.sub_activity !== undefined) ? that.edit_entry.attr.sub_activity : undefined,
					sub_sub_activity: (that.edit_entry.attr.sub_sub_activity !== undefined) ? that.edit_entry.attr.sub_sub_activity : undefined
				}, that.edit_entry);
				that.$notes.val (that.edit_entry.attr.notes);
			}
			else {
				// Set default activity for user
				that.activity_selector.refresh (that.data.activities, {
					/*
					activity: that.data.user_prefs.default_activity,
					sub_activity: that.data.user_prefs.default_sub_activity,
					sub_sub_activity: that.data.user_prefs.default_sub_sub_activity
					*/
				}, that.edit_entry);

				// Set notes
				that.$notes.val ((that.edit_entry.attr.notes === undefined) ? '' : that.edit_entry.attr.notes).trigger ('focus');
			}
			that.$notes.trigger ('focus');

			// Populate recent entries list, if visible
			if (that.$recent_cont.is (':visible')) {
				// Recent entries
				that.$recent_entries.empty ();
				if ((that.data.recent !== undefined) && (that.data.recent.length)) {
					$.each (that.data.recent, function (index, recent_entry) {
						var colors = that.data.colors[recent_entry.attr.activity];
						var $entry = $('<li></li>').css ({color: colors.fgcolor, 'background-color': colors.bgcolor}).appendTo (that.$recent_entries).click (function () {
							MIOGA.logDebug ('DemingMainPage', 1, 'click on recent_entry ',recent_entry);
							var attributes = {
								activity: recent_entry.attr.activity,
								sub_activity: recent_entry.attr.sub_activity,
								sub_sub_activity: recent_entry.attr.sub_sub_activity,
								notes: recent_entry.attr.notes,
								dtstart: entry.attr.dtstart,
								dtend: entry.attr.dtend
							};
							// Store entry and get its rowid
							var new_entry = new DemingEntry (attributes);
							var res = new_entry.store ();
							if (res.success === true) {
								var stored_entry = new DemingEntry (res.entry);
								that.data.entries.push (stored_entry);
								that.$cal.fullCalendar( 'refetchEvents');
								$dialog.dialog ('destroy');
							}
							else {
								// TODO
							}
						});

						// Activity details
						var activity = that.data.activities.getDetails (recent_entry.attr.activity, recent_entry.attr.sub_activity, recent_entry.attr.sub_sub_activity);
						$entry.append ($('<h1></h1>').append (activity.activity.name));
						if (activity.sub_activity !== undefined) {
							$entry.append ('<span class="separator"></span>');
							$entry.append ($('<h2></h2>').append (activity.sub_activity.name));
							if (activity.sub_sub_activity !== undefined) {
								$entry.append ('<span class="separator"></span>');
								$entry.append ($('<h3></h3>').append (activity.sub_sub_activity.name));
							}
						}

						// Notes
						$entry.append ($('<div class="notes"></div>').append (recent_entry.attr.notes));
					});
				}
			}
		}



		// ------------------------------------------------------------------------------------------------
		// Fetch user Deming entries
		// ------------------------------------------------------------------------------------------------
		function fetchEntries (start, end) {
			MIOGA.logDebug ('DemingMainPage', 1, '[fetchEntries] Entering');

			var range = {
				start: start.formatMioga (),
				end: end.formatMioga ()
			};

			var entries;
			$.ajax ({
				url: 'GetEntries.json',
				type: 'GET',
				async: false,
				traditional: true,
				dataType: 'json',
				data: range,
				success: function (data) {
					MIOGA.logDebug ('DemingMainPage', 3, '[fetchEntries] GetEntries.json returned, data: ', data);
					entries = [ ];
					var category_list = {};
					var colors = that.data.activities.getColorHash();
					if ((data.entries !== undefined) && (data.entries.length)) {
						$.each (data.entries, function (index, entry) {
							var entry_obj = new DemingEntry (entry);
							var details = that.data.activities.getDetails(entry.activity);
							if (category_list[entry.activity] === undefined) {
								category_list[entry.activity] = {
									cat_name : details.activity.name,
									bgcolor : colors[entry.activity].bgcolor
								};
							}
							entries.push (entry_obj);
						});
					}
					// HTML categories list
					that.$cont_legend.children().not('.legend-action').remove();
					for (var i in category_list) {
						that.$cont_legend.append('<div class="cat"><span class="col" style="background-color:' + category_list[i].bgcolor + '"></span><p>' + category_list[i].cat_name + '</p></div>')
					}
				},
				error: function () {
					MIOGA.logError ('Internal error', true);
				}
			});

			MIOGA.logDebug ('DemingMainPage', 1, '[fetchEntries] Leaving, data: ', entries);
			return (entries);
		}

		// --------------------------------------------------------
		// Initialize (or re-initialize calendar)
		// --------------------------------------------------------
		function initCalendar (slotMinutes) {
			MIOGA.logDebug("DemingMainPage", 1, 'initCalendar');

			// Ensure initialization is not called twice (multiple UI clicks)
			if (that.cal_initializing === true) {
				MIOGA.logDebug("DemingMainPage", 1, 'initializing true');
				return;
			}
			that.cal_initializing = true;

			// Set slotMinutes
			if ((slotMinutes !== undefined) && slotMinutes!== that.options.calendar.slotMinutes) {
				that.options.calendar.slotMinutes = slotMinutes;
			}

			// Weekends visibility
			if ((that.data !== undefined) && (that.data.user_prefs !== undefined) && (that.data.user_prefs.weekends !== undefined)) {
				that.options.calendar.weekends = (that.data.user_prefs.weekends === 1) ? true : false;
			}

			// Destroy already existing calendar, if any
			// Destroy is needed as slotMinutes value change doesn't update existing calendar (http://code.google.com/p/fullcalendar/issues/detail?id=293)
			if (this.calendar_on === true) {
				MIOGA.logDebug("DemingMainPage", 1, 'calendar destroy ');
				this.$cal.fullCalendar('destroy');
			}

			// (Re-)define event source
			that.options.calendar.events = function (start, end, callback) {
				MIOGA.logDebug("DemingMainPage", 1, 'event source start ' + start + '  end ' + end, start);
				if (that.data !== undefined) {
					MIOGA.logDebug("DemingMainPage", 1, 'that.data defined');
					that.data.total = 0;
					that.$weekly_total.empty ().append (formatTotal.call (that));

					// Check if entries for current week should be fetched
					var key = end.getUTCFullYear () + '_' + end.getWeek ();
					//if (that.fetch_status[key] === undefined) {
						// Create internal entry array if not defined
						//if (that.data.entries === undefined) {
							that.data.entries = [ ];
						//}

						// Fetch events for selected week
						var entries = fetchEntries.call (that, start, end);
						for (i=0; i<entries.length; i++) {
							that.data.entries.push (entries[i]);
						}
						
						// Remember these entries don't need to be fetched anymore
					//	that.fetch_status[key] = true;
					//}

					// Format entries for display
					if ((that.data !== undefined) && (that.data.entries !== undefined)) {
						var data = [ ];
						$.each (that.data.entries, function (index, entry) {
							var attr = {
								title: entry.attr.notes,
								start: entry.attr.dtstart,
								end: entry.attr.dtend,
								allDay: false,
								textColor: that.data.colors[entry.attr.activity]['fgcolor'],
								color: that.data.colors[entry.attr.activity]['bgcolor'],
								activity: that.data.activities.getDetails (entry.attr.activity, entry.attr.sub_activity, entry.attr.sub_sub_activity),
								DemingEntry: entry
							};
							data.push (attr);
						});
						callback (data);
					}
				}
				else {
					MIOGA.logDebug("DemingMainPage", 1, 'that.data undefined');
					callback ({});
				}
			};

			// Start calendar
			this.$cal.fullCalendar(that.options.calendar);
			this.calendar_on = true;
			
			// categories legend
			this.$button_legend = $('<div class="legend-title-cont"><div class="legend-title">' + i18n.categoryLegend + '</div><div class="icon-down"></div></div>');
			this.$cont_legend = $('<div class="legend-list"></div>');
			this.$legend_action = $('<div class="legend-action"><button class="legend-back">' + i18n.reattachWindow + '</button></div>').appendTo(that.$cont_legend);
			this.$legend_cont = $('<div class="legend-cont toolbar-item"></div>').prependTo(this.$cal.find('.fc-header-right'));
			this.$legend_cont.after('<span class="fc-header-space"></span>');
			this.$legend_action.find('.legend-back').hide();
			this.$legend_cont.dropDown({
				button : that.$button_legend,
				content : that.$cont_legend,
				openDirection : "left"
			});
			that.$cont_legend.parent().draggable({
				start : function (event, ui) {
					if (that.$legend_cont.parent().data('origin_position') === undefined) {
						that.$legend_cont.parent().data('origin_position', ui.position);
					}
					that.$legend_action.find('.legend-back').show();
				}
			}).delegate('.legend-back', 'click', function () {
				that.$cont_legend.parent().animate(that.$legend_cont.parent().data('origin_position'),"slow", function () {
					that.$legend_action.find('.legend-back').hide();
				});
			});

			// Add zoom buttons
			var $cont = this.$cal.find ('td.fc-header-left');
			var $buttonset = $('<div></div>').appendTo ($cont);
			var slots = Array (10, 15, 30);
			for (i=0; i<slots.length; i++) {
				var slot = slots[i];
				var $input = $('<input name="radio-slot' + slot + 'min" id="radio-slot' + slot + 'min" type="radio"/>').attr ('slot', slot).appendTo ($buttonset);
				$('<label for="radio-slot' + slot + 'min"></label>').append (i18n['slot' + slot + 'min']).appendTo ($buttonset)

				// Handle current slot value
				if (slot === that.options.calendar.slotMinutes) {
					$input.attr ('checked', true);
				}

				// Handle slot change
				$input.change (function () {
					// TODO Store into preferences
					var slot = parseInt ($(this).attr ('slot'), 10);
					initCalendar.call (that, slot);
				});
			}
			$buttonset.buttonset ();

			// Add total duration placeholder
			var $cont = $('<span class="weekly-total"></span>').appendTo (this.$cal.find ('td.fc-header-center'));
			$('<h3></h3>').append (i18n.weekly_total).appendTo ($cont);
			that.$weekly_total = $('<span></span>').append ("0").appendTo ($cont);
			$('<span></span>').append (i18n.weekly_total_unit).appendTo ($cont);

			// Render events
			if (this.data !== undefined) {
				this.refresh (this.data);
			}
			
			that.cal_initializing = false;
		}


		// --------------------------------------------------------
		// Pad a value with a zero if less than 10
		// --------------------------------------------------------
		function zeroPad(n) {
			return (n < 10 ? '0' : '') + n;
		}


		// --------------------------------------------------------
		// Store preferences into user session
		// --------------------------------------------------------
		function storePreferences () {
			MIOGA.logDebug ('DemingMainPage', 1, '[storePreferences] Entering');
			// Get user-managed preferences
			var new_prefs = that.preferences_editor.getPreferences ();
			MIOGA.logDebug ('DemingMainPage', 1, 'new_prefs = ', new_prefs);
			that.data.user_prefs.firstHour = new_prefs.firstHour;
			that.data.user_prefs.dayLength = new_prefs.dayLength;

			// Weekends visibility
			var redraw = false;
			if (new_prefs.weekends !== ((that.data.user_prefs.weekends === 1) ? true : false)) {
				redraw = true;
			}
			that.data.user_prefs.weekends = (new_prefs.weekends === true) ? 1 : 0;
			MIOGA.logDebug ('DemingMainPage', 1, 'redraw '  + redraw);


			// Redraw calendar if needed (weekends visibility changed)
			if (redraw) {
				that.fetch_status = { };
				delete (that.data.entries);
				initCalendar.call (that);
				showCalendarView.call (that);
			}

			// Add automatically-managed preferences
			that.data.user_prefs.slotMinutes = that.options.calendar.slotMinutes;
			that.data.user_prefs.year = that.options.calendar.year;
			that.data.user_prefs.month = that.options.calendar.month;
			that.data.user_prefs.date = that.options.calendar.date;

			MIOGA.logDebug ('DemingMainPage', 1, '[storePreferences] Preferences to be stored: ', that.data.user_prefs);

			// POST to Mioga2
			var success = false;
			var message = '[DemingMainPage::storePreferences] Failed';	// To be replaced with message from server
			$.ajax ({
				url: 'SetPreferences.json',
				type: 'POST',
				async: false,
				traditional: true,
				dataType: 'json',
				data: that.data.user_prefs,
				success: function (data) {
					MIOGA.logDebug ('DemingMainPage', 2, "[storePreferences] SetPreferences.json POST returned, data: ", data);
					success = data.success;
					message = data.message;
					errors = data.errors;
				}
			});

			MIOGA.logDebug ('DemingMainPage', 1, '[storePreferences] Leaving, success: ' + success);
			return ({success: success, message: message, errors: errors});
		}


		// --------------------------------------------------------
		// Show preferences dialog
		// --------------------------------------------------------
		function showPreferences () {
			that.preferences_editor.refresh ({
				prefs: that.data.user_prefs,
			});

			// Show dialog
			var $dialog = MIOGA.dialog ({
				title: i18n.preferences,
				content: that.$preferences_dialog_contents,
				buttons: [
					{
						i18n: i18n.btn_ok,
						cb: function (ev, $dialog) {
							var res = storePreferences.call (that);
							if (res.success === true) {
								$dialog.dialog ('destroy');
							}
							else {
								// TODO
							}
						}
					},
					{
						i18n: i18n.btn_close,
						classes: ['button', 'cancel'],
						cb: function (ev, $dialog) {
							$dialog.dialog ('destroy');
						}
					}
				]
			});
		}



		// --------------------------------------------------------
		// Check a date range overlaps an existing event
		// --------------------------------------------------------
		this.isOverlapping = isOverlapping;
		function isOverlapping (startDate, endDate, rowid){
			MIOGA.logDebug ('DemingMainPage', 1, 'isOverlapping start = ' + startDate + ' end = ' + endDate + ' rowid = ' + rowid);
			//var array = that.$cal.fullCalendar('clientEvents');
							//that.data.entries.push (entries[i]);
			var entries = that.data.entries;
			for(i in entries){
				MIOGA.logDebug ('DemingMainPage', 1, 'i = '+ i + ' rowid = '+ entries[i].attr.rowid+ ' start = ' + entries[i].attr.dtstart + ' end = ' + entries[i].attr.dtend, entries[i]);
				if ((rowid === undefined) || (rowid !== entries[i].attr.rowid)) {
					if(!(entries[i].attr.dtstart >= endDate || entries[i].attr.dtend <= startDate)){
						MIOGA.logDebug ('DemingMainPage', 1, 'got one ');
						return true;
					}
				}
			}
			return false;
		}

		// --------------------------------------------------------
		// Search first free slot for task
		// --------------------------------------------------------
		this.correctHours = correctHours;
		function correctHours (startDate, endDate, offset){
			var array = that.$cal.fullCalendar('clientEvents');
			var max;
			for(i in array){
				if (array[i].end.getDate() === startDate.getDate()
						&& array[i].end.getMonth() === startDate.getMonth()
						&& array[i].end.getYear() === startDate.getYear()) {
					if ( (max === undefined) || ( max < array[i].end) ) {
						max = array[i].end;
					}
				}
			}
			if (max !== undefined) {
				startDate.setHours(max.getHours());
				startDate.setMinutes(max.getMinutes());
				endDate.setHours(max.getHours() + 1);
				endDate.setMinutes(max.getMinutes());
			}
		}

		// --------------------------------------------------------
		// Format weekly total a user-friendly way
		// --------------------------------------------------------
		function formatTotal () {
			var hours = parseInt (that.data.total, 10);
			var minutes = Math.round ((that.data.total - hours) * 60);
			MIOGA.logDebug ('DemingMainPage', 1, 'formatTotal  hours = ' + hours + ' minutes = ' + minutes);
			return (hours + ':' + ((minutes >= 10) ? minutes : '0' + minutes));
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DemingMainPage', 1, '[new] Entering.');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };
		this.fetch_status = { };	// Keep track of data that we fetch so it's not fetched multiple times

		var i18n = options.i18n;

		var deming = options.deming;
		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="calendar-container"></div>').appendTo (this.elem);

		// Process defaults
		var defaults = {
			calendar: {
				timeFormat: 'HH:mm{ - HH:mm}',
				defaultView: 'agendaWeek',
				selectable: true,
				editable: true,
				allDaySlot: false,
				firstDay: 1,
				weekends: false,
				height: $(window).height () - this.$cont.offset().top,
				firstHour: 9,
				dayLength: 8,
				slotMinutes: 30,
				year: undefined,
				month: undefined,
				date: undefined,
				theme: true,
				eventColor: 'white',
				eventBorderColor: 'black',
				header: {
					left: '',
					center: 'title',
					right: 'today prev,next'
				},
				titleFormat: { "month": "MMMM yyyy", "week": "d[ MMM yyyy]{ '&#8212; 'd MMM yyyy}", "day": "dddd, d MMMM yyyy" },
				events: function (start, end, callback) {
					if ((that.data !== undefined) && (that.data.entries !== undefined)) {
						var data = [ ];
						$.each (that.data.entries, function (index, entry) {
							var attr = {
								title: entry.attr.notes,
								start: entry.attr.dtstart,
								end: entry.attr.dtend,
								allDay: false,
								textColor: that.data.colors[entry.attr.activity]['fgcolor'],
								color: that.data.colors[entry.attr.activity]['bgcolor'],
								activity: that.data.activities.getDetails (entry.attr.activity, entry.attr.sub_activity, entry.attr.sub_sub_activity),
								DemingEntry: entry
							};
							data.push (attr);
						});
						callback (data);
					}
				}
			},
			sources: { },
			max_recent: 10
		};
		this.options =  $.extend (true, {}, defaults, options);


		// --------------------------------------------------------
		// Add entry dialog contents
		// --------------------------------------------------------
		that.$add_entry_dialog_contents = $('<div class="deming-entry-form"></div>');

		// User input form
		var $form_cont = $('<div class="form"></div>').appendTo (that.$add_entry_dialog_contents);
		that.$form_title = $('<h2></h2>').appendTo ($form_cont);
		var $cont = $('<div class="form-item"></div>').appendTo ($form_cont);
		$('<label for="dtstart"></label>').append (i18n.time_range).appendTo ($cont);
		that.$start = $('<input type="text" class="time" name="dtstart" tabindex="-1"/>').appendTo ($cont).timepicker ({	// tabindex -1 so that input doesn't automatically gets focused when dialog is opened which opens the timepicker
			hourText : i18n.hourText,
			minuteText : i18n.minuteText,
			showPeriodLabels: false,
			showPeriod: false
		});
		that.$end = $('<input type="text" class="time" name="dtend" tabindex="-1"/>').appendTo ($cont).timepicker ({	// tabindex -1 so that input doesn't automatically gets focused when dialog is opened which opens the timepicker
			hourText : i18n.hourText,
			minuteText : i18n.minuteText,
			showPeriodLabels: false,
			showPeriod: false
		});

		// Activity selector
		that.activity_selector = new DemingActivitySelector ($('<div></div>').appendTo ($form_cont), {
			i18n: i18n,
			deming: deming,
			sources: that.options.sources
		});

		// Notes
		var $cont = $('<div class="form-item"></div>').appendTo ($form_cont);
		$('<label for="notes"></label>').append (i18n.notes).appendTo ($cont);
		that.$notes = $('<textarea name="notes"></textarea>').appendTo ($cont);

		// Recent entries
		that.$recent_cont = $('<div class="recent"></div>').appendTo (that.$add_entry_dialog_contents);
		that.$recent_cont.append ($('<h2></h2>').append (i18n.recent_entries));
		that.$recent_entries = $('<ul></ul>').appendTo (that.$recent_cont);


		// Preferences dialog contents
		this.$preferences_dialog_contents = $('<div class="preferences-dialog"></div>');
		that.preferences_editor = new DemingPreferencesEdit (this.$preferences_dialog_contents, {
			i18n: i18n,
			deming: deming,
			sources: that.options.sources
		});


		// --------------------------------------------------------
		// Time range select callback
		// --------------------------------------------------------
		this.options.calendar.select = function (startDate, endDate) {
			MIOGA.logDebug ('DemingMainPage', 1, 'calendar select');
			if (!isOverlapping.call (that, startDate, endDate)) {
				if ((that.data.clipboard !== undefined) && (that.data.clipboard.entry !== undefined)) {
					// Entry in clipboard, paste it
					var attributes = {
						activity: that.data.clipboard.entry.DemingEntry.attr.activity,
						notes: that.data.clipboard.entry.DemingEntry.attr.notes,
						dtstart: startDate,
						dtend: endDate
					};
					if ((that.data.clipboard.entry.DemingEntry.attr.sub_activity !== undefined) && (that.data.clipboard.entry.DemingEntry.attr.sub_activity !== null)) {
						attributes.sub_activity = that.data.clipboard.entry.DemingEntry.attr.sub_activity;
					}
					if ((that.data.clipboard.entry.DemingEntry.attr.sub_sub_activity !== undefined) && (that.data.clipboard.entry.DemingEntry.attr.sub_sub_activity !== null)) {
						attributes.sub_sub_activity = that.data.clipboard.entry.DemingEntry.attr.sub_sub_activity;
					}
					var entry = new DemingEntry (attributes);
					if (that.data.clipboard.edit) {
						editEntry.call (that, entry);
					}
					else {
						var res = entry.store ();
						if (res.success === true) {
							var stored_entry = new DemingEntry (res.entry);
							that.data.entries.push (stored_entry);
							that.$cal.fullCalendar( 'refetchEvents');
						}
						else {
							// TODO
						}
				}
					delete (that.data.clipboard);
				}
				else {
					// Nothing in clipboard, display form dialog
					var entry = new DemingEntry ({
						dtstart: startDate,
						dtend: endDate
					});
					editEntry.call (that, entry);
				}
			}
			else {
				alert (i18n.overlaps_event);
			}
		};

		// --------------------------------------------------------
		// Event move callback
		// --------------------------------------------------------
		this.options.calendar.eventDrop = function (event, dayDelta, minuteDelta, allDay, revert) {
			// Update entry start and end dates
			var entry = event.DemingEntry;
			entry.attr.dtstart = event.start;
			entry.attr.dtend = event.end;
			MIOGA.logDebug ('DemingMainPage', 1, 'calendar eventDrop entry ', entry);

			if (!isOverlapping.call (that, event.start, event.end, entry.attr.rowid)) {
				// Store changes
				var res = entry.store ();
				if (res.success === true) {
					that.data.total = 0;
				}
				else {
					// TODO display error
					revert ();
				}
			}
			else {
				alert (i18n.overlaps_event);
				revert ();
			}
		};

		// --------------------------------------------------------
		// Event resize callback
		// --------------------------------------------------------
		this.options.calendar.eventResize = function (event, dayDelta, minuteDelta, revert) {
			// Update entry start and end dates
			var entry = event.DemingEntry;
			entry.attr.dtstart = event.start;
			entry.attr.dtend = event.end;
			MIOGA.logDebug ('DemingMainPage', 1, 'calendar eventResize entry = ', entry);

			if (!isOverlapping.call (that, event.start, event.end, entry.attr.rowid)) {
				// Store changes
				var res = entry.store ();
				if (res.success === true) {
					that.data.total = 0;
				}
				else {
					// TODO display error
					revert ();
				}
			}
			else {
				alert (i18n.overlaps_event);
				revert ();
			}
		};

		// --------------------------------------------------------
		// eventRender, event formatting callback
		// --------------------------------------------------------
		this.options.calendar.eventRender = function(event, element) {
			// Actions menu into event title
			var $cont = $('<div class="deming-dropdown-cont"></div>').appendTo (element).append (i18n.actions);
			var $icon = $('<div class="ui-icon ui-icon-triangle-1-s"></div>').appendTo ($cont);
			var $menu = $('<ul class="deming-dropdown-menu"></ul>').appendTo ($cont).hide ();
			// Ensure container doesn't get smaller that menu
			$(element).css ({
				'min-height': '1em'
			});
			$cont.click (function () {
				// Change z-index so that current event is above the others and its menu doesn't get cut
				if ($cont.hasClass ('open')) {
					$(element).css ({
						'z-index': 8
					});
				}
				else {
					$(element).css ({
						'z-index': 9
					});
				}

				// Open menu
				$cont.toggleClass ('open');
				$menu.slideToggle ();
			});

			// Event copy
			$('<li></li>').append (i18n.copy_entry).appendTo ($menu).click (function () {
				that.data.clipboard = {
					entry: event
				};
			});

			// Event copy & edit
			$('<li></li>').append (i18n.copy_and_edit_entry).appendTo ($menu).click (function () {
				that.data.clipboard = {
					entry: event,
					edit: true
				};
			});

			// Event edition
			$('<li></li>').append (i18n.edit_entry).appendTo ($menu).click (function () {
				editEntry.call (that, event.DemingEntry, event._id);
			});

			// Event deletion
			$('<li></li>').append (i18n.delete_entry).appendTo ($menu).click (function () {
				if (confirm (i18n.confirm_entry_deletion)) {
					var entry_index;
					for (i=0; i<that.data.entries.length; i++) {
						if (that.data.entries[i].attr.rowid === event.DemingEntry.attr.rowid) {
							entry_index = i;
							break;
						}
					}
					if (entry_index !== undefined) {
						var entry = that.data.entries[entry_index];
						var res = entry.destroy ();
						if (res.success === true) {
							that.data.entries.splice (entry_index, 1);

							// Refresh view
							that.$cal.fullCalendar ('refetchEvents');
						}
						else {
							// TODO
						}
					}
				}
			});

			// Rich-text contents into event details
			var $contents = element.find ('div.fc-event-title');
			$contents.empty ();
			$contents.addClass ('deming-entry');
			$contents.append ($('<h1></h1>').append (event.activity.activity.name));
			if (event.activity.sub_activity !== undefined) {
				$contents.append ('<span class="separator"></span>');
				$contents.append ($('<h2></h2>').append (event.activity.sub_activity.name));
				if (event.activity.sub_sub_activity !== undefined) {
					$contents.append ('<span class="separator"></span>');
					$contents.append ($('<h3></h3>').append (event.activity.sub_sub_activity.name));
				}
			}
			$contents.append ($('<hr/>'));
			$contents.append ($('<div class="note"></div>').append (event.title));
			$contents.click (function () {
				editEntry.call (that, event.DemingEntry, event._id)
			});

			// Add event duration to total
			that.data.total += (event.DemingEntry.attr.dtend - event.DemingEntry.attr.dtstart) / 3600000;
			that.$weekly_total.empty ().append (formatTotal.call (that));
		};

		// --------------------------------------------------------
		// viewDisplay, callback on date change
		// --------------------------------------------------------
		this.options.calendar.viewDisplay = function (element) {
			var current = new Date ();

			// Enable or disable "next-week" button
			if ((element.start < current) && (current < element.end)) {
				// Can't go forward to the future
				that.$cal.find ('span.fc-button-next').addClass ('ui-state-disabled');
			}
			else {
				// Can go back to the past
				that.$cal.find ('span.fc-button-next').removeClass ('ui-state-disabled');
			}

			// Store current date for further use
			that.options.calendar.date = element.end.getUTCDate ();
			that.options.calendar.month = element.end.getUTCMonth ();
			that.options.calendar.year = element.end.getUTCFullYear ();

			if ((that.data !== undefined) && (that.data.user_prefs !== undefined)) {
				storePreferences.call (that);;
			}
		};

		// Calendar
		this.$cal = $('<div></div>').appendTo (this.$cont);
		initCalendar.call (this);

		// Table
		this.$table = $('<div class="table-view"></div>').appendTo (this.$cont).hide ();
		this.$table_view_title = $('<div class="table-view-title"></div>').appendTo (this.$table);
		var $table = $('<table class="list"></table>').appendTo (this.$table);
		var $head = $('<thead></thead>').appendTo ($table);
		var $body = $('<tbody></tbody>').appendTo ($table);
		$.each (['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'], function (index, day) {
			$('<th></th>').addClass (day).append ('<span class="date"></span>').appendTo ($head);
			$('<td></td>').addClass (day).appendTo ($body);
		});

		// Display data, if any
		if (this.options.data !== undefined) {
			this.refresh (this.options.data);
		}

		MIOGA.logDebug ('DemingMainPage', 1, '[new] Leaving.');
	};
