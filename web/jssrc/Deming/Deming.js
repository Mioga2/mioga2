	// ========================================================================================================
	// Main Deming object
	// ========================================================================================================
	function Deming (elem, options) {
		var defaults = {
			sources: { }
		};

		MIOGA.debug.Deming = 0;


		// ========================================================
		// Public attributes
		// ========================================================
		this.applicationName = 'Deming';


		// ========================================================
		// Public methods
		// ========================================================


		// ------------------------------------------------------------------------------------------------
		// Browser navigation hash generator
		// ------------------------------------------------------------------------------------------------
		this.generateNavigationHash = generateNavigationHash;
		function generateNavigationHash (type, rowid) {
			if (rowid) {
				return ('#' + this.applicationName +  '-' + type + '-' + rowid);
			}
			else {
				return ('#' + this.applicationName +  '-' + type);
			}
		};


		// ------------------------------------------------------------------------------------------------
		// Main menu
		// ------------------------------------------------------------------------------------------------
		this.generateMenu = generateMenu;
		function generateMenu () {
			that.$menu.empty ();
			// that.$menu.append ($('<li></li>').append ($('<a href=""></a>').append (i18n.main_page)));
			if (mioga_context.rights.Animation === true) {
				$('<a></a>').attr ('href', this.generateNavigationHash ('activities')).append (i18n.manage_activities).appendTo ($('<li></li>').appendTo (that.$menu));
			}
			return (that.$menu);
		};


		// ------------------------------------------------------------------------------------------------
		// pushEntry, give an entry to main page to be pushed into entry list
		// ------------------------------------------------------------------------------------------------
		this.pushEntry = pushEntry;
		function pushEntry (entry) {
			that.mainPage.pushEntry (entry);
		}


		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// show, show an object according to its type (user, team, etc.) and rowid
		// --------------------------------------------------------
		function show (type, rowid) {
			MIOGA.logDebug ('Deming', 1, '[show] Entering for ' + type + ' ' + rowid);

			// Get editor for object
			var page = undefined;
			var object = undefined;
			switch (type) {
				case undefined:
					page = this.mainPage;
					object = {
						activities: new DemingActivities (),
						user_prefs: getPreferences.call (that)
					};
					break;
					/*
				case 'activities':
					page = this.activitiesPage;
					object = new DemingActivities ();
					break;
					*/
			}

			// Check editor exists before trying to display it
			if ((page !== undefined) && (object !== undefined)) {
				page.refresh (object);
				if (this.currentPage !== undefined) {
					this.currentPage.hide ();
				}
				page.show ();
				this.currentPage = page;
			}
			else {
				alert (this.i18n.no_ui + type);
			}

			MIOGA.logDebug ('Deming', 1, '[show] Leaving.');
		};


		// ------------------------------------------------------------------------------------------------
		// Fetch user preferences
		// ------------------------------------------------------------------------------------------------
		function getPreferences () {
			MIOGA.logDebug ('Deming', 1, '[getPreferences] Entering');

			var defaults = {
				firstHour: 9,
				dayLength: 8
			};

			var prefs;
			$.ajax ({
				url: 'GetPreferences.json',
				type: 'GET',
				async: false,
				traditional: true,
				dataType: 'json',
				success: function (data) {
					MIOGA.logDebug ('Deming', 3, '[getPreferences] GetPreferences.json returned, data: ', data);
					prefs = $.extend (true, {}, defaults, data.preferences);
				},
				error: function () {
					MIOGA.logError ('Internal error', true);
				}
			});

			MIOGA.logDebug ('Deming', 1, '[getPreferences] Leaving, data: ', prefs);
			return (prefs);
		}


		// ------------------------------------------------------------------------------------------------
		// Browser navigation (back and next buttons) handler
		// ------------------------------------------------------------------------------------------------
		function navigationClick () {
			MIOGA.logDebug ('Deming', 1, '[navigationClick] Entering.');

			// load_page will be set to 'false' if page is part of Deming and needs to be handled in JavaScript
			var load_page = true;

			// Extract parts of hash being in the form #Deming-<type>-<rowid>, type being 'user', 'team', etc.
			var hash = window.location.hash;
			var parts = hash.split ('-');

			if ((parts[0] === '#' + that.applicationName) || ((parts[0] === '') && (window.location === that.appLocation))) {
				// Target URL belongs to application, handle it here
				MIOGA.logDebug ('Deming', 1, '[navigationClick] Target URL ' + hash + ' belongs to application');
				show.call (that, parts[1], parseInt (parts[2], 10));
				load_page = false;
			}
			else {
				MIOGA.logDebug ('Deming', 1, '[navigationClick] Target URL does not belong to application');
			}

			MIOGA.logDebug ('Deming', 1, '[navigationClick] Leaving');
			return (load_page);
		};


		// ========================================================
		// Constructor
		// ========================================================
		var that = this;

		// Attributs
		this.options    = $.extend (true, {}, defaults, options);
		this.elem       = elem;
		this.i18n       = options.i18n;

		var i18n = this.i18n;

		this.data = options.data;
		this.currentPage = undefined;
		this.appLocation = window.location;

		// Create and activate progress bar
		var pb_opt = { i18n : { "displayText" : this.i18n.progressBarText } };
		this.appPB = new AppProgressBar(this.elem, pb_opt);
		this.appPB.setValue(0);

		this.$menu = $('<ul class="main-menu actions hmenu"></ul>').appendTo ($(that.elem));
		//this.$menu.append ($('<a href=""></a>').append (i18n.main_page).appendTo ($('<li></li>')));

		// ------------------------------------------------------------------------------------------------
		// Main page
		// ------------------------------------------------------------------------------------------------
		this.mainPage = new DemingMainPage (that.elem, {
			deming: this,
			i18n: options.i18n,
			calendar: options.calendar,
			sources: options.sources
		});
		this.mainPage.hide ();

		this.appPB.setValue (33);

		// ------------------------------------------------------------------------------------------------
		// Activities page
		// ------------------------------------------------------------------------------------------------
		/*
		this.activitiesPage = new DemingActivitiesEdit (that.elem, {
			deming: this,
			i18n: options.i18n
		});
		this.activitiesPage.hide ();

		this.appPB.setValue (66);
*/
		// ------------------------------------------------------------------------------------------------
		// Trap browser's navigation buttons
		// ------------------------------------------------------------------------------------------------
		if (window.addEventListener) {
			window.addEventListener ("hashchange", navigationClick, false);
		}
		else if (window.attachEvent) {
			window.attachEvent ("onhashchange", navigationClick);
		}
		else {
			alert (i18n.internal_error + "Can't attach event listener to hashchange.");
		}
		navigationClick.call (this);

		this.appPB.setValue (100);
		this.appPB.$pbcont.fadeOut ('slow');
	};
