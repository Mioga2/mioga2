// -----------------------------------------------------------------------------
// Deming, the activity reporting application
// -----------------------------------------------------------------------------
(function ($) {
	var defaults = {
		i18n: {
			"progressBarText": "Deming is loading",
			"no_ui": "No UI for",
			"main_page": "Main page",
			"manage_activities": "Manage activities",
			"save": "Save",
			"validate": "Validate",
			"cancel": "Cancel",
			"cancel_confirm": "Are you sure you want to cancel ?",
			"add_activity": "Add an activity",
			"add_external": "Add an external activity",
			"delete_activity": "Delete activity",
			"create_subactivity": "Create sub-activity",
			"foreground_color": "Foreground color",
			"background_color": "Background color",
			"change_colors": "Change colors",
			"close": "Close",
			"btn_ok": "OK",
			"btn_close": "Close",
			"activity": "Activity",
			"sub_activity": "&#160;",
			"notes": "Notes",
			"new_entry_title": "New entry",
			"edit_entry_title": "Edit entry",
			"recent_entries": "Recent entries",
			"actions": "Actions",
			"delete_entry": "Delete",
			"copy_entry": "Copy",
			"edit_entry": "Edit",
			"confirm_entry_deletion": "Are you sure you want to delete this entry ?",
			"copy_and_edit_entry": "Copy & Edit",
			"slot10min": "10 min",
			"slot15min": "15 min",
			"slot30min": "30 min",
			"time_range": "Time range",
			"preferences": "Preferences",
			"default_activity": "Default activity",
			"apply_filter": "Apply",
			"report_column_user_label": "User",
			"report_column_activity_label": "Activity",
			"report_column_sub_activity_label": "Sub-activity",
			"report_column_sub_sub_activity_label": "Task",
			"report_column_dtstart_label": "Start",
			"report_column_dtend_label": "End",
			"report_column_duration_label": "Duration",
			"report_column_notes_label": "notes",
			"total_duration": "Total duration:",
			"days": "days",
			"csv_raw_export": "Export raw data",
			"csv_synthesis_export": "Export synthesis",
			"user_report_view": "Table view",
			"calendar_view": "Calendar view",
			"monday": "Monday",
			"tuesday": "Tuesday",
			"wednesday": "Wednesday",
			"thursday": "Thursday",
			"friday": "Friday",
			"chronos_import": "Import from Chronos",
			"formatDateImport": "dddd d/M",
			"my_calendars": "My calendars",
			"my_groups_calendars": "My groups calendars",
			"click_here_to_import": "Click here to import",
			"prev_day": "Previous day",
			"next_day": "Next day",
			"show_weekends": "Show weekends",
			"weekly_total": "Weekly total",
			"all_day_event": "All-day event",
			"overlaps_event": "Time range is invalid, it overlaps an existing event",
			"weekly_total_unit": "hours",
			"first_hour": "First hour",
			"day_length": "Day length",
			"hourText": "Hour",
			"minuteText": "Minute",
			"all_sub_activities": "All sub-activities",
			"view": "View",
			"categoryLegend" : "Category legend",
			"reattachWindow": "Reattach the window"
		}
	};

	// ========================================================
	// Effective plugin declaration with methods abstraction
	// Usage :
	//     <div id="deming"></div>
	//
	//     $('#deming').deming(options);
	// ========================================================
	$.fn.deming = function(options) {
		// method call, get existing Deming Object and apply method
		// if method not returns result we return this for chainability
		if (typeof options === 'string') {
			var args = Array.prototype.slice.call(arguments, 1);
			this.each(function() {
				var deming = $.data(this, 'deming');
				if (deming && $.isFunction(deming[options])) {
					var res = deming[options].apply(deming, args);
					if (res !== undefined) {
						return res;
					}
				}
				else {
					$.error( 'Method ' +  options + ' does not exist on jQuery.deming' );
				}
			});
		}
		else if (typeof options === 'object') {
			options =  $.extend(true, {}, defaults, options);

			this.each(function(i, item) {
				var $elem = $(item);
				var deming = new Deming (item, options);
				$(item).data('deming', deming);
			});
		}
		else {
			$.error( 'Deming must not be there ... ');
		}
		return this;
	};

