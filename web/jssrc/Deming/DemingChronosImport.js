	// ========================================================================================================
	// DemingChronosImport object, UI entry point
	// ========================================================================================================
	function DemingChronosImport (elem, options) {

		MIOGA.debug.DemingChronosImport = 0;

		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (date) {
			MIOGA.logDebug ('DemingChronosImport', 1, '[refresh] Entering for day ' + date);
			var that = this; // For callback reference to current object

			if (date !== that.date) {
				var calendars = fetchEvents.call (that, date);
				that.date = date;

				// Show current date
				that.$current_date.empty ().append ($.fullCalendar.formatDate(date, this.i18n.formatDateImport, options.calendar));

				// Clear containers
				that.$owner.empty ();
				that.$group.empty ();

				var calendarEvents = [];

				$.each (calendars, function (index, calendar) {
					$.each (calendar.events, function (index, calEvent) {
						calEvent.calendar_type = calendar.type;
						calEvent.start = new Date ();
						calEvent.start.parseMiogaDate (calEvent.dstart, calEvent.allday);
						calEvent.end = new Date ();
						calEvent.end.parseMiogaDate (calEvent.dend, calEvent.allday);

						calendarEvents.push(calEvent);
					});
				});

				$.each (calendarEvents.sort(sort_task), function (index, calEvent) {
					var $cont;
					switch (calEvent.calendar_type) {
						case 'owner':
							$cont = that.$owner;
							break;
						case 'group':
							$cont = that.$group;
							break;
					}
					var activity = that.data.activities.getDetails (calEvent.category_id, calEvent.sub_category_id, calEvent.sub_sub_category_id);
					var $entry = $('<li></li>').appendTo ($cont);

					// Show activity name and calEvent notes
					var $date_range_form;
					var offset = 0;
					if (!calEvent.allday) {
						var dtstart = calEvent.start;
						var dtend = calEvent.end;
					}
					else {
						//$('<div class="form form-item></div>"></div>').appendTo ($('<div class="dates"></div>').append (i18n.all_day_event).appendTo ($entry)).hide ();

						// Get event dates from firstHour and dayLength
						MIOGA.logDebug ('DemingChronosImport', 1, 'firstHour ' + that.options.firstHour);
						var dtstart = new Date (date.getTime ());
						dtstart.setHours (that.options.firstHour);
						dtstart.setMinutes (0);
						dtstart.setSeconds (0);
						calEvent.dstart = dtstart.formatMioga (true, true);
						MIOGA.logDebug ('DemingChronosImport', 1, 'dstart ' + calEvent.dstart);

						var dtend = new Date (date.getTime());		// Event is all day, thus spreads the whole day
						var items = that.options.dayLength.split(":");
						MIOGA.logDebug ('DemingChronosImport', 1, 'items ', items);
						dtend.setHours (that.options.firstHour + parseInt(items[0], 10));
						dtend.setMinutes (parseInt(items[1], 10));
						dtend.setSeconds (0);
						MIOGA.logDebug ('DemingChronosImport', 1, 'dtend ' + dtend);
						calEvent.dend = dtend.formatMioga (true, true);
						MIOGA.logDebug ('DemingChronosImport', 1, 'dend ' + calEvent.dend);
						deming.mainPage.correctHours(dtstart, dtend, offset);
						offset += 1;
					}
					var isOverlapping = deming.mainPage.isOverlapping(dtstart, dtend);
					if (isOverlapping) {
						$entry.addClass("overlapping");
					}
					else {
						if (that.data.colors[calEvent.category_id] !== undefined) {
							$entry.css ({
								color: that.data.colors[calEvent.category_id]['fgcolor'],
								'background-color': that.data.colors[calEvent.category_id]['bgcolor']
							});
						}
					}

					var $box_title = dtstart.formatMioga (true).replace (/^([\d]+)-([\d]+)-([\d]+)/, '').replace (/:([\d]+)$/, '') + ' - ' + dtend.formatMioga (true).replace (/^([\d]+)-([\d]+)-([\d]+)/, '').replace (/:([\d]+)$/, '');
					$('<div class="dates"></div>').append ($box_title).appendTo ($entry);

					$('<h1></h1>').append (activity.activity.name).appendTo ($entry);
					if (activity.sub_activity !== undefined) {
						$('<h2></h2>').append (activity.sub_activity.name).appendTo ($entry);
						if (activity.sub_sub_activity !== undefined) {
							$('<h3></h3>').append (activity.sub_sub_activity.name).appendTo ($entry);
						}
					}
					$('<div class="notes"></div>').append (calEvent.subject).appendTo ($entry);

					// Import callback
					var import_cb = function (imp_start, imp_end) {
						MIOGA.logDebug ('DemingChronosImport', 1, 'importCB imp_start = ' + imp_start + ' imp_end = ' + imp_end);
						
						if (deming.mainPage.isOverlapping(imp_start, imp_end)) {
								alert (i18n.overlaps_event);
						}
						else {
							var entry = new DemingEntry ({
								activity: calEvent.category_id,
								sub_activity: calEvent.sub_category_id,
								sub_sub_activity: calEvent.sub_sub_category_id,
								notes: calEvent.subject,
								dtstart: imp_start,
								dtend: imp_end,
								import_ref: calEvent.rowid
							});
							var res = entry.store ();
							if (res.success === true) {
								var stored_entry = new DemingEntry (res.entry);
								deming.pushEntry (stored_entry);
								$entry.remove ();
							}
							else {
								// TODO
							}
						}
					}

					// Import entry on click
					$entry.attr ('title', i18n.click_here_to_import).click (function () {
						import_cb (dtstart, dtend);
					});
				});
			}

			MIOGA.logDebug ('DemingChronosImport', 1, '[refresh] Leaving.');
		};


		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// sort_task
		// Sort given task function of
		//        start date
		//        allday first
		// --------------------------------------------------------
		function sort_task(a, b) {
			MIOGA.logDebug("Deming", 1, 'sort_task  a.start ' + a.start + ' b.start = ' + b.start);
			var ret_val = 0;
			// if one is allday and not the other choose the allday one first
			if (a.allday && !b.allday) {
					ret_val = -1;
			}
			else if (!a.allday && b.allday) {
					ret_val = 1;
			}
			// all tasks are allday or all tasks are not
			else  {
				if (a.start < b.start) {
					ret_val = -1;
				}
				else if (a.start > b.start) {
					ret_val = 1;
				}
				else {
					ret_val = 0;
				}
			}

			MIOGA.logDebug("EiffelMain", 1, ' ===> retval = ' + ret_val);

			return ret_val;
		}



		function fetchEvents (date) {
			MIOGA.logDebug ('DemingChronosImport', 1, '[fetchEvents] Entering for day ' + date);

			// Prepare arguments
			var range = {
				start_year: date.getUTCFullYear (),
				start_month: date.getUTCMonth () + 1,
				start_day: date.getUTCDate (),
				end_year: date.getUTCFullYear (),
				end_month: date.getUTCMonth () + 1,
				end_day: date.getUTCDate ()
			};

			// Fetch calEvent
			var calendars = [];
			$.ajax ({
				url: 'GetCalendarEvents.json',
				type: 'GET',
				async: false,
				traditional: true,
				dataType: 'json',
				data: range,
				success: function (data) {
					MIOGA.logDebug ('DemingChronosImport', 3, '[fetchEvents] GetCalendarEvents.json returned, data: ', data);
					calendars = data.calendars;
				},
				error: function (jqXHR, textStatus, errorThrown) {
					MIOGA.logError ('Internal error', true);
				}
			});

			MIOGA.logDebug ('DemingChronosImport', 1, '[fetchEvents] Leaving, calEvent: ', calendars);
			return (calendars);
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DemingChronosImport', 1, '[new] Entering.');
		var that = this; // For callback reference to current object
		this.elem       = elem;

		this.i18n = options.i18n;
		var i18n = options.i18n;

		var deming = options.deming;

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="chronos-import-container"></div>').appendTo (this.elem);

		// Process defaults
		var defaults = {
			firstHour: 9,
			dayLength: 8
		};
		this.options =  $.extend (true, {}, defaults, options);

		that.data = {
			activities: that.options.activities,
			colors: that.options.activities.getColorHash ()
		};

		// Add date selector
		var $cont = $('<div class="header"></div>').appendTo (this.$cont);
		$('<button class="button normal"></button>').append (i18n.prev_day).appendTo ($cont).click (function () {
			var date = new Date (that.date.getTime () - 86400000);		// Remove number of milliseconds in a day from date passed as argument
			that.refresh (date);
		});
		this.$current_date = $('<span class="current-date"></span>').appendTo ($cont);

		$('<button class="button normal"></button>').append (i18n.next_day).appendTo ($cont).click (function () {
			var date = new Date (that.date.getTime () + 86400000);		// Add number of milliseconds in a day to date passed as argument
			that.refresh (date);
		});

		// Create multi-tab pane
		var $tab_container = $('<div class="tab-container"></div>').appendTo (this.$cont);
		var $tabs = $('<ul></ul>').appendTo ($tab_container);
		$('<li></li>').append ($('<a href="#chronos-cal-owner"></a>').append (i18n.my_calendars)).appendTo ($tabs);
		$('<li></li>').append ($('<a href="#chronos-cal-group"></a>').append (i18n.my_groups_calendars)).appendTo ($tabs);
		that.$owner = $('<ul class="events"></ul>').appendTo ($('<div class="cal-event-list" id="chronos-cal-owner"></div>').appendTo ($tab_container));
		that.$group = $('<ul class="events"></ul>').appendTo ($('<div class="cal-event-list" id="chronos-cal-group"></div>').appendTo ($tab_container));
		$tab_container.tabs ();

		// Display data, if any
		if (this.options.data !== undefined) {
			this.refresh (this.options.data);
		}

		MIOGA.logDebug ('DemingChronosImport', 1, '[new] Leaving.');
	};
