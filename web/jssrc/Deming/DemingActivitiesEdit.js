	// ========================================================================================================
	// DemingActivitiesEdit object, UI entry point
	// ========================================================================================================
	function DemingActivitiesEdit (elem, options) {


		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data) {
			MIOGA.logDebug ('DemingActivitiesEdit', 1, '[refresh] Entering', data);
			var that = this; // For callback reference to current object

			that.data = data;

			// Menu
			deming.generateMenu ();

			// List
			that.$list.empty ();
			$.each (that.data.attr, function (index, entry) {
				// Create list entry
				var $entry = createActivityEntry.call (that, entry);
				that.$list.append ($entry);

				// If item has sub-values, then create sublist
				if ((entry.children !== undefined) && (entry.children.length !== 0)) {
					var $sublist = $('<ul class="activity-list"></ul>').appendTo ($entry).sortable ();
					$.each (entry.children, function (index, subentry) {
						$sublist.append (createActivityEntry.call (that, subentry, entry));
					});
				}
			});

			MIOGA.logDebug ('DemingActivitiesEdit', 1, '[refresh] Leaving.');
		};


		// ========================================================
		// Private methods
		// ========================================================


		// --------------------------------------------------------
		// createActivityEntry
		// Create an entry into activity list.
		// First argument 'entry' is a hash describing the entry,
		// second argument 'parent_entry' (optional) references parent entry, thus no more sub-entries can be created below.
		// If first argument 'entry' is undefined, then an empty (with input field) entry is created
		// --------------------------------------------------------
		function createActivityEntry (entry, parent_entry) {
			MIOGA.logDebug ('DemingActivitiesEdit', 1, '[createActivityEntry] Entering (parent_entry: ' + (parent_entry !== undefined) ? true : false + ')', entry);

			var $entry = $('<li class="activity"></li>');

			// Handle external activities (Haussmann)
			if ((entry !== undefined) && (entry.external !== undefined) && (entry.external === true)) {
				$entry.addClass ('external');
			}

			// Add activity attributes
			if ((entry !== undefined) && (entry.rowid !== undefined)) {
				$entry.attr ('rowid', entry.rowid);
			}
			else if (parent_entry !== undefined) {
				if (parent_entry.children === undefined) {
					parent_entry.children = [ ];
				}
				entry = { };
				parent_entry.children.push (entry);
			}
			else {
				entry = ((entry === undefined) || (!entry.external)) ? { } : { external: true };
				that.data.attr.push (entry);
			}

			// Container for activity
			var $cont = $('<div class="activity-name"></div>').appendTo ($entry);

			// Deletion icon
			$('<div class="item-delete"></div>').append (i18n.delete_activity).attr ('title', i18n.delete_activity).appendTo ($cont).click (function () {
				$entry.remove ();
			});
			// Creation icon, unless entry is a sub-entry or is a external entry
			if ((!parent_entry) && ((entry === undefined) || (entry.external !== true))) {
				$('<div class="item-create"></div>').append (i18n.create_subactivity).attr ('title', i18n.create_subactivity).appendTo ($cont).click (function (event) {
					var $sublist = $entry.find ('ul');
					if ($sublist.length === 0) {
						$sublist = $('<ul class="activity-list"></ul>').appendTo ($entry).sortable ();
					}
					var $node = createActivityEntry.call (that, undefined, entry);
					$sublist.prepend ($node);
					// Empty node added, with 'input', trigger focus (can't be done before as the 'input' is not attached to DOM)
					$node.find ('input').focus ();

					// Rise hasChanges flag into underlying object
					that.data.hasChanges = true;
				});
			}


			// Activity-rename handler
			var activity_renamer = function () {
				// Input field to set activity label
				var $cont = $('<div class="activity-renamer"></div>');
				var $input = $('<input type="text"/>').val (entry.name).appendTo ($cont);
				var $validator = $('<div class="item-validate"></div>').append (i18n.validate).attr ('title', i18n.validate).appendTo ($cont);
				var $cancelator = $('<div class="item-cancel"></div>').append (i18n.cancel).attr ('title', i18n.cancel).appendTo ($cont);

				// Input validate function
				var validate_input = function () {
					if ($input.val () !== '') {
						entry.name = $input.val ();
						var $span = $('<span class="activity-name"></span>').append (entry.name);
						$span.on ('click', activity_renamer);
						$cont.replaceWith ($span);

						// Rise hasChanges flag into underlying object
						that.data.hasChanges = true;
					}
				};

				// Input cancel function
				var cancel_input = function () {
					var $span = $('<span class="activity-name"></span>').append (entry.name);
					$span.on ('click', activity_renamer);
					$cont.replaceWith ($span);
				};

				// Click handlers to validate/cancel input
				$validator.click (validate_input);
				$cancelator.click (validate_input);
				$input.on ('keypress', function (evt) {
					if (evt.keyCode == 13) {
						validate_input ();
					}
					else if (evt.keyCode == 27) {
						cancel_input ();
					}
				});

				$(this).replaceWith ($cont);
				$input.focus ();
			};

			// Entry contents
			if ((entry !== undefined) && (entry.name !== undefined)) {
				var $name = $('<span class="activity-name"></span>').append (entry.name).appendTo ($cont);
				$name.on ('click', activity_renamer);
			}
			else {
				// Input field to set activity label
				var $input = $('<input type="text"/>').appendTo ($('<div class="activity-renamer"></div>').appendTo ($cont));
				var $validator = $('<div class="item-validate"></div>').append (i18n.validate).appendTo ($cont);

				// Input validate function
				var validate_input = function () {
					if ($input.val () !== '') {
						entry.name = $input.val ();
						var $name = $('<span class="activity-name"></span>').append (entry.name);
						$input.replaceWith ($name);
						$name.on ('click', activity_renamer);
						$validator.remove ();
					}
				}

				// Click handlers to validate input
				$validator.click (validate_input);
				$input.on ('keypress', function (evt) {
					if (evt.keyCode == 13) {
						validate_input ();
					}
				});
			}

			// Color-picker
			if (!parent_entry) {
				var $picker = $('<div class="color-picker"></div>').appendTo ($cont);
				var disable_sortable = function () {
					$('ul.activity-list').sortable ('disable');
				};
				var enable_sortable = function () {
					$('ul.activity-list').sortable ('enable');
				};
				var $fg = $('<input id="fgcolor" name="bgcolor"/>').attr ('title', 'truc').appendTo ($picker).minicolors ({textfield: false, defaultValue: entry.fgcolor, show: disable_sortable, hide: enable_sortable, change: function () {
					entry.fgcolor = $fg.minicolors ('value');

					// Rise hasChanges flag into underlying object
					that.data.hasChanges = true;
				}});
				var $bg = $('<input id="bgcolor" name="bgcolor"/>').appendTo ($picker).minicolors ({textfield: false, defaultValue: entry.bgcolor, show: disable_sortable, hide: enable_sortable, change: function () {
					entry.bgcolor = $bg.minicolors ('value');

					// Rise hasChanges flag into underlying object
					that.data.hasChanges = true;
				}});
			}

			MIOGA.logDebug ('DemingActivitiesEdit', 1, '[createActivityEntry] Leaving.');
			return ($entry);
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DemingActivitiesEdit', 1, '[new] Entering.');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };

		var i18n = options.i18n;

		var deming = options.deming;

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="activities-container form"></div>').appendTo (this.elem);

		// Process defaults
		var defaults = {
		};
		this.options =  $.extend (true, {}, defaults, options);

		// --------------------------------------------------------
		// Button bar
		// --------------------------------------------------------
		var $button_bar = $('<ul class="button_list"></ul>').appendTo (this.$cont);
		// Save button
		$('<button class="button"></button>').append (i18n.save).appendTo ($('<li></li>').appendTo ($button_bar)).click (function () {
			that.data.store ();
			return (false);
		});
		// Cancel button
		$('<button class="button cancel"></button>').append (i18n.cancel).appendTo ($('<li></li>').appendTo ($button_bar)).click (function () {
			if (that.data.hasChanges) {
				if (confirm (i18n.cancel_confirm)) {
					window.location.hash = '';
				}
			}
			else {
				window.location.hash = '';
			}
			return (false);
		});


		// --------------------------------------------------------
		// Create activity list container
		// --------------------------------------------------------
		var $button_bar = $('<ul class="button_list list-actions"></ul>').appendTo (this.$cont);
		$('<button class="button normal"></button>').append (i18n.add_activity).appendTo ($('<li></li>').appendTo ($button_bar)).click (function () {
			var $node = createActivityEntry.call (that, undefined);
			that.$list.prepend ($node);
			// Empty node added, with 'input', trigger focus (can't be done before as the 'input' is not attached to DOM)
			$node.find ('div.activity-renamer input').focus ();

			// Rise hasChanges flag into underlying object
			that.data.hasChanges = true;
		});
		$('<button class="button normal"></button>').append (i18n.add_external).appendTo ($('<li></li>').appendTo ($button_bar)).click (function () {
			var $node = createActivityEntry.call (that, {external: true});
			that.$list.prepend ($node);
			// Empty node added, with 'input', trigger focus (can't be done before as the 'input' is not attached to DOM)
			$node.find ('div.activity-renamer input').focus ();

			// Rise hasChanges flag into underlying object
			that.data.hasChanges = true;
		});
		this.$list = $('<ul class="activity-list"></ul>').appendTo (this.$cont).sortable ();
		// $(document).on ('click', 'span.activity-name', activity_renamer);

		// Display data, if any
		if (this.options.data !== undefined) {
			this.refresh (this.options.data);
		}

		MIOGA.logDebug ('DemingActivitiesEdit', 1, '[new] Leaving.');
	};
