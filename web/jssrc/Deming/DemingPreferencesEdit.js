	// ========================================================================================================
	// DemingPreferencesEdit object, UI entry point
	// ========================================================================================================
	function DemingPreferencesEdit (elem, options) {

		MIOGA.debug.DemingPreferencesEdit = 0;

		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data) {
			MIOGA.logDebug ('DemingPreferencesEdit', 1, '[refresh] Entering', data);
			var that = this; // For callback reference to current object

			that.data = data;
			that.$first_hour.val (data.prefs.firstHour);

			var re = /([0-9]*):([0-9]*)/;
			if (re.test(data.prefs.dayLength)) {
				MIOGA.logDebug ('DemingPreferencesEdit', 1, 'match for dayLength ' + data.prefs.dayLength );
				that.$day_length.timepicker('setTime', data.prefs.dayLength);
			}
			else {
				MIOGA.logDebug ('DemingPreferencesEdit', 1, 'NO match for dayLength ' + data.prefs.dayLength );
				that.$day_length.timepicker('setTime', "08:00");
			}

			if (data.prefs.weekends === 1) {
				that.$weekends.attr('checked', true);
			}
			else {
				that.$weekends.removeAttr('checked');
			}

			MIOGA.logDebug('DemingPreferencesEdit', 1, '[refresh] Leaving.');
		};

		// --------------------------------------------------------
		// Return current preferences
		// --------------------------------------------------------
		this.getPreferences = getPreferences;
		function getPreferences () {
			MIOGA.logDebug ('DemingPreferencesEdit', 1, '[getPreferences] Entering');

			var prefs = { };

			var val = parseInt (that.$first_hour.val (), 10);
			if (!isNaN (val)) {
				prefs.firstHour = val;
			}

			prefs.dayLength = that.$day_length.timepicker('getTime');

			// week ends
			prefs.weekends = that.$weekends.is (':checked') ? true : false;

			return (prefs);
		}


		// ========================================================
		// Private methods
		// ========================================================


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DemingPreferencesEdit', 1, '[new] Entering.');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };

		var i18n = options.i18n;

		var deming = options.deming;

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="preferences-container form"></div>').appendTo (this.elem);

		// Process defaults
		var defaults = {
		};
		this.options =  $.extend (true, {}, defaults, options);

		var $cont = $('<div class="form-item"></div>').appendTo (this.$cont);
		$('<label for="weekends"></label>').append (i18n.show_weekends).appendTo ($cont);
		this.$weekends = $('<input type="checkbox" name="weekends" id="weekends"/>').appendTo ($cont);

		var $cont = $('<div class="form-item"></div>').appendTo (this.$cont);
		$('<label for="firstHout"></label>').append (i18n.first_hour).appendTo ($cont);
		this.$first_hour = $('<input type="text" name="firstHour" id="firstHour"/>').appendTo ($cont);

		var $cont = $('<div class="form-item"></div>').appendTo (this.$cont);
		$('<label for="dayLength"></label>').append (i18n.day_length).appendTo ($cont);
		this.$day_length = $('<input type="text" name="dayLength" id="dayLength"/>').appendTo ($cont);
		this.$day_length.timepicker({
										hourText : i18n.hourText
										, minuteText : i18n.minuteText
									});

		MIOGA.logDebug ('DemingPreferencesEdit', 1, '[new] Leaving.');
	};
