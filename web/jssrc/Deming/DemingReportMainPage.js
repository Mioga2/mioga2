	// ========================================================================================================
	// DemingReportMainPage object, UI entry point
	// ========================================================================================================
	function DemingReportMainPage (elem, options) {


		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data) {
			MIOGA.logDebug ('DemingReportMainPage', 1, '[refresh] Entering', data);
			var that = this; // For callback reference to current object

			that.data = data;

			// Refresh view
			if (that.$view !== undefined) {
				that.$view.refresh (data, that.options.filter.grouping.find ('select').val ());
			}

			MIOGA.logDebug ('DemingReportMainPage', 1, '[refresh] Leaving.');
		};


		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// Get filter data from form elements
		// --------------------------------------------------------
		function getFilter () {
			MIOGA.logDebug ('DemingReportMainPage', 1, '[getFilter] Entering');
			var filter = {
				raw: 1,
				activities: [ ]
			};

			// Get date range
			that.options.filter.dates.find ('input').each (function (index, input) {
				filter[$(input).attr ('name')] = $(input).datepicker ('getDate').formatMioga ();
			});

			// Parse selected activities
			if (that.options.filter.activities.find ('select#activity').is (':visible')) {
				if ((that.options.filter.activities.find ('select#sub_activity').is (':visible')) && (that.options.filter.activities.find ('select#sub_activity').val () !== '')) {
					filter.activities.push (that.options.filter.activities.find ('select#activity').val  () + '-' + that.options.filter.activities.find ('select#sub_activity').val ())
				}
				else {
					filter.activities.push (that.options.filter.activities.find ('select#activity').val ());
				}
			}

			// Parse selected groups
			if (that.options.filter.groups.find ('select#group').is (':visible')) {
				filter.groups.push (that.options.filter.groups.find ('select#group').val ());
			}

			MIOGA.logDebug ('DemingReportMainPage', 1, '[getFilter] Filter: ', filter);
			return (filter);
		}

		// --------------------------------------------------------
		// Get filtered data from server
		// --------------------------------------------------------
		function filterData () {
			var filter = getFilter.call (that);

			// POST to Mioga2
			var success = false;
			var message = '[DemingReportMainPage::filterData] Failed';	// To be replaced with message from server
			var entries = [ ];
			$.ajax ({
				url: 'GetEntries.json',
				type: 'POST',
				async: false,
				traditional: true,
				dataType: 'json',
				data: filter,
				success: function (data) {
					MIOGA.logDebug ('DemingReportMainPage', 2, "[filterData] GetEntries.json POST returned, data: ", data);
					success = data.success;
					message = data.message;
					errors = data.errors;
					entries = [ ];
					if ((data.entries !== undefined) && (data.entries.length)) {
						$.each (data.entries, function (index, entry) {
							var entry_obj = new DemingEntry (entry);
							entries.push (entry_obj);
						});
					}
				}
			});

			MIOGA.logDebug ('DemingReportMainPage', 1, '[filterData] Leaving, entries: ', entries);
			return (entries);
		}

		// --------------------------------------------------------
		// Get data as CSV
		// --------------------------------------------------------
		function exportTable (raw) {
			var filter = getFilter.call (that);

			// Create a hidden form from filter arguments, then POST it to get the CVS file
			var $form = $('<form style="display:none" target="_blank" action="ExportEntries" method="POST"></form>');
			
			$.each (filter.activities, function (index, rowid) {
				$('<input name="activities"></input>').attr ('value', rowid).attr ('checked', true).appendTo ($form);
			});
			$('<input type="text" name="start"></input>').attr ('value', filter.start).appendTo ($form);
			$('<input type="text" name="end"></input>').attr ('value', filter.end).appendTo ($form);
			$('<input type="text" name="raw"></input>').attr ('value', 1).appendTo ($form);		// Only raw data

			$form.appendTo ('body').submit ().remove ();
		}

		// --------------------------------------------------------
		// Get instance users and return them as a hash
		// --------------------------------------------------------
		function getUsers () {
			// Get from Mioga2
			var users = { };
			$.ajax ({
				url: 'GetUsers.json',
				type: 'GET',
				async: false,
				traditional: true,
				dataType: 'json',
				success: function (data) {
					MIOGA.logDebug ('DemingReportMainPage', 2, "[getUsers] GetEntries.json POST returned, data: ", data);
					if ((data.users !== undefined) && (data.users.length)) {
						$.each (data.users, function (index, user) {
							users[user.rowid] = user.label;
						});
					}
				}
			});

			MIOGA.logDebug ('DemingReportMainPage', 1, '[getUsers] Leaving, users: ', users);
			return (users);
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DemingReportMainPage', 1, '[new] Entering.');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.activity_fetch_status = false;

		var i18n = options.i18n;

		var deming = options.deming;

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="report"></div>').appendTo (this.elem);

		// Process defaults
		var defaults = {
			filter: {
				groups: $('<ul></ul>'),
				activities: $('<ul></ul>'),
				dates: $('<div></div>'),
				grouping: $('<div></div>')
			},
			sources: { },
			day_duration: 8
		};
		this.options =  $.extend (true, {}, defaults, options);

		// Activity-selector
		that.options.filter.activities.find ('input[type=radio]').change (function () {
			var $activity_selector = that.options.filter.activities.find ('div.activity-selector');
			// User selected 'single activity'
			if (($(this).attr ('value') === 'single') && $(this).is (':checked')) {
				// Place all activities into 'select' element
				var $select = that.options.filter.activities.find ('select[name=activity]');
				if ((that.options.activities !== undefined) && (that.options.activities.attr !== undefined) && (that.options.activities.attr.length)) {
					$select.empty ();
					$.each (that.options.activities.attr, function (index, act) {
						$('<option></option>').attr ('value', act.rowid).append (act.name).appendTo ($select);
					});
					// On activity change, check if sub-activities should be displayed
					$select.change (function () {
						var act_id = parseInt ($(this).val (), 10);
						var act = $.grep (that.options.activities.attr, function (activity) { return (activity.rowid === act_id); })[0];
						if (act.children !== undefined) {
							that.options.filter.activities.find ('div.sub-activity-selector label span').attr ('class', that.options.sources[act.rowid]);
							var $select = that.options.filter.activities.find ('div.sub-activity-selector select');
							$select.empty ();
							$select.append ('<option></option>').append (i18n.all_sub_activities);
							$.each (act.children, function (index, subact) {
								$('<option></option>').attr ('value', subact.rowid).append (subact.name).appendTo ($select);
							});
							that.options.filter.activities.find ('div.sub-activity-selector').show ();
						}
						else {
							that.options.filter.activities.find ('div.sub-activity-selector').hide ();
						}
					});
					$select.trigger ('change');
				}
				$activity_selector.show ();
			}
			else {
				that.options.filter.activities.find ('div.sub-activity-selector').hide ();
				$activity_selector.hide ();
			}
		});
		that.options.filter.activities.find ('input[type=radio]').trigger ('change');

		// Group-selector
		that.options.filter.groups.find ('input[type=radio]').change (function () {
			var $group_selector = that.options.filter.groups.find ('div.group-selector');
			// User selected 'single group'
			if (($(this).attr ('value') === 'single') && $(this).is (':checked')) {
				$group_selector.show ();
			}
			else {
				$group_selector.hide ();
			}
		});
		that.options.filter.groups.find ('input[type=radio]').trigger ('change');

		// Set initial date range
		that.options.filter.dates.find ('input').each (function (index, input) {
			var $input = $(input);

			// Compute first and last day of month as Date objectCompute first and last day of month as Date objects
			var dstart = new Date ();
			dstart.setDate (1);
			var dend = new Date ();
			dend.setFullYear (dend.getFullYear (), dend.getMonth () + 1, 0);
			var dates = {
				start: dstart,
				end: dend
			};

			$input.datepicker ({
				dateFormat: 'yy-mm-dd'
			});
			$input.datepicker ('setDate', dates[$input.attr ('name')]);
		});

		// List actions
		var $actions = $('div.filter ul.button_list');
		$('<button class="button normal"></button>').append (i18n.csv_raw_export).appendTo ($('<li></li>').appendTo ($actions)).click (function () {
			exportTable.call (that, true);
			return (false);
		});

		// Add report view
		that.$view = new DemingReportView (that.$cont, {
			deming: this,
			i18n: options.i18n,
			activities: that.options.activities,
			users: getUsers.call (that),
			day_duration: that.options.day_duration
		});

		// Filter update button
		if (that.$view !== undefined) {
			var $update = $('<button class="button"></button>').append (i18n.view).appendTo ($('<li></li>').appendTo ($('div.filter ul.button_list')));
			$update.click (function () {
				that.refresh (filterData.call (that));
				return (false);
			});
		}

		// No view, expand filter and place export buttons into tab bar
		if (that.$view === undefined) {
			$('div.deming-report div.filter').css ({
				width: 'auto',
				bottom: '0px',
				left: '0px',
				right: '0px'
			});
			$actions.appendTo ($('div.filter ul.ui-tabs-nav'));
			this.$cont.hide ();
		}

		// Display data, if any
		if (this.options.data !== undefined) {
			this.refresh (this.options.data);
		}

		MIOGA.logDebug ('DemingReportMainPage', 1, '[new] Leaving.');
	};
