// ========================================================
// DemingActivities OO-interface to activities
// ========================================================
function DemingActivities (options) {
	// ========================================================
	// Default values
	// ========================================================
	defaults = {
	},

	// ========================================================
	// Public methods
	// ========================================================

	// --------------------------------------------------------
	// fetch, fetch activities from Mioga2
	// --------------------------------------------------------
	this.fetch = fetch;
	function fetch () {
		MIOGA.logDebug ('DemingActivities', 2, '[fetch] Entering');

		this.attr = { };
		$.ajax ({
			url: 'GetActivities.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			success: function (data) {
				MIOGA.logDebug ('DemingActivities', 3, '[fetch] GetActivities.json returned, data: ', data);
				that.attr = data.activities;
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('DemingActivities', 2, '[fetch] Leaving, data: ', that.attr);
	};

	// --------------------------------------------------------
	// store, store activities to Mioga2
	// --------------------------------------------------------
	this.store = store;
	function store () {
		MIOGA.logDebug ('DemingActivities', 1, '[store] Entering');

		// TODO
		var success = false;
		var message = '[DemingActivities::store] Failed';	// To be replaced with message from server
		var errors = [ ];
		var activities = [ ];

		MIOGA.logDebug ('DemingActivities', 1, '[store] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, activities: activities});
	};

	// --------------------------------------------------------
	// getColorHash, translate activities to a hash:
	// 	{
	// 		activity_id: {
	// 			bgcolor: ...,
	// 			fgcolor: ...
	// 		},
	// 		...
	// 	}
	// --------------------------------------------------------
	this.getColorHash = getColorHash;
	function getColorHash () {
		MIOGA.logDebug ('DemingActivities', 1, '[getColorHash] Entering');

		var colors = { };
		if ((this.attr !== undefined) && (this.attr.length)) {
			$.each (this.attr, function (index, activity) {
				var current_colors = {
					fgcolor: activity.fgcolor,
					bgcolor: activity.bgcolor
				};
				colors[activity.rowid] = current_colors;
			});
		}

		MIOGA.logDebug ('DemingActivities', 1, '[getColorHash] Leaving, data: ', colors);
		return (colors);
	};

	// --------------------------------------------------------
	// getDetails, get activity details from ID
	// 	{
	// 		activity: "...",
	// 		sub_activity: "...",
	// 		sub_sub_activity; "..."
	// 	}
	// --------------------------------------------------------
	this.getDetails = getDetails;
	function getDetails (activity, sub_activity, sub_sub_activity) {
		MIOGA.logDebug ('DemingActivities', 1, '[getDetails] Entering, activity: ' + activity + ', sub_activity: ' + sub_activity + ', sub_sub_activity: ' + sub_sub_activity);

		// Generate unique code from combination of activities
		var code = activity;
		if ((sub_activity !== null) && (sub_activity !== undefined)) {
			code += '-' + sub_activity;
			if ((sub_sub_activity !== null) && (sub_sub_activity !== undefined)) {
				code += '-' + sub_sub_activity;
			}
		}

		// Create a hash with all possible combinations of activity codes to get details quickly
		if (that.details === undefined) {
			if ((this.attr !== undefined) && (this.attr.length)) {
				that.details = { };
				$.each (this.attr, function (index, activity) {
					var current_activity = {
						rowid: activity.rowid,
						name: activity.name
					};
					that.details[activity.rowid] = {
						activity: current_activity
					};
					if ((activity.children !== undefined) && (activity.children.length)) {
						$.each (activity.children, function (index, child) {
							var current_sub_activity = {
								rowid: child.rowid,
								name: child.name
							};
							that.details[activity.rowid + '-' + child.rowid] = {
								activity: current_activity,
								sub_activity: current_sub_activity
							};
							if ((child.children !== undefined) && (child.children.length)) {
								$.each (child.children, function (index, grandchild) {
									that.details[activity.rowid + '-' + child.rowid + '-' + grandchild.rowid] = {
										activity: current_activity,
										sub_activity: current_sub_activity,
										sub_sub_activity: {
											rowid: grandchild.rowid,
											name: grandchild.name
										}
									};
								});
							}
						});
					}
				});
			}
		}

		MIOGA.logDebug ('DemingActivities', 1, '[getDetails] Leaving, data: ', that.details[code]);
		return (that.details[code]);
	};


	// ========================================================
	// Constructor
	// ========================================================
	MIOGA.logDebug ('DemingActivities', 1, '[new] Entering, options: ', options);
	var that = this;

	this.attr = $.extend (true, {}, this.defaults, options);

	if ((options === undefined) || (Object.keys (options).length === 0)) {
		this.fetch ();
	}

	MIOGA.logDebug ('DemingActivities', 1, '[new] Leaving, data: ', that.attr);
}
