// ========================================================
// DemingEntry OO-interface to Deming entry
// ========================================================
function DemingEntry (options) {
	MIOGA.debug.DemingEntry = 0;
	// ========================================================
	// Default values
	// ========================================================
	defaults = {
	},

	// ========================================================
	// Public methods
	// ========================================================

	// --------------------------------------------------------
	// fetch, fetch entry from Mioga2
	// --------------------------------------------------------
	this.fetch = fetch;
	function fetch () {
		MIOGA.logDebug ('DemingEntry', 2, '[fetch] Entering');

		// TODO

		MIOGA.logDebug ('DemingEntry', 2, '[fetch] Leaving, data: ', that.attr);
	};

	// --------------------------------------------------------
	// store, store entry to Mioga2
	// --------------------------------------------------------
	this.store = store;
	function store () {
		MIOGA.logDebug ('DemingEntry', 1, '[store] Entering');

		// Format internal attributes
		var entry_attr = {
			activity: that.attr.activity,
			dtstart: (that.attr.dtstart instanceof Date) ? that.attr.dtstart.formatMioga (true, true) : that.attr.dtstart,
			dtend: (that.attr.dtend instanceof Date) ? that.attr.dtend.formatMioga (true, true) : that.attr.dtend,
			notes: ((that.attr.notes !== undefined) && (that.attr.notes !== null)) ? that.attr.notes : ''
		};
		if ((that.attr.sub_activity !== undefined) && (that.attr.sub_activity !== null)) {
			entry_attr.sub_activity = that.attr.sub_activity;
		}
		if ((that.attr.sub_sub_activity !== undefined) && (that.attr.sub_sub_activity !== null)) {
			entry_attr.sub_sub_activity = that.attr.sub_sub_activity;
		}
		if (that.attr.rowid !== undefined) {
			entry_attr.rowid = that.attr.rowid;
		}
		if (that.attr.import_ref !== undefined) {
			entry_attr.import_ref = that.attr.import_ref;
		}

		// POST to Mioga2
		var success = false;
		var message = '[DemingEntry::store] Failed';	// To be replaced with message from server
		var errors = [ ];
		var entry = {rowid: this.attr.rowid};	// For further loading from WebService response
		$.ajax ({
			url: 'SetEntry.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: entry_attr,
			success: function (data) {
				MIOGA.logDebug ('DemingEntry', 2, "[store] SetEntry.json POST returned, data: ", data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.entry) {
					entry = data.entry;
				}
			}
		});

		MIOGA.logDebug ('DemingEntry', 1, '[store] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, entry: entry});
	};

	// --------------------------------------------------------
	// destroy, destroy entry from Mioga2
	// --------------------------------------------------------
	this.destroy = destroy;
	function destroy () {
		MIOGA.logDebug ('DemingEntry', 1, '[destroy] Entering');

		// Format internal attributes
		var entry_attr = {
			rowid: that.attr.rowid
		};

		// POST to Mioga2
		var success = false;
		var message = '[DemingEntry::destroy] Failed';	// To be replaced with message from server
		var errors = [ ];
		$.ajax ({
			url: 'DeleteEntry.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: entry_attr,
			success: function (data) {
				MIOGA.logDebug ('DemingEntry', 2, "[destroy] DeleteEntry.json POST returned, data: ", data);
				success = data.success;
				message = data.message;
				errors = data.errors;
			}
		});

		MIOGA.logDebug ('DemingEntry', 1, '[destroy] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors});
	};


	// ========================================================
	// Constructor
	// ========================================================
	MIOGA.logDebug ('DemingEntry', 1, '[new] Entering, options: ', options);
	var that = this;

	this.attr = $.extend (true, {}, this.defaults, options);

	if ((options === undefined) || (Object.keys (options).length === 0)) {
		this.fetch ();
	}
	else {
		// Ensure date are proper JavaScript Date objects
		if ((this.attr.dtstart !== undefined) && (!(this.attr.dtstart instanceof Date))) {
			var d = new Date ();
			d.parseMiogaDate (this.attr.dtstart);
			this.attr.dtstart = d;
		}
		if ((this.attr.dtend !== undefined) && (!(this.attr.dtend instanceof Date))) {
			var d = new Date ();
			d.parseMiogaDate (this.attr.dtend);
			this.attr.dtend = d;
		}
	}

	MIOGA.logDebug ('DemingEntry', 1, '[new] Leaving, data: ', that.attr);
}
