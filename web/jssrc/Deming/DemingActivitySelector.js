	// ========================================================================================================
	// DemingActivitySelector object, UI entry point
	// ========================================================================================================
	function DemingActivitySelector (elem, options) {


		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data, values, entry) {
			MIOGA.logDebug ('DemingActivitySelector', 1, '[refresh] Entering', data);
			var that = this; // For callback reference to current object

			that.data = data;
			if (entry !== undefined) {
				that.entry = entry;
			}
			else {
				delete (that.entry);
			}

			// Get colors for activities
			that.data.colors = that.data.getColorHash ();

			// Display activities
			displayActivities.call (that, that.data.attr, that.$activities);

			// Select expected activities
			if (values.activity !== undefined) {
				that.$activities.val (values.activity).trigger ('change');
				if ((values.sub_activity !== undefined) && (values.sub_activity !== null)) {
					that.$subactivities.val (values.sub_activity).trigger ('change');
					if ((values.sub_sub_activity !== undefined) && (values.sub_sub_activity !== null)) {
						that.$subsubactivities.val (values.sub_sub_activity).trigger ('change');
					}
				}
			}

			MIOGA.logDebug ('DemingActivitySelector', 1, '[refresh] Leaving.');
		};


		// --------------------------------------------------------
		// Get selected ((sub-)sub-)activities as a hash
		// --------------------------------------------------------
		this.getValues = getValues;
		function getValues () {
			var activities = { };

			if (!isNaN (parseInt (that.$activities.val (), 10))) {
				activities.default_activity = parseInt (that.$activities.val (), 10)
			}

			// Push sub-activities, if any
			if (that.$subactivities.is (':visible')) {
				activities.default_sub_activity = parseInt (that.$subactivities.val (), 10);
				if ((that.$subsubactivities.is (':visible'))) {
					activities.default_sub_sub_activity = parseInt (that.$subsubactivities.val (), 10);
				}
			}

			return (activities);
		}


		// ========================================================
		// Private methods
		// ========================================================


		// --------------------------------------------------------
		// Display list of activities and handle eventual sub-activities
		// --------------------------------------------------------
		var activity_sort = function (a, b) {
			a = unaccent (a.name.toLowerCase ());
			b = unaccent (b.name.toLowerCase ());

			if (a < b)	return (-1);
			if (a > b)	return (1);
			return (0);
		}
		function displayActivities (activities, $cont) {
			// Add activities to list
			$cont.empty ();
			$.each (activities.sort (activity_sort), function (index, activity) {
				var $option = $('<option></option>').attr ('value', activity.rowid).append (activity.name).appendTo ($cont);
				$option.css ({
					color: activity.fgcolor,
					'background-color': activity.bgcolor
				});
			});
			$cont.css ({
				color: activities[0].fgcolor,
				'background-color': activities[0].bgcolor
			});

			// Ensure list is visible
			$cont.parent ().show ();

			// On activity change, display sub-activity selector, if any
			$cont.unbind ('change');
			$cont.change (function () {
				var rowid = parseInt ($cont.val (), 10);
				var activity = $.grep (activities, function (act) { return (act.rowid === rowid); })[0];
				if (activity !== undefined) {
					$cont.css ({
						color: activity.fgcolor,
						'background-color': activity.bgcolor
					});
				}
				if ((activity !== undefined) && (activity.children !== undefined) && activity.children.length) {
					var $next_cont = $(this).parent ().next ().find ('select');
					displayActivities.call (that, activity.children, $next_cont)
				}
				else {
					$cont.parent ().nextAll ().has ('select').hide ();
				}


				// Update labels
				if ((that.options.sources !== undefined) && (that.options.sources[rowid] !== undefined)) {
					that.$subactivity_label.attr ('class', that.options.sources[rowid]);
					that.$subsubactivity_label.attr ('class', that.options.sources[rowid]);
				}
				else {
					that.$subactivity_label.attr ('class', 'sub_activity');
					that.$subsubactivity_label.attr ('class', 'sub_sub_activity');
				}

				// Update associated DemingEntry object, if any
				if (that.entry !== undefined) {
					that.entry.attr.activity = parseInt (that.$activities.val (), 10);
					if (that.$subactivities.is (':visible')) {
						that.entry.attr.sub_activity = parseInt (that.$subactivities.val (), 10);
					}
					else {
						delete (that.entry.attr.sub_activity);
					}
					if (that.$subsubactivities.is (':visible')) {
						that.entry.attr.sub_sub_activity = parseInt (that.$subsubactivities.val (), 10);
					}
					else {
						delete (that.entry.attr.sub_sub_activity);
					}
				}
			});
			$cont.trigger ('change');
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DemingActivitySelector', 1, '[new] Entering.');
		var that = this; // For callback reference to current object
		this.elem       = elem;

		var i18n = options.i18n;

		var deming = options.deming;

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="activity-selector-container"></div>').appendTo (this.elem);

		// Process defaults
		var defaults = {
		};
		this.options =  $.extend (true, {}, defaults, options);


		// --------------------------------------------------------
		// Add activity selection boxes
		// --------------------------------------------------------
		var $cont = $('<div class="form-item"></div>').appendTo (that.$cont);
		$('<label for="activity"></label>').append (i18n.activity).appendTo ($cont);
		that.$activities = $('<select name="activity"></select>').appendTo ($cont);
		var $cont = $('<div class="form-item"></div>').appendTo (that.$cont).hide ();
		that.$subactivity_label = $('<span></span>').appendTo ($('<label class="sub-activity" for="sub-activity"></label>').append ("&#160;").appendTo ($cont));
		that.$subactivities = $('<select name="sub-activity"></select>').appendTo ($cont);
		var $cont = $('<div class="form-item"></div>').appendTo (that.$cont).hide ();
		that.$subsubactivity_label = $('<span></span>').appendTo ($('<label class="sub-sub-activity" for="sub-sub-activity"></label>').append (i18n.sub_activity).appendTo ($cont));
		that.$subsubactivities = $('<select name="sub-sub-activity"></select>').appendTo ($cont);

		// Display data, if any
		if (this.options.data !== undefined) {
			this.refresh (this.options.data);
		}

		MIOGA.logDebug ('DemingActivitySelector', 1, '[new] Leaving.');
	};
