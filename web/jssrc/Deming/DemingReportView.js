	// ========================================================================================================
	// DemingReportView object, UI entry point
	// ========================================================================================================
	function DemingReportView (elem, options) {


		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data, grouping) {
			MIOGA.logDebug ('DemingReportView', 1, '[refresh] Entering', data);
			var that = this; // For callback reference to current object

			that.data = data;
			that.total_duration = 0;

			that.$contents.empty ();
			if ((that.data !== undefined) && that.data.length) {
				// Show / hide report columns according to grouping criteria
				switch (grouping) {
					case 'user':
						that.$head.find ('th.user').show ();
						that.$head.find ('th.activity').hide ();
						that.$head.find ('th.sub_activity').hide ();
						that.$head.find ('th.sub_sub_activity').hide ();
						break;
					case 'activity':
						that.$head.find ('th.user').show ();
						that.$head.find ('th.activity').show ();
						that.$head.find ('th.sub_activity').hide ();
						that.$head.find ('th.sub_sub_activity').hide ();
						break;
					case 'sub_activity':
						that.$head.find ('th.user').show ();
						that.$head.find ('th.activity').show ();
						that.$head.find ('th.sub_activity').show ();
						that.$head.find ('th.sub_sub_activity').hide ();
						break;
					case 'sub_sub_activity':
						that.$head.find ('th.user').show ();
						that.$head.find ('th.activity').show ();
						that.$head.find ('th.sub_activity').show ();
						that.$head.find ('th.sub_sub_activity').show ();
						break;
				}
				$.each (that.data, function (index, entry) {
					var $line = $('<tr></tr>').appendTo (that.$contents);
					$.each (that.options.columns, function (index, column) {
						var contents = formatValue.call (that, entry.attr, column, grouping);
						if (contents !== undefined) {
							$('<td></td>').addClass (column).append (contents).appendTo ($line);
						}
					});
				});
			}

			MIOGA.logDebug ('DemingReportView', 1, '[refresh] Leaving.');
		};


		// ========================================================
		// Private methods
		// ========================================================


		// --------------------------------------------------------
		// formatValue, user-friendly formating of values
		// --------------------------------------------------------
		function formatValue (entry, field, grouping) {
			// Default behavior is to return field as-is
			value = entry[field];

			// According to grouping value, some fields may not be displayed. In such a case, return undefined
			switch (grouping) {
				case 'user':
					if ((field === 'activity') || (field === 'sub_activity') || (field === 'sub_sub_activity')) {
						return (undefined);
					}
					break;
				case 'activity':
					if ((field === 'sub_activity') || (field === 'sub_sub_activity')) {
						return (undefined);
					}
					break;
				case 'sub_activity':
					if (field === 'sub_sub_activity') {
						return (undefined);
					}
					break;
			}

			// Some fields require formating
			switch (field) {
				case 'activity':
					var details = that.options.activities.getDetails (entry.activity);
					if ((details.activity !== undefined) && (details.activity.name !== undefined)) {
						value = details.activity.name;
					}
					else {
						value = '';
					}
					break;
				case 'sub_activity':
					var details = that.options.activities.getDetails (entry.activity, entry.sub_activity);
					if ((details.sub_activity !== undefined) && (details.sub_activity.name !== undefined)) {
						value = details.sub_activity.name;
					}
					else {
						value = '';
					}
					break;
				case 'sub_sub_activity':
					var details = that.options.activities.getDetails (entry.activity, entry.sub_activity, entry.sub_sub_activity);
					if ((details.sub_sub_activity !== undefined) && (details.sub_sub_activity.name !== undefined)) {
						value = details.sub_sub_activity.name;
					}
					else {
						value = '';
					}
					break;
				case 'user':
					value = that.options.users[entry.user_id];
					break;
				case 'duration':
					//var re = /^(\d+):(\d+):(\d+)/;
					//var res = re.exec(entry[field]);
					//var seconds = parseInt ((res[1] * 3600), 10) + parseInt ((res[2] * 60), 10) + parseInt (res[3]);
					//value = Math.round (seconds / (that.options.day_duration*3600) * 100) / 100;
					//that.total_duration += seconds;
					var re = /:\d+$/;
					value = entry.duration.replace(re, "");
					break;
				case 'dtstart':
				case 'dtend':
					value = entry[field].formatMioga (true);
					var re = /:\d+$/;
					value = value.replace(re, "");
					break;
				case 'notes':
					value = (entry.notes !== undefined) ? entry.notes : '';
					break;
			}

			return (value);
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DemingReportView', 1, '[new] Entering.');
		var that = this; // For callback reference to current object
		this.elem       = elem;

		var i18n = options.i18n;

		var deming = options.deming;

		var time_units = ['s', 'm', 'h', 'd'];

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="report-view"></div>').appendTo (this.elem);

		// Process defaults
		var defaults = {
			columns: ['user', 'activity', 'sub_activity', 'sub_sub_activity', 'dtstart', 'dtend', 'duration', 'notes'],
			day_duration: 8
		};
		this.options =  $.extend (true, {}, defaults, options);

		var $table = $('<table class="report list"></table>').appendTo (this.$cont);
		this.$head = $('<tr></tr>').appendTo ($('<thead></thead>').appendTo ($table));
		$.each (that.options.columns, function (index, column) {
			$('<th></th>').addClass (column).append (i18n['report_column_' + column + '_label']).appendTo (that.$head);
		});
		this.$contents = $('<tbody></tbody>').appendTo ($table);

		// Display data, if any
		if (this.options.data !== undefined) {
			this.refresh (this.options.data);
		}

		MIOGA.logDebug ('DemingReportView', 1, '[new] Leaving.');
	};
