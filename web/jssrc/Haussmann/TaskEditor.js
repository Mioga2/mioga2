// =============================================================================
/**
Create a TaskEditor object.

    var options = {
        i18n : { .... }
    };

	this.taskEditor = new TaskEditor($elem, options);

@class Haussmann
@constructor
@param {Object} $elem JQuery that will be contains this editor
@param {Object} options
**/
// =============================================================================
function TaskEditor ($elem, options) {
	"use strict";
	MIOGA.debug.TaskEditor = 0;
	MIOGA.logDebug('TaskEditor', 1, ' TaskEditor loaded');
	
	var that = this;

	this.$elem = $elem;
	this.i18n = options.i18n;
	this.haussmann = options.haussmann;
	this.generateHash = options.haussmann.generateHash;
	this.insertVocabulary = options.haussmann.insertVocabulary;
	this.project = options.project;
	// publics method declaration
	this.showWindow = showWindow;
	this.hideWindow = hideWindow;
	this.createFineblanking = createFineblanking;
	this.createTaskNote = createTaskNote;
	this.refreshGlobalInfo = refreshGlobalInfo;
	this.refreshAffectation = refreshAffectation;
	this.refreshAttendees = refreshAttendees;
	this.refreshDelegation = refreshDelegation;
	this.refreshStatus = refreshStatus;
	this.refreshGrouping = refreshGrouping;
	this.refreshFineblanking = refreshFineblanking;
	this.refreshTaskNote = refreshTaskNote;
	this.refresh = refresh;
	
	this.$cont = that.$elem.find('.task-editor');
	// ---------------------------------------------------------------------------
	// id generation
	this.information_form_id = MIOGA.generateID("haussmann");
	this.label_id = MIOGA.generateID("haussmann");
	this.description_id = MIOGA.generateID("haussmann");
	this.workload_id = MIOGA.generateID("haussmann");
	this.info_p_start_id = MIOGA.generateID("haussmann");
	this.info_p_end_id = MIOGA.generateID("haussmann");
	this.tags_id = MIOGA.generateID("haussmann");
	
	this.status_form_id = MIOGA.generateID("haussmann");
	
	this.grouping_form_id = MIOGA.generateID("haussmann");
	
	this.delegation_form_id = MIOGA.generateID("haussmann");
	this.affectation_person_id = MIOGA.generateID("haussmann");
	this.affectation_dstart_id = MIOGA.generateID("haussmann");
	this.affectation_dend_id = MIOGA.generateID("haussmann");
	this.add_attendee_id = MIOGA.generateID("haussmann");
	this.attendee_list_id = MIOGA.generateID("haussmann");
	
	this.new_unit_task_id = MIOGA.generateID("haussmann");
	this.unit_task_form_id = MIOGA.generateID("haussmann");
	
	this.edit_unit_task_form_id = MIOGA.generateID("haussmann");
	this.unit_task_label_id = MIOGA.generateID("haussmann");
	this.unit_task_workload_id = MIOGA.generateID("haussmann");
	this.unit_task_checked_id = MIOGA.generateID("haussmann");
	this.unit_task_comment_id = MIOGA.generateID("haussmann");
	this.unit_task_date_id = MIOGA.generateID("haussmann");
	
	this.task_note_form_id = MIOGA.generateID("haussmann");
	this.edit_note_form_id = MIOGA.generateID("haussmann");
	this.edit_note_text_id = MIOGA.generateID("haussmann");
	// End of constructor 
	// ---------------------------------------------------------------------------

	// ===================================================================================
	// PRIVATES METHODS
	// ===================================================================================
	// ---------------------------------------------------------------------------
	// Checking methods
	// TODO factorize methods to checkItem (...)
	/**
	@description Private method that checks status item and draw specific error if needed.
	@method checkStatusItem
	@param Object args that represents status item like :
			{
				status : Int - index of status ( optional ),
				date : Object date of status item ( optional )
			}
	@param Function onSuccess that is callback on success checking
	@param Object $error_cont is Jquery element that is container of errors
	**/
	function checkStatusItem (args, onSuccess, $error_cont) {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::checkStatusItem - loaded');
		$error_cont.find('.error').remove();
		var checked = that.task.checkStatusItem (args);
		if (checked.status === "success") {
			onSuccess (args);
		}
		else {
			$.each(checked.error, function (i,text) {
				$error_cont.append('<p class="error">' + that.i18n[text] + '</p>');
			});
		}
	}
	/**
	@description Private method that checks progress item and draw specific error if needed.
	@method checkProgressItem
	@param Object args that represents progress item like :
			{
				value : Int - value of progress item,
				date : Object date of progress item
			}
	@param Function onSuccess that is callback on success checking
	@param Object $error_cont is Jquery element that is container of errors
	**/
	function checkProgressItem (args, onSuccess, $error_cont) {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::checkProgressItem - loaded');
		$error_cont.find('.error').remove();
		var checked = that.task.checkProgressItem (args);
		if (checked.status === "success") {
			onSuccess (args);
		}
		else {
			$.each(checked.error, function (i,text) {
				$error_cont.append('<p class="error">' + that.i18n[text] + '</p>');
			});
		}
	}
	/**
	@description Private method that checks fineblanking and draw specific error if needed.
	@method checkFineBlanking
	@param Object args that represents fineblanking like :
			{
				label : String,
				workload : Int,
				checked : Bool,
				comment : String,
				date : Object date
			}
	@param Function onSuccess that is callback on success checking
	@param Object $error_cont is Jquery element that is container of errors
	**/
	function checkFineBlanking (args, onSuccess, $error_cont) {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::checkFineBlanking - loaded');
		$error_cont.find('.error').remove();
		var checked = that.task.checkFineBlanking (args);
		if (checked.status === "success") {
			onSuccess (args);
		}
		else {
			$.each(checked.error, function (i,text) {
				$error_cont.append('<p class="error">' + that.i18n[text] + '</p>');
			});
		}
	}
	/**
	@description Private method that checks task note and draw specific error if needed.
	@method checkTaskNote
	@param Object args that represents task note like :
			{
				text : String
			}
	@param Function onSuccess that is callback on success checking
	@param Object $error_cont is Jquery element that is container of errors
	**/
	function checkTaskNote (args, onSuccess, $error_cont) {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::checkTaskNote - loaded');
		$error_cont.find('.error').remove();
		var checked = that.taskNoteList.checkTaskNote (args);
		if (checked.status === "success") {
			onSuccess (args);
		}
		else {
			$.each(checked.error, function (i,text) {
				$error_cont.append('<p class="error">' + that.i18n[text] + '</p>');
			});
		}
	}
	/**
	@description Private method that serializes a task informations and return it as JSON Object.
	@method serializeGlobalInfo
	**/
	function serializeGlobalInfo () {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::serializeGlobalInfo - loaded');
		var $form = that.$global_info.find('#' + that.information_form_id);
		var attr = {};
		attr['label'] = $.trim($form.find('[name="label"]').val());
		attr['description'] = $.trim($form.find('[name="description"]').val());
		attr['workload'] = parseInt($.trim($form.find('[name="workload"]').val()), 10);
		if ($form.find('[name="pstart"]').val().length > 0) {
			attr['pstart'] = $.trim($form.find('[name="pstart"]').val());
		}
		else {
			attr['pstart'] = null;
		}
		if ($form.find('[name="pend"]').val().length > 0) {
			attr['pend'] = $.trim($form.find('[name="pend"]').val());
		}
		else {
			attr['pend'] = null;
		}
		attr['tags'] = [];
		$form.find('#' + that.tags_id + " span.tag").each(function () {
			attr.tags.push($.trim($(this).text()));
		});
		return attr;
	}
	/**
	@description Private method that draws a temporarily success message
	@method successMessage
	@param Object $cont that is jQuery element that will be contains a success message
	@param String message that is a success message
	**/
	function successMessage ($cont, message) {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::successMessage - loaded');
		var $message = $('<span class="success-message">' + message + '</span>').appendTo($cont).hide();
		$message.fadeIn(400, function () {
			$(this).fadeOut(2200, function () {
				$(this).remove();
			});
		});
	}
	/**
	@description Private method that makes HTML render of status item, apply behaviors to this and insert in DOM
	@method createStatusItem
	@param Int statusValue that is the status index ( sthe same index that is defined in advanced parameters with translate string corresponding.
	@param Object date
	@param String comment that is comment of status item
	**/
	function createStatusItem (statusValue, date, comment) {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::createStatusItem - loaded');
		that.$status_cont.find('span.h_delete').remove();
		that.$status_tbody.find('tr:last').before(
			'<tr class="status-item">'
				+ '<td class="status-item-status" value="' + statusValue + '">' + that.advancedParams.task_status[statusValue] + '</td>'
				+ '<td class="status-item-date">' + date + '</td>'
				+ '<td class="status-item-comment" >' + comment.replace(/\n/g, '<br>') + '</td>'
				+ '<td class="status-item-delete" ><span class="h_delete">&#160;</span></td>'
			+ '</tr>'
		);
		if (that.advancedParams.task_status[(statusValue+1)] !== undefined) {
			that.$status_status.attr('value',(statusValue+1)).text(that.advancedParams.task_status[(statusValue+1)]);
			// determination of minimum date for future creations of status item.
			that.$status_date.datepicker("option", "minDate", date).val('');
			that.$status_comment.val('');
		}
		else {
			that.$status_control_cont.hide();
		}
		if (that.$status_link_cont.find('.show-history').html() === that.i18n.show_history) {
			hideStatusHistory();
		}
	}
	/**
	@description Private method that makes HTML render of progress item, apply behaviors to this and insert in DOM
	@method createProgressItem
	@param Int value that will be represented like a percent value
	@param Object date
	**/
	function createProgressItem (value, date) {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::createProgressItem - loaded');
		that.$progress_tbody.find('tr:last').before(
			'<tr class="progress-item">'
				+ '<td class="progress-item-value" ><span>' + value + '</span><span> %</span></td>'
				+ '<td class="progress-item-date">' + date + '</td>'
			+ '</tr>'
		);
		that.$progress_value.val('');
		if (that.$progress_link_cont.find('.show-history').html() === that.i18n.show_history) {
			hideProgressHistory();
		}
	}
	/**
	@description Private method that adds a progress item. For to do this, it retrieves HTML values from form, and call checkProgressItem with callback containing request to task object.
	@method addProgressItem
	**/
	function addProgressItem () {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::addProgressItem - loaded');
		var toDay = new Date();
		var params = {
			date : that.haussmann.getStrDateFromObj(toDay),
			value : $.trim(that.$progress_value.val())
		};
		var onCheckSuccess = function (args) {
			that.task.addProgressItem (args, function () {
				that.task.setProgress(function () {
					createProgressItem (args.value, args.date);
					toggleLink (that.task.progress, that.$progress_link_cont.find('.show-history'));
					successMessage (that.$progress_link_cont, that.i18n.successCreateProgressItem);
				});
			});
		};
		checkProgressItem (params, onCheckSuccess, that.$progress_cont);
	}
	/**
	@description Private method that clears fineblanking form
	@method clearFineblankingForm
	**/
	function clearFineblankingForm () {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::clearFineblankingForm - loaded');
		that.$ut_label.val('');
		that.$ut_workload.val('');
		that.$ut_checked.attr('checked', false);
		that.$ut_date.val('');
		that.$ut_comment.val('');
	}
	/**
	@description Private method that clears task note edit form
	@method clearEditNoteForm
	**/
	function clearEditNoteForm () {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::clearEditNoteForm - loaded');
		that.$edit_note_text.val('');
	}
	/**
	@description Private method that tests if current status is last possible status.
				It draws read mod link or edit mod link according to result.
				It draws the next status and shows or hide add status container and thead of table according to result.
	@method showEditStatusIfNeeded
	**/
	function showEditStatusIfNeeded () {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::showEditStatusIfNeeded - loaded');
		that.$status_fieldset.find('.error').remove();
		var adv_status_index = that.advancedParams.task_status.length-1;
		var task_status_index = that.task.status.length-1;
		that.$status_link_cont.find('.edit-mod').html(that.i18n.read_mod);
		if (task_status_index < adv_status_index) {
			that.$status_status.attr('value',(task_status_index+1)).text(that.advancedParams.task_status[task_status_index+1]);
			that.$status_link_cont.find('.edit-mod').html(that.i18n.read_mod);
			that.$status_control_cont.show();
			that.$status_fieldset.find('thead').show();
		}
		else {
			that.$status_control_cont.hide();
			if (that.task.current_status !== null) {
				that.$status_fieldset.find('thead').show();
			}
			else {
				that.$status_fieldset.find('thead').hide();
			}
		}
	}
	/**
	@description Private method that draws read mod link of status and shows or hides thead of table if needed.
	@method hideStatusEdit
	**/
	function hideStatusEdit () {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::hideStatusEdit - loaded');
		that.$status_fieldset.find('.error').remove();
		that.$status_link_cont.find('.edit-mod').html(that.i18n.edit_mod);
		that.$status_control_cont.hide();
		if (that.task.current_status !== null) {
			that.$status_fieldset.find('thead').show();
		}
		else {
			that.$status_fieldset.find('thead').hide();
		}
	}
	/**
	@description Private method that shows only last status and adapts history status link
	@method hideStatusEdit
	**/
	function hideStatusHistory() {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::hideStatusHistory - loaded');
		that.$status_link_cont.find('.show-history').html(that.i18n.show_history);
		that.$status_tbody.find('.status-item').hide();
		that.$status_tbody.find('.status-item').last().show();
	}
	/**
	@description Private method that shows only last progress and adapts history progress link
	@method hideProgressHistory
	**/
	function hideProgressHistory () {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::hideProgressHistory - loaded');
		that.$progress_link_cont.find('.show-history').html(that.i18n.show_history);
		that.$progress_tbody.find('.progress-item').hide();
		that.$progress_tbody.find('.progress-item').last().show();
	}
	/**
	@description Private method that hides progress edit control and makes progress edit link to read mode
	@method hideProgressHistory
	**/
	function hideProgressEdit () {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::hideProgressEdit - loaded');
		that.$progress_link_cont.find('.edit-mod').html(that.i18n.edit_mod);
		that.$progress_control_cont.hide();
	}
	/**
	@description Private method that show progress edit control and makes progress edit link to eit mode
	@method showProgressEdit
	**/
	function showProgressEdit () {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::showProgressEdit - loaded');
		that.$progress_cont.find('.error').remove();
		that.$progress_link_cont.find('.edit-mod').html(that.i18n.read_mod);
		that.$progress_control_cont.show();
	}
	/**
	@description Private method that show HTML link if list contains row.
	@method toggleLink
	@param Array list that will be tested
	@param Object $link is a Jquery object that is the link will be showed or hided
	**/
	function toggleLink (list, $link) {
		MIOGA.logDebug('TaskEditor', 1, ' TaskEditor::toggleLink - loaded');
		if (list.length > 1) {
			$link.show();
		}
		else {
			$link.hide();
		}
	}
	// ===================================================================================
	// PUBLICS METHODS
	// ===================================================================================
	/**
	@description Public method that shows this window and hides other. It calls refresh method. 
	@method showWindow
	**/
	function showWindow() {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::showWindow - showWindow loaded');
			that.$cont.show();
			that.refresh();
	}
	/**
	@description Public method that hides this window.
	@method showWindow
	**/
	function hideWindow() {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::hideWindow - hideWindow loaded');
		that.$cont.hide();
	}
	/**
	@description Public method that makes HTML render of fineblanking item and insert this in DOM
	@method createFineblanking
	@param Object args contains pair key / value
		@option String label
		@option String comment
		@option Bool checked
	**/
	function createFineblanking (args) {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::createFineblanking - loaded');
		var $elm = $(
			'<div class="fineblanking-item">'
				+ '<div class="u_t_checkbox"><input type="checkbox"/></div>'
				+ '<div class="u_t_date"><input type="text" disabled/></div>'
				+ '<div class="u_t_label">' + args.label + '</div>'
				+ '<div class="u_t_workload"><span></span></span></span></div>'
				+ '<div class="u_t_comment">' + args.comment.replace(/\n/g, '<br>') + '</div>'
				+ '<div class="u_t_edit"><span class="h_edit">&#160;</span></div>'
				+ '<div class="u_t_remove"><span class="h_delete">&#160;</span></div>'
			+ '</div>'
		);

		if (args.checked === true) {
			$elm.addClass('completed').find('.u_t_checkbox input').attr("checked", "checked");
			$elm.find('.u_t_date input').val(args.date);
		}
		else {
		}
		if (args.workload !== null && args.workload !== "") {
			if (args.workload == 1) {
				$elm.find('.u_t_workload').html(args.workload + ' ' + that.i18n.hour);
			}
			else {
				$elm.find('.u_t_workload').html(args.workload + ' ' + that.i18n.hours);
			}
		}
		$elm.prependTo(that.$unit_task_list);
	}
	/**
	@description Public method that makes HTML render of task note and insert this in DOM
	@method createTaskNote
	@param Object taskNote
	**/
	function createTaskNote (taskNote) {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::createTaskNote - loaded');
		var created_str = that.haussmann.getStrDateFromObj(taskNote.created);
		var $taskNote = $(
				'<div class="task-note-item sbox">'
					+ '<h3 class="task-note-title">'
						+ '<div class="task-note-item-info"><span>' + taskNote.firstname + ' ' + taskNote.lastname + '   ' + created_str + '</span></div>'
					+ '</h3>'
					+ '<div class="task-note-text"><p>' + taskNote.text.replace(/\n/g, '<br>') + '</p></div>'
				+ '</div>').data({
			user_id : taskNote.owner_id,
			task_note_id : taskNote.task_note_id,
			text : taskNote.text
		});
		// check rigths of current user.
		if ( mioga_context.rights.Anim || (mioga_context.rights.WriteProjectIfOwner && that.task.user_is_proj_owner === true ) || (mioga_context.rights.WriteTask  && mioga_context.user.email === taskNote.email)) {
			$taskNote.find('h3').append('<div class="task-note-edit-link-cont"><span class="h_edit task-note-edit">&#160;</span></div>');
		}
		if ( mioga_context.rights.Anim || (mioga_context.rights.WriteProjectIfOwner && that.task.user_is_proj_owner === true ) || (mioga_context.rights.WriteTask && taskNote.owner_id === mioga_context.user.rowid) ) {
			$taskNote.find('h3').append('<div class="task-note-delete"><span class="h_delete">&#160;</span></div>');
		}
		that.$task_note_list.prepend($taskNote);
	}
	// =============================================================================================
	// REFRESH METHODS
	// =============================================================================================
	/**
	@description Public method that refreshs global informations of task according to user acces rights.
	For to do this, it remove all fields concerned and makes read mode fields or write mode fields and associates behaviors to this
	@method refreshGlobalInfo
	**/
	function refreshGlobalInfo () {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::refreshGlobalInfo - loaded');
		that.$global_info.find('fieldset [id]').remove();
		that.$global_info.find('.global-info-actions-cont').remove();
		if ( mioga_context.rights.Anim || (mioga_context.rights.WriteProjectIfOwner && that.task.user_is_proj_owner === true ) ) {
			that.$pstart_write = $('<input id="' + that.info_p_start_id + '" type="text" name="pstart"/>').datepicker({
				dateFormat: 'yy-mm-dd',
				defaultDate : 0,
				constrainInput : true,
				onSelect : function (date_str) {
					that.$global_info.find('#' + that.info_p_end_id).datepicker('option','minDate', date_str); 
				}
			});
			that.$pend_write = $('<input id="' + that.info_p_end_id + '" type="text" name="pend"/>').datepicker({
				dateFormat: 'yy-mm-dd',
				defaultDate : 0,
				constrainInput : true
			});

			that.$global_info.find('.form-item.label label').after(that.$label_write.val(that.task.label));
			that.$global_info.find('.form-item.description label').after(that.$description_write.val(that.task.description));
			that.$global_info.find('.form-item.workload label').after(that.$workload_write.val(that.task.workload));
			that.$global_info.find('.form-item.pstart label').after(that.$pstart_write.datepicker("setDate" , that.task.pstart));
			that.$global_info.find('.form-item.pend label').after(that.$pend_write.datepicker("setDate" , that.task.pend).datepicker("option", "minDate", that.task.pstart));
			that.$global_info.find('.form-item.tags label').after(that.$info_tags.tags ({
				available : tags_info.available_tags,
				free : tags_info.free_tags,
				tags : that.task.tags,
				warning :tags_info.tag_does_not_exist
			}));
			that.$info_actions_cont.appendTo(that.$global_info.find('fieldset:first'));
		}
		else {
			that.$global_info.find('.form-item.label label').after(that.$label_read.html(that.task.label));
			that.$global_info.find('.form-item.description label').after(that.$description_read.html(that.task.description));
			that.$global_info.find('.form-item.workload label').after(that.$workload_read.html(that.task.workload));
			that.$global_info.find('.form-item.pstart label').after(that.$pstart_read.html(that.haussmann.getStrDateFromObj(that.task.pstart)));
			that.$global_info.find('.form-item.pend label').after(that.$pend_read.html(that.haussmann.getStrDateFromObj(that.task.pend)));
			that.$global_info.find('.form-item.tags label').after(that.$info_tags);
			$.each(that.task.tags, function (i,tag) {
				that.$info_tags.append('<span class="tag-item"> ' + tag + '</span>');
			});
		}

		var workload_unit = that.i18n.hour;
		if (that.task.workload > 1) {
			workload_unit = that.i18n.hours;
		}
		that.$global_info.find('#' + that.workload_id).next('span').text(workload_unit);
	}
	/**
	@description Public method that refreshs task affectation container according to user acces rights.
	@method refreshAffectation
	**/
	function refreshAffectation () {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::refreshAffectation - loaded');
		if (that.task.delegated_user_id !== undefined && that.task.delegated_user_id !== null) {
			that.$affectation_person_cont.find('span:first').html('<a href="mailto:' + that.task.delegated_user_email + '">' + that.task.delegated_user_firstname + ' ' + that.task.delegated_user_lastname + '</a>');
			that.$affectation_dstart_cont.show();
			that.$affectation_dend_cont.show();
			if (that.task.dstart !== undefined && that.task.dstart !== null) {
				that.$affectation_dstart_cont.find('span:first').html( that.haussmann.getStrDateFromObj(that.task.dstart));
			}
			else {
				that.$affectation_dstart_cont.find('span:first').html(that.i18n.not_affected);
			}
			if (that.task.dend !== undefined && that.task.dend !== null) {
				that.$affectation_dend_cont.find('span:first').html(that.haussmann.getStrDateFromObj(that.task.dend));
			}
			else {
				that.$affectation_dend_cont.find('span:first').html(that.i18n.not_affected);
			}
		}
		else {
			if ( mioga_context.rights.Anim || (mioga_context.rights.WriteProjectIfOwner && that.task.user_is_proj_owner === true ) ) {
				that.$affectation_person_cont.find('span:first').html(that.i18n.choose_personn);
			}
			else {
				that.$affectation_person_cont.find('span:first').html(that.i18n.not_delegated);
			}
			that.$affectation_dstart_cont.hide();
			that.$affectation_dend_cont.hide();
			that.$task_affectation_cont.find('.task-affectation-dstart-cont').hide();
		}
		if ( mioga_context.rights.Anim || (mioga_context.rights.WriteProjectIfOwner && that.task.user_is_proj_owner === true )) {
			that.$delegation_cont.find('.choose-person-icon').show();
			that.$delegation_cont.find('#'+that.add_attendee_id).show();
		}
		else {
			// hide icon
			that.$delegation_cont.find('.choose-person-icon').hide();
			that.$delegation_cont.find('#'+that.add_attendee_id).hide();
		}
	}
	/**
	@description Public method that refreshs attendee list container according to user acces rights.
	@method refreshAttendees
	**/
	function refreshAttendees () {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::refreshAttendees - loaded');
		$('#'+that.attendee_list_id).children().remove();

		if ($.isArray(that.task.attendees)) {
			MIOGA.logDebug("TaskEditor", 1, ' attendees is array');
			$.each(that.task.attendees, function(idx, item) {
				MIOGA.logDebug("TaskEditor", 1, ' idx = '+idx+' item ',item);
				var el = $('<li><span class="remove-attendee">'+that.i18n.removeAttendee+'</span><span><a href="mailto:'+item.email+'">'+item.firstname+' '+item.lastname+'</a></<span></li>')
					.appendTo('#'+that.attendee_list_id)
					.data('user_id', item.user_id);
				var user_id = el.data('user_id');
				MIOGA.logDebug('TaskEditor', 1, ' el.user_id = ' + user_id);
			});
		}
		else {
			MIOGA.logDebug("TaskEditor", 1, ' attendees is NOT array');
		}
	}
	/**
	@description Public method that refreshs progress container and call the refresh method to refresh affectation
	@method refreshAffectation
	**/
	function refreshDelegation () {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::refreshDelegation - loaded');
		that.refreshAffectation();
		that.$progress_tbody.find('.progress-item').remove();
		//	insert progres item items
		for (var progressItemIndex = 0; progressItemIndex <  that.task.progress.length; progressItemIndex ++) {
			createProgressItem(that.task.progress[progressItemIndex].value, that.haussmann.getStrDateFromStr(that.task.progress[progressItemIndex].date));
		}
		hideProgressEdit ();
		hideProgressHistory ();
		toggleLink (that.task.progress, that.$progress_link_cont.find('.show-history'));
		if ( mioga_context.rights.Anim || (mioga_context.rights.WriteProjectIfOwner && that.task.user_is_proj_owner === true ) || (mioga_context.rights.WriteTask && that.task.user_is_task_delegated === true) ) {
			that.$progress_cont.show();
			that.$progress_link_cont.find('a.edit-mod').show();
		}
		else {
			that.$progress_link_cont.find('a.edit-mod').hide();
			if (that.task.progress.length === 0) {
				that.$progress_cont.hide();
			}
			else {
				that.$progress_cont.show();
			}
		}
	}
	/**
	@description Public method that refreshs status container
	@method refreshStatus
	**/
	function refreshStatus () {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::refreshStatus - loaded');
		showEditStatusIfNeeded ();
		if (that.advancedParams.task_status.length === 0) {
			that.$status_cont.hide();
		}
		else {
			that.$status_cont.show();
			// insert status value in label for next entry status.
			//	insert status items.
			that.$status_tbody.find('.status-item').remove();
			var count_status = that.task.status.length;
			if (that.task.current_status !== null) {
				for (var statusItemStatus = 0; statusItemStatus < count_status ; statusItemStatus ++) {
					createStatusItem (statusItemStatus, that.haussmann.getStrDateFromStr(that.task.status[statusItemStatus].date), that.task.status[statusItemStatus].comment);
				}
				if (count_status > 1) {
					that.$status_link_cont.find('.show-history').show();
				}
			}
			else {
				if (that.task.user_is_proj_owner === false && mioga_context.rights.Anim === false && (mioga_context.rights.WriteTask === false || that.task.user_is_task_delegated === false) ){
					that.$status_cont.hide();
				}
				else {
				}
			}
			
			hideStatusEdit();
			hideStatusHistory();
			toggleLink (that.task.status, that.$status_link_cont.find('.show-history'));
			if ( mioga_context.rights.Anim || (mioga_context.rights.WriteProjectIfOwner && that.task.user_is_proj_owner === true ) || (mioga_context.rights.WriteTask && that.task.user_is_task_delegated === true) ) {
				that.$status_fieldset.find('thead').show();
				that.$status_link_cont.find('.edit-mod').show();
				that.$status_fieldset.find('.h_delete').show();
			}
			else {
				that.$status_link_cont.find('.edit-mod').hide();
				that.$status_fieldset.find('th:last').hide();
				that.$status_fieldset.find('tr td.status-item-delete').hide();
			}
		}
	}
	/**
	@description Public method that refreshs grouping container
	@method refreshGrouping
	**/
	function refreshGrouping () {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::refreshGrouping - loaded');
		var $grouping_tbody = that.$grouping.find('.edit-task-grouping tbody');
		var grouping_exists = false;
		var affectation = that.i18n.unaffected;

		$grouping_tbody.children().remove();
		// TREE grouping
		if (that.task.position_tree[2].length > 0) {
			affectation = that.task.position_tree[2].join(' / ');
		}
		if (that.advancedParams.grouping_tasks.grouping_tree.length > 0 ) {
			grouping_exists = true;
			$grouping_tbody.append('<tr><td>' + that.i18n.groupingByTree + '</td><td>' + affectation + '</td></tr>');
		}
		// LIST grouping
		for (var groupingListIndex = 0; groupingListIndex < that.advancedParams.grouping_tasks.grouping_list.length; groupingListIndex ++) {
			grouping_exists = true;
			var indexContainer = -1;
			if (that.task.position_list.length > 0){
				if (that.task.position_list[groupingListIndex] !== undefined) {
					if (that.task.position_list[groupingListIndex] !== null) {
						indexContainer = that.task.position_list[groupingListIndex][1];
					}
				}
			}
			if (indexContainer !== -1) {
				$grouping_tbody.append(
					'<tr>'
						+'<td>' + that.advancedParams.grouping_tasks.grouping_list[groupingListIndex].label + '</td>'
						+ '<td>' + that.advancedParams.grouping_tasks.grouping_list[groupingListIndex].values[indexContainer] + '</td>'
					+ '</tr>'
				);
			}
			else {
				$grouping_tbody.append(
					'<tr>'
						+ '<td>' + that.advancedParams.grouping_tasks.grouping_list[groupingListIndex].label + '</td>'
						+ '<td><p>' + that.i18n.unaffected + '</p></td>'
					+ '</tr>'
				);
			}
		}
		// RESULT grouping
		if (that.advancedParams.result_status.length > 0) {
			grouping_exists = true;
			var indexContainer = -1;
			if (that.task.position_result.length > 0) {
				for (var i=0; i<that.project.resultList.result_list.length ; i++) {
					if (that.task.position_result[0] === that.project.resultList.result_list[i].result_id) {
						indexContainer = i;
					}
				}
			}
			if (indexContainer !== -1) {
				var content = that.project.resultList.result_list[indexContainer].label;
				$grouping_tbody.append('<tr><td>' + that.i18n.result + '</td><td>' + content  + '</td></tr>');
			}
			else {
				$grouping_tbody.append('<tr><td>' + that.i18n.result + '</td><td><p>' + that.i18n.unaffected  + '</p></td></tr>');
			}
		}
		if (grouping_exists === true) {
			that.$grouping.show();
		}
		else {
			that.$grouping.hide();
		}
		that.insertVocabulary(that.haussmann.vocabulary);
	}
	/**
	@description Public method that refreshs fineblanking container
	@method refreshFineblanking
	**/
	function refreshFineblanking () {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::refreshFineblanking - loaded');
		that.$unit_task_list.children().remove();
		var countUT = that.task.fineblanking.length;
		for (var i = 0; i < countUT ; i++) {
			that.createFineblanking (that.task.fineblanking[i]);
		}
		
		if ( mioga_context.rights.Anim || (mioga_context.rights.WriteProjectIfOwner && that.task.user_is_proj_owner === true ) || (mioga_context.rights.WriteTask && that.task.user_is_task_delegated === true ) ) {
			that.$unit_task.show();
			that.$unit_task.find('.unit-task-list').addClass("ui-sortable").sortable('enable');
			that.$unit_task.find('.edit-task-fineblanking').show();
		}
		else {
			if (countUT === 0) {
				that.$unit_task.hide();
			}
			else {
				that.$unit_task.show();
				that.$unit_task.find('.edit-task-fineblanking').hide();
				that.$unit_task.find('.link-create-UT, .h_edit, .h_delete').hide();
				that.$unit_task.find('input').attr('disabled', true);
				that.$unit_task.find('.ui-sortable').removeClass('ui-sortable').sortable('disable');
			}
			
		}
	}
	/**
	@description Public method that refreshs task note container according to user acces rights
	@method refreshTaskNote
	**/
	function refreshTaskNote () {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::refreshTaskNote - loaded');
		that.$task_note_list.children().remove();
		for (var i = 0; i < that.taskNoteList.task_note_list.length ; i++) {
			that.createTaskNote (that.taskNoteList.task_note_list[i]);
		}
		if ( mioga_context.rights.Anim || (mioga_context.rights.WriteProjectIfOwner && that.task.user_is_proj_owner === true ) || mioga_context.rights.WriteTask ) {
			that.$task_note.find('.task-note-fieldset').show();
			that.$task_note.find('.create-task-note-link').show();
		}
		else {
			if (that.taskNoteList.task_note_list.length === 0) {
				that.$task_note.find('.task-note-fieldset').hide();
			}
			else {
				that.$task_note.find('.task-note-fieldset').show();
				that.$task_note.find('.create-task-note-link').hide();
			}
		}
	}
	/**
	@description Public method that refreshs task editor
	@method refresh
	**/
	function refresh () {
		MIOGA.logDebug("TaskEditor", 1, 'TaskEditor::refresh - loaded');
		that.taskNoteList = that.task.getTaskNoteList();
		
		that.project_id = that.project.project_id;
		that.advancedParams = that.project.advancedParams;

		that.refreshGlobalInfo ();
		that.refreshStatus ();
		that.refreshGrouping ();
		that.refreshDelegation ();
		that.refreshAttendees ();
		that.refreshFineblanking ();
		that.refreshTaskNote ();
	}
	// ===================================================================================
	// INIT
	// ===================================================================================
	this.$cont.html('');
	// back behavior
	that.$cont.append('<div class="task-global-action-cont"></div>');
	$('<input type="button" class="button cancel task-action-back" value="' + that.i18n.backToProject + '"/>')
		.click(function (ev) {
			location.hash = that.generateHash("ProjectEditor", that.project_id);
		}).appendTo(that.$cont.find('.task-global-action-cont'));
	// ------------------------------------------------------
	// INIT TASK INFOMATION
	// ------------------------------------------------------
	this.$global_info = $(
		'<div class="task-informations-cont">'
			+ '<form id="' + that.information_form_id + '" class="form">'
				+ '<fieldset class="edit-task-info-cont">'
					+ '<legend>' + that.i18n.informations + '</legend>'
					+ '<div class="form-item label"><label class="task-editor-label" for="' + that.label_id + '">' + that.i18n.label + '</label></div>'
					+ '<div class="form-item description"><label class="task-editor-label" for="' + that.description_id + '">' + that.i18n.description + '</label></div>'
					+ '<div class="form-item workload"><label class="task-editor-label" for="' + that.workload_id + '">' + that.i18n.workload + '</label><span class="unit"></span></div>'
					+ '<div class="form-item pstart"><label class="task-editor-label" for="' + that.info_p_start_id + '">' + that.i18n.p_start + '</label></div>'
					+ '<div class="form-item pend"><label class="task-editor-label" for="' + that.info_p_end_id + '">' + that.i18n.p_end + '</label></div>'
					+ '<div class="form-item tags"><label class="task-editor-label" for="' + that.tags_id + '">' + that.i18n.tags + '</label></div>'
				+ '</fieldset>'
			+ '</form>'
		+ '</div>'
	).appendTo(that.$cont);
	// ------------------------------------------------------
	// write fields
	this.$label_write = $('<input id="' + that.label_id + '" type="text" name="label"/>');
	this.$description_write = $('<textarea id="' + that.description_id + '" type="text" name="description"></textarea>');
	this.$workload_write = $('<input id="' + that.workload_id + '" type="text" name="workload" value=""/>');
	this.$pstart_write = $('<input id="' + that.info_p_start_id + '" type="text" name="pstart"/>').datepicker({
		dateFormat: 'yy-mm-dd',
		defaultDate : 0,
		constrainInput : true,
		minDate : new Date(),
		onSelect : function (date_str) {
			that.$global_info.find('#' + that.info_p_end_id).datepicker('option','minDate', date_str); 
		}
	});
	this.$pend_write = $('<input id="' + that.info_p_end_id + '" type="text" name="pend"/>').datepicker({
		dateFormat: 'yy-mm-dd',
		defaultDate : 0,
		constrainInput : true
	});
	// ------------------------------------------------------
	// read fields
	this.$label_read = $('<span id="' + that.label_id + '" type="text" name="label"></span>');
	this.$description_read = $('<p id="' + that.description_id + '" name="description"></p>');
	this.$workload_read = $('<span id="' + that.workload_id + '" name="workload"></span>');
	this.$pstart_read = $('<span id="' + that.info_p_start_id + '" name="pstart"></span>');
	this.$pend_read = $('<span id="' + that.info_p_end_id + '" name="pend"></span>');
	this.$info_tags = $('<div id="' + that.tags_id + '"></div>');
	// ------------------------------------------------------
	// apply global info
	this.$info_actions_cont = $('<div class="global-info-actions-cont"></div>');
	this.$apply_global_info = $('<input type="button" class="button apply-global-info" value="' + that.i18n.apply + '"/>').appendTo(that.$info_actions_cont);
	this.$global_info.delegate('.apply-global-info', 'click',function (ev) {
			that.$cont.find('.error').remove();
			var $error_cont = $('<div class="error error-global-info"></div>').prependTo(that.$global_info);
			var global_info_attr = serializeGlobalInfo();
			var onError = function (errorList) {
				$.each(errorList, function (i,text) {
					$error_cont.append('<p class="error">' + that.i18n[text] + '</p>');
				});
			};
			var onSuccess = function () {
				successMessage (that.$info_actions_cont, that.i18n.successSaveInformation);
				that.refreshGlobalInfo ();
			};
			that.task.setGlobalInfo(global_info_attr,onSuccess , onError);
		});
	// ------------------------------------------------------
	// INIT TASK DELEGATION
	// ------------------------------------------------------
	this.$delegation_cont = $(
		'<div class="task-delegation-cont">'
			+ '<form id="' + that.delegation_form_id + '" class="form">'
				+ '<fieldset class="edit-task-delegation-cont">'
					+ '<legend>' + that.i18n.delegation + '</legend>'
					+ '<div class="task-responsable">'
						+ '<div class="form-item task-affectation-cont">'
							+ '<label class="task-editor-label" for="' + that.affectation_person_id + '">' + that.i18n.affectation_person + '</label>'
							+ '<div class="affectation-person-cont" id="' + that.affectation_person_id + '">'
								+ '<span></span>'
								+ '<span class="choose-person-icon"></span>'
							+ '</div>'
						+ '</div>'
						+ '<div class="form-item task-affectation-dstart-cont">'
							+ '<label class="task-editor-label" for="' + that.affectation_dstart_id + '">' + that.i18n.w_start + '</label>'
							+ '<span id="' + that.affectation_dstart_id + '"></span>'
						+ '</div>'
						+ '<div class="form-item task-affectation-dend-cont">'
							+ '<label class="task-editor-label" for="' + that.affectation_dend_id + '">' + that.i18n.w_end + '</label>'
							+ '<span id="' + that.affectation_dend_id + '"></span>'
						+ '</div>'
					+ '</div>'
					+ '<div class="task-attendees">'
						+ '<p><a id="'+that.add_attendee_id+'" class="add-attendee">'+that.i18n.add_attendee_label+'</a></p>'
						+ '<ul id="'+that.attendee_list_id+'" class="task-attendee-list">'
						+ '</ul>'
					+ '</div>'
					+ '<fieldset class="progress-fieldset">'
						+ '<legend>' + that.i18n.progress + '</legend>'
						+ '<table class="task-progress-table sbox list">'
							+ '<tbody>'
								+ '<tr class="task-progress-control-cont">'
									+ '<td><span><span class="h_create">&nbsp;</span><input type="text" class="progress-input-value"/> %</span></td>'
									+ '<td></td>'
								+ '</tr>'
							+ '</tbody>'
						+ '</table>'
						+ '<div class="progress-edit-link-cont">'
							+ '<a class="edit-mod" href="#">' + that.i18n.edit_mod + '</a>'
							+ '<a class="show-history" href="#">' + that.i18n.show_history + '</a>'
						+ '</div>'
					+ '</fieldset>'
				+ '</fieldset>'
			+ '</form>'
		+ '</div>'
	).appendTo(that.$cont);

	this.$task_affectation_cont = this.$delegation_cont.find('.task-affectation-cont');
	this.$affectation_person_cont = this.$task_affectation_cont.find('.affectation-person-cont');
	this.$affectation_dstart_cont = this.$delegation_cont.find('.task-affectation-dstart-cont');
	this.$affectation_dend_cont = this.$delegation_cont.find('.task-affectation-dend-cont');

	this.$progress_cont = this.$delegation_cont.find('.progress-fieldset');
	this.$progress_link_cont = this.$progress_cont.find('.progress-edit-link-cont');
	this.$progress_control_cont = this.$progress_cont.find('.task-progress-control-cont');
	this.$progress_tbody = this.$delegation_cont.find('.task-progress-table tbody');
	this.$progress_date = this.$progress_tbody.find('.progress-input-date');
	this.$progress_value = this.$progress_tbody.find('.progress-input-value');

	// edit mod behavior
	this.$progress_link_cont.find('.edit-mod').toggle(
		function () {
			showProgressEdit();
			that.$progress_value.focus();
		},
		function () {
			hideProgressEdit();
		}
	);
	// toggle history visibility
	this.$progress_link_cont.find('.show-history').toggle(
		function () {
			$(this).html(that.i18n.hide_history);
			that.$progress_tbody.find('.progress-item').show();
		},
		function () {
			hideProgressHistory();
		}
	);

	// Choose responsable
	var $dial = $('<div class="dial"><div class="us"></div></div>');
	
	var optionsUS = {
		app_name : "Haussmann",
		data : [],
		single : true,
		i18n : {
			select_all		: that.i18n.select_all,
			deselect_all 	: that.i18n.select_none,
			add_btn			: that.i18n.choose,
			all				: that.i18n.All,
			columns : [
					{
						field: 'firstname',
						label: that.i18n.firstname
					},
					{
						field: 'lastname',
						label: that.i18n.lastname,
						Default: true
					},
					{
						field: 'email',
						label: that.i18n.email
					}
			]
		},
		select_add : function (arg1) {
			// args1 like : [16]
			$.each(that.user_list, function (i,e) {
				if (e.rowid === arg1[0]) {
					var onSuccess = function () {
						successMessage (that.$task_affectation_cont, that.i18n.successSaveAffectation);
						that.refreshAffectation();
					};
					that.task.setAffectation (e, onSuccess);
				}
			});
			$dial.dialog('close');
		}
	};
	$dial.find('.us').userSelect(optionsUS);
	
	$dial.dialog({
		resizable : false,
		autoOpen : false,
		modal : true,
		width : "850",
		height : "400",
		title : that.i18n.choose_personn
	});
	// change affectation behavior
	that.$affectation_person_cont.find('.choose-person-icon').click(function () {
		var new_list = [];
		var attendees_rowids = [];
		var pos;
		that.user_list = that.haussmann.getUsers();
		MIOGA.logDebug('TaskEditor', 1, ' user_list ', that.user_list);
		attendees_rowids = that.task.getAttendeesRowids();
		$.each(that.user_list, function(idx, item) {
			
			pos = $.inArray( item.rowid, attendees_rowids);
			// pos = attendees_rowids.indexOf(item.rowid);
			if ( (pos < 0) && (item.rowid !== that.task.delegated_user_id) ) {
				new_list.push(item);
			}
		});
		$dial.find('.us').userSelect('replace_list', new_list);
		$dial.dialog('open');
	});

	// Add attendees Dialog
	var $dial_attendees = $('<div class="dial"><div class="us"></div></div>');
	
	var opt_attendees = {
		app_name : "Haussmann",
		data : [],
		single : false,
		i18n : {
			select_all		: that.i18n.select_all,
			deselect_all 	: that.i18n.select_none,
			add_btn			: that.i18n.add,
			all				: that.i18n.All,
			columns : [
					{
						field: 'firstname',
						label: that.i18n.firstname
					},
					{
						field: 'lastname',
						label: that.i18n.lastname,
						Default: true
					},
					{
						field: 'email',
						label: that.i18n.email
					}
			]
		},
		select_add : function (rowids) {
			MIOGA.logDebug('TaskEditor', 1, ' Add attendee dialog select add...', rowids);
			MIOGA.logDebug('TaskEditor', 1, ' task ', that.task);
			that.task.addAttendees(rowids, function() {
				MIOGA.logDebug('TaskEditor', 1, ' Add attendee dialog select add cb return...');
				that.refreshAttendees();
			});
			$dial_attendees.dialog('close');
		}
	};
	$dial_attendees.find('.us').userSelect(opt_attendees);
	
	$dial_attendees.dialog({
		resizable : false,
		autoOpen : false,
		modal : true,
		width : "850",
		height : "400",
		title : that.i18n.add_attendees
	});
	// Add attendees actions
	$('#'+that.add_attendee_id).click(function(ev) {
		MIOGA.logDebug('TaskEditor', 1, ' Add attendee ...');
		var new_list = [];
		var attendees_rowids = [];
		var pos;
		that.user_list = that.haussmann.getUsers();
		MIOGA.logDebug('TaskEditor', 1, ' user_list ', that.user_list);
		attendees_rowids = that.task.getAttendeesRowids();
		$.each(that.user_list, function(idx, item) {
			pos = attendees_rowids.indexOf(item.rowid);
			if ( (pos < 0) && (item.rowid !== that.task.delegated_user_id) ) {
				new_list.push(item);
			}
		});
		MIOGA.logDebug('TaskEditor', 1, ' new_list ', that.new_list);
		$dial_attendees.find('.us').userSelect('replace_list', new_list);
		$dial_attendees.dialog('open');
	});
	// behavior to suppress attendee
	$('#'+that.attendee_list_id).on('click', '.remove-attendee', function(ev) {
		MIOGA.logDebug('TaskEditor', 1, ' remove-attendee click ');
		var user_id = $(ev.currentTarget).parent().data('user_id');
		MIOGA.logDebug('TaskEditor', 1, ' user_id = ' + user_id);
		that.task.removeAttendee(user_id, function() {
			MIOGA.logDebug('TaskEditor', 1, ' suppressAttendee return ');
			that.refreshAttendees();
		});
	});

	// behavior to create new progress item
	that.$progress_value.on ('keypress', function (evt) {
		if ((evt.keyCode == 13) && (evt.target.type !== "input")) {
			addProgressItem ();
			return (false);
		}
	});
	this.$progress_tbody.find('.h_create').click(function () {
		addProgressItem ();
	});
	// ------------------------------------------------------
	// INIT TASK STATUS
	// ------------------------------------------------------
	this.$status_cont = $(
		'<div class="task-status-cont" style="display:none;">'
			+ '<form id="' + that.status_form_id + '" class="form">'
				+ '<fieldset class="status-fieldset">'
					+ '<legend>' + that.i18n.status + '</legend>'
					+ '<table class="status-table sbox list">'
						+ '<thead>'
							+ '<tr><th class="header-status-label">' + that.i18n.status + '</th><th class="header-status-date">' + that.i18n.date + '</th><th class="header-status-comment">' + that.i18n.comment + '</th><th class="header-status-delete"></th></tr>'
						+ '</thead>'
						+ '<tbody>'
							+ '<tr class="status-control-cont" style="display:none;">'
								+ '<td><span><span class="h_create">&nbsp;</span><span value="" class="status-input-label"></span></span></td>'
								+ '<td><input type="text" class="status-input-date"/></td><td><textarea value="" class="status-input-comment"></textarea></td><td></td>'
							+ '</tr>'
						+ '</tbody>'
					+ '</table>'
					+ '<div class="status-edit-link-cont">'
						+ '<a href="#" class="edit-mod">' + that.i18n.edit_mod + '</a>'
						+ '<a href="#" class="show-history">' + that.i18n.show_history + '</a>'
					+ '</div>'
				+ '</fieldset>'
			+ '</form>'
		+ '</div>'
	).appendTo(that.$cont);

	this.$status_fieldset = this.$status_cont.find('.status-fieldset');
	this.$status_link_cont = this.$status_fieldset.find('.status-edit-link-cont');
	this.$status_tbody = this.$status_fieldset.find('tbody');
	this.$status_control_cont = this.$status_tbody.find('.status-control-cont');
	this.$status_status = this.$status_tbody.find('.status-input-label');
	this.$status_date = this.$status_tbody.find('.status-input-date');
	this.$status_comment = this.$status_tbody.find('.status-input-comment');

	// edit mod
	this.$status_link_cont.find('.edit-mod').toggle(
		function () {
			showEditStatusIfNeeded ();
			
		},
		function () {
			hideStatusEdit();
		}
	);
	// toggle history visibility
	this.$status_link_cont.find('.show-history').toggle(
		function () {
			$(this).html(that.i18n.hide_history);
			that.$status_tbody.find('.status-item').show();
		},
		function () {
			hideStatusHistory();
		}
	);

	// behavior datepicker dates for status
	this.$status_date
		.datepicker({
			dateFormat : 'yy-mm-dd',
			defaultDate : 0
		})
		.click(function () {
			that.$status_cont.find('.error').remove();
		}
	);
	// behavior create status item
	this.$status_tbody.find('.h_create').click(function () {
		var params = {
			status : parseInt(that.$status_status.attr('value'), 10),
			date : that.$status_date.val(),
			comment : $.trim(that.$status_comment.val().replace(/<br>/g, '\n').replace(/<BR>/g, '\n'))
		};
		that.$status_fieldset.find('.error').remove();
		var onCheckSuccess = function (params) {
			that.task.addStatusItem (params, function () {
				that.task.setStatus(function () {
					createStatusItem (params.status, that.haussmann.getStrDateFromStr(params.date), params.comment);
					toggleLink (that.task.status, that.$status_link_cont.find('.show-history'));
					successMessage (that.$status_cont, that.i18n.successCreateStatusItem);
				});
			});
		};
		checkStatusItem (params, onCheckSuccess, that.$status_fieldset);
	});
	// delete behavior
	this.$status_tbody.delegate('span.h_delete', 'click', function () {
		var $tr = $(this).parent().parent('tr');
		var status_val = $tr.find('.status-item-status').attr('value');
		var status_label = that.advancedParams.task_status[status_val];
		var onSuccess = function () {
			that.$status_tbody.find('.status-input-label')
				.attr('value',status_val)
				.text(status_label);
			that.$status_comment.val('');
			if ( $tr.prev('tr').length > 0 ) {
				that.$status_date.datepicker("option", "minDate", $tr.prev('tr').find('.status-date').text()).val('');
				$tr.prev('tr').find('.status-item-delete').html('<span class="h_delete"></span>');
			}
			else {
				that.$status_date.datepicker("option", "minDate", null).val('');
				that.$status_fieldset.find('thead').hide();
				
				
			}
			$tr.remove();
			if (that.$status_link_cont.find('.edit-mod').html() === that.i18n.read_mod) {
				showEditStatusIfNeeded ();
			}
			if (that.$status_link_cont.find('.show-history').html() === that.i18n.show_history) {
				hideStatusHistory ();
			}
			toggleLink (that.task.status, that.$status_link_cont.find('.show-history'));
		};
		that.task.deleteLastStatusItem(onSuccess);
	});
	// ------------------------------------------------------
	// INIT GROUPING TASK
	// ------------------------------------------------------
	this.$grouping = $(
		'<div class="task-grouping-cont">'
			+ '<form id="' + that.grouping_form_id + '" class="form">'
				+ '<fieldset class="edit-task-grouping"><legend>' + that.i18n.groupingAffectation + '</legend>'
					+ '<table class="list">'
						+ '<tbody></tbody>'
					+ '</table>'
				+ '</fieldset>'
			+ '</form>'
		+ '</div>'
	).appendTo(that.$cont);

	// ------------------------------------------------------
	// INIT UNIT TASK
	// ------------------------------------------------------
	this.$unit_task = $(
		'<div class="unit-task-cont">'
			+ '<form id="' + that.unit_task_form_id + '" class="form">'
				+ '<fieldset class="edit-task-fineblanking">'
					+ '<legend>' + that.i18n.fineblanking + '</legend>'
					+ '<div class="unit-task-link-cont"></div>'
					+ '<div class="unit-task-list"></div>'
				+ '</fieldset>'
			+ '</form>'
		+ '</div>'
	).appendTo(that.$cont);

	this.$unit_task_list = this.$unit_task.find('.unit-task-list');

	// behavior to delete unit task
	this.$unit_task.delegate('.u_t_remove span', 'click', function (ev) {
		var $UT = $(this).parent().parent();
		var params = {
			UT_index : $UT.index()
		};
		var onSuccess = function () {
			$UT.hide('slow', function(){ $UT.remove(); });
		};
		that.task.deleteFineblanking(params, onSuccess);
	});

	// behavior to dynamic checking unit task
	this.$unit_task.delegate('.u_t_checkbox input', 'click', function (ev) {
		var $UT = $(this).parent().parent();
		var UT_index = $UT.index();
		if ($(this).is(':checked') === false) {
			var params = {
				UT_index : UT_index,
				checked : false,
				date : ""
			};
			that.task.setUnitTask(params, function () {
				$UT.removeClass('completed').find('.u_t_date input').val('');
			});
		}
		else {
			$UT.addClass('completed').find('.u_t_date input').datepicker({
				dateFormat : 'yy-mm-dd',
				showButtonPanel: true,
				onSelect : function (date_str) {
					var params = {
						UT_index : UT_index,
						checked : true,
						date : date_str
					};
					that.task.setUnitTask(params, function () {
						$UT.find('.u_t_date input').val(date_str);
					});
				},
				onClose : function (date_str) {
					if (date_str.length === 0) {
						$UT.find('.u_t_checkbox input').attr('checked', false);
					}
				}
			}).datepicker('show');
			// close datepicker on escape
			$UT.find('.u_t_checkbox input').keyup(function(e) {
				if (e.keyCode == 27) {
					$UT.find('.u_t_date input').datepicker('hide');
					$UT.find('.u_t_checkbox input').unbind('keyup');
				}
			});
		}
	});
	// link to create unit task
	this.$create_unit_task = $('<span class="link-create-UT">' + that.i18n.create_fine_blanking + '</span>')
		.appendTo(that.$unit_task.find('.unit-task-link-cont'))
		.click(function (ev) {
			clearFineblankingForm();
			that.$dial_ut.data('UT_mod', 'create');
			that.$dial_ut.dialog("open");
		});
	// sortable behavior to unit task
	this.$unit_task_list.sortable({
		items		: '.fineblanking-item',
		tolerance 	: 'pointer',
		placeholder : "h-sortable-placeholder",
		update : function (ev, ui) {
			var params = {
				i_start : $(this).data("index_item_start"),
				i_end : ui.item.index()
			};
			that.task.moveFineblanking(params);
		},
		start		: function (event, ui) {
			$(this).data("index_item_start", ui.item.index());
			ui.placeholder.height(ui.item.height());
			ui.placeholder.width(ui.item.width());
			ui.placeholder.show();
		}
	}).disableSelection();

	// dialog to edit unit task
	this.$dial_ut = $('<div class="dial-ut"></div>').dialog({
		resizable : false,
		autoOpen : false,
		modal : true,
		width : "550",
		height : "280",
		title : that.i18n.edit_fine_blanking,
		open : function () {
			that.$dial_ut.append(that.$form_edit_UT);
			that.$ut_label.focus();
		}
	});
	// form to edit unit task
	this.$form_edit_UT = $(
		'<form id="' + that.edit_unit_task_form_id + '" class="form form-edit-UT">'
			+ '<div class="form-item edit-UT-label-cont">'
				+ '<label class="task-editor-label" for="' + that.unit_task_label_id + '">' + that.i18n.label + '</label>'
				+ '<input type="text" id="' + that.unit_task_label_id + '"></input>'
			+ '</div>'
			+ '<div class="form-item edit-UT-workload-cont">'
				+ '<label class="task-editor-label" for="' + that.unit_task_workload_id + '">' + that.i18n.workload + '</label>'
				+ '<input type="text" id="' + that.unit_task_workload_id + '"></input>'
			+ '</div>'
			+ '<div class="form-item edit-UT-checked-cont">'
				+ '<label class="task-editor-label" for="' + that.unit_task_checked_id + '">' + that.i18n.validated + '</label>'
				+ '<input type="checkbox" id="' + that.unit_task_checked_id + '"></input>'
			+ '</div>'
			+ '<div class="form-item edit-UT-date-cont" style=display:none;">'
				+ '<label class="task-editor-label" for="' + that.unit_task_date_id + '">' + that.i18n.date + '</label><input type="text" id="' + that.unit_task_date_id + '"></input>'
			+ '</div>'
			+ '<div class="form-item edit-UT-comment-cont">'
				+ '<label class="task-editor-label" for="' + that.unit_task_comment_id + '">' + that.i18n.comment + '</label>'
				+ '<textarea id="' + that.unit_task_comment_id + '"></textarea>'
			+ '</div>'
			+ '<div class="ut-actions-cont">'
				+ '<ul class="button_list">'
					+ '<li><input type="button" class="button" value="' + that.i18n.apply + '"/></li>'
					+ '<li><input type="button" class="button cancel" value="' + that.i18n.cancel + '"/></li>'
				+ '</ul>'
			+ '</div>'
		+ '</form>'
	).appendTo(that.$dial_ut);

	this.$ut_label = this.$form_edit_UT.find('#' + that.unit_task_label_id);
	this.$ut_workload = this.$form_edit_UT.find('#' + that.unit_task_workload_id);
	this.$ut_checked = this.$form_edit_UT.find('#' + that.unit_task_checked_id);
	this.$ut_date = this.$form_edit_UT.find('#' + that.unit_task_date_id);
	this.$ut_comment = this.$form_edit_UT.find('#' + that.unit_task_comment_id);
	this.$ut_actions_cont = this.$form_edit_UT.find('.ut-actions-cont');
	this.$ut_apply = this.$ut_actions_cont.find('input:first');
	this.$ut_cancel = this.$ut_actions_cont.find('input:last');

	// datepicker for unit task date
	this.$ut_date.datepicker({
		dateFormat : 'yy-mm-dd'
	});
	// behavior to change check unit task
	this.$ut_checked.change (function () {
		if ($(this).is(':checked')) {
			that.$ut_date.parent().show();
		}
		else {
			that.$ut_date.parent().hide();
		}
	});
	// cancel behavior
	this.$ut_cancel.click(function () {
		that.$dial_ut.dialog('close');
		clearFineblankingForm();
	});

	// apply edit dialog
	this.$ut_apply.click(function () {
		var UT_attr = {
			label : $.trim(that.$ut_label.val()),
			workload : $.trim(that.$ut_workload.val()),
			checked : that.$ut_checked.is(':checked'),
			date : $.trim(that.$ut_date.val()),
			comment : $.trim(that.$ut_comment.val().replace(/<br>/g, '\n').replace(/<BR>/g, '\n'))
		};
		if (that.$dial_ut.data('UT_mod') === "create") {
			var onCheckSuccess = function (UT_attr) {
				that.task.addFineblanking(UT_attr, function () {
					that.task.setFineblanking(function () {
						that.createFineblanking (UT_attr);
						that.$dial_ut.dialog('close');
						clearFineblankingForm();
					});
				});
			};
			checkFineBlanking (UT_attr, onCheckSuccess, that.$ut_actions_cont);
		}
		else if (that.$dial_ut.data('UT_mod') === "edit") {
			var UT_index = that.$dial_ut.data('UT_index');
			UT_attr.UT_index = UT_index;
			var onCheckSuccess = function (UT_attr) {
				that.task.setUnitTask(UT_attr, function () {
					that.$dial_ut.dialog('close');
					clearFineblankingForm();
					that.refreshFineblanking ();
				});
			};
			checkFineBlanking (UT_attr, onCheckSuccess, that.$ut_actions_cont);
		}
	});

	// edit unit task
	this.$unit_task_list.delegate('.u_t_edit span', 'click', function (ev) {
		clearFineblankingForm();
		that.$dial_ut.data('UT_mod', "edit");
		var UT_index = $(this).parent().parent().index();
		that.$dial_ut.data('UT_index', UT_index);

		var ut = that.task.fineblanking[that.task.fineblanking.length-1-UT_index];
		that.$ut_label.val(ut.label);
		that.$ut_workload.val(ut.workload);
		if (ut.checked === false) {
			that.$ut_checked.removeAttr('checked');
			that.$ut_date.datepicker('setDate', null);
		}
		else {
			that.$ut_checked.attr('checked', true);
			that.$ut_date.datepicker('setDate', ut.date).parent().show();
		}
		that.$ut_comment.val(ut.comment);
		that.$dial_ut.dialog("open");
	});
	// ------------------------------------------------------
	// INIT TASK NOTE
	// ------------------------------------------------------
	this.$task_note = $(
		'<div class="task-note-cont">'
			+ '<form id="' + that.task_note_form_id + '" class="form">'
				+ '<fieldset class="task-note-fieldset">'
					+ '<legend>' + that.i18n.notes + '</legend>'
					+ '<span class="create-task-note-link">' + that.i18n.createNote + '</span>'
					+ '<div class="task-note-list"></div>'
				+ '</fieldset>'
			+ '</form>'
		+ '</div>'
	).appendTo(that.$cont);
	this.$task_note_list = this.$task_note.find('.task-note-list');

	// dialog to edit unit task
	this.$dial_note = $('<div class="dial-note"></div>').dialog({
		resizable : false,
		autoOpen : false,
		modal : true,
		width : "550",
		height : "280",
		title : that.i18n.edit_task_note,
		open : function () {
			that.$dial_note.append(that.$form_edit_note);
			that.$edit_note_text.focus();
		}
	});
	// form to edit unit task
	this.$form_edit_note = $(
		'<div class="edit-note-form-cont">'
			+'<textarea id="' + that.edit_note_text_id + '"></textarea>'
			+ '<div class="edit-note-actions-cont">'
				+ '<ul class="button_list">'
					+ '<li><input type="button" class="button" value="' + that.i18n.apply + '"/></li>'
					+ '<li><input type="button" class="button cancel" value="' + that.i18n.cancel + '"/></li>'
				+ '</ul>'
			+ '</div>'
		+ '</div>'
	).appendTo(that.$dial_note);
	this.$edit_note_actions_cont = this.$form_edit_note.find('.edit-note-actions-cont');
	this.$edit_note_apply = this.$form_edit_note.find('li:first').find('input:first');
	this.$edit_note_cancel = this.$form_edit_note.find('li:last').find('input:first');
	this.$edit_note_text =  this.$form_edit_note.find('#' + that.edit_note_text_id);
	// cancel behavior
	this.$edit_note_cancel.click(function () {
		that.$dial_note.dialog('close');
		clearEditNoteForm();
	});
	// apply edit dialog
	this.$edit_note_apply.click(function () {
		var note_attr = {
			text : $.trim(that.$edit_note_text.val().replace(/<br>/g, '\n').replace(/<BR>/g, '\n'))
		};
		if (that.$dial_note.data('edit_note_mod') === "create") {
			var onCheckSuccess = function (note_attr) {
				that.taskNoteList.createTaskNote(note_attr, function (new_task_note) {
					that.createTaskNote (new_task_note);
					that.$dial_note.dialog('close');
					clearEditNoteForm();
				});
			};
			checkTaskNote (note_attr, onCheckSuccess, that.$edit_note_actions_cont);
		}
		else if (that.$dial_note.data('edit_note_mod') === "edit") {
			var task_note_id = that.$dial_note.data('task_note_id');
			note_attr.task_note_id = task_note_id;
			var onCheckSuccess = function (note_attr) {
				that.taskNoteList.task_note_list_by_rowid[task_note_id].setTaskNote(note_attr, function () {
					that.$dial_note.dialog('close');
					clearEditNoteForm();
					that.refreshTaskNote ();
				});
			};
			checkTaskNote (note_attr, onCheckSuccess, that.$edit_note_actions_cont);
		}
	});

	// behavior to create new note.
	this.$task_note.find('.create-task-note-link').click(function () {
		clearEditNoteForm();
		that.$dial_note.data('edit_note_mod', 'create');
		that.$dial_note.dialog("open");
	});
	// behavior to edit note
	this.$task_note_list.delegate('.task-note-edit', 'click', function () {
		var $task = $(this).parent().parent().parent();
		var task_note_id = $task.data('task_note_id');
		that.$dial_note.data('edit_note_mod', 'edit');
		that.$dial_note.data('task_note_id', task_note_id);
		that.$edit_note_text.val(that.taskNoteList.task_note_list_by_rowid[task_note_id].text);
		that.$dial_note.dialog("open");
	});
	// behavior to delete note
	this.$task_note_list.delegate('.task-note-delete span', 'click', function () {
		var $task = $(this).parent().parent().parent();
		var params = {
			task_note_id : $task.data('task_note_id')
		};
		var onSuccess = function () {
			$task.remove();
		};
		that.taskNoteList.deleteTaskNote(params, onSuccess);
	});
}// End of taskEditor object.
