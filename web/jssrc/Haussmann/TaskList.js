// =============================================================================
/**
@description Create a TaskList object.

    var options = {
        haussmann : { .... }
    };

@class TaskList
@constructor
**/
// =============================================================================
function TaskList (options) {
	"use strict";
	MIOGA.debug.TaskList = 0;
	MIOGA.logDebug('TaskList', 1, ' TaskList loaded');
	
	var that = this;
	this.task_list = [];
	this.task_list_by_rowid = {};
	this.haussmann = options.haussmann;
	this.project_id = options.project_id;
	this.project_owner_id = options.project_owner_id;
	this.project_owner_firstname = options.project_owner_firstname;
	this.project_owner_lastname = options.project_owner_lastname;
	this.project_owner_email = options.project_owner_email;
	this.user_is_proj_owner = options.user_is_proj_owner;
	this.advancedParams = options.advancedParams;

	// publics method declaration
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;
	this.hasTaskStatus = hasTaskStatus;
	this.hasTaskInResult = hasTaskInResult;
	this.hasTaskInGroupingList = hasTaskInGroupingList;
	this.hasTaskInGroupingTree = hasTaskInGroupingTree;
	this.updateTaskAttr = updateTaskAttr;
	this.getProjectTaskTreeIndex = options.haussmann.projectList.project_list_by_rowid[options.haussmann.projectList.current_project_id].getProjectTaskTreeIndex;
	this.getTaskList = getTaskList;
	this.getTaskListByPositionTree = getTaskListByPositionTree;
	this.getTaskListByPositionList = getTaskListByPositionList;
	this.getTaskListByPositionResult = getTaskListByPositionResult;
	this.createTask = createTask;
	this.updtaeTaskPositionToChangeStructure = updtaeTaskPositionToChangeStructure;
	this.updatePositionToDeleteResult = updatePositionToDeleteResult;
	this.setTaskList = setTaskList;
	this.deleteTask = deleteTask;
	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================================================
	// PRIVATES METHODS
	// ===================================================================================================================
	/**
	@description Private method that adds task in list and index by rowid
	@method addTask
	@param Object new_task is Task object that will be added
	@param Function cb is callback functin that called on add success
	**/
	function addTask(new_task, cb) {
		MIOGA.logDebug('TaskList', 1, 'TaskList::addTask - loaded');
		that.task_list.push(new_task);
		that.task_list_by_rowid[new_task.task_id] = new_task;
		if (cb !== undefined) {
			cb(new_task);
		}
	}
	/**
	@description Private method that checks if arguments length is empty. If this is it, that returns true, else returns false.
	@method isEmpty
	@param Object elem can be all type. This is the element that will be tested.
	@param String error_str that is a string value that represents the error message if tested element is empty.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isEmpty (elem, error_str, global_result) {
		MIOGA.logDebug('TaskList', 1, 'TaskList::isEmpty - loaded');
		var result = false;

		if (elem.length === 0) {
			if (global_result !== undefined) {
				global_result.status = "error";
				global_result.error.push(error_str);
			}
			result = true;
		}
		return result;
	}
	/**
	@description Private method that checks if the element exists in a list. If this is it, that returns true, else returns false.
	@method isUnique
	@param Object elem can be all type. This is the element that will be searched in list.
	@param Array list. This is the list that will be matched.
	@param String error_str that is a string value that represents the error message if tested element is not unique.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isUnique (elem, list, error_str, global_result) {
		MIOGA.logDebug('TaskList', 1, 'TaskList::isUnique - loaded');
		var result = true;
		for (var i = 0; i < list.length; i++) {
			if (list[i] === elem) {
				if (global_result !== undefined) {
					global_result.status = "error";
					global_result.error.push(error_str);
				}
				MIOGA.logDebug('TaskList', 1, 'TaskList::isUnique - return FALSE');
				result = false;
				break;
			}
		}
		return result;
	}
	// ===================================================================================================================
	// PUBLICS METHODS
	// ===================================================================================================================
	/**
	@description Public method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	this.refreshCB = [];
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('TaskList', 1, 'TaskList::addRefreshCB - loaded');
		that.refreshCB.push( { cb : cb, context : context });
	}
	/**
	@description Public method that loads each callback.
	@method callRefreshCB
	**/
	function callRefreshCB() {
		MIOGA.logDebug('TaskList', 1, 'TaskList::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that tests if a current status of project task is defined
	@method hasTaskStatus
	**/
	function hasTaskStatus () {
		MIOGA.logDebug('TaskList', 1, 'TaskList::hasTaskStatus - loaded');
		var result = false;
		var tl_length = that.task_list.length;
		for (var i=0; i < tl_length; i++) {
			if (that.task_list[i].current_status !== null) {
				result = true;
				MIOGA.logDebug('TaskList', 1, 'TaskList::hasTaskStatus - return true');
				break;
			}
		}
		return result;
	}
	/**
	@description Public method that tests if a current status of project result is defined
	@method hasTaskInResult
	**/
	function hasTaskInResult () {
		MIOGA.logDebug('TaskList', 1, 'TaskList::hasTaskInResult - loaded');
		var result = false;
		var tl_length = that.task_list.length;
		for (var i=0; i < tl_length; i++) {
			if(that.task_list[i].position_result.length > 0) {
				result = true;
				MIOGA.logDebug('TaskList', 1, 'TaskList::hasTaskInResult - return true');
				break;
			}
		}
		return result;
	}
	/**
	@description Public method that tests if a position list of project task is defined
	@method hasTaskInGroupingList
	**/
	function hasTaskInGroupingList () {
		MIOGA.logDebug('TaskList', 1, 'TaskList::hasTaskInGroupingList - loaded');
		var result = false;
		var tl_length = that.task_list.length;
		for (var i=0; i < tl_length; i++) {
			$.each(that.task_list[i].position_list, function (ind,el) {
				if (el !== null) {
					if (el[1] !== -1) {
						result = true;
						MIOGA.logDebug('TaskList', 1, 'TaskList::hasTaskInGroupingList - return true');
					}
				}
			});
		}
		return result;
	}
	/**
	@description Public method that tests if a position tree of project task is defined
	@method hasTaskInGroupingTree
	**/
	function hasTaskInGroupingTree () {
		MIOGA.logDebug('TaskList', 1, 'TaskList::hasTaskInGroupingTree - loaded');
		var result = false;
		var tl_length = that.task_list.length;
		for (var i=0; i < tl_length; i++) {
			if(that.task_list[i].position_tree[0] !== -1) {
				result = true;
				MIOGA.logDebug('TaskList', 1, 'TaskList::hasTaskInGroupingTree - return true');
				break;
			}
		}
		return result;
	}
	/**
	@description Public method that retrieves task and update Task object attribute
	@method updateTaskAttr
	@param Object args contains project_id and task_id
	@param Boolean async that determine is Request will be asynchronous or not
	@param Function cb that is callback called on AJAX request success
	**/
	function updateTaskAttr (args, async, cb) {
		MIOGA.logDebug('TaskList', 1, 'TaskList::updateTaskAttr - loaded');
		var params = {
			project_id : args.project_id,
			task_id : args.task_id
		};
		$.ajax({
			async : async,
			url : "GetTask.json",
			data : params,
			success : function (response) {
				if (response.status === "OK") {
					var workload;
					var current_status;
					var delegated_user_id;
					var current_progress;
					if (response.data.workload !== null) {
						workload = parseInt(response.data.workload,10);
					}
					else {
						workload = null;
					}
					if (response.data.current_status !== null) {
						current_status = parseInt(response.data.current_status,10);
					}
					else {
						current_status = null;
					}
					if (response.data.delegated_user_id !== null) {
						delegated_user_id = parseInt(response.data.delegated_user_id,10);
					}
					else {
						delegated_user_id = null;
					}
					if (response.data.current_progress !== null) {
						current_progress = parseInt(response.data.current_progress,10);
					}
					else {
						current_progress = null;
					}
					var new_task = new Task({
						haussmann : that.haussmann,
						task_list : that.task_list,
						created : response.data.created,
						modified : response.data.modified,
						last_access : response.data.last_access,
						project_id : that.project_id,
						task_id : parseInt(response.data.task_id,10),
						label : response.data.label,
						description : response.data.description,
						workload : workload,
						pstart : response.data.pstart,
						pend : response.data.pend,
						tags : $.parseJSON(response.data.tags),
						status : $.parseJSON(response.data.status),
						current_status : current_status,
						progress : $.parseJSON(response.data.progress),
						current_progress : current_progress,
						position_tree : $.parseJSON(response.data.position_tree),
						position_list : $.parseJSON(response.data.position_list),
						position_result : $.parseJSON(response.data.position_result),
						fineblanking : $.parseJSON(response.data.fineblanking),
						project_owner_id : that.project_owner_id,
						project_owner_firstname : that.project_owner_firstname,
						project_owner_lastname : that.project_owner_lastname,
						project_owner_email : that.project_owner_email,

						delegated_user_id : delegated_user_id,
						delegated_user_firstname : response.data.delegated_user_firstname,
						delegated_user_lastname : response.data.delegated_user_lastname,
						delegated_user_email : response.data.delegated_user_email,
						attendees : response.data.attendees,
						dstart : response.data.dstart,
						dend : response.data.dend,
						user_is_proj_owner : that.user_is_proj_owner,
						user_is_task_delegated : response.data.user_is_task_delegated
					});

					if (new_task !== undefined) {
						MIOGA.logDebug('TaskList', 1, 'TaskList::updateTaskAttr - success GetTask request, the new Task object is defined');
						checkPositionTree (new_task);
						that.task_list_by_rowid[response.data.task_id] = new_task;
						for (var i = 0; i < that.task_list.length; i++) {
							if (that.task_list[i].task_id === response.data.task_id) {
								that.task_list[i] = new_task;
								break;
							}
						}
					}
					else {
						MIOGA.logError ("TaskList. getTask method can not created a Task Object from Ajax request response. Return on main page...", true);
					}
					if (cb !== undefined) {
						cb(new_task);
					}
				}
				else {
					MIOGA.logError ("TaskList. getTask AJAX request returned KO status", false);
				}
			},
			error : function (err) {
				MIOGA.logError ("TaskList. getTask AJAX request returned an error : " + err, false);
			}
		});
	}
	/**
	@description Public method that checks position_tree. For to do this, it checks if position_tree[0] that is id of grouping tree exists in task_group_id from task_tree from project.
				If not exists, make position_tree of this task as not affected task.
	@method checkPositionTree
	@param Object task is task object
	@param Function cb is a callback called on request success
	**/
	function checkPositionTree (task) {
		var task_tree_index = that.getProjectTaskTreeIndex ();
		var tree_id = parseInt(task.position_tree[0],10);
		if (tree_id === -1 || task_tree_index[tree_id]) {
			// OK
		}
		else {
			task.position_tree[0] = -1;
			task.position_tree[1] = 0;
			task.position_tree[2] = [];
		}
	}
	/**
	@description Public method that retrieves task list and initializes new object Task for each
	@method getTaskList
	@param Boolean  async determines if request is asynchronous or not
	@param Function cb is a callback called on request success
	**/
	function getTaskList (async, cb) {
		MIOGA.logDebug('TaskList', 1, 'TaskList::getTaskList - loaded');
		var params = {};
		params.project_id = that.project_id;
		$.ajax({
			async : async,
			url : "GetTaskList.json",
			data : params,
			success : function (response) {
				if (response.status === "OK") {
					$.each(response.data, function (i,task) {
						var workload;
						var current_status;
						var delegated_user_id;
						var current_progress;
						if (task.workload !== null) {
							workload = parseInt(task.workload,10);
						}
						else {
							workload = null;
						}
						if (task.current_status !== null) {
							current_status = parseInt(task.current_status,10);
						}
						else {
							current_status = null;
						}
						if (task.delegated_user_id !== null) {
							delegated_user_id = parseInt(task.delegated_user_id,10);
						}
						else {
							delegated_user_id = null;
						}
						if (task.current_progress !== null) {
							current_progress = parseInt(task.current_progress,10);
						}
						else {
							current_progress = null;
						}
						
						var new_task = new Task({
							haussmann : that.haussmann,
							task_list : that.task_list,
							created : task.created,
							modified : task.modified,
							last_access : task.last_access,
							project_id : that.project_id,
							task_id : parseInt(task.task_id,10),
							label : task.label,
							description : task.description,
							workload : workload,
							pstart : task.pstart,
							pend : task.pend,
							tags : $.parseJSON(task.tags),
							status : $.parseJSON(task.status),
							current_status : current_status,
							progress : $.parseJSON(task.progress),
							current_progress : current_progress,
							position_tree : $.parseJSON(task.position_tree),
							position_list : $.parseJSON(task.position_list),
							position_result : $.parseJSON(task.position_result),
							fineblanking : $.parseJSON(task.fineblanking),
							project_owner_id : that.project_owner_id,
							project_owner_firstname : that.project_owner_firstname,
							project_owner_lastname : that.project_owner_lastname,
							project_owner_email : that.project_owner_email,

							delegated_user_id : delegated_user_id,
							delegated_user_firstname : task.delegated_user_firstname,
							delegated_user_lastname : task.delegated_user_lastname,
							delegated_user_email : task.delegated_user_email,
							attendees : task.attendees,
							dstart : task.dstart,
							dend : task.dend,
							user_is_proj_owner : that.user_is_proj_owner,
							user_is_task_delegated : response.data.user_is_task_delegated
						});
						
						if (new_task !== undefined) {
							MIOGA.logDebug('TaskList', 1, 'TaskList::getTaskList -the new Task object is defined');
							checkPositionTree (new_task);
							addTask(new_task);
						}
						else {
							MIOGA.logError ("TaskList. getTaskList method can not created a Task Object from Ajax request response. Return on main page...", true);
						}
					});
					if (cb !== undefined) {
						cb();
					}
				}
				else {
					MIOGA.logError ("TaskList. getTaskList AJAX request returned KO status", false);
				}
			},
			error : function (err) {
				MIOGA.logError ("TaskList. getTaskList AJAX request returned an error : " + err, false);
			}
		});
	}
	/**
	@description Public method that sorts task list by position tree and return it
	@method getTaskListByPositionTree
	**/
	function getTaskListByPositionTree () {
		MIOGA.logDebug('TaskList', 1, 'TaskList::getTaskListByPositionTree - loaded');
		var task_list = $.extend([], true, that.task_list);
		task_list.sort(function(a,b){
			if (a.position_tree[0] > b.position_tree[0]) {
				return 1;
			}
			else if (a.position_tree[0] === b.position_tree[0]){
				return 0;
			}
			else {
				return -1;
			}
		});
		task_list.sort(function(a,b){
			if (a.position_tree[0] === b.position_tree[0]) {
				if (a.position_tree[1] > b.position_tree[1]) {
					return 1;
				}
				else if (a.position_tree[1] === b.position_tree[1]){
					return 0;
				}
				else {
					return -1;
				}
			}
			else {
				return 0;
			}
		});
		return task_list;
	}
	/**
	@description Public method that sorts task list by position list and return it
	@method getTaskListByPositionList
	**/
	function getTaskListByPositionList (indexOfList) {
		MIOGA.logDebug('TaskList', 1, 'TaskList::getTaskListByPositionList - loaded');
		var task_list = $.extend([], true, that.task_list);
		task_list.sort(function(a,b){
			if (a.position_list[indexOfList] !== undefined && b.position_list[indexOfList] !== undefined) {
				if (a.position_list[indexOfList] !== null && b.position_list[indexOfList] !== null) {
					if (a.position_list[indexOfList][1] > b.position_list[indexOfList][1]) {
						return 1;
					}
					else {
						return -1;
					}
				}
				else {
					return 0;
				}
			}
			else {
				return 0;
			}
		});
		task_list.sort(function(a,b){
			if (a.position_list[indexOfList] !== undefined && b.position_list[indexOfList] !== undefined) {
				if (a.position_list[indexOfList] !== null && b.position_list[indexOfList] !== null) {
					if (a.position_list[indexOfList][1] === b.position_list[indexOfList][1]) {
						if (a.position_list[indexOfList][2] > b.position_list[indexOfList][2]) {
							return 1;
						}
						else {
							return -1;
						}
					}
					else {
						return 0;
					}
				}
				else {
					return 0;
				}
			}
			else {
				return 0;
			}
		});
		return task_list;
	}
	/**
	@description Public method that sorts task list by position result and return it
	@method getTaskListByPositionResult
	**/
	function getTaskListByPositionResult () {
		MIOGA.logDebug('TaskList', 1, 'TaskList::getTaskListByPositionResult - loaded');
		// sort all tasks by positionResult container and position result index
		var task_list = $.extend([], true, that.task_list);
		task_list.sort(function(a,b){
			if (a.position_result[0] > b.position_result[0]) {
				return 1;
			}
			else {
				return -1;
			}
		});
		task_list.sort(function(a,b){
			if (a.position_result[0] === b.position_result[0]) {
				if (a.position_result[1] > b.position_result[1]) {
					return 1;
				}
				else {
					return 0;
				}
			}
			else {
				return 0;
			}
		});
		return task_list;
	}
	/**
	@description Public method that creates a new task
	@method createTask
	@param Object args contains task label
	@param Function onSuccess is callback called on AJAX request is success
	@param Function onError is callback called on AJAX request is failure
	**/
	function createTask (args, onSuccess, onError) {
		MIOGA.logDebug('TaskList', 1, 'TaskList::createTask - loaded');
		var check = {
			status : "success",
			error : []
		};
		var label_list = [];
		for (var i = 0; i < that.task_list.length; i++) {
			label_list.push(that.task_list[i].label);
		}
		isEmpty(args.label, "labelDisallowEmpty", check);
		isUnique (args.label, label_list, "taskAlreadyExists", check);
		if (check.status === "success") {
			var task_list_attr = [];
			task_list_attr.unshift({
				label : args.label,
				position_tree : [-1,0,[]],
				position_list : [],
				position_result : []
			});
			var position_list = [];
			$.each(that.task_list, function (i,task) {
				// if task position for minimum one view is no affected, must be modify this task position
				var modifyNeeded = false;
				var position_tree = [];
				var position_list = [];
				var position_result = [];
				
				if (task.position_tree[0] === -1) {
					modifyNeeded = true;
					position_tree = $.extend([],task.position_tree);
					position_tree[1] ++;
				}
				if (task.position_list.length > 0) {
					for (var j=0; j < task.position_list.length; j++) {
						if (task.position_list[j] != null) {
							if (task.position_list[j][1] === -1) {
								task.position_list[j][2] ++;
							}
						}
					}
					position_list = $.extend([],task.position_list);
				}
				if (task.position_result.length > 0 ) {
					position_result = $.extend([], task.position_result);
					if (task.position_result[0] === -1) {
						modifyNeeded = true;
						position_result[1] ++;
					}
					else {
					}
				}
				
				if (modifyNeeded === true) {
					task_list_attr.push({
						label : task.label,
						task_id : task.task_id,
						position_tree : position_tree,
						position_list : position_list,
						position_result : position_result,
						set_modified : "f"
					});
				}
			});
			that.setTaskList({task_list : task_list_attr}, onSuccess);
		}
		else {
			onError(check.error);
		}
	}
	
	/**
	@description Public method that updates position result when his result is deleted. It makes position_result as [-1,x]
	@method updatePositionToDeleteResult
	@param Integer result_id is id of deleted result
	**/
	function updatePositionToDeleteResult (result_id) {
		console.log('updatePositionToDeleteResult loaded. result_id = ' + result_id);
		var tl = [];
		var count_no_affected = 0;
		for (count_no_affected; count_no_affected < that.task_list.length; count_no_affected++) {
			if (that.task_list[count_no_affected].position_result.length > 0) {
				if (that.task_list[count_no_affected].position_result[0] === result_id) {
					tl.push(that.task_list[count_no_affected]);
				}
			}
		}
		for (var i=0; i < tl.length; i++) {
			tl[i].position_result = [-1, (count_no_affected+i)];
		}
		that.setTaskList ({
			task_list : tl
		});
	}
	
	/**
	@description Public method that updates position list and position result for each task
	@method updtaeTaskPositionToChangeStructure
	@param Object adv contains advanced parameters of project
	**/
	function updtaeTaskPositionToChangeStructure (adv) {
		MIOGA.logDebug('TaskList', 1, 'TaskList::updtaeTaskPositionToChangeStructure - loaded');
		var params= {
			task_list : []
		};
		var countList = adv.grouping_tasks.grouping_list.length;
		$.each(that.task_list, function (iTask, task_item) {
			var task = {
				label : task_item.label,
				task_id : task_item.task_id,
				position_list : task_item.position_list,
				position_result : task_item.position_result
			};
			if (task_item.position_list.length !== adv.grouping_tasks.grouping_list.length) {
				task.position_list = [];
				for (var i=0; i < countList; i++) {
					task.position_list.push([i,-1,iTask]);
				}
			}
			// if project result defined and task position result not defined ==> create task position result
			// if project result not defined and task position result defined ==> make task position result as an empty array
			if (that.haussmann.projectList.project_list_by_rowid[that.project_id].resultList.result_list.length > 0){
				if (task.position_result.length === 0) {
					task.position_result = [-1,iTask];
				}
				else {
					MIOGA.logDebug('TaskList', 2, 'TaskList::updtaeTaskPositionToChangeStructure - result defined and position result defined for this task.');
				}
			}
			else {
				if (task.position_result.length > 0) {
					task.position_result = [];
				}
				else {
					MIOGA.logDebug('TaskList', 2, 'TaskList::updtaeTaskPositionToChangeStructure - No result defined and no position result for this task.');
				}
			}
			params.task_list.push(task);
		});
		that.setTaskList (params);
	}
	/**
	@description Public method that updates task list. It called when task position is changed: on create task, on move task or grouping tree 
	@method setTaskList
	@param Object adv contains advanced parameters of project
	**/
	function setTaskList (args, onSuccess, onError) {
		MIOGA.logDebug('TaskList', 1, 'TaskList::setTaskList - loaded');
		var create = false;
		var params = {
			task_list : []
		};
		$.each(args.task_list, function (i,task_attr) {
			var task = {};
			task.project_id = that.project_id;
			if (task_attr.set_modified !== undefined) {
				task.set_modified = task_attr.set_modified;
			}
			if (task_attr.task_id !== undefined) {
				task.task_id = task_attr.task_id;
			}
			if (task_attr.label !== undefined ) {
				task.label = task_attr.label;
			}
			else {
				task.label = that.task_list_by_rowid[task_attr.task_id].label;
			}
			if (task_attr.position_tree !== undefined) {
				if (task_attr.position_tree[2] === undefined) {
					task_attr.position_tree[2] = [];
				}
				else {
					MIOGA.logDebug('TaskList', 2, 'TaskList::setTaskList - Array of tree element is already defined');
				}
				task.position_tree = JSON.stringify(task_attr.position_tree);
			}
			else {
				task.position_tree = JSON.stringify(that.task_list_by_rowid[task.task_id].position_tree);
			}
			if (task_attr.position_list !== undefined) {
				task.position_list = JSON.stringify(task_attr.position_list);
			}
			else {
				task.position_list = JSON.stringify(that.task_list_by_rowid[task.task_id].position_list);
			}
			if (task_attr.position_result !== undefined) {
				task.position_result = JSON.stringify(task_attr.position_result);
			}
			else {
				task.position_result = JSON.stringify(that.task_list_by_rowid[task.task_id].position_result);
			}
			params.task_list.push(task);
		});

		params.task_list = JSON.stringify(params.task_list);
		$.ajax({
			url : "SetTaskList.json",
			dataType:'json',
			data : params,
			type : "POST",
			async : false,
			success: function(response){
				if (response.status === "OK") {
					params.task_list = $.parseJSON(params.task_list);
					$.each(params.task_list, function (i,task) {
						if (task.task_id !== undefined) {
							that.task_list_by_rowid[task.task_id].position_tree = $.parseJSON(task.position_tree);
							that.task_list_by_rowid[task.task_id].position_list = $.parseJSON(task.position_list);
							that.task_list_by_rowid[task.task_id].position_result = $.parseJSON(task.position_result);
						}
						else {
							MIOGA.logDebug("TaskList", 1, 'TaskList::setTaskList new task called');

							create = true;
							var new_task = new Task({
								haussmann : that.haussmann,
								task_list : that.task_list,
								project_id : parseInt(response.data.project_id,10),
								task_id : parseInt(response.data.task_id,10),
								created : response.data.created,
								modified : response.data.modified,
								last_access : response.data.last_access,
								label : response.data.label,
								description : response.data.description,
								pstart : response.data.pstart,
								pend : response.data.pend,
								project_owner_id : that.project_owner_id,
								project_owner_firstname : that.project_owner_firstname,
								project_owner_lastname : that.project_owner_lastname,
								project_owner_email : that.project_owner_email,
								delegated_user_id : null,
								delegated_user_firstname : response.data.delegated_user_firstname,
								delegated_user_lastname : response.data.delegated_user_lastname,
								delegated_user_email : response.data.delegated_user_email,
								current_progress :null,
								fineblanking : $.parseJSON(response.data.fineblanking),
								current_status : null,
								workload : null,
								position_tree : $.parseJSON(task.position_tree),
								position_list : $.parseJSON(task.position_list),
								position_result : $.parseJSON(task.position_result),
								dstart : task.dstart,
								dend : task.dend,
								status : [],
								progress : [],
								tags : $.parseJSON(task.tags),
								user_is_proj_owner : that.user_is_proj_owner,
								user_is_task_delegated : response.data.user_is_task_delegated
							});
							if (new_task !== undefined) {
								checkPositionTree (new_task);
								addTask(new_task, onSuccess);
							}
						}
					});
					if (create === false && onSuccess !== undefined) {
						onSuccess ();
					}
					that.callRefreshCB ();
				}
				else {
					MIOGA.logError ("TaskList. Status of setTaskList AJAX request is 'KO'. The API last good status will be reloaded", true);
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("TaskList. setTaskList AJAX request returned an error : " + err, true);
			}
		});
	}
	/**
	@description method that deletes task
	@method deleteTask
	@param Object args contains pairs key / value : task_id
	@param Function cb that called on AJAX request success
	**/
	function deleteTask (args, cb) {
		MIOGA.logDebug("TaskList", 1, 'TaskList::deleteTask - loaded');
		var params = {
			task_id : args.task_id,
			project_id : that.project_id
		};
		$.ajax({
			url : "DeleteTask.json",
			dataType:'json',
			data : params,
			type : "POST",
			success : function(response){ 
				if (response.status === "OK") {
					for (var task_i = 0; task_i < that.task_list.length; task_i ++) {
						if (that.task_list[task_i].task_id === params.task_id) {
							that.task_list.splice(task_i,1);
							delete that.task_list_by_rowid[params.task_id];
							break;
						}
					}
					if (cb !== undefined) {
						cb(that);
					}
					else {
						that.callRefreshCB();
					}
				}
				else {
					MIOGA.logError ("TaskList. Status of deleteTask AJAX request is 'KO'. The API last good status will be reloaded", false);
 					that.callRefreshCB();
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("TaskList. deleteTask AJAX request returned an error : " + err, true);
			}
		});
	}
	// -------------------------------------------------------
	// INIT
	// -------------------------------------------------------
	this.getTaskList(false);

	if (
		this.project_id === undefined || this.advancedParams === undefined || 
		this.project_owner_id === undefined || this.project_owner_firstname === undefined || 
		this.project_owner_lastname === undefined || this.project_owner_email === undefined
		) {
		MIOGA.logError ("TaskList. Object can not be initialized.", false);
		return undefined;
	}
} // End TaskList object
