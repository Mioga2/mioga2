// =============================================================================
/**
Create a AdvancedParamsEditor object.

    var options = {
        i18n : { .... }
    };

	this.advancedParamsEditor = new AdvancedParamsEditor($elem, options);

@class Haussmann
@constructor
@param {Object} $elem JQuery that will be containing this editor
@param {Object} options
**/
// =============================================================================
function AdvancedParamsEditor ($elem, options) {
	"use strict";
	MIOGA.debug.AdvancedParamsEditor = 0;
	MIOGA.logDebug('AdvancedParamsEditor', 1, ' AdvancedParamsEditor loaded');

	var that = this;
	this.$elem = $elem;

	this.i18n = options.i18n;
	this.changeMustBeSaveFlag = options.changeMustBeSaveFlag;
	this.project = options.project;
	this.resultList = undefined;
	this.haussmann = options.haussmann;
	this.insertVocabulary = options.haussmann.insertVocabulary;
	this.advancedParams = undefined;
	this.modelList = options.haussmann.projectList.modelList;
	this.taskList = undefined;
	this.showProjectEditor = options.showProjectEditor;

	this.result_status_values = [];
	this.task_status_values = [];
	this.project_status_values = [];
	this.compl_data_values = [];
	this.grouping_tree_values = [];
	this.grouping_list_values = [];

	$.fn.editable.defaults.mode = 'inline';
	// -----------------------------------------------------------------
	// generate Ids
	this.model_label_id = MIOGA.generateID("haussmann");
	this.params_result_cont_id = MIOGA.generateID("haussmann");
	this.params_task_status_cont_id = MIOGA.generateID("haussmann");
	this.params_project_status_cont_id = MIOGA.generateID("haussmann");
	this.params_compl_data_cont_id = MIOGA.generateID("haussmann");
	this.params_grouping_tree_cont_id = MIOGA.generateID("haussmann");
	this.params_grouping_list_cont_id = MIOGA.generateID("haussmann");
	this.params_vocabulay_id = MIOGA.generateID("haussmann");
	// -----------------------------------------------------------------
	// publics method declaration
	this.showWindow = showWindow;
	this.hideWindow = hideWindow;
	this.editAdvancedParams = editAdvancedParams;
	this.refresh = refresh;
	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// =======================================================================
	// PRIVATES METHODS
	// =======================================================================
	/**
	@description Private method that checks all attributes of advanced parametersobject
	@method checkAdvParams
	@param object params that contains paire key / value of advanced parameters attributes
	@param Function onSuccess that is callback on success checking
	@param Object $error_cont that is jquery object container of error message
	**/	
	function checkAdvParams (params, onSuccess, $error_cont) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::checkAdvParams - loaded');
		var checkAdvParameters = that.advancedParams.checkAdvancedParameters (params.advanced_params);
		var checkModel = {
			status : "success",
			error : []
		};
		if (params.label !== undefined) {
			checkModel = that.modelList.checkModel(params);
		}
		if (checkAdvParameters.status === "success" && checkModel.status === "success") {
			onSuccess(params);
		}
		else {
			$.each(checkAdvParameters.error, function (i,text) {
				$error_cont.append('<p class="error">' + that.i18n[text] + '</p>');
			});
			$.each(checkModel.error, function (i,text) {
				$error_cont.append('<p class="error">' + that.i18n[text] + '</p>');
			});
		}
	}
	/**
	@description Private method that serializes advanced parameters attribute from DOM and return it
	@method checkAdvParams
	**/	
	function serializeAdvancedParams () {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::serializeAdvancedParams - loaded');
		var advanced_params_attr = {};
		advanced_params_attr.result_status = [];
		advanced_params_attr.task_status = [];
		advanced_params_attr.project_status = [];
		advanced_params_attr.complementary_data = [];
		advanced_params_attr.grouping_tasks = {
			"grouping_tree": [],
			"grouping_list": []
		};
		advanced_params_attr.vocabulary = [];
		var result_status = that.$result_box.find('#' + that.params_result_cont_id).extensibleList('getValues');
		$.each(result_status, function (i,result) {
			advanced_params_attr.result_status.push(result.label);
		});
		var task_status = that.$task_status_box.find('#' + that.params_task_status_cont_id).extensibleList('getValues');
		$.each(task_status, function (i,task) {
			advanced_params_attr.task_status.push(task.label);
		});
		var project_status = that.$project_status_box.find('#' + that.params_project_status_cont_id).extensibleList('getValues');
		$.each(project_status, function (i,project) {
			advanced_params_attr.project_status.push(project.label);
		});
		var compl_data = that.$compl_data_box.find('#' + that.params_compl_data_cont_id).extensibleList('getValues');
		$.each(compl_data, function (i, comp_data) {
			advanced_params_attr.complementary_data.push({
				label : comp_data.label,
				type : that.i18n.complDataType[comp_data.type]
			});
		});
		var grouping_tree = that.$grouping_tasks_box.find('#' + that.params_grouping_tree_cont_id).extensibleList('getValues');
		$.each(grouping_tree, function (i,tree) {
			advanced_params_attr.grouping_tasks.grouping_tree.push(tree.label);
		});
		var grouping_list = that.$grouping_tasks_box.find('#' + that.params_grouping_list_cont_id).extensibleList('getValues');
		
		$.each(grouping_list, function (i,list) {
			var list_item = {
				label : list.label,
				values : []
			};
			$.each(list.values, function (ind, value) {
				list_item.values.push(value.label);
			});
			advanced_params_attr.grouping_tasks.grouping_list.push(list_item);
		});
		$.each(that.$vocabulary.find('.params-vocabulary-table tbody tr'), function (i,voc) {
			var $tr = $(this);
			var term_str = $tr.find('td').eq(0).text();
			var item = {
				term : term_str,
				code: i,
				value : {}
			};
			$.each($tr.find('td').eq(1).find('p'), function (ind,elm) {
				var lang = $(elm).find('label').text();
				var singular = $.trim($(elm).find('input').val());
				item.value[lang] = [];
				item.value[lang].push(singular);
			});
			$.each($tr.find('td').eq(2).find('p'), function (ind,elm) {
				var lang = $(elm).find('label').text();
				var plurual = $.trim($(elm).find('input').val());
				item.value[lang].push(plurual);
			});
			advanced_params_attr.vocabulary.push(item);
		});
		return advanced_params_attr;
	}
	/**
	@description Private method that update advanced parameters of project temporarily
	@method onApply
	**/	
	function onApply () {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::onApply - loaded');
		var cloneAdv = that.advancedParams.cloneAttr();
		that.project.setProjectAdvancedParams(cloneAdv, function () {
			that.showProjectEditor ();
			that.hideWindow();
			
		});
	}
	/**
	@description Private method that draws temporarily success message
	@method successMessage
	@param Object $cont that is jQuery element that will be contains a success message
	@param String message that is a success message
	**/
	function successMessage ($cont, message) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::successMessage - loaded');
		var $message = $('<span class="success-message">' + message + '</span>').appendTo($cont).hide();
		$message.fadeIn(1800, function () {
			$(this).fadeOut(2500, function () {
				$(this).remove();
			});
		});
	}
	/**
	@description Private function that draws errors list in a HTML container
	@method drawErrorSimpleList
	@param Object $elem is the Jquery element as container of error list will be drawed.
	@param Array error_list is the list of translate error message.
	**/
	function drawErrorSimpleList ($elem, error_list) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::drawErrorSimpleList - loaded');
		$elem.find('.error').remove();
		var $error_cont = $('<div class="error"></div>').appendTo($elem);
		$.each(error_list, function (i,error_str) {
			$error_cont.append('<p class="error">' + that.i18n[error_str] + '</p>');
		});
	}
	/**
	@description Private method that creates a new model. On success of creation, it inserts a vocabulary , updates a data key for model id and show success message of model creation.
	@method createModel
	@param Object params contains paire key / value : label and / or project_id
	**/
	function createModel (params) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::createModel - loaded');
		that.advancedParams.createModel(false,params,function (new_model) {
			that.$update_model_btn.removeAttr("disabled").removeClass("disabled");
			that.$model_box.data('model_id', new_model.model_id);
			that.insertVocabulary(params.advanced_params.vocabulary);
			successMessage(that.$model_box.find('fieldset'), that.i18n.model_created_success);
		});
	}
	/**
	@description Private method that calls advancedParams object method to update a current model. On success of updating model, it inserts a vocabulary and show success message of model updating.
	@method setModel
	@param Object params contains paire key / value : label and / or project_id
	**/
	function setModel (params) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::setModel - loaded');
		that.advancedParams.setModel(false,params,function (new_model) {
			that.$update_model_btn.removeAttr("disabled").removeClass("disabled");
			that.insertVocabulary(params.advanced_params.vocabulary);
			successMessage(that.$model_box.find('fieldset'), that.i18n.model_updated_success);
		});
	}
	/**
	@description Private method that is called on click to item list.
				It initializes Jquery editable Plugin and defines check function of this.
				In this function that checks validity of new value, calling update value to extensibleList Plugin that returns true or false.
				If this is success, a callback is called : onSuccess like : updateResultStatusList or updateTasktStatusList ...
				NB : the editable plugin is used just for HTML rendering of form.
				
	@method fieldEditable
	@param Object params contains paire key / value : label and / or project_id
	**/
	var fieldEditable = function (ev, elem, cont_list, onSuccess) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::fieldEditable - loaded');
		var $clone = $(elem).parent().parent('li').clone();
		$(cont_list).find('.error').remove();
		if ($(elem).hasClass('editable') === false) {
			$(elem).editable({
				validate : function (value) {
					$(elem).show().editable('destroy');
					if (value !== $(elem).attr('value')) {
						var upd = $(cont_list).extensibleList('updateValue',elem, $.trim(value), $clone);
						if (upd === true) {
							$(elem).attr('value', $.trim(value)).html($.trim(value));
							$(elem).editable('destroy');
							var list = $(cont_list).extensibleList('getValues');
							onSuccess(cont_list, list);
						}
					}
				},
				onblur : "ignore",
				savenochange : true
			}).editable('show');
			$(elem).parent().find('.editable-cancel').click(function (ev) {
				$(elem).show().editable('destroy');
				ev.stopPropagation();
			});
		}
	};
	/**
	@description Private method that checks if label that will be added already exists in initial list.
				It uses because, when extensible list Plugin is initialized with existing value, a callback on add item is called for each adding.
				And this callback containing a method of advancedParams object that push a new value in her list. So if the item already exists, it will be RE added in object list.
	@method addableToList
	@param Object params contains paire key / value : label and / or project_id
	**/
	function addableToList (list, label) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::addableToList - loaded');
		var canAdd = true;
		for (var i = 0; i < list.length; i++){
			if (list[i].label === label) {
				canAdd = false;
				break;
			}
		}
		return canAdd;
	}
	// ----------------------------------------------------------------
	// check methods for extensible list adding
	// -----------------------------------------------------------------
	/**
	@description Private method that checks status item of result validity and return status with error list. On error, it calls method that draws error. 
	@method checkResultStatus
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function checkResultStatus(data, elem) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::checkResultStatus - loaded');
		var resultCheck = that.advancedParams.checkResultStatus({label : $.trim(data.label)});
		var result = true;
		if (resultCheck.status === "error") {
			result = false;
			drawErrorSimpleList ($(elem), resultCheck.error);
		}
		return result;
	}
	/**
	@description Private method that checks status item of task validity and return status with error list. On error, it calls method that draws error. 
	@method checkTaskStatus
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function checkTaskStatus (data, elem) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::checkTaskStatus - loaded');
		var taskCheck = that.advancedParams.checkTaskStatus({label : $.trim(data.label)});
		var result = true;
		if (taskCheck.status === "error") {
			result = false;
			drawErrorSimpleList ($(elem), taskCheck.error);
		}
		return result;
	}
	/**
	@description Private method that checks status item of project validity and return status with error list. On error, it calls method that draws error. 
	@method checkProjectStatus
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function checkProjectStatus (data, elem) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::checkProjectStatus - loaded');
		var projectCheck = that.advancedParams.checkProjectStatus({label : $.trim(data.label)});
		var result = true;
		if (projectCheck.status === "error") {
			result = false;
			drawErrorSimpleList ($(elem), projectCheck.error);
		}
		return result;
	}
	/**
	@description Private method that checks status item of comlementary data validity and return status with error list. On error, it calls method that draws error. 
	@method checkComplementaryData
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function checkComplementaryData(data, elem) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::checkComplementaryData - loaded');
		var complDataCheck = that.advancedParams.checkComplementaryData({label : $.trim(data.label)});
		var result = true;
		if (complDataCheck.status === "error") {
			result = false;
			drawErrorSimpleList ($(elem), complDataCheck.error);
		}
		return result;
	}
	/**
	@description Private method that checks status item of grouping tree validity and return status with error list. On error, it calls method that draws error. 
	@method checkGroupingTree
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function checkGroupingTree(data, elem) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::checkGroupingTree - loaded');
		var resultCheck = that.advancedParams.checkGroupingTree({label : $.trim(data.label)});
		var result = true;
		if (resultCheck.status === "error") {
			result = false;
			drawErrorSimpleList ($(elem), resultCheck.error);
		}
		return result;
	}
	/**
	@description Private method that checks status item of gouping list validity and return status with error list. On error, it calls method that draws error. 
	@method checkGroupingList
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function checkGroupingList (data, elem) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::checkGroupingList - loaded');
		var resultCheck = that.advancedParams.checkGroupingList({label : $.trim(data.label)});
		var result = true;
		if (resultCheck.status === "error") {
			result = false;
			drawErrorSimpleList ($(elem), resultCheck.error);
		}
		return result;
	}
	/**
	@description Private method that checks status item of grouping sub list validity and return status with error list. On error, it calls method that draws error. 
	@method checkSubGroupingList
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function checkSubGroupingList (data, elem) {
		MIOGA.logDebug('AdvancedParamsEditor', 1, 'AdvancedParamsEditor::checkSubGroupingList - loaded');
		var parent_label = $(elem).parent().children('div.label').children('span[name="label"]').attr('value');
		var resultCheck = that.advancedParams.checkSubGroupingList({
			label : $.trim(data.label),
			parent_label : parent_label
		});
		var result = true;
		if (resultCheck.status === "error") {
			result = false;
			drawErrorSimpleList ($(elem), resultCheck.error);
		}
		return result;
	}
	// ----------------------------------------------------------------
	// Update list  methods for extensible list
	// -----------------------------------------------------------------
	/**
	@description Private method that updates result status list to advanced parameter object 
	@method updateResultStatusList
	@param Object elem is DOM element that will be contains error message
	@param Array list of label
	**/
	function updateResultStatusList (elem, list) {
		that.changeMustBeSaveFlag (true);
		that.advancedParams.updateResultStatusList(list);
	}
	/**
	@description Private method that updates task status list to advanced parameter object 
	@method updateTaskStatusList
	@param Object elem is DOM element that will be contains error message
	@param Array list of label
	**/
	function updateTaskStatusList (elem, list) {
		that.advancedParams.updateTaskStatusList(list);
	}
	/**
	@description Private method that updates project status list to advanced parameter object 
	@method updateTaskStatusList
	@param Object elem is DOM element that will be contains error message
	@param Array list of label
	**/
	function updateProjectStatusList (elem, list) {
		that.advancedParams.updateProjectStatusList(list);
	}
	/**
	@description Private method that updates complementary data list to advanced parameter object 
	@method updateComplementaryDataList
	@param Object elem is DOM element that will be contains error message
	@param Array list of paire key / value contains : type and label
	**/
	function updateComplementaryDataList (elem, list) {
		if (list.length) {
			for (var i=0; i < list.length; i++) {
				list[i].type = that.i18n.complDataType[list[i].type];
			}
		}
		that.advancedParams.updateComplementaryDataList(list);
	}
	/**
	@description Private method that updates grouping tree list to advanced parameter object 
	@method updateGroupingTreeList
	@param Object elem is DOM element that will be contains error message
	@param Array list of label
	**/
	function updateGroupingTreeList (elem, list) {
		that.advancedParams.updateGroupingTreeList(list);
	}
	/**
	@description Private method that updates grouping list list to advanced parameter object 
	@method updateGroupingList
	@param Object elem is DOM element that will be contains error message
	@param Array list of label
	**/
	function updateGroupingList (elem, list) {
		that.advancedParams.updateGroupingList(list);
	}
	/**
	@description Private method that updates grouping sub list list to advanced parameter object 
	@method updateSubGroupingList
	@param Object elem is DOM element that will be contains error message
	@param Array list of label
	**/
	function updateSubGroupingList (elem, list) {
		var parent_label = $(elem).parent().children('div.label').children('span[name="label"]').attr('value');
		that.advancedParams.updateSubGroupingList({
			list : list,
			parent_label : parent_label
		});
	}
	// ----------------------------------------------------------------
	// callbacks to click on extensible list item
	// -----------------------------------------------------------------
	/**
	@description Private method that is called on click on result item from list for rename it. The arguments come from extensibleList that implement click event to item list
	@method resultEditable
	@param Object ev is event click
	@param Object elem is DOM element that is clicked
	@param Object cont_list is DOM element that is selector of extensible list
	**/
	function resultEditable (ev, elem, cont_list) {
		fieldEditable(ev, elem, cont_list, updateResultStatusList);
	}
	/**
	@description Private method that is called on click on task item from list for rename it. The arguments come from extensibleList that implement click event to item list
	@method taskEditable
	@param Object ev is event click
	@param Object elem is DOM element that is clicked
	@param Object cont_list is DOM element that is selector of extensible list
	**/
	function taskEditable (ev, elem, cont_list) {
		fieldEditable(ev, elem, cont_list, updateTaskStatusList);
	}
	/**
	@description Private method that is called on click on result item from list for rename it. The arguments come from extensibleList that implement click event to item list
	@method projectEditable
	@param Object ev is event click
	@param Object elem is DOM element that is clicked
	@param Object cont_list is DOM element that is selector of extensible list
	**/
	function projectEditable (ev, elem, cont_list) {
		fieldEditable(ev, elem, cont_list, updateProjectStatusList);
	}
	/**
	@description Private method that is called on click on project status item from list for rename it. The arguments come from extensibleList that implement click event to item list
	@method complementaryDataEditable
	@param Object ev is event click
	@param Object elem is DOM element that is clicked
	@param Object cont_list is DOM element that is selector of extensible list
	**/
	function complementaryDataEditable (ev, elem, cont_list) {
		fieldEditable(ev, elem, cont_list, updateComplementaryDataList);
	}
	/**
	@description Private method that is called on click on complementary data item from list for rename it. The arguments come from extensibleList that implement click event to item list
	@method treeEditable
	@param Object ev is event click
	@param Object elem is DOM element that is clicked
	@param Object cont_list is DOM element that is selector of extensible list
	**/
	function treeEditable (ev, elem, cont_list) {
		fieldEditable(ev, elem, cont_list, updateGroupingTreeList);
	}
	/**
	@description Private method that is called on click on tree grouping item from list for rename it. The arguments come from extensibleList that implement click event to item list
	@method groupingListEditable
	@param Object ev is event click
	@param Object elem is DOM element that is clicked
	@param Object cont_list is DOM element that is selector of extensible list
	**/
	function groupingListEditable (ev, elem, cont_list) {
		fieldEditable(ev, elem, cont_list, updateGroupingList);
	}
	/**
	@description Private method that is called on click on grouping list item from list for rename it. The arguments come from extensibleList that implement click event to item list
	@method subGroupingListEditable
	@param Object ev is event click
	@param Object elem is DOM element that is clicked
	@param Object cont_list is DOM element that is selector of extensible list
	**/
	function subGroupingListEditable(ev, elem, cont_list) {
		that.advancedParams.updateGroupingList(that.$list.extensibleList('getValues'));
		fieldEditable(ev, elem, cont_list, updateSubGroupingList);
	}
	// ----------------------------------------------------------------
	// Adding methods for extensible list
	// -----------------------------------------------------------------
	/**
	@description Private method that adds result status in advanced parameter object 
	@method addResultStatus
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function addResultStatus (elem, data) {
		if (addableToList(that.result_status_values, data.label)) {
			$(elem).find('.error').remove();
			that.advancedParams.addResultStatus({label : data.label});
			that.changeMustBeSaveFlag (true);
		}
	}
	/**
	@description Private method that adds task status in advanced parameter object 
	@method addTaskStatus
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function addTaskStatus (elem, data) {
		$(elem).find('.error').remove();
		if (addableToList(that.task_status_values, data.label)) {
			that.advancedParams.addTaskStatus({label : data.label});
		}
	}
	/**
	@description Private method that adds project status in advanced parameter object 
	@method addProjectStatus
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function addProjectStatus (elem, data) {
		$(elem).find('.error').remove();
		if (addableToList(that.project_status_values, data.label)) {
			that.advancedParams.addProjectStatus({label : data.label});
		}
	}
	/**
	@description Private method that adds complementary data in advanced parameter object 
	@method addComplementaryData
	@param Object data contains paire key / value : label and type
	@param Object elem is DOM element that will be contains error message
	**/
	function addComplementaryData(elem, data) {
		$(elem).find('.error').remove();
		if (addableToList(that.compl_data_values, data.label)) {
			var type = that.i18n.complDataType[data.type];
			that.advancedParams.addComplementaryData ({label : data.label, type : type});
		}
	}
	/**
	@description Private method that adds grouping tree in advanced parameter object 
	@method addGroupingTree
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function addGroupingTree (elem, data) {
		$(elem).find('.error').remove();
		if (addableToList(that.grouping_tree_values, data.label)) {
			that.advancedParams.addGroupingTree({label : data.label});
		}
	}
	/**
	@description Private method that adds grouping sub list in advanced parameter object 
	@method addSubGroupingList
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function addSubGroupingList (elem, data) {
		var args = {
			label : data.label,
			parent_label : $(elem).parent().children('div.label').children('span[name="label"]').attr('value')
		};
		var initial_list = $(elem).data('extensibleList').values;
		$(elem).find('.error').remove();
		if (addableToList(initial_list, args.label)) {
			that.advancedParams.addSubGroupingList (args);
		}
	}
	/**
	@description Private method that adds grouping list in advanced parameter object 
	@method addGroupingList
	@param Object data contains paire key / value : label
	@param Object elem is DOM element that will be contains error message
	**/
	function addGroupingList (elem, data) {
		var indexList = $(elem).extensibleList('getValues').length -1;
		var sub_values = [];
		var $sub = $('<div class="sub-list-cont" name="values"></div>').delegate('input', 'click', function () {
			$sub.find('.error').remove();
		});
		$(elem).find('.error').remove();
		if (addableToList(that.grouping_list_values, data.label)) {
			that.advancedParams.addGroupingList (data);
		}
		$.each(that.advancedParams.grouping_tasks.grouping_list[indexList].values, function (ind,el) {
			sub_values.push({
				label : el
			});
		});

		$(elem).find('ul:first').find('li:last').append($sub);
		$sub.extensibleList ({
			values : sub_values,
			template: $('<span><input type="text" name="label"/></span>'),
			parentDeletable : false,
			updateCB : addSubGroupingList,
			checkCB : checkSubGroupingList,
			removeCB : updateSubGroupingList,
			orderCB : updateSubGroupingList,
			delegates : {
				nameList : ["label"],
				onClickCB : subGroupingListEditable
			},
			sortable: {
				status : true,
				items : "li:not(.no-sortable)"
			}
		});
	};

	// =======================================================================
	// PUBLICS METHODS
	// =======================================================================
	/**
	@description Public method that shows this window and hide other. It calls refresh method. 
	@method showWindow
	**/
	function showWindow() {
		MIOGA.logDebug("AdvancedParamsEditor::showWindow", 1, 'showWindow loaded');
		that.$cont.show();
		that.refresh();
	}
	/**
	@description Public method that hides this window.
	@method showWindow
	**/
	function hideWindow() {
		MIOGA.logDebug("AdvancedParamsEditor::hideWindow", 1, 'hideWindow loaded');
		that.$cont.hide();
		that.$elem.find('.project-params-editor').show();
	}
	/**
	@description Public method that initialize new advanced parameter object and show this window.
	@method editAdvancedParams
	**/
	function editAdvancedParams (proj, adv) {
		that.advancedParams = new AdvancedParams(adv);
		that.modelList = that.advancedParams.modelList;
		that.project = proj;
		that.resultList = proj.resultList;
		that.taskList = proj.taskList;

		that.showWindow ();
	}
	/**
	@description Public method that refresh advanced parameter editor
	@method refresh
	**/
	function refresh () {
		var $template_compl_data = $(
			'<span>'
				+'<input type="text" name="label"/>'
				+ '<select name="type" class="params-compl-data-type"></select>'
			+ '</span>'
		);
		// initialize extensibleList
		this.$result_box.find('.params-result-box > *').not('legend').remove();
		this.$result_box.find('.params-result-box').append('<div class="form-item" id="' + that.params_result_cont_id + '"></div>');
		// ----------------------------------------------------------------
		// result status
		that.result_status_values = [];
		$.each(that.advancedParams.result_status, function (i,result) {
			that.result_status_values.push({
				label : result
			});
		});
		var $result_list = this.$result_box.find('#' + that.params_result_cont_id)
		.extensibleList ({
			values: that.result_status_values,
			template: $('<span><input type="text" name="label"/></span>'),
			updateCB : addResultStatus,
			checkCB : checkResultStatus,
			orderCB : updateResultStatusList,
			removeCB : updateResultStatusList,
			onblur : "ignore",
			delegates : {
				nameList : ["label"],
				onClickCB : resultEditable
			},
			sortable: {
				status : true,
				items : "li:not(.no-sortable)"
			}
		});
		if (that.resultList.hasResultStatus () === true) {
			$result_list.sortable('destroy');
			$result_list.find('ul .item-delete').removeClass('item-delete').addClass('item-not-delete');
		}
		// ----------------------------------------------------------------
		// task status
		// ----------------------------------------------------------------
		that.task_status_values = [];
		$.each(that.advancedParams.task_status, function (i,task_status) {
			that.task_status_values.push({
				label : task_status
			});
		});
		// initialize extensibleList
		this.$task_status_box.find('.params-task-status-box *').not('legend').remove();
		this.$task_status_box.find('.params-task-status-box').append('<div class="form-item" id="' + that.params_task_status_cont_id + '"></div>');
		
		var $task_status_list = this.$task_status_box.find('#' + that.params_task_status_cont_id).extensibleList ({
			values: that.task_status_values,
			template: $('<span><input type="text" name="label"/></span>'),
			updateCB : addTaskStatus,
			checkCB : checkTaskStatus,
			orderCB : updateTaskStatusList,
			removeCB : updateTaskStatusList,
			delegates : {
				nameList : ["label"],
				onClickCB : taskEditable
			},
			sortable: {
				status : true,
				items : "li:not(.no-sortable)"
			}
		});
		if (that.taskList.hasTaskStatus () === true) {
			$task_status_list.find('ul').sortable('destroy');
			$task_status_list.find('ul .item-delete').removeClass('item-delete').addClass('item-not-delete');
		}
		// ----------------------------------------------------------------
		// project status
		// ----------------------------------------------------------------
		that.project_status_values = [];
		$.each(that.advancedParams.project_status, function (i,project_status) {
			that.project_status_values.push({
				label : project_status
			});
		});
		// initialize extensibleList
		this.$project_status_box.find('.params-project-status-box *').not('legend').remove();
		this.$project_status_box.find('.params-project-status-box').append('<div class="form-item" id="' + that.params_project_status_cont_id + '"></div>');
		var $project_status_list = this.$project_status_box.find('#' + that.params_project_status_cont_id).extensibleList ({
			values: that.project_status_values,
			template: $('<span><input type="text" name="label"/></span>'),
			updateCB : addProjectStatus,
			checkCB : checkProjectStatus,
			orderCB : updateProjectStatusList,
			removeCB : updateProjectStatusList,
			delegates : {
				nameList : ["label"],
				onClickCB : projectEditable
			},
			sortable: {
				status : true,
				items : "li:not(.no-sortable)"
			}
		});
		if (that.project.hasProjectStatus () === true) {
			that.$project_status_box.find('ul').sortable('destroy');
			that.$project_status_box.find('ul .item-delete').removeClass('item-delete').addClass('item-not-delete');
		}
		else {
			that.$project_status_box.find('ul').sortable('refresh');
		}
		// ----------------------------------------------------------------
		// complementary data
		// ----------------------------------------------------------------
		that.compl_data_values = [];
		$.each(that.advancedParams.complementary_data, function (i,compl_data) {
			var type_str = "";
			$.each(that.i18n.complDataType, function (str, value) {
				if (compl_data.type === value) {
					type_str = str;
				}
			});
			that.compl_data_values.push({
				label : compl_data.label,
				type : type_str
			});
		});
		this.$compl_data_box.find('.params-compl-data-box *').not('legend').remove();
		this.$compl_data_box.find('.params-compl-data-box').append('<div class="form-item" id="' + that.params_compl_data_cont_id + '"></div>');
		$.each(that.i18n.complDataType, function (str,val) {
			$template_compl_data.find('select').append('<option value="' + str + '">' + str + '</option>');
		});
		var $compl_data_list = this.$compl_data_box.find('#' + that.params_compl_data_cont_id).extensibleList ({
			values: that.compl_data_values,
			template: $template_compl_data,
			updateCB : addComplementaryData,
			checkCB : checkComplementaryData,
			removeCB : updateComplementaryDataList,
			orderCB : updateComplementaryDataList,
			delegates : {
				nameList : ["label"],
				onClickCB : complementaryDataEditable
			},
			sortable: {
				status : true,
				items : "li:not(.no-sortable)"
			}
		});
		// remove sortable behavior and deletable behavior for complementary data item not empty.
		if (that.project.complementary_data.length > 0) {
			var max_index_not_empty = 0;
			for (var comp_dataI = 0; comp_dataI < that.project.complementary_data.length; comp_dataI ++ ) {
				if (that.project.complementary_data[comp_dataI].content.length > 0) {
					max_index_not_empty = comp_dataI;
				}
			}
			$.each(that.$compl_data_box.find('ul li') , function (i,e) {
				if (i <= max_index_not_empty) {
					$(e).addClass('no-sortable');
					$(e).find('.item-delete').removeClass('item-delete').addClass('item-not-delete');
				}
			});
		}
		that.$compl_data_box.find('ul').sortable('refresh');
		// ----------------------------------------------------------------
		// groupings
		// ----------------------------------------------------------------
		that.$grouping_tasks_box.find('fieldset > div').remove();
		// ----------------------------------------------------------------
		// grouping tree
		// ----------------------------------------------------------------
		that.grouping_tree_values = [];
		$.each(that.advancedParams.grouping_tasks.grouping_tree, function (i,tree) {
			that.grouping_tree_values.push({
				label : tree
			});
		});
		// initialize extensibleList
		this.$grouping_tasks_box.find('.params-grouping-tree-box').append('<div class="form-item" id="' + that.params_grouping_tree_cont_id + '"></div>');
		var $tree = this.$grouping_tasks_box.find('#' + that.params_grouping_tree_cont_id).extensibleList ({
			values: that.grouping_tree_values,
			template: $('<span><input type="text" name="label"/></span>'),
			updateCB : addGroupingTree,
			checkCB : checkGroupingTree,
			orderCB : updateGroupingTreeList,
			removeCB : updateGroupingTreeList,
			delegates : {
				nameList : ["label"],
				onClickCB : treeEditable
			},
			sortable: {
				status : true,
				items : "li:not(.no-sortable)"
			}
		});
		// update lock / unlock tree view list
		if (that.taskList.hasTaskInGroupingTree() === true) {
			that.$grouping_tasks_box.find('.params-grouping-tree-box').find('.el-container').hide();
			that.$grouping_tasks_box.find('.params-grouping-tree-box legend span').removeClass('h_unlocked').addClass('h_locked');
			that.$grouping_tasks_box.find('.params-grouping-tree-box').find('ul').sortable('destroy');
			that.$grouping_tasks_box.find('.params-grouping-tree-box').find('span.item-delete').remove();
		}
		else {
			that.$grouping_tasks_box.find('.params-grouping-tree-box legend span').removeClass('h_locked').addClass('h_unlocked');
		}
		// ----------------------------------------------------------------
		// grouping list
		// ----------------------------------------------------------------
		that.grouping_list_values = [];
		$.each(that.advancedParams.grouping_tasks.grouping_list, function (i,list) {
			that.grouping_list_values.push({
				label : list.label
			});
		});
		this.$grouping_tasks_box.find('.params-grouping-list-box').append('<div class="form-item" id="' + that.params_grouping_list_cont_id + '"></div>');
		this.$list = that.$grouping_tasks_box.find('#' + that.params_grouping_list_cont_id).extensibleList ({
			values: that.grouping_list_values,
			template: $('<span><input type="text" name="label"/></span>'),
			updateCB : addGroupingList,
			checkCB : checkGroupingList,
			removeCB : updateGroupingList,
			orderCB : updateGroupingList,
			delegates : {
				nameList : ["label"],
				onClickCB : groupingListEditable
			},
			sortable: {
				status : true,
				items : "li:not(.no-sortable)"
			}
		});
		// update lock / unlock list view list
		if (that.taskList.hasTaskInGroupingList() === true) {
			that.$grouping_tasks_box.find('.params-grouping-list-box').find('.el-container').hide();
			that.$grouping_tasks_box.find('.params-grouping-list-box legend span').removeClass('h_unlocked').addClass('h_locked');
			that.$grouping_tasks_box.find('.params-grouping-list-box').find('ul').sortable('destroy');
			that.$grouping_tasks_box.find('.params-grouping-list-box').find('span.item-delete').remove();
		}
		else {
			that.$grouping_tasks_box.find('.params-grouping-list-box legend span').removeClass('h_locked').addClass('h_unlocked');
		}
		// model
		if (that.project.model_id === null) {
			that.$update_model_btn.attr("disabled", "disabled").addClass("disabled");
			that.$model_label_input.val('');
		}
		else {
			that.$update_model_btn.removeAttr("disabled").removeClass("disabled");
			that.$model_label_input.val(that.modelList.model_list_by_rowid[that.project.model_id].label);
			that.$model_box.data('model_id', that.project.model_id);
		}
		// vocabulary
		that.$vocabulary.find('.params-vocabulary-table tbody').children().remove();
		$.each(that.advancedParams.vocabulary, function (i,voc) {
			that.$vocabulary.find('.params-vocabulary-table tbody').append('<tr><td>' + voc.term + '</td><td></td><td></td></tr>');
			$.each(voc.value, function (key,val) {
				$('<p><label>' + key + '</label><input type="text" value="' + val[0] + '"/></p>').appendTo($('.params-vocabulary-table tbody tr:last').find('td').eq(1));
				$('<p><label>' + key + '</label><input type="text" value="' + val[1] + '"/></p>').appendTo($('.params-vocabulary-table tbody tr:last').find('td').eq(2));
			});
		});
		that.insertVocabulary(that.advancedParams.vocabulary);
		that.$cont.find('.error').remove();
	} // END of refresh
	// =============================================================================================================
	// INIT
	// =============================================================================================================
	
	this.$cont = that.$elem.find('.advanced-params-editor');
	this.$cont.html('');
	// ----------------------------------------------------------------
	// advanced params buttons controller
	// ----------------------------------------------------------------
	this.$adv_params_btn_cont = $('<div class="buttons-control-cont"></div>').appendTo(that.$cont);
	// ----------------------------------------------------------------
	// APPLY behavior containing : 
	// on vocabulary updating success, private method checkAdvParams is called with a callback succes like : onApply.
	this.apply_btn = $('<input type="button" class="button adv-params-apply-btn" value="' + that.i18n.apply + '"/>')
		.appendTo(that.$adv_params_btn_cont)
		.click(function (ev) {
			that.$cont.find('div.error').remove();
			var $error_cont = $('<div class="error error-advanced-params"></div>').prependTo(that.$cont);
			var params = {};
			var onSuccess = function () {
				checkAdvParams(params, onApply, $error_cont);
			};
			var onError = function (errorList) {
				$.each(errorList, function (i,text) {
					$error_cont.append('<p class="error">' + that.i18n[text] + '</p>');
				});
			};
			params.advanced_params = serializeAdvancedParams ();
			that.advancedParams.setVocabulary(params.advanced_params.vocabulary,onSuccess,onError);
		}
	);
	// ----------------------------------------------------------------
	// CANCEL behavior
	this.cancel_btn = $('<input type="button" class="button cancel adv-params-cancel-btn" value="' + that.i18n.cancel + '"/>')
		.appendTo(that.$adv_params_btn_cont)
		.click(function (ev) {
			that.changeMustBeSaveFlag (false);
			that.hideWindow();
		}
	);
	// ----------------------------------------------------------------
	// model managing
	// ----------------------------------------------------------------
	this.$model_box = $(
		'<form class="form params-model">'
			+ '<fieldset class="model-control-box">'
				+ '<legend>' + that.i18n.model + '</legend>'
				+ '<label for="' + that.model_label_id + '">' + that.i18n.label + ' :</label>'
			+ '</fieldset>'
		+ '</form>'
	).appendTo(that.$cont);
	this.$model_label_input = $('<input id="' + that.model_label_id + '" type="text" name="label" value=""/>')
		.appendTo(that.$model_box.find('.model-control-box'))
		.click(function () {
			that.$model_box.find('.error').remove();
		}
	);
	// ----------------------------------------------------------------
	// CREATE MODEL behavior
	this.$create_model_btn = $('<input type="button" class="button create-model-btn" value="' + that.i18n.createNewModel + '"/>')
		.appendTo(that.$model_box.find('.model-control-box'))
		.click(function () {
			that.$cont.find('div.error').remove();
			var $error_cont = $('<div class="error error-model"></div>').prependTo(that.$model_box);
			var params = {};
			params.label = $.trim(that.$model_label_input.val());
			params.advanced_params = serializeAdvancedParams ();
			params.project_id = that.project.project_id;
			checkAdvParams(params, createModel, $error_cont);
		}
	);
	// ----------------------------------------------------------------
	// UPATE MODEL behavior
	this.$update_model_btn = $('<input type="button" class="button update-model-btn" value="' + that.i18n.updateModel + '"/>')
		.appendTo(that.$model_box.find('.model-control-box'))
		.click(function () {
			that.$cont.find('.error').remove();
			var $error_cont = $('<div class="error error-model"></div>').prependTo(that.$model_box);
			var params = {};
			params.label = $.trim(that.$model_label_input.val());
			params.advanced_params = serializeAdvancedParams ();
			params.project_id = that.project.project_id;
			params.model_id = that.$model_box.data('model_id');
			checkAdvParams(params, setModel, $error_cont);
		}
	);

	// ----------------------------------------------------------------
	// advanced parameters managing
	// ----------------------------------------------------------------
	// result status
	this.$result_box = $('<form class="form params-result-form"><fieldset class="params-result-box"><legend>' + that.i18n.title_result_status + '</legend></fieldset></form>')
		.appendTo(that.$cont)
		.delegate('input','click', function () {
			that.$result_box.find('.error').remove();
		});
	// task status
	this.$task_status_box = $('<form class="form params-task-status-form"><fieldset class="params-task-status-box"><legend>' + that.i18n.title_task_status + '</legend></fieldset></form>')
		.appendTo(that.$cont)
		.delegate('input','click', function () {
			that.$task_status_box.find('.error').remove();
		});
	// project status
	this.$project_status_box = $('<form class="form params-project-status-form"><fieldset class="params-project-status-box"><legend>' + that.i18n.title_project_status + '</legend></fieldset></form>')
		.appendTo(that.$cont)
		.delegate('input','click', function () {
			that.$project_status_box.find('.error').remove();
		});
	// complementary data
	this.$compl_data_box = $('<form class="form params-compl-data-form"><fieldset class="params-compl-data-box"><legend>' + that.i18n.title_compl_data_status + '</legend></fieldset></form>')
		.appendTo(that.$cont)
		.delegate('input','click', function () {
			that.$compl_data_box.find('.error').remove();
		});
	// grouping task
	this.$grouping_tasks_box = $(
		'<form class="form params-grouping-form">'
			+ '<fieldset>'
				+ '<legend>' + that.i18n.title_grouping_tasks + '</legend>'
				+ '<fieldset class="params-grouping-tree-box"><legend>' + that.i18n.title_groupingTree + ' <span class="h_unlocked"></span></legend></fieldset>'
				+ '<fieldset class="params-grouping-list-box"><legend>' + that.i18n.title_groupingList + ' <span class="h_unlocked"></span></legend></fieldset>'
			+ '</fieldset>'
		+'</form>'
	).appendTo(that.$cont).delegate('input','click', function () {
		that.$grouping_tasks_box.find('.error').remove();
	});
	this.$grouping_tasks_box.find('.params-grouping-tree-box').delegate('input','click', function () {
		that.$grouping_tasks_box.find('.params-grouping-tree-box .error').remove();
	});
	// vocabulary
	this.$vocabulary = $(
		'<form class="form params-vocabulary-form">'
			+ '<fieldset class="params-vocabulary-box">'
				+ '<legend>' + that.i18n.title_vocabulary + '</legend>'
				+ '<table class="params-vocabulary-table" class="ui-sortable">'
					+ '<thead><tr><th></th><th>' + that.i18n.singular + '</th><th>' + that.i18n.plural + '</th></tr></thead>'
					+ '<tbody></tbody>'
				+ '</table>'
			+ '</fieldset>'
		+ '</form>'
	).appendTo(that.$cont).delegate('*','click', function () {
		that.$vocabulary.find('.error').remove();
	});
} // End of Object AdvancedParamsEditor
