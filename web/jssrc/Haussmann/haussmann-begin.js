// -----------------------------------------------------------------------------
// Haussmann header file to buils effective javascript file
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// Haussmann is the project manager application for Mioga2 Project
//     It is construct as a jQuery plugin
//
//     Documentation for methods is inserted as comment
// -----------------------------------------------------------------------------


(function ($) {

var defaults = {
	i18n : {
			// fields
			"hour" 				: "hour (s)",
			"hours"				: "Hours",
			"date"				: "Date",
			"dates"				: "Dates",
			"plural"			: "Plural",
			"singular"			: "Singular",
			"informations"		: "Information",
			"history" 			: "History",
			// app fields
			"firstname"			: "Firstname",
			"lastname"			: "Lastname",
			"email"				: "Email",
			"label"				: "Label",
			"description"		: "Description",
			"workload"			: "Workload",
			"dirOfProject" 		: "Directory of project",
			"status"			: "Status",
			"comment"			: "Comment",
			"progress"			: "Progress",
			"tags"				: "Tags",
			"model"				: "Model",
			"resultList"		: "Result list",
			"task_list" 		: "Task list",
			"budget"			: "Budget",
			"notes" 			: "Notes",
			"fineblanking"		: "Fineblanking",
			"advancedParameters" : "Advanced parameters",
			"complementaryData" : "Complementary data",
			"started_date"		: "Started date",
			"planned_date"		: "Planned date",
			"p_start"			: "Work start",
			"p_end"				: "Work end",
			"ended_date"		: "Ended date",
			"result"			: "<span class=\"haussmannVoc_0_0\"></span>",
			"use_result"		: "Use result",
			"color" 			: "Color",
			"bgColor" 			: "Background color",
			// description
			"groupingAffectation" : "Grouping task affectation",
			"labelOfNewProject" : "Label of new Project",
			"searchProject" 	: "Search project",
			"groupingByTree" 	: "Grouping by tree",
			"complDataType_multi" : "Multiline text",
			"complDataType" : {
				"simple text" : 0,
				"multiline text" : 1
			},
			"viewsList"					: "Views list",
			"delegation"				: "Delegation",
			"affectation_person" 		: "Affectation person",
			"projectFiles"				: "Project files",
			"title_result_status" 		: "Definition of" + ' <span class="haussmannVoc_0_0"></span> ' + " status",
			"title_task_status"			: "Definition of task status",
			"title_project_status"		: "Definition of project status",
			"title_compl_data_status"	: "Definition of complementary data status",
			"title_vocabulary"			: "Vocabulary",
			"title_grouping_tasks"		: "Tasks grouping",
			"title_groupingTree"		: "Structure",
			"title_groupingList"		: "List",
			// status
			"All"						: "All",
			"unaffected" 				: "Unaffected",
			"not_indicated"				: "Not indicated",
			"none"						: "None",
			"New"						: "New",
			"not_affected" 				: "N / A",
			"not_delegated" 			: "Not delegated",
			"undefined_status"			: "Undefined status",
			"filterByStatus"			: "Filter By status",
			"end" 						: "End",
			"checked"					: 'Checked',
			"validated"					: "Validated",
			"edit_mod" 					: "Edit mode",
			"read_mod" 					: "Read mode",
			// basic actions
			"close"						: "Close",
			"save" 						: "Save",
			"apply" 					: "Apply",
			"cancel"					: "Cancel",
			"edit"						: "Edit",
			"create"					: "Create",
			"createAndEdit" 			: "Create and edit",
			"saveAndContinue" 			: "Save and continue",
			"destroy" 					: "Delete",
			"removeItem" 				: "Remove item",
			"remove" 					: "Delete",
			"select_all"				: "Select all",
			"select_none"				: "Select none",
			"choose"					: "Choose",
			"add" 						: "Add",
			// app actions
			"changeProject" 			: "Change project",
			"createProject"				: "Create a new project",
			"editProject"				: "Edit project",
			"edit_task"					: "Edit task",
			"delete_task"				: "Delete task",
			"createNote"				: "Create note",
			"createNewModel"			: "Create new model",
			"updateModel"				: "Update model",
			"edit_result"				: "Edit result",
			"delete_result"				: "Delete result",
			"create_fine_blanking" 		: "Create fineblanking",
			"edit_fine_blanking" 		: "Edit fineblanking",
			"edit_task_note" 			: "Edit task note",
			"edit_result_note" 			: "Edit result note",
			"choose_personn" 			: "Choose person",
			"add_attendees" 			: "Add attendees",
			"comment_of"				: "Comment of",
			"choose_project_dir"		: "Choose directory of files project",
			"add_task" 					: "Add task",
			"add_result" 				: "Add result",
			"editGroupingResult" 		: "Edit grouping result",
			"editGroupingList"			: "Edit grouping list",
			"removeAttendee"			: "Remove attendee",
			// navigation
			"showAll" 					: "Show all",
			"hideAll" 					: "Hide all",
			"hide"						: "Hide",
			"show"						: "Show",
			"back"						: "Back",
			"hide_history" 				: "Hide history",
			"show_history" 				: "Show history",
			"result_view" 				: "Result view",
			"projectList" 				: "Project list",
			"backToProject" 			: "Back to project",
			// information
			"alert" 					: "Alert",
			"permanently_remove_message" : "These items will be permanently deleted and cannot be recovered",
			"remove_message_confirmation" :"Are you sure you want to delete this item ?",
			"saveProjectToContinue" 	: "Project changes must be saved to continue",
			"task_notification_message" : "This task has been modified since your last access",
			"progressBarText"			: "Haussmann initializing, please wait ...",
			"PB_main"					: "Haussmann Main is loading...",
			"confirmSuppressProject"	: "Are you sure you want to destroy this project and all tasks ?",
			"mustBeApplyProject"		: "you must click Apply to save the advanced settings",
			"attendeeCount"	        	: "Attendee count",
			// errors message
			"disallowEmpty"				: "Disallow empty",
			"colorDisallowEmpty" 		: "Color disallowEmpty",
			"bgColorDisallowEmpty" 		: "Background color disallowEmpty",
			"progressDateDisallowEmpty" : "Progress date disallow empty",
			"statusDateDisallowEmpty" 	: "Status date disallow empty",
			"textDisallowEmpty" 		: "Text disallow empty",
			"groupingSubListDisallowEmpty" : "Label of grouping sub list disallow empty",
			"labelDisallowEmpty" 		: "Label disallow empty",
			"groupingListDisallowEmpty" : "Label of grouping list disallow empty",
			"groupingTreeDisallowEmpty" : "Label of grouping tree disallow empty",
			"resultStatusDisallowEmpty" : "Label of result status disallow empty",
			"taskStatusDisallowEmpty" 	: "Label of task status disallow empty",
			"projectStatusDisallowEmpty" : "Label of project status disallow empty",
			"complementaryDataDisallowEmpty" : "Label of complementary data disallow empty",
			"groupingListEmpty" 		: "An entry is required for new grouping list",
			"labelRequired" 			: "Label is required",
			"errorDeleteProject" 		: "This project is used by other applications and cannot be destroyed",
			"resultStatusAlreadyExists" : "A result status with a same name already exists",
			"taskStatusAlreadyExists"	: "A task status with a same name already exists",
			"projectStatusAlreadyExists" : "A project status with a same name already exists",
			"complementaryDataAlreadyExists" : "A complementary data with a same name already exists",
			"groupingTreeAlreadyExists" : "A grouping tree with a same name already exists",
			"groupingListAlreadyExists" : "A grouping list with a same name already exists",
			"groupingSubListAlreadyExists" : "A grouping sub list with a same name already exists",
			"projectAlreadyExists" 		: "A project with a same name already exists",
			"resultAlreadyExists" 		: "A result with a same name already exists",
			"taskAlreadyExists"			: "A project task with a same name already exists",
			"modelAlreadyExists" 		: "A model with a same name already exists",
			"mustBePosNum" 				: "Must be positif numeric",
			"mustBeSubList" 			: "Groupings list are needed to one or more sub list",
			"workloadMustBeNum" 		: "Workload value must be numeric",
			"budgetMustBeNum" 			: "Budget value must be numeric",
			"progress_percent_error" 	: "Progress must be a number between 0 and 100",
			"all_field_of_vocabulary_must_be_filled" : "All fiels of vocabulary must be filled",
			"errorStartedPlanned"		: "Planned date must be greater than the started date",
			"errorStartedEnded" 		: "Ended date must be greater than the started date",
			"dateRequired"				: "Date is required",
			"dateInvalid" 				: "Invalid date",
			"progressDateInvalid" 		: 'Progress date is invalid',
			"pstartInvalid" 			: 'Planned start is an invalid date',
			"pendInvalid" 				: 'Planned end is an invalid date',
			"progressDateRequired" 		: "A progress date is required",
			"progressValueRequired" 	: "A progress value is required",
			"statusDateRequired" 		: "A status date is required",
			"plannedDateRequired" 		: "Planned date is required",
			"startedDateRequired" 		: "Started date is required",
			"pstartMinToDay" 			: "Planned start must be to day or later",
			"pendMinToDay" 				: "Planned end must be to day or later",
			"pendMinPstart"				: "Planned end must be as to planned start or later",
			"errorPstartPend" 			: "Planned end must be later than planned start",
			"baseDirNotFound" 			: "Directory is not found",
			"noExistingProject" 		: "There is no existing project",
			"rightsInsufficient" 		: "Rights are insufficient",
			"rightsInsufficientForAccess" : "Your rights are insufficient to access this content",
			"baseDirNotAllowed" 		: "This directory is not allowed",
			"badColorFormat" 			: "Bad color format",
			"badBgColorFormat" 			: "Bad background color format",
			// success message
			"successSaveDelegation" 	: "Task delegation is updated with success",
			"successSaveAffectation" 	: "Task affectation is updated with success",
			"successSaveInformation" 	: "Informations is updated with success",
			"successSaveStatus" 		: "Task status is updated with success",
			"successCreateProgressItem" : "Progress item created with success",
			"successCreateStatusItem" 	: "Status item created with success",
			"successUpdateProject" 		: "Project is updated with success",
			"successCreateStatusItemTmp": "Status item temporarily created successfully",
			"model_created_success" 	: "Model created with success but the advanced parameters are not applied to the project",
			"model_updated_success" 	: "Model updated with success but the advanced parameters are not applied to the project",
			// attendee list
			"add_attendee_label"		: "Add an attendee",
			// GANTT
			monthNames					:["January","February","March","April","May","June", "July","August","September","October","November","December"],
			backwardAllLabel 			:"backward N week",
			backwardLabel 				:"backward one week",
			forwardLabel 				:"forward one week",
			forwardAllLabel 			:"forward N week",
			zoomInLabel 				:"Zoom in",
			zoomOutLabel 				:"Zoom out",
			todayLabel 					:"Today",
			configurationText 			:"Configuration",
			showDetailsTitle			:"Details for",
			closeLabel					:"Close",
			noTasksText					:"No tasks for this day",
			weekLabel					:"Week",
			weeksLabel					:"Weeks",
			hideTimeline 				: "Hide timeline",
			showTimeline 				: "Show timeline",
			display 					: "Display",
			detail 						: "Detail",
			display_filter 				: "Display filter",
			all_projects				: 'All projects',
			milestone					: "Milestone",
			ganttView					: "Gantt view",
			timelineView				: "Timeline"
		}
	};

// ========================================================
// Effective plugin declaration with methods abstraction
// Usage :
//     <div id="haussmann"></div>
//
//     $('#haussmann').haussmann(options);
// ========================================================
$.fn.haussmann = function(options) {
	// method call, get existing Haussmann Object and apply method
	// if method not returns result we return this for chainability
	if (typeof options === 'string') {
		var args = Array.prototype.slice.call(arguments, 1);
		this.each(function() {
			var haussmann = $.data(this, 'haussmann');
			if (haussmann && $.isFunction(haussmann[options])) {
				var res = haussmann[options].apply(haussmann, args);
				if (res !== undefined) {
					return res;
				}
			}
			else {
				$.error( 'Method ' +  options + ' does not exist on jQuery.haussmann' );
			}
		});
	}
	else if (typeof options === 'object') {
		options =  $.extend(true, {}, defaults, options);

		this.each(function(i, item) {
			var $elem = $(item);
			var haussmann = new Haussmann(item, options);
			$(item).data('haussmann', haussmann);
		});
	}
	else {
		$.error( 'Haussmann must not be there ... ');
	}
	return this;
};

