// =============================================================================
/**
Create and anim a progress bar for application initialization. Must be hide at the end of process.

    var options = {
        i18n : { displayText: "some text to display with progress bar" }
    };

	this.appPB = new AppProgressBar(this.elem, options);
	this.appPB.setValue(0);
	....
	this.appPB.setValue(10);
	....


@class AppProgressBar
@constructor
@param {Object} elem JQuery element parent of progress bar
@param {Object} options
**/
// =============================================================================
function AppProgressBar(elem, options) {

	this.elem = elem; // parent element
	this.i18n = options.i18n;

	// Create the DOM elements
	var that = this; // For callback reference to current object
	this.$pbcont = $('<div class="app-progressbar-cont"></div>').appendTo(this.elem);
	$('<h1>'+this.i18n.displayText+'</h1>').appendTo(this.$pbcont);
	this.$pb = $('<div class="app-progressbar"></div>').progressbar({value : 0}).appendTo(this.$pbcont);

	/**
	Set the value for the progress bar.

	@method setValue
	@param integer progress value
	**/
	// --------------------------------------------------------
	this.setValue = setValue;
	function setValue(val) {
		this.$pb.progressbar("value", val);
	}
}
