// =============================================================================
/**
@description Create a TaskNote object.

    var options = {
        haussmann : { .... }
    };

@class TaskNote
@constructor
**/
// =============================================================================
function TaskNote (options) {
	"use strict";
	MIOGA.debug.TaskNote = 0;
	MIOGA.logDebug('TaskNote', 1, ' TaskNote loaded');

	var that = this;
	this.task_note_id = options.task_note_id,
	this.project_id = options.project_id;
	this.task_id = options.task_id;
	this.text = options.text;
	this.created_str = options.created_str;
	this.created = new Date();
	this.created.parseMiogaDate(options.created, options.created);
	this.modified = new Date();
	this.modified.parseMiogaDate(options.modified, options.modified);
	this.owner_id = options.owner_id;
	this.firstname = options.firstname;
	this.lastname = options.lastname;
	this.email = options.email;
	this.project_owner_id = options.project_owner_id;
	this.project_owner_firstname = options.project_owner_firstname;
	this.project_owner_lastname = options.project_owner_lastname;
	this.project_owner_email = options.project_owner_email;
	this.user_is_proj_owner = options.user_is_proj_owner;
	this.user_is_task_delegated = options.user_is_task_delegated;

	// publics method declaration
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;
	this.isEmpty = isEmpty;
	this.setTaskNote = setTaskNote;

	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================================================
	// PUBLICS METHODS
	// ===================================================================================================================
	/**
	@description Public method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('TaskNote', 1, 'TaskNote::addRefreshCB - loaded');
		this.refreshCB.push( { cb : cb, context : context });
	}
	/**
	@description Public method that loads each callback.
	@method callRefreshCB
	**/
	function callRefreshCB() {
		MIOGA.logDebug('TaskNote', 1, 'TaskNote::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that checks if arguments length is empty. If this is it, that returns true, else returns false.
	@method isEmpty
	@param Object elem can be all type. This is the element that will be tested.
	@param String error_str that is a string value that represents the error message if tested element is empty.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isEmpty (elem, error_str, global_result) {
		MIOGA.logDebug('TaskNote', 1, 'TaskNote::isEmpty - loaded');
		var result = false;
		if (elem.length === 0) {
			if (global_result) {
				global_result.status = "error";
				global_result.error.push(error_str);
			}
			result = true;
		}
		return result;
	}
	/**
	@description Public method that updates task note
	@method setTaskNote
	@param Object args that is contains pair key / value : text
	@param Function cb that is callback called on set task note success
	**/
	function setTaskNote(args, cb) {
		MIOGA.logDebug('TaskNote', 1, 'TaskNote::setTaskNote - loaded');
		var params = {
			task_note_id : that.task_note_id,
			task_id : that.task_id,
			project_id : that.project_id
		};
		if (args.text) {
			params.text = args.text;
		}
		else {
			params.text = that.text;
		}
		$.ajax({
			url : "SetTaskNote.json",
			data : params,
			dataType:'json',
			type : "POST",
			async : false,
			success : function(response){
				if (response.status === "OK") {
					that.modified = new Date();
					that.modified.parseMiogaDate(response.data.modified, response.data.modified);
					that.text = response.data.text;
					if (cb !== undefined) {
						cb();
					}
					else {
						that.callRefreshCB();
					}
				}
				else {
					MIOGA.logError ("TaskNote. setTaskNote AJAX request returned KO status", false);
					that.callRefreshCB();
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("TaskNote. setTaskNote AJAX request returned an error : " + err, true);
			}
		});
	}

	if (
		this.project_id === undefined || this.task_id === undefined || 
		this.task_note_id === undefined || this.text === undefined || 
		this.created === undefined || this.modified === undefined || 
		this.owner_id === undefined || this.firstname === undefined || 
		this.lastname === undefined || this.email === undefined || 
		this.project_owner_id === undefined || 	this.project_owner_firstname === undefined || 
		this.project_owner_lastname === undefined || this.project_owner_email === undefined
		) {
		MIOGA.logError ("TaskNote. Object can not be initialized.", false);
		return undefined;
	}
} // End TaskNote object
