// =============================================================================
/**
Create a ResultEditor object.

    var options = {
        i18n : { .... }
    };

	this.resultEditor = new ResultEditor($elem, options);

@class ResultEditor
@constructor
@param {Object} $elem JQuery that will be contains this editor
@param {Object} options
**/
// =============================================================================
function ResultEditor ($elem, options) {
	"use strict";
	MIOGA.debug.ResultEditor = 0;
	MIOGA.logDebug('ResultEditor', 1, ' ResultEditor loaded');
	var that = this;

	this.haussmann = options.haussmann;
	this.$elem = $elem;
	this.i18n = options.i18n;

	this.generateHash = options.haussmann.generateHash;
	this.insertVocabulary = options.haussmann.insertVocabulary;

	this.$cont = that.$elem.find('.result-editor');
	// ---------------------------------------------------------------------------
	// id generation
	this.information_form_id = MIOGA.generateID("haussmann");
	this.label_id = MIOGA.generateID("haussmann");
	this.budget_id = MIOGA.generateID("haussmann");
	this.pend_id = MIOGA.generateID("haussmann");
	this.tags_id = MIOGA.generateID("haussmann");
	this.status_form_id = MIOGA.generateID("haussmann");
	this.result_note_form_id = MIOGA.generateID("haussmann");
	this.edit_note_form_id = MIOGA.generateID("haussmann");
	this.edit_note_text_id = MIOGA.generateID("haussmann");
	this.checked_id = MIOGA.generateID("haussmann");
	// publics method declaration
	this.showWindow = showWindow;
	this.hideWindow = hideWindow;
	this.createResultNote = createResultNote;
	this.refreshGlobalInfo = refreshGlobalInfo;
	this.refreshStatus = refreshStatus;
	this.refreshResultNote = refreshResultNote;
	this.refresh = refresh;
	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================
	// PRIVATES METHODS
	// ===================================================================================
	/**
	@description Private method that draws a temporarily success message
	@method successMessage
	@param Object $cont that is jQuery element that will be contains a success message
	@param String message that is a success message
	**/
	function successMessage ($cont, message) {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::successMessage - loaded');
		var $message = $('<span class="success-message">' + message + '</span>').appendTo($cont).hide();
		$message.fadeIn(1800, function () {
			$(this).fadeOut(2500, function () {
				$(this).remove();
			});
		});
	}
	// TODO factorize methods to checkItem (...). it is same method to task Editor...
	/**
	@description Private method that checks status item and draws specific error if needed.
	@method checkStatusItem
	@param Object args that represents status item like :
			{
				status : Int - index of status ( optional ),
				date : Object date of status item ( optional )
			}
	@param Function onSuccess that is callback on success checking
	@param Object $error_cont is Jquery element that is container of errors
	**/
	function checkStatusItem (args, onSuccess, $error_cont) {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::checkStatusItem - loaded');
		$error_cont.find('.error').remove();
		var checked = that.result.checkStatusItem (args);
		if (checked.status === "success") {
			onSuccess (args);
		}
		else {
			$.each(checked.error, function (i,text) {
				$error_cont.append('<p class="error">' + that.i18n[text] + '</p>');
			});
		}
	}
	/**
	@description Private method that checks result note and draws specific error if needed.
	@method checkResultNote
	@param Object args that represents task note like :
			{
				text : String
			}
	@param Function onSuccess that is callback on success checking
	@param Object $error_cont is Jquery element that is container of errors
	**/
	function checkResultNote (args, onSuccess, $error_cont) {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::checkResultNote - loaded');
		$error_cont.find('.error').remove();
		var checked = that.resultNoteList.checkResultNote (args);
		if (checked.status === "success") {
			onSuccess (args);
		}
		else {
			$.each(checked.error, function (i,text) {
				$error_cont.append('<p class="error">' + that.i18n[text] + '</p>');
			});
		}
	}
	/**
	@description Private method that serializes result informations and returns it.
	@method serializeGlobalInfo
	**/
	function serializeGlobalInfo () {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::serializeGlobalInfo - loaded');
		var $form = that.$global_info.find('#' + that.information_form_id);
		var attr = {};
		attr['label'] = $.trim($form.find('[name="label"]').val());
		attr['budget'] = $.trim($form.find('[name="budget"]').val());
		if ($form.find('[name="pend"]').val().length > 0) {
			attr['pend'] = $.trim($form.find('[name="pend"]').val());
		}
		else {
			attr['pend'] = null;
		}
		if (that.$global_info.find('#' + that.checked_id).attr('checked') === "checked" ) {
			attr['checked'] = true;
		}
		else {
			attr['checked'] = false;
		}
		
		attr['tags'] = [];
		$form.find('#' + that.tags_id + " span.tag").each(function () {
			attr.tags.push($.trim($(this).text()));
		});
		return attr;
	}
	/**
	@description Private method that adds status item in DOM and adjusts next status entry according to advanced parameters
	@method createStatusItem
	@param Int statusValue is status index
	@param Object date is date of status item
	@param String comment is comment of status item
	**/
	function createStatusItem (statusValue, date, comment) {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::createStatusItem - loaded');
		that.$status_cont.find('span.h_delete').remove();
		that.$status_tbody.find('tr:last').before(
			'<tr class="status-item">'
				+ '<td class="status-item-status" value="' + statusValue + '">' + that.advancedParams.result_status[statusValue] + '</td>'
				+ '<td class="status-item-date">' + date + '</td>'
				+ '<td class="status-item-comment" >' + comment.replace(/\n/g, '<br>') + '</td>'
				+ '<td class="status-item-delete" ><span class="h_delete">&#160;</span></td>'
			+ '</tr>'
		);
		if (that.advancedParams.result_status[(statusValue+1)] !== undefined) {
			that.$status_status.attr('value',(statusValue+1)).text(that.advancedParams.result_status[(statusValue+1)]);
			// determination of minimum date for future creations of status item.
			that.$status_date.datepicker("option", "minDate", date).val('');
			that.$status_comment.val('');
		}
		else {
			that.$status_control_cont.hide();
		}
		if (that.$status_link_cont.find('.show-history').html() === that.i18n.show_history) {
			hideStatusHistory();
		}
	}
	/**
	@description Private method that clears result note edit form
	@method clearEditNoteForm
	**/
	function clearEditNoteForm () {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::clearEditNoteForm - loaded');
		that.$edit_note_text.val('');
	}
	/**
	@description Private method that tests if current status is last possible status.
				It draws read mode link or edit mode link according to result.
				It draws the next status and shows or hide add status container and thead of table according to result.
	@method showEditStatusIfNeeded
	**/
	function showEditStatusIfNeeded () {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::showEditStatusIfNeeded - loaded');
		that.$status_fieldset.find('.error').remove();
		var adv_status_index = that.advancedParams.result_status.length-1;
		var result_status_index = that.result.status.length-1;
		that.$status_link_cont.find('.edit-mod').html(that.i18n.read_mod);
		if (result_status_index < adv_status_index) {
			that.$status_status.attr('value',(result_status_index+1)).text(that.advancedParams.result_status[result_status_index+1]);
			that.$status_link_cont.find('.edit-mod').html(that.i18n.read_mod);
			that.$status_control_cont.show();
			that.$status_fieldset.find('thead').show();
		}
		else {
			that.$status_control_cont.hide();
			if (that.result.current_status !== null) {
				that.$status_fieldset.find('thead').show();
			}
			else {
				that.$status_fieldset.find('thead').hide();
			}
		}
	}
	/**
	@description Private method that draws read mode link of status and shows or hides thead of table if needed.
	@method hideStatusEdit
	**/
	function hideStatusEdit () {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::hideStatusEdit - loaded');
		that.$status_fieldset.find('.error').remove();
		that.$status_link_cont.find('.edit-mod').html(that.i18n.edit_mod);
		that.$status_control_cont.hide();
		if (that.result.current_status !== null) {
			that.$status_fieldset.find('thead').show();
		}
		else {
			that.$status_fieldset.find('thead').hide();
		}
	}
	/**
	@description Private method that shows only last status and adapts history status link
	@method hideStatusEdit
	**/
	function hideStatusHistory () {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::hideStatusHistory - loaded');
		that.$status_link_cont.find('.show-history').html(that.i18n.show_history);
		that.$status_tbody.find('.status-item').hide();
		that.$status_tbody.find('.status-item').last().show();
	}
	/**
	@description Private method that show or hides Jquery element if list is get in parameter is empty or not.
	@method historyTogglable
	@param Array list is a list wish length will be checked
	@param Object $link is a Jquery element that will be showed or hided according to length of list
	**/
	function historyTogglable (list, $link) {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::historyTogglable - loaded');
		if (list.length > 1) {
			$link.show();
		}
		else {
			$link.hide();
		}
	}
	// ===================================================================================
	// PUBLICS METHODS
	// ===================================================================================
	/**
	@description Public method that shows this window and hides other. It calls refresh method. 
	@method showWindow
	**/
	function showWindow() {
		MIOGA.logDebug("ResultEditor::showWindow", 1, 'showWindow loaded');
			that.$cont.show();
			that.refresh();
	}
	/**
	@description Public method that hides this window.
	@method showWindow
	**/
	function hideWindow() {
		MIOGA.logDebug("ResultEditor::hideWindow", 1, 'hideWindow loaded');
		that.$cont.hide();
	}
	/**
	@description Public method that makes HTML render of result note and insert this in DOM
	@method createResultNote
	@param Object resultNote
	**/
	function createResultNote (resultNote) {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::createResultNote - loaded');
		var created_str = that.haussmann.getStrDateFromObj(resultNote.created);
		var $resultNote = $(
				'<div class="result-note-item sbox">'
					+ '<h3 class="result-note-title">'
						+ '<div class="result-note-item-info"><span>' + resultNote.firstname + ' ' + resultNote.lastname + '   ' + created_str + '</span></div>'
					+ '</h3>'
					+ '<div class="result-note-text"><p>' + resultNote.text.replace(/\n/g, '<br>') + '</p></div>'
				+ '</div>').data({
			user_id : resultNote.owner_id,
			result_note_id : resultNote.result_note_id,
			text : resultNote.text
		});
		// check rigths of current user.
		if (mioga_context.user.email === resultNote.email || that.project.user_is_proj_owner === true || userRights.Anim === 1) {
			$resultNote.find('h3').append('<div class="result-note-edit-link-cont"><span class="h_edit result-note-edit">&#160;</span></div>');
		}
		if (that.project.user_is_proj_owner === true || userRights.Anim === 1) {
			$resultNote.find('h3').append('<div class="result-note-delete"><span class="h_delete">&#160;</span></div>');
		}
		that.$result_note_list.prepend($resultNote);
	}
	// =============================================================================================
	// REFRESH METHODS
	// =============================================================================================
	/**
	@description Public method that refresh result general informations
	@method refreshGlobalInfo
	**/
	function refreshGlobalInfo () {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::refreshGlobalInfo - loaded');
		that.$label_write.val(that.result.label);
		that.$budget_write.val(that.result.budget);
		if (that.result.pend === null) {
			that.$pend_write.val('');
		}
		else {
			
			that.$pend_write.datepicker("setDate" , that.result.pend);
		}
		
		if (that.result.checked === true) {
			that.$checked_write.attr('checked', that.result.checked);
		}
		else {
			that.$checked_write.removeAttr('checked');
		}
		that.$info_tags.tags ({
			available : tags_info.available_tags,
			free : tags_info.free_tags,
			tags : that.result.tags,
			warning :tags_info.tag_does_not_exist
		});
	}
	/**
	@description Public method that refresh result status
	@method refreshStatus
	**/
	function refreshStatus () {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::refreshStatus - loaded');
		showEditStatusIfNeeded ();
		if (that.advancedParams.result_status.length === 0) {
			that.$status_cont.hide();
		}
		else {
			that.$status_cont.show();
			// insert status value in label for next entry status.
			//	insert status items.
			that.$status_tbody.find('.status-item').remove();
			var count_status = that.result.status.length;
			if (that.result.current_status !== null) {
				for (var statusItemStatus = 0; statusItemStatus < count_status ; statusItemStatus ++) {
					createStatusItem (statusItemStatus, that.haussmann.getStrDateFromStr(that.result.status[statusItemStatus].date), that.result.status[statusItemStatus].comment);
				}
				that.$status_fieldset.find('thead').show();
			}
			else {
				that.$status_fieldset.find('thead').hide();
				if (that.project.user_is_proj_owner !== true && userRights.Anim !== 1) {
					that.$status_cont.hide();
				}
				else {
					that.$status_link_cont.find('.show-history').hide();
				}
			}
			hideStatusEdit();
			hideStatusHistory();
			historyTogglable (that.result.status, that.$status_link_cont.find('.show-history'));
			if (that.project.user_is_proj_owner === true || userRights.Anim === 1) {
				that.$status_link_cont.find('.edit-mod').show();
				that.$status_fieldset.find('.h_delete').show();
			}
			else {
				that.$status_link_cont.find('.edit-mod').hide();
				that.$status_fieldset.find('th:last').hide();
				that.$status_fieldset.find('tr td.status-item-delete').hide();
			}
		}
	}
	/**
	@description Public method that refresh result notes
	@method refreshStatus
	**/
	function refreshResultNote () {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::refreshResultNote - loaded');
		that.$result_note_list.children().remove();
		for (var i = 0; i < that.resultNoteList.result_note_list.length ; i++) {
			that.createResultNote (that.resultNoteList.result_note_list[i]);
		}
	}
	/**
	@description Public method that refresh result editor
	@method refresh
	**/
	function refresh () {
		MIOGA.logDebug("ResultEditor", 1, 'ResultEditor::refresh - loaded');
		that.project_id = that.project.project_id;
		that.resultNoteList = that.result.resultNoteList;
		that.advancedParams = that.project.advancedParams;

		that.refreshGlobalInfo ();
		that.refreshStatus ();
		that.refreshResultNote ();
	}
	// ===========================================
	//	INITIALIZATION
	// ===========================================
	that.$cont.html('');
	that.$cont.append('<div class="result-global-action-cont"></div>');
	// ------------------------------------------------------
	// back behavior
	$('<input type="button" class="cancel button result-action-back" value="' + that.i18n.back + '"/>')
	.click(function (ev) {
		history.back();
	}).appendTo(that.$cont.find('.result-global-action-cont'));
	$('<span class="back-result-view">' + that.i18n.result_view + '</span>')
		.click(function () {
			that.haussmann.current_view.type = "result";
			location.hash = that.generateHash("ProjectEditor", that.project_id);
		}).appendTo(that.$cont.find('.result-global-action-cont'));
	// ------------------------------------------------------
	// INIT RESULT INFOMATION
	this.$global_info = $(
		'<div class="result-informations-cont">'
			+ '<form id="' + that.information_form_id + '" class="form">'
				+ '<fieldset class="edit-result-info-cont">'
					+ '<legend>' + that.i18n.informations + '</legend>'
					+ '<div class="form-item label"><label for="' + that.label_id + '">' + that.i18n.label + '</label></div>'
					+ '<div class="form-item pend"><label for="' + that.pend_id + '">' + that.i18n.p_end + '</label></div>'
					+ '<div class="form-item budget"><label for="' + that.budget_id + '">' + that.i18n.budget + '</label></div>'
					+ '<div class="form-item checked"><label for="' + that.checked_id + '">' + that.i18n.checked + '</label></div>'
					+ '<div class="form-item tags"><label for="' + that.tags_id + '">' + that.i18n.tags + '</label></div>'
				+ '</fieldset>'
			+ '</form>'
		+ '</div>'
	).appendTo(that.$cont);

	this.$label_write = $('<input id="' + that.label_id + '" type="text" name="label"/>').appendTo(that.$global_info.find('.label'));
	this.$pend_write = $('<input id="' + that.pend_id + '" type="text" name="pend"/>').appendTo(that.$global_info.find('.pend'));
	
	// behavior datepicker dates planned date
	this.$pend_write
		.datepicker({
			dateFormat : 'yy-mm-dd',
			defaultDate : 0
		})
		.click(function () {
			that.$status_cont.find('.error').remove();
		}
	);
	
	this.$budget_write = $('<input id="' + that.budget_id + '" type="text" name="budget" value=""/>').appendTo(that.$global_info.find('.budget'));
	this.$checked_write = $('<input id="' + that.checked_id + '" type="checkbox" name="checked"/>').appendTo(that.$global_info.find('.checked'));
	this.$info_tags = $('<div id="' + that.tags_id + '"></div>').appendTo(that.$global_info.find('.tags'));
	
	this.$info_actions_cont = $('<div class="global-info-actions-cont"></div>').appendTo(that.$global_info.find('.edit-result-info-cont'));
	this.$apply_btn = $('<input type="button" class="button task-apply-btn" value="' + that.i18n.apply + '"/>').appendTo(that.$info_actions_cont);
	// apply global info
	this.$global_info.delegate('.task-apply-btn', 'click',function (ev) {
		that.$cont.find('.error').remove();
		var $error_cont = $('<div class="error error-global-info"></div>').prependTo(that.$global_info);
		var global_info_attr = serializeGlobalInfo();
		var onError = function (errorList) {
			$.each(errorList, function (i,text) {
				$error_cont.append('<p class="error">' + that.i18n[text] + '</p>');
			});
		};
		var onSuccess = function () {
			successMessage (that.$info_actions_cont, that.i18n.successSaveInformation);
			that.refreshGlobalInfo ();
		};
		that.result.setGlobalInfo(global_info_attr,onSuccess , onError);
	});
	// ------------------------------------------------------
	// INIT RESULT STATUS
	this.$status_cont = $(
		'<div class="result-status-cont" style="display:none;">'
			+ '<form id="' + that.status_form_id + '" class="form">'
				+ '<fieldset class="status-fieldset">'
					+ '<legend>' + that.i18n.status + '</legend>'
					+ '<table class="status-table sbox list">'
						+ '<thead>'
							+ '<tr><th class="header-status-label">' + that.i18n.status + '</th><th class="header-status-date">' + that.i18n.date + '</th><th class="header-status-comment">' + that.i18n.comment + '</th><th class="header-status-delete"></th></tr>'
						+ '</thead>'
						+ '<tbody>'
							+ '<tr class="status-control-cont" style="display:none;">'
								+ '<td><span><span class="h_create">&nbsp;</span><span value="" class="status-input-label"></span></span></td>'
								+ '<td><input type="text" class="status-input-date"/></td><td><textarea value="" class="status-input-comment"></textarea></td><td></td>'
							+ '</tr>'
						+ '</tbody>'
					+ '</table>'
					+ '<div class="status-edit-link-cont">'
						+ '<a href="#" class="edit-mod">' + that.i18n.edit_mod + '</a>'
						+ '<a href="#" class="show-history">' + that.i18n.show_history + '</a>'
					+ '</div>'
				+ '</fieldset>'
			+ '</form>'
		+ '</div>'
	).appendTo(that.$cont);

	this.$status_fieldset = this.$status_cont.find('.status-fieldset');
	this.$status_link_cont = this.$status_fieldset.find('.status-edit-link-cont');
	this.$status_tbody = this.$status_fieldset.find('tbody');
	this.$status_control_cont = this.$status_tbody.find('.status-control-cont');
	this.$status_status = this.$status_tbody.find('.status-input-label');
	this.$status_date = this.$status_tbody.find('.status-input-date');
	this.$status_comment = this.$status_tbody.find('.status-input-comment');
	// edit mod
	this.$status_link_cont.find('.edit-mod').toggle(
		function () {
			showEditStatusIfNeeded ();
			
		},
		function () {
			hideStatusEdit();
		}
	);
	// toggle history visibility
	this.$status_link_cont.find('.show-history').toggle(
		function () {
			$(this).html(that.i18n.hide_history);
			that.$status_tbody.find('.status-item').show();
		},
		function () {
			hideStatusHistory();
		}
	);
	// behavior datepicker dates for status
	this.$status_date
		.datepicker({
			dateFormat : 'yy-mm-dd',
			defaultDate : 0
		})
		.click(function () {
			that.$status_cont.find('.error').remove();
		}
	);
	// behavior create status item
	this.$status_tbody.find('.h_create').click(function () {
		var params = {
			status : parseInt(that.$status_status.attr('value'), 10),
			date : that.$status_date.val(),
			comment : $.trim(that.$status_comment.val().replace(/<br>/g, '\n').replace(/<BR>/g, '\n'))
		};
		that.$status_fieldset.find('.error').remove();
		var onCheckSuccess = function (params) {
			that.result.addStatusItem (params, function () {
				that.result.setStatus(function () {
					createStatusItem (params.status, that.haussmann.getStrDateFromStr(params.date), params.comment);
					historyTogglable (that.result.status, that.$status_link_cont.find('.show-history'));
					successMessage (that.$status_cont, that.i18n.successCreateStatusItem);
				});
			});
		};
		checkStatusItem (params, onCheckSuccess, that.$status_fieldset);
	});
	// delete behavior
	this.$status_tbody.delegate('span.h_delete', 'click', function () {
		var $tr = $(this).parent().parent('tr');
		var status_val = $tr.find('.status-item-status').attr('value');
		var status_label = that.advancedParams.result_status[status_val];
		var onSuccess = function () {
			that.$status_tbody.find('.status-input-label')
				.attr('value',status_val)
				.text(status_label);
			that.$status_comment.val('');
			if ( $tr.prev('tr').length > 0 ) {
				that.$status_date.datepicker("option", "minDate", $tr.prev('tr').find('.status-date').text()).val('');
				$tr.prev('tr').find('.status-item-delete').html('<span class="h_delete"></span>');
			}
			else {
				that.$status_date.datepicker("option", "minDate", null).val('');
				that.$status_fieldset.find('thead').hide();
			}
			$tr.remove();
			if (that.$status_link_cont.find('.edit-mod').html() === that.i18n.read_mod) {
				showEditStatusIfNeeded ();
			}
			if (that.$status_link_cont.find('.show-history').html() === that.i18n.show_history) {
				hideStatusHistory ();
			}
			historyTogglable (that.result.status, that.$status_link_cont.find('.show-history'));
		};
		that.result.deleteLastStatusItem(onSuccess);
	});
	// ------------------------------------------------------
	// INIT RESULT NOTE
	this.$result_note = $(
		'<div class="result-note-cont">'
			+ '<form id="' + that.result_note_form_id + '" class="form">'
				+ '<fieldset class="result-note-fieldset">'
					+ '<legend>' + that.i18n.notes + '</legend>'
					+ '<span class="create-result-note-link">' + that.i18n.createNote + '</span>'
					+ '<div class="result-note-list"></div>'
				+ '</fieldset>'
			+ '</form>'
		+ '</div>'
	).appendTo(that.$cont);
	this.$result_note_list = this.$result_note.find('.result-note-list');
	// form to edit unit result
	this.$dial_note = $('<div class="dial-note"></div>').dialog({
		resizable : false,
		autoOpen : false,
		modal : true,
		width : "550",
		height : "280",
		title : that.i18n.edit_result_note,
		open : function () {
			that.$dial_note.append(that.$form_edit_note);
			that.$edit_note_text.focus();
		}
	});
	this.$form_edit_note = $(
		'<div class="edit-note-form-cont">'
			+'<textarea id="' + that.edit_note_text_id + '"></textarea>'
			+ '<div class="edit-note-actions-cont">'
				+ '<ul class="button_list">'
					+ '<li><input type="button" class="button" value="' + that.i18n.apply + '"/></li>'
					+ '<li><input type="button" class="button cancel" value="' + that.i18n.cancel + '"/></li>'
				+ '</ul>'
			+ '</div>'
		+ '</div>'
	).appendTo(that.$dial_note);
	this.$edit_note_actions_cont = this.$form_edit_note.find('.edit-note-actions-cont');
	this.$edit_note_apply = this.$form_edit_note.find('li:first').find('input:first');
	this.$edit_note_cancel = this.$form_edit_note.find('li:last').find('input:first');
	this.$edit_note_text =  this.$form_edit_note.find('#' + that.edit_note_text_id);
	// cancel behavior
	this.$edit_note_cancel.click(function () {
		that.$dial_note.dialog('close');
		clearEditNoteForm();
	});
	// apply edit dialog
	this.$edit_note_apply.click(function () {
		var note_attr = {
			text : $.trim(that.$edit_note_text.val().replace(/<br>/g, '\n').replace(/<BR>/g, '\n'))
		};
		if (that.$dial_note.data('edit_note_mod') === "create") {
			var onCheckSuccess = function (note_attr) {
				that.resultNoteList.createResultNote(note_attr, function (new_result_note) {
					that.createResultNote (new_result_note);
					that.$dial_note.dialog('close');
					clearEditNoteForm();
				});
			};
			checkResultNote (note_attr, onCheckSuccess, that.$edit_note_actions_cont);
		}
		else if (that.$dial_note.data('edit_note_mod') === "edit") {
			var result_note_id = that.$dial_note.data('result_note_id');
			note_attr.result_note_id = result_note_id;
			var onCheckSuccess = function (note_attr) {
				that.resultNoteList.result_note_list_by_rowid[result_note_id].setResultNote(note_attr, function () {
					that.$dial_note.dialog('close');
					clearEditNoteForm();
					that.refreshResultNote ();
				});
			};
			checkResultNote (note_attr, onCheckSuccess, that.$edit_note_actions_cont);
		}
	});
	// behavior to create new note.
	this.$result_note.find('.create-result-note-link').click(function () {
		clearEditNoteForm();
		that.$dial_note.data('edit_note_mod', 'create');
		that.$dial_note.dialog("open");
	});
	// behavior to edit new note.
	this.$result_note_list.delegate('.result-note-edit', 'click', function () {
		var $result = $(this).parent().parent().parent();
		var result_note_id = $result.data('result_note_id');
		that.$dial_note.data('edit_note_mod', 'edit');
		that.$dial_note.data('result_note_id', result_note_id);
		that.$edit_note_text.val(that.resultNoteList.result_note_list_by_rowid[result_note_id].text);
		that.$dial_note.dialog("open");
	});
	// behavior to delete new note.
	this.$result_note_list.delegate('.result-note-delete span', 'click', function () {
		var $result = $(this).parent().parent().parent();
		var params = {
			result_note_id : $result.data('result_note_id')
		};
		var onSuccess = function () {
			$result.remove();
		};
		that.resultNoteList.deleteResultNote(params, onSuccess);
	});
}// End of resultEditor object.
