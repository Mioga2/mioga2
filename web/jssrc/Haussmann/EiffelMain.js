// =============================================================================
/**
Display the main Eiffel window.

    var options = {
        i18n : { ... }
    };

@class EiffelMain
@constructor
@param {Object} elem JQuery element parent of progress bar
@param {Object} options
**/
// =============================================================================

function EiffelMain(elem, options) {
	var that = this; // For callback reference to current object
	var Defaults = {
		i18n : undefined,
		app : {},
		maxZoom : 5,
		mode : "d", // match d for day, w for week and m for month
		step_mode : { // step to change mode
			"week":5,
			"month":8
		},
		getTaskDetail : function (lines, line_num, offset, cell_num) {
			var start;
			var end;
			var $table = $('<table class="task-list"></table>');
			$.each(lines[line_num].cell_tasks[cell_num], function(n, task) {
				var cell1 = "";
				if (task.allday) {
					cell1 = "&nbsp;";
				}
				else {
					cell1 = task.start.formatHour(false, false) + " - " +  task.end.formatHour(false, false);
				}
				$('<tr>'
						+'<td class="task-hour">'+cell1+'</td>'
						+'<td class="task-subject">'+task.subject+'</td>'
					+'</tr>')
						.appendTo($table);
			});
			return $table;
		},
		$config : function () {
			var config_id = MIOGA.generateID("chronos");
			return $('<a id="'+config_id+'" href="#config">'+that.options.i18n.configurationText+'</a>');
		}
	};
	this.options = $.extend(true, {}, Defaults, options);
	this.elem          = elem;
	this.p_start       = undefined;
	// debug level
	MIOGA.debug.EiffelMain = 0;
	MIOGA.logDebug("EiffelMain", 1, "EiffelMain constructor begin >>>>>>>>>>>>>>>>>>>>> config = ", this.options.app.config);

	// Initialize view dates
	this.p_start = new Date();
	this.p_start = this.p_start.firstDayOfWeek(this.options.app.config.first_day);
	MIOGA.logDebug("EiffelMain", 1, "p_start = " + this.p_start);

	// Initialize graphic view
	this.$cont = $('<div class="eiffel-main"></div>').appendTo(this.elem);
	this.$content = $('<div class="eiffel-content"></div>').appendTo(this.$cont);

	// Planning header
	this.$p_head = $('<div class="p-header" style="position:relative;" ></div>').appendTo(this.$content);
	this.$toolbar = $('<div class="toolbar"></div>').appendTo(this.$p_head);
	// Configuration button
	this.$config_cont = $('<div class="toolbar-item"></div>').appendTo(this.$toolbar);
	if (this.options.app.config.animRight) {
		that.$config_cont.append(that.options.$config () );
	}
	// Backward actions
	this.$back_cont = $('<div class="backward-cont toolbar-item"></div>').appendTo(this.$toolbar);
	$('<span class="tb-button backward-many">'+this.options.i18n.backwardAllLabel+'</span>')
				.click(function(e) {
					MIOGA.logDebug("EiffelMain", 1, "Click on backward all");
					if (that.options.mode === "m") {
						that.p_start.setMonth(that.p_start.getMonth() - that.options.app.config.month_count );
					}
					else {
						// The transition to summer date or winter date can be problematic for handling Date object.
						// This transition is a change in the scale of hour. So, if we add to the current date : x hour or x second or x milisecond for add y days to this current date,
						// there will be one day shift if we do in the transition to summer date or winter date.
						// If time as hour does not matter, change day of date does not cause a problem. If change hour or less for change day date, poblem is possible.
						// example OK  :
						//
						//		var dd = new Date(2013,10,24);
						//		dd.setDate ( dd.getDate() + 5 );
						// ==> dd = 2013-10-29
						//
						//example NOT OK :
						//
						//		var dd = new Date(2013,10,24);
						//		dd.setTime ( dd.getTime() + (86400000 * 5) );
						// ==> dd = 2013-10-28
						
						
						// Le passage à l'heure d'été ou d'hiver peut être problématique dans la manipulation de l'objet Date.
						// Ce passage est un changement à l'échelle de l'heure, donc si on ajoute à une date courante x heure, seconde ou milliseconde représentant y jours de plus,
						// il y aura un jour de décalage si la date correspond au passage à l'heure d'été ou d'hiver.
						// Si l'heure n'importe pas, modifier la date à l'échelle du jour préserve la cohérence. Si on modifie la date à l'échelle de l'heure ou plus précis, il y aura une incohérence.
						// exemple OK  :
						//
						//		var dd = new Date(2013,10,24);
						//		dd.setDate ( dd.getDate() + 5 );
						// ==> dd = 2013-10-29
						//
						//exemple NOT OK :
						//
						//		var dd = new Date(2013,10,24);
						//		dd.setTime ( dd.getTime() + (86400000 * 5) );
						// ==> dd = 2013-10-28
						that.p_start.setDate( that.p_start.getDate() - ( 7 * that.options.app.config.week_count ) );
					}
					refresh.call(that);
				})
				.appendTo(this.$back_cont);
	$('<span class="tb-button backward-one">'+this.options.i18n.backwardLabel+'</span>')
				.click(function(e) {
					MIOGA.logDebug("EiffelMain", 1, "Click on backward");
					if (that.options.mode === "m") {
						that.p_start.setMonth(that.p_start.getMonth() - 1 );
					}
					else {
						that.p_start.setDate( that.p_start.getDate() - 7 );
					}
					refresh.call(that);
				})
				.appendTo(this.$back_cont);

	// Today actions
	this.$today_cont = $('<div class="today-cont toolbar-item"></div>').appendTo(this.$toolbar);
	$('<span class="today" >'+this.options.i18n.todayLabel+'</span>')
				.click(function(e) {
					MIOGA.logDebug("EiffelMain", 1, "Click on today");
					that.p_start = new Date();
					if (that.options.mode === "m") {
						that.p_start.setMonth(that.p_start.getMonth());
					}
					else {
						that.p_start = that.p_start.firstDayOfWeek(that.options.app.config.first_day);
					}
					refresh.call(that);
				})
				.appendTo(this.$today_cont);

	// Forward actions
	this.$forw_cont = $('<div class="forward-cont toolbar-item"></div>').appendTo(this.$toolbar);
	$('<span class="tb-button forward-one" >'+this.options.i18n.forwardLabel+'</span>')
				.click(function(e) {
					MIOGA.logDebug("EiffelMain", 1, "Click on forward");
					if (that.options.mode === "m") {
						that.p_start.setMonth(that.p_start.getMonth() + 1 );
					}
					else {
						that.p_start.setDate( that.p_start.getDate() + 7 );
					}
					refresh.call(that);
				})
				.appendTo(this.$forw_cont);
	$('<span class="tb-button forward-many">'+this.options.i18n.forwardAllLabel+'</span>')
				.click(function(e) {
					MIOGA.logDebug("EiffelMain", 1, "Click on forward all");
					if (that.options.mode === "m") {
						var count_day = new Date(that.p_start.getFullYear(), that.p_start.getMonth()+1, -1).getDate()+1;
						that.p_start.setMonth(that.p_start.getMonth() + that.options.app.config.month_count );
					}
					else {
						that.p_start.setDate( that.p_start.getDate() + (7 * that.options.app.config.week_count ) );
					}
					refresh.call(that);
				})
				.appendTo(this.$forw_cont);

	// Comment
	this.$comment_cont = $('<div class="comment-cont toolbar-item"></div>').appendTo(this.$toolbar);
	// Zoom actions
	this.$zoom_cont = $('<div class="zoom-cont toolbar-item"></div>').appendTo(this.$toolbar);
	$('<span class="tb-button zoom-in" >'+this.options.i18n.zoomInLabel+'</span>')
				.click(function(e) {
					MIOGA.logDebug("EiffelMain", 1, "Click on zoom in");
					if (that.options.maxZoom !== 0 && that.options.app.config.week_count >= that.options.maxZoom ) {
						that.options.app.config.week_count = that.options.maxZoom;
					}
					else {
						if (that.options.mode === "d" || that.options.mode === "w") {
							that.options.app.config.week_count += 1;
							if (that.options.app.config.week_count <= that.options.step_mode["week"]) {
								that.options.mode = "d";
							}
							else if (that.options.app.config.week_count < that.options.step_mode["month"]) {
								that.options.mode = "w";
							}
							else {
								// month mode
								that.options.mode = "m";
								that.options.app.config.month_count = parseInt((that.options.step_mode["month"]+1)/4);
								that.count_days = 0;
								for (var i=0; i < that.options.app.config.month_count; i++) {
									that.count_days = that.count_days + new Date(that.p_start.getFullYear(), that.p_start.getMonth()+1+i, -1).getDate()+1;
								}
							}
						}
						else {
							that.options.app.config.month_count = that.options.app.config.month_count + 1;
							that.options.app.config.week_count = that.options.app.config.week_count + 4;
						}
					}
					refresh.call(that);
				})
				.appendTo(this.$zoom_cont);
	$('<span class="tb-button zoom-out">'+this.options.i18n.zoomOutLabel+'</span>')
				.click(function(e) {
					MIOGA.logDebug("EiffelMain", 1, "Click on zoom out");
					if (that.options.mode === "d" || that.options.mode === "w") {
						that.options.app.config.week_count -= 1;
						if (that.options.app.config.week_count <= 1) {
							that.options.app.config.week_count = 1;
						}
						else if (that.options.app.config.week_count <= that.options.step_mode["week"]) {
							that.options.mode = "d";
							that.p_start.firstDayOfWeek(that.options.app.config.first_day);
						}
						else {
							that.p_start.firstDayOfWeek(that.options.app.config.first_day);
						}
					}
					else if (that.options.mode === "m") {
						that.options.app.config.month_count -= 1;
						that.options.app.config.week_count = that.options.app.config.week_count - 4;
						if (that.options.app.config.month_count < parseInt((that.options.step_mode["month"]+1)/4)) {
							that.options.mode = "w";
							that.p_start = that.p_start.firstDayOfWeek(that.options.app.config.first_day);
							that.options.app.config.week_count = that.options.step_mode["month"]-1;
						}
					}
					else {
						MIOGA.logError("EiffelMain. zoom out event. Invalid mode ( d, w or m ).", true);
					}
					refresh.call(that);
				})
				.appendTo(this.$zoom_cont);

	// Planning
	this.$p_cont = $('<div class="planning-cont"></div>').appendTo(this.$content);
	this.$planning = $('<div class="planning" style="position:relative;" ></div>').appendTo(this.$p_cont);

	this.cont_w = this.$content.width();

	// Dialog to show task details
	this.$details = $('<div class="eiffel-dialog"></div>').appendTo(this.$cont);
	this.$detail_content = $('<div class="details"></div>').appendTo(this.$details);
	this.$detail_buttons = $('<ul class="button_list"></ul>').appendTo(this.$details);
	$('<li><button class="button cancel">'+this.options.i18n.closeLabel+'</button></li>')
						.appendTo(this.$detail_buttons)
						.click(function(e) {
							that.$details.dialog('close');
						});

	this.$details.dialog({ title: this.options.i18n.showDetailsTitle,
			                            autoOpen:false,
										modal:true,
										width:'40em',
										closeOnEscape:true
									});

	MIOGA.logDebug("EiffelMain", 2, "app : ", this.options.app);
	this.options.app.addRefreshCB(refresh, that);

	MIOGA.logDebug("EiffelMain", 1, "constructor end <<<<<<<<<<<<<<<< ");
	// End constructor
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	// --------------------------------------------------------
	// showWindow display and init window
	// --------------------------------------------------------
	this.showWindow = showWindow;
	function showWindow() {
		MIOGA.logDebug("EiffelMain", 1, 'EiffelMain  showWindow ===');
		if (this.currentDialog) {
			this.currentDialog.hide();
		}
		that.$cont.show();
		// this is necessary for draw correctly the first display. It can be resolve with resizing function but not with IE.
		refresh.call(that);
		refresh.call(that);
	}
	// --------------------------------------------------------
	// hideWindow hides the window
	// --------------------------------------------------------
	this.hideWindow = hideWindow;
	function hideWindow() {
		MIOGA.logDebug("EiffelMain", 1, 'EiffelMain  hideWindow ===');
		this.$cont.hide();
	}
	// ========================================================================================
	// Private functions (must not be added to "this")
	// ========================================================================================
	// --------------------------------------------------------
	// drawBackground draw and place the background for planning
	//   1 table for horizontal lines and div to draw in tasks
	//   1 table for vertical lines for each days for the interval
	// --------------------------------------------------------
	function drawBackground() {
		MIOGA.logDebug("EiffelMain", 1, "drawBackground()");

		this.$planning.children().remove();
		this.$comment_cont.children().remove();

		// Format date information for header
		var header_text = "";
		var start_month = this.p_start.getMonth();
		var start_week = this.p_start.getWeek();
		var p_end =  new Date(this.p_start.getFullYear (), this.p_start.getMonth (), this.p_start.getDate () + (7 * this.options.app.config.week_count), 0, 0);
		MIOGA.logDebug("EiffelMain", 1, "p_end = " + p_end + " start_month = " + start_month + ' start_week = ' + start_week);
		var end_month = p_end.getMonth();
		var end_week = p_end.getWeek();
		if (start_month === end_month) {
			header_text = this.options.i18n.monthNames[start_month];
		}
		else {
			header_text = this.options.i18n.monthNames[start_month] + ' - ' + this.options.i18n.monthNames[end_month];
		}
		if (start_week === end_week) {
			header_text += ' ('+this.options.i18n.weekLabel+' : '+start_week+')';
		}
		else {
			header_text += ' ('+this.options.i18n.weeksLabel+' : '+start_week+'-'+end_week+')';
		}

		$('<span>'+header_text+'</span>').appendTo(this.$comment_cont);
		// Get planning width
		MIOGA.logDebug("EiffelMain", 1, "cont_w = " + this.cont_w + " week_count = " + this.options.app.config.week_count);
		var p_width = this.$planning.width();
		MIOGA.logDebug("EiffelMain", 1, "p_width = " + p_width);

		// Draw background with 2 tables : one for horizontal lines and another for vertical lines
		// the horizontal lines set the total heigh for planning function of each entry and task for this entries
		// the vertical lines draws axes for weeks and days.

		// Horizontal planning cells
		var $l;
		var $c;
		$('<div class="h-head"></div>').appendTo(this.$planning); // reserve place for header of week table
		this.$entries = $('<table class="entries-table"></table>').appendTo(this.$planning);
		$.each(this.lines, function (num, item) {
			if ( (item.type === 'U') || (item.type === 'R') ) {
				// Check if line is a project entry. If this is it, make specific CSS class for this line.
				var typeOfLine = "";
				var itemId = "" + item.rowid;
				if (num === 0 || ( num > 0 && that.lines[num-1].type !== "U" && that.lines[num-1].type !== "R") ) {
					typeOfLine = "project-line";
				}
				else if (itemId.indexOf("res_id") !== -1) {
					typeOfLine = "milestone-line";
				}
				else {
				}
				$l = $('<tr class="p-entry ' + typeOfLine + '"></tr>').appendTo(that.$entries);
				$('<th class="entry-head" >'+item.label+'</th>').appendTo($l);
				$c = $('<td class="entry-cell" ></td>')
										.click(function(e) {
											MIOGA.logDebug("EiffelMain", 1, "click on line number = " + num);
											MIOGA.logDebug("EiffelMain", 1, "pos offset left = " + $(this).offset().left);
											MIOGA.logDebug("EiffelMain", 1, "pageX  = " + e.pageX);
											showDetails.call(that, num, (e.pageX - $(this).offset().left));
										})
										.appendTo($l);
				item.$cont = $('<div class="entry-val" style="z-index:1;" >&nbsp;</div>')
										.appendTo($c);
			}
			else {
				$c = $('<tr class="p-sep"></tr>').appendTo(that.$entries);
				$('<td colspan="2"></td>').appendTo($c);
			}
		});

		// Calculate widths for all parts
		MIOGA.logDebug("EiffelMain", 2, "entries width = " + this.$entries.width() + " planning width : " + this.$planning.width());
		var entry_head_w = $('.entry-head').width();  // aproximatly set from CSS
		var week_w = parseInt((p_width - entry_head_w) / (that.options.app.config.week_count));
		var day_w = (p_width - entry_head_w) / ((p_end - this.p_start) / 86400000);

		MIOGA.logDebug("EiffelMain", 2, "1) entry_head_w = " + entry_head_w + " week_w = " + week_w + " day_w = " + day_w);
		week_w = 7 * day_w;
		MIOGA.logDebug("EiffelMain", 2, "2) entry_head_w = " + entry_head_w + " week = " + week_w + " total = " + (entry_head_w + (week_w * this.options.app.config.week_count)));
		this.day_w = day_w;

		$('.entry-head').width(entry_head_w);
		// Set width for fist part of header to put button just on day numbers
		this.$config_cont.width(entry_head_w);

		if (that.options.mode === "m") {
			this.week_width = parseInt( (p_width - entry_head_w));
		}
		else {
			// week grid for vertical lines
			this.week_width = week_w * this.options.app.config.week_count;
		}

		MIOGA.logDebug("EiffelMain", 2, "week_width = " + this.week_width);
		this.$weeks = $('<table class="week-table" style="position:absolute;top:0px;left:'+entry_head_w+'px;width:'+this.week_width+'px;z-index:-1;"></table>').appendTo(this.$planning);
		var html = "";
		var day = new Date(that.p_start);
		var iweek = 0;
		this.cells_per_line = this.options.app.config.week_count * 7;
		// Make HTML header for month. for example:
		//  ____________________________
		// | January | February | ...  |
		// 
		
		if (that.options.mode === "m") {
			this.cells_per_line = 0;
			var count_day_in_month = new Date(day.getFullYear(), day.getMonth()+1, -1).getDate()+1;
			for ( var i=0; i < that.options.app.config.month_count; i++) {
				count_day_in_month = new Date(day.getFullYear(), day.getMonth()+i+1, -1).getDate()+1;
				that.cells_per_line = that.cells_per_line + count_day_in_month;
				
				html += '<td class="month" style="width:'+(count_day_in_month * that.day_w)+'px;">'
				+'<table class="days-table">';
				
				html += '<tr class="day-label" style="display:none;">';
				html += '<th colspan="4" class="day-head" style="border-left-width:1px; border-right-width:1px;"><div><div>' + that.options.i18n.monthNames[((start_month+i) % 12)] + '</div></div></th>';
				html += '</tr>';
				html += '<tr class="day-content">';
				for (var j=0; j < 4; j++) {
					html += '<td class="day-cell"></td>';
				}
				html += '</tr>';
				html += '</table>';
				html += '</td>';
			}
			that.day_w = (p_width - entry_head_w) / that.cells_per_line;
		}
		else if (that.options.mode === "d" || that.options.mode === "w") {
			//
			// |           week 1          |             week 2             |
			// | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 |
			//
			// Show and hide alternativly week header or day header according to step mode.
			//
			while (iweek < that.options.app.config.week_count) {
				var week_number = start_week+iweek;
				if (week_number > 52) {
					week_number = parseInt(week_number % 52);
				}
				html += '<td class="week" style="width:'+week_w+'px;">'
							+'<table class="days-table">';
				html += '<tr class="week-label" style="display:none;"><th class="week-head" colspan="' + 7 + '" style="border-left-width:1px; border-right-width:1px;">' + that.options.i18n.weekLabel + ' ' + (week_number) + '</th></tr>';
				html += '<tr class="day-label" style="display:none;">';
				for (var i = 0; i < 7; i++) {
					html += '<th class="day-head" style="border-left-width:1px; border-right-width:1px;" >'+day.getDate()+'</th>';
					day.setDate(day.getDate() + 1);
				}
				html += '</tr>';
				html += '<tr class="day-content"><td class="day-cell"></td><td class="day-cell"></td><td class="day-cell"></td><td class="day-cell"></td><td class="day-cell"></td><td class="week-end day-cell"></td><td class="week-end day-cell"></td></tr>';
				html += '</table>';
				html += '</td>';
				iweek++;
				MIOGA.logDebug("EiffelMain", 2, 'iweek = ' + iweek);
			}
		}
		else {
			MIOGA.logDebug("EiffelMain", 2, 'unknow mode');
		}

		var $tr = $('<tr>'+html+'</tr>');
		$tr.appendTo(this.$weeks);
		$('.day-head').width(day_w - 2);
		if (that.options.mode === "d" || that.options.mode === "m") {
			$tr.find('.day-label').show();
		}
		else if (that.options.mode === "w") {
			$tr.find('.week-label').show();
		}
		else {
			$tr.find('.month-label').show();
		}
	}
	// --------------------------------------------------------
	// refresh draw window content
	// --------------------------------------------------------
	function refresh() {
		MIOGA.logDebug("EiffelMain", 1, 'refresh');
		var that = this;

		MIOGA.logDebug("EiffelMain", 1, 'changeSize');
		that.cont_w = that.$content.width();
		MIOGA.logDebug("EiffelMain", 1, 'cont_w = ' + that.cont_w);
		that.p_cont_w = that.$p_cont.width();
		MIOGA.logDebug("EiffelMain", 1, 'p_cont_w = ' + that.p_cont_w);
		that.planning_w = that.$planning.width();
		MIOGA.logDebug("EiffelMain", 1, 'planning_w = ' + that.planning_w);

		//Prepare lines to display function of config
		// initialize arrays of rowid for calendars
		this.lines = [];
		var users_rowid = [];
		var res_rowid = [];
		$.each(that.options.app.config.display, function (num, item) {
			var label = "";
			var type;
			var rowid;
			if (item.type === 'user') {
				MIOGA.logDebug("EiffelMain", 2, 'user rowid = ' + item.rowid);
				label = that.options.app.config.users[that.options.app.indexOfUsers[item.rowid]].firstname + ' ' +  that.options.app.config.users[that.options.app.indexOfUsers[item.rowid]].lastname;
				type = 'U';
				rowid = item.rowid;
				users_rowid.push(rowid);
			}
			else if (item.type === 'res') {
				MIOGA.logDebug("EiffelMain", 2, 'resource rowid = ' + item.rowid);
				label = that.options.app.config.resources[that.options.app.indexOfResources[item.rowid]].ident;
				type = 'R';
				rowid = item.rowid;
				res_rowid.push(rowid);
			}
			else {
				type = 'S';
				rowid = undefined;
			}
			that.lines.push( { label : label,
								type: type,
								rowid : rowid});
		});

		drawBackground.call(this);

		// -----------------------------------------------------
		// Get tasks and populate table body
		// -----------------------------------------------------
		if (that.options.mode === "m") {
			that.p_start.setDate(1); 
		}
		var first_day = that.p_start.getDate();
		this.options.app.getTasks(that.p_start, that.options.app.config.week_count, users_rowid, res_rowid, function(tasks, prj_tasks, cals) {
			MIOGA.logDebug("EiffelMain", 1, "getTasks CB");
			MIOGA.logDebug("EiffelMain", 3, "tasks", tasks);
			MIOGA.logDebug("EiffelMain", 2, "cals", cals);
			var cal_idx = {};
			for (var i = 0; i < cals.length; i++) {
				MIOGA.logDebug("EiffelMain", 2, "i = " +i+ " calendar_id = " +cals[i].calendar_id);
				if (!cal_idx.hasOwnProperty(cals[i].calendar_id)) {
					cal_idx[cals[i].calendar_id] = [];
				}
				cal_idx[cals[i].calendar_id].push(i);
			}
			MIOGA.logDebug("EiffelMain", 2, 'cal_idx = ', cal_idx);
			that.entryTasks = { 'U' : {}, 'R' : {} };
			//
			// Organize tasks by item type and rowid to process line by line
			//
			var c_id; // calendar rowid
			var type; // 'U' or 'R'
			var rowid; // rowid of user or resource
			$.each(tasks, function (num, item) {
				c_id = item.calendar_id;
				MIOGA.logDebug("EiffelMain", 2, 'c_id = ' + c_id + ' cal_idx[c_id] = '  +cal_idx[c_id]);
				for (var ic = 0; ic < cal_idx[c_id].length; ic++) {
					MIOGA.logDebug("EiffelMain", 2, 'ic = ' + ic + ' cal_idx[c_id][ic] = '  +cal_idx[c_id][ic]);
					if (cals[cal_idx[c_id][ic]].type === 'user') {
						type = 'U';
					}
					else {
						type = 'R';
					}
					rowid = cals[cal_idx[c_id][ic]].item_id;
					item.start = new Date();
					item.start.parseMiogaDate(item.dstart, item.allday);
					item.end = new Date();
					item.end.parseMiogaDate(item.dend, item.allday);
					// get color
					if (that.options.app.config.act_colors.hasOwnProperty(item.category_id)) {
						item.bgcolor = that.options.app.config.act_colors[item.category_id].bgcolor;
					}
					else {
						//MIOGA.logDebug("EiffelMain", 2, 'no act_colors for category_id = ' + task.category_id);
						item.bgcolor = "#e0e0e0";
					}

					if (! that.entryTasks[type].hasOwnProperty(rowid)) {
						MIOGA.logDebug("EiffelMain", 2, 'entryTasks for type = ' + type + ' rowid = ' + rowid + " doesn't exists");
						that.entryTasks[type][rowid] = [];
					}
					that.entryTasks[type][rowid].push(item);
				}
			});
			// Add project tasks
			//
			$.each(prj_tasks, function (num, item) {
				item.allday = true;
				item.subject = item.project_label + ':' + item.label;
				item.start = new Date();
				item.start.parseMiogaDate(item.dstart, item.allday);
				item.end = new Date();
				item.end.parseMiogaDate(item.dend, item.allday);
				if (! that.entryTasks['U'].hasOwnProperty(item.user_id)) {
					MIOGA.logDebug("EiffelMain", 2, 'entryTasks for U rowid = ' + item.user_id + " doesn't exists");
					that.entryTasks['U'][item.user_id] = [];
				}
				that.entryTasks['U'][item.user_id].push(item);
			});
			MIOGA.logDebug("EiffelMain", 2, 'entryTasks = ', that.entryTasks);
			// Process each line
			//
			$.each(that.lines, function (num, line) {
				
				MIOGA.logDebug("EiffelMain", 2, 'line = ',line);
				if ( (line.type === 'U') || (line.type === 'R') ) {
					MIOGA.logDebug("EiffelMain", 1, 'rowid = ' + line.rowid);
					var line_divs = new Array();

					// Sort task by start date and allday before
					// Generate x_pos numbers function of start date
					// search best y_pos to optimise display => all allday task are before
					// -----------------------------------------
					// |=======================================|
					// |     ======       |                    |
					// |         =====    |                    |
					// -----------------------------------------
					var i;
					var s_cell;
					var cell_count;
					var offset;
					var length;
					var found_subline;
					var y_pos;
					line.cell_tasks = [];
					line.height = 0;
					line.sublines = [];
					var entry_w;
					entry_w = line.$cont.width();
					line.sublines.push({xmax : 0, ypos : 0, next_y : 0 });   // set the xmax and ypos for first subline
					MIOGA.logDebug("EiffelMain", 2, '  entry_w = ' + entry_w);
					if (that.entryTasks[line.type].hasOwnProperty(line.rowid)) {
						// Organize tasks by day cells (s_cell and cell_count)
						// and calculate dimensions for rectangle (offset and length)
						//
						$.each(that.entryTasks[line.type][line.rowid].sort(sort_task), function (num, task) {
							MIOGA.logDebug("EiffelMain", 1, 'subject = '+ task.subject);
							MIOGA.logDebug("EiffelMain", 1, 'pstart = ' + that.p_start +' start = ' + task.start + '  end = ' + task.end + ' allday = ' + task.allday);
							MIOGA.logDebug("EiffelMain", 2, 'time start = ' + task.start.getTime() + '  time pstart = ' + that.p_start.getTime());
							s_cell = Math.floor((task.start.getTime() - that.p_start.getTime()) / 86400000);
							cell_count = Math.floor((task.end.getTime() - task.start.getTime()) / 86400000) + 1;
							MIOGA.logDebug("EiffelMain", 2, 's_cell = ' + s_cell + '  cell_count = ' + cell_count);
							if (task.allday) {
								offset = s_cell * that.day_w;
								length = cell_count * that.day_w;
							}
							else {
								offset = parseInt(((that.day_w * (task.start.getHours()*3600 + task.start.getMinutes()*60 + task.start.getSeconds()) ) / 86400) + s_cell * that.day_w);
								length = parseInt((that.day_w * (task.end.getTime() - task.start.getTime()) / 86400000));
							}
							MIOGA.logDebug("EiffelMain", 2, 'before limit tests week_width = '+ that.week_width+' offset = ' + offset + ' length = ' + length);
							// Verify s_cell and cell_count to limit them
							// Construct list of tasks for 
							if (s_cell < 0) {
								cell_count += s_cell;
								s_cell = 0;
							}
							if (s_cell >= that.cells_per_line) {
								s_cell = that.cells_per_line - 1;
							}
							if ((s_cell + cell_count) >= that.cells_per_line) {
								cell_count = that.cells_per_line - s_cell;
							}
							MIOGA.logDebug("EiffelMain", 2, 'after limits s_cell = ' + s_cell + '  cell_count = ' + cell_count);
							// Verify offset and length to limit them in window
							if (offset < 0) {
								length = length + offset;
								offset = 0;
							}
							else if (offset >= that.week_width) {
								offset = that.week_width - 1;
							}
							if ((offset + length) > that.week_width) {
								length = that.week_width - offset;
							}
							MIOGA.logDebug("EiffelMain", 1, 'after limit tests offset = ' + offset + ' length = ' + length);
							MIOGA.logDebug("EiffelMain", 1, 'subline0  xmax = ' + line.sublines[0].xmax + ' ypos = ' + line.sublines[0].ypos);
							if (length > 0) {
								found_subline = false;
								for (is = 0; is < line.sublines.length; is++) {
									if (line.sublines[is].xmax < offset) {
										task.subline = is;
										task.xmax = offset + length;
										line.sublines[is].xmax = task.xmax;
										found_subline = true;
										MIOGA.logDebug("EiffelMain", 1, 'subline found '+ is + ' xmax = ' + line.sublines[is].xmax);
									}
								}
								if (! found_subline) {
									// add a subline
									MIOGA.logDebug("EiffelMain", 1, 'subline not found');
									task.subline = line.sublines.length;
									line.sublines.push({xmax : (offset + length), ypos : 0, next_y : 0 });
									MIOGA.logDebug("EiffelMain", 1, 'subline added  xmax = ' + line.sublines[task.subline].xmax);
								}
							}
							else {
								length = 0;
							}
							// Keep calculate values and organize by cells
							for (i = s_cell; i < (s_cell + cell_count); i++) {
								task.offset = offset;
								task.length = length;
								if (!$.isArray(line.cell_tasks[i])) {
									MIOGA.logDebug("EiffelMain", 2, 'allocate array for cell i = '+i);
									line.cell_tasks[i] = [];
								}
								line.cell_tasks[i].push(task);
							}
						});

						y_pos = 0;
						$.each(that.entryTasks[line.type][line.rowid].sort(sort_subline), function (num, task) {
							MIOGA.logDebug("EiffelMain", 1, 'subject = '+ task.subject +' offset ' +task.offset + '  length : ' + task.length + ' subline = ' + task.subline);
							if (task.offset !== undefined && task.length !== undefined) {
								var lineIdStr = "" + line.rowid;
								if ((task.subline > 0) && (line.sublines[task.subline].ypos === 0) ) {
									line.sublines[task.subline].ypos = line.sublines[task.subline - 1].next_y;
								}
								y_pos = line.sublines[task.subline].ypos;
								var title = "";
								if (task.label) {
									title = task.label;
								}
								else if (task.subject){
									title = task.subject;
								}
								else {
									MIOGA.logDebug("EiffelMain", 1, 'no label and no subject to task.');
								}
								var $c = $('<div class="task-cell" title="' + title + '" style="position:absolute; left:'+task.offset+'px; top:'+y_pos+'px; background-color:'+task.bgcolor+';"> </div>').appendTo(line.$cont);
								$c.width(task.length);
								MIOGA.logDebug("EiffelMain", 1, 'c outerheight= ' + $c.outerHeight() + " height = " + $c.height());
								line.sublines[task.subline].next_y = y_pos + $c.outerHeight();
								if (line.height < line.sublines[task.subline].next_y) {
									line.height = line.sublines[task.subline].next_y;
								}
								else {
								}
								if (lineIdStr.indexOf("res_id") !== -1 && $c.width() > 1) {
									$c.width($c.outerHeight()).css('background-color', 'red');
								}
								MIOGA.logDebug("EiffelMain", 1, 'line.height = ' + line.height);
							}
							else {
								MIOGA.logDebug("EiffelMain", 1, 'offset or length undefined');
							}
						});
						line.$cont.height(line.height);
					}
				}
			});

			var total_height = that.$planning.height();
			MIOGA.logDebug("EiffelMain", 2, ' planning height = '+ that.$planning.height() + ' entries height = '+ that.$entries.height() + ' total_height = ' + total_height);
			that.$weeks.height(total_height);
		});
		this.$planning.hide().show();
	}
	// --------------------------------------------------------
	// sort_task
	// Sort given task function of
	//        start date
	//        allday first
	// --------------------------------------------------------
	function sort_task(a, b) {
		MIOGA.logDebug("EiffelMain", 3, 'sort_task  a.start ' + a.start + ' b.start = ' + b.start);
		var ret_val = 0;
		// if one is allday and not the other choose the allday one first
		if (a.allday && !b.allday) {
				ret_val = -1;
		}
		else if (!a.allday && b.allday) {
				ret_val = 1;
		}
		// all tasks are allday or all tasks are not
		else  {
			if (a.start < b.start) {
				ret_val = -1;
			}
			else if (a.start > b.start) {
				ret_val = 1;
			}
			else {
				ret_val = 0;
			}
		}

		MIOGA.logDebug("EiffelMain", 3, ' ===> retval = ' + ret_val);

		return ret_val;
	}
	
	// --------------------------------------------------------
	// sort_ypos
	// Sort given task function of y_pos
	// --------------------------------------------------------
	function sort_subline(a, b) {
		MIOGA.logDebug("EiffelMain", 3, 'sort_subline  a.subline ' + a.subline + ' b.subline = ' + b.subline);
		if (a.subline < b.subline) {
			return -1;
		}
		else if (a.subline > b.subline) {
			return 1;
		}
		else {
			return 0;
		}
	}
	// ----------------------------------------------------------------------------
	/**
	Show detail of task in one cell of planning

	@method showDetails
	@param integer line_num gives the line number in this.lines
	@param integer offset gives in pixels the offset in entry line
	**/
	// ----------------------------------------------------------------------------
	function showDetails(line_num, offset) {
		MIOGA.logDebug("EiffelMain", 1, 'showDetails  line_num ' + line_num + ' offset = ' + offset);
		var that = this;
		var cell_num = Math.floor(offset / this.day_w);
		var day_date = new Date(that.p_start.getFullYear(), that.p_start.getMonth(),that.p_start.getDate() + cell_num);

		this.$detail_content.children().remove();
		if ($.isArray(this.lines[line_num].cell_tasks[cell_num])) {
			that.$detail_content.append(that.options.getTaskDetail(that.lines, line_num, offset, cell_num));
		}
		else {
			$('<p class="no-tasks">'+this.options.i18n.noTasksText+'</p>').appendTo(this.$detail_content);
		}
		this.$details.dialog('option', 'title', day_date.formatMioga(false) + " - " + this.lines[line_num].label);
		this.$details.dialog('open');
	}
};
