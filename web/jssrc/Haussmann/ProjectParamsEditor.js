// =============================================================================
/**
Create a ProjectParamsEditor object.

    var options = {
        i18n : { .... }
    };

	this.projectParamsEditor = new ProjectParamsEditor($elem, options);

@class ProjectParamsEditor
@constructor
@param {Object} $elem JQuery that will be contains this editor
@param {Object} options
**/
// =============================================================================
function ProjectParamsEditor ($elem, options) {
	"use strict";
	MIOGA.debug.ProjectParamsEditor = 0;
	MIOGA.logDebug('ProjectParamsEditor', 1, ' ProjectParamsEditor loaded');
	var that = this;
	this.$elem = $elem;
	this.i18n = options.i18n;
	this.haussmann = options.haussmann;
	this.generateHash = options.haussmann.generateHash;
	this.insertVocabulary = options.haussmann.insertVocabulary;
	this.project = undefined;
	this.must_be_save = false;
	// publics method declaration
	this.showWindow = showWindow;
	this.hideWindow = hideWindow;
	this.changeMustBeSaveFlag = changeMustBeSaveFlag;
	this.serializeProjectParams = serializeProjectParams;
	this.refreshGeneralInfo = refreshGeneralInfo;
	this.refreshHistoryStatus = refreshHistoryStatus;
	this.refreshComplementaryData = refreshComplementaryData;
	this.refresh = refresh;

	this.advancedParamsEditor = new AdvancedParamsEditor (that.$elem, {
		i18n : that.i18n,
		showProjectEditor : function () {
			that.showWindow ();
			that.$edit_project_actions_cont.find('.success-message').remove();
			that.$edit_project_actions_cont.append('<span class="success-message">' + that.i18n.mustBeApplyProject + '</span>');
		},
		haussmann : that.haussmann,
		changeMustBeSaveFlag : that.changeMustBeSaveFlag
	});
	this.$cont = that.$elem.find('.project-params-editor');
	// ids
	this.project_label_id = MIOGA.generateID("haussmann");
	this.project_description_id = MIOGA.generateID("haussmann");
	this.project_use_result_id = MIOGA.generateID("haussmann");
	this.project_base_dir_id = MIOGA.generateID("haussmann");
	this.project_tags_id = MIOGA.generateID("haussmann");
	this.project_started_id = MIOGA.generateID("haussmann");
	this.project_planned_id = MIOGA.generateID("haussmann");
	this.project_ended_id = MIOGA.generateID("haussmann");
	this.status_form_id = MIOGA.generateID("haussmann");
	this.color_id = MIOGA.generateID("haussmann");
	this.bg_color_id = MIOGA.generateID("haussmann");
	this.result_list_id = MIOGA.generateID("haussmann");
	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================
	// PRIVATES METHODS
	// ===================================================================================
	/**
	@description Private method that draws a temporarily success message
	@method successMessage
	@param Object $cont that is jQuery element that will be contains a success message
	@param String message that is a success message
	**/
	function successMessage ($cont, message) {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::successMessage - loaded');
		var $message = $('<span class="success-message">' + message + '</span>').appendTo($cont).hide();
		$message.fadeIn(1800, function () {
			$(this).fadeOut(2500, function () {
				$(this).remove();
			});
		});
	}
	/**
	@description Private function that draws errors list in a HTML container
	@method drawErrorSimpleList
	@param Object $elem is the Jquery element as container of error list will be drawed.
	@param Array error_list is the list of translate error message.
	**/
	function drawErrorSimpleList ($elem, error_list) {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::drawErrorSimpleList - loaded');
		$elem.find('.error').remove();
		var $error_cont = $('<div class="error"></div>').appendTo($elem);
		$.each(error_list, function (i,error_str) {
			$error_cont.append('<p class="error">' + that.i18n[error_str] + '</p>');
		});
	}
	/**
	@description Public method that set flag that determines if advanced parameters is modified or not
	@method changeMustBeSaveFlag
	@param Boolean bool
	**/
	function changeMustBeSaveFlag (bool) {
		that.must_be_save = bool;
	}
	/**
	@description Private mehod that adds result
	@method addResult
	**/
	function addResult () {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::addResult - loaded');
		that.$result_add_cont.find('.error').remove();
		var $label = that.$result_add_cont.find('.new-result-label-input');
		var $error_cont = $('<div class="error error-create-result"></div>').appendTo(that.$result_add_cont);
		var params = {
			label : $.trim($label.val())
		};
		var onSuccess = function (newResult) {
			$label.val('');
			location.hash = that.generateHash('ResultEditor', newResult.result_id);
		};
		var onError = function (error_list) {
			drawErrorSimpleList ($error_cont, error_list);
		};
		that.resultList.createResult(false, params, onSuccess, onError);
	}
	/**
	@description Private method that calls addResut method if there is no modification in advanced parameters, else a dialog is showed.
	@method addResult
	**/
	function createResult () {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::createResult - loaded');
		if (that.must_be_save === true) {
			MIOGA.dialog({
				title : that.i18n.alert,
				content : $('<div>' + that.i18n.saveProjectToContinue + '</div>'),
				buttons : [
					{
						i18n : that.i18n.saveAndContinue,
						classes : ["button","confirm-btn"],
						cb : function (evClick,$dialog) {
							$dialog.dialog('destroy');
							applyProjectModifications (function () {
								addResult ();
							});
						}
					},
					{
						i18n : that.i18n.cancel,
						classes : ["button","cancel"],
						cb : function (evClick,$dialog) {
							$dialog.dialog('destroy');
						}
					}
				]
			});
		}
		else {
			addResult();
		}
	}
	/**
	@description Private function that draws result item as Jquery object and return it.
	@method getHTMLResultItem
	@param Bool checked is cheked attribute
	@param String label is the reuslt label
	@param Int result_id is a result rowid
	**/
	function getHTMLResultItem (checked, label, result_id) {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::getHTMLResultItem - loaded');
		var check_class = "unchecked";
		var span_edit = '';
		var span_delete = '';
		if (checked === true) {
			check_class = 'checked';
		}
		span_edit = '<span class="h_edit" title="' + that.i18n.edit_result + '"></span>';
		span_delete = '<span class="h_delete" title="' + that.i18n.delete_result + '"></span>';

		var $res = $(
			'<div class="result-item-' + result_id + '">'
				+ '<span class="result-item-check ' + check_class + '"></span>'
				+ '<span class="result-item-label">' + label + '</span>'
				+ span_edit
				+ span_delete
			+ '</div>'
		);
		return $res;
	}
	/**
	@description Private method that saves project modifications
	@method applyProjectModifications
	@param Function cb that is a callback called on save project is a success
	**/
	function applyProjectModifications (cb) {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::applyProjectModifications - loaded');
		var attr = serializeProjectParams ();
		var check = that.project.checkProjectParams(attr);
		if (check.status === "error") {
			that.$cont.find('.error').remove();
			var $error_cont = $('<div class="error"></div>').appendTo(that.$project_info_cont);
			$.each(check.error, function (i,error_str) {
				$error_cont.append('<p class="error">' + that.i18n[error_str] + '</p>');
			});
		}
		else {
			that.project.setProject(attr, function () {
				that.project.taskList.updtaeTaskPositionToChangeStructure(that.project.advancedParams.cloneAttr());
				that.changeMustBeSaveFlag(false);
				successMessage(that.$project_info_cont, that.i18n.successUpdateProject);
				that.$edit_project_actions_cont.find('.success-message').remove();
				if (cb !== undefined) {
					cb();
				}
			});
		}
	}
	/**
	@description Private method that shows or hides edit mode for project status
	@method showEditStatusIfNeeded
	**/
	function showEditStatusIfNeeded () {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::showEditStatusIfNeeded - loaded');
		that.$status_cont.find('.error').remove();
		var adv_status_index = that.project.advancedParams.project_status.length-1;
		var project_status_index = that.project.status.length-1;
		that.$status_link_cont.find('.edit-mod').html(that.i18n.read_mod);
		if (project_status_index < adv_status_index) {
			that.$status_status.attr('value',(project_status_index+1)).text(that.project.advancedParams.project_status[project_status_index+1]);
			that.$status_link_cont.find('.edit-mod').html(that.i18n.read_mod).show();
			that.$status_control_cont.show();
			that.$status_cont.find('thead').show();
		}
		else {
			that.$status_control_cont.hide();
			that.$status_link_cont.find('.edit-mod').hide();
			if (that.project.current_status !== null) {
				that.$status_cont.find('thead').show();
			}
			else {
				that.$status_cont.find('thead').hide();
			}
		}
	}
	/**
	@description Private method that shows or hides Jquery element if list is geted in parameter is empty or not.
	@method historyTogglable
	@param Array list is a list wish length will be checked
	@param Object $link is a Jquery element that will be showed or hided according to length of list
	**/
	function historyTogglable (list, $link) {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::historyTogglable - loaded');
		if (list.length > 1) {
			$link.show();
		}
		else {
			$link.hide();
		}
	}
	/**
	@description Private method that hides edit mode for status
	@method hideStatusEdit
	**/
	function hideStatusEdit () {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::hideStatusEdit - loaded');
		that.$status_cont.find('.error').remove();
		that.$status_link_cont.find('.edit-mod').html(that.i18n.edit_mod);
		that.$status_control_cont.hide();
		if (that.project.current_status !== null) {
			that.$status_cont.find('thead').show();
		}
		else {
			that.$status_cont.find('thead').hide();
		}
	}
	/**
	@description Private method that hides status item without last item.
	@method hideStatusHistory
	**/
	function hideStatusHistory () {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::hideStatusHistory - loaded');
		that.$status_link_cont.find('.show-history').html(that.i18n.show_history);
		that.$status_tbody.find('.status-item').hide();
		that.$status_tbody.find('.status-item').last().show();
	}
	/**
	@description Private method that verifies if status item is valid
	@method checkStatusItem
	@param Object args is status item that will be checked
	@param Function onSuccess is callback that will be called on check success
	@param Object $error_cont is Jquery element that is container contains error messages if cheking is faillure
	**/
	function checkStatusItem (args, onSuccess, $error_cont) {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::checkStatusItem - loaded');
		$error_cont.find('.error').remove();
		var checked = that.project.checkStatusItem (args);
		if (checked.status === "success") {
			onSuccess (args);
		}
		else {
			$.each(checked.error, function (i,text) {
				$error_cont.append('<p class="error">' + that.i18n[text] + '</p>');
			});
		}
	}
	/**
	@description Private method that adds status item in DOM and adjusts next status entry according to advanced parameters
	@method createStatusItem
	@param Int statusValue is status index
	@param Object date is date of status item
	@param String comment is comment of status item
	**/
	function createStatusItem (statusValue, date, comment) {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::createStatusItem - loaded');
		that.$status_cont.find('span.h_delete').remove();
		that.$status_tbody.find('tr:last').before(
			'<tr class="status-item">'
				+ '<td class="status-item-status" value="' + statusValue + '">' + that.project.advancedParams.project_status[statusValue] + '</td>'
				+ '<td class="status-item-date">' + date + '</td>'
				+ '<td class="status-item-comment" >' + comment.replace(/\n/g, '<br>') + '</td>'
				+ '<td class="status-item-delete" ><span class="h_delete">&#160;</span></td>'
			+ '</tr>'
		);
		if (that.project.advancedParams.project_status[(statusValue+1)] !== undefined) {
			that.$status_status.attr('value',(statusValue+1)).text(that.project.advancedParams.project_status[(statusValue+1)]);
			// determination of minimum date for future creations of status item.
			that.$status_date.datepicker("option", "minDate", date).val('');
			that.$status_comment.val('');
		}
		else {
			that.$status_control_cont.hide();
		}
		if (that.$status_link_cont.find('.show-history').html() === that.i18n.show_history) {
			hideStatusHistory();
		}
	}
	// ===================================================================================
	// PUBLICS METHODS
	// ===================================================================================
	/**
	@description Public method that show this window and hide other. It calls refresh method. 
	@method showWindow
	**/
	function showWindow() {
		MIOGA.logDebug("ProjectParamsEditor::showWindow", 1, 'showWindow loaded');
		that.$cont.show();
		that.refresh ();
	}
	/**
	@description Public method that hides this window.
	@method showWindow
	**/
	function hideWindow() {
		MIOGA.logDebug("ProjectParamsEditor::hideWindow", 1, 'hideWindow loaded');
		that.$cont.hide();
	}
	/**
	@description Public method that serializes project informations and return it as JSON structure
	@method serializeProjectParams
	**/
	function serializeProjectParams () {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::serializeProjectParams - loaded');
		var projectInfo = {};
		projectInfo.project_id = that.project.project_id;
		projectInfo.label = $.trim(that.$project_info_cont.find('[name="label"]').val());
		projectInfo.description = $.trim(that.$project_info_cont.find('[name="description"]').val());
		projectInfo.base_dir = $.trim(that.$project_info_cont.find('[name="base_dir"]').val().replace (/\/\//g, '/'));
		// tags
		projectInfo.tags = [];
		that.$project_info_cont.find('#' + that.project_tags_id + " span.tag").each(function () {
			projectInfo.tags.push($.trim($(this).text()));
		});
		// use result
		if (that.$project_info_cont.find('#' + that.project_use_result_id).attr('checked') === "checked" ) {
			projectInfo.use_result = true;
		}
		else {
			projectInfo.use_result = false;
		}
		// color
		projectInfo.color = $.trim(that.$project_info_cont.find('#' + that.color_id).val());
		projectInfo.bgcolor = $.trim(that.$project_info_cont.find('#' + that.bg_color_id).val());
		// dates
		if (that.$project_dates_cont.find('[name="started"]').datepicker('getDate') !== null) {
			projectInfo.started = new Date();
			projectInfo.started.setFullYear(that.$project_dates_cont.find('[name="started"]').datepicker('getDate').getFullYear());
			projectInfo.started.setMonth(that.$project_dates_cont.find('[name="started"]').datepicker('getDate').getMonth());
			projectInfo.started.setDate(that.$project_dates_cont.find('[name="started"]').datepicker('getDate').getDate());
		}
		else {
			projectInfo.started = null;
		}
		if (that.$project_dates_cont.find('[name="planned"]').datepicker('getDate') !== null) {
			projectInfo.planned = new Date();
			projectInfo.planned.setFullYear(that.$project_dates_cont.find('[name="planned"]').datepicker('getDate').getFullYear());
			projectInfo.planned.setMonth(that.$project_dates_cont.find('[name="planned"]').datepicker('getDate').getMonth());
			projectInfo.planned.setDate(that.$project_dates_cont.find('[name="planned"]').datepicker('getDate').getDate());
		}
		else {
			projectInfo.planned = null;
		}
		
		if (that.$project_dates_cont.find('[name="ended"]').val() != "") {
			projectInfo.ended = new Date();
			projectInfo.ended.setFullYear(that.$project_dates_cont.find('[name="ended"]').datepicker('getDate').getFullYear());
			projectInfo.ended.setMonth(that.$project_dates_cont.find('[name="ended"]').datepicker('getDate').getMonth());
			projectInfo.ended.setDate(that.$project_dates_cont.find('[name="ended"]').datepicker('getDate').getDate());
		}
		else {
			projectInfo.ended = null;
		}
		// complementary data
		projectInfo.complementary_data = [];
		that.$project_compl_data_cont.find('.form-item') .each(function(){
			projectInfo.complementary_data.push({
				"label": $.trim($(this).find('label').text()),
				"content": $.trim($(this).find('span').children().attr('value'))
			});
		});
		return projectInfo;
	}
	// =============================================================================================
	// REFRESH METHODS
	// =============================================================================================
	/**
	@description Public method that refreshs project general informations
	@method refreshGeneralInfo
	**/
	function refreshGeneralInfo () {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::refreshGeneralInfo - loaded');
		that.$project_info_cont.find('#' + that.color_id).miniColors("value", that.project.color);
		that.$project_info_cont.find('#' + that.bg_color_id).miniColors("value", that.project.bgcolor);
		that.$project_info_cont.find('#' + that.project_label_id).val(that.project.label);
		that.$project_info_cont.find('#' + that.project_description_id).val(that.project.description.replace(/<br>/g, '\n').replace(/<BR>/g, '\n'));
		that.$project_info_cont.find('#' + that.project_base_dir_id).val(that.project.base_dir);
		// initialize tags
		that.$project_info_cont.find('#' + that.project_tags_id).tags ({
			available : tags_info.available_tags,
			free : tags_info.free_tags,
			tags : that.project.tags,
			warning :tags_info.tag_does_not_exist
		});
		// use result
		that.$result_list.find('[class^="result-item-"]').remove();
		for (var i=0; i < that.resultList.result_list.length; i++) {
			that.$result_list.find('.result-add-cont').after(getHTMLResultItem(that.resultList.result_list[i].checked, that.resultList.result_list[i].label, that.resultList.result_list[i].result_id));
		}
		if (that.project.use_result === true) {
			that.$project_info_cont.find('#' + that.project_use_result_id).attr('checked', that.project.use_result);
			that.$result_list_cont.show();
		}
		else {
			that.$project_info_cont.find('#' + that.project_use_result_id).removeAttr('checked');
			that.$result_list_cont.hide();
		}
		// dates
		that.$project_dates_cont.find('#' + that.project_started_id).datepicker("setDate" , that.project.started );
		that.$project_dates_cont.find('#' + that.project_planned_id).datepicker("option", "minDate", that.project.started).datepicker("setDate" , that.project.planned );
		var ended = null;
		if (that.project.ended !== null) {
			ended = that.project.ended;
		}
		that.$project_dates_cont.find('#' + that.project_ended_id).datepicker("option", "minDate", that.project.started).datepicker("setDate" , ended );
	}
	/**
	@description Public method that refreshs project history status
	@method refreshHistoryStatus
	**/
	function refreshHistoryStatus () {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::refreshHistoryStatus - loaded');
		if (that.project.advancedParams.project_status.length === 0) {
			that.$status_cont.hide();
		}
		else {
			that.$status_cont.show();
			// insert status value in label for next entry status.
			// insert status items.
			that.$status_tbody.find('.status-item').remove();
			var count_status = that.project.status.length;
			if (that.project.current_status !== null) {
				for (var statusItemStatus = 0; statusItemStatus < count_status ; statusItemStatus ++) {
					createStatusItem (statusItemStatus, that.haussmann.getStrDateFromStr(that.project.status[statusItemStatus].date), that.project.status[statusItemStatus].comment);
				}
				that.$status_cont.find('thead').show();
			}
			else {
				that.$status_cont.find('thead').hide();
				that.$status_link_cont.find('.show-history').hide();
			}
			historyTogglable (that.project.status, that.$status_link_cont.find('.show-history'));
			showEditStatusIfNeeded ();
			that.$status_cont.find('.h_delete').show();
			hideStatusEdit();
			hideStatusHistory();
		}
	}
	/**
	@description Public method that refreshs complementary data
	@method refreshComplementaryData
	**/
	function refreshComplementaryData () {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::refreshComplementaryData - loaded');
		that.$project_compl_data_cont.children().not('legend').remove();
		if (that.project.advancedParams.complementary_data.length > 0) {
			that.$project_compl_data_cont.show();
			$.each(that.project.advancedParams.complementary_data, function (i,cd) {
				var elm = "";
				var content = "";
				if (that.project.complementary_data[i] !== undefined) {
					content = that.project.complementary_data[i].content;
				}
				if (cd.type == '0' ) {
					elm = '<div class="form-item"><label for="complementary_data_' + i + '">' + cd.label + '</label><span><input id="complementary_data_' + i + '" type="text" value="' + content + '"></span></div>';
				}
				else if (cd.type == '1') {
					elm = '<div class="form-item"><label for="complementary_data_' + i + '">' + cd.label + '</label><span><textarea id="complementary_data_' + i + '" value="' + content + '">' + content + '</textarea></span></div>';
				}
				$(elm).appendTo(that.$project_compl_data_cont);
			});
		}
		else {
			that.$project_compl_data_cont.hide();
		}
	}
	/**
	@description Public method that refreshs project editor
	@method refresh
	**/
	function refresh (current_project) {
		MIOGA.logDebug("ProjectParamsEditor", 1, 'ProjectParamsEditor::refresh - loaded');
		that.resultList = that.project.resultList;

		that.$cont.find('.error').remove();
		refreshGeneralInfo ();
		refreshHistoryStatus ();
		refreshComplementaryData ();
	}
	// ===================================================================================
	// INIT
	// ===================================================================================
	this.$cont.html('');
	this.$edit_project_actions_cont = $('<div class="edit-project-actions-cont"></div>').appendTo(that.$cont);
	// --------------------------------------------------------
	// edit project control behaviors 
	// --------------------------------------------------------
	$('<input type="button" class="button project-action-back cancel" value="' + that.i18n.backToProject + '"/>')
	.click(function (ev) {
		location.hash = that.generateHash("ProjectEditor", that.project.project_id);
	}).appendTo(that.$edit_project_actions_cont);
	// --------------------------------------------------------
	// Link to advanced parameters
	// --------------------------------------------------------
	this.$advParamsLink = $('<span class="adv-params-toggle-btn">' + that.i18n.advancedParameters + '</span>')
		.appendTo(that.$edit_project_actions_cont)
		.click(function () {
			that.advancedParamsEditor.editAdvancedParams(that.project, that.project.advancedParams.cloneAttr());
			that.$cont.hide();
		});
	// --------------------------------------------------------
	// draw generals informations of project
	// --------------------------------------------------------
	this.$project_info_cont = $(
		'<fieldset class="edit-project-info-cont">'
			+ '<legend>' + that.i18n.informations + '</legend>'
			+ '<div class="form-item"><label for="' + that.project_label_id + '">' + that.i18n.label + '</label><input type="text" id="' + that.project_label_id + '" name="label" value=""/></div>'
			+ '<div class="form-item"><label for "' + that.project_description_id + '">' + that.i18n.description + '</label><textarea id="' + that.project_description_id + '" name="description"></textarea></div>'
			+ '<div class="form-item"><label for="' + that.project_base_dir_id + '">' + that.i18n.dirOfProject + '</label><input id="' + that.project_base_dir_id + '" type="text" name="base_dir" value=""/><span class="h_edit"/></div>'
			+ '<div class="form-item">'
				+ '<label for="' + that.color_id + '">' + that.i18n.color + '</label><input id="' + that.color_id + '" type="text" name="txt_color" class="color-picker" size="7" />'
			+ '</div>'
			+ '<div class="form-item">'
				+'<label for="' + that.bg_color_id + '">' + that.i18n.bgColor + '</label><input id="' + that.bg_color_id + '" type="text" name="bg_color" class="color-picker" size="7" />'
			+ '</div>'
			+ '<div class="form-item">'
				+ '<label for="' + that.project_tags_id + '">' + that.i18n.tags + '</label>'
				+ '<div id="' + that.project_tags_id + '"></div>'
			+ '</div>'
			+ '<div class="form-item"><label for="' + that.project_use_result_id + '">' + that.i18n.use_result + '</label><input type="checkbox" id="' + that.project_use_result_id + '" name="useResult" value=""/></div>'
			+ '<fieldset class="edit-project-dates-cont">'
				+ '<legend>' + that.i18n.dates + '</legend>'
				+ '<div class="form-item"><label for="' + that.project_started_id + '">' + that.i18n.started_date + '</label><input id="' + that.project_started_id + '" name="started"/></div>'
				+ '<div class="form-item"><label for="' + that.project_planned_id + '">' + that.i18n.planned_date + '</label><input id="' + that.project_planned_id + '" name="planned"/></div>'
				+ '<div class="form-item"><label for="' + that.project_ended_id + '">' + that.i18n.ended_date + '</label><input id="' + that.project_ended_id + '" name="ended"/></div>'
			+ '</fieldset>'
			+ '<fieldset class="status-fieldset">'
				+ '<legend>' + that.i18n.status + '</legend>'
				+ '<table class="status-table sbox list">'
					+ '<thead>'
						+ '<tr><th class="header-status-label">' + that.i18n.status + '</th><th class="header-status-date">' + that.i18n.date + '</th><th class="header-status-comment">' + that.i18n.comment + '</th><th class="header-status-delete"></th></tr>'
					+ '</thead>'
					+ '<tbody>'
						+ '<tr class="status-control-cont" style="display:none;">'
							+ '<td><span><span class="h_create">&nbsp;</span><span value="" class="status-input-label"></span></span></td>'
							+ '<td><input type="text" class="status-input-date"/></td><td><textarea value="" class="status-input-comment"></textarea></td><td></td>'
						+ '</tr>'
					+ '</tbody>'
				+ '</table>'
				+ '<div class="status-edit-link-cont">'
					+ '<a href="#" class="edit-mod">' + that.i18n.edit_mod + '</a>'
					+ '<a href="#" class="show-history">' + that.i18n.show_history + '</a>'
				+ '</div>'
			+ '</fieldset>'
			+ '<fieldset class="edit-project-compl-data-cont">'
				+ '<legend>' + that.i18n.complementaryData + '</legend>'
			+ '</fieldset>'
		+ '</fieldset>'
	);

	this.$project_compl_data_cont = this.$project_info_cont.find('.edit-project-compl-data-cont');
	// APPLY project informations
	$('<input type="button" class="button save-edit-project-btn" value="' + that.i18n.apply + '"/>')
	.appendTo(that.$project_info_cont)
	.click(function () {
		applyProjectModifications ();
	});
	// on change use result attribute
	this.$use_result = this.$project_info_cont.find('#' + that.project_use_result_id).on('change', function () {
		var $checkbox = $(this);
		if ($checkbox.attr('checked') === "checked" ) {
			if (that.must_be_save === true) {
				MIOGA.confirm({
					title : that.i18n.alert,
					content : $('<div>' + that.i18n.saveProjectToContinue + '</div>'),
					buttons : [
						{
							i18n : that.i18n.saveAndContinue,
							classes : ["button","confirm-btn"],
							cb : function (evClick,$dialog) {
								$dialog.dialog('destroy');
								applyProjectModifications (function () {
									that.$result_list_cont.show();
								});
							}
						},
						{
							i18n : that.i18n.cancel,
							classes : ["button","cancel"],
							cb : function (evClick,$dialog) {
								$checkbox.removeAttr('checked');
								$dialog.dialog('destroy');
							}
						}
					]
				});
			}
			else {
				that.$result_list_cont.show();
			}
		}
		else {
			that.$result_list_cont.hide();
		}
	});
	// minicolor
	this.$project_info_cont.find('.color-picker').miniColors();
	// draw fileselector
	this.$project_info_cont.find('#' + that.project_base_dir_id).next('span.h_edit').click(function () {
		var $base_dir_input = that.$project_info_cont.find('#' + that.project_base_dir_id);
		$('<div class="fileselector-dialog"><div class="fileselector_cont"></div></div>')
		.dialog({
			resizable : false,
			autoOpen : true,
			modal : true,
			width : 500,
			height : 400,
			title : that.i18n.choose_project_dir,
			open : function () {
				var $dial = $(this);
				var $contButton = $('div.ui-dialog-buttonset');
				if ($contButton.find('button').length > 0) {
					$contButton.find('button').remove();
				}
				var optFileSelector = {
					single : true,
					root : mioga_context.private_dav + "/" + that.project.group_ident,
					current : "",
					selected : false,
					i18n : {
						add_btn : that.i18n.choose,
						close_btn : that.i18n.close,
						select_all : that.i18n.select_all,
						deselect_all : that.i18n.deselect_all
					},
					select_add : function () {
						var path = $dial.find('.fileselector_cont').fileselector('getSelection')[0][0].relpath;
						$base_dir_input.val(path);
						$dial.dialog('destroy');
						$dial.remove();
					},
					select_close : function () {
						$dial.dialog('destroy');
					}
				};
				if ($base_dir_input.val().length > 0) {
					optFileSelector.current = $base_dir_input.val();
					optFileSelector.selected = true;
				}
				$dial.find('.fileselector_cont').fileselector(optFileSelector);
			}
		});
	});
	// --------------------------------------------------------
	// behavior project dates
	// datepicker to dates in project informations
	//     Maximum date of started is the smallest date between planned date and ended date
	//     Minimum date of planned and ended date is the started date
	// --------------------------------------------------------
	this.$project_dates_cont = this.$project_info_cont.find('.edit-project-dates-cont');
	this.$project_dates_cont.find('#' + that.project_started_id)
		.datepicker({dateFormat: 'yy-mm-dd'})
		.change(function () {
			var started = $(this).datepicker('getDate');
			that.$project_info_cont.find('#' + that.project_planned_id).datepicker("option", "minDate", started);
			that.$project_info_cont.find('#' + that.project_ended_id).datepicker("option", "minDate", started);
		});
	this.$project_dates_cont.find('#' + that.project_planned_id).datepicker({dateFormat: 'yy-mm-dd'});
	this.$project_dates_cont.find('#' + that.project_ended_id).datepicker({dateFormat: 'yy-mm-dd'});
	// ------------------------------------------------------
	// INIT PROJECT STATUS
	this.$status_cont = this.$project_info_cont.find('.status-fieldset');

	this.$status_link_cont = this.$status_cont.find('.status-edit-link-cont');
	this.$status_tbody = this.$status_cont.find('tbody');
	this.$status_control_cont = this.$status_tbody.find('.status-control-cont');
	this.$status_status = this.$status_tbody.find('.status-input-label');
	this.$status_date = this.$status_tbody.find('.status-input-date');
	this.$status_comment = this.$status_tbody.find('.status-input-comment');
	// edit mod
	this.$status_link_cont.find('.edit-mod').toggle(
		function () {
			showEditStatusIfNeeded ();
			
		},
		function () {
			hideStatusEdit();
		}
	);
	// toggle history visibility
	this.$status_link_cont.find('.show-history').toggle(
		function () {
			$(this).html(that.i18n.hide_history);
			that.$status_tbody.find('.status-item').show();
		},
		function () {
			hideStatusHistory();
		}
	);
	// behavior datepicker dates for status
	this.$status_date
		.datepicker({
			dateFormat : 'yy-mm-dd',
			defaultDate : 0
		})
		.click(function () {
			that.$status_cont.find('.error').remove();
		}
	);
	// behavior create status item
	this.$status_tbody.find('.h_create').click(function () {
		var params = {
			status : parseInt(that.$status_status.attr('value'), 10),
			date : that.$status_date.val(),
			comment : $.trim(that.$status_comment.val().replace(/<br>/g, '\n').replace(/<BR>/g, '\n'))
		};
		that.$status_cont.find('.error').remove();
		var onCheckSuccess = function (params) {
			that.project.addStatusItem (params, function () {
				createStatusItem (params.status, that.haussmann.getStrDateFromStr(params.date), params.comment);
				historyTogglable (that.project.status, that.$status_link_cont.find('.show-history'));
				showEditStatusIfNeeded();
				successMessage (that.$status_cont, that.i18n.successCreateStatusItemTmp);
			});
		};
		checkStatusItem (params, onCheckSuccess, that.$status_cont);
	});
	// delete behavior
	this.$status_tbody.delegate('span.h_delete', 'click', function () {
		var $tr = $(this).parent().parent('tr');
		var status_val = $tr.find('.status-item-status').attr('value');
		var status_label = that.project.advancedParams.project_status[status_val];
		var onSuccess = function () {
			that.$status_tbody.find('.status-input-label')
				.attr('value',status_val)
				.text(status_label);
			that.$status_comment.val('');
			if ( $tr.prev('tr').length > 0 ) {
				that.$status_date.datepicker("option", "minDate", $tr.prev('tr').find('.status-date').text()).val('');
				$tr.prev('tr').find('.status-item-delete').html('<span class="h_delete"></span>');
			}
			else {
				that.$status_date.datepicker("option", "minDate", null).val('');
				that.$status_cont.find('thead').hide();
			}
			$tr.remove();
			if (that.$status_link_cont.find('.edit-mod').html() === that.i18n.read_mod) {
				showEditStatusIfNeeded ();
			}
			if (that.$status_link_cont.find('.show-history').html() === that.i18n.show_history) {
				hideStatusHistory ();
			}
			historyTogglable (that.project.status, that.$status_link_cont.find('.show-history'));
			showEditStatusIfNeeded ();
		};
		that.project.deleteLastStatusItem(onSuccess);
	});
	// --------------------------------------------------------
	// draw project result list
	// --------------------------------------------------------
	this.$result_list_cont = $(
		'<fieldset class="edit-project-info-cont result-list-cont" style="display:none;">'
			+ '<legend>' + that.i18n.resultList + '</legend>'
			+ '<div class="result-list" id="' + that.result_list_id + '">'
				+ '<div class="result-add-cont">'
					+ '<span class="add_result_link read-mod">' + that.i18n.add_result + '</span>'
					+ '<span class="h_create create-result-btn" style="display:none;"></span>'
					+ '<input class="new-result-label-input" type="text" placeholder="' + that.i18n.label + '" style="display:none;">'
				+ '</div>'
			+ '</div>'
		+ '</fieldset>'
	);
	this.$result_list = this.$result_list_cont.find('#' + that.result_list_id); 
	this.$result_add_cont = this.$result_list_cont.find('.result-add-cont');

	this.$result_add_cont.find('.add_result_link').toggle(
		function () {
			that.$result_add_cont.find('.create-result-btn, .new-result-label-input').show();
			that.$result_add_cont.find('.new-result-label-input').trigger('focus');
			$(this).removeClass('read-mod').addClass('edit-mod').html(that.i18n.read_mod);
		},
		function () {
			that.$result_add_cont.find('.create-result-btn, .new-result-label-input').hide();
			$(this).removeClass('edit-mod').addClass('read-mod').html(that.i18n.add_result);
		}
	);
	// CREATE result
	this.$result_add_cont.find('.create-result-btn').click(function (ev) {
		createResult ();
	});
	that.$result_add_cont.find('.new-result-label-input').on ('keypress', function (evt) {
		if ((evt.keyCode == 13) && (evt.target.type !== "input")) {
			createResult ();
			return (false);
		}
	});
	// DELETE result
	this.$result_list.delegate('.h_delete', 'click', function () {
		var $container = $(this).parent();
		var result_id = parseInt($container.attr('class').replace('result-item-',''),10);
		MIOGA.deleteConfirm({
			title : that.i18n.removeItem ,
			i18n_OK : that.i18n.removeItem,
			i18n_cancel : that.i18n.cancel,
			message : '<div class="tree-delete-dialog"><p class="error"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+ that.i18n.permanently_remove_message + '</br>' + that.i18n.remove_message_confirmation + '</p></div>',
			cb_OK : function (ev,$dial) {
				var params = {
					result_id : result_id,
					project_id : that.project.project_id
				};
				that.resultList.deleteResult(params, function (RL) {
					$container.remove();
					$dial.dialog('destroy');
				});
			},
			cb_cancel : function (ev,$dial) {
				$dial.dialog('destroy');
			}
		});
	});
	// EDIT result
	this.$result_list.delegate('.h_edit', 'click', function () {
		var $container = $(this).parent();
		var result_id = parseInt($container.attr('class').replace('result-item-',''),10);
		if (that.must_be_save === true) {
			MIOGA.confirm({
				title : that.i18n.alert,
				content : $('<div>' + that.i18n.saveProjectToContinue + '</div>'),
				buttons : [
					{
						i18n : that.i18n.saveAndContinue,
						classes : ["button","confirm-btn"],
						cb : function (evClick,$dialog) {
							$dialog.dialog('destroy');
							applyProjectModifications (function () {
								location.hash = that.generateHash('ResultEditor', result_id);
							});
						}
					},
					{
						i18n : that.i18n.cancel,
						classes : ["button","cancel"],
						cb : function (evClick,$dialog) {
							$dialog.dialog('destroy');
						}
					}
				]
			});
		}
		else {
			location.hash = that.generateHash('ResultEditor', result_id);
		}
	});
	// form of project information
	this.$edit_project_form = $('<form class="edit-project-form form"></form>')
		.appendTo(that.$cont)
		.append(that.$project_info_cont, that.$result_list_cont);
} // END ProjectParamsEditor object
