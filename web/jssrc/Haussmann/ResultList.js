// =============================================================================
/**
@description Create a ResultlList object.

    var options = {
        haussmann : { .... }
    };

@class ResultlList
@constructor
**/
// =============================================================================
function ResultList (options) {
	"use strict";
	MIOGA.debug.ResultList = 0;
	MIOGA.logDebug('ResultList', 1, ' ResultList loaded');
	var that = this;
	this.haussmann = options.haussmann;
	this.result_list = [];
	this.result_list_by_rowid = {};
	this.project_id = options.project_id;
	this.project_owner_id = options.project_owner_id;
	this.project_owner_firstname = options.project_owner_firstname;
	this.project_owner_lastname = options.project_owner_lastname;
	this.project_owner_email = options.project_owner_email;

	// publics method declaration
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;
	this.isEmpty = isEmpty;
	this.isUnique = isUnique;
	this.hasResultStatus = hasResultStatus;
	this.createResult = createResult;
	this.deleteResult = deleteResult;
	this.getResultList = getResultList;
	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================================================
	// PRIVATES METHODS
	// ===================================================================================================================
	/**
	@description Private method that adds result in result list
	@method addResult
	@param Object new_result that is result object
	@param Function cb that callback called on adding result success
	**/
	function addResult (new_result, cb) {
		MIOGA.logDebug('ResultList', 1, 'ResultList::addResult - loaded');
		that.result_list.push(new_result);
		that.result_list_by_rowid[new_result.result_id] = new_result;
		if (cb !== undefined) {
			cb(new_result);
		}
		else {
			MIOGA.logDebug('ResultList', 1, 'ResultList::addResult - cb is undefined');
		}
	}
	// ===================================================================================================================
	// PUBLICS METHODS
	// ===================================================================================================================
	/**
	@description method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('ResultList', 1, 'ResultList::addRefreshCB - loaded');
		this.refreshCB.push( { cb : cb, context : context });
	}
	/**
	@description method that loads each callback.
	@method callRefreshCB
	**/
	function callRefreshCB() {
		MIOGA.logDebug('ModelList', 1, 'ResultList::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that checks if arguments length is empty. If this is it, that returns true, else returns false.

	@method isEmpty
	@param Object elem can be all type. This is the element that will be tested.
	@param String error_str that is a string value that represents the error message if tested element is empty.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isEmpty (elem, error_str, global_result) {
		MIOGA.logDebug('ModelList', 1, 'ResultList::isEmpty - loaded');
		var result = false;
		
		if (elem.length === 0) {
			if (global_result !== undefined) {
				global_result.status = "error";
				global_result.error.push(error_str);
			}
			result = true;
		}
		return result;
	}
	/**
	@description Public method that checks if the element exists in a list. If this is it, that returns true, else returns false.

	@method isUnique
	@param Object elem can be all type. This is the element that will be searched in list.
	@param Array list. This is the list that will be matched.
	@param String error_str that is a string value that represents the error message if tested element is not unique.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isUnique (elem, list, error_str, global_result) {
		MIOGA.logDebug('ModelList', 1, 'ResultList::isUnique - loaded');
		var result = true;
		for (var i = 0; i < list.length; i++) {
			if (list[i] === elem) {
				if (global_result !== undefined) {
					global_result.status = "error";
					global_result.error.push(error_str);
				}
				result = false;
				break;
			}
		}
		return result;
	}
	/**
	@description Public method that checks if result status exists and return boolean result
	@method hasResultStatus
	**/
	function hasResultStatus () {
		MIOGA.logDebug('ModelList', 1, 'ResultList::hasResultStatus - loaded');
		var result = false;
		for (var i = 0; i < that.result_list.length; i++) {
			if (that.result_list[i].current_status !== null) {
				result = true;
				break;
			}
		}
		return result;
	}
	/**
	@description method that creates result
	@method createResult
	@param Bool async that determines if AJAX request is synchronous or asynchronous
	@param Object params contains pairs key / value : label
	@param Function onSuccess that called on AJAX request success
	@param Function onError that called on AJAX request error
	**/
	function createResult(async, params, onSuccess, onError) {
		MIOGA.logDebug('ResultList', 1, 'ResultList::createResult - loaded');
		var check = {
			status : "success",
			error : []
		};
		var label_list = [];
		for (var i = 0; i < that.result_list.length; i++) {
			label_list.push(that.result_list[i].label);
		}
		that.isEmpty(params.label, "labelDisallowEmpty", check);
		that.isUnique (params.label, label_list, "resultAlreadyExists", check);
		params.project_id = that.project_id;
		if (check.status === "success") {
			$.ajax({
				url : "CreateResult.json",
				dataType : 'json',
				data :params,
				type : "POST",
				async : async,
				success : function(response){ 
					if (response.status === "OK") {
						var budget;
						var current_status;
						if (response.data.budget !== null) {
							budget = parseInt(response.data.budget,10);
						}
						else {
							budget = null;
						}
						if (response.data.current_status !== null) {
							current_status = parseInt(response.data.current_status,10);
						}
						else {
							current_status = null;
						}
						var newResult = new Result ({
							result_id : response.data.result_id,
							created : response.data.created,
							modified : response.data.modified,
							project_id : response.data.project_id,
							label : response.data.label,
							pend : response.data.pend,
							budget : budget,
							tags : $.parseJSON(response.data.tags),
							status : $.parseJSON(response.data.status),
							current_status : current_status,
							checked : response.data.checked,
							project_owner_id : that.project_owner_id,
							project_owner_firstname : that.project_owner_firstname,
							project_owner_lastname : that.project_owner_lastname,
							project_owner_email : that.project_owner_email
						});
						if (newResult !== undefined) {
							var onAddResult = undefined;
							if (onSuccess !== undefined) {
								onAddResult = onSuccess;
							}
							addResult(newResult, onAddResult);
						}
					}
					else {
						MIOGA.logError ("ResultList. setResultList AJAX request returned KO status", false);
						that.callRefreshCB();
					}
				},
				error: function(err, ioArgs) {
					MIOGA.logError ("ResultList. setResultList AJAX request returned an error : " + err, true);
				}
			});
		}
		else {
			if (onError !== undefined) {
				onError(check.error);
			}
			else {
				MIOGA.logDebug('ResultList', 1, 'ResultList::createResult - check status is KO but no onEror method existing');
				MIOGA.logError ("ResultList. setResultList AJAX request returned an error : " + err, true);
			}
		}
	}
	/**
	@description method that deletes result
	@method deleteResult
	@param Object args contains pairs key / value : result_id
	@param Function cb that called on AJAX request success
	**/
	function deleteResult (args, cb) {
		MIOGA.logDebug('ResultList', 1, 'ResultList::deleteResult - loaded');
		var params = {
			result_id : args.result_id,
			project_id : that.project_id
		};
		$.ajax({
			url : "DeleteResult.json",
			dataType:'json',
			data : params,
			type : "POST",
			async : false,

			success : function(response){ 
				if (response.status === "OK") {
					var result_l = $.extend([],that.result_list);
					$.each(result_l, function (i,result) {
						if (result.result_id == response.data) {
							that.result_list.splice(i,1);
							delete that.result_list_by_rowid[params.result_id];
							that.haussmann.projectList.project_list_by_rowid[that.project_id].taskList.updatePositionToDeleteResult(result.result_id);
						}
					});
					if (cb !== undefined) {
						cb(that);
					}
					else {
						that.callRefreshCB();
					}
				}
				else {
					MIOGA.logError ("ResultList. Status of deleteResult AJAX request is 'KO'. The API last good status will be reloaded", false);
 					that.callRefreshCB();
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("ResultList. deleteResult AJAX request returned an error : " + err, true);
			}
		});
	}
	/**
	@description Public method that retrieves result list
	@method getResultList
	@param Bool async that determines if AJAX request is synchronous or asynchronous
	**/
	function getResultList (async) {
		MIOGA.logDebug('ModelList', 1, 'ResultList::getResultList - loaded');
		var params = {
			project_id  : that.project_id
		};
		$.ajax({
			url : "GetResultList.json",
			async : async,
			dataType : 'json',
			type : "POST",
			data : params,
			success: function(response){
				if (response.status === "OK") {
					that.result_list = [];
					$.each(response.data, function (i, result) {
						var budget;
						var current_status;
						if (result.budget !== null) {
							budget = parseInt(result.budget,10);
						}
						else {
							budget = null;
						}
						if (result.current_status !== null) {
							current_status = parseInt(result.current_status,10);
						}
						else {
							current_status = null;
						}
						var new_result = new Result({
							result_id : parseInt(result.result_id,10),
							created : result.created,
							modified : result.modified,
							project_id : parseInt(result.project_id,10),
							label : result.label,
							budget : budget,
							pend: result.pend,
							tags : $.parseJSON(result.tags),
							status : $.parseJSON(result.status),
							current_status : current_status,
							checked : result.checked,
							result_list : that.result_list,
							project_owner_id : that.project_owner_id,
							project_owner_firstname : that.project_owner_firstname,
							project_owner_lastname : that.project_owner_lastname,
							project_owner_email : that.project_owner_email
						});
						if (new_result !== undefined) {
							addResult(new_result);
						}
						else {
							MIOGA.logError ("ResultList. getResultList method can not created a Result Object from Ajax request response. Return on main page...", false);
							that.callRefreshCB();
						}
					});
				}
				else {
					MIOGA.logError ("ResultList. getResultList AJAX request returned KO status", false);
				}
			},
			error : function (err) {
				MIOGA.logError ("ResultList. getResultList AJAX request returned an error : " + err, false);
			}
		});
	}
	// ===================================================================================================================
	// INIT
	// ===================================================================================================================
	this.getResultList(false);
}// End ResultList object
