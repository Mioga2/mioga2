/*! Plugin jQuery : extensibleList v1 */

/*
 * This plugin creates an extensible and sortable list of data.
 *
 * Data can be single values. In this case, the module should be instanciated as below:
 *
 * 		var $list = $('<div></div>').extensibleList ({values: [1, 2, 3]});
 *
 * Data can be objects. In this case, the module instanciation is more complex as it needs a template for data insertion. The template input elements need to be placed in a container node (for example a span):
 *
 * 		var $template = $('<span><input name="field1"/><select name="field2"><option value="value1">Label 1</option><option value="value2">Label 2</option></select></span>');
 * 		var $list = $('<div></div>').extensibleList ({values: [{field1: 'value1', field2: 'value2'}], template: $template});
 *
 * delegates option can be containing a list of key / value pairs that represents a fields could be clicked. 
 * 		For example : 
 * 			delegates : {
 * 				nameList : ["name1", "name2"],
 * 				onClickCB : function (ev, elem, that) {
 * 					// ev is click event.
 * 					// elem is HTML element that is clicked.
 * 					// that is a container of plugin.
 * 					$(elem).remove();
 * 				}
 * 			}
 *		For this example, when it clicks on field with name attribute is "name1" or "name2", the HTML element is removed.
 *
 * List data can be retrieved by calling 'getValues' method:
 *
 * 		var values = $list.extensibleList ('getValues');
 *
 * The resulting data will be a list of single values or a list of key / value pairs according to how it has be instanciated.
 *
 * Several callbacks can be passed to object when instanciated:
 * 		- updateCB will be triggered when a new entry is added to list,
 * 		- orderCB will be triggered when list order is changed,
 * 		- removeCB will be triggered when an entry is removed.
 *
 * The "updateCB" callback receives two arguments:
 * 		- the extensibleList DOM node,
 * 		- the data structure that was just inserted.
 *
 * The "orderCB" and "removeCB" callbacks receive two arguments:
 * 		- the extensibleList DOM node,
 * 		- the output of 'getValues'.
 *
 * Data can be checked before insertion. The check function is passed at instanciation in the "checkCB" key. It receives the data to be inserted as only argument and must return true to permit data insertion or false to prevent insertion.
 *
 * Formatting data for display (for example when template is composed of a 'select' element) can be achieved by passing a 'labels' key upon instanciation:
 *
 * 		var labels = {
 * 			field1: {
 * 				value1: 'Some text for value 1',
 * 				value2: 'Some text for value 2'
 * 			}
 * 		};
 *
 */
(function( $ ){

	var defaults = {
		sortable: {
			status : true,
			items : "li"
		},
		values: [ ],
		template: '<input name="value" type="text"/>',
		labels: { },
		checkCB: function (data) { return (true); },
		parentDeletable : true,
		delegates : undefined
	};

	function getValue ($li) {
//		console.log ('[extensibleList::getValue] $li: ');
//		console.log ($li);
		var item = {};
		$li.find ('> div.el-field > span').each (function () {
			if (!$(this).hasClass ('el-container')) {	// Ignore sublist containers
				if ($(this).attr ('name') && ($(this).attr ('type') && ($(this).attr ('type') === 'array'))) {
					// Key / array of values value pairs
					if (item[$(this).attr ('name')] === undefined) {
						item[$(this).attr ('name')] = [ ];
					}
					item[$(this).attr ('name')].push ($(this).attr ('value'));
				}
				else if ($(this).attr ('name')) {
					// Key / values value pairs
					item[$(this).attr ('name')] = $(this).attr ('value');
				}
				else {
					// Simple values
					item = $(this).text ();
				}
			}
		});
		return item;
	}
	function getValues (elem) {
//		console.log ('[extensibleList::getValues] elem: ');
//		console.dir (elem);
		var res = new Array();
		$(this).find('ul:first > li').each(function() {
			var item = getValue ($(this));
			// Process sublist, if any
			if ($(this).has ('.extensibleList').length) {
				var $sublist = $(this).find ('.extensibleList:first');
				// Process sublist
				item[$sublist.attr ('name')] = $sublist.extensibleList ('getValues');
			}
			// Append data
			res.push (item);
		});
		return res;
	}

	function callCB (mode) {
		var params = $(this).data ('extensibleList');
		if (params) {
			if ((mode === 'insert') && params.updateCB) {
				params.updateCB (this, getValues.call (this));
			}
			else if ((mode === 'order') && params.orderCB) {
				params.orderCB (this, getValues.call (this));
			}
			else if ((mode === 'remove') && params.removeCB) {
				params.removeCB (this, getValues.call (this));
			}
		}
	}

	function updateValue (elem, new_value, $line_tmp) {
//		console.log ('[extensibleList::updateValue] elem, new_value, $line_tmp: ');
//		console.dir (elem);
//		console.log ("new_value = " + new_value);
//		console.dir ($line_tmp);
		var result= false;
		var params = $(this).data ('extensibleList');
		var $elem_tmp = undefined;
		$.each($line_tmp.find('*'), function (i,e) {
			if ($(e).html() === $(elem).html() ) {
				$(e).attr('value', new_value).html(new_value);
				$elem_tmp = $(e);
			}
		});
		if (params.checkCB) {
			result = params.checkCB(getValue($line_tmp), $(this));
		}
		return result;
	};
	
	function createEntry (data) {
//		console.log ('[extensibleList::createEntry] Data: ');
//		console.dir (data);
		var that = this;
		var params = $(this).data ('extensibleList');
		// generates HTML content of field. If this is actionable, "onClickCB" defined in Plugin option is called on click event. 
		var generateHTMLValue = function generateHTMLValue (key, label, array) {
			var $elem = $('<span name="' + key + '" value="' + label + '">' + label + '</span>');
			if (array) {
				$elem.addClass('array').attr('type', 'array');
			}
			return $elem;
		};

		var $entry = $('<li></li>').appendTo ($(this).find ('ul:first'))
			.hover(
				function () {
					$(this).find('> span.item-delete').show();
				},
				function () {
					$(this).find('> span.item-delete').hide();
				}
			);
		if (typeof (data) === 'object') {
			for (var key in data) {
				var $cont = $('<div class="el-field ' + key + '"></div>').appendTo ($entry);
				if (data[key] instanceof Array) {
					for (var i=0; i<data[key].length; i++) {
						var label = (params.labels[key] && params.labels[key][data[key][i]]) ? params.labels[key][data[key][i]] : data[key][i];
						$cont.append( generateHTMLValue (key, label, true) );
					}
				}
				else {
					// Try to find a suitable label for key
					var label = (params.labels[key] && params.labels[key][data[key]]) ? params.labels[key][data[key]] : data[key];
					$cont.append( generateHTMLValue (key, label, false) );
				}
			}
		}
		else {
			var $cont = $('<div class="el-field "></div>').appendTo ($entry);
			$('<span></span>').append (data).appendTo ($cont);
		}
		if ($(that).parent().children('div.el-field').length) {
			if (params.parentDeletable === false ) {
				$(that).prev('span').removeClass('item-delete').addClass('item-not-delete');
			}
		}
		$('<span class="item-delete" style="display:none;">&#160;</span>').appendTo($entry)
			.click( function () {
				$(this).parent('li').remove();
				if ($(that).parent().children('div.el-field').length && $(that).children('ul').children('li').length === 0 && params.parentDeletable === false) {
					$(that).prev('span').removeClass('item-not-delete').addClass('item-delete');
				}
				callCB.call (that, 'remove');
			});
	}

	// Serialize template data to an object
	function serializeObject ($template) {
		var o = {};

		// Basic serialization (to array)
		var a = $template.serializeArray ();
		// Also include unchecked checkboxes
		a = a.concat ($template.find ('input[type=checkbox]:not(:checked)').map (function () {
			return ({name: this.name, value: ''});
		}).get ());
		// Array to object conversion
		$.each (a, function () {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			}
			else {
				o[this.name] = this.value || '';
			}
		});
		return (o);
	}

	// Callback to be called at entry insert
	function insertCB ($template) {
		var that = this;
		// According to $template type (simple input or nested structure), data can't be retreived the same way.
		// If simple input, it can be serialized directly,
		// if nested structure, all nodes should be passed to serializer
		var entry;
		if ($template.find ('*').length) {
			entry = serializeObject ($template.find ('*'));
		}
		else {
			entry = $template.val ();
		}
		var params = $(this).data ('extensibleList');

		//TODO suppress white space at beginning and end
		if (entry && params.checkCB (entry, that)) {
			createEntry.call(that, entry);
			if ($template.find ('*').length) {
				// Complex form template
				$template.find ('input:not(input:checkbox, input:radio),select,textarea').val ('').change ();
				$template.find ('input:checkbox, input:radio').removeAttr ('checked').removeAttr ('selected');
				$template.find ('input:not(input:checkbox, input:radio):visible,select:visible,textarea:visible').first ().focus ();
			}
			else {
				// Simple form template
				$template.val ('').change ();
				$template.focus ();
			}
			if (params && params.updateCB) {
				params.updateCB (this, entry);
			}
		}
	};

	var methods = {
		init : function(params) {
			return this.each(function() {
//				console.log('[extensibleList::init] Params: ');
//				console.dir(params);
				var $this = $(this);
				var that = this;

				$(this).empty ();

				params = $.extend(true, {}, defaults, params);
				$(this).data ('extensibleList', params);

				$this.addClass ('extensibleList');

				var $list = $('<ul class="ui-sortable"></ul>').appendTo(this);
				if (params.sortable.status === true) {
					var opt_sortable = {
						update: function (event, ui) { callCB.call (that, 'order'); },
						items : params.sortable.items
					};
					$list.sortable(opt_sortable);
				}

				for (var entry = 0; entry < params.values.length; entry ++) {
					createEntry.call (this, params.values[entry]);
					if (params) {
						if (params.updateCB) {
							params.updateCB (that, params.values[entry]);
						}
					}
				}

				var $cont = $('<span class="el-container"></span>').appendTo ($(this));
				var $template = $(params.template);
				$template.on ('keypress', function (evt) {
					if ((evt.keyCode == 13) && (evt.target.type !== "textarea")) {
						// User pressed ENTER, create node in list
						insertCB.call (that, $template);
						return (false);
					}
				});
				$cont.append ($template);
				$('<span class="item-create">&#160;</span>').appendTo ($cont).click (function () { insertCB.call (that, $template); });

				// Focus input
				if ($template.find ('*').length) {
					// Complex form template
					$template.find ('input:not(input:checkbox, input:radio):visible,select:visible,textarea:visible').first ().focus ();
				}
				else {
					// Simple form template
					$template.focus ();
				}
				// make selector str for click event according to delegates nameList option
				var delegateSelector = new Array();
				if (params.delegates) {
					if (params.delegates.onClickCB && params.delegates.nameList) {
						$.each(params.delegates.nameList, function (i,e) {
							delegateSelector.push('span[name="' + e + '"]');
						});
					}
				}
				$this.delegate(delegateSelector.join(','), 'click', function (ev) {
					if ($(this).parent().parent().parent().parent().html() === $this.html()) {
						var elem = this;
						params.delegates.onClickCB(ev, elem, that);
					}
				});
			});
		},
		getValues : function() {
			return getValues.call(this);
		},
		updateValue : function (elem, new_value, $line_tmp) {
//			console.log ('[jquery.extensibleList] Update value');
//			console.log ($(this));
			var result = updateValue.call(this, elem, new_value, $line_tmp);
			return result;
		},
		enable: function () {
//			console.log ('[jquery.extensibleList] Enabling list');
//			console.log ($(this));
			$(this).find ('> .el-container').show ();
		},
		disable: function () {
//			console.log ('[jquery.extensibleList] Disabling list');
//			console.log ($(this));
			$(this).find ('> .el-container').hide ();
		},
		clear: function () {
//			console.log ('[jquery.extensibleList] Clearing list');
//			console.log ($(this));
			$(this).find ('ul > li').remove ();
		}
	};

	$.fn.extensibleList = function(method) {
		//	Method calling logic
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.extensibleList' );
		}
	};
})( jQuery );
