// =============================================================================
/**
Create a ProjectListEditor object.

    var options = {
        i18n : { .... }
    };

	this.projectListEditor = new ProjectListEditor($elem, options);

@class Haussmann
@constructor
@param {Object} $elem JQuery that will be contains this editor
@param {Object} options
**/
// =============================================================================
function ProjectListEditor ($elem, options) {
	"use strict";
	MIOGA.debug.ProjectListEditor = 0;
	MIOGA.logDebug("ProjectListEditor", 1, "ProjectListEditor constructor begin ");

	var that = this;
	this.$elem = $elem;
	this.i18n = options.i18n;
	this.$project_list_editor = this.$elem.find('.project-list-editor');
	this.haussmann = options.haussmann;
	this.ganttEditor = options.haussmann.ganttEditor;
	this.gantt = options.haussmann.gantt;
	this.addProjectInGanttPrefs = options.addProjectInGanttPrefs;
	this.generateHash = options.haussmann.generateHash;
	this.insertVocabulary = options.haussmann.insertVocabulary;

	var shapshift_opt = {
		centerGrid: true,
		enableAnimation: true,
		enableAnimationOnInit: true,
		enableAutoHeight: true,
		enableDrag: false,
		enableRearrange: true,
		enableResize: true,
		animateSpeed: 250,
		gutterX: 10,
		gutterY: 10,
		paddingY: 0,
		paddingX: 0
	};

	// publics method declaration
	this.showWindow = showWindow;
	this.hideWindow = hideWindow;
	this.refresh = refresh;

	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================
	// PRIVATES METHODS
	// ===================================================================================
	/**
	@description Private method that generates and return Jquery element that represents a project item with label and description.
	@method getHTMLProject
	@param Int id is rowid of project.
	@param String label is label of project.
	@param String description is description of project.
	**/
	function getHTMLProject (id, label, desc) {
		MIOGA.logDebug("ProjectListEditor", 1, "ProjectListEditor::getHTMLProject loaded");
		var delete_elm = "";
		if (mioga_context.rights.Anim) {
			if (mioga_context.rights.Anim === true) {
				delete_elm = '<span class="project-delete"></span>';
			}
			else {
				MIOGA.logDebug("ProjectListEditor", 1, "ProjectListEditor::getHTMLProject No rights to delete project so no draw delete button.");
			}
		}
		else {
			MIOGA.logDebug("ProjectListEditor", 1, "ProjectListEditor::getHTMLProject no right Anim exists.");
		}
		var $elem = $(
			'<div class="project-list-item project_id_' + id + ' sbox" style="position: absolute;">'
				+ delete_elm
				+ '<h2 class="project-label">' + label + '</h2>'
				+ '<div class="project-description">' + desc + '</div>'
			+ '</div>'
		);
		return $elem;
	}
	/**
	@description Private method that creates a new project, load this and redirect HTML page to ProjectEditor.
				If there is an error, that method draws error in specific container.
	@method createProject
	**/
	function createProject () {
		MIOGA.logDebug("ProjectListEditor", 1, "ProjectListEditor::createProject loaded");
		that.$error_cont_dial_create_project.children().remove();
		var label = $.trim(that.$dial_create_project_label.val());
		var model = that.$dial_create_project_model.val();
		if (label.length > 0) {
			var onSuccessCreateProject = function (new_project) {
				that.$dial_create_project.dialog('close');
				that.addProjectInGanttPrefs (new_project);
				location.hash = that.generateHash("ProjectParamsEditor", new_project.project_id);
			};
			var onErrorCreateProject = function (params) {
				that.$error_cont_dial_create_project.append('<p class="error error_label">' + params.label + ' : ' + that.i18n.projectAlreadyExists + '</p>');
			};
			var params = {
				label : label
			};
			if (model !== that.i18n.none) {
				params.model_id = model;
			}
			that.haussmann.projectList.createProject(false, params,onSuccessCreateProject,onErrorCreateProject);
		}
		else {
			that.$error_cont_dial_create_project.find('div.error').append('<p class="error error_label">' + that.i18n.labelRequired + '</p>');
		}
	}
	/**
	@description Private method that shows or hides project. If none status is selected, status filter is disabled
	@method filterStatus
	@param Object $elem is Jquery object that represents item of status list that is changed
	**/
	function filterStatus ($elem) {
		var pl = $elem.data('project_list');
		if ($elem.hasClass('selected')) {
			$elem.removeClass('selected');
			if (that.$statusList.find('.item.selected').length > 0) {
				for (var i=0; i < pl.length; i++) {
					that.$project_list_cont.find('.project_id_' + pl[i] ).addClass ('hide-by-status');
				}
			}
			else {
				that.$project_list_cont.find('.hide-by-status').removeClass('hide-by-status');
			}
		}
		else {
			$elem.addClass('selected');
			if (that.$statusList.find('.item.selected').length === 1) {
				that.$project_list_cont.find('.project-list-item').addClass('hide-by-status');
			}
			else {
			}
			
			for (var j=0; j < pl.length; j++) {
				that.$project_list_cont.find('.project_id_' + pl[j] ).removeClass ('hide-by-status');
			}
		}
		that.$project_list_cont.shapeshift(shapshift_opt);
	}
	/**
	@description Private method that shows or hides project by label. If none label is writed, label filter is disabled
	@method filterLabel
	**/
	function filterLabel () {
		var label = $.trim(that.$inputSearch.val());
		for(var i=0; i < that.haussmann.projectList.project_list.length; i++) {
			var selected = true;
			var $proj = that.$project_list_cont.find('.project_id_' + that.haussmann.projectList.project_list[i].project_id);
			if (label.length > 0 && that.haussmann.projectList.project_list[i].label.toLowerCase().indexOf(label.toLowerCase()) === -1) {
				$proj.addClass('hide-by-label');
			}
			else {
				$proj.removeClass('hide-by-label');
			}
		}
		that.$project_list_cont.shapeshift(shapshift_opt);
	}
	// ===================================================================================
	// PUBLICS METHODS
	// ===================================================================================
	/**
	@description Public method that show this window and hide other. It calls refresh method. 
	@method showWindow
	**/
	function showWindow() {
		MIOGA.logDebug("ProjectListEditor", 1, 'ProjectListEditor::showWindow - showWindow loaded');
		that.$project_list_editor.show();
		that.refresh();
	}
	/**
	@description Public method that hides this window.
	@method showWindow
	**/
	function hideWindow() {
		MIOGA.logDebug("ProjectListEditor", 1, 'ProjectListEditor::hideWindow - hideWindow loaded');
		that.$project_list_editor.hide();
	}
	/**
	@description Public method that refresh status filter according to each status label
	@method refreshStatusFilter
	@param Object status_str_list is JSON structure contains label of status and array of project id that have this status.
	**/
	function refreshStatusFilter (status_str_list) {
		that.$statusList.find('.item').remove();
		// sort status by label.
		var keys = [];
		for (var key in status_str_list) {
			if (status_str_list.hasOwnProperty(key)) {
				keys.push(key);
			}
		}
		keys.sort (function(a,b){
			if (a.toLowerCase() === b.toLowerCase()) {
				return 0;
			}
			else if (a.toLowerCase() > b.toLowerCase()) {
				return 1;
			}
			else {
				return -1;
			}
		});
		for (var i=0; i < keys.length; i++) {
			var $item = $('<div class="item"><span class="item-label">' + keys[i] + '</span></div>').data({project_list : status_str_list[keys[i]]});
			if (keys[i] === that.haussmann.i18n.undefined_status) {
				$item.addClass('undefined-status').prependTo(that.$statusList);
			}
			else {
				$item.appendTo(that.$statusList);
			}
		}
	}
	/**
	@description Public method that refresh project list editor
	@method refresh
	**/
	function refresh () {
		MIOGA.logDebug("ProjectListEditor", 1, 'ProjectListEditor::refresh - loaded');
		// data of dialog create project
		this.$project_list_editor.find('.error').remove();
		if (mioga_context.rights.WriteProjectIfOwner || mioga_context.rights.Anim ) {
			that.$dial_create_project_label.val("");
			that.$dial_create_project_model.children().remove();
			that.$dial_create_project_model.append('<option value="' + that.i18n.none + '">' + that.i18n.none + '</option>');
			$.each(that.haussmann.projectList.modelList.model_list, function (i,model) {
				that.$dial_create_project_model.append('<option value="' + model.model_id + '">' + model.label + '</option>');
			});
		}
		else {
			MIOGA.logDebug('ProjectListEditor', 1, 'ProjectListEditor::refresh - User don t have a permission to create project');
		}
		// data of dialog change project
		that.$project_list_cont.children().remove();
		var PL = that.haussmann.projectList.getProjectListByLabel ();
		var PL_length = PL.length;
		var args;
		var status_str_list = {};
		if (PL_length > 0) {
			$.each(PL, function(idx, project) {
				var $item = getHTMLProject(project.project_id, project.label, project.description.replace(/\n/g, '<br>'));
				that.$project_list_cont.append($item);
				$item.find('.project-delete').click(function(ev) {
					ev.stopPropagation();
					MIOGA.logDebug("ProjectListEditor", 1, "delete_project id = " + project.project_id);
					if (confirm(that.i18n.confirmSuppressProject)) {
						that.haussmann.projectList.deleteProject({project_id : project.project_id});
					}
				});
				
				if (status_str_list[project.current_status_str]){
					status_str_list[project.current_status_str].push(project.project_id);
				}
				else {
					status_str_list[project.current_status_str] = [project.project_id];
				}
			});
			that.$project_list_cont.shapeshift(shapshift_opt);
			// ellipsis for project description
			$.each(that.$project_list_cont.find('.project-description'), function (ind,desc) {
				$(desc).ellipsis({lines : 10});
			});
			that.$inputSearch.show();
			refreshStatusFilter (status_str_list);
			that.$statusFilter.removeAttr('style');
			that.$gantt_link.show();
		}
		else {
			that.$inputSearch.hide();
			that.$statusFilter.attr('style', "display:none;");
			that.$gantt_link.hide();
			that.$project_list_editor.find('.project-list-cont').html('<p>' + that.i18n.noExistingProject + '</p>');
		}
	}
	// ================================================================================================================
	// INIT project list editor
	// ================================================================================================================
	this.$project_list_editor.html('<div class="project-create-link-cont"></div><div class="project-search-cont"></div>');
	// ----------------------------------------------------------------------------------------------
	// Behavior to dynamic show and hide project item according to input value.
	// ----------------------------------------------------------------------------------------------
	this.$inputSearch = $('<input type="text" class="search-project" placeholder="' + that.i18n.searchProject + '"/>').on('keyup', function (ev) {
		filterLabel ();
	}).appendTo(that.$project_list_editor.find('.project-search-cont'));
	
	//	status filter
	this.$statusFilter = $(
		'<div class="container">'
			+ '<div class="title-cont">'
				+ '<div class="title">' + that.i18n.filterByStatus + '</div>'
				+ '<div class="icon-down"></div>'
			+ '</div>'
		+ '</div>'
	).appendTo(that.$project_list_editor.find('.project-search-cont'));

	this.$statusList = $('<div class="list close" style="display:none;">').appendTo(that.$statusFilter);

	this.$statusFilter.find('.title-cont').on ('click', function () {
		if (that.$statusList.hasClass('close')) {
			that.$statusList.slideDown(300, function () {
				$(this).removeClass('close');
				$(this).show();
			});
		}
		else {
			that.$statusList.slideUp(300, function () {
				$(this).addClass('close');
				$(this).hide();
			});
		}
	});

	this.$statusFilter.on( "mouseleave", function () {
		if (!that.$statusList.hasClass('close')) {
			that.$statusList.slideUp(300).addClass('close');
		}
	});

	this.$statusList.delegate('.item','click', function () {
		filterStatus($(this));
	});
	this.$project_list_cont = $('<div class="project-list-cont" style="position: relative;"></div>').appendTo(that.$project_list_editor);
	// ------------------------------------------------------------------------------------------------
	// GANTT VIEW
	// ------------------------------------------------------------------------------------------------
	this.$gantt_cont = $('<div class="gantt-cont" style="display:none;"></div>').appendTo(that.$project_list_editor);
	this.$gantt_link = $('<button class="button normal show-gantt">'+ that.i18n.showTimeline + '</button>').on('click', function () {
		location.hash = that.haussmann.generateHash("GanttEditor");
	}).appendTo(that.$project_list_editor.find('.project-create-link-cont'));
	// ----------------------------------------------------------------------------------------------
	// Behavior to change project
	// ----------------------------------------------------------------------------------------------
	this.$project_list_cont.delegate('.project-list-item', 'click', function () {
		var project_id = null;
		var classList = $(this).attr('class').split(/\s+/);
		$.each(classList, function(i,e){
			if (e.indexOf('project_id_') !== -1) {
				project_id = parseInt(/([0-9]+)/.exec(e.replace('project_id_', '')), 10);
			}
			else {
				MIOGA.logDebug("ProjectListEditor", 2, 'ProjectListEditor - INIT - click on project item. CSS class : ' + e + ' dont containing project_id_');
			}
		});
		that.haussmann.current_view.type = "tree";
		location.hash = that.generateHash("ProjectEditor", project_id);
	});
	// ----------------------------------------------------------------------------------------------
	// show create project button and load behavior to do this if user access rights are granted.
	// ----------------------------------------------------------------------------------------------
	if (mioga_context.rights.WriteProjectIfOwner || mioga_context.rights.Anim) {
		this.$createBtn = $( '<button class="button normal create-project-link">' + that.i18n.createProject + '</button>')
			.prependTo(that.$project_list_editor.find('.project-create-link-cont'))
			.click(function() {
				that.$dial_create_project.dialog('open');
				return false;
			});
		// DIALOG to Create project
		this.$dial_create_project = $(
			'<div class="dialog-create-project" style="display:none;">'
				+ '<form class="form">'
					+ '<div class="form-item"><label>' + that.i18n.label + ' : </label><input name="label" type="text" placeholder="' + that.i18n.labelOfNewProject + '"/></div>'
					+ '<div class="form-item"><label>' + that.i18n.model + ' : </label><select name="model_id"><option>' + that.i18n.none + '</option></select></div>'
					+ '<div class="error"></div>'
					+ '<ul class="button_list">'
						+ '<li><input type="button" class="button create-project-btn" value="' + that.i18n.createAndEdit + '"/></li>'
						+ '<li><input type="button" class="button cancel" value="' + that.i18n.cancel + '"/></li>'
					+ '</ul>'
				+ '</form>'
			+ '</div>')
			.appendTo(that.$elem)
			.dialog({
				autoOpen : false,
				modal : true,
				width : "35em",
				title : that.i18n.createProject,
				closeOnEscape : true
			}
		);
		this.$error_cont_dial_create_project = that.$dial_create_project.find('div.error');
		this.$dial_create_project_label = that.$dial_create_project.find('[name="label"]').on ('keypress', function (evt) {
			if ((evt.keyCode == 13) && (evt.target.type !== "input")) {
				createProject ();
				return (false);
			}
		});
		this.$dial_create_project_label.click(function () {
			that.$error_cont_dial_create_project.find('p.error_label').remove();
		});
		this.$dial_create_project_model = this.$dial_create_project.find('[name="model_id"]');
		// validate
		this.$dial_create_project.find('.create-project-btn').click(function () {
			createProject();
		});
		// cancel
		this.$dial_create_project.find('.button_list .cancel').click(function () {
			that.$dial_create_project_label.val("");
			that.$dial_create_project.dialog('close');
		});
	}
	else {
		MIOGA.logDebug("ProjectListEditor", 1, "Not initialize project creation behavior according to users restrictions.");
	}
} // End ProjectEditor object
