// =============================================================================
/**
@description Create a Task object.

    var options = {
        haussmann : { .... }
    };

@class Task
@constructor
**/
// =============================================================================
function Task (options) {
	"use strict";
	var that = this;
	MIOGA.debug.Task = 0;
	MIOGA.logDebug('Task', 1, ' Task loaded - options : ', options);
	this.haussmann = options.haussmann;
	this.task_list = options.task_list;
	this.project_id = options.project_id;
	this.task_id = options.task_id;
	this.label = options.label;
	if (options.description !== undefined) {
		if (options.description !== null) {
			this.description = options.description;
		}
		else {
			this.description = "";
		}
	}
	else {
		this.description = "";
	}
	if (options.tags === null) {
		this.tags = [];
	}
	else {
		this.tags = options.tags;
	}

	this.pstart = new Date();
	if (options.pstart === null) {
		this.pstart = null;
	}
	else {
		this.pstart.parseMiogaDate(options.pstart, options.pstart);
	}
	this.pend = new Date();
	if (options.pend === null) {
		this.pend = null;
	}
	else {
		this.pend.parseMiogaDate(options.pend, options.pend);
	}
	this.project_owner_id = options.project_owner_id;
	this.project_owner_firstname = options.project_owner_firstname;
	this.project_owner_lastname = options.project_owner_lastname;
	this.project_owner_email = options.project_owner_email;
	this.delegated_user_id = options.delegated_user_id;
	this.delegated_user_firstname = options.delegated_user_firstname;
	this.delegated_user_lastname = options.delegated_user_lastname;
	this.delegated_user_email = options.delegated_user_email;
	this.current_progress = options.current_progress;
	this.current_status = options.current_status;
	this.workload = options.workload;

	this.position_tree = options.position_tree;
	this.position_list = options.position_list;
	this.position_result = options.position_result;
	this.created = new Date();
	this.created.parseMiogaDate(options.created,options.created);

	this.last_access = options.last_access;
	this.modified = options.modified;
	if (options.progress !== undefined) {
		if (options.progress === null) {
			this.progress = [];
		}
		else {
			this.progress = options.progress;
		}
	}
	else {
		this.progress = [];
	}
	if (options.status === null) {
		this.status = [];
	}
	else {
		this.status = options.status;
	}
	if (options.tags) {
		if (options.tags === null) {
			this.tags = [];
		}
		else {
			this.tags = options.tags;
		}
	}
	if (options.dstart !== undefined) {
		if (options.dstart === null) {
			this.dstart = null;
		}
		else {
			this.dstart = new Date();
			this.dstart.parseMiogaDate(options.dstart,options.dstart);
		}
	}
	else {
		this.dstart = null;
	}
	if (options.dend !== undefined) {
		if (options.dend === null) {
			this.dend = null;
		}
		else {
			this.dend = new Date();
			this.dend.parseMiogaDate(options.dend,options.dend);
		}
	}
	else {
		this.dend = null;
	}
	if (options.fineblanking !== undefined) {
		if (options.fineblanking !== null) {
			that.fineblanking = options.fineblanking;
		}
		else {
			that.fineblanking = [];
		}
	}
	else {
		that.fineblanking = [];
	}
	this.user_is_proj_owner = options.user_is_proj_owner;
	if (options.user_is_task_delegated === 1) {
		that.user_is_task_delegated = true;
	}
	else {
		that.user_is_task_delegated = false;
	}

	// Attendees

	if ($.isArray(options.attendees)) {
		MIOGA.logDebug('Task', 1, ' attendees defined ', options.attendees);
		this.attendees = options.attendees;
	}
	else {
		this.attendees = [];
	}

	// TaskNoteList
	this.taskNoteList = undefined;

	// publics method declaration
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;
	this.isEmpty = isEmpty;
	this.isUnique = isUnique;
	this.checkGlobalInfo = checkGlobalInfo;
	this.checkProgressItem = checkProgressItem;
	this.checkStatusItem = checkStatusItem;
	this.checkFineBlanking = checkFineBlanking;
	this.setUserToAccessTask = setUserToAccessTask;
	this.setGlobalInfo = setGlobalInfo;
	this.setStatus = setStatus;
	this.setProgress = setProgress;
	this.setUnitTask = setUnitTask;
	this.setFineblanking = setFineblanking;
	this.setAffectation = setAffectation;
	this.setTask = setTask;
	this.addProgressItem = addProgressItem;
	this.deleteLastProgressItem = deleteLastProgressItem;
	this.addStatusItem = addStatusItem;
	this.deleteLastStatusItem = deleteLastStatusItem;
	this.addFineblanking = addFineblanking;
	this.moveFineblanking = moveFineblanking;
	this.deleteFineblanking = deleteFineblanking;
	this.getAttendeesRowids = getAttendeesRowids;
	this.addAttendees = addAttendees;
	this.removeAttendee = removeAttendee;
	this.getTaskNoteList = getTaskNoteList;
	// -------------------------------------------------------------------------
	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================================================
	// PRIVATES METHODS
	// ===================================================================================================================
	/**
	@description Private method that initializes TaskNoteListObject and return it.
	@method getTaskNoteList
	**/
	function getTaskNoteList () {
		that.taskNoteList = new TaskNoteList({
			project_id : that.project_id,
			task_id : that.task_id,
			project_owner_id : that.project_owner_id,
			project_owner_firstname : that.project_owner_firstname,
			project_owner_lastname : that.project_owner_lastname,
			project_owner_email : that.project_owner_email,
			user_is_proj_owner : that.user_is_proj_owner,
			user_is_task_delegated : that.user_is_task_delegated
		});
		return that.taskNoteList;
	}
	/**
	@description Private method that makes global information of task formating to AJAX request and return it
	@method getGlobalInfoAttr
	@param Object args is result object
	**/
	function getGlobalInfoAttr (args) {
		MIOGA.logDebug('Task', 1, 'Task::getGlobalInfoAttr - loaded');
		var global_info_attr = {};
		// label
		if (args.label) {
			global_info_attr.label = args.label; 
		}
		else {
			global_info_attr.label = that.label;
		}
		// description
		if (args.description) {
			global_info_attr.description = args.description;
		}
		else {
			global_info_attr.description = that.description;
		}
		// pstart
		if (args.pstart !== undefined) {
			if (args.pstart !== null) {
				global_info_attr.pstart = args.pstart;
			}
			else {
				global_info_attr.pstart = null;
			}
		}
		else {
			if (that.pstart !== null) {
				global_info_attr.pstart = that.pstart.getUTCFullYear() + "-" + (that.pstart.getUTCMonth()+1) + "-" + that.pstart.getUTCDate();
			}
			else {
				global_info_attr.pstart = null;
			}
		}
		// pend
		if (args.pend !== undefined) {
			if (args.pend !== null) {
				global_info_attr.pend = args.pend;
			}
			else {
				global_info_attr.pend = null;
			}
		}
		else {
			if (that.pend !== null) {
				global_info_attr.pend = that.pend.getUTCFullYear() + "-" + (that.pend.getUTCMonth()+1) + "-" + that.pend.getUTCDate();
			}
			else {
				global_info_attr.pend = null;
			}
		}
		// workload
		if (args.workload) {
			global_info_attr.workload = args.workload;
		}
		else {
			global_info_attr.workload = that.workload;
		}
		// tags
		if (args.tags) {
			global_info_attr.tags = JSON.stringify(args.tags);
		}
		else {
			global_info_attr.tags = JSON.stringify(that.tags);
		}
		return global_info_attr;
	}
	/**
	@description Private method that makes delegation parameters of task formating to AJAX request and return it
	@method getDelegationAttr
	@param Object args is result object
	**/
	function getDelegationAttr (args) {
		MIOGA.logDebug('Task', 1, 'Task::getDelegationAttr - loaded');
		var realization_attr = {};
		// progress
		if (args.progress) {
			realization_attr.progress = JSON.stringify(args.progress);
		}
		else {
			realization_attr.progress = JSON.stringify(that.progress);
		}
		// current_progress
		if (args.current_progress) {
			realization_attr.current_progress = args.current_progress;
		}
		else {
			realization_attr.current_progress = that.current_progress;
		}
		// status
		if (args.status) {
			realization_attr.status = JSON.stringify(args.status);
		}
		else {
			realization_attr.status = JSON.stringify(that.status);
		}
		// current_status
		if (args.current_status) {
			realization_attr.current_status = args.current_status;
		}
		else {
			realization_attr.current_status = that.current_status;
		}
		return realization_attr;
	}
	/**
	@description Private method that checks date format
	@method isMiogaDate
	@param String date_str 
	@param String format match possibility of date format. for example : DD MM YYYY or YYYY DD MM
	**/
	function isMiogaDate(date_str,format) {
		MIOGA.logDebug('Task', 1, 'Task::isMiogaDate - loaded');
		var formatter = function (date_str,format) {
			// TODO check separator from format
			var arr = [];
			var date_array = [];
			var rxDatePattern1 = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
			var rxDatePattern2 = /^(\d{1,2})(\/|-)(\d{4})(\/|-)(\d{1,2})$/;
			var rxDatePattern3 = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;

			if(format === 'DD MM YYYY') {
				date_array = date_str.match(rxDatePattern3);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[1],date_array[3],date_array[5]);
				}
			}
			else if(format === 'DD YYYY MM') {
				date_array = date_str.match(rxDatePattern2);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[1], date_array[5], date_array[3]);
				}
			}
			else if (format === 'MM DD YYYY') {
				date_array = date_str.match(rxDatePattern3);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[3], date_array[1],date_array[5]);
				}
			}
			else if(format === 'MM YYYY DD') {
				date_array = date_str.match(rxDatePattern2);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[3], date_array[5], date_array[1]);
				}
			}
			else if (format === 'YYYY DD MM') {
				date_array = date_str.match(rxDatePattern1);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[5], date_array[1],date_array[3]);
				}
			}
			else if (format === 'YYYY MM DD') {
				date_array = date_str.match(rxDatePattern1);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[5], date_array[3],date_array[1]);
				}
			}
			else {
				// apply the default format
				date_array = date_str.match(rxDatePattern1);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[5], date_array[3],date_array[1]);
				}
			}
			return arr;
		};
		var date_day, date_month, date_year;
		if(date_str.length === 0) {
			return false;
		}

		var date_array = formatter(date_str, format);
		if (date_array === null || date_array === false) {
			return false;
		}
		date_day = parseInt(date_array[0], 10);
		date_month = parseInt(date_array[1], 10);
		date_year = parseInt(date_array[2], 10);
		if (date_month < 1 || date_month > 12) {
			return false;
		}
		else if (date_day < 1 || date_day > 31) {
			return false;
		}
		else if ((date_month === 4 || date_month === 6 || date_month === 9 || date_month === 11) && date_day === 31) {
			return false;
		}
		else if (date_month === 2) {
			var is_leap = ( new Date(date_year, 1, 29).getMonth() == 2 ) ? true : false;
			if (date_day > 29 || (date_day === 29 && is_leap)) {
				return false;
			}
		}
		return true;
	}

	// ===================================================================================================================
	// PUBLICS METHODS
	// ===================================================================================================================
	/**
	@description Public method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('Task', 1, 'Task::addRefreshCB - loaded');
		this.refreshCB.push( { cb : cb, context : context });
	}
	/**
	@description Public method that loads each callback.
	@method callRefreshCB
	**/
	function callRefreshCB() {
		MIOGA.logDebug('Task', 1, 'Task::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that checks if arguments length is empty. If this is it, that returns true, else returns false.
	@method isEmpty
	@param Object elem can be all type. This is the element that will be tested.
	@param String error_str that is a string value that represents the error message if tested element is empty.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isEmpty (elem, error_str, global_result) {
		MIOGA.logDebug('Task', 1, 'Task::isEmpty - loaded');
		var result = false;

		if (elem.length === 0) {
			if (global_result) {
				global_result.status = "error";
				global_result.error.push(error_str);
			}
			result = true;
		}
		return result;
	}
	/**
	@description Public method that checks if the element exists in a list. If this is it, that returns true, else returns false.
	@method isUnique
	@param Object elem can be all type. This is the element that will be searched in list.
	@param Array list. This is the list that will be matched.
	@param String error_str that is a string value that represents the error message if tested element is not unique.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isUnique (elem, list, error_str, global_result) {
		MIOGA.logDebug('Task', 1, 'Task::isUnique - loaded');
		var result = true;
		for (var i = 0; i < list.length; i++) {
			if (list[i] === elem) {
				if (global_result) {
					global_result.status = "error";
					global_result.error.push(error_str);
				}
				result = false;
				break;
			}
		}
		return result;
	}
	/**
	@description Public method that checks global information of task
	@method checkGlobalInfo
	@param Object args that is task attribute as pair key / value
	**/
	function checkGlobalInfo (args) {
		MIOGA.logDebug('Task', 1, 'Task::checkGlobalInfo - loaded');
		var result = {
			status : "success",
			error : []
		};
		var toDayy = new Date();
		var toDay = new Date(toDayy.getUTCFullYear(), toDayy.getUTCMonth(), toDayy.getUTCDate());
		var pstartMiogaDate = new Date();
		var pendMiogaDate = new Date();
		if (args.label !== undefined) {
			if (args.label !== that.label) {
				var label_list = [];
				$.each(that.task_list, function (i,e) {
					label_list.push(e.label);
				});
				isUnique(args.label, label_list, 'taskAlreadyExists', result);
				isEmpty(args.label, "labelDisallowEmpty", result);
			}
		}
		if (args.workload !== undefined) {
			if ($.isNumeric(args.workload) === false) {
				result.status = "error";
				result.error.push('workloadMustBeNum');
			}
		}
		// dates
		if (args.pstart !== undefined) {
			if (args.pstart !== null) {
				if (isMiogaDate(args.pstart) === false) {
					result.status = "error";
					result.error.push('pstartInvalid');
					pstartMiogaDate = undefined;
				}
				else {
					pstartMiogaDate.parseMiogaDate(args.pstart, args.pstart);
				}
			}
		}
		else {
			MIOGA.logError ("Task::checkGlobalInfo - pstart is undefined", true);
		}
		if (args.pend !== undefined) {
			if (args.pend !== null) {
				if (isMiogaDate(args.pend) === false) {
					result.status = "error";
					result.error.push('pendInvalid');
					pendMiogaDate = undefined;
				}
				else {
					pendMiogaDate.parseMiogaDate(args.pend, args.pend);
				}
			}
		}
		else {
			MIOGA.logError ("Task::checkGlobalInfo - pend is undefined", true);
		}
		// compare dates.
		if (pendMiogaDate !== undefined) {
			if (pstartMiogaDate !== undefined) {
				if (pendMiogaDate - pstartMiogaDate < 0) {
					result.status = "error";
					result.error.push('pendMinPstart');
				}
			}
		}
		return result;
	}
	/**
	@description Public method that checks progress item of task
	@method checkProgressItem
	@param Object args that is task attribute like pair key / value
	**/
	function checkProgressItem (args) {
		MIOGA.logDebug('Task', 1, 'Task::checkProgressItem - loaded');
		var result = {
			status : "success",
			error : []
		};
		if (args.date !== undefined) {
			if (args.date.length === 0) {
				result.status = "error";
				result.error.push('progressDateRequired');
			}
			else if (isMiogaDate(args.date) === false) {
				result.status = "error";
				result.error.push('progressDateInvalid');
			}
		}
		else {
			MIOGA.logError ("Task::checkProgressItem missing date for progress item", true);
		}
		if (args.value !== undefined) {
			if (args.value.length === 0) {
				result.status = "error";
				result.error.push('progressValueRequired');
			} else if ($.isNumeric(args.value)) {
				if (args.value < 0 || args.value > 100) {
					result.status = "error";
					result.error.push('progress_percent_error');
				}
			}
			else {
				result.status = "error";
				result.error.push('progress_percent_error');
			}
		}
		else {
			MIOGA.logError ("Task::checkProgressItem missing value for progress item", true);
		}
		return result;
	}
	/**
	@description Public method that checks status item of task
	@method checkStatusItem
	@param Object args that is task attribute as pair key / value
	**/
	function checkStatusItem (args) {
		MIOGA.logDebug('Task', 1, 'Task::checkStatusItem - loaded');
		var result = {
			status : "success",
			error : []
		};
		if (args.status !== undefined) {
			if ($.isNumeric(args.status) === false) {
				MIOGA.logError ("Task::checkStatusItem status is not a numeric value.", true);
			}
			else if (args.status.length === 0) {
				MIOGA.logError ("Task::checkStatusItem status disallow empty.", true);
			}
			else {
			}
		}
		else {
			MIOGA.logError ("Task::checkStatusItem status is required.", true);
		}

		if (args.date !== undefined) {
			if (args.date.length === 0) {
				result.status = "error";
				result.error.push('statusDateRequired');
			}
			else if (isMiogaDate(args.date) === false) {
				result.status = "error";
				result.error.push('progressDateInvalid');
			}
			else {
			}
		}
		else {
			MIOGA.logError ("Task::checkStatusItem date is required.", true);
		}
		return result;
	}
	/**
	@description Public method that checks fineblanking of task
	@method checkFineBlanking
	@param Object args that is task attribute as pair key / value
	**/
	function checkFineBlanking (args) {
		MIOGA.logDebug('Task', 1, 'Task::checkFineBlanking - loaded');
		var result = {
			status : "success",
			error : []
		};

		if (args.label === undefined || args.workload === undefined || args.checked === undefined || args.date === undefined || args.comment === undefined) {
			MIOGA.logError ("Task::checkFineBlanking missing argument.", true);
		}
		else {
			// label
			isEmpty(args.label, "labelDisallowEmpty", result);
			// workload
			if (args.workload.length > 0 && $.isNumeric(args.workload) === false) {
				result.status = "error";
				result.error.push('workloadMustBeNum');
			}
			// checked and date
			if (args.checked === true) {
				isEmpty(args.date, "dateRequired", result);
				if (args.date.length > 0 && isMiogaDate(args.date) === false) {
					result.status = "error";
					result.error.push('dateInvalid');
				}
			}
		}
		return result;
	}
	/**
	@description Public method that sets user task access.
	@method setUserToAccessTask
	**/
	function setUserToAccessTask (cb) {
		MIOGA.logDebug('Task', 1, 'Task::setUserToAccessTask - loaded');
		var params = {
			task_id : that.task_id
		};
		$.ajax({
			url : "SetUserToAccessTask.json",
			data : params,
			dataType :'json',
			type : "POST",

			success : function(response){
				if (response.status === "OK") {
					if (cb !== undefined) {
						cb ();
					}
				}
				else {
					MIOGA.logError ("Task. setUserToAccessTask AJAX request returned KO status", true);
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("Task. setUserToAccessTask AJAX request returned an error : " + err, true);
			}
		});
	}
	/**
	@description Public method that sets global informations of task
	@method setGlobalInfo
	@param Object args that is task attribute as pair key / value
	@param Function onSuccess that is callback called on set method success
	@param Function onError that is callback called on set method error
	**/
	function setGlobalInfo(args, onSuccess, onError) {
		MIOGA.logDebug('Task', 1, 'Task::setGlobalInfo - loaded');
		var check = that.checkGlobalInfo (args);
		if (check.status === "success") {
			var attr = getGlobalInfoAttr (args);
			that.setTask(attr, onSuccess);
		}
		else {
			if (onError !== undefined) {
				onError(check.error);
			}
		}
	}
	/**
	@description Public method that sets task status
	@method setStatus
	@param Function onSuccess that is callback called on set method success
	**/
	function setStatus (onSuccess) {
		MIOGA.logDebug('Task', 1, 'Task::setStatus - loaded');
		var params = {};
		params.status = JSON.stringify(that.status);
		if (that.current_status !== null) {
			params.current_status = that.current_status;
		}
		that.setTask(params, onSuccess);
	}
	/**
	@description Public method that sets task progress
	@method setProgress
	@param Function onSuccess that is callback called on set method success
	**/
	function setProgress (onSuccess) {
		MIOGA.logDebug('Task', 1, 'Task::setProgress - loaded');
		var params = {};
		params.progress = JSON.stringify(that.progress);
		if (that.current_progress !== undefined) {
			if (that.current_progress !== null) {
				params.current_progress = that.current_progress;
			}
		}
		else {
			params.current_progress = null;
		}
		that.setTask(params, onSuccess);
	}
	/**
	@description Public method that sets unit task
	@method setUnitTask
	@param Object args that is unit task that will be updated
	@param Function onSuccess that is callback called on set method success
	**/
	function setUnitTask (args, onSuccess) {
		MIOGA.logDebug('Task', 1, 'Task::setUnitTask - loaded');
		var ut_index = that.fineblanking.length -1 -args.UT_index;
		if (args.label !== undefined) {
			that.fineblanking[ut_index].label = args.label;
		}
		if (args.workload !== undefined) {
			that.fineblanking[ut_index].workload = args.workload;
		}
		if (args.comment !== undefined) {
			that.fineblanking[ut_index].comment = args.comment;
		}
		if (args.checked !== undefined) {
			that.fineblanking[ut_index].checked = args.checked;
		}
		if (args.date !== undefined) {
			that.fineblanking[ut_index].date = args.date;
		}
		that.setFineblanking (onSuccess);
	}
	/**
	@description Public method that sets fineblanking ( all unit task )
	@method setFineblanking
	@param Function onSuccess that is callback called on set method success
	**/
	function setFineblanking(onSuccess) {
		MIOGA.logDebug('Task', 1, 'Task::setFineblanking - loaded');
		var params = {
			fineblanking : JSON.stringify(that.fineblanking)
		};
		that.setTask(params, onSuccess);
	}
	/**
	@description Public method that sets task affectation
	@method setAffectation
	@param Object args that is affectation data like pair key / value : rowid, firstname, lastname, email
	@param Function onSuccess that is callback called on set method success
	**/
	function setAffectation(args, onSuccess) {
		MIOGA.logDebug('Task', 1, 'Task::setAffectation - loaded');
		var params = {};
		if (args.rowid === undefined || args.firstname === undefined || args.lastname === undefined || args.email === undefined) {
			MIOGA.logError ("Task::setAffectation missing data for user", true);
		}
		else {
			params.delegated_user_id = parseInt(args.rowid,10);
			params.delegated_user_firstname = args.firstname;
			params.delegated_user_lastname = args.lastname;
			params.delegated_user_email = args.email;
			params.dstart = null;
			params.dend = null;
			that.setTask(params, onSuccess);
		}
	}
	/**
	@description Public method that sets task
	@method setTask
	@param Object args that is task attributes like pair key / value
	@param Function onSuccess that is callback called on set method success
	**/
	function setTask(args, onSuccess) {
		MIOGA.logDebug('Task', 1, 'Task::setTask - loaded');
		var params = args;
		params.task_id = that.task_id;
		params.project_id = that.project_id;
		$.ajax({
			url : "SetTask.json",
			data : params,
			dataType :'json',
			type : "POST",
			success : function(response){
				if (response.status === "OK") {
					that.last_access = response.data.last_access;
					that.label = response.data.label;
					that.description = response.data.description || "";
					that.pstart = new Date();
					if (response.data.pstart === null) {
						that.pstart = null;
					}
					else {
						that.pstart.parseMiogaDate(response.data.pstart, response.data.pstart);
					}
					that.pend = new Date();
					if (response.data.pend === null) {
						that.pend = null;
					}
					else {
						that.pend.parseMiogaDate(response.data.pend, response.data.pend);
					}
					if (response.data.delegated_user_id !== null) {
						that.delegated_user_id = parseInt(response.data.delegated_user_id,10);
					}
					else {
						that.delegated_user_id = null;
					}
					
					that.delegated_user_firstname = response.data.delegated_user_firstname;
					that.delegated_user_lastname = response.data.delegated_user_lastname;
					that.delegated_user_email = response.data.delegated_user_email;
					that.current_progress = response.data.current_progress;
					if (response.data.current_status !== null) {
						that.current_status = parseInt(response.data.current_status,10);
					}
					else {
						that.current_status = null;
					}
					if (response.data.workload !== null) {
						that.workload = parseInt(response.data.workload,10);
					}
					else {
						that.workload = null;
					}

					that.position_tree = $.parseJSON(response.data.position_tree);
					that.position_list = $.parseJSON(response.data.position_list);
					that.position_result = $.parseJSON(response.data.position_result);

					that.modified = response.data.modified;

					if (response.data.progress === null) {
						that.progress = [];
					}
					else {
						that.progress = $.parseJSON(response.data.progress);
					}
					if (response.data.status === null) {
						that.status = [];
					}
					else {
						that.status = $.parseJSON(response.data.status);
					}
					if (response.data.tags) {
						if (response.data.tags === null) {
							that.tags = [];
						}
						else {
							that.tags = $.parseJSON(response.data.tags);
						}
					}
					if (response.data.dstart === null) {
						that.dstart = null;
					}
					else {
						that.dstart = new Date();
						that.dstart.parseMiogaDate(response.data.dstart,response.data.dstart);
					}
					if (response.data.dend === null) {
						that.dend = null;
					}
					else {
						that.dend = new Date();
						that.dend.parseMiogaDate(response.data.dend,response.data.dend);
					}
					if (response.data.fineblanking !== undefined) {
						if (response.data.fineblanking !== null) {
							that.fineblanking = $.parseJSON(response.data.fineblanking);
						}
						else {
							that.fineblanking = [];
						}
					}
					else {
						that.fineblanking = [];
					}
					
					if (onSuccess !== undefined) {
						onSuccess(that);
					}
					else {
						that.callRefreshCB();
					}
				}
				else {
					MIOGA.logError ("Task. setTask AJAX request returned KO status", true);
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("Task. setTask AJAX request returned an error : " + err, true);
			}
		});
	}
	/**
	@description Public method that adds progress item in progress list
	@method addProgressItem
	@param Object args that is task attributes like pair key / value
	@param Function cb that is callback called on adding success
	**/
	function addProgressItem (args, cb) {
		MIOGA.logDebug('Task', 1, 'Task::addProgressItem - loaded');
		var params = {
			date : args.date,
			value : args.value
		};
		that.progress.push(params);
		that.current_progress = args.value;
		if (cb !== undefined) {
			cb ();
		}
	}
	/**
	@description Public method that removes last progress item from progress list
	@method deleteLastProgressItem
	**/
	function deleteLastProgressItem () {
		MIOGA.logDebug('Task', 1, 'Task::deleteLastProgressItem - loaded');
		that.progress.pop();
		if (that.progress.length > 0) {
			that.current_progress = (that.progress.length-1);
		}
		else {
			that.current_progress = null;
		}
	}
	/**
	@description Public method that add status item in status list
	@method addStatusItem
	@param Object args that is status item like pair key / value : date, status, comment
	@param Function cb that is callback called on adding success
	**/
	function addStatusItem(args, cb) {
		MIOGA.logDebug('Task', 1, 'Task::addStatusItem - loaded');
		var params = {
			date : args.date,
			status : args.status,
			comment : args.comment
		};
		that.status.push(params);
		that.current_status = args.status;
		if (cb !== undefined) {
			cb ();
		}
	}
	/**
	@description Public method that removes last status item from status list
	@method deleteLastStatusItem
	@param Function cb that is callback called on removing success
	**/
	function deleteLastStatusItem (onSuccess) {
		MIOGA.logDebug('Task', 1, 'Task::deleteLastStatusItem - loaded');
		that.status.pop();
		if (that.status.length > 0) {
			that.current_status = (that.status.length-1);
		}
		else {
			that.current_status = null;
		}
		that.setStatus(onSuccess);
	}
	/**
	@description Public method that adds unit task item in fineblanking list
	@method addFineblanking
	@param Object args that is status item like pair key / value
	@param Function cb that is callback called on adding success
	**/
	function addFineblanking (args, cb) {
		MIOGA.logDebug('Task', 1, 'Task::addFineblanking - loaded');
		that.fineblanking.push(args);
		if (cb !== undefined) {
			cb ();
		}
	}
	/**
	@description Public method that sets position of unit task in fineblanking list
	@method moveFineblanking
	@param Object args that is status item like pair key / value : i_start and i_end
	**/
	function moveFineblanking (args) {
		MIOGA.logDebug('Task', 1, 'Task::moveFineblanking - loaded');
		var i_start = that.fineblanking.length-1-args.i_start;
		var i_end = that.fineblanking.length-1-args.i_end;
		var item = that.fineblanking.splice( i_start, 1 );
		that.fineblanking.splice(i_end, 0, item[0]);
		that.setFineblanking ();
	}
	/**
	@description Public method that removes unit task from list
	@method deleteFineblanking
	@param Object args that is status item like pair key / value
	@param Function cb that is callback called on removing success
	**/
	function deleteFineblanking (args, cb) {
		MIOGA.logDebug('Task', 1, 'Task::deleteFineblanking - loaded');
		var ut_index = that.fineblanking.length -1 -args.UT_index;
		that.fineblanking.splice( ut_index, 1 );
		that.setFineblanking (cb);
	}
	/**
	@description Public method to get list of attendees rowid
	@method getAttendeesRowids
	**/
	function getAttendeesRowids () {
		MIOGA.logDebug('Task', 1, 'Task::getAttendeesRowids ');
		var rowids = [];
		$.each(that.attendees, function(idx, item) {
			rowids.push(item.user_id);
		});
		return rowids;
	}
	/**
	@description Public method to add attendees
	@method addAttendees
	**/
	function addAttendees (rowids, cb) {
		MIOGA.logDebug('Task', 1, 'Task::addAttendees ');

		var data = {project_id : that.project_id, task_id : that.task_id, rowids : JSON.stringify(rowids)};
		MIOGA.logDebug('Task', 1, 'data = ', data);
		$.ajax({
			url : "AddAttendees.json"
			,data : data
			,dataType :'json'
			,type : "POST"

			,success : function(response){
				if (response.status === "OK") {
					that.attendees = response.data;
					if (cb !== undefined) {
						cb ();
					}
				}
				else {
					MIOGA.logError ("Task. setUserToAccessTask AJAX request returned KO status", true);
				}
			}
			,error: function(err, ioArgs) {
				MIOGA.logError ("Task. setUserToAccessTask AJAX request returned an error : " + err, true);
			}
		});
	}
	/**
	@description Public method to remove attendee
	@method removeAttendees
	@param user_id rowid for attendee to remove
	**/
	function removeAttendee (user_id, cb) {
		MIOGA.logDebug('Task', 1, 'Task::removeAttendees ');

		var data = {project_id : that.project_id, task_id : that.task_id, user_id : user_id};
		MIOGA.logDebug('Task', 1, 'data = ', data);
		$.ajax({
			url : "RemoveAttendee.json"
			,data : data
			,dataType :'json'
			,type : "POST"

			,success : function(response){
				if (response.status === "OK") {
					MIOGA.logDebug('Task', 1, 'attendees ', response.data);
					that.attendees = response.data;
					if (cb !== undefined) {
						cb ();
					}
				}
				else {
					MIOGA.logError ("Task. setUserToAccessTask AJAX request returned KO status", true);
				}
			}
			,error: function(err, ioArgs) {
				MIOGA.logError ("Task. setUserToAccessTask AJAX request returned an error : " + err, true);
			}
		});
	}



	if (
		this.project_id === undefined || this.task_id === undefined || 
		this.label === undefined || this.description === undefined || 
		this.pstart === undefined || this.pend === undefined || 
		this.project_owner_id === undefined || this.project_owner_firstname === undefined || 
		this.project_owner_lastname === undefined || this.project_owner_email === undefined || 
		this.current_progress === undefined || this.current_status === undefined || 
		this.workload === undefined || this.position_tree === undefined || 
		this.position_list === undefined || this.position_result === undefined || 
		this.created === undefined || this.modified === undefined || 
		this.progress === undefined || this.status === undefined || 
		this.tags === undefined || this.fineblanking === undefined
		) {
		MIOGA.logError ("Task. Object can not be initialized.", false);
		return undefined;
	}
} // End Task object
