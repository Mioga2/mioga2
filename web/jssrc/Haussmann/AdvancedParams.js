/**
@name AdvancedParams
@type Object
@author developers@mioga2.org

@description This object that manages the advanced parameters of a project and models.

@param Object
	settings An object literal containing key/value pairs to provide optional settings.
		@option Object haussmann that is haussmann object.
		@option Object modelList that is model list object that containing model list according to group id.
		@option Int model_id that is the id of model associated to project
		
		@option Array result_status that is the list of result status labels.
		@option Array task_status that is the list of task status labels.
		@option Array project_status that is the list of project status labels.
		@option Array complementary_data that is the list of complementary data label and type.
		@option Object grouping_tasks that containing grouping tree list of Sting and grouping list. The grouping list can contains a sub grouping list like :
				... grouping_list : {
						label : "gpl_1,
						values : ["sub_gp_1", "sub_gp_2"]
					}
		@option Object vocabulary that is a reference come from haussmann.
**/
function AdvancedParams (options) {
	"use strict";
	MIOGA.debug.AdvancedParams = 0;
	MIOGA.logDebug('AdvancedParams', 1, ' AdvancedParams loaded');

	var that = this;
	this.haussmann = options.haussmann;
	this.model_id = options.model_id;
	this.modelList = options.modelList;
	this.modelList.checkAdvancedParameters = that.checkAdvancedParameters;
	this.result_status = options.result_status || [];
	this.task_status = options.task_status || [];
	this.project_status = options.project_status || [];
	this.complementary_data = options.complementary_data || [];
	this.grouping_tasks = {
		"grouping_tree": [],
		"grouping_list": []
	};
	if (options.grouping_tasks) {
		if (options.grouping_tasks.grouping_tree) {
			that.grouping_tasks.grouping_tree = options.grouping_tasks.grouping_tree;
		}
		if (options.grouping_tasks.grouping_list) {
			that.grouping_tasks.grouping_list = options.grouping_tasks.grouping_list;
		}
	}
	if (options.vocabulary !== undefined) {
		this.vocabulary = options.vocabulary;
	}
	else {
		this.vocabulary = that.haussmann.vocabulary;
	}

	this.refreshCB = [];
	// Constructor end ================================
	// =================================================================================================
	// Privates methods for Object
	// =================================================================================================
	/**
	@description Private method that checks if arguments length is empty. If this is it, that returns true, else returns false.

	@method isEmpty
	@param Object elem can be all type. This is the element that will be tested.
	@param String error_str that is a string value that represents the error message if tested element is empty.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isEmpty (elem, error_str, global_result) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::isEmpty - loaded');
		var result = false;
		if (elem.length === 0) {
			if (global_result) {
				global_result.status = "error";
				global_result.error.push(error_str);
			}
			result = true;
		}
		return result;
	}
	/**
	@description Private method that checks if the element exists in a list. If this is it, that returns true, else returns false.

	@method isUnique
	@param Object elem can be all type. This is the element that will be searched in list.
	@param Array list. This is the list that will be matched.
	@param String error_str that is a string value that represents the error message if tested element is not unique.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isUnique (elem, list, error_str, global_result) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::isUnique - loaded');
		var result = true;
		for (var i = 0; i < list.length; i++) {
			if (list[i] === elem) {
				if (global_result) {
					global_result.status = "error";
					global_result.error.push(error_str);
				}
				result = false;
				break;
			}
		}
		return result;
	}
	/**
	@description Private method that verify item of list. For to do this, it checks if element is empty and is unique to the list. It return JSON structure like : 
					{
						status : "success" / "error",
						error : ["error_message_1", "error_message_2"]
					}

	@method checkList
	@param Object args settings An object literal containing key/value pairs to provide optional settings. It return JSON structure like :
			{
				status : "success" / "error",
				error : ["error_message_1", "error_message_2"]
			}
		@option Object can be all type. This is element that will be tested.
		@option Array list that is a list that will be matched for test.
		@param String empty_error_str that is a string value that represents the error message if tested element is empty.
		@param String unique_error_str that is a string value that represents the error message if tested element is not unique.
	**/
	function checkList (elem, list, empty_error_str, unique_error_str) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::checkList - loaded');
		var result = {
			status : "success",
			error : []
		};
		isEmpty(elem, empty_error_str, result);
		isUnique (elem, list, unique_error_str, result);
		return result;
	};
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	/**
	@description Public method that checks data of vocabulary ( call of checkVocaulary method ) and data of grouping tasks. It return JSON structure like:
					{
						status : "success" / "error",
						error : ["error_message_1", "error_message_2"]
					}

	@method checkAdvancedParameters
	@param Object args settings An object literal containing key/value pairs to provide optional settings.
		@option Object vocabulary that is vocabulary structure defined in Haussmann object
		@option Object grouping_tasks settings An object literal containing key/value pairs.
			@option Array grouping_tree containing labels of grouping tree.
			@option Array grouping_list settings containing list of grouping list like :
				{
					label : "gpl_1,
					values : ["sub_gp_1", "sub_gp_2"]
				}
	**/
	this.checkAdvancedParameters = checkAdvancedParameters;
	function checkAdvancedParameters (args) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::checkAdvancedParameters - loaded');
		var result = {
			status : "success",
			error : []
		};
		if (args.vocabulary) {
			var checkVoca = that.checkVocabulary (args.vocabulary);
			if (checkVoca.status === "error") {
				result.status = "error";
				for (var i=0; i < checkVoca.error.length; i++) {
					result.error.push(checkVoca.error[i]);
				}
			}
		}
		else {
			MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::checkAdvancedParameters - vocabulary arg is not defined');
		}
		if (args.grouping_tasks) {
			var grouping_list = args.grouping_tasks.grouping_list;
			var tmp_grouping_list = {};
			$.each(grouping_list, function (i,e) {
				var tmp_grouping_sub_list = {};
				tmp_grouping_list[e.label] = (tmp_grouping_list[e.label] +1) || 0;
				if (e.values.length > 0) {
					$.each(e.values, function (ind,el) {
						tmp_grouping_sub_list[el] = (tmp_grouping_sub_list[el] +1) || 0;
					});
					for (var j=0; j < tmp_grouping_sub_list.length; j++) {
						if (tmp_grouping_sub_list[j] === 1) {
							result.status = "error";
							result.error.push("groupingSubListAlreadyExists");
							break;
						}
					}
				}
				else {
					result.status = "error";
					result.error.push("mustBeSubList");
				}
			});
			for (var i=0; i < tmp_grouping_list.length; i++) {
				if (tmp_grouping_list[i] === 1) {
					result.status = "error";
					result.error.push("groupingListAlreadyExists");
					break;
				}
			}
		}
		else {
			MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::checkAdvancedParameters - grouping_tasks arg is not defined');
		}
		return result;
	}
	
	/**
	@description Public method that checks structure of vocabulary data. It calls the haussman's method for to do this.

	@method checkVocabulary
	@param Object voc that is a vocabulary data. The structure is defined in Haussmann object.
	**/
	this.checkVocabulary = checkVocabulary;
	function checkVocabulary (voc) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::checkVocabulary - loaded');
		var result = that.haussmann.checkVocabulary(voc);
		return result;
	}
	// -----------------------------------------------------
	/**
	@description Public method that checks structure of vocabulary data. It calls the haussman's method for to do this.

	@method setVocabulary
	@param Object voc that is a vocabulary data. The structure is defined in Haussmann object.
	@param Function onSuccess that is callback called on success updated.
	@param Function onError that is callback called on error updated.
	**/
	this.setVocabulary = setVocabulary;
	function setVocabulary (voca, onSuccess, onError) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::setVocabulary - loaded');
		var canSetVocabulary = that.haussmann.checkVocabulary(voca);
		if (canSetVocabulary.status === "success") {
			that.haussmann.setVocabulary(voca, onSuccess);
		}
		else {
			onError (canSetVocabulary.error);
		}
		return canSetVocabulary;
	}
	/**
	@description Public method that call createModel method of modelList object for add a model.

	@method createModel
	@param Boolean async that determines if AJAX request will be synchrone or asynchrone.
	@param Object params that containing model parameters
	
	
	@param Function onSuccess that is callback called on success created.
	**/
	this.createModel = createModel;
	function createModel (async, params, onSuccess) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::createModel - loaded');
		that.modelList.createModel(async, params, onSuccess);
	}
	/**
	@description Public method that call setModel method of model object for modify a model.

	@method setModel
	@param Boolean async that determines if AJAX request will be synchrone or asynchrone.
	@param Object params that containing model parameters
	
	@param Function onSuccess that is callback called on success modify.
	**/
	this.setModel = setModel;
	function setModel (async, params, onSuccess) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::setModel - loaded');
		that.modelList.model_list_by_rowid[params.model_id].setModel(async, params, onSuccess);
	}
	/**
	method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	this.addRefreshCB = addRefreshCB;
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::addRefreshCB - loaded');
		this.refreshCB.push( { cb : cb, context : context });
	}
	/**
	method that loads each callback.
	@method callRefreshCB
	**/
	this.callRefreshCB = callRefreshCB;
	function callRefreshCB() {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that attributes of AdvancedParams object and return it.

	@method cloneAttr
	**/
	this.cloneAttr = cloneAttr;
	function cloneAttr () {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::cloneAttr - loaded');
		var attr = {};
		attr.result_status = $.extend(true,[], that.result_status);
		attr.task_status = $.extend(true,[], that.task_status);
		attr.project_status = $.extend(true,[], that.project_status);
		attr.complementary_data = [];
		$.each(that.complementary_data, function (i,e) {
			attr.complementary_data.push({
				label : e.label,
				type : e.type
			});
		});
		attr.grouping_tasks = {
			"grouping_tree": [],
			"grouping_list": []
		};
		attr.grouping_tasks.grouping_tree = $.extend(true, [], that.grouping_tasks.grouping_tree);
		attr.grouping_tasks.grouping_list = [];
		$.each(that.grouping_tasks.grouping_list, function (ind,elm) {
			var list = {
				label : elm.label,
				values : []
			};
			$.each(elm.values, function (i,e) {
				list.values.push(e);
			});
			attr.grouping_tasks.grouping_list.push(list);
		});
		
		attr.vocabulary = that.haussmann.cloneVocabulary ();
		attr.haussmann = that.haussmann;
		attr.model_id = that.model_id;
		attr.modelList = that.modelList;
		attr.modelList.checkAdvancedParameters = that.checkAdvancedParameters;
		return attr;
	}
	/**
	@description Public method that serialize advanced parmaeters attributes like JSON structure and return it.
	@method serializeAttr
	**/
	this.serializeAttr = serializeAttr;
	function serializeAttr () {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::serializeAttr - loaded');
		var attr_str = {};
		attr_str.result_status = that.result_status;
		attr_str.task_status = that.task_status;
		attr_str.project_status = that.project_status;
		attr_str.complementary_data = that.complementary_data;
		attr_str.grouping_tasks = that.grouping_tasks;
		attr_str.vocabulary = that.vocabulary;

		return attr_str;
	}
	/**
	@description Public method that checks a result status item according to result_status list. It return JSON structure like :
					{
						status : "success" / "error",
						error : ["error_message_1", "error_message_2"]
					}

	@method checkResultStatus
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING"}
	**/
	this.checkResultStatus = checkResultStatus;
	function checkResultStatus(args) {
		return checkList (args.label, that.result_status, "resultStatusDisallowEmpty", "resultStatusAlreadyExists");
	}
	/**
	@description Public method that adds result status of result_status list.

	@method addResultStatus
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING"}
	**/
	this.addResultStatus = addResultStatus;
	function addResultStatus (args) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::addResultStatus loaded');
		that.result_status.push(args.label);
	}
	/**
	@description Public method that re makes result_status list according to new values.

	@method updateResultStatusList
	@param Array list that is a list of object like : [ { label : "my_label_1" } , { label : "my_label_2"} ]
	**/
	this.updateResultStatusList = updateResultStatusList;
	function updateResultStatusList (list) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::updateResultStatusList loaded');
		that.result_status = [];
		for (var i = 0; i < list.length; i++) {
			addResultStatus({label : list[i].label});
		}
	}
	/**
	@description Public method that check a task status item according to task_status list. It return JSON structure like :
					{
						status : "success" / "error",
						error : ["error_message_1", "error_message_2"]
					}
	@method checkTaskStatus
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING"}
	**/
	this.checkTaskStatus = checkTaskStatus;
	function checkTaskStatus(args) {
		return checkList (args.label, that.task_status, "taskStatusDisallowEmpty", "taskStatusAlreadyExists");
	}
	/**
	@description Public method that adds task status of task_status list.

	@method addTaskStatus
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING"}
	**/
	this.addTaskStatus = addTaskStatus;
	function addTaskStatus (args) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::addTaskStatus loaded');
		that.task_status.push(args.label);
	}
	/**
	@description Public method that re makes task_status list according to new values.

	@method updateTaskStatusList
	@param Array list that is a list of object like : [ { label : "my_label_1" } , { label : "my_label_2"} ]
	**/
	this.updateTaskStatusList = updateTaskStatusList;
	function updateTaskStatusList (list) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::updateTaskStatusList loaded');
		that.task_status = [];
		for (var i = 0; i < list.length; i++) {
			that.task_status.push(list[i].label);
		}
	}
	/**
	@description Public method that checks a project status item according to project_status list. It return JSON structure like :
					{
						status : "success" / "error",
						error : ["error_message_1", "error_message_2"]
					}

	@method checkProjectStatus
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING"}
	**/
	this.checkProjectStatus = checkProjectStatus;
	function checkProjectStatus(args) {
		return checkList (args.label, that.project_status, "projectStatusDisallowEmpty", "projectStatusAlreadyExists");
	}
	/**
	@description Public method that adds project status of project_status list.

	@method addProjectStatus
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING"}
	**/
	this.addProjectStatus = addProjectStatus;
	function addProjectStatus (args) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::addProjectStatus loaded');
		that.project_status.push(args.label);
	}
	/**
	@description Public method that re makes project_status list according to new values.

	@method updateProjectStatusList
	@param Array list that is a list of object like : [ { label : "my_label_1" } , { label : "my_label_2"} ]
	**/
	this.updateProjectStatusList = updateProjectStatusList;
	function updateProjectStatusList (list) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::updateProjectStatusList loaded');
		that.project_status = [];
		for (var i = 0; i < list.length; i++) {
			that.project_status.push(list[i].label);
		}
	}
	/**
	@description Public method that check a complementary data item according to complementary_data list. It return JSON structure like :
					{
						status : "success" / "error",
						error : ["error_message_1", "error_message_2"]
					}

	@method checkComplementaryData
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING"}
	**/
	this.checkComplementaryData = checkComplementaryData;
	function checkComplementaryData(args) {
		var label_list = [];
		for (var i = 0; i < that.complementary_data.length; i++) {
			label_list.push(that.complementary_data[i].label);
		}
		return checkList (args.label, label_list, "complementaryDataDisallowEmpty", "complementaryDataAlreadyExists");
	}
	/**
	@description Public method that adds complementary data of complementary_data list.

	@method addComplementaryData
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING", type : INT}
	**/
	this.addComplementaryData = addComplementaryData;
	function addComplementaryData (args) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::addComplementaryData loaded');
		var args_add = {
			label : args.label,
			type : args.type
		};
		that.complementary_data.push(args_add);
	}
	/**
	@description Public method that re makes complementary data list according to new values.

	@method updateComplementaryDataList
	@param Array list that is a list of object like : [ { label : "my_label_1" , type : 1} , { label : "my_label_2" , type : 2} ]
	**/
	this.updateComplementaryDataList = updateComplementaryDataList;
	function updateComplementaryDataList (list) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::updateComplementaryDataList loaded');
		that.complementary_data = [];
		for (var i = 0; i < list.length; i++) {
			that.complementary_data.push({
				label : list[i].label,
				type : list[i].type
			});
		}
	}
	/**
	@description Public method that checks a grouping tree item according to grouping_tasks.grouping_tree list. It return JSON structure like :
					{
						status : "success" / "error",
						error : ["error_message_1", "error_message_2"]
					}

	@method checkGroupingTree
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING"}
	**/
	this.checkGroupingTree = checkGroupingTree;
	function checkGroupingTree(args) {
		return checkList (args.label, that.grouping_tasks.grouping_tree, "groupingTreeDisallowEmpty", "groupingTreeAlreadyExists");
	}
	/**
	@description Public method that adds grouping tree of grouping_tasks.grouping_tree list.

	@method addGroupingTree
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING"}
	**/
	this.addGroupingTree = addGroupingTree;
	function addGroupingTree (args) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::addGroupingTree loaded');
		that.grouping_tasks.grouping_tree.push(args.label);
	}
	/**
	@description Public method that re makes grouping tree list according to new values.

	@method updateGroupingTreeList
	@param Array list that is a list of object like : [ { label : "my_label_1" } , { label : "my_label_2"} ]
	**/
	this.updateGroupingTreeList = updateGroupingTreeList;
	function updateGroupingTreeList (list) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::updateGroupingTreeList loaded');
		that.grouping_tasks.grouping_tree = [];
		for (var i = 0; i < list.length; i++) {
			that.grouping_tasks.grouping_tree.push(list[i].label);
		}
	}
	/**
	@description Public method that checks a grouping list item according to grouping_tasks.grouping_list list. It return JSON structure like :
					{
						status : "success" / "error",
						error : ["error_message_1", "error_message_2"]
					}
	@method checkGroupingList
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING"}
	**/
	this.checkGroupingList = checkGroupingList;
	function checkGroupingList (args) {
		var groupingLabelList = [];
		$.each(that.grouping_tasks.grouping_list, function (i,e) {
			groupingLabelList.push(e.label);
		});
		return checkList (args.label, groupingLabelList, "groupingListDisallowEmpty", "groupingListAlreadyExists");
	}
	/**
	@description Public method that adds grouping list of grouping_tasks.grouping_list list.
	@method addGroupingList
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING"}
	**/
	this.addGroupingList = addGroupingList;
	function addGroupingList (args) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::addGroupingList loaded');
		that.grouping_tasks.grouping_list.push({
			label : args.label,
			values : []
		});
	}
	/**
	@description Public method that re makes grouping list label according to new labels.
	@method updateGroupingTreeList
	@param Array list that is a list of object like : [ { label : "my_label_1", values : [...] } , { label : "my_label_2"}, values : [...] ]
	**/
	this.updateGroupingList = updateGroupingList;
	function updateGroupingList (list) {
		MIOGA.logDebug('AdvancedParams', 1, ' updateGroupingList loaded');
		that.grouping_tasks.grouping_list = [];
		$.each( list, function (i,e) {
			that.grouping_tasks.grouping_list.push({
				label : e.label,
				values : []
			});
			$.each(e.values, function (ind, el) {
				that.grouping_tasks.grouping_list[i].values.push(el.label);
			});
		});
	}
	/**
	@description Public method that check a sub grouping list item according to grouping_tasks.grouping_list[index].values list. It return JSON structure like :
					{
						status : "success" / "error",
						error : ["error_message_1", "error_message_2"]
					}
	@method checkSubGroupingList
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING"}
	**/
	this.checkSubGroupingList = checkSubGroupingList;
	function checkSubGroupingList(args) {
		var indexList = null;
		var list = [];
		for (var i = 0; i<that.grouping_tasks.grouping_list.length; i++) {
			if (that.grouping_tasks.grouping_list[i].label === args.parent_label) {
				indexList = i;
				list = that.grouping_tasks.grouping_list[indexList].values;
				break;
			}
		}
		return checkList (args.label, list, "groupingSubListDisallowEmpty", "groupingSubListAlreadyExists");
	}
	/**
	@description Public method that re makes sub grouping list according to new values.
	@method updateSubGroupingList
	@param Object args settings An object literal containing key/value pairs.
		@option String parent_label that the label of grouping list that containing this sub grouping list.
		@option Array list that is a sub grouping list
			@option String that are a labels.
	**/
	this.updateSubGroupingList = updateSubGroupingList;
	function updateSubGroupingList (args) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::updateSubGroupingList loaded');
		var indexList = null;
		for (var i = 0; i<that.grouping_tasks.grouping_list.length; i++) {
			if (that.grouping_tasks.grouping_list[i].label === args.parent_label) {
				indexList = i;
				break;
			}
		}
		that.grouping_tasks.grouping_list[indexList].values = [];
		$.each(args.list, function (ind,elem) {
			that.grouping_tasks.grouping_list[indexList].values.push(elem.label);
		});
	}
	/**
	@description Public method that adds sub grouping list of grouping_tasks.grouping_list[index].values list.

	@method addSubGroupingList
	@param Object args settings An object literal containing key/value pairs to provide optional settings like : { label : "STRING", parent_label : "STRING"}
	**/
	this.addSubGroupingList = addSubGroupingList;
	function addSubGroupingList (args) {
		MIOGA.logDebug('AdvancedParams', 1, 'AdvancedParams::addSubGroupingList loaded');
		var indexList = null;
		var label = args.label;
		for (var i = 0; i<that.grouping_tasks.grouping_list.length; i++) {
			if (that.grouping_tasks.grouping_list[i].label === args.parent_label) {
				indexList = i;
				break;
			}
		}
		that.grouping_tasks.grouping_list[indexList].values.push(label);
	}
} // End of AdvanceParams object
