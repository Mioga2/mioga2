// =============================================================================
/**
@description Create a Project object.

    var options = {
        haussmann : { .... }
    };

@class ProjectList
@constructor
**/
// =============================================================================
function Project (options) {
	"use strict";
	MIOGA.debug.Project = 0;
	MIOGA.logDebug('Project', 1, ' Project loaded. options :', options);
	var that = this;
	this.haussmann = options.haussmann;
	this.insertVocabulary = options.haussmann.insertVocabulary;
	this.user_preferences = options.user_preferences || {};
	
	this.user_is_proj_owner = false;

	this.projectIsUnique = options.projectIsUnique;

	this.modelList = options.modelList;
	this.taskList = {};
	this.resultList = {};

	this.project_id = options.project_id;
	this.project_owner_id = parseInt(options.project_owner_id,10);
	this.project_owner_firstname = options.project_owner_firstname;
	this.project_owner_lastname = options.project_owner_lastname;
	this.project_owner_email = options.project_owner_email;
	this.label = options.label;
	if (options.description !== undefined) {
		if (options.description !== null) {
			this.description = options.description;
		}
		else {
			this.description = "";
		}
	}
	else {
		this.description = "";
	}

	this.created = {};
	this.modified = {};

	this.started = {};
	this.planned = {};
	this.ended = {};

	this.base_dir = "";
	this.model_id = null;
	this.advanced_params = options.advanced_params;

	this.use_result = true;
	this.status = [];
	this.tags = [];
	this.complementary_data = [];
	this.task_tree = [];
	this.last_id_tree_task = 0;
	this.group_ident = options.group_ident || "";

	this.bgcolor = undefined;
	this.color = undefined;
	this.refreshCB = [];
	
	// publics method declaration
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;
	this.hasProjectStatus = hasProjectStatus;
	this.getProjectTaskTreeIndex = getProjectTaskTreeIndex;
	this.checkStatusItem = checkStatusItem;
	this.checkProjectParams = checkProjectParams;
	this.load = load;
	this.deleteLastStatusItem = deleteLastStatusItem;
	this.addStatusItem = addStatusItem;
	this.setProject = setProject;
	this.setProjectAdvancedParams = setProjectAdvancedParams;
	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================================================
	// PRIVATES METHODS
	// ===================================================================================================================
	/**
	@description Private method that refreshes project status label.

	@method updateStatusStr
	@param Object opt is JSON object can be containing String represents advanced_params of project and Int represents current_status of project
	**/
	function updateStatusStr (opt) {
		if (opt.advanced_params !== undefined && opt.advanced_params !== null) {
			that.advanced_params = $.parseJSON(opt.advanced_params);
			if (that.advanced_params.project_status.length > 0 && opt.current_status !== undefined && opt.current_status !== null) {
				that.current_status_str = that.advanced_params.project_status[opt.current_status];
			}
			else {
				that.current_status_str = that.haussmann.i18n.undefined_status;
			}
		}
		else {
			that.current_status_str = that.haussmann.i18n.undefined_status;
		}
	}
	/**
	@description Private method that checks if arguments length is empty. If this is it, that returns true, else returns false.

	@method isEmpty
	@param Object elem can be all type. This is the element that will be tested.
	@param String error_str that is a string value that represents the error message if tested element is empty.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isEmpty (elem, error_str, global_result) {
		MIOGA.logDebug('Project', 1, 'Project::isEmpty - loaded');
		var result = false;
		
		if (elem.length === 0) {
			if (global_result) {
				global_result.status = "error";
				global_result.error.push(error_str);
			}
			result = true;
		}
		return result;
	}
	/**
	@description Private method that checks date format
	@method isMiogaDate
	@param String date_str 
	@param String format match possibility of date format. for example : DD MM YYYY or YYYY DD MM
	**/
	function isMiogaDate(date_str,format) {
		MIOGA.logDebug('Project', 1, 'Project::isMiogaDate - loaded');
		var formatter = function (date_str,format) {
			// TODO check separator from format
			var arr = [];
			var date_array = [];
			var rxDatePattern1 = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
			var rxDatePattern2 = /^(\d{1,2})(\/|-)(\d{4})(\/|-)(\d{1,2})$/;
			var rxDatePattern3 = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;

			if(format === 'DD MM YYYY') {
				date_array = date_str.match(rxDatePattern3);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[1],date_array[3],date_array[5]);
				}
			}
			else if(format === 'DD YYYY MM') {
				date_array = date_str.match(rxDatePattern2);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[1], date_array[5], date_array[3]);
				}
			}
			else if (format === 'MM DD YYYY') {
				date_array = date_str.match(rxDatePattern3);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[3], date_array[1],date_array[5]);
				}
			}
			else if(format === 'MM YYYY DD') {
				date_array = date_str.match(rxDatePattern2);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[3], date_array[5], date_array[1]);
				}
			}
			else if (format === 'YYYY DD MM') {
				date_array = date_str.match(rxDatePattern1);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[5], date_array[1],date_array[3]);
				}
			}
			else if (format === 'YYYY MM DD') {
				date_array = date_str.match(rxDatePattern1);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[5], date_array[3],date_array[1]);
				}
			}
			else {
				// apply the default format
				date_array = date_str.match(rxDatePattern1);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[5], date_array[3],date_array[1]);
				}
			}
			return arr;
		};
		var date_day, date_month, date_year;
		if(date_str.length === 0) {
			return false;
		}

		var date_array = formatter(date_str, format);
		if (date_array === null || date_array === false) {
			return false;
		}
		date_day = parseInt(date_array[0], 10);
		date_month = parseInt(date_array[1], 10);
		date_year = parseInt(date_array[2], 10);
		if (date_month < 1 || date_month > 12) {
			return false;
		}
		else if (date_day < 1 || date_day > 31) {
			return false;
		}
		else if ((date_month === 4 || date_month === 6 || date_month === 9 || date_month === 11) && date_day === 31) {
			return false;
		}
		else if (date_month === 2) {
			var is_leap = ( new Date(date_year, 1, 29).getMonth() == 2 ) ? true : false;
			if (date_day > 29 || (date_day === 29 && is_leap)) {
				return false;
			}
		}
		return true;
	}
	/**
	@description Private method that read tree_task attribute and return the largest number of last_id_tree_task key.
	@method getLastIdTreeTask
	**/
	function getLastIdTreeTask () {
		var result = 0;
		function readTaskTree (tab, level) {
			for (var item=0; item < tab.length; item ++) {
				if (parseInt(tab[item].task_group_id,10) >= result) {
					result = (parseInt(tab[item].task_group_id,10) + 1);
				}
				readTaskTree(tab[item].children, (level+1));
			}
		}
		readTaskTree(that.task_tree, 0);
		return result;
	}
	
	/**
	@description Private method that makes index of task_tree by task_group_id and return it
	@method makeIndex
	@param Array tt is task_tree
	@param Int lev is level of grouping_tree
	**/
	function makeIndex (tt, lev) {
		var result = {};
		var tab = tt;
		var level = lev;
		function readTaskTree (array, current_level) {
			for (var item=0; item < array.length; item ++) {
				if (result[parseInt(array[item].task_group_id,10)]) {
					result[parseInt(array[item].task_group_id,10)] = 2;
				}
				else {
					result[parseInt(array[item].task_group_id,10)] = 1;
				}
				readTaskTree(array[item].children, (current_level+1));
			}
		}
		readTaskTree (tab, level);
		return result;
	}
	/**
	@description Private method that checks task_tree validity and updates value of task_group_id if needed.
	@method checkTaskTree
	**/
	function checkTaskTree () {
		var last_id = getLastIdTreeTask ();
		var tt_by_task_group_id = {};
		function readTaskTree (tab, level) {
			for (var item=0; item < tab.length; item ++) {
				if (tt_by_task_group_id[parseInt(tab[item].task_group_id,10)] === 2) {
					tab[item].task_group_id = (last_id +1);
					last_id ++;
					that.last_id_tree_task ++;
				}
				readTaskTree(tab[item].children, (level+1));
			}
		}
		tt_by_task_group_id = makeIndex (that.task_tree,0);
		readTaskTree(that.task_tree, 0);
	}
	/**
	@description Private method that parses parameters and re-affect to project attributes
	@method parseAttributes
	@param Object attr that contains project attributes
	@Function cb is a call back called at end method
	**/
	function parseAttributes (attr, cb) {
		MIOGA.logDebug('Project', 1, 'Project::parseAttributes - loaded');
		//created
		that.created = new Date();
		that.created.parseMiogaDate(attr.created, attr.created);
		// modified
		that.modified = new Date();
		that.modified.parseMiogaDate(attr.modified, attr.modified);
		// started
		that.started = new Date();
		that.started.parseMiogaDate(attr.started, attr.started);
		// planned
		that.planned = new Date();
		that.planned.parseMiogaDate(attr.planned,attr.planned);
		// ended
		if (attr.ended !== null) {
			that.ended = new Date();
			that.ended.parseMiogaDate(attr.ended, attr.ended);
		}
		else {
			that.ended = null;
		}
		that.label = attr.label;
		if (attr.description !== undefined) {
			if (attr.description !== null) {
				that.description = attr.description;
			}
			else {
				that.description = "";
			}
		}
		else {
			that.description = "";
		}
		
		if (attr.use_result === 0) {
			that.use_result = false;
		}
		else {
			that.use_result = true;
		}
		that.base_dir = "";
		if ( attr.base_dir !== null ){
			that.base_dir = attr.base_dir;
		}
		if (attr.model_id !== null) {
			that.model_id = parseInt(attr.model_id,10);
		}
		else {
			that.model_id = null;
		}
		// current status
		if (attr.current_status !== undefined) {
			if (attr.current_status !== null ) {
				that.current_status = parseInt(attr.current_status,10);
			}
			else {
				that.current_status = null;
			}
		}
		else {
			that.current_status = null;
		}
		// history status
		if (attr.status === null || attr.status.length === 0) {
			that.status = [];
		}
		else {
			that.status = $.parseJSON(attr.status);
		}
		// tags
		if (attr.tags === null) {
			that.tags = [];
		}
		else {
			that.tags = $.parseJSON(attr.tags);
		}
		// complementary data
		if (attr.complementary_data === null) {
			that.complementary_data = [];
		}
		else {
			that.complementary_data = $.parseJSON(attr.complementary_data);
		}
		// task_tree
		if (attr.task_tree === null) {
			that.task_tree = [];
			that.last_id_tree_task = getLastIdTreeTask ();
		}
		else {
			that.task_tree = $.parseJSON(attr.task_tree);
			that.last_id_tree_task = getLastIdTreeTask ();
			checkTaskTree ();
		}

		that.color = attr.color;
		that.bgcolor = attr.bgcolor;
		MIOGA.logDebug('Project', 1, 'Project::parseAttributes - end, project : ', that);
		if (cb !== undefined) {
			cb(that);
		}
	}
	// ===================================================================================================================
	// PUBLICS METHODS
	// ===================================================================================================================
	/**
	@description Public method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('Project', 1, 'Project::addRefreshCB - loaded');
		this.refreshCB.push( { cb : cb, context : context });
	}
	/**
	@description Public method that loads each callback.
	@method callRefreshCB
	**/
	function callRefreshCB() {
		MIOGA.logDebug('Project', 1, 'Project::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that tests if exists a current status
	@method hasProjectStatus
	**/
	function hasProjectStatus () {
		MIOGA.logDebug('Project', 1, 'Project::hasProjectStatus - loaded');
		if (that.current_status !== null) {
			MIOGA.logDebug('Project', 1, 'Project::hasProjectStatus - return true');
			return true;
		}
		else {
			MIOGA.logDebug('Project', 1, 'Project::hasProjectStatus - return false');
			return false;
		}
	}
	
	/**
	@description Public method that makes index of task_tree by task_group_id
	@method getProjectTaskTreeIndex
	**/
	function getProjectTaskTreeIndex () {
		var index = makeIndex (that.task_tree,0);
		return index;
	}
	
	/**
	@description Public method that checks validity of status item
	@method checkStatusItem
	@param Object args contains status as Interger and date as String
	**/
	function checkStatusItem (args) {
		MIOGA.logDebug('Project', 1, 'Project::checkStatusItem - loaded');
		var result = {
			status : "success",
			error : []
		};
		if (args.status !== undefined) {
			if ($.isNumeric(args.status) === false) {
				MIOGA.logError ("Project::checkStatusItem status is not a numeric value.", true);
			}
			else if (args.status.length === 0) {
				MIOGA.logError ("Project::checkStatusItem status disallow empty.", true);
			}
			else {
			}
		}
		else {
			MIOGA.logError ("Project::checkStatusItem status is required.", true);
		}

		if (args.date !== undefined) {
			if (args.date.length === 0) {
				result.status = "error";
				result.error.push('statusDateRequired');
			}
			else if (isMiogaDate(args.date) === false) {
				result.status = "error";
				result.error.push('progressDateInvalid');
			}
			else {
			}
		}
		else {
			MIOGA.logError ("Project::checkStatusItem date is required.", true);
		}
		MIOGA.logDebug('Project', 1, 'Project::checkStatusItem - status of response = ' + result.status);
		return result;
	}
	/**
	@description Public method that checks validity of project informations
	@method checkProjectParams
	@param Object args contains :
		@option String label
		@option String planned
		@option String ended
		@option String base_dir
		@option String color
		@option String bgcolor
	**/
	function checkProjectParams (args) {
		MIOGA.logDebug('Project', 1, 'Project::checkProjectParams - loaded');
		var result = {
			status : "success",
			error : []
		};

		isEmpty (args.label, "labelDisallowEmpty", result);
		if (that.projectIsUnique (args) === false) {
			result.status = "error";
			result.error.push("projectAlreadyExists"); 
		}
		// Dates validation
		if (args.started !== null) {
			if (args.planned !== null) {
				if (args.planned.getTime() - args.started.getTime() < 0) {
					result.status = "error";
					result.error.push("errorStartedPlanned"); 
				} else if (args.ended != null) {
					if (args.ended.getTime() - args.started.getTime() < 0) {
						result.status = "error";
						result.error.push("errorStartedEnded");
					}
				}
				else {
				}
			}
			else {
				result.status = "error";
				result.error.push("plannedDateRequired");
			}
		}
		else {
			result.status = "error";
			result.error.push("startedDateRequired");
		}
		// Directory validation.
		if (args.base_dir.indexOf("~") != -1 || args.base_dir.indexOf("../") != -1) {
			result.status = "error";
			result.error.push("baseDirNotAllowed");
		}
		else {
			var pathDav = mioga_context.private_dav + "/" + that.group_ident + "/" + args.base_dir;
			pathDav.replace(/\/\//g, "/");
			jQuery.Dav(pathDav).readFolder({
				async: false,
				success: function(dat, stat,response) {
				},
				error : function () {
					result.status = "error";
					result.error.push("baseDirNotFound");
				}
			});
		}
		// color
		if (args.color.length === 0) {
			result.status = "error";
			result.error.push("colorDisallowEmpty");
		}
		else if (args.color.indexOf('#') !== 0) {
			result.status = "error";
			result.error.push("badColorFormat");
		}
		// bgcolor
		if (args.bgcolor.length === 0) {
			result.status = "error";
			result.error.push("bgColorDisallowEmpty");
		}
		else if (args.bgcolor.indexOf('#') !== 0) {
			result.status = "error";
			result.error.push("badBgColorFormat");
		}
		MIOGA.logDebug('Project', 1, 'Project::checkProjectParams - status of response = ' + result.status);
		return result;
	}
	/**
	@description Public method that load project. For to do this, an AJAX request get project attributes
	@method load
	@param Boolean async defines if request is called as asynchronous or not
	@Function cb is a call back called on success request
	**/
	function load (async, cb) {
		MIOGA.logDebug('Project', 1, 'Project::load - loaded');
		var params = {};
		params.project_id = that.project_id;
		$.ajax({
			url : "GetProject.json",
			async : async,
			dataType : 'json',
			data : params,
			type : "GET",
			success : function(response){
				if (response.status === "OK") {
					if (response.data.user_is_proj_owner == 1) {
						that.user_is_proj_owner = true;
					}
					else {
						that.user_is_proj_owner = false;
					}
					parseAttributes (response.data);
					that.project_owner_id = parseInt(response.data.project_owner_id,10);
					that.project_owner_firstname = response.data.project_owner_firstname;
					that.project_owner_lastname = response.data.project_owner_lastname;
					that.project_owner_email = response.data.project_owner_email;
					// advanced parameters
					var advanced_params_attr = $.parseJSON(response.data.advanced_params);
					if (advanced_params_attr === null) {
						advanced_params_attr = {};
					}
					advanced_params_attr.modelList = that.modelList;
					if (response.data.model_id !== null) {
						advanced_params_attr.model_id = parseInt(that.model_id,10);
					}
					else {
						advanced_params_attr.model_id = null;
					}
					
					advanced_params_attr.user_is_proj_owner = that.user_is_proj_owner;
					advanced_params_attr.haussmann = that.haussmann;
					that.advancedParams = new AdvancedParams(advanced_params_attr);
					
					// task_list
					that.taskList = new TaskList({
						haussmann : that.haussmann,
						project_id: parseInt(response.data.project_id,10),
						user_is_proj_owner : that.user_is_proj_owner,
						advancedParams : that.advancedParams,
						project_owner_id : parseInt(response.data.project_owner_id,10),
						project_owner_firstname : response.data.project_owner_firstname,
						project_owner_lastname : response.data.project_owner_lastname,
						project_owner_email : response.data.project_owner_email
					});
					that.taskList.advancedParams = that.advancedParams;
					// result list
					that.resultList = new ResultList({
						haussmann : that.haussmann,
						user_is_proj_owner : that.user_is_proj_owner,
						project_id : parseInt(response.data.project_id,10),
						project_owner_id : parseInt(response.data.project_owner_id,10),
						project_owner_firstname : response.data.project_owner_firstname,
						project_owner_lastname : response.data.project_owner_lastname,
						project_owner_email : response.data.project_owner_email
					});
					// test dir of project is available or not.
					var pathDav = mioga_context.private_dav + "/" + that.group_ident + "/" + that.base_dir;
					pathDav.replace(/\/\//g, "/");
					jQuery.Dav(pathDav).readFolder({
						async: false,
						success: function(dat, stat,response) {
						},
						error : function () {
							that.setProject ({
								base_dir : ""
							},function () {
								that.base_dir = "";
							});
						}
					});
					
					if (cb !== undefined) {
						cb(that);
					}
				}
				else {
					MIOGA.logError ("Project. getProject AJAX request returned KO status.", false);
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("Project. getProject AJAX request returned an error : " + err, false);
			}
		});
	}

	/**
	@description Public method that removes last status item from list
	@method deleteLastStatusItem
	@param Function onSuccess that is call back called on remove success
	**/
	function deleteLastStatusItem (onSuccess) {
		MIOGA.logDebug('Project', 1, 'Project::deleteLastStatusItem - loaded');
		that.status.pop();
		if (that.status.length > 0) {
			MIOGA.logDebug('Project', 1, 'Project::deleteLastStatusItem - status not empty');
			that.current_status = (that.status.length-1);
		}
		else {
			that.current_status = null;
		}
		if (onSuccess !== undefined) {
			onSuccess ();
		}
	}
	/**
	@description Public method that adds status item to list
	@method addStatusItem
	@param Object args contains :
		@option Int status
		@option String date
		@option String comment
	@param Function onSuccess that is callback called on add success
	**/
	function addStatusItem (args, cb) {
		MIOGA.logDebug('Project', 1, 'Project::addStatusItem - loaded');
		var params = {
			status : args.status,
			date : args.date,
			comment : args.comment
		};
		that.status.push(params);
		that.current_status = args.status;
		if (cb !== undefined) {
			cb();
		}
	}
	/**
	@description Public method that updates project
	@method setProject
	@param Object args contains project attributes
	@param Function onSuccess that is callback called on add success
	**/
	function setProject (args, onSuccess) {
		MIOGA.logDebug('Project', 1, 'Project::setProject - loaded, args', args);
		var params = {};
		params.project_id = that.project_id;
		// label
		if (args.label !== undefined) {
			params.label = args.label;
		}
		else {
			params.label = that.label;
		}
		// use result
		if (args.use_result !== undefined) {
			if (args.use_result === true) {
				params.use_result = 1;
			}
			else {
				params.use_result = 0;
			}
		}
		else {
			if (that.use_result === true) {
				params.use_result = 1;
			}
			else {
				params.use_result = 0;
			}
		}
		
		// description
		if (args.description !== undefined) {
			params.description = args.description;
		}
		else {
			params.description = that.description;
		}
		// started
		if (args.started !== undefined) {
			params.started = args.started.getUTCFullYear() + "-" + (args.started.getUTCMonth()+1) + "-" + args.started.getUTCDate();
		}
		else {
			params.started = that.started.getUTCFullYear() + "-" + (that.started.getUTCMonth()+1) + "-" + that.started.getUTCDate();
		}
		// planned
		if (args.planned !== undefined) {
			params.planned = args.planned.getUTCFullYear() + "-" + (args.planned.getUTCMonth()+1) + "-" + args.planned.getUTCDate();
		}
		else {
			params.planned = that.planned.getUTCFullYear() + "-" + (that.planned.getUTCMonth()+1) + "-" + that.planned.getUTCDate();
		}
		// ended
		if (args.ended !== undefined) {
			if (args.ended !== null) {
				params.ended = args.ended.getUTCFullYear() + "-" + (args.ended.getUTCMonth()+1) + "-" + args.ended.getUTCDate();
			}
			else {
				params.ended = null;
			}
		}
		else {
			if (that.ended !== null) {
				params.ended = that.ended.getUTCFullYear() + "-" + (that.ended.getUTCMonth()+1) + "-" + that.ended.getUTCDate();
			}
			else {
				params.ended = null;
			}
		}
		// base_dir
		if (args.base_dir !== undefined) {
			params.base_dir = args.base_dir;
		}
		else {
			params.base_dir = that.base_dir;
		}
		// model_id
		if (args.model_id) {
			if (args.model_id !== null) {
				params.model_id = args.model_id;
			}
		}
		else {
			if (that.model_id !== null) {
				params.model_id = that.model_id;
			}
		}
		// current_status
		if (args.current_status !== undefined) {
			if (args.current_status === null) {
				params.current_status = "";
			}
			else {
				params.current_status = args.current_status;
			}
		}
		else {
			if (that.current_status === null) {
				params.current_status = "";
			}
			else {
				params.current_status = that.current_status;
			}
		}
		// status
		if (args.status !== undefined) {
			params.status = args.status;
		}
		else {
			params.status = JSON.stringify(that.status);
		}
		// tags
		if (args.tags !== undefined) {
			params.tags = JSON.stringify(args.tags);
		}
		else {
			params.tags = JSON.stringify(that.tags);
		}
		// complementary data
		if (args.complementary_data !== undefined) {
			params.complementary_data = JSON.stringify(args.complementary_data);
		}
		else {
			params.complementary_data = JSON.stringify(that.complementary_data);
		}
		// task_tree
		if (args.task_tree !== undefined) {
			params.task_tree = JSON.stringify(args.task_tree);
		}
		else {
			params.task_tree = JSON.stringify(that.task_tree);
		}
		// colors
		params.color = args.color;
		params.bgcolor = args.bgcolor;
		var adv_params_attr = that.advancedParams.serializeAttr();
		params.advanced_params = JSON.stringify(adv_params_attr);
		$.ajax({
			url : "SetProject.json",
			data : params,
			dataType:'json',
			async : false,
			type : "POST",
			success : function(response){
				if (response.status === "OK") {
					var cb = undefined;
					if (onSuccess !== undefined) {
						cb = onSuccess;
					}
					else {
						cb = that.callRefreshCB;
					}
					updateStatusStr (response.data);
					parseAttributes (response.data, cb);
					
				}
				else {
					MIOGA.logError ("Project. setProject AJAX request returned KO status", false);
					that.callRefreshCB();
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("Project. setProject AJAX request returned an error : " + err, true);
			}
		});
	}
	/**
	@description Public method that updates advanced parameters of project
	@method setProjectAdvancedParams
	@param Object new_adv_params contains advanced parameters attributes
	@param Function cb that is callback called on add success
		@option Object new_adv_params that is created AdvancedParameter object
	**/
	function setProjectAdvancedParams (new_adv_params, cb) {
		MIOGA.logDebug('Project', 1, 'Project::setProjectAdvancedParams - loaded');
		new_adv_params.user_is_proj_owner = that.user_is_proj_owner;
		that.advancedParams = new AdvancedParams(new_adv_params);
		if (that.advancedParams !== undefined) {
			if (cb) {
				cb (new_adv_params);
			}
			else {
				that.callRefreshCB ();
			}
		}
	}
	// ======================================================
	// INIT
	// ======================================================
	updateStatusStr (options);
	
	if (that.project_id === undefined || that.label === undefined || that.label.length === 0 || that.use_result === undefined) {
		MIOGA.logError ("Project. Object can not be initialized.", false);
		return undefined;
	}
} // END Project object
