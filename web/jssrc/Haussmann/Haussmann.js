// =============================================================================
/**
Create a Haussmann object.

    var options = {
        i18n : { .... }
    };

	this.appPB = new Haussmann(this.elem, options);

@class Haussmann
@constructor
@param {Object} elem DOM element containing app container.
@param {Object} options
**/
// =============================================================================
function Haussmann(elem, options) {
	"use strict";
	MIOGA.debug.Haussmann = 0;
	MIOGA.object = this;
	MIOGA.logDebug("Haussmann", 1, "Haussmann object loaded." + options);
	var that = this;

	this.i18n = options.i18n;
	this.user_lang = options.user_lang;
	this.elem = elem;
	this.currentWindow; // Used by hashchange system to hide the current window
	// match "tree", "result", "list"
	this.current_view = {
		type : "tree",
		index : null
	};
	this.max_user = 2;
	this.vocabulary = [{
		term: "Result",
		code: "0",
		value:{
			en_US:["Result", "Results"],
			fr_FR:["Résultat", "Résultats"]
		}
	}];
	// publics method declaration
	this.getStrDateFromObj = getStrDateFromObj;
	this.getStrDateFromStr = getStrDateFromStr;
	this.insertVocabulary = insertVocabulary;
	this.setVocabulary = setVocabulary;
	this.cloneVocabulary = cloneVocabulary;
	this.checkVocabulary = checkVocabulary;
	this.getUsers = getUsers;
	this.generateHash = generateHash;
	this.getGanttTaskDetail = getGanttTaskDetail;
	// End constructor
	// -----------------------------------------------------------------------------------

	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	/**
	@description Public method that receives Date object and return date to string format like: "2013-02-08", or nothing if date value is null.
	@method getStrDateFromObj
	@param Object args settings An object Date.
	**/
	function getStrDateFromObj (date) {
		MIOGA.logDebug("Haussmann", 1, 'Haussmann::getStrDateFromObj - loaded');
		if (date !== null) {
			var month = "" + (date.getUTCMonth()+1);
			if (month.length === 1) {
				month = "0" + month;
			}
			var day = "" + date.getUTCDate();
			if (day.length === 1) {
				day = "0" + day;
			}
			return date.getFullYear() + "-" + month + '-' + day;
		}
		else {
			MIOGA.logDebug("Haussmann", 1, 'Haussmann::getStrDateFromObj - argument is null. Traitment not possible, return nothing');
		}
	}
	/**
	@description Public method that receives date as String and return Date object corresponding or nothing if args is an empty string.
	@method getStrDateFromStr
	@param String str as date format like: "2013-08-09" 
	**/
	function getStrDateFromStr (str) {
		MIOGA.logDebug("Haussmann", 1, 'Haussmann::getStrDateFromStr - loaded');
		if (str.length > 0) {
			var d = new Date();
			d.parseMiogaDate(str, str);
			var month = "" + (d.getUTCMonth()+1);
			if (month.length === 1) {
				month = "0" + month;
			}
			var day = "" + d.getUTCDate();
			if (day.length === 1) {
				day = "0" + day;
			}
			return d.getFullYear() + "-" + month + '-' + day;
		}
		else {
			MIOGA.logDebug("Haussmann", 1, 'Haussmann::getStrDateFromStr - argument is an empty string, return nothing');
		}
	}
	/**
	@description Public method that inserts vocabulary in Project page:
					For each vocabulary, this function retrieve a good translation 
					according to current language of user and content of class tags "span".
					To do this, the class is expressed like an index for language and an index for singular or plural of this word. 
	@method insertVocabulary
	@param Array vocabulary that containing JSON structure that represents a vocabulary like : 
		{
			term: "Result",
			code: "0",
			value:{
				en_US:["Result", "Results"],
				fr_FR:["Résultat", "Résultats"]
			}
		}
	**/
	function insertVocabulary (vocabulary) {
		MIOGA.logDebug("Haussmann", 1, 'Haussmann::insertVocabulary - loaded');
		$.each(vocabulary, function (ind, voc) {
			$('span.haussmannVoc_' + ind + '_0').html(voc.value[mioga_context.group.lang][voc.code].toLowerCase());
		});
	}
	/**
	@description Public method that updates vocabulary attribute with a new complex JSON value passed in parameters.
	@method setVocabulary
	@param Array voc that containing JSON structure that represents a vocabulary like : 
		{
			term: "Result",
			code: "0",
			value:{
				en_US:["Result", "Results"],
				fr_FR:["Résultat", "Résultats"]
			}
		}
	@param Function onSuccess is a callback loaded when vocabulary attribute is updated with success.
	**/
	function setVocabulary (voc, onSuccess) {
		MIOGA.logDebug("Haussmann", 1, 'Haussmann::setVocabulary - loaded');
		that.vocabulary = voc;
		that.vocabulary = that.cloneVocabulary(onSuccess);
	}
	/**
	@description Public method that clones vocabulary attribute for remove all references in this JSON structure. 
	@method cloneVocabulary
	@param Function onSuccess is a callback loaded when vocabulary attribute is cloned with success.
	**/
	function cloneVocabulary (cb) {
		MIOGA.logDebug("Haussmann", 1, 'Haussmann::cloneVocabulary - loaded');
		var vocabulary = [];
		$.each(that.vocabulary, function (i,e) {
			var vocItem = {
				term : e.term,
				code : e.code,
				value : {}
			};
			$.each(e.value, function (key,val) {
				vocItem.value[key] = [val[0], val[1]];
			});
			vocabulary.push(vocItem);
		});
		if (cb !== undefined) {
			cb();
		}
		return vocabulary;
	}
	/**
	@description Public method that verify if all fields from vocabulary is not empty. If it detectes an error, it return array of error with corresponding key of i18n variable.
	@method checkVocabulary
	@param Array voc like vocabulary attribute
	**/
	function checkVocabulary (voc) {
		MIOGA.logDebug("Haussmann", 1, 'Haussmann::checkVocabulary - loaded');
		var result = {
			status : "success",
			error : []
		};
		for (var i=0; i < voc.length; i++) {
			$.each(voc[i].value, function (lang, sing_plur_list) {
				if (sing_plur_list[0].length === 0 || sing_plur_list[1].length === 0) {
					result.status = "error";
					result.error.push("all_field_of_vocabulary_must_be_filled");
				}
			});
		}
		return result;
	}
	/**
	@description Public method that retrieves users from Mioga who are part of the user group.
	@method getUsers
	@param Object args containing lastname of user that will be searched.
	**/
	function getUsers (args) {
		MIOGA.logDebug("Haussmann", 1, 'Haussmann::getUsers - loaded');
		var params = {};
		params.app_name = "Haussmann";
		if (args) {
			if (args.lastname !== undefined) {
				params.lastname = args.lastname;
			} else if (args.max) {
				params.max = args.max;
			}
			else {
				MIOGA.logDebug("Haussmann", 1, 'Haussmann::getUsers - not lastname and not max in arg.');
			}
		}
		
		var data = undefined;
		$.ajax({
			async : false,
			url : "GetUsers.json",
			data : params,
			success : function (response) {
				if (response.status === "OK") {
					data = response.data;
				}
				else {
					MIOGA.logError ("Haussmann::getUsers. AJAX request returned KO status", false);
				}
			},
			error : function (err) {
				MIOGA.logError ("Haussmann::getUsers. AJAX request returned an error : " + err, false);
			}
		});
		return data;
	}
	/**
	@description Public method that generates a new hash for browser navigation according to editor object and id of element that will be loaded and showed.
	@method generateHash
	@param String editor is the editor that will be drawed the called page.
	@param Int id is rowid of element that will be loaded.
	**/
	function generateHash (editor, id) {
		MIOGA.logDebug("Haussmann", 1, 'Haussmann::generateHash - loaded');
		var hash = "#";
		if (editor === "ProjectListEditor") {
			hash += "show-project-list";
		} else if (editor === "GanttEditor") {
			hash += "projectlist-timeline";
		} else if (editor === "GanttProjectEditor") {
			hash += "project-timeline-" + id;
		} else if (editor === "ProjectEditor") {
			hash += "show-project-" + id;
		} else if (editor === "TaskEditor") {
			hash += "edit-task-" + id;
		} else if (editor === "ProjectParamsEditor") {
			hash += "edit-project-" + id;
		} else if (editor === "ResultEditor") {
			hash += "edit-result-" + id;
		}
		else {
			MIOGA.logError ("Haussmann::generateHash. editor argument is not found", false);
			hash = "error";
		}
		if (hash !== "error") {
			return hash;
		}
	}
	/**
	@description Public method that generates a HTML table that contains detail of task. Function is called by EiffelMain editor when user click on task for see detail.
	@method getGanttTaskDetail
	@param Array lines contains all lines of gantt view.
	@param Int line_num is index of line that will be showed.
	@param Int offset that is y position of click to determine day of task
	@param Int cell_num that is index of task for this day.
	**/
	function getGanttTaskDetail (lines, line_num, offset, cell_num) {
		var start;
		var end;
		var $table = $('<table class="task-list"></table>');
		$.each(lines[line_num].cell_tasks[cell_num], function(n, task) {
			var cell1 = "";
			if (task.allday) {
				cell1 = "&nbsp;";
			}
			else {
				cell1 = task.start.formatHour(false, false) + " - " +  task.end.formatHour(false, false);
			}
			
			var task_id_str = "" + task.rowid;
			if (task_id_str.indexOf("project_id") !== -1) {
				// its project as task so dont draw progress info.
				$('<tr>'
						+'<td class="task-pstart-label">'+ that.i18n.p_start +'</td>'
						+'<td class="task-pstart">'+task.dstart+'</td>'
					+'</tr>')
						.appendTo($table);
				$('<tr>'
						+'<td class="task-pend-label">'+ that.i18n.p_end +'</td>'
						+'<td class="task-pend">'+task.dend+'</td>'
					+'</tr>')
						.appendTo($table);
				$('<tr>'
						+'<td class="task-res-label">'+ that.i18n.milestone +'</td>'
						+'<td class="task-res"></td>'
					+'</tr>')
						.appendTo($table);
				// table of milestone
				var res_str = "";
				var $res_table = $('<table></table>').appendTo($table.find('.task-res'));
				if (task.result.length > 0) {
					for (var i=0; i < task.result.length; i++) {
						if (task.result[i].pend === null) {
							res_str = that.i18n.not_indicated;
						}
						else {
							res_str = task.result[i].pend;
						}
						$res_table.append('<tr><td>' + task.result[i].label + '</td><td>' + res_str + '</td></tr>');
					}
				}
				else {
					$res_table.append('<tr><td>' + that.i18n.none + '</td></tr>');
				}
			}
			else if (task_id_str.indexOf("res_id") !== -1) {
				$('<tr><td>' + that.i18n.milestone + '</td></tr>').appendTo($table);
			}
			else {
				var p_start_str = task.pstart;
				var p_end_str = task.pend;
				if (task.pstart === null) {
					p_start_str = that.i18n.not_indicated;
				}
				if (task.pend === null) {
					p_end_str = that.i18n.not_indicated;
				}
				$('<tr>'
						+'<td class="task-pstart-label">'+ that.i18n.p_start +'</td>'
						+'<td class="task-pstart">'+p_start_str+'</td>'
					+'</tr>')
						.appendTo($table);
				$('<tr>'
						+'<td class="task-pend-label">'+ that.i18n.p_end +'</td>'
						+'<td class="task-pend">'+p_end_str+'</td>'
					+'</tr>')
						.appendTo($table);
				$('<tr>'
						+'<td class="task-progress-label">'+ that.i18n.progress +'</td>'
						+'<td class="task-progress">'+parseInt(task.current_progress)+' %</td>'
					+'</tr>')
						.appendTo($table);
			}
		});
		return $table;
	}
	// -----------------------------------------------------------------------------------------------------------------------------------------
	// INIT Haussmann App
	// -----------------------------------------------------------------------------------------------------------------------------------------
	// insert container of each page of application
	this.$project_list_editor_cont = $('<div class="project-list-editor"></div>').appendTo($(that.elem));
	this.$projectlist_timeline = $('<div class="projectlist-timeline-editor"></div>').appendTo($(that.elem));
	this.$projectlist_timeline_prefs = $('<div class="projectlist-timeline-prefs-editor" style="display:none;"></div>').appendTo($(that.elem));
	this.$project_timeline = $('<div class="project-timeline-editor"></div>').appendTo($(that.elem));
	this.$project_editor_cont = $('<div class="project-editor" style="display:none;"></div>').appendTo($(that.elem));
	this.$project_params_editor = $('<div class="project-params-editor" style="display:none;"></div>').appendTo($(that.elem));
	this.$adv_params_editor = $('<div class="advanced-params-editor" style="display:none;"></div>').appendTo($(that.elem));
	this.$result_editor = $('<div class="result-editor" style="display:none;"></div>').appendTo($(that.elem));
	this.$task_editor = $('<div class="task-editor" style="display:none;"></div>').appendTo($(that.elem));
	// load data and editor object
	this.projectList = new ProjectList({
		haussmann : that,
		i18n : that.i18n
	});
	// Make gantt preferences from project list.
	// By default, project is displayed but project tasks ae hidded.
	var prefsGantt = {};
	if (that.projectList.project_list.length > 0) {
		for (var i=0; i < that.projectList.project_list.length; i++) {
			prefsGantt[that.projectList.project_list[i].project_id] = {
				project_id : that.projectList.project_list[i].project_id,
				label : that.projectList.project_list[i].label,
				display : true,
				detail : false
			};
		}
	}
	else {
		prefsGantt = {};
	}
	
	// Gantt object for project list
	this.gantt = new Gantt({
		i18n : that.i18n,
		prefs : prefsGantt
	});
	// Gantt object for project
	this.ganttProject = new Gantt({
		i18n : that.i18n,
		prefs : {}
	});

	this.projectListEditor = new ProjectListEditor($(that.elem),{
		i18n : that.i18n,
		haussmann : that,
		// function that adds new project in gantt prefs
		addProjectInGanttPrefs : function (proj) {
			that.gantt.prefs[proj.project_id] = {
				project_id : proj.project_id,
				label : proj.label,
				display : true,
				detail : false
			};
		}
	});
	this.ganttConfigEditor = new GanttConfigEditor (that.$projectlist_timeline_prefs, {
		i18n : that.i18n,
		prefs : that.gantt.prefs,
		onChange : function (newData) {
			that.gantt.prefs = {};
			for (var proj_id in newData) {
				var item = {
					project_id : newData[proj_id].project_id,
					label : newData[proj_id].label,
					display : newData[proj_id].display,
					detail: newData[proj_id].detail
				};
				that.gantt.prefs[newData[proj_id].project_id] = item;
			}
			that.ganttConfigEditor.hideWindow ();
			
			that.gantt.getTasks(that.ganttEditor.p_start, that.gantt.config.week_count, [], [], function () {
				that.ganttEditor.showWindow();
			});
		},
		onCancel : function () {
			that.ganttConfigEditor.hideWindow ();
			that.ganttEditor.showWindow();
		}
	});
	// eiffel editor from Eiffel application for project list
	this.ganttEditor = new EiffelMain (that.$projectlist_timeline, {
		app: that.gantt,
		i18n : that.i18n,
		maxZoom : 0,
		step_mode : {
			"week":5,
			"month":8
		},
		indexOfUsers : that.gantt.indexOfUsers,
		getTaskDetail : that.getGanttTaskDetail,
		$config : function () {
			var config_id = MIOGA.generateID("haussmann");
			var $conf = $('<span id="' + config_id + '" class="gantt-config-link">' + that.i18n.configurationText + '</span>').click(function () {
				that.ganttEditor.hideWindow();
				that.ganttConfigEditor.showWindow (that.gantt.prefs);
			});
			
			return $conf;
			
		}
	});
	// eiffel editor from Eiffel application for project
	this.ganttProjectEditor = new EiffelMain (that.$project_timeline, {
		app: that.ganttProject,
		i18n : that.i18n,
		maxZoom : 0,
		step_mode : {
			"week":5,
			"month":8
		},
		indexOfUsers : that.ganttProject.indexOfUsers,
		getTaskDetail : that.getGanttTaskDetail,
		$config : function () {
			return $('<span class="gantt-config-link"></span>');
		}
	});
	// in Gantt project view, no preferences. 
	this.$gantt_project_link = $('<button class="button normal show-gantt">'+ that.i18n.hideTimeline + '</button>').on('click', function () {
		for (pf in that.ganttProject.prefs) {
			location.hash = that.generateHash("ProjectEditor", pf);
			return;
		}
	}).prependTo(that.$project_timeline.find('.eiffel-main'));
	// =============================================================================================================================
	// Add button to hide gant view in eiffelmain module.
	// This is to do here for dont modify eiffel module
	this.$gantt_link = $('<button class=" button normal show-gantt">'+ that.i18n.hideTimeline + '</button>').on('click', function () {
		location.hash = that.generateHash("ProjectListEditor");
	}).prependTo(that.$projectlist_timeline.find('.eiffel-main'));
	// =============================================================================================================================

	this.projectEditor = new ProjectEditor ($(that.elem),{
		haussmann : that,
		i18n : that.i18n,
		project : undefined
	});
	this.projectParamsEditor = new ProjectParamsEditor($(that.elem),{
		i18n : that.i18n,
		haussmann : that,
		project : undefined
	});
	this.resultEditor = new ResultEditor($(that.elem), {
		i18n : that.i18n,
		haussmann : that,
		result : undefined,
		project : undefined
	});
	this.taskEditor = new TaskEditor ($(that.elem), {
		i18n : that.i18n,
		haussmann : that,
		task : undefined,
		project : undefined
	});
	var pb_opt = { i18n : { "displayText" : this.i18n.progressBarText } };
	this.appPB = new AppProgressBar(this.elem, pb_opt);
	this.appPB.setValue(0);
	this.appPB.setValue(30);
	this.appPB.setValue(60);
	// ------------------------------------------------------------------------------------------------
	// Prepare callback for navigation on hashchange
	// ------------------------------------------------------------------------------------------------
	$(window).hashchange( function(){
		MIOGA.logDebug("Haussmann", 1, 'Haussmann::hashchange - hashchange loaded');
		var hash = location.hash;
		var hash_number = parseInt(/([0-9]+)/.exec(hash));
		var newWindow = undefined;
		// ------------------------------------------------------------------------------------------------
		// Hash : Empty
		// ------------------------------------------------------------------------------------------------
		if (hash.length === 0) {
			MIOGA.logDebug("Haussmann", 1, 'Haussmann::hashchange - Draw main window');
			that.projectList = new ProjectList({
				haussmann : that
			});
			var current_project_id = that.projectList.current_project_id;
			if (current_project_id !== null ){
				if (that.projectList.project_list_by_rowid[current_project_id] !== undefined) {
					location.hash = "#show-project-" + current_project_id;
					return false;
				}
				else {
					newWindow = that.projectListEditor;
					location.hash = '#show-project-list';
				}
			}
			else {
				newWindow = that.projectListEditor;
				location.hash = '#show-project-list';
			}
		}
		// ------------------------------------------------------------------------------------------------
		// Hash : #projectlist-timeline ( GANTT view )
		// ------------------------------------------------------------------------------------------------
		else if (hash === '#projectlist-timeline') {
			$(that.elem).find('.project-timeline-editor, .project-list-editor, .project-editor, .project-params-editor, .advanced-params-editor, .project-task-editor, .project-result-editor').hide();
			$(that.elem).find('.projectlist-timeline-editor').show();
			newWindow = that.ganttEditor;
		} 
		// ------------------------------------------------------------------------------------------------
		// Hash : #project-timeline-x ( GANTT view )
		// ------------------------------------------------------------------------------------------------
		else if (hash.indexOf('#project-timeline-') !== -1 ) {
			MIOGA.logDebug("Haussmann", 1, 'Haussmann::hashchange - Show project timeline  ' + hash_number);
			if ($.isNumeric(hash_number) ) {
				if ( that.projectList.project_list_by_rowid[hash_number] !== undefined) {
					that.projectList.current_project_id = hash_number;
					that.projectList.project_list_by_rowid[hash_number].load(false, function (project) {
						that.projectEditor.project = project;
						that.ganttProject.prefs[hash_number] = {
							project_id : hash_number,
							label : project.label,
							display : true,
							detail : true
						};
						that.ganttProjectEditor.app = that.ganttProject;
						$(that.elem).find('.projectlist-timeline-editor, .project-list-editor, .project-editor, .project-params-editor, .advanced-params-editor, .project-task-editor, .project-result-editor').hide();
						$(that.elem).find('.project-timeline-editor').show();
						newWindow = that.ganttProjectEditor;
					});
				}
				else {
					newWindow = that.projectListEditor;
					location.hash = '#show-project-list';
				}
			}
			else {
				MIOGA.logError ("The rowid of project is not a numeric value", false);
				newWindow = that.projectListEditor;
				location.hash = '#show-project-list';
			}
		// ------------------------------------------------------------------------------------------------
		// Hash : #show-project-list
		// ------------------------------------------------------------------------------------------------
		} else if (hash === '#show-project-list') {
			$(that.elem).find('.project-timeline-editor, .projectlist-timeline-editor, .project-editor, .project-params-editor, .advanced-params-editor, .project-task-editor, .project-result-editor').hide();
			newWindow = that.projectListEditor;
		// ------------------------------------------------------------------------------------------------
		// Hash : #show-project-x
		// ------------------------------------------------------------------------------------------------
		} else if (hash.indexOf('#show-project-') !== -1 ) {
			MIOGA.logDebug("Haussmann", 1, 'Haussmann::hashchange - Show project ' + hash_number);
			if ($.isNumeric(hash_number) ) {
				if ( that.projectList.project_list_by_rowid[hash_number] !== undefined) {
					that.projectList.current_project_id = hash_number;
					that.projectList.project_list_by_rowid[hash_number].load(false, function (project) {
						
						that.projectEditor.project = project;
						that.appPB.setValue(80);
						that.appPB.setValue(100);
						that.appPB.$pbcont.hide();
						$(that.elem).find('.project-timeline-editor, .projectlist-timeline-editor, .project-list-editor, .project-params-editor, .advanced-params-editor, .project-task-editor, .project-result-editor').hide();
						newWindow = that.projectEditor;
					});
				}
				else {
					newWindow = that.projectListEditor;
					location.hash = '#show-project-list';
				}
			}
			else {
				MIOGA.logError ("The rowid of project is not a numeric value", false);
				newWindow = that.projectListEditor;
				location.hash = '#show-project-list';
			}
		// ------------------------------------------------------------------------------------------------
		// Hash : #edit-project-x
		// ------------------------------------------------------------------------------------------------
		} else if (hash.indexOf('#edit-project-') !== -1 ) {
			MIOGA.logDebug("Haussmann", 1, 'Haussmann::hashchange - Show edition page of project ' + hash_number);
			if (mioga_context.rights.WriteProjectIfOwner || mioga_context.rights.Anim) {
				MIOGA.logDebug("Haussmann", 1, 'Haussmann::hashchange - Show edition page of project. rights OK');
				if ($.isNumeric(hash_number) ) {
					MIOGA.logDebug("Haussmann", 1, "index of project list : ",that.projectList);
					MIOGA.logDebug("Haussmann", 1, "index of editor project list : ",that.projectListEditor.projectList);
					if ( that.projectList.project_list_by_rowid[hash_number] !== undefined) {
						that.projectList.current_project_id = hash_number;
						that.projectList.project_list_by_rowid[hash_number].load(false,function (project) {
							if (project.user_is_proj_owner === true || mioga_context.rights.Anim) {
								that.projectParamsEditor.project = project;
								that.appPB.setValue(80);
								that.appPB.setValue(100);
								that.appPB.$pbcont.hide();
								$(that.elem).find('.eiffel-main, .project-list-editor, .project-editor, .advanced-params-editor, .project-task-editor, .project-result-editor').hide();
								newWindow = that.projectParamsEditor;
							}
							else {
								// no access right
								MIOGA.logError ("No access right for edit project", false);
								newWindow = that.projectListEditor;
								location.hash = '#show-project-list';
							}
						});
					}
					else {
						newWindow = that.projectListEditor;
						location.hash = '#show-project-list';
					}
				}
				else {
					MIOGA.logError ("The rowid of project is not a numeric value", false);
					newWindow = that.projectListEditor;
					location.hash = '#show-project-list';
				}
			}
			else {
				// no access right
				MIOGA.logError ("No access right for edit project", false);
				newWindow = that.projectListEditor;
				location.hash = '#show-project-list';
			}
		// ------------------------------------------------------------------------------------------------
		// Hash : #edit-task-x
		// ------------------------------------------------------------------------------------------------
		} else if (hash.indexOf('#edit-task-') !== -1 ) {
			MIOGA.logDebug("Haussmann", 1, 'Haussmann::hashchange - Show edition page of task ' + hash_number);
			if (mioga_context.rights.Read || mioga_context.rights.Anim || mioga_context.rights.WriteProjectIfOwner || mioga_context.rights.WriteTask) {
				if ($.isNumeric(hash_number)) {
					var currentProject = that.projectList.getProjectByTask(false, hash_number);
					if (currentProject === null) {
						newWindow = that.projectListEditor;
						location.hash = '#show-project-list';
					}
					else {
						that.projectList.current_project_id = currentProject.project_id;
						that.projectList.project_list_by_rowid[currentProject.project_id].load(false, function (project) {
							$.each(project.taskList.task_list, function (i,task) {
								if (task.task_id === hash_number) {
									var args = {
										project_id :project.project_id,
										task_id : hash_number
									};
									project.taskList.updateTaskAttr(args, false, function (task_updated) {
										that.taskEditor.task = task_updated;
										that.taskEditor.project = project;
										that.appPB.setValue(80);
										that.appPB.setValue(100);
										that.appPB.$pbcont.hide();
										$(that.elem).find('.eiffel-main, .project-list-editor, .project-editor, .project-params-editor, .advanced-params-editor, .project-result-editor').hide();
										newWindow = that.taskEditor;
									});
								}
								else {
									MIOGA.logDebug("Haussmann", 1, 'Haussmann::hashchange - task_id is not equal of hash number');
								}
							});
						});
					}
				}
				else {
					MIOGA.logError ("The rowid of task is not a numeric value", false);
					newWindow = that.projectListEditor;
					location.hash = '#show-project-list';
				}
			}
			else {
				MIOGA.logError ("No access right for to do this", false);
				newWindow = that.projectListEditor;
				location.hash = '#show-project-list';
			}
		// ------------------------------------------------------------------------------------------------
		// Hash : #edit-result-x
		// ------------------------------------------------------------------------------------------------
		} else if (hash.indexOf('#edit-result-') !== -1 ) {
			MIOGA.logDebug("Haussmann", 1, 'Haussmann::hashchange - Show edition page of result ' + hash_number);
			if (mioga_context.rights.WriteProjectIfOwner || mioga_context.rights.Anim) {
				if ($.isNumeric(hash_number)) {
					var currentProject = that.projectList.getProjectByResult(false, hash_number);
					if (currentProject === null) {
						newWindow = that.projectListEditor;
						location.hash = '#show-project-list';
					}
					else {
						that.projectList.current_project_id = currentProject.project_id;
						that.projectList.project_list_by_rowid[currentProject.project_id].load(false, function (project) {
							if (project.user_is_proj_owner === true  || mioga_context.rights.Anim) {
								$.each(project.resultList.result_list, function (i,result) {
									if (result.result_id === hash_number) {
										that.resultEditor.result = result;
										that.resultEditor.project = project;
										that.appPB.setValue(80);
										that.appPB.setValue(100);
										that.appPB.$pbcont.hide();
										$(that.elem).find('.eiffel-main, .project-list-editor, .project-editor, .project-params-editor, .project-task-editor, .advanced-params-editor, .project-result-editor').hide();
										newWindow = that.resultEditor;
									}
									else {
										MIOGA.logDebug("Haussmann", 1, 'Haussmann::hashchange - result_id is not equal of hash number');
									}
								});
							}
							else {
								// no access right
								MIOGA.logError ("No access right for edit project", false);
								newWindow = that.projectListEditor;
								location.hash = '#show-project-list';
							}
						});
					}
				}
				else {
					MIOGA.logError ("The rowid of result is not a numeric value", false);
					newWindow = that.projectListEditor;
					location.hash = '#show-project-list';
				}
			}
			else {
				// no access right
				MIOGA.logError ("No access right for edit project", false);
				newWindow = that.projectListEditor;
				location.hash = '#show-project-list';
			}
		}
		// ------------------------------------------------------------------------------------------------
		// Hash : not found
		// ------------------------------------------------------------------------------------------------
		else {
			MIOGA.logError (" Haussmann - Hashchange not found", false);
			newWindow = that.projectListEditor;
			location.hash = '#show-project-list';
		}

		if ( (that.currentWindow === undefined) || (that.currentWindow !== newWindow) ) {
			MIOGA.logDebug("Haussmann", 1, 'Haussmann::hashchange - currentView not defined or change of view');
			if (that.currentWindow !== undefined) {
				that.currentWindow.hideWindow();
			}
			newWindow.showWindow();
			that.appPB.setValue(80);
			that.appPB.setValue(100);
			that.appPB.$pbcont.hide();
			that.currentWindow = newWindow;
		}
		else {
			newWindow.showWindow();
		}
	});
	$(window).hashchange();
} // End Haussmann object
