// =============================================================================
/**
@description Create a ResultNote object.

    var options = {
        haussmann : { .... }
    };

@class ResultNote
@constructor
**/
// =============================================================================
function ResultNote (options) {
	"use strict";
	MIOGA.logDebug('ResultNote', 1, ' ResultNote loaded');
	MIOGA.debug.ResultNote = 0;
	var that = this;
	this.result_note_id = options.result_note_id,
	this.project_id = options.project_id;
	this.result_id = options.result_id;
	this.text = options.text;
	this.created_str = options.created_str;
	this.created = new Date();
	this.created.parseMiogaDate(options.created, options.created);
	this.modified = new Date();
	this.modified.parseMiogaDate(options.modified, options.modified);
	this.owner_id = options.owner_id;
	this.firstname = options.firstname;
	this.lastname = options.lastname;
	this.email = options.email;
	this.project_owner_id = options.project_owner_id;
	this.project_owner_firstname = options.project_owner_firstname;
	this.project_owner_lastname = options.project_owner_lastname;
	this.project_owner_email = options.project_owner_email;

	// publics method declaration
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;
	this.isEmpty = isEmpty;
	this.setResultNote = setResultNote;
	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================================================
	// PUBLICS METHODS
	// ===================================================================================================================
	/**
	@description Public method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('ResultNote', 1, 'ResultNote::addRefreshCB - loaded');
		this.refreshCB.push( { cb : cb, context : context });
	}
	/**
	@description Public method that loads each callback.
	@method callRefreshCB
	**/
	function callRefreshCB() {
		MIOGA.logDebug('ResultNote', 1, 'ResultNote::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that checks if arguments length is empty. If this is it, that returns true, else returns false.
	@method isEmpty
	@param Object elem can be all type. This is the element that will be tested.
	@param String error_str that is a string value that represents the error message if tested element is empty.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isEmpty (elem, error_str, global_result) {
		MIOGA.logDebug('ResultNote', 1, 'ResultNote::isEmpty - loaded');
		var result = false;
		
		if (elem.length === 0) {
			if (global_result) {
				global_result.status = "error";
				global_result.error.push(error_str);
			}
			result = true;
		}
		return result;
	}
	
	/**
	@description Public method that updates result note
	@method setResultNote
	@param Object args that is contains pair key / value : text
	@param Function cb that is callback called on set result note success
	**/
	function setResultNote(args, cb) {
		MIOGA.logDebug('ResultNote', 1, 'ResultNote::setResultNote - loaded');
		var params = {
			result_note_id : that.result_note_id,
			result_id : that.result_id,
			project_id : that.project_id
		};
		if (args.text) {
			params.text = args.text;
		}
		else {
			params.text = that.text;
		}
		$.ajax({
			url : "SetResultNote.json",
			data : params,
			dataType:'json',
			type : "POST",
			async : false,

			success : function(response){
				if (response.status === "OK") {
					that.modified = new Date();
					that.modified.parseMiogaDate(response.data.modified, response.data.modified);
					that.text = response.data.text;
					if (cb !== undefined) {
						cb(that);
					}
					else {
						that.callRefreshCB();
					}
				}
				else {
					MIOGA.logError ("ResultNote. setResultNote AJAX request returned KO status", false);
					that.callRefreshCB();
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("ResultNote. setResultNote AJAX request returned an error : " + err, true);
			}
		});
	}

	if (
		this.project_id === undefined || this.result_id === undefined || 
		this.result_note_id === undefined || this.text === undefined || 
		this.created === undefined || this.modified === undefined || 
		this.owner_id === undefined || this.firstname === undefined || 
		this.lastname === undefined || this.email === undefined || 
		this.project_owner_id === undefined || this.project_owner_firstname === undefined || 
		this.project_owner_lastname === undefined || this.project_owner_email === undefined
		) {
		MIOGA.logError ("ResultNote. Object can not be initialized.", false);
		return undefined;
	}
} // End ResultNote object
