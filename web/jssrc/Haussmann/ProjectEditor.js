// =============================================================================
/**
Create a ProjectEditor object.

    var options = {
        i18n : { .... }
    };

	this.projectEditor = new ProjectEditor(this.elem, options);

@class Haussmann
@constructor
@param {Object} $elem JQuery that will be contains this editor
@param {Object} options
**/
// =============================================================================
function ProjectEditor ($elem, options) {
	"use strict";
	MIOGA.debug.ProjectEditor = 0;
	MIOGA.logDebug('ProjectEditor', 1, ' ProjectEditor loaded');
	var that = this;
	this.$elem = $elem;
	this.i18n = options.i18n;
	this.haussmann = options.haussmann;
	this.generateHash = options.haussmann.generateHash;;
	this.insertVocabulary = options.haussmann.insertVocabulary;
	this.project = undefined;
	this.edit_mod = "edit";
	this.edit_tree_comment_id = MIOGA.generateID("haussmann");
	// publics method declaration
	this.showWindow = showWindow;
	this.hideWindow = hideWindow;
	this.drawView = drawView;
	this.drawListView = drawListView;
	this.drawListViewTasks = drawListViewTasks;
	this.drawResultView = drawResultView;
	this.drawResultViewTasks = drawResultViewTasks;
	this.drawTreeView = drawTreeView;
	this.drawTreeViewTasks = drawTreeViewTasks;
	this.drawDeleteTree = drawDeleteTree;
	this.treeSerialize = treeSerialize;
	this.serializeTaskPosition = serializeTaskPosition;
	this.refreshLabelDesc = refreshLabelDesc;
	this.refreshFileSelector = refreshFileSelector;
	this.refreshViewList = refreshViewList;
	this.refresh = refresh;
	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================================================
	// PRIVATES METHODS
	// ===================================================================================================================
	/**
	@description Private function that draws errors list in a HTML container
	@method drawErrorSimpleList
	@param Object $elem is the Jquery element as container of error list will be drawed.
	@param Array error_list is the list of translate error message.
	**/
	function drawErrorSimpleList ($elem, error_list) {
		$elem.find('.error').remove();
		var $error_cont = $('<div class="error"></div>').appendTo($elem);
		$.each(error_list, function (i,error_str) {
			$error_cont.append('<p class="error">' + that.i18n[error_str] + '</p>');
		});
	}
	/**
	@description Private function that makes a read mode for no affected tasks container.
	@method makeReadMod
	@param String i18n_edit is translate of link to edit mode.
	**/
	function makeReadMod (i18n_edit) {
		that.$no_affected_task_block.find('.error').remove();
		that.$no_affected_task_block.find('.create-task-btn, .new-task-label-input').hide();
		that.$no_affected_task_block.find('.add_task_link').removeClass('edit-mod').addClass('read-mod').html(i18n_edit);
		that.edit_mod = "read";
	}
	/**
	@description Private function that makes a read mode for tree container.
	@method makeTreeReadMod
	@param String i18n_edit is translate of link to edit mode.
	**/
	function makeTreeReadMod (i18n_edit) {
		that.$tree_view_cont.find('.error').remove();
		that.$tree_view_cont.find('h2 .h_edit, h2 .h_delete').hide();
		that.$tree_view_cont.find('div[class^="container_level_"].ui-sortable').sortable('disable');
		that.$tree_view_cont.find('.edit-cont').hide();
		that.$edit_tree_link.removeClass('edit-mod').addClass('read-mod').html(i18n_edit);
	}
	/**
	@description Private function that makes a edit mode for tree container.
	@method makeTreeEditMod
	@param String i18n_edit is translate of link to read mode.
	**/
	function makeTreeEditMod (i18n_read) {
		that.$tree_view_cont.find('.error').remove();
		that.$tree_view_cont.find('h2 .h_edit, h2 .h_delete').show();
		that.$tree_view_cont.find('div[class^="container_level_"].ui-sortable').sortable('enable');
		that.$tree_view_cont.find('.edit-cont').show();
		that.$edit_tree_link.removeClass('read-mod').addClass('edit-mod').html(i18n_read);
	}
	/**
	@description Private function that makes a read mode for result container.
	@method MakeResultReadMod
	@param String i18n_edit is translate of link to edit mode.
	**/
	function makeResultReadMod (i18n_edit) {
		that.$result_view_cont.find('.error').remove();
		that.$result_view_cont.find('h2 .h_edit, h2 .h_delete').hide();
		that.$result_view_cont.find('.edit-cont').hide();
		that.$edit_result_link.removeClass('edit-mod').addClass('read-mod').html(i18n_edit);
	}
	/**
	@description Private function that makes a edit mode for result container.
	@method makeResultEditMod
	@param String i18n_edit is translate of link to read mode.
	**/
	function makeResultEditMod (i18n_read) {
		that.$result_view_cont.find('.error').remove();
		that.$result_view_cont.find('h2 .h_edit, h2 .h_delete').show();
		that.$result_view_cont.find('.edit-cont').show();
		that.$edit_result_link.removeClass('read-mod').addClass('edit-mod').html(i18n_read);
	}
	/**
	@description Private function that makes a read mode for grouping list container.
	@method makeListReadMod
	@param String i18n_edit is translate of link to edit mode.
	**/
	function makeListReadMod (i18n_edit) {
		that.$list_view_cont.find('.error').remove();
		that.$list_view_cont.find('h2 .h_edit, h2 .h_delete').hide();
		that.$list_view_cont.find('.edit-cont').hide();
		that.$edit_list_link.removeClass('edit-mod').addClass('read-mod').html(i18n_edit);
	}
	/**
	@description Private function that makes a edit mode for grouping list container.
	@method makeListEditMod
	@param String i18n_edit is translate of link to read mode.
	**/
	function makeListEditMod (i18n_read) {
		that.$list_view_cont.find('.error').remove();
		that.$list_view_cont.find('h2 .h_edit, h2 .h_delete').show();
		that.$list_view_cont.find('.edit-cont').show();
		that.$edit_list_link.removeClass('read-mod').addClass('edit-mod').html(i18n_read);
	}
	/**
	@description Private function that creates a new task and load callback if success or error.
	@method createNewTask
	@param Object $label is Jquery element that is a label input field
	**/
	function createNewTask ($label) {
		that.$no_affected_task_block.find('.error').remove();
		var $error_cont = $('<div class="error error-project-editor"></div>').prependTo(that.$no_affected_task_block);
		var label = $.trim($label.val());
		var onSuccess = function (newTask) {
			var $newTask = getHTMLTask(newTask);
			that.$no_affected_task_cont.prepend($newTask);
			$label.val("");
		};
		var onError = function (error_list) {
			drawErrorSimpleList ($error_cont, error_list);
		};
		that.taskList.createTask({
				label : label
			},
			onSuccess,
			onError
		);
	}
	/**
	@description Private function that returns HTML rendering task.
	@method getHTMLTask
	@param Object task is the task will be used to make HTML rendering task.
	**/
	function getHTMLTask (task) {
		var pend_str = "";
		var current_progress_str = "";
		var delegated_to_str = that.i18n.not_delegated;
		var notification = "";
		if (task.pend !== null) {
			pend_str = that.haussmann.getStrDateFromObj(task.pend);
		}
		if (task.current_progress !== null) {
			current_progress_str = task.current_progress;
		}
		else {
			current_progress_str = 0;
		}
		if (task.delegated_user_id !== null) {
			delegated_to_str = '<a href="mailto:' + task.delegated_user_email + '">' + task.delegated_user_firstname + ' ' + task.delegated_user_lastname + "</a>";
		}
		if (task.attendees.length > 0) {
			delegated_to_str += '<span class="attendee-count" title="'+that.i18n.attendeeCount+'">'+task.attendees.length+'</span>';
		}
		if (task.last_access !== undefined) {
			if (task.last_access !== null) {
				if (parseInt(task.modified.replace(/[^0-9]/g, '')) > parseInt(task.last_access.replace(/[^0-9]/g, ''))) {
					notification = '<span class="task-notification" title="' + that.i18n.task_notification_message + '"></span>';
				}
			}
		}
		
		var $task = $('<div class="project-task item-no-sortable sbox" id="task-id-' + task.task_id + '"></div>');
		$(notification).appendTo($task);
		$('<div class="task-label"><p>' + task.label + '</p></div>').appendTo($task);
		$('<div class="task-delegated">' + delegated_to_str + '</div>').appendTo($task);
		$('<div class="task-pend"><p>' + that.i18n.end + " : " + pend_str + '</p></div>').appendTo($task);
		var $progress = $('<div class="task-current-progress"></div>').appendTo($task);
		var $progress_label = $('<div class="progress-label">' + current_progress_str +' %</div>').appendTo($progress);
		$('<div class="task-edit-icone"><span title="' + that.i18n.edit_task + '" class="h_edit"></span></div>').appendTo($task);
		var $delete = $('<div class="task-delete-icone"><span title="' + that.i18n.delete_task + '" class="h_delete"></span></div>');
		if (mioga_context.rights.Anim || (mioga_context.rights.WriteProjectIfOwner && that.project.user_is_proj_owner === true) ) {
			$task.removeClass('item-no-sortable').addClass('item-sortable');
			$delete.appendTo($task);
		}
		else {
			$task.find('.h_edit').removeClass('h_edit').addClass('h_see');
		}
		
		$progress.progressbar({
			value: false,
			change: function() {
				$progress_label.text( $progress.progressbar( "value" ) + " %" );
			}
		}).progressbar( "value", parseInt(current_progress_str,10) );
		return $task;
	}
	/**
	@description Private function that returns HTML rendering of creation of tree block.
	@method getHtmlAddTreeCont
	@param Int level is level of tree container concerned.
	@param String placeholder is translation of input field placeholder
	**/
	function getHtmlAddTreeCont (level, placeholder) {
		var display = "";
		if (that.$edit_tree_link.hasClass('read-mod') ) {
			display = 'style="display:none;"';
		}
		var elem = '<div class="add-tree-cont edit-cont" ' + display + '>'
			+ '<span name="' + level + '" class="h_create">&#160;</span>'
			+ '<span><input type="text" class="input_new_tree" placeholder="' + placeholder + '"/></span>'
		+ '</div>';

		return elem;
	}
	/**
	@description Private function that returns HTML rendering of tree element according to user access rigths.
	@method getHTMLGroupingTree
	@param Int level is level of tree container concerned
	@param String label is label of tree element
	@param String comment is comment of tree element
	@param Int tree_cont_id is rowid of tree element
	
	**/
	function getHTMLGroupingTree (level, label, comment, tree_cont_id) {
		var lev = level;
		var lab = label;
		var com = comment;
		var t_c_id = tree_cont_id;

		var add_tree_cont = "";
		var h_edit = "";
		var h_delete = "";
		var h_comment = "";
		var comment_edit = "";
		var sortable_class = "item-no-sortable";

		if (comment == null) {
			com = "";
		}
		else {
			MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::getHTMLGroupingTree - comment of grouping tree exists.");
		}
		// span to create new sub level of grouping tasks according to count level in advanced parameters.
		if (that.advancedParams.grouping_tasks.grouping_tree[lev+1] !== undefined) {
			add_tree_cont = getHtmlAddTreeCont ((lev+1), that.i18n.New +  ' ' + that.advancedParams.grouping_tasks.grouping_tree[lev+1]);
		}
		else {
			MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::getHTMLGroupingTree - A grouping tree exists for level +1 : " + lev+1);
		}

		if (mioga_context.rights.Anim || that.project.user_is_proj_owner === true) {
			h_edit = '<span class="h_edit edit-tree-label">&#160;</span>';
			h_delete = '<span class="h_delete">&#160;</span>';
			sortable_class = "item-sortable";
			comment_edit = '<span class="edit_comment" title="">' + that.i18n.edit + '</span>';
		}
		else {
			MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::getHTMLGroupingTree - User is not project owner and has no anim rights. Tree is not sortable, not editable and not deletable. Comment is not editable.");
		}

		if ( mioga_context.rights.Anim || that.project.user_is_proj_owner === true || com.length > 0 ) {
			h_comment = '<span class="tree_comment" title="">' + that.i18n.comment + '</span>';
		}
		else {
			MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::getHTMLGroupingTree - User has anim rights OR he is the owner of project OR tree comment exists. Comment link is visible.");
		}
		var $new_elm = $(
			'<div class="level_' + lev + ' ' + sortable_class + ' sbox">'
				+ '<h2 class="title_bg_color">'
					+ h_delete
					+ '<span class="tree_label" >' + lab + '</span>'
					+ h_edit
					+ h_comment
					+ '<input type="hidden" value="' + lab + '"/>'
				+ '</h2>'
				+ '<div class="content">'
					+ '<div class="grouping_tree_comment" style="display:none;">'
						+ '<div class="comment_cont">'
							+ '<div class="coment_link">'
								+ comment_edit
								+ '<span class="close_comment" title="">' + that.i18n.close + '</span>'
							+ '</div>'
							+ '<div class="comment_content">' + com.replace(/\n/g, '<br>') + '</div>'
						+ '</div>'
					+ '</div>'
					+ '<div class="container_level_' + (lev+1) + ' portlet target" id="tree_cont_' + t_c_id + '">'
						+ '<div class="task-target"></div>'
					+ '</div>' + add_tree_cont
				+ '</div>'
			+ '</div>'
		);
		return $new_elm;
	}
	/**
	@description Private method that creates grouping tree if label is not empty.
	@method createGroupingTree
	@param Object $label is Jquery element represent label field.
	@param Int level is level of created grouping tree.
	@param Object $treeCont is Jquery element represents parent container of new grouping tree 
	**/
	function createGroupingTree ($label, level, $treeCont) {
		var label = $.trim($label.val());
		$label.val('');
		if (label.length > 0) {
			that.project.last_id_tree_task ++;
			var add_cont = "";
			if (that.advancedParams.grouping_tasks.grouping_tree[level+1]) {
				add_cont = getHtmlAddTreeCont ((level+1), that.i18n.New +  ' ' + that.advancedParams.grouping_tasks.grouping_tree[level+1]);
			}
			else {
			}
			var $new_elem = getHTMLGroupingTree (level, label, "", that.project.last_id_tree_task);

			if ($treeCont.attr('id') == "tree_cont_0") {
				$treeCont.append($new_elem);
			}
			else {
				$treeCont.children('.task-target:').before($new_elem);
			}
			$new_elem.foldable();
			$(this).parent().parent().children('h2').find('span.h_delete').remove();
			$(this).parent().find('input:first').val('');

			var $elemSerialize = that.$tree_view_cont.find('.container_level_0:first');
			var params = {
				task_tree : that.treeSerialize($elemSerialize)
			};
			that.project.setProject(params, function () {
				sortableElem();
				that.taskList.setTaskList(that.serializeTaskPosition());
				that.drawDeleteTree ();
				$label.val('');
			});
		}
		else {
			MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::createGroupingTree - label is empty so no action.");
		}
	}
	/**
	@description Private method that activates sortable behavior for tree element and task according to user access rights
	@method sortableElem
	**/
	function sortableElem () {
		// TREE element sortable
		if (mioga_context.rights.Anim || that.project.user_is_proj_owner === true) {
			for(var level = 0; level <= that.advancedParams.grouping_tasks.grouping_tree.length; level ++) {
				that.$tree_view_cont.find('div.container_level_' + level).sortable('destroy').sortable({
					connectWith	: that.$tree_view_cont.find('div[class^="container_level_' + level + '"]'),
					items		: 'div.level_' + level + '.item-sortable:not(.task-target.item-sortable)',
					tolerance 	: 'pointer',
					distance: 15,
					placeholder : "h-sortable-placeholder",
					update		: function(event, ui) {
						var $elemSerialize = that.$tree_view_cont.find('.container_level_0:first');
						var params = {
							task_tree : that.treeSerialize($elemSerialize)
						};
						that.project.setProject(params, function () {
							that.taskList.setTaskList(that.serializeTaskPosition(), function () {
								that.drawDeleteTree ();
								sortableElem();
							});
						});
					},
					start		: function (event, ui) {
						ui.placeholder.height(ui.item.height());
						ui.placeholder.width(ui.item.width());
						ui.placeholder.show();
					}
				});
			}
			// All tasks in tasks list.
			that.$main_right.find('.task-target').sortable({
				connectWith	: '.task-target',
				items		: 'div.project-task.item-sortable',
				tolerance 	: 'pointer',
				distance: 15,
				placeholder : "h-sortable-placeholder",
				update		: function(event, ui) {
					var task_id = ui.item.attr('id').replace('task-id-', '');
					that.taskList.setTaskList(that.serializeTaskPosition(task_id), function () {
						that.drawDeleteTree ();
					});
				},
				start		: function (event, ui) {
					ui.placeholder.height(ui.item.innerHeight());
					ui.placeholder.width(ui.item.innerWidth());
					ui.placeholder.show();
				}
			});
		}
		else {
			MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::sortableElem - user access rights are insufficient for sortable behavior");
		}
	}
	// ===================================================================================================================
	// PUBLICS METHODS
	// ===================================================================================================================
	/**
	@description Public method that show this window and hide other. It calls refresh method. 
	@method showWindow
	**/
	function showWindow() {
		MIOGA.logDebug("ProjectEditor", 1, 'ProjectEditor::showWindow - showWindow loaded');
		that.$cont.show();
		that.refresh();
	}
	/**
	@description Public method that hides this window.
	@method showWindow
	**/
	function hideWindow() {
		MIOGA.logDebug("ProjectEditor", 1, 'ProjectEditor::hideWindow - hideWindow loaded');
		that.$cont.hide();
	}
	/**
	@description Public method that determines the selected view and call method to draws it.
	@method drawView
	**/
	function drawView () {
		MIOGA.logDebug("ProjectEditor", 1, 'ProjectEditor::drawView -loaded');
		if (that.haussmann.current_view.type === "tree") {
			MIOGA.logDebug("ProjectEditor", 1, 'ProjectEditor::drawView -that.haussmann.current_view.type = "tree"');
			that.$left_view_list.find('li.tree_view_link').addClass("selected");
			that.drawTreeView();
			sortableElem ();
			makeTreeReadMod (that.i18n.edit_mod);
		}
		else if (that.haussmann.current_view.type === "list") {
			MIOGA.logDebug("ProjectEditor", 1, 'ProjectEditor::drawView -that.haussmann.current_view.type = "list"');
			that.$left_view_list.find('li.list_view_link').eq(that.haussmann.current_view.index).addClass("selected");
			that.drawListView(that.haussmann.current_view.index);
			makeListReadMod (that.i18n.edit_mod);
		}
		else if (that.haussmann.current_view.type === "result") {
			MIOGA.logDebug("ProjectEditor", 1, 'ProjectEditor::drawView -that.haussmann.current_view.type = "result"');
			that.$left_view_list.find('li.result_view_link').addClass("selected");
			that.drawResultView();
			makeResultReadMod (that.i18n.edit_mod);
		}
		else {
			MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::drawView - current_view type is unknow");
		}
	}
	/**
	@description Public method that draws grouping list view.
	@method drawListView
	@param Int indexOfTask is index of grouping list that is selected
	**/
	function drawListView (indexOfList) {

		var drawListElement = function (index, cb) {
			for (var item=0; item < that.advancedParams.grouping_tasks.grouping_list[index].values.length; item ++) {
				var $new_list = $(
					'<div class="value_' + item + ' sbox">'
					+ '<h2 class="title_bg_color">'
						+ '<span>' + that.advancedParams.grouping_tasks.grouping_list[index].values[item] + '</span>'
					+ '</h2>'
					+ '<div class="content">'
						+ '<div class="task-target"></div>'
					+ '</div>'
				+ '</div>'
				).appendTo(that.$list_view_cont.find('div.list_cont_0'));
				$new_list.foldable();
			}
			if (cb !== undefined) {
				cb();
			}
		};

		that.$list_view_cont.html('<div class="list_cont_0"></div>');
		that.$edit_link_list_cont = $('<div class="edit-link-cont"></div>').prependTo(that.$list_view_cont);
		that.$edit_link_list_cont.append(that.$edit_list_link);
		if (that.advancedParams.grouping_tasks.grouping_list.length > 0) {
			drawListElement(indexOfList, function () {
				that.drawListViewTasks(indexOfList);
			});
				that.$edit_link_list_cont.prepend(that.$toggle_list_link);
		}
		else {
			MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::drawListView - No grouping list is defined in advanced parameters.");
		}
		sortableElem ();
		// hide other view blocks
		that.$result_view_cont.hide();
		that.$tree_view_cont.hide();
		that.$list_view_cont.show();
		that.$no_affected_task_block.show();
	}
	/**
	@description Public method that draws task in grouping list view
	@method drawListViewTasks
	@param Int indexOfTask is index of grouping list that is selected
	**/
	function drawListViewTasks(indexOfList) {
		var tl = that.taskList.getTaskListByPositionList(indexOfList);
		that.$no_affected_task_cont.find('[id^="task-id-"].project-task').remove();

		for (var taskIndex = 0; taskIndex < tl.length; taskIndex ++) {
			var $task= getHTMLTask (tl[taskIndex]);
			if (tl[taskIndex].position_list.length > 0) {
				if (tl[taskIndex].position_list[indexOfList] !== undefined) {
					if (tl[taskIndex].position_list[indexOfList] !== null) {
						if (tl[taskIndex].position_list[indexOfList][1] !== -1) {
							that.$list_view_cont.find('.list_cont_0 div.task-target').eq(tl[taskIndex].position_list[indexOfList][1]).append($task);
						}
						else {
							that.$no_affected_task_cont.append($task);
						}
					}
					else {
						that.$no_affected_task_cont.append($task);
					}
				}
				else {
					that.$no_affected_task_cont.append($task);
				}
			}
			else {
				that.$no_affected_task_cont.append($task);
			}
		}
	}
	/**
	@description Public method that draws result view
	@method drawResultView
	**/
	function drawResultView () {
		
		that.$tree_view_cont.hide();
		that.$list_view_cont.hide();
		that.$no_affected_task_block.show();
		that.$result_view_cont.show();

		that.$result_view_cont.html('<div class="result_cont_0"></div>');
		that.$edit_link_result_cont = $('<div class="edit-link-cont"></div>').prependTo(that.$result_view_cont);
		that.$edit_link_result_cont.append(that.$edit_result_link);
		var h_edit = '';
		if (mioga_context.rights.Anim || that.project.user_is_proj_owner === true) {
			h_edit = '<span class="h_edit">&#160;</span>';
		}
		$.each(that.resultList.result_list, function (i,e) {
			var pend_str = "";
			if (e.pend !== undefined) {
				if (e.pend !== null) {
					pend_str = e.pend.getUTCFullYear() + "-" + (e.pend.getUTCMonth()+1) + "-" + e.pend.getUTCDate();
				}
				else {
					pend_str = that.i18n.not_indicated;
				}
			}
			var $new_elm = $(
				'<div class="value_' + i + ' sbox">'
					+ '<h2 class="title_bg_color ">'
						+ '<span>' + e.label + '</span>'
						+ h_edit
						+ '<span class="result-pend">'  + that.i18n.planned_date + ' : ' + pend_str + '</span>'
					+ '</h2>'
					+ '<div class="content">'
						+ '<div class="task-target"></div>'
					+ '</div>'
				+ '</div>'
			).data({"result_id": e.result_id});
			that.$result_view_cont.find('div.result_cont_0').append($new_elm);

			// behavior to edit result
			$new_elm.find('.h_edit').click(function () {
				var id_res = $(this).parent().parent().data('result_id');
				$.each(that.resultList.result_list, function (ind,elem) {
					if (id_res == elem.result_id) {
						location.hash = that.generateHash("ResultEditor", elem.result_id);
					}
				});
			});
		});

		that.drawResultViewTasks();
		if (that.resultList.result_list.length > 0 ) {
			that.$edit_link_result_cont.prepend(that.$toggle_result_link);
		}
		sortableElem ();
		makeResultReadMod (that.i18n.edit_mod);
		that.$result_view_cont.find('[class^="value_"].sbox').foldable();
	}
	/**
	@description Public method that draws task in result view
	@method drawResultViewTasks
	**/
	function drawResultViewTasks() {
		var tl = that.taskList.getTaskListByPositionResult();
		that.$no_affected_task_cont.find('[id^="task-id-"].project-task').remove();

		for (var taskIndex = 0; taskIndex < tl.length; taskIndex ++) {
			var $task= getHTMLTask (tl[taskIndex]);
			if (tl[taskIndex].position_result.length > 0) {
				var id_res = tl[taskIndex].position_result[0];
				if (id_res !== -1) {
					$.each(that.$result_view_cont.find('[class^="value_"].sbox'), function () {
						var res_id = $(this).data('result_id');
						if (res_id === id_res) {
							$(this).find('.task-target').append($task);
						}
					});
				}
				else {
					that.$no_affected_task_cont.append($task);
				}
			}
			else {
				that.$no_affected_task_cont.append($task);
			}
		}
	}
	/**
	@description Public method that draws tree view element
	@method drawTreeView
	**/
	function drawTreeView() {
		MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::drawTreeView - loaded");
		//	Recursive function that draws tree view 
		var drawTreeElement = function (tab, level) {
			var task_group_id = undefined;
			var comment = "";
			var label = "";
			for (var item=0; item < tab.length; item ++) {
				label = tab[item].label;
				comment = tab[item].comment;
				task_group_id = parseInt(tab[item].task_group_id,10);
				var $new_elem = getHTMLGroupingTree (level, label, comment, task_group_id);
				var $cont = that.$tree_view_cont.find('div.container_level_' + level + ".target").last();
				if ($cont.attr('id') == "tree_cont_0") {
					$cont.append($new_elem);
				}
				else {
					$cont.children('.task-target:').before($new_elem);
				}
				// remove delete button to parent if container is not level 0
				if ($cont.parent().attr('id') != "tree_tasks_view") {
					$cont.parent().find('h2:first').find('span.h_delete').remove();
				}
				drawTreeElement(tab[item].children, (level+1));
			}
			that.drawTreeViewTasks();
		};
		MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::drawTreeView - tree view cont remove children");
		that.$tree_view_cont.children().remove();
		//	create entry tree level one if exists.
		if (that.advancedParams.grouping_tasks.grouping_tree.length > 0) {
			// show all and hide all links
			that.$edit_link_cont = $('<div class="edit-link-cont"></div>').appendTo(that.$tree_view_cont);
			that.$edit_link_cont.append(that.$edit_tree_link);

			that.$tree_view_cont.append(
				'<div class="container_level_0 target" id="tree_cont_0"></div>'
				+ getHtmlAddTreeCont (0, that.i18n.New +  ' ' + that.advancedParams.grouping_tasks.grouping_tree[0])
			);
		}
		//	============================================================================
		//	Generate draw Tree View
		drawTreeElement(that.project.task_tree, 0);
		if (that.project.task_tree.length > 0) {
			that.$edit_link_cont.prepend(that.$toggle_tree_link);
		}
		that.drawDeleteTree ();
		that.$tree_view_cont.find('div[class^="level_"].sbox').foldable();
		// hide other view blocks

		that.$tree_view_cont.show();
		that.$list_view_cont.hide();
		that.$no_affected_task_block.show();
		that.$result_view_cont.hide();
	}
	/**
	@description Public method that draws tasks in tree view
	@method drawTreeViewTasks
	**/
	function drawTreeViewTasks () {
		MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::drawTreeViewTasks - loaded");
		that.$tree_view_cont.find('[id^="task-id-"].project-task').remove();
		that.$no_affected_task_cont.find('[id^="task-id-"].project-task').remove();
		// sort all tasks by positionTree container and position tree index
		var tl = that.taskList.getTaskListByPositionTree ();
		for (var taskIndex = 0; taskIndex < tl.length; taskIndex ++) {
			var $task= getHTMLTask (tl[taskIndex]);
			if (tl[taskIndex].position_tree[0] !== -1) {
				that.$tree_view_cont.find('div[id="tree_cont_' + tl[taskIndex].position_tree[0] + '"] > div.task-target').append($task);
			}
			else {
				that.$no_affected_task_cont.append($task);
			}
			MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::drawTreeViewTasks - SHOW tree view cont");
			that.$tree_view_cont.show();
		}
		that.$result_view_cont.hide();
		that.$list_view_cont.hide();
	}
	/**
	@description Public method that draws or removes delete button on tree element according to children of this contains other title or task.
	@method drawDeleteTree
	**/
	function drawDeleteTree () {
		if (mioga_context.rights.Anim || that.project.user_is_proj_owner === true) {
			that.$tree_view_cont.find('h2').each(function() {
				if ($(this).parent().find('h2').length > 1  || $(this).parent().find('div.project-task').length > 0) {
					$(this).find('span.h_delete:first').remove();
				}
				else {
					if ($(this).children('span.h_delete').length == 0) {
						$(this).prepend('<span class="h_delete">&#160;</span>');
					}
				}
			});
		}
		else {
			that.$tree_view_cont.find('h2 span.h_delete').remove();
		}
	}
	/**
	@description Public method that serializes tree view of current project
	@method treeSerialize
	**/
	function treeSerialize (elm) {
		var task_tree = [];
		elm.find(' > div[class^="level_"]').each(function() {
			task_tree.push({
				"label" : $(this).find('h2:first').find('span.tree_label:first').text().replace(/^\s+/g, ''),
				"comment" : $(this).find('.comment_content:first').html().replace(/<br>/g, '\n').replace(/<BR>/g, '\n'),
				"children" : treeSerialize($(this).find('div[class^="container"]:first')),
				"task_group_id" : parseInt($(this).find('div[class*="container"]:first').attr('id').replace('tree_cont_', ''),10)
			});
		});
		return task_tree;
	};
	/**
	@description Public method that serializes tasks position according to current view (list, tree or result view ) and return an object like : 
				{
					task_list : [
						{
							task_id : INT,
							position_tree : [ ... ]
						},
						{
							task_id : INT,
							position_tree : [ ... ]
						}
					
					]
				}

				For Tree view, the serialization is like : [ 1, 0, [tree_label_1, tree_label_2] ].
					The first element of array is the index of task container ( -1 if no affectaed task ).
					The second element of array is the index of tasks
					the array in array is the label of groupings tree after which this task is containing.
					
				For list view, the serialization is like : [ [0,5], [1,3] ]
					Each grouping list is represented by an array.
					The first number in this array is the index of list,
					The second number is index of task.
					We can have a response like : [null, [1,6] ]. This means that in first view list, the task is no affected.
					
				For result view, the serialization is like : [0,5].
					The first number is the index of result,
					The seconf number is the index of task.
					
				If none list is defined, this means that all task are unaffected. So it seeing in her position_tree that is like [ -1, x ].
	@method refreshLabelDesc
	@param Int moved_task_id is optionnal argument that represents id of drag and droped task. For this task, must be change her modification date
	**/
	function serializeTaskPosition(moved_task_id) {
		
		var params = {"task_list": []};
		// TREE VIEW serialize
		if (that.$left_view_list.find('li.tree_view_link.selected').length > 0) {
			that.$tree_view_cont.find('div[id^="task-id-"].project-task').each(function() {
				var task_id = $(this).attr('id').replace('task-id-', '');
				var task = {
					task_id : task_id,
					position_tree : []
				};
				if (moved_task_id !== undefined) {
					if (moved_task_id !== task_id) {
						task.set_modified = "f";
					}
				}
				else {
					task.set_modified = "f";
				}
				var affectation = [];
				var $taskContainer = $(this).parent().parent();
				while ($taskContainer.parent().parent().children('h2').length !== 0) {
					affectation.push($taskContainer.parent().parent().children('h2').children('span.tree_label').text());
					$taskContainer = $taskContainer.parent().parent().parent();
				}
				task.position_tree.push(parseInt($(this).parent().parent().attr('id').replace('tree_cont_', ''), 10));
				task.position_tree.push($(this).index());
				task.position_tree.push(affectation.reverse());
				params.task_list.push(task);
			});
			that.$no_affected_task_cont.find('div[id^="task-id-"]').each(function () {
				var task_id = $(this).attr('id').replace('task-id-', '');
				var task = {
					task_id : task_id,
					position_tree : [-1, $(this).index(), []]
				};
				if (moved_task_id !== undefined) {
					if (moved_task_id !== task_id) {
						task.set_modified = "f";
					}
				}
				params.task_list.push(task);
			});
			return params;
		}
		// LIST VIEW serialize
		else if (that.$left_view_list.find('li.list_view_link.selected').length > 0) {
			var groupingItem = that.$left_view_list.find('li.list_view_link.selected').attr('value');
			//update position in list
			that.$list_view_cont.find('div[id^="task-id-"].project-task').each(function() {
				var task_id = $(this).attr('id').replace('task-id-', '');
				var task = {
					task_id : task_id,
					position_list : that.taskList.task_list_by_rowid[task_id].position_list
				};
				if (moved_task_id !== undefined) {
					if (moved_task_id !== task_id) {
						task.set_modified = "f";
					}
				}
				
				task.position_list[groupingItem] = [];
				task.position_list[groupingItem][0] = groupingItem;
				task.position_list[groupingItem][1] = $(this).parent().parent().parent().index();
				task.position_list[groupingItem][2] = $(this).parent().children("div.project-task").index($(this));
				params.task_list.push(task);
			});
			// update position in no affected block according to index list
			$.each(that.$no_affected_task_cont.find('div[id^="task-id-"]'), function (i,e) {
				var task_id = $(this).attr('id').replace('task-id-', '');
				var task = {
					task_id : task_id,
					position_list : that.taskList.task_list_by_rowid[task_id].position_list
				};
				if (moved_task_id !== undefined) {
					if (moved_task_id !== task_id) {
						task.set_modified = "f";
					}
				}
				task.position_list[groupingItem] = [];
				task.position_list[groupingItem][0] = groupingItem;
				task.position_list[groupingItem][1] = -1;
				task.position_list[groupingItem][2] = i;
				params.task_list.push(task);
			});
			return params;
		}
		// RESULT VIEW serialize
		else if (that.$left_view_list.find('li.result_view_link.selected').length > 0) {
			that.$result_view_cont.find('div[id^="task-id-"].project-task').each(function() {
				var $result = $(this).parent().parent().parent();
				var task_id = $(this).attr('id').replace('task-id-', '');
				var index_result = that.$result_view_cont.find('div[class^="value_"]').index($result);
				var result_id = $result.data('result_id');
				var task = {
					task_id : task_id,
					position_result : that.taskList.task_list_by_rowid[task_id].position_result
				};
				if (moved_task_id !== undefined) {
					if (moved_task_id !== task_id) {
						task.set_modified = "f";
					}
				}
				task.position_result[0] = result_id;
				task.position_result[1] = $(this).parent().children("div.project-task").index($(this));
				params.task_list.push(task);
			});
			// update position in no affected block according to index list
			$.each(that.$no_affected_task_cont.find('div[id^="task-id-"]'), function (i,e) {
				var task_id = $(this).attr('id').replace('task-id-', '');
				var task = {
					task_id : task_id,
					position_result : [-1,i]
				};
				if (moved_task_id !== undefined) {
					if (moved_task_id !== task_id) {
						task.set_modified = "f";
					}
				}
				params.task_list.push(task);
			});
			return params;
		}
		else {
			$.each(that.$no_affected_task_cont.find('div[id^="task-id-"]'), function (i,e) {
				var task_id = $(this).attr('id').replace('task-id-', '');
				var task = {
					task_id : task_id,
					position_tree : [-1,i]
				};
				if (moved_task_id !== undefined) {
					if (moved_task_id !== task_id) {
						task.set_modified = "f";
					}
				}
				params.task_list.push(task);
			});
			return params;
		}
	}
	// =============================================================================================
	// REFRESH METHODS
	// =============================================================================================
	/**
	@description Public method that refreshs project label and project description. It shows or hides icone to edit project according to user access rights.
	@method refreshLabelDesc
	**/
	function refreshLabelDesc () {
		MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refreshLabelDesc - refreshLabelDesc loaded');
		// label
		that.$left_global_info.find('h2 p').html(that.project.label);
		
		if (mioga_context.rights.Anim || mioga_context.rights.WriteProjectIfOwner) {
			if (mioga_context.rights.Anim || ( mioga_context.rights.WriteProjectIfOwner && that.project.user_is_proj_owner === true )) {
				that.$left_global_info.find('h2 span.h_edit').show();
			}
			else {
				MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::refreshLabelDesc - no right to edit project so don't draw edit button.");
				that.$left_global_info.find('h2 span.h_edit').remove();
			}
		}
		else {
			that.$left_global_info.find('h2 span.h_edit').remove();
		}
		// description
		that.$left_global_info.find('.project-description').html(that.project.description.replace(/\n/g, '<br>')).ellipsis({lines : 10});
	}
	/**
	@description Public method that refresh file selector plugin even if directory to project is a defined attribute.
	@method refreshFileSelector
	**/
	function refreshFileSelector () {
		MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refreshFileSelector - refreshFileSelector loaded');
		if (that.project.base_dir.length > 0) {
			var root = mioga_context.private_dav + "/" + that.project.group_ident + "/" + that.project.base_dir;
			if (that.project.base_dir.indexOf('/') === 0) {
				root = mioga_context.private_dav + "/" + that.project.group_ident + that.project.base_dir;
			}
			else {
				MIOGA.logDebug("ProjectEditor", 1, "ProjectEditor::refreshFileSelector - directory of project files dont begin by /");
			}
			var optFileBrowser = {
				root : root,
				current : "",
				change_directory : function (current_path) {
				}
			};

			that.$left_file_browser.show();
			that.$left_file_browser_cont.children().remove();
			that.$left_file_browser_cont.fileBrowser(optFileBrowser);
		}
		else {
			MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refreshFileSelector - base dir length = 0');
			that.$left_file_browser.hide();
		}
	}
	/**
	@description Public method that refreshs view list container. It test if a grouping is defined in advanced parameters of project. If it is, there is a view can be showed.
					If result view is wanted ("back to result view" from resultEditor), result view is the selected view.
					It call drawView that draw a view according to selected view from this view list.
	@method refreshViewList
	**/
	function refreshViewList () {
		MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refreshViewList - refreshViewList loaded');
		var treeExist = 0;
		var countList = 0;
		var resultExist = 0;
		var tree_list = [];
		that.$left_view_list.find('li').remove();
		// hide all view container
		that.$tree_view_cont.hide();
		that.$list_view_cont.hide();
		that.$result_view_cont.hide();

		// if grouping by tree exists, making string of this and drawing view item
		if (that.advancedParams.grouping_tasks.grouping_tree.length > 0) {
			treeExist = 1;
			that.$left_view_list.find('ul').append('<li class="tree_view_link"></li>');
			$.each(that.advancedParams.grouping_tasks.grouping_tree, function (i,tree) {
				tree_list.push(tree);
			});
			that.$left_view_list.find('ul li:first').append( tree_list.join(' / ') + '</li>');
		}
		else {
			MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refreshViewList - No grouping tree is defined in advanced parameters of project.');
		}
		// if grouping list exists, draw each grouping list
		if (that.advancedParams.grouping_tasks.grouping_list.length > 0) {
			$.each(that.advancedParams.grouping_tasks.grouping_list, function (i,list) {
				countList ++;
				that.$left_view_list.find('ul').append('<li value="' + i + '" class="list_view_link">' + list.label + '</li>');
			});
		}
		else {
			MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refreshViewList - No grouping list is defined in advanced parameters of project.');
		}
		// result view
		if (that.resultList.result_list.length > 0) {
			resultExist = 1;
			that.$left_view_list.find('ul').append('<li class="result_view_link">' + that.i18n.result + '</li>');
		}
		else {
			MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refreshViewList - No result is created for this project.');
		}
		$('<li class="gantt_view_link">' + that.i18n.timelineView + '</li>').appendTo(that.$left_view_list.find('ul'));
		// Determine the current selected view 
		var countView = treeExist + countList + resultExist;
		MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refreshFileSelector - countView = ' + countView);
		if (countView === 0){
			MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refreshFileSelector - countView = 0. current_view = tree and call drawView()');
			that.haussmann.current_view.type = "tree";
			that.drawView();
		}
		else {
			that.drawView();
		}
	}
	/**
	@description Public method that refreshs main page of current project
	@method refresh
	**/
	function refresh () {
		MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refresh - refreh loaded');
		that.project_id = that.project.project_id;
		that.taskList = that.project.taskList;
		that.advancedParams = that.project.advancedParams;
		that.resultList = that.project.resultList;

		that.$cont.find('.error').remove();

		that.refreshLabelDesc ();
		that.refreshFileSelector ();
		that.refreshViewList ();
		that.insertVocabulary(that.advancedParams.vocabulary);
		
		if (mioga_context.rights.Anim || mioga_context.rights.WriteProjectIfOwner) {
			if (mioga_context.rights.Anim === false && that.project.user_is_proj_owner === false) {
				MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refresh - current user is not owner of project and has no anim rights, make read mod for this window.');
				makeReadMod(that.i18n.add_task);
				that.$no_affected_task_block.find('.add_task_link').hide();
				that.$tree_view_cont.find('span.edit-tree-label').unbind('click');
				that.$edit_tree_link.hide();
				that.$edit_result_link.hide();
				that.$edit_list_link.hide();
			}
			else {
				MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refresh - current user is owner of project or has anim rights, no restrictions action here.');
			}
		}
		else {
			MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor::refresh - current user is not owner of project and has no anim rights, make read mod for this window.');
			makeReadMod(that.i18n.add_task);
			that.$no_affected_task_block.find('.add_task_link').hide();
			that.$tree_view_cont.find('span.edit-tree-label').unbind('click');
			that.$edit_tree_link.hide();
			that.$edit_result_link.hide();
			that.$edit_list_link.hide();
		}
	}

	// =============================================================================================
	// INIT main left
	// =============================================================================================
	this.$cont = that.$elem.find('.project-editor').html('');
	this.$main_left = $('<div class="main-left"><a href="' + that.generateHash("ProjectListEditor") + '">' + that.i18n.projectList + '</a></div>').appendTo(that.$cont);
	this.$left_global_info = $('<div class="project-global-info sbox"><h2><span class="h_edit"></span><p></p></h2><p class="project-description"></p></div>').appendTo(that.$main_left);
	this.$left_view_list = $('<div class="project-view-list sbox"><h2>' + that.i18n.viewsList + '</h2><ul class="vmenu"></ul></div>').appendTo(that.$main_left);

	this.$left_file_browser = $('<div class="project-file-browser-cont sbox"><h2>' + that.i18n.projectFiles + '</h2><div class="project-file-browser"></div></div>').appendTo(that.$main_left);
	this.$left_file_browser_cont = this.$left_file_browser.find('.project-file-browser');

	// behavior to edit project
	this.$left_global_info.find('span').click(function () {
		location.hash = that.generateHash("ProjectParamsEditor", that.project.project_id);
	});

	// behavior to change view
	this.$left_view_list.delegate('li', 'click', function (ev) {
		var $item = $(this);
		if ($item.hasClass('selected') === false) {
			$item.parent().find('li').removeClass('selected');
			$item.addClass('selected');
			if ($item.hasClass('tree_view_link') === true) {
				that.haussmann.current_view.type = "tree";
				that.drawView ();
			}
			else if ($item.hasClass('list_view_link') === true) {
				that.haussmann.current_view.type = "list";
				that.haussmann.current_view.index = $item.attr('value');
				that.drawView ();
			}
			else if ($item.hasClass('result_view_link') === true) {
				that.haussmann.current_view.type = "result";
				that.drawView ();
			}
			else if ($item.hasClass('gantt_view_link') === true) {
				location.hash = that.generateHash("GanttProjectEditor", that.project.project_id);
			}
			else {
				MIOGA.logError ("ProjectEditor INIT - behavior to change view - class of selected list view is unknow. Reload current project page", false);
				location.hash = that.generateHash("ProjectEditor", that.project.project_id);
			}
		}
		else {
			MIOGA.logDebug('ProjectEditor', 1, 'ProjectEditor INIT - behavior to change view - item from view list is already selected');
		}
	});
	// ------------------------------------------------------------------------------------------
	// INIT main right project
	// ------------------------------------------------------------------------------------------
	this.$main_right = $(
		'<div class="main-right">'
			+ '<div class="no-affected-task-block sbox">'
				+ '<div class="edit-cont">'
					+ '<span class="toggle-view" title="' + that.i18n.hide + ' ' + that.i18n.task_list + '">' + that.i18n.hide + '</span>'
					+ '<span class="add_task_link read-mod">' + that.i18n.add_task + '</span>'
					+ '<span class="h_create create-task-btn" style="display:none;"></span>'
					+ '<input class="new-task-label-input" placeholder="' + that.i18n.label + '" type="text" style="display:none;"/>'
				+ '</div>'
				+ '<div class="no-affected-task-cont task-target"></div>'
			+ '</div>'
			+ '<div class="tree_view_cont"></div>'
			+ '<div class="list_view_cont"></div>'
			+ '<div class="result_view_cont"></div>'
		+ '</div>'
	).appendTo(that.$cont);
	this.$tree_view_cont = this.$main_right.find('.tree_view_cont');
	this.$list_view_cont = this.$main_right.find('.list_view_cont');
	this.$result_view_cont = this.$main_right.find('.result_view_cont');
	this.$no_affected_task_block =  this.$main_right.find('.no-affected-task-block');
	this.$no_affected_task_cont = this.$no_affected_task_block.find('.no-affected-task-cont');

	this.$toggle_tree_link = $('<span class="toogle-tree-link" title="' + that.i18n.hideAll + '">' + that.i18n.hideAll + '</span>');
	this.$edit_tree_link = $('<span class="edit-tree-link read-mod" title="' + that.i18n.edit_mod + '">' + that.i18n.edit_mod + '</span>');
	this.$toggle_tree_link.after(that.$edit_tree_link);

	this.$toggle_result_link = $('<span class="toogle-result-link" title="' + that.i18n.hideAll + '">' + that.i18n.hideAll + '</span>');
	this.$edit_result_link = $('<span class="edit-result-link read-mod" title="' + that.i18n.read_mod + '">' + that.i18n.read_mod + '</span>');
	this.$toggle_result_link.after(that.$edit_result_link);
	
	this.$toggle_list_link = $('<span class="toogle-list-link" title="' + that.i18n.hideAll + '">' + that.i18n.hideAll + '</span>');
	this.$edit_list_link = $('<span class="edit-list-link read-mod" title="' + that.i18n.edit_mod + '">' + that.i18n.edit_mod + '</span>');
	this.$toggle_list_link.after(that.$edit_list_link);
	
	// TODO show link when edit label functionality will be OK
	//		this.$edit_list_link.hide();
	// --------------------------------
	// -------------------------------------------------------------------------------------------------
	// toggle MODE edit / write
	// toggle hide / show
	// -------------------------------------------------------------------------------------------------
	// show / hide block of no affected tasks
	this.$no_affected_task_block.find('.toggle-view').click(function () {
		var $link = $(this);
		if ($link.text() === that.i18n.show) {
			$link.text(that.i18n.hide);
		}
		else {
			$link.text(that.i18n.show);
		}
		that.$no_affected_task_cont.slideToggle();
	});
	// show / hide input field to create a new task
	this.$no_affected_task_block.delegate('.add_task_link', 'click', function () {
		if ($(this).hasClass('edit-mod')) {
			makeReadMod(that.i18n.add_task);
		}
		else {
			that.$no_affected_task_block.find('.create-task-btn, .new-task-label-input').show().trigger('focus');
			that.edit_mod = "edit";
			$(this).removeClass('read-mod').addClass('edit-mod').html(that.i18n.read_mod);
		}
	});
	// toggle edit mod / read mod to grouping tree elements
	this.$main_right.delegate('.edit-tree-link', 'click', function () {
		if ($(this).hasClass('edit-mod')) {
			makeTreeReadMod(that.i18n.edit_mod);
		}
		else {
			makeTreeEditMod(that.i18n.read_mod);
		}
	});
	// show all and hide all behavior for grouping tree elements
	this.$main_right.delegate('.toogle-tree-link', 'click', function () {
		if ($(this).attr('title') === that.i18n.showAll) {
			$(this).attr('title', that.i18n.hideAll).text(that.i18n.hideAll);
			that.$tree_view_cont.find('.content').show();
			that.$tree_view_cont.find('span.fold-icon.unfold').removeClass('unfold');
		}
		else {
			$(this).attr('title', that.i18n.showAll).text(that.i18n.showAll);
			that.$tree_view_cont.find('.content').hide();
			that.$tree_view_cont.find('span.fold-icon').addClass('unfold');
		}
	});
	// toggle edit mod / read mod to grouping result elements
	this.$main_right.delegate('.edit-result-link', 'click', function () {
		if ($(this).hasClass('edit-mod')) {
			makeResultReadMod(that.i18n.edit_mod);
		}
		else {
			makeResultEditMod(that.i18n.read_mod);
		}
	});
	// show all and hide all behavior for grouping result elements
	this.$main_right.delegate('.toogle-result-link', 'click', function () {
		if ($(this).attr('title') === that.i18n.showAll) {
			$(this).attr('title', that.i18n.hideAll).text(that.i18n.hideAll);
			that.$result_view_cont.find('.content').show();
			that.$result_view_cont.find('span.fold-icon.unfold').removeClass('unfold');
		}
		else {
			$(this).attr('title', that.i18n.showAll).text(that.i18n.showAll);
			that.$result_view_cont.find('.content').hide();
			that.$result_view_cont.find('span.fold-icon').addClass('unfold');
		}
	});
	// toggle edit mod / read mod to grouping list elements
	this.$main_right.delegate('.edit-list-link', 'click', function () {
		if ($(this).hasClass('edit-mod')) {
			makeListReadMod(that.i18n.edit_mod);
		}
		else {
			makeListEditMod(that.i18n.read_mod);
		}
	});
	// show all and hide all behavior for grouping list elements
	this.$main_right.delegate('.toogle-list-link', 'click', function () {
		if ($(this).attr('title') === that.i18n.showAll) {
			$(this).attr('title', that.i18n.hideAll).text(that.i18n.hideAll);
			that.$list_view_cont.find('.content').show();
			that.$list_view_cont.find('span.fold-icon.unfold').removeClass('unfold');
		}
		else {
			$(this).attr('title', that.i18n.showAll).text(that.i18n.showAll);
			that.$list_view_cont.find('.content').hide();
			that.$list_view_cont.find('span.fold-icon').addClass('unfold');
		}
	});
	// -------------------------------------------------------------------------------------------------
	// TASKS
	// -------------------------------------------------------------------------------------------------
	// Behavior to CREATE task
	this.$main_right.find('.new-task-label-input').click(function () {
		that.$main_right.find('.no-affected-task-block .error').remove();
	});
	this.$main_right.find('.create-task-btn').click(function () {
		var $label = $(this).parent().find(".new-task-label-input");
		createNewTask ($label);
	});
	this.$no_affected_task_block.delegate('input.new-task-label-input','keypress',function(evt) {
		if ((evt.keyCode == 13) && (evt.target.type !== "input")) {
			var $label = $(this);
			createNewTask ($label);
			return (false);
		}
	});
	// Behavior to EDIT task
	this.$main_right.delegate('.project-task .task-edit-icone span.h_edit, .project-task .task-edit-icone span.h_see', 'click', function (ev) {
		var task_id = $(this).parent().parent().attr('id').replace('task-id-', '');
		location.hash = that.generateHash('TaskEditor', task_id);
	});

	// Behavior to DELETE task
	this.$main_right.delegate('.project-task .h_delete', 'click', function (ev) {
		var $task =  $(this).parent().parent();
		var task_id = parseInt($task.attr('id').replace('task-id-', ''), 10);
		MIOGA.deleteConfirm({
			title : that.i18n.delete_task + ' : ' + that.taskList.task_list_by_rowid[task_id].label,
			i18n_OK : that.i18n.delete_task,
			i18n_cancel : that.i18n.cancel,
			message : '<p class="error"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+ that.i18n.permanently_remove_message + '</br>' + that.i18n.remove_message_confirmation + '</p>',
			cb_OK : function (ev,$dial) {
				that.taskList.deleteTask({
					task_id : task_id
				},
				function () {
					$task.remove();
					$dial.dialog('destroy');
				});
			},
			cb_cancel : function (ev,$dial) {
				$dial.dialog('destroy');
			}
		});
	});
	// -------------------------------------------------------------------------------------------------
	// GROUPING TREE
	// -------------------------------------------------------------------------------------------------
	//	Behavior to CREATE grouping tree
	this.$tree_view_cont.delegate('span.h_create','click',function() {
		var $label = $(this).parent().find('input:first');
		var level = parseInt($(this).attr('name'));
		var $treeCont = $(this).parent().parent().find('.target:first');
		createGroupingTree($label, level, $treeCont);
		if (that.$tree_view_cont.find('.toogle-tree-link').length === 0) {
			that.$edit_link_cont.prepend(that.$toggle_tree_link);
		}
		else {
			MIOGA.logDebug('ProjectEditor', 2, 'ProjectEditor INIT - toggle tree link si founded');
		}
	});
	this.$tree_view_cont.delegate('input.input_new_tree','keypress',function(evt) {
		if ((evt.keyCode == 13) && (evt.target.type !== "input")) {
			var $label = $(this);
			var level = parseInt($(this).parent().parent().children('span.h_create').attr('name'));
			var $treeCont = $(this).parent().parent().parent().find('.target:first');
			createGroupingTree($label, level, $treeCont);
			if (that.$tree_view_cont.find('.toogle-tree-link').length === 0) {
				that.$edit_link_cont.prepend(that.$toggle_tree_link);
			}
			return (false);
		}
		else {
			MIOGA.logDebug('ProjectEditor', 2, 'ProjectEditor INIT - key press for create grouping tree but key is not 13 or target dif to input');
		}
	});
	//	Behavior to DELETE grouping tree
	this.$tree_view_cont.delegate('h2 span.h_delete', 'click', function() {
		var $container = $(this).parent().parent().parent().parent();
		var $delIcon = $(this);

		MIOGA.deleteConfirm({
			title : that.i18n.removeItem ,
			i18n_OK : that.i18n.removeItem,
			i18n_cancel : that.i18n.cancel,
			message : '<div class="tree-delete-dialog"><p class="error"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+ that.i18n.permanently_remove_message + '</br>' + that.i18n.remove_message_confirmation + '</p></div>',
			cb_OK : function (ev,$dial) {
				$delIcon.parent().parent().remove();
				var $elemSerialize = that.$tree_view_cont.find('.container_level_0:first');
				var params = {
					task_tree : treeSerialize($elemSerialize)
				};
				that.project.setProject(params, function () {

					that.taskList.setTaskList(that.serializeTaskPosition(), function () {
						that.drawDeleteTree ();
						$dial.dialog('destroy');
					});
				});
			},
			cb_cancel : function (ev,$dial) {
				$dial.dialog('destroy');
			}
		});
	});

	//	Behavior to EDIT grouping tree label
	this.$tree_view_cont.delegate('span.edit-tree-label', 'click', function() {
		
		var rename = function ($ok) {
			$ok.parent().parent().parent().find('span.h_edit').show();
			var $newTitle = $ok.next('input[type="text"]');
			var newTitle  = $.trim($ok.next('input[type="text"]').val());
			if (newTitle.length > 0) {
				$ok.parent().parent().parent().find('input[type="hidden"]').attr("value", newTitle);
				$ok.parent().parent().parent().find('span.tree_label').html(newTitle);
				var $elemSerialize = that.$tree_view_cont.find('.container_level_0:first');
				var params = {
					task_tree : that.treeSerialize($elemSerialize)
				};
				that.project.setProject(params, function () {
					that.taskList.setTaskList(that.serializeTaskPosition(), function () {
						$('.ui-sortable').sortable('option', 'disabled', false );
					});
				});
			}
			else {
				$newTitle
				.addClass('h_error')
				.attr('placeholder', that.i18n.disallowEmpty)
				.focus(function() {
					$(this)
						.removeClass('h_error')
						.removeAttr('placeholder')
						.unbind('focus');
					}
				);
			}
		};

		$('.ui-sortable').sortable('option', 'disabled', true );
		$(this).hide();
		var indexContainer = $('div.target').index($(this).parent().parent());
		var inputEdit = '<span class="h_setTitle"><span class="h_cancel">&#160;</span><span class="h_ok" id="index_' + indexContainer + '">&#160;</span><input type="text" value="' + $(this).parent('h2').find('input[type="hidden"]').val() + '"/></span>';
		$(this).parent('h2').find('span.tree_label').html(inputEdit);
		
		$(this).parent('h2').find('span.tree_label input[type="text"]').on('keypress',function(evt) {
			if ((evt.keyCode == 13) && (evt.target.type !== "input")) {
				rename ($(this).parent().find('.h_ok'));
				return (false);
			}
		});
		
		$(this).parent('h2').find('span.h_setTitle span.h_ok').click(function() {
			rename ($(this));
		});
		$(this).parent('h2').find('span.h_cancel').click(function() {
			var $h2 = $(this).parent().parent().parent('h2');
			$h2.find('.tree_label').html($h2.find('input[type="hidden"]').val());
			$h2.find('span.edit-tree-label').show();
			$('.ui-sortable').sortable('option', 'disabled', false );
		});
	});
	// -------------------------------------------------------------------------------------------------
	// GROUPING TREE COMMENT
	// -------------------------------------------------------------------------------------------------
	this.$dial_edit_tree_comment = $('<div class="dial-tree-comment"></div>').dialog({
		resizable : false,
		autoOpen : false,
		modal : true,
		width : "550",
		height : "280",
		title : that.i18n.comment_of,
		open : function () {
			that.$dial_edit_tree_comment.append(that.$form_edit_tree_comment);
			that.$tree_comment_text.focus();
		}
	});
	this.$form_edit_tree_comment = $(
		'<form id="' + that.edit_tree_comment_form_id + '" class="form form-edit-tree-comment">'
			+ '<textarea class="tree-comment-text"></textarea>'
			+ '<div class="tree-comment-actions-cont">'
				+ '<ul class="button_list">'
					+ '<li><input type="button" class="button" value="' + that.i18n.apply + '"/></li>'
					+ '<li><input type="button" class="button cancel" value="' + that.i18n.cancel + '"/></li>'
				+ '</ul>'
			+ '</div>'
		+ '</form>'
	).appendTo(that.$dial_edit_tree_comment);

	this.$tree_comment_text = this.$form_edit_tree_comment.find('.tree-comment-text');
	this.$tree_comment_actions_cont = this.$form_edit_tree_comment.find('.tree-comment-actions-cont');
	// VALIDATE tree comment dialog
	this.$tree_comment_apply = this.$tree_comment_actions_cont.find('input:first').click(function () {
		var $comment_cont = that.$dial_edit_tree_comment.data('$comment_cont');
		var text = that.$tree_comment_text.val();
		$comment_cont.find('.comment_content').html(text.replace(/\n/g, '<br>'));
		var $elemSerialize = that.$tree_view_cont.find('.container_level_0:first');
		var params = {
			task_tree : that.treeSerialize($elemSerialize)
		};
		that.project.setProject(params, function () {
			that.$dial_edit_tree_comment.dialog('close');
			$comment_cont.show();
		});
	});
	// CANCEL tree comment dialog
	this.$tree_comment_cancel = this.$tree_comment_actions_cont.find('input:last').click(function () {
		that.$dial_edit_tree_comment.dialog('close');
	});
	// EDIT tree comment behavior
	this.$tree_view_cont.delegate('.edit_comment', 'click', function () {
		var $comment_cont = $(this).parent().parent().parent();
		var tree_label = $(this).parent().parent().parent().parent().parent().find('.tree_label:first').html();
		that.$dial_edit_tree_comment.data('$comment_cont', $comment_cont);
		that.$dial_edit_tree_comment.data('tree_label', tree_label);
		that.$tree_comment_text.val($comment_cont.find('.comment_content').html().replace(/<br>/g, '\n').replace(/<BR>/g, '\n'));
		that.$dial_edit_tree_comment.dialog('option', 'title',that.i18n.comment_of + " : " + tree_label).dialog('open');
	});
	// CLOSE tree comment behavior
	this.$tree_view_cont.delegate('.close_comment', 'click', function () {
		$(this).parent().parent().parent().hide('slow');
	});
	// LINK tree comment behavior
	// 		If no comment, open tree comment dialog for create it.
	// 		If exists comment, draw comment block
	this.$tree_view_cont.delegate('.tree_comment', 'click', function () {
		var current_comment = $(this).parent().parent().find('.comment_content:first').html();
		if (current_comment.length === 0) {
			var $comment_cont = $(this).parent().parent().find('.grouping_tree_comment:first');
			var tree_label = $(this).parent().find('.tree_label:first').html();

			that.$dial_edit_tree_comment.data('$comment_cont', $comment_cont);
			that.$dial_edit_tree_comment.data('tree_label', tree_label);
			that.$tree_comment_text.val('');
			that.$dial_edit_tree_comment.dialog('option', 'title',that.i18n.comment_of + " : " + tree_label).dialog('open');
		}
		else {
			$(this).parent().next('div.content').find('.grouping_tree_comment:first').show('slow');
		}
		if ($(this).parent().find('span.fold-icon.unfold').length > 0) {
			$(this).parent().find('span.fold-icon').trigger('click');
		}
		else {
			MIOGA.logDebug('ProjectEditor', 2, 'ProjectEditor INIT - span to fold icon not found.');
		}
	});
} // End ProjectEditor object
