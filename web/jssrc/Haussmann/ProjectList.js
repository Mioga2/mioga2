// =============================================================================
/**
@description Create a ProjectList object.

    var options = {
        haussmann : { .... }
    };

@class ProjectList
@constructor
**/
// =============================================================================

function ProjectList (options) {
	"use strict";
	MIOGA.debug.ProjectList = 0;
	MIOGA.logDebug('ProjectList', 1, ' ProjectList loaded - options : ', options);
	var that = this;
	this.i18n = options.i18n;
	this.haussmann = options.haussmann;
	this.current_project_id = null;
	this.user_preferences = undefined;
	this.project_list_by_rowid = {};
	this.project_list = [];
	this.refreshCB = [];
	this.modelList = new ModelList({
		haussmann : that.haussmann
	});
	// publics method declaration
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;
	this.projectIsUnique = projectIsUnique;
	this.getProjectByTask = getProjectByTask;
	this.getProjectByResult = getProjectByResult;
	this.getProjectListByLabel = getProjectListByLabel;
	this.getProjectList = getProjectList;
	this.createProject = createProject;
	this.deleteProject = deleteProject;

	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================================================
	// PRIVATES METHODS
	// ===================================================================================================================
	/**
	@description Private method that adds project in project_list
	@method addProject
	@param Object args is Project object
	@param Function cb is callback called on add project success
	**/
	function addProject(new_project, cb) {
		MIOGA.logDebug('ProjectList', 1, 'ProjectList::addProject - loaded, new_project = ', new_project);
		that.project_list.push(new_project);
		that.project_list_by_rowid[new_project.project_id] = new_project;
		if (cb !== undefined) {
			cb(new_project);
		}
	}
	// ===================================================================================================================
	// PUBLICS METHODS
	// ===================================================================================================================
	/**
	@description Public method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('Project', 1, 'Project::addRefreshCB - loaded');
		that.refreshCB.push( { cb : cb, context : context });
	}
	/**
	@description Public method that loads each callback.
	@method callRefreshCB
	**/
	function callRefreshCB() {
		MIOGA.logDebug('Project', 1, 'Project::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that checks if project is unique
	@method projectIsUnique
	@param Object args contains project_id and label
	**/
	function projectIsUnique (args) {
		MIOGA.logDebug('ProjectList', 1, 'ProjectList::projectIsUnique - loaded. args : ', args);
		var result = true;
		$.each(that.project_list, function (i,e) {
			if (args.project_id !== e.project_id && args.label === e.label) {
				result = false;
			}
		});
		return result;
	}
	/**
	@description Public method that retrieves project by task id
	@method getProjectByTask
	@param Bool async that determines if AJAX request is sunchrone or asynchrone
	@param Int task_id is rowid of task
	**/
	function getProjectByTask (async, task_id) {
		MIOGA.logDebug('ProjectList', 1, 'ProjectList::getProjectByTask - getProjectByTask loaded. task_id : ', task_id);
		var params = {
			task_id : task_id
		};
		var project = undefined;
		$.ajax({
			async : async,
			url : "GetProjectByTask.json",
			data : params,
			success : function (response) {
				if (response.status === "OK") {
					if (response.data !== null) {
						var project_attr = {};
						$.each(response.data, function (key,val) {
							project_attr[key] = val;
						});
						project_attr.user_preferences = response.data.user_preferences;
						project_attr.current_project_id = response.data.current_project_id;

						project_attr.projectIsUnique = that.projectIsUnique;
						project_attr.modelList = that.modelList;
						project_attr.haussmann = that.haussmann;

						project = new Project(project_attr);
						that.project_list_by_rowid[response.data.project_id] = project;
						for ( var i= 0; i < that.project_list.length; i++ ) {
							if (that.project_list[i].project_id === response.data.project_id) {
								that.project_list[i] = project;
								break;
							}
						}
					}
					else {
						project = null;
					}
				}
				else {
					MIOGA.logError ("ProjectList. getProjectByTask AJAX request returned KO status", false);
				}
			},
			error : function (err) {
				MIOGA.logError ("ProjectList. getProjectByTask AJAX request returned an error : " + err, false);
			}
		});
		return project;
	}
	/**
	@description Public method that retrieves project by result id
	@method getProjectByResult
	@param Bool async that determines if AJAX request is synchrone or asynchrone
	@param Int result_id is rowid of result
	**/
	function getProjectByResult (async, result_id) {
		MIOGA.logDebug('ProjectList', 1, 'ProjectList::getProjectByResult - getProjectByResult loaded. result_id : ', result_id);
		var params = {
			result_id : result_id
		};
		var project = undefined;
		$.ajax({
			async : async,
			url : "GetProjectByResult.json",
			data : params,
			success : function (response) {
				if (response.status === "OK") {
					if (response.data !== null) {
						var project_attr = {};
						$.each(response.data, function (key,val) {
							project_attr[key] = val;
						});
						project_attr.user_preferences = response.data.user_preferences;
						project_attr.current_project_id = response.data.current_project_id;

						project_attr.projectIsUnique = that.projectIsUnique;
						project_attr.modelList = that.modelList;
						project_attr.haussmann = that.haussmann;

						project = new Project(project_attr);
						that.project_list_by_rowid[response.data.project_id] = project;
						for ( var i= 0; i < that.project_list.length; i++ ) {
							if (that.project_list[i].project_id === response.data.project_id) {
								that.project_list[i] = project;
								break;
							}
						}
					}
					else {
						project = null;
					}
				}
				else {
					MIOGA.logError ("ProjectList. getProjectByResult AJAX request returned KO status", false);
				}
			},
			error : function (err) {
				MIOGA.logError ("ProjectList. getProjectByResult AJAX request returned an error : " + err, false);
			}
		});
		return project;
	}
	/**
	@description Public method that sorts project by label
	@method getProjectListByLabel
	**/
	function getProjectListByLabel () {
		MIOGA.logDebug('ProjectList', 1, 'ProjectList::getProjectListByLabel - getProjectListByLabel loaded');
		var project_list = $.extend([], true, that.project_list);
		project_list.sort(function(a,b){
			if (a.label > b.label) {
				return 1;
			}
			else {
				return -1;
			}
		});
		return project_list;
	}
	/**
	@description Public method that retrieves project list
	@method getProjectList
	@param Bool async that determines if AJAX request is sunchrone or asynchrone
	@param Function  cb is callback called on AJAX request success
	**/
	function getProjectList (async, cb) {
		MIOGA.logDebug('ProjectList', 1, 'ProjectList::getProjectList - getProjectList loaded');
		$.ajax({
			async : async,
			url : "GetProjectList.json",
			success : function (response) {
				if (response.status === "OK") {
					MIOGA.logDebug('ProjectList', 1, ' getProjectList response');
					that.user_preferences = response.data.user_preferences;

					$.each(response.data.project_list, function (i,e) {
						if (e.current_project_id === "t") {
							that.current_project_id = parseInt(e.project_id,10);
						}
						e.projectIsUnique = that.projectIsUnique;
						e.user_preferences = that.user_preferences;
						e.modelList = that.modelList;
						e.haussmann = that.haussmann;
						var new_project = new Project(e);
						if (new_project !== undefined) {
							addProject(new_project);
						}
						else {
							MIOGA.logError ("ProjectList. getProjectList method can not created a Project Object from Ajax request response. Return on main page...", false);
						}
					});
				}
				else {
					MIOGA.logError ("ProjectList. getProjectList AJAX request returned KO status", false);
				}
			},
			error : function (err) {
				MIOGA.logError ("ProjectList. getProjectList AJAX request returned an error : " + err, false);
			}
		});
	}
	/**
	@description Public method that creates project
	@method createProject
	@param Bool async that determines if AJAX request is sunchrone or asynchrone
	@param Object args contains pairs key / value : model_id ( optionnal ) and label
	@param Function onSuccess that called on AJAX request success
	@param Function onError that called on AJAX request faillure
	**/
	function createProject (async,args, onSuccess, onError) {
		MIOGA.logDebug('ProjectList', 1, 'ProjectList::createProject - createProject loaded');
		var params = {};
		params.label = args.label;
		if (args.model_id !== undefined) {
			params.model_id = args.model_id;
		}
		if (that.projectIsUnique (params) === true) {
			$.ajax({
				url : "CreateProject.json",
				data : params,
				dataType:'json',
				async : async,
				type : "POST",
				success : function (response) {
					if (response.status === "OK") {
						var attr = response.data;
						attr.projectIsUnique = that.projectIsUnique;
						attr.haussmann = that.haussmann;
						attr.modelList = that.modelList;
						var new_project = new Project(attr);
						if (new_project !== undefined) {
							that.project_list.push(new_project);
							that.project_list_by_rowid[new_project.project_id] = new_project;
							MIOGA.logDebug('ProjectList', 1, 'ProjectList::createProject - insert in INDEX !!', that.project_list_by_rowid);
							that.current_project_id = response.data.project_id;
							if (onSuccess !== undefined) {
								onSuccess(new_project);
							}
						}
						else {
							MIOGA.logError ("ProjectList. createdProject method can not created a Project Object from Ajax request response. Return on main page...", true);
						}
					}
					else {
						MIOGA.logError ("ProjectList. Status of CreateProject AJAX request is 'KO'. The API last good status will be reloaded", true);
					}
				},
				error : function (err) {
					MIOGA.logError ("ProjectList. createProject AJAX request returned an error : " + err, true);
				}
			});
		}
		else {
			if (onError) {
				onError (params);
			}
		}
	}
	/**
	@description Public method that removes a project
	@method deleteProject
	@param Object args contains pairs key / value : project_id
	@param Function cb callback called on AJAX request is success
	**/
	function deleteProject (args, cb) {
		MIOGA.logDebug('ProjectList', 1, 'ProjectList::deleteProject - deleteProject loaded');
		var params = {};
		params.project_id = args.project_id;
		$.ajax({
			url : "DeleteProject.json",
			data : params,
			dataType : "JSON",
			method : "POST",
			success : function (response) {
				if (response.status === "OK" ) {
					if (cb !== undefined) {
						cb(that);
					}
					else {
						window.location.reload();
					}
				}
				else {
					//MIOGA.logError ("ProjectList. deleteProject AJAX request returned KO status", false);
					alert(that.i18n.errorDeleteProject);
					that.callRefreshCB();
				}
			},
			error : function (err) {
				MIOGA.logError ("ProjectList. DeleteProject AJAX request returned an error : " + err, true);
			}
		});
	}
	// -------------------------------------------------------
	// INIT
	// -------------------------------------------------------
	this.getProjectList(false);
} // End ProjectList
