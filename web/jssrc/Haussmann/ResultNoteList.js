// =============================================================================
/**
@description Create a ResultNoteList object.

    var options = {
        haussmann : { .... }
    };

@class ResultNoteList
@constructor
**/
// =============================================================================
function ResultNoteList (options) {
	"use strict";
	MIOGA.debug.ResultNoteList = 0;
	MIOGA.logDebug('ResultNoteList', 1, ' ResultNoteList loaded');
	
	var that = this;
	this.project_id = options.project_id;
	this.result_id = options.result_id;

	this.project_owner_id = options.project_owner_id;
	this.project_owner_firstname = options.project_owner_firstname;
	this.project_owner_lastname = options.project_owner_lastname;
	this.project_owner_email = options.project_owner_email;

	this.result_note_list = [];
	this.result_note_list_by_rowid = {};
	
	// publics method declaration
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;
	this.isEmpty = isEmpty;
	this.checkResultNote = checkResultNote;
	this.createResultNote = createResultNote;
	this.deleteResultNote = deleteResultNote;
	this.addResultNote = addResultNote;
	this.getResultNoteList = getResultNoteList;

	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================================================
	// PRIVATES METHODS
	// ===================================================================================================================
	
	// ===================================================================================================================
	// PUBLICS METHODS
	// ===================================================================================================================
	/**
	@description Public method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('ResultNoteList', 1, 'ResultNoteList::addRefreshCB - loaded');
		this.refreshCB.push( { cb : cb, context : context });
	}
	/**
	@description Public method that loads each callback.
	@method callRefreshCB
	**/
	function callRefreshCB() {
		MIOGA.logDebug('ResultNoteList', 1, 'ResultNoteList::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that checks if arguments length is empty. If this is it, that returns true, else returns false.
	@method isEmpty
	@param Object elem can be all type. This is the element that will be tested.
	@param String error_str that is a string value that represents the error message if tested element is empty.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isEmpty (elem, error_str, global_result) {
		MIOGA.logDebug('ResultNoteList', 1, 'ResultNoteList::isEmpty - loaded');
		var result = false;
		
		if (elem.length === 0) {
			if (global_result) {
				global_result.status = "error";
				global_result.error.push(error_str);
			}
			result = true;
		}
		return result;
	}
	/**
	@description Public method that checks result note validity
	@method checkResultNote
	@param Object args that is result note as pair key / value : text
	**/
	function checkResultNote (args) {
		MIOGA.logDebug('ResultNoteList', 1, 'ResultNoteList::checkResultNote - loaded');
		var result = {
			status : "success",
			error : []
		};
		isEmpty(args.text, "textDisallowEmpty", result);
		return result;
	}
	/**
	@description Public method that creates result note
	@method createResultNote
	@param Object args contains pairs key / value : text
	@param Function cb that called on AJAX request success
	**/
	function createResultNote(args, cb) {
		MIOGA.logDebug('ResultNoteList', 1, 'ResultNoteList::createResultNote - loaded');
		var params = {
			project_id : that.project_id,
			result_id : that.result_id,
			text : args.text
		};
		$.ajax({
			url : "CreateResultNote.json",
			dataType :'json',
			data : params,
			type : "POST",
			async : false,
			success : function(response){
				if (response.status === "OK") {
					var new_result_note = new ResultNote({
						created : response.data.created,
						modified : response.data.modified,
						project_id : that.project_id,
						result_id : that.result_id,
						result_note_id : parseInt(response.data.result_note_id,10),
						text : response.data.text,
						owner_id : parseInt(response.data.owner_id,10),
						firstname : response.data.firstname,
						lastname : response.data.lastname,
						email : response.data.email,
						project_owner_id : that.project_owner_id,
						project_owner_firstname : that.project_owner_firstname,
						project_owner_lastname : that.project_owner_lastname,
						project_owner_email : that.project_owner_email
					});
					if (new_result_note !== undefined) {
						that.addResultNote(new_result_note, cb);
					}
					else {
						MIOGA.logError ("ResultNoteList. createdResultNote method can not created a ResultNote Object from Ajax request response. Return on main page...", true);
					}
				}
				else {
					MIOGA.logError ("ResultNoteList. Status of createdResultNote AJAX request is 'KO'. The API last good status will be reloaded", false);
 					that.callRefreshCB();
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("ResultNoteList. createdResultNote AJAX request returned an error : " + err, true);
			}
		});
	}
	/**
	@description Public method that removes result note
	@method deleteResultNote
	@param Object args contains pairs key / value : result_note_id
	@param Function cb that called on AJAX request success
	**/
	function deleteResultNote (args, cb) {
		MIOGA.logDebug('ResultNoteList', 1, 'ResultNoteList::deleteResultNote - loaded');
		var params = {
			result_note_id : args.result_note_id,
			result_id : that.result_id,
			project_id : that.project_id
		};
		$.ajax({
			url : "DeleteResultNote.json",
			dataType : 'json',
			data : params,
			type : "POST",
			success : function(response){ 
				if (response.status === "OK") {
					for (var i = 0; i < that.result_note_list.length; i++) {
						if (that.result_note_list[i].result_note_id === params.result_note_id) {
							that.result_note_list.splice(i,1);
							delete that.result_note_list_by_rowid[params.result_note_id];
							break;
						}
					}
					if (cb !== undefined) {
						cb(that);
					}
					else {
						that.callRefreshCB();
					}
				}
				else {
					MIOGA.logError ("ResultNoteList. Status of deleteResultNote AJAX request is 'KO'. The API last good status will be reloaded", false);
 					that.callRefreshCB();
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("ResultNoteList. deleteResultNote AJAX request returned an error : " + err, true);
			}
		});
	}
	/**
	@description Public method that add result note in result note list
	@method addResultNote
	@param Object new_result_note that is resultNote object
	@param Function cb that callback called on adding result note success
	**/
	function addResultNote(new_result_note, cb) {
		MIOGA.logDebug('ResultNoteList', 1, 'ResultNoteList::addResultNote - loaded');
		that.result_note_list.push(new_result_note);
		that.result_note_list_by_rowid[new_result_note.result_note_id] = new_result_note;
		if (cb !== undefined) {
			cb(new_result_note);
		}
	}
	/**
	@description Public method that retrieves result note list
	@method getResultNoteList
	@param Bool async that determines if AJAX request is synchronous or asynchronous
	**/
	function getResultNoteList (async) {
		MIOGA.logDebug('ResultNoteList', 1, 'ResultNoteList::getResultNoteList - loaded');
		var params = {
			project_id : that.project_id,
			result_id : that.result_id
		};
		$.ajax({
			url : "GetResultNoteList.json",
			async : async,
			dataType : 'json',
			type : "GET",
			data : params,
			success : function(response){
				if (response.status === "OK") {
					$.each(response.data, function (i, result_note) {
						var new_result_note = new ResultNote({
							project_id : that.project_id,
							result_id : that.result_id,
							result_note_id : parseInt(result_note.result_note_id,10),
							text : result_note.text,
							created : result_note.created,
							modified : result_note.modified,
							published : result_note.published,
							status : result_note.status,
							owner_id : parseInt(result_note.owner_id,10),
							firstname : result_note.firstname,
							lastname : result_note.lastname,
							email : result_note.email,
							project_owner_id : that.project_owner_id,
							project_owner_firstname : that.project_owner_firstname,
							project_owner_lastname : that.project_owner_lastname,
							project_owner_email : that.project_owner_email,
							created_str : result_note.created
						});
						if (new_result_note !== undefined) {
							that.addResultNote(new_result_note);
						}
						else {
							MIOGA.logError ("ResultNoteList. getResultNoteList method can not created a ResultNote Object from Ajax request response. Return on main page...", true);
						}
					});
					// sorting by created
					that.result_note_list.sort(function (a,b){
						if (a.created_str > b.created_str) {
							return 1;
						}
						else {
							return -1;
						}
					});
				}
				else {
					MIOGA.logError ("ResultNoteList. getResultNoteList AJAX request returned KO status", false);
				}
			},
			error : function (err) {
				MIOGA.logError ("ResultNoteList. getResultNoteList AJAX request returned an error : " + err, false);
			}
		});
	}
	// ===================================================================================================================
	// INIT
	// ===================================================================================================================
	this.getResultNoteList(false);

	if (
		this.project_id === undefined || this.result_id === undefined || 
		this.project_owner_id === undefined || this.project_owner_firstname === undefined || 
		this.project_owner_lastname === undefined || this.project_owner_email === undefined
		) {
		MIOGA.logError ("ResultNoteList. Object can not be initialized.", false);
		return undefined;
	}
} // End ResultNoteList object
