(function( $ ){
	// declaration of plugin itemSelect
	// options can be : 
	
	//	{
	//		data : "URL of WS that return array of item" or array of item or function that return array of item,
	//		i18n : {
	//			select_all	: "Select all",
	//			deselect_all: "Deselect all",
	//			add_btn 	: "Add",
	//			close_btn	: "Close"
	//		},
	//		******* optionals arguments *******
	//		single : boolean,
	//		select_add		: CB called when the user clicks on the submit button,
	//		select_close	: CB called each selection or deselection,
	//		select_close	: CB called when the user clicks on the close button
	//	}
	
	// ******  usage :  ********************** 
	//		$('#id-cont').itemSelect(options);
	// ***************************************
	var defaults = {
		single 	: false,
		show_size : false,
		i18n	: {
				add_file	: "Add file",
				upload		: "Upload",
				drop_here   : "Drop Here",
		},
		current_path		: undefined,
		select_up		: undefined,
		select_change	: undefined,
		select_close	: undefined
    };
	// ==================================================
	// ========= privates methods =======================
	// ==================================================
	function convertSize (size) {
		var size_converted = 0;
		if (size < 10) {
			size_converted = Math.round(size, 2) + " Bytes"; 
		}
		else if (size < 1048576) {
			size_converted = parseFloat(size/1024).toFixed(2) + " KB";
		}
		else if (size < 1073741824) {
			size_converted = parseFloat(size/1048576.00).toFixed(2) + " MB"; 
		}
		else {
			size_converted = parseFloat(size/1073741824.00).toFixed(2) + " GB";
		}
		return size_converted;
	}
	function addFile ($this, show_size, file) {
		var file_exists = false;
    	$.each($this.find('.mioga2-fileupload-file-table tr'), function (i,e) {
    		if ($(e).data('file_uploader_file').name === file.name) {
    			file_exists = true;
    		}
    	});
    	if (file_exists === false) {
			var item_size = convertSize(file.size);
			var size = "";
			if (show_size === true) {
				size = '<td class="mioga2-fileupload-file-size">' + item_size + '</td>';
			}
			var $item = $(
				'<tr>' + 
					'<td class="mioga2-fileupload-file-name">' + file.name + '</td>' + 
					size + 
					'<td class="mioga2-fileupload-file-progress">progress</td>' + 
					'<td><span class="mioga2-fileupload-delete"></span></td>' + 
				'</tr>'
			).data('file_uploader_file', file).appendTo($this.find('.mioga2-fileupload-file-table'));
			if ($this.find('tr:last').hasClass('even')) {
				$item.addClass('odd');
			}
			else {
				$item.addClass('even');
			}
    	}
    	else {
    		alert('A file with a same name already exists');
    	}
	}
	function select_up ($this,current_path) {
		var files = new Array();
		$.each($this.find('.mioga2-fileupload-file-table tr'), function (i,e) {
			if ($(e).data('file_uploader_file')) {
				files.push($(e).data('file_uploader_file'));
				function fileReadFinished(evt) {
				    var request = new XMLHttpRequest();
				    var url = current_path + file.name;
				    request.open('PUT', url, false);
				    request.sendAsBinary(evt.target.result);
				}
				 
			    for (var i = 0; i < files.length; i++) {
			        var file = $(e).data('file_uploader_file');
			        var fileReader = new FileReader();
			        fileReader.onload = fileReadFinished;
			        fileReader.context = this;
			        fileReader.filename = file.name;
			        fileReader.readAsBinaryString(file);
			    }
			}
//				var gg;
//				console.log('FILE : ');
//				console.log(file);
//				  function loadFile() {
//				        var input, fr;
//
//				        if (typeof window.FileReader !== 'function') {
////				            bodyAppend("p", "The file API isn't supported on this browser yet.");
//				            return;
//				        }
//
//				            
//				            fr = new FileReader();
//				            console.log('filereader : ');
//				            console.log(fr);
//				            fr.onload = receivedBinary;
//				            fr.readAsBinaryString(file);
//
////				        function receivedText() {
////				            showResult(fr, "Text");
////
////				            fr = new FileReader();
////				            fr.onload = receivedBinary;
////				            fr.readAsDataURL(file);
////				        }
//
//				        function receivedBinary() {
//				            showResult(fr, "Binary");
//				        }
//				    }
//
//				    function showResult(fr, label) {
//				        var markup, result, n, aByte, byteStr;
//
//				        markup = [];
//				        result = fr.result;
//				        gg = fr.result;
//				        for (n = 0; n < result.length; ++n) {
//				            aByte = result.charCodeAt(n);
//				            byteStr = aByte.toString(16);
//				            if (byteStr.length < 2) {
//				                byteStr = "0" + byteStr;
//				            }
//				            markup.push(byteStr);
//				        }
////				        for (n = 0; n < result.length; ++n) {
////				            aByte = result.charCodeAt(n);
////				            byteStr = aByte.toString(16);
////				            if (byteStr.length < 2) {
////				                byteStr = "0" + byteStr;
////				            }
////				        	var nn = "";
////				        	for ( var ii= 0 ; ii < 3; ii++) {
////				        		if (result[n+ii]) {
////				        			nn += result[n+ii];
////				        		}
////				        	}
////				        	i = i+2;
////				            markup.push(nn);
////				        }
//				        bodyAppend(result, markup.join(" "));
////				        bodyAppend("pre", markup.join(" "));
//				    }

//				    function bodyAppend(result, mark) {
//				        console.log('resultttttttttttttttttttttttttttttt');
//				        console.log(result);
//				        console.log('markupppppppppppppppppppppppppppppppppppppppppppppppppp');
//				        console.log(mark);
//				        jQuery.Dav(current_path + file.name).createFile({
//					        complete:  function(dat, stat) {
//					          console.log('put completed !!');
//					        },
//					        async: false // want to make sure we do this prior to next test
//					      });
//					    console.log('gg : ');
//					    console.log(gg);
//					    console.log(mark);
//					    $.each(markup, function (ind,ee) {
//					    	jQuery.Dav(current_path + file.name).put({
//						        complete:  function(dat, stat) {
//						          console.log('put completed !!');
//						        },
//						        data: mark,
//						        async: false // want to make sure we do this prior to next test
//						     });
//					    });
					    
					    
					    
//					    for (var i=0; i < gg.length; i++) {
//					    	var jj = "";
//					    	for(var j=0; j< 3; j++) {
//					    		if (gg[i+j]) {
//					    			jj += gg[i+j];
//					    		}
//					    	}
//					    	i = i+2;
//					    	jQuery.Dav(current_path + file.name).put({
//						        complete:  function(dat, stat) {
//						          console.log('put completed !!');
//						        },
//						        data: jj,
//						        async: false // want to make sure we do this prior to next test
//						     });
//					    	
//					    }
//					    	jQuery.Dav(current_path + file.name).put({
//						        complete:  function(dat, stat) {
//						          console.log('put completed !!');
//						        },
//						        data: jj,
//						        async: false // want to make sure we do this prior to next test
//						      });
//					    }
//					    jQuery.Dav(current_path + file.name).put({
//					        complete:  function(dat, stat) {
//					          console.log('put completed !!');
//					        },
//					        data: gg,
//					        async: false // want to make sure we do this prior to next test
//					      });
//				    }
//				    loadFile();
				
				
				
				
//				function readBlob() {
//					var file = $(e).data('file_uploader_file');
//					var start = 0;
//				    var stop = file.size - 1;
//				    var reader = new FileReader();
//				    var data = "";
//				    reader.onloadend = function(evt) {
//				        if (evt.target.readyState == FileReader.DONE) { // DONE == 2
//				        	 console.log('data : ');
//				        	console.log(evt.target.result);
//				        	data = evt.target.result;
//				        }
//				      };
//				      
//				      var blob = file.slice(start, stop + 1);
//				      reader.readAsBinaryString(blob);
//				    
//				    jQuery.Dav(current_path + file.name).createFile({
//				        complete:  function(dat, stat) {
//				          console.log('put completed !!');
//				        },
//				        async: false // want to make sure we do this prior to next test
//				      });
//				    
//				    jQuery.Dav(current_path + file.name).put({
//				        complete:  function(dat, stat) {
//				          console.log('put completed !!');
//				        },
//				        data: data,
//				        async: false // want to make sure we do this prior to next test
//				      });
//			}
			    
			    
			    
			    
//			    readBlob();
			    
//			}
		});
		
//		function readBlob(opt_startByte, opt_stopByte) {
//		    
//
//		    var file = files[0];
//		    var start = parseInt(opt_startByte) || 0;
//		    var stop = parseInt(opt_stopByte) || file.size - 1;
//
//		    var reader = new FileReader();
//
//		    // If we use onloadend, we need to check the readyState.
//		    reader.onloadend = function(evt) {
//		      if (evt.target.readyState == FileReader.DONE) { // DONE == 2
//		        document.getElementById('byte_content').textContent = evt.target.result;
//		        document.getElementById('byte_range').textContent = 
//		            ['Read bytes: ', start + 1, ' - ', stop + 1,
//		             ' of ', file.size, ' byte file'].join('');
//		      }
//		    };
//
//		    var blob = file.slice(start, stop + 1);
//		    reader.readAsBinaryString(blob);
//		  }
//		  
//		  document.querySelector('.readBytesButtons').addEventListener('click', function(evt) {
//		    if (evt.target.tagName.toLowerCase() == 'button') {
//		      var startByte = evt.target.getAttribute('data-startbyte');
//		      var endByte = evt.target.getAttribute('data-endbyte');
//		      readBlob(startByte, endByte);
//		    }
//		  }, false);
		
		
		
		
	}
	// ========================================================
	// Methods declaration
	// ========================================================
	var methods = {
	    init : function(args) {
	    	var base_obj = $.extend({}, defaults, args);
	    	return $(this).each(function() {
		        
		       var $this = $(this);
		        if (typeof base_obj.data === 'string') {
		        	base_obj.list = $.get(base_obj.data);
		        }
		        // array
		        else if (Object.prototype.toString.apply(base_obj.data) === '[object Array]') {
		        	base_obj.list = base_obj.data;
		        }
		        // function
		        else if (base_obj.data instanceof Function) {
		        	base_obj.list = base_obj.data();
		        }
		        else {
		        	base_obj.list	= new Array();
		        }
		        $this.data("fileupload_obj", base_obj);
		        
			    // Draw list container
		        $this.children().remove();
		        $this.append(
		        	'<div class="mioga2-fileupload-cont">' + 
			        	'<div class="mioga2-fileupload-actions-cont">' + 
			        		'<span class="button mioga2-fileupload-add-file-btn-cont">' + 
			        			'<span>' + base_obj.i18n.add_file + '</span>' + 
			        			'<input type="file" id="mioga2-fileupload-add-file-btn-id" class="mioga2-fileupload-add-file-btn"/>' +
			        		'</span>' + 
			        		'<input type="button" class="button mioga2-fileupload-upload-btn" value="Upload"/>' +
			        	'</div>' + 
			        	'<div id="mioga2-fileupload-drop-zone-cont">' + base_obj.i18n.drop_here + '</div>' + 
				    	'<div class="mioga2-fileupload-file-list-cont">' + 
				    		'<table class="mioga2-fileupload-file-table"></table>' +  
				    	'</div>' + 
				    '</div>'
			    );
		        var list_cont_h = parseInt($this.height()) - parseInt($this.find('.mioga2-fileupload-actions-cont').outerHeight(true)) - parseInt($this.find('#mioga2-fileupload-drop-zone-cont').outerHeight(true));
		        $this.find('.mioga2-fileupload-file-list-cont').height(list_cont_h + "px");
		        // add file on search in FS
		        $this.find('.mioga2-fileupload-add-file-btn').change(function (ev) {
		        	var fileInput = document.getElementById ("mioga2-fileupload-add-file-btn-id");
		        	var file = fileInput.files[0];
		        	addFile($this, base_obj.show_size, file);
		        });
		        
		        // drag and drop test HTML5 support
		        if (typeof(FileReader) === 'function') {
		        	function handleDragOver(e) {
		        		if (e.preventDefault) {
		        			e.preventDefault();
		        		}
		        		e.dataTransfer.dropEffect = 'move';
		        		return false;
		        	}
		        	function handleDragEnter(e) {
		        		this.classList.add('mioga2-fileupload-over');
		        	}
		        	function handleDragLeave(e) {
		        		this.classList.remove('mioga2-fileupload-over');
		        	}
		        	function handleDrop(e) {
		        		e.stopPropagation();
		        		e.preventDefault();
		        		var files = e.dataTransfer.files;
		        		for (var i = 0, f; f = files[i]; i++) {
		        			var file = files[i];
		        			addFile($this, base_obj.show_size, file);
		        			this.classList.remove('mioga2-fileupload-over');
		        		}
		        		return false;
		        	}
					document.getElementById('mioga2-fileupload-drop-zone-cont').addEventListener('dragenter', handleDragEnter, false);
					document.getElementById('mioga2-fileupload-drop-zone-cont').addEventListener('dragover', handleDragOver, false);
					document.getElementById('mioga2-fileupload-drop-zone-cont').addEventListener('dragleave', handleDragLeave, false);
					document.getElementById('mioga2-fileupload-drop-zone-cont').addEventListener('drop', handleDrop, false);
		        } else {
	        	   	// no drag and drop support available
		        	$this.find('#mioga2-fileupload-drop-zone-cont').hide();
		        	// TODO resize elements
	        	}
		        // behavior to delete file from list
		        $this.find('.mioga2-fileupload-file-table').delegate('.mioga2-fileupload-delete', 'click', function (ev) {
		        	var $target = $(this).parent().parent(); 
		        	$target.hide('slow', function(){ $target.remove(); });
		        });
		        
		        
		        // Behavior to upload selection
		        $this.find('.mioga2-fileupload-upload-btn').click(function (ev) {
		        	if (base_obj.select_up instanceof Function) {
		        		base_obj.select_up($this, $this.data("fileupload_obj").current_path);
		        	}
		        	else {
		        		select_up($this, $this.data("fileupload_obj").current_path);
		        	}
		        });
	    	});
	    },
//	    current_path : function (current_path) {
//	    	console.log('GGGGGGGGGGGGGGGGGGGGG');
//	    	return $(this).each(function() {
//	    		var $this = $(this);
//	    		$this.data("fileupload_obj").current_path = current_path;
//	    		console.log($this.data("fileupload_obj"));
//	    		console.log('11111');
//	    	});
//	    },
	    // Retrieves the list and the current selection state of its components
	    get_list	: function () {
	    	var $this = $(this);
	    	var current_list = new Array();
	    	return current_list;
	    }
	};

	$.fn.fileuploader = function(method) {
	    var args = arguments;
	    if ( methods[method] ) {
	    	return methods[ method ].apply( this, Array.prototype.slice.call( args, 1 ));
	    }
	    else if ( typeof method === 'object' || ! method ) {
	    	return methods.init.apply( this, args );
	    }
	    else {
	        $.error( 'Method ' +  method + ' does not exist on jQuery.itemSelect' );
	    }
	};
})( jQuery );
