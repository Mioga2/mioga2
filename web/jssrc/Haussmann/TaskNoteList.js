// =============================================================================
/**
@description Create a TaskNoteList object.

    var options = {
        haussmann : { .... }
    };

@class TaskNoteList
@constructor
**/
// =============================================================================
function TaskNoteList (options) {
	"use strict";
	MIOGA.debug.TaskNoteList = 0;
	MIOGA.logDebug('TaskNoteList', 1, ' TaskNoteList loaded');

	var that = this;
	this.project_id = options.project_id;
	this.task_id = options.task_id;

	this.project_owner_id = options.project_owner_id;
	this.project_owner_firstname = options.project_owner_firstname;
	this.project_owner_lastname = options.project_owner_lastname;
	this.project_owner_email = options.project_owner_email;
	
	this.user_is_proj_owner = options.user_is_proj_owner;
	this.user_is_task_delegated = options.user_is_task_delegated;

	this.task_note_list = [];
	this.task_note_list_by_rowid = {};

	// publics method declaration
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;
	this.isEmpty = isEmpty;
	this.checkTaskNote = checkTaskNote;
	this.createTaskNote = createTaskNote;
	this.deleteTaskNote = deleteTaskNote;
	this.addTaskNote = addTaskNote;
	this.getTaskNoteList = getTaskNoteList;

	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================================================
	// PRIVATES METHODS
	// ===================================================================================================================
	
	// ===================================================================================================================
	// PUBLICS METHODS
	// ===================================================================================================================
	/**
	@description Public method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('TaskNoteList', 1, 'TaskNoteList::addRefreshCB - loaded');
		this.refreshCB.push( { cb : cb, context : context });
	}
	/**
	@description Public method that loads each callback.
	@method callRefreshCB
	**/
	function callRefreshCB() {
		MIOGA.logDebug('TaskNoteList', 1, 'TaskNoteList::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that checks if arguments length is empty. If this is it, that returns true, else returns false.
	@method isEmpty
	@param Object elem can be all type. This is the element that will be tested.
	@param String error_str that is a string value that represents the error message if tested element is empty.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isEmpty (elem, error_str, global_result) {
		MIOGA.logDebug('TaskNoteList', 1, 'TaskNoteList::isEmpty - loaded');
		var result = false;
		if (elem.length === 0) {
			if (global_result) {
				global_result.status = "error";
				global_result.error.push(error_str);
			}
			result = true;
		}
		return result;
	}

	/**
	@description Public method that checks task note validity
	@method checkTaskNote
	@param Object args that is result note as pair key / value : text
	**/
	function checkTaskNote (args) {
		MIOGA.logDebug('TaskNoteList', 1, 'TaskNoteList::checkTaskNote - loaded');
		var result = {
			status : "success",
			error : []
		};
		isEmpty(args.text, "textDisallowEmpty", result);
		return result;
	}
	/**
	@description Public method that creates task note
	@method createTaskNote
	@param Object args contains pairs key / value : text
	@param Function cb that called on AJAX request success
	**/
	function createTaskNote(args, cb) {
		MIOGA.logDebug('TaskNoteList', 1, 'TaskNoteList::createTaskNote - loaded');
		var params = {
			project_id : that.project_id,
			task_id : that.task_id,
			text : args.text
		};
		$.ajax({
			url : "CreateTaskNote.json",
			dataType :'json',
			data : params,
			type : "POST",
			async : false,
			success : function(response){
				if (response.status === "OK") {
					var new_task_note = new TaskNote({
						created : response.data.created,
						modified : response.data.modified,
						project_id : that.project_id,
						task_id : that.task_id,
						task_note_id : response.data.task_note_id,
						text : response.data.text,
						owner_id : response.data.owner_id,
						firstname : response.data.firstname,
						lastname : response.data.lastname,
						email : response.data.email,
						project_owner_id : that.project_owner_id,
						project_owner_firstname : that.project_owner_firstname,
						project_owner_lastname : that.project_owner_lastname,
						project_owner_email : that.project_owner_email,
						user_is_proj_owner : that.user_is_proj_owner,
						user_is_task_delegated : that.user_is_task_delegated
					});
					if (new_task_note !== undefined) {
						that.addTaskNote(new_task_note, cb);
					}
					else {
						MIOGA.logError ("TaskNoteList. createdTaskNote method can not created a TaskNote Object from Ajax request response. Return on main page...", true);
					}
				}
				else {
					MIOGA.logError ("TaskNoteList. Status of createdTaskNote AJAX request is 'KO'. The API last good status will be reloaded", false);
 					that.callRefreshCB();
				}
			},
			error: function(err, ioArgs) {
				$('body').html(err.responseText);
				MIOGA.logError ("TaskNoteList. createdTaskNote AJAX request returned an error : ", true);
			}
		});
	}
	/**
	@description Public method that removes task note
	@method deleteTaskNote
	@param Object args contains pairs key / value : task_note_id
	@param Function cb that called on AJAX request success
	**/
	function deleteTaskNote (args, cb) {
		MIOGA.logDebug('TaskNoteList', 1, 'TaskNoteList::deleteTaskNote - loaded');
		var params = {
			task_note_id : args.task_note_id,
			task_id : that.task_id,
			project_id : that.project_id
		};
		$.ajax({
			url : "DeleteTaskNote.json",
			dataType : 'json',
			data : params,
			type : "POST",
			success : function(response){ 
				if (response.status === "OK") {
					for (var i = 0; i < that.task_note_list.length; i++) {
						if (that.task_note_list[i].task_note_id === params.task_note_id) {
							that.task_note_list.splice(i,1);
							delete that.task_note_list_by_rowid[params.task_note_id];
							break;
						}
					}
					if (cb !== undefined) {
						cb(that);
					}
					else {
						that.callRefreshCB();
					}
				}
				else {
					MIOGA.logError ("TaskNoteList. Status of deleteTaskNote AJAX request is 'KO'. The API last good status will be reloaded", false);
 					that.callRefreshCB();
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("TaskNoteList. deleteTaskNote AJAX request returned an error : " + err, true);
			}
		});
	}
	/**
	@description Public method that adds task note in task note list
	@method addTaskNote
	@param Object new_task_note that is taskNote object
	@param Function cb that callback called on adding task note success
	**/
	function addTaskNote(new_task_note, cb) {
		MIOGA.logDebug('TaskNoteList', 1, 'TaskNoteList::addTaskNote - loaded');
		that.task_note_list.push(new_task_note);
		that.task_note_list_by_rowid[new_task_note.task_note_id] = new_task_note;
		if (cb !== undefined) {
			cb(new_task_note);
		}
	}
	/**
	@description Public method that retrieves task note list
	@method getTaskNoteList
	@param Bool async that determines if AJAX request is synchronous or asynchronous
	**/
	function getTaskNoteList (async) {
		MIOGA.logDebug('TaskNoteList', 1, 'TaskNoteList::getTaskNoteList - loaded');
		var params = {
			project_id : that.project_id,
			task_id : that.task_id
		};
		$.ajax({
			url : "GetTaskNoteList.json",
			async : async,
			dataType : 'json',
			type : "GET",
			data : params,
			success : function(response){
				if (response.status === "OK") {
					$.each(response.data, function (i, task_note) {
						var new_task_note = new TaskNote({
							project_id : that.project_id,
							task_id : that.task_id,
							task_note_id : task_note.task_note_id,
							text : task_note.text,
							created : task_note.created,
							modified : task_note.modified,
							published : task_note.published,
							status : task_note.status,
							owner_id : task_note.owner_id,
							firstname : task_note.firstname,
							lastname : task_note.lastname,
							email : task_note.email,
							project_owner_id : that.project_owner_id,
							project_owner_firstname : that.project_owner_firstname,
							project_owner_lastname : that.project_owner_lastname,
							project_owner_email : that.project_owner_email,
							created_str : task_note.created,
							user_is_proj_owner : that.user_is_proj_owner,
							user_is_task_delegated : that.user_is_task_delegated
						});
						if (new_task_note !== undefined) {
							that.addTaskNote(new_task_note);
						}
						else {
							MIOGA.logError ("TaskNoteList. getTaskNoteList method can not created a TaskNote Object from Ajax request response. Return on main page...", true);
						}
					});
					// sorting by created
					that.task_note_list.sort(function (a,b){
						if (a.created_str > b.created_str) {
							return 1;
						}
						else {
							return -1;
						}
					});
				}
				else {
					MIOGA.logError ("TaskNoteList. getTaskNoteList AJAX request returned KO status", false);
				}
			},
			error : function (err) {
				MIOGA.logError ("TaskNoteList. getTaskNoteList AJAX request returned an error : " + err, false);
			}
		});
	}
	// ===================================================================================================================
	// INIT
	// ===================================================================================================================
	this.getTaskNoteList(false);

	if (
		this.project_id === undefined || 
		this.task_id === undefined || 
		this.project_owner_id === undefined || 
		this.project_owner_firstname === undefined || 
		this.project_owner_lastname === undefined || 
		this.project_owner_email === undefined
		) {
		MIOGA.logError ("TaskNoteList. Object can not be initialized.", false);
		return undefined;
	}
} // End TaskNoteList object
