// =============================================================================
/**
@description Create a ModelList object.

    var options = {
        haussmann : { .... }
    };

@class ModelList
@constructor
**/
// =============================================================================
function ModelList (options) {
	"use strict";
	MIOGA.debug.ModelList = 0;
	MIOGA.logDebug('ModelList', 1, ' ModelList loaded');

	var that = this;
	this.haussmann = options.haussmann;
	this.model_list = [];
	this.model_list_by_rowid = {};
	this.refreshCB = [];
	// publics method declaration
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;
	this.checkModel = checkModel;
	this.createModel = createModel;
	this.getModelList = getModelList;

	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================================================
	// PRIVATES METHODS
	// ===================================================================================================================
	/**
	@description Private method that add model in model list
	@method addModel
	@param Object new_model that is model object
	@param Function cb that callback called on adding model success
	**/
	function addModel (new_model, cb) {
		MIOGA.logDebug('ModelList', 1, 'ModelList::addModel - loaded');
		that.model_list_by_rowid[new_model.model_id] = new_model;
		that.model_list.push(new_model);
		if (cb !== undefined) {
			cb(new_model);
		}
	}
	// ===================================================================================================================
	// PUBLICS METHODS
	// ===================================================================================================================
	/**
	@description Public method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('ModelList', 1, 'ModelList::addRefreshCB - loaded');
		this.refreshCB.push( { cb : cb, context : context });
	}
	/**
	@description Public method that loads each callback.
	@method callRefreshCB
	**/
	function callRefreshCB() {
		MIOGA.logDebug('ModelList', 1, 'ModelList::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that checks model validity and return boolean result
	@method checkModel
	@param Object args contains pairs key / value : model attributes
	**/
	function checkModel (args) {
		MIOGA.logDebug('ModelList', 1, 'ModelList::checkModel - loaded');
		var result = {
			status : "success",
			error : []
		};
		if (args.label.length === 0) {
			result.status = "error";
			result.error.push("labelDisallowEmpty");
		}
		return result;
	}
	/**
	@description Public method that creates model
	@method createModel
	@param Bool async that determines if AJAX request is synchronous or asynchronous
	@param Object args contains pairs key / value : model_id ( optional ) and label
	@param Function onSuccess that called on AJAX request success
	**/
	function createModel (async,args, onSuccess) {
		MIOGA.logDebug('ModelList', 1, 'ModelList::createModel - loaded');
		var params = {
			label : args.label,
			advanced_params : JSON.stringify(args.advanced_params),
			project_id : args.project_id
		};
		$.ajax({
			url : "SetModel.json",
			data : params,
			dataType :'json',
			type : "POST",
			async : async,

			success : function(response){ // {model_id: int, project_id: int }
				if (response.status === "OK") {
					var model_attr = {};
					model_attr.model_id = parseInt(response.data.model_id,10);
					model_attr.modelIsUnique = that.modelIsUnique;
					model_attr.advanced_params = args.advanced_params;
					model_attr.label = params.label;
					model_attr.project_id = params.project_id;
					model_attr.haussmann = that.haussmann;
					var new_model = new Model(model_attr);
					if (new_model !== undefined) {
						if (onSuccess) {
							addModel(new_model,onSuccess);
						}
						else {
							addModel(new_model,that.callRefreshCB);
						}
					}
					else {
						MIOGA.logError ("ModelList::createModel method can not created a Model Object from Ajax request response. Return on main page...", true);
					}
				}
				else {
					MIOGA.logError ("ModelList::createModel - error -  " + response.errors[0], true);
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("ModelList. createModel AJAX request returned an error : " + err, true);
			}
		});
	}
	/**
	@description Public method that retrieves model list according to user group
	@method getModelList
	@param Bool async that determines if AJAX request is synchronous or asynchronous
	**/
	function getModelList (async) {
		MIOGA.logDebug('ModelList', 1, 'ModelList::getModelList - loaded');
		$.ajax({
			url : "GetModelList.json",
			dataType : 'json',
			type : "GET",
			async : async,
			success : function(response){
				if (response.status === "OK") {
					$.each(response.data, function (i,model) {
						model.haussmann = that.haussmann;
						var new_model = new Model(model);
						if (new_model !== undefined) {
							addModel(new_model);
						}
	 					else {
	 						MIOGA.logError ("ModelList::getModelList - method can not created a Model Object from Ajax request response. Return on main page...", true);
	 					}
					});
				}
				else {
					MIOGA.logError ("ModelList::getModelList - AJAX request returned KO status", false);
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("ModelList::getModelList - AJAX request returned an error : " + err, true);
			}
		});
	}
	// ===================================================================================================================
	// INIT
	// ===================================================================================================================
	this.getModelList(false);
} // End ModelList object 
