// =============================================================================
/**
@description Create a Result object.

    var options = {
        haussmann : { .... }
    };

@class Result
@constructor
**/
// =============================================================================
function Result (options) {
	"use strict";
	MIOGA.debug.Result = 0;
	MIOGA.logDebug('Result', 1, ' Result loaded');

	var that = this;
	this.result_list = options.result_list;
	this.result_id = options.result_id;
	this.project_id = options.project_id;
	this.label = options.label;
	
	if (options.pend !== undefined) {
		if (options.pend !== null) {
			that.pend = new Date();
			that.pend.parseMiogaDate(options.pend, options.pend);
		}
		else {
			that.pend = null;
		}
	}
	else {
		that.pend = null;
	}

	if (options.checked !== undefined) {
		if (options.checked === 0) {
			that.checked = false;
		}
		else {
			that.checked = true;
		}
	}
	else {
		// error
	}
	this.budget = options.budget;

	this.created = new Date();
	this.created.parseMiogaDate(options.created,options.created);
	this.modified = new Date();
	this.modified.parseMiogaDate(options.modified,options.modified);
	if (options.tags !== undefined) {
		if (options.tags === null) {
			this.tags = [];
		}
		else {
			this.tags = options.tags;
		}
	}
	else {
		this.tags = [];
	}
	if (options.status !== undefined) {
		if (options.status === null) {
			this.status = [];
		}
		else {
			this.status = options.status;
		}
	}
	else {
		this.status = [];
	}

	this.current_status = options.current_status;

	this.project_owner_id = options.project_owner_id;
	this.project_owner_firstname = options.project_owner_firstname;
	this.project_owner_lastname = options.project_owner_lastname;
	this.project_owner_email = options.project_owner_email;

	// publics method declaration
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;
	this.isEmpty = isEmpty;
	this.isUnique = isUnique;
	this.checkGlobalInfo = checkGlobalInfo;
	this.checkStatusItem = checkStatusItem;
	this.setGlobalInfo = setGlobalInfo;
	this.setStatus = setStatus;
	this.addStatusItem = addStatusItem;
	this.deleteLastStatusItem = deleteLastStatusItem;

	this.resultNoteList = new ResultNoteList({
		project_id : that.project_id,
		result_id : that.result_id,
		project_owner_id : that.project_owner_id,
		project_owner_firstname : that.project_owner_firstname,
		project_owner_lastname : that.project_owner_lastname,
		project_owner_email : that.project_owner_email
	});
	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// ===================================================================================================================
	// PRIVATES METHODS
	// ===================================================================================================================
	/**
	@description Private method that makes global information of result formating to AJAX request and return it
	@method getGlobalInfoAttr
	@param Object args is result object
	**/
	function getGlobalInfoAttr (args) {
		MIOGA.logDebug('Result', 1, 'Result::getGlobalInfoAttr - loaded');
		var global_info_attr = {};
		// label
		if (args.label) {
			global_info_attr.label = args.label; 
		}
		else {
			global_info_attr.label = that.label;
		}
		// pend
		if (args.pend !== undefined) {
			if (args.pend !== null) {
				global_info_attr.pend = args.pend;
			}
			else {
				global_info_attr.pend = null;
			}
		}
		else {
			if (that.pend !== null) {
				global_info_attr.pend = that.pend.getUTCFullYear() + "-" + (that.pend.getUTCMonth()+1) + "-" + that.pend.getUTCDate();
			}
			else {
				global_info_attr.pend = null;
			}
		}
		// checked
		if (args.checked !== undefined) {
			if (args.checked === true) {
				global_info_attr.checked = 1;
			}
			else {
				global_info_attr.checked = 0;
			}
		}
		else {
			if (that.checked === true) {
				global_info_attr.checked = 1;
			}
			else {
				global_info_attr.checked = 0;
			}
		}
		// budget
		if (args.budget !== undefined) {
			if (args.budget !== null) {
				if (args.budget.length > 0) {
					global_info_attr.budget = args.budget;
				}
			}
		}
		else {
			if (that.budget !== null) {
				if (that.budget.length > 0) {
					global_info_attr.budget = that.budget;
				}
			}
		}
		// tags
		if (args.tags) {
			global_info_attr.tags = JSON.stringify(args.tags);
		}
		else {
			global_info_attr.tags = JSON.stringify(that.tags);
		}
		return global_info_attr;
	}
	/**
	@description Private method that checks date format
	@method isMiogaDate
	@param String date_str 
	@param String format match possibility of date format. for example : DD MM YYYY or YYYY DD MM
	**/
	function isMiogaDate(date_str,format) {
		MIOGA.logDebug('Result', 1, 'Result::isMiogaDate - loaded');
		var formatter = function (date_str,format) {
			// TODO check separator from format
			var arr = [];
			var date_array = [];
			var rxDatePattern1 = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
			var rxDatePattern2 = /^(\d{1,2})(\/|-)(\d{4})(\/|-)(\d{1,2})$/;
			var rxDatePattern3 = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;

			if(format === 'DD MM YYYY') {
				date_array = date_str.match(rxDatePattern3);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[1],date_array[3],date_array[5]);
				}
			}
			else if(format === 'DD YYYY MM') {
				date_array = date_str.match(rxDatePattern2);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[1], date_array[5], date_array[3]);
				}
			}
			else if (format === 'MM DD YYYY') {
				date_array = date_str.match(rxDatePattern3);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[3], date_array[1],date_array[5]);
				}
			}
			else if(format === 'MM YYYY DD') {
				date_array = date_str.match(rxDatePattern2);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[3], date_array[5], date_array[1]);
				}
			}
			else if (format === 'YYYY DD MM') {
				date_array = date_str.match(rxDatePattern1);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[5], date_array[1],date_array[3]);
				}
			}
			else if (format === 'YYYY MM DD') {
				date_array = date_str.match(rxDatePattern1);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[5], date_array[3],date_array[1]);
				}
			}
			else {
				// apply the default format
				date_array = date_str.match(rxDatePattern1);
				if (date_array === null) {
					return false;
				}
				else {
					arr.push(date_array[5], date_array[3],date_array[1]);
				}
			}
			return arr;
		};
		var date_day, date_month, date_year;
		if(date_str.length === 0) {
			return false;
		}

		var date_array = formatter(date_str, format);
		if (date_array === null || date_array === false) {
			return false;
		}
		date_day = parseInt(date_array[0], 10);
		date_month = parseInt(date_array[1], 10);
		date_year = parseInt(date_array[2], 10);
		if (date_month < 1 || date_month > 12) {
			return false;
		}
		else if (date_day < 1 || date_day > 31) {
			return false;
		}
		else if ((date_month === 4 || date_month === 6 || date_month === 9 || date_month === 11) && date_day === 31) {
			return false;
		}
		else if (date_month === 2) {
			var is_leap = ( new Date(date_year, 1, 29).getMonth() == 2 ) ? true : false;
			if (date_day > 29 || (date_day === 29 && is_leap)) {
				return false;
			}
		}
		return true;
	}
	
	// ===================================================================================================================
	// PUBLICS METHODS
	// ===================================================================================================================
	/**
	@description Public method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('Result', 1, 'Result::addRefreshCB - loaded');
		this.refreshCB.push( { cb : cb, context : context });
	}
	/**
	@description Public method that loads each callback.
	@method callRefreshCB
	**/
	function callRefreshCB() {
		MIOGA.logDebug('Result', 1, 'Result::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that checks if arguments length is empty. If this is it, that returns true, else returns false.

	@method isEmpty
	@param Object elem can be all type. This is the element that will be tested.
	@param String error_str that is a string value that represents the error message if tested element is empty.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isEmpty (elem, error_str, global_result) {
		MIOGA.logDebug('Result', 1, 'Result::isEmpty - loaded');
		var result = false;

		if (elem.length === 0) {
			if (global_result) {
				global_result.status = "error";
				global_result.error.push(error_str);
			}
			result = true;
		}
		return result;
	}
	/**
	@description Public method that checks if the element exists in a list. If this is it, that returns true, else returns false.

	@method isUnique
	@param Object elem can be all type. This is the element that will be searched in list.
	@param Array list. This is the list that will be matched.
	@param String error_str that is a string value that represents the error message if tested element is not unique.
	@param Object global_result that is a JSON structure that containing a status and error list. It is optional and it is updated according to result of test.
	**/
	function isUnique (elem, list, error_str, global_result) {
		MIOGA.logDebug('Result', 1, 'Result::isUnique - loaded');
		var result = true;
		for (var i = 0; i < list.length; i++) {
			if (list[i] === elem) {
				if (global_result) {
					global_result.status = "error";
					global_result.error.push(error_str);
				}
				result = false;
				break;
			}
		}
		return result;
	}
	/**
	@description Public method that checks if result global informations
	@method checkGlobalInfo
	@param Object args that is result object
	**/
	function checkGlobalInfo (args) {
		MIOGA.logDebug('Result', 1, 'Result::checkGlobalInfo - loaded');
		var result = {
			status : "success",
			error : []
		};
		if (args.label !== undefined) {
			if (args.label !== that.label) {
				var label_list = [];
				$.each(that.result_list, function (i,e) {
					label_list.push(e.label);
				});
				isUnique(args.label, label_list, 'resultAlreadyExists', result);
				
				isEmpty(args.label, "labelDisallowEmpty", result);
			}
		}
		else {
			MIOGA.logDebug('Result', 1, 'Result::checkGlobalInfo - label is undefined');
		}
		if (args.budget !== undefined) {
			if (args.budget.length > 0) {
				if ($.isNumeric(args.budget) === false) {
					result.status = "error";
					result.error.push('budgetMustBeNum');
				}
			}
		}
		else {
			MIOGA.logDebug('Result', 1, 'Result::checkGlobalInfo - budget is undefined');
		}
		return result;
	}
	/**
	@description Public method that checks status item
	@method checkStatusItem
	@param Object args that contains optional pair key / value : status and date 
	**/
	function checkStatusItem (args) {
		MIOGA.logDebug('Result', 1, 'Result::checkStatusItem - loaded');
		var result = {
			status : "success",
			error : []
		};
		if (args.status !== undefined) {
			if ($.isNumeric(args.status) === false) {
				MIOGA.logError ("Result::checkStatusItem status is not a numeric value.", true);
			}
			else if (args.status.length === 0) {
				MIOGA.logError ("Result::checkStatusItem status disallow empty.", true);
			}
			else {
			}
		}
		else {
			MIOGA.logError ("Result::checkStatusItem status is required.", true);
		}

		if (args.date !== undefined) {
			if (args.date.length === 0) {
				result.status = "error";
				result.error.push('statusDateRequired');
			}
			else if (isMiogaDate(args.date) === false) {
				result.status = "error";
				result.error.push('progressDateInvalid');
			}
			else {
			}
		}
		else {
			MIOGA.logError ("Result::checkStatusItem date is required.", true);
		}
		return result;
	}
	/**
	@description Public method that checks result attributes and calls update method to set result
	@method setGlobalInfo
	@param Object args that is result object
	@param Function onSuccess that is callback called on AJAX request success
	@param Function onError that is callback called on AJAX request error
	**/
	function setGlobalInfo(args, onSuccess, onError) {
		MIOGA.logDebug('Result', 1, 'Result::setGlobalInfo - loaded');
		var check = that.checkGlobalInfo (args);
		if (check.status === "success") {
			var attr = getGlobalInfoAttr (args);
			that.setResult(attr, onSuccess);
		}
		else {
			if (onError !== undefined) {
				onError(check.error);
			}
			else {
				MIOGA.logDebug('Result', 1, 'Result::setGlobalInfo - onError callback is undefined');
			}
		}
	}
	/**
	@description Public method that checks status item
	@method checkStatusItem
	@param Object args that contains optional pair key / value : status and date 
	**/
	function setStatus (onSuccess) {
		MIOGA.logDebug('Result', 1, 'Result::setStatus - loaded');
		var params = {
			status : JSON.stringify(that.status)
		};
		if (that.current_status !== null) {
			params.current_status = that.current_status;
		}
		else {
			MIOGA.logDebug('Result', 1, 'Result::setGlobalInfo - current_status is null');
		}
		that.setResult(params, onSuccess);
	}
	/**
	@description Public method that add result in result list
	@method addStatusItem
	@param Object args that is result object
	@param Function cb that is callback called on add result success
	**/
	function addStatusItem(args, cb) {
		MIOGA.logDebug("Result", 1, 'addStatusItem loaded');
		var params = {
			date : args.date,
			status : args.status,
			comment : args.comment
		};
		that.status.push(params);
		that.current_status = args.status;
		if (cb !== undefined) {
			cb ();
		}
		else {
			MIOGA.logDebug('Result', 1, 'Result::addStatusItem - none callback');
		}
	}
	/**
	@description Public method that removes last result from result list
	@method addStatusItem
	@param Function onSuccess that is callback called on remove result success
	**/
	function deleteLastStatusItem (onSuccess) {
		MIOGA.logDebug('Result', 1, 'Result::deleteLastStatusItem - loaded');
		that.status.pop();
		if (that.status.length > 0) {
			that.current_status = (that.status.length-1);
		}
		else {
			that.current_status = null;
		}
		that.setStatus(onSuccess);
	}
	/**
	@description Public method that updates result
	@method setGlobalInfo
	@param Object args that is result object
	@param Function onSuccess that is callback called on AJAX request success
	**/
	this.setResult = setResult;
	function setResult(args, onSuccess) {
		MIOGA.logDebug('Result', 1, 'Result::setResult - loaded');
		var params = args;
		params.result_id = that.result_id;
		params.project_id = that.project_id;
		$.ajax({
			url : "SetResult.json",
			data : params,
			dataType :'json',
			type : "POST",

			success : function(response){
				if (response.status === "OK") {
					that.modified = new Date();
					that.modified.parseMiogaDate(response.data.modified,response.data.modified);
					that.label = response.data.label;

					if (response.data.budget !== null) {
						that.budget = parseInt(response.data.budget,10);
					}
					else {
						that.budget = null;
					}
					if (response.data.pend !== undefined) {
						if (response.data.pend !== null) {
							that.pend = new Date();
							that.pend.parseMiogaDate(response.data.pend, response.data.pend);
						}
						else {
							that.pend = null;
						}
					}
					else {
						that.pend = null;
					}
					
					if (response.data.current_status !== null) {
						that.current_status = parseInt(response.data.current_status,10);
					}
					else {
						that.current_status = null;
					}

					if (response.data.tags) {
						if (response.data.tags === null) {
							that.tags = [];
						}
						else {
							that.tags = $.parseJSON(response.data.tags);
						}
					}
					if (response.data.status === null) {
						that.status = [];
					}
					else {
						that.status = $.parseJSON(response.data.status);
					}
					that.current_status = response.data.current_status;
					if (response.data.checked === 0) {
						that.checked = false;
					}
					else {
						that.checked = true;
					}
					
					if (onSuccess !== undefined) {
						onSuccess(that);
					}
					else {
						that.callRefreshCB();
					}
				}
				else {
					MIOGA.logError ("Result. setResult AJAX request returned KO status", false);
					that.callRefreshCB();
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("Result. setResult AJAX request returned an error : " + err, true);
			}
		});
	}

	if (this.result_id === undefined || this.project_id === undefined || this.label === undefined || this.budget === undefined  || this.checked === undefined  || $.isArray(that.tags) === false ) {
		MIOGA.logError ("Result. Object can not be initialized.", false);
		return undefined;
	}
} // END Result object 
