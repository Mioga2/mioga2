// =============================================================================
/**
Create a Gantt object.

@class Gantt
@constructor
**/
// =============================================================================
function Gantt(opt) {
	
	// Attributs
	// config is formated according to EiffelMain's attibute. 
	this.config = {
		"resources" : [],
		"animRight":true,
		"first_day":1,
		"show_groups":0,
		"act_colors" : [{
			0:{
				fgcolor: "#999999",
				bgcolor: "red",
				name : "ma categorie"
			}
		}],
		"categories":[],
		"users":[],
		"display":[],
		"projects" : [],
		"week_count":1,
		"month_count":2
	};
	this.addRefreshCB = addRefreshCB;
	this.refreshCB = [];
	this.prefs = opt.prefs;
	this.indexOfUsers = {};
	this.getTasks = getTasks;
	this.i18n = opt.i18n;
	var that = this; // For callback reference to current object
	
	// End of constructor
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	
	// users_rowid and res_rowid are parameters from Eiffel application. they dont used here.
	// the preferences are used to show project and project tasks.
	// If no prefs, all projects will be showed but no project tasks. In this case, prefs are initialized according to response of AJAX request.
	
	function getTasks(start, count, users_rowid, res_rowid, cb) {
		MIOGA.logDebug("Gantt", 1, "getTasks");
		var data = {};
		// Calculate limit dates;
		var end_date = new Date(start);
		end_date.setTime(end_date.getTime() + 7 * count * 86400000 - 1000);
		data.start_year = start.getFullYear();
		data.start_month = start.getMonth() + 1;
		data.start_day = start.getDate();
		data.end_year = end_date.getFullYear();
		data.end_month = end_date.getMonth() + 1;
		data.end_day = end_date.getDate();

		if (that.prefs) {
			data.projects_id = [];
			data.project_detail_id = [];
			for (pf in that.prefs) {
				if (that.prefs[pf].display === true ) {
					data.projects_id.push(that.prefs[pf].project_id);
				}
				if (that.prefs[pf].detail === true ) {
					data.project_detail_id.push(that.prefs[pf].project_id);
				}
			}
			data.projects_id = JSON.stringify(data.projects_id);
			data.project_detail_id = JSON.stringify(data.project_detail_id);
		}
		else {
		}

		that.tl = [];
		that.config.users = [];
		that.config.display = [];
		that.config.projects = [];

		$.ajax({
			async : false,
			type : "POST",
			url : "GetProjectList.json",
			dataType : 'json',
			data : data,
			success : function (response) {
				if (response.status === "OK") {
					$.each(response.data.project_list, function (i,proj) {
						var task_list = [];
						if (data.projects_id === undefined && data.project_detail_id === undefined) {
							that.prefs[proj.project_id] = {
								project_id : proj.project_id,
								label : proj.label,
								display : true,
								detail : false
							};
						}
						else {
							// project already defined in prefs attibute
						}
						if (proj.started !== undefined && proj.started !== null) {
							if (proj.planned === null) {
								proj.planned = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
							}
							else {
							}
							// "user" is a eiffel name of variable. But here, it is a task ( project, projects task or milestone ) that is represents each line
							that.config.users.push({
								firstname : proj.label,
								lastname : "",
								ident : "ident project",
								rowid:"project_id_" + proj.project_id,
								bg_project : proj.color,
								project_owner_id : proj.project_owner_id,
								result : proj.result
							});
							
							that.config.display.push({
								type:"user",
								rowid:"project_id_" + proj.project_id
							});
							that.tl.push({
								dstart:proj.started,
								dend:proj.planned,
								rowid: "project_id_" + proj.project_id,
								user_id : "project_id_" + proj.project_id,
								result : proj.result,
								project_label:proj.label,
								label:proj.label,
								bgcolor : proj.color
							});
							if (proj.tasks !== undefined) {
								for (var j = 0; j < proj.tasks.length ; j++) {
									if (proj.tasks[j].pstart === null) {
										proj.tasks[j].dstart = proj.started;
									}
									else {
										proj.tasks[j].dstart = proj.tasks[j].pstart;
									}
									if (proj.tasks[j].pend === null) {
										proj.tasks[j].dend = proj.planned;
									}
									else {
										proj.tasks[j].dend = proj.tasks[j].pend;
									}
									proj.tasks[j].bgcolor = proj.color;
									proj.tasks[j].bg_project = proj.color;
									proj.tasks[j].firstname = proj.tasks[j].label;
									proj.tasks[j].lastname = "";
									proj.tasks[j].user_id = proj.tasks[j].task_id;
									proj.tasks[j].rowid = proj.tasks[j].task_id;
									
									task_list.push(proj.tasks[j]);
								}
							}
							//milestone
							if (proj.result !== undefined && that.prefs[proj.project_id].detail === true) {
								if (proj.result.length > 0) {
									
									for (var ii=0; ii < proj.result.length; ii++) {
										if (proj.result[ii].pend !== null) {
											task_list.push({
												firstname : proj.result[ii].label,
												lastname:"",
												dstart:proj.result[ii].pend,
												dend:proj.result[ii].pend,
												pstart:proj.result[ii].pend,
												pend:proj.result[ii].pend,
												project_label:proj.label,
												label:proj.result[ii].label,
												bgcolor : proj.color,
												bgproject : proj.color,
												rowid: "res_id_" + proj.result[ii].result_id,
												task_id : "res_id_" + proj.result[ii].result_id,
												user_id : "res_id_" + proj.result[ii].result_id
											});
										}
									}
								}
							}
						}
						else {
							MIOGA.logDebug("Gantt", 1, "invalid project");
						}
						// SORT TASKS ....
						task_list.sort(function(a,b){
							if (a.dstart > b.dstart) {
								return 1;
							}
							else if (a.dstart < b.dstart){
								return -1;
							}
							else {
								if (a.created > b.created) {
									return -1
								}
								else if (a.created < b.created) {
									return 1
								}
								else {
									return 0;
								}
							}
						});
						for (var d=0; d < task_list.length; d++) {
							that.tl.push(task_list[d]);
							that.config.users.push(task_list[d]);
							
							that.config.display.push({
								type:"user",
								rowid:task_list[d].rowid
							});
						}
						// add separation
						that.config.display.push({
							type:"sep",
							rowid:"project_id_" + proj.project_id
						});
					});
				}
				else {
					MIOGA.logError("Gantt.getProjectList Error in web service", false);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				MIOGA.logError("Gantt.getProjectList Error in web service  textStatus = " + textStatus, false);
			}
		});
		initIndexes.call(that);
		if (cb) {
			cb( [],that.tl,[]);
		}
	}
	// --------------------------------------------------------
	// Add a new refresh callback called when configuration change
	// --------------------------------------------------------
	function addRefreshCB(cb, context) {
		MIOGA.logDebug("Gantt", 1, "addRefreshCB");
		var that = this;
		this.refreshCB.push( { cb : cb, context : context });
	}
	// --------------------------------------------------------
	// Call all refresh callback
	// --------------------------------------------------------
	this.callRefreshCB = callRefreshCB;
	function callRefreshCB() {
		MIOGA.logDebug("Gantt", 1, "callRefreshCB config = ", this.config);
		var that = this;
		$.each(this.refreshCB, function(i, item) {
//			item.cb.call(item.context);
		});
	}
	// --------------------------------------------------------
	// initIndexes prepares indexes for users.
	//
	//WARNING - for this module , "user" is a line of Gantt view. Keep indexOfUsers for compatibility with EiffelMain. 
	// --------------------------------------------------------
	function initIndexes() {
		MIOGA.logDebug("Gantt", 1, 'initIndexes');
		var that = this;

		that.indexOfUsers = {};
		for (var i = 0; i <that.config.users.length; i++) {
			that.indexOfUsers[that.config.users[i].rowid] = i;
		}
		MIOGA.logDebug("Gantt", 2, 'indexOfUsers = ', that.indexOfUsers);
	}
} // End of Gantt object
