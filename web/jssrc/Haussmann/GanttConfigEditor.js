// =============================================================================
/**
This module permits to modify Gantt configuration. For to do this, It draw and initializes dialog with table contains project label, display or not, detail or not foreach project.
When refresh called, newdata passed in parameter can be used to remake table of visual configuration and show dialog.

    var options = {
        i18n : { ... }
    };

@class GanttConfigEditor
@constructor
@param {Object} options
**/
// =============================================================================
function GanttConfigEditor(elem, options) {
	"use strict";
	MIOGA.debug.GanttConfigEditor = 0;
	MIOGA.logDebug("GanttConfigEditor", 1, "GanttConfigEditor constructor begin >>>>>>>>>>>>>>>> ");
	var that = this;
	this.$elem = elem;
	this.i18n = options.i18n;
	this.prefs = options.prefs;
	this.onChange = options.onChange;
	this.onCancel = options.onCancel;
	this.refresh = refresh;
	this.$cont = $('<div class="gantt-pref-dial"></div>').appendTo(that.$elem);
	this.$gantt_pl = $(
		'<table class="gantt-pref-pl highlight">' +
			'<thead><tr class="gantt-pref-head"><th class="gantt-pref-projects"></th><th class="gantt-pref-display">' + that.i18n.display + '</th><th class="gantt-pref-detail">' + that.i18n.detail + '</th></tr></thead>' +
			'<tbody><tr><td>' + that.i18n.all_projects + '</td><td><input type="checkbox" class="config-all-display"/></td><td><input type="checkbox" class="config-all-detail"/></td></tr></tbody>' + 
		'</table>'
	).appendTo(that.$cont);
	// if click on display item, unckeck detail if no display
	that.$gantt_pl.delegate('.config-display', 'click', function () {
		if (!$(this).is(':checked')) {
			$(this).parent().parent().find('.config-detail').replaceWith('<input type="checkbox" class="config-detail"/>');
			that.$gantt_pl.find('.config-all-display').replaceWith('<input type="checkbox" class="config-all-display"/>');
		}
	});
	// if click on detail item, ckeck display if you choose to show details
	that.$gantt_pl.delegate('.config-detail', 'click', function () {
		if ($(this).is(':checked')) {
			$(this).parent().parent().find('.config-display').replaceWith('<input type="checkbox" class="config-display" checked="checked"/>')
		}
		else {
			that.$gantt_pl.find('.config-all-detail').replaceWith('<input type="checkbox" class="config-all-detail"/>');
		}
	});
	// not display all ==> hide all project ==> uncheck all 
	that.$gantt_pl.delegate('.config-all-display', 'click', function () {
		if ($(this).is(':checked') === false) {
			that.$gantt_pl.find('.config-display').each(function (i,e) {
				$(e).replaceWith('<input type="checkbox" class="config-display"/>');
			});
			that.$gantt_pl.find('.config-detail').each(function (i,e) {
				$(e).replaceWith('<input type="checkbox" class="config-detail"/>');
			});
			that.$gantt_pl.find('.config-all-detail').replaceWith('<input type="checkbox" class="config-all-detail"/>');
		}
		else {
			that.$gantt_pl.find('.config-display').each(function (i,e) {
				$(e).replaceWith('<input type="checkbox" class="config-display" checked="checked"/>');
			});
		}
	});
	// all detail => if show all details, active also each display and "display all" 
	that.$gantt_pl.delegate('.config-all-detail', 'click', function () {
		if ($(this).is(':checked')) {
			that.$gantt_pl.find('.config-display').each(function (i,e) {
				$(e).replaceWith('<input type="checkbox" class="config-display" checked="checked"/>');
			});
			that.$gantt_pl.find('.config-detail').each(function (i,e) {
				$(e).replaceWith('<input type="checkbox" class="config-detail" checked="checked"/>');
			});
			that.$gantt_pl.find('.config-all-display').attr('checked', 'checked');
		}
		else {
			that.$gantt_pl.find('.config-detail').each(function (i,e) {
				$(e).replaceWith('<input type="checkbox" class="config-detail"/>');
			});
		}
	});

	this.$detail_buttons = $('<ul class="button_list"></ul>').appendTo(that.$cont);

	$('<li><button class="button">'+that.i18n.apply+'</button></li>')
		.appendTo(this.$detail_buttons)
		.click(function(e) {
			that.onChange( parsePrefs () );
		});
	$('<li><button class="button cancel">'+this.i18n.cancel+'</button></li>')
		.appendTo(this.$detail_buttons)
		.click(function(e) {
			that.onCancel ();
		});
	// == end constructor ===========================================
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	// --------------------------------------------------------
	// showWindow display and init window
	// --------------------------------------------------------
	this.showWindow = showWindow;
	function showWindow(prefs) {
		MIOGA.logDebug("GanttConfigEditor", 1, 'GanttConfigEditor  showWindow ===');
		that.$elem.show();
		that.refresh(prefs);
	}
	// --------------------------------------------------------
	// hideWindow hides the window
	// --------------------------------------------------------
	this.hideWindow = hideWindow;
	function hideWindow() {
		MIOGA.logDebug("GanttConfigEditor", 1, 'GanttConfigEditor  hideWindow ===');
		that.$elem.hide();
	}
	// refresh table and checkbox according to new pref
	function refresh (new_prefs) {
		that.$gantt_pl.find('tbody tr').not(':first').remove();
		for(var proj_id in new_prefs) {
			var $proj = $('<tr><td>' + new_prefs[proj_id].label + '</td><td><input type="checkbox" class="config-display"/></td><td><input type="checkbox" class="config-detail"/></td></tr>').data({
				gantt_pref_project_id : new_prefs[proj_id].project_id,
				gantt_pref_label : new_prefs[proj_id].label
			});
			if (new_prefs[proj_id].display) {
				$proj.find('[type="checkbox"]:first').attr('checked',"");
			}
			else {
			}
			if (new_prefs[proj_id].detail) {
				$proj.find('[type="checkbox"]:last').attr('checked',"");
			}
			else {
			}
			that.$gantt_pl.append($proj);
		}
	}
	// ===============================================================
	// Private methods
	// ===============================================================
	function parsePrefs () {
		var newData = {};
		that.$cont.find('tbody tr').not(':first').each(function (i,e) {
			var display = false;
			var detail = false;
			newData[$(e).data('gantt_pref_project_id')] = {
				project_id : parseInt($(e).data('gantt_pref_project_id')),
				label : $(e).data('gantt_pref_label'),
				display : $(e).find('[type="checkbox"]:first').is(':checked'),
				detail : $(e).find('[type="checkbox"]:last').is(':checked')
			};
		});
		return newData;
	}
	// init with initial prefs
	refresh(that.prefs);
}