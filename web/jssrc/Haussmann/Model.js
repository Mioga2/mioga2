// =============================================================================
/**
@description Create a Model object.

    var options = {
        haussmann : { .... }
    };

@class Model
@constructor
**/
// =============================================================================
function Model (options) {
	"use strict";
	MIOGA.debug.Model = 0;
	MIOGA.logDebug('Model', 1, ' Model loaded');

	var that = this;
	this.model_id = options.model_id;
	this.project_id = options.project_id;
	this.haussmann = options.haussmann;
	this.label = options.label;
	this.advanced_params = options.advanced_params;

	// publics method declaration
	this.addRefreshCB = addRefreshCB;
	this.callRefreshCB = callRefreshCB;
	this.setModel = setModel;
	// END constructor
	// --------------------------------------------------------------------------------------------------------------------

	// =======================================================================
	// PRIVATES METHODS
	// =======================================================================
	
	// =======================================================================
	// PUBLICS METHODS
	// =======================================================================
	/**
	@description Public method that increments callback to this object, which will be launched when callRefreshCB method is called.
	@method addRefreshCB
	**/
	function addRefreshCB(cb, context) {
		MIOGA.logDebug('Model', 1, 'Model::addRefreshCB - loaded');
		this.refreshCB.push( { cb : cb, context : context });
	}
	/**
	@description Public method that loads each callback.
	@method callRefreshCB
	**/
	function callRefreshCB() {
		MIOGA.logDebug('Model', 1, 'Model::callRefreshCB - loaded');
		$.each(that.refreshCB, function(i, item) {
			item.cb.call(item.context);
		});
	}
	/**
	@description Public method that updates project
	@method setModel
	@param Boolean async that determines if AJAX request is called as asynchronous or not
	@param Object args that is contains pair key / value : label and model_id 
	@param Function cb that is callback called on set model success
	**/
	function setModel (async, args, cb) {
		MIOGA.logDebug('Model', 1, 'Model::setModel - loaded');
		var params = {
			model_id : that.model_id,
			project_id : args.project_id,
			label : args.label,
			advanced_params : JSON.stringify(args.advanced_params)
		};

		$.ajax({
			url : "SetModel.json",
			async : async,
			data : params,
			dataType:'json',
			type : "POST",
			success : function(response){
				if (response.status === "OK") {
					that.label = params.label;
					that.advanced_params = args.advanced_params; 
					if (cb !== undefined) {
						cb(that);
					}
					else {
						that.callRefreshCB();
					}
				}
				else {
					MIOGA.logError ("Model. " + response.errors[0], true);
				}
			},
			error: function(err, ioArgs) {
				MIOGA.logError ("Model::setModel - AJAX request returned an error : " + err, true);
			}
		});
	}

	if ($.isNumeric(that.model_id) === false || that.label.length === 0) {
		return undefined;
	}
} // End Model object
