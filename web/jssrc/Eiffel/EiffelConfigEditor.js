// =============================================================================
/**
This module permits to modify Eiffel configuration.

    var options = {
        i18n : { ... }
    };

@class EiffelConfigEditor
@constructor
@param {Object} elem JQuery element parent of progress bar
@param {Object} options
**/
// =============================================================================
function EiffelConfigEditor(elem, options) {

	this.elem           = elem;
	this.i18n           = options.i18n;
	this.eiffel         = options.eiffel;

	// Count for items to sequence, including separators
	this.item_count = 0;

	MIOGA.debug.EiffelConfigEditor = 0;

	MIOGA.logDebug("EiffelConfigEditor", 1, "EiffelConfigEditor constructor begin >>>>>>>>>>>>>>>> ");

	this.refresh = refresh;
	// ----------------------------------------------------------------
	// Create interface
	// ----------------------------------------------------------------
	var that = this; // For callback reference to current object
	var i18n = this.i18n;
	// main window
	this.$cont = $('<div class="eiffel-config"></div>').appendTo(this.elem);

	// navigation bar
	var $nav = $('<div class="app-nav"></div>').appendTo(this.$cont);
	$('<button class="button cancel">'+i18n.returnToMain+'</button>')
	           .appendTo($nav)
			   .click(function(event) {
			   				MIOGA.logDebug("EiffelConfigEditor", 1, 'EiffelConfigEditor main-return event');
							history.back();
	           });

	$('<button class="button">'+i18n.applyLabel+'</button>')
	           .appendTo($nav)
			   .click(function(event) {
			   				// TODO move to a validate function
			   				var error = false;
			   				MIOGA.logDebug("EiffelConfigEditor", 1, 'EiffelConfigEditor apply configuration');
							var d = that.$display.sortable('toArray', {attribute:"item-name"});
							MIOGA.logDebug("EiffelConfigEditor", 1, "d = ", d);
							var weekCount = $('#'+that.weekCount_id).val();
							MIOGA.logDebug("EiffelConfigEditor", 1, "weekCount = " + weekCount);
							if (parseInt(weekCount, 10) <= 0 || parseInt(weekCount, 10) > 5) {
								MIOGA.logDebug("EiffelConfigEditor", 1, "error");
								$('#'+that.weekCount_id).addClass('error');
								error = true;
							}
							else {
								$('#'+that.weekCount_id).removeClass('error');
							}
							var showGroups = 0;
							if ($('#'+that.evtFromGroups_id).is(':checked')) {
								showGroups = 1;
							}
							if (!error) {
								that.eiffel.configChange({ displaySeq : d
												, showGroups: showGroups
							                    , weekCount : weekCount },
												  function(err) {
														history.back();
												  });
							}
	           });

	// Parameters 
	var $block = $('<div id="eiffel-params" class="sbox"><h2 class="title_bg_color">'+i18n.paramsLabel+'</h2></div>').appendTo(this.$cont);
	this.$pform = $('<form class="form"></form>').appendTo($block);

	this.weekCount_id = MIOGA.generateID('eiffel');
	this.evtFromGroups_id = MIOGA.generateID('eiffel');

	$('<div class="form-item"><label>'+i18n.weekCountLabel+'</label><input id="'+this.weekCount_id+'" type="text" name="weekCount" /> < 6</div>').appendTo(this.$pform);
	$('<div class="form-item"><label>'+i18n.evtFromGroupsLabel+'</label><input id="'+this.evtFromGroups_id+'" type="checkbox" name="evtFromGroups" /></div>').appendTo(this.$pform);



	// Sortable list to display
	$block = $('<div class="sbox"><h2 class="title_bg_color">'+i18n.itemSeqLabel+'</h2></div>').appendTo(this.$cont);
	this.$display = $('<ul class="display-list"></ul>').appendTo($block);
	this.$display.sortable();
	$('<div class="action"><span class="add-new"></span>'+i18n.addSepLabel+'</div>')
						.appendTo($block)
						.click(function(e) {
							MIOGA.logDebug("EiffelConfigEditor", 1, "add separator");
							that.eiffel.config.display.push({ type: "sep" });
							addSeparator.call(that, that.$display, (that.eiffel.config.display.length - 1));
						});


	MIOGA.logDebug("EiffelConfigEditor", 1, "EiffelConfigEditor constructor end <<<<<<<<<<<<<<<< ");
	// == end constructor ===========================================
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	// --------------------------------------------------------
	// showWindow display and init window
	// --------------------------------------------------------
	this.showWindow = showWindow;
	function showWindow() {
		MIOGA.logDebug("EiffelConfigEditor", 1, 'EiffelConfigEditor  showWindow ===');
		this.refresh();
		this.$cont.show();
	}
	// --------------------------------------------------------
	// hideWindow hides the window
	// --------------------------------------------------------
	this.hideWindow = hideWindow;
	function hideWindow() {
		MIOGA.logDebug("EiffelConfigEditor", 1, 'EiffelConfigEditor  hideWindow ===');
		this.$cont.hide();
	}
	// -------------------------------------------------------------
	// refresh function
	// redraw list content function of calendarList
	// -------------------------------------------------------------
	function refresh() {
		MIOGA.logDebug("EiffelConfigEditor", 1, ' refresh');
		var that = this;

		// Parameters
		$('#'+that.weekCount_id).removeClass('error');
		$('#'+that.weekCount_id).val(that.eiffel.config.week_count);
		if (that.eiffel.config.show_groups) {
			$('#'+this.evtFromGroups_id).prop('checked', 1);
		}
		else {
			$('#'+this.evtFromGroups_id).prop('checked', 0);
		}

		// Sequence of users
		this.$display.children().remove();
		var name;
		var $el;
		$.each(this.eiffel.config.display, function(num, item) {
			MIOGA.logDebug("EiffelConfigEditor", 1, ' item.type = ' + item.type);
			if (item.type !== "sep") {
				$el = $('<li item-name="'+num+'" class="display-'+item.type+'">'+item.ident+'</li>').appendTo(that.$display);
			}
			else {
				addSeparator.call(that, that.$display, num);
			}
		});
	}
	// ===============================================================
	// Private methods
	// ===============================================================
	function addSeparator($list, num) {
		MIOGA.logDebug("EiffelConfigEditor", 1, ' addSeparator');
		var that = this;
		var $el = $('<li item-name="'+num+'" class="display-sep"><span class="separator"></span></li>').appendTo($list);
		$('<span class="destroy-sep">'+that.i18n.destroySeparator+'</span>')
						.click(function(evt) {
							MIOGA.logDebug("EiffelConfigEditor", 1, ' destroy num = ' + num);
							$el.remove();
						})
						.appendTo($el);
	}
} // End of EiffelConfigEditor
