// -----------------------------------------------------------------------------
// Eiffel header file to buils effective javascript file
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// Eiffel is the planning application for Mioga2 Project
//     It is construct as a jQuery plugin 
//     Documentation for methods is inserted as comment
// -----------------------------------------------------------------------------


(function ($) {


var defaults = {
			i18n : { 	
						monthNames:["January","February","March","April","May","June", "July","August","September","October","November","December"],
						returnToMain :"Return to main",
						applyLabel :"Apply",
						backwardAllLabel :"backward N week",
						backwardLabel :"backward one week",
						forwardLabel :"forward one week",
						forwardAllLabel :"forward N week",
						zoomInLabel :"Zoom in",
						zoomOutLabel :"Zoom out",
						todayLabel :"Today",
						weekCountLabel :"Week count",
						evtFromGroupsLabel :"Show events from groups",
						paramsLabel :"Parameters",
						itemSeqLabel :"Display sequence",
						addSepLabel :"Add a separator",
						destroySeparator :"Destroy",
						configurationText :"Configuration",
						showDetailsTitle:"Details for",
						fromLabel:"From",
						toLabel:"To",
						closeLabel:"Close",
						noTasksText:"No tasks for this day",
						weekLabel:"Week",
						weeksLabel:"Weeks",
						progressBarText:"Eiffel initializing, please wait ...",
						categoryLegend : "Category legend",
						reattachWindow:"Reattach the window"
					  }
	};

// ========================================================
// Effective plugin declaration with methods abstraction
// Usage :
//     <div id="eiffel"></div>
//
//     $('#eiffel').eiffel(options);
// ========================================================
$.fn.eiffel = function(options) {
	// method call, get existing Eiffel Object and apply method
	// if method not returns result we return this for chainability
	if (typeof options === 'string') {
		var args = Array.prototype.slice.call(arguments, 1);
		this.each(function() {
			var eiffel = $.data(this, 'eiffel');
			if (eiffel && $.isFunction(eiffel[options])) {
				var res = eiffel[options].apply(eiffel, args);
				if (res !== undefined) {
					return res;
				}
			}
			else {
				$.error( 'Method ' +  options + ' does not exist on jQuery.eiffel' );
			}
		});
	}
	else if (typeof options === 'object') {
		options =  $.extend(true, {}, defaults, options);

		this.each(function(i, item) {
			var $elem = $(item);
			var eiffel = new Eiffel(item, options);
			$(item).data('eiffel', eiffel);
		});
	}
	else {
		$.error( 'Eiffel must not be there ... ');
	}
	return this;
};

