// =============================================================================
/**
Create a Eiffel object.

    var options = {
        i18n : { .... }
    };

	this.appPB = new Eiffel(this.elem, options);

@class Eiffel
@constructor
@param {Object} elem JQuery element parent of progress bar
@param {Object} options
**/
// =============================================================================
function Eiffel(elem, options) {
	
	// Attributs
	this.i18n       = options.i18n;
	this.elem       = elem;
	this.config     = undefined;
	this.indexOfUsers = undefined;
	this.indexOfResources = undefined;
	/**
	List of callback to call on refresh event
	@attribute refreshCB
	@type array
	@private
	**/
	this.refreshCB  = [];


	this.addRefreshCB = addRefreshCB;
	this.getTasks = getTasks;
	// calendar preferences
	this.currentWindow;  // Used by hashchange system to hide the current window

	var that = this; // For callback reference to current object
	MIOGA.debug.Eiffel = 0;
	MIOGA.object = this;

	// --------------------------------------------------------
	// Create and activate progress bar
	// --------------------------------------------------------
	var pb_opt = { i18n : { "displayText" : this.i18n.progressBarText } };
	this.appPB = new AppProgressBar(this.elem, pb_opt);
	this.appPB.setValue(0);

	// --------------------------------------------------------
	// Read configuration on server (synchronous call) and initialize object
	// --------------------------------------------------------
	if (!options.config) {
		MIOGA.logDebug("Eiffel", 2, 'must read configuration');
		this.config = readConfiguration.apply(this);
	}
	else {
		this.config = options.config;
	}
	MIOGA.logDebug("Eiffel", 2, 'Config ......', this.config);

	this.appPB.setValue(10);

	// ------------------------------------------------------------------------------------------------
	// Configuration window
	// ------------------------------------------------------------------------------------------------
	this.configEditor = new EiffelConfigEditor(elem, {i18n           : options.i18n,
													  eiffel         : this
													});
	this.configEditor.hideWindow();
	this.appPB.setValue(30);
	// ------------------------------------------------------------------------------------------------
	// Create main window
	// ------------------------------------------------------------------------------------------------
	initIndexes.call(that);
	this.eiffelMain = new EiffelMain(elem, {
											app          : this,
											i18n            : options.i18n,
											indexOfResources : that.indexOfResources,
											indexOfUsers : that.indexOfUsers
	                                   });

	this.eiffelMain.hideWindow();

	this.appPB.setValue(100);
	this.appPB.$pbcont.hide();

	// ------------------------------------------------------------------------------------------------
	// Prepare callback for navigation on hashchange
	// ------------------------------------------------------------------------------------------------
	$(window).hashchange( function(){
		MIOGA.logDebug("Eiffel", 1,  'new hashchange ' + location.hash );
		var hash = location.hash;
		var newWindow;
		if (hash === '#config') {
			MIOGA.logDebug("Eiffel", 1, 'Show config window');
			newWindow = that.configEditor;
		}
		else {
			MIOGA.logDebug("Eiffel", 1, 'Show main window');
			newWindow = that.eiffelMain;
		}
		if ( (that.currentWindow === undefined) || (that.currentWindow !== newWindow)) {
			MIOGA.logDebug("Eiffel", 1, 'currentView not defined or change of view');
			if (that.currentWindow !== undefined) {
				that.currentWindow.hideWindow();
			}
			newWindow.showWindow();
			that.currentWindow = newWindow;
		}
	});
	$(window).hashchange();
	// End of constructor
	// =================================================================================================
	// Public methods for Object
	// =================================================================================================
	function getTasks(start, count, users_rowid, res_rowid, cb) {
		MIOGA.logDebug("Eiffel", 1, "getTasks");
		var data = {};
		// Calculate limit dates;
		var end_date = new Date(start);
		end_date.setTime(end_date.getTime() + 7 * count * 86400000 - 1000);
		MIOGA.logDebug("Eiffel", 2, "end_date = " + end_date);
		data.start_year = start.getFullYear();
		data.start_month = start.getMonth() + 1;
		data.start_day = start.getDate();
		data.end_year = end_date.getFullYear();
		data.end_month = end_date.getMonth() + 1;
		data.end_day = end_date.getDate();

		data.show_groups = this.config.show_groups;

		data.users_rowid = JSON.stringify(users_rowid);
		data.res_rowid = JSON.stringify(res_rowid);
		MIOGA.logDebug("Eiffel", 2, "data = ", data);

		$.ajax({
				 url: mioga_context.private_bin_uri + "/Eiffel/GetTasks.json",
				 dataType:"json",
				 data:data,
				 type: "POST",
				 success : function(response) {
				 			if (response.status === "OK") {
				 				initIndexes.call(that);
				 				cb(response.tasks, response.project_tasks, response.calendars);
							}
							else {
								MIOGA.logError("Eiffel.getTasks Error in web service", false);
							}
					  },
					  error: function(jqXHR, textStatus, errorThrown) {
						MIOGA.logError("Eiffel.getTasks Error in web service  textStatus = " + textStatus, false);
					  }
		});
	}
	// ========================================================================================
	// Private functions must not be added to this
	// ========================================================================================
	// --------------------------------------------------------
	// initIndexes prepares indexes for users et resources from config
	// --------------------------------------------------------
	function initIndexes() {
		MIOGA.logDebug("Eiffel", 1, 'initIndexes');
		var that = this;

		that.indexOfUsers = {};
		for (var i = 0; i <that.config.users.length; i++) {
			that.indexOfUsers[that.config.users[i].rowid] = i;
		}
		MIOGA.logDebug("Eiffel", 2, 'indexOfUsers = ', that.indexOfUsers);

		that.indexOfResources = {};
		for (var i = 0; i <that.config.resources.length; i++) {
			that.indexOfResources[that.config.resources[i].rowid] = i;
		}
		MIOGA.logDebug("Eiffel", 2, 'indexOfResources = ', that.indexOfResources);
	}
	// --------------------------------------------------------
	// Initialize configuration
	// --------------------------------------------------------
	function initializeConfig(data) {
		MIOGA.logDebug("Eiffel", 1, "initializeConfig");
		var config = {};

		config.first_day = data.first_day || 1;
		config.week_count = data.week_count || 3;
		config.show_groups = data.show_groups || 0;

		if (parseInt(data.anim_right, 10) === 0) {
			config.animRight = false;
		} else {
			config.animRight = true;
		}

		config.users = data.users || [];
		config.resources = data.resources || [];

		// categories colors
		config.act_colors = {};
		MIOGA.logDebug("Eiffel", 2, "categories length = " + data.categories.length);
		for (var i = 0; i < data.categories.length; i++) {
			MIOGA.logDebug("Eiffel", 2, "categories i = " + i, data.categories[i]);
			MIOGA.logDebug("Eiffel", 2, "categories name = " + data.categories[i].name);
			config.act_colors[data.categories[i].category_id] = { fgcolor : data.categories[i].fgcolor,
																	bgcolor : data.categories[i].bgcolor,
																	name : data.categories[i][name] };
		}

		// Prepare display struct by adding new users and resources
		//
		var disp = JSON.parse(data.display);
		if (! disp) {
			disp = [];
		}
		MIOGA.logDebug("Eiffel", 2, "display = ", disp);
		var userIdents = [];
		var userIdx = {};
		var resIdents = [];
		var resIdx = {};
		// Prepare a list of resources and user to put in display if they doesn't exists
		for (var i = 0; i < config.users.length; i++) {
			userIdents.push({ rowid : config.users[i].rowid, ident : config.users[i].lastname + ' ' + config.users[i].firstname });
			userIdx[config.users[i].rowid] = i;
		}
		MIOGA.logDebug("Eiffel", 2, "userIdents = ", userIdents);
		MIOGA.logDebug("Eiffel", 2, "userIdx = ", userIdx);
		for (var i = 0; i < config.resources.length; i++) {
			resIdents.push({ rowid : config.resources[i].rowid, ident : config.resources[i].ident });
			resIdx[config.resources[i].rowid] = i;
		}
		MIOGA.logDebug("Eiffel", 2, "resIdents = ", resIdents);
		MIOGA.logDebug("Eiffel", 2, "resIdx = ", resIdx);
		
		config.display = [];
		for (var i = 0; i < disp.length; i++) {
			MIOGA.logDebug("Eiffel", 2, "disp i " + i + " type = " + disp[i].type + " rowid = " + disp[i].rowid);
			if (disp[i].type === 'user') {
				MIOGA.logDebug("Eiffel", 1, "userIdx : ", userIdx[disp[i].rowid]);
				if (userIdx.hasOwnProperty(disp[i].rowid)) {
					disp[i].ident = userIdents[userIdx[disp[i].rowid]].ident;
					config.display.push(disp[i]);
					userIdents[userIdx[disp[i].rowid]].done = true;
				}
			}
			else if (disp[i].type === 'res') {
				MIOGA.logDebug("Eiffel", 2, "resIdx : ", resIdx[disp[i].rowid]);
				if (resIdx.hasOwnProperty(disp[i].rowid)) {
					disp[i].ident = resIdents[resIdx[disp[i].rowid]].ident;
					config.display.push(disp[i]);
					resIdents[resIdx[disp[i].rowid]].done = true;
				}
			}
			else if (disp[i].type === 'sep') {
				config.display.push(disp[i]);
			}
			else {
				MIOGA.logDebug("Eiffel", 2, "unknown type = " + disp[i].type);
			}
		}
		// Process not affected users and resources
		for (var i = 0; i < userIdents.length; i++) {
			if (!userIdents[i].hasOwnProperty('done') || !userIdents[i].done) {
				MIOGA.logDebug("Eiffel", 1, "got a new user : ", userIdents[i]);
				config.display.push({ type : "user", rowid : userIdents[i].rowid, ident : userIdents[i].ident });
			}
		}
		for (var i = 0; i < resIdents.length; i++) {
			if (!resIdents[i].hasOwnProperty('done') || !resIdents[i].done) {
				MIOGA.logDebug("Eiffel", 1, "got a new resource : ", resIdents[i]);
				config.display.push({ type : "res", rowid : resIdents[i].rowid, ident : resIdents[i].ident });
			}
		}

		MIOGA.logDebug("Eiffel", 2, "config.display = ", config.display);

		return config;
	}
	// --------------------------------------------------------
	// readConfiguration from server
	// --------------------------------------------------------
	function readConfiguration() {
		MIOGA.logDebug("Eiffel", 1, "readConfiguration");
		var config;
		//TODO Check config for display = somme(users + resources) must be done in Perl ?
		$.ajax({
				 url: mioga_context.private_bin_uri + "/Eiffel/GetConfiguration.json",
				 async:false,
				 dataType:"json",
				 success : function(data) {
				 			if (data.status === "OK") {
								MIOGA.logDebug("Eiffel", 1, "data.config", data.config);
								config = initializeConfig.call(that, data.config);
							}
							else {
								MIOGA.logError("Eiffel.readConfiguration Error in web service", false);
							}
					  },
					  error: function(jqXHR, textStatus, errorThrown) {
						MIOGA.logError("Eiffel.readConfiguration Error in web service  textStatus = " + textStatus, false);
					  }
				});
		return config;
	}
	// --------------------------------------------------------
	// configChange
	// --------------------------------------------------------
	this.configChange = configChange;
	function configChange(params, cb) {
		MIOGA.logDebug("Eiffel", 1, "configChange params = ", params);
		var that = this;
		var newDisplay = new Array();
		var type;
		for (var i = 0; i < params.displaySeq.length; i++) {
			type = that.config.display[params.displaySeq[i]].type;
			if (type === 'sep') {
				newDisplay.push({ type : type });
			}
			else {
				newDisplay.push({ type : type, rowid : that.config.display[params.displaySeq[i]].rowid });
			}
		}
		MIOGA.logDebug("Eiffel", 2, "newDisplay = ", newDisplay);

		var data = {};
		data.week_count = params.weekCount;
		data.show_groups = params.showGroups;
		data.display = JSON.stringify(newDisplay);

		$.ajax({
				 url: mioga_context.private_bin_uri + "/Eiffel/SetPreferences.json",
				 dataType:"json",
				 data: data,
				 type: "POST",
				 success : function(data) {
							MIOGA.logDebug("Eiffel", 1, "preferences saved");
				 			if (data.status === "OK") {
								MIOGA.logDebug("Eiffel", 1, "data.config", data.config);
								that.config = initializeConfig.call(that, data.config);
								cb(null);
								that.callRefreshCB();
							}
							else {
								MIOGA.logError("Eiffel.readConfiguration Error in web service", false);
							}
					  },
					  error: function(jqXHR, textStatus, errorThrown) {
					  		MIOGA.logError("Eiffel.configChange Error in web service", false);
					  }
		});
	}
	// --------------------------------------------------------
	// Add a new refresh callback called when configuration change
	// --------------------------------------------------------
	function addRefreshCB(cb, context) {
		MIOGA.logDebug("Eiffel", 1, "addRefreshCB");
		var that = this;
		this.refreshCB.push( { cb : cb, context : context });
	}
	// --------------------------------------------------------
	// Call all refresh callback
	// --------------------------------------------------------
	this.callRefreshCB = callRefreshCB;
	function callRefreshCB() {
		MIOGA.logDebug("Eiffel", 1, "callRefreshCB config = ", this.config);
		var that = this;
		$.each(this.refreshCB, function(i, item) {
				item.cb.call(item.context);
			});
	}

} // End of Eiffel object
