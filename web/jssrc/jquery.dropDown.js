/**
@name jquery.dropDown
@type jQuery
@author developers@mioga2.org

@requires jQuery v1.7.1

Copyright 2014, (http://alixen.fr)
The Mioga2 Project. All Rights Reserved.
This Plugin is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

@description 	This Plugin draws button from parameters with click event to open and close HTML pan as Jquery object passed in parameters

@param Object
	settings An object literal containing key/value pairs to provide optional settings.
		@option Object button is Jquery object that represents button.
		@option Object content is Jquery object that represents pan that will be toggled
		@option Function openEvent that is first click event on button
			@param Object $content_cont is content container as Jquery object
		@option Function closeEvent that is second click event on button
			@param Object $content_cont is content container as Jquery object

@example 

	var options = {
		button : $('<div class="title-cont">'
					+ '<div class="title">Toggle content</div>'
					+ '<div class="icon-down"></div>'
				+ '</div>')
		content : $('<div class="list">
					+ '<div class="item">'
						+ '<span class="item-label">item_1</span>'
					+ '</div>'
					+ '<div class="item">'
						+ '<span class="item-label">item_2</span>'
					+ '</div>'
					+ '<div class="item">'
						+ '<span class="item-label">item_3</span>'
					+ '</div>'
				+ '</div>')
		openEvent : function ($content_cont) {
			$content_cont.show('slow');
		},
		closeEvent : function ($content_cont) {
			$content_cont.hide('slow');
		},
		openDirection : "right"
	};

	$('#id-cont').dropDown(options);
	
	====================================================================
	public methods : 
	
	$('#id-cont').dropDown('options',{
		content : $('<div class="new-content">'<div class="item">This is a new content</div></div>')
	});
	$('#id-cont').dropDown('open');
	$('#id-cont').dropDown('close');
	$('#id-cont').dropDown('get','button');
	$('#id-cont').dropDown('get','content');
	$('#id-cont').dropDown('get','openEvent');
	====================================================================
**/
(function( $ ){
	var defaults = {
		button : $('<div class="mioga2-dropDown-button"></div>'),
		content : $('<div class="mioga2-dropDown-list closed" style="display:none;"></div>'),
		openEvent : function ($content_cont) {
			$content_cont.slideDown(300, function () {
				$(this).show();
			});
		},
		closeEvent : function ($content_cont) {
			$content_cont.slideUp(300, function () {
				$(this).hide();
			});
		},
		openDirection : "right"
	};
	// ==================================================
	// ========= privates methods =======================
	// ==================================================
	/**
	@description This function replaces button
	@method changeButton
	@param Jquery selector of Plugin.
	@param Jquery object is new button
	**/
	function changeButton (elem, btn) {
		elem.$button_cont.children().remove();
		elem.$button_cont.append(btn);
	}
	/**
	@description This function replaces content
	@method changeContent
	@param Jquery selector of Plugin
	@param Jquery object is new content
	**/
	function changeContent (elem, ct) {
		elem.$content_cont.children().remove();
		elem.$content_cont.append(ct);
	}
	/**
	@description This function defined a direction to opening container
	@method changeOpenDirection
	@param Jquery selector of Plugin
	**/
	function changeOpenDirection (elem) {
		elem.$content_cont.removeClass('open-right').removeClass('open-left').addClass("open-" + elem.base_obj.openDirection);
	}
	/**
	@description This function update jquery data of plugin options and affect open and close event to button
	@method affectToggleEvent
	@param Jquery selector of Plugin
	**/
	function affectToggleEvent (elem) {
		// Update Jquery data of plugin container 
		$(elem).data('dropDown_options').openEvent = elem.base_obj.openEvent;
		$(elem).data('dropDown_options').closeEvent = elem.base_obj.closeEvent;
		// toggle click behavior affectation
		elem.$button_cont.unbind('click').on('click',  function (e) {
			if ($(elem).hasClass('closed')) {
				open(elem);
			}
			else {
				close(elem);
			}
			e.preventDefault();
			e.stopPropagation();
		});
	}
	/**
	@description This function calls openEvent of plugin
	@method open
	@param Jquery selector of Plugin
	**/
	function open (elem) {
		elem.base_obj.openEvent(elem.$content_cont);
		$(elem).removeClass('closed');
	}
	/**
	@description This function calls closeEvent of plugin
	@method close
	@param Jquery selector of Plugin
	**/
	function close (elem) {
		elem.base_obj.closeEvent(elem.$content_cont);
		$(elem).addClass('closed');
	}
	/**
	@description This function return plugin option that passed as string in parameter
	@method get
	@param Jquery selector of Plugin
	@param String args match plugin options
	**/
	function get (elem, args) {
		return elem[args];
	}
	/**
	@description Public method that is called on plugin initialization.
				 It draws HTML containers and buttons, and apply behavior to each element.
				 It draws data and initializes "tableScrollableSortable" plugin.
	@method init
	@param Object containing plugin options extends with default values of plugin.
	**/
	var methods = {
		// ==================================================
		// ========== publics methods =======================
		// ==================================================
		/**
		@description Public method that initialize plugin. It makes plugin container, button container and content container. It appends Jquery elements passed in plugin options and affect behavior to it.
		@method init
		@param Object args containing plugin options
		**/
		init : function(args) {
			if (args.openDirection !== undefined && args.openDirection !== "right" && args.openDirection !== "left") {
				$.error('Jquery.dropDown::init - openDirection parameter must be "right" or "left"');
			}
			else {
				return $(this).each(function() {
					// Declaration
					var that = this;
					this.elem = this;
					this.elem.$cont = $(this).addClass ("mioga2-dropDown closed");
					this.$elem = this.elem.$cont;
					this.elem.base_obj = $.extend({}, defaults, args);
					this.elem.$button_cont  = $('<div class="button-cont"></div>');
					this.elem.$content_cont = $('<div class="content-cont" style="display:none;"></div>');
					this.elem.$cont.data("dropDown_options", that.elem.base_obj);
					// Draw HTML content
					this.elem.$button_cont.append(that.elem.base_obj.button);
					this.elem.$content_cont.append(that.elem.base_obj.content);
					this.$elem.append(that.$button_cont);
					this.$elem.append(that.$content_cont);
					// Behavior affectation
					affectToggleEvent(that.elem);
					changeOpenDirection(that.elem);
				});
			}
		},
		/**
		@description Public method that updates plugin options
		@method options
		@param Object containing options
		**/
		options	: function (args) {
			return $(this).each(function() {
				if (args && $.type(args) === "object") {
					if (args.button) {
						if ($.type(args.button) === "object") {
							this.elem.base_obj.button = args.button;
							changeButton (this.elem, args.button);
						}
						else {
							$.error('Jquery.dropDown::options - button must be a Jquery object');
						}
					}
					if (args.content) {
						if ($.type(args.content) === "object") {
							this.elem.base_obj.content = args.content;
							changeContent (this.elem, args.content);
						}
						else {
							$.error('Jquery.dropDown::options - content must be a Jquery object');
						}
					}
					if (args.openEvent && $.isFunction(args.openEvent) ) {
						if ($.isFunction(args.openEvent)) {
							this.elem.base_obj.openEvent = args.openEvent;
							affectToggleEvent(this.elem, args.openEvent, this.elem.base_obj.closeEvent);
						}
						else {
							$.error('Jquery.dropDown::options - openEvent must be a function');
						}
						
					}
					if (args.closeEvent) {
						if ( $.isFunction(args.closeEvent)) {
							this.elem.base_obj.closeEvent = args.closeEvent;
							affectToggleEvent(this.elem, this.elem.base_obj.openEvent, args.closeEvent);
						}
						else {
							$.error('Jquery.dropDown::options - closeEvent must be a function');
						}
					}
					if (args.openDirection) {
						if ($.type( args.openDirection ) === "string") {
							if (args.openDirection === "left" || args.openDirection === "right") {
								this.elem.base_obj.openDirection = args.openDirection;
								changeOpenDirection (this.elem, args.openDirection);
							}
							else {
								$.error('Jquery.dropDown::options - openDirection must be a string matches left or right');
							}
						}
						else {
							$.error('Jquery.dropDown::options - openDirection must be a string matches left or right');
						}
					}
				}
				else {
					$.error('Jquery.dropDown::options - args must be an object containing key / value pair');
				}
			});
		},
		/**
		@description Public method that triggering open method
		@method open
		**/
		open : function () {
			return $(this).each(function() {
				open (this.elem);
			});
		},
		/**
		@description Public method that triggering close method
		@method close
		**/
		close : function () {
			return $(this).each(function() {
				close (this.elem);
			});
		},
		/**
		@description Public method that is getter of plugin attributes.
		@method get
		@param Object args containing string matches in plugin options
		**/
		get : function (args) {
			var elm = $(this).data('dropDown_options');
			if (args && $.type( args ) === "string" && elm[args] !== undefined) {
				var res = get (elm, args);
				if ($.isFunction(res) || $.type( res ) === "string") {
					return res;
				}
				else  {
					return res.clone();
				}
			}
			else {
				$.error('Jquery.dropDown::get - args is not a string that represents a know argument');
			}
		}
};
	/**
	@namespace $.fn.dropDown
	**/
	$.fn.dropDown = function(method) {
		var args = arguments;
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( args, 1 ));
		}
		else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, args );
		}
		else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.dropDown' );
		}
	};
})( jQuery );
