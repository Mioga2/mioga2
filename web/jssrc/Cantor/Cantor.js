	// ========================================================================================================
	// Main Cantor object
	// ========================================================================================================
	function Cantor (elem, options) {
		var defaults = {
			page: undefined
		};


		// ========================================================
		// Public attributes
		// ========================================================
		this.applicationName = 'Cantor';


		// ========================================================
		// Public methods
		// ========================================================


		// ------------------------------------------------------------------------------------------------
		// Browser navigation hash generator
		// ------------------------------------------------------------------------------------------------
		this.generateNavigationHash = generateNavigationHash;
		function generateNavigationHash (type, rowid) {
			if (rowid) {
				return ('#' + this.applicationName +  '-' + type + '-' + rowid);
			}
			else {
				return ('#' + this.applicationName +  '-' + type);
			}
		};


		// ========================================================
		// Private methods
		// ========================================================


		// ------------------------------------------------------------------------------------------------
		// getActiveSurveys, get list of active surveys
		// ------------------------------------------------------------------------------------------------
		function getActiveSurveys () {
			MIOGA.logDebug ('Cantor', 1, '[getActiveSurveys] Entering');

			var surveys = [ ];

			$.ajax ({
				url: 'GetActiveSurveys.json',
				type: 'GET',
				async: false,
				traditional: true,
				dataType: 'json',
				success: function (data) {
					MIOGA.logDebug ('Cantor', 3, '[getActiveSurveys] GetActiveSurveys.json returned, data: ', data);
					for (i=0; i<data.surveys.length; i++) {
						var survey = new CantorSurvey (data.surveys[i]);
						surveys.push (survey);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					MIOGA.logError ('Internal error', true);
				}
			});

			MIOGA.logDebug ('Cantor', 1, '[getActiveSurveys] Leaving, surveys: ', surveys);
			return (surveys);
		}


		// ------------------------------------------------------------------------------------------------
		// getSolicitations, get list of active surveys
		// ------------------------------------------------------------------------------------------------
		function getSolicitations () {
			MIOGA.logDebug ('Cantor', 1, '[getSolicitations] Entering');

			var surveys = [ ];

			$.ajax ({
				url: 'GetSolicitations.json',
				type: 'GET',
				async: false,
				traditional: true,
				dataType: 'json',
				success: function (data) {
					MIOGA.logDebug ('Cantor', 3, '[getSolicitations] GetSolicitations.json returned, data: ', data);
					for (i=0; i<data.surveys.length; i++) {
						var survey = new CantorSurvey (data.surveys[i]);
						surveys.push (survey);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					MIOGA.logError ('Internal error', true);
				}
			});

			MIOGA.logDebug ('Cantor', 1, '[getSolicitations] Leaving, surveys: ', surveys);
			return (surveys);
		}

		// --------------------------------------------------------
		// show, show an object according to its type (user, team, etc.) and rowid
		// --------------------------------------------------------
		function show (type, rowid) {
			MIOGA.logDebug ('Cantor', 1, "[show] Entering for " + type + " " + rowid);

			// Get editor for object
			var page = undefined;
			var object = undefined;
			switch (type) {
				case undefined:
					page = this.mainPage;
					object = {
						surveys: getActiveSurveys.call (that),
						solicitations: getSolicitations.call (that)
					};
					break;
				case 'survey':
					page = this.SurveyEdit;
					if (rowid !== undefined) {
						object = new CantorSurvey ({rowid: rowid});
						checkAttendeeId.call (this, object);
					} else {
						object = new CantorSurvey ();
					}
					break;
			}

			// Check editor exists before trying to display it
			if ((page !== undefined) && (object !== undefined)) {
				page.refresh (object);
				if (this.currentPage !== undefined) {
					this.currentPage.hide ();
				}
				page.show ();
				this.currentPage = page;
			}
			else {
				alert (i18n.no_ui + type);
			}

			MIOGA.logDebug ('Cantor', 1, "[show] Leaving");
		};


		// ------------------------------------------------------------------------------------------------
		// Browser navigation (back and next buttons) handler
		// ------------------------------------------------------------------------------------------------
		function navigationClick () {
			MIOGA.logDebug ('Cantor', 1, '[navigationClick] Entering');

			// load_page will be set to 'false' if page is part of Fouquet and needs to be handled in JavaScript
			var load_page = true;

			// Extract parts of hash being in the form #Fouquet-<type>-<rowid>, type being 'user', 'team', etc.
			var hash = window.location.hash;
			var parts = hash.split ('-');

			if ((parts[0] === '#' + that.applicationName) || ((parts[0] === '') && (window.location === that.appLocation))) {
				// Target URL belongs to application, handle it here
				MIOGA.logDebug ('Cantor', 1, '[navigationClick] Target URL ' + hash + ' belongs to application');
				if (parts[2] !== undefined) {
					show.call (that, parts[1], parseInt (parts[2], 10));
				} else {
					show.call (that, parts[1]);
				}
				load_page = false;
			}
			else {
				MIOGA.logDebug ('Cantor', 1, '[navigationClick] Target URL does not belong to application');
			}

			MIOGA.logDebug ('Cantor', 1, '[navigationClick] Leaving');
			return (load_page);
		};


		// ------------------------------------------------------------------------------------------------
		// Check attendee_id is set into CantorSurvey object, otherwise prompt for user identification
		// ------------------------------------------------------------------------------------------------
		function checkAttendeeId (survey) {
			if ((survey.attr.attendee_id === undefined) && (this.attendee_id === undefined)) {
				var $contents = $('<div class="form"></div>').append ($('<p></p>').append (i18n.identify_yourself_email_prompt));
				var $mail_container = $('<div class="form-item"></div>').appendTo ($contents);
				$('<label for="email"></label>').append (i18n.email_address).appendTo ($mail_container);
				var $email = $('<input type="text" name="email"/>').appendTo ($mail_container).focus ();
				var $fullname_container = $('<div class="form"></div>').append ($('<p></p>').append (i18n.identify_yourself_fullname_prompt)).appendTo ($contents).hide ();
				$('<label for="fullname"></label>').append (i18n.fullname).appendTo ($fullname_container);
				var $fullname = $('<input type="text" name="fullname"/>').appendTo ($fullname_container);
				var $dialog = MIOGA.dialog ({
					title: i18n.identify_yourself,
					content: $contents,
					buttons: [
						{
							i18n: i18n.btn_ok,
							cb: function (ev, $dialog) {
								var email = $email.val ();
								if (email !== '') {
									if (!$fullname_container.is (':visible')) {
										// User only input email, check if email is known
										if (survey.getAttendeeId (email)){
											// Email is known, user can answer
											that.attendee_id = survey.attr.attendee_id;

											// Refresh survey and close dialog
											that.currentPage.refresh (survey);
											$dialog.dialog ('destroy');
										}
										else {
											// Email is not known, user needs to fill fullname to continue
											$fullname_container.slideDown ();
											$fullname.focus ();
										}
									}
									else {
										var fullname = $fullname.val ();
										if (fullname !== '') {
											survey.addAttendee ({
												fullname: fullname,
												email: email
											});

											// Refresh survey and close dialog
											that.currentPage.refresh (survey);
											$dialog.dialog ('destroy');
										}
									}
								}
							}
						}
					]
				});

				// Keyboard submit
				$contents.find("input").on ('keypress', function (evt) {
					if (evt.keyCode == 13) {
						$dialog.find ('button').trigger ('click');
					}
				});
			}
			else {
				survey.setAttendeeId (this.attendee_id);
			}
		};


		// ========================================================
		// Constructor
		// ========================================================
		var that = this;

		// Attributs
		this.options    = $.extend (true, {}, defaults, options);
		this.elem       = elem;
		this.i18n       = options.i18n;
		var i18n        = this.i18n;

		this.data = options.data;
		this.currentPage = undefined;
		this.appLocation = window.location;

		// Create and activate progress bar
		var pb_opt = { i18n : { "displayText" : this.i18n.progressBarText } };
		this.appPB = new AppProgressBar(this.elem, pb_opt);
		this.appPB.setValue(0);

		// ------------------------------------------------------------------------------------------------
		// Main page
		// ------------------------------------------------------------------------------------------------
		this.mainPage = new CantorMainPage (that.elem, {
			cantor: this,
			i18n: options.i18n
		});
		this.mainPage.hide ();

		this.appPB.setValue (33);

		// ------------------------------------------------------------------------------------------------
		// Survey editor
		// ------------------------------------------------------------------------------------------------
		this.SurveyEdit = new CantorSurveyEdit (that.elem, {
			cantor: this,
			i18n: options.i18n
		});
		this.SurveyEdit.hide ();

		this.appPB.setValue (66);

		// ------------------------------------------------------------------------------------------------
		// Trap browser's navigation buttons
		// ------------------------------------------------------------------------------------------------
		if (window.addEventListener) {
			window.addEventListener ("hashchange", navigationClick, false);
		}
		else if (window.attachEvent) {
			window.attachEvent ("onhashchange", navigationClick);
		}
		else {
			alert (i18n.internal_error + "Can't attach event listener to hashchange.");
		}
		navigationClick.call (this);

		this.appPB.setValue (100);
		this.appPB.$pbcont.fadeOut ('slow');
	};
