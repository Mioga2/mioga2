// ========================================================
// CantorSurvey OO-interface to surveys
// ========================================================
function CantorSurvey (options) {
	// ========================================================
	// Default values
	// ========================================================
	var defaults = {
		ident: '',
		title: '',
		description: '',
		duration: '',
		url: '',
		open_attendee_list: true,
		dates: [],
		attendees: [],
		resources: []
	};

	// ========================================================
	// Public methods
	// ========================================================

	// --------------------------------------------------------
	// fetch, fetch survey from Mioga2
	// --------------------------------------------------------
	this.fetch = fetch;
	function fetch () {
		MIOGA.logDebug ('CantorSurvey', 1, '[fetch] Entering: rowid=' + this.attr.rowid);
		var that = this;

		$.ajax ({
			url: 'GetSurvey.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			data: { rowid: this.attr.rowid },
			success: function (data) {
				MIOGA.logDebug ('CantorSurvey', 2, "[fetch] GetSurvey.json returned, data: ", data);
				if (data.success) {
					that.attr = data.survey;
				}
			}
		});

		// Set methods according to connected user and survey owner
		setMethods.call (this);

		MIOGA.logDebug ('CantorSurvey', 1, '[fetch] Leaving. Data: ', this.attr);
	};

	// --------------------------------------------------------
	// store, store survey to Mioga2
	// --------------------------------------------------------
	function store () {
		MIOGA.logDebug ('CantorSurvey', 1, '[store] Entering');

		// Basic attributes
		var survey_attr = {
			title: this.attr.title,
			description: this.attr.description,
			duration: this.attr.duration,
			open_attendee_list: this.attr.open_attendee_list
		};

		// Set survey rowid, if defined
		if (this.attr.rowid !== undefined) {
			survey_attr.rowid = this.attr.rowid;
		}

		// Set short URL, if defined
		if (this.attr.short_url !== undefined) {
			survey_attr.short_url = this.attr.short_url;
		}

		// Push attendees, if any
		if ((this.attr.attendees !== undefined) && (this.attr.attendees.length)) {
			survey_attr.attendees = [ ];
			for (i=0; i<this.attr.attendees.length; i++) {
				var attendee = this.attr.attendees[i];
				if ((attendee.user_id !== undefined) && (attendee.user_id !== null)) {
					// Attendee is a Mioga user, only send user_id
					survey_attr.attendees.push (attendee.user_id);
				}
				else {
					// Attendee is not a Mioga user, send fullname and email
					survey_attr.attendees.push (attendee.fullname, attendee.email);
				}
			}
		}

		// Push survey resources, if any
		if ((this.attr.resources !== undefined) && (this.attr.resources.length)) {
			survey_attr.resources = [ ];
			for (i=0; i<this.attr.resources.length; i++) {
				var resource = this.attr.resources[i];
				survey_attr.resources.push (resource.rowid);
			}
		}

		// Push survey dates, if any
		if ((this.attr.dates !== undefined) && (this.attr.dates.length)) {
			survey_attr.dates = [ ];
			for (i=0; i<this.attr.dates.length; i++) {
				var date = this.attr.dates[i];
				survey_attr.dates.push (date);
			}
		}

		MIOGA.logDebug ('CantorSurvey', 1, '[store] Data to be posted: ', survey_attr);

		// Post data to Mioga2
		var success = false;
		var message = "[store] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		var survey = {rowid: this.attr.rowid};	// For further loading from WebService response
		$.ajax ({
			url: 'SetSurvey.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: survey_attr,
			success: function (data) {
				MIOGA.logDebug ('CantorSurvey', 2, "[store] SetSurvey.json POST returned, data: ", data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.survey) {
					survey = data.survey;
				}
			}
		});

		MIOGA.logDebug ('CantorSurvey', 1, '[store] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, survey: survey});
	};

	// --------------------------------------------------------
	// answer, answer survey to Mioga2
	// source_attendees is optional, it is used to clone answers from another survey
	// --------------------------------------------------------
	this.answer = answer;
	function answer (source_attendees) {
		MIOGA.logDebug ('CantorSurvey', 1, '[answer] Entering');

		// Merge answers from source, if any
		if ((source_attendees !== undefined) && (source_attendees.length)) {
			for (i=0; i<source_attendees.length; i++) {
				var src = source_attendees[i];
				var dest;
				if (src.user_id !== undefined) {
					dest = $.grep (this.attr.attendees, function (obj) { return (obj.user_id === src.user_id); })[0];
				}
				else {
					dest = $.grep (this.attr.attendees, function (obj) { return (obj.email === src.email); })[0];
				}

				dest.dates = src.dates;
			}
		}

		// Get answering attendee
		var attendee = $.grep (this.attr.attendees, function (obj) { return (obj.rowid === that.attr.attendee_id); })[0];
		MIOGA.logDebug ('CantorSurvey', 1, '[answer] Answering attendee: ', attendee);

		// Get dates for attendee
		var dates = [ ];
		for (var date in attendee.dates) {
			if (attendee.dates[date] === true) {
				dates.push (date);
			}
		}

		// Basic attributes
		var survey_attr = {
			rowid: this.attr.rowid,
			attendee_id: attendee.rowid,
			dates: dates
		};

		// Add attendee attributes in case he is not already registered
		if (attendee.rowid === 0) {
			survey_attr.fullname = attendee.fullname;
			survey_attr.email = attendee.email;
		}

		MIOGA.logDebug ('CantorSurvey', 1, '[answer] Data to be posted: ', survey_attr);

		// Post data to Mioga2
		var success = false;
		var message = "[answer] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		var survey = {rowid: this.attr.rowid};	// For further loading from WebService response
		$.ajax ({
			url: 'AnswerSurvey.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: survey_attr,
			success: function (data) {
				MIOGA.logDebug ('CantorSurvey', 2, "[answer] AnswerSurvey.json POST returned, data: ", data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.survey) {
					survey = data.survey;
				}
			}
		});

		MIOGA.logDebug ('CantorSurvey', 1, '[answer] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, survey: survey});
	};

	// --------------------------------------------------------
	// destroy, destroy survey from Mioga2
	// --------------------------------------------------------
	this.destroy = destroy;
	function destroy () {
		MIOGA.logDebug ('CantorSurvey', 1, '[destroy] Entering: rowid=' + this.attr.rowid);
		var that = this;

		var success = false;
		var message = "[destroy] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		$.ajax ({
			url: 'DeleteSurvey.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: { rowid: this.attr.rowid },
			success: function (data) {
				MIOGA.logDebug ('CantorSurvey', 2, "[destroy] DeleteSurvey.json returned, data: ", data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.success) {
					that.attr = { };
				}
			}
		});

		MIOGA.logDebug ('CantorSurvey', 1, '[destroy] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors});
	};

	// --------------------------------------------------------
	// getAttendeeId, given an email address, fetch attendee_id from Mioga2
	// --------------------------------------------------------
	this.getAttendeeId = getAttendeeId;
	function getAttendeeId (email) {
		MIOGA.logDebug ('CantorSurvey', 1, '[getAttendeeId] Entering');

		var args = {
			rowid: that.attr.rowid,
			email: email
		};

		// Fetch data from Mioga2
		var success = false;
		$.ajax ({
			url: 'GetAttendeeId.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			data: args,
			success: function (data) {
				MIOGA.logDebug ('CantorSurvey', 2, "[getAttendeeId] AnswerSurvey.json POST returned, data: ", data);
				success = data.success;
				if (data.attendee_id) {
					that.attr.attendee_id = data.attendee_id;
				}
			}
		});

		MIOGA.logDebug ('CantorSurvey', 1, '[getAttendeeId] Leaving, success: ' + success);
		return (success);
	};

	// --------------------------------------------------------
	// setAttendeeId, set attendee_id from externally-given value, unless it is already set internally
	// --------------------------------------------------------
	this.setAttendeeId = setAttendeeId;
	function setAttendeeId (rowid) {
		if (this.attr.attendee_id === undefined) {
			this.attr.attendee_id = rowid;
		}
	};

	// --------------------------------------------------------
	// addAttendee, add a new attendee to survey
	// This checks if user owns the survey or if the list is open
	// --------------------------------------------------------
	this.addAttendee = addAttendee;
	function addAttendee (attendee) {
		var ret = false;
		MIOGA.logDebug ('CantorSurvey', 1, '[addAttendee] Entering, attendee: ', attendee);

		// Create empty attendee list, if required
		if (this.attr.attendees === undefined) {
			this.attr.attendees = [ ];
		}

		// Check attendee does not already exist in list
		var exists;
		if (attendee.rowid !== undefined) {
			// Attendee is a Mioga2 user
			exists = ($.grep (this.attr.attendees, function (att) { return (att.user_id === attendee.rowid); }).length !== 0);
		}
		else {
			// Attendee is not a Mioga2 user
			exists = ($.grep (this.attr.attendees, function (att) { return (att.email === attendee.email); }).length !== 0);
		}

		if (!exists) {
			if ((attendee.rowid === undefined) && (this.attr.is_mine || this.attr.open_attendee_list)) {
				this.attr.attendees.push ({
					rowid: 0,
					fullname: attendee.fullname,
					email: attendee.email
				});
				this.setAttendeeId (0);
			}
			else if ((attendee.rowid !== undefined)) {
				this.attr.attendees.push ({
					user_id: attendee.rowid,
					firstname: attendee.firstname,
					lastname: attendee.lastname,
					email: attendee.email,
					is_me: attendee.is_me
				});
			}
			ret = true;
		}

		MIOGA.logDebug ('CantorSurvey', 1, '[addAttendee] Leaving, status: ' + ret);
		return (ret);
	};

	// --------------------------------------------------------
	// addResource, add a new resource to survey
	// --------------------------------------------------------
	this.addResource = addResource;
	function addResource (resource) {
		var ret = false;
		MIOGA.logDebug ('CantorSurvey', 1, '[addResource] Entering, resource: ', resource);

		// Create empty resources list if required
		if (this.attr.resources === undefined) {
			this.attr.resources = [ ];
		}

		// Check resource does not already exist in list
		var exists = ($.grep (this.attr.resources, function (res) { return (res.rowid === resource.rowid); }).length !== 0);

		if (!exists) {
			this.attr.resources.push ({
				rowid: resource.rowid,
				ident: resource.ident
			});
			ret = true;
		}

		MIOGA.logDebug ('CantorSurvey', 1, '[addResource] Leaving, status: ' + ret);
		return (ret);
	};

	// --------------------------------------------------------
	// addDate, add a new date to survey
	// --------------------------------------------------------
	this.addDate = addDate;
	function addDate (date) {
		var ret = false;
		MIOGA.logDebug ('CantorSurvey', 1, '[addDate] Entering, date: ' + date);

		// Create empty dates list if required
		if (this.attr.dates === undefined) {
			this.attr.dates = [ ];
		}

		// Check date does not already exist in list
		var exists = ($.grep (this.attr.dates, function (dt) { return (dt === date); }).length !== 0);

		if (!exists) {
			this.attr.dates.push (date);
			this.attr.dates.sort ();
			ret = true;
		}

		MIOGA.logDebug ('CantorSurvey', 1, '[addDate] Leaving, status: ' + ret);
		return (ret);
	};


	// ========================================================
	// Private methods
	// ========================================================
	function setMethods () {
		if (this.attr.is_mine) {
			// Survey is mine, I just can edit it
			this.store = store;
		}
		else {
			// Survey is not mine, I just can answer it
			this.store = answer;
		}
	};

	// Convert a object date to a beautiful string date dd/mm/yyyy hh:mm:ss
	function dateToString (objDate){
		var year  = objDate.getFullYear();
		var month = (objDate.getMonth()   <  9)? "0" + (objDate.getMonth() + 1):(objDate.getMonth() + 1);
		var day   = (objDate.getDate()    < 10)? "0" + objDate.getDate():objDate.getDate();
		var hour  = (objDate.getHours()   < 10)? "0" + objDate.getHours():objDate.getHours();
		var min   = (objDate.getMinutes() < 10)? "0" + objDate.getMinutes():objDate.getMinutes();
		var sec   = "00";

		return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
	}


	// ========================================================
	// Constructor
	// ========================================================
	MIOGA.logDebug ('CantorSurvey', 1, '[new] Entering, options: ', options);
	var that = this;

	// Fetch user data from Mioga2 if required
	if ((options !== undefined) && (Object.keys (options).length === 1) && (options.rowid !== undefined)) {
		// Object attributes has a single key (rowid), fetch data from Mioga2
		this.attr = options;
		this.fetch ();
	}
	else {
		// Object attribute is supposed to a complete user description, no need to fetch data
		this.attr = $.extend (true, {}, defaults, options);
		if (this.attr.rowid === undefined) {
			// Empty survey for creation, is mine, of course !
			this.attr.is_mine = true;

			// Add connected user
			this.addAttendee ({
				rowid: mioga_context.user.rowid,
				ident: mioga_context.user.ident,
				firstname: mioga_context.user.firstname,
				lastname: mioga_context.user.lastname,
				email: mioga_context.user.email,
				is_me: true
			});

			// Insert fake attendee_id, otherwise, all attendees have rowid 0 and no way to identify connected user
			this.attr.attendees[0].rowid = -1;
			this.attr.attendee_id = -1;

			// Add current date
			var d = new Date ();
			d.setHours (d.getHours () + 1);
			d.setMinutes (0);
			d.setSeconds (0);
			this.attr.dates.push (dateToString.call (this, d));
		}
		setMethods.call (this);
	}

	MIOGA.logDebug ('CantorSurvey', 1, '[new] Leaving, data: ', this.attr);
}
