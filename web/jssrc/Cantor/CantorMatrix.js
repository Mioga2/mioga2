	// ========================================================================================================
	// CantorMatrix object, allows to manipulate CantorMatrix UI
	// ========================================================================================================
	function CantorMatrix (elem, options) {

		// ========================================================
		// Private methods
		// ========================================================

		// -------------------------------------------------------------
		// Draw the matrix
		// -------------------------------------------------------------
		function drawMatrix() {
			var that = this;
			MIOGA.logDebug ('CantorMatrix', 1, "[draw] data: ", that.survey.attr);

			this.$table_body.empty ();

			var $tr = $('<tr></tr>').appendTo(this.$table_body);
			$('<th></th>').addClass('th_none_style').appendTo($tr);

			// Show / hide attendee insertion box
			if (!this.survey.attr.is_mine) {
				this.$insert_attendee_box.hide ();
			}
			else {
				this.$insert_attendee_box.show ();
			}

			// Create columns for dates
			if ((that.survey.attr.dates !== undefined) && that.survey.attr.dates.length) {
				$.each (that.survey.attr.dates, function (index, date) {
					var $th_date = $('<th></th>').addClass('th_date_style').addClass (date.replace(/[ \-:]/g, '')).attr('col_index', index).appendTo ($tr);

					var $item_delete_container = $('<div class="contener-item-delete-date"></div>').appendTo ($th_date);
					var $item_delete = $('<div class="item-delete" style="display:none;">&#160;</div>').appendTo ($item_delete_container);
					$th_date.append('<div class="th_date_style">' + date + '</div>');
					if (that.survey.attr.is_mine) {
						// Survey is mine, I can delete dates
						$th_date.hover(
							function () {
								$item_delete.show();
							},
							function () {
								$item_delete.hide();
							}
						);

						// Date deletion
						$item_delete.click(function(){
							// Delete record for date
							that.survey.attr.dates.splice (index, 1);
							that.survey.attr.modified = true;

							// Delete attendees' availability, if any
							if ((that.survey.attr.attendees !== undefined) && that.survey.attr.attendees.length) {
								$.each (that.survey.attr.attendees, function (index, attendee) {
									if ((attendee.dates !== undefined) && (attendee.dates[date] !== undefined)) {
										delete (attendee.dates[date]);
									}
								});
							}

							// Delete resources' availability, if any
							if ((that.survey.attr.resources !== undefined) && that.survey.attr.resources.length) {
								$.each (that.survey.attr.resources, function (index, resource) {
									if ((resource.dates !== undefined) && (resource.dates[date] !== undefined)) {
										delete (resource.dates[date]);
									}
								});
							}

							// Redraw
							drawMatrix.call(that);
						});
					}
				});
			}

			// Create rows for attendees
			if ((that.survey.attr.attendees !== undefined) && that.survey.attr.attendees.length) {
				$.each (that.survey.attr.attendees, function (index, attendee) {
					createEntry_Attendee.call (that, attendee, index);
				});
			}

			// Create rows for resources
			if ((that.survey.attr.resources !== undefined) && that.survey.attr.resources.length) {
				$.each (that.survey.attr.resources, function (index, resource) {
					createEntry_Resource.call (that, resource, index);
				});
			}
		}


		// -------------------------------------------------------------
		// createEntry_Attendee, create an attendee table row
		// -------------------------------------------------------------
		function createEntry_Attendee (attendee, index) {
			MIOGA.logDebug ('CantorMatrix', 1, '[createEntry_Attendee] Data: ', attendee);

			var that = this;

			var $entry = $('<tr></tr>').appendTo (this.$table_body);
			if (((that.survey.attr.attendee_id !== undefined) && (attendee.rowid === that.survey.attr.attendee_id)) || attendee.is_me) {
				$entry.addClass ('is-me');
			}
			var $cont = $('<th></th>').addClass ('th_attendee_style').appendTo ($entry);

			var user_name = ((attendee.user_id !== undefined) && (attendee.user_id !== null)) ? attendee.firstname + ' ' + attendee.lastname : attendee.fullname;
			$('<span></span>').append (user_name).appendTo ($cont);

			if (that.survey.attr.is_mine) {
				// Survey is mine, I can remove attendees
				$cont_span = $('<div></div>').addClass('contener-item-delete-attendee').appendTo ($cont);
				$('<span class="item-delete" style="display:none;"></span>').appendTo ($cont_span);

				// Apply hover on node div
				$cont.hover(function () {
						$(this).find('span.item-delete').show();
					},
					function () {
						$(this).find('span.item-delete').hide();
					}
				);

				// Apply click
				$cont.find('span.item-delete').click(function(){
					that.survey.attr.attendees.splice (index, 1);
					that.survey.attr.modified = true;
					$(this).parents('tr').remove();
				});
			}

			// Add cell
			if ((that.survey.attr.dates !== undefined) && that.survey.attr.dates.length) {
				$.each (that.survey.attr.dates, function (index, date) {
					var $cell = $('<td></td>').addClass ('matrixcantor-appointement').addClass (date.replace(/[ \-:]/g, '')).attr('date', date).appendTo ($entry);

					if ((attendee.dates !== undefined) && (attendee.dates[date] !== undefined) && (attendee.dates[date] === true)) {
						$cell.addClass('matrixcantor-appointement-selected');
					}

					if (that.survey.attr.attendee_id === attendee.rowid) {
						$cell.addClass ('can-modify');
						$cell.click(function(){
							if($(this).hasClass("matrixcantor-appointement-selected")) {
								$(this).removeClass("matrixcantor-appointement-selected");
								delete (attendee.dates[date]);
							}
							else {
								$(this).addClass("matrixcantor-appointement-selected");
								if (attendee.dates === undefined) {
									attendee.dates = { };
								}
								attendee.dates[date] = true;
							}
							that.survey.attr.modified = true;
						});
					}
				});
			}
		}


		// -------------------------------------------------------------
		// createEntry_Resource, create a resource table row
		// -------------------------------------------------------------
		function createEntry_Resource (resource, index) {
			MIOGA.logDebug ('jquery.matrixCantor', 1, '[createEntry_Resource] Data: ' + resource);

			var that = this;

			var $entry = $('<tr></tr>').attr('index_resource', index).appendTo (this.$table_body);
			var $cont = $('<th></th>').addClass ('th_resource_style').appendTo ($entry);

			$('<span></span>').append(resource.ident).appendTo ($cont);

			if (that.survey.attr.is_mine) {
				// Survey is mine, I can remove resources
				$cont_span = $('<div></div>').addClass('contener-item-delete-resource').appendTo ($cont);
				$('<span class="item-delete" style="display:none;">Delete resource</span>').appendTo ($cont_span);

				// Apply hover on node div
				$cont.hover(function () {
						$(this).find('span.item-delete').show();
					},
					function () {
						$(this).find('span.item-delete').hide();
					}
				);

				// Apply click
				$cont.find('span.item-delete').click(function(){
					that.survey.attr.resources.splice (index, 1);
					that.survey.attr.modified = true;
					$(this).parents('tr').remove();
				});
			}

			// Add cell
			if ((that.survey.attr.dates !== undefined) && that.survey.attr.dates.length) {
				$.each (that.survey.attr.dates, function (index, date) {
					var $cell = $('<td></td>').addClass ('matrixcantor-appointement').addClass (date.replace(/[ \-:]/g, '')).appendTo ($entry);

					if ((resource.dates !== undefined) && (resource.dates[date] !== undefined) && (resource.dates[date] === true)) {
						$cell.addClass('matrixcantor-appointement-selected');
					}
				});
			}
		}

		// Callback to be called at date insert
		function insertCB_date(data, highlight) {
			// Convert date object to string
			var converted = convert_ObjectDate_to_StringDate(data);

			that.survey.addDate (converted);

			// Show data
			drawMatrix.call (that);

			if (highlight) {
				// Blink newly-added date
				var blink = function () {
					$('.' + converted.replace (/[ \-:]/g, '')).animate({opacity: 0}, 500, 'linear').animate({opacity: 1}, 500, "linear");
				}
				blink ();
				var timer = setInterval (blink, 2000);
				$('.' + converted.replace (/[ \-:]/g, '')).hover (function () {
					clearTimeout (timer);
				});
			}

			return true;
		};

		// Callback to be called at attendee insert
		function insertCB_attendee(data) {
			// Verify data
			if(data['name'] === ''){ // TODO : Verify that this feature is implemented on IE8
				alert(i18n.msg_error_attendee_name_empty);
				return false;
			}

			if(isEmail(data['email']) === false){
				alert(i18n.msg_error_attendee_email);
				return false;
			}

			// Insert data if the attendee don't exist
			var added = that.survey.addAttendee ({
				fullname: data.name,
				email: data.email,
				dates: {}
			});
			if (added === true) {
				drawMatrix.call (that);
			}
			else {
				alert(i18n.msg_error_attendee_alreadyexist);
				return false;
			}

			return true;
		};


		// Check string is a valid e-mail address
		function isEmail(emailStr) {
			var checkTLD = 1;
			var knownDomsPat = /^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum|fr)$/;
			var emailPat = /^(.+)@(.+)$/;
			var specialChars = "\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
			var validChars = "\[^\\s" + specialChars + "\]";
			var quotedUser = "(\"[^\"]*\")";
			var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
			var atom = validChars + '+';
			var word = "(" + atom + "|" + quotedUser + ")";
			var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
			var domainPat = new RegExp("^" + atom + "(\\." + atom +")*$");
			var matchArray = emailStr.match(emailPat);

			if (matchArray == null) { return false; }

			var user = matchArray[1];
			var domain = matchArray[2];

			for (i=0; i<user.length; i++) {
				if (user.charCodeAt(i) > 127) { return false; }
			}
			for (i=0; i<domain.length; i++) {
				if (domain.charCodeAt(i) > 127) { return false; }
			}
			if (user.match(userPat) == null) { return false; }
			var IPArray=domain.match(ipDomainPat);
			if (IPArray != null) {
				for (var i=1; i<=4; i++) {
					if (IPArray[i] > 255) { return false; }
				}
				return true;
			}
			var atomPat = new RegExp("^" + atom + "$");
			var domArr = domain.split(".");
			var len = domArr.length;
			for (i=0; i<len; i++) {
				if (domArr[i].search(atomPat) == -1) { return false; }
			}
			if (checkTLD && domArr[domArr.length-1].length!=2 && domArr[domArr.length-1].search(knownDomsPat)==-1) { return false; }
			if (len < 2) { return false; }
			return true;
		}


		// Convert a date at the format dd/mm/yyyy hh:mm:ss to object date
		function convert_StringDate_to_ObjectDate(sTimeStamp, sep){
			var tmp = sTimeStamp.split(' ');

			// Set default separator
			if (sep === undefined) {
				sep = '/';
			}

			if(tmp.length != 2) {
				return [false, ''];
			}

			var tmp_date = tmp[0].split(sep);
			var tmp_time = tmp[1].split(':');

			if(tmp_date.length != 3) {
				return [false, ''];
			}

			if(tmp_time.length != 3) {
				return [false, ''];
			}

			if(sep == '/') {
				var obj_Date = new Date(tmp_date[2], tmp_date[1]-1, tmp_date[0], tmp_time[0], tmp_time[1], 0);
			}
			else {
				var obj_Date = new Date(tmp_date[0], tmp_date[1]-1, tmp_date[2], tmp_time[0], tmp_time[1], 0);
			}

			if (isNaN(obj_Date.valueOf())) {
				return [false, ''];
			}
			else {
				return [true, obj_Date];
			}
		}

		// Convert a object date to a beautiful string date dd/mm/yyyy hh:mm:ss
		function convert_ObjectDate_to_StringDate(objDate){
			var year  = objDate.getFullYear();
			var month = (objDate.getMonth()   <  9)? "0" + (objDate.getMonth() + 1):(objDate.getMonth() + 1);
			var day   = (objDate.getDate()    < 10)? "0" + objDate.getDate():objDate.getDate();
			var hour  = (objDate.getHours()   < 10)? "0" + objDate.getHours():objDate.getHours();
			var min   = (objDate.getMinutes() < 10)? "0" + objDate.getMinutes():objDate.getMinutes();
			var sec   = "00";

			return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('CantorMatrix', 1, '[CantorMatrix] Entering');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };

		this.options    = options;
		this.survey     = options.survey;

		var i18n = options.i18n;


		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="cantor-survey-matrix"></div>').appendTo ($(this.elem));

		// -------------------------------------------------------------
		// Dialog box add appointement
		// -------------------------------------------------------------
		var $dialog_box_date = $('<div style="display: none;"></div>').appendTo (this.$cont);
		var $content = $('<div></div>').addClass("form").appendTo ($dialog_box_date);

		var $row = $('<div class="row"></div>').appendTo ($content);
		var $cell = $('<div class="span4"></div>').appendTo ($row);
		var $label_date = $("<span></span>").append(i18n.label_date).appendTo ($cell);
		var $cell = $('<div class="span8"></div>').appendTo ($row);
		var $input_date = $('<input></input>').attr('maxlength',10).appendTo ($cell);
		var $row = $('<div class="row"></div>').appendTo ($content);
		var $cell = $('<div class="span12"></div>').appendTo ($row);
		var $selector_date = $('<div></div>').appendTo ($cell);
		$selector_date.datepicker({
			dateFormat: 'dd/mm/yy',
			onSelect: function(dateText) {
				$input_date.val	(dateText);
			}
		});

		var $row = $('<div class="row"></div>').appendTo ($content);
		var $cell = $('<div class="span4"></div>').appendTo ($row);
		var $label_time = $("<span></span>").append(i18n.label_time).appendTo ($cell);
		var $cell = $('<div class="span8"></div>').appendTo ($row);
		var $input_time = $('<input></input>').attr('maxlength',5).appendTo ($cell);
		var $row = $('<div class="row"></div>').appendTo ($content);
		var $cell = $('<div class="span12"></div>').appendTo ($row);
		var $selector_time = $('<div></div>').appendTo ($cell);
		$selector_time.timepicker({
			timeSeparator: ':',
			onSelect: function(dateText) {
				$input_time.val	(dateText);
			}
		});

		// Save and cancel datetime button
		var $row = $('<div class="row"></div>').appendTo ($content);
		var $row = $('<div class="row"></div>').appendTo ($content);
		var $save_btn_cont = $('<div class="span6"></div>').appendTo ($row);
		var $save_btn = $('<button class="button"></button>').append (i18n.btn_ok).appendTo ($save_btn_cont).click (function () {
			var ret = convert_StringDate_to_ObjectDate($input_date.val() + " " + $input_time.val() + ":00");

			// Date format is valid ?
			if(ret[0] == true) {
				if(insertCB_date.call (that, ret[1], true) == true) {
					that.survey.attr.modified = true;
					$dialog_box_date.dialog ('close');
				}
			}
			else {
				alert(i18n.msg_error_date_invalid);
			}
		});

		var $cancel_btn_cont = $('<div class="span6"></div>').appendTo ($row);
		var $cancel_btn = $('<button class="button cancel"></button>').append (i18n.btn_cancel).appendTo ($cancel_btn_cont).click (function () {
			$dialog_box_date.dialog('close');
		});

		// -------------------------------------------------------------
		// Matrix Attendee & Ressource / Appointement
		// -------------------------------------------------------------
		// Add matrix attendee / date
		var $div_container_table = $('<div></div>').addClass("el-container").appendTo (this.$cont);
		var $div = $('<div></div>').addClass("el-container-table").appendTo($div_container_table);
		var $table = $('<table></table>').addClass("cantor_matrix_survey").addClass("list").appendTo($div);
		this.$table_body = $('<tbody></tbody>').appendTo ($table);

		// Add button for insert date
		if (this.survey.attr.is_mine) {
			// Survey is mine, I can add dates
			var $cont = $('<div></div>').addClass("el-container-adddate").appendTo ($div_container_table);
			$('<span class="item-create-date"></span>').append (i18n.add_date).appendTo ($cont).click (function () {
				$dialog_box_date.dialog('destroy');
				$dialog_box_date.dialog({
					title    : i18n.select_date,
					resizable: false,
					modal    : true,
					open	 : function () {
						$selector_date.datepicker("setDate", new Date());
						$input_date.val($selector_date.datepicker().val());

						var min = new Date().getMinutes();
						min = min - min % 5;
						$selector_time.timepicker('setTime', new Date().getHours() + ':' + min);
						$input_time.val($selector_time.timepicker().val());
					}
				});
			});
		}

		// Add field for insert attendees
		this.$insert_attendee_box = $('<div></div>').addClass("el-container-addattendee").appendTo ($div_container_table);
		var $cell = $('<div></div>').appendTo(this.$insert_attendee_box);

		if (this.survey.attr.is_mine) {
			// Survey is mine, I can add attendees and resources
			// Button bar, add Mioga users, add resources
			var $button_bar = $('<ul class="button_list"></ul>').appendTo ($cell);

			// -------------------------------------------------------------
			// Add line button and dialog
			// -------------------------------------------------------------
			$('<button class="button"></button>').append (i18n.add_from_mioga.replace (/\$instance\$/, mioga_context.instance.ident)).appendTo ($('<li></li>').appendTo ($button_bar)).click (function () {
				if (that.$dialog_box_add_lines === undefined) {
					// Dialog contents
					that.$dialog_box_add_lines = $('<div></div>');
					function getUser () {
						var users = undefined;
						$.ajax ({
							url: 'GetUsers.json',
							type: 'GET',
							async: false,
							traditional: true,
							dataType: 'json',
							data: {
								app_name : "Cantor"
							},
							success: function (data) {
								MIOGA.logDebug ('CantorMatrix', 1, "[fetch] GetUsers.json returned, data: ", data);
								users = data;
							}
						});
						return (users);
					}
					var $userselect_args = {
							app_name : "Cantor",
							single : false,
							i18n: {
								select_all: i18n.select_all,
								deselect_all: i18n.deselect_all,
								add_btn: i18n.add,
								all:i18n.all,
								columns: [
									{
										field: 'firstname',
										label: i18n.firstname
									},
									{
										field: 'lastname',
										label: i18n.lastname,
										Default: true
									},
									{
										field: 'email',
										label: i18n.email
									}
								]
							},
							data	: getUser (),
							select_add : function (rowids) {
								var userList = that.$user_selector.userSelect ('get_list');
								$.each (userList, function (index, user) {
									if (user.selected === true) {
										that.survey.addAttendee ({
											rowid: user.rowid,
											firstname: user.firstname,
											lastname: user.lastname,
											email: user.email
										});
										drawMatrix.call (that);
									}
								});
								that.survey.attr.modified = true;
								that.$user_selector.userSelect('refresh_list');
							},
							select_change : function () {
							}
					};

					var team_members = { };
					var $teamselect_args = {
							app_name : "Cantor",
							single : false,
							i18n: {
								select_all: i18n.select_all,
								deselect_all: i18n.deselect_all,
								add_btn: i18n.add
							},
							data	: function (arg) {
								// Get teams
								var teams = undefined;
								$.ajax ({
									url: 'GetTeams.json',
									type: 'GET',
									async: false,
									traditional: true,
									dataType: 'json',
									success: function (data) {
										MIOGA.logDebug ('CantorMatrix', 1, "[fetch] GetTeams.json returned, data: ", data);
										teams = data;
										if (teams && teams.length) {
											for (var i=0; i<teams.length; i++) {
												var team = teams[i];
												team_members[team['rowid']] = team.members;
												delete (team.members);
											}
										}
									}
								});
								return (teams);
							},
							select_add : function (rowids) {
								if (rowids && rowids.length) {
									for (var i=0; i<rowids.length; i++) {
										var rowid = rowids[i];
										if (team_members[rowid] && team_members[rowid].length) {
											for (var j=0; j<team_members[rowid].length; j++) {
												var user = team_members[rowid][j];
												that.survey.addAttendee ({
													rowid: user.rowid,
													firstname: user.firstname,
													lastname: user.lastname,
													email: user.email
												});
												drawMatrix.call (that);
											}
										}
									}
								}
								that.survey.attr.modified = true;
								that.$team_selector.itemSelect('refresh_list');
							},
							select_change : function () {
							}
					};

					var $resourceselect_args = {
							app_name : "Cantor",
							single : false,
							i18n: {
								select_all: i18n.select_all,
								deselect_all: i18n.deselect_all,
								add_btn: i18n.add
							},
							data	: function (arg) {
								// Get resources
								var resources = undefined;
								$.ajax ({
									url: 'GetResources.json',
									type: 'GET',
									async: false,
									traditional: true,
									dataType: 'json',
									success: function (data) {
										MIOGA.logDebug ('CantorMatrix', 1, "[fetch] GetResources.json returned, data: ", data);
										resources = data;
									}
								});
								return (resources);
							},
							select_add : function (rowids) {
								var resourceList = that.$resource_selector.itemSelect ('get_list');

								$.each (resourceList, function (index, resource) {
									if (resource.selected === true) {
										that.survey.addResource ({
											rowid: resource.rowid,
											ident: resource.ident
										});
										drawMatrix.call (that);
									}
								});
								that.survey.attr.modified = true;
								that.$resource_selector.itemSelect('refresh_list');
							},
							select_change : function () {
							}
					};

					// Mail search
					var $mail_search = $('<div class="form form-item"></div>');
					$('<label for="email"></label>').append (i18n.email).appendTo ($mail_search);
					var $search_mail_input = $('<input type="text" name="email"/>').appendTo ($mail_search).focus ();
					var $mail_search_btn = $('<button class="button"></button>').append (i18n.search).appendTo ($mail_search);
					var $mail_search_results = $('<div class="mail-search-results"></div>').appendTo ($mail_search);
					$search_mail_input.keypress (function (evt) {
						if (evt.keyCode == 13) {
							$mail_search_btn.trigger ('click');
						}
					});
					$mail_search_btn.click (function () {
						if ($search_mail_input.val () !== '') {
							$.ajax ({
								url: 'SearchUser.json',
								type: 'GET',
								async: false,
								traditional: true,
								dataType: 'json',
								data: {email: $search_mail_input.val ()},
								success: function (data) {
									MIOGA.logDebug ('CantorMatrix', 1, "[fetch] SearchUser.json returned, data: ", data);
									$mail_search_results.empty ();
									if (data && data.rowid) {
										var $user_import = $('<span></span>').append (data.label).appendTo ($mail_search_results);
										$('<button class="button"></button>').append (i18n.add).appendTo ($mail_search_results).click (function () {
											that.survey.addAttendee ({
												rowid: data.rowid,
												firstname: data.firstname,
												lastname: data.lastname,
												email: data.email
											});
											drawMatrix.call (that);
											that.survey.attr.modified = true;

											$search_mail_input.val ('').focus ();
											$mail_search_results.empty ();
											return (false);
										}).focus ();
									}
									else {
										$mail_search_results.append ($('<p></p>').append (i18n.no_user_found));
										var $user_add = $('<div class="form form-item"></div>').appendTo ($mail_search_results);
										var $fullname_input = $('<input type="text" name="fullname"/>').attr ('placeholder', i18n.fullname_placeholder).appendTo ($user_add).focus ();
										var $fullname_btn = $('<button class="button"></button>').append (i18n.add).appendTo ($user_add).click (function () {
											if ($fullname_input.val () !== '') {
												that.survey.addAttendee ({
													fullname: $fullname_input.val (),
													email: $search_mail_input.val ()
												});
												drawMatrix.call (that);
												that.survey.attr.modified = true;

												$search_mail_input.val ('').focus ();
												$fullname_input.val ('');
												$mail_search_results.empty ();
											}
											return (false);
										});

										$fullname_input.keypress (function (evt) {
											if (evt.keyCode == 13) {
												$fullname_btn.trigger ('click');
											}
										});
									}
								}
							});
						}

						return (false);
					});

					var $tab_cont = $('<div class="dialog-tabs"></div>').appendTo (that.$dialog_box_add_lines);
					var $tabs = $('<ul></ul>').appendTo ($tab_cont);

					// Search user by mail
					$tabs.append ($('<li></li>').append ($('<a href="#tab-search-mail"></a>').append (i18n.search_users)));
					$tab_cont.append ($('<div id="tab-search-mail"></div>').append ($mail_search));

					// Select users from list
					$tabs.append ($('<li></li>').append ($('<a href="#tab-select-users"></a>').append (i18n.select_users)));
					that.$user_selector = $('<div class="user-select-dialog"></div>').appendTo ($('<div id="tab-select-users"></div>').appendTo ($tab_cont));

					// Select teams from list
					$tabs.append ($('<li></li>').append ($('<a href="#tab-select-teams"></a>').append (i18n.select_teams)));
					that.$team_selector = $('<div class="team-select-dialog"></div>').appendTo ($('<div id="tab-select-teams"></div>').appendTo ($tab_cont));

					// Select resources from list
					$tabs.append ($('<li></li>').append ($('<a href="#tab-select-resources"></a>').append (i18n.select_resources)));
					that.$resource_selector = $('<div class="resource-select-dialog"></div>').appendTo ($('<div id="tab-select-resources"></div>').appendTo ($tab_cont));
					
					// Close button
					$('<button class="button cancel"></button>').append (i18n.close).appendTo ($('<li></li>').appendTo ($('<ul class="button_list"></ul>').appendTo (that.$dialog_box_add_lines))).click (function () {
						that.$dialog_box_add_lines.dialog ('close');
						return (false);
					});

					// Create dialog
					that.$dialog_box_add_lines.dialog({
						title    : i18n.select_from_mioga.replace (/\$instance\$/, mioga_context.instance.ident),
						width: Math.round( (0.7 * $(window).width () ) / 10 ) * 10,
						height: Math.round( (0.8 * $(window).height () ) / 10 ) * 10,
						modal    : true,
						resizable: true
					});
					
					// Create sub-plugins
					that.$user_selector.css('visibility' , 'hidden');
					that.$user_selector.userSelect ($userselect_args);
					that.$team_selector.itemSelect ($teamselect_args);
					that.$resource_selector.itemSelect ($resourceselect_args);
					// Create tabs
					$tab_cont.tabs ({
						show: function (e, ui) {
							switch ($(ui.tab).attr ('href')) {
								case '#tab-search-mail':
									$search_mail_input.focus ();
									break;
							}
						}
					});
					that.$user_selector.css('visibility' , 'visible');
				}
				else {
					that.$dialog_box_add_lines.dialog ('open');
				}
				return false;
			});
		}

		// Draw the matrix
		drawMatrix.call(this);

		MIOGA.logDebug ('CantorMatrix', 1, '[CantorMatrix] Leaving');
	};
