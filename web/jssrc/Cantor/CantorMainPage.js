	// ========================================================================================================
	// CantorMainPage object, UI entry point
	// ========================================================================================================
	function CantorMainPage (elem, options) {


		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data) {
			MIOGA.logDebug ('CantorMainPage', 1, '[refresh] data:', data);
			var that = this; // For callback reference to current object

			that.data = data;

			// Remove error messages
			$('span.error-message').remove ();

			// User's surveys
			that.$my_surveys.empty ();
			if ((data.surveys !== undefined) && data.surveys.length) {
				$.each (data.surveys, function (index, survey) {
					var $line = $('<tr></tr>').appendTo (that.$my_surveys);
					// Survey deletion
					var $deleter = $('<span class="item-delete"></span>').appendTo ($('<td class="delete-container"></td>').appendTo ($line)).hide ().click (function () {
						MIOGA.confirm ({
							title: i18n.confirm_survey_deletion_title.replace (/\$title\$/, survey.attr.title),
							message: $('<p class="error"></p>').append (i18n.confirm_survey_deletion.replace (/\$title\$/, survey.attr.title)),
							i18n_OK: i18n.btn_delete,
							i18n_cancel: i18n.btn_cancel,
							cb_OK: function (ev, $dialog) {
								var res = survey.destroy ();
								if (res.success) {
									that.$message_box.inlineMessage ('info', res.message);
									$line.empty ();
								}
								else {
									that.$message_box.inlineMessage ('error', res.message);
								}
								$dialog.dialog ('destroy');
							},
							cb_cancel: function (ev, $dialog) {
								$dialog.dialog ('destroy');
							}
						});
					});
					$line.hover (function () {
						$deleter.toggle ();
					});

					// Survey attributes
					$.each (that.options.columns.my_surveys, function (index, field) {
						$('<td></td>').append (formatValue.call (that, survey, field, 'survey')).addClass (field).appendTo ($line);
					});
				});
			}

			// Solicitation
			that.$solicitations.empty ();
			if ((data.solicitations !== undefined) && data.solicitations.length) {
				$.each (data.solicitations, function (index, solicitation) {
					var $line = $('<tr></tr>').appendTo (that.$solicitations);
					$.each (that.options.columns.solicitation, function (index, field) {
						$('<td></td>').append (formatValue.call (that, solicitation, field, 'solicitation')).addClass (field).appendTo ($line);
					});
				});
			}

			MIOGA.logDebug ('CantorMainPage', 1, '[CantorMainPage.refresh] Leaving');
		};


		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// formatValue
		// Given an object (hash) the name of a field and an object type,
		// format inner data for field display an interaction (click events, etc.)
		// --------------------------------------------------------
		function formatValue (object, field, object_type) {
			var that = this;

			// Default behavior is to return the field as-is
			var value = object.attr[field];

			// Some fields need a specific contruction rule
			if (field === 'progress') {
				if (object.attr[field] !== undefined) {
					value = '<progress id="barre-progression" max="100" value="' + value + '"></progress>';
				}
				else {
					value = $('<span class="note"></span>').append (i18n.non_applicable);
				}
			}
			else if (field === 'modified') {
				value = object.attr[field].replace (/\.[0-9]*$/, '');
			}
			else if (field === 'title') {
				value = $('<a></a>').attr ('href', cantor.generateNavigationHash ('survey', object.attr.rowid)).append (object.attr[field]);
			}
			else if (field === 'status') {
				if (object.attr[field] === true) {
					value = $('<div class="answered"></div>');
				}
			}

			return (value);
		};

		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('CantorMainPage', 1, '[CantorMainPage] Entering');
		var that = this; // For callback reference to current object
		this.elem       = elem;

		var i18n = options.i18n;

		var cantor = options.cantor;

		var defaults = {
			data: undefined,
			columns: {
				my_surveys: ['title', 'description', 'modified', 'progress'],
				solicitation: ['title', 'status']
			}
		};

		this.options =  $.extend (true, {}, defaults, options);

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="cantor-summary"></div>').appendTo (this.elem);

		// Message box
		this.$message_box = $('<div></div>').inlineMessage ().appendTo (this.$cont);

		// -------------------------------------------------------------
		// Button bar
		// -------------------------------------------------------------
		// Create Survey button
		var $btn_cont = $('<ul class="button_list"></ul>').appendTo (this.$cont);
		this.$create_btn = $('<a class="button"></a>').append (i18n.btn_create).attr ('href', cantor.generateNavigationHash ('survey')).appendTo ($('<li></li>').appendTo ($btn_cont));


		// -------------------------------------------------------------
		// Contents
		// -------------------------------------------------------------
		var $row = $('<div class="row"></div>').appendTo (this.$cont);

		//--- First column ---
		var $cell = $('<div class="span8"></div>').appendTo ($row);

		// My surveys list box
		var $box = $('<div class="sbox my-surveys"><h2 class="title_bg_color">' + i18n.my_surveys + '</h2></div>').appendTo ($cell);
		var $contents = $('<div class="content"></div>').appendTo ($box);
		var $table = $('<table class="highlight"></table>').appendTo ($contents);
		$('<th class="delete-container"></th>').appendTo ($table);
		$.each (this.options.columns.my_surveys, function (index, value) {
			$('<th>' + i18n[value] + '</th>').addClass (value).appendTo ($table);
		});
		this.$my_surveys = $('<tbody></tbody>').appendTo ($table);

		//--- Second column ---
		$cell = $('<div class="span4"></div>').appendTo ($row);

		// Solicitation list box
		var $box = $('<div class="sbox solicitation"><h2 class="title_bg_color">' + i18n.list_solicitation + '</h2></div>').appendTo ($cell);
		var $contents = $('<div class="content"></div>').appendTo ($box);
		var $table = $('<table class="highlight"></table>').appendTo ($contents);
		$.each (this.options.columns.solicitation, function (index, value) {
			$('<th>' + i18n[value] + '</th>').addClass (value).appendTo ($table);
		});
		this.$solicitations = $('<tbody></tbody>').appendTo ($table);

		MIOGA.logDebug ('CantorMainPage', 1, '[new] Leaving');
	};
