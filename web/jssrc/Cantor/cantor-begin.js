// -----------------------------------------------------------------------------
// Fouquet is Mioga2 group animation application. It is constructed as a jQuery
// plugin.
// -----------------------------------------------------------------------------
(function ($) {
	var defaults = {
		i18n: {
			"progressBarText": "Cantor is loading...",
			"btn_create": "Create survey",
			"btn_save": "Save survey",
			"btn_cancel": "Cancel",
			"btn_add_user": "Add attendee ",
			"my_surveys": "My surveys",
			"title": "Title",
			"description": "Description",
			"modified": "Last modification",
			"progress": "Progress",
			"duration": "Duration",
			"open_attendee_list": "Attendee list",
			"open_list": "Open list",
			"closed_list": "Closed list",
			"url": "Survey URL",
			"list_solicitation": "Solicitation",
			"status": "Status",
			"confirm_cancel_survey_edit" : "Are you sure you want to cancel ?",
			"survey_update_success" : "Survey successfully updated.",
			"msg_error_attendee_name_empty": "Attendee name is empty.",
			"msg_error_attendee_email": "Attendee e-mail address is invalid.",
			"msg_error_attendee_alreadyexist": "Attendee is already present in the list.",
			"msg_error_resource_alreadyexist": "Resource is already present in the list.",
			"label_date": "Date:",
			"btn_ok": "OK",
			"msg_error_date_invalid": "Date is invalid.",
			"add_date": "Add date",
			"select_date": "Select date",
			"label_add_attendee": "Add attendee",
			"fullname_placeholder": "Attendee name",
			"email_placeholder": "Attendee e-mail",
			"add_from_mioga": "Add attendee",
			"select_from_mioga": "Select from $instance$",
			"btn_add_resource": "Add",
			"label_time": "Time:",
			"select_all": "Select all",
			"deselect_all": "Select none",
			"all": "All",
			"add": "Add",
			"firstname": "Firstname",
			"lastname": "Lastname",
			"email": "E-mail",
			"select_mioga_resource": "Select resources",
			"no_ui": "No UI for ",
			"short_url": "Short URL to survey",
			"enable_short_url": "Click here to enable short URL access to your survey",
			"short_url_enabled_later": "The short URL will be enabled when you store your survey",
			"short_url_disabled_later": "The short URL will be disabled when you store your survey",
			"confirm_survey_deletion_title": "Delete survey \"$title$\" ?",
			"confirm_survey_deletion": "Are you sure you want to delete survey \"$title$\" ?",
			"btn_delete": "Delete",
			"btn_cancel": "Cancel",
			"identify_yourself": "Identify yourself",
			"identify_yourself_email_prompt": "In order to answer this survey, you need to be identified. Please type your email address below.",
			"email_address": "Email address",
			"identify_yourself_fullname_prompt": "You are not registered in survey attendees yet. To register yourself, please type your full name below.",
			"fullname": "Full name",
			"duration_placeholder": "HH:MM",
			"duration_format_error": "The duration is invalid, please fill a duration in the form \"HH:MM\"",
			"btn_back_to_cantor": "Back to Cantor",
			"search_users": "Search users",
			"select_users": "Select users",
			"select_teams": "Select teams",
			"select_resources": "Select resources",
			"search": "Search",
			"no_user_found": "No known user matches this e-mail address. If you want this person to answer your survey, you can add him after fillin his name.",
			"close": "Close"
		}
	};

	// ========================================================
	// Effective plugin declaration with methods abstraction
	// Usage :
	//     <div id="cantor"></div>
	//
	//     $('#cantor').cantor(options);
	// ========================================================
	$.fn.cantor = function(options) {
		// method call, get existing Fouquet Object and apply method
		// if method not returns result we return this for chainability
		if (typeof options === 'string') {
			var args = Array.prototype.slice.call(arguments, 1);
			this.each(function() {
				var fouquet = $.data(this, 'fouquet');
				if (fouquet && $.isFunction(fouquet[options])) {
					var res = fouquet[options].apply(fouquet, args);
					if (res !== undefined) {
						return res;
					}
				}
				else {
					$.error( 'Method ' +  options + ' does not exist on jQuery.fouquet' );
				}
			});
		}
		else if (typeof options === 'object') {
			options =  $.extend(true, {}, defaults, options);

			this.each(function(i, item) {
				var $elem = $(item);
				var cantor = new Cantor(item, options);
				$(item).data('cantor', cantor);
			});
		}
		else {
			$.error( 'Cantor must not be there ... ');
		}
		return this;
	};
