	// ========================================================================================================
	// CantorSurveyEdit object, allows to manipulate CantorSurveyEdit UI
	// ========================================================================================================
	function CantorSurveyEdit (elem, options) {

		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// Fill view fields with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data) {
			MIOGA.logDebug ('CantorSurveyEdit', 1, '[refresh] Entering, data: ', data);
			var that = this; // For callback reference to current object

			that.survey = data;

			// --------------------------------------------------------
			// Survey attributes
			// --------------------------------------------------------
			$.each (Array ('title', 'description', 'duration', 'open_attendee_list', 'short_url'), function (index, field) {
				that.fields[field].empty ().append (formatValue.call (that, data.attr, field, 'survey'));
			});

			// --------------------------------------------------------
			// Hide elements that shouldn't appear
			// --------------------------------------------------------
			if (!this.survey.attr.is_mine) {
				that.lines.open_attendee_list.hide ();
			}
			else {
				that.lines.open_attendee_list.show ();
			}

			if ((!this.survey.attr.is_mine && (this.survey.attr.short_url === undefined)) || (this.survey.attr.rowid === undefined)) {
				that.lines.short_url.hide ();
			}
			else {
				that.lines.short_url.show ();
			}

			// --------------------------------------------------------
			// Attendees and day list
			// --------------------------------------------------------
			that.$matrix.empty ();
			new CantorMatrix (that.$matrix, {
				i18n: i18n,
				survey: this.survey
			});

			MIOGA.logDebug ('CantorSurveyEdit', 1, '[refresh] Leaving');
		};


		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// formatValue
		// Given an object (hash) and the name of a field,
		// format inner data for field display an interaction (click events, etc.)
		// --------------------------------------------------------
		function formatValue (object, field) {
			var that = this;

			var value;

			if (object.is_mine) {
				// Survey is mine, I can modify its attributes
				// Default behavior is to return a simple input
				var value = $('<input type="text"/>').attr ('name', field).val (object[field]);

				// Some fields need a specific contruction rule
				if (field === 'short_url') {
					value = $('<span></span>');
					if (object[field] !== undefined) {
						value.append ($('<a></a>').attr ('href', object[field]).append (object[field]));
						var $short_url_remover = $('<span class="item-delete"></span>').appendTo (value).hide ();
						$short_url_remover.click (function () {
							delete (that.survey.attr.short_url);
							that.survey.attr.modified = true;
							value.empty ().append (i18n.short_url_disabled_later);
						});
						value.hover (function () {
							$short_url_remover.toggle ();
						});
					}
					else {
						var $short_url_enabler = $('<a href="#"></a>').append (i18n.enable_short_url);
						value.append ($short_url_enabler);
						$short_url_enabler.click (function () {
							that.survey.attr.short_url = '';
							that.survey.attr.modified = true;
							value.empty ().append (i18n.short_url_enabled_later);
							return (false);
						});
					}
				} else if (field === 'description') {
					value = $('<textarea></textarea>').attr ('name', field).val (object[field]);
				} else if (field === 'open_attendee_list') {
					// Dropdown menu to change attendee list openness
					var $div = $('<div></div>').addClass ('inline-chooser');
					var $span = $('<span></span>').appendTo($div);
					if(object[field] === true) {
						$span.append(i18n.open_list);
					}
					else {
						$span.append(i18n.closed_list);
					}
					var $dropdown = $('<ul class="dropdown"></ul>').appendTo ($div);
					var $entry = $('<li class="dir"></li>').append ('&#x25BC;').appendTo ($dropdown);
					var $submenu = $('<ul></ul>').appendTo ($entry);

					$.each (Array ('open_list', 'closed_list'), function (ele_index, ele) {
						var $li = $('<li></li>').append(i18n[ele]).attr('key', ele).appendTo ($submenu).click (function () {
							$span.html(i18n[$(this).attr('key')]);
							that.survey.attr.open_attendee_list = (ele === 'open_list') ? true : false;
							that.survey.attr.modified = true;
						});
					});
					value = $div;
				} else if (field === 'duration') {
					var duration = object[field];
					var hours = parseInt (duration / 60, 10);
					var minutes = parseInt (duration - (hours * 60), 10);
					if (hours < 10) {
						hours = '0' + hours;
					}
					if (minutes < 10) {
						minutes = '0' + minutes;
					}
					value.val (hours + ':' + minutes);
					value.timepicker ({
						showPeriodLabels: false,
						showPeriod: false
					});
				}

				if (i18n[field + '_placeholder'] !== undefined) {
					value.attr ('placeholder', i18n[field + '_placeholder']);
				}

				// Register change handler to update underlying CantorSurvey object
				value.change (function () {
					var val = value.val ();
					if (field === 'duration') {
						// Check for input consistency
						if (!val.match (/^[0-9]{2}:[0-9]{2}$/)) {
							alert (i18n.duration_format_error);
							return;
						}
						var parts = val.split (/:/);
						val = parseInt (parts[0] * 60, 10) + parseInt (parts[1], 10);
					}
					that.survey.attr[field] = val;
					that.survey.attr.modified = true;
				});
			}
			else {
				// Survey is not mine, I can only view its attributes
				value = $('<span></span>').append (object[field]);

				// Some fields need a specific contruction rule
				if (field === 'short_url') {
					if (object[field] !== undefined) {
						value = $('<a></a>').attr ('href', object[field]).append (object[field]);
					}
				}
				else if (field === 'duration') {
					var duration = object[field];
					var hours = parseInt (duration / 60, 10);
					var minutes = parseInt (duration - (hours * 60), 10);
					if (hours < 10) {
						hours = '0' + hours;
					}
					if (minutes < 10) {
						minutes = '0' + minutes;
					}
					value = $('<span></span>').append (hours + ':' + minutes);
				}
			}

			// Set class matching field
			value.addClass ('survery-attribute-' + field);

			return (value);
		};


		// --------------------------------------------------------
		// displayError
		// display error message from WebService response
		// --------------------------------------------------------
		function displayError (res) {
			that.$message_box.inlineMessage ('error', res.message);
			$('span.error-message').remove ();
			$.each (res.errors, function (index, error) {
				MIOGA.logDebug ('CantorSurveyEdit', 1, '[displayError] Server returned error for field ' + error.field);
				var field = error.field.replace (/_id/, '');
				that.fields[field].append ('<span class="error-message">' + error.message + '</span>');
			});
		}

		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('CantorSurveyEdit', 1, '[CantorSurveyEdit] Entering');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.lines      = { };
		this.fields     = { };

		options =  $.extend (true, {}, defaults, options);
		this.options    = options;

		this.survey = options.survey;

		var i18n = options.i18n;


		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="cantor-survey"></div>').appendTo (this.elem);

		// Message box
		this.$message_box = $('<div></div>').inlineMessage ().appendTo (this.$cont);

		// -------------------------------------------------------------
		// Buttons bar
		// -------------------------------------------------------------
		var $btn_bar = $('<ul class="button_list back-btn-container"></ul>').appendTo (this.$cont);

		// Cancel button
		this.$cancel_btn = $('<button class="button cancel"></button>').append (i18n.btn_back_to_cantor).appendTo ($('<li></li>').appendTo ($btn_bar)).click (function () {
			MIOGA.logDebug ('CantorSurveyEdit', 1, '[cancel] Entering');

			if ((!that.survey.attr.modified) || confirm (i18n.confirm_cancel_survey_edit)) {
				location.href = 'DisplayMain';
			}

			MIOGA.logDebug ('CantorSurveyEdit', 1, '[cancel] Leaving');
			return (false);
		});

		// Save button
		this.$save_btn = $('<button class="button"></button>').append (i18n.btn_save).appendTo ($('<li></li>').appendTo ($btn_bar)).click (function () {
			var res = that.survey.store ();
			if (res.success) {
				var survey = new CantorSurvey (res.survey);
				if ((that.survey.attr.is_mine) && (that.$cont.find ('tr.is-me td.matrixcantor-appointement-selected').length > 0)) {
					// Answer survey for owner
					var res = survey.answer (that.survey.attr.attendees);
					if (!res.success) {
						displayError.call (that, res);
					}
				}
				var message = (res.message !== undefined) ? res.message : i18n.survey_update_success;
				that.$message_box.inlineMessage ('info', message);
				that.refresh (survey);
			}
			else {
				displayError.call (that, res);
			}
			return (false);
		});


		// -------------------------------------------------------------
		// Survey attributes box
		// -------------------------------------------------------------
		var $row = $('<div class="row"></div>').appendTo (this.$cont);
		var $cell = $('<div class="span12"></div>').appendTo ($row);

		var $content = $('<div class="content survey-edit"></div>').appendTo ($cell);
		var $table = $('<table class="table-survey form"></table>').appendTo ($content);

		$.each (Array ('title', 'description', 'duration', 'open_attendee_list', 'short_url'), function (index, field) {
			that.lines[field] = $('<tr></tr>').appendTo ($table);
			$('<th></th>').append (i18n[field]).appendTo (that.lines[field]);
			that.fields[field] = $('<td></td>').appendTo (that.lines[field]);
		});

		// -------------------------------------------------------------
		// Attendee list
		// -------------------------------------------------------------
		var $row = $('<div class="row"></div>').appendTo (this.$cont);
		var $cell = $('<div class="span12"></div>').appendTo ($row);
		that.$matrix = $('<div class="content form"></div>').appendTo ($cell);

		MIOGA.logDebug ('CantorSurveyEdit', 1, '[CantorSurveyEdit] Leaving');
	};
