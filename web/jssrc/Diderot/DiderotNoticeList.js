	// ========================================================================================================
	// DiderotNoticeList object, notice list display UI
	// ========================================================================================================
	function DiderotNoticeList (elem, options) {


		// ========================================================
		// Private methods
		// ========================================================


		// --------------------------------------------------------
		// drawTable
		// Given a list of notices, draw them into a table
		// --------------------------------------------------------
		function drawTable () {
			// Container table
			var $table = $('<table class="list sortable"></table>');

			// Load template
			var template = this.notices[0].attr.template;

			// Get max column count across lines
			var max_col_count = 0;
			$.each (template.getListOrder (), function (index, line) {
				var current_count = 0;
				if (line.length > max_col_count) {
					max_col_count = line.length;
				}
			});

			// Add column titles according to template data
			var $last_cell;
			var $head = $('<thead></thead>').appendTo ($table);
			var $line = $('<tr></tr>').appendTo ($head);
			// Get max column count from lines definitions
			var col_count = 0;
			$.each (template.getListOrder (), function (index, line) {
				col_count = (line.length > col_count) ? line.length : col_count;
			});
			$('<td class="view-notice"></td>').appendTo ($line);
			for (i=0; i<col_count; i++) {
				var $cell = $('<th></th>').appendTo ($line).addClass ('col-' + parseInt (i+1, 10));
				var $t = $('<table class="sub-table"></table>').appendTo ($cell);
				$.each (template.getListOrder (), function (index, line) {
					var field = line[i];
					var $elem = $('<td></td>').appendTo ($('<tr></tr>').appendTo ($t));
					if (field !== undefined) {
						var template_field = template.getField (field);
						if (template_field !== undefined) {
							$elem.attr ('field', field).append (template_field['field-name']);
						}
					}
					if (index != (col_count - 1)) {
						$elem.addClass ('sub-line');
					}
				});
			}

			// Add last_modified to last empty header cell
			// TODO This won't work if last line has no empty cell but previous line do
			var $last_empty_cell = $head.find ('table.sub-table tr td:empty:first');
			var append_last_modified_cell = false;
			var append_last_modified_line = false;
			if (($last_empty_cell.length === 0) && (template.getListOrder ().length === 1)) {
				// Single-line view, add cell
				var $subtable = $('<table class="sub-table"></table>').appendTo ($('<th></th>').appendTo ($line));
				$last_empty_cell = $('<td></td>').appendTo ($('<tr></tr>').appendTo ($subtable));
				append_last_modified_cell = true;
			}
			else if ($last_empty_cell.length === 0) {
				// Multi-line view with no empty cell
				$('<tr></tr>').appendTo ($head.find ('table.sub-table'));
				var $line = $head.find ('table.sub-table:first tr:last');
				$line.append ($('<td></td>').append (i18n.last_modified).attr ('field', 'modified'));
				append_last_modified_line = true;
			}
			$last_empty_cell.append (i18n.last_modified).attr ('field', 'modified')

			// Handle heading click to reorder table
			$head.find ('td').click (function () {
				sortTable.call (that, $(this).attr ('field'), ($(this).find ('div.sort-order').hasClass ('sort-desc')) ? true : false);
			});

			var $body = $('<tbody></tbody>').appendTo ($table);
			var line_count = template.getListOrder ().length;
			$.each (this.notices, function (index, notice) {
				if (line_count > 1) {
					$('<tr class="spacer"></tr>').addClass ('diderot-notice-access-' + notice.attr.access).appendTo ($body).append ($('<td></td>').attr ('colspan', col_count + 1));
				}
				var $line = $('<tr></tr>').addClass ('diderot-notice-access-' + notice.attr.access).appendTo ($body);
				$('<a class="view-notice"></a>').append (i18n.view_notice).attr ('href', diderot.generateNavigationHash ('notice', notice.attr.rowid)).appendTo ($('<td class="normal-cell"></td>').appendTo ($line));
				$.each (template.getListOrder (), function (index, line) {
					if (index > 0) {
						$line = $('<tr></tr>').addClass ('diderot-notice-access-' + notice.attr.access).appendTo ($body);
						$('<td></td>').appendTo ($line);
					}
					var last_index = 0;
					$.each (line, function (ind, field) {
						last_index = ind;
						if ((index === 0) && (ind === 0)) {
							// First field of table, convert it to a link
							$('<td></td>').append ($('<a></a>').attr ('href', diderot.generateNavigationHash ('notice', notice.attr.rowid)).append (notice.getValue (field))).appendTo ($line);
						}
						else {
							$('<td></td>').append ($('<pre></pre>').append (notice.getValue (field))).appendTo ($line);
						}
					});
					if (index !== (line_count - 1)) {
						$line.addClass ('sub-line');
					}

					// If on last line, append last modified
					if (index === (line_count - 1)) {
						// Convert date to user timezone
						var d = new Date ();
						d.parseMiogaDate (notice.attr.modified);
						var modified = d.formatMioga (true);

						// Place date into correct table cell
						if (last_index < (col_count - 1)) {
							// Empty cell available on line, add information
							$('<td></td>').append (modified).appendTo ($line);
							last_index++;
						}
						else if (append_last_modified_cell) {
							$('<td></td>').append (modified).appendTo ($line);
						}
						else if (append_last_modified_line) {
							$body.find ('tr:last').addClass ('sub-line');
							$line = $('<tr></td>').appendTo ($body);
							$line.append ($('<td></td>'));
							$('<td></td>').append (modified).appendTo ($line);
							for (i=1; i<(col_count); i++) {
								$('<td></td>').appendTo ($line);
							}
						}
						else {
							$line.find ('td:last').append (modified);
						}
					}

					// Add empty cells so that current line has "col_count" columns
					if (last_index < (col_count - 1)) {
						for (i=last_index; i<(col_count - 1); i++) {
							$('<td></td>').appendTo ($line);
						}
					}
				});
			});

			$(this.elem).empty ().append ($table);
		}


		// --------------------------------------------------------
		// sortTable
		// Sort table according to field and re-draws it
		// --------------------------------------------------------
		function sortTable (field, asc) {
			var template = this.notices[0].attr.template;

			// Default field is first field of first line
			if (field === undefined) {
				field = template.getListOrder ()[0][0];
			}

			// Set sort order
			var reverse = (asc) ? -1 : 1;

			// Comparator function
			var noticeSortComparator;
			if (field !== 'modified') {
				noticeSortComparator = function (a, b) {
					var value_a = a.getValue (field);
					var value_b = b.getValue (field);
					if (value_a === undefined) {
						value_a = '';
					}
					if (value_b === undefined) {
						value_b = '';
					}
					if (value_a.toString ().toLowerCase () < value_b.toString ().toLowerCase ()) return (-1 * reverse);
					if (value_a.toString ().toLowerCase () > value_b.toString ().toLowerCase ()) return (1 * reverse);
					return (0);
				};
			}
			else {
				noticeSortComparator = function (a, b) {
					if (a.attr.modified < b.attr.modified) return (-1 * reverse);
					if (a.attr.modified > b.attr.modified) return (1 * reverse);
					return (0);
				};
			}
			this.notices.sort (noticeSortComparator);
			drawTable.call (this);
			$('<div class="sort-order"></div>').appendTo ($(this.elem).find ('td[field=' + field + ']')).addClass (asc ? 'sort-asc' : 'sort-desc');
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DiderotNoticeList', 1, '[new] Entering');
		var that = this; // For callback reference to current object
		this.elem       = elem;

		var i18n = options.i18n;

		diderot = options.diderot;
		this.notices = options.notices;

		MIOGA.logDebug ('DiderotNoticeList', 1, '[new] Notices: ', this.notices);

		// Draw notice table or display a message
		if ((this.notices !== undefined) && this.notices.length) {
			sortTable.call (this);
		}
		else {
			$(elem).empty ().append ($('<p class="note"></p>').append (i18n.no_notice_to_display));
		}

		MIOGA.logDebug ('DiderotNoticeList', 1, '[new] Leaving');
	}
