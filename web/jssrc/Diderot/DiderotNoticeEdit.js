	// ========================================================================================================
	// DiderotNoticeEdit object, notice editor (or viewer) UI
	// ========================================================================================================
	function DiderotNoticeEdit (elem, options) {
		var defaults = {
		};

		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};


		// --------------------------------------------------------
		// refresh
		// Fill UI with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data, args) {
			MIOGA.logDebug ('DiderotNoticeEdit', 1, '[refresh] Entering, data: ', data);
			MIOGA.logDebug ('DiderotNoticeEdit', 1, '[refresh] args: ', args);

			var that = this;
			this.notice = data;
			this.fields = { };

			// Adapt UI to user access rights
			if (this.notice.attr.access === diderot.CONSTANT.ACCESS.WRITE) {
				this.$edit_btn.show ();
			}
			else {
				this.$edit_btn.hide ();
			}

			var mode = (args !== undefined) ? args.mode : 'view';

			// If notice is empty (new notice), automatically switch to edit args
			if (this.notice.attr.rowid === undefined) {
				mode = 'edit';
			}

			// Add notice fields
			if ((mode === undefined) || (mode === 'view')) {
				this.$form.hide ();
				this.$notice_edit.hide ();
				this.$notice_view.empty ().show ();

				// Show / hide buttons
				this.$actions_list.show ();
				this.$button_list.hide ();

				// Display notice fields
				$.each (this.notice.attr.template.attr.fields, function (index, field) {
					var $line = $('<tr></td>').appendTo (that.$notice_view);
					$('<th></th>').append (field['field-name']).appendTo ($line);
					$('<td></td>').append (that.notice.getValue (field['field-id'])).appendTo ($line);
				});

				// Display linked files
				$('<tr><td colspan=2><hr/></td></tr>').appendTo (this.$notice_view);
				var $line = $('<tr></tr>').appendTo (this.$notice_view);
				$('<th></th>').append (i18n.associated_documents).appendTo ($line);
				var $cell = $('<td></td>').appendTo ($line);
				var $list = $('<ul class="files"></ul>').appendTo ($cell);
				$.each (this.notice.attr.files, function (index, file) {
					if (file !== null) {
						var relpath = file.replace (mioga_context.private_dav_uri + '/', '');
						var path = file.replace (/\/[^\/]*$/, '');
						var $elem = $('<li></li>').append (relpath).appendTo ($list);
						$('<a class="download"></a>').attr ('href', file).append (i18n.open_file).appendTo ($elem);
						$('<a class="download"></a>').attr ('href', 'DownloadFile?entries=' + file + '&path=' + path).append (i18n.download_file).appendTo ($elem);
					}
					else {
						$('<li class="broken-link"></li>').append (i18n.broken_link).appendTo ($list);
					}
				});
				if (this.notice.attr.hidden_files) {
					$('<p class="note"></p>').append (i18n.hidden_files).appendTo ($cell);
				}
			}
			else {
				this.$form.show ();
				this.$notice_view.hide ();
				this.$notice_edit.empty ().show ();

				// Show / hide buttons
				this.$actions_list.hide ();
				this.$button_list.show ();

				// Display notice fields
				var $notice_fields = $('<form class="notice-fields"></form>').appendTo (that.$notice_edit);
				$.each (this.notice.attr.template.attr.fields, function (index, field) {
					var $cont = $('<div class="form-item"></div>').appendTo ($notice_fields);
					$('<label></label>').attr ('for', field['field-id']).append (field['field-name']).appendTo ($cont);
					var $node;
					switch (field['field-type']) {
						case 'string':
							$node = $('<input type="text"/>').attr ('name', field['field-id']);
							break;
						case 'text':
							$node = $('<textarea/>').attr ('name', field['field-id']);
							break;
						case 'value':
						case 'values':
							$node = $('<select></select>').attr ('name', field['field-id']);
							if (field['field-type'] === 'values') {
								$node.attr ('multiple', true);
							}
							else {
								// Single-value, add an empty entry
								$('<option></option>').appendTo ($node);
							}
							$.each (field.values, function (index, value) {
								$('<option></option>').attr ('value', value['field-id']).append (value['field-name']).appendTo ($node);
							});
							break;
					}
					$node.val (that.notice.attr[field['field-id']]).appendTo ($cont);
					if (field.description) {
						$node.attr ('title', field.description);
						$('<div class="info"></div>').appendTo ($cont).attr ('title', field.description);
					}
					that.fields[field['field-id']] = $cont;
				});


				// Linked files
				$notice_fields.append ('<hr/>');
				var $cont = $('<div class="associated-documents-container"></div>').appendTo ($notice_fields);
				$('<h3></h3>').append (i18n.associated_documents).appendTo ($cont);
				var $list = $('<ul class="files"></ul>').appendTo ($cont);

				var $fileselector = $('<div class="file-picker"></div>').appendTo ($cont).fileselector ({
					single: false,
					root: mioga_context.private_dav_uri,
					selectable_file: true,
					selectable_folder: false,
					select_change: function (data) {
						if ($.grep ($('ul.files:visible input'), function (elem) { return ($(elem).val () === data.fullpath); }).length === 0) {
							var $entry = $('<li></li>').append (data.relpath).append ($('<input name="files" type="hidden"/>').val (data.fullpath)).appendTo ($list);
							var $delete_icon = $('<div class="item-delete"></div>').appendTo ($entry).click (function () {
								$entry.remove ();
							});
							$entry.hover (function () {
								$delete_icon.toggle ();
							})
						}
					}
				}).hide ();

				// Buttons opening and closing fileselector
				var $btn_container = $('<ul class="button_list"></ul>').appendTo ($cont);
				var $open_btn = $('<button class="button"></button>').append (i18n.add_files).appendTo ($('<li></li>').appendTo ($btn_container));
				var $close_btn = $('<button class="button cancel"></button>').append (i18n.close_filepicker).appendTo ($('<li></li>').appendTo ($btn_container)).hide ();
				$open_btn.click (function () {
					$open_btn.hide ();
					$close_btn.show ();
					$fileselector.show ();
					return (false);
				});
				$close_btn.click (function () {
					$open_btn.show ();
					$close_btn.hide ();
					$fileselector.hide ();
					return (false);
				});

				// Append files
				if (that.notice.attr.files && that.notice.attr.files.length) {
					$.each (that.notice.attr.files, function (index, file) {
						if (file !== null) {
							var relpath = file.replace (mioga_context.private_dav_uri + '/', '');
							var $entry = $('<li></li>').append (relpath).append ($('<input name="files" type="hidden"/>').val (file)).appendTo ($list);
							var $delete_icon = $('<div class="item-delete"></div>').appendTo ($entry).click (function () {
								$entry.remove ();
							});
							$entry.hover (function () {
								$delete_icon.toggle ();
							});
						}
						else {
							$('<li class="broken-link"></li>').append (i18n.broken_link).appendTo ($list);
						}
					});
				}

				if ((args.files !== undefined) && args.files.length) {
					$.each (args.files, function (index, file) {
					var relpath = file.replace (mioga_context.private_dav_uri + '/', '');
						var $entry = $('<li></li>').append (relpath).append ($('<input name="files" type="hidden"/>').val (file)).appendTo ($list);
						var $delete_icon = $('<div class="item-delete"></div>').appendTo ($entry).click (function () {
							$entry.remove ();
						});
						$entry.hover (function () {
							$delete_icon.toggle ();
						})
					});
				}
			}

			MIOGA.logDebug ('DiderotNoticeEdit', 1, '[refresh] Leaving');
		};


		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// Store notice
		// --------------------------------------------------------
		function storeNotice () {
			MIOGA.logDebug ('DiderotNoticeEdit', 1, '[storeNotice] Entering');
			var that = this;

			// Get notice attributes
			that.notice.attr.files = [ ];
			$.each (this.$notice_edit.find ('.notice-fields').serializeArray (), function (index, attribute) {
				if (attribute.name !== 'files') {
					that.notice.attr[attribute.name] = attribute.value;
				}
				else {
					that.notice.attr.files.push (attribute.value);
				}
			});

			// Store notice
			var res = this.notice.store ();
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.notice_store_success;
				that.$message_box.inlineMessage ('info', message);
				var notice = new DiderotNotice (res.notice);
				if (this.notice.attr.rowid !== undefined) {
					this.refresh (notice);
				}
				else {
					window.location.hash = diderot.generateNavigationHash ('notice', notice.attr.rowid);
				}
			}
			else {
				displayError.call (this, res);
			}

			MIOGA.logDebug ('DiderotNoticeEdit', 1, '[storeNotice] Leaving');
			return (false);
		}


		// --------------------------------------------------------
		// displayError
		// display error message from WebService response
		// --------------------------------------------------------
		function displayError (res) {
			that.$message_box.inlineMessage ('error', res.message);
			$('span.error-message').remove ();
			$.each (res.errors, function (index, error) {
				MIOGA.logDebug ('DiderotNoticeEdit', 1, '[displayError] Server returned error for field ' + error.field);
				that.fields[error.field].append ('<span class="error-message">' + error.message + '</span>');
			});
		}


		// --------------------------------------------------------
		// createFieldID
		// Create a unique ID for internal reference to field
		// --------------------------------------------------------
		function createFieldID () {
			var newDate = new Date;
			return newDate.getTime();
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DiderotNoticeEdit', 1, '[new] Entering');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };

		var i18n = options.i18n;

		var diderot = options.diderot;
		this.parent = options.parent;
		this.options =  $.extend (true, {}, defaults, options);

		// Message box
		this.$message_box = this.parent.$message_box;

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="diderot-notice"></div>').appendTo (this.elem);

		// Actions list
		this.$actions_list = $('<ul class="actions hmenu"></ul>').appendTo (this.$cont);
		this.$button_list = $('<ul class="button_list"></ul>').appendTo (this.$cont).hide ();

		// View and edit containers
		this.$notice_view = $('<table class="notice-view"></table>').appendTo (this.$cont).hide ();
		this.$form = $('<form class="form notice-edit"></form>').appendTo (this.$cont);
		this.$notice_edit = $('<fieldset></fieldset>').appendTo (this.$form).hide ();

		// Actions
		this.$edit_btn = $('<li></li>').appendTo (this.$actions_list).append ($('<a href="#"></a>').append (i18n.edit_notice_btn).click (function () {
			that.refresh (that.notice, {mode: 'edit'});
			return (false);
		}));
		if (mioga_context.rights.Animation) {
			// Access-rights management
			$('<a href="#"></a>').append (i18n.manage_access_rights).appendTo ($('<li></li>').appendTo (this.$actions_list)).click (function () {
				var notice_name = that.notice.attr[that.notice.attr.template.attr.order[0][0]];
				var title = i18n.access_rights.replace (/\$name\$/, notice_name);
				var $dialog_contents = $('<div></div>').attr ('title', title);
				var $access_rights = $('<div></div>').appendTo ($dialog_contents);

				// Dialog navigation buttons
				var $dialog_btns = $('<ul class="button_list dialog-buttons"></ul>').appendTo ($dialog_contents);
				var $close_btn = $('<button class="button cancel"></button>').append (i18n.close).appendTo ($('<li></li>').appendTo ($dialog_btns));
				$close_btn.click (function () {
					$access_rights.parents ('.ui-dialog').find (".ui-dialog-titlebar-close").trigger('click');
					return (false);
				});

				new DiderotAccessRightsEdit ($access_rights, {
					i18n: i18n,
					diderot: diderot,
					data: {
						type: 'notice',
						rowid: that.notice.attr.rowid
					}
				});
				$dialog_contents.dialog ({
					modal: true,
					resizable: false,
					width: 'auto',
					dialogClass: 'diderot-dialog'
				});
				return (false);
			});
		}

		// Buttons
		$('<li></li>').appendTo (this.$button_list).append ($('<button class="button"></button>').append (i18n.store_notice).click (function () {
			storeNotice.call (that);
		}));
		$('<li></li>').appendTo (this.$button_list).append ($('<button class="button cancel"></button>').append (i18n.cancel_store_notice).click (function () {
			if (confirm (i18n.confirm_cancel)) {
				that.refresh (that.notice, 'view');
			}
		}));

		// --------------------------------------------------------
		// Fill UI with data, if any
		// --------------------------------------------------------
		if (options.data) {
			notice = new DiderotNotice (options.data);
			this.refresh (notice);
		}


		MIOGA.logDebug ('DiderotNoticeEdit', 1, '[new] Leaving');
	}
