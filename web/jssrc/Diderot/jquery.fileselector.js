(function( $ ){
	/* 			Plugin fileselector
	 * 
	 * Copyright 2012, (http://alixen.fr)
	 * 
	 * This plugin retrieves and draws content of directory by webdav request.
	 * The user can navigates in the file tree and selects one or more element from current folder.
	 * He can validates the selection or and activates the close action.
	 * 
	 * 
	 * DEPENDS:
	 * 		jquery.ba-resize.js
	 * 		jquery.dav.js
	 * 		xml2json.js
	 * 
	 * 
	 * OPTIONS:
	 * 		{
	 * 			single			: true,
	 * 			selectable_file : true, determine if file elements are selectable or not.
	 * 			selectable_folder: true, determine if folder elements are selectable or not.
	 * 			selected		: false (selected element already exists or not. If selected is true, the selected element will be selected )
	 *			root 			: undefined,
	 *			current 		: "",
	 *			i18n : {
	 *				add_btn 	: "Add",
	 *				close_btn	: "Close",
	 *				select_all	: "Select all",
	 *				deselect_all: "Deselect all"
	 *			},
	 *			******* optionals arguments *******
	 *			select_add		: CB called when the user clicks on the submit button. arguments are array of JSON structure and array of DOM element, like : 
	 *
	 * 								[
	 * 									{
	 * 										fullpath : string,
	 * 										relpath : string,
	 * 										name : string,
	 * 										mimetype : string,
	 * 										current : string,
	 * 										selected : boolean
	 * 									},
	 * 									{
	 * 										...
	 * 									}
	 * 								],
	 * 								[
	 * 									this1,
	 * 									this2,
	 * 									...
	 * 								]
	 *								 
	 *			select_change	: CB called for each selection or deselection. The arguments are JSON structure of data and DOM element like:
	 *  							{
	 * 									fullpath : string,
	 * 									relpath : string,
	 * 									name : string,
	 * 									mimetype : string,
	 * 									current : string,
	 * 									selected : boolean
	 * 								},
	 * 								this
	 * 
	 * 
	 *			select_close	: CB called when the user clicks on the close button.
	 *
	 *			change_directory: CB called when the user clicks on a directory link. The argument is the fullpath as string
	 *		}
	 * 
	 * USAGE:
	 * 
	 * $('#id-cont').fileselector(options);
	 * 
	 * A public method can be called for retrieve the current selection :
	 *  
	 * var current_selection = $('#id-cont').fileselector('getSelection');
	 * 
	 * It returns an array contains tow arrays contains data of each element as JSON structure and DOM element of each element like : 
	 * 
	 *   [
	 * 		[
	 * 			{
	 * 				fullpath : string,
	 * 				relpath : string,
	 * 				name : string,
	 * 				mimetype : string,
	 * 				current : string,
	 * 				selected : boolean
	 * 			},
	 * 			{
	 * 				...
	 * 			}
	 * 		],
	 * 
	 * 		[
	 *			this1,
	 *			this2,
	 *			...
	 * 		]
	 *   ]
	 * 
	 * WARNING : 
	 * 
	 * It must be defined just css background for mimestype icon. For example : 
	 * 
	 * if mimetype = "text/plain", adjust CSS class like : 
	 * 
	 *  .mioga2-fileselector-mimetype-text\/plain {
	 *		background: url("../images/16x16/mimetypes/application-vnd.oasis.opendocument.text.png") no-repeat scroll left;
	 *	}
	 * 
	 */

	var defaults = {
		root 	: undefined,
		current	: "",
		single  : true,
		selectable_file : true,
		selectable_folder : true,
		selected : false,
		i18n	: {
				add_btn 	: "Add",
				close_btn	: "Close",
				select_all	: "Select all",
				deselect_all: "Deselect all"
		},
		select_add		: undefined,
		select_change	: undefined,
		select_close	: undefined,
		change_directory: undefined
	};
	// ==================================================
	// ========= privates methods =======================
	// ==================================================
	
	// Function that actives or desactives a controls buttons according to status of data list
	function activateBtn ($this) {
		if ($this.find('.mioga2-fileselector-item').length === 0) {
			$this.find('.mioga2-fileselector-select_all, .mioga2-fileselector-deselect_all').attr('disabled', true).removeClass('disabled').addClass('disabled');
		}
		else {
			$this.find('.mioga2-fileselector-select_all, .mioga2-fileselector-deselect_all').removeAttr('disabled').removeClass('disabled');
		}
		if ($this.find('.mioga2-fileselector-selected').length === 0) {
			$this.find('.mioga2-fileselector-close-btn').removeAttr('disabled').removeClass('disabled');
			$this.find('.mioga2-fileselector-add-btn').attr('disabled', true).removeClass('disabled').addClass('disabled');
		}
		else {
			$this.find('.mioga2-fileselector-close-btn').attr('disabled', true).removeClass('disabled').addClass('disabled');
			$this.find('.mioga2-fileselector-add-btn').removeAttr('disabled').removeClass('disabled');
		}
	}
	// Function that retrieves content of folder by webdav request.
	// It draws the list of elements. 
	function refreshList ($this, root, current, selected, change_directory, selectable_file, selectable_folder, selected_elem) {
		var pathDav = root + "/";
		if (current !== "") {
			pathDav = pathDav + current + "/";
		}
		jQuery.Dav(pathDav).readFolder({
			async: false,
			success: function(dat, stat,response) {
				refreshDirectory($this, root, current);
				if (change_directory instanceof Function) {
					change_directory(pathDav);
				}
				var $list = $this.find('.mioga2-fileselector-list');
				$list.children().remove();
				
				var json_tmp 	= $.xml2json(dat);
				var str_tmp 	= JSON.stringify(json_tmp);
				str_tmp 		= str_tmp.replace(/Mioga:/g, "").replace(/D:/g, "");
				var t 			= $.parseJSON(str_tmp);
				var arrayFolder = [];
				var arrayFile = [];
				var selectableClass = "";
				$.each(t.response, function (ind, elem) {
					if (elem.propstat) {
						if (elem.propstat.prop) {
							if (elem.propstat.prop.mimetype) {
								if (elem.propstat.prop.mimetype === "directory") {
									arrayFolder.push(elem);
								}
								else {
									arrayFile.push(elem);
								}
							}
						}
					} 
				});
				$.each(arrayFolder, function (i,e) {
					var name 	 = e.href;
					var mimetype = e.propstat.prop.mimetype;
					if (name.charAt(name.length-1) === "/") {
						name = name.substr(0, name.length-1);
					}
					name = decodeURIComponent (name.substr(name.lastIndexOf('/')+1));
					
					if (selectable_folder === true) {
						selectableClass = "mioga2-fileselector-selectable";
					}
					$('<li class="mioga2-fileselector-item ' + selectableClass + '"><span class="mioga2-fileselector-mimetype-' + mimetype + '"></span><span href="#" class="mioga2-fileselector-item-name mioga2-fileselector-dir-link">' + name + '</a></li>')
						.data({
							fullpath	: cleanupPath (root + '/' + current + '/' + name),
							relpath		: cleanupPath (current + '/' + name, true),
							name 		: name,
							mimetype 	: mimetype,
							current 	: current
						})
						.appendTo($list);
				});
				$.each(arrayFile, function (i,e) {
					var name 	 = decodeURIComponent (e.href.substring(e.href.lastIndexOf('/') + 1));
					var mimetype = e.propstat.prop.mimetype;
					if (selectable_file === true) {
						selectableClass = "mioga2-fileselector-selectable";
					}
					$('<li class="mioga2-fileselector-item ' + selectableClass + '"><span class="mioga2-fileselector-mimetype-' + mimetype + '"></span><span class="mioga2-fileselector-item-name">' + name + '</span></li>')
						.data({
							fullpath	: cleanupPath (root + '/' + current + '/' + name),
							relpath		: cleanupPath (current + '/' + name, true),
							name 		: name,
							mimetype 	: mimetype,
							current 	: current
						})
						.appendTo($list);
				});
				if (selected_elem !== undefined && selected === true) {
					$.each($list.find('li'), function (i,e) {
						if ($(e).find('.mioga2-fileselector-item-name').text() === selected_elem ) {
							$(e).trigger('click');
						}
					});
				}
				activateBtn($this);
			},
			error : function () {
				if (current != "") {
					current = "";
					alert('Directory not found. Go at home. If this problem is persistent, contact your Mioga2 administrator.');
					refreshList ($this, root, current, selected, change_directory, selectable_file, selectable_folder);
				}
				else {
					alert('bad directory !');
				}
			}
		});
	}
	// Function thats redraws block of directory links
	function refreshDirectory ($this,root, current) {
		var $cont_dir = $this.find('.mioga2-fileselector-directory p');
		$cont_dir.children().remove();
		var arrayCurrent = [];
		var current_root = root.substr(0, root.length-2);
		current_root = root.substr(root.lastIndexOf('/'));
		var dir_w = 0;
		if (current === "" ) {
			$cont_dir.append('<span>/</span><span>' + current_root.substr(1) + '</span>');
		}
		else {
			$cont_dir.append('<span>/</span><a href="#" class="mioga2-fileselector-dir-link">' + current_root.substr(1) + '</a>');
		}
		if (current !== "") {
			arrayCurrent = current.split('/');
		}
		$.each(arrayCurrent, function (i,e) {
			if (arrayCurrent.length > 1 && i < arrayCurrent.length-1) {
				$cont_dir.append('<span>/</span><a href="#" class="mioga2-fileselector-dir-link">' + e + '</a>');
			}
			else {
				$cont_dir.append('<span>/</span><span>' + e + '</span>');
			}
		});
		$cont_dir.parent().data({
			'fileselector_current': current,
			'fileselector_root' : root
		});
		$.each($cont_dir.children(), function (i,e) {
			dir_w += parseInt($(e).outerWidth(true));
		});
		
		if (dir_w > parseInt($this.find('.mioga2-fileselector-directory-cont').outerWidth(true)) - parseInt($this.find('.mioga2-fileselector-home-icon').outerWidth(true)) ) {
			$this.find('.mioga2-fileselector-directory p').css('direction', 'rtl');
		}
		else {
			$this.find('.mioga2-fileselector-directory p').css('direction', 'ltr');
		}
	}
	// Function that resizes all necessaries elements according to master container dimensions
	function drawResize($this) {
		var this_h = parseInt($this.height());
		var this_w = parseInt($this.width());
		
		var $cont  = $this.find('.mioga2-fileselector-cont');
		var cont_h = this_h - parseInt($cont.css('margin-top')) - parseInt($cont.css('margin-bottom'));
		var cont_w = this_w - parseInt($cont.css('margin-left')) - parseInt($cont.css('margin-right'));
		$cont.height(cont_h + "px");
		$cont.width(cont_w + "px");
		var cont_dir_h  = parseInt($cont.find('.mioga2-fileselector-directory-cont').height()) + parseInt($cont.find('.mioga2-fileselector-directory-cont').css('padding-bottom'))  + parseInt($cont.find('.mioga2-fileselector-directory-cont').css('padding-top'));
		var cont_win_h = cont_h - cont_dir_h;
		var dir_w = this_w - parseInt($cont.css('margin-left')) - parseInt($cont.css('margin-right')) - parseInt($cont.find('.mioga2-fileselector-home-icon').outerWidth(true))-4 ;
		$cont.find('.mioga2-fileselector-directory').width(dir_w + 'px');
		$cont.find('.mioga2-fileselector-window-cont').height(cont_win_h + "px");
		
		var link_cont_h = 0;
		if ($this.find('.mioga2-fileselector-link-cont:visible').length) {
			link_cont_h = $this.find('.mioga2-fileselector-link-cont').outerHeight(true);
		}
		var action_cont_h = 0;
		if ($this.find('.mioga2-fileselector-actions-cont:visible').length) {
			action_cont_h = parseInt($this.find('.mioga2-fileselector-actions-cont').outerHeight(true));
		}
		$this.find('.mioga2-fileselector-list-cont').height(cont_win_h - link_cont_h - action_cont_h + "px");
	}
	// Function that makes an array of selected elements and return this.
	function retrieveSelection ($this) {
		var root = $this.find('.mioga2-fileselector-directory').data('fileselector_root');
    	var current_list = new Array();
		var node_list = new Array ();
    	$.each($this.find('.mioga2-fileselector-selected'), function (i,e) {
    		var current = $(e).data('current');
			var name = $(e).data('name');
    		current_list.push({
				fullpath	: cleanupPath (root + '/' + current + '/' + name),
				relpath		: cleanupPath (current + '/' + name, true),
				current		: current,
				selected	: true,
    			mimetype 	: $(e).data('mimetype'),
    			name 		: name
    		});
			node_list.push (e);
    	});
    	return ([current_list, node_list]);
	}
	// Function that cleanup string path: remove double slash character and optionaly remove first slash if exists.
	function cleanupPath (path, remove_leading_slash) {
		if (remove_leading_slash) {
			path = path.replace (/^\//, '');
		}
		return (path.replace (/\/\//g, '/'));
	}
	// ========================================================
	// Methods declaration
	// ========================================================
	var methods = {
		init : function(args) {
			var base_obj = $.extend({}, defaults, args);
			return $(this).each(function() {
				var $this = $(this);
				function onResize () {
					drawResize($this);
				}
				if (base_obj.root !== undefined) {
					//format root
					if (base_obj.root.charAt(0) === "/") {
						base_obj.root.substr(1,base_obj.root.length-1);
					}
					if (base_obj.root.charAt(base_obj.root.length-1) === "/") {
						base_obj.root = base_obj.root.substr(0,base_obj.root.length-2);
					}
					// format current
					if (base_obj.current.charAt(0) === "/") {
						base_obj.current = base_obj.current.substr(1,base_obj.current.length-1);
					}
					if (base_obj.current.charAt(base_obj.current.length-1) === "/") {
						base_obj.current = base_obj.current.substr(0,base_obj.current.length-2);
					}
					
					var selected_elem = undefined;
					
					if (base_obj.current !== "" && base_obj.selected === true) {
						if (base_obj.current.indexOf('/') === -1) {
							selected_elem = base_obj.current;
							base_obj.current = "";
						}
						else {
							selected_elem = base_obj.current.substr(base_obj.current.lastIndexOf('/')+1);
							base_obj.current = base_obj.current.substr(0, base_obj.current.lastIndexOf('/'));
						}
					}
					$this.data("fileselector-params", base_obj);
					var $f_manager = $('<div class="mioga2-fileselector-cont"></div>').append( 
						'<div class="mioga2-fileselector-directory-cont">' + 
							'<span class="mioga2-fileselector-home-icon"></span>' + 
							'<div class="mioga2-fileselector-directory"><p></p></div>' + 
						'</div>' +
						'<div class="mioga2-fileselector-window-cont">' + 
							'<div class="mioga2-fileselector-list-cont"><ul class="mioga2-fileselector-list"></ul></div>' + 
							'<div class="mioga2-fileselector-actions-cont">' + 
								'<input type="button" class="button mioga2-fileselector-add-btn" value="' + base_obj.i18n.add_btn + '"/>' + 
								'<input type="button" class="button mioga2-fileselector-close-btn cancel" value="' + base_obj.i18n.close_btn + '"/>' + 
							'</div>' + 
						'</div>'
					);
					var $glob_selection = $(
						'<div class="mioga2-fileselector-link-cont">' + 
							'<input type="button" class="button mioga2-fileselector-select-all" value="' + base_obj.i18n.select_all   + '"/>' +  
							'<input type="button" class="button mioga2-fileselector-deselect-all" value="' + base_obj.i18n.deselect_all + '"/>' +
						'</div>'
					);
					if (base_obj.single === false) {
						$f_manager.find('.mioga2-fileselector-window-cont').prepend($glob_selection);
						// behavior to select or deselect all items
				        $this.find('.mioga2-fileselector-select-all').live('click',function () {
				        	$this.find('li').removeClass('mioga2-fileselector-selected').addClass('mioga2-fileselector-selected');
				        	activateBtn($this);
				        });
				        $this.find('.mioga2-fileselector-deselect-all').live('click',function () {
				        	$this.find('li').removeClass('mioga2-fileselector-selected');
				        	activateBtn($this);
				        });
					}
					$f_manager.appendTo($this);
					
					// behavior to go at home
					$this.find('.mioga2-fileselector-home-icon').live('click', function (ev) {
						refreshList($this, base_obj.root, "", base_obj.selected, base_obj.change_directory, base_obj.selectable_file, base_obj.selectable_folder);
					});
					// behavior to click on directory link
					$this.find('.mioga2-fileselector-directory').find('.mioga2-fileselector-dir-link').live('click', function (ev){
						var elemIndex = $this.find('.mioga2-fileselector-directory').find('.mioga2-fileselector-dir-link').index($(this));
						var root = $this.find('.mioga2-fileselector-directory').data('fileselector_root');
						var current = "";
						$.each($this.find('.mioga2-fileselector-directory').find('.mioga2-fileselector-dir-link'), function (i,e) {
							if (i > 0 && i <= elemIndex) {
								if (current === "") {
									current = current + $(this).text();
								}
								else {
									current = current + "/" + $(this).text();
								}
							}
						});
						refreshList($this, root, current, base_obj.selected, base_obj.change_directory, base_obj.selectable_file, base_obj.selectable_folder);
						return (false);
					});
					// behavior to click on link folder in list
					$this.find('.mioga2-fileselector-list .mioga2-fileselector-dir-link').live('click',function (ind,elem) {
						console.log('link clicked');
						var current = $this.find('.mioga2-fileselector-directory').data('fileselector_current');
						if (current !== "") {
							current = current + "/" + $(this).text();
						}
						else {
							current = $(this).text();
						}
						console.log('current : ' + current);
						refreshList($this, base_obj.root, current, base_obj.selected, base_obj.change_directory, base_obj.selectable_file, base_obj.selectable_folder);
						return (false);
					});
					// behavior to select or deselect item
					$this.find('.mioga2-fileselector-list').delegate('li', 'click', function (ev) {
						if ($(ev.target).hasClass('mioga2-fileselector-dir-link') === false ) { // && $(ev.target).hasClass('mioga2-fileselector-item-name') === false
							if ($(this).hasClass('mioga2-fileselector-selectable')) {
								var selected = false;
								if ($(this).hasClass('mioga2-fileselector-selected')) {
									$(this).removeClass('mioga2-fileselector-selected');
								}
								else {
									if (base_obj.single === true) {
										$(this).parent().find('.mioga2-fileselector-selected').removeClass('mioga2-fileselector-selected');
									}
									$(this).addClass('mioga2-fileselector-selected');
									selected = true;
								}
								activateBtn($this);
								if (base_obj.select_change instanceof Function) {
									var data = $(this).data ();
									data.selected = selected;
									base_obj.select_change(data, this);
								}
							}
						}
					});
					// behavior to add click
					$this.find('.mioga2-fileselector-add-btn').click(function (ev) {
						if (base_obj.select_add instanceof Function) {
							var ret = retrieveSelection ($this);
				    		base_obj.select_add(ret[0], ret[1]);
						}
						return (false);
					});
					// behavior to close selection
			        $this.find('.mioga2-fileselector-close-btn').click(function(event) {
						if (base_obj.select_close instanceof Function) {
							base_obj.select_close();
						}
						return (false);
					});
			        refreshList($this, base_obj.root, base_obj.current, base_obj.selected, base_obj.change_directory, base_obj.selectable_file, base_obj.selectable_folder, selected_elem);
					drawResize($this);
					$this.bind('resize', onResize);
				}
				else {
					alert('bad initialization of plugin.');
				}
			});
		},
		getSelection : function () {
			var $this = $(this);
			return retrieveSelection ($this);
		}
	};
	
	$.fn.fileselector = function(method) {
	    var args = arguments;
	    if ( methods[method] ) {
	    	return methods[ method ].apply( this, Array.prototype.slice.call( args, 1 ));
	    }
	    else if ( typeof method === 'object' || ! method ) {
	    	return methods.init.apply( this, args );
	    }
	    else {
	        $.error( 'Method ' +  method + ' does not exist on jQuery.fileselector' );
	    }
	};
})( jQuery );