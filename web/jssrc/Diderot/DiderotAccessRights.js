// ========================================================
// DiderotAccessRights OO-interface to access rights
// ========================================================
function DiderotAccessRights (options) {
	// ========================================================
	// Default values
	// ========================================================
	defaults = {
		type: '',
		rowid: undefined
	},

	// ========================================================
	// Public methods
	// ========================================================

	// --------------------------------------------------------
	// fetch, fetch rights from Mioga2
	// --------------------------------------------------------
	this.fetch = fetch;
	function fetch () {
		MIOGA.logDebug ('DiderotAccessRights', 1, '[fetch] Entering, rowid: ' + this.attr.rowid);
		var that = this;

		var document_attr = {
			type: this.attr.type
		};

		if ((this.attr.rowid !== null) && (this.attr.rowid !== undefined)) {
			document_attr.rowid = this.attr.rowid;
		}

		$.ajax ({
			url: 'GetRights.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			data: document_attr,
			success: function (data) {
				MIOGA.logDebug ('DiderotAccessRights', 3, '[fetch] GetRights.json returned, data: ', data);
				that.attr = data;
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('DiderotAccessRights', 1, '[fetch] Leaving, data: ', this.attr);
	};

	// --------------------------------------------------------
	// store, store rights to Mioga2
	// --------------------------------------------------------
	this.store = store;
	function store () {
		MIOGA.logDebug ('DiderotAccessRights', 1, '[store] Entering');

		// Basic attributes
		var rights_attr = {
			type: this.attr.type,
			inherited: this.attr.inherited
		};

		if (this.attr.rowid !== null) {
			rights_attr.rowid = this.attr.rowid;
		}

		// Flatten profile, user and team rights
		for (i=0; i<this.attr.profiles.length; i++) {
			rights_attr['profile_' + this.attr.profiles[i].rowid] = this.attr.profiles[i].access;
		}
		for (i=0; i<this.attr.teams.length; i++) {
			rights_attr['team_' + this.attr.teams[i].rowid] = this.attr.teams[i].access;
		}
		for (i=0; i<this.attr.users.length; i++) {
			rights_attr['user_' + this.attr.users[i].rowid] = this.attr.users[i].access;
		}

		MIOGA.logDebug ('DiderotAccessRights', 1, '[store] Data to be posted: ', rights_attr);

		// Post data to Mioga2
		var success = false;
		var message = "[DiderotAccessRights::store] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		var rights = { };
		$.ajax ({
			url: 'SetRights.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: rights_attr,
			success: function (data) {
				MIOGA.logDebug ('DiderotAccessRights', 3, '[store] SetRights.json POST returned, data: ', data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.rights) {
					rights = data.rights;
				}
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('DiderotAccessRights', 1, '[store] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, rights: rights});
	};

	// --------------------------------------------------------
	// remove, remove specific access right from Mioga2
	// --------------------------------------------------------
	this.remove = remove;
	function remove (type, rowid) {
		MIOGA.logDebug ('DiderotAccessRights', 1, '[remove] Entering');

		// Basic attributes
		var rights_attr = {
			type: this.attr.type,
			object_type: type,
			object_id: rowid
		};

		if (this.attr.rowid !== null) {
			rights_attr.rowid = this.attr.rowid;
		}

		MIOGA.logDebug ('DiderotAccessRights', 1, '[remove] Data to be posted: ', rights_attr);

		// Post data to Mioga2
		var success = false;
		var message = "[DiderotAccessRights::remove] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		var rights = { };
		$.ajax ({
			url: 'RemoveRight.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: rights_attr,
			success: function (data) {
				MIOGA.logDebug ('DiderotAccessRights', 3, '[remove] SetRights.json POST returned, data: ', data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.rights) {
					rights = data.rights;
				}
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('DiderotAccessRights', 1, '[remove] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, rights: rights});
	};


	// ========================================================
	// Private methods
	// ========================================================


	// ========================================================
	// Constructor
	// ========================================================
	MIOGA.logDebug ('DiderotAccessRights', 1, '[new] Entering, options: ', options);
	var that = this;

	this.attr = $.extend (true, {}, this.defaults, options);

	if ((Object.keys(options).length === 2) && (options.type !== undefined) && (options.rowid !== undefined)) {
		// Only type and rowid given, fetch data from Mioga2
		this.fetch ();
	}
	else if ((Object.keys(options).length === 2) && (options.type === 'category') && (options.rowid === undefined)) {
		// Rights for root category, fetch data from Mioga2
		this.fetch ();
	}
	else {
		// Full access rights structure, no need to fetch
		this.attr = options;
	}

	MIOGA.logDebug ('DiderotAccessRights', 1, '[new] Leaving, data: ', that.attr);
}
