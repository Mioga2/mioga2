// ========================================================
// DiderotNotice OO-interface to notices
// ========================================================
function DiderotNotice (options) {
	// ========================================================
	// Default values
	// ========================================================
	defaults = {
	},

	// ========================================================
	// Public methods
	// ========================================================

	// --------------------------------------------------------
	// fetch, fetch notice from Mioga2
	// --------------------------------------------------------
	this.fetch = fetch;
	function fetch () {
		MIOGA.logDebug ('DiderotNotice', 1, '[fetch] Entering, rowid: ' + this.attr.rowid);
		var that = this;

		// Notice to load
		var notice_attr = {};
		if (this.attr.rowid) {
			notice_attr.rowid = this.attr.rowid;
		}

		$.ajax ({
			url: 'GetNotice.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			data: notice_attr,
			success: function (data) {
				MIOGA.logDebug ('DiderotNotice', 3, '[fetch] GetNotice.json returned, data: ', data);
				that.attr = data;
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		// Expand notice template
		this.attr.template = new DiderotNoticeTemplate (this.attr.template);

		MIOGA.logDebug ('DiderotNotice', 1, '[fetch] Leaving, data: ', this.attr);
	};

	// --------------------------------------------------------
	// store, store notice to Mioga2
	// --------------------------------------------------------
	this.store = store;
	function store () {
		MIOGA.logDebug ('DiderotNotice', 1, '[store] Entering');

		// Basic attributes
		var notice_attr = { };
		$.each (Object.keys (this.attr), function (index, key) {
			if ((key !== 'files') && (key !== 'template') && (that.attr[key] !== '')) {
				notice_attr[key] = that.attr[key];
			}
		});

		// Parse template for field constraints
		var template = this.attr.template;
		notice_attr.mandatory_fields = template.getMandatoryFields ();
		notice_attr.optional_fields = template.getOptionalFields ();

		// Linked files
		notice_attr.files = this.attr.files;

		MIOGA.logDebug ('DiderotNotice', 1, '[store] Data to be posted: ', notice_attr);

		// Post data to Mioga2
		var success = false;
		var message = "[DiderotNotice::store] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		var notice = {rowid: this.attr.rowid};	// For further loading from WebService response
		$.ajax ({
			url: 'SetNotice.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: notice_attr,
			success: function (data) {
				MIOGA.logDebug ('DiderotNotice', 2, '[store] SetNotice.json POST returned, data: ', data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.notice) {
					notice = data.notice;
				}
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('DiderotNotice', 1, '[store] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, notice: notice});
	};

	// --------------------------------------------------------
	// getValue, given a field ID, return its value into notice
	// --------------------------------------------------------
	this.getValue = getValue;
	function getValue (id) {
		// Get field description from template
		var field = this.attr.template.getField (id);

		var value;

		// Notice field may be direct string or internal ID to value
		if ((field !== undefined) && (field.values === undefined)) {
			value = that.attr[field['field-id']];
		}
		else if ((field !== undefined) && (that.attr[field['field-id']] !== undefined)) {
			var value_desc = that.attr.template.getValue (that.attr[field['field-id']]);
			if (value_desc !== undefined) {
				value = value_desc['field-name'];
			}
		}
		else {
			value = '';
		}

		return (value);
	}


	// ========================================================
	// Private methods
	// ========================================================


	// ========================================================
	// Constructor
	// ========================================================
	MIOGA.logDebug ('DiderotNotice', 1, '[new] Entering, options: ', options);
	var that = this;

	this.attr = $.extend (true, {}, this.defaults, options);

	// Fetch notice data from Mioga2 if required
	if ((Object.keys (this.attr).length === 0) || ((Object.keys (this.attr).length === 1) && (this.attr.rowid !== undefined))) {
		// No attribute, or only rowid is provided, fetch from Mioga2
		this.fetch ();
	}
	else {
		// Object attribute is supposed to a complete notice description, no need to fetch data
		this.attr = options;
		this.attr.template = new DiderotNoticeTemplate (this.attr.template);
	}

	MIOGA.logDebug ('DiderotNotice', 1, '[new] Leaving, data: ', this.attr);
}
