	// ========================================================================================================
	// DiderotSearch object, Search form UI
	// ========================================================================================================
	function DiderotSearch (elem, options) {
		var defaults = {
			form_operators: ['and', 'or', 'not'],
			form_constraints: ['contains_any', 'contains_all', 'equals', 'does_not_contain']
		};

		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};

		// --------------------------------------------------------
		// refresh
		// Useless, defined for compatibility with other pages
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data) {
			MIOGA.logDebug ('DiderotSearch', 1, '[refresh] Entering, data: ', data);

			if (data !== undefined && data.mode !== undefined) {
				this.current_mode = data.mode;
			}

			if ((this.current_mode === 'results') || this.current_mode === 'fakesearch') {
				// Some results already available, hide form and show results
				this.$form.hide ();
				this.$results.show ();

				// If showing results of a real search, change hash, otherwise, change mode
				if (this.current_mode === 'results')
					window.location.hash = diderot.generateNavigationHash ('searchresults');
				else
					this.current_mode = 'results';

				if (data.notices && data.success) {
					// Display search results
					new DiderotNoticeList (this.$result_list, {
						i18n: i18n,
						diderot: diderot,
						notices: data.notices
					});

					this.current_mode = 'results';

					// Show results, hide form
					this.$results.show ();
					this.$form.hide ();

					// Inform Diderot that search-results are present
					if (data.notices.length) {
						diderot.has_search_results = true;
					}
				}
				else {
					// Search failed, display error
					that.$message_box.inlineMessage ('error', data.message);
				}
			}
			else {
				// Display search form
				this.notice_template = data.template;

				if (data !== undefined && data.clear === true) {
					that.$form.find ('input, select').val ('');

					// Clear static field list
					this.$select_lists.empty ();
					$($.grep (that.notice_template.attr.fields, function (field) { return ((field['field-type'] === 'value') || (field['field-type'] === 'values')); })).each (function (index, field) {
						$query_block = $('<select class="query"></select>').attr ('name', field['field-id']).attr ('multiple', true);
						$('<option></option>').append (i18n.all_values).attr ('value', '').attr ('selected', true).appendTo ($query_block);
						if ((field.values !== undefined) && (field.values.length)) {
							$.each (field.values, function (index, item) {
								$('<option></option>').attr ('value', item['field-id']).append (item['field-name']).appendTo ($query_block);
							});
						}
						var $fieldset = $('<fieldset></fieldset>').append ($('<legend></legend>').append (field['field-name'])).append ($query_block);
						$('<li></li>').append ($fieldset).appendTo (that.$select_lists);
					});

					// Loop through each 'static_search' field
					that.$list.empty ();
					$($.grep (that.notice_template.attr.fields, function (field) { return (field.static_search === true); })).each (function (index, static_field) {
						var $cont = $('<div class="form-item"></div>').appendTo (that.$list);
						var $select = $('<select class="field"></select>').appendTo ($cont);

						// Allow user to change current field from non-'value*' fields
						$($.grep (that.notice_template.attr.fields, function (field) { return ((field['field-type'] !== 'value') && (field['field-type'] !== 'values')); })).each (function (index, field) {
							var $option = $('<option></option>').attr ('value', field['field-id']).append (field['field-name']).appendTo ($select);
							if (field['field-id'] === static_field['field-id']) {
								$option.attr ('selected', true);
							}
						});

						// Search terms
						var $input = $('<input type="text"/>').appendTo ($cont);

						// Linking operator
						var $select = $('<select class="operator"></select>').appendTo ($cont);
						$.each (that.options.form_operators, function (index, operator) {
							$('<option></option>').attr ('value', operator).append (i18n['operator_' + operator + '_label']).appendTo ($select);
						});
					});

					// Handle ENTER keypress
					this.$form.find ('input,select').on ('keypress', function (event) {
						if (event.keyCode == 13) {
							that.$form.trigger ('submit');
							return (false);
						}
					});

					// Inform Diderot that search-results are no-more present
					diderot.has_search_results = false;
				}

				// Initialize actions menu
				diderot.generateMenu (that.$menu);

				// Show form, hide results
				this.$form.show ();
				this.$results.hide ();

				// Focus first visible input
				that.$form.find ('input:visible:first').trigger ('focus');
			}

			MIOGA.logDebug ('DiderotSearch', 1, '[refresh] Leaving');
		};


		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// Proceed search request
		// --------------------------------------------------------
		function proceedSearch (advanced) {
			MIOGA.logDebug ('DiderotSearch', 1, '[proceedSearch] Entering');

			// Format data for POST
			var form_data = { };

			if (advanced) {
				// Merge dynamic form data and static fields values
				var query = [ ];
				var operator;
				that.$list.find ('div.form-item').each (function () {
					var value = $(this).find ('input').val ();
					if (value !== '') {
						var current = {
							field: $(this).find ('select.field').val (),
							query: value,
							constraint: 'contains_all'
						};

						// Add operator from previous query item
						if (operator !== undefined) {
							// Translate operator 'NOT' to 'AND does_not_contain'
							if (operator === 'not') {
								operator = 'and';
								current.constraint = 'does_not_contain';
							}
							current.operator = operator;
						}

						// Push current query item into list
						query.push (current);

						// Store operator for next query item
						operator = $(this).find ('select.operator').val ();
					}
				});
				var static_fields = $('form.field-list').serializeArray ();
				if (static_fields && static_fields.length) {
					$.each (static_fields, function (index, field) {
						if (field.value !== '') {
							query.push ({
								operator: 'and',
								field: field.name,
								query: field.value
							});
						}
					});
				}

				MIOGA.logDebug ('DiderotSearch', 1, '[proceedSearch] query: ', query);

				// Convert query to JSON string
				form_data.query = JSON.stringify (query);

				// Add plain-text search if needed
				if (this.$advanced_contents_search.val () !== '') {
					form_data.query_string = this.$advanced_contents_search.val ();
					form_data.search_description = 'on';
					form_data.search_tags = 'on';
					form_data.search_text = 'on';
					form_data.operator = operator;
				}
			}
			else {
				// Simple plain-text search
				if (this.$contents_search.val () !== '') {
					form_data.query_string = this.$contents_search.val ();
					form_data.search_description = 'on';
					form_data.search_tags = 'on';
					form_data.search_text = 'on';
				}
			}

			MIOGA.logDebug ('DiderotSearch', 2, '[proceedSearch] Form data: ', form_data);

			// Post search query to Mioga2
			var success = false;
			var message = i18n.search_failed;	// This is a default message to be replaced by message from WebService response
			var errors = [ ];
			var notices = [ ];
			$.ajax ({
				url: 'SearchNotices.json',
				type: 'POST',
				async: false,
				traditional: true,
				dataType: 'json',
				data: form_data,
				success: function (data) {
					MIOGA.logDebug ('DiderotSearch', 3, '[proceedSearch] SearchNotices.json POST returned, data: ', data);
					success = data.success;
					message = data.message;
					errors = data.errors;
					if (data.notices) {
						// Add template as sub-key into each notice
						if (data.notices && data.notices.length) {
							notices = $.map (data.notices, function (notice) { notice.template = data.template; var notice_obj = new DiderotNotice (notice); return (notice_obj); });
						}
					}
				},
				error: function () {
					MIOGA.logError ('Internal error', true);
				}
			});

			MIOGA.logDebug ('DiderotSearch', 1, '[proceedSearch] Leaving, success: ', success);
			return ({success: success, message: message, errors: errors, notices: notices, mode: 'results'});
		};


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DiderotSearch', 1, '[new] Entering');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };

		var i18n = options.i18n;

		var diderot = options.diderot;

		this.options =  $.extend (true, {}, defaults, options);

		this.current_mode = undefined;

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="diderot-search"></div>').appendTo (this.elem);

		this.$message_box = $('<div></div>').inlineMessage ().appendTo (this.$cont);


		// --------------------------------------------------------
		// Menu
		// --------------------------------------------------------
		this.$menu = $('<ul class="hmenu"></ul>').appendTo (this.$cont);


		// --------------------------------------------------------
		// Page contents
		// --------------------------------------------------------
		this.$page_contents = $('<div></div>').appendTo (this.$cont);

		this.$form = $('<form class="form"></form>').submit (function () { that.refresh (proceedSearch.call (that)); return (false);}).appendTo (this.$page_contents);
		var $form = $('<fieldset></fieldset>').appendTo (this.$form);

		// Action buttons container
		var $cont = $('<ul class="button_list form-actions-container"></ul>').appendTo ($form);

		// Form-clear
		$('<button class="button cancel"></button>').append (i18n.clear_form).appendTo ($('<li></li>').appendTo ($cont)).click (function () {
			that.$form.find ('input, select').val ('');
			that.refresh ({template: that.notice_template});
			return (false);
		});

		// Fake empty search (show all notices)
		$('<button class="button normal"></button>').append (i18n.all_notices).appendTo ($('<li></li>').appendTo ($cont)).click (function () {
			that.$form.find ('input, select').val ('');
			that.refresh (proceedSearch.call (that));
			return (false);
		});

		// --------------------------------------------------------
		// Simple search
		// --------------------------------------------------------
		this.$simple_form_container = $('<div class="simple-search"></div>').appendTo ($form);
		$('<h2 class="search-form-type"></h2>').append (i18n.simple_search_label).appendTo (this.$simple_form_container);
		var $block = $('<div class="search-form-block"></div>').appendTo (this.$simple_form_container)
		var $cont = $('<div class="form-item"></div>').appendTo ($block);
		$('<label for="document_contents"></label>').append (i18n.search_terms).appendTo ($cont);
		this.$contents_search = $('<input type="text" name="document_contents"/>').appendTo ($cont).trigger ('focus');
		$('<button class="button"></button>').append (i18n.proceed_search).appendTo ($('<li></li>').appendTo ($('<ul class="button_list"></ul>').appendTo (this.$simple_form_container)));

		// --------------------------------------------------------
		// Advanced search
		// --------------------------------------------------------
		this.$advanced_form_container = $('<div class="advanced-search"></div>').appendTo ($form);
		var $adv_search_toggle = $('<h2 class="search-form-type"></h2>').append (i18n.advanced_search_label).appendTo (this.$advanced_form_container);
		var $adv_search_container = $('<div class="search-form-block"></div>').appendTo (this.$advanced_form_container);
		// Field list
		this.$list = $('<div></div>').appendTo ($adv_search_container);
		var $cont = $('<div class="form-item"></div>').appendTo ($adv_search_container);
		$('<label for="document_contents"></label>').append (i18n.search_terms).appendTo ($cont);
		this.$advanced_contents_search = $('<input type="text" name="document_contents"/>').appendTo ($cont);
		this.$select_lists = $('<ul></ul>').appendTo ($('<form class="field-list"></form>').appendTo ($adv_search_container));
		$('<button name="advanced" class="button"></button>').append (i18n.proceed_search).appendTo ($('<li></li>').appendTo ($('<ul class="button_list"></ul>').appendTo (this.$advanced_form_container))).click (function () {
			that.refresh (proceedSearch.call (that, true));
			return (false);
		});

		// --------------------------------------------------------
		// Search results
		// --------------------------------------------------------
		this.$results = $('<div class="search-results"></div>').appendTo (this.$page_contents);
		$('<button class="button"></button>').append (i18n.modify_search).appendTo ($('<li></li>').appendTo ($('<ul class="button_list"></ul>').appendTo (this.$results))).click (function () {
			window.location.hash = diderot.generateNavigationHash ('search');
			that.current_mode = undefined;
			diderot.has_search_results = false;
			that.$results.hide ();
			that.$form.show ();

			that.$form.find ('input:visible:first').trigger ('focus');
		});
		this.$result_list = $('<div class="diderot-notice-list"></div>').appendTo (this.$results);


		MIOGA.logDebug ('DiderotSearch', 1, '[new] Leaving');
	}
