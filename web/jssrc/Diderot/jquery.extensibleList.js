/*! Plugin jQuery : extensibleList v1 */

/*
 * This plugin creates an extensible and sortable list of data.
 *
 * Data can be single values. In this case, the module should be instanciated as below:
 *
 * 		var $list = $('<div></div>').extensibleList ({values: [1, 2, 3]});
 *
 * Data can be objects. In this case, the module instanciation is more complex as it needs a template for data insertion. The template input elements need to be placed in a container node (for example a span):
 *
 * 		var $template = $('<span><input name="field1"/><select name="field2"><option value="value1">Label 1</option><option value="value2">Label 2</option></select></span>');
 * 		var $list = $('<div></div>').extensibleList ({values: [{field1: 'value1', field2: 'value2'}], template: $template});
 *
 * List data can be retreived by calling 'getValues' method:
 *
 * 		var values = $list.extensibleList ('getValues');
 *
 * The resulting data will be a list of single values or a list of key / value pairs according to how it has be instanciated.
 *
 * Several callbacks can be passed to object when instanciated:
 * 		- updateCB will be triggered when a new entry is added to list,
 * 		- orderCB will be triggered when list order is changed,
 * 		- removeCB will be triggered when an entry is removed.
 *
 * The "updateCB" callback receives two arguments:
 * 		- the extensibleList DOM node,
 * 		- the data structure that was just inserted.
 *
 * The "orderCB" and "removeCB" callbacks receive two arguments:
 * 		- the extensibleList DOM node,
 * 		- the output of 'getValues',
 * 		- the current list-element.
 *
 * Data can be checked before insertion. The check function is passed at instanciation in the "checkCB" key. It receives the data to be inserted as only argument and must return true to permit data insertion or false to prevent insertion.
 *
 * parentDeletable attribute determines if the item of the parent extensible list can be deleted or not.
 * 
 * Formatting data for display (for example when template is composed of a 'select' element) can be achieved by passing a 'labels' key upon instanciation:
 *
 * 		var labels = {
 * 			field1: {
 * 				value1: 'Some text for value 1',
 * 				value2: 'Some text for value 2'
 * 			}
 * 		};
 *
 * can_change contains an array of field names that can be changed once inserted. Those fields can only be simple text values (field change will be handled by '<input type="text"/>' elements).
 *
 */
(function( $ ){

	var defaults = {
		sortable: true,
		can_change: [ ],
		values: [ ],
		template: '<input name="value" type="text"/>',
		labels: { },
		checkCB: function (data) { return (true); },
		parentDeletable : true
	};

	function getValues (elem) {
		var params = $(this).data ('extensibleList');
		var res = new Array();
		$(this).find('ul:first > li').each(function() {
			var item = {};
			$(this).find ('> div.el-field > span').each (function () {
				if (!$(this).hasClass ('el-container')) {	// Ingore sublist containers
					if ($(this).attr ('name') && ($(this).attr ('type') && ($(this).attr ('type') === 'array'))) {
						// Key / array of values value pairs
						if (item[$(this).attr ('name')] === undefined) {
							item[$(this).attr ('name')] = [ ];
						}
						item[$(this).attr ('name')].push ($(this).attr ('value'));
					}
					else if ($(this).attr ('name')) {
						// Key / values value pairs
						item[$(this).attr ('name')] = $(this).attr ('value');
					}
					else {
						// Simple values
						item = $(this).text ();
					}
				}
			});

			// Process sublist, if any
			if ($(this).has ('.extensibleList').length) {
				var $sublist = $(this).find ('.extensibleList:first');
				// Process sublist
				item[$sublist.attr ('name')] = $sublist.extensibleList ('getValues');
			}

			// Append data
			res.push (item);
		});
		return res;
	}

	function callCB (mode, data) {
		var params = $(this).data ('extensibleList');
		if (params) {
			if ((mode === 'insert') && params.updateCB) {
				return (params.updateCB (this, getValues.call (this)));
			}
			else if ((mode === 'order') && params.orderCB) {
				return (params.orderCB (this, getValues.call (this), data));
			}
			else if ((mode === 'remove') && params.removeCB) {
				return (params.removeCB (this, getValues.call (this), data));
			}
		}
		return (true);
	}

	function createEntry (data) {
		console.log ('[extensibleList::createEntry] Data: ');
		console.dir (data);
		var that = this;
		var params = $(this).data ('extensibleList');
		var $entry = $('<li></li>').appendTo ($(this).find ('ul:first'))
			.hover(
				function () {
					$(this).find('> span.item-delete').show()
				},
				function () {
					$(this).find('> span.item-delete').hide();
				}
			);
		if (typeof (data) === 'object') {
			for (var key in data) {
				if (data[key] instanceof Array) {
					var $cont = $('<div></div>').addClass ('el-field').addClass (key).appendTo ($entry);
					for (i=0; i<data[key].length; i++) {
						var label = (params.labels[key] && params.labels[key][data[key][i]]) ? params.labels[key][data[key][i]] : data[key][i];
						$('<span></span>').attr ('name', key).attr ('type', 'array').addClass ('array').attr ('value', data[key][i]).append (label).appendTo ($cont);
					}
				}
				else {
					// Try to find a suitable label for key
					var label = (params.labels[key] && params.labels[key][data[key]]) ? params.labels[key][data[key]] : data[key];
					var $cont = $('<div></div>').addClass ('el-field').addClass (key).appendTo ($entry);
					var $span = $('<span></span>').attr ('name', key).attr ('value', data[key]).append (label).appendTo ($cont);
					if ($.grep (params.can_change, function (field) { return (field === key); }).length > 0) {
						toggleFieldChange.call (that, $span);
					}
				}
			}
		}
		else {
			var $cont = $('<div></div>').addClass ('el-field').addClass (key).appendTo ($entry);
			$('<span></span>').append (data).appendTo ($cont);
		}
		if ($(that).parent().children('div.el-field').length) {
			if (params.parentDeletable === false ) {
				$(that).prev('span').removeClass('item-delete').addClass('item-not-delete');
			}
		}
		$('<span class="item-delete" style="display:none;">&#160;</span>').appendTo($entry)
			.click( function () {
				if (callCB.call (that, 'remove', data) !== false) {
					$(this).parent('li').remove();
					if ($(that).parent().children('div.el-field').length && $(that).children('ul').children('li').length === 0 && params.parentDeletable === false) {
						$(that).prev('span').removeClass('item-not-delete').addClass('item-delete');
					}
				}
			});
	}

	// Add value change capabilities to a field (see "can_change" option)
	function toggleFieldChange ($span) {
		var that = this;

		// Field value can be changed
		$span.addClass ('can-change');

		// Handle element click
		$span.click (function () {
			var $cont = $('<span></span>');
			var $input = $('<input type="text"/>').attr ('name', $span.attr ('name')).val ($span.attr ('value')).appendTo ($cont);
			var $confirm_icon = $('<span class="edit-confirm"></span>').appendTo ($cont);
			var $cancel_icon = $('<span class="edit-cancel"></span>').appendTo ($cont);
			$span.replaceWith ($cont);

			// ENTER keypress callback
			$input.on ('keypress', function (evt) {
				if (evt.keyCode == 13) {
					// User pressed ENTER, save changes
					var $newspan = $('<span></span>').attr ('name', $span.attr ('name')).attr ('value', $input.val ()).append ($input.val ());
					$cont.replaceWith ($newspan);
					toggleFieldChange.call (that, $newspan);
				}
			});

			// Confirm callback
			$confirm_icon.click (function () {
				var $newspan = $('<span></span>').attr ('name', $span.attr ('name')).attr ('value', $input.val ()).append ($input.val ());
				$cont.replaceWith ($newspan);
				toggleFieldChange.call (that, $newspan);
			});

			// Cancel callback
			$cancel_icon.click (function () {
				$cont.replaceWith ($span);
				toggleFieldChange.call (that, $span);
			});

			// Set focus to input element
			$input.focus ();
		});
	}

	// Serialize template data to an object
	function serializeObject ($template) {
		var o = {};

		// Basic serialization (to array)
		var a = $template.serializeArray ();

		// Also include unchecked checkboxes
		a = a.concat ($template.find ('input[type=checkbox]:not(:checked)').map (function () {
			return ({name: this.name, value: ''});
		}).get ());

		// Array to object conversion
		$.each (a, function () {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			}
			else {
				o[this.name] = this.value || '';
			}
		});
		return (o);
	}

	// Callback to be called at entry insert
	function insertCB ($template) {
		var that = this;
		// According to $template type (simple input or nested structure), data can't be retreived the same way.
		// If simple input, it can be serialized directly,
		// if nested structure, all nodes should be passed to serializer
		var entry;
		if ($template.find ('*').length) {
			entry = serializeObject ($template.find ('*'));
		}
		else {
			entry = $template.val ();
		}

		var params = $(this).data ('extensibleList');

		//TODO suppress white space at beginning and end
		if (entry && params.checkCB (entry)) {
			createEntry.call(that, entry);
			if ($template.find ('*').length) {
				// Complex form template
				$template.find ('input:not(input:checkbox, input:radio),select,textarea').val ('').change ();
				$template.find ('input:checkbox, input:radio').removeAttr ('checked').removeAttr ('selected');
				$template.find ('input:not(input:checkbox, input:radio):visible,select:visible,textarea:visible').first ().focus ();
			}
			else {
				// Simple form template
				$template.val ('').change ();
				$template.focus ();
			}
			if (params && params.updateCB) {
				params.updateCB (this, entry);
			}
		}
	};

	var methods = {
		init : function(params) {
			return this.each(function() {
				console.log('[extensibleList::init] Params: ');
				console.dir(params);
				var $this = $(this);
				var that = this;

				$(this).empty ();

				params = $.extend(true, {}, defaults, params);
				$(this).data ('extensibleList', params);

				$this.addClass ('extensibleList');

				var $list = $('<ul class="ui-sortable"></ul>').appendTo(this);
				if (params.sortable) {
					$list.sortable({
						update: function (event, ui) { callCB.call (that, 'order'); }
					});
				}

				for (var entry = 0; entry < params.values.length; entry ++) {
					createEntry.call (this, params.values[entry]);
					if (params) {
						if (params.updateCB) {
							params.updateCB (that, params.values[entry]);
						}
					}
				}

				var $cont = $('<span class="el-container"></span>').appendTo ($(this));
				var $template = $(params.template);
				$template.on ('keypress', function (evt) {
					if ((evt.keyCode == 13) && (evt.target.type !== "textarea")) {
						// User pressed ENTER, create node in list
						insertCB.call (that, $template);
						return (false);
					}
				});
				$cont.append ($template);
				$('<span class="item-create">&#160;</span>').appendTo ($cont).click (function () { insertCB.call (that, $template); });

				// Focus input
				if ($template.find ('*').length) {
					// Complex form template
					$template.find ('input:not(input:checkbox, input:radio):visible,select:visible,textarea:visible').first ().focus ();
				}
				else {
					// Simple form template
					$template.focus ();
				}
			});
		},
		getValues : function() {
			return getValues.call(this);
		},
		enable: function () {
			console.log ('[jquery.extensibleList] Enabling list');
			console.log ($(this));
			$(this).find ('> .el-container').show ();
		},
		disable: function () {
			console.log ('[jquery.extensibleList] Disabling list');
			console.log ($(this));
			$(this).find ('> .el-container').hide ();
		},
		clear: function () {
			console.log ('[jquery.extensibleList] Clearing list');
			console.log ($(this));
			$(this).find ('ul > li').remove ();
		}
	};

	$.fn.extensibleList = function(method) {
		//	Method calling logic
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.extensibleList' );
		}
	};
})( jQuery );
