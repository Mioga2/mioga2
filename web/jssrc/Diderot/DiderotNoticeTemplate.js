// ========================================================
// DiderotNoticeTemplate OO-interface to notice templates
// ========================================================
function DiderotNoticeTemplate (options) {
	// ========================================================
	// Default values
	// ========================================================
	defaults = {
	},

	// ========================================================
	// Public methods
	// ========================================================

	// --------------------------------------------------------
	// fetch, fetch template from Mioga2
	// --------------------------------------------------------
	this.fetch = fetch;
	function fetch () {
		MIOGA.logDebug ('DiderotNoticeTemplate', 1, '[fetch] Entering');
		var that = this;

		$.ajax ({
			url: 'GetTemplate.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			success: function (data) {
				MIOGA.logDebug ('DiderotNoticeTemplate', 3, '[fetch] GetNoticeTemplate.json returned, data: ', data);
				that.attr = data;
				if (that.attr.fields) {
					that.attr.fields = $.parseJSON (that.attr.fields);
					that.attr.order = $.parseJSON (that.attr.order);
				}
				else {
					that.attr.fields = [ ];
					that.attr.order = [ [ ] ];
				}
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('DiderotNoticeTemplate', 1, '[fetch] Leaving, data: ', this.attr);
	};

	// --------------------------------------------------------
	// store, store template to Mioga2
	// --------------------------------------------------------
	this.store = store;
	function store () {
		MIOGA.logDebug ('DiderotNoticeTemplate', 1, '[store] Entering');

		// Basic attributes
		var template_attr = {
			title: this.attr.title,
			description: this.attr.description,
			fields: JSON.stringify (this.attr.fields, function(key, value) { return value === "" ? "" : value }),	// filter function is for IE replacing empty strings with "null"
			order: JSON.stringify (this.attr.order)
		};

		MIOGA.logDebug ('DiderotNoticeTemplate', 2, '[store] Data to be posted: ', template_attr);

		// Post data to Mioga2
		var success = false;
		var message = "[DiderotNoticeTemplate::store] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		var template = {rowid: this.attr.rowid};	// For further loading from WebService response
		$.ajax ({
			url: 'SetTemplate.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: template_attr,
			success: function (data) {
				MIOGA.logDebug ('DiderotNoticeTemplate', 3, '[store] SetNoticeTemplate.json POST returned, data: ', data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.template) {
					template = data.template;
				}
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('DiderotNoticeTemplate', 1, '[store] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, template: template});
	};

	// --------------------------------------------------------
	// getField
	// Given a field internal ID, return field attributes as a hash
	// --------------------------------------------------------
	this.getField = getField;
	function getField (id) {
		var field = $.grep (that.attr.fields, function (entry) { return (entry['field-id'] === id.toString ()); })[0];
		return (field);
	};

	// --------------------------------------------------------
	// getValue
	// Given a value internal ID, return value attributes as a hash
	// --------------------------------------------------------
	this.getValue = getValue;
	function getValue (id) {
		// Extract field values
		var field_values = $.map (that.attr.fields, function (field) { if (field.values !== undefined) { return (field.values); } });

		// Get value for ID
		var value = $.grep (field_values, function (entry) { return (entry['field-id'] === id.toString ()); })[0];

		return (value);
	};

	// --------------------------------------------------------
	// getFields
	// Get ident of all fields
	// --------------------------------------------------------
	this.getFields = getFields;
	function getFields () {
		var fields = $.map (this.attr.fields, function (field) { return (field['field-id']); });
		return (fields);
	};

	// --------------------------------------------------------
	// getOptionalFields
	// Get ident of mandatory fields
	// --------------------------------------------------------
	this.getOptionalFields = getOptionalFields;
	function getOptionalFields () {
		var fields = $($.grep (this.attr.fields, function (field) { return (field.mandatory !== 'on'); })).map (function () { return (this['field-id']); }).get ();
		return (fields);
	};

	// --------------------------------------------------------
	// getMandatoryFields
	// Get ident of mandatory fields
	// --------------------------------------------------------
	this.getMandatoryFields = getMandatoryFields;
	function getMandatoryFields () {
		var fields = $($.grep (this.attr.fields, function (field) { return (field.mandatory === 'on'); })).map (function () { return (this['field-id']); }).get ();
		return (fields);
	};

	// --------------------------------------------------------
	// getHiddenFields
	// Get ident of fields that won't appear in search results
	// --------------------------------------------------------
	this.getHiddenFields = getHiddenFields;
	function getHiddenFields () {
		// First merge displayed fields into an unique array
		var merge = [ ];
		for (var line = 0; line < this.attr.order.length; line++) {
			for (var elem = 0; elem < this.attr.order[line].length; elem++) {
				merge.push (this.attr.order[line][elem]);
			}
		}

		// Get hidden fields
		var fields = $($.grep (this.attr.fields, function (field) { return ($.grep (merge, function (visible_field) { return (field['field-id'] === visible_field); }).length === 0) })).map (function () { return (this['field-id']); }).get ();
		return (fields);
	}

	// --------------------------------------------------------
	// getListOrder
	// Get fields according to list display order
	// --------------------------------------------------------
	this.getListOrder = getListOrder;
	function getListOrder () {
		return (this.attr.order);
	};


	// ========================================================
	// Private methods
	// ========================================================


	// ========================================================
	// Constructor
	// ========================================================
	MIOGA.logDebug ('DiderotNoticeTemplate', 1, '[new] Entering, options: ', options);
	var that = this;

	this.attr = $.extend (true, {}, this.defaults, options);

	// Fetch template data from Mioga2 if required
	if (Object.keys (this.attr).length === 0) {
		// No attribute at all, fetch template from Mioga2, will return empty template if none exists
		this.fetch ();
	}
	else {
		// Object attribute is supposed to a complete template description, no need to fetch data
		this.attr = options;

		// Expand fields and order from string to JSON, if required
		if (typeof (this.attr.fields) === 'string') {
			this.attr.fields = $.parseJSON (this.attr.fields);
		}
		if (typeof (this.attr.order) === 'string') {
			this.attr.order = $.parseJSON (this.attr.order);
		}
	}

	MIOGA.logDebug ('DiderotNoticeTemplate', 1, '[new] Leaving, data: ', that.attr);
}
