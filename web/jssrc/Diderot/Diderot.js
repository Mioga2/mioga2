	// ========================================================================================================
	// Main Diderot object
	// ========================================================================================================
	function Diderot (elem, options) {
		var defaults = {
			page: "search",
			access: 0
		};

		// Constants
		this.CONSTANT = {
		/**
		Access rights
		0 : no right, 1 : read-only, 2 : read-write
		@attribute CONSTANT.ACCESS
		@readOnly
		@type integer
		**/
			ACCESS: { NONE: 0, READ: 1, WRITE: 2 }
		}; // End of constants


		// ========================================================
		// Public attributes
		// ========================================================
		this.applicationName = 'Diderot';
		this.data_expiration_delay = 0;	// Data expires immediatly


		// ========================================================
		// Public methods
		// ========================================================


		// ------------------------------------------------------------------------------------------------
		// Browser navigation hash generator
		// ------------------------------------------------------------------------------------------------
		this.generateNavigationHash = generateNavigationHash;
		function generateNavigationHash (type, rowid) {
			if (rowid !== undefined) {
				return ('#' + this.applicationName +  '-' + type + '-' + rowid);
			}
			else {
				return ('#' + this.applicationName +  '-' + type);
			}
		};


		// ------------------------------------------------------------------------------------------------
		// Generate actions menu
		// ------------------------------------------------------------------------------------------------
		this.generateMenu = generateMenu;
		function generateMenu ($menu, access) {
			$menu.empty ().addClass ('main-menu');

			var cur_page = window.location.hash;

			// Home page
			var hash = that.generateNavigationHash ('home');
			var $link = $('<a></a>').append (this.i18n.home_page).attr ('href', hash).appendTo ($('<li class="homepage"></li>').addClass ((cur_page === hash) ? 'selected' : '').appendTo ($menu));

			// Notice search
			var hash = that.generateNavigationHash ('newsearch');
			$('<a></a>').append (this.i18n.search_notice).attr ('href', hash).appendTo ($('<li class="search"></li>').addClass ((cur_page === hash) ? 'selected' : '').appendTo ($menu));

			// Search results
			if (this.has_search_results) {
				var hash = that.generateNavigationHash ('searchresults');
				$('<a></a>').append (this.i18n.last_search_results).attr ('href', hash).appendTo ($('<li class="search"></li>').addClass ((cur_page === hash) ? 'selected' : '').appendTo ($menu));
			}

			if (mioga_context.rights.Animation) {
				// Template edition
				var hash = that.generateNavigationHash ('template');
				$('<a></a>').append (this.i18n.edit_template).attr ('href', hash).appendTo ($('<li class="template"></li>').addClass ((cur_page === hash) ? 'selected' : '').appendTo ($menu));
			}

			var hash = that.generateNavigationHash ('notice');
			if ((access === this.CONSTANT.ACCESS.WRITE) || (cur_page === hash)) {
				// Notice creation
				$('<a></a>').append (this.i18n.create_notice).attr ('href', hash).appendTo ($('<li class="notice"></li>').addClass ((cur_page === hash) ? 'selected' : '').appendTo ($menu));
			}
		};


		// ------------------------------------------------------------------------------------------------
		// Trigger UI refresh
		// ------------------------------------------------------------------------------------------------
		this.refresh = refresh;
		function refresh () {
			navigationClick.call (this);
		};


		// ------------------------------------------------------------------------------------------------
		// Get group users from Mioga2
		// ------------------------------------------------------------------------------------------------
		this.getUsers = getUsers;
		function getUsers () {
			MIOGA.logDebug ('Diderot', 1, '[getUsers] Entering');

			var users = [ ];

			if (this.users !== undefined) {
				users = this.users;
			}
			else {
				$.ajax ({
					url: 'GetUsers.json',
					type: 'GET',
					async: false,
					traditional: true,
					dataType: 'json',
					success: function (data) {
						MIOGA.logDebug ('Diderot', 3, '[getUsers] GetUsers.json returned, data: ', data);
						users = data;
					},
					error: function () {
						MIOGA.logError ('Internal error', true);
					}
				});
				this.users = users;
			}

			MIOGA.logDebug ('Diderot', 1, '[getUsers] Leaving, users: ', users);
			return (users);
		};


		// ------------------------------------------------------------------------------------------------
		// Get group teams from Mioga2
		// ------------------------------------------------------------------------------------------------
		this.getTeams = getTeams;
		function getTeams () {
			MIOGA.logDebug ('Diderot', 1, '[getTeams] Entering');

			var teams = [ ];

			if (this.teams !== undefined) {
				teams = this.teams;
			}
			else {
				$.ajax ({
					url: 'GetTeams.json',
					type: 'GET',
					async: false,
					traditional: true,
					dataType: 'json',
					success: function (data) {
						MIOGA.logDebug ('Diderot', 3, '[getTeams] GetTeams.json returned, data: ', data);
						teams = data;
					},
					error: function () {
						MIOGA.logError ('Internal error', true);
					}
				});
				this.teams = teams;
			}

			MIOGA.logDebug ('Diderot', 1, '[getTeams] Leaving, teams: ', teams);
			return (teams);
		};


		// ------------------------------------------------------------------------------------------------
		// Get uncategorized notices from Mioga2
		// ------------------------------------------------------------------------------------------------
		function getUncategorized () {
			MIOGA.logDebug ('Diderot', 1, '[getUncategorized] Entering');

			var notices = [ ];

			$.ajax ({
				url: 'GetUncategorized.json',
				type: 'GET',
				async: false,
				traditional: true,
				dataType: 'json',
				data: { count: 10 },
				success: function (data) {
					MIOGA.logDebug ('Diderot', 3, '[getUncategorized] GetUncategorized.json returned, data: ', data);

					// Add template as sub-key into each notice
					if (data.notices && data.notices.length) {
						notices = $.map (data.notices, function (notice) { notice.template = data.template; var notice_obj = new DiderotNotice (notice); return (notice_obj); });
					}
				},
				error: function () {
					MIOGA.logError ('Internal error', true);
				}
			});

			MIOGA.logDebug ('Diderot', 1, '[getUncategorized] Leaving, notices: ', notices);
			return (notices);
		};


		// ------------------------------------------------------------------------------------------------
		// Get notice template from Mioga2
		// ------------------------------------------------------------------------------------------------
		this.getTemplate = getTemplate;
		function getTemplate () {
			MIOGA.logDebug ('Diderot', 1, '[getTemplate] Entering');

			var template = undefined;


			if ((this.template !== undefined) && (this.template_timestamp + this.data_expiration_delay > (new Date().getTime () / 1000))) {
				template = this.template
			}
			else {
				$.ajax ({
					url: 'GetTemplate.json',
					type: 'GET',
					async: false,
					traditional: true,
					dataType: 'json',
					success: function (data) {
						MIOGA.logDebug ('Diderot', 3, '[getTemplate] GetTemplate.json returned, data: ', data);
						template = new DiderotNoticeTemplate (data);
					},
					error: function () {
						MIOGA.logError ('Internal error', true);
					}
				});

				if (template.attr.title) {
					this.has_template = true;
				}

				this.template = template;
				this.template_timestamp = new Date().getTime () / 1000;
			}

			MIOGA.logDebug ('Diderot', 1, '[getTemplate] Leaving, template: ', template);
			return (template);
		};


		// ========================================================
		// Private methods
		// ========================================================


		// --------------------------------------------------------
		// mergeRights
		// Fill low-level access rights according to top-level
		// For example, "Admin" right implies "Read" right
		// --------------------------------------------------------
		function mergeRights () {
			if(mioga_context.rights.Animation) {
				mioga_context.rights.Read     = true;
			}
		}


		// --------------------------------------------------------
		// show, show an object according to its type (notice, template, etc.) and rowid
		// --------------------------------------------------------
		function show (type, rowid, args) {
			MIOGA.logDebug ('Diderot', 1, '[show] Entering for ' + type + ' ' + rowid + ', args: ', args);

			// Get editor for object
			var page = undefined;
			var object = undefined;
			var active_menu = undefined;
			var skip_refresh = false;
			switch (type) {
				case undefined:
					page = this.searchForm;
					active_menu = 'search';
					object = {
						template: getTemplate.call (this),
						mode: 'form'
					};
					break;
				case 'home':
					page = this.mainPage;
					active_menu = 'homepage';
					object = {
						categories: getCategories.call (this),
						notices: getRecent.call (this),
						uncategorized: getUncategorized.call (this),
						template: getTemplate.call (this),
						access: this.options.access
					};
					this.current_category = undefined;
					break;
				case 'category':
					page = this.mainPage;
					active_menu = 'homepage';
					object = {
						categories: getCategories.call (this),
						notices: getNotices.call (this, {category_id: rowid}),
						template: getTemplate.call (this),
						category: rowid,
						access: this.options.access		// Warning, access right to root category !
					};
					this.current_category = rowid;
					break;
				case 'newsearch':
					page = this.searchForm;
					active_menu = 'search';
					object = {
						template: getTemplate.call (this),
						mode: 'form',
						clear: true
					};
					break;
				case 'search':
					page = this.searchForm;
					active_menu = 'search';
					object = {
						template: getTemplate.call (this),
						mode: 'form'
					};
					break;
				case 'searchresults':
					page = this.searchForm;
					active_menu = 'search';
					object = { };
					skip_refresh = true;
					break;
				case 'template':
					page = this.noticeTemplateEdit;
					active_menu = 'template';
					object = getTemplate.call (this);
					break;
				case 'notice':
					page = this.mainPage;
					if (rowid) {
						active_menu = 'homepage';
						object = {
							categories: getCategories.call (this),
							notice: new DiderotNotice ({rowid: rowid}),
							template: getTemplate.call (this)
						};
					}
					else {
						active_menu = 'notice';
						object = {
							categories: getCategories.call (this),
							notice: new DiderotNotice (),
							template: getTemplate.call (this)
						};
					}
					if (this.current_category !== undefined) {
						object.notice.attr.category_id = this.current_category;
					}
					break;
				case 'attach':
					page = this.attachFiles;
					object = {
						notices: getNotices.call (this),
						template: getTemplate.call (this)
					};
					break;
				case 'all_notices':
					page = this.searchForm;
					active_menu = 'search';
					object = {
						mode: 'fakesearch',
						success: true,
						notices: getNotices.call (this),
						template: getTemplate.call (this)
					};
					break;
			}

			// Check template already exists in group
			if ((page !== this.noticeTemplateEdit) && !this.has_template) {
				// Template does not exist, redirect the user to template editor
				window.location.hash = this.generateNavigationHash ('template');
				MIOGA.logDebug ('Diderot', 1, '[show] Template not defined yet, redirecting user to template editor.');
				return;
			}

			// Check editor exists before trying to display it
			if ((page === this.mainPage) || ((page !== undefined) && (object !== undefined))) {
				if (!skip_refresh)
					page.refresh (object, args);
				if (this.currentPage !== undefined) {
					this.currentPage.hide ();
				}
				page.show ();
				this.currentPage = page;

				// Select active menu item
				$(this.elem).find ('ul.main-menu li').removeClass ('selected');
				$(this.elem).find ('ul.main-menu li.' + active_menu).addClass ('selected');
			}
			else {
				alert (this.i18n.no_ui + type);
			}

			MIOGA.logDebug ('Diderot', 1, '[show] Leaving');
		};


		// ------------------------------------------------------------------------------------------------
		// Browser navigation (back and next buttons) handler
		// ------------------------------------------------------------------------------------------------
		function navigationClick () {
			MIOGA.logDebug ('Diderot', 1, '[navigationClick] Entering');

			// load_page will be set to 'false' if page is part of Diderot and needs to be handled in JavaScript
			var load_page = true;

			// Extract parts of hash being in the form #Diderot-<type>-<rowid>, type being 'user', 'team', etc.
			var hash = window.location.hash;
			var split = hash.split ('?');
			var url_args = (split[1] !== undefined) ? split[1].split (/&|=/) : [ ];
			var parts = split[0].split ('-');

			// Convert url_args to hash
			var args = { };
			for (i=0; i<url_args.length; i+=2) {
				if (args[url_args[i]] === undefined) {
					args[url_args[i]] = url_args[i+1];
				}
				else if (typeof (args[url_args[i]]) === 'string') {
					var array = [ ];
					array.push (args[url_args[i]]);
					array.push (url_args[i+1])
					args[url_args[i]] = array;
				}
				else {
					args[url_args[i]].push (url_args[i+1])
				}
			}

			if ((parts[0] === '#' + that.applicationName) || ((parts[0] === '') && (window.location === that.appLocation))) {
				// Target URL belongs to application, handle it here
				MIOGA.logDebug ('Diderot', 1, '[navigationClick] Target URL ' + hash + ' belongs to application');
				show.call (that, parts[1], parseInt (parts[2], 10), args);
				load_page = false;
			}
			else {
				MIOGA.logDebug ('Diderot', 1, '[navigationClick] Target URL does not belong to application');
			}

			MIOGA.logDebug ('Diderot', 1, '[navigationClick] Leaving');
			return (load_page);
		};


		// ------------------------------------------------------------------------------------------------
		// Get categories from Mioga2
		// ------------------------------------------------------------------------------------------------
		function getCategories () {
			MIOGA.logDebug ('Diderot', 1, '[getCategories] Entering');

			var categories = [ ];

			if ((this.categories !== undefined) && (this.categories_timestamp + this.data_expiration_delay > (new Date().getTime () / 1000))) {
				categories = this.categories
			}
			else {
				$.ajax ({
					url: 'GetCategories.json',
					type: 'GET',
					async: false,
					traditional: true,
					dataType: 'json',
					success: function (data) {
						MIOGA.logDebug ('Diderot', 3, '[getCategories] GetCategories.json returned, data: ', data);

						// Process each element to make it a DiderotNoticeCategory object
						var tmp = { };
						for (var i = 0; i < data.length; i++) {
							var category = new DiderotNoticeCategory (data[i]);
							if (data[i]['parent_id'] !== data[i]['rowid']) {
								// Link category to parent
								category.attr.parent = tmp[data[i]['parent_id']];
							}
							tmp[data[i]['rowid']] = category;
						}

						// Convert data to a standard sorted array
						categories = $.map (tmp, function (category) { return (category); });
						var comparator = function (a, b) {
							if (a.attr.ident.toString ().toLowerCase () < b.attr.ident.toString ().toLowerCase ()) return (-1);
							if (a.attr.ident.toString ().toLowerCase () > b.attr.ident.toString ().toLowerCase ()) return (1);
							return (0);
						};
						categories.sort (comparator);
					},
					error: function () {
						MIOGA.logError ('Internal error', true);
					}
				});

				this.categories = categories;
				this.categories_timestamp = new Date ().getTime () / 1000;
			}

			MIOGA.logDebug ('Diderot', 1, '[getCategories] Leaving, categories: ', categories);
			return (categories);
		};


		// ------------------------------------------------------------------------------------------------
		// Get uncategorized notices from Mioga2
		// ------------------------------------------------------------------------------------------------
		function getUncategorized () {
			MIOGA.logDebug ('Diderot', 1, '[getUncategorized] Entering');

			var notices = [ ];

			$.ajax ({
				url: 'GetUncategorized.json',
				type: 'GET',
				async: false,
				traditional: true,
				dataType: 'json',
				data: { count: 10 },
				success: function (data) {
					MIOGA.logDebug ('Diderot', 3, '[getUncategorized] GetUncategorized.json returned, data: ', data);

					// Add template as sub-key into each notice
					if (data.notices && data.notices.length) {
						notices = $.map (data.notices, function (notice) { notice.template = data.template; var notice_obj = new DiderotNotice (notice); return (notice_obj); });
					}
				},
				error: function () {
					MIOGA.logError ('Internal error', true);
				}
			});

			MIOGA.logDebug ('Diderot', 1, '[getUncategorized] Leaving, notices: ', notices);
			return (notices);
		};


		// ------------------------------------------------------------------------------------------------
		// Get recent notices from Mioga2
		// ------------------------------------------------------------------------------------------------
		function getRecent () {
			MIOGA.logDebug ('Diderot', 1, '[getRecent] Entering');

			var notices = [ ];

			$.ajax ({
				url: 'GetRecent.json',
				type: 'GET',
				async: false,
				traditional: true,
				dataType: 'json',
				data: { count: 10 },
				success: function (data) {
					MIOGA.logDebug ('Diderot', 3, '[getRecent] GetRecent.json returned, data: ', data);

					// Add template as sub-key into each notice
					if (data.notices && data.notices.length) {
						notices = $.map (data.notices, function (notice) { notice.template = data.template; var notice_obj = new DiderotNotice (notice); return (notice_obj); });
					}
				},
				error: function () {
					MIOGA.logError ('Internal error', true);
				}
			});

			MIOGA.logDebug ('Diderot', 1, '[getRecent] Leaving, notices: ', notices);
			return (notices);
		};


		// ------------------------------------------------------------------------------------------------
		// Get notices from Mioga2
		// ------------------------------------------------------------------------------------------------
		function getNotices (data) {
			MIOGA.logDebug ('Diderot', 1, '[getNotices] Entering');

			var notices = [ ];

			$.ajax ({
				url: 'SearchNotices.json',
				type: 'GET',
				async: false,
				traditional: true,
				dataType: 'json',
				data: data,
				success: function (data) {
					MIOGA.logDebug ('Diderot', 3, '[getNotices] SearchNotices.json returned, data: ', data);

					// Add template as sub-key into each notice and convert notices to a DiderotNotice object
					if (data.notices && data.notices.length) {
						notices = $.map (data.notices, function (notice) { notice.template = data.template; var notice_obj = new DiderotNotice (notice); return (notice_obj); });
					}
				},
				error: function () {
					MIOGA.logError ('Internal error', true);
				}
			});

			MIOGA.logDebug ('Diderot', 1, '[getNotices] Leaving, notices: ', notices);
			return (notices);
		};


		// ========================================================
		// Constructor
		// ========================================================
		var that = this;

		// Attributs
		this.options    = $.extend (true, {}, defaults, options);
		this.elem       = elem;
		this.i18n       = options.i18n;

		// Flag indicating Diderot has a notice template
		this.has_template = false;

		this.data = options.data;
		this.currentPage = undefined;
		this.appLocation = window.location;

		// Create and activate progress bar
		var pb_opt = { i18n : { "displayText" : this.i18n.progressBarText } };
		this.appPB = new AppProgressBar(this.elem, pb_opt);
		this.appPB.setValue(0);


		// ------------------------------------------------------------------------------------------------
		// Merge access rights
		// ------------------------------------------------------------------------------------------------
		mergeRights.call (this);

		// ------------------------------------------------------------------------------------------------
		// Create main page
		// ------------------------------------------------------------------------------------------------
		this.mainPage = new DiderotMainPage (that.elem, {
			diderot: this,
			i18n: options.i18n
		});
		this.mainPage.hide ();
		this.appPB.setValue(20);

		// ------------------------------------------------------------------------------------------------
		// Create search form
		// ------------------------------------------------------------------------------------------------
		this.searchForm = new DiderotSearch (that.elem, {
			diderot: this,
			i18n: options.i18n
		});
		this.searchForm.hide ();
		this.appPB.setValue(40);

		// ------------------------------------------------------------------------------------------------
		// Create notice template form
		// ------------------------------------------------------------------------------------------------
		this.noticeTemplateEdit = new DiderotNoticeTemplateEdit (that.elem, {
			diderot: this,
			i18n: options.i18n
		});
		this.noticeTemplateEdit.hide ();
		this.appPB.setValue(60);

		this.attachFiles = new DiderotAttachFiles (that.elem, {
			diderot: this,
			i18n: options.i18n
		});
		this.attachFiles.hide ();
		this.appPB.setValue(80);

		// ------------------------------------------------------------------------------------------------
		// Trap browser's navigation buttons
		// ------------------------------------------------------------------------------------------------
		if (window.addEventListener) {
			window.addEventListener ("hashchange", navigationClick, false);
		}
		else if (window.attachEvent) {
			window.attachEvent ("onhashchange", navigationClick);
		}
		else {
			alert (i18n.internal_error + "Can't attach event listener to hashchange.");
		}
		navigationClick.call (this);

		this.appPB.setValue (100);
		this.appPB.$pbcont.fadeOut ('slow');
	};
