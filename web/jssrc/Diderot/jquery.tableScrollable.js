/*!
 * jQuery tableScrollable v1.0
 *
 * Copyright 2012, (http://alixen.fr)
 *
 * Depends:
 *   jquery.ba-resize.js
 */
(function( $ ){
	// ******  usage :  ********************** 
	//		$('#scroll').tableScrollable( [options] );
	// ***************************************
	var defaults = {
		step : 3
    };

	function cursorAt($that, options) {
		var perc = options.first_visible_elm * 100 / options.elem_not_vis;
		$that.find('.mioga2-scr-pan-cursor').css({"top" : perc* options.h_restante/100 + "px"});
	}
	function scrollAt ($that, options) {
		$that.find('.mioga2-scr-active').removeClass('mioga2-scr-active');
		for (var i=options.first_visible_elm; i < options.first_visible_elm + options.count_visible_elm; i++) {
			$that.find('.mioga2-scr-item').eq(i).addClass('mioga2-scr-active');
		}
	}
	
	function tableRefresh ($that, options) {
		$that.find('.mioga2-scr-pan-cont').remove();
		$that.find('> div > table > thead > tr, > div > table > tbody > tr').removeClass('mioga2-scr-item mioga2-scr-active mioga2-scr-header'); 
		
		var h_this		= parseFloat($that.height()) - parseFloat($that.css('paddingTop')) - parseFloat($that.css('paddingBottom'));
		var h_header 	= 0;
		var h_cont		= 0;
		var h_total		= 0;
		var h_item		= 0;
		
		options.count_elm 	= 0;
		options.count_visible_elm = 0;
		options.elem_not_vis = 0;
		
		var h_cursor_cont = 0;
		var w_cursor = 0;
		options.h_cursor = 0;
		
		options.h_restante = 0;
		options.cursor_step = 0;
		options.min_offset = 0;
		options.max_offset = 0;
		
		var $pan = $(
			'<div class="mioga2-scr-pan-cont">' + 
				'<span class="mioga2-scr-pan-top"></span>' + 
				'<div class="mioga2-scr-pan-cursor-cont">' + 
					'<span class="mioga2-scr-pan-cursor draggable"></span>' + 
				'</div>' + 
				'<span class="mioga2-scr-pan-bottom"></span>' + 
			'</div>'
		).appendTo($that);
		
		// =========================================================================
		// DRAW TABLE
		// =========================================================================
		$.each($that.find('> div > table > thead > tr, > div > table > tbody > tr'), function (i,e) {
			if ($(e).find('> th').length) {
				$(e).addClass('mioga2-scr-header');
				h_header = parseFloat($(e).outerHeight( true )); 
			}
			else {
				h_total += parseFloat($(e).outerHeight( true ));
				$(e).addClass('mioga2-scr-item');
				options.count_elm ++;
			}
		});
		
		h_cont 	= h_this - h_header;
		h_item	= h_total/options.count_elm;
		if (h_cont/h_item <  options.count_elm) {
			options.count_visible_elm = parseInt(h_cont/h_item);
		}
		else {
			options.count_visible_elm = options.count_elm;
		}
		options.elem_not_vis = options.count_elm - options.count_visible_elm;
		
		if (options.elem_not_vis < options.first_visible_elm) {
			options.first_visible_elm = options.elem_not_vis; 
		}
		scrollAt ($that, options);
		// =========================================================================
		// DRAW CURSOR IF NEEDED
		// =========================================================================
		if (options.count_visible_elm < options.count_elm) {
			$that.find('.mioga2-scr-pan-cont').height(h_cont + 'px');
			h_cursor_cont = (options.count_visible_elm * h_item ) - $that.find('.mioga2-scr-pan-top').outerHeight(true) - $that.find('.mioga2-scr-pan-bottom').outerHeight(true);
			w_cursor = parseFloat($pan.outerWidth(true));
			$that.find('.mioga2-scr-pan-cursor-cont').height(h_cursor_cont  + 'px');
			
			if ( options.count_visible_elm*h_cursor_cont/options.count_elm > 15) {
				options.h_cursor = options.count_visible_elm*h_cursor_cont/options.count_elm;
			}
			else {
				options.h_cursor = 15;
			}
			options.h_restante = h_cursor_cont - options.h_cursor;
			$that.find('.mioga2-scr-pan-cursor').height( options.h_cursor + 'px');

			$pan.css({"position" : "relative", "top" : h_header + "px"});
			
			options.cursor_step = options.h_restante/options.elem_not_vis;
			options.min_offset = $that.find('.mioga2-scr-pan-cursor-cont').offset().top;
			options.max_offset = options.min_offset + options.h_restante;
			
			cursorAt($that, options);
		}
		else {
			$pan.hide();
		}
		$that.find('.mioga2-scr-table-cont').width(parseFloat($that.outerWidth(true)) - parseFloat($that.css('paddingLeft')) - parseFloat($that.css('paddingRight')) - w_cursor - 4 + "px");
	}
	// ========================================================
	// Methods declaration
	// ========================================================
	var methods = {
		init : function(args) {
			var options = $.extend({}, defaults, args);
			return $(this).each(function() {
				var $this 	= $(this);
				var that = this;
				options.first_visible_elm = 0;
				options.current_top = 0;
				
				function onResize () {
					tableRefresh($this, options);
				}
				
				$this.addClass('mioga2-scr-container');
				$this.find('> table').wrap('<div class="mioga2-scr-table-cont"></div>');
				tableRefresh($this, options);
				// ===========================================================================================
				// BEHAVIORS WHEN TABLE OR SELECTOR IS RESIZED
				// ===========================================================================================
				$this.bind('resize', onResize);
				$this.find('.mioga2-scr-table-cont').bind('resize', onResize);
				// ===========================================================================================
				// BEHAVIORS TO PAN NAVIGATION
				// ===========================================================================================
				$this.find('.mioga2-scr-pan-bottom').live('click',function (ev) {
					var current_first_elm = options.first_visible_elm; 
					for ( var i=0; i < options.step; i++ ) {
						if (options.first_visible_elm+1 <= options.count_elm - options.count_visible_elm) {
							options.first_visible_elm ++;
						}
						else {
							break;
						}
					}
					if (options.first_visible_elm > current_first_elm) {
						scrollAt($this, options);
						cursorAt($this, options);
					}
				});
				$this.find('.mioga2-scr-pan-top').live('click',function (ev) {
					var current_first_elm = options.first_visible_elm;
					for ( var i=0; i < options.step; i++ ) {
						if (options.first_visible_elm - 1 >= 0 ) {
							options.first_visible_elm --;
						}
						else {
							break;
						}
					}
					if (options.first_visible_elm < current_first_elm) {
						scrollAt($this, options);
						cursorAt($this, options);
					}
				});
				$this.find('.mioga2-scr-pan-cursor-cont').live('click', function (ev) {
					// page up called
					var offset_click = ev.clientY;
					var cursor_top = $(this).children().offset().top;
					if (ev.pageY < cursor_top) {
						var current_first_elm = options.first_visible_elm;
						for ( var i=0; i < options.count_visible_elm; i++ ) {
							if (options.first_visible_elm - 1 >= 0 ) {
								options.first_visible_elm --;
							}
						}
						if (options.first_visible_elm < current_first_elm) {
							scrollAt($this, options);
							cursorAt($this, options);
						}
					}
					// page down called
					else if (ev.pageY > cursor_top + options.h_cursor){
						var current_first_elm = options.first_visible_elm; 
						for ( var i=0; i < options.count_visible_elm; i++ ) {
							if (options.first_visible_elm + 1 <= options.count_elm - options.count_visible_elm) {
								options.first_visible_elm ++;
							}
						}
						if (options.first_visible_elm > current_first_elm) {
							scrollAt($this, options);
							cursorAt($this, options);
						}
					}
				});
				// ===========================================================================================
				// Dragging mouse event for pan cursor
				// ===========================================================================================
				var mouseDown = false;
			    var mouseDownY = 0;
			    var clonedElm = null;
			    $().ready(function(){
			        $(document).mousedown(onMouseDown);
			        $(document).mousemove(onMouseMove);
			        $(document).mouseup(onMouseUp);
			    });

			    function onMouseDown (event) {
			    	if ($(event.target).hasClass('mioga2-scr-pan-cursor')) {
			    		options.current_top = parseFloat($this.find('.mioga2-scr-pan-cursor').offset().top);
			    	}
			        if (event.srcElement == undefined && event.target != undefined) {
			            event.srcElement = event.target;
			        }
			        if (!$(event.srcElement).hasClass("draggable")) {
			            return;
			        }
			        mouseDown = true;
			        clonedElm = $(event.srcElement);
			        mouseDownY = event.pageY;

			        return false;
			    }

			    function onMouseMove (event) {
			        if (clonedElm == null) {
			            return;
			        }
			        var cloneOffset = $(clonedElm).offset();
			        var y = cloneOffset.top + (event.pageY - mouseDownY);
			        // If the cursor is after start or before end container.
			        if (y <= options.max_offset && y - options.min_offset >= 0) {
			        	if (Math.abs(y - options.current_top) >= options.cursor_step) {
			        		var perc = (y - options.min_offset)*100/options.h_restante;
			        		var new_elm = perc * options.elem_not_vis / 100;
			        		if (new_elm - parseInt(new_elm) < 1/2) {
			        			options.first_visible_elm = parseInt(new_elm);
			        		}
			        		else {
			        			options.first_visible_elm = parseInt(new_elm)+1;
			        		}
			        		
			        		scrollAt ($this, options);
			        		options.current_top = y;
			        	}
			        	else if (options.max_offset - y < options.cursor_step && options.max_offset > options.current_top) {
			        		options.first_visible_elm = options.elem_not_vis;
			        		scrollAt ($this, options);
			        		y = options.max_offset;
			        		options.current_top = y;
			        	}
			        	else if (y - options.min_offset < options.cursor_step && options.min_offset < options.current_top) {
			        		options.first_visible_elm = 0;
			        		scrollAt ($this, options);
			        		y = options.min_offset;
			        		options.current_top = y;
			        	}
			        	$(clonedElm).offset({top:y});
			        }
			        
		        	mouseDownY = event.pageY;
			        return false;
			    }

			    function onMouseUp (event)  {
			        mouseDown = false;
			        clonedElm = null;
			        mouseDownY = 0;
			    }
			    // ===========================================================================================
				// Event handler for mouse wheel event.
			    // ===========================================================================================
				function wheel(event){
					var delta = 0;
					// For IE
					if (!event)
						event = window.event;
					// IE/Opera
					if (event.wheelDelta) { 
						delta = event.wheelDelta/120;
					} else if (event.detail) {
						// In Mozilla, sign of delta is different than in IE.
						// Also, delta is multiple of 3.
						delta = -event.detail/3;
					}
					// If delta is nonzero, handle it.
					// Basically, delta is now positive if wheel was scrolled up,
					// and negative, if wheel was scrolled down.
					if (delta) {
						handle(delta);
					// Prevent default actions caused by mouse wheel.
					// That might be ugly, but we handle scrolls somehow
					// anyway, so don't bother here..
					}
					if (event.preventDefault) {
						event.preventDefault();
					}
					event.returnValue = false;
				}
				// This is high-level function.
				// It must react to delta being more/less than zero.
				function handle(delta) {
						if (delta < 0 ) {
							$this.find('.mioga2-scr-pan-bottom').trigger('click');
						}
						else {
							$this.find('.mioga2-scr-pan-top').trigger('click');
						}
				}
				// Initialize mouse whell event
				if (that.addEventListener) {
					if (!that.addEventListener.DOMMouseScroll) {
				        // DOMMouseScroll is for mozilla
						that.addEventListener('DOMMouseScroll', wheel, false);
					}
				}
				//IE/Opera
				that.onmousewheel = wheel;
	    	});
	    }
	};
	
	$.fn.tableScrollable = function(method) {
	    var args = arguments;
	    if ( methods[method] ) {
	    	return methods[ method ].apply( this, Array.prototype.slice.call( args, 1 ));
	    }
	    else if ( typeof method === 'object' || ! method ) {
	    	return methods.init.apply( this, args );
	    }
	    else {
	        $.error( 'Method ' +  method + ' does not exist on jQuery.tableScrollable' );
	    }
	};
})( jQuery );
