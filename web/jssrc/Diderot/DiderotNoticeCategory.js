// ========================================================
// DiderotNoticeCategory OO-interface to notice categories
// ========================================================
function DiderotNoticeCategory (options) {
	// ========================================================
	// Default values
	// ========================================================
	defaults = {
	},

	// ========================================================
	// Public methods
	// ========================================================

	// --------------------------------------------------------
	// fetch, fetch categories from Mioga2
	// --------------------------------------------------------
	this.fetch = fetch;
	function fetch () {
		MIOGA.logDebug ('DiderotNoticeCategory', 1, '[fetch] Entering');
		var that = this;

		$.ajax ({
			url: 'GetCategory.json',
			type: 'GET',
			async: false,
			traditional: true,
			dataType: 'json',
			data: {rowid: that.attr.rowid},
			success: function (data) {
				MIOGA.logDebug ('DiderotNoticeCategory', 3, '[fetch] GetCategory.json returned, data: ', data);
				that.attr = data;
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('DiderotNoticeCategory', 1, '[fetch] Leaving, data: ', this.attr);
	};

	// --------------------------------------------------------
	// store, store category to Mioga2
	// --------------------------------------------------------
	this.store = store;
	function store () {
		MIOGA.logDebug ('DiderotNoticeCategory', 1, '[store] Entering');

		// Basic attributes
		var category_attr = {
			rowid: this.attr.rowid,
			ident: this.attr.ident,
			parent_id: (this.attr.parent_id !== 0) ? this.attr.parent_id : this.attr.rowid
		};

		MIOGA.logDebug ('DiderotNoticeCategory', 2, '[store] Data to be posted: ', category_attr);

		// Post data to Mioga2
		var success = false;
		var message = "[DiderotNoticeCategory::store] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		var categories = { };	// For further loading from WebService response
		$.ajax ({
			url: 'SetCategory.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: category_attr,
			success: function (data) {
				MIOGA.logDebug ('DiderotNoticeCategory', 3, '[store] SetCategory.json POST returned, data: ', data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.categories) {
					// Process each element to make it a DiderotNoticeCategory object
					var tmp = { };
					for (var i = 0; i < data.categories.length; i++) {
						var category = new DiderotNoticeCategory (data.categories[i]);
						if (data.categories[i]['parent_id'] !== data.categories[i]['rowid']) {
							// Link category to parent
							category.attr.parent = tmp[data.categories[i]['parent_id']];
						}
						tmp[data.categories[i]['rowid']] = category;
					}

					// Convert data to a standard array
					categories = $.map (tmp, function (category) { return (category); });
				}
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('DiderotNoticeCategory', 1, '[store] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, categories: categories});
	};

	// --------------------------------------------------------
	// destroy, destroy category from Mioga2
	// --------------------------------------------------------
	this.destroy = destroy;
	function destroy () {
		MIOGA.logDebug ('DiderotNoticeCategory', 1, '[destroy] Entering, rowid: ' + this.attr.rowid);

		// Post data to Mioga2
		var success = false;
		var message = "[DiderotNoticeCategory::destroy] Operation failed.";	// This is a default message to be replaced by message from WebService response
		var errors = [ ];
		var categories = { };	// For further loading from WebService response
		$.ajax ({
			url: 'DeleteCategory.json',
			type: 'POST',
			async: false,
			traditional: true,
			dataType: 'json',
			data: {rowid: that.attr.rowid},
			success: function (data) {
				MIOGA.logDebug ('DiderotNoticeCategory', 3, '[destroy] DeleteCategory.json POST returned, data: ', data);
				success = data.success;
				message = data.message;
				errors = data.errors;
				if (data.categories) {
					// Process each element to make it a DiderotNoticeCategory object
					var tmp = { };
					for (var i = 0; i < data.categories.length; i++) {
						var category = new DiderotNoticeCategory (data.categories[i]);
						if (data.categories[i]['parent_id'] !== data.categories[i]['rowid']) {
							// Link category to parent
							category.attr.parent = tmp[data.categories[i]['parent_id']];
						}
						tmp[data.categories[i]['rowid']] = category;
					}

					// Convert data to a standard array
					categories = $.map (tmp, function (category) { return (category); });
				}
			},
			error: function () {
				MIOGA.logError ('Internal error', true);
			}
		});

		MIOGA.logDebug ('DiderotNoticeCategory', 1, '[destroy] Leaving, success: ' + success);
		return ({success: success, message: message, errors: errors, categories: categories});
	};


	// ========================================================
	// Private methods
	// ========================================================


	// ========================================================
	// Constructor
	// ========================================================
	MIOGA.logDebug ('DiderotNoticeCategory', 1, '[new] Entering, options: ', options);
	var that = this;

	this.attr = $.extend (true, {}, this.defaults, options);

	// Fetch category data from Mioga2 if required
	if ((Object.keys (this.attr).length === 1) && this.attr.rowid !== undefined) {
		// Single attribute given is a rowid, fetch single category
		this.fetch ();
	}
	else {
		// Object attribute is supposed to a complete category, no need to fetch data
		this.attr = options;
	}

	MIOGA.logDebug ('DiderotNoticeCategory', 1, '[new] Leaving, data: ', that.attr);
}
