// -----------------------------------------------------------------------------
// Diderot is Mioga2 group animation application. It is constructed as a jQuery
// plugin.
// -----------------------------------------------------------------------------
(function ($) {
	var defaults = {
		i18n: {
			"progressBarText": "Diderot is loading...",
			"no_ui_for_page": "ERROR. No UI available for page ",
			"proceed_search": "Search",
			"subexpression": "Sub-expression",
			"special-subexp": "Sub-expression",
			"close_subexpr": "Close this sub-expression",
			"store_template": "Store",
			"notice_field_name": "Field name",
			"notice_field_type": "Field type",
			"mandatory": "Mandatory",
			"close_value_list": "Close this list of values",
			"notice_fields": "Notice fields",
			"notice_fields_order": "Order of fields in search results",
			"add_line": "Add a line",
			"edit_value_list": "Edit this list of values",
			"template_store_success": "Template store successfully",
			"template_attributes": "Template attributes",
			"template_title": "Title",
			"template_description": "Description",
			"no_ui": "Error. No UI for object: ",
			"field_type_string_label": "Single-line text",
			"field_type_text_label": "Multi-line text",
			"field_type_value_label": "Single-value list-box",
			"field_type_values_label": "Multi-value list-box",
			"field_type_mandatory_on": "Mandatory",
			"notice_template_edit_title": "Notice template edition",
			"operator_and_label": "AND",
			"operator_or_label": "OR",
			"operator_not_label": "NOT",
			"constraint_contains_any": "contains any of the words",
			"constraint_contains_all": "contains all the words",
			"constraint_equals": "is exactly",
			"constraint_does_not_contain": "does not contain",
			"notice_store_success": "Notice stored successfully",
			"notice_view_title": "Notice display",
			"notice_edit_title": "Notice edition",
			"store_notice": "Store",
			"associated_documents": "Associated documents",
			"download_file": "Download",
			"edit_notice_btn": "Edit this notice",
			"cancel_store_notice": "Cancel",
			"confirm_cancel": "If you cancel notice edition, all changes will be lost.\nAre you sure you want to cancel ?",
			"edit_template": "Edit notice template",
			"create_notice": "Create a notice",
			"recently_modified_notices": "Recently-modified notices",
			"search_notice": "Search notices",
			"all_values": "All values",
			"no_notice_to_display": "No notice to display.",
			"last_modified": "Last modified",
			"home_page": "Home page",
			"search_failed": "Search failed.",
			"modify_search": "Modify search",
			"new_search": "New search",
			"description": "Description",
			"notice_excluded_fields": "Fields not to be displayed in search results",
			"notice_excluded_fields_help": "Drop here the fields from above that you don't want to be displayed into search results.",
			"notices": "notices",
			"notices_in_category": "Notices in category \"$category$\"",
			"create_category": "Create a category",
			"new_category": "New category",
			"create": "Create",
			"cancel": "Cancel",
			"category_name": "Category name",
			"documentary_base_attributes": "Documentary base attributes",
			"delete_category": "Delete this category",
			"rename_category": "Rename this category",
			"rename": "Rename",
			"category_rename_success": "Category successfully renamed.",
			"no_notice_template_yet": "This group does not have defined a notice template yet and your access rights to the application don't allow you to define it.\nPlease contact the group animator so he creates this template.",
			"simple_search_label": "Simple search",
			"search_terms": "Search terms",
			"add_files": "Add files",
			"close_filepicker": "Close",
			"clear_form": "Clear form",
			"uncategorized_notices": "Uncategorized notices",
			"notice_category_change_success": "Notice successfully modified.",
			"manage_access_rights": "Manage access rights",
			"access_rights": "Access rights to \"$name$\"",
			"root_category": "Root category",
			"profile_rights": "Profile rights",
			"team_rights": "Team rights",
			"user_rights": "User rights",
			"inheritance": "Inheritance",
			"inherited_rights": "Rights are inherited from parent",
			"not_inherited_rights": "Rights are not inherited from parent",
			"no_right": "No right",
			"read_only": "Read-only",
			"read_write": "Read-write",
			"user_label_mask": "$firstname$ $lastname$",
			"access_rights_update_success": "Access rights successfully updated.",
			"remove_specific_right": "Remove specific right",
			"add_teams": "Add teams",
			"add_users": "Add users",
			"specific_right_add_success": "Specific right successfully added.",
			"no_more_teams_to_add": "There are no more teams to add",
			"no_more_users_to_add": "There are no more users to add",
			"back": "Back",
			"close": "Close",
			"attach_files": "Attach files to a notice",
			"files_to_attach": "Files to attach",
			"select_existing_notice": "Select an existing notice",
			"attach_files_help": "You can attach the files on the left to an existing notice or you can create a new notice with these files attached. If you want to remove files from the list, you can do so after having switched to notice edition or creation.",
			"add": "add",
			"select_all": "Select all",
			"deselect_all": "Select none",
			"firstname": "Firstname",
			"lastname": "Lastname",
			"email": "E-mail",
			"broken_link": "An attached file is missing, it has been deleted from Mioga2.",
			"advanced_search_label": "Advanced search",
			"no_advanced_search_criteria": "There is no advanced search criteria yet. To add some, please fill the box below.",
			"hidden_files": "This notice has attached files you are not allowed to view.",
			"open_file": "Open",
			"view_notice": "View",
			"notice_static_fields": "Fields to be displayed into advanced-search form",
			"advanced_search": "Advanced search",
			"simple_search": "Simple search",
			"all_notices": "See all notices",
			"last_search_results": "Last search results"
		}
	};

	// ========================================================
	// Effective plugin declaration with methods abstraction
	// Usage :
	//     <div id="diderot"></div>
	//
	//     $('#diderot').diderot(options);
	// ========================================================
	$.fn.diderot = function(options) {
		// method call, get existing Diderot Object and apply method
		// if method not returns result we return this for chainability
		if (typeof options === 'string') {
			var args = Array.prototype.slice.call(arguments, 1);
			this.each(function() {
				var diderot = $.data(this, 'diderot');
				if (diderot && $.isFunction(diderot[options])) {
					var res = diderot[options].apply(diderot, args);
					if (res !== undefined) {
						return res;
					}
				}
				else {
					$.error( 'Method ' +  options + ' does not exist on jQuery.diderot' );
				}
			});
		}
		else if (typeof options === 'object') {
			options =  $.extend(true, {}, defaults, options);

			this.each(function(i, item) {
				var $elem = $(item);
				var diderot = new Diderot(item, options);
				$(item).data('diderot', diderot);
			});
		}
		else {
			$.error( 'Diderot must not be there ... ');
		}
		return this;
	};
