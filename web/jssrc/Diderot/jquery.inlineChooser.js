// ========================================================
// jquery.inlineChooser.js
// A inlineChooser box plugin
// Usage :
//     <div id="my-div">
//     		initial_value
//     </div>
//
//     $('#my-div').inlineChooser ({
//     		values: {
//     			value1: 'label1',
//     			value2: 'label2',
//     			...
//     		},
//     		onChange: function (value) { }
//     });
// ========================================================
(function ($) {
	$.fn.inlineChooser = function (options) {
		this.each (function (i, item) {
			var $elem = $(item);

			$elem.addClass ('inline-chooser');

			// Create and populate dropdown
			var $dropdown = $('<ul class="dropdown"></ul>').appendTo ($elem);
			var $list = $('<ul></ul>').appendTo ($('<li class="dir"></li>').append ('&#x25BC;').appendTo ($dropdown));
			for (var key in options.values) {
				$('<li></li>').attr ('field-value', key).append (options.values[key]).appendTo ($list);
			}

			// Attach click events
			$list.find ('li').click (function () {
				options.onChange ($(this).attr ('field-value'));
			});
		});

		return (this);
	};
}) (jQuery);
