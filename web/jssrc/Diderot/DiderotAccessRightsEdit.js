	// ========================================================================================================
	// DiderotAccessRightsEdit object, notice editor (or viewer) UI
	// ========================================================================================================
	function DiderotAccessRightsEdit (elem, options) {
		var defaults = {
		};

		// ========================================================
		// Public methods
		// ========================================================


		// --------------------------------------------------------
		// refresh
		// Fill UI with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data, diderot_refresh) {
			MIOGA.logDebug ('DiderotAccessRightsEdit', 1, '[refresh] Entering, data: ', data);

			var that = this;
			this.rights = data;

			this.$selector.hide ();
			this.$form.show ();

			// Empty lists
			this.$profiles.empty ();
			this.$users.empty ();
			this.$teams.empty ();

			// Restore buttons
			that.$button_bar.find ('.button.cancel').show ();
			that.$back_btn.hide ();

			// Inheritance
			if (data.attr.inherited) {
				this.$not_inherited.removeAttr ('disabled').attr ('checked', false);
				this.$inherited.removeAttr ('disabled').attr ('checked', true);
			}
			else {
				this.$inherited.removeAttr ('disabled').attr ('checked', false);
				this.$not_inherited.removeAttr ('disabled').attr ('checked', true);
			}
			if (data.attr.rowid === null) {
				this.$inherited.attr ('disabled', true);
				this.$not_inherited.attr ('disabled', true);
			}

			// Create values hash for inlineChooser
			var inlineChooserValues = { };
			inlineChooserValues[diderot.CONSTANT.ACCESS.NONE] = i18n.no_right;
			inlineChooserValues[diderot.CONSTANT.ACCESS.READ] = i18n.read_only;
			inlineChooserValues[diderot.CONSTANT.ACCESS.WRITE] = i18n.read_write;

			// Profile access rights
			if (data.attr.profiles && data.attr.profiles.length) {
				$.each (data.attr.profiles, function (index, profile) {
					// Set class name from access right
					var classname;
					if (profile.access === diderot.CONSTANT.ACCESS.WRITE) {
						classname = 'read-write';
					}
					else if (profile.access === diderot.CONSTANT.ACCESS.READ) {
						classname = 'read-only';
					}
					else {
						classname = 'no-right';
					}

					var $entry = $('<li></li>').addClass (classname).append (profile.ident).append ($('<span class="access-right"></span>').append (inlineChooserValues[profile.access])).appendTo (that.$profiles);
					$entry.inlineChooser ({
						values: inlineChooserValues,
						onChange: function (value) {
							profile.access = value;
							setRights.call (that);
						}
					});
				});

				// Ask Diderot to refresh underlying view, if required
				if (diderot_refresh) {
					diderot.refresh ();
				}
			}

			inlineChooserValues[-1] = i18n.remove_specific_right;
			// user access rights
			if (diderot.getUsers ().length) {
				this.$users_fieldset.show ();
				$.each (data.attr.users, function (index, user) {
					// Set class name from access right
					var classname;
					if (user.access === diderot.CONSTANT.ACCESS.WRITE) {
						classname = 'read-write';
					}
					else if (user.access === diderot.CONSTANT.ACCESS.READ) {
						classname = 'read-only';
					}
					else {
						classname = 'no-right';
					}

					var user_label = i18n.user_label_mask.replace (/\$firstname\$/, user.firstname).replace (/\$lastname\$/, user.lastname);
					var $entry = $('<li></li>').addClass (classname).append (user_label).append ($('<span class="access-right"></span>').append (inlineChooserValues[user.access])).attr ('title', user.email).appendTo (that.$users);
					$entry.inlineChooser ({
						values: inlineChooserValues,
						onChange: function (value) {
							if (value === -1) {
								// Remove specific right
								removeRight.call (that, 'user', user.rowid);
							}
							else {
								// Change user right
								user.access = value;
								setRights.call (that);
							}
						}
					});
				});
			}

			// Set add user button visibility
			if (diderot.getUsers ().length > data.attr.users.length) {
				this.$add_users_btn.show ();
			}
			else {
				this.$add_users_btn.hide ();
			}

			// Team access rights
			if (diderot.getTeams ().length) {
				this.$teams_fieldset.show ();
				$.each (data.attr.teams, function (index, team) {
					// Set class name from access right
					var classname;
					if (team.access === diderot.CONSTANT.ACCESS.WRITE) {
						classname = 'read-write';
					}
					else if (team.access === diderot.CONSTANT.ACCESS.READ) {
						classname = 'read-only';
					}
					else {
						classname = 'no-right';
					}

					var $entry = $('<li></li>').addClass (classname).append (team.ident).append ($('<span class="access-right"></span>').append (inlineChooserValues[team.access])).appendTo (that.$teams);
					$entry.inlineChooser ({
						values: inlineChooserValues,
						onChange: function (value) {
							if (value === -1) {
								// Remove specific right
								removeRight.call (that, 'team', team.rowid);
							}
							else {
								// Change team right
								team.access = value;
								setRights.call (that);
							}
						}
					});
				});
			}

			// Set add team button visibility
			if (diderot.getTeams ().length > data.attr.teams.length) {
				this.$add_teams_btn.show ();
			}
			else {
				this.$add_teams_btn.hide ();
			}

			MIOGA.logDebug ('DiderotAccessRightsEdit', 1, '[refresh] Leaving');
		}

		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// setRights
		// Store access rights
		// --------------------------------------------------------
		function setRights () {
			MIOGA.logDebug ('DiderotAccessRightsEdit', 1, '[setRights] Entering, data: ', this.rights);

			var res = this.rights.store ();
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.access_rights_update_success;
				that.$message_box.inlineMessage ('info', message);

				var rights = new DiderotAccessRights (res.rights);
				this.refresh (rights, true);
			}
			else {
				displayError.call (this, res);
			}

			MIOGA.logDebug ('DiderotAccessRightsEdit', 1, '[setRights] Leaving');
		};

		// --------------------------------------------------------
		// removeRight
		// Remove specific access right
		// --------------------------------------------------------
		function removeRight (type, rowid) {
			MIOGA.logDebug ('DiderotAccessRightsEdit', 1, '[removeRight] Entering for ' + type + ' rowid ' + rowid);

			var res = this.rights.remove (type, rowid);
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.access_rights_update_success;
				that.$message_box.inlineMessage ('info', message);

				var rights = new DiderotAccessRights (res.rights);
				this.refresh (rights, true);
			}
			else {
				displayError.call (this, res);
			}

			MIOGA.logDebug ('DiderotAccessRightsEdit', 1, '[removeRight] Leaving');
		};


		// --------------------------------------------------------
		// displayError
		// display error message from WebService response
		// --------------------------------------------------------
		function displayError (res) {
			that.$message_box.inlineMessage ('error', res.message);
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DiderotAccessRightsEdit', 1, '[new] Entering');
		var that = this; // For callback reference to current object
		this.elem       = elem;

		var i18n = options.i18n;

		var diderot = options.diderot;
		this.options =  $.extend (true, {}, defaults, options);


		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="diderot-access-rights"></div>').appendTo (this.elem);

		// Message box
		this.$message_box = $('<div></div>').inlineMessage ().appendTo (this.$cont);


		this.$form = $('<div class="form"></div>').appendTo (this.$cont);
		this.$selector = $('<div class="selector"></div>').appendTo (that.$cont).hide ();

		// Dialog buttons
		this.$button_bar;
		if ($(this.elem).parent ().find ('ul.dialog-buttons').length) {
			this.$button_bar = $(this.elem).parent ().find ('ul.dialog-buttons');
		}
		else {
			this.$button_bar = $('<ul class="button_list dialog-buttons"></ul>').appendTo (this.$cont);
		}
		this.$back_btn = $('<button class="button cancel"></button>').append (i18n.back).appendTo ($('<li></li>').appendTo (this.$button_bar)).hide ();
		this.$back_btn.click (function () {
			that.$selector.hide ();
			that.$form.show ();
			that.$button_bar.find ('.button').show ();
			that.$back_btn.hide ();
			return (false);
		});

		// --------------------------------------------------------
		// Columns
		// --------------------------------------------------------
		var $left = $('<div class="left-column"></div>').appendTo (this.$form);
		var $right = $('<div class="right-column"></div>').appendTo (this.$form);


		// --------------------------------------------------------
		// Inheritance
		// --------------------------------------------------------
		var $fieldset = $('<fieldset></fieldset>').appendTo ($left);
		$('<legend></legend>').append (i18n.inheritance).appendTo ($fieldset);
		var $cont = $('<div class="form-item inheritance"></div>').appendTo ($fieldset);
		$('<label for="diderot-rights-inheritance-1"></label>').append (i18n.inherited_rights).appendTo ($cont);
		this.$inherited = $('<input name="inheritance" type="radio" value="1" id="diderot-rights-inheritance-1"/>').appendTo ($cont).click (function () {
			that.rights.attr.inherited = true;
			setRights.call (that);
		});
		var $cont = $('<div class="form-item inheritance"></div>').appendTo ($fieldset);
		$('<label for="diderot-rights-inheritance-0"></label>').append (i18n.not_inherited_rights).appendTo ($cont);
		this.$not_inherited = $('<input name="inheritance" type="radio" value="0" id="diderot-rights-inheritance-0"/>').appendTo ($cont).click (function () {
			that.rights.attr.inherited = false;
			setRights.call (that);
		});


		// --------------------------------------------------------
		// Profile access rights list
		// --------------------------------------------------------
		var $fieldset = $('<fieldset class="profile-rights"></fieldset>').appendTo ($right);
		$('<legend></legend>').append (i18n.profile_rights).appendTo ($fieldset);
		this.$profiles = $('<ul class="rights list highlight profile-rights"></ul>').appendTo ($fieldset);

		// --------------------------------------------------------
		// Team access rights list
		// --------------------------------------------------------
		this.$teams_fieldset = $('<fieldset class="team-rights"></fieldset>').appendTo ($left).hide ();
		$('<legend></legend>').append (i18n.team_rights).appendTo (this.$teams_fieldset);
		this.$teams = $('<ul class="rights list highlight team-rights"></ul>').appendTo (this.$teams_fieldset);
		// this.$add_teams_btn_cont = $('<ul class="button_list"></ul>').appendTo (this.$teams_fieldset);
		this.$add_teams_btn = $('<button class="button normal"></button>').append (i18n.add_teams).appendTo ($('<li></li>').prependTo (this.$button_bar)).click (function () {
			var args = {
				app_name: 'Diderot',
				single: false,
				i18n: {
					select_all: i18n.select_all,
					deselect_all: i18n.deselect_all,
					add_btn: i18n.add
				},
				data: function (arg) {
					var teams = $.grep (diderot.getTeams (), function (team) { return ($.grep (that.rights.attr.teams, function (team_with_right) { return (team.rowid === team_with_right.rowid) }).length === 0) });
					that.$form.hide ();
					that.$selector.show ();
					return (teams);
				},
				select_add: function (rowids) {
					MIOGA.logDebug ('DiderotAccessRightsEdit', 1, '[addTeams] Adding team rowids: ', rowids);

					// Append teams to access rights
					for (i=0; i<rowids.length; i++) {
						that.rights.attr.teams.push ({
							rowid: rowids[i],
							access: 0
						});
					}

					// Store access rights and refresh
					var res = that.rights.store ();
					if (res.success) {
						var message = (res.message !== undefined) ? res.message : i18n.specific_right_add_success;
						that.$message_box.inlineMessage ('info', message);

						var rights = new DiderotAccessRights (res.rights);
						that.refresh (rights, true);
					}
					else {
						displayError.call (this, res);
					}
				}
			};
			that.$selector.empty ().itemSelect (args);
			that.$button_bar.find ('.button').hide ();
			that.$back_btn.show ();
		});

		// --------------------------------------------------------
		// User access rights list
		// --------------------------------------------------------
		this.$users_fieldset = $('<fieldset class="user-rights"></fieldset>').appendTo ($right).hide ();
		$('<legend></legend>').append (i18n.user_rights).appendTo (this.$users_fieldset);
		this.$users = $('<ul class="rights list highlight user-rights"></ul>').appendTo (this.$users_fieldset);
		this.$add_users_btn = $('<button class="button normal"></button>').append (i18n.add_users).appendTo ($('<li></li>').prependTo (this.$button_bar)).click (function () {
			var args = {
				app_name: 'Diderot',
				single: false,
				i18n: {
					select_all: i18n.select_all,
					deselect_all: i18n.deselect_all,
					add_btn: i18n.add,
					columns: [
						{
							field: 'firstname',
							label: i18n.firstname
						},
						{
							field: 'lastname',
							label: i18n.lastname,
							Default: true
						},
						{
							field: 'email',
							label: i18n.email
						}
					]
				},
				data: $.grep (diderot.getUsers (), function (user) { return ($.grep (that.rights.attr.users, function (user_with_right) { return (user.rowid === user_with_right.rowid) }).length === 0) }),
				select_add: function (rowids) {
					MIOGA.logDebug ('DiderotAccessRightsEdit', 1, '[addUsers] Adding user rowids: ', rowids);
					// Append users to access rights
					for (var i=0; i<rowids.length; i++) {
						that.rights.attr.users.push ({
							rowid: rowids[i],
							access: 0
						});
					}
					// Store access rights and refresh
					var res = that.rights.store ();
					if (res.success) {
						var message = (res.message !== undefined) ? res.message : i18n.specific_right_add_success;
						that.$message_box.inlineMessage ('info', message);

						var rights = new DiderotAccessRights (res.rights);
						that.refresh (rights, true);
					}
					else {
						displayError.call (this, res);
					}
				}
			};
			that.$selector.show ().empty ().userSelect (args);
			that.$form.hide ();
			that.$selector.show ();
			that.$button_bar.find ('.button').hide ();
			that.$back_btn.show ();
		});

		// --------------------------------------------------------
		// Fill UI with data, if any
		// --------------------------------------------------------
		if (options.data) {
			rights = new DiderotAccessRights (options.data);
			this.refresh (rights);
		}

		MIOGA.logDebug ('DiderotAccessRightsEdit', 1, '[new] Leaving');
	}
