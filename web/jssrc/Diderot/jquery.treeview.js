// ========================================================
// jquery.treeview.js
// A treeview plugin
// Usage :
//     <ul class="treeview">
//         <li>...</li>
//         <li>
//             <span>....</span>
//             <ul>
//                 <li>...</li>
//             </ul>
//     </ul>
//
//     $('ul.treeview').treeview ();
// ========================================================
(function ($) {
	$.fn.treeview = function () {
		this.each (function (i, item) {
			var $elem = $(item);

			// Remove internally-added DOM nodes (in case of multiple calls to same list)
			$elem.find ('span.treeview-node-expander').remove ();

			// Set main class
			$elem.addClass ('treeview');

			// Find all 'li' nodes having a 'ul' child and add them 'node' and 'collapsable' classes
			$elem.find ('li:has(ul)').addClass ('node collapsable').prepend ($('<span class="treeview-node-expander ui-state-default ui-icon ui-icon-minus"></span>'));

			// Attach click event handler to immediate span child of nodes
			$elem.find ('span.treeview-node-expander').click (function (event) {
				$(this).parent ().toggleClass ('collapsable').find ('> ul').slideToggle ('fast');
				$(this).toggleClass ('ui-icon-minus').toggleClass ('ui-icon-plus');
			});

			// Toggle "selected" class on item click
			$elem.find ('li').off ('click').click (function (event) {
				if (event.target === event.currentTarget) {
					$elem.find ('li').removeClass ('selected');
					$(this).addClass ('selected');
				}
				return (true);
			});

		});

		return (this);
	};
}) (jQuery);
