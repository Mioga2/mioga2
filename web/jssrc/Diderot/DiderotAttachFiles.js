	// ========================================================================================================
	// DiderotAttachFiles object, notice editor (or viewer) UI
	// ========================================================================================================
	function DiderotAttachFiles (elem, options) {
		var defaults = {
		};

		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};


		// --------------------------------------------------------
		// refresh
		// Fill UI with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data, args) {
			MIOGA.logDebug ('DiderotAttachFiles', 1, '[refresh] Entering, data: ', data);
			MIOGA.logDebug ('DiderotAttachFiles', 1, '[refresh] args: ', args);

			var that = this;

			that.$list.empty ();
			if ((args.files !== undefined) && args.files.length) {
				$.each (args.files, function (index, file) {
				var relpath = file.replace (mioga_context.private_dav_uri + '/', '');
					var $entry = $('<li></li>').append (relpath).append ($('<input name="files" type="hidden"/>').val (file)).appendTo (that.$list);
					var $delete_icon = $('<div class="item-delete"></div>').appendTo ($entry).click (function () {
						$entry.remove ();
					});
					$entry.hover (function () {
						$delete_icon.toggle ();
					})
				});
			}

			var files_arg = '?files=' + args.files.join ('&files=');

			// Notice creation
			$('<a class="button"></a>').attr ('href', diderot.generateNavigationHash ('notice') + files_arg).append (i18n.create_notice).appendTo ($('<li></li>').appendTo (this.$actions.empty ()));

			// Existing notices
			new DiderotNoticeList (this.$notices, {
				i18n: i18n,
				diderot: diderot,
				notices: data.notices
			});
			this.$notices.find ('a').each (function (index, item) {
				var target = $(item).attr ('href');
				$(item).attr ('href', target + files_arg + '&mode=edit');
			});

			MIOGA.logDebug ('DiderotAttachFiles', 1, '[refresh] Leaving');
		};


		// ========================================================
		// Private methods
		// ========================================================


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DiderotAttachFiles', 1, '[new] Entering');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };

		var i18n = options.i18n;

		var diderot = options.diderot;
		this.options =  $.extend (true, {}, defaults, options);

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="diderot-attach-files"></div>').appendTo (this.elem);

		var $row = $('<div class="row"></div>').appendTo (this.$cont);

		var $left = $('<div class="span3"></div>').appendTo ($row);
		var $right = $('<div class="span9"></div>').appendTo ($row);

		// Title
		$('<h2></h2>').append (i18n.attach_files).appendTo ($left);

		// File list
		var $fieldset = $('<fieldset></fieldset>').append ($('<legend></legend>').append (i18n.files_to_attach)).appendTo ($('<div class="form"></div>').appendTo ($left));
		this.$list = $('<ul class="files"></ul>').appendTo ($fieldset);

		// --------------------------------------------------------
		// Help message
		// --------------------------------------------------------
		$('<p class="note"></p>').append (i18n.attach_files_help).appendTo ($right);

		// --------------------------------------------------------
		// Actions
		// --------------------------------------------------------
		this.$actions = $('<ul class="button_list"></ul>').appendTo ($right);

		// --------------------------------------------------------
		// Notices list
		// --------------------------------------------------------
		var $box = $('<div class="sbox"></div>').appendTo ($right);
		this.$box_title = $('<h2 class="title_bg_color"></h2>').append (i18n.select_existing_notice).appendTo ($box);
		this.$notices = $('<div class="content"></div>').appendTo ($box);

		MIOGA.logDebug ('DiderotAttachFiles', 1, '[new] Leaving');
	}
