	// ========================================================================================================
	// DiderotNoticeTemplateEdit object, Template editor UI
	// ========================================================================================================
	function DiderotNoticeTemplateEdit (elem, options) {
		var defaults = {
			field_types: ['string', 'text', 'value', 'values']
		};

		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();

			// Set order list width
			setElementWidth.call (that);
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};


		// --------------------------------------------------------
		// refresh
		// Fill UI with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data) {
			MIOGA.logDebug ('DiderotNoticeTemplateEdit', 1, '[refresh] Entering, data: ', data);

			var that = this;
			this.template = data;

			// Initialize actions menu
			diderot.generateMenu (that.$menu);

			if (mioga_context.rights.Animation) {
				// Remove previous errors
				$('span.error-message').remove ();

				// Template attributes
				this.$template_title.val (data.attr.title);
				this.$template_description.val (data.attr.description);

				// Template contents
				this.$list.extensibleList ({values: data.attr.fields, template: this.$el_template, updateCB: list_add_callback, removeCB: list_remove_callback, checkCB: list_check_callback, labels: that.options.field_labels, can_change: ['field-name']});
				this.$list.find ('ul').addClass ('highlight');

				// --------------------------------------------------------
				// Initialize fields order if initial data
				// --------------------------------------------------------
				$order.find ('ul').remove ();
				if (this.template.attr.order && this.template.attr.order.length) {
					$.each (data.attr.order, function (index, line) {
						var $line = $('<ul></ul>').appendTo ($order).sortable ({
							placeholder: 'placeholder',
							connectWith: '.field-order ul, .excluded-fields ul'
						});
						$.each (line, function (index, field) {
							var field_desc = that.template.getField (field);
							if (field_desc !== undefined) {
								$('<li></li>').append (field_desc['field-name']).attr ('field-id', field_desc['field-id']).appendTo ($line);
							}
						});
					});
				}

				// --------------------------------------------------------
				// Initialize hidden fields
				// --------------------------------------------------------
				var $exclude_list = $($exclude.find ('ul'));
				$exclude_list.empty ();
				var fields = this.template.getHiddenFields ();
				if (fields && fields.length) {
					$.each (fields, function (index, field) {
						$('<li></li>').attr ('field-id', field).append (that.template.getField (field)['field-name']).appendTo ($exclude_list);
					});
				}

				// --------------------------------------------------------
				// Static advanced-search fields
				// --------------------------------------------------------
				that.$static_search_fields.empty ();
				if (data.attr.fields && data.attr.fields.length) {
					$.each (data.attr.fields, function (index, field) {
						if ((field['field-type'] !== 'value') && (field['field-type'] !== 'values')) {
							var $item = $('<li></li>').appendTo (that.$static_search_fields);
							var $input = $('<input type="checkbox"/>').attr ('id', field['field-id']).appendTo ($item);
							$('<label></label>').attr ('for', field['field-id']).append (field['field-name']).appendTo ($item);
							if (field.static_search === true) {
								$input.attr ('checked', true);
							}
						}
					});
				}
			}

			MIOGA.logDebug ('DiderotNoticeTemplateEdit', 1, '[refresh] Leaving');
		};


		// ========================================================
		// Private methods
		// ========================================================

		// --------------------------------------------------------
		// Store notice template
		// --------------------------------------------------------
		function storeNoticeTemplate () {
			MIOGA.logDebug ('DiderotNoticeTemplateEdit', 1, '[storeNoticeTemplate] Entering');
			var that = this;

			// Get attributes
			this.template.attr.title = this.$template_title.val ();
			this.template.attr.description = this.$template_description.val ();

			if (this.template.attr.title !== '') {
				// Get fields
				this.template.attr.fields = this.$list.extensibleList ('getValues');

				// Get field order
				that.template.attr.order = [ ];
				$('.field-order ul').each (function (index, list) {
					var data = $(list).find ('li').map (function (index) { return $(this).attr ('field-id'); }).get();
					if ((data !== undefined) && (data.length > 0)) {
						that.template.attr.order.push (data);
					}
				});

				// Get static fields
				$.each (that.template.attr.fields, function (index, field) {
					if (that.$static_search_fields.find ('input#' + field['field-id']).is (':checked')) {
						field.static_search = true;
					}
					else {
						field.static_search = false;
					}
				});

				// Store template
				var res = this.template.store ();
				if (res.success) {
					var message = (res.message !== undefined) ? res.message : i18n.template_store_success;
					that.$message_box.inlineMessage ('info', message);
					var template = new DiderotNoticeTemplate (res.template);
					this.refresh (template);
				}
				else {
					displayError.call (this, res);
				}
			}

			MIOGA.logDebug ('DiderotNoticeTemplateEdit', 1, '[storeNoticeTemplate] Leaving');
			return (false);
		}


		// --------------------------------------------------------
		// Close current sub-expression and re-enable parent list
		// --------------------------------------------------------
		function closeSubExp ($list) {
			var that = this;

			// Disable active list
			$list.extensibleList ('disable');
			// Add edit button to re-enable list
			var $edit_btn = $('<div class="item-edit"></div>').attr ('title', i18n.edit_value_list).click (function () {
				$edit_btn.remove ();
				$list.extensibleList ('enable');

				// Sub-expression closer
				$('<button class="button"></button>').append (i18n.close_value_list).click (function () { closeSubExp.call (that, $list); return (false); }).appendTo ($('<div class="close-subexp"></div>').appendTo ($list));
			});
			$list.parent ().find ('div.field-type').append ($edit_btn);
			$list.find ('div.close-subexp').remove ();
		}


		// --------------------------------------------------------
		// displayError
		// display error message from WebService response
		// --------------------------------------------------------
		function displayError (res) {
			that.$message_box.inlineMessage ('error', res.message);
			$('span.error-message').remove ();
			$.each (res.errors, function (index, error) {
				MIOGA.logDebug ('DiderotNoticeTemplateEdit', 1, '[displayError] Server returned error for field ' + error.field);
				that.fields[error.field].append ('<span class="error-message">' + error.message + '</span>');
			});
		}


		// --------------------------------------------------------
		// createFieldID
		// Create a unique ID for internal reference to field
		// --------------------------------------------------------
		function createFieldID () {
			var newDate = new Date;
			return newDate.getTime();
		}


		// --------------------------------------------------------
		// setElementWidth
		// Set width for elements that can't be automatically sized
		// --------------------------------------------------------
		function setElementWidth () {
			var width = that.$cont.find ('fieldset.template-attributes').width ();
			that.order_list_width = width - $order.css ('borderLeftWidth').replace ('px', '') - $order.css ('borderRightWidth').replace ('px', '') - $order.css ('paddingLeft').replace ('px', '') - $order.css ('paddingRight').replace ('px', '') - $order.css ('marginLeft').replace ('px', '') - $order.css ('marginRight').replace ('px', '') - that.$form.css ('paddingLeft').replace ('px', '') - that.$form.css ('paddingRight').replace ('px', '');
			$order.width (width);
			$order.find ('ul').width (that.order_list_width);
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DiderotNoticeTemplateEdit', 1, '[new] Entering');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };

		var i18n = options.i18n;

		var diderot = options.diderot;
		this.options =  $.extend (true, {}, defaults, options);

		// Get field labels from i18n
		that.options.field_labels = { };
		that.options.field_labels['field-type'] = { };
		$.each (that.options.field_types, function (index, type) {
			that.options.field_labels['field-type'][type] = i18n['field_type_' + type + '_label'];
		});
		that.options.field_labels.mandatory = {
			'on': i18n.field_type_mandatory_on
		};

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="diderot-notice-template"></div>').appendTo (this.elem);


		// --------------------------------------------------------
		// Menu
		// --------------------------------------------------------
		this.$menu = $('<ul class="hmenu"></ul>').appendTo (this.$cont);

		// Message box
		this.$message_box = $('<div></div>').inlineMessage ().appendTo (this.$cont);


		// --------------------------------------------------------
		// Page contents
		// --------------------------------------------------------
		this.$page_contents = $('<div></div>').appendTo (this.$cont);

		// Form container
		if (mioga_context.rights.Animation) {
			var $form = $('<form class="form"></form>').submit (function () { return (storeNoticeTemplate.call (that)); }).appendTo (this.$page_contents);
			that.$form = $form;

			// Store button
			$('<button class="button"></button>').append (i18n.store_template).appendTo ($('<li></li>').appendTo ($('<ul class="button_list"></ul>').appendTo ($form)));

			// Template attributes
			var $fieldset = $('<fieldset class="template-attributes"></fieldset>').appendTo ($form);
			$('<legend></legend>').append (i18n.template_attributes).appendTo ($fieldset);
			var $entry = $('<div class="form-item"></div>').appendTo ($fieldset).append ($('<label>' + i18n.template_title + '</label>'));
			this.$template_title = $('<input type="text" name="title"/>').appendTo ($entry);
			this.fields.title = $entry;
			var $entry = $('<div class="form-item"></div>').appendTo ($fieldset).append ($('<label>' + i18n.template_description + '</label>'));
			this.$template_description = $('<textarea name="description"/>').appendTo ($entry);

			// Field list
			var $list_fieldset = $('<fieldset></fieldset>').appendTo ($form);
			$('<legend></legend>').append (i18n.notice_fields).appendTo ($list_fieldset);

			// Field order list
			var $order = $('<fieldset class="field-order"></fieldset>').appendTo ($form);
			$('<legend></legend>').append (i18n.notice_fields_order).appendTo ($order);
			$('<div class="btn-container"></div>').appendTo ($order).append ($('<button class="button"></button>').append (i18n.add_line).click (function () {
				$order.append ('<ul></ul>');
				$order.find ('ul').width (that.order_list_width).sortable ({
					placeholder: 'placeholder',
					connectWith: '.field-order ul, .excluded-fields ul'
				});
				return (false);
			}));
			$('<ul></ul>').appendTo ($order).sortable ({
				placeholder: 'placeholder',
				connectWith: '.field-order ul, .excluded-fields ul'
			});

			// Fields to be excluded from search results
			var $exclude = $('<fieldset class="excluded-fields"></fieldset>').appendTo ($form);
			$('<legend></legend>').append (i18n.notice_excluded_fields).appendTo ($exclude);
			$('<p class="note"></p>').append (i18n.notice_excluded_fields_help).appendTo ($exclude);
			$('<ul></ul>').appendTo ($exclude).sortable ({
				placeholder: 'placeholder',
				connectWith: '.field-order ul'
			});

			// Fields to be displayed static on search form
			var $static_search_fields = $('<fieldset class="static-fields"></fieldset>').appendTo ($form);
			$('<legend></legend>').append (i18n.notice_static_fields).appendTo ($static_search_fields);
			this.$static_search_fields = $('<ul></ul>').appendTo ($static_search_fields);


			// --------------------------------------------------------
			// Create extensibleList template
			// --------------------------------------------------------
			this.$el_template = $('<span class="notice-template-template"></span>');
			$('<input name="field-id" type="hidden"/>').val (createFieldID.call (that)).appendTo (this.$el_template);
			var $cont = $('<div class="form-item"></div>').appendTo (this.$el_template);
			$('<label for="field-name"></label>').append (i18n.notice_field_name).appendTo ($cont);
			$('<input name="field-name" type="text"></input>').appendTo ($cont);
			var $cont = $('<div class="form-item"></div>').appendTo (this.$el_template);
			$('<label for="field-type"></label>').append (i18n.notice_field_type).appendTo ($cont);
			var $types = $('<select name="field-type"></select>').appendTo ($cont);
			$.each (this.options.field_types, function (index, field_type) {
				$('<option></option>').attr ('value', field_type).append (i18n['field_type_' + field_type + '_label']).appendTo ($types);
			});
			var $cont = $('<div class="form-item"></div>').appendTo (this.$el_template);
			$('<input name="mandatory" id="mandatory" type="checkbox"/>').appendTo ($cont);
			$('<label for="mandatory"></label>').append (i18n.mandatory).appendTo ($cont);
			var $cont = $('<div class="form-item"></div>').appendTo (this.$el_template);
			$('<label for="description"></label>').append (i18n.description).appendTo ($cont);
			$('<textarea name="description"/>').appendTo ($cont);


			// --------------------------------------------------------
			// Callback to be executed when item is added to list.
			// Handles sublists if any
			// --------------------------------------------------------
			var list_add_callback = function (elem, data) {
				if ((data['field-type'] === 'value') || (data['field-type'] === 'values')) {
					MIOGA.logDebug ('DiderotNoticeTemplateEdit', 1, 'Creating sub-expression');
					var values;
					if (data['values']) {
						values = data['values'];
					}

					// Create template and sublist
					var $el_template = $('<span class="notice-template-template"><input name="field-id" type="hidden"/><input name="field-name"/></span>');
					var updateCB = function (elem, data) {
						// Generate field ID for next entry
						$(elem).find ('input[name=field-id]').val (createFieldID.call (that));
					};
					var $list = $('<div name="values" class="el-field values"></div>').appendTo ($(elem).find ('ul:first li:last')).extensibleList ({values: values, template: $el_template, updateCB: updateCB, checkCB: list_check_callback});
					$list.find ('ul').addClass ('highlight');

					// Add close button or directly close sub-expression according to the presence of values (non-empty sublist creation) or not (empty sublist creation)
					if (values === undefined) {
						// Sub-expression closer
						$('<button class="button"></button>').append (i18n.close_value_list).click (function () { closeSubExp.call (that, $list); return (false); }).appendTo ($('<div class="close-subexp"></div>').appendTo ($list));
					}
					else {
						closeSubExp.call (that, $list);
					}
				}

				// Generate field ID for next entry
				$(elem).find ('input[name=field-id]').val (createFieldID.call (that));

				// Add field to last list of the order block
				$('<li></li>').append (data['field-name']).attr ('field-id', data['field-id']).appendTo ($order.find ('ul:last'));

				// Add field to list of static search fields
				if ((data['field-type'] !== 'value') && (data['field-type'] !== 'values')) {
					var $item = $('<li></li>').appendTo (that.$static_search_fields);
					$('<input type="checkbox"/>').attr ('id', data['field-id']).appendTo ($item);
					$('<label></label>').attr ('for', data['field-id']).append (data['field-name']).appendTo ($item);
				}
			};


			// --------------------------------------------------------
			// Callback checking extensibleList values before insertion
			// --------------------------------------------------------
			var list_check_callback = function (data) {
				var res = true;
				if (data['field-name'] === "") {
					res &= false;
				}
				return (res);
			};


			// --------------------------------------------------------
			// Callback to be executed when item is removed from list.
			// --------------------------------------------------------
			var list_remove_callback = function (elem, list_data, list_elem) {
				if ($.grep (that.template.attr.used_fields, function (field_id) { return (list_elem['field-id'] === field_id.toString ()); }).length) {
					return (false);
				}
				// Remove items from fields order
				$order.find ('li').each (function (index, item) {
					if ($.grep (list_data, function (value) { return (value['field-id'] === $(item).attr ('field-id')); }).length === 0) {
						$(item).remove ();
					}
				});
			};


			// --------------------------------------------------------
			// Create extensibleList
			// --------------------------------------------------------
			this.$list = $('<div></div>').appendTo ($list_fieldset);


			// --------------------------------------------------------
			// Fill UI with data, if any
			// --------------------------------------------------------
			if (options.data) {
				template = new DiderotNoticeTemplate (options.data);
				this.refresh (template);
			}

			$(window).on ('resize', function () {
				if (that.$cont.is (':visible')) {
					setElementWidth.call (that);
				}
			});
		}
		else {
			$('<p class="note"></p>').append (i18n.no_notice_template_yet).appendTo (this.$page_contents);
		}


		MIOGA.logDebug ('DiderotNoticeTemplateEdit', 1, '[new] Leaving');
	}
