	// ========================================================================================================
	// DiderotMainPage object, main UI
	// ========================================================================================================
	function DiderotMainPage (elem, options) {
		var defaults = {
		};

		// ========================================================
		// Public methods
		// ========================================================

		// --------------------------------------------------------
		// Show UI
		// --------------------------------------------------------
		this.show = show;
		function show () {
			this.$cont.show ();
		};

		// --------------------------------------------------------
		// Hide UI
		// --------------------------------------------------------
		this.hide = hide;
		function hide () {
			this.$cont.hide ();
		};


		// --------------------------------------------------------
		// refresh
		// Fill UI with data
		// --------------------------------------------------------
		this.refresh = refresh;
		function refresh (data, args) {
			MIOGA.logDebug ('DiderotMainPage', 1, '[refresh] Entering, data: ', data);
			MIOGA.logDebug ('DiderotMainPage', 1, '[refresh] args: ', args);

			var that = this;
			this.data = data;

			// Hide form elements
			this.$subcat_creator.hide ();
			this.$cat_renamer.hide ();

			// Set user ACL to current view
			var access = data.access;
			if (data.category) {
				var current_category = $($.grep (data.categories, function (category) { return (category.attr.rowid === data.category); })).map (function () { return (this); })[0];
				access = current_category.attr.access;
			}

			// Initialize actions menu
			diderot.generateMenu (that.$menu, access);

			if (data.notice !== undefined) {
				// Notice display
				this.$notice_list.hide ();
				this.$noticeEdit.refresh (data.notice, args);
				this.$noticeEdit.show ();
			}
			else {
				var current_category = $.grep (data.categories, function (category) { return (category.attr.rowid === data.category); })[0];

				// Actions
				this.$actions.empty();

				// Add link to all notices
				if (data.category === undefined) {
					$('<a></a>').attr ('href', diderot.generateNavigationHash ('all_notices')).append (i18n.all_notices).appendTo ($('<li></li>').appendTo (this.$actions));
				}

				if (access === diderot.CONSTANT.ACCESS.WRITE) {
					// A category can be created if:
					// 		- user has sufficient rights
					$('<a href="#"></a>').append (i18n.create_category).appendTo ($('<li></li>').appendTo (this.$actions)).click (function () {
						that.$actions.find ('li').removeClass ('selected');
						that.$cat_renamer.hide ();
						that.$subcat_creator.toggle ();
						if (that.$subcat_creator.is (':visible')) {
							$(this).parent ().addClass ('selected');
							that.$subcat_creator.find ('input').val ('').focus ();
						}
						else {
							$(this).parent ().removeClass ('selected');
						}
						return (false);
					});

					// A category can be renamed if:
					// 		- user has sufficient rights,
					// 		- a category is selected
					if (data.category) {
						$('<a href="#"></a>').append (i18n.rename_category).appendTo ($('<li></li>').appendTo (this.$actions)).click (function () {
							that.$actions.find ('li').removeClass ('selected');
							that.$subcat_creator.hide ();
							that.$cat_renamer.toggle ();
							if (that.$cat_renamer.is (':visible')) {
								$(this).parent ().addClass ('selected');
								that.$cat_renamer.find ('input[name=rowid]').val (current_category.attr.rowid);
								that.$cat_renamer.find ('input[name=ident]').val (current_category.attr.ident).focus ().select ();
							}
							else {
								$(this).parent ().removeClass ('selected');
							}
							return (false);
						});
					}

					// A category can be deleted if:
					// 		- user has sufficient rights,
					// 		- it has no notice attached,
					// 		- it has no sub-categories
					if ((data.category !== undefined) && data.notices && !data.notices.length && !$.grep (data.categories, function (category) { return ((category.attr.parent_id === data.category) && (category.attr.rowid != data.category)); }).length) {
						var target = diderot.generateNavigationHash ('home');
						if ((data.category !== undefined) && (current_category.attr.parent_id !== data.category)) {
							target = diderot.generateNavigationHash ('category', current_category.attr.parent_id);
						}
						$('<a></a>').attr ('href', target).append (i18n.delete_category).appendTo ($('<li></li>').appendTo (this.$actions)).click (function () {
							return (deleteCategory.call (that, current_category));
						});
					}
				}

				if (mioga_context.rights.Animation) {
					// Access-rights management
					$('<a href="#"></a>').append (i18n.manage_access_rights).appendTo ($('<li></li>').appendTo (this.$actions)).click (function () {
						var category_name = (data.category !== undefined) ? current_category.attr.ident : root_node_title;
						var title = i18n.access_rights.replace (/\$name\$/, category_name);
						var $dialog_contents = $('<div></div>').attr ('title', title);
						var $access_rights = $('<div></div>').appendTo ($dialog_contents);

						// Dialog navigation buttons
						var $dialog_btns = $('<ul class="button_list dialog-buttons"></ul>').appendTo ($dialog_contents);
						var $close_btn = $('<button class="button cancel"></button>').append (i18n.close).appendTo ($('<li></li>').appendTo ($dialog_btns));
						$close_btn.click (function () {
//							$access_rights.parents ('.ui-dialog').find (".ui-dialog-titlebar-close").trigger('click');
							$dialog_contents.dialog('close').remove();
							return (false);
						});

						// Access rights
						new DiderotAccessRightsEdit ($access_rights, {
							i18n: i18n,
							diderot: diderot,
							data: {
								type: 'category',
								rowid: data.category
							}
						});
						$dialog_contents.dialog ({
							modal: true,
							resizable: false,
							width: 'auto',
							dialogClass: 'diderot-dialog'
						});
						return (false);
					});
				}

				// Set correct class to notice list
				if (data.category !== undefined) {
					this.$recent.removeClass ('recent-notices').addClass ('category-notices');
				}

				// Recent notices list
				new DiderotNoticeList (this.$recent, {
					i18n: i18n,
					diderot: diderot,
					notices: data.notices
				});
				this.$recent.tableScrollable ();

				// Set box title
				if (data.category !== undefined) {
					var category_name = $($.grep (data.categories, function (category) { return (category.attr.rowid === data.category); })).map (function () { return (this.attr.ident); }).get ();
					var title = i18n.notices_in_category.replace (/\$category\$/, category_name);
					this.$box_title.empty ().append (title);
					this.$uncategorized_box.hide ();

					// Notices can be dragged and dropped to categories
					this.$recent.find ('tr.diderot-notice-access-' + diderot.CONSTANT.ACCESS.WRITE + ' a').draggable (that.options.draggable_opts);
				}
				else {
					this.$box_title.empty ().append (i18n.recently_modified_notices);

					// Uncategorized notices list
					new DiderotNoticeList (this.$uncategorized, {
						i18n: i18n,
						diderot: diderot,
						notices: data.uncategorized
					});
					this.$uncategorized_box.show ();

					// Notices can be dragged and dropped to categories
					this.$uncategorized.find ('tr.diderot-notice-access-' + diderot.CONSTANT.ACCESS.WRITE + ' a').draggable (that.options.draggable_opts);
					this.$uncategorized.tableScrollable ();

					this.$recent.removeClass ('category-notices').addClass ('recent-notices');
				}

				this.$noticeEdit.hide ();
				this.$notice_list.show ();
			}

			// Categories
			if (data.categories !== undefined) {
				// Root node title
				var root_node_title = mioga_context.group.ident;
				if (this.data.notices && this.data.notices.length) {
					root_node_title = this.data.notices[0].attr.template.attr.title;
				}
				else if (this.data.notice) {
					root_node_title = this.data.notice.attr.template.attr.title;
				}
				else {
					var template = diderot.getTemplate ();
					if (template.attr.title) {
						root_node_title = template.attr.title;
					}
				}

				this.$categories.empty ();
				var $nodes = { };
				var categories = data.categories;
				var $root_node = $('<li class="diderot-category-0"></li>').addClass ('diderot-category-access-' + data.access).append ($('<a class="root-node"></a>').append (root_node_title).attr ('href', diderot.generateNavigationHash ('home'))).appendTo (this.$categories).append ('<ul></ul>');
				while (categories && categories.length) {
					var postponed = [ ];
					for (var i = 0; i < categories.length; i++) {
						var category = categories[i];

						// Get correct parent node (according to category parent_id)
						var $parent_node = $root_node.find ('>ul');
						if (category.attr.parent_id !== category.attr.rowid) {
							$parent_node = $nodes[category.attr.parent_id];

							// Postpone entry if:
							// 		- parent node does not exist yet,
							// 		- some children of parent node are already postponed
							if (($parent_node === undefined) || ($.grep (postponed, function (postponed_cat) { return (postponed_cat.attr.parent_id === category.attr.parent_id) }).length)) {
								postponed.push (category);
								continue;
							}

							// Create sub-list container or grab already-existing one
							if (!$parent_node.find ('> ul').length) {
								$parent_node = $('<ul></ul>').appendTo ($parent_node);
							}
							else {
								$parent_node = $parent_node.find ('> ul');
							}
						}

						// Append current node
						var $node = $('<li></li>').addClass ('diderot-category-' + category.attr.rowid).addClass ('diderot-category-access-' + category.attr.access).append ($('<a></a>').attr ('href', diderot.generateNavigationHash ('category', category.attr.rowid)).append (category.attr.ident + ' (' + category.attr.notices + ' ' + i18n.notices + ')')).appendTo ($parent_node);
						$nodes[category.attr.rowid] = $node;
					}

					// Store postponed for next loop
					categories = postponed;
				}

				// Select current category
				if (this.data.category) {
					$('li.diderot-category-' + this.data.category).addClass ('selected');
				}

				// Refresh treeview
				this.$categories.treeview ();

				// Add drag 'n' drop to categories if user is allowed
				this.$categories.find ('li.diderot-category-access-' + diderot.CONSTANT.ACCESS.WRITE + '').draggable (that.options.draggable_opts);
				$(this.$categories.find ('li.diderot-category-access-' + diderot.CONSTANT.ACCESS.WRITE + '').get ().reverse ()).droppable ({
					greedy: true,
					hoverClass: 'ui-state-hover',
					drop: function (event, ui) {
						// Get base href from application
						var category_base = diderot.generateNavigationHash ('category', '');
						var re = new RegExp('^' + category_base);

						if ($(event.target).attr ('href').match (re)) {
							// dropped a category, re-parent it
							var category_id = parseInt ($(event.target).attr ('href').replace (category_base, ''), 10);
							var parent_id = parseInt ($(this).find ('a:first').attr ('href').replace (category_base, ''), 10);
							if (isNaN (parent_id)) {
								// Dropped to root category
								parent_id = 0;
							}
							MIOGA.logDebug ('DiderotMainPage', 1, '[categoryDrop] Re-parenting category ' + category_id + ' to category ' + parent_id);

							// Move category
							var category = new DiderotNoticeCategory ({rowid: category_id});
							moveCategory.call (that, category, parent_id);
						}
						else {
							// Dropped a notice, reparent it
							var notice_base = diderot.generateNavigationHash ('notice', '');
							var notice_id = parseInt ($(event.target).attr ('href').replace (notice_base, ''), 10);
							var category_id = parseInt ($(this).find ('a:first').attr ('href').replace (category_base, ''), 10);
							if (isNaN (category_id)) {
								// Dropped to root category
								category_id = undefined;
							}
							MIOGA.logDebug ('DiderotMainPage', 1, '[categoryDrop] Re-parenting notice ' + notice_id + ' to category ' + category_id);

							// Move notice
							var notice = new DiderotNotice ({rowid: notice_id});
							moveNotice.call (that, notice, category_id);
						}
					}
				});
			}

			MIOGA.logDebug ('DiderotMainPage', 1, '[refresh] Leaving');
		};


		// ========================================================
		// Private methods
		// ========================================================


		// --------------------------------------------------------
		// createCategory
		// Create a category
		// --------------------------------------------------------
		function createCategory () {
			MIOGA.logDebug ('DiderotMainPage', 1, '[createCategory] Entering');

			var ident = that.$subcat_creator.find ('input').val ();

			if (ident !== '') {
				var category = new DiderotNoticeCategory ({
					ident: ident,
					parent_id: that.data.category
				});
				var res = category.store ();
				if (res.success) {
					var message = (res.message !== undefined) ? res.message : i18n.subcat_store_success;
					that.$message_box.inlineMessage ('info', message);

					// Store new category list
					var data = this.data;
					data.categories = res.categories;
					data.categories.sort (categorySortComparator);

					// Hide category creator
					this.$subcat_creator.hide ();
					this.$actions.find ('li').removeClass ('selected');

					this.refresh (data);
				}
				else {
					displayError.call (this, res);
				}
			}

			MIOGA.logDebug ('DiderotMainPage', 1, '[createCategory] Leaving');
		}


		// --------------------------------------------------------
		// moveCategory
		// Change a category's parent
		// --------------------------------------------------------
		function moveCategory (category, parent_id) {
			MIOGA.logDebug ('DiderotMainPage', 1, '[moveCategory] Entering, category: ' + category.attr.rowid + ', new parent: ' + parent_id);

			category.attr.parent_id = parent_id;
			var res = category.store ();
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.category_destroy_success;
				that.$message_box.inlineMessage ('info', message);

				// Store new category list
				var data = this.data;
				data.categories = res.categories;
				data.categories.sort (categorySortComparator);

				// Postpone refresh so that droppable can finish its execution before
				setTimeout (function () { that.refresh (data); }, 500);
			}
			else {
				displayError.call (this, res);
			}

			MIOGA.logDebug ('DiderotMainPage', 1, '[moveCategory] Leaving');
		}


		// --------------------------------------------------------
		// moveNotice
		// Change a notice's category
		// --------------------------------------------------------
		function moveNotice (notice, category_id) {
			MIOGA.logDebug ('DiderotMainPage', 1, '[moveNotice] Entering, notice: ' + notice.attr.rowid + ', new category: ' + category_id);

			notice.attr.category_id = category_id;
			var res = notice.store ();
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.notice_category_change_success;
				that.$message_box.inlineMessage ('info', message);

				// Postpone refresh so that droppable can finish its execution before
				setTimeout (function () { diderot.refresh (); }, 500);
			}
			else {
				displayError.call (this, res);
			}

			MIOGA.logDebug ('DiderotMainPage', 1, '[moveNotice] Leaving');
		}


		// --------------------------------------------------------
		// renameCategory
		// Delete a category
		// --------------------------------------------------------
		function renameCategory () {
			MIOGA.logDebug ('DiderotMainPage', 1, '[renameCategory] Entering');

			var rowid = parseInt (this.$cat_renamer.find ('input[name=rowid]').val (), 10);
			var ident = this.$cat_renamer.find ('input[name=ident]').val ();

			if (ident !== '') {
				// Load and update category
				var category = new DiderotNoticeCategory ({rowid: rowid});
				category.attr.ident = ident;

				var res = category.store ();
				if (res.success) {
					var message = (res.message !== undefined) ? res.message : i18n.category_rename_success;
					that.$message_box.inlineMessage ('info', message);

					// Store new category list
					var data = this.data;
					data.categories = res.categories;
					data.categories.sort (categorySortComparator);

					this.refresh (data);
				}
				else {
					displayError.call (this, res);
				}
			}

			MIOGA.logDebug ('DiderotMainPage', 1, '[renameCategory] Leaving');
		}


		// --------------------------------------------------------
		// deleteCategory
		// Delete a category
		// --------------------------------------------------------
		function deleteCategory (category) {
			MIOGA.logDebug ('DiderotMainPage', 1, '[deleteCategory] Entering');

			// Default return value, target link (to DisplayMain) will be followed unless deletion fails
			var follow_link = true;

			var res = category.destroy ();
			if (res.success) {
				var message = (res.message !== undefined) ? res.message : i18n.category_destroy_success;
				that.$message_box.inlineMessage ('info', message);

				// Store new category list
				var data = this.data;
				data.categories = res.categories;
				data.categories.sort (categorySortComparator);

				this.refresh (data);
			}
			else {
				follow_link = false;
				displayError.call (this, res);
			}

			MIOGA.logDebug ('DiderotMainPage', 1, '[deleteCategory] Leaving');
			return (follow_link);
		}


		// --------------------------------------------------------
		// categorySortComparator
		// Sort categories by name
		// --------------------------------------------------------
		function categorySortComparator (a, b) {
			if (a.attr.ident.toString ().toLowerCase () < b.attr.ident.toString ().toLowerCase ()) return (-1);
			if (a.attr.ident.toString ().toLowerCase () > b.attr.ident.toString ().toLowerCase ()) return (1);
			return (0);
		};


		// --------------------------------------------------------
		// displayError
		// display error message from WebService response
		// --------------------------------------------------------
		function displayError (res) {
			that.$message_box.inlineMessage ('error', res.message);
		}


		// ========================================================
		// Constructor
		// ========================================================
		MIOGA.logDebug ('DiderotMainPage', 1, '[new] Entering');
		var that = this; // For callback reference to current object
		this.elem       = elem;
		this.fields     = { };

		var i18n = options.i18n;

		var diderot = options.diderot;
		this.options =  $.extend (true, {}, defaults, options);

		// Dragging fails on IE if "distance" argument is set
		if ($.browser.msie) {
			this.options.draggable_opts = { revert: "invalid", helper: "clone", cursor: "move" };
		}
		else {
			this.options.draggable_opts = { revert: "invalid", helper: "clone", cursor: "move", distance: 20 };
		}

		// ========================================================
		// Create empty UI
		// ========================================================
		this.$cont = $('<div class="diderot-main-page"></div>').appendTo (this.elem);


		// --------------------------------------------------------
		// Menu
		// --------------------------------------------------------
		this.$menu = $('<ul class="hmenu main-menu"></ul>').appendTo (this.$cont);

		// Message box
		this.$message_box = $('<div></div>').inlineMessage ().appendTo (this.$cont);


		// --------------------------------------------------------
		// Category tree
		// --------------------------------------------------------
		// var $cell = $('<div></div>').appendTo ($row);	// This div allows div.span4 below to have div.span4:first-child CSS rules applied, needed because of fixed positioning
		$cell = $('<div class="category-tree"></div>').appendTo (this.$cont);
		this.$categories = $('<ul></ul>').appendTo ($cell);


		// --------------------------------------------------------
		// Page contents
		// --------------------------------------------------------
		this.$page_contents = $('<div class="page-contents"></div>').appendTo (this.$cont);


		// --------------------------------------------------------
		// Notice list view
		// --------------------------------------------------------
		this.$notice_list = $('<div class="diderot-notice-list"></div>').appendTo (this.$page_contents);


		// --------------------------------------------------------
		// Action buttons container
		// --------------------------------------------------------
		this.$actions = $('<ul class="actions hmenu"></ul>').appendTo (this.$notice_list);


		// --------------------------------------------------------
		// Category creation form
		// --------------------------------------------------------
		this.$subcat_creator = $('<form class="form form-block new-category"></form>').submit (function () { return (false); }).appendTo (this.$notice_list).hide ();
		$('<div class="form-item"></div>').append ($('<label for="category_name"></label>').append (i18n.category_name)).append ($('<input type="text" name="category_name"/>')).appendTo (this.$subcat_creator);
		var $btn_list = $('<ul class="button_list"></ul>').appendTo (this.$subcat_creator);
		$('<button class="button"></button>').append (i18n.create).click (function () { createCategory.call (that); }).appendTo ($('<li></li>').appendTo ($btn_list));
		$('<button class="button cancel"></button>').append (i18n.cancel).click (function () { that.$subcat_creator.hide (); that.$actions.find ('li').removeClass ('selected'); }).appendTo ($('<li></li>').appendTo ($btn_list));


		// --------------------------------------------------------
		// Category rename form
		// --------------------------------------------------------
		this.$cat_renamer = $('<form class="form form-block rename-category"></form>').submit (function () { return (false); }).appendTo (this.$notice_list).hide ();
		$('<input name="rowid" type="hidden"/>').appendTo (this.$cat_renamer);
		$('<div class="form-item"></div>').append ($('<label for="ident"></label>').append (i18n.category_name)).append ($('<input type="text" name="ident"/>')).appendTo (this.$cat_renamer);
		var $btn_list = $('<ul class="button_list"></ul>').appendTo (this.$cat_renamer);
		$('<button class="button"></button>').append (i18n.rename).click (function () { renameCategory.call (that); }).appendTo ($('<li></li>').appendTo ($btn_list));
		$('<button class="button cancel"></button>').append (i18n.cancel).click (function () { that.$cat_renamer.hide (); that.$actions.find ('li').removeClass ('selected'); }).appendTo ($('<li></li>').appendTo ($btn_list));


		// --------------------------------------------------------
		// Recent notices list
		// --------------------------------------------------------
		this.$box_title = $('<h2></h2>').appendTo (this.$notice_list);
		this.$recent = $('<div class="fullsize"></div>').appendTo ($('<div class="notice-list"></div>').appendTo (this.$notice_list));

		// --------------------------------------------------------
		// Uncategorized notices list
		// --------------------------------------------------------
		this.$uncategorized_box = $('<div></div>').appendTo (this.$notice_list);
		$('<h2></h2>').append (i18n.uncategorized_notices).appendTo (this.$uncategorized_box);
		this.$uncategorized = $('<div class="fullsize"></div>').appendTo ($('<div class="notice-list uncategorized-notices"></div>').appendTo (this.$uncategorized_box));

		// --------------------------------------------------------
		// Notice display / edit
		// --------------------------------------------------------
		this.$noticeEdit = new DiderotNoticeEdit (this.$page_contents, {
			diderot: diderot,
			parent: this,
			i18n: options.i18n
		});
		this.$noticeEdit.hide ();


		MIOGA.logDebug ('DiderotMainPage', 1, '[new] Leaving');
	}
