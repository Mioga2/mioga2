//>>built
define({root:
({
	deleteCategory:'Do you really want to delete the category "%CATEGORY%"?',
	deleteEntry:'Do you really want to delete the entry\n"%TITLE%"?',
	titleLabel: 'Title:',
	questionLabel: 'Question:',
	answerLabel: 'Answer:',
	saveEntryLabel: 'Save',
	cancelEntryLabel: 'Cancel',
	fillAllFields: 'Please fill out all fields.',
	errorLabel: 'Error'
}),
"fr":1,
"en":1
});
