//>>built
define({root:
({
	Identifier: "Identifier",
	Firstname: "Firstname",
	Lastname: "Lastname",
	Email: "E-mail",
	Password: "Password",
	ConfirmPassword: "Confirm password",
	Skeleton: "Skeleton",
	Language: "Language",
	Autonomous: "Autonomous",
	Shared: "Shared",
	Status: "Status",
	Active: "Active",
	Disabled: "Disabled",
	DeletedNotPurged: "Deleted Not Purged",
	DisabledAttacked: "Disabled Attacked"
})
,
"fr":1,
"en":1
});
