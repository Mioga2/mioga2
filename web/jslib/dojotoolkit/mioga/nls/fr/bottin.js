//>>built
define(
({
	firstname: "Prénom",
	lastname: "Nom",
	email: "E-mail",
	password: "Mot de passe",
	password2: "Mot de passe (2)",
	disallow_empty: "La valeur ne peut être vide",
	want_email: "L'adresse e-mail valeur n'est pas valide",
	unused_email: "L'adresse e-mail existe déjà"
})

);