//>>built
define({root:
({
	firstname: "Firstname",
	lastname: "Lastname",
	email: "Email",
	password: "Password",
	password2: "Password (2)",
	disallow_empty: "The field can not be empty",
	want_email: "Invalid email address",
	unused_email: "Email address already used"
})
,
"en":1,
"fr":1
});
