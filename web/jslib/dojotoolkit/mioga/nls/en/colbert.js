//>>built
define(
({
	firstname: "Firstname",
	lastname: "Lastname",
	email: "E-mail",
	password: "Password",
	password2: "Confirm password",
	skeleton: "Skeleton",
	lang: "Language",
	anim_id: "Animator",
	ident: "Identifier",
	nb_users: "Users",
	nb_user: "Users",
	nb_groups: 'Groups',
	nb_teams: "Teams",
	server: "Server",
	label: "Name",
	name: "Name",
	default_app: "Default application",
	disallow_empty: "The field is empty",
	different_passwords: "Passwords do not match",
	want_email: "E-mail is invalid",
	already_used_email: "Email address is already used",
	valid_ident: "Identifier is invalid",
	max: "Max length exceeded",
	unused_email: "E-mail is already used",
	not_used_group_ident: "Ident is already used",
	bgcolor: "Background color",
	fgcolor: "Foreground color",
	TaskCatDelMsg: "You must associated tasks first.",
	ExtMiogaDelMsg: "You must delete all users from this external Mioga first.",
	location: "Location",
	theme_already_exists: "A theme with this identifier already exists",
	cannot_create_theme: "Can not create theme",
	ThemeDelMsg: "Theme is in use, can not delete it.",
	ThemeDefaultMsg: "Theme is default for instance, can not delete it.",
	DeleteUserErrorHeader: "The following users could not be deleted:",
	is_animator: "is group or resource animator.",
	applications: "Applications",
	cannot_disable_Colbert: "Colbert application can not be disabled for instance administrator.",
	cannot_disable_AnimGroup: "Animation application can not be disabled.",
	cannot_disable_Workspace: "Workspace application can not be disabled.",
	cannot_disable_AnimUser: "Home management application can not be disabled.",
	cannot_disable_Narkissos: "Narkissos application can not be disabled.",
	cannot_disable_default_app: "Group's default application can not be disabled.",
	name: "Name",
	generic_error_title: "Internal error",
	generic_error: "An internal error has occured, please contact your Mioga2 administrator and give him the information below.",
	password_policy: "Password does not match security policy.",
	pwd_min_length: "Minimum length: ",
	pwd_min_letter: "Minimum number of letters: ",
	pwd_min_digit: "Minimum number of digits: ",
	pwd_min_special: "Minimum number of special characters: ",
	pwd_min_chcase: "Minimum number of case changes: ",
	over_quota: "Maximum number reached, you can not create any more groups.",
	GroupDelMsg: "You can not delete instance's default group.",
	pwd_min_length: "Minimum password length",
	pwd_min_letter: "Minimum number of letters",
	pwd_min_digit: "Minimum number of digits",
	pwd_min_special: "Minimum number of special characters",
	pwd_min_chcase: "Minimum number of case changes",
	use_secret_question: "Secure passwords with a secret question",
	want_int: "Incorrect numeric value",
	UserIsAnimator: "A group animator can not be deleted, please update your groups configuration and try again.",
	cannot_add: "Can not invite user",
	forbidden: "Forbidden",
	membership: "Membership",
	authorization: "Authorization",
	parse_error: "Parse error",
	operation_rollback: "The operation failed and has been rolled back.",
	show_journal: "See the journal.",
	journal: "Journal of operations",
	show_password_policy: "Show security policy"
})

);