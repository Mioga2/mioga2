//>>built
define("mioga/GroupAttributes", [
	"dojo/_base/declare", // declare
	"dojo/dom-attr", // domAttr.set
	"dojo/_base/event", // event.stop
	"dojo/_base/kernel", // kernel.deprecated
	"dojo/_base/sniff", // has("ie")
	"dijit/_Widget",
	"dijit/_Templated",
	"dijit/form/_FormMixin"
], function(declare, domAttr, event, kernel, has, _Widget, _Templated, _FormMixin){

/* ----------------------------------------------------------
 * mioga.GroupAttributes
 * ---------------------------------------------------------- */
	return declare("mioga.GroupAttributes", [_Widget, _Templated, _FormMixin], {
	image: '',
	store: null,
	templateString:"<div class=\"group-attributes\">\n\t<div dojoType=\"dijit.form.Form\" id=\"group_attributes_form\" class=\"dojo-form\">\n\t\t<div class=\"group-attributes-left-align\">\n\t\t\t<input dojoType=\"dijit.form.TextBox\" type=\"hidden\" id=\"group_attributes_rowid\" name=\"rowid\"/>\n\n\t\t\t<div class=\"form-item\">\n\t\t\t\t<label id=\"group_attributes_ident_label\" for=\"ident\" i18n=\"Identifier\">Identifier</label>\n\t\t\t\t<input dojoType=\"dijit.form.TextBox\" class=\"group-attributes-input\" id=\"group_attributes_ident\" name=\"ident\"/>\n\t\t\t</div>\n\n\t\t\t<div class=\"form-item\">\n\t\t\t\t<label id=\"group_attributes_description_label\" for=\"description\" i18n=\"Description\">Description</label>\n\t\t\t\t<input dojoType=\"dijit.form.Textarea\" class=\"group-attributes-input\" id=\"group_attributes_description\" name=\"description\"/>\n\t\t\t</div>\n\n\t\t\t<div class=\"form-item\">\n\t\t\t\t<label id=\"group_attributes_lang_label\" for=\"lang\" i18n=\"Language\">Language</label>\n\t\t\t\t<div dojoType=\"mioga.data.ItemFileReadStore\" jsId=\"langStore\" url=\"GetLanguages.json\" nodename=\"language\" identifier=\"ident\"></div>\n\t\t\t\t<input dojoType=\"dijit.form.FilteringSelect\" class=\"group-attributes-input\" id=\"group_attributes_lang\" name=\"lang\" store=\"langStore\" searchAttr=\"label\"/>\n\t\t\t</div>\n\n\t\t\t<div class=\"form-item\">\n\t\t\t\t<label id=\"group_attributes_anim_id_label\" for=\"anim_id\" i18n=\"Animator\">Animator</label>\n\t\t\t\t<div dojoType=\"mioga.data.ItemFileReadStore\" jsId=\"animatorStore\" url=\"GetUsers.json\" nodename=\"user\" identifier=\"rowid\"></div>\n\t\t\t\t<input dojoType=\"dijit.form.FilteringSelect\" class=\"group-attributes-input\" id=\"group_attributes_animator_id\" name=\"anim_id\" store=\"animatorStore\" searchAttr=\"label\" />\n\t\t\t</div>\n\n\t\t\t<div id=\"group-skel-chooser\" class=\"form-item\">\n\t\t\t\t<label id=\"group_attributes_skeleton_label\" for=\"skeleton\" i18n=\"Skeleton\">Skeleton</label>\n\t\t\t\t<div dojoType=\"mioga.data.ItemFileReadStore\" jsId=\"skelStore\" url=\"GetSkeletons.json?type=group\" nodename=\"skeleton\" identifier=\"file\"></div>\n\t\t\t\t<input dojoType=\"dijit.form.FilteringSelect\" class=\"group-attributes-input\" id=\"group_attributes_skeleton\" name=\"skeleton\" store=\"skelStore\" searchAttr=\"name\" onChange=\"UpdateGroupSkeletonDetails ();\"/>\n\t\t\t</div>\n\n\t\t\t<div id=\"group-default-app-chooser\" class=\"form-item\">\n\t\t\t\t<label id=\"group_attributes_default_app_label\" for=\"default_app\" i18n=\"default_application\">default_application</label>\n\t\t\t\t<input dojoType=\"dijit.form.FilteringSelect\" class=\"group-attributes-input\" id=\"group_attributes_default_app\" name=\"default_app\" searchAttr=\"label\"/>\n\t\t\t</div>\n\n\t\t\t<div class=\"form-item\">\n\t\t\t\t<label id=\"public_part_label\" for=\"public_part\" i18n=\"public_part\">Public part</label>\n\t\t\t\t<input dojoType=\"dijit.form.CheckBox\" class=\"group-attributes-input\" id=\"group_attributes_public_part\" name=\"public_part\"/>\n\t\t\t</div>\n\n\t\t\t<div class=\"form-item\">\n\t\t\t\t<label id=\"history_label\" for=\"history\" i18n=\"history\">Files history</label>\n\t\t\t\t<input dojoType=\"dijit.form.CheckBox\" class=\"group-attributes-input\" id=\"group_attributes_history\" name=\"history\"/>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n",
	widgetsInTemplate: true,
	isContainer: true,

	startup: function () {
		// Translate labels
		var transl = dojo.i18n.getLocalization("mioga", "groupattributes");
		dojo.query("[i18n]", this.containerNode).forEach(function(node){
			var a = node.getAttribute("i18n");
			node.innerHTML = transl[a];
		});

		animatorStore.comparatorMap = {};
		animatorStore.comparatorMap['label'] = function (a, b) {
			if ((typeof (a) == 'string') && (typeof (b) == 'string')) {
				var a = unaccent (a.toLowerCase());
				var b = unaccent (b.toLowerCase());
			}

			if (a>b) return 1;
			if (a<b) return -1;
			return 0;
		};
		dijit.byId ('group_attributes_animator_id').attr ('queryExpr', '*${0}*');
		dijit.byId ('group_attributes_animator_id').attr ('autoComplete', false);
	}
});
});
