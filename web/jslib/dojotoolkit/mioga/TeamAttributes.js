//>>built
// wrapped by build app
define("mioga/TeamAttributes", ["dijit","dojo","dojox","dojo/i18n!mioga/nls/teamattributes","dojo/require!dijit/form/FilteringSelect,dijit/form/CheckBox,dijit/form/RadioButton"], function(dijit,dojo,dojox){
dojo.require ('dijit.form.FilteringSelect');
dojo.require ('dijit.form.CheckBox');
dojo.require ('dijit.form.RadioButton');
dojo.requireLocalization ('mioga', 'teamattributes');

/* ----------------------------------------------------------
 * mioga.TeamAttributes
 * ---------------------------------------------------------- */
dojo.provide ('mioga.TeamAttributes');
dojo.declare("mioga.TeamAttributes", [dijit._Widget, dijit._Templated], {
	image: '',
	store: null,
	templateString:"<div class=\"team-attributes\">\n\t<div dojoType=\"dijit.form.Form\" id=\"team_attributes_form\" class=\"dojo-form\">\n\t\t<div class=\"team-attributes-left-align\">\n\t\t\t<input dojoType=\"dijit.form.TextBox\" type=\"hidden\" id=\"team_attributes_rowid\" name=\"rowid\"/>\n\n\t\t\t<div class=\"form-item\">\n\t\t\t\t<label id=\"team_attributes_ident_label\" for=\"ident\" i18n=\"Identifier\">Identifier</label>\n\t\t\t\t<input dojoType=\"dijit.form.TextBox\" class=\"team-attributes-input\" id=\"team_attributes_ident\" name=\"ident\"/>\n\t\t\t</div>\n\n\t\t\t<div class=\"form-item\">\n\t\t\t\t<label id=\"team_attributes_description_label\" for=\"description\" i18n=\"Description\">Description</label>\n\t\t\t\t<input dojoType=\"dijit.form.Textarea\" class=\"team-attributes-input\" id=\"team_attributes_description\" name=\"description\"/>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n",
	widgetsInTemplate: true,
	isContainer: true,

	startup: function () {
		// Translate labels
		var transl = dojo.i18n.getLocalization("mioga", "teamattributes");
		dojo.query("[i18n]", this.containerNode).forEach(function(node){
			var a = node.getAttribute("i18n");
			node.innerHTML = transl[a];
		});
	}
});

});
