//>>built
// wrapped by build app
define("mioga/MiniList", ["dijit","dojo","dojox"], function(dijit,dojo,dojox){
/* ----------------------------------------------------------
   mioga.MiniList
   ---------------------------------------------------------- */
dojo.provide ('mioga.MiniList');
dojo.declare("mioga.MiniList", [dijit._Widget, dijit._Templated], {
	name:'',
	sel_store:null,
	unsel_store:null,
	templateString: '<div class="mini-list">'
	                  + '<div class="mini-list-selected"><div dojoAttachPoint="cont" class="mini-list-content" dojoAttachEvent="onclick:selectItem" ></div></div>'
					  + '<div dojoType="dijit.form.ComboBox" dojoAttachPoint="cmbbox" searchAttr="ident" dojoAttachEvent="onChange:addItem" autoComplete="false" highlightMatch="all" ></div>'
				 + '</div>',
	widgetsInTemplate:true,

	setStore: function(select_list, unselect_list) {
		this.select_list = new Array();
		dojo.forEach(select_list,
						function(item, index, array) {
							var nitem = new Object();
							nitem.node_id = dojo.dnd.getUniqueId();
							nitem.status = 'std';
							nitem.ident = item.ident;
							nitem.rowid = item.rowid;
							this.select_list.push(nitem);
						},
						this
					);

		//console.dir(unselect_list);
		this.unsel_data = { identifier:"ident", label:"ident", items:unselect_list };
		this.unsel_store = new dojo.data.ItemFileWriteStore({data:this.unsel_data});
		//console.debug('unsel_store ' + this.unsel_store);
		//console.debug('cmbbox ' + this.cmbbox);
		this.cmbbox.attr("store", this.unsel_store);
		this.cmbbox.attr("queryExpr", '*${0}*');
		//unsel_store.fetch();
		//console.dir(this.cmbbox);
		this.cmbbox.reset();
		this.genSelContent();
	},
	selectItem: function(event) {
		// console.debug('target Id ' + event.target.id);
		var destroyIndex = -1; // if set to a positive or nul value, need to splice the select_list with value
		                       // and regenerate displayed list
		dojo.forEach(this.select_list,
						function(item, index, array) {
							// console.debug('node_id ' + item.node_id + ' targetId ' + event.target.id);
							if (item.node_id == event.target.id) {
								// console.debug('status ' + item.status);
								if (item.status == 'std') {
									item.status = 'del';
									dojo.attr(item.node_id , 'class', 'ml-item ml-' + item.status);
								}
								else if (item.status == 'del') {
									item.status = 'std';
									dojo.attr(item.node_id , 'class', 'ml-item ml-' + item.status);
								}
								else if (item.status == 'add') {
									this.unsel_store.newItem({ident:item.ident, rowid:item.rowid});
									this.unsel_store.save();
									this.cmbbox.reset();
									destroyIndex = index;
								}
							}
						},
						this);
		if (destroyIndex >= 0) {
			this.select_list.splice(destroyIndex, 1);
			this.genSelContent();
		}
	},
	addItem: function(event) {
		// console.debug('miniList.addItem');
		var cur_value = this.cmbbox.attr("displayedValue");
		if (cur_value) {
			// console.debug('cur_value ' + cur_value);
			this.unsel_store.fetch({query:{ident:cur_value},
									scope: this,
			                        onComplete:function(item, request) {
												// console.debug('request : ' + request);
												// console.dir(request);
												// console.debug('item : ' + item);
												// console.dir(item);
												var nitem = new Object();
												nitem.node_id = dojo.dnd.getUniqueId();
												nitem.status = 'add';
												nitem.ident = this.unsel_store.getValue(item[0], "ident")
												nitem.rowid = this.unsel_store.getValue(item[0], "rowid");
												// console.debug('ident ' + nitem.ident+'  node_id '+nitem.node_id);
												this.select_list.push(nitem);
												this.unsel_store.deleteItem(item[0]);
												this.unsel_store.save();
												this.cmbbox.reset();
											}
									});
			this.genSelContent();
		}
	},
	genSelContent: function() {
		// console.debug('miniList.genSelContent');
		var html = "";
		this.select_list.sort(function(a,b) { return a.ident.localeCompare(b.ident); });
		dojo.forEach(this.select_list,
						function(item, index, array) {
							// console.debug('create id '+item.node_id+'  ident '+item.ident + ' status ' + item.status);
							html += '<p id="'+item.node_id+'" class="ml-item ml-' + item.status + '">' + item.ident + '</p>';
						}
					);
		//var node = dojo.byId(this.cont);
		this.cont.innerHTML = html;
	},
/*
	destroy: function() {
	},
*/
	getList: function() {
		var data = new Array();
		dojo.forEach(this.select_list,
						function(item, index, array) {
							if ( (item.status == 'add') || (item.status == 'std') ) {
								data.push({rowid:item.rowid,ident:item.ident});
							}
						}
					);
		return data;
	}
});

});
