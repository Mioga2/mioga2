//>>built
// wrapped by build app
define("mioga/widgets", ["dijit","dojo","dojox","dojo/i18n!mioga/nls/widgets"], function(dijit,dojo,dojox){
dojo.requireLocalization("mioga", "widgets");

/* ----------------------------------------------------------
   SelectList
   ---------------------------------------------------------- */
dojo.provide ("mioga.widgets");

dojo.declare("mioga.SelectList", [dijit._Widget, dijit._Templated], {
	store:null,
	templateString:"<div class=\"select-list\">\n\t<div class=\"clear-both\" style=\"height: 0px;\"></div>\n\t<div dojoAttachPoint=\"list\" class=\"sl-list\">\n\t\t<div class=\"sort-container\">\n\t\t\t<ul dojoAttachPoint=\"sort_selector\" class=\"sort-selector\"></ul>\n\t\t</div>\n\t\t<div class=\"sl-header\"><span class=\"sel-label\">${sel_label}</span><span class=\"unsel-label\">${unsel_label}</span></div>\n\t\t<div dojoAttachPoint=\"cont\" class=\"sl-cont\" dojoAttachEvent=\"onclick:selectItem\" ></div>\n\t</div>\n</div>\n",
	widgetsInTemplate:true,
	label: 'ident',
	sort: '',
	sort_fields: null,
	secondary_sort_field: null,
	sort_highlight: 0,
	reverse: 1,
	sel_label: 'Selected',
	unsel_label: 'Not Selected',
	store_prefs: null,

	startup: function () {
		if (this.sort_fields) {
			var transl = dojo.i18n.getLocalization("mioga", "widgets");
			dojo.create ('li', {innerHTML: transl.sortby}, this.sort_selector);
			var item = dojo.create ('li', null, this.sort_selector);
			var container = dojo.create ('ul', {'class': "hmenu"}, item);
			for (var key in this.sort_fields) {
				var classname = 'inactive sort-field';
				if (key == this.sort) {
					classname = 'active sort-field';
					if (this.reverse == 1) {
						classname += ' normal';
					}
					else {
						classname += ' reverse';
					}
				}
				var sel = dojo.create ("li", {innerHTML: '<a href="javascript:void (0);" field="' + key + '">' + this.sort_fields[key] + '</a>', 'class': classname}, container);
				dojo.connect (sel, 'onclick', this, 'sortBy');
			}
		}
	},

	createStore: function(global_list, select_list) {
		var index = new Object();
		if (select_list) {
			for (var i = 0; i < select_list.length; i++) {
				index[select_list[i].rowid] = 1;
			}
		}
		this.store = new dojo.data.ItemFileWriteStore({ data:{identifier:'rowid', label:'rowid',items:[]} });

		// Case-insensitive sorting
		var self=this;
		this.store.comparatorMap = {};
		this.store.comparatorMap[this.sort] = function (a, b) {
			a = a.toString ();
			b = b.toString ();
			a = unaccent (a.toLowerCase ());
			b = unaccent (b.toLowerCase ());

			if (a > b) return (self.reverse);
			if (a < b) return (-self.reverse);
			return (0);
		};

		if (this.secondary_sort_field && this.secondary_sort_field != this.sort) {
			this.store.comparatorMap[this.secondary_sort_field] = function (a, b) {
				a = a.toString ();
				b = b.toString ();
				a = unaccent (a.toLowerCase ());
				b = unaccent (b.toLowerCase ());

				if (a > b) return (1);
				if (a < b) return (-1);
				return (0);
			}
		}

		if (global_list) {
			for (var i = 0; i < global_list.length; i++) {
				//console.debug("global_list i " + i);
				//console.dir(global_list[i]);
				var item = global_list[i];
				item.selected = 0;
				item.changed = 0;
				if (index[global_list[i].rowid]) {
					item.selected = 1;
				}
				item['protected'] = global_list[i]['protected'];
				item.node_id = dojo.dnd.getUniqueId();
				//console.debug("item " + item);
				//console.dir(item);
				this.store.newItem(item);
				//res.newItem({rowid:item.rowid, ident:item.ident, selected:item.selected });
			}
			this.store.save();
		}

		this.update ();
		//console.debug("res " + res);
		//console.dir(res);
	},
	update: function() {
		var html = "";

		var label = this.label;
		var sort = this.sort;
		var secondary_sort = this.secondary_sort_field;

		var that = this;

		this.store.fetch({
				query: { rowid:'*' },
				sort: [{ attribute: sort }, { attribute: secondary_sort }],
				onComplete: function(items, request) {
							for (var i = 0; i < items.length; i++) {
								var classname = "";
								if (i % 2) {
									classname +="even";
								}
								else {
									classname +="odd";
								}
								if (items[i].selected == 1) {
									classname += " select"
								}
								else {
									classname += " unselect"
								}
								if (items[i].changed == 1) {
									classname += "-changed"
								}
								if (items[i]['protected'] && items[i]['protected'] == 1) {
									classname += ' locked';
								}
								var labelstr = items[i][label].toString ();
								// Highlight current sort field in label
								if (sort && that.sort_highlight) {
									var regex = new RegExp ("(^|\\s|[^\\wáâãäåÀÁÂÃÄÅèéêëÈÉÊËìíîïÌÍÎÏòóôõöøÒÓÔÕÖØùúûüÙÚÛÜñÑçÇ])" + items[i][sort]);
									labelstr = labelstr.replace (regex, "$1<span class='highlight'>" + items[i][sort] + "</span>");
								}
								html += '<p id="' + items[i].node_id + '" class="' + classname + '" >' + labelstr + '</p>';
							}
				}
		});

		this.cont.innerHTML = html;
	},

	unlockItem: function (value, identifier) {
		this.store.fetch({
			scope:this,
			onComplete: function(items, request) {
				for (var i = 0; i < items.length; i++) {
					if (items[i][identifier] == value) {
						var classname = dojo.attr(dojo.byId (items[i].node_id.toString ()), 'class').replace (/locked/, '');
						dojo.attr(dojo.byId (items[i].node_id.toString ()), 'class', classname);
					}
				}
			}
		});
	},

	lockItem: function (value, identifier) {
		this.store.fetch({
			scope:this,
			onComplete: function(items, request) {
				for (var i = 0; i < items.length; i++) {
					if (items[i][identifier] == value) {
						var classname = dojo.attr(dojo.byId (items[i].node_id.toString ()), 'class');
						if (classname.match (/unselect/)) {
							classname = classname.replace (/unselect/, 'select-changed');
							items[i].selected[0] = 1;
							items[i].changed = 1;
						}
						if (!classname.match (/locked/)) {
							classname += ' locked';
						}
						dojo.attr(dojo.byId (items[i].node_id.toString ()), 'class', classname);
					}
				}
			}
		});
	},

	selectItem: function(event) {
		var node_id = event.target.id;
		if (!node_id && event.target.tagName === 'SPAN' && event.target.className === 'highlight') {
			node_id = event.target.parentNode.id;
		}
		if (node_id) {
			var old_class = dojo.attr(node_id, 'class');
			var queryObj = {};
			queryObj['node_id'] = node_id;
			this.store.fetch({
					query: queryObj,
					scope:this,
					onComplete: function(items, request) {
								for (var i = 0; i < items.length; i++) {
									var old_class = dojo.attr(dojo.byId (items[i].node_id.toString ()), 'class');
									var classname = 'odd ';
									if (old_class.match (/locked/)) {
										break;
									}
									if (old_class.match (/even/)) {
										classname = 'even ';
									}
									if (items[i].selected == 1) {
										classname += "unselect";
										items[i].selected[0] = 0; // "selected" attribute needs to be an array with value in element 0 so that the list can be sorted against its value (see method getValues from ItemFileReadStore)
									}
									else {
										classname += "select";
										items[i].selected[0] = 1; // "selected" attribute needs to be an array with value in element 0 so that the list can be sorted against its value (see method getValues from ItemFileReadStore)
									}
									if (items[i].changed == 0) {
										items[i].changed = 1;
										classname += "-changed";
									}
									else {
										items[i].changed = 0;
									}
									dojo.attr(dojo.byId (items[i].node_id.toString ()), 'class', classname);
								}
					}
			});
		}
	},
	showFilter: function() {
	},
	getList: function() {
		var data = new Array();
		this.store.fetch({
				query: {rowid:'*'},
				scope:this,
				onComplete: function(items, request) {
							for (var i = 0; i < items.length; i++) {
								if ( (items[i].selected == 1) ) {
									data.push({rowid:items[i].rowid[0],ident:items[i].ident[0]});
								}
							}
						}
					});
		return data;
	},
	sortBy: function (elm) {
		// Reverse order if needed
		if (this.sort == elm.target.attributes.getNamedItem ('field').nodeValue) {
			this.reverse = -this.reverse;
		}
		else {
			this.reverse = 1;
		}
		this.sort = elm.target.attributes.getNamedItem ('field').nodeValue;

		// Set classes for differents items
		var sort = this.sort;
		var reverse = this.reverse;
		dojo.query('li.sort-field', this.sort_selector).forEach (function (node) {
			if (node.childNodes[0].attributes.getNamedItem ('field').nodeValue == sort) {
				var order = 'normal';
				if (reverse == -1) {
					order = 'reverse';
				}
				node.className = order + ' active sort-field';
			}
			else {
				node.className = 'inactive sort-field';
			}
		});

		// Case-insensitive sorting
		var self=this;
		this.store.comparatorMap = {};
		this.store.comparatorMap[this.sort] = function (a, b) {
			a = a.toString ();
			b = b.toString ();
			a = unaccent (a.toLowerCase ());
			b = unaccent (b.toLowerCase ());

			if (a > b) return (self.reverse);
			if (a < b) return (-self.reverse);
			return (0);
		};

		if (this.secondary_sort_field && this.secondary_sort_field != this.sort) {
			this.store.comparatorMap[this.secondary_sort_field] = function (a, b) {
				a = a.toString ();
				b = b.toString ();
				a = unaccent (a.toLowerCase ());
				b = unaccent (b.toLowerCase ());

				if (a > b) return (1);
				if (a < b) return (-1);
				return (0);
			}
		}

		// Store user preferences
		if (this.store_prefs) {
			this.store_prefs ();
		}

		this.update ();
		return (false);
	}
});
/* ----------------------------------------------------------
   miniList
   ---------------------------------------------------------- */
dojo.declare("mioga.MiniList", [dijit._Widget, dijit._Templated], {
	name:'',
	sel_store:null,
	unsel_store:null,
	templateString: '<div class="mini-list">'
	                  + '<div class="mini-list-selected"><div dojoAttachPoint="cont" class="mini-list-content" dojoAttachEvent="onclick:selectItem" ></div></div>'
					  + '<div dojoType="dijit.form.ComboBox" dojoAttachPoint="cmbbox" searchAttr="ident" dojoAttachEvent="onChange:addItem" ></div>'
				 + '</div>',
	widgetsInTemplate:true,

	setStore: function(select_list, unselect_list) {
		this.select_list = new Array();
		dojo.forEach(select_list,
						function(item, index, array) {
							var nitem = new Object();
							nitem.node_id = dojo.dnd.getUniqueId();
							nitem.status = 'std';
							nitem.ident = item.ident;
							nitem.rowid = item.rowid;
							this.select_list.push(nitem);
						},
						this
					);

		//console.dir(unselect_list);
		this.unsel_data = { identifier:"ident", label:"ident", items:unselect_list };
		this.unsel_store = new dojo.data.ItemFileWriteStore({data:this.unsel_data});
		//console.debug('unsel_store ' + this.unsel_store);
		//console.debug('cmbbox ' + this.cmbbox);
		this.cmbbox.attr("store", this.unsel_store);
		//unsel_store.fetch();
		//console.dir(this.cmbbox);
		this.cmbbox.reset();
		this.genSelContent();
	},
	selectItem: function(event) {
		// console.debug('target Id ' + event.target.id);
		var destroyIndex = -1; // if set to a positive or nul value, need to splice the select_list with value
		                       // and regenerate displayed list
		dojo.forEach(this.select_list,
						function(item, index, array) {
							// console.debug('node_id ' + item.node_id + ' targetId ' + event.target.id);
							if (item.node_id == event.target.id) {
								// console.debug('status ' + item.status);
								if (item.status == 'std') {
									item.status = 'del';
									dojo.attr(item.node_id , 'class', 'ml-item ml-' + item.status);
								}
								else if (item.status == 'del') {
									item.status = 'std';
									dojo.attr(item.node_id , 'class', 'ml-item ml-' + item.status);
								}
								else if (item.status == 'add') {
									this.unsel_store.newItem({ident:item.ident, rowid:item.rowid});
									this.unsel_store.save();
									this.cmbbox.reset();
									destroyIndex = index;
								}
							}
						},
						this);
		if (destroyIndex >= 0) {
			this.select_list.splice(destroyIndex, 1);
			this.genSelContent();
		}
	},
	addItem: function(event) {
		// console.debug('miniList.addItem');
		var cur_value = this.cmbbox.attr("displayedValue");
		if (cur_value) {
			// console.debug('cur_value ' + cur_value);
			this.unsel_store.fetch({query:{ident:cur_value},
									scope: this,
			                        onComplete:function(item, request) {
												// console.debug('request : ' + request);
												// console.dir(request);
												// console.debug('item : ' + item);
												// console.dir(item);
												var nitem = new Object();
												nitem.node_id = dojo.dnd.getUniqueId();
												nitem.status = 'add';
												nitem.ident = this.unsel_store.getValue(item[0], "ident")
												nitem.rowid = this.unsel_store.getValue(item[0], "rowid");
												// console.debug('ident ' + nitem.ident+'  node_id '+nitem.node_id);
												this.select_list.push(nitem);
												this.unsel_store.deleteItem(item[0]);
												this.unsel_store.save();
												this.cmbbox.reset();
											}
									});
			this.genSelContent();
		}
	},
	genSelContent: function() {
		// console.debug('miniList.genSelContent');
		var html = "";
		this.select_list.sort(function(a,b) { return a.ident.localeCompare(b.ident); });
		dojo.forEach(this.select_list,
						function(item, index, array) {
							// console.debug('create id '+item.node_id+'  ident '+item.ident + ' status ' + item.status);
							html += '<p id="'+item.node_id+'" class="ml-item ml-' + item.status + '">' + item.ident + '</p>';
						}
					);
		//var node = dojo.byId(this.cont);
		this.cont.innerHTML = html;
	},
/*
	destroy: function() {
	},
*/
	getList: function() {
		var data = new Array();
		dojo.forEach(this.select_list,
						function(item, index, array) {
							if ( (item.status == 'add') || (item.status == 'std') ) {
								data.push(item.rowid);
							}
						}
					);
		return data;
	}
});

/* ----------------------------------------------------------
   userChoice
   ---------------------------------------------------------- */
dojo.declare("mioga.UserChoice", [dijit._Widget, dijit._Templated], {
	sel_store:null,
	unsel_store:null,
	templateString:"<div class=\"user-choice\" dojoType=\"dijit.BorderContainer\">\n\t<div dojoType=\"dijit.layout.ContentPane\" region=\"top\" class=\"uc-fields\" splitter=\"false\">\n\t\t<table>\n\t\t\t<tr>\n\t\t\t\t<th>Lastname</th>\n\t\t\t\t<th>Firstname</th>\n\t\t\t\t<th>Identifier</th>\n\t\t\t\t<th>Email</th>\n\t\t\t</tr>\n\t\t\t<tr>\n\t\t\t\t<td><input dojoType=\"dijit.form.TextBox\" dojoAttachPoint=\"lastname\" value=\"\" /></td>\n\t\t\t\t<td><input dojoType=\"dijit.form.TextBox\" dojoAttachPoint=\"firstname\" value=\"\" /></td>\n\t\t\t\t<td><input dojoType=\"dijit.form.TextBox\" dojoAttachPoint=\"ident\" value=\"\" /></td>\n\t\t\t\t<td><input dojoType=\"dijit.form.TextBox\" dojoAttachPoint=\"email\" value=\"\" /></td>\n\t\t\t</tr>\n\t\t</table>\n\n\t</div>\n\t<div dojoType=\"dijit.layout.ContentPane\" region=\"center\" class=\"uc-list\" splitter=\"true\">\n\t\t<div class=\"display-list\" dojoAttachPoint=\"list\" >\n\t\t\t<div class=\"list-content\" dojoAttachPoint=\"list_cont\" >\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div dojoType=\"dijit.layout.ContentPane\" region=\"bottom\" class=\"uc-select\" >\n\t\t<div class=\"display-list\" dojoAttachPoint=\"select\" >\n\t\t\t<div class=\"list-content\" dojoAttachPoint=\"select_cont\" >\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n",
	widgetsInTemplate:true,

	getList: function() {
		/*
		var data = new Array();
		dojo.forEach(this.select_list,
						function(item, index, array) {
							if ( (item.status == 'add') || (item.status == 'std') ) {
								data.push(item.rowid);
							}
						}
					);
		return data;
		*/
	}
});

});
