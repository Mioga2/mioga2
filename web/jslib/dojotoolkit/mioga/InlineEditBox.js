//>>built
// wrapped by build app
define("mioga/InlineEditBox", ["dijit","dojo","dojox","dojo/require!dijit/InlineEditBox,dojo/cache"], function(dijit,dojo,dojox){
dojo.provide("mioga.InlineEditBox");

dojo.require("dijit.InlineEditBox");
dojo.require("dojo.cache");

dojo.declare("mioga.InlineEditBox", [ dijit.InlineEditBox ], {
	autoSave: false,
	editorWrapper: "mioga._InlineEditor",
		
	edit: function() {
		this.inherited(arguments);
		
		// This FORM helps to make the InlineEditBox submittable with ENTER
		var form_id = dijit.getUniqueId("mioga_InlineEditBox_form");
		var form = dojo.create("form", { id: form_id }, this.domNode.parentNode, "first");
		var form = new dijit.form.Form({}, form_id);
		dojo.place(this.wrapperWidget.domNode, form_id, "first");
		
		dojo.connect(form, 'onSubmit', this, function(evt) {
			this.save(true);
			evt.stopPropagation();
		});
		this.wrapperWidgetFormID = form_id;
	},
	
	cancel: function() {
		this.inherited(arguments);
		dijit.byId(this.wrapperWidgetFormID).destroy();
	},
	
	_onFocus: function() {
		if (this.editing) {
			dijit.focus(dojo.query("#"+this.wrapperWidgetFormID+" input")[0]);
		}
	}
	
});



dojo.declare("mioga._InlineEditor", dijit._InlineEditor, {
	templateString: dojo.cache("mioga", "templates/InlineEditBox.html", "<span dojoAttachPoint=\"editNode\" waiRole=\"presentation\" style=\"position: absolute; visibility:hidden\" class=\"dijitReset dijitInline\"\n        dojoAttachEvent=\"onkeypress: _onKeyPress\"\n        ><span dojoAttachPoint=\"editorPlaceholder\"></span\n        ><div class=\"mioga_inlineEditBox_savecancel\" dojoAttachPoint=\"buttonContainer\"\n                ><div class='icon_save' dojoAttachPoint=\"saveButton\" dojoAttachEvent=\"onclick:save\" label=\"${buttonSave}\"></div\n                ><div class='icon_cancel' dojoAttachPoint=\"cancelButton\" dojoAttachEvent=\"onclick:cancel\" label=\"${buttonCancel}\"></div\n        ></div\n></span>"),
	
	postCreate: function() {
		this.saveButton.set = function() {}; // so that the superclass method calls don't break
		this.inherited(arguments);
	}
});
});
