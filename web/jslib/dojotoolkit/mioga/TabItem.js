//>>built
// wrapped by build app
define("mioga/TabItem", ["dijit","dojo","dojox"], function(dijit,dojo,dojox){
/* ----------------------------------------------------------
 * MiogaTabItem
 * ---------------------------------------------------------- */
dojo.provide ('mioga.TabItem');
dojo.declare("mioga.TabItem", [dijit._Widget, dijit._Templated], {
	image: '',
	legend: '',
	tabid: '',
	templateString:"<div class=\"tab-item\" tabid=\"${id}\" legend=\"${legend}\">\n\t<div width=\"auto\" class=\"center-aligned-tab-content\">\n\t\t<div dojoAttachPoint=\"containerNode\" class=\"left-aligned-tab-content\">\n\t\t\t<h1 class=\"tabtitle\">${legend}</h1>\n\t\t</div>\n\t</div>\n</div>\n",
	widgetsInTemplate: true,
	isContainer: true
});

});
