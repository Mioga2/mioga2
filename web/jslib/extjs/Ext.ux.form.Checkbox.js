Ext.namespace('Ext.ux.form');
Ext.ux.form.Checkbox = function(config) {
	Ext.ux.form.Checkbox.superclass.constructor.call(this, config);
}
Ext.extend(Ext.ux.form.Checkbox, Ext.form.Checkbox, {
	checkboxCls: 'x-checkbox',

	checkedCls: 'x-checkbox-checked',

	cbFocusCls: 'x-checkbox-focus',

	cbOverCls: 'x-checkbox-over',

	cbDownCls: 'x-checkbox-down',

	cbDisabledCls: 'x-checkbox-disabled',

	defaultAutoCreate: {tag: 'input', type: 'checkbox', autocomplete: 'off', cls: 'x-hidden'},

	onRender: function(ct, position){
		Ext.ux.form.Checkbox.superclass.onRender.call(this, ct, position);
		this.checkbox = this.wrap.createChild({tag: 'img', src: Ext.BLANK_IMAGE_URL, cls: this.checkboxCls}, this.el);
		if (this.checked) {
			this.wrap.addClass(this.checkedCls);
		}
	},

	initEvents: function() {
		Ext.ux.form.Checkbox.superclass.initEvents.call(this);
		this.checkbox.on({
			mouseover: this.onMouseOver,
			mouseout: this.onMouseOut,
			mousedown: this.onMouseDown,
			mouseup: this.onMouseUp,
			click: this.toggle,
			scope: this
		});
	},

	onDisable: function() {
		Ext.ux.form.Checkbox.superclass.onDisable.call(this);
		this.checkbox.addClass(this.cbDisabledCls);
	},

	onEnable: function() {
		Ext.ux.form.Checkbox.superclass.onDisable.call(this);
		this.checkbox.removeClass(this.cbDisabledCls);
	},

	onFocus: function(e) {
		Ext.ux.form.Checkbox.superclass.onFocus.call(this, e);
		this.checkbox.addClass(this.cbFocusCls);
	},

	onBlur: function(e) {
		Ext.ux.form.Checkbox.superclass.onBlur.call(this, e);
		this.checkbox.removeClass(this.cbFocusCls);
	},

	onMouseOver: function() {
		this.checkbox.addClass(this.cbOverCls);
	},

	onMouseOut: function() {
		this.checkbox.removeClass(this.cbOverCls);
	},

	onMouseDown: function() {
		this.checkbox.addClass(this.cbDownCls);
	},

	onMouseUp: function() {
		this.checkbox.removeClass(this.cbDownCls);
	},

	toggle: function() {
		if (!this.disabled && !this.readOnly) {
			this.setValue(!this.checked);
		}
	},

	setValue : function(v) {
		Ext.ux.form.Checkbox.superclass.setValue.call(this, v);
		this.wrap[v ? 'addClass' : 'removeClass'](this.checkedCls);
	}
});
Ext.reg('checkbox', Ext.ux.form.Checkbox);
