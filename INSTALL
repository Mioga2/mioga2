# ============================================================================
# MIOGA2 Project (C) 2004-2008 The Mioga2 project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================

--------------------------------------
/!\ Important notice about locales /!\
--------------------------------------
To have Mioga localized in a given language, your system must have the
corresponding locale generated in UTF-8. For example, to have Mioga in french
(fr_FR), the fr_FR.UTF-8 locale must be present on your system.
On a debian system, you can regenerate locales with this command as root:
 dpkg-reconfigure locales

On a typical Mioga2 installation, the two locales below should be selected:
	- fr_FR.UTF-8
	- en_US.UTF-8

If you want to know what are current locales installed on your system, just type
this command:
 locale -a

Dependances and pre-install configuration
-----------------------------------------

Before installing Mioga 2, you must install the following packages.

Debian users can use mioga2-deps package downloadable on :

	http://packages.alixen.org

See http://www.alixen.org/projects/mioga2/wiki/Notes_spécifiques_Debian for more details.


Perl Modules :                            Debian packages
-----------------------------------------------------------------
Apache::Request                    >= 2.08           libapache2-request-perl
Benchmark::Timer                   >= 0.7.0          libbenchmark-timer-perl
Crypt::SSLeay	                   >= 0.51           libcrypt-ssleay-perl
Data::Dumper                       >= 2.121          perl-base
Date::Calc                         >= 5.4            libdate-calc-perl
Date::ICal                         >= 1.72           libdate-ical-perl
Date::Manip                        >= 6.21           libdate-manip-perl
Date::Parse                        >= 2.27           libtimedate-perl
DBI                                >= 1.53           libdbi-perl
DBD::Pg                            >= 1.49           libdbd-pg-perl
Error                              >= 0.15           liberror-perl
ExtUtils::MakeMaker                >= 6.30           perl-modules
File::Copy                         >= 2.09           perl-modules
File::Find                         >= 1.10           perl-modules
FileHandle                         >= 2.01           perl-base
File::MimeInfo::Magic              >= 0.13           libfile-mimeinfo-perl
File::Path                         >= 1.08           perl-modules
Getopt::Std                        >= 1.05           perl-modules
HTML::TokeParser::Simple           >= 3.15           libhtml-tokeparser-simple-perl
HTTP::DAV                          >= 0.31           libhttp-dav-perl
IO::Handle                         >= 1.25           perl-base
IO::Socket::SSL	                   >= 1.01           libio-socket-ssl-perl
IO::Socket                         >= 1.29           perl-base
LIST::MoreUtils                    >= 0.25           liblist-moreutils-perl
LWP::UserAgent                     >= 2.33           libwww-perl
MIME::Base64                       >= 3.07           libmime-perl
MIME::Entity                       >= 5.420          libmime-perl
MIME::Parser                       >= 5.420          libmime-perl
MIME::QuotedPrint                  >= 3.07           perl
Net::LDAP                          >= 0.33           libnet-ldap-perl
Parse::Yapp::Driver                >= 1.05           libparse-yapp-perl
POSIX                              >= 1.09           perl-base
Storable                           >= 2.15           perl
String::Checker [See Note 1]       >= 0.03           libstring-checker-perl
Sys::Syslog                        >= 0.13           perl
Unicode::MapUTF8                   >= 1.11           libunicode-maputf8-perl
Unicode::String                    >= 2.09           libunicode-string-perl
URI::Escape                        >= 3.28           liburi-perl
XML::LibXML                        >= 1.59           libxml-libxml-perl
XML::LibXSLT                       >= 1.59           libxml-libxslt-perl, libxslt-dev
Text::Iconv                        >= 1.4            libtext-iconv-perl
Term::Readline::Gnu                >= 1.16           libterm-readline-gnu-perl
Algorithm::Diff                    >= 1.19           libalgorithm-diff-perl
Time::Piece                        >= 1.11           libtime-piece-perl
Encode::Detect                     >= 1.00           libencode-detect-perl
Locale::TextDomain                 >= 1.16           libintl-perl
Locale::TextDomain::UTF8
XML::XML2JSON                      >= 0.03           libxml-xml2json-perl
XML::Atom::SimpleFeed              >= 0.82           libxml-atom-simplefeed-perl
Test::WWW::Mechanize               >= 1.20           libtest-www-mechanize-perl
Image::Magick                      >= 6.3.7          perlmagick
XML::Simple                                          libxml-simple-perl
File::Copy::Recursive                                libfile-copy-recursive-perl
JSON::Syck                                           libyaml-syck-perl
Archive::Zip                       >= 1.18           libarchive-zip-perl
Proc::ProcessTable                                   libproc-processtable-perl
JSON::XS                           >= 2.29           libjson-xs-perl

[Note 1] Mioga2 requires a patched version of String::Checker. If you don't use
   mioga2 Debian repository, you should install the latest String::Checker from
   package that can be found at the following address:
   http://packages.alixen.org/contribs/


Non-perl dependencies :                   Debian packages
-----------------------------------------------------------------
Apache                    >= 2.4            apache2, apache2-mpm-prefork
mod_perl                  >= 2.0.2          libapache2-mod-perl2
mod_dav                                     apache2
PostgreSQL                >= 8.3            postgresql
make                                        make
xsltproc                                    xsltproc
gnupg                     >= 1.4.6          gnupg
libxslt                   >= 1.1.19         libxslt1
gettext                   >= 0.16           gettext
7zip                                        p7zip-full

For search engine :

catdoc                                    catdoc
catppt                                    catppt
xls2csv                                   xls2csv
unzip                                     unzip
pstotext                                  pstotext
pdftotext (included in xpdf-utils)        xpdf-utils
Xapian
Search::Xapian

PostgreSQL configuration :
_________________________

Before installing Mioga 2, create a PostgreSQL user named "mioga" :

# su - postgres
$ createuser -d -a -P mioga
Enter password for user "mioga": <user_password>
$ exit

You must switch PostgreSQL behavior to treat backslashes literally, as
specified in the SQL standard. For this, in
/etc/postgresql/<version>/main/postgresql.conf, ensure the following line is
present :
standard_conforming_strings = on

Now you must configure PostgreSQL authentication in
/etc/postgresql/<version>/main/pg_hba.conf.

At the end of this file, you must find the following lines :

You must add the following lines BEFORE every other line :

local        mioga2    mioga              md5


Save this file and restart PostgreSQL.


Mioga 2 Installation
====================

Run:
# perl Makefile.PL

In the configuration menu, you must probably change the following parameters:

7 - smtp_server : Your SMTP server name
8 - domain_name : Your domain name


In the submenu  : 14 - Database settings, change:

4 - dbi_passwd  : The password of the PostgreSQL user named "mioga".


And run:
# make

IMPORTANT:
Make a backup of database and file system before updating.

If you upgrade from 2.0.x to 2.1, do :
# cd sql
# perl -w -I../lib transformdb_to_2.1.pl

# make install
# make installall

If you upgrade from 2.0.x to 2.1, you must modify existing themes to make
them compatible to new organisation (see docs/en_US)

# cd web
# install_theme.sh default $(INSTALL_DIR) instance theme_dest_name

WARNING : Don't upgrade specific themes directly. Use a source comparator before.

Apache configuration
--------------------

IMPORTANT: don't install Mioga2 in Apache DocumentRoot directory with
default config file to avoid serious security problem.

Copy /var/lib/Mioga2/conf/apache/* in /etc/apache2/conf-available/ then enable it:

ln -s /etc/apache2/conf-available/mioga-* /etc/apache2/conf-enabled/

Be careful that mod_perl, mod_dav and apache_request (apreq) are enabled and
configured:

ln -s /etc/apache2/mods-available/apreq* /etc/apache2/mods-enabled
ln -s /etc/apache2/mods-available/dav*   /etc/apache2/mods-enabled
ln -s /etc/apache2/mods-available/perl*  /etc/apache2/mods-enabled
ln -s /etc/apache2/mods-available/headers*  /etc/apache2/mods-enabled

Disable mpm_event and enable mpm_prefork :

a2dismod mpm_event
a2enmod mpm_prefork

Restart Apache 2.


You can now use Mioga 2 with your favourite browser on
http://your-server-name/Mioga2/Mioga.

NB : By default, the administrator login is "admin" and his password is
"admin".

Note about automated tests
--------------------------
Mioga2 distribution comes with some scripts that automate testing. These
scripts are located under directory "tests/mechanize". You can run all tests
with the script "run_tests.sh"

IMPORTANT NOTE REGARDING WEBDAV ACCESS:
Apache has to listen on localhost and Mioga has to be allowed to access it.
Else webdav access will not work properly. (access for webdav is at location
localhost/dav_mioga2 by default)

Additional configuration for Web authentication
-----------------------------------------------

Mioga provides a Web authentication method (as opposed to HTTP-Basic). This
method is enabled a configure time from the following menu items:
	13 - Security settings
		3 - html_login         :       yes
		4 - login_uri          :       /login
		5 - authen_timeout     :       36000

Default login Web resources (pages, images and styles) are installed in
INSTALL_DIR/LOGIN_URI/default (typically /var/lib/Mioga2/login/default/) but
Mioga gets login resources from INSTALL_DIR/LOGIN_URI. You should:
	- place your own Web resources to this directory so the authentication page
	  looks the way you want it,
	- copy the default resources to the correct location so your users can
	  authenticate with the default Mioga2 resources.


Additional configuration for LDAP and External Users
-----------------------------------------------------

You must use mioga2_nonlocal_synchronizer.pl to synchronize Mioga2
database with an LDAP Directory and other Mioga2 server.

You must add a simple line in your crontab (every 2 minutes seems
to be a good delay) :

*/20 * * * * su - www-data -c "/usr/local/bin/mioga2_nonlocal_synchronizer.pl /home/mioga/www/Mioga2/conf/Mioga.conf" 2>&1 >> /tmp/mioga2_nonlocal_synchronizer.log

LDAP fields (sn, givenName and mail) MUST be initialized beforie user
can be created.



Additional configuration for Mailing List
-----------------------------------------

If you want to use the Mioga2 Mailing List, you must configure your
Mail Transfert Agent (MTA).

The goal of this document is not to describe how to setup your MTA,
but to explain the Mioga2 daemon and script that must be launched.


* miogamailinglist.pl :

  miogamainlinglist.pl is the Mioga2 mailinglist daemon.
  It must be launched at boot time (with an init.d script) with the
  command :

miogamailinglist.pl /path/to/Mioga.conf

See web/conf/mioga2.init.d



* miogaqueue :

  It's the Mioga2 mail handler.
  You must configure your MTA to run this program when a mail for a
  Mioga2 group is delivered.

miogaqueue /path/to/Mioga.conf instancename groupname

The mail body must be sent on the standard input.

Be careful, this program must have write access on /var/spool/mioga
(or the maildir mioga configuration parameter).
The execution user and group of tis script depends on your MTA.



* Installing Mioga2 MailingList in Postfix :

Copy the script <mioga_source_dir>bin/mailinglist/mta-script/mioga-postfix.pl in /usr/local/bin

Edit /etc/postfix/master.cf and add the following line :

mioga     unix  -       n       n       -       -       pipe flags=R user=www-data
  argv=/usr/local/bin/mioga-postfix.pl <domain_name> </path/to/Mioga.conf> $recipient

If you use multiple instances, or wish to do it, you must use mioga-postfix-multiple-instance.pl.

Now, you must configure Postfix to use the "mioga" transport. It's
really dependent on your existing Postfix installation. So, it's not possible
to describe a "standard" Postfix configuration. Read the Postfix manual.


Additional configuration for Disc Space Updater
-----------------------------------------------

It is recommended to launch periodically the Mioga2 Disc Space Updater.
This script updates the "disk space used" entry of Mioga 2 groups in
database.

mioga2discspace.pl /path/to/mioga.conf

Here, an example of a crontab entry that launches this script every
night at 2.30am :

30 2 * * * mioga2discspace.pl /var/lib/Mioga2/conf/Mioga.conf

Additional configuration for multiple instances
-----------------------------------------------

Launch miogadm.pl and select the master instance, Then add ColbertSA application.

miogadm # instance Mioga
miogadm # addapp Mioga2::ColbertSA
miogadm # <Ctrl-D>

To enable multi-instance user-account management through LDAP, enable the BottinSA applicaion. From within miogadm shell:

miogadm # addapp Mioga2::BottinSA

Additionnal configuration for awstats plugin
--------------------------------------------
you need to install awstats and create pacahe configurtion to enable the icon acces

you need to add the following line in the crontab
59 23 * * * su - www-data -c "/usr/local/bin/mioga_awstats.pl /var/lib/Mioga2/conf/Mioga.conf /var/log/apache2" 2>&1 >> /tmp/mioga_awstats.log


If you change the name for the first instance, you must edit Portal.xml in admin-skel.xml.d

Additional configuration for the Notifier daemon
------------------------------------------------

The notifier daemon has to be launched at boot time (with an init.d script) with the
following command:

      notifier.pl /path/to/Mioga.conf

See web/conf/mioga2.init.d

Default init script for Mioga2 services
---------------------------------------

A default init script is provided in file web/conf/mioga2.init.d. This script works with a defaults file (web/conf/mioga2.default) that should be copied to /etc/default/mioga2.

you can use to next command lines : 

	cp web/conf/mioga2.init.d /etc/init.d/mioga2
	chmod +x /etc/init.d/mioga2
	update-rc.d mioga2 defaults

Additional configuration for the Search engine
----------------------------------------------

You must edit the crawl_sample.sh to specify default location for configuration files and rename it to crawl.sh.
This script must be run by cron in a day basis with apache user (www-data for Debian)

The Search engine daemon has to be launched at boot time (started by init.d script above) with the
following command:

      searchengine.pl /path/to/Mioga.conf

See web/conf/mioga2.init.d

Additional configuration for HTML form-based authentication
-----------------------------------------------------------

From version 2.3.2, Mioga2 comes with a module for handling HTML form-based authentication. It is automatically used when the following directive is found in Mioga2 Apache configuration file.
	PerlSetVar AuthenWeb 1

This directive should not be set at the top of the file as WebDAV access requires HTTP-Basic authentication to work correctly. Instead, it should be placed in the LocationMatch block(s) corresponding to private binaries.

Database dump and restore
-------------------------

su - postgres -c "pg_dump -b -Fc mioga2" >  /var/backups/mioga_dump

pg_restore -d mioga2 /tmp/mioga_dump

Note about proxyfying Mioga2
----------------------------

If you configure a front-end Web server listening on HTTPS to transmit request to back-end Mioga2 server listening on HTTP, you'll experience problems with auto-generated Mioga2 URLs because they are generated from incoming request parameters, thus being HTTP. If you want to force the protocol to be used in such a case, you should set "protocol" configuration value from the configuration menu.
	17 - Others advanced parameters
		5 - Apache settings
			1 - protocol
