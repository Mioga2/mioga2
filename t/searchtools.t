# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

#########################
use utf8;
use Test::More tests => 1;

diag ('TODO: Broken test-script is disabled, needs to be re-written !!!');
pass ();

__DATA__
use Data::Dumper;
use Error qw(:try);
plan tests => 12;
use Mioga2::Search::WProc;
#
# Create Word processor for french language
#
my $wfrench;
try {
	$wfrench = Mioga2::Search::WProc->new( { lang => 'french', });
	$res = 1;
}
otherwise {
	my $err = shift;
	print STDERR "Error : " . Dumper($err) . "\n";
	$res = 0;
};
ok ( $res );
#
# french tests
#
my $word;
my $sword;
($word, $sword) = $wfrench->ProcessWord("l'abandon");
print STDERR " word = $word  sword = $sword\n";
ok ( $word eq 'abandon' );
ok ( $sword eq 'abandon' );
binmode(STDERR, ":utf8");
my $toto = "éèàçùïö";
print STDERR " is_utf8 : " . utf8::is_utf8($toto) . "  toto = $toto\n";
($word, $sword) = $wfrench->ProcessWord($toto);
print STDERR " word = $word  sword = $sword\n";
($word, $sword) = $wfrench->ProcessWord("éèàçùïö");
print STDERR " word = $word  sword = $sword\n";
ok ( $word eq 'eeacuio' );
ok ( $sword eq 'eeacuio' );
#
# Create Word processor for english language
#
my $wenglish;
try {
	$wenglish = Mioga2::Search::WProc->new( { lang => 'english', });
	$res = 1;
}
otherwise {
	my $err = shift;
	print STDERR "Error : " . Dumper($err) . "\n";
	$res = 0;
};
ok ( $res );
