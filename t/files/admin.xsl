<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="base.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:include href="simple_large_list.xsl"/>
<xsl:include href="admin-colorsel.xsl"/>


<!--
=======================================
 filesize template. Quick fix for 
 simple large list not showing 
 correctly size of a group.
 SHOULD NOT BE HERE. NEED A CLEANER FIX
=======================================
-->

<xsl:template match="filesize" name="filesize">

<xsl:choose>
	<xsl:when test="./@mult='1'"><xsl:value-of select="." /> o</xsl:when>
	<xsl:when test="./@mult='k'"><xsl:value-of select="." /> ko</xsl:when>
	<xsl:when test="./@mult='M'"><xsl:value-of select="." /> Mo</xsl:when>
	<xsl:when test="./@mult='G'"><xsl:value-of select="." /> Go</xsl:when>
	<xsl:otherwise></xsl:otherwise>
</xsl:choose>

</xsl:template>
    
<!-- ===============
	root document
	================ -->

<xsl:template match="/">
<html>
<head>
	<xsl:call-template name="mioga-css"/>
	<xsl:call-template name="mioga-js"/>
	<link rel="stylesheet" type="text/css" href="{$theme_uri}/css/admin.css" media="screen" />
	<xsl:call-template name="favicon"/>
	<xsl:call-template name="title"/>
</head>
	<xsl:apply-templates />
</html>
</xsl:template>
  
  <!-- =================================
       Tabs Menu description
       ================================= -->

<xsl:template match="Display*" mode="MiogaMenuBody">
  
  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayUsers</xsl:with-param>
    <xsl:with-param name="label">DisplayUsers</xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayTeams</xsl:with-param>
    <xsl:with-param name="label">DisplayTeams</xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayResources</xsl:with-param>
    <xsl:with-param name="label">DisplayResources</xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayGroups</xsl:with-param>
    <xsl:with-param name="label">DisplayGroups</xsl:with-param>
  </xsl:call-template>

  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayExternalMioga</xsl:with-param>
    <xsl:with-param name="label">DisplayExternalMioga</xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayApplications</xsl:with-param>
    <xsl:with-param name="label">DisplayApplications</xsl:with-param>
  </xsl:call-template>

  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayTaskCategories</xsl:with-param>
    <xsl:with-param name="label">DisplayTaskCategories</xsl:with-param>
  </xsl:call-template>

  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayThemes</xsl:with-param>
    <xsl:with-param name="label">DisplayThemes</xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="MiogaMenuTab">
    <xsl:with-param name="href">DisplayProperties</xsl:with-param>
    <xsl:with-param name="label">DisplayProperties</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- =================================
     DisplayUsers search field name
     ================================= -->

<xsl:template match="Display*" mode="sll-field-name" name="FieldsName">
    <xsl:param name="name"/>
   
    <xsl:call-template name="mioga2-common-name-translation">
        <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>

</xsl:template>


<!-- =================================
       DisplayUsers change profile button
       ================================= -->
  
  <xsl:template match="Display*" mode="sll-checkboxes-button">
      
      <xsl:choose>
          <xsl:when test="local-name(.) ='DisplayProperties'">
              <xsl:call-template name="change-form-button">
                  <xsl:with-param name="name">change_prop_act</xsl:with-param>
              </xsl:call-template>
          </xsl:when>
          
          <xsl:otherwise>
              <xsl:call-template name="change-form-button">
                  <xsl:with-param name="name">change_profile_act</xsl:with-param>
              </xsl:call-template>
          </xsl:otherwise>
      </xsl:choose>
      
  </xsl:template>

<!-- =================================
     DisplayUsers field value translation
     ================================= -->

<xsl:template match="Display*" mode="sll-translate-values" name="TranslateValues">
    <xsl:param name="name"/>
    <xsl:param name="value"/>

    <xsl:call-template name="mioga2-common-value-translation">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value" select="$value"/>
    </xsl:call-template>

</xsl:template>

<xsl:template match="DisplayUsers|DisplayGroups|DisplayTeams|DisplayResources|DisplayExternalMioga">
    <xsl:variable name="current-func" select="local-name(.)"/>

    <body xsl:use-attribute-sets="body_attr">
        <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">administration.html#users</xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="MiogaMenu"/>

        <xsl:call-template name="MiogaTitle" />

        <div xsl:use-attribute-sets="app_body_attr">

            <xsl:if test="SimpleLargeList/List">
                <xsl:call-template name="MiogaAction"/>
            </xsl:if>

            <xsl:apply-templates select="SimpleLargeList">
                <xsl:with-param name="label" select="name(.)"/>
                <xsl:with-param name="advancedsearch">1</xsl:with-param>
                <xsl:with-param name="nb-search-column">
                    <xsl:choose>
                        <xsl:when test="$current-func='DisplayUsers'">2</xsl:when>
                        <xsl:when test="$current-func='DisplayResources'">2</xsl:when>
                        <xsl:otherwise>1</xsl:otherwise>
                    </xsl:choose>
                </xsl:with-param>
            </xsl:apply-templates>

            <xsl:call-template name="MiogaMessage">
              <xsl:with-param name="message"><xsl:call-template name="searchResultTranslation" /></xsl:with-param>
            </xsl:call-template>
        </div>
        
    </body>
</xsl:template>

<!-- =================================
     DisplayApplications template
     ================================= -->
<xsl:template match="DisplayApplications">
  <body xsl:use-attribute-sets="body_attr">
      <xsl:call-template name="DisplayAppTitle">
          <xsl:with-param name="help">administration.html#apps</xsl:with-param>
      </xsl:call-template>
      
      <xsl:call-template name="MiogaMenu"/>
      
      <xsl:call-template name="MiogaTitle" />
      
      <div xsl:use-attribute-sets="app_body_attr">
          
          <xsl:apply-templates select="SimpleLargeList">
              <xsl:with-param name="label" select="name()"/>
          </xsl:apply-templates>
          
      </div>
      
  </body>
</xsl:template>

<xsl:template match="DisplayApplicationAllowed*">
    <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">administration.html#apps</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>
    
    <div xsl:use-attribute-sets="app_body_attr">
            
        <xsl:if test="SimpleLargeList/List">
            <xsl:call-template name="MiogaAction">
                <xsl:with-param name="wrap">1</xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    
        <xsl:if test="SimpleLargeList/List and count(SimpleLargeList/List/Row) = 0">
            <xsl:call-template name="MiogaMessage">
                <xsl:with-param name="message">
                    <xsl:apply-templates select="." mode="empty-list"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>

        <xsl:if test="count(SimpleLargeList/List) = 0 or count(SimpleLargeList/List/Row) != 0">
            <xsl:apply-templates select="SimpleLargeList"/>
        </xsl:if>
        
    </div>

    <div xsl:use-attribute-sets="app_footer_attr">
        <xsl:if test="SimpleLargeList/List">
            <xsl:call-template name="back-button">
                <xsl:with-param name="href"><xsl:value-of select="./referer" /></xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </div>

  </body>
</xsl:template>


<!-- =================================
     DisallowApplicationForAllUsers
     ================================= -->
<xsl:template match="DisallowApplicationForAllUsers">
<body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">administration.html#apps</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>

    <div xsl:use-attribute-sets="app_body_attr">
        
        <xsl:call-template name="MiogaMessage">
            <xsl:with-param name="message">
                <xsl:call-template name="adminMessage">
                    <xsl:with-param name="type">disallow-apps-user</xsl:with-param>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>

    </div>

    <div xsl:use-attribute-sets="app_footer_attr">
        <xsl:call-template name="ok-cancel-button">
            <xsl:with-param name="ok-href">DisallowApplicationForAllUsers?disallow_application_ok=1</xsl:with-param>
            <xsl:with-param name="cancel-href" select="referer"/>
        </xsl:call-template>
    </div>
</body>   
</xsl:template>

<!-- =================================
     DisallowApplicationForAllGroups
     ================================= -->
<xsl:template match="DisallowApplicationForAllGroups">
    <body xsl:use-attribute-sets="body_attr">
        <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">administration.html#apps</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="MiogaTitle"/>
        
        <div xsl:use-attribute-sets="app_body_attr">
            
            <xsl:call-template name="MiogaMessage">
                <xsl:with-param name="message">
                    <xsl:call-template name="adminMessage">
                        <xsl:with-param name="type">disallow-apps-group</xsl:with-param>
                    </xsl:call-template>
                </xsl:with-param>
            </xsl:call-template>
            
        </div>
        
        <div xsl:use-attribute-sets="app_footer_attr">
            <xsl:call-template name="ok-cancel-button">
                <xsl:with-param name="ok-href">DisallowApplicationForAllGroups?disallow_application_ok=1</xsl:with-param>
                <xsl:with-param name="cancel-href" select="referer"/>
            </xsl:call-template>
        </div>
    </body>
</xsl:template>


<!-- =================================
     CreateGroup ModifyGroup CreateLocalUser 
     CreateTeam and ModifyTeam slls templates 
     callback.
     ================================= -->

<xsl:template match="UsersList|TeamsList|GroupsList" mode="sll-field-name">
    <xsl:param name="name"/>

    <xsl:call-template name="FieldsName">
        <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>

</xsl:template>

<xsl:template match="UsersList|TeamsList|GroupsList" mode="sll-translate-values">
    <xsl:param name="name"/>
    <xsl:param name="value"/>
    
    <xsl:value-of select="$value"/>
</xsl:template>


<!-- =================================
     CreateGroup and ModifyGroup form description 
     template
     ================================= -->

<xsl:template match="CreateResource|CreateGroup|CreateTeam|CreateLocalUser|CreateExternalMioga|ModifyGroup|ModifyUser|ModifyResource|CreateTheme|CreateLang" mode="arg-checker-field-name">
    <xsl:param name="name"/>

    <xsl:call-template name="FieldsName">
        <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="CreateGroup|ModifyGroup" mode="MiogaFormBody">

    <xsl:choose>
        <xsl:when test="local-name(.) = 'CreateGroup'">
            <xsl:call-template name="MiogaFormInputText">
                <xsl:with-param name="label">ident</xsl:with-param>
            </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">ident</xsl:with-param>
            </xsl:call-template>            
        </xsl:otherwise>
    </xsl:choose>

    <xsl:if test="local-name(.) = 'CreateGroup' and count(GroupSkeletons/skeleton) != 0">
        <xsl:call-template name="MiogaFormInputSelect">
            <xsl:with-param name="label">skeleton</xsl:with-param>
            <xsl:with-param name="choices" select="GroupSkeletons/skeleton/@file"/>
        </xsl:call-template>
    </xsl:if>

    <xsl:call-template name="MiogaFormInputExternalSelectSingle">
        <xsl:with-param name="label">anim_id</xsl:with-param>
        <xsl:with-param name="disp_value" select="fields/anim_ident"/>
        <xsl:with-param name="action">select_anim_act</xsl:with-param>
    </xsl:call-template>
        
    <xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">public_part</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">history</xsl:with-param>
    </xsl:call-template>
        
    <xsl:call-template name="MiogaFormTinyVertRule"/>
    
    <xsl:call-template name="MiogaFormInputTextArea">
        <xsl:with-param name="label">description</xsl:with-param>        
    </xsl:call-template>
        
    <xsl:call-template name="MiogaFormHorizRule"/>
    
    <xsl:call-template name="MiogaFormInputExternalSelectMultiple">
        <xsl:with-param name="label">users</xsl:with-param>
        <xsl:with-param name="action">act_add_user</xsl:with-param>
        <xsl:with-param name="list-name">UsersList</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormTinyVertSep"/>

    <xsl:call-template name="MiogaFormInputExternalSelectMultiple">
        <xsl:with-param name="label">teams</xsl:with-param>
        <xsl:with-param name="action">act_add_team</xsl:with-param>
        <xsl:with-param name="list-name">TeamsList</xsl:with-param>
    </xsl:call-template>

</xsl:template>

<xsl:template match="CreateGroup|ModifyGroup" mode="MiogaFormButton">
      <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">
            <xsl:choose>
                <xsl:when test="name(.)='ModifyGroup'">group_act_modify</xsl:when>
                <xsl:when test="name(.)='CreateGroup'">group_act_create</xsl:when>
            </xsl:choose>
        </xsl:with-param>
        <xsl:with-param name="referer" select="referer"/>
      </xsl:call-template>
</xsl:template>


<!-- =================================
     CreateGroup and ModifyGroup templates
     ================================= -->

<xsl:template match="CreateGroup|ModifyGroup">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">administration.html#groups</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>
        
    <div xsl:use-attribute-sets="app_form_body_attr">
        <xsl:call-template name="arg_errors"/>

           
        <xsl:call-template name="MiogaForm">
            <xsl:with-param name="label">
                <xsl:choose>
                    <xsl:when test="name(.)='CreateGroup'">create_group_form</xsl:when>
                    <xsl:when test="name(.)='ModifyGroup'">modify_group_form</xsl:when>
                </xsl:choose>
            </xsl:with-param>
            <xsl:with-param name="action" select="name(.)"/>
            <xsl:with-param name="method">POST</xsl:with-param>
        </xsl:call-template>

    </div>
  </body>
</xsl:template>

<!-- =================================
     CreateTeam and ModifyTeam form description 
     template
     ================================= -->

<xsl:template match="CreateTeam|ModifyTeam" mode="MiogaFormBody">

    <xsl:choose>
        <xsl:when test="local-name(.) = 'CreateTeam'">
            <xsl:call-template name="MiogaFormInputText">
                <xsl:with-param name="label">ident</xsl:with-param>
            </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">ident</xsl:with-param>
            </xsl:call-template>            
        </xsl:otherwise>
    </xsl:choose>

    <!--
         <xsl:call-template name="MiogaFormTinyVertRule"/>

         <xsl:call-template name="MiogaFormInputTextArea">
             <xsl:with-param name="label">description</xsl:with-param>        
         </xsl:call-template>
         -->

    <xsl:call-template name="MiogaFormHorizRule"/>

    <xsl:call-template name="MiogaFormInputExternalSelectMultiple">
        <xsl:with-param name="label">users</xsl:with-param>
        <xsl:with-param name="action">act_add_user</xsl:with-param>
        <xsl:with-param name="list-name">UsersList</xsl:with-param>
    </xsl:call-template>

</xsl:template>

<xsl:template match="CreateTeam|ModifyTeam" mode="MiogaFormButton">
      <xsl:call-template name="ok-cancel-form-button">
          <xsl:with-param name="name">
              <xsl:choose>
                  <xsl:when test="name(.)='ModifyTeam'">team_act_modify</xsl:when>
                  <xsl:when test="name(.)='CreateTeam'">team_act_create</xsl:when>
              </xsl:choose>
          </xsl:with-param>
          <xsl:with-param name="referer" select="referer"/>
      </xsl:call-template>
</xsl:template>

<!-- =================================
     CreateTeam and ModifyTeam templates
     ================================= -->

<xsl:template match="CreateTeam|ModifyTeam">
<body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">administration.html#teams</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>

    <div xsl:use-attribute-sets="app_form_body_attr">
        <xsl:call-template name="arg_errors"/>
        
        <xsl:call-template name="MiogaForm">
            <xsl:with-param name="label">
                <xsl:choose>
                    <xsl:when test="name(.)='CreateTeam'">create_team_form</xsl:when>
                    <xsl:when test="name(.)='ModifyTeam'">modify_team_form</xsl:when>
                </xsl:choose>
            </xsl:with-param>
            <xsl:with-param name="action" select="name(.)"/>
            <xsl:with-param name="method">POST</xsl:with-param>
        </xsl:call-template>

    </div>
  </body>
</xsl:template>



<!-- =================================
     CreateResource and ModifyResource form description 
     template
     ================================= -->

<xsl:template match="CreateResource|ModifyResource" mode="MiogaFormBody">
    <xsl:choose>
        <xsl:when test="local-name(.) = 'CreateResource'">
            <xsl:call-template name="MiogaFormInputText">
                <xsl:with-param name="label">ident</xsl:with-param>
            </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">ident</xsl:with-param>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>

    <xsl:if test="local-name(.) = 'CreateResource' and count(ResourceSkeletons/skeleton) != 0">
        <xsl:call-template name="MiogaFormInputSelect">
            <xsl:with-param name="label">skeleton</xsl:with-param>
            <xsl:with-param name="choices" select="ResourceSkeletons/skeleton/@file"/>
        </xsl:call-template>
    </xsl:if>

    <xsl:if test="local-name(.) = 'ModifyResource'">
        <xsl:call-template name="MiogaFormDisplayText">
            <xsl:with-param name="label">status</xsl:with-param>
            <xsl:with-param name="value">
                <xsl:call-template name="TranslateValues">
                    <xsl:with-param name="name">status</xsl:with-param>
                    <xsl:with-param name="value"><xsl:value-of select="fields/status"/></xsl:with-param>
                </xsl:call-template>                
            </xsl:with-param>
        </xsl:call-template>
    </xsl:if>

    <!--
         <xsl:call-template name="MiogaFormTinyVertRule"/>
         
         <xsl:call-template name="MiogaFormInputTextArea">
             <xsl:with-param name="label">description</xsl:with-param>        
         </xsl:call-template>
         -->
        
    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">location</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputExternalSelectSingle">
        <xsl:with-param name="label">anim_id</xsl:with-param>
        <xsl:with-param name="disp_value" select="fields/anim_ident"/>
        <xsl:with-param name="action">select_anim_act</xsl:with-param>
    </xsl:call-template>
    
</xsl:template>

<xsl:template match="CreateResource|ModifyResource" mode="MiogaFormButton">
      <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">
            <xsl:choose>
                <xsl:when test="name(.)='ModifyResource'">resource_act_modify</xsl:when>
                <xsl:when test="name(.)='CreateResource'">resource_act_create</xsl:when>
            </xsl:choose>
        </xsl:with-param>
        <xsl:with-param name="referer" select="referer"/>
      </xsl:call-template>
</xsl:template>


<!-- =================================
     CreateResource and ModifyResource templates
     ================================= -->

<xsl:template match="CreateResource|ModifyResource">
<body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">administration.html#resources</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>

    <div xsl:use-attribute-sets="app_form_body_attr">
        <xsl:if test="name(.)='ModifyResource'">
            <xsl:call-template name="MiogaAction"/>
        </xsl:if>

        <xsl:call-template name="arg_errors"/>
        
        <xsl:call-template name="MiogaForm">
            <xsl:with-param name="label">
                <xsl:choose>
                    <xsl:when test="name(.)='CreateResource'">create_resource_form</xsl:when>
                    <xsl:when test="name(.)='ModifyResource'">modify_resource_form</xsl:when>
                </xsl:choose>
            </xsl:with-param>
            <xsl:with-param name="action" select="name(.)"/>
            <xsl:with-param name="method">POST</xsl:with-param>
        </xsl:call-template>

    </div>
  </body>
</xsl:template>


<!-- =================================
     CreateLocalUser and ModifyUser form description
     templates
     ================================= -->

<xsl:template match="CreateLocalUser|ModifyUser" mode="MiogaFormBody">

    <xsl:choose>
        <xsl:when test="name(.) = 'CreateLocalUser'">
            <xsl:call-template name="MiogaFormInputText">
                <xsl:with-param name="label">ident</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">ident</xsl:with-param>
            </xsl:call-template>            
        </xsl:otherwise>
    </xsl:choose>

    <xsl:if test="local-name(.) = 'CreateLocalUser' and count(UserSkeletons/skeleton) != 0">
        <xsl:call-template name="MiogaFormInputSelect">
            <xsl:with-param name="label">skeleton</xsl:with-param>
            <xsl:with-param name="choices" select="UserSkeletons/skeleton/@file"/>
        </xsl:call-template>
    </xsl:if>


    <xsl:if test="local-name(.) = 'ModifyUser'">
        <xsl:call-template name="MiogaFormDisplayText">
            <xsl:with-param name="label">type</xsl:with-param>
            <xsl:with-param name="value">
                <xsl:call-template name="TranslateValues">
                    <xsl:with-param name="name">type</xsl:with-param>
                    <xsl:with-param name="value"><xsl:value-of select="fields/type"/></xsl:with-param>
                </xsl:call-template>                
            </xsl:with-param>
        </xsl:call-template>

        <xsl:if test="fields/type = 'external_user'">
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">server</xsl:with-param>
                <xsl:with-param name="value">
                    <xsl:call-template name="TranslateValues">
                        <xsl:with-param name="name">server</xsl:with-param>
                        <xsl:with-param name="value"><xsl:value-of select="fields/server"/></xsl:with-param>
                    </xsl:call-template>                
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>

        <xsl:call-template name="MiogaFormDisplayText">
            <xsl:with-param name="label">status</xsl:with-param>
            <xsl:with-param name="value">
                <xsl:call-template name="TranslateValues">
                    <xsl:with-param name="name">status</xsl:with-param>
                    <xsl:with-param name="value"><xsl:value-of select="fields/status"/></xsl:with-param>
                </xsl:call-template>                
            </xsl:with-param>
        </xsl:call-template>

    </xsl:if>

    <xsl:choose>
        <xsl:when test="local-name(.) != 'ModifyUser' or fields/type = 'local_user'">
            <xsl:call-template name="MiogaFormInputText">
                <xsl:with-param name="label">firstname</xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="MiogaFormInputText">
                <xsl:with-param name="label">lastname</xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="MiogaFormInputText">
                <xsl:with-param name="label">email</xsl:with-param>
            </xsl:call-template>

        </xsl:when>

        <xsl:otherwise>
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">firstname</xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">lastname</xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="MiogaFormDisplayText">
                <xsl:with-param name="label">email</xsl:with-param>
            </xsl:call-template>

            
        </xsl:otherwise>
    </xsl:choose>

    <xsl:if test="local-name(.) != 'ModifyUser' or fields/type = 'local_user'">

        <xsl:call-template name="MiogaFormInputPassword">
            <xsl:with-param name="label">password</xsl:with-param>
        </xsl:call-template>
    
        <xsl:call-template name="MiogaFormInputPassword">
            <xsl:with-param name="label">password2</xsl:with-param>
        </xsl:call-template>
    
    </xsl:if>

    <xsl:if test="local-name(.) != 'ModifyUser' or fields/type != 'external_user'">
        <xsl:call-template name="MiogaFormInputCheckbox">
            <xsl:with-param name="label">public_part</xsl:with-param>
        </xsl:call-template>

        <xsl:if test="fields/autonomous=1 or fields/autonomous='on'">
            <input type="hidden" name="autonomous" value="on"/>
        </xsl:if>

    </xsl:if>

    <xsl:call-template name="MiogaFormHorizRule"/>
   
    <xsl:call-template name="MiogaFormInputExternalSelectMultiple">
        <xsl:with-param name="label">teams</xsl:with-param>
        <xsl:with-param name="action">act_add_team</xsl:with-param>
        <xsl:with-param name="list-name">TeamsList</xsl:with-param>
    </xsl:call-template>
    
    <xsl:call-template name="MiogaFormTinyVertSep"/>

    <xsl:call-template name="MiogaFormInputExternalSelectMultiple">
        <xsl:with-param name="label">groups</xsl:with-param>
        <xsl:with-param name="action">act_add_group</xsl:with-param>
        <xsl:with-param name="list-name">GroupsList</xsl:with-param>
    </xsl:call-template>
   
</xsl:template>

<xsl:template match="CreateLocalUser|ModifyUser" mode="MiogaFormButton">
  <xsl:call-template name="ok-cancel-form-button">
    <xsl:with-param name="name">
      <xsl:choose>
        <xsl:when test="name(.)='ModifyUser'">user_act_modify</xsl:when>
        <xsl:when test="name(.)='CreateLocalUser'">user_act_create</xsl:when>
      </xsl:choose>
    </xsl:with-param>
    <xsl:with-param name="referer" select="referer"/>
  </xsl:call-template>
</xsl:template>

<!-- =================================
     CreateLocalUser and ModifyUser templates
     ================================= -->

<xsl:template match="CreateLocalUser|ModifyUser">
<body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">administration.html#users</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>

    <div xsl:use-attribute-sets="app_form_body_attr">
        <xsl:if test="name(.)='ModifyUser'">
            <xsl:call-template name="MiogaAction"/>
        </xsl:if>

        <xsl:call-template name="arg_errors"/>
        
        <xsl:call-template name="MiogaForm">
            <xsl:with-param name="label">
                <xsl:choose>
                    <xsl:when test="name(.)='CreateLocalUser'">create_user_form</xsl:when>
                    <xsl:when test="name(.)='ModifyUser'">modify_user_form</xsl:when>
                </xsl:choose>
            </xsl:with-param>
            <xsl:with-param name="name"><xsl:value-of select="name(.)"/>Sll</xsl:with-param>
            <xsl:with-param name="action" select="name(.)"/>
            <xsl:with-param name="method">POST</xsl:with-param>
        </xsl:call-template>

    </div>
  </body>
</xsl:template>

<!-- =================================
     CreateExternalMioga template
     ================================= -->

<xsl:template match="CreateExternalMioga" mode="MiogaFormBody">
    <xsl:call-template name="MiogaFormInputUploadFile">
        <xsl:with-param name="label">public_key_file</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template match="CreateExternalMioga" mode="MiogaFormButton">
  <xsl:call-template name="ok-cancel-form-button">
    <xsl:with-param name="name">ext_mioga_act_import</xsl:with-param>
    <xsl:with-param name="referer" select="referer"/>
  </xsl:call-template>
</xsl:template>

<xsl:template match="CreateExternalMioga">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">administration.html#external</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>
    <div xsl:use-attribute-sets="app_form_body_attr">
      <xsl:call-template name="arg_errors"/>
      <xsl:call-template name="MiogaForm">
        <xsl:with-param name="label">add_external_mioga</xsl:with-param>
        <xsl:with-param name="action" select="name(.)"/>
        <xsl:with-param name="method">POST</xsl:with-param>
        <xsl:with-param name="enctype">multipart/form-data</xsl:with-param>
      </xsl:call-template>
    </div>
  </body>
</xsl:template>

<!-- =================================
     DisplayThemes
     ================================= -->

<xsl:template match="DisplayThemes">
    <xsl:variable name="current-func" select="local-name(.)"/>

    <body xsl:use-attribute-sets="body_attr">
        <xsl:call-template name="DisplayAppTitle">
            <xsl:with-param name="help">administration.html#theme</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="MiogaMenu"/>

        <xsl:call-template name="MiogaTitle" />

        <div xsl:use-attribute-sets="app_body_attr">

            <xsl:call-template name="MiogaAction" />

            <table align="center">
                <tr>
                    <td>
						<div class="sbox">
						<h2 class="title_bg_color" >Thèmes</h2>
                        <table xsl:use-attribute-sets="mioga-content-table" align="center">
                		<xsl:for-each select="./Themes/theme">
                    		<tr>
								<xsl:choose>
									<xsl:when test="position() mod 2 = 1">
										<xsl:attribute name="class">odd</xsl:attribute>
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="class">even</xsl:attribute>
									</xsl:otherwise>
								</xsl:choose>
                        		<td>
                                	<xsl:call-template name="themeNameTranslation" />
                        		</td>
                        		<td>
                                	<a href="DisplayThemeDetails?rowid={@rowid}"><img src="{$image_uri}/edit-text.gif" /></a>
                        		</td>
                    		</tr>
                    	</xsl:for-each>
						</table>
						</div>
					</td>
					<td>
						<div class="sbox">
						<h2 class="title_bg_color" >Langues</h2>
                        <table xsl:use-attribute-sets="mioga-content-table" align="center">
                		<xsl:for-each select="./Languages/lang">
                    		<tr>
								<xsl:choose>
									<xsl:when test="position() mod 2 = 1">
										<xsl:attribute name="class">odd</xsl:attribute>
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="class">even</xsl:attribute>
									</xsl:otherwise>
								</xsl:choose>
                            	<td><xsl:value-of select="./@locale"/></td>
                        	</tr>
                    	</xsl:for-each>
            			</table>
						</div>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</xsl:template>


<!-- =================================
     DisplayThemeDetails
     ================================= -->

<xsl:template match="DisplayThemeDetails">
<xsl:variable name="current-func" select="local-name(.)"/>
<body xsl:use-attribute-sets="body_attr">
	<xsl:call-template name="DisplayAppTitle">
		<xsl:with-param name="help">administration.html#theme</xsl:with-param>
	</xsl:call-template>
	<xsl:call-template name="MiogaMenu"/>

	<xsl:call-template name="MiogaTitle" />

	<div class="theme_details">
	<xsl:for-each select="dir_diff">
		<h2><xsl:value-of select="./@label" /></h2>
		<table class="dir_diff">
			<tr class="header">
				<th><xsl:value-of select="./@theme1" /></th>
				<th><xsl:value-of select="./@theme2" /></th>
			</tr>
			<xsl:for-each select="diff">
				<xsl:variable name="class">
					<xsl:choose>
						<xsl:when test="position() mod 2 = 1">odd</xsl:when>
						<xsl:otherwise>even</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<tr class="{$class}">
					<td class="filename"><xsl:value-of select="ls" /></td>
					<td class="filename"><xsl:value-of select="rs" /></td>
				</tr>
				<xsl:for-each select="fdiff">
					<tr class="{$class}">
						<td class="fdiff"><span class="line_number"><xsl:value-of select="ls/@line" />&#160;:&#160;</span><xsl:value-of select="ls" /></td>
						<td class="fdiff"><xsl:value-of select="rs" /></td>
					</tr>
				</xsl:for-each>
			</xsl:for-each>
			<tr class="header">
				<th></th>
				<th></th>
			</tr>
		</table>
	</xsl:for-each>
	</div>
</body>
</xsl:template>

<!-- =================================
     CreateTheme template
     ================================= -->

<xsl:template match="CreateTheme|CreateLang" mode="MiogaFormBody">

    <xsl:if test="name(.) = 'CreateLang'">
        <xsl:call-template name="MiogaFormInputSelect">
            <xsl:with-param name="label">theme_id</xsl:with-param>
            <xsl:with-param name="choices" select="./Themes/theme/@rowid"/>
        </xsl:call-template>
    </xsl:if>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">ident</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">name</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">path</xsl:with-param>
    </xsl:call-template>

</xsl:template>

<xsl:template match="CreateTheme|CreateLang" mode="MiogaFormButton">

    <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">act_ok</xsl:with-param>
        <xsl:with-param name="referer" select="referer"/>
    </xsl:call-template>

</xsl:template>


<xsl:template match="CreateTheme|CreateLang">
  <body xsl:use-attribute-sets="body_attr">

    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">administration.html#themes</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>
        
    <div xsl:use-attribute-sets="app_form_body_attr">

        <xsl:call-template name="arg_errors"/>

           
        <xsl:call-template name="MiogaForm">
            <xsl:with-param name="label" select="name(.)"/>
            <xsl:with-param name="action" select="name(.)"/>
            <xsl:with-param name="method">POST</xsl:with-param>
        </xsl:call-template>

    </div>

  </body>
</xsl:template>



<!-- =================================
     DisableUser template
     ================================= -->

<xsl:template match="DisableUser">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">administration.html#users</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>
    
    <div xsl:use-attribute-sets="app_body_attr">
      <xsl:call-template name="arg_errors"/>
    </div>

    <div xsl:use-attribute-sets="app_footer_attr">
      <xsl:call-template name="back-button">
        <xsl:with-param name="href" select="referer"/>
      </xsl:call-template>
    </div>
  </body>
</xsl:template>



<!-- =================================
     EnableUser template
     ================================= -->

<xsl:template match="EnableUser">
  <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle">
        <xsl:with-param name="help">administration.html#users</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="MiogaTitle"/>
    
    <div xsl:use-attribute-sets="app_body_attr">
      <xsl:call-template name="arg_errors"/>
    </div>

    <div xsl:use-attribute-sets="app_footer_attr">
      <xsl:call-template name="back-button">
        <xsl:with-param name="href" select="referer"/>
      </xsl:call-template>
    </div>
  </body>
</xsl:template>

<!-- =================================
     Search form title
     ================================= -->

<xsl:template name="SllSearchFormTitle">
  <xsl:variable name="current-func" select="local-name(../..)"/>
</xsl:template>


<!-- =================================
     Search form body add-ons
     ================================= -->

<xsl:template match="DisplayApplications" mode="advanced-search-field-content">
    <xsl:param name="name"/>
    <xsl:param name="value"/>

    <xsl:call-template name="SllAdvSearchFieldContent">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value" select="$value"/>
    </xsl:call-template>
   
</xsl:template>


<xsl:template match="DisplayGroups|DisplayTeams" mode="advanced-search-field-content">
    <xsl:param name="name"/>
    <xsl:param name="value"/>

    <xsl:call-template name="SllAdvSearchFieldContent">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value" select="$value"/>
    </xsl:call-template>           
    
</xsl:template>

<xsl:template match="DisplayUsers" mode="advanced-search-field-content">
    <xsl:param name="name"/>
    <xsl:param name="value"/>

    <xsl:choose>
      <xsl:when test="$name='sll_search_type'">
          <xsl:call-template name="MiogaFormInputSelect">
              <xsl:with-param name="label">sll_search_type</xsl:with-param>
              <xsl:with-param name="choices" select="GroupType/type[contains(., '_user')]"/>
              <xsl:with-param name="value" select="$value"/>
              <xsl:with-param name="empty-choice">1</xsl:with-param>
          </xsl:call-template>
      </xsl:when>
      <xsl:when test="$name='sll_search_status'">
          <xsl:call-template name="MiogaFormInputSelect">
              <xsl:with-param name="label">sll_search_status</xsl:with-param>
              <xsl:with-param name="choices" select="UserStatus/status"/>
              <xsl:with-param name="value" select="$value"/>
              <xsl:with-param name="empty-choice">1</xsl:with-param>
          </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="SllAdvSearchFieldContent">
          <xsl:with-param name="name" select="$name"/>
          <xsl:with-param name="value" select="$value"/>
        </xsl:call-template>
      </xsl:otherwise>
   </xsl:choose>
</xsl:template>


<xsl:template match="DisplayResources" mode="advanced-search-field-content">
    <xsl:param name="name"/>
    <xsl:param name="value"/>
   
    <xsl:choose>
        <xsl:when test="$name='sll_search_status'">
            <xsl:call-template name="MiogaFormInputSelect">
                <xsl:with-param name="label">sll_search_status</xsl:with-param>
                <xsl:with-param name="value" select="$value"/>
                <xsl:with-param name="choices" select="ResourceStatus/status"/>
                <xsl:with-param name="empty-choice">1</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="SllAdvSearchFieldContent">
                <xsl:with-param name="name" select="$name"/>
                <xsl:with-param name="value" select="$value"/>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<xsl:template match="DisplayExternalMioga" mode="advanced-search-field-content">
    <xsl:param name="name"/>
    <xsl:param name="value"/>

    <xsl:choose>
        <xsl:when test="$name='sll_search_status'">
             <xsl:call-template name="MiogaFormInputSelect">
                <xsl:with-param name="label">sll_search_status</xsl:with-param>
                <xsl:with-param name="value" select="$value"/>
                <xsl:with-param name="choices" select="ExternalMiogaStatus/status"/>
                <xsl:with-param name="empty-choice">1</xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="SllAdvSearchFieldContent">
                <xsl:with-param name="name" select="$name"/>
                <xsl:with-param name="value" select="$value"/>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>



<!-- *********************************************************************** -->
<!-- * DisplayTaskCategories                                               * -->
<!-- *********************************************************************** -->

<xsl:template match="DisplayTaskCategories" mode="sll-translate-values">
  <xsl:param name="name"/>
  <xsl:param name="value"/>
  <xsl:param name="row"/>
    
  <xsl:attribute name="style">background-color: <xsl:value-of select="$row/bgcolor"/>; color: <xsl:value-of select="$row/fgcolor"/></xsl:attribute>

  <xsl:choose>
    <xsl:when test="$value='default'">
      <xsl:call-template name="adminLabel">
        <xsl:with-param name="label">undefined</xsl:with-param>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$value"/></xsl:otherwise>
  </xsl:choose>
  <xsl:if test="$row/private = 1">
    <xsl:call-template name="adminLabel">
      <xsl:with-param name="label">private</xsl:with-param>
    </xsl:call-template>
  </xsl:if>
</xsl:template>

<!-- =================================
     DisplayProperties translation
     ================================= -->

<xsl:template match="DisplayProperties" mode="sll-field-name">
    <xsl:param name="name"/>
   
    <xsl:call-template name="DisplayPropertiesTranslation">
        <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>

</xsl:template>

<!--
     Advanced SearchForm body callback 
     -->
<xsl:template match="DisplayProperties" mode="advanced-search-field-content">
    <xsl:param name="name"/>
    <xsl:param name="value"/>

    <xsl:call-template name="SllAdvSearchFieldContent">
        <xsl:with-param name="name" select="$name"/>
        <xsl:with-param name="value" select="$value"/>
    </xsl:call-template>
   
</xsl:template>

<xsl:template name="AddPropertyTranslate">
	Ajouter une propriété
</xsl:template>

<xsl:template match="AddProperty">
	<body xsl:use-attribute-sets="body_attr">
        <xsl:call-template name="DisplayAppTitle"/>

        <xsl:call-template name="MiogaMenu"/>
        <xsl:call-template name="MiogaTitle"/>

        <div xsl:use-attribute-sets="app_body_attr">
			<form method="POST" action="AddProperty">
			<table border="0" cellspacing="0" cellpadding="5" bgcolor="white" align="center" style="border: 1px {$mioga-title-bg-color} solid;">
				<tr bgcolor="{$mioga-title-bg-color}">
					<td colspan="2">
						<p style="color: #0332b2; font-weight: bold; font-family: sans-serif; padding: 0.1em 0.1em 0em 0.5em;" align="left">
						<xsl:call-template name="AddPropertyTranslate"/>
						</p>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2"><input type="text" name="property"/></td>
				</tr>
				<tr>
					<td align="center">
						<input type="image" border="0" src="{$image_uri}/button_ok.gif"/>
					</td>
					<td align="center">
						<a href="DisplayProperties"><img border="0" src="{$image_uri}/button_cancel.gif"/></a>
					</td>
				</tr>
			</table>
			</form>
        </div>
   </body>
</xsl:template>

<xsl:template match="DisplayTaskCategories|DisplayProperties">
    <body xsl:use-attribute-sets="body_attr">
        <xsl:call-template name="DisplayAppTitle"/>

        <xsl:call-template name="MiogaMenu"/>
        <xsl:call-template name="MiogaTitle"/>

        <div xsl:use-attribute-sets="app_body_attr">

            <xsl:if test="SimpleLargeList/List">
                <xsl:call-template name="MiogaAction">
                    <xsl:with-param name="wrap">1</xsl:with-param>
                </xsl:call-template>
            </xsl:if>

            <xsl:apply-templates select="SimpleLargeList">
                <xsl:with-param name="advancedsearch">0</xsl:with-param>
                <xsl:with-param name="label" select="name()"/>
            </xsl:apply-templates>

        </div>
   </body>
</xsl:template>

<!-- =================================
     CreateTaskCategory
     ================================= -->

<xsl:template match="CreateTaskCategory|ModifyTaskCategory" mode="MiogaFormBody">

    <xsl:call-template name="MiogaFormInputText">
        <xsl:with-param name="label">name</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputSimpleColorSelect">
        <xsl:with-param name="label">bgcolor</xsl:with-param>
        <xsl:with-param name="adv-color-select-action">choose_color_act</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputSimpleColorSelect">
        <xsl:with-param name="label">fgcolor</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="MiogaFormInputCheckbox">
        <xsl:with-param name="label">private</xsl:with-param>
    </xsl:call-template>

</xsl:template>

<xsl:template match="CreateTaskCategory|ModifyTaskCategory" mode="MiogaFormButton">

    <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">
            <xsl:choose>
                <xsl:when test="name(.)='CreateTaskCategory'">category_create_act</xsl:when>
                <xsl:otherwise>category_modify_act</xsl:otherwise>
            </xsl:choose>
        </xsl:with-param>
        <xsl:with-param name="referer" select="referer"/>
    </xsl:call-template>

</xsl:template>

<xsl:template match="CreateTaskCategory|ModifyTaskCategory" mode="arg-checker-field-name">
    <xsl:param name="name"/>

    <xsl:call-template name="TaskCatFieldsName">
        <xsl:with-param name="name" select="$name"/>
    </xsl:call-template>
</xsl:template>

<xsl:template match="CreateTaskCategory|ModifyTaskCategory">
    <body xsl:use-attribute-sets="body_attr">
    <xsl:call-template name="DisplayAppTitle"/>
    <xsl:call-template name="MiogaTitle"/>
        
    <div xsl:use-attribute-sets="app_form_body_attr">
      <xsl:call-template name="arg_errors"/>
      
      
      <xsl:call-template name="MiogaForm">
          <xsl:with-param name="label" select="name()"/>
          <xsl:with-param name="action" select="name()"/>
          <xsl:with-param name="method">POST</xsl:with-param>
      </xsl:call-template>
      
    </div>
  </body>
</xsl:template>


<!-- =================================
     ChooseTaskCatColor
     ================================= -->

<xsl:template match="ChooseTaskCatColor" mode="MiogaFormBody">
    <xsl:call-template name="MiogaFormInputAdvColorSelect"/>
</xsl:template>

<xsl:template match="ChooseTaskCatColor" mode="MiogaFormButton">
    <xsl:call-template name="ok-cancel-form-button">
        <xsl:with-param name="name">choose_color_act</xsl:with-param>
        <xsl:with-param name="referer"><xsl:value-of select="referer"/>?back_from_color=1</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template match="ChooseTaskCatColor">
  <body xsl:use-attribute-sets="body_attr">
      <xsl:call-template name="DisplayAppTitle"/>
      <xsl:call-template name="MiogaTitle"/>
      
      <div xsl:use-attribute-sets="app_form_body_attr">
          <xsl:call-template name="MiogaForm">
              <xsl:with-param name="label" select="name(.)"/>
              <xsl:with-param name="action" select="name(.)"/>
              <xsl:with-param name="method">POST</xsl:with-param>
          </xsl:call-template>
      </div>
  </body>
</xsl:template>


<xsl:template match="DisplayApplicationAllowedUsers" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">AddUser</xsl:with-param>
    <xsl:with-param name="href">DisplayApplicationAllowedUsers?add_user_act=1</xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">AllowForAllUsers</xsl:with-param>
    <xsl:with-param name="href">
      AllowApplicationForAllUsers?rowid=<xsl:value-of select="./rowid"/>&amp;referer=<xsl:value-of select="referer"/>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="DisplayThemes" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">CreateTheme</xsl:with-param>
    <xsl:with-param name="href">CreateTheme</xsl:with-param>
  </xsl:call-template>

  <!--<xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">CreateLang</xsl:with-param>
    <xsl:with-param name="href">CreateLang</xsl:with-param>
  </xsl:call-template> -->
</xsl:template>

<xsl:template match="DisplayTaskCategories" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">CreateTaskCategory</xsl:with-param>
    <xsl:with-param name="href">CreateTaskCategory</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="DisplayProperties" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">AddProperty</xsl:with-param>
    <xsl:with-param name="href">AddProperty</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="ModifyUser" mode="MiogaActionBody">
  <xsl:choose>
    <xsl:when test="fields/status = 'disabled'">
      <xsl:call-template name="MiogaActionItem">
        <xsl:with-param name="label">EnableUser</xsl:with-param>
        <xsl:with-param name="href">EnableUser?rowid=<xsl:value-of select="fields/rowid"/>&amp;referer=<xsl:value-of select="referer"/></xsl:with-param>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="MiogaActionItem">
        <xsl:with-param name="label">DisableUser</xsl:with-param>
        <xsl:with-param name="href">DisableUser?rowid=<xsl:value-of select="fields/rowid"/>&amp;referer=<xsl:value-of select="referer"/></xsl:with-param>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>

  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">DeleteUser</xsl:with-param>
    <xsl:with-param name="href">
      DisplayUsers?select_delete_<xsl:value-of select="fields/rowid"/>=1&amp;sll_delete_act=1&amp;referer=<xsl:value-of select="referer"/>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="ModifyResource" mode="MiogaActionBody">
  <xsl:choose>
    <xsl:when test="fields/status = 'disabled'">
      <xsl:call-template name="MiogaActionItem">
        <xsl:with-param name="label">EnableResource</xsl:with-param>
        <xsl:with-param name="href">
          EnableResource?rowid=<xsl:value-of select="fields/rowid"/>&amp;referer=<xsl:value-of select="referer"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="MiogaActionItem">
        <xsl:with-param name="label">DisableResource</xsl:with-param>
        <xsl:with-param name="href">
          DisableResource?rowid=<xsl:value-of select="fields/rowid"/>&amp;referer=<xsl:value-of select="referer"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
  
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">DeleteResource</xsl:with-param>
    <xsl:with-param name="href">
      DisplayResources?select_delete_<xsl:value-of select="fields/rowid"/>=1&amp;sll_delete_act=1&amp;referer=<xsl:value-of select="referer"/>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="DisplayUsers" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">CreateLocalUser</xsl:with-param>
    <xsl:with-param name="href">CreateLocalUser</xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">InviteExternalUser</xsl:with-param>
    <xsl:with-param name="href">
        <xsl:value-of select="$private_bin_uri"/>/Select/SelectSingleExternalMioga?referer=<xsl:value-of select="$private_bin_uri"/>/Admin/InviteExternalUser&amp;action=select_ext_mioga_back&amp;title=<xsl:call-template name="adminLabel">
<xsl:with-param name="label">select-mioga</xsl:with-param></xsl:call-template>&amp;selusertitle=<xsl:call-template name="adminLabel">
<xsl:with-param name="label">select-ext-mioga-user</xsl:with-param>
</xsl:call-template>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="DisplayGroups" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">CreateGroup</xsl:with-param>
    <xsl:with-param name="href">CreateGroup</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="DisplayTeams" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">CreateTeam</xsl:with-param>
    <xsl:with-param name="href">CreateTeam</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="DisplayResources" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">CreateResource</xsl:with-param>
    <xsl:with-param name="href">CreateResource</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="DisplayExternalMioga" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">DownloadMyPublicKey</xsl:with-param>
    <xsl:with-param name="href">DownloadMyPublicKey</xsl:with-param>
  </xsl:call-template>

  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">CreateExternalMioga</xsl:with-param>
    <xsl:with-param name="href">CreateExternalMioga</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="DisplayApplicationAllowedGroups" mode="MiogaActionBody">
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">AddGroups</xsl:with-param>
    <xsl:with-param name="href">DisplayApplicationAllowedGroups?add_group_act=1</xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="MiogaActionItem">
    <xsl:with-param name="label">AllowForAllGroups</xsl:with-param>
    <xsl:with-param name="href">
      AllowApplicationForAllGroups?rowid=<xsl:value-of select="./rowid"/>&amp;referer=<xsl:value-of select="referer"/>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template name="MiogaFormInputSelectTranslation">
    <xsl:param name="label"/>
    <xsl:param name="value"/>

    <xsl:choose>
        <xsl:when test="starts-with($label, 'sll_search_')">
            <xsl:variable name="name" select="substring-after($label, 'sll_search_')"/>

            <xsl:call-template name="mioga2-common-value-translation">
                <xsl:with-param name="name" select="$name"/>
                <xsl:with-param name="value" select="$value"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:when test="$label='skeleton'"><xsl:value-of select=".."/></xsl:when>
        <xsl:when test="$label='theme_id'"><xsl:value-of select="../@name"/></xsl:when>
        <xsl:otherwise>            
            <xsl:value-of select="$value"/>            
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<!-- ====================
     == FR TRANSLATION ==
     ==================== -->

<xsl:template name="DisplayPropertiesTranslation">
	<xsl:param name="name"/>
	<xsl:choose>
		<xsl:when test="$name = 'property'">Propriété</xsl:when>
		<xsl:when test="$name = 'enabled'">Visible</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template name="MiogaMenuTranslation">
  <xsl:param name="label"/>
  <xsl:choose>
    <xsl:when test="$label='DisplayUsers'">Utilisateurs</xsl:when>
    <xsl:when test="$label='DisplayTeams'">Equipes</xsl:when>
    <xsl:when test="$label='DisplayGroups'">Groupes</xsl:when>
    <xsl:when test="$label='DisplayResources'">Ressources</xsl:when>
    <xsl:when test="$label='DisplayThemes'">Thèmes/Langues</xsl:when>
    <xsl:when test="$label='DisplayExternalMioga'">Mioga externes</xsl:when>
    <xsl:when test="$label='DisplayTaskCategories'">Catégories de tâches</xsl:when>
    <xsl:when test="$label='DisplayApplications'">Applications</xsl:when>
    <xsl:when test="$label='DisplayProperties'">Propriétés fichiers</xsl:when>
   </xsl:choose>

</xsl:template>

<xsl:template name="MiogaTitleTranslation">
  <xsl:choose>
    <xsl:when test="name(.)='DisplayUsers'">Gestion des utilisateurs</xsl:when>
    <xsl:when test="name(.)='DisplayGroups'">Gestion des groupes</xsl:when>
    <xsl:when test="name(.)='DisplayTeams'">Gestion des équipes</xsl:when>
    <xsl:when test="name(.)='DisplayThemes'">Gestion des thèmes et langues</xsl:when>
    <xsl:when test="name(.)='DisplayResources'">Gestion des ressources</xsl:when>
    <xsl:when test="name(.)='DisplayExternalMioga'">Gestion des Mioga externes</xsl:when>
    <xsl:when test="name(.)='DisplayApplications'">Gestion des applications</xsl:when>
    <xsl:when test="name(.)='DisplayProperties'">Gestion des propriétés des fichiers</xsl:when>
    <xsl:when test="name(.)='DisplayApplicationAllowedUsers'">Utilisateurs ayant accès à l'application : <xsl:value-of select="ident" /></xsl:when>
    <xsl:when test="name(.)='DisplayApplicationAllowedGroups'">Groupes ayant accès à l'application : <xsl:value-of select="ident" /></xsl:when>
    <xsl:when test="name(.)='DisallowApplicationForAllUsers'">Désactivation d'application pour tous les utilisateurs</xsl:when>
    <xsl:when test="name(.)='DisallowApplicationForAllGroups'">Désactivation d'appliction pour tous les groupes</xsl:when>
    <xsl:when test="name(.)='CreateGroup'">Création d'un groupe</xsl:when>
    <xsl:when test="name(.)='CreateTeam'">Création d'une équipe</xsl:when>
    <xsl:when test="name(.)='ModifyTeam'">Modification d'une équipe</xsl:when>
    <xsl:when test="name(.)='CreateResource'">Création d'une ressource</xsl:when>
    <xsl:when test="name(.)='CreateLocalUser'">Création d'un utilisateur local</xsl:when>
    <xsl:when test="name(.)='CreateExternalMioga'">Ajout d'un Mioga externe</xsl:when>
    <xsl:when test="name(.)='ModifyGroup'">Modification d'un groupe</xsl:when>
    <xsl:when test="name(.)='ModifyResource'">Modification d'une ressource</xsl:when>
    <xsl:when test="name(.)='DisableUser'">Désactivation d'un utilisateur</xsl:when>
    <xsl:when test="name(.)='EnableUser'">Activation d'un utilisateur</xsl:when>
    <xsl:when test="name(.)='DisplayTaskCategories'">Gestion des catégories de tâches</xsl:when>
    <xsl:when test="name(.)='CreateTaskCategory'">Créer une catégorie de tâche</xsl:when>
    <xsl:when test="name(.)='ModifyTaskCategory'">Modifier la catégorie de tâche <xsl:value-of select="fields/name"/></xsl:when>
    <xsl:when test="name(.)='ChooseTaskCatColor'">Sélectionner une couleur</xsl:when>
    <xsl:when test="name(.)='ModifyUser'">
      Modification de l'utilisateur
      <xsl:value-of select="fields/firstname"/>&#160;<xsl:value-of select="fields/lastname"/>&#160;(<xsl:value-of select="fields/ident"/>)
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="adminLabel">
  <xsl:param name="label" />
  <xsl:choose>
    <xsl:when test="$label='users'">Utilisateurs :</xsl:when>
    <xsl:when test="$label='teams'">Equipes :</xsl:when>
    <xsl:when test="$label='no-user'">Aucun utilisateur</xsl:when>
    <xsl:when test="$label='no-team'">Aucune équipe</xsl:when>
    <xsl:when test="$label='group-member'">Membre du groupe :</xsl:when>
    <xsl:when test="$label='working-group'">Groupe de travail :</xsl:when>
    <xsl:when test="$label='no-working-group'">Aucun groupe de travail</xsl:when>
    <xsl:when test="$label='undefined'">Non définie</xsl:when>
    <xsl:when test="$label='private'">( privée )</xsl:when>
    <xsl:when test="$label='select-mioga'">Sélectionnez le Mioga à partir duquel vous voulez inviter un utilisateur</xsl:when>
    <xsl:when test="$label='select-ext-mioga-user'">Invitation d'un utilisateur externe</xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="MiogaFormTranslation">
    <xsl:param name="label"/>

    <xsl:choose>
        <xsl:when test="$label='create_group_form'">Ajouter un groupe</xsl:when>
        <xsl:when test="$label='modify_group_form'">Modifier un groupe</xsl:when>
        <xsl:when test="$label='create_team_form'">Ajouter une équipe</xsl:when>
        <xsl:when test="$label='modify_team_form'">Modifier une équipe</xsl:when>
        <xsl:when test="$label='create_resource_form'">Ajouter une ressource</xsl:when>
        <xsl:when test="$label='modify_resource_form'">Modifier une ressource</xsl:when>
        <xsl:when test="$label='create_user_form'">Ajouter un utilisateur</xsl:when>
        <xsl:when test="$label='modify_user_form'">Modifier un utilisateur</xsl:when>
        <xsl:when test="$label='add_external_mioga'">Ajouter un Mioga externe</xsl:when>
        <xsl:when test="$label='invite_external_user'">Rechercher un utilisateur externe</xsl:when>
        <xsl:when test="$label='DisplayUsers'">Rechercher un utilisateur</xsl:when>
        <xsl:when test="$label='DisplayGroups'">Rechercher un groupe</xsl:when>
        <xsl:when test="$label='DisplayTeams'">Rechercher une équipe</xsl:when>
        <xsl:when test="$label='DisplayResources'">Rechercher une ressource</xsl:when>
        <xsl:when test="$label='DisplayExternalMioga'">Rechercher un mioga externe</xsl:when>
        <xsl:when test="$label='DisplayApplications'">Rechercher une application</xsl:when>
        <xsl:when test="$label='DisplayTaskCategories'">Rechercher une catégorie de tâches</xsl:when>
        <xsl:when test="$label='CreateTaskCategory'">Ajouter une catégorie</xsl:when>
        <xsl:when test="$label='ModifyTaskCategory'">Modifier une catégorie</xsl:when>
        <xsl:when test="$label='ChooseTaskCatColor'">Sélectionner une couleur</xsl:when>
        <xsl:when test="$label='CreateTheme'">Ajouter un theme</xsl:when>
        <xsl:when test="$label='CreateLang'">Ajouter une langue</xsl:when>
    </xsl:choose>
</xsl:template>

<xsl:template name="MiogaFormInputTranslation">
  <xsl:param name="label"/>
  <xsl:choose>
    
    <xsl:when test="$label='name'">Nom</xsl:when>
    <xsl:when test="$label='theme_id'">Thème</xsl:when>
    <xsl:when test="$label='bgcolor'">Couleur de fond</xsl:when>
    <xsl:when test="$label='fgcolor'">Couleur du texte</xsl:when>
    <xsl:when test="contains(name(), 'TaskCategory') and $label='private'">Tâches privées</xsl:when>
    <xsl:when test="$label='users'">Utilisateurs</xsl:when>
    <xsl:when test="$label='teams'">Equipes</xsl:when>
    <xsl:when test="$label='groups'">Groupes</xsl:when>
    <xsl:when test="starts-with($label, 'sll_search_')">
      <xsl:variable name="name" select="substring-after($label, 'sll_search_')"/>
      <xsl:call-template name="mioga2-common-name-translation">
        <xsl:with-param name="name" select="$name"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="mioga2-common-name-translation">
        <xsl:with-param name="name" select="$label"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="searchResultTranslation">
  <xsl:choose>
    <xsl:when test="name(.)='DisplayUsers' and count(SimpleLargeList/AskConfirmForDelete) = 0 and count(SimpleLargeList/List/Row) = 0 and count(SimpleLargeList/SuccessMessage) = 0">Aucun utilisateur affichable</xsl:when>
    <xsl:when test="name(.)='DisplayGroups' and count(SimpleLargeList/AskConfirmForDelete) = 0 and count(SimpleLargeList/List/Row) = 0 and count(SimpleLargeList/SuccessMessage) = 0">Aucun groupe affichable</xsl:when>
    <xsl:when test="name(.)='DisplayTeams' and count(SimpleLargeList/AskConfirmForDelete) = 0 and count(SimpleLargeList/List/Row) = 0 and count(SimpleLargeList/SuccessMessage) = 0">Aucune équipe affichable</xsl:when>
    <xsl:when test="name(.)='DisplayResources' and count(SimpleLargeList/AskConfirmForDelete) = 0 and count(SimpleLargeList/List/Row) = 0 and count(SimpleLargeList/SuccessMessage) = 0">Aucune ressource affichable</xsl:when>
    <xsl:when test="name(.)='DisplayExternalMioga' and count(SimpleLargeList/AskConfirmForDelete) = 0 and count(SimpleLargeList/List/Row) = 0 and count(SimpleLargeList/SuccessMessage) = 0">Aucun Mioga externe affichable</xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="adminMessage">
  <xsl:param name="type"/>

  <xsl:choose>
    <xsl:when test="$type='disallow-apps-user'">
      Voulez-vous réellement désactiver l'application "<xsl:value-of select="app_ident" />"
      <br/>
      pour tous les utilisateurs ?
    </xsl:when>
    <xsl:when test="$type='disallow-apps-group'">
      Voulez-vous réellement désactiver l'application "<xsl:value-of select="app_ident" />"
      <br/>
      pour tous les groupes ?
    </xsl:when>
    <xsl:when test="$type='min-char-number'">Vous devez saisir au moins un critère de recherche.</xsl:when>
    <xsl:when test="$type='insuffisant-results'">
      Cette recherche a retourné <xsl:value-of select="./Mioga2Response/UserList/@size"/> résultats.
      <br/>
      Veuillez affiner votre recherche.
    </xsl:when>
  </xsl:choose>
</xsl:template>

<!-- SuccessMessage -->
<xsl:template match="SuccessMessage">
  <xsl:variable name="message">
    <xsl:choose>
      <xsl:when test="name='group' or name='mioga_groups_sll'">Le groupe</xsl:when>
      <xsl:when test="name='team' or name='mioga_teams_sll'">L'équipe</xsl:when>
      <xsl:when test="name='theme'">Le thème</xsl:when>
      <xsl:when test="name='lang'">La langue</xsl:when>
      <xsl:when test="name='user' or name='mioga_users_sll'">L'utilisateur</xsl:when>
      <xsl:when test="name='resource' or name='mioga_resources_sll'">La ressource</xsl:when>
      <xsl:when test="name='external_mioga' or name='external_mioga_sll'">Le mioga externe</xsl:when>
      <xsl:when test="name='external_user' or name='external_user_failed'">Les utilisateurs externes</xsl:when>
      <xsl:when test="name='application_users'">Les utilisateurs ont été ajoutés</xsl:when>
      <xsl:when test="name='application_groups'">Les groupes ont été ajoutés</xsl:when>
      <xsl:when test="name='applications_allowed_group_sll'">Le groupe</xsl:when>
      <xsl:when test="name='applications_allowed_user_sll'">L'utilisateur</xsl:when>
      <xsl:when test="name='application'">L'application</xsl:when>
      <xsl:when test="name='task_categories_sll'">Les catégories de tâche</xsl:when>
      <xsl:when test="name='task_category'">La catégorie de tâche</xsl:when>
    </xsl:choose>&#160;<xsl:choose>
      <xsl:when test="name='external_user_failed'">n'ont pas pu être créés. Veuillez recommencer la procédure d'invitation.</xsl:when>
      <xsl:when test="name='external_user'">ont été invités</xsl:when>
      <xsl:when test="name='task_categories_sll'">ont été supprimées</xsl:when>
      <xsl:when test="action='create'">a été créé</xsl:when>
      <xsl:when test="name='application_users' or name='application_groups'"></xsl:when>
      <xsl:when test="contains(name, 'application_') = 0 and action='modify'">a été modifié</xsl:when>
      <xsl:when test="action='disable'">a été désactivé</xsl:when>
      <xsl:when test="action='enable'">a été réactivé</xsl:when>
      <xsl:when test="action='remove'">a été supprimé</xsl:when>
      <xsl:when test="action='invite'">a été invité</xsl:when>
      <xsl:when test="contains(name, 'application')">a été modifiée</xsl:when>
    </xsl:choose>
    <xsl:if test="name='resource' or name='mioga_resources_sll' or name='team' or name='mioga_teams_sll' or name='task_category' or name='lang'">e</xsl:if>
    <xsl:if test="name!='external_user_failed'">
        &#160;avec succès.
    </xsl:if>
  </xsl:variable>
  
  <xsl:call-template name="SuccessMessage">
    <xsl:with-param name="message" select="$message"/>
    <xsl:with-param name="referer" select="referer"/>
    <xsl:with-param name="reload_menu">1</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- =======================
     ARG CHECKER UNKNOWN ARG
     ======================= -->

<xsl:template match="CreateGroup|ModifyGroup" mode="arg-checker-unknown-args">
  <xsl:param name="name"/>
  <xsl:choose>
    <xsl:when test="$name = '__create__'">Erreur lors de la création du groupe : <xsl:value-of select="$arg"/>.</xsl:when>
    <xsl:when test="$name = '__modify__'">Erreur lors de la modification du groupe : <xsl:value-of select="$arg"/>.</xsl:when>
    <xsl:when test="$name = '__quota__'">Erreur lors de la création du groupe : nombre maximum de groupes autorisés dépassé.</xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="CreateTeam|ModifyTeam" mode="arg-checker-unknown-args">
  <xsl:param name="name"/>
  <xsl:choose>
    <xsl:when test="$name = '__create__'">Erreur lors de la création de l'équipe : <xsl:value-of select="$arg"/>.</xsl:when>
    <xsl:when test="$name = '__modify__'">Erreur lors de la modification de l'équipe : <xsl:value-of select="$arg"/>.</xsl:when>
    <xsl:when test="$name = '__quota__'">Erreur lors de la création de l'équipe : nombre maximum de groupes autorisés dépassé.</xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="CreateResource" mode="arg-checker-unknown-args">
  <xsl:param name="name"/>
  <xsl:choose>
    <xsl:when test="$name = '__create__'">Erreur lors de la création de la resource : <xsl:value-of select="$arg"/>.</xsl:when>
    <xsl:when test="$name = '__modify__'">Erreur lors de la modification de la resource : <xsl:value-of select="$arg"/>.</xsl:when>
    <xsl:when test="$name = '__quota__'">Erreur lors de la création de la resource : nombre maximum de groupes autorisés dépassé.</xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="CreateTaskCategory|ModifyTaskCategory" mode="arg-checker-unknown-args">
    <xsl:param name="name"/>
    <xsl:param name="arg"/>

    <xsl:if test="$arg='name'">
        Une catégorie ayant le même nom existe déjà.
    </xsl:if>
</xsl:template>

<xsl:template match="CreateLocalUser|ModifyUser|DisableUser|EnableUser" mode="arg-checker-unknown-args">
  <xsl:param name="name"/>
  <xsl:choose>
    <xsl:when test="$name = '__external_disabled__'">Vous ne pouvez pas réactiver cet utilisateur externe car son Mioga est désactivé.</xsl:when>
    <xsl:when test="$name = '__create__'">Erreur lors de la création de l'utilisateur : <xsl:value-of select="$arg"/>.</xsl:when>
    <xsl:when test="$name = 'different_passwords'">Les mots de passe sont différents<xsl:value-of select="$arg"/>.</xsl:when>
    <xsl:when test="$name = '__modify__'">Erreur lors de la modification de l'utilisateur : <xsl:value-of select="$arg"/>.</xsl:when>
    <xsl:when test="$name = '__disable__'">Erreur lors de la désactivation de l'utilisateur : <xsl:value-of select="$arg"/>.</xsl:when>
    <xsl:when test="$name = '__quota__'">Erreur lors de la création de l'utilisateur : nombre maximum de groupe autorisés dépassé.</xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="CreateExternalMioga" mode="arg-checker-unknown-args">
  <xsl:param name="name"/>
  <xsl:choose>
    <xsl:when test="$name = 'already_in_database'">Cette clé est déjà présente dans la base.</xsl:when>
    <xsl:when test="$name = 'bad_key'">Clé publique invalide.</xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="CreateTheme|CreateLang" mode="arg-checker-unknown-args">
  <xsl:param name="name"/>
  <xsl:choose>
    <xsl:when test="$name = '__theme__'">Erreur lors de la création du theme : <xsl:value-of select="$arg"/>.</xsl:when>
    <xsl:when test="$name = '__lang__'">Erreur lors de la création de la langue : <xsl:value-of select="$arg"/>.</xsl:when>
  </xsl:choose>
</xsl:template>


<!-- =======================
     ACTION BUTTON LIST BODY
     ======================= -->

<xsl:template name="MiogaActionTranslation">
  <xsl:param name="label"/>
  <xsl:choose>
    <xsl:when test="$label='CreateLocalUser'">Ajouter un utilisateur local</xsl:when>
    <xsl:when test="$label='InviteExternalUser'">Inviter un utilisateur externe</xsl:when>
    <xsl:when test="$label='CreateGroup'">Ajouter un groupe</xsl:when>
    <xsl:when test="$label='CreateTeam'">Ajouter une équipe</xsl:when>
    <xsl:when test="$label='CreateResource'">Ajouter une ressource</xsl:when>
    <xsl:when test="$label='DownloadMyPublicKey'">Télécharger ma clé publique</xsl:when>
    <xsl:when test="$label='CreateExternalMioga'">Ajouter un mioga externe</xsl:when>
    <xsl:when test="$label='AddGroups'">Ajouter des groupes</xsl:when>
    <xsl:when test="$label='AllowForAllGroups'">Rendre disponible pour tous les groupes</xsl:when>
    <xsl:when test="$label='DeleteResource'">Supprimer la ressource</xsl:when>
    <xsl:when test="$label='DisableResource'">Désactiver la ressource</xsl:when>
    <xsl:when test="$label='EnableResource'">Réactiver la ressource</xsl:when>
    <xsl:when test="$label='DeleteUser'">Supprimer l'utilisateur</xsl:when>
    <xsl:when test="$label='EnableUser'">Réactiver l'utilisateur</xsl:when>
    <xsl:when test="$label='DisableUser'">Désactiver l'utilisateur</xsl:when>
    <xsl:when test="$label='CreateTaskCategory'">Ajouter une catégorie</xsl:when>
    <xsl:when test="$label='AllowForAllUsers'">Rendre disponible pour tous les utilisateurs</xsl:when>
    <xsl:when test="$label='AddUser'">Ajouter des utilisateurs</xsl:when>
    <xsl:when test="$label='CreateTheme'">Ajouter un theme</xsl:when>
    <xsl:when test="$label='CreateLang'">Ajouter une langue</xsl:when>
    <xsl:when test="$label='AddProperty'">Ajouter une propriété</xsl:when>
  </xsl:choose>
</xsl:template>

<!-- =============
     MISCELLANEOUS
     ============= -->

<!-- ChangeExternalMiogaStatus deletion confirmation page -->
<xsl:template match="ChangeExternalMiogaStatus">
  <xsl:call-template name="Confirm">
    <xsl:with-param name="question">
      Etes vous sûr de vouloir
      <xsl:choose>
        <xsl:when test="current_status = 'active'">désactiver</xsl:when>
        <xsl:otherwise>activer</xsl:otherwise>
      </xsl:choose>
      le serveur
      <xsl:value-of select="mioga_server"/> ?
    </xsl:with-param>
    <xsl:with-param name="valid_action_uri">ChangeExternalMiogaStatus?act_confirm_delete=1</xsl:with-param>
    <xsl:with-param name="cancel_uri" select="referer"/>
  </xsl:call-template>
</xsl:template>

<xsl:template match="DisplayTaskCategories" mode="sll-field-name" name="TaskCatFieldsName">
  <xsl:param name="name"/>

  <xsl:call-template name="MiogaFormInputTranslation">
      <xsl:with-param name="label" select="$name"/>
  </xsl:call-template>

</xsl:template>

<xsl:template match="DisplayApplicationAllowedUsers" mode="empty-list">
  Aucun utilisateur ne peut utiliser cette application.
</xsl:template>

<xsl:template match="DisplayApplicationAllowedGroups" mode="empty-list">
  Aucun groupe ne peut utiliser cette application.
</xsl:template>

<xsl:template name="themeNameTranslation">
    Thème&#160;:&#160;<xsl:value-of select="@name"/>
</xsl:template>

</xsl:stylesheet>
