#
#===============================================================================
#
#         FILE:  990-instance.t
#
#  DESCRIPTION:  Destroy test instance
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project (), developers@mioga2.org
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      VERSION:  1.0
#      CREATED:  14/02/2014 15:17
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

use Test::More;

use Error qw(:try);
use Data::Dumper;

# Include test-suite constants
use Cwd 'abs_path';
use File::Basename;
use lib dirname( abs_path $0 );
require ('test_include.pm');

# Test count
plan tests => scalar (@$MiogaTest::instances);

use Mioga2::InstanceList;

use Mioga2::MiogaConf;
my $miogaconf = Mioga2::MiogaConf->new ('web/conf/Mioga.conf');

my $instance;
for my $inst (@$MiogaTest::instances) {
	$instance = Mioga2::InstanceList->new (
			$miogaconf,
			{
				attributes => {
					ident    => $inst->{ident},
				}
			}
		);
	ok (try_run (sub { $instance->Delete () }), 'Delete test instance: ' . $inst->{ident});
}

1;

