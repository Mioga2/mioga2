# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

#########################

use Test::More tests => 1;

diag ('TODO: Broken test-script is disabled, needs to be re-written !!!');
pass ();

__DATA__
use Error qw(:try);
plan tests => 3;
use Mioga2::Search::Document::File;
use Mioga2::Search::Query;
use Mioga2::Search::Finder;

my $finder;
ok ( $finder = Mioga2::Search::Finder->new( { base_dir => "t/", xapian_path => "xapiandb/", } ) );

my $qstring = "test1";
my $query;
ok ( $query = Mioga2::Search::Query->new( { mode => 'standard', lang => 'french', qstring => $qstring, } ) );


try {
	$finder->Process($query);
	$res = 1;
}
otherwise {
	$res = 0;
};
ok ( $res );

if (0) {
try {
	$indexer->Process($text_doc);
	$res = 1;
}
otherwise {
	$res = 0;
};
ok ( $res );
try {
	$indexer->Process($html_doc);
	$res = 1;
}
otherwise {
	$res = 0;
};
ok ( $res );
}
