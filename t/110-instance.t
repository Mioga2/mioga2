#
#===============================================================================
#
#         FILE:  110instance.t
#
#  DESCRIPTION:  Test script for Mioga2::InstanceList object
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project (), developers@mioga2.org
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      VERSION:  1.0
#      CREATED:  14/02/2014 09:14
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

use Test::More;

# Include test-suite constants
use Cwd 'abs_path';
use File::Basename;
use lib dirname( abs_path $0 );
require ('test_include.pm');

# Test count
plan tests => scalar (@$MiogaTest::instances) + 5;

my $miogaconf = Mioga2::MiogaConf->new ('web/conf/Mioga.conf');

my ($instance, $res);

require_ok ('Mioga2::InstanceList');

# Fancy display of instance idents
diag ("Test instances:\n" . join ("\n", map { "\t- " . $_->{ident} } @$MiogaTest::instances));

# Object instanciation
ok ($instance = Mioga2::InstanceList->new (
		$miogaconf,
		{
			attributes => {
				ident    => $MiogaTest::instances->[0]->{ident},
				skeleton => $MiogaTest::instances->[0]->{skeleton},
			}
		}
	), 'Instanciate Mioga2::InstanceList object');

# Check instance doesn't exist
ok ($instance->Count () == 0, "Instance doesn't exist");

# Instance creation
ok (try_run (sub { $instance->Store () }), 'Create instance');

# Instance deletion
ok (try_run (sub { $instance->Delete () }), 'Delete instance');

# Create instances for following test scripts
# WARNING: This block of code should remain at the end
for my $inst (@$MiogaTest::instances) {
	$instance = Mioga2::InstanceList->new (
			$miogaconf,
			{
				attributes => {
					ident    => $inst->{ident},
					skeleton => $inst->{skeleton}
				}
			}
		);
	ok (try_run (sub { $instance->Store () }), 'Create test instance: ' . $inst->{ident});
}
