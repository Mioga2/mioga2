#===============================================================================
#
#         FILE:  test_include.pm
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  14/02/2014 15:21
#===============================================================================
#
#  Copyright (c) year 2014, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME
	
test_include.pm

=head1 DESCRIPTION

Some defaults for Mioga2 test suite

=cut

#
#===============================================================================

use strict;
use warnings;

use Mioga2;

package MiogaTest;

# Base name for test instances
$MiogaTest::instance_base_name = 'test_';
$MiogaTest::instances = [
	{
		ident    => $MiogaTest::instance_base_name . $Mioga2::VERSION,
		skeleton => 't/skel/fr_FR/instance/standard.xml',
	},
	{
		ident    => $MiogaTest::instance_base_name . $Mioga2::VERSION . '-2',
		skeleton => 't/skel/fr_FR/instance/standard.xml',
	},
];

# Base name for test users
$MiogaTest::user_base_name = 'test_';
$MiogaTest::users = [
	{
		ident     => $MiogaTest::user_base_name . $Mioga2::VERSION,
		firstname => 'Firstname ' . $Mioga2::VERSION,
		lastname  => 'Lastname ' . $Mioga2::VERSION,
		email     => "email-" . $Mioga2::VERSION . '@noname.org',
		skeleton  => 't/skel/fr_FR/user/standard.xml',
	},
];

1;



package main;
use Error qw(:try);
use Data::Dumper;
use Mioga2::MiogaConf;
use Mioga2::Config;

# Run code into a try/otherwise block then return a status according to code execution success
sub try_run {
	my ($code, $silent) = @_;

	my $res;
	try {
		$code->();
		$res = 1;
	}
	otherwise {
		my ($err) = shift;
		print Dumper $err unless ($silent);
		$res= 0;
	};
	return ($res);
}

# Run code into a try/otherwise block then return a status according to code execution failure
sub try_fail {
	my ($code) = @_;

	my $res = try_run ($code, 1) ? 0 : 1;
	return ($res);
}

1;
