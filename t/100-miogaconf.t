#
#===============================================================================
#
#         FILE:  100-miogaconf.t
#
#  DESCRIPTION:  Test script for Mioga2::MiogaConf object
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project (), developers@mioga2.org
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      VERSION:  1.0
#      CREATED:  14/02/2014 09:23
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

use Test::More tests => 2;                      # last test to print

require_ok ('Mioga2::MiogaConf');
ok (my $miogaconf = Mioga2::MiogaConf->new ('web/conf/Mioga.conf'), 'Load Mioga.conf');
