# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

#########################

use Test::More tests => 1;

diag ('TODO: Broken test-script is disabled, needs to be re-written !!!');
pass ();

__DATA__

use Data::Dumper;
use Error qw(:try);
plan tests => 12;
use Mioga2::Search::Document::File;
use Mioga2::Search::Indexer;
use Mioga2::Search::Config;
#
# Pseudo config
#
my $config = { 'application/pdf' => { command => "/usr/bin/pdftotext -enc UTF-8 %s",
                                      res_mime => 'text/plain', },
				'application/msword' => { command => "/bin/antiword %s",
                                      res_mime => 'text/plain', },
				};
my $search_config = Mioga2::Search::Config->new ('web/conf/search_conf.xml');
#
# Create documents
#
my $dummy;
my $res;
	try {
		$dummy = Mioga2::Search::Document::File->new( { config => $config, base_dir => "t/", path => "files/fr_test_file.txt", lang => 'french', mime => "text/html",  } );
		$res = 0;
	}
	otherwise {
		$res = 1;
	};
ok ($res = 1, "no cache arg");
	try {
		$dummy = Mioga2::Search::Document::File->new( { config => $config, path => "files/fr_test_file.txt", cache => "cache/", lang => 'french', mime => "text/html",  } );
		$res = 0;
	}
	otherwise {
		$res = 1;
	};
ok ($res = 1, "no base_dir arg");
	try {
		$dummy = Mioga2::Search::Document::File->new( { config => $config, base_dir => "t/", cache => "cache/", lang => 'french', mime => "text/html" } );
		$res = 0;
	}
	otherwise {
		$res = 1;
	};
ok ($res = 1, "no path arg");
	try {
		$dummy = Mioga2::Search::Document::File->new( {  config => $config, path => "files/fr_test_file.txt", base_dir => "t/", cache => "cache/", lang => 'french', } );
		$res = 0;
	}
	otherwise {
		$res = 1;
	};
ok ($res = 1, "no mime arg");
my $text_doc;
ok ( $text_doc = Mioga2::Search::Document::File->new( { config => $config, force_read => 1, base_dir => "t/", path => "files/fr_test_file.txt", cache => "cache/", lang => 'french', mime => "text/plain", search_config => $search_config, } ) );
my $html_doc;
ok ( $html_doc = Mioga2::Search::Document::File->new( { config => $config, force_read => 1, base_dir => "t/", path => "files/fr_test_file.html" , cache => "cache/", lang => 'french', mime => "text/html", search_config => $search_config, } ) );
ok ( $text_doc = Mioga2::Search::Document::File->new( { config => $config, base_dir => "t/", path => "files/fr_test_file.txt", cache => "cache/", lang => 'french', mime => "text/plain", search_config => $search_config,  } ) );
#
# Create Indexer and process documents
#
my $indexer;
ok ( $indexer = Mioga2::Search::Indexer->new( { base_dir => "t/", xapian_path => "xapiandb/", search_config => $search_config,  } ) );
ok ( $res );
try {
	$indexer->Process($text_doc);
	$res = 1;
}
otherwise {
	my $err = shift;
	print STDERR "Error : " . Dumper($err) . "\n";
	$res = 0;
};
ok ( $res );
try {
	$indexer->Process($html_doc);
	$res = 1;
}
otherwise {
	my $err = shift;
	print STDERR "Error : " . Dumper($err) . "\n";
	$res = 0;
};
ok ( $res );
