#
#===============================================================================
#
#         FILE:  00convert.t
#
#  DESCRIPTION:  Test script for Mioga2::tools::Convert module
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project (), developers@mioga2.org
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      VERSION:  1.0
#      CREATED:  13/02/2012 09:45
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

use Test::More tests => 4;                      # last test to print

my ($data, $json, $xml);

require_ok ('Mioga2::tools::Convert');
$data = {
	key1 => 'val1',
	key2 => [
		'val2.1',
		'val2.2'
	],
	key3 => {
		key1 => 'val3.1',
		key2 => 'val3.2',
	}
};

$json = Mioga2::tools::Convert::PerlToJSON ($data);
ok ($json eq '{"key2":["val2.1","val2.2"],"key1":"val1","key3":{"key2":"val3.2","key1":"val3.1"}}', 'Basic conversion to JSON');

$xml = Mioga2::tools::Convert::PerlToXML ($data);
ok ($xml eq '<key2>val2.1</key2><key2>val2.2</key2><key1>val1</key1><key3><key2>val3.2</key2><key1>val3.1</key1></key3>', 'Basic conversion to XML');

# B1200 - Strings are not escaped
$data = {
	string => 'this "is" a string',
};

$json = Mioga2::tools::Convert::PerlToJSON ($data);
ok ($json eq '{"string":"this \"is\" a string"}', 'B1200 -- Strings are not escaped');
