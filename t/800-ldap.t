#
#===============================================================================
#
#         FILE:  800-ldap.t
#
#  DESCRIPTION:  Test script for Mioga2::LDAP object
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project (), developers@mioga2.org
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      VERSION:  1.0
#      CREATED:  22/09/2014 15:01
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

use Test::More;

# Include test-suite constants
use Cwd 'abs_path';
use File::Basename;
use lib dirname( abs_path $0 );
require ('test_include.pm');

# Test count
plan tests => 13;

use Mioga2::InstanceList;
use Data::Dumper;

my $miogaconf = Mioga2::MiogaConf->new ('web/conf/Mioga.conf');

my $instance = Mioga2::InstanceList->new ($miogaconf, { attributes => { ident => $MiogaTest::instances->[0]->{ident} } });
my $config = Mioga2::Config->new ($miogaconf, $MiogaTest::instances->[0]->{ident});

require_ok ('Mioga2::LDAP');
my $ldap = new_ok ('Mioga2::LDAP' => [$config], 'Mioga2::LDAP instanciation for ' . $config->GetMiogaIdent ());



#-------------------------------------------------------------------------------
# Test logic variables
#-------------------------------------------------------------------------------
my $user;

my $user1 = {
	dn          => 'uid=ldap_user_1,' . $config->GetLDAPUserBaseDN (),
	attributes  => {
		cn          => 'Firstname Lastname ' . $Mioga2::VERSION . ' LDAP',
		givenName   => 'Firstname',
		sn          => 'Lastname ' . $Mioga2::VERSION . ' LDAP-1',
		mail        => 'email-ldap-' . $Mioga2::VERSION . '@noname.org',
		description => ''
	}
};

my $user2 = {
	dn          => 'uid=ldap_user_2,' . $config->GetLDAPUserBaseDN (),
	attributes  => {
		cn          => 'Firstname Lastname ' . $Mioga2::VERSION . ' LDAP',
		givenName   => 'Firstname',
		sn          => 'Lastname ' . $Mioga2::VERSION . ' LDAP-2',
		mail        => 'email-ldap2-' . $Mioga2::VERSION . '@noname.org',
		description => ''
	}
};


#-------------------------------------------------------------------------------
# User creation
#-------------------------------------------------------------------------------

# Read-only access
$instance->Set ('ldap_access', 0);
$instance->Store ();
$config = Mioga2::Config->new ($miogaconf, $MiogaTest::instances->[0]->{ident});
$ldap = Mioga2::LDAP->new ($config);
$user = copy_user ($user1);
$user->{attributes}->{dn} = $user->{dn};
ok (try_fail (sub { $ldap->AddUser ($user->{attributes}); }), 'User creation throws an exception when ldap_access = 0');

# Affectation-only
$instance->Set ('ldap_access', 1);
$instance->Store ();
$config = Mioga2::Config->new ($miogaconf, $MiogaTest::instances->[0]->{ident});
$ldap = Mioga2::LDAP->new ($config);
$user = copy_user ($user1);
$user->{attributes}->{dn} = $user->{dn};
ok (try_fail (sub { $ldap->AddUser ($user->{attributes}); }), 'User creation throws an exception when ldap_access = 1');

# Read-write access
$instance->Set ('ldap_access', 2);
$instance->Store ();
$config = Mioga2::Config->new ($miogaconf, $MiogaTest::instances->[0]->{ident});
$ldap = Mioga2::LDAP->new ($config);
$user = copy_user ($user1);
$user->{attributes}->{dn} = $user->{dn};
ok (try_run (sub { $ldap->AddUser ($user->{attributes}); }), 'User creation succeeds when ldap_access = 2');

$user = copy_user ($user2);
$user->{attributes}->{dn} = $user->{dn};
ok (try_run (sub { $ldap->AddUser ($user->{attributes}); }), 'User creation succeeds when ldap_access = 2');


#-------------------------------------------------------------------------------
# User affectation
#-------------------------------------------------------------------------------
$instance = Mioga2::InstanceList->new ($miogaconf, { attributes => { ident => $MiogaTest::instances->[1]->{ident} } });
$user = copy_user ($user1);
$user->{attributes}->{ou} = [$MiogaTest::instances->[0]->{ident}, $MiogaTest::instances->[1]->{ident}];

# Read-only access
$instance->Set ('ldap_access', 0);
$instance->Store ();
$config = Mioga2::Config->new ($miogaconf, $MiogaTest::instances->[1]->{ident});
$ldap = Mioga2::LDAP->new ($config);
ok (try_fail (sub { $ldap->ModifyUser ($user->{dn}, { ou => [$MiogaTest::instances->[0]->{ident}, $MiogaTest::instances->[1]->{ident}] }); }), 'User affectation throws an exception when ldap_access = 0');

# Affectation-only
$instance->Set ('ldap_access', 1);
$instance->Store ();
$config = Mioga2::Config->new ($miogaconf, $MiogaTest::instances->[1]->{ident});
$ldap = Mioga2::LDAP->new ($config);
ok (try_run (sub { $ldap->ModifyUser ($user->{dn}, { ou => [$MiogaTest::instances->[0]->{ident}, $MiogaTest::instances->[1]->{ident}] }); }), 'User affectation succeeds when ldap_access = 1');

# Read-write
$instance->Set ('ldap_access', 2);
$instance->Store ();
$config = Mioga2::Config->new ($miogaconf, $MiogaTest::instances->[1]->{ident});
$ldap = Mioga2::LDAP->new ($config);
$user = copy_user ($user2);
ok (try_run (sub { $ldap->ModifyUser ($user->{dn}, { ou => [$MiogaTest::instances->[0]->{ident}, $MiogaTest::instances->[1]->{ident}] }); }), 'User affectation succeeds when ldap_access = 2');


#-------------------------------------------------------------------------------
# User deletion
#-------------------------------------------------------------------------------
$instance = Mioga2::InstanceList->new ($miogaconf, { attributes => { ident => $MiogaTest::instances->[0]->{ident} } });

# Read-only access
$instance->Set ('ldap_access', 0);
$instance->Store ();
$config = Mioga2::Config->new ($miogaconf, $MiogaTest::instances->[0]->{ident});
$ldap = Mioga2::LDAP->new ($config);
ok (try_fail (sub { $ldap->DeleteUser ($user1->{dn}); }), 'User deletion throws an exception when ldap_access = 0');

# Affectation-only
$instance->Set ('ldap_access', 1);
$instance->Store ();
$config = Mioga2::Config->new ($miogaconf, $MiogaTest::instances->[0]->{ident});
$ldap = Mioga2::LDAP->new ($config);
ok (try_fail (sub { $ldap->DeleteUser ($user1->{dn}); }), 'User deletion throws an exception when ldap_access = 1');

# Read-write access
$instance->Set ('ldap_access', 2);
$instance->Store ();
$config = Mioga2::Config->new ($miogaconf, $MiogaTest::instances->[0]->{ident});
$ldap = Mioga2::LDAP->new ($config);
ok (try_run (sub { $ldap->DeleteUser ($user1->{dn}); }), 'User deletion succeeds when ldap_access = 2');
ok (try_run (sub { $ldap->DeleteUser ($user2->{dn}); }), 'User deletion succeeds when ldap_access = 2');



#-------------------------------------------------------------------------------
# Private subs
#-------------------------------------------------------------------------------


# Copy user data so it's not modified by underlying Mioga2::LDAP object
sub copy_user {
	my ($src) = @_;

	my $out = {
		dn         => $src->{dn},
		attributes => {}
	};

	for (keys (%{$src->{attributes}})) {
		$out->{attributes}->{$_} = $src->{attributes}->{$_};
	}

	return ($out);
}
