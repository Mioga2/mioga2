#
#===============================================================================
#
#         FILE:  120-user.t
#
#  DESCRIPTION:  Test script for Mioga2::UserList object
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project (), developers@mioga2.org
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      VERSION:  1.0
#      CREATED:  14/02/2014 16:28
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

use Test::More;


# Include test-suite constants
use Cwd 'abs_path';
use File::Basename;
use lib dirname( abs_path $0 );
require ('test_include.pm');

# Test count
plan tests => 7 + scalar (@$MiogaTest::users);

use Data::Dumper;

my $miogaconf = Mioga2::MiogaConf->new ('web/conf/Mioga.conf');
my $config = Mioga2::Config->new ($miogaconf, $MiogaTest::instances->[0]->{ident});

my ($user);

require_ok ('Mioga2::UserList');

diag ('Test user: ' . Dumper $MiogaTest::users->[0]);


# Object instanciation
my %u = map { $_ => $MiogaTest::users->[0]->{$_} } keys (%{$MiogaTest::users->[0]});
ok ($user = Mioga2::UserList->new (
		$config,
		{
			attributes => \%u
		}
	), "Instanciate Mioga2::UserList object within an instance");

# Check user doesn't already exists
ok ($user->Count () == 0, "User doesn't exist");

# User creation
ok (try_run (sub { $user->Store () }), "User creation");

# Check user actually exists
my $rowid = $user->Get ('rowid');
$user = Mioga2::UserList->new (
		$config,
		{
			attributes => {
				rowid => $rowid
			}
		}
	);
ok ($user->Count () == 1, "User exists");

# User deletion
ok (try_run (sub { $user->Delete () }), "User deletion");

# Check user doesn't exists anymore
$user = Mioga2::UserList->new (
		$config,
		{
			attributes => {
				rowid => $rowid
			}
		}
	);
ok ($user->Count () == 0, "User doesn't exist anymore");

# Create users for following test scripts
# WARNING: This block of code should remain at the end
for my $u (@$MiogaTest::users) {
	# Copy user description hash
	my %u = map { $_ => $u->{$_} } keys (%$u);

	$user = Mioga2::UserList->new (
			$config,
			{
				attributes => \%u
			}
		);
	ok (try_run (sub { $user->Store () }), "Create test user: \"" . $u->{firstname} . ' ' . $u->{lastname} . ' <' . $u->{email} . ">\"");
}
