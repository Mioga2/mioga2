// Static data for preview, to be obtained from Mioga2 server

// Users, as returned by GetUsers.json
var users = Array (
	{
		rowid: 1,
		ident: "ident1",
		firstname: "Marc",
		lastname: "HASSIN",
		email: "marc.hassin@noname.org",
		label: "Marc HASSIN (marc.hassin@noname.org)",
		profile: 'Animation',
		last_connection: "2012-06-30 12:50:30",
		teams: ['Everybody', 'Admins']
	},
	{
		rowid: 2,
		ident: "ident2",
		firstname: "Laure",
		lastname: "HAGE",
		email: "laure.hage@noname.org",
		label: "Laure HAGE (laure.hage@noname.org)",
		profile: 'Guest',
		teams: ['Everybody', 'Admins'],
		filesystem: Array (
			{
				path: "/Mioga2/Mioga/home/Common/Folder3",
				access: 2
			}
		)
	},
	{
		rowid: 3,
		ident: "ident3",
		firstname: "Annie",
		lastname: "CROCHE",
		email: "annie.croche@noname.org",
		label: "Annie CROCHE (annie.croche@noname.org)",
		last_connection: "2012-06-30 12:50:30",
		teams: ['Everybody']
	},
	{
		rowid: 4,
		ident: "ident4",
		firstname: "Paul",
		lastname: "HOCHON",
		email: "paul.hochon@noname.org",
		last_connection: "2012-06-30 12:50:30",
		profile: 'Animation',
		teams: ['Everybody']
	}
);

// Teams, as returned by GetTeams.json
var teams = Array (
	{
		rowid: 5,
		ident: "Everybody",
		profile: 'Member',
		filesystem: Array (
			{
				path: "/Mioga2/Mioga/home/Common/Folder3",
				access: 2
			}
		),
		users: [1, 2, 3]
	},
	{
		rowid: 6,
		ident: "Admins",
		profile: 'Animation',
		users: [1]
	},
	{
		rowid: 7,
		ident: "External",
		users: []
	}
);

// Profiles, as returned by GetProfiles.json
var profiles = Array (
	{
		rowid: 1,
		ident: "Guest",
		rights: {
			filesystem: Array (
				{
					path: "/Mioga2/Mioga/home/Common",
					access: 1
				}
			),
			applications: Array (
				{
					label: "Louvre",
					ident: "Louvre",
					functions: Array (
						{
							ident: "Read",
							label: "View galleries"
						}
					)
				},
				{
					label: "Magellan",
					ident: "Magellan",
					functions: Array (
						{
							ident: "Read",
							label: "Access files"
						}
					)
				}
			)
		}
	},
	{
		rowid: 2,
		ident: "Member",
		rights: {
			filesystem: Array (
				{
					path: "/Mioga2/Mioga/home/Common",
					access: 2
				},
				{
					path: "/Mioga2/Mioga/home/Common/Folder1",
					access: 1
				},
				{
					path: "/Mioga2/Mioga/home/Common/Folder2",
					access: 0
				}
			),
			applications: Array (
				{
					label: "FAQ",
					ident: "FAQ",
					functions: Array (
						{
							ident: "Read",
							label: "Read"
						},
						{
							ident: "Write",
							label: "Write"
						},
						{
							ident: "Moder",
							label: "Moderation"
						}
					)
				},
				{
					label: "Forum",
					ident: "Forum",
					functions: Array (
						{
							ident: "Read",
							label: "Read"
						},
						{
							ident: "Write",
							label: "Write"
						},
						{
							ident: "Moder",
							label: "Moderation"
						}
					)
				},
				{
					label: "Louvre",
					ident: "Louvre",
					functions: Array (
						{
							ident: "Read",
							label: "View galleries"
						},
						{
							ident: "Anim",
							label: "Create and delete galeries"
						}
					)
				},
				{
					label: "Magellan",
					ident: "Magellan",
					functions: Array (
						{
							ident: "Read",
							label: "Access files"
						},
						{
							ident: "History",
							label: "Manage history"
						},
						{
							ident: "AccessRights",
							label: "Manage access rights"
						},
						{
							ident: "RootAccessRights",
							label: "Manage root access rights"
						},
						{
							ident: "Properties",
							label: "Manage file properties"
						},
						{
							ident: "Hidden",
							label: "View hidden files"
						}
					)
				}
			)
		}
	},
	{
		rowid: 3,
		ident: "Animation",
		rights: {
			filesystem: Array (
				{
					path: "/Mioga2/Mioga/home/Common",
					access: 2
				},
				{
					path: "/Mioga2/Mioga/home/Common/Folder1",
					access: 2
				}
			),
			applications: Array (
				{
					label: "Fouquet",
					ident: "Fouquet",
					functions: Array (
						{
							ident: "Users",
							label: "Invite users"
						},
						{
							ident: "Teams",
							label: "Invite teams"
						},
						{
							ident: "Resources",
							label: "Invite resources"
						},
						{
							ident: "ProfilesCreate",
							label: "Create profiles"
						},
						{
							ident: "Profiles",
							label: "Modify profiles"
						}
					)
				}
			)
		}
	}
);

var applications = Array (
	{
		rowid: 1,
		label: "FAQ",
		ident: "FAQ",
		description: "Write frequent questions and their associated answer",
		selected: true,
		functions: Array (
			{
				rowid: 2,
				ident: "Read",
				label: "Read"
			},
			{
				rowid: 3,
				ident: "Write",
				label: "Write"
			},
			{
				rowid: 4,
				ident: "Moder",
				label: "Moderation"
			}
		)
	},
	{
		rowid: 2,
		label: "Forum",
		ident: "Forum",
		description: "Give your users a place to talk about their work",
		selected: true,
		functions: Array (
			{
				rowid: 5,
				ident: "Read",
				label: "Read"
			},
			{
				rowid: 6,
				ident: "Write",
				label: "Write"
			},
			{
				rowid: 7,
				ident: "Moder",
				label: "Moderation"
			}
		)
	},
	{
		rowid: 3,
		label: "Louvre",
		ident: "Louvre",
		description: "Display you images in a gallery",
		selected: true,
		functions: Array (
			{
				rowid: 8,
				ident: "Read",
				label: "View galleries"
			},
			{
				rowid: 9,
				ident: "Anim",
				label: "Create and delete galeries"
			}
		)
	},
	{
		rowid: 4,
		label: "Magellan",
		ident: "Magellan",
		description: "Browse files and directories",
		selected: true,
		functions: Array (
			{
				rowid: 10,
				ident: "Read",
				label: "Access files"
			},
			{
				rowid: 11,
				ident: "History",
				label: "Manage history"
			},
			{
				rowid: 12,
				ident: "AccessRights",
				label: "Manage access rights"
			},
			{
				rowid: 13,
				ident: "RootAccessRights",
				label: "Manage root access rights"
			},
			{
				rowid: 14,
				ident: "Properties",
				label: "Manage file properties"
			},
			{
				rowid: 15,
				ident: "Hidden",
				label: "View hidden files"
			}
		)
	},
	{
		rowid: 5,
		label: "Fouquet",
		ident: "Fouquet",
		description: "Manage your group users, teams, profiles, applications",
		selected: true,
		functions: Array (
			{
				rowid: 16,
				ident: "Users",
				label: "Invite users"
			},
			{
				rowid: 17,
				ident: "Teams",
				label: "Invite teams"
			},
			{
				rowid: 18,
				ident: "Resources",
				label: "Invite resources"
			},
			{
				rowid: 19,
				ident: "ProfilesCreate",
				label: "Create profiles"
			},
			{
				rowid: 20,
				ident: "Profiles",
				label: "Modify profiles"
			}
		)
	},
	{
		rowid: 6,
		label: "Bottin",
		ident: "Bottin",
		description: "Manage Mioga2 users in a cross-instance way",
		selected: false,
		functions: Array (
			{
				rowid: 21,
				ident: "Read",
				label: "View users"
			},
			{
				rowid: 22,
				ident: "Write",
				label: "Modify users"
			},
			{
				rowid: 23,
				ident: "Create",
				label: "Create and delete users"
			}
		)
	},
	{
		rowid: 7,
		label: "Articles",
		ident: "Articles",
		description: "Write articles",
		selected: false,
		functions: Array (
			{
				rowid: 24,
				ident: "Read",
				label: "Read articles"
			},
			{
				rowid: 25,
				ident: "Write",
				label: "Write articles"
			},
			{
				rowid: 26,
				ident: "Moder",
				label: "Moderate articles"
			}
		)
	}
);

// Languages, as returned by GetLanguages.json
var languages = Array (
	{
		ident: "en_US",
		name: "English",
		rowid: 1
	},
	{
		ident: "fr_FR",
		name: "Français",
		rowid: 2
	}
);

// Themes, as returned by GetThemes.json
var themes = Array (
	{
		ident: "default",
		name: "Mioga2",
		rowid: 1
	},
	{
		ident: "MyTheme",
		name: "My Theme",
		rowid: 2
	}
);

// Resources, to be returned by GetGroup.json
var resources = [
	{
		rowid: 1,
		ident: 'VideoProjector1',
		animator: {
			rowid: 1,
			firstname: 'Marc',
			lastname: 'HASSIN',
			email: 'marc.hassin@noname.org'
		},
		description: 'Blue video projector with DVI input'
	},
	{
		rowid: 2,
		ident: 'VideoProjector2',
		animator: {
			rowid: 2,
			firstname: 'Laure',
			lastname: 'HAGE',
			email: 'laure.hage@noname.org'
		},
		description: 'White video projector with VGA input',
		selected: true
	}
];

// Group
var group = {
	rowid: 1234,
	ident: "Common",
	animator: 4,
	disk_space_used: 24862720,
	disk_limit_warning: true,
	disk_limit_exceed: false,
	lang: "en_US",
	theme: "Mioga2",
	default_profile: "Guest",
	default_app_id: 4
};

// =========================================
// Initialize response for GetGroup.json
// =========================================
var get_group_json = {
	users: [],
	teams: [],
	profiles: [],
	langs: [],
	themes: []
};
$.each (Object.keys (group), function (index, key) {
	get_group_json[key] = group[key];
})
// Convert default_profile, theme, lang, animator to complex data structure
var default_profile = $.grep (profiles, function (item) { return (item.ident === group.default_profile); })[0];
get_group_json.default_profile = default_profile;
var lang = $.grep (languages, function (item) { return (item.ident === group.lang); })[0];
get_group_json.lang = lang;
var theme = $.grep (themes, function (item) { return (item.name === group.theme); })[0];
get_group_json.theme = theme;
var animator = $.grep (users, function (item) { return (item.rowid === group.animator); })[0];
get_group_json.animator = animator;
get_group_json.applications = applications;
get_group_json.resources = resources;
// Push users
$.each (users, function (index, user) {
	var user_attrs = {
		rowid: user.rowid,
		ident: user.ident,
		firstname: user.firstname,
		lastname: user.lastname,
		email: user.email,
		last_connection: user.last_connection
	};
	var profile = $.grep (profiles, function (item) { return (item.ident === user.profile); })[0];
	if (profile !== undefined) {
		user_attrs.profile = {
			rowid: profile.rowid,
			ident: profile.ident
		};
	}
	get_group_json.users.push (user_attrs);
});
// Push teams
$.each (teams, function (index, team) {
	var team_attrs = {
		rowid: team.rowid,
		ident: team.ident
	};
	var profile = $.grep (profiles, function (item) { return (item.ident === team.profile); })[0];
	if (profile !== undefined) {
		team_attrs.profile = {
			rowid: profile.rowid,
			ident: profile.ident
		};
	}
	var user_count = 0;
	$.each (users, function (index, user) {
		$.each (user.teams, function (index, user_team) {
			if (user_team === team.ident) {
				user_count++;
			}
		})
	});
	team_attrs.nb_users = user_count;
	get_group_json.teams.push (team_attrs);
});
// Push profiles
$.each (profiles, function (index, profile) {
	var profile_attrs = {
		rowid: profile.rowid,
		ident: profile.ident
	};
	var user_count = 0;
	$.each (users, function (index, user) {
		if (user.profile === profile.ident) {
			user_count++;
		}
	});
	profile_attrs.nb_users = user_count;
	var team_count = 0;
	$.each (teams, function (index, team) {
		if (team.profile === profile.ident) {
			team_count++;
		}
	});
	profile_attrs.nb_teams = team_count;
	get_group_json.profiles.push (profile_attrs);
});
$.each (themes, function (index, theme) {
	get_group_json.themes.push ({
		rowid: theme.rowid,
		ident: theme.ident,
		name: theme.name
	});
});
$.each (languages, function (index, lang) {
	get_group_json.langs.push ({
		rowid: lang.rowid,
		ident: lang.ident,
		name: lang.name
	});
});

// =========================================
// Fake WebServices
// =========================================
function getUser (rowid) {
	var user_attrs = undefined;
	$.each (users, function (index, user) {
		if (user.rowid === rowid) {
			user_attrs = {
				rowid: user.rowid,
				ident: user.ident,
				firstname: user.firstname,
				lastname: user.lastname,
				email: user.email,
				last_connection: user.last_connection,
				profiles: []
			};

			// Add teams
			user_attrs.teams = [];
			$.each (user.teams, function (index, ident) {
				var team = $.grep (teams, function (item) { return (item.ident === ident); })[0];
				user_attrs.teams.push ({
					rowid: team.rowid,
					ident: team.ident
				});
			});

			// Add profile information
			var profile = $.grep (profiles, function (item) { return (item.ident === user.profile); })[0];
			if (profile !== undefined) {
				user_attrs.profile = {
					rowid: profile.rowid,
					ident: profile.ident
				};
			}

			// Push profiles
			$.each (profiles, function (index, profile) {
				user_attrs.profiles.push ({
					rowid: profile.rowid,
					ident: profile.ident
				});
			});

			// Add filesystem access information
			user_attrs.filesystem = [];
			if (user.filesystem !== undefined) {
				$.each (user.filesystem, function (index, user_rule) {
					var rule = {
						path: user_rule.path,
						user: user_rule.access,
						profile: undefined,
						teams: []
					};
					user_attrs.filesystem.push (rule);
				});
			}
			if (profile !== undefined) {
				$.each (profile.rights.filesystem, function (index, profile_rule) {
					var path = profile_rule.path;

					// Check for existing rule for path
					var rule = $.grep (user_attrs.filesystem, function (item) { return (item.path === path); } )[0];
					if (rule === undefined) {
						rule = { path: path, user: undefined, profile: undefined, teams: [] };
						user_attrs.filesystem.push (rule);
					}

					// Add access and push rule
					rule.profile = profile_rule.access;
				});
			}
			$.each (user.teams, function (index, ident) {
				var team = $.grep (teams, function (item) { return (item.ident === ident); })[0];
				var profile = $.grep (profiles, function (item) { return (item.ident === team.profile); })[0];
				$.each (profile.rights.filesystem, function (index, profile_rule) {
					var path = profile_rule.path;

					// Check for existing rule for path
					var rule = $.grep (user_attrs.filesystem, function (item) { return (item.path === path); })[0];
					if (rule === undefined) {
						rule = { path: path, user: undefined, profile: undefined, teams: [] };
						user_attrs.filesystem.push (rule);
					}

					// Add access and push rule
					rule.teams.push ({rowid: team.rowid, access: profile_rule.access});
				});
			});

			// Add application access information
			user_attrs.applications = { };
			if (profile !== undefined) {
				user_attrs.applications.profile = profile.rights.applications;
			}
			user_attrs.applications.teams = [ ];
			$.each (user.teams, function (index, ident) {
				var team = $.grep (teams, function (item) { return (item.ident === ident); })[0];
				var profile = $.grep (profiles, function (item) { return (item.ident === team.profile); })[0];
				user_attrs.applications.teams.push ({rowid: team.rowid, ident: team.ident, rights: profile.rights.applications});
			});
		}
	});

	return (user_attrs);
}

function getTeam (rowid) {
	var team_attrs = undefined;
	$.each (teams, function (index, team) {
		if (team.rowid === rowid) {
			team_attrs = {
				rowid: team.rowid,
				ident: team.ident,
				users: [],
				profiles: []
			};

			// Add profile information
			var profile = $.grep (profiles, function (item) { return (item.ident === team.profile); })[0];
			if (profile !== undefined) {
				team_attrs.profile = {
					rowid: profile.rowid,
					ident: profile.ident
				};
			}

			// Push profiles
			$.each (profiles, function (index, profile) {
				team_attrs.profiles.push ({
					rowid: profile.rowid,
					ident: profile.ident
				});
			});

			// Add team members
			$.each (users, function (index, user) {
				var user_team = $.grep (user.teams, function (item) { return (item === team.ident); })[0];
				if (user_team !== undefined) {
					team_attrs.users.push (user);
				}
			});

			// Add filesystem access information
			team_attrs.filesystem = [];
			if (team.filesystem !== undefined) {
				$.each (team.filesystem, function (index, team_rule) {
					var rule = {
						path: team_rule.path,
						team: team_rule.access,
						profile: undefined,
						teams: []
					};
					team_attrs.filesystem.push (rule);
				});
			}
			if (profile !== undefined) {
				$.each (profile.rights.filesystem, function (index, profile_rule) {
					var path = profile_rule.path;

					// Check for existing rule for path
					var rule = $.grep (team_attrs.filesystem, function (item) { return (item.path === path); } )[0];
					if (rule === undefined) {
						rule = { path: path, team: undefined, profile: undefined, teams: [] };
						team_attrs.filesystem.push (rule);
					}

					// Add access and push rule
					rule.profile = profile_rule.access;
				});
			}

			// Add application access information
			team_attrs.applications = { };
			if (profile !== undefined) {
				team_attrs.applications.profile = profile.rights.applications;
			}
			team_attrs.applications.teams = [ ];
		}
	});

	return (team_attrs);
}

function getProfile (rowid, full) {
	var profile_attrs = undefined;
	$.each (profiles, function (index, profile) {
		if (profile.rowid === rowid) {
			profile_attrs = {
				rowid: profile.rowid,
				ident: profile.ident,
				rights: {
					filesystem: profile.rights.filesystem
				},
				users: [],
				teams: []
			};
			// Push users
			$.each (users, function (index, user) {
				var selected = (user.profile === profile.ident) ? true : false;
				if ((user.profile === profile.ident) || full) {
					var user_attrs = {
						rowid: user.rowid,
						ident: user.ident,
						firstname: user.firstname,
						lastname: user.lastname,
						email: user.email,
						last_connection: user.last_connection,
						selected: selected
					};
					profile_attrs.users.push (user_attrs);
				}
			});
			// Push teams
			$.each (teams, function (index, team) {
				var selected = (team.profile === profile.ident) ? true : false;
				if ((team.profile === profile.ident) || full) {
					var team_attrs = {
						rowid: team.rowid,
						ident: team.ident,
						selected: selected
					};
					profile_attrs.teams.push (team_attrs);
				}
			});
			// Application rights, full list, if applicable
			if (full) {
				profile_attrs.rights.applications = [];
				$.each (applications, function (index, application) {
					var app = {
						ident: application.ident,
						label: application.label,
						functions: []
					};
					var profile_app = $.grep (profile.rights.applications, function (app) { return (app.label === application.label) })[0];
					if (profile_app) {
						$.each (application.functions, function (index, funct) {
							var profile_funct = $.grep (profile_app.functions, function (f) { return (funct.ident === f.ident); });
							funct.selected = (profile_funct) ? true : false;
							app.functions.push (funct);
						});
					}
					else {
						app.functions = application.functions;
					}
					profile_attrs.rights.applications.push (app);
				});
			}
			else {
				profile_attrs.rights.applications = profile.rights.applications;
			}
		}
	});

	return (profile_attrs);
}
