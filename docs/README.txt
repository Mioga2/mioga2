HowTo generate Mioga2 documentation

Requirements : 
- OpenOffice.Org >= 1.1  (www.openoffice.org)
- Writer2XHtml >= 0.3.3f (http://www.hj-gym.dk/~hj/writer2latex)

A java interpreter is required by Writer2XHtml.

To generate documentation : 
- Load index.sxg in OpenOffice.
- Select File->Export... and export into a "OpenOffice 1.0 sxw" 
  file name index.sxw.
- Edit Makefile to change path to writer2latex and java.
- Run "make doc"

