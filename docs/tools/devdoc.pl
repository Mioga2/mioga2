#!/usr/bin/perl -w

use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::tools::database;
use URI::Escape;

sub ProcessDir
{
	my ($dirname, $dest_dir) = @_;
	my $dir_h;
	my $dir = opendir($dir_h, $dirname) or die "Cannot open $dirname\n";
	my $files;
	print "Process $dirname in $dest_dir\n";
	while ( defined ($file = readdir $dir_h) ) {
    	next if $file =~ /^\.\.?$/;     # skip . and ..
    	next if $file =~ /^\..*$/;
		print "  ==> $file\n";
		if (-d "$dirname/$file") {
			mkdir "$dest_dir/$file"; 
			ProcessDir($dirname."/".$file, "$dest_dir/$file");
		}
		elsif (($file =~ /.*\.pm/) || ($file =~ /.*\.pl/))
		{
			my $cmd = "/usr/bin/perltidy -html $dirname/$file -o $dest_dir/$file.html";
			print ("$cmd\n"); 
			system("$cmd"); 

		}
    }
	closedir $dir_h;
}


if(@ARGV != 1) {
	print "Usage : $0 /path/to/Mioga.conf\n";
	exit(-1);
}

my $bin_dir = "../bin";
my $lib_dir = "../lib";
my $dest_dir = "dev";
mkdir $dest_dir;

#Generate docs for lib dir
mkdir "$dest_dir/lib";
ProcessDir($lib_dir, "$dest_dir/lib");


