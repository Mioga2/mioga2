#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  generate-icon-list.pl
#
#        USAGE:  ./generate-icon-list.pl /path/to/standard_icons.csv /path/to/standard_icons.html
#
#  DESCRIPTION:  Generate HTML page describing Mioga2 standard icons from
#                a CSV export of docs/css/standard_icons.ods
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  08/12/2011 13:46
#
#===============================================================================
#
#  Copyright (c) year 2011, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================

use strict;
use warnings;

use Template;
use Mioga2::CSV;
use Data::Dumper;

my $debug = 0;

my @sizes = qw/16x16 32x32 48x48 64x64 128x128/;

my $csvfile = shift;
my $htmlfile = shift;

if (!defined ($csvfile) || !defined ($htmlfile)) {
	die "Usage: ./generate-icon-list.pl /path/to/standard_icons.csv /path/to/standard_icons.html\n";
}

if ((!-f $csvfile)) {
	die "$csvfile does not exist\n";
}

my $data;

# Read CSV contents
my $converter = new Mioga2::CSV (
		columns => [ qw/filename class comment size/ ],
		separator => ',',
		quote => '"'
	);
open (CSV, $csvfile);
my $unsorted = $converter->Convert (join ('', <CSV>));
close (CSV);

# Remove CSV header
shift (@$unsorted);

# Sort icons according to class and filename
my @sorted = sort { $a->{class} . $a->{filename} cmp $b->{class} . $b->{filename} } @$unsorted;

$data->{nb_sizes} = scalar (@sizes);

my %labels = (
	actions    => 'Action',
	apps       => 'Application',
	categories => 'Category',
	emblems    => 'Emblem',
	places     => 'Place',
	status     => 'Status',
);
for my $icon (@sorted) {
	my @avail_sizes = map { s/[^0-9x]//g; $_ } split (/,/, delete ($icon->{size}));
	for my $size (@sizes) {
		push (@{$icon->{sizes}}, [$size, (grep (/^$size$/, @avail_sizes)) ? 1 : 0]);
	}
	push (@{$data->{classes}->{$icon->{class}}->{icons}}, $icon);
	unless (defined ($data->{classes}->{$icon->{class}}->{class})) {
		$data->{classes}->{$icon->{class}}->{class} = $icon->{class};
		$data->{classes}->{$icon->{class}}->{label} = $labels{$icon->{class}};
	}
}
print Dumper $data if ($debug);

my $tt = Template->new () or die Template->error() . "\n";
$tt->process(\*DATA, $data, $htmlfile) or die $tt->error() . "\n";

__DATA__
<html>
	<head>
		<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
		<link media="screen" href="../../web/themes/default/css/mioga.css" type="text/css" rel="stylesheet"/>
		<link rel="stylesheet" type="text/css" href="../doc.css" media="screen"/>
	</head>
	<body class="doc">
		<h1 class="doc_title">Mioga standard icons documentation</h1>
		<p>The icon files used in the Mioga2 project should respect the <a href="http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html">Icon Naming Specification</a>.</p>

	<table class="doc iconlist">
		[% FOREACH class IN classes.keys.sort %]
			[% INCLUDE category data = classes.$class %]
		[% END %]
	</table>
	</body>
</html>

[% BLOCK category %]
<div class="docblock">
	<tr>
		<td colspan="[% nb_sizes + 2 %]">
			<h2 class="doc">[% data.label %] icons (NxN/[% data.class %])</h2>
		</td>
	</tr>
	<tr>
		<th>Name</th>
		<th>Usage</th>
		<th colspan="[% nb_sizes %]">Sizes</th>
	</tr>
	[% FOREACH icon = data.icons %]
		[% INCLUDE entry icon = icon class = data.class %]
	[% END %]
</div>
[% END %]

[% BLOCK entry %]
<tr>
	<td>[% icon.filename %]</td>
	<td>[% icon.comment %]</td>
	[% FOREACH size = icon.sizes %]
		<td class="icon-container">
			[% IF size.1 == 1 %]
				<div class="icon-container">
					<div class="icon-image"><img src="../../web/themes/default/images/[% size.0 %]/[% class %]/[% icon.filename %]"/></div>
					<div class="icon-size">[% size.0 %]</div>
				</div>
			[% END %]
		</td>
	[% END %]
</td>
[% END %]
