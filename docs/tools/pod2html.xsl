<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="iso-8859-1" />

  <xsl:include href="../../web/xsl/base.xsl"/>

  <xsl:template match="Application">
      <body xsl:use-attribute-sets="body_attr">
          <xsl:call-template name="MiogaTitle">
              <xsl:with-param name="title">Mioga2 Applications</xsl:with-param>
          </xsl:call-template>

          <p>
              Mioga2 applications pods explain all XML code generated 
              by applications
          </p>

          <xsl:apply-templates select="app"/>
      </body>
  </xsl:template>

  <xsl:template match="app">
      
      <p>
          <a href="{@esc_url}.html"><xsl:value-of select="."/></a>
      </p>

  </xsl:template>

</xsl:stylesheet>