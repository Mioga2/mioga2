<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                version="1.1"
                extension-element-prefixes="exsl" >
    
    <xsl:output method="xml" indent="yes" encoding="iso-8859-1" />


    <!--
         Copy $body in a separated HTML file names $filename.
         Add HTML headers including title $title
         -->
    <xsl:template name="chunk-into">
        <xsl:param name="filename"/>
        <xsl:param name="body"/>
        <xsl:param name="title"/>
        
        <exsl:document
            href="{$filename}"
            method="xml"
            encoding="utf-8"
            standalone="yes"
            indent="yes">
            
                 <html>
                    <head>
                        <title><xsl:value-of select="$title"/></title>
                        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                        <link href="css/mioga.css" media="all" rel="stylesheet" type="text/css" />
                    </head>
                    
                    <body>
                        <xsl:copy-of select="$body"/>
                    </body>
                </html>

        </exsl:document>

    </xsl:template>

    
    <!--
         Add the sub table of content in file
         -->
    <xsl:template name="add_sub_toc">
        <xsl:variable name="heading-name">#<xsl:value-of select="./@id"/></xsl:variable>
        
        <xsl:variable name="first-entry" select="//div[@id='tableofcontents']/p[@class='Contents1' and .//a/@href=$heading-name]"/>
        
        <div id="tableofcontents">
            <xsl:call-template name="recurs-copy-toc-entry">
                <xsl:with-param name="node" select="$first-entry/following-sibling::p[position() = 1]"/>
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template name="recurs-copy-toc-entry">
        <xsl:param name="node"/>

        <xsl:if test="$node/@class != 'Contents1'">
            <xsl:copy-of select="$node"/>

            <xsl:call-template name="recurs-copy-toc-entry">
                <xsl:with-param name="node" select="$node/following-sibling::p[position() = 1]"/>
            </xsl:call-template>

        </xsl:if>
    </xsl:template>

    <!--
         Copy each H1 in a separated file
         -->
    <xsl:template match="h1">
        
        <xsl:call-template name="chunk-into">
            <xsl:with-param name="filename"><xsl:value-of select="substring-before(../@id, 'sxw')"/>.html</xsl:with-param>

            <xsl:with-param name="title" select="." />
            <xsl:with-param name="body">
                <xsl:copy-of select="."/>
                <xsl:call-template name="add_sub_toc"/>
                <xsl:copy-of select="following-sibling::*"/>
            </xsl:with-param>

        </xsl:call-template>
    </xsl:template>
    
    
    <!--
         Rewrite links of toc
         -->
    <xsl:template name="rewrite_anchor">
        <xsl:param name="contents1"/>

        <xsl:variable name="contents-ref" select="substring-after($contents1//a/@href, '#')"/>
        <xsl:variable name="contents-id" select="substring-before(//h1[@id=$contents-ref]/../@id, 'sxw')"/>
        <xsl:variable name="filename"><xsl:value-of select="$contents-id"/>.html</xsl:variable>

        <xsl:attribute name="href"><xsl:value-of select="$filename"/><xsl:value-of select="@href"/></xsl:attribute>

    </xsl:template>

    <xsl:template match="a">
        <a>
            <xsl:variable name="ancestor_p" select="ancestor::p[position() = 1]"/>
            
            <xsl:choose>
                <xsl:when test="$ancestor_p/@class = 'Contents1'">
                    <xsl:call-template name="rewrite_anchor">
                        <xsl:with-param name="contents1" select="$ancestor_p"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="rewrite_anchor">
                        <xsl:with-param name="contents1" select="$ancestor_p/preceding-sibling::p[@class='Contents1'][position() = 1]"/>
                    </xsl:call-template>                    
                </xsl:otherwise>
            </xsl:choose>

            <xsl:value-of select="."/>
        </a>
    </xsl:template>
    

    <xsl:template match="div[@id='tableofcontents']">
        <div id="tableofcontents">

            <xsl:for-each select="./p">
                <p class="{@class}">
                    <xsl:apply-templates select="node()"/>
                </p>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/">

        <xsl:call-template name="chunk-into">
            <xsl:with-param name="filename">index.html</xsl:with-param>
            <xsl:with-param name="title">Documentation Mioga2</xsl:with-param>
            <xsl:with-param name="body">
                <xsl:copy-of select="//div[position()=1]/preceding-sibling::*"/>
                <xsl:apply-templates select="//div[@id='tableofcontents']"/>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:apply-templates select="//h1"/>

    </xsl:template>

</xsl:stylesheet>


