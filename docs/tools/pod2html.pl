#!/usr/bin/perl -w

use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::tools::database;
use URI::Escape;

if(@ARGV != 1) {
	print "Usage : $0 /path/to/Mioga.conf\n";
	exit(-1);
}

my $dir = "docs/pods";
mkdir $dir;

my $miogaconf = new Mioga2::MiogaConf($ARGV[0]);

my $dbh = $miogaconf->GetDBH();
my $apps = SelectMultiple($dbh, "SELECT DISTINCT package FROM m_application");

my @packages = map {$_->{package}} @$apps;
push @packages, qw(Mioga2::LargeList Mioga2::SimpleLargeList Mioga2::Organizer::View Mioga2::Organizer::Month Mioga2::Organizer::User Mioga2::Organizer::Periodic
	   	   Mioga2::tools::args_checker Mioga2::RequestContext Mioga2::tools::date_utils);


my $xml = '<?xml version="1.0" encoding="ISO-8859-1" ?>';
$xml .= "<Application>";

foreach my $pack (sort {$a cmp $b } @packages) {
	my $path = "lib/$pack.pm";
	$path =~ s!::!/!g;

	if(-f $path) {
		system("pod2html --outfile='$dir/$pack.html' --title='$pack' '$path'");
	}
	
	my $esc_url = uri_escape($pack);

	$xml .= "<app esc_url=\"$esc_url\">$pack</app>";
}

$xml .= "</Application>";

open(F_XSLT, "| xsltproc docs/tools/pod2html.xsl - > docs/pods/index.html")
	or die "Can't run xsltproc : $!";

print F_XSLT $xml;

close(F_XSLT);
