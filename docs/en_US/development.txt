This file gives notes about Mioga2 development environment and other things to know for a developer.

The Dojo Toolkit
----------------

Mioga uses Dojo Toolkit and provides specific objects. The installed version of the Dojo Toolkit is located under web/jslib/dojotoolkit. Developers SHOULD NOT modify files in this directory as its contents is dynamically re-generated from both Dojo Toolkit distribution (obtained from dojotoolkit.org) and Mioga specific classes (obtained from tools/dojo/mioga_src directory).

If a developer needs to modify Mioga Dojo classes, he has two options:
	- Modify the contents of tools/dojo/mioga_src files, re-generate the Dojo build (see tools/dojo/HOWTO_BUILD.txt) and run make install from web/ directory. This solution is the closest to the installed environment, but is time wasting because of the Dojo build step.
	- Modify the contents of tools/dojo/mioga_src files, then run make install from tools/dojo/ directory. This solution will install non-packaged Dojo distribution but will reduce spared time during development. WARNING: Running make install from web/ directory after having run make install from tools/dojo/ directory will overwrite changes from tools/dojo/. The make install should be run from tools/dojo/ AFTER having been run from web/.
