-- ##################################################################
-- Forum : add a boolean field to only send one mail to subscriber
-- ##################################################################

alter table forum_user2subject add send_mail boolean;
alter table forum_user2subject alter column send_mail set default 'f';