-- ============================================================================
-- HTML authentication sessions
-- ============================================================================
CREATE TABLE m_auth_session (
	hash      varchar(32) UNIQUE NOT NULL,
	ident     varchar(128),
	password  varchar(128),
	created   timestamp NOT NULL,
	modified  timestamp NOT NULL
);

-- ============================================================================
-- Default application for group
-- ============================================================================
ALTER TABLE m_group ADD COLUMN default_app_id int4;
ALTER TABLE m_group ADD CONSTRAINT default_app_id_fkey FOREIGN KEY (default_app_id) REFERENCES m_application (rowid);
-- Set default application to Magellan for all groups
UPDATE m_group SET default_app_id = m_application.rowid FROM m_application WHERE m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'group') AND m_application.mioga_id = m_group.mioga_id AND m_application.ident = 'Magellan';

-- ============================================================================
-- Default group for instance (already exists in table but no foreign key)
-- ============================================================================
ALTER TABLE m_mioga ADD CONSTRAINT m_mioga_default_group_id_chk FOREIGN KEY (default_group_id) REFERENCES m_group (rowid);
