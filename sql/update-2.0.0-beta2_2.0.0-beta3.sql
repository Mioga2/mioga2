--
-- Disable trigger and constraint
--

DROP TRIGGER m_group_base_rowid_check ON m_group_base;
DROP TRIGGER m_group_rowid_check ON m_group;
DROP TRIGGER m_resource_rowid_check ON m_resource;
DROP TRIGGER m_user_base_rowid_check ON m_user_base;
DROP TRIGGER m_local_user_rowid_check ON m_local_user;
DROP TRIGGER m_external_user_rowid_check ON m_external_user;
DROP TRIGGER m_ldap_user_rowid_check ON m_ldap_user;

ALTER TABLE m_mioga DROP CONSTRAINT m_mioga_admin_chk;

--
-- Add ident to m_group_base
--

ALTER TABLE m_group_base ADD COLUMN ident varchar(128);

create unique index m_group_base_ident_mioga_index on m_group_base (ident, mioga_id);
create unique index m_group_ident_rowid_index on m_group (rowid);
create unique index m_resource_rowid_index on m_resource(rowid);
create unique index m_resource_ident_mioga_index on m_resource (ident, mioga_id);

--
-- Add fn, ln, email and pwd to m_user_base
--

ALTER TABLE m_user_base ADD COLUMN firstname varchar(128);
ALTER TABLE m_user_base ADD COLUMN lastname varchar(128);
ALTER TABLE m_user_base ADD COLUMN password varchar(128);
ALTER TABLE m_user_base ADD COLUMN email varchar(128);

create unique index m_user_base_rowid_index on m_user_base(rowid);
create unique index m_user_base_ident_mioga_index on m_user_base (ident, mioga_id);


--
-- Copy data from m_local_user to m_user_base
--

INSERT INTO m_user_base (rowid, created, modified, mioga_id, type_id, public_part, autonomous, theme_lang_id, anim_id, creator_id, default_profile_id, disk_space_used, timezone_id, time_per_day, working_days, monday_first, status_id, ident, firstname,lastname, password, email) SELECT rowid, created, modified, mioga_id, type_id, public_part, autonomous, theme_lang_id, anim_id, creator_id, default_profile_id, disk_space_used, timezone_id, time_per_day, working_days, monday_first, status_id, ident, firstname,lastname, password, email FROM ONLY m_local_user;

ALTER TABLE m_external_user RENAME TO m_external_user_to_delete;
ALTER TABLE m_ldap_user RENAME TO m_ldap_user_to_delete;


--
-- Create m_nonlocal_user
--

create table m_nonlocal_user (
    previous_user_status int4,
    data_expiration_date timestamp,
    password_expiration_date timestamp,

   FOREIGN KEY (previous_user_status) REFERENCES m_user_status (rowid)
) inherits (m_user_base);


create unique index m_nonlocal_user_rowid_index on m_nonlocal_user(rowid);
create unique index m_nonlocal_user_ident_mioga_index on m_nonlocal_user(ident, mioga_id);

--
-- "Move" External user
--
create table m_external_user (
    external_ident varchar(128) not null,
    external_mioga_id int4 not null

) inherits (m_nonlocal_user);


INSERT INTO m_external_user (rowid, created, modified, mioga_id, type_id, public_part, autonomous, theme_lang_id, anim_id, creator_id, default_profile_id, disk_space_used, timezone_id, time_per_day, working_days, monday_first, status_id, ident, firstname, lastname, password, email, external_ident, external_mioga_id, previous_user_status, data_expiration_date, password_expiration_date) SELECT rowid, created, modified, mioga_id, type_id, public_part, autonomous, theme_lang_id, anim_id, creator_id, default_profile_id, disk_space_used, timezone_id, time_per_day, working_days, monday_first, status_id, ident, firstname, lastname, password, email, external_ident, external_mioga_id, previous_user_status, data_expiration_date, password_expiration_date FROM m_external_user_to_delete;

--
-- "Move" LDAP user
--
create table m_ldap_user (
   dn           text not null
) inherits (m_nonlocal_user);


INSERT INTO m_ldap_user (rowid, created, modified, mioga_id, type_id, public_part, autonomous, theme_lang_id, anim_id, creator_id, default_profile_id, disk_space_used, timezone_id, time_per_day, working_days, monday_first, status_id, dn) SELECT rowid, created, modified, mioga_id, type_id, public_part, autonomous, theme_lang_id, anim_id, creator_id, default_profile_id, disk_space_used, timezone_id, time_per_day, working_days, monday_first, status_id, dn FROM m_ldap_user_to_delete;


--
-- Remove deprecated tables and recreate indices
--
DROP TABLE m_external_user_to_delete;
DROP TABLE m_ldap_user_to_delete;
DROP TABLE m_local_user;

create unique index m_external_user_rowid_index on m_external_user(rowid);
create unique index m_external_user_ident_mioga_index on m_external_user(ident, mioga_id);
create index m_ldap_user_dn_index on m_ldap_user(dn);
create unique index m_ldap_user_dn_mioga_index on m_ldap_user(dn, mioga_id);
create unique index m_ldap_user_rowid_index on m_ldap_user(rowid);
create unique index m_ldap_user_ident_mioga_index on m_ldap_user(ident, mioga_id);

--
-- Add UNIQUE constraint to m_group_base
--

UPDATE m_group_base SET ident = rowid WHERE ident IS NULL;

ALTER TABLE m_group_base ALTER COLUMN ident SET NOT NULL;



--
-- Recreate Constraint and trigger
--

ALTER TABLE m_nonlocal_user ADD CONSTRAINT m_nonlocal_user_animator_chk
FOREIGN KEY (anim_id) REFERENCES m_group_rowids (rowid);

ALTER TABLE m_nonlocal_user ADD CONSTRAINT m_nonlocal_user_creator_chk
FOREIGN KEY (creator_id) REFERENCES m_group_rowids (rowid);

ALTER TABLE m_mioga ADD CONSTRAINT m_mioga_admin_chk
FOREIGN KEY (admin_id) REFERENCES m_user_base (rowid);
	


CREATE TRIGGER m_group_base_rowid_check 
     BEFORE INSERT OR DELETE ON m_group_base FOR EACH ROW
     EXECUTE PROCEDURE check_group_rowid ();


CREATE TRIGGER m_group_rowid_check 
     BEFORE INSERT OR DELETE ON m_group FOR EACH ROW
     EXECUTE PROCEDURE check_group_rowid ();


CREATE TRIGGER m_resource_rowid_check 
     BEFORE INSERT OR DELETE ON m_resource FOR EACH ROW
     EXECUTE PROCEDURE check_group_rowid ();


CREATE TRIGGER m_user_base_rowid_check 
     BEFORE INSERT OR DELETE ON m_user_base FOR EACH ROW
     EXECUTE PROCEDURE check_group_rowid ();


CREATE TRIGGER m_nonlocal_user_rowid_check 
     BEFORE INSERT OR DELETE ON m_nonlocal_user FOR EACH ROW
     EXECUTE PROCEDURE check_group_rowid ();


CREATE TRIGGER m_external_user_rowid_check 
     BEFORE INSERT OR DELETE ON m_external_user FOR EACH ROW
     EXECUTE PROCEDURE check_group_rowid ();


CREATE TRIGGER m_ldap_user_rowid_check 
     BEFORE INSERT OR DELETE ON m_ldap_user FOR EACH ROW
     EXECUTE PROCEDURE check_group_rowid ();


UPDATE m_nonlocal_user SET data_expiration_date = now()-'1 year'::interval;
UPDATE m_nonlocal_user SET password_expiration_date = now()-'1 year'::interval;
