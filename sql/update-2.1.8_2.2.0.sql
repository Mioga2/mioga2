--
-- Update table m_group_base
--
alter table m_group_base add history boolean;
alter table m_group_base alter column history set default 'f';
UPDATE m_group_base SET history = FALSE;

--
-- Update table m_uri
--
ALTER TABLE m_uri ADD modified TIMESTAMP;
update m_uri SET modified = NOW();
ALTER TABLE m_uri ALTER COLUMN modified SET NOT NULL;
ALTER TABLE m_uri ALTER COLUMN modified SET DEFAULT NOW();
ALTER TABLE m_uri ADD user_id int4;
alter table m_uri add constraint "m_uri_user_id" FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid);
UPDATE m_uri SET user_id = m_group_base.anim_id FROM m_group_base WHERE m_group_base.rowid=group_id;
ALTER TABLE m_uri ALTER COLUMN user_id SET NOT NULL;
ALTER TABLE m_uri ADD lang VARCHAR(64);
ALTER TABLE m_uri ADD mimetype VARCHAR(128);

--
-- Create table uri_history
--
create table uri_history (
    rowid       serial UNIQUE NOT NULL,
    uri_id      int NOT NULL,
    modified    timestamp NOT NULL,
    user_id     int NOT NULL,
    old_name    text,
    
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (uri_id) REFERENCES m_uri (rowid)
);

--
-- INDEXES
--
CREATE INDEX modified_idx ON m_uri(modified);