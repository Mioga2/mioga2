-- ============================================================================
-- Articles tables
-- ============================================================================

CREATE TABLE article_comment (
    ROWID           serial UNIQUE NOT NULL,
    created         timestamp DEFAULT NOW(),
    modified        timestamp DEFAULT NOW(),
    comment         text NOT NULL,
    subject         text NOT NULL,
    user_id         integer NOT NULL,
    article_id      integer NOT NULL,
    parent_id       integer,
                               
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (parent_id) REFERENCES article_comment (rowid),
    FOREIGN KEY (article_id) REFERENCES article (rowid)
);

-- ============================================================================
-- Magellan tables
-- ============================================================================

CREATE TABLE file_comment (
    ROWID           serial UNIQUE NOT NULL,
    created         timestamp DEFAULT NOW(),
    modified        timestamp DEFAULT NOW(),
    comment         text,
    user_id         integer NOT NULL,
    uri_id          integer NOT NULL,
                               
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (uri_id) REFERENCES m_uri (rowid)
);


-- ============================================================================
-- m_external_mioga (for synchronisation purpose)
-- ============================================================================

ALTER TABLE m_external_mioga add column last_synchro timestamp;
UPDATE m_external_mioga SET last_synchro = '2000-01-01';
