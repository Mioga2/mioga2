-- ============================================================================
-- RSS tables
-- ============================================================================

CREATE TABLE rss_subscription (
    ROWID           serial UNIQUE NOT NULL,
    created         timestamp DEFAULT NOW(),
    modified        timestamp DEFAULT NOW(),
    user_id         integer NOT NULL,
    group_id        integer NOT NULL,
    application_id  integer NOT NULL,
                               
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (application_id) REFERENCES m_application (rowid)
);

CREATE TABLE rss_option (
    ROWID           serial UNIQUE NOT NULL,
    created         timestamp DEFAULT NOW(),
    modified        timestamp DEFAULT NOW(),
    user_id         integer NOT NULL,
    feed_per_group  boolean default false,
                               
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);