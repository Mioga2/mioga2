
create table m_dav_property (
    rowid               serial UNIQUE NOT NULL,
    property            varchar(128),
    enabled             bool
);
