
-- ============================================================================
-- Add tools : Function ti simplify sort in simplelargelist
-- ============================================================================
CREATE OR REPLACE FUNCTION lower(boolean) RETURNS boolean
AS
'
BEGIN
RETURN $1;
END;
'
LANGUAGE 'plpgsql';

ALTER TABLE m_mioga ALTER COLUMN private_key DROP NOT NULL;
ALTER TABLE m_mioga ALTER COLUMN public_key DROP NOT NULL;

ALTER TABLE m_mioga ADD COLUMN default_theme_lang_id int4 DEFAULT NULL;

ALTER TABLE m_mioga ADD CONSTRAINT m_mioga_theme_lang_chk
FOREIGN KEY (default_theme_lang_id) REFERENCES m_theme_lang (rowid);

UPDATE m_mioga 
SET default_theme_lang_id = temp.default_theme_lang_id
FROM (  SELECT min(m_theme_lang.rowid) AS default_theme_lang_id, m_theme.mioga_id AS mioga_id 
	FROM m_theme_lang, m_theme 
	WHERE m_theme_lang.theme_id = m_theme.rowid 
	GROUP BY mioga_id) AS temp
WHERE m_mioga.rowid = temp.mioga_id;

