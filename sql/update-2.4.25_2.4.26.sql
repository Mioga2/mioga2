-- Re-declare functions for PostgreSQL 9
CREATE OR REPLACE FUNCTION subject_count_messages(integer) RETURNS INTEGER AS '
DECLARE
        subj_id ALIAS FOR $1;
        mycount INTEGER;
BEGIN
        SELECT INTO mycount count(forum_message.rowid) FROM  forum_subject LEFT JOIN forum_message ON forum_subject.rowid = forum_message.subject_id WHERE forum_subject.rowid = subj_id GROUP BY forum_subject.rowid;
        RETURN mycount;
END;
' LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION subject_lastpost(integer) RETURNS INTEGER AS '
DECLARE
        subj_id ALIAS FOR $1;
        myid INTEGER;
BEGIN
        SELECT INTO myid  forum_message.rowid FROM forum_subject LEFT JOIN forum_message ON forum_subject.rowid = forum_message.subject_id WHERE forum_subject.rowid = subj_id ORDER BY forum_message.modified DESC LIMIT 1 ;
        RETURN myid;
END;
' LANGUAGE 'plpgsql';


-- LDAP Synchro enhancements
ALTER TABLE m_mioga ADD COLUMN ldap_access int4 DEFAULT 0;
UPDATE m_mioga SET ldap_access=2 WHERE modify_ldap_password=TRUE;
ALTER TABLE m_mioga DROP COLUMN modify_ldap_password;
