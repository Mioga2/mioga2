-- ============================================================================
-- Mioga2 Project (C) 2003-2007 The Mioga2 Project
--
--       This program is free software; you can redistribute it and/or modify it
--       under the terms of the GNU General Public License as published by the
--       Free Software Foundation; either version 2, or (at your option) any
--       later version.
--
--       This program is distributed in the hope that it will be useful,
--       but WITHOUT ANY WARRANTY; without even the implied warranty of
--       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--       GNU General Public License for more details.
--
--       You should have received a copy of the GNU General Public License
--       along with this program; if not, write to the Free Software
--       Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
--
-- ============================================================================
--
--       Description: 
--       This is the schema of Mioga2 database
--
-- ============================================================================
-- ============================================================================
-- TABLES
-- ============================================================================

--
-- Mioga Instances
--
create table m_mioga (
    rowid            serial UNIQUE NOT NULL,
    created          timestamp NOT NULL,
    modified         timestamp NOT NULL,
    ident            varchar(128) UNIQUE NOT NULL,
    admin_id         int4 UNIQUE,

    askConfirmForDelete        boolean NOT NULL DEFAULT 't',
    rightToAnimGroupForCreator boolean NOT NULL DEFAULT 'f',

    holidays                     varchar(128) NOT NULL DEFAULT '',
    domain_name                  varchar(128) NOT NULL DEFAULT 'none',

    private_key      oid,
    public_key       oid,
    
    use_ldap         boolean NOT NULL DEFAULT 'f',
	ldap_access      int4 DEFAULT 0,
    ldap_host        varchar(128),
    ldap_port        varchar(128),
    ldap_bind_dn     varchar(128),
    ldap_bind_pwd    varchar(128),
    ldap_user_base_dn varchar(128),
    ldap_user_filter varchar(128),
    ldap_user_scope  varchar(128),
    ldap_user_ident_attr varchar(128),
    ldap_user_firstname_attr varchar(128),
    ldap_user_lastname_attr varchar(128),
    ldap_user_email_attr varchar(128),
    ldap_user_pwd_attr varchar(128),
    ldap_user_desc_attr varchar(128),
    ldap_user_inst_attr varchar(128),
   
    -- User/Team/Resourece/Group number limitation. 
    -- NULL = no limit.
    max_allowed_group int4 DEFAULT NULL,
   	
    -- Disk quota per instance. 
    -- NULL = no limit.
    -- Size are in Mo
    quota_soft  int4 DEFAULT NULL,
    quota_hard  int4 DEFAULT NULL,
    quota_warning_date timestamp DEFAULT NULL,
    disk_usage  int4 DEFAULT NULL,
    locked	boolean DEFAULT 'f',

    -- Default theme and lang for instance
    default_theme_id int4 DEFAULT NULL, 
    default_lang_id int4 DEFAULT NULL, 

    -- Password related parameters
    password_crypt_method varchar(128) DEFAULT 'md5',

	-- Brute-force prevention
	auth_fail_timeout int4 DEFAULT 1800,

	-- Password policy
	pwd_min_length int4 default 6,
	pwd_min_letter int4 default 3,
	pwd_min_digit int4 default 3,
	pwd_min_special int4 default 0,
	pwd_min_chcase int4 default 0,
	use_secret_question boolean,
	doris_can_set_secret_question boolean DEFAULT 't',

	referent_id int4,
	referent_comment varchar(128),

	comment text,

	default_group_id int4,
	homepage varchar(128),
	target_url varchar(128)
);

create index m_mioga_ident_index on m_mioga (ident);

--
-- Persistent Session
--
create table m_persistent_session (
    session_id      varchar(128) UNIQUE NOT NULL,
    data 	    oid	NOT NULL
);
create index m_persistent_session_index on m_persistent_session (session_id);
create index m_persistent_session_data_index on m_persistent_session (data);



--
-- External Mioga Status
--
create table m_external_mioga_status (
    rowid           serial UNIQUE NOT NULL,
    ident           varchar(128) UNIQUE NOT NULL
);
create index m_external_mioga_status_ident_index on m_external_mioga_status (ident);


--
-- External Mioga
--
create table m_external_mioga (
    rowid            serial UNIQUE NOT NULL,
    created          timestamp NOT NULL,
    modified         timestamp NOT NULL,

    server           varchar(128) NOT NULL,
    public_key       oid UNIQUE NOT NULL,

    status_id        int4 NOT NULL,

    mioga_id         int4 NOT NULL,
    last_synchro     timestamp,
	
    unique (server, mioga_id),

    FOREIGN KEY (status_id) REFERENCES m_external_mioga_status (rowid),
    FOREIGN KEY (mioga_id) REFERENCES m_mioga (rowid)
        );
create index m_external_mioga_server_index on m_external_mioga (server);


--
-- Timezone
--
create table m_timezone (
    rowid            serial UNIQUE NOT NULL,
    created          timestamp NOT NULL,
    modified         timestamp NOT NULL,
    ident            varchar(128) UNIQUE NOT NULL
);
create index m_timezone_ident_index on m_timezone (ident);


--
-- Languages
--
create table m_lang (
    rowid            serial UNIQUE NOT NULL,
    ident             varchar(32) NOT NULL,
    locale            varchar(32) NOT NULL
);

--
-- Themes
--
create table m_theme (
    rowid            serial UNIQUE NOT NULL,
    created          timestamp NOT NULL,
    modified         timestamp NOT NULL,
    name             varchar(128) NOT NULL,
    ident            varchar(128) NOT NULL,
    mioga_id         int4 NOT NULL,
    FOREIGN KEY (mioga_id) REFERENCES m_mioga (rowid)
);
create unique index m_theme_ident_mioga_index on m_theme (ident, mioga_id);

--
-- Add constraint on m-mioga table
--
ALTER TABLE m_mioga ADD CONSTRAINT m_mioga_theme_chk
FOREIGN KEY (default_theme_id) REFERENCES m_theme (rowid);
ALTER TABLE m_mioga ADD CONSTRAINT m_mioga_lang_chk
FOREIGN KEY (default_lang_id) REFERENCES m_lang (rowid);
--
-- Application type
--
create table m_application_type (
   rowid                serial UNIQUE NOT NULL,
   ident                varchar (128) unique not null
);


--
-- Application
--
create table m_application (
    rowid               serial UNIQUE NOT NULL,
    created             timestamp NOT NULL,
    modified            timestamp NOT NULL,
    ident               varchar(128) NOT NULL,
    package             varchar(128) NOT NULL,
    description         text,
    can_be_public       boolean,                        -- tell if application can be public
    is_user             boolean,                        -- application can be used by a user or not
    is_group            boolean,                        -- application can be used by a group or not
    is_resource         boolean,                        -- application can be used by a resource or not
    type_id             int4 NOT NULL,

    FOREIGN KEY (type_id)  REFERENCES m_application_type (rowid)
);
create unique index m_application_ident_index on m_application (ident);
create unique index m_application_package_index on m_application (package);

--
-- Instance to Application table
--
CREATE TABLE m_instance_application (
	mioga_id         int4 NOT NULL,
	application_id   int4 NOT NULL,
	all_users        boolean DEFAULT false,
	all_groups       boolean DEFAULT false,
	all_resources    boolean DEFAULT false,

    FOREIGN KEY (mioga_id)       REFERENCES m_mioga (rowid),
    FOREIGN KEY (application_id) REFERENCES m_application (rowid)
);
CREATE UNIQUE INDEX m_instance_application_index ON m_instance_application (mioga_id, application_id);

--
-- Group type
--
create table m_group_type (
   rowid                serial UNIQUE NOT NULL,
   ident                varchar (128) unique not null
);
create index m_group_type_ident_index on m_group_type (ident);



--
-- Group base
--
create table m_group_base (
   rowid                serial UNIQUE NOT NULL,
   created              timestamp NOT NULL,
   modified             timestamp NOT NULL,
   ident		varchar(128) NOT NULL,
   mioga_id             int4,
   type_id              int4,
   public_part          boolean DEFAULT 'no',
   autonomous           boolean DEFAULT 'no',
   theme_id             int4,                   -- rowid of theme
   lang_id              int4,                   -- rowid of lang
   anim_id              int4,                   -- rowid of m_user for group administrator
   creator_id           int4,                   -- rowid of m_user for group creator
   default_profile_id   int4,                   -- rowid of m_profile for default new users
   disk_space_used      int4 DEFAULT 0,         -- disk space used by files in kilobytes
   history              boolean DEFAULT 'no',

   FOREIGN KEY (mioga_id) REFERENCES m_mioga (rowid),
   FOREIGN KEY (type_id) REFERENCES m_group_type (rowid),
   FOREIGN KEY (theme_id) REFERENCES m_theme (rowid),
   FOREIGN KEY (lang_id) REFERENCES m_lang (rowid)
);
create unique index m_group_base_ident_mioga_index on m_group_base (ident, mioga_id);


--
-- Group Status
--
create table m_group_status (
   rowid                serial UNIQUE NOT NULL,
   ident                varchar (128) UNIQUE not null
);
create index m_group_status_ident_index on m_group_status (ident);


--
-- Groups
--
create table m_group (
   description          text,
   status_id            int4 not null,
   default_app_id       int4,

   FOREIGN KEY (status_id) REFERENCES m_group_status (rowid),
   FOREIGN KEY (default_app_id) REFERENCES m_application (rowid)
) inherits (m_group_base);

-- create this unique index for the folowing foreign key.
create unique index m_group_ident_rowid_index on m_group (rowid);
create unique index m_group_ident_mioga_index on m_group (ident, mioga_id);

-- Foreign key from m_mioga default_group_id to m_group rowid
ALTER TABLE m_mioga ADD CONSTRAINT m_mioga_default_group_id_chk FOREIGN KEY (default_group_id) REFERENCES m_group (rowid);

--
-- Resource Status
--
create table m_resource_status (
   rowid                serial UNIQUE NOT NULL,
   ident                varchar (128) UNIQUE not null
);
create index m_resource_status_ident_index on m_resource_status (ident);



--
-- resource
--
create table m_resource (
   status_id            int4 NOT NULL,                   -- rowid of resource status
   location             varchar(128),
   timezone_id          int4 DEFAULT 1,                  -- rowid of timezone table
   time_per_day         int4 DEFAULT 480,                -- working time a day in minutes
   working_days         varchar(13) DEFAULT '1,2,3,4,5', -- comma separated list of working days from 0=Sunday to 6=Saturday
   monday_first         boolean DEFAULT 't',             -- true means show Monday as first week day, Sunday otherwise
   FOREIGN KEY (timezone_id) REFERENCES m_timezone (rowid),
   FOREIGN KEY (status_id) REFERENCES m_resource_status (rowid)
) inherits (m_group);

-- create this unique index for the folowing foreign key.
create unique index m_resource_rowid_index on m_resource(rowid);
create unique index m_resource_ident_mioga_index on m_resource (ident, mioga_id);



--
-- User Status
--
create table m_user_status (
   rowid                serial UNIQUE NOT NULL,
   ident                varchar (128) UNIQUE not null
);
create index m_user_status_ident_index on m_user_status (ident);



--
-- User base
--
create table m_user_base (
   firstname                varchar(128),
   lastname                 varchar(128),
   password                 varchar(128),
   email                    varchar(128),
   timezone_id              int4 DEFAULT 1,                  -- rowid of timezone table
   time_per_day             int4 DEFAULT 480,                -- working time a day in minutes
   working_days             varchar(13) DEFAULT '1,2,3,4,5', -- comma separated list of working days from 1=Monday to 7=Sunday
   monday_first             boolean DEFAULT 't',             -- true means show Monday as first week day, Sunday otherwise
   status_id                int4,
   auth_fail			    int4 DEFAULT 0,
   last_auth_fail		    timestamp,
   previous_user_status     int4,
   data_expiration_date     timestamp,
   password_expiration_date timestamp,
   external_ident           varchar(128),
   external_mioga_id        int4,
   dn                       text,
   shared                   boolean,

   FOREIGN KEY (timezone_id) REFERENCES m_timezone (rowid),
   FOREIGN KEY (status_id) REFERENCES m_user_status (rowid)
) inherits (m_group_base);

-- create this unique index for the folowing foreign key.
create unique index m_user_base_rowid_index on m_user_base(rowid);
create unique index m_user_base_ident_mioga_index on m_user_base (ident, mioga_id);
CREATE UNIQUE INDEX m_user_base_email_mioga_index ON m_user_base (email, mioga_id);

ALTER TABLE m_mioga ADD CONSTRAINT m_mioga_admin_chk
FOREIGN KEY (admin_id) REFERENCES m_user_base (rowid);


--
-- LDAP lock table
--
-- Table used while LDAP <=> Mioga synchronization
-- to avoid concurrent synchro.
--
create table m_ldap_lock_table (
   mioga_id             int4 unique not null,
   last_synchro_date    timestamp NOT NULL,
   FOREIGN KEY (mioga_id) REFERENCES m_mioga (rowid)
);



--
-- Due to a bug in postgresql with inheritance and foreign key
-- referencial integrity on m_group_base and its children can't 
-- be done using foreign keys.
--
-- To resolve this problem, we used triggers on this tables.
-- These triggers add/update/delete rowids in the m_group_rowid
-- table, and all foreign keys on groups/users/resource rowid 
-- point to this table.
--

create table m_group_rowids (
   rowid        serial UNIQUE NOT NULL
);



CREATE OR REPLACE FUNCTION check_group_base_default_profile_id () RETURNS trigger AS '
    DECLARE
        rec record;
    BEGIN
        SELECT * INTO rec FROM m_group_base WHERE default_profile_id = OLD.rowid;
        IF FOUND THEN
            RAISE EXCEPTION ''Profile rowid % is still referenced in m_group_base'', OLD.rowid;
            RETURN NULL;
        END IF;

        RETURN OLD;
    END;
' LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION check_group_base_mioga_id () RETURNS trigger AS '
    DECLARE
        rec record;
    BEGIN
        SELECT * INTO rec FROM m_group_base WHERE mioga_id = OLD.rowid;
        IF FOUND THEN
            RAISE EXCEPTION ''Instance rowid % is still referenced in m_group_base'', OLD.rowid;
            RETURN NULL;
        END IF;

        RETURN OLD;
    END;
' LANGUAGE 'plpgsql';

CREATE TRIGGER m_group_base_mioga_id_check
     BEFORE DELETE ON m_mioga FOR EACH ROW
     EXECUTE PROCEDURE check_group_base_mioga_id ();


CREATE OR REPLACE FUNCTION check_group_rowid () RETURNS trigger AS '
    BEGIN
        IF TG_OP = ''INSERT'' THEN
            INSERT INTO m_group_rowids VALUES (NEW.rowid);
        ELSIF TG_OP = ''DELETE'' THEN
            DELETE FROM m_group_rowids WHERE rowid = OLD.rowid;
            RETURN OLD;
        END IF;

        RETURN NEW;
    END;
' LANGUAGE 'plpgsql';


CREATE TRIGGER m_group_base_rowid_check 
     BEFORE INSERT OR DELETE ON m_group_base FOR EACH ROW
     EXECUTE PROCEDURE check_group_rowid ();


CREATE TRIGGER m_group_rowid_check 
     BEFORE INSERT OR DELETE ON m_group FOR EACH ROW
     EXECUTE PROCEDURE check_group_rowid ();


CREATE TRIGGER m_resource_rowid_check 
     BEFORE INSERT OR DELETE ON m_resource FOR EACH ROW
     EXECUTE PROCEDURE check_group_rowid ();


CREATE TRIGGER m_user_base_rowid_check 
     BEFORE INSERT OR DELETE ON m_user_base FOR EACH ROW
     EXECUTE PROCEDURE check_group_rowid ();




--
-- Add a foreign key on m_group_base.anim_id to m_group_rowids.rowid
--
ALTER TABLE m_group_base ADD CONSTRAINT m_group_base_animator_chk
FOREIGN KEY (anim_id) REFERENCES m_group_rowids (rowid);

ALTER TABLE m_group ADD CONSTRAINT m_group_animator_chk
FOREIGN KEY (anim_id) REFERENCES m_group_rowids (rowid);

ALTER TABLE m_resource ADD CONSTRAINT m_resource_animator_chk
FOREIGN KEY (anim_id) REFERENCES m_group_rowids (rowid);

ALTER TABLE m_user_base ADD CONSTRAINT m_user_base_animator_chk
FOREIGN KEY (anim_id) REFERENCES m_group_rowids (rowid);

--
-- Add a foreign key on m_group_base.creator_id to m_user_base.rowid
--

ALTER TABLE m_group_base ADD CONSTRAINT m_group_base_creator_chk
FOREIGN KEY (creator_id) REFERENCES m_group_rowids (rowid);

ALTER TABLE m_group ADD CONSTRAINT m_group_creator_chk
FOREIGN KEY (creator_id) REFERENCES m_group_rowids (rowid);

ALTER TABLE m_resource ADD CONSTRAINT m_resource_creator_chk
FOREIGN KEY (creator_id) REFERENCES m_group_rowids (rowid);

ALTER TABLE m_user_base ADD CONSTRAINT m_user_base_creator_chk
FOREIGN KEY (creator_id) REFERENCES m_group_rowids (rowid);



--
-- Groups <--> Groups
--
create table m_group_group (
    group_id            int4,        -- rowid of m_group
    invited_group_id    int4,        -- rowid of m_group (invited)

    primary key (group_id,invited_group_id),
    unique (group_id, invited_group_id),

    FOREIGN KEY (invited_group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);


--
-- User last connection date to group
--
create table m_group_user_last_conn (
    group_id            int4,        -- rowid of m_group
    user_id             int4,        -- rowid of m_user_base
    last_connection     timestamp,

    primary key (group_id,user_id),
    unique (group_id,user_id),
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);



--
-- Like m_group_group but add the type of the invited group.
--
create view m_group_group_type as
    select m_group_group.*, m_group_type.ident as type

    from   m_group_group, m_group_type, m_group_base

    where  m_group_type.rowid = m_group_base.type_id and
           m_group_base.rowid = m_group_group.invited_group_id;
        


--
-- View that show only groups (m_group) invited in groups
--
create view m_group_invited_group as
    select m_group_group.* 

    from m_group_group, m_group

    where m_group_group.invited_group_id = m_group.rowid;

--
-- View that show only users (m_user_base) invited in groups
--
create view m_group_invited_user as
    select m_group_group.*

    from m_group_group, m_user_base

    where m_group_group.invited_group_id = m_user_base.rowid;


--
--  Expand m_group_invited_group.
--
create view m_group_expanded_user as
        select distinct m_group_invited_group.group_id as group_id,
                        m_group_invited_user.invited_group_id as invited_group_id

        from   m_group_invited_group, m_group_invited_user

        where  m_group_invited_user.group_id = m_group_invited_group.invited_group_id;


--
-- Applications <--> GroupBase
--
create table m_application_group (
    application_id      int4, -- rowid of m_application
    group_id            int4, -- rowid of m_group_base

    unique(application_id, group_id),

    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);



--
-- Applications enabled for a group
--
create table m_application_group_allowed (
    rowid               serial UNIQUE NOT NULL,
    created             timestamp NOT NULL,
    modified            timestamp NOT NULL,
    application_id      int4,
    group_id            int4,
    is_public           boolean NOT NULL DEFAULT 'f', -- application can be public in this group or not

    FOREIGN KEY (application_id) REFERENCES m_application (rowid),
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);
create unique index m_application_group_allowed_app_group_index on m_application_group_allowed (application_id, group_id);


--
-- View application status for groups
--

CREATE VIEW m_application_group_status AS
	SELECT DISTINCT m_application.*, m_group.rowid AS group_id,
		   (SELECT count(*) FROM m_application_group_allowed 

			WHERE m_application_group_allowed.application_id = m_application.rowid AND
				  m_application_group_allowed.group_id = m_group.rowid
		   ) AS status,

		   ((SELECT count(*) FROM m_application_group_allowed 

			 WHERE m_application_group_allowed.application_id = m_application.rowid AND
				   m_application_group_allowed.group_id = m_group.rowid AND 
				   m_application_group_allowed.is_public = 't'
			) !=0 AND can_be_public) AS is_public,
			m_instance_application.mioga_id


	FROM m_application, m_application_group, m_group, m_application_type, m_instance_application

	WHERE 
		   (m_application_group.group_id = m_group.rowid AND 
			m_application.rowid = m_application_group.application_id) AND 
		  m_application_type.rowid = m_application.type_id AND 
		  m_application.rowid = m_instance_application.application_id AND
		  m_group.mioga_id = m_instance_application.mioga_id AND
		  m_application_type.ident NOT IN ( 'library', 'shell')
	UNION SELECT DISTINCT m_application.*, m_group.rowid AS group_id,
		   (SELECT count(*) FROM m_application_group_allowed 

			WHERE m_application_group_allowed.application_id = m_application.rowid AND
				  m_application_group_allowed.group_id = m_group.rowid
		   ) AS status,

		   ((SELECT count(*) FROM m_application_group_allowed 

			 WHERE m_application_group_allowed.application_id = m_application.rowid AND
				   m_application_group_allowed.group_id = m_group.rowid AND 
				   m_application_group_allowed.is_public = 't'
			) !=0 AND can_be_public) AS is_public,
			m_instance_application.mioga_id


	FROM m_application, m_group, m_application_type, m_instance_application

	WHERE 
		  m_instance_application.all_groups = 't' AND
		  m_application_type.rowid = m_application.type_id AND 
		  m_application_type.ident NOT IN ( 'library', 'shell') AND
		  m_application.rowid = m_instance_application.application_id AND
		  m_instance_application.mioga_id = m_group.mioga_id;


--
-- View application status for users
--

CREATE VIEW m_application_user_status AS
	SELECT DISTINCT 
		m_application.rowid, 
		m_application.created, 
		m_application.modified, 
		m_application.ident, 
		m_application.package, 
		m_application.description, 
		m_application.can_be_public, 
		m_instance_application.all_resources, 
		m_instance_application.all_groups, 
		m_instance_application.all_users, 
		m_application.type_id, 
		m_instance_application.mioga_id, 
		m_user_base.rowid AS group_id, 
		( SELECT count(*) AS count
			   FROM m_application_group_allowed
			  WHERE m_application_group_allowed.application_id = m_application.rowid AND 
					m_application_group_allowed.group_id = m_user_base.rowid) AS status, 
		(( SELECT count(*) AS count
			   FROM m_application_group_allowed
			  WHERE m_application_group_allowed.application_id = m_application.rowid AND 
					m_application_group_allowed.group_id = m_user_base.rowid AND 
					m_application_group_allowed.is_public = true)) <> 0 AND 
					m_application.can_be_public AS is_public
	FROM 
		m_application, 
		m_application_group, 
		m_user_base, 
		m_instance_application,
		m_application_type
	WHERE 
		m_instance_application.all_users = false AND
		m_application_group.group_id = m_user_base.rowid AND 
		m_application.rowid = m_application_group.application_id AND 
		m_application.rowid = m_instance_application.application_id AND
		m_user_base.mioga_id = m_instance_application.mioga_id AND
		m_application_type.rowid = m_application.type_id AND 
		m_application_type.ident::text <> 'library'::text AND 
		m_application_type.ident::text <> 'shell'::text
	UNION SELECT DISTINCT
		m_application.rowid, 
		m_application.created, 
		m_application.modified, 
		m_application.ident, 
		m_application.package, 
		m_application.description, 
		m_application.can_be_public, 
		m_instance_application.all_resources, 
		m_instance_application.all_groups, 
		m_instance_application.all_users, 
		m_application.type_id, 
		m_instance_application.mioga_id, 
		m_user_base.rowid AS group_id, 
		( SELECT count(*) AS count
			   FROM m_application_group_allowed
			  WHERE m_application_group_allowed.application_id = m_application.rowid AND 
					m_application_group_allowed.group_id = m_user_base.rowid) AS status, 
		(( SELECT count(*) AS count
			   FROM m_application_group_allowed
			  WHERE m_application_group_allowed.application_id = m_application.rowid AND 
					m_application_group_allowed.group_id = m_user_base.rowid AND 
					m_application_group_allowed.is_public = true)) <> 0 AND 
					m_application.can_be_public AS is_public
	FROM 
		m_application, 
		m_user_base, 
		m_instance_application,
		m_application_type
	WHERE 
		m_instance_application.all_users = true AND
		m_application_type.rowid = m_application.type_id AND 
		m_application_type.ident::text <> 'library'::text AND 
		m_application_type.ident::text <> 'shell'::text AND
		m_application.rowid = m_instance_application.application_id AND
		m_instance_application.mioga_id = m_user_base.mioga_id
	ORDER BY rowid, 
		created, 
		modified, 
		ident, 
		package, 
		description, 
		can_be_public, 
		all_resources, 
		all_groups, 
		all_users, 
		type_id, 
		mioga_id, 
		group_id, 
		status,
		is_public;


--
-- Function
--
create table m_function (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp NOT NULL,
    modified     timestamp NOT NULL,
    ident        varchar(128) NOT NULL,
    description  text,
    application_id     int4 NOT NULL,        -- rowid of application table
    FOREIGN KEY (application_id) REFERENCES m_application (rowid),
    unique(application_id, ident)
);
create unique index m_function_ident_application_index on m_function (ident, application_id);


--
-- Method type
--
create table m_method_type (
   rowid                serial UNIQUE NOT NULL,
   ident                varchar (128) unique not null
);

--
-- Method
--
create table m_method (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp NOT NULL,
    modified     timestamp NOT NULL,
    ident        varchar(128) NOT NULL,
    function_id  int4 NOT NULL,                  -- rowid of function table
    type_id  int4 NOT NULL,                      -- rowid of function table
    FOREIGN KEY (function_id) REFERENCES m_function (rowid),
    FOREIGN KEY (type_id) REFERENCES m_method_type (rowid),

    unique(function_id, ident)
);
create index m_method_ident_function_index on m_method (ident, function_id);



--
-- Profile
--
create table m_profile (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp NOT NULL,
    modified     timestamp NOT NULL,
    ident        varchar(128) NOT NULL,
    group_id     int4 NOT NULL,                   -- rowid of m_group_base
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);
create unique index m_profile_ident_group_index on m_profile (ident, group_id);

CREATE TRIGGER m_group_base_default_profile_id_check
     BEFORE DELETE ON m_profile FOR EACH ROW
     EXECUTE PROCEDURE check_group_base_default_profile_id ();


--
-- Add referencial integrity on default_profile_id in m_group_base
--

ALTER TABLE m_group_base ADD CONSTRAINT m_group_base_profile_chk

FOREIGN KEY (default_profile_id) REFERENCES m_profile (rowid);


--
-- Profile <--> Group
--
create table m_profile_group (
    profile_id          int4,        -- rowid of m_profile
    group_id            int4,        -- rowid of m_group_base
    FOREIGN KEY (profile_id) REFERENCES m_profile (rowid),
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);
create unique index m_profile_group_prof_group_index on m_profile_group (profile_id, group_id);


--
-- Profile <--> Function
--
create table m_profile_function (
    profile_id  int4,        -- rowid of m_profile
    function_id int4,        -- rowid of m_function
    FOREIGN KEY (profile_id) REFERENCES m_profile (rowid),
    FOREIGN KEY (function_id) REFERENCES m_function (rowid)
);

create unique index m_profile_function_prof_group_index on m_profile_function (profile_id, function_id);

--
-- expand m_profile_group from m_group_expanded_user
--
create view m_profile_expanded_user as
    select distinct m_profile_group.profile_id as profile_id,
           m_group_group.invited_group_id as user_id

    from   m_group_group, m_profile_group, m_group

    where  m_group_group.group_id = m_profile_group.group_id AND
           m_group.rowid = m_group_group.group_id;
           

--
-- This view based on m_group_expanded_user, m_profile_group and 
-- m_profile_function show which user can access to a given function
--

create view m_profile_expanded_user_functio as
    select distinct  m_profile.rowid as profile_id,
           m_application.rowid as app_id,
           m_application.ident as app_ident,
           m_function.rowid as function_id,
           m_function.ident as function_ident,
           m_profile.group_id as group_id,
           m_group_group.invited_group_id as user_id

    from   m_profile, m_group_group, m_group, m_profile_function,
           m_profile_group, m_function, m_application

    where  m_profile_group.profile_id = m_profile.rowid and
           m_profile_function.profile_id = m_profile.rowid and
           m_function.rowid = m_profile_function.function_id and
           m_application.rowid = m_function.application_id and
           m_group_group.group_id = m_profile_group.group_id AND
           m_group.rowid = m_group_group.group_id;
           


--
-- This view based on m_group_expanded_user, m_profile_group,
-- m_profile_function and m_method show which user can access
-- to a given method
--

create view m_profile_expanded_user_method as
    select distinct m_profile.rowid as profile_id,
           m_application.rowid as app_id,
           m_application.ident as app_ident,
           m_method.rowid as method_id,
           m_method.ident as method_ident,
           m_profile.group_id as group_id,
           m_group_group.invited_group_id as user_id

    from   m_profile, m_group_group, m_group, m_profile_function,
           m_profile_group, m_method, m_function, m_application

    where  m_profile_group.profile_id = m_profile.rowid and
           m_profile_function.profile_id = m_profile.rowid and
           m_method.function_id = m_profile_function.function_id and
           m_function.rowid = m_profile_function.function_id and
           m_application.rowid = m_function.application_id and
           m_group_group.group_id = m_profile_group.group_id AND
           m_group.rowid = m_group_group.group_id;
                      

--
-- Show only user (m_user_base) profiles
--
create view m_profile_user as
    select distinct m_profile_group.*

    from   m_profile_group, m_user_base

    where  m_profile_group.group_id = m_user_base.rowid;


--
-- This view based on m_profile_user, m_profile_group and 
-- m_profile_function show which user can access to a given function
--

create view m_profile_user_function as
    select distinct m_profile.rowid as profile_id,
           m_application.rowid as app_id,
           m_application.ident as app_ident,
           m_function.rowid as function_id,
           m_function.ident as function_ident,
           m_profile.group_id as group_id,
           m_profile_group.group_id as user_id

    from   m_profile, m_profile_function,
           m_profile_group, m_function, m_application,
           m_user_base

    where  m_profile_group.profile_id = m_profile.rowid and
           m_profile_group.group_id = m_user_base.rowid and
           m_profile_function.profile_id = m_profile.rowid and
           m_function.rowid = m_profile_function.function_id and
           m_application.rowid = m_function.application_id;
           


--
-- This view based on m_profile_user, m_profile_group,
-- m_profile_function and m_method show which user can access
-- to a given method
--

create view m_profile_user_method as
    select distinct m_profile.rowid as profile_id,
           m_application.rowid as app_id,
           m_application.ident as app_ident,
           m_method.rowid as method_id,
           m_method.ident as method_ident,
           m_profile.group_id as group_id,
           m_profile_group.group_id as user_id

    from   m_profile, m_profile_function,
           m_profile_group, m_method, m_function, m_application,
           m_user_base

    where  m_profile_group.profile_id = m_profile.rowid and
           m_profile_group.group_id = m_user_base.rowid and
           m_profile_function.profile_id = m_profile.rowid and
           m_method.function_id = m_profile_function.function_id and
           m_function.rowid = m_profile_function.function_id and
           m_application.rowid = m_function.application_id;



--
-- Uri
--

create table m_uri (
    rowid         serial UNIQUE NOT NULL,
    group_id      int4,          -- rowid of m_group_base table
    uri           text UNIQUE,
    parent_uri_id int4 NOT NULL, -- uri_id of parent uri (uri having defined rights)
    stop_inheritance boolean NOT NULL DEFAULT FALSE,
    modified      timestamp NOT NULL DEFAULT NOW(),
    user_id       int4 NOT NULL,
    lang          varchar(64),
    mimetype      text,
	elements      integer NOT NULL DEFAULT 0,
	size          integer,
	hidden        boolean NOT NULL DEFAULT FALSE,
	directory_id  int4,
	history       boolean NOT NULL DEFAULT FALSE,
    
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (directory_id) REFERENCES m_uri (rowid)
);
CREATE INDEX modified_idx ON m_uri(modified);
CREATE UNIQUE INDEX uri_directory_idx ON m_uri (uri, directory_id);

create table uri_history (
    rowid       serial UNIQUE NOT NULL,
    modified    timestamp NOT NULL,
    user_id     int NOT NULL,
    uri_id      int NOT NULL,
    old_name    text,
    
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (uri_id) REFERENCES m_uri (rowid)
);

--create index m_uri_rowid_group_id_index on m_uri (rowid, group_id);
create index m_uri_uri_index on m_uri (uri);
create index m_uri_uri_text_index on m_uri (uri text_pattern_ops);

--
-- Authorize
--


create table m_authorize (
    rowid        serial UNIQUE NOT NULL ,   -- rowid of authorization schemes
    uri_id       int4 NOT NULL,             -- rowid of m_uri table
    access       int2 NOT NULL,             -- 0 : no right, 1 : read only, 2 : read / write
    FOREIGN KEY (uri_id) REFERENCES m_uri (rowid),
    unique      (uri_id, access)
);

create index m_authorize_uri_rowid_index on m_authorize (rowid, uri_id);


--
-- Authorize for profile
--

create table m_authorize_profile (
    authz_id     int4 NOT NULL,     -- rowid of m_authorize table
    profile_id   int4 NOT NULL,     -- rowid of m_profile table or NULL
    unique (authz_id, profile_id),
    FOREIGN KEY (profile_id) REFERENCES m_profile (rowid),
    FOREIGN KEY (authz_id)   REFERENCES m_authorize (rowid)
);

create index m_authorize_profile_profile_index on m_authorize_profile (profile_id);


--
-- Authorize for user
--


create table m_authorize_user (
    authz_id     int4 NOT NULL,       -- rowid of m_authorize table
    user_id      int4 NOT NULL,       -- rowid of m_user_base table
    FOREIGN KEY (user_id)  REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (authz_id) REFERENCES m_authorize (rowid),
    unique      (authz_id, user_id)
);
create index m_authorize_user_user_index on m_authorize_user (user_id);


--
-- Authorize for teams
--


create table m_authorize_team (
    authz_id     int4 NOT NULL,       -- rowid of m_authorize table
    team_id      int4 NOT NULL,       -- rowid of m_group table
    FOREIGN KEY (team_id)  REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (authz_id) REFERENCES m_authorize (rowid),
    unique      (authz_id, team_id)
);
create index m_authorize_team_team_index on m_authorize_team (team_id);

--
-- User authorization to URIs
-- This view returns a full list of URIs and user rowids with associated access
--
CREATE VIEW user_uri_access AS
	SELECT uri_id, uri, user_id, (max(access)%10) AS access
		FROM (
			SELECT m_uri.rowid AS uri_id, m_uri.uri AS uri, m_authorize_user.user_id AS user_id, (50 + m_authorize.access) AS access
				FROM
					m_authorize, m_uri, m_authorize_user
				WHERE
					m_authorize.uri_id = m_uri.parent_uri_id
					AND m_authorize_user.authz_id = m_authorize.rowid
			UNION SELECT m_uri.rowid AS uri_id, m_uri.uri AS uri, m_group_group.invited_group_id AS user_id, (40 + m_authorize.access) AS access
				FROM
					m_authorize, m_uri, m_authorize_team, m_group_group
				WHERE
					m_authorize.uri_id = m_uri.parent_uri_id
					AND m_authorize_team.authz_id = m_authorize.rowid
					AND m_group_group.group_id = m_authorize_team.team_id
			UNION SELECT m_uri.rowid AS uri_id, m_uri.uri AS uri, m_profile_group.group_id AS user_id, (30 + m_authorize.access) AS access
				FROM
					m_authorize, m_uri, m_authorize_profile, m_profile_group
				WHERE
					m_authorize.uri_id = m_uri.parent_uri_id
					AND m_authorize_profile.authz_id = m_authorize.rowid
					AND m_profile_group.profile_id = m_authorize_profile.profile_id
			UNION SELECT m_uri.rowid AS uri_id, m_uri.uri AS uri, m_profile_expanded_user.user_id AS user_id, (20 + m_authorize.access) AS access
				FROM
					m_authorize, m_uri, m_authorize_profile, m_profile_expanded_user
				WHERE
					m_authorize.uri_id = m_uri.parent_uri_id
					AND m_authorize_profile.authz_id = m_authorize.rowid
					AND m_profile_expanded_user.profile_id = m_authorize_profile.profile_id
		) AS tmp_req
	GROUP BY uri_id, uri, user_id;



CREATE OR REPLACE FUNCTION uri2regexp(VARCHAR) RETURNS VARCHAR AS '
    my ($uri) = @_;

    my @splited = split ("/", $uri);

    # skip first undef element.
    shift (@splited);

    my $regexp;
    my $tmp;
       
    while (defined($tmp = pop (@splited))) {
        $regexp = "(/$tmp"."$regexp)?";
    }

    return "$regexp";

' LANGUAGE 'plperl';


CREATE OR REPLACE FUNCTION authzTestAccessForURI(VARCHAR, INTEGER) RETURNS INTEGER AS '
        DECLARE 
                uriToCheck ALIAS FOR $1;
                userId ALIAS FOR $2;
                myrecord RECORD;

        BEGIN
        --
        -- Search authorization for profile.
        -- 
 
	SELECT INTO myrecord uri_id, max(accesskey) AS accesskey FROM (

	SELECT m_uri.rowid AS uri_id, (50 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_user
	WHERE  m_uri.uri = uriToCheck AND
       	       m_authorize.uri_id = m_uri.parent_uri_id AND
	       m_authorize_user.authz_id = m_authorize.rowid AND
	       m_authorize_user.user_id = userId

	UNION

	SELECT m_uri.rowid AS uri_id, (40 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_team, m_group_group
	WHERE  m_uri.uri = uriToCheck AND
	       m_authorize.uri_id = m_uri.parent_uri_id AND
	       m_authorize_team.authz_id = m_authorize.rowid AND
	       m_group_group.group_id = m_authorize_team.team_id AND
	       m_group_group.invited_group_id = userId

	UNION

	SELECT m_uri.rowid AS uri_id, (30 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_profile, m_profile_group
	WHERE  m_uri.uri = uriToCheck AND
	       m_authorize.uri_id = m_uri.parent_uri_id AND
	       m_authorize_profile.authz_id = m_authorize.rowid AND
	       m_profile_group.profile_id = m_authorize_profile.profile_id AND
	       m_profile_group.group_id = userId

	UNION

	SELECT m_uri.rowid AS uri_id, (20 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_profile, m_profile_expanded_user
	WHERE  m_uri.uri = uriToCheck AND
	       m_authorize.uri_id = m_uri.parent_uri_id AND
	       m_authorize_profile.authz_id = m_authorize.rowid AND
	       m_profile_expanded_user.profile_id = m_authorize_profile.profile_id AND
	       m_profile_expanded_user.user_id = userId

	UNION

	SELECT m_uri.rowid AS uri_id, 10 AS accesskey FROM m_uri WHERE m_uri.uri = uriToCheck
	
	
	) AS tmp_req

	GROUP BY (uri_id);


	IF FOUND THEN
		return (myrecord.accesskey % 10);
	END IF;

	return NULL;

        END;

' LANGUAGE 'plpgsql';




CREATE OR REPLACE FUNCTION authzTestAccessForUserOnApp(INTEGER, INTEGER, INTEGER) RETURNS BOOLEAN AS '
        DECLARE 
                userId  ALIAS FOR $1;
                groupId ALIAS FOR $2;
                appId   ALIAS FOR $3;
                myrecord RECORD;

        BEGIN

	        SELECT INTO myrecord *
		FROM  m_profile_user_function
		WHERE m_profile_user_function.group_id = groupId AND
		      m_profile_user_function.user_id = userId AND
		      m_profile_user_function.app_id = appId;
	

                IF FOUND THEN
                        return TRUE;
                END IF; 


	        SELECT INTO myrecord *
		FROM  m_profile_expanded_user_functio
		WHERE m_profile_expanded_user_functio.group_id = groupId AND
		      m_profile_expanded_user_functio.user_id = userId AND
		      m_profile_expanded_user_functio.app_id = appId;
	

                IF FOUND THEN
                        return TRUE;
                END IF; 

		return FALSE;
        END;

' LANGUAGE 'plpgsql';




--
-- User home URL
--

create table m_home_url (
    rowid               serial UNIQUE NOT NULL,
    created             timestamp NOT NULL,
    modified            timestamp NOT NULL,
    home_url            text,
    user_id             int4,
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);



--
-- Workspace menu tables
--
create table workspace_branch (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp NOT NULL,
    modified    timestamp NOT NULL,
    ident       text,
    number      int4,
    ancestor_id int4 default 0,
    user_id     int4,
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);

create table workspace_link (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp NOT NULL,
    modified    timestamp NOT NULL,
    ident       text,
    link        text,
    number      int4,
    ancestor_id int4 default NULL,
    user_id     int4,
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (ancestor_id) REFERENCES workspace_branch (rowid)
);

create table workspace_default_url (
    rowid               serial UNIQUE NOT NULL,
    created             timestamp NOT NULL,
    modified            timestamp NOT NULL,
    default_url         text,
    user_id             int4,
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);

create table workspace_display_entry (
    rowid               serial UNIQUE NOT NULL,
    created             timestamp NOT NULL,
    modified            timestamp NOT NULL,
    ident               varchar(128) NOT NULL,
    display_ok          bool,
    number              int4,
    user_id             int4,
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);

create table workspace_resize (
    rowid               serial UNIQUE NOT NULL,
    created             timestamp NOT NULL,
    modified            timestamp NOT NULL,
    resize              bool default 'f',
    user_id             int4,
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);

-- ##################################################################
-- organizer tables
-- ##################################################################

--
-- Task Categories
--

create table task_category (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp NOT NULL,
    modified    timestamp NOT NULL,
    name        varchar(128) NOT NULL,
    bgcolor     varchar(7),
    fgcolor     varchar(7),
    private     boolean NOT NULL DEFAULT 'f',
    mioga_id    int4 NOT NULL,

    UNIQUE(name, mioga_id),
    FOREIGN KEY (mioga_id) REFERENCES m_mioga (rowid)
);


--
-- Organizer user preference
--

create table org_user_pref (
    rowid       serial UNIQUE NOT NULL NOT NULL,
    created     timestamp NOT NULL,
    modified    timestamp NOT NULL,

    user_id     int4 UNIQUE NOT NULL,
    
    start_time   time DEFAULT '08:00:00',         -- first time displayed
    end_time     time DEFAULT '20:00:00',         -- last time displayed
    interval     int4 DEFAULT 60,                 -- time interval (in minutes) between two time divisions

    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)

);


--
-- Sychronization tables
--

create table synchro_agents (
   rowid                serial UNIQUE NOT NULL,
   ident                varchar (256) not null,
   mioga_id             int4 not null,
   last_synchro_date    timestamp NOT NULL DEFAULT '1970-01-01 00:00:00',

   unique(ident, mioga_id)
);


create table synchro_rowids (
   synchro_id           int4   NOT NULL,
   app_id               int4   NOT NULL,
   mioga_rowid          int4   NOT NULL,
   remote_rowid         int4   NOT NULL,

   FOREIGN KEY (app_id) REFERENCES m_application(rowid),
   FOREIGN KEY (synchro_id) REFERENCES synchro_agents(rowid)
);




--
-- Org task  type
--

create table org_task_type (
   rowid                serial UNIQUE NOT NULL,
   ident                varchar (128) unique not null
);


insert into org_task_type (ident) VALUES ('todo');
insert into org_task_type (ident) VALUES ('planned');
insert into org_task_type (ident) VALUES ('periodic');
insert into org_task_type (ident) VALUES ('flexible');

--
-- Organizer Tasks base class (virtual)
--

create table org_task (
   rowid           serial UNIQUE NOT NULL,
   primary key (rowid),
   created         timestamp NOT NULL,
   modified        timestamp NOT NULL,
   type_id         int4      NOT NULL,   -- task type
   group_id        int4      NOT NULL,   -- the group corresponding to the user's organiser to watch
   name            varchar(128),
   description     text,
   category_id     int4    NOT NULL,
   outside         boolean DEFAULT 'f',   -- in or out side the company
   first_name      varchar(128),
   last_name       varchar(128),
   telephone       varchar(128),
   fax             varchar(128),
   email           varchar(128),
   waiting_book    boolean DEFAULT 'f',
   user_id         int,

   FOREIGN KEY (type_id) REFERENCES org_task_type (rowid),
   FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
   FOREIGN KEY (category_id) REFERENCES task_category (rowid),
   FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);


--
-- Organiser todo task priority
--

create table org_todo_task_priority (
   rowid                serial UNIQUE NOT NULL,
   ident                varchar (128) unique not null
);

insert into org_todo_task_priority (ident) VALUES ('very urgent');
insert into org_todo_task_priority (ident) VALUES ('urgent');
insert into org_todo_task_priority (ident) VALUES ('normal');
insert into org_todo_task_priority (ident) VALUES ('not urgent');

--
-- Organizer Todo Tasks (is a org_task)
--

   create table org_todo_task (
      priority_id        int4 NOT NULL,               -- 1: very urgent 2: urgent 3: normal 4: not urgent
      done               boolean NOT NULL DEFAULT 'f',

      FOREIGN KEY (priority_id) REFERENCES org_todo_task_priority (rowid)
      
   ) inherits (org_task);


--
-- Organizer planned tasks (is a org_task)
--

   create table org_planned_task (
      start             timestamp,
      is_planned        boolean DEFAULT 't',
      duration          int -- in seconds
      
   ) inherits (org_task);


--
-- Organizer periodic tasks (is a org_planned_task)
--
   create table org_periodic_task (
      stop          timestamp,
      repetition    varchar(256)
   ) inherits (org_planned_task);


--
-- Cancelled instance of periodic tasks
--
create table org_cancelled_periodic_task (
   task_id         int4 not null,
   date            timestamp not null,
   created         timestamp not null,
   modified        timestamp not null,
   unique(task_id, date)
);


--
-- Organizer flexible tasks (is a org_task)
--

   create table org_flexible_task (
      start             timestamp,
      stop              timestamp,
      creator_id        int4 NOT NULL,
      delegate_id       int4 NOT NULL,
      start_constraint  int4 NOT NULL,  -- 0 none, 1 fixed, 2 earliest, 3 latest
      stop_constraint   int4 NOT NULL,  -- 0 none, 1 fixed, 2 earliest, 3 latest
      load_value        int,
      load_unit         int4,      -- 0 = second, 1 = minute, 2 = hour, 3 = day
      progress          int4 DEFAULT 0, -- in percent

      FOREIGN KEY (creator_id) REFERENCES m_group_rowids (rowid),
      FOREIGN KEY (delegate_id) REFERENCES m_group_rowids (rowid)

   ) inherits (org_task);



--
-- meeting tables
--
create table org_meeting (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp NOT NULL,
    modified    timestamp NOT NULL,
    group_id    int4 NOT NULL,      -- meeting owner group id
    category_id int4 NOT NULL,      -- category id
    name        varchar(128),       -- appointment name
    description text,               -- appointment description
    duration    int4 NOT NULL,      -- time length for the meeting

    FOREIGN KEY (category_id) REFERENCES task_category (rowid),
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

create table org_meeting_resource (
    meeting_id  int4 NOT NULL,
    resource_id int4 NOT NULL,
    task_id     int4,
    unique(meeting_id, resource_id),

    FOREIGN KEY (meeting_id) REFERENCES org_meeting (rowid),
    FOREIGN KEY (resource_id) REFERENCES m_group_rowids (rowid)
);

create table org_meeting_date (
    meeting_id  int4 NOT NULL,
    date        timestamp NOT NULL,
    unique(meeting_id, date),
    FOREIGN KEY (meeting_id) REFERENCES org_meeting (rowid)
);

--
-- Due to a bug in postgresql with inheritance and foreign key
-- referencial integrity on org_task and its children can't 
-- be done using foreign keys.
--
-- To resolve this problem, we used triggers on this tables.
-- These triggers add/update/delete rowids in the org_task_rowids
-- table, and all foreign keys on rowid 
-- point to this table.
--

create table org_task_rowids (
   rowid        serial UNIQUE NOT NULL
);

CREATE OR REPLACE FUNCTION check_org_task_rowid () RETURNS trigger AS '
    BEGIN
        IF TG_OP = ''INSERT'' THEN
            INSERT INTO org_task_rowids VALUES (NEW.rowid);
        ELSIF TG_OP = ''DELETE'' THEN
            DELETE FROM org_task_rowids WHERE rowid = OLD.rowid;
            RETURN OLD;
        END IF;

        RETURN NEW;
    END;
' LANGUAGE 'plpgsql';

CREATE TRIGGER org_task_rowid_check 
     BEFORE INSERT OR DELETE ON org_task FOR EACH ROW
     EXECUTE PROCEDURE check_org_task_rowid ();

CREATE TRIGGER org_todo_task_rowid_check 
     BEFORE INSERT OR DELETE ON org_todo_task FOR EACH ROW
     EXECUTE PROCEDURE check_org_task_rowid ();

CREATE TRIGGER org_planned_task_rowid_check 
     BEFORE INSERT OR DELETE ON org_planned_task FOR EACH ROW
     EXECUTE PROCEDURE check_org_task_rowid ();

CREATE TRIGGER org_flexible_task_rowid_check 
     BEFORE INSERT OR DELETE ON org_periodic_task FOR EACH ROW
     EXECUTE PROCEDURE check_org_task_rowid ();

--
-- Contacts Manager table
--
create table contacts (
       rowid             serial UNIQUE NOT NULL,
       created           timestamp NOT NULL,
       modified          timestamp NOT NULL,
       group_id          int4 NOT NULL,
       name              varchar(128) NOT NULL,
       surname           varchar(128),
       photo_data        oid,
       photo_url         text,
       title             varchar(128),
       orgname           varchar(128),
       email_work        varchar(128),
       email_home        varchar(128),
       tel_work          varchar(128),
       tel_home          varchar(128),
       tel_mobile        varchar(128),
       tel_fax           varchar(128),
       addr_home         text,
       city_home         varchar(128),
       postal_code_home  varchar(128),
       state_home        varchar(128),
       country_home      varchar(128),
       addr_work         text,
       city_work         varchar(128),
       postal_code_work  varchar(128),
       state_work        varchar(128),
       country_work      varchar(128),
       url               text,
       comment           text,

       FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

-- ##################################################################
-- Document tables
-- ##################################################################

create table document (
    rowid            serial UNIQUE NOT NULL,
    created          timestamp NOT NULL,
    modified         timestamp NOT NULL,
	url              text NOT NULL,
	app_id           int4 NOT NULL,
	mioga_id         int4 NOT NULL,

	FOREIGN KEY (app_id) REFERENCES m_application (rowid),
	FOREIGN KEY (mioga_id) REFERENCES m_mioga (rowid)
);

--
-- Document nesting - optional
--
CREATE TABLE document_tree (
	document_id      int4 NOT NULL,
	inherits_from    int4 NOT NULL,
	stop_inheritance boolean NOT NULL DEFAULT FALSE,

	FOREIGN KEY (document_id) REFERENCES document (rowid),
	FOREIGN KEY (inherits_from) REFERENCES document (rowid)
);

--
-- Document to group association
--
CREATE TABLE document_group (
	document_id      int4 NOT NULL,
	group_id         int4 NOT NULL,

	FOREIGN KEY (document_id) REFERENCES document (rowid),
	FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

--
-- Document authz for user
--
create table doc_authz_user (
    rowid        serial UNIQUE NOT NULL,
    doc_id       int4 NOT NULL,       -- rowid of document
    user_id      int4 NOT NULL,       -- rowid of m_user_base table
    access       int2 NOT NULL,       -- 0 : no right, 1 : read only, 2 : read / write

    FOREIGN KEY (user_id)  REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (doc_id) REFERENCES document (rowid),
    unique      (doc_id, user_id)
);
--
-- Document authz for teams
--
create table doc_authz_team (
    rowid        serial UNIQUE NOT NULL,
    doc_id       int4 NOT NULL,       -- rowid of document
    team_id      int4 NOT NULL,       -- rowid of m_group table
    access       int2 NOT NULL,       -- 0 : no right, 1 : read only, 2 : read / write

    FOREIGN KEY (team_id)  REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (doc_id) REFERENCES document (rowid),
    unique      (doc_id, team_id)
);
--
-- Authorize for groups
--
create table doc_authz_profile (
    rowid        serial UNIQUE NOT NULL,
    doc_id       int4 NOT NULL,       -- rowid of document
    profile_id   int4 NOT NULL,     -- rowid of m_profile table or NULL
    access       int2 NOT NULL,       -- 0 : no right, 1 : read only, 2 : read / write

    FOREIGN KEY (profile_id) REFERENCES m_profile (rowid),
    FOREIGN KEY (doc_id) REFERENCES document (rowid),
    unique      (doc_id, profile_id)
);

create index doc_authz_profile_profile_index on doc_authz_profile (profile_id);

-- Does the opposite of getChildDocIds
-- Given a document rowid, returns the document_id from which the rights have to be obtained (considering document tree and inheritance)
-- If document is not in a tree, just returns the document rowid.
CREATE OR REPLACE FUNCTION getRefDocId (INTEGER) RETURNS INTEGER AS '
	DECLARE
		docId ALIAS FOR $1;
		myrecord RECORD;
	
	BEGIN
		SELECT INTO myrecord COALESCE (T2.ref_doc_id, docId) AS document_id FROM document LEFT JOIN (WITH RECURSIVE tree (document_id, inherits_from, stop_inheritance) AS (SELECT document_id, inherits_from, stop_inheritance FROM document_tree WHERE document_id = docId UNION ALL SELECT document_tree.document_id, document_tree.inherits_from, document_tree.stop_inheritance FROM tree, document_tree WHERE tree.document_id != tree.inherits_from AND tree.stop_inheritance IS FALSE AND document_tree.document_id = tree.inherits_from) SELECT docId AS document_id, document_id AS ref_doc_id FROM tree WHERE stop_inheritance IS TRUE OR document_id IN (SELECT doc_id FROM doc_authz_profile)) AS T2 ON T2.document_id = document.rowid WHERE document.rowid = docId LIMIT 1;

	IF FOUND THEN
		return (myrecord.document_id);
	END IF;

	return docId;

END;
' LANGUAGE 'plpgsql';

-- Does the opposite of getRefDocId
-- Given a document rowid, returns the document_id of all documents "below" in the tree.
-- If document is not in a tree, just returns the document rowid.
CREATE OR REPLACE FUNCTION getChildDocIds (INTEGER) RETURNS SETOF INTEGER AS '
	DECLARE
		docId ALIAS FOR $1;
		r INTEGER;

	BEGIN
		FOR r IN WITH RECURSIVE tree (document_id, inherits_from, stop_inheritance) AS (SELECT document_id, inherits_from, stop_inheritance FROM document_tree WHERE document_id = docId UNION ALL SELECT document_tree.document_id, document_tree.inherits_from, document_tree.stop_inheritance FROM tree, document_tree WHERE document_tree.stop_inheritance IS FALSE AND tree.document_id = document_tree.inherits_from AND document_tree.document_id != document_tree.inherits_from) SELECT document_id FROM tree UNION SELECT docId LOOP
			return next r;
		END LOOP;
	END
' LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION authzTestAccessForDocument(INTEGER, INTEGER) RETURNS INTEGER AS '
	DECLARE 
		docToCheck ALIAS FOR $1;
		userId ALIAS FOR $2;
		myrecord RECORD;

	BEGIN
 
	SELECT INTO myrecord doc_id, max(accesskey) AS accesskey FROM (

    	-- Search authorization for user.
		--
		SELECT document.rowid AS doc_id, (50 + doc_authz_user.access) AS accesskey FROM document, doc_authz_user
		WHERE doc_authz_user.doc_id = getRefDocId (docToCheck)
		  AND doc_authz_user.user_id = userId
	UNION
    	-- Search authorization for team.
		--
		SELECT document.rowid AS doc_id, (40 + doc_authz_team.access) AS accesskey FROM document, doc_authz_team, m_group_group
		WHERE doc_authz_team.doc_id = getRefDocId (docToCheck)
		  AND m_group_group.group_id = doc_authz_team.team_id
		  AND m_group_group.invited_group_id = userId

	UNION

		-- Search authorization for profile with direct user
		--
		SELECT document.rowid AS doc_id, (30 + doc_authz_profile.access) AS accesskey FROM document, doc_authz_profile, m_profile_group
		WHERE doc_authz_profile.doc_id = getRefDocId (docToCheck)
		  AND m_profile_group.profile_id = doc_authz_profile.profile_id
		  AND m_profile_group.group_id = userId

	UNION

		-- Search authorization for profile for team
		--
		SELECT document.rowid AS doc_id, (20 + doc_authz_profile.access) AS accesskey FROM document, doc_authz_profile, m_profile_expanded_user
		WHERE doc_authz_profile.doc_id = getRefDocId (docToCheck)
		  AND m_profile_expanded_user.profile_id = doc_authz_profile.profile_id
		  AND m_profile_expanded_user.user_id = userId

	UNION

		SELECT document.rowid AS doc_id, 10 AS accesskey FROM document WHERE document.rowid = getRefDocId (docToCheck)
	) AS tmp_req
	GROUP BY (doc_id);

	IF FOUND THEN
		return (myrecord.accesskey % 10);
	END IF;

	return NULL;

END;
' LANGUAGE 'plpgsql';


-- ##################################################################
-- poll tables
-- ##################################################################

create table poll (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp not null,
    modified    timestamp not null,
    title       varchar(128) not null,
    description text,
    group_id    int not null,
    visible_for_all boolean default 't',
    anonymous   boolean default 'f',
    opening     timestamp,
    closing     varchar(64) not null,
    
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

create table poll_question (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp not null,
    modified    timestamp not null,
    poll_id     int not null,
    type        varchar(128) not null,
    label       text not null,
    
    FOREIGN KEY	(poll_id) REFERENCES poll (rowid)
);

create table poll_choice (
	rowid	    serial UNIQUE NOT NULL,
	created	    timestamp not null,
	modified	timestamp not null,
	question_id int not null,
    label       varchar(128) not null,
    
    FOREIGN KEY	(question_id) REFERENCES poll_question (rowid)
);

create table poll_user_answer (
	rowid		serial UNIQUE NOT NULL,
	created		timestamp not null,
	modified	timestamp not null,
    user_id     int not null,
    poll_id     int not null,
    validated   boolean default 'f',
    
    FOREIGN KEY	(user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY	(poll_id) REFERENCES poll (rowid)
);

create table poll_answer (
	rowid		serial UNIQUE NOT NULL,
	created		timestamp not null,
	modified	timestamp not null,
	ua_id       int not null,
    question_id int not null,
    text        text,
    
    FOREIGN KEY	(ua_id) REFERENCES poll_user_answer (rowid),
    FOREIGN KEY	(question_id) REFERENCES poll_question (rowid)
);

create table poll_answer_choice (
	choice_id    int not null,
    answer_id    int not null,
    
    FOREIGN KEY	(choice_id) REFERENCES poll_choice (rowid),
    FOREIGN KEY	(answer_id) REFERENCES poll_answer (rowid)
);

--
-- Planning application tables
--

create table planning_pref (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp not null,
    modified     timestamp not null,
    group_id     int4 NOT NULL,
    show_we      boolean default 'f',
    view_type    varchar(10) default 'week',
    show_global  boolean default 't',
    app          VARCHAR(128) DEFAULT '/Planning/ViewTasks',
    use_order    BOOLEAN DEFAULT 'f',

    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

create table planning_user_order (
    rowid        serial UNIQUE NOT NULL,
    planning_id  int NOT NULL,
    user_id      int,

    FOREIGN KEY (planning_id) REFERENCES planning_pref (rowid),
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);

--
-- News application tables
--

create table article_and_news_status (
    rowid       serial UNIQUE NOT NULL,
    label       varchar(32),
    created     timestamp not null,
    modified    timestamp not null
);

create table news (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp NOT NULL,
    modified     timestamp NOT NULL,
	published    timestamp,
    group_id     int4 NOT NULL,
    owner        int NOT NULL,
    icon_id      int4,
    title        varchar(128),
    text         text,
    status_id    int,
    moderator_id int,
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (owner) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (status_id) REFERENCES article_and_news_status (rowid),
    FOREIGN KEY (moderator_id) REFERENCES m_group_rowids (rowid)
);

create table news_pref (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp not null,
    modified     timestamp not null,
    group_id     int4 NOT NULL,
    delay        int default 90,
    news_per_page int default 10,
    moderated    boolean default 'f',
    moderator_mail boolean default 't',
    user_mail    boolean default 't',
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

create table news_icons (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp not null,
    modified     timestamp not null,
    group_id     int4 NOT NULL,
    ident        varchar(64) UNIQUE NOT NULL,
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

create table news_moderation (
    old_id  int not null,
    new_id  int not null,
    FOREIGN KEY (old_id) REFERENCES news (rowid),
    FOREIGN KEY (new_id) REFERENCES news (rowid)
);

--
-- articles table creation
--

create table article (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp not null,
    modified    timestamp not null,
	published	timestamp,
    title       varchar(128) not null,
    header      text,
    contents    text,
    status_id   int,
    group_id    int,
    owner_id    int,
    moderator_id    int,
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (owner_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (status_id) REFERENCES article_and_news_status (rowid),
    FOREIGN KEY (moderator_id) REFERENCES m_group_rowids (rowid)
);

create table article_pref (
    rowid       serial UNIQUE NOT NULL,
    group_id    int,
    created     timestamp not null,
    modified    timestamp not null,
    public_access   boolean default 'f',
    extended_ui     boolean default 't',
    moderated       boolean default 'f',
    moderator_mail  boolean default 't',
    user_mail   boolean default 't',
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

create table article_category_list (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp not null,
    modified    timestamp not null,
    label       varchar(64) not null,
    article_pref_id int,
    description text,
    FOREIGN KEY (article_pref_id) REFERENCES article_pref (rowid)
);

create table article_category (
    category_id int,
    article_id  int,
    FOREIGN KEY (category_id) REFERENCES article_category_list (rowid),
    FOREIGN KEY (article_id) REFERENCES article (rowid)
);

create table article_moderation (
    old_id  int not null,
    new_id  int not null,
    FOREIGN KEY (old_id) REFERENCES article (rowid),
    FOREIGN KEY (new_id) REFERENCES article (rowid)
);

CREATE TABLE article_comment (
    ROWID           serial UNIQUE NOT NULL,
    created         timestamp DEFAULT NOW(),
    modified        timestamp DEFAULT NOW(),
    comment         text NOT NULL,
    subject         text NOT NULL,
    user_id         integer NOT NULL,
    article_id      integer NOT NULL,
    parent_id       integer,
                               
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (parent_id) REFERENCES article_comment (rowid),
    FOREIGN KEY (article_id) REFERENCES article (rowid)
);

--
-- mailing list table creation
--
create table mailinglist (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp NOT NULL,
    modified    timestamp NOT NULL,
    group_id    int4,
    mail_from   text,
    subject     text,
    date        timestamp,
    content     oid,

    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

--
-- Faq application tables
--

create table faq (
    rowid        serial UNIQUE,
    created      timestamp not null,
    modified     timestamp not null,
    title        varchar(128),
    question     text,            -- question's text
    answer       text,            -- answer's text
    group_id     int4,            -- faq owner
    category_id  int4,            -- category of this faq
    position     int4,            -- position of this entry with regards to the others
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);
--
-- Faq categories table
--

create table faq_categories (
    rowid        serial UNIQUE,
    created      timestamp not null,
    modified     timestamp not null,
    title        varchar(128),    -- title of the category
    parent_category int4,         -- parent category
    group_id     int4,            -- faq owner
    position     int4,            -- position of this entry with regards to the others
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);


-- ==========================================
-- Forum application tables
-- ==========================================
--
-- Main table
--
create table forum (
    rowid        serial UNIQUE,
    created      timestamp not null,
    modified     timestamp not null,
    group_id     int4,                          -- id of forum's group
	simple_ihm   boolean default 't',           -- true for simple IHM for HTML area, false for complex.
	moderated   boolean default 'f',            -- true for moderated forum
	subscription   boolean default 'f',         -- true if subscription possible
	mail_alert   boolean default 'f',           -- true for sending alert by mail

    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);
--
-- Category table
--
create table forum_category (
    rowid        serial UNIQUE,
    created      timestamp not null,
    modified     timestamp not null,
    label        varchar(128),
    forum_id     int4,

    FOREIGN KEY (forum_id) REFERENCES forum (rowid)
);

create table forum_thematic (
    rowid        serial UNIQUE,
    created      timestamp not null,
    modified     timestamp not null,
    label        varchar(128),
    description  text,                          -- description of the thematic
    category_id  int4,

    FOREIGN KEY (category_id) REFERENCES forum_category (rowid)
);

create table forum_subject (
    rowid        serial UNIQUE,
    created      timestamp not null,
    modified     timestamp not null,
    subject      varchar(128),
    thematic_id  int4,
	open         boolean default 't',
	post_it      boolean default 'f',

    FOREIGN KEY (thematic_id) REFERENCES forum_thematic (rowid)
);

create table forum_message (
    rowid        serial UNIQUE,
    created      timestamp not null,
    modified     timestamp not null,
    author_id    int4,
    modifier_id  int4,                -- default to author_id
    subject_id   int4,
    message      text,

    FOREIGN KEY (author_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (modifier_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (subject_id) REFERENCES forum_subject (rowid)
);

create table forum_user (
    forum_id     int4,
    user_id     int4,
	subscribe   boolean default 'f',         -- true for mail subscription

    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (forum_id) REFERENCES forum (rowid)
);

create table forum_user2subject (
    user_id     int4,
    subject_id   int4,
    last_visit   timestamp not null,
    send_mail   boolean default 'f',         -- true if a mail was send

    unique (user_id, subject_id),
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (subject_id) REFERENCES forum_subject (rowid)
);

CREATE OR REPLACE FUNCTION thm_count_subjects(integer) RETURNS INTEGER AS '
DECLARE
        thm_id ALIAS FOR $1;
        mycount INTEGER;
BEGIN
        SELECT INTO mycount count(forum_subject.rowid) FROM  forum_thematic LEFT JOIN forum_subject ON forum_thematic.rowid = forum_subject.thematic_id WHERE forum_thematic.rowid = thm_id GROUP BY forum_thematic.rowid;
        RETURN mycount;
END;
' LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION thm_count_messages(integer) RETURNS INTEGER AS '
DECLARE
        thm_id ALIAS FOR $1;
        mycount INTEGER;
BEGIN
        SELECT INTO mycount count(forum_message.rowid) FROM  forum_thematic LEFT JOIN forum_subject ON forum_thematic.rowid = forum_subject.thematic_id LEFT JOIN forum_message ON forum_subject.rowid = forum_message.subject_id WHERE forum_thematic.rowid = thm_id GROUP BY forum_thematic.rowid
;
        RETURN mycount;
END;
' LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION thm_lastpost(integer) RETURNS INTEGER AS '
DECLARE
        thm_id ALIAS FOR $1;
        myid INTEGER;
BEGIN
        SELECT INTO myid forum_message.rowid FROM  forum_thematic LEFT JOIN forum_subject ON forum_thematic.rowid = forum_subject.thematic_id LEFT JOIN forum_message ON forum_subject.rowid = forum_message.subject_id WHERE forum_thematic.rowid = thm_id ORDER BY forum_message.modified DESC LIMIT 1 ;
        RETURN myid;
END;
' LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION subject_count_messages(integer) RETURNS INTEGER AS '
DECLARE
        subj_id ALIAS FOR $1;
        mycount INTEGER;
BEGIN
        SELECT INTO mycount count(forum_message.rowid) FROM  forum_subject LEFT JOIN forum_message ON forum_subject.rowid = forum_message.subject_id WHERE forum_subject.rowid = subj_id GROUP BY forum_subject.rowid;
        RETURN mycount;
END;
' LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION subject_lastpost(integer) RETURNS INTEGER AS '
DECLARE
        subj_id ALIAS FOR $1;
        myid INTEGER;
BEGIN
        SELECT INTO myid  forum_message.rowid FROM forum_subject LEFT JOIN forum_message ON forum_subject.rowid = forum_message.subject_id WHERE forum_subject.rowid = subj_id ORDER BY forum_message.modified DESC LIMIT 1 ;
        RETURN myid;
END;
' LANGUAGE 'plpgsql';


--
-- Crawler/Search Engine
--

--
-- Portal
--

CREATE TABLE portal_mioglet (
    rowid            serial       UNIQUE NOT NULL,
    created          timestamp    NOT NULL,
    modified         timestamp    NOT NULL,
    ident            varchar(128) NOT NULL,
    package          varchar(128) NOT NULL,
    xml_prefix       varchar(128) NOT NULL,
    app_id           int4,

    UNIQUE (app_id, ident),
    UNIQUE (app_id, package),
    UNIQUE (app_id, xml_prefix),

    FOREIGN KEY (app_id) REFERENCES m_application (rowid)
);

CREATE TABLE portal_mioglet_method (
    rowid            serial       UNIQUE NOT NULL,
    created          timestamp    NOT NULL,
    modified         timestamp    NOT NULL,
    mioglet_id       int4         NOT NULL,
    ident            varchar(128) NOT NULL,
    function_id      int4,

    FOREIGN KEY (function_id) REFERENCES m_function (rowid),
    FOREIGN KEY (mioglet_id)  REFERENCES portal_mioglet (rowid),
    FOREIGN KEY (mioglet_id)  REFERENCES portal_mioglet (rowid),

    UNIQUE      (mioglet_id, ident)
);


-- ============================================================================
-- DAV properties table
-- ============================================================================

create table m_dav_property (
    rowid               serial UNIQUE NOT NULL,
    property            varchar(128),
    enabled             bool,
    mioga_id            int NOT NULL,
    
    FOREIGN KEY (mioga_id) REFERENCES m_mioga(ROWID)
);

-- ============================================================================
-- Add tools : Function to simplify sort in simplelargelist
-- ============================================================================
CREATE OR REPLACE FUNCTION lower(boolean) RETURNS boolean
AS
'
BEGIN
RETURN $1;
END;
'
LANGUAGE 'plpgsql';

-- ============================================================================
-- Search engine
-- ============================================================================

---
--- search_doc : documents indexed in Mioga search system
---
create table search_doc (
	rowid           serial UNIQUE NOT NULL,
	created         timestamp not null,
	modified        timestamp not null,
	indexed         timestamp,
	app_code        int4,
	app_id          int4,       -- rowid for app record table
	xapian_docid    int4,
	fl_error        boolean DEFAULT 'f',
	fl_deleted      boolean DEFAULT 'f',
	mioga_id        int4,

	unique (app_code, app_id, mioga_id)
);
---
--- search_file : special table for files to add fl_index flag
---
create table search_file (
	rowid           serial UNIQUE NOT NULL,
	uri_id          int4,
	mioga_id        int4,
	fl_index        boolean DEFAULT 't',
	gallery_id      int4 DEFAULT NULL
);

-- ============================================================================
-- Mermoz tables
-- ============================================================================

CREATE TABLE mermoz_signature (
  ROWID     serial UNIQUE NOT NULL,
  user_id   INTEGER NOT NULL,
  created   timestamp DEFAULT now(),
  modified  timestamp DEFAULT now(),
  signature text,
  enabled   boolean DEFAULT false,
  
  FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);

-- ============================================================================
-- RSS tables
-- ============================================================================

CREATE TABLE rss_subscription (
    ROWID           serial UNIQUE NOT NULL,
    created         timestamp DEFAULT NOW(),
    modified        timestamp DEFAULT NOW(),
    user_id         integer NOT NULL,
    group_id        integer NOT NULL,
    application_id  integer NOT NULL,
                               
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (application_id) REFERENCES m_application (rowid)
);

CREATE TABLE rss_option (
    ROWID           serial UNIQUE NOT NULL,
    created         timestamp DEFAULT NOW(),
    modified        timestamp DEFAULT NOW(),
    user_id         integer NOT NULL,
    feed_per_group  boolean default false,
                               
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);

-- ============================================================================
-- Magellan tables
-- ============================================================================

CREATE TABLE file_comment (
    ROWID           serial UNIQUE NOT NULL,
    created         timestamp DEFAULT NOW(),
    modified        timestamp DEFAULT NOW(),
    comment         text,
    user_id         integer NOT NULL,
    uri_id          integer NOT NULL,
                               
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (uri_id) REFERENCES m_uri (rowid)
);

-- ============================================================================
-- Password assistance table
-- ============================================================================

CREATE table m_passwd_reinit (
	hash            varchar(32) UNIQUE NOT NULL,
	rowid           int4 UNIQUE NOT NULL,
	expiration_date timestamp NOT NULL
);

-- ============================================================================
-- User's secret question
-- ============================================================================
CREATE TABLE m_secret_question (
	rowid     serial UNIQUE,
	question  varchar(1024),
	answer    varchar(1024)
);

CREATE TABLE m_user2question (
    user_id       int4,
	question_id   int4
);

-- ============================================================================
-- Louvre
-- ============================================================================
CREATE TABLE m_louvre (
	rowid     serial UNIQUE,
	uri_id    integer UNIQUE NOT NULL,
	max_tn_width  integer NOT NULL,
	max_tn_height integer NOT NULL,
	locale    varchar(5) NOT NULL,
	show_filename boolean DEFAULT TRUE,
	show_description boolean DEFAULT TRUE,
	show_tags boolean DEFAULT TRUE,
	show_dimensions boolean DEFAULT TRUE,
	show_weight boolean DEFAULT TRUE,
	popup_display boolean DEFAULT TRUE,
	
	FOREIGN KEY (uri_id)   REFERENCES m_uri (rowid) ON DELETE CASCADE
);
-- ============================================================================
-- Captcha
-- ============================================================================
CREATE TABLE m_captcha (
	hash      varchar(32) UNIQUE NOT NULL,
	captcha   varchar(16) NOT NULL,
	base64    text,
	created   timestamp NOT NULL
);

-- ============================================================================
-- HTML authentication sessions
-- ============================================================================
CREATE TABLE m_auth_session (
	token     varchar(32) UNIQUE NOT NULL,
	email     varchar(128),
	csrftoken varchar(32) UNIQUE NOT NULL,
	created   timestamp NOT NULL,
	modified  timestamp NOT NULL
);

-- ============================================================================
-- Reporting
-- ============================================================================
CREATE VIEW view_user_groups AS
	SELECT
		m_user_base.rowid,
		m_user_base.firstname,
		m_user_base.lastname,
		m_user_base.email,
		T2.group_id,
		T2.group_ident,
		T1.group_id AS team_id,
		T1.ident AS team_ident,
		T2.profile_id,
		T2.profile_ident,
		m_user_base.mioga_id
	FROM
		m_user_base,
		(SELECT
			m_user_base.rowid,
			m_group_group.group_id,
			m_group.ident
		FROM
			m_user_base,
			m_group_group,
			m_group
		WHERE
			m_user_base.rowid = m_group_group.invited_group_id AND
			m_group_group.group_id = m_group.rowid AND
			m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'team')
		) AS T1,
		(SELECT
			m_profile_group.group_id AS invited_group_id,
			m_group.rowid AS group_id,
			m_group.ident AS group_ident,
			m_profile.rowid AS profile_id,
			m_profile.ident AS profile_ident
		FROM
			m_profile_group,
			m_profile,
			m_group
		WHERE
			m_profile_group.profile_id = m_profile.rowid AND
			m_profile.group_id = m_group.rowid AND
			m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'group')
		) AS T2
	WHERE
		m_user_base.rowid = T1.rowid AND
		T1.group_id = T2.invited_group_id
	UNION ALL SELECT m_user_base.rowid AS user_id,
		m_user_base.firstname,
		m_user_base.lastname,
		m_user_base.email,
		T1.rowid AS group_id,
		T1.ident AS group_ident,
		NULL AS team_id,
		NULL AS team_ident,
		T2.profile_id,
		T2.ident AS profile_ident,
		m_user_base.mioga_id
	FROM
		m_user_base
	LEFT JOIN (
		SELECT
			m_group.rowid,
			m_group.ident,
			m_group.mioga_id
		FROM
			m_group,
			m_group_type
		WHERE
			m_group.type_id = m_group_type.rowid AND
			m_group_type.ident = 'group'
		) AS T1
		ON
			T1.mioga_id = m_user_base.mioga_id
	LEFT JOIN (
		SELECT
			m_user_base.rowid,
			m_profile.rowid AS profile_id,
			m_profile.ident,
			m_profile.group_id
		FROM
			m_user_base,
			m_profile_group,
			m_profile
		WHERE
			m_user_base.rowid = m_profile_group.group_id AND
			m_profile_group.profile_id = m_profile.rowid
		) AS T2
		ON
			T2.rowid = m_user_base.rowid AND
			T2.group_id = T1.rowid
	ORDER BY lastname, rowid, group_ident, group_id;

CREATE VIEW view_user_teams AS
	SELECT
		m_user_base.rowid,
		m_user_base.firstname,
		m_user_base.lastname,
		m_user_base.email,
		T1.rowid AS team_id,
		T1.ident AS team_ident,
		T2.is_member,
		m_user_base.mioga_id
	FROM
		m_user_base
	LEFT JOIN (
		SELECT
			m_group.rowid,
			m_group.ident,
			m_group.mioga_id
		FROM
			m_group,
			m_group_type
		WHERE
			m_group.type_id = m_group_type.rowid AND
			m_group_type.ident = 'team'
		) AS T1
	ON
		m_user_base.mioga_id = T1.mioga_id
	LEFT JOIN (
		SELECT
			m_group_group.group_id,
			m_group_group.invited_group_id,
			1 AS is_member
		FROM
			m_group_group
		) AS T2
	ON
		T2.invited_group_id = m_user_base.rowid AND
		T2.group_id = T1.rowid
	ORDER BY
		lastname, rowid, team_ident, team_id;

CREATE VIEW view_team_groups AS
	SELECT
		T1.rowid,
		T1.ident,
		T2.rowid AS group_id,
		T2.ident AS group_ident,
		T3.rowid AS profile_id,
		T3.ident AS profile_ident,
		T1.mioga_id
	FROM (
		SELECT
			m_group.rowid,
			m_group.ident,
			m_group.mioga_id
		FROM
			m_group,
			m_group_type
		WHERE
			m_group.type_id = m_group_type.rowid AND
			m_group_type.ident = 'team'
	) AS T1
	LEFT JOIN
		m_group
	AS T2
	ON
		T1.mioga_id = T2.mioga_id
	LEFT JOIN (
		SELECT
			m_profile_group.group_id AS team_id,
			m_profile.rowid,
			m_profile.group_id,
			m_profile.ident
		FROM
			m_profile_group,
			m_profile
		WHERE
			m_profile_group.profile_id = m_profile.rowid
	) AS T3
	ON
		T3.team_id = T1.rowid AND
		T3.group_id = T2.rowid
	WHERE T2.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'group')
	ORDER BY
		ident, rowid, group_ident, group_id;


-- ============================================================================
-- Tagging system
-- ============================================================================
CREATE TABLE m_tag_catalog (
	rowid            serial UNIQUE NOT NULL,
	ident            varchar(128) NOT NULL,
	mioga_id         int4,
	created          timestamp NOT NULL,
	modified         timestamp NOT NULL,

	FOREIGN KEY (mioga_id) REFERENCES m_mioga (rowid)
);
CREATE UNIQUE INDEX m_tag_catalog_mioga_index ON m_tag_catalog (ident, mioga_id);

CREATE TABLE m_tag (
	rowid            serial UNIQUE NOT NULL,
	tag              varchar(128) NOT NULL CHECK (length(tag) > 0),
	catalog_id       int4,

	FOREIGN KEY (catalog_id) REFERENCES m_tag_catalog (rowid)
);
CREATE UNIQUE INDEX m_tag_mioga_index ON m_tag (tag, catalog_id);

CREATE TABLE uri_data (
	uri_id           int4 NOT NULL,
	tags             text,
	description      text,

	FOREIGN KEY (uri_id) REFERENCES m_uri (rowid)
);

-- ============================================================================
-- Log system
-- ============================================================================
CREATE TABLE m_log (
	date           timestamp NOT NULL,
	instance       varchar(128),
	user_ident     varchar(128),
	method         varchar(64),
	status         varchar(5),
	uri            text,
	args           text
);


-- ============================================================================
-- Short URL
-- ============================================================================
CREATE TABLE short_url (
	rowid          SERIAL UNIQUE NOT NULL,
	hash           varchar(32),
	target         text
);


-- ============================================================================
-- Diderot tables
-- ============================================================================
CREATE TABLE notice_template (
	rowid			serial UNIQUE NOT NULL,
	created			timestamp NOT NULL,
	modified		timestamp NOT NULL,
	title			varchar(128) NOT NULL,
	description		text,
	fields			text,
	field_order		text,
	group_id		int4 NOT NULL,
	document_id		int4,	-- Ideally should be NOT NULL but will fail on initial creation

	FOREIGN KEY (group_id) REFERENCES m_group(rowid),
	FOREIGN KEY (document_id) REFERENCES document(rowid)
);

CREATE TABLE notice_category (
	rowid			serial UNIQUE NOT NULL,
	ident			varchar(128) NOT NULL,
	parent_id		int4 NOT NULL,
	group_id		int4 NOT NULL,
	created			timestamp NOT NULL,
	modified		timestamp NOT NULL,
	document_id		int4 NOT NULL,

	FOREIGN KEY (parent_id) REFERENCES notice_category(rowid),
	FOREIGN KEY (group_id) REFERENCES m_group(rowid),
	FOREIGN KEY (document_id) REFERENCES document(rowid)
);

CREATE TABLE notice (
	rowid			serial UNIQUE NOT NULL,
	created			timestamp NOT NULL,
	modified		timestamp NOT NULL,
	category_id		int4,
	group_id		int4 NOT NULL,
	document_id		int4 NOT NULL,
	user_id         int4 NOT NULL,

	FOREIGN KEY (group_id) REFERENCES m_group(rowid),
	FOREIGN KEY (category_id) REFERENCES notice_category(rowid),
	FOREIGN KEY (document_id) REFERENCES document(rowid),
	FOREIGN KEY (user_id) REFERENCES m_group_rowids(rowid)
);

CREATE TABLE notice_field (
	ident			varchar(128) NOT NULL,
	value			text,
	notice_id		int4 NOT NULL,

	FOREIGN KEY (notice_id) REFERENCES notice(rowid)
);
CREATE UNIQUE INDEX notice_ident_idx ON notice_field(ident, notice_id);

CREATE TABLE notice_files (
	notice_id		int4 NOT NULL,
	uri_id			int4 NOT NULL,

	FOREIGN KEY (notice_id) REFERENCES notice(rowid),
	FOREIGN KEY (uri_id) REFERENCES m_uri(rowid)
);


-----------------------
-- Chronos tables
-----------------------
create table chronos_group_pref (
	group_id         int4,
	currentCalId     int4 DEFAULT 0,  -- id of current calendar. If 0, must be reinitialized
    currentView      text DEFAULT 'agendaWeek',
    firsthour        int4 DEFAULT 7,
	colorscheme      int4 DEFAULT 1,    -- 1 (activity = background, agenda = border ), 2 (activity = border, agenda = background)
	showWeekend      boolean default 't',

	FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

create table chronos_user_pref (
	user_id          int4,
	currentCalId     int4 DEFAULT 0,  -- id of current calendar. If 0, must be reinitialized
    currentView      text DEFAULT 'agendaWeek',
    firsthour        int4 DEFAULT 7,
	colorscheme      int4 DEFAULT 1,    -- 1 (activity = background, agenda = border ), 2 (activity = border, agenda = background)
	showWeekend      boolean default 't',

	FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);


create table calendar (
    rowid            serial UNIQUE NOT NULL,
    created          timestamp NOT NULL,
    modified         timestamp NOT NULL,
	owner_type       int4 NOT NULL,     -- 1 : user, 2 : group, 3 : resource
	document_id      int4,
	private          boolean DEFAULT 'f',

	FOREIGN KEY (document_id) REFERENCES document (rowid)
);

create table group2cal (
	group_id       int4,
	calendar_id    int4,
    ident          text NOT NULL,
    color          char(7) DEFAULT '#000000',
    bgcolor        char(7) DEFAULT '#ffffff',


	FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
	FOREIGN KEY (calendar_id) REFERENCES calendar (rowid)
);
create unique index group2cal_group_calendar_index on group2cal (group_id, calendar_id);

create table user2cal (
	user_id          int4,
	calendar_id      int4,
	type             int4 DEFAULT NULL,  -- 0 : owner, 1 : other user, 2 : group calendar
    ident            text NOT NULL,
    color            char(7) DEFAULT '#000000',
    bgcolor          char(7) DEFAULT '#ffffff',
	visible          boolean DEFAULT true,

	FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
	FOREIGN KEY (calendar_id) REFERENCES calendar (rowid)
);
create unique index user2cal_user_calendar_index on user2cal (user_id, calendar_id);

create table cal_event (
    rowid            serial UNIQUE NOT NULL,
    created          timestamp NOT NULL,
    modified         timestamp NOT NULL,
	user_id          int4,    -- ID of last user who modified task
	calendar_id      int4,
    subject          text,
    description      text,
	dstart           timestamp,
	dend             timestamp,
	allDay           boolean DEFAULT false,
	fl_recur         boolean DEFAULT false,
	rrule            text,
	category_id      int4,

	FOREIGN KEY (category_id) REFERENCES task_category (rowid),
	FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
	FOREIGN KEY (calendar_id) REFERENCES calendar (rowid)
);

CREATE VIEW groupcalendar AS
	SELECT group2cal.*, calendar.* FROM group2cal, calendar WHERE calendar.rowid=group2cal.calendar_id;

CREATE VIEW usercalendar AS
	SELECT user2cal.*, calendar.* FROM user2cal, calendar WHERE calendar.rowid=user2cal.calendar_id;

CREATE OR REPLACE FUNCTION testAccessOnCalendar(INTEGER, INTEGER) RETURNS INTEGER AS '
        DECLARE 
                c_id ALIAS FOR $1;
                u_id ALIAS FOR $2;
                myrecord RECORD;

        BEGIN
 
        SELECT INTO myrecord max(access) as access FROM (

                select authzTestAccessForDocument(document_id, u_id) as access FROM calendar WHERE calendar.rowid = c_id
        union
                SELECT 1 as access FROM (SELECT * FROM m_profile_user_function WHERE app_ident=''Chronos'' AND user_id=u_id UNION SELECT * FROM m_profile_expanded_user_functio WHERE app_ident=''Chronos'' AND user_id=u_id) AS t1  LEFT JOIN group2cal ON group2cal.group_id = t1.group_id, m_group WHERE t1.group_id = m_group.rowid AND calendar_id= c_id AND function_ident = ''Read''
        union
                SELECT 2 as access FROM (SELECT * FROM m_profile_user_function WHERE app_ident=''Chronos'' AND user_id=u_id UNION SELECT * FROM m_profile_expanded_user_functio WHERE app_ident=''Chronos'' AND user_id=u_id) AS t1  LEFT JOIN group2cal ON group2cal.group_id = t1.group_id, m_group WHERE t1.group_id = m_group.rowid AND calendar_id= c_id AND function_ident = ''Write''


        ) AS tmp_req;

        IF FOUND THEN
                return myrecord.access;
        END IF;

        return NULL;

END;
' LANGUAGE 'plpgsql';

-----------------------
-- Haussmann tables
-----------------------

CREATE TABLE project_model (
    rowid        serial UNIQUE NOT NULL,
 	group_id     int4 NOT NULL,
    created      timestamp DEFAULT NOW(),
    modified     timestamp DEFAULT NOW(),
	label        varchar(64) NOT NULL,
	model        text NOT NULL,
	FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

CREATE TABLE project (
    rowid        serial UNIQUE NOT NULL,
 	group_id     int4 NOT NULL,
 	owner_id     int4 NOT NULL,
    created      timestamp DEFAULT NOW(),
    modified     timestamp DEFAULT NOW(),
    label        varchar(150),
    use_result   bool DEFAULT TRUE,
    description  text,
	status       text,
    current_status int4,
    base_dir     text,
    tags         text,
    started      date NOT NULL,
    planned      date NOT NULL,
    ended        date,
    complementary_data   text,
    task_tree   text,
    model_id     int4,
    color          char(7) DEFAULT '#b7b7ef',
    bgcolor        char(7) DEFAULT '#ffffff',
    advanced_params text,
	FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
	FOREIGN KEY (owner_id) REFERENCES m_user_base (rowid),
	FOREIGN KEY (model_id) REFERENCES project_model (rowid)
);

CREATE TABLE project_task (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp DEFAULT NOW(),
    modified     timestamp DEFAULT NOW(),
    project_id   int4,
    label        text,
    description  text,
    workload     int4 DEFAULT 0,
    progress     text,
    current_progress numeric(5,2) DEFAULT 0.00,
	pstart       date,
    pend         date,
    status       text,
    current_status int4,
    tags         text,
    fineblanking text,
    position_tree text,
    position_list text,
    position_result text,
   	FOREIGN KEY (project_id) REFERENCES project (rowid)
);
create table project_task_note (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp DEFAULT NOW(),
    modified     timestamp DEFAULT NOW(),
    group_id     int4 NOT NULL,
    owner_id     int NOT NULL,
    text         text,
    project_id	 int4,
    task_id		 int,
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (owner_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (project_id) REFERENCES project (rowid),
    FOREIGN KEY (task_id) REFERENCES project_task (rowid)
);

CREATE TABLE project_result (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp DEFAULT NOW(),
    modified     timestamp DEFAULT NOW(),
    project_id   int4 NOT NULL,
    label        varchar(50) NOT NULL,
    budget     	 INT8 DEFAULT 0,
    pend         date,
    tags         text,
    status       text,
    current_status int4,
    checked		BOOL DEFAULT false,
	FOREIGN KEY (project_id) REFERENCES project (rowid)
);
create table project_result_note (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp NOT NULL,
    modified     timestamp NOT NULL,
    group_id     int4 NOT NULL,
    owner_id     int NOT NULL,
    text         text,
    project_id	 int4,
    result_id	 int,
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (owner_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (project_id) REFERENCES project (rowid),
    FOREIGN KEY (result_id) REFERENCES project_result (rowid)
);

-------------------------------------
-- Relation tables
-------------------------------------

create table user2project_task (
	user_id      int4,
	task_id      int4,
	dstart       date,
	dend         date,
	show_event   boolean default false,

	FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
	FOREIGN KEY (task_id) REFERENCES project_task (rowid)
);

create table attendee2project_task (
	user_id      int4,
	task_id      int4,
	write        boolean default false,
	show_event   boolean default false,

	FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
	FOREIGN KEY (task_id) REFERENCES project_task (rowid)
);
create unique index attendee2project_task_user_task_index on attendee2project_task (user_id, task_id);

create table user2task_access (
	user_id      int4,
	task_id      int4,
	last_access  timestamp NOT NULL,

	FOREIGN KEY (user_id) REFERENCES m_user_base (rowid),
	FOREIGN KEY (task_id) REFERENCES project_task (rowid)
);
create unique index user2task_access_user_access_index on user2task_access  (user_id, task_id);

create table current_project_user2group_id (
	user_id     	 	int4,
	group_id     		int4,
	current_project_id 	int4,

	FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
	FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
	FOREIGN KEY (current_project_id) REFERENCES project (rowid)
);
create unique index current_project_user2group_id_index on current_project_user2group_id  (user_id, group_id);

CREATE VIEW project_task_view AS
SELECT project_task.rowid, project_task.created, project_task.modified, project_task.project_id, project_task.label, project_task.description, project_task.workload, project_task.progress, project_task.current_progress, project_task.pend, project_task.pstart, project_task.status, project_task.current_status, project_task.tags, project_task.fineblanking, project_task.position_tree, project_task.position_list, project_task.position_result, project.label AS project_label, project.group_id, m_group_base.ident AS group_ident, user2project_task.user_id, user2project_task.dstart, user2project_task.dend, user2project_task.show_event, project.color, project.bgcolor
   FROM project_task
   LEFT JOIN user2project_task ON user2project_task.task_id = project_task.rowid, project, m_group_base
  WHERE project.rowid = project_task.project_id AND m_group_base.rowid = project.group_id;

CREATE VIEW user_project_task_view AS
SELECT project_task.rowid, project_task.created, project_task.modified, project_task.project_id, project_task.label, project_task.description, project_task.workload, project_task.progress, project_task.current_progress, project_task.pend, project_task.pstart, project_task.status, project_task.current_status, project_task.tags, project_task.fineblanking, project_task.position_tree, project_task.position_list, project_task.position_result, project.label AS project_label, project.group_id, m_group_base.ident AS group_ident, tmp.user_id, tmp.dstart, tmp.dend, tmp.show_event, tmp.type, project.color, project.bgcolor
   FROM project_task
   LEFT JOIN (select task_id, user_id, 't' as write,'r' as type, show_event, dstart,dend from user2project_task union select attendee2project_task.task_id, attendee2project_task.user_id, write, 'a' as type, attendee2project_task.show_event, user2project_task.dstart as dstart, user2project_task.dend as dend from attendee2project_task LEFT JOIN user2project_task ON attendee2project_task.task_id=user2project_task.task_id) as tmp ON tmp.task_id = project_task.rowid, project, m_group_base
  WHERE project.rowid = project_task.project_id AND m_group_base.rowid = project.group_id;
-------------------------------------
-- Eiffel tables
-------------------------------------

create table eiffel_pref (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp NOT NULL,
    modified     timestamp NOT NULL,
	group_id     int4,
	week_count   int4,
	show_groups  boolean default 'f',
	display      text,

	FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);
create unique index eiffel_pref_group_idx on eiffel_pref (group_id);

-------------------------------------
-- Deming tables
-------------------------------------

CREATE TABLE activity_report (
	rowid         serial UNIQUE NOT NULL,
	user_id       int4 NOT NULL,
	activity_id   int4 NOT NULL,
	dtstart       timestamp NOT NULL,
	dtend         timestamp NOT NULL,
	notes         text,

	FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
	FOREIGN KEY (activity_id) REFERENCES task_category(rowid)
);

CREATE TABLE activity_report_source (
	task_category_id  int4 UNIQUE NOT NULL,
	ident             varchar(128),

	FOREIGN KEY (task_category_id) REFERENCES task_category (rowid)
);

-- ==========================================
-- Cantor application tables
-- ==========================================

--
-- Main table
--
CREATE TABLE survey (
    rowid               serial UNIQUE,

    title               varchar(128) NOT NULL,  -- a title for the survey
    description         text NOT NULL,          -- a description for the survey
    duration            int NOT NULL,           -- a duration for the survey
    open_attendee_list  boolean DEFAULT 'f',    -- a type private or public for the survey (default is false => public)
    short_url_id        int4,
    owner_id            int NOT NULL,

    created             timestamp NOT NULL,
    modified            timestamp NOT NULL,

    FOREIGN KEY (owner_id) REFERENCES m_user_base (rowid),
    FOREIGN KEY (short_url_id) REFERENCES short_url (rowid)
);

--
-- Attendee list
--
CREATE TABLE survey_attendee (
	rowid        serial UNIQUE,
	survey_id    int4 NOT NULL,
	user_id      int4,
	fullname     varchar(128),
	email        varchar(128),

	FOREIGN KEY (survey_id) REFERENCES survey (rowid),
	FOREIGN KEY (user_id) REFERENCES m_user_base (rowid)
);

--
-- Resource list
--
CREATE TABLE survey_resource (
	survey_id    int4 NOT NULL,
	resource_id  int4 NOT NULL,

	FOREIGN KEY (survey_id) REFERENCES survey (rowid),
	FOREIGN KEY (resource_id) REFERENCES m_resource (rowid)
);

--
-- Date list
--
CREATE TABLE survey_date (
	rowid        serial UNIQUE,
	survey_id    int4 NOT NULL,
	date         timestamp NOT NULL,

	FOREIGN KEY (survey_id) REFERENCES survey (rowid)
);
CREATE UNIQUE INDEX survey_date_survey_id ON survey_date (survey_id, date);

--
-- Answer list
--
CREATE TABLE survey_answer (
	attendee_id  int4 NOT NULL,
	date_id      int4 NOT NULL,

	FOREIGN KEY (attendee_id) REFERENCES survey_attendee (rowid),
	FOREIGN KEY (date_id) REFERENCES survey_date (rowid)
);

-- ============================================================================
-- Insertions
-- ============================================================================

insert into m_lang (ident, locale) values ('en_US', 'en_US');
insert into m_lang (ident, locale) values ('fr_FR', 'fr_FR');

insert into m_group_type (ident) values ('group');
insert into m_group_type (ident) values ('team');
insert into m_group_type (ident) values ('local_user');
insert into m_group_type (ident) values ('ldap_user');
insert into m_group_type (ident) values ('external_user');
insert into m_group_type (ident) values ('resource');

insert into m_user_status (ident) values ('active');
insert into m_user_status (ident) values ('disabled');
insert into m_user_status (ident) values ('deleted_not_purged');
INSERT INTO m_user_status (ident) VALUES ('disabled_attacked');

insert into m_resource_status (ident) values ('active');
insert into m_resource_status (ident) values ('disabled');

insert into m_group_status (ident) values ('active');

insert into m_external_mioga_status (ident) values ('active');
insert into m_external_mioga_status (ident) values ('disabled');

insert into m_method_type (ident) values ('normal');
insert into m_method_type (ident) values ('sxml');
insert into m_method_type (ident) values ('xml');

insert into m_application_type (ident) values ('normal');
insert into m_application_type (ident) values ('library');
insert into m_application_type (ident) values ('shell');
insert into m_application_type (ident) values ('resource');
INSERT INTO m_application_type (ident) values ('portal');

insert into article_and_news_status (created, modified, label) values (NOW(), NOW(), 'published');
insert into article_and_news_status (created, modified, label) values (NOW(), NOW(), 'waiting');
insert into article_and_news_status (created, modified, label) values (NOW(), NOW(), 'draft');

-- ============================================================================
-- AUTHORS
-- 
-- The Mioga2 Project <developers@mioga2.org>
-- 
-- Copyright (C) 2003-2007, The Mioga2 Project. All Rights Reserved.
-- 
-- This module is free software; you can redistribute it and/or
-- modify it under the terms of the GNU Public License.
-- 
-- ============================================================================
