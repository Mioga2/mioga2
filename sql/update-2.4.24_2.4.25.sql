-- pend is planned date to end result. Result is as milestone.
ALTER TABLE project_result ADD COLUMN pend date;

-- Change user_id alias name as it fails on PostgreSQL 9.x (ambiguous)
CREATE OR REPLACE FUNCTION authzTestAccessForDocument(INTEGER, INTEGER) RETURNS INTEGER AS '
	DECLARE 
		docToCheck ALIAS FOR $1;
		userId ALIAS FOR $2;
		myrecord RECORD;

	BEGIN
 
	SELECT INTO myrecord doc_id, max(accesskey) AS accesskey FROM (

    	-- Search authorization for user.
		--
		SELECT document.rowid AS doc_id, (50 + doc_authz_user.access) AS accesskey FROM document, doc_authz_user
		WHERE doc_authz_user.doc_id = getRefDocId (docToCheck)
		  AND doc_authz_user.user_id = userId
	UNION
    	-- Search authorization for team.
		--
		SELECT document.rowid AS doc_id, (40 + doc_authz_team.access) AS accesskey FROM document, doc_authz_team, m_group_group
		WHERE doc_authz_team.doc_id = getRefDocId (docToCheck)
		  AND m_group_group.group_id = doc_authz_team.team_id
		  AND m_group_group.invited_group_id = userId

	UNION

		-- Search authorization for profile with direct user
		--
		SELECT document.rowid AS doc_id, (30 + doc_authz_profile.access) AS accesskey FROM document, doc_authz_profile, m_profile_group
		WHERE doc_authz_profile.doc_id = getRefDocId (docToCheck)
		  AND m_profile_group.profile_id = doc_authz_profile.profile_id
		  AND m_profile_group.group_id = userId

	UNION

		-- Search authorization for profile for team
		--
		SELECT document.rowid AS doc_id, (20 + doc_authz_profile.access) AS accesskey FROM document, doc_authz_profile, m_profile_expanded_user
		WHERE doc_authz_profile.doc_id = getRefDocId (docToCheck)
		  AND m_profile_expanded_user.profile_id = doc_authz_profile.profile_id
		  AND m_profile_expanded_user.user_id = userId

	UNION

		SELECT document.rowid AS doc_id, 10 AS accesskey FROM document WHERE document.rowid = getRefDocId (docToCheck)
	) AS tmp_req
	GROUP BY (doc_id);

	IF FOUND THEN
		return (myrecord.accesskey % 10);
	END IF;

	return NULL;

END;
' LANGUAGE 'plpgsql';


-- [BUG #1422] HAUSSMANN - Application group but always activated as a user application

-- Remove user profile / Haussmann function association for profiles associated to user group type
-- Supprime l'association profile utilisateur / fonctions d'Haussmann pour les profiles associés aux groupes de type "utilisateur"
DELETE FROM m_profile_function WHERE
	profile_id IN (SELECT rowid FROM m_profile WHERE group_id IN (SELECT rowid FROM m_user_base)) 
	AND 
	function_id IN (SELECT rowid FROM m_function WHERE application_id = (SELECT rowid FROM m_application WHERE ident='Haussmann'));
-- Remove Haussmann access to user group type
-- supprime l'accès de Haussmann aux groupes de type "utilisateur"
DELETE FROM m_application_group_allowed WHERE 
	application_id = (SELECT rowid FROM m_application WHERE ident='Haussmann')
	AND 
	group_id IN (SELECT rowid FROM m_user_base);
-- Remove application / group association for user group type
-- Supprime les associations application / groupe pour les groupes qui sont de type "utilisateur"
DELETE FROM m_application_group WHERE 
	application_id = (SELECT rowid FROM m_application WHERE ident='Haussmann')
	AND 
	group_id IN (SELECT rowid FROM m_user_base);


