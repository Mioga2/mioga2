--
-- news table creation/modification
--

create table article_and_news_status (
    rowid       serial UNIQUE NOT NULL,
    label       varchar(32),
    created     timestamp not null,
    modified    timestamp not null
);

alter table news add status_id int;
alter table news add moderator_id int;
alter table news add FOREIGN KEY (status_id) REFERENCES article_and_news_status (rowid);
alter table news add FOREIGN KEY (moderator_id) REFERENCES m_group_rowids (rowid);

alter table news_pref add moderated boolean;
alter table news_pref add moderator_mail boolean;
alter table news_pref add user_mail boolean;

create table news_moderation (
    old_id  int not null,
    new_id  int not null,
    FOREIGN KEY (old_id) REFERENCES news (rowid),
    FOREIGN KEY (new_id) REFERENCES news (rowid)
);

--
-- articles table creation
--

create table article (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp not null,
    modified    timestamp not null,
    title       varchar(128) not null,
    chapo       text,
    contents    text,
    status_id   int,
    group_id    int,
    owner_id    int,
    moderator_id    int,
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (owner_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (status_id) REFERENCES article_and_news_status (rowid),
    FOREIGN KEY (moderator_id) REFERENCES m_group_rowids (rowid)
);

create table article_pref (
    rowid       serial UNIQUE NOT NULL,
    group_id    int,
    created     timestamp not null,
    modified    timestamp not null,
    public_access   boolean default 'f',
    extended_ui     boolean default 't',
    moderated       boolean default 'f',
    moderator_mail  boolean default 't',
    user_mail   boolean default 't',
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

create table article_category_list (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp not null,
    modified    timestamp not null,
    label       varchar(64) not null,
    article_pref_id int,
    description text,
    FOREIGN KEY (article_pref_id) REFERENCES article_pref (rowid)
);

create table article_category (
    category_id int,
    article_id  int,
    FOREIGN KEY (category_id) REFERENCES article_category_list (rowid),
    FOREIGN KEY (article_id) REFERENCES article (rowid)
);

create table article_moderation (
    old_id  int not null,
    new_id  int not null,
    FOREIGN KEY (old_id) REFERENCES article (rowid),
    FOREIGN KEY (new_id) REFERENCES article (rowid)
);

-- ==========================================
-- Forum application tables
-- ==========================================
--
-- Main table
--
drop table forum;
drop sequence forum_rowid_seq;

create table forum (
    rowid        serial UNIQUE,
    created      timestamp not null,
    modified     timestamp not null,
    group_id     int4,                          -- id of forum's group
	simple_ihm   boolean default 't',           -- true for simple IHM for HTML area, false for complex.
	moderated   boolean default 'f',            -- true for moderated forum
	subscription   boolean default 'f',         -- true if subscription possible
	mail_alert   boolean default 'f',           -- true for sending alert by mail

    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);
--
-- Category table
--
create table forum_category (
    rowid        serial UNIQUE,
    created      timestamp not null,
    modified     timestamp not null,
    label        varchar(128),
    forum_id     int4,

    FOREIGN KEY (forum_id) REFERENCES forum (rowid)
);

create table forum_thematic (
    rowid        serial UNIQUE,
    created      timestamp not null,
    modified     timestamp not null,
    label        varchar(128),
    description  text,                          -- description of the thematic
    category_id  int4,

    FOREIGN KEY (category_id) REFERENCES forum_category (rowid)
);

create table forum_subject (
    rowid        serial UNIQUE,
    created      timestamp not null,
    modified     timestamp not null,
    subject      varchar(128),
    thematic_id  int4,

    FOREIGN KEY (thematic_id) REFERENCES forum_thematic (rowid)
);

create table forum_message (
    rowid        serial UNIQUE,
    created      timestamp not null,
    modified     timestamp not null,
    author_id    int4,
    subject_id   int4,
    content      text,

    FOREIGN KEY (author_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (subject_id) REFERENCES forum_subject (rowid)
);

create table forum_user (
    forum_id     int4,
    user_id     int4,
	subscribe   boolean default 'f',         -- true for mail subscription

    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (forum_id) REFERENCES forum (rowid)
);

create table forum_user2subject (
    user_id     int4,
    subject_id   int4,
    last_visit   timestamp not null,

    unique (user_id, subject_id),
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (subject_id) REFERENCES forum_subject (rowid)
);

CREATE OR REPLACE FUNCTION thm_count_subjects(integer) RETURNS INTEGER AS '
DECLARE
        thm_id ALIAS FOR $1;
        mycount INTEGER;
BEGIN
        SELECT INTO mycount count(forum_subject.rowid) FROM  forum_thematic LEFT JOIN forum_subject ON forum_thematic.rowid = forum_subject.thematic_id WHERE forum_thematic.rowid = thm_id GROUP BY forum_thematic.rowid;
        RETURN mycount;
END;
' LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION thm_count_messages(integer) RETURNS INTEGER AS '
DECLARE
        thm_id ALIAS FOR $1;
        mycount INTEGER;
BEGIN
        SELECT INTO mycount count(forum_message.rowid) FROM  forum_thematic LEFT JOIN forum_subject ON forum_thematic.rowid = forum_subject.thematic_id LEFT JOIN forum_message ON forum_subject.rowid = forum_message.subject_id WHERE forum_thematic.rowid = thm_id GROUP BY forum_thematic.rowid
;
        RETURN mycount;
END;
' LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION thm_lastpost(integer) RETURNS INTEGER AS '
DECLARE
        thm_id ALIAS FOR $1;
        myid INTEGER;
BEGIN
        SELECT INTO myid forum_message.rowid FROM  forum_thematic LEFT JOIN forum_subject ON forum_thematic.rowid = forum_subject.thematic_id LEFT JOIN forum_message ON forum_subject.rowid = forum_message.subject_id WHERE forum_thematic.rowid = thm_id ORDER BY forum_message.modified DESC LIMIT 1 ;
        RETURN myid;
END;
' LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION subject_count_messages(integer) RETURNS INTEGER AS '
DECLARE
        subject_id ALIAS FOR $1;
        mycount INTEGER;
BEGIN
        SELECT INTO mycount count(forum_message.rowid) FROM  forum_subject LEFT JOIN forum_message ON forum_subject.rowid = forum_message.subject_id WHERE forum_subject.rowid = subject_id GROUP BY forum_subject.rowid;
        RETURN mycount;
END;
' LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION subject_lastpost(integer) RETURNS INTEGER AS '
DECLARE
        subject_id ALIAS FOR $1;
        myid INTEGER;
BEGIN
        SELECT INTO myid  forum_message.rowid FROM forum_subject LEFT JOIN forum_message ON forum_subject.rowid = forum_message.subject_id WHERE forum_subject.rowid = subject_id ORDER BY forum_message.modified DESC LIMIT 1 ;
        RETURN myid;
END;
' LANGUAGE 'plpgsql';
