#!/usr/bin/perl -w

use strict;
use lib "../lib";
use Mioga2::MiogaConf;
use Mioga2::InstanceList;
use Mioga2::XML::Simple;
use Mioga2::tools::database;
use Data::Dumper;
use Mioga2::Config;
use Error qw(:try);

$Error::Debug = 1;

my $configxml = "../conf/Config.xml";
my $miogaconf = "../web/conf/Mioga.conf";
my $timezonexml = "./timezone.xml";
my $dir = "..";
my $force_init_sql = 0;

sub LoadTimezone {
	my $config = shift;

	if(-e $timezonexml) {
		my $xs = new Mioga2::XML::Simple(forcearray => 1);
		my $xmltree = $xs->XMLin($timezonexml);
		
		foreach my $tz (@{$xmltree->{timezone}}) {
			
			ExecSQL($config->GetDBH(), 
					"INSERT INTO m_timezone (created, modified, ident) VALUES (now(), now(), '$tz')");
		}
	}
}


foreach my $var (qw(configxml miogaconf timezonexml dir force_init_sql)) {
	my @res = grep {$_ =~ /^$var=(.*)$/} @ARGV;
	
	if(@res) {
		my ($val) = ($res[0] =~ /^$var=(.*)$/);
		eval "\$$var = \$val";
	}
}

my $config = new Mioga2::MiogaConf($miogaconf);

my $conf = new MiogaConf( dir => $dir, config => $configxml, force_init_sql => $force_init_sql);
$conf->Install($config);

LoadTimezone($config);


open (F_XML, $miogaconf)
	or die "Can't open $miogaconf : $!";

my $xml;

{
	local $/ = undef;
	$xml = <F_XML>;
}

close(F_XML);

my $xs = new Mioga2::XML::Simple();
my $xmltree = $xs->XMLin($xml);

$xmltree->{ident} = $xmltree->{instance_ident};
$xmltree->{creator_id} = 1;

my $lang = $config->{default_values}->{default_lang};
my $inst = Mioga2::InstanceList->new ($config, {attributes => { ident => $config->{instance_ident}, skeleton => "../web/skel/$lang/instance/50-standard.xml" } });
$inst->Store ();
