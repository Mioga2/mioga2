-- Remove Doris application
DELETE FROM m_application_group_allowed USING m_application WHERE m_application_group_allowed.application_id = m_application.rowid AND m_application.ident = 'Doris';
DELETE FROM m_method USING m_function, m_application WHERE m_method.function_id = m_function.rowid AND m_function.application_id = m_application.rowid AND m_application.ident = 'Doris';
DELETE FROM m_profile_function USING m_function, m_application WHERE m_profile_function.function_id = m_function.rowid AND m_function.application_id = m_application.rowid AND m_application.ident = 'Doris';
DELETE FROM m_function USING m_application WHERE m_function.application_id = m_application.rowid AND m_application.ident = 'Doris';
DELETE FROM m_instance_application USING m_application WHERE m_instance_application.application_id = m_application.rowid AND m_application.ident = 'Doris';
DELETE FROM m_application WHERE ident = 'Doris';

-- Compiled-version of home url
ALTER TABLE m_mioga ADD COLUMN target_url varchar(128);

-- Default application is 'Search' for groups that don't have it already configured
UPDATE m_group SET default_app_id = m_application.rowid FROM m_application, m_group_type WHERE m_group.type_id = m_group_type.rowid AND m_group_type.ident = 'group' AND m_group.default_app_id IS NULL AND m_application.ident = 'Search';

-- Create target_url if default_group_id or homepage is set
UPDATE m_mioga SET target_url = '/bin/' || m_group.ident || '/' || m_application.ident || '/DisplayMain' FROM m_group, m_application WHERE m_group.rowid = m_mioga.default_group_id AND m_application.rowid = m_group.default_app_id AND m_mioga.default_group_id IS NOT NULL;
UPDATE m_mioga SET target_url = homepage WHERE homepage IS NOT NULL;

-- Default target_url is user's Workspace, unless already configured to something else
UPDATE m_mioga set target_url='/bin/__MIOGA-USER__/Workspace/DisplayMain' WHERE homepage IS NULL AND default_group_id IS NULL;

-- Tagging system
CREATE TABLE m_tag_catalog (
	rowid            serial UNIQUE NOT NULL,
	ident            varchar(128) UNIQUE NOT NULL,
	mioga_id         int4,
	created          timestamp NOT NULL,
	modified         timestamp NOT NULL,

	FOREIGN KEY (mioga_id) REFERENCES m_mioga (rowid)
);

CREATE TABLE m_tag (
	rowid            serial UNIQUE NOT NULL,
	tag              varchar(128) NOT NULL CHECK (length(tag) > 0),
	catalog_id       int4,

	FOREIGN KEY (catalog_id) REFERENCES m_tag_catalog (rowid)
);
CREATE UNIQUE INDEX m_tag_mioga_index ON m_tag (tag, catalog_id);

CREATE TABLE uri_data (
	uri_id           int4 NOT NULL,
	tags             text,
	description      text,

	FOREIGN KEY (uri_id) REFERENCES m_uri (rowid)
);
-- Log system
CREATE TABLE m_log (
	date           timestamp NOT NULL,
	instance       varchar(128),
	user_ident     varchar(128),
	method         varchar(64),
	status         varchar(5),
	uri            text,
	args           text
);


CREATE OR REPLACE FUNCTION check_group_base_mioga_id () RETURNS trigger AS '
    DECLARE
        rec record;
    BEGIN
        SELECT * INTO rec FROM m_group_base WHERE mioga_id = OLD.rowid;
        IF FOUND THEN
            RAISE EXCEPTION ''Instance rowid % is still referenced in m_group_base'', OLD.rowid;
            RETURN NULL;
        END IF;

        RETURN OLD;
    END;
' LANGUAGE 'plpgsql';

CREATE TRIGGER m_group_base_mioga_id_check
     BEFORE DELETE ON m_mioga FOR EACH ROW
     EXECUTE PROCEDURE check_group_base_mioga_id ();

-- Grant access to newly-introduced Louvre "Read" function to profiles having access to Louvre "Anim" function
INSERT INTO m_profile_function SELECT m_profile_function.profile_id, F2.rowid AS function_id FROM m_profile_function, m_function F1, m_function F2, m_application WHERE m_profile_function.function_id = F1.rowid AND F1.application_id = m_application.rowid AND m_application.ident = 'Louvre' AND F1.ident = 'Anim' AND F2.application_id = m_application.rowid AND F2.ident = 'Read' AND (m_profile_function.profile_id, F2.rowid) NOT IN (SELECT * FROM m_profile_function);

-- Add locale to gallery properties
ALTER TABLE m_louvre ADD COLUMN locale varchar(5);
UPDATE m_louvre SET locale=m_lang.locale FROM m_uri, m_group, m_lang WHERE m_louvre.uri_id = m_uri.rowid AND m_uri.group_id = m_group.rowid AND m_group.lang_id = m_lang.rowid;
ALTER TABLE m_louvre ALTER COLUMN locale SET NOT NULL;
ALTER TABLE m_louvre ADD COLUMN show_filename boolean DEFAULT TRUE;
ALTER TABLE m_louvre ADD COLUMN show_description boolean DEFAULT TRUE;
ALTER TABLE m_louvre ADD COLUMN show_tags boolean DEFAULT TRUE;
ALTER TABLE m_louvre ADD COLUMN show_dimensions boolean DEFAULT TRUE;
ALTER TABLE m_louvre ADD COLUMN show_weight boolean DEFAULT TRUE;

-- Hidden URIs (grant access to 'Hidden' to profiles that have access to 'Animation' of application 'AnimGroup')
ALTER TABLE m_uri ADD COLUMN hidden boolean NOT NULL DEFAULT FALSE;
UPDATE m_uri SET hidden = 't' WHERE uri IN (SELECT m_uri.uri || '/mioga2-louvre' FROM m_uri, m_louvre WHERE m_louvre.uri_id = m_uri.rowid);
INSERT INTO m_profile_function SELECT m_profile_function.profile_id, (SELECT m_function.rowid FROM m_function, m_application WHERE m_function.application_id = m_application.rowid AND m_application.ident = 'Magellan' AND m_function.ident = 'Hidden') AS function_id FROM m_profile_function, m_function, m_application WHERE m_profile_function.function_id = m_function.rowid AND m_function.application_id = m_application.rowid AND m_application.ident = 'AnimGroup' AND m_function.ident = 'Animation';

-- search_file
alter table search_file add column gallery_id int4;

ALTER TABLE m_louvre ADD COLUMN popup_display boolean DEFAULT TRUE;
