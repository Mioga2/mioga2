---
--- Add size to m_uri
---
ALTER TABLE m_uri ADD COLUMN size INTEGER;

---
--- Mermoz tables
---
CREATE TABLE mermoz_signature (
  ROWID     serial UNIQUE NOT NULL,
  user_id   INTEGER NOT NULL,
  created   timestamp DEFAULT now(),
  modified  timestamp DEFAULT now(),
  signature text,
  enabled   boolean DEFAULT false,
  
  FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);
