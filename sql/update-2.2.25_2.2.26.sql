--
-- Brute-force protection
--

-- Default timeout for attacked-account lock
ALTER TABLE m_mioga ADD COLUMN auth_fail_timeout int4 DEFAULT 1800;

-- Authentication failure record
ALTER TABLE m_user_base ADD COLUMN auth_fail int4 DEFAULT 0;
ALTER TABLE m_user_base ADD COLUMN last_auth_fail timestamp;

-- Status for disabled-accounts due to attacks
INSERT INTO m_user_status (ident) VALUES ('disabled_attacked');


--
-- Password policy
--

-- Minimum password length
ALTER TABLE m_mioga ADD COLUMN pwd_min_length int4 default 6;

-- Minimum number of letters
ALTER TABLE m_mioga ADD COLUMN pwd_min_letter int4 default 3;

-- Minimum number of digits
ALTER TABLE m_mioga ADD COLUMN pwd_min_digit int4 default 3;

-- Minimum number of special characters
ALTER TABLE m_mioga ADD COLUMN pwd_min_special int4 default 0;

-- Minimum number of case changes
ALTER TABLE m_mioga ADD COLUMN pwd_min_chcase int4 default 0;
