--
-- E-mail unicity for a given instance
--
CREATE UNIQUE INDEX m_user_base_email_mioga_index ON m_user_base (email, mioga_id);
