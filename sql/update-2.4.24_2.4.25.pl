#! /usr/bin/perl

#===============================================================================
#
#  Copyright (c) year 2013, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================

# ===============================================================================================
# USAGE : perl update-2.4.24_2.4.25.pl
#
# DESCRIPTION :
#
# script that changes position result of project task.
# in old version, the position_result attributes of task were : [result index, task position]
# after this script, the position_result will be : [id of result, task position]
# ===============================================================================================

use strict;
use warnings;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::UserList;
use Mioga2::GroupList;
use Data::Dumper;
use File::Basename;
use Mioga2::tools::database;

use JSON::XS;

my $miogaconf = new Mioga2::MiogaConf ("../web/conf/Mioga.conf");
my $dbh = $miogaconf->GetDBH ();

my $tl = SelectMultiple($dbh,"SELECT rowid AS task_id, project_id, position_result FROM project_task");
my $rl = SelectMultiple($dbh,"SELECT rowid AS result_id, project_id FROM project_result ORDER BY created ASC");

# =====================================================================
# make result_list as : 
#
#key is project id,
#value is array of project result id order by created.
#
#{
#          '6' => [ # id of project
#                   14, # id of result
#                   15,
#                   16,
#                   17
#                 ],
#          '2' => [
#                   1
#                 ]
#};
# =====================================================================
my $result_list = {};

foreach my $r (@$rl) {
	my $proj_id = $r->{project_id};
	if (exists $result_list->{$proj_id}) {
		push(@{$result_list->{$proj_id}}, $r->{result_id});
	}
	else {
		$result_list->{$proj_id} = [$r->{result_id}];
	}
}
# =====================================================================
# make task list according to change position_result necessary : 
#
#[
#          {
#            'position_result' => [0,1], # [ index of result , task position ]
#            'new_result' => '[1,1]', # [ id of result , task position ]
#            'project_id' => 2,
#            'task_id' => 35
#          },
#          {
#            'position_result' => [1,0], # [ index of result , task position ]
#            'new_result' => '[15,0]', # [ id of result , task position ]
#            'project_id' => 6,
#            'task_id' => 38
#          }
#];
# =====================================================================
my $task_list = [];
foreach my $t (@$tl) { 
	if ($t->{position_result} ne '') {
		$t->{position_result} = JSON::XS::decode_json($t->{position_result});
		if (scalar @{$t->{position_result}} && $t->{position_result}[0] != -1) {
			my $index = $t->{position_result}[0];
			my $pr_str = "[" . $result_list->{$t->{project_id}}->[$index] . "," . $t->{position_result}[1] . "]";
			$t->{new_result} = $pr_str;
			$t->{position_result}[0] = $result_list->{$t->{project_id}}[$index];
			push(@{$task_list}, $t);
		}
	}
	else {
		# position_result is empty
	}
}
# =====================================================================
# SQL update position_result foreach project_task
# =====================================================================
foreach my $newData (@$task_list) { 
	my $sql = "UPDATE project_task SET position_result='" . $newData->{new_result} . "' WHERE rowid=" . $newData->{task_id};
	ExecSQL($dbh,$sql);
}

1;
