
---
--- search_doc : documents indexed in Mioga search system
---
create table search_doc (
	rowid           serial UNIQUE NOT NULL,
	created         timestamp not null,
	modified        timestamp not null,
	indexed         timestamp,
	app_code        int4,
	app_id          int4,       -- rowid for app record table
	xapian_docid    int4,
	fl_error        boolean DEFAULT 'f',
	fl_deleted      boolean DEFAULT 'f',
	mioga_id        int4,

	unique (app_code, app_id, mioga_id)
);
---
--- search_file : special table for files to add fl_index flag
---
create table search_file (
	rowid           serial UNIQUE NOT NULL,
	uri_id          int4,
	mioga_id        int4,
	fl_index        boolean DEFAULT 't'
);


