--
-- Articles publication date
--

ALTER TABLE article ADD COLUMN published timestamp;
UPDATE article SET published=article.created WHERE published IS NULL AND status_id=(SELECT rowid FROM article_and_news_status WHERE label='published');

--
-- News publication date
--

ALTER TABLE news ADD COLUMN published timestamp;
UPDATE news SET published=news.created WHERE published IS NULL AND status_id=(SELECT rowid FROM article_and_news_status WHERE label='published');
