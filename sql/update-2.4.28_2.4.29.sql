DELETE FROM m_auth_session;
ALTER TABLE m_auth_session ADD COLUMN csrftoken varchar(32) UNIQUE NOT NULL;
