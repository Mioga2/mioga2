--
-- Update table m_dav_property
--
ALTER TABLE m_dav_property ADD mioga_id int;
UPDATE m_dav_property SET mioga_id = 1;
ALTER TABLE m_dav_property ADD CONSTRAINT "m_property_instance" FOREIGN KEY (mioga_id) REFERENCES m_mioga(ROWID);
ALTER TABLE m_dav_property ALTER COLUMN mioga_id SET NOT NULL;
