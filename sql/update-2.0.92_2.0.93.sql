-- ##################################################################
-- poll tables
-- ##################################################################

-- Drop old tables
DROP TABLE poll_vote_result, poll_answer, poll_question, poll_comment_answer, poll_comment, poll_user_close, poll_description CASCADE;

-- Create new tables

create table poll (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp not null,
    modified    timestamp not null,
    title       varchar(128) not null,
    description text,
    group_id    int not null,
    visible_for_all boolean default 't',
    anonymous   boolean default 'f',
    opening     timestamp,
    closing     varchar(64) not null,
    
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

create table poll_question (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp not null,
    modified    timestamp not null,
    poll_id     int not null,
    type        varchar(128) not null,
    label       text not null,
    
    FOREIGN KEY	(poll_id) REFERENCES poll (rowid)
);

create table poll_choice (
	rowid	    serial UNIQUE NOT NULL,
	created	    timestamp not null,
	modified timestamp not null,
	question_id int not null,
    label       varchar(128) not null,
    
    FOREIGN KEY	(question_id) REFERENCES poll_question (rowid)
);

create table poll_user_answer (
	rowid	 serial UNIQUE NOT NULL,
	created	 timestamp not null,
	modified timestamp not null,
    user_id     int not null,
    poll_id     int not null,
    validated   boolean default 'f',
    
    FOREIGN KEY	(user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY	(poll_id) REFERENCES poll (rowid)
);

create table poll_answer (
	rowid	 serial UNIQUE NOT NULL,
	created	 timestamp not null,
	modified timestamp not null,
	ua_id       int not null,
    question_id int not null,
    text        text,
    
    FOREIGN KEY	(ua_id) REFERENCES poll_user_answer (rowid),
    FOREIGN KEY	(question_id) REFERENCES poll_question (rowid)
);

create table poll_answer_choice (
	choice_id    int not null,
    answer_id    int not null,
    
    FOREIGN KEY	(choice_id) REFERENCES poll_choice (rowid),
    FOREIGN KEY	(answer_id) REFERENCES poll_answer (rowid)
);


-- ##################################################################
-- organizer tables
-- ##################################################################

alter table org_task add waiting_book boolean;
alter table org_task alter column waiting_book set default 'f';
