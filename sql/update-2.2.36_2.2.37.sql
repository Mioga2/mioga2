ALTER TABLE m_captcha ADD COLUMN base64 text;

-- ============================================================================
-- Louvre
-- ============================================================================
CREATE TABLE m_louvre (
	rowid     serial UNIQUE,
	uri_id    integer UNIQUE NOT NULL,
	max_tn_width  integer NOT NULL,
	max_tn_height integer NOT NULL,

	FOREIGN KEY (uri_id)   REFERENCES m_uri (rowid) ON DELETE CASCADE
);