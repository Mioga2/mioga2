ALTER TABLE m_uri ADD COLUMN history boolean NOT NULL DEFAULT FALSE;
UPDATE m_uri SET history = 't' FROM m_group WHERE m_uri.group_id = m_group.rowid AND m_group.history = 't';

-- ============================================================================
-- Short URL
-- ============================================================================
CREATE TABLE short_url (
	rowid          SERIAL UNIQUE NOT NULL,
	hash           varchar(32),
	target         text
);
