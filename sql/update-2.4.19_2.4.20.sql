-- Cleanup inconsistencies intoduced in 2.4.5 to 2.4.6 migration script
DELETE FROM m_profile_function WHERE function_id IS NULL;


-- Purge old calendar tables
--

DROP table cal_alarm_todo;
DROP table cal_todo_attendee;
DROP table cal_todo_category;
DROP table cal_todo;
DROP table cal_alarm_event;
DROP table cal_event_attendee;
DROP table cal_event_exdate;
DROP table cal_event_category;
DROP table cal_event;
DROP table cal_alarm_attendee;
DROP table cal_alarm;
DROP table calendar;

-- ##################################################################
-- Document tables
-- ##################################################################

create table document (
    rowid            serial UNIQUE NOT NULL,
    created          timestamp NOT NULL,
    modified         timestamp NOT NULL,
    url              text NOT NULL,
    app_id           int4 NOT NULL,
    mioga_id         int4 NOT NULL,

    FOREIGN KEY (app_id) REFERENCES m_application (rowid),
    FOREIGN KEY (mioga_id) REFERENCES m_mioga (rowid)
);

--
-- Document nesting - optional
--
CREATE TABLE document_tree (
    document_id      int4 NOT NULL,
    inherits_from    int4 NOT NULL,
    stop_inheritance boolean NOT NULL DEFAULT FALSE,

    FOREIGN KEY (document_id) REFERENCES document (rowid),
    FOREIGN KEY (inherits_from) REFERENCES document (rowid)
);

--
-- Document to group association
--
CREATE TABLE document_group (
    document_id      int4 NOT NULL,
    group_id         int4 NOT NULL,

    FOREIGN KEY (document_id) REFERENCES document (rowid),
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

--
-- Document authz for user
--
create table doc_authz_user (
    rowid        serial UNIQUE NOT NULL,
    doc_id       int4 NOT NULL,       -- rowid of document
    user_id      int4 NOT NULL,       -- rowid of m_user_base table
    access       int2 NOT NULL,       -- 0 : no right, 1 : read only, 2 : read / write

    FOREIGN KEY (user_id)  REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (doc_id) REFERENCES document (rowid),
    unique      (doc_id, user_id)
);
--
-- Document authz for teams
--
create table doc_authz_team (
    rowid        serial UNIQUE NOT NULL,
    doc_id       int4 NOT NULL,       -- rowid of document
    team_id      int4 NOT NULL,       -- rowid of m_group table
    access       int2 NOT NULL,       -- 0 : no right, 1 : read only, 2 : read / write

    FOREIGN KEY (team_id)  REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (doc_id) REFERENCES document (rowid),
    unique      (doc_id, team_id)
);
--
-- Authorize for groups
--
create table doc_authz_profile (
    rowid        serial UNIQUE NOT NULL,
    doc_id       int4 NOT NULL,       -- rowid of document
    profile_id   int4 NOT NULL,     -- rowid of m_profile table or NULL
    access       int2 NOT NULL,       -- 0 : no right, 1 : read only, 2 : read / write

    FOREIGN KEY (profile_id) REFERENCES m_profile (rowid),
    FOREIGN KEY (doc_id) REFERENCES document (rowid),
    unique      (doc_id, profile_id)
);

create index doc_authz_profile_profile_index on doc_authz_profile (profile_id);

-- Does the opposite of getChildDocIds
-- Given a document rowid, returns the document_id from which the rights have to be obtained (considering document tree and inheritance)
-- If document is not in a tree, just returns the document rowid.
CREATE OR REPLACE FUNCTION getRefDocId (INTEGER) RETURNS INTEGER AS '
    DECLARE
        docId ALIAS FOR $1;
        myrecord RECORD;
    
    BEGIN
        SELECT INTO myrecord COALESCE (T2.ref_doc_id, docId) AS document_id FROM document LEFT JOIN (WITH RECURSIVE tree (document_id, inherits_from, stop_inheritance) AS (SELECT document_id, inherits_from, stop_inheritance FROM document_tree WHERE document_id = docId UNION ALL SELECT document_tree.document_id, document_tree.inherits_from, document_tree.stop_inheritance FROM tree, document_tree WHERE tree.document_id != tree.inherits_from AND tree.stop_inheritance IS FALSE AND document_tree.document_id = tree.inherits_from) SELECT docId AS document_id, document_id AS ref_doc_id FROM tree WHERE stop_inheritance IS TRUE OR document_id IN (SELECT doc_id FROM doc_authz_profile)) AS T2 ON T2.document_id = document.rowid WHERE document.rowid = docId LIMIT 1;

    IF FOUND THEN
        return (myrecord.document_id);
    END IF;

    return docId;

END;
' LANGUAGE 'plpgsql';

-- Does the opposite of getRefDocId
-- Given a document rowid, returns the document_id of all documents "below" in the tree.
-- If document is not in a tree, just returns the document rowid.
CREATE OR REPLACE FUNCTION getChildDocIds (INTEGER) RETURNS SETOF INTEGER AS '
    DECLARE
        docId ALIAS FOR $1;
        r INTEGER;

    BEGIN
        FOR r IN WITH RECURSIVE tree (document_id, inherits_from, stop_inheritance) AS (SELECT document_id, inherits_from, stop_inheritance FROM document_tree WHERE document_id = docId UNION ALL SELECT document_tree.document_id, document_tree.inherits_from, document_tree.stop_inheritance FROM tree, document_tree WHERE document_tree.stop_inheritance IS FALSE AND tree.document_id = document_tree.inherits_from) SELECT document_id FROM tree UNION SELECT docId LOOP
            return next r;
        END LOOP;
    END
' LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION authzTestAccessForDocument(INTEGER, INTEGER) RETURNS INTEGER AS '
    DECLARE 
        docToCheck ALIAS FOR $1;
        user_id ALIAS FOR $2;
        myrecord RECORD;

    BEGIN
 
    SELECT INTO myrecord doc_id, max(accesskey) AS accesskey FROM (

        -- Search authorization for user.
        --
        SELECT document.rowid AS doc_id, (50 + doc_authz_user.access) AS accesskey FROM document, doc_authz_user
        WHERE doc_authz_user.doc_id = getRefDocId (docToCheck)
          AND doc_authz_user.user_id = user_id
    UNION
        -- Search authorization for team.
        --
        SELECT document.rowid AS doc_id, (40 + doc_authz_team.access) AS accesskey FROM document, doc_authz_team, m_group_group
        WHERE doc_authz_team.doc_id = getRefDocId (docToCheck)
          AND m_group_group.group_id = doc_authz_team.team_id
          AND m_group_group.invited_group_id = user_id

    UNION

        -- Search authorization for profile with direct user
        --
        SELECT document.rowid AS doc_id, (30 + doc_authz_profile.access) AS accesskey FROM document, doc_authz_profile, m_profile_group
        WHERE doc_authz_profile.doc_id = getRefDocId (docToCheck)
          AND m_profile_group.profile_id = doc_authz_profile.profile_id
          AND m_profile_group.group_id = user_id

    UNION

        -- Search authorization for profile for team
        --
        SELECT document.rowid AS doc_id, (20 + doc_authz_profile.access) AS accesskey FROM document, doc_authz_profile, m_profile_expanded_user
        WHERE doc_authz_profile.doc_id = getRefDocId (docToCheck)
          AND m_profile_expanded_user.profile_id = doc_authz_profile.profile_id
          AND m_profile_expanded_user.user_id = user_id

    UNION

        SELECT document.rowid AS doc_id, 10 AS accesskey FROM document WHERE document.rowid = getRefDocId (docToCheck)
    ) AS tmp_req
    GROUP BY (doc_id);

    IF FOUND THEN
        return (myrecord.accesskey % 10);
    END IF;

    return NULL;

END;
' LANGUAGE 'plpgsql';

--
-- Diderot tables
--
CREATE TABLE notice_template (
    rowid            serial UNIQUE NOT NULL,
    created            timestamp NOT NULL,
    modified        timestamp NOT NULL,
    title            varchar(128) NOT NULL,
    description        text,
    fields            text,
    field_order        text,
    group_id        int4 NOT NULL,
    document_id        int4,    -- Ideally should be NOT NULL but will fail on initial creation

    FOREIGN KEY (group_id) REFERENCES m_group(rowid),
    FOREIGN KEY (document_id) REFERENCES document(rowid)
);

CREATE TABLE notice_category (
    rowid            serial UNIQUE NOT NULL,
    ident            varchar(128) NOT NULL,
    parent_id        int4 NOT NULL,
    group_id        int4 NOT NULL,
    created            timestamp NOT NULL,
    modified        timestamp NOT NULL,
    document_id        int4 NOT NULL,

    FOREIGN KEY (parent_id) REFERENCES notice_category(rowid),
    FOREIGN KEY (group_id) REFERENCES m_group(rowid),
    FOREIGN KEY (document_id) REFERENCES document(rowid)
);

CREATE TABLE notice (
    rowid            serial UNIQUE NOT NULL,
    created            timestamp NOT NULL,
    modified        timestamp NOT NULL,
    category_id        int4,
    group_id        int4 NOT NULL,
    document_id        int4 NOT NULL,
    user_id         int4 NOT NULL,

    FOREIGN KEY (group_id) REFERENCES m_group(rowid),
    FOREIGN KEY (category_id) REFERENCES notice_category(rowid),
    FOREIGN KEY (document_id) REFERENCES document(rowid),
    FOREIGN KEY (user_id) REFERENCES m_group_rowids(rowid)
);

CREATE TABLE notice_field (
    ident            varchar(128) NOT NULL,
    value            text,
    notice_id        int4 NOT NULL,

    FOREIGN KEY (notice_id) REFERENCES notice(rowid)
);
CREATE UNIQUE INDEX notice_ident_idx ON notice_field(ident, notice_id);

CREATE TABLE notice_files (
    notice_id        int4 NOT NULL,
    uri_id            int4 NOT NULL,

    FOREIGN KEY (notice_id) REFERENCES notice(rowid),
    FOREIGN KEY (uri_id) REFERENCES m_uri(rowid)
);

--
-- User authorization to URIs
-- This view returns a full list of URIs and user rowids with associated access
--
CREATE VIEW user_uri_access AS
    SELECT uri_id, uri, user_id, (max(access)%10) AS access
        FROM (
            SELECT m_uri.rowid AS uri_id, m_uri.uri AS uri, m_authorize_user.user_id AS user_id, (50 + m_authorize.access) AS access
                FROM
                    m_authorize, m_uri, m_authorize_user
                WHERE
                    m_authorize.uri_id = m_uri.parent_uri_id
                    AND m_authorize_user.authz_id = m_authorize.rowid
            UNION SELECT m_uri.rowid AS uri_id, m_uri.uri AS uri, m_group_group.invited_group_id AS user_id, (40 + m_authorize.access) AS access
                FROM
                    m_authorize, m_uri, m_authorize_team, m_group_group
                WHERE
                    m_authorize.uri_id = m_uri.parent_uri_id
                    AND m_authorize_team.authz_id = m_authorize.rowid
                    AND m_group_group.group_id = m_authorize_team.team_id
            UNION SELECT m_uri.rowid AS uri_id, m_uri.uri AS uri, m_profile_group.group_id AS user_id, (30 + m_authorize.access) AS access
                FROM
                    m_authorize, m_uri, m_authorize_profile, m_profile_group
                WHERE
                    m_authorize.uri_id = m_uri.parent_uri_id
                    AND m_authorize_profile.authz_id = m_authorize.rowid
                    AND m_profile_group.profile_id = m_authorize_profile.profile_id
            UNION SELECT m_uri.rowid AS uri_id, m_uri.uri AS uri, m_profile_expanded_user.user_id AS user_id, (20 + m_authorize.access) AS access
                FROM
                    m_authorize, m_uri, m_authorize_profile, m_profile_expanded_user
                WHERE
                    m_authorize.uri_id = m_uri.parent_uri_id
                    AND m_authorize_profile.authz_id = m_authorize.rowid
                    AND m_profile_expanded_user.profile_id = m_authorize_profile.profile_id
        ) AS tmp_req
    GROUP BY uri_id, uri, user_id;


-----------------------
-- Chronos tables
-----------------------
create table chronos_group_pref (
    group_id         int4,
    currentCalId     int4 DEFAULT 0,  -- id of current calendar. If 0, must be reinitialized
    currentView      text DEFAULT 'agendaWeek',
    firsthour        int4 DEFAULT 7,
    colorscheme      int4 DEFAULT 1,    -- 1 (activity = background, agenda = border ), 2 (activity = border, agenda = background)

    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

create table chronos_user_pref (
    user_id          int4,
    currentCalId     int4 DEFAULT 0,  -- id of current calendar. If 0, must be reinitialized
    currentView      text DEFAULT 'agendaWeek',
    firsthour        int4 DEFAULT 7,
    colorscheme      int4 DEFAULT 1,    -- 1 (activity = background, agenda = border ), 2 (activity = border, agenda = background)

    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);


create table calendar (
    rowid            serial UNIQUE NOT NULL,
    created          timestamp NOT NULL,
    modified         timestamp NOT NULL,
    owner_type       int4 NOT NULL,     -- 1 : user, 2 : group, 3 : resource
    document_id      int4,
    private          boolean DEFAULT 'f',

    FOREIGN KEY (document_id) REFERENCES document (rowid)
);

create table group2cal (
    group_id       int4,
    calendar_id    int4,
    ident          text NOT NULL,
    color          char(7) DEFAULT '#000000',
    bgcolor        char(7) DEFAULT '#ffffff',


    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (calendar_id) REFERENCES calendar (rowid)
);
create unique index group2cal_group_calendar_index on group2cal (group_id, calendar_id);

create table user2cal (
    user_id          int4,
    calendar_id      int4,
    type             int4 DEFAULT NULL,  -- 0 : owner, 1 : other user, 2 : group calendar
    ident            text NOT NULL,
    color            char(7) DEFAULT '#000000',
    bgcolor          char(7) DEFAULT '#ffffff',
    visible          boolean DEFAULT true,

    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (calendar_id) REFERENCES calendar (rowid)
);
create unique index user2cal_user_calendar_index on user2cal (user_id, calendar_id);

create table cal_event (
    rowid            serial UNIQUE NOT NULL,
    created          timestamp NOT NULL,
    modified         timestamp NOT NULL,
    user_id          int4,    -- ID of last user who modified task
    calendar_id      int4,
    subject          text,
    description      text,
    dstart           timestamp,
    dend             timestamp,
    allDay           boolean DEFAULT false,
    fl_recur         boolean DEFAULT false,
    rrule            text,
    category_id      int4,

    FOREIGN KEY (category_id) REFERENCES task_category (rowid),
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (calendar_id) REFERENCES calendar (rowid)
);

CREATE VIEW groupcalendar AS
    SELECT group2cal.*, calendar.* FROM group2cal, calendar WHERE calendar.rowid=group2cal.calendar_id;

CREATE VIEW usercalendar AS
    SELECT user2cal.*, calendar.* FROM user2cal, calendar WHERE calendar.rowid=user2cal.calendar_id;

CREATE OR REPLACE FUNCTION testAccessOnCalendar(INTEGER, INTEGER) RETURNS INTEGER AS '
        DECLARE 
                c_id ALIAS FOR $1;
                u_id ALIAS FOR $2;
                myrecord RECORD;

        BEGIN
 
        SELECT INTO myrecord max(access) as access FROM (

                select authzTestAccessForDocument(document_id, u_id) as access FROM calendar WHERE calendar.rowid = c_id
        union
                SELECT 1 as access FROM (SELECT * FROM m_profile_user_function WHERE app_ident=''Chronos'' AND user_id=u_id UNION SELECT * FROM m_profile_expanded_user_functio WHERE app_ident=''Chronos'' AND user_id=u_id) AS t1  LEFT JOIN group2cal ON group2cal.group_id = t1.group_id, m_group WHERE t1.group_id = m_group.rowid AND calendar_id= c_id AND function_ident = ''Read''
        union
                SELECT 2 as access FROM (SELECT * FROM m_profile_user_function WHERE app_ident=''Chronos'' AND user_id=u_id UNION SELECT * FROM m_profile_expanded_user_functio WHERE app_ident=''Chronos'' AND user_id=u_id) AS t1  LEFT JOIN group2cal ON group2cal.group_id = t1.group_id, m_group WHERE t1.group_id = m_group.rowid AND calendar_id= c_id AND function_ident = ''Write''


        ) AS tmp_req;

        IF FOUND THEN
                return myrecord.access;
        END IF;

        return NULL;

END;
' LANGUAGE 'plpgsql';

-----------------------
-- Haussmann tables
-----------------------

CREATE TABLE project_model (
    rowid        serial UNIQUE NOT NULL,
     group_id     int4 NOT NULL,
    created      timestamp DEFAULT NOW(),
    modified     timestamp DEFAULT NOW(),
    label        varchar(64) NOT NULL,
    model        text NOT NULL,
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);

CREATE TABLE project (
    rowid        serial UNIQUE NOT NULL,
     group_id     int4 NOT NULL,
     owner_id     int4 NOT NULL,
    created      timestamp DEFAULT NOW(),
    modified     timestamp DEFAULT NOW(),
    label        varchar(150),
    use_result   bool DEFAULT TRUE,
    description  text,
    status       text,
    current_status int4,
    base_dir     text,
    tags         text,
    started      date NOT NULL,
    planned      date NOT NULL,
    ended        date,
    complementary_data   text,
    task_tree   text,
    model_id     int4,
    color          char(7) DEFAULT '#000000',
    bgcolor        char(7) DEFAULT '#ffffff',
    advanced_params text,
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (owner_id) REFERENCES m_user_base (rowid),
    FOREIGN KEY (model_id) REFERENCES project_model (rowid)
);

CREATE TABLE project_task (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp DEFAULT NOW(),
    modified     timestamp DEFAULT NOW(),
    project_id   int4,
    label        text,
    description  text,
    workload     int4 DEFAULT 0,
    progress     text,
    current_progress numeric(5,2) DEFAULT 0.00,
    pstart       date,
    pend         date,
    status       text,
    current_status int4,
    tags         text,
    fineblanking text,
    position_tree text,
    position_list text,
    position_result text,
       FOREIGN KEY (project_id) REFERENCES project (rowid)
);
create table project_task_note (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp DEFAULT NOW(),
    modified     timestamp DEFAULT NOW(),
    group_id     int4 NOT NULL,
    owner_id     int NOT NULL,
    text         text,
    project_id     int4,
    task_id         int,
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (owner_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (project_id) REFERENCES project (rowid),
    FOREIGN KEY (task_id) REFERENCES project_task (rowid)
);

CREATE TABLE project_result (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp DEFAULT NOW(),
    modified     timestamp DEFAULT NOW(),
    project_id   int4 NOT NULL,
    label        varchar(50) NOT NULL,
    budget          INT8 DEFAULT 0,
    tags         text,
    status       text,
    current_status int4,
    checked        BOOL DEFAULT false,
    FOREIGN KEY (project_id) REFERENCES project (rowid)
);
create table project_result_note (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp NOT NULL,
    modified     timestamp NOT NULL,
    group_id     int4 NOT NULL,
    owner_id     int NOT NULL,
    text         text,
    project_id     int4,
    result_id     int,
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (owner_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (project_id) REFERENCES project (rowid),
    FOREIGN KEY (result_id) REFERENCES project_result (rowid)
);

-------------------------------------
-- Relation tables
-------------------------------------

create table user2project_task (
    user_id      int4,
    task_id      int4,
    dstart       date,
    dend         date,
    show_event   boolean default false,
    color        char(7),
    bgcolor      char(7),

    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (task_id) REFERENCES project_task (rowid)
);
create unique index user2project_task_user_task_index on user2project_task (user_id, task_id);

create table user2task_access (
    user_id      int4,
    task_id      int4,
    last_access  timestamp NOT NULL,

    FOREIGN KEY (user_id) REFERENCES m_user_base (rowid),
    FOREIGN KEY (task_id) REFERENCES project_task (rowid)
);
create unique index user2task_access_user_access_index on user2task_access  (user_id, task_id);

create table current_project_user2group_id (
    user_id              int4,
    group_id             int4,
    current_project_id     int4,

    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (current_project_id) REFERENCES project (rowid)
);
create unique index current_project_user2group_id_index on current_project_user2group_id  (user_id, group_id);

CREATE VIEW project_task_view AS
    SELECT project_task.*, project.label as project_label, project.group_id as group_id, m_group_base.ident as group_ident,
           user2project_task.user_id, user2project_task.dstart, user2project_task.dend, user2project_task.show_event,
            user2project_task.color, user2project_task.bgcolor
        FROM project_task LEFT JOIN user2project_task ON user2project_task.task_id = project_task.rowid, project, m_group_base
        WHERE project.rowid = project_task.project_id
              AND m_group_base.rowid = project.group_id;

-------------------------------------
-- Eiffel tables
-------------------------------------

create table eiffel_pref (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp NOT NULL,
    modified     timestamp NOT NULL,
    group_id     int4,
    week_count   int4,
    display      text,

    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);
create unique index eiffel_pref_group_idx on eiffel_pref (group_id);
