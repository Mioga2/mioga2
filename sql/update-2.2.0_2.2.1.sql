--
-- Update table planning_pref
--
alter table planning_pref add app VARCHAR(128);
ALTER TABLE planning_pref ALTER COLUMN app SET DEFAULT '/Planning/ViewTasks';
ALTER TABLE planning_pref ADD use_order BOOLEAN;
ALTER TABLE planning_pref ALTER COLUMN use_order SET DEFAULT 'f';
UPDATE planning_pref SET app='/Planning/ViewTasks';
UPDATE planning_pref SET use_order='f';

--
-- CREATE TABLE planning_user_order
--

create table planning_user_order (
    rowid        serial UNIQUE NOT NULL,
    planning_id  int NOT NULL,
    user_id      int,

    FOREIGN KEY (planning_id) REFERENCES planning_pref (rowid),
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);

--
-- update table m_uri
--

ALTER TABLE m_uri ALTER COLUMN mimetype TYPE text;
