-- m_mioga update
ALTER TABLE m_mioga ADD COLUMN referent_id int4;
ALTER TABLE m_mioga ADD COLUMN referent_comment varchar(128);
ALTER TABLE m_mioga ADD COLUMN comment text;
ALTER TABLE m_mioga ADD COLUMN default_group_id int4;
ALTER TABLE m_mioga ADD COLUMN homepage varchar(128);

-- m_user_base update
ALTER TABLE m_user_base ADD COLUMN shared boolean;

-- faq update
ALTER TABLE faq ADD COLUMN position int4;
ALTER TABLE faq_categories ADD COLUMN position int4;
