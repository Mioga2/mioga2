DROP VIEW project_task_view;

CREATE VIEW project_task_view AS
SELECT project_task.rowid, project_task.created, project_task.modified, project_task.project_id, project_task.label, project_task.description, project_task.workload, project_task.progress, project_task.current_progress, project_task.pend, project_task.pstart, project_task.status, project_task.current_status, project_task.tags, project_task.fineblanking, project_task.position_tree, project_task.position_list, project_task.position_result, project.label AS project_label, project.group_id, m_group_base.ident AS group_ident, user2project_task.user_id, user2project_task.dstart, user2project_task.dend, user2project_task.show_event, project.color, project.bgcolor
   FROM project_task
   LEFT JOIN user2project_task ON user2project_task.task_id = project_task.rowid, project, m_group_base
  WHERE project.rowid = project_task.project_id AND m_group_base.rowid = project.group_id;


alter table user2project_task drop column color;
alter table user2project_task drop column bgcolor;


CREATE OR REPLACE FUNCTION getChildDocIds (INTEGER) RETURNS SETOF INTEGER AS '
	DECLARE
		docId ALIAS FOR $1;
		r INTEGER;

	BEGIN
		FOR r IN WITH RECURSIVE tree (document_id, inherits_from, stop_inheritance) AS (SELECT document_id, inherits_from, stop_inheritance FROM document_tree WHERE document_id = docId UNION ALL SELECT document_tree.document_id, document_tree.inherits_from, document_tree.stop_inheritance FROM tree, document_tree WHERE document_tree.stop_inheritance IS FALSE AND tree.document_id = document_tree.inherits_from AND document_tree.document_id != document_tree.inherits_from) SELECT document_id FROM tree UNION SELECT docId LOOP
			return next r;
		END LOOP;
	END
' LANGUAGE 'plpgsql';

-------------------------------------
-- Deming tables
-------------------------------------

CREATE TABLE activity_report (
	rowid         serial UNIQUE NOT NULL,
	user_id       int4 NOT NULL,
	activity_id   int4 NOT NULL,
	dtstart       timestamp NOT NULL,
	dtend         timestamp NOT NULL,
	notes         text,

	FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
	FOREIGN KEY (activity_id) REFERENCES task_category(rowid)
);

CREATE TABLE activity_report_source (
	task_category_id  int4 UNIQUE NOT NULL,
	ident             varchar(128),

	FOREIGN KEY (task_category_id) REFERENCES task_category (rowid)
);

-- GPT 2013-06-04

alter table eiffel_pref add column show_groups  boolean default 'f';

-- ==========================================
-- Cantor application tables
-- ==========================================

--
-- Main table
--
CREATE TABLE survey (
    rowid               serial UNIQUE,

    title               varchar(128) NOT NULL,  -- a title for the survey
    description         varchar(128) NOT NULL,  -- a description for the survey
    duration            int NOT NULL,           -- a duration for the survey
    open_attendee_list  boolean DEFAULT 'f',    -- a type private or public for the survey (default is false => public)
    short_url_id        int4,
    owner_id            int NOT NULL,

    created             timestamp NOT NULL,
    modified            timestamp NOT NULL,

    FOREIGN KEY (owner_id) REFERENCES m_user_base (rowid),
    FOREIGN KEY (short_url_id) REFERENCES short_url (rowid)
);

--
-- Attendee list
--
CREATE TABLE survey_attendee (
	rowid        serial UNIQUE,
	survey_id    int4 NOT NULL,
	user_id      int4,
	fullname     varchar(128),
	email        varchar(128),

	FOREIGN KEY (survey_id) REFERENCES survey (rowid),
	FOREIGN KEY (user_id) REFERENCES m_user_base (rowid)
);

--
-- Resource list
--
CREATE TABLE survey_resource (
	survey_id    int4 NOT NULL,
	resource_id  int4 NOT NULL,

	FOREIGN KEY (survey_id) REFERENCES survey (rowid),
	FOREIGN KEY (resource_id) REFERENCES m_resource (rowid)
);

--
-- Date list
--
CREATE TABLE survey_date (
	rowid        serial UNIQUE,
	survey_id    int4 NOT NULL,
	date         timestamp NOT NULL,

	FOREIGN KEY (survey_id) REFERENCES survey (rowid)
);
CREATE UNIQUE INDEX survey_date_survey_id ON survey_date (survey_id, date);

--
-- Answer list
--
CREATE TABLE survey_answer (
	attendee_id  int4 NOT NULL,
	date_id      int4 NOT NULL,

	FOREIGN KEY (attendee_id) REFERENCES survey_attendee (rowid),
	FOREIGN KEY (date_id) REFERENCES survey_date (rowid)
);
