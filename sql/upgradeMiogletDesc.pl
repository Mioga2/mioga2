#!/usr/bin/perl -w

use strict;
use lib "../lib";
use Mioga2::XML::Simple;
use Mioga2::tools::string_utils;
use Mioga2::tools::database;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIProfile;
use Mioga2::AppDesc;
use Mioga2::Portal::MiogletDesc;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Data::Dumper;
use DBI;

print "upgradeMiogletDesc.pl running ...\n";

my $miogaconf = "../web/conf/Mioga.conf";
my $configxml = "../conf/Config.xml";

foreach my $var (qw(configxml miogaconf)) {
	my @res = grep {$_ =~ /^$var=(.*)$/} @ARGV;
	
	if(@res) {
		my ($val) = ($res[0] =~ /^$var=(.*)$/);
		eval "\$$var = \$val";
	}
}

if(-f "$configxml.d/Portal.xml") {
	$configxml =  "$configxml.d/Portal.xml";
}

my $miogaconfobj = LoadMiogaConf();
my $xmltree = LoadConfigXML();

my $inst = GetInstances($miogaconfobj);
foreach my $i (@$inst) {
	print "    upgrading instance $i ...\n";
	my $config = new Mioga2::Config($miogaconfobj, $i);
	UpgradeMiogletDesc($config, $xmltree);
}


sub LoadMiogaConf {
	open (F_XML, $miogaconf)
		or die "Can't open $miogaconf : $!";

	my $xml;

	{
		local $/ = undef;
		$xml = <F_XML>;
	}

	close(F_XML);

	my $xs = new Mioga2::XML::Simple();
    my $xmltree = $xs->XMLin($xml);


	my $miogaconf = new Mioga2::MiogaConf("$miogaconf");

	return $miogaconf;
}



sub LoadConfigXML {
	open (F_XML, $configxml)
		or die "Can't open $configxml : $!";

	my $xml;

	{
		local $/ = undef;
		$xml = <F_XML>;
	}

	close(F_XML);

	my $xs = new Mioga2::XML::Simple(forcearray => 1);
    my $xmltree = $xs->XMLin($xml);
	
	return $xmltree;
}

sub UpgradeMiogletDesc {
	my ($config, $xmltree) = @_;

	my $dbh = $config->GetDBH();

	my $installed_mioglet = SelectMultiple($dbh,
										   "SELECT DISTINCT portal_mioglet.ident, portal_mioglet.package FROM portal_mioglet, m_application, m_instance_application WHERE m_application.rowid = portal_mioglet.app_id and m_instance_application.application_id = m_application.rowid and m_instance_application.mioga_id = ".$config->GetMiogaId());

	my $conf_dir = $config->GetInstallPath()."/conf";
	
	foreach my $mioglet (@{$xmltree->{mioglets}->[0]->{mioglet}}) {
		if(grep {$_->{ident} eq $mioglet->{ident}} @$installed_mioglet) {
			UpdateMioglet($config, $mioglet->{package});
		}
		else {
			InstallMioglet($config, $mioglet->{package});
		}
	}


#	foreach my $mioglet (@$installed_mioglet) {
		#
		# Should delete uninstalled Mioglet
		# 
#	}
}


#
# Private functions
#

sub UpdatePortalMioglet {
	my ($config, $mioglet_desc) = @_;
	
	my $dbh = $config->GetDBH();

	my $app_desc = new Mioga2::AppDesc($config, package => $mioglet_desc->{application});
	my $mioglet = new Mioga2::Portal::MiogletDesc($config, $mioglet_desc->{ident});	

	foreach my $func (keys %{$mioglet_desc->{methods}}) {
		my $function_id = SelectSingle($dbh,
									   "SELECT m_function.rowid FROM m_function ".
									   "WHERE  m_function.application_id = ".$app_desc->GetRowid()." AND ".
									   "       m_function.ident = '$func'");
		
		if(!defined $function_id) {
			die "Function $func not found.";
		}

		$function_id = $function_id->{rowid};


		my $funcs = SelectMultiple($dbh, "SELECT portal_mioglet_method.* FROM portal_mioglet_method WHERE function_id = $function_id");

		foreach my $meth (@{$mioglet_desc->{methods}->{$func}}) {
			if(grep { $_->{ident} eq $meth} @$funcs) {
				next;
			}

			my $values = { mioglet_id => $mioglet->GetRowid(),
						   ident => $meth,
						   function_id => $function_id,
						   created => 'now()',
						   modified => 'now()',
						 };

			my $sql = BuildInsertRequest($values, table => 'portal_mioglet_method',
				    					          string => ["ident"],
					    						  other => ["mioglet_id", "function_id", "created", "modified"]);
			ExecSQL($dbh, $sql);
		}
	}
}

sub UpdateMioglet {
	my ($config, $package) = @_;

	eval "require $package";
	if(@!) {
		die $@;
	}
	
	my $mioglet = $package->new();
	my $mioglet_desc = $mioglet->GetMiogletDesc();

	UpdatePortalMioglet($config, $mioglet_desc);
}

sub ChoosePrefix {
	my ($config, $mioglet_desc) = @_;

	my $dbh = $config->GetDBH();
	my $prefix = $mioglet_desc->{xml_prefix};

	my $done = 0;
	my $i = 1;
	while(! $done) {
		my $sql = "SELECT portal_mioglet.* FROM portal_mioglet, m_application, m_instance_application ".
			      "WHERE portal_mioglet.app_id = m_application.rowid AND ".
				  "      m_instance_application.application_id = m_application.rowid AND ".
				  "      m_instance_application.mioga_id = ".$config->GetMiogaId()." AND ".
				  "      portal_mioglet.xml_prefix = '$prefix'";

		my $res = SelectSingle($dbh, $sql);
		if(! defined $res) {
			$done = 1;
			next;
		}

		$prefix = $mioglet_desc->{xml_prefix}."$i";
		$i++;
	}

	$mioglet_desc->{xml_prefix} = $prefix;
}

sub CreatePortalMioglet {
	my ($config, $mioglet_desc) = @_;
	
	my $dbh = $config->GetDBH();

	my $app_desc = new Mioga2::AppDesc($config, package => $mioglet_desc->{application});
	$mioglet_desc->{app_id} = $app_desc->GetRowid();
	$mioglet_desc->{created} = $mioglet_desc->{modified} = 'now()';

	ChoosePrefix($config, $mioglet_desc);	
	
	my $done = 0;
	my $sql = BuildInsertRequest($mioglet_desc, table => 'portal_mioglet',
													string => ["ident", "package", "xml_prefix"],
													other => ["app_id", 'created', 'modified']);
	ExecSQL($dbh, $sql);

	my $mioglet = new Mioga2::Portal::MiogletDesc($config, $mioglet_desc->{ident});	
	foreach my $func (keys %{$mioglet_desc->{methods}}) {
		my $function_id = SelectSingle($dbh,
									   "SELECT m_function.rowid FROM m_function ".
									   "WHERE  m_function.application_id = ".$app_desc->GetRowid()." AND ".
									   "       m_function.ident = '$func'");
		
		if(!defined $function_id) {
			die "Function $func not found.";
		}

		$function_id = $function_id->{rowid};

		foreach my $meth (@{$mioglet_desc->{methods}->{$func}}) {
			my $values = { mioglet_id => $mioglet->GetRowid(),
						   ident => $meth,
						   function_id => $function_id,
						   created => 'now()',
						   modified => 'now()',
						 };

			$sql = BuildInsertRequest($values, table => 'portal_mioglet_method',
									           string => ["ident"],
											   other => ["mioglet_id", "function_id", "created", "modified"]);
			ExecSQL($dbh, $sql);
		}
	}
}

sub InstallMioglet {
	my ($config, $package) = @_;

	eval "require $package";
	if(@!) {
		die $@;
	}
	
	my $mioglet = $package->new();
	my $mioglet_desc = $mioglet->GetMiogletDesc();

	CreatePortalMioglet($config, $mioglet_desc);
}
# ----------------------------------------------------------------------------
#  Return Instance List
# ----------------------------------------------------------------------------
sub GetInstances {
    my $miogaconf = shift;
                 
    my $res = SelectMultiple($miogaconf->GetDBH(), "SELECT * FROM m_mioga"); 
    my @inst = map {$_->{ident}} @$res;
    return \@inst;
}

1;
