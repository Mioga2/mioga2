-- 
-- Alter m_user_base table
-- 

-- According to m_nonlocal_user
ALTER TABLE m_user_base ADD COLUMN previous_user_status int4;
ALTER TABLE m_user_base ADD COLUMN data_expiration_date timestamp;
ALTER TABLE m_user_base ADD COLUMN password_expiration_date timestamp;

-- According to m_external_user
ALTER TABLE m_user_base ADD COLUMN external_ident varchar(128);
ALTER TABLE m_user_base ADD COLUMN external_mioga_id int4;

-- According to m_ldap_user
ALTER TABLE m_user_base ADD COLUMN dn text;


-- 
-- Move data to m_user_base table
-- 

-- Disable trigger and constraint
DROP TRIGGER m_user_base_rowid_check ON m_user_base;
DROP TRIGGER m_nonlocal_user_rowid_check ON m_nonlocal_user;
DROP TRIGGER m_external_user_rowid_check ON m_external_user;
DROP TRIGGER m_ldap_user_rowid_check ON m_ldap_user;

-- Move data from m_ldap_user
INSERT INTO m_user_base (rowid, created, modified, mioga_id, type_id, public_part, autonomous, theme_id, lang_id, anim_id, creator_id, default_profile_id, disk_space_used, timezone_id, time_per_day, working_days, monday_first, status_id, ident, firstname, lastname, password, email, history, data_expiration_date, password_expiration_date, previous_user_status, external_ident, external_mioga_id, dn) SELECT rowid, created, modified, mioga_id, type_id, public_part, autonomous, theme_id, lang_id, anim_id, creator_id, default_profile_id, disk_space_used, timezone_id, time_per_day, working_days, monday_first, status_id, ident, firstname,lastname, password, email, history, data_expiration_date, password_expiration_date, previous_user_status, external_ident, external_mioga_id, dn FROM ONLY m_ldap_user;
DELETE FROM ONLY m_ldap_user;

-- Move data from m_external_user
INSERT INTO m_user_base (rowid, created, modified, mioga_id, type_id, public_part, autonomous, theme_id, lang_id, anim_id, creator_id, default_profile_id, disk_space_used, timezone_id, time_per_day, working_days, monday_first, status_id, ident, firstname, lastname, password, email, history, data_expiration_date, password_expiration_date, previous_user_status, external_ident, external_mioga_id, dn) SELECT rowid, created, modified, mioga_id, type_id, public_part, autonomous, theme_id, lang_id, anim_id, creator_id, default_profile_id, disk_space_used, timezone_id, time_per_day, working_days, monday_first, status_id, ident, firstname,lastname, password, email, history, data_expiration_date, password_expiration_date, previous_user_status, external_ident, external_mioga_id, dn FROM ONLY m_external_user;
DELETE FROM ONLY m_external_user;


--
-- Drop useless tables
--
DROP TABLE m_ldap_user;
DROP TABLE m_external_user;
DROP TABLE m_nonlocal_user;

-- Create trigger
CREATE TRIGGER m_user_base_rowid_check
	BEFORE INSERT OR DELETE ON m_user_base FOR EACH ROW
	EXECUTE PROCEDURE check_group_rowid ();


-- 
-- Create table for password assistance
-- 

CREATE table m_passwd_reinit (
	hash varchar(32) UNIQUE NOT NULL,
	ident varchar(128) UNIQUE NOT NULL,
	expiration_date timestamp NOT NULL
);

--
-- Modify m_mioga table
--
ALTER TABLE m_mioga ADD COLUMN ldap_user_desc_attr varchar(128);
ALTER TABLE m_mioga ADD COLUMN ldap_user_inst_attr varchar(128);
ALTER TABLE m_mioga ADD COLUMN use_secret_question boolean;

--
-- User's secret question
--
CREATE TABLE m_secret_question (
	rowid     serial UNIQUE,
	question  varchar(1024),
	answer    varchar(1024)
);

CREATE TABLE m_user2question (
    user_id       int4,
	question_id   int4
);

