CREATE FUNCTION plpgsql_call_handler () RETURNS language_handler
        AS '$libdir/plpgsql'
        LANGUAGE C;

CREATE TRUSTED LANGUAGE plpgsql HANDLER plpgsql_call_handler;

CREATE FUNCTION plperl_call_handler () RETURNS language_handler
        AS '$libdir/plperl'
        LANGUAGE C;

CREATE TRUSTED LANGUAGE plperl HANDLER plperl_call_handler;
