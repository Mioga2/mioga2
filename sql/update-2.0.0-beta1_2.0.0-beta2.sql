--
-- Add new column
--

ALTER TABLE m_mioga ADD COLUMN max_allowed_group int4 ;
ALTER TABLE m_mioga ALTER COLUMN max_allowed_group SET DEFAULT NULL ;



ALTER TABLE m_mioga ADD COLUMN quota_soft int4 ;
ALTER TABLE m_mioga ALTER COLUMN quota_soft SET DEFAULT NULL ;

ALTER TABLE m_mioga ADD COLUMN quota_hard int4 ;
ALTER TABLE m_mioga ALTER COLUMN quota_hard SET DEFAULT NULL ;

ALTER TABLE m_mioga ADD COLUMN quota_warning_date timestamp ;
ALTER TABLE m_mioga ALTER COLUMN quota_warning_date SET DEFAULT NULL ;

ALTER TABLE m_mioga ADD COLUMN disk_usage int4 ;
ALTER TABLE m_mioga ALTER COLUMN disk_usage SET DEFAULT NULL ;

ALTER TABLE m_mioga ADD COLUMN locked boolean ;
ALTER TABLE m_mioga ALTER COLUMN locked SET DEFAULT 'f' ;



ALTER TABLE synchro_agents ADD COLUMN last_synchro_date timestamp ;
ALTER TABLE synchro_agents ALTER COLUMN last_synchro_date SET DEFAULT '1970-01-01 00:00:00' ;


--
-- Fix installation bug of previous version
--

UPDATE m_theme SET ident = 'orange' WHERE ident IS NULL or ident = '';
UPDATE m_theme_lang SET ident = 'fr_FR' WHERE ident IS NULL or ident = '';


--
-- User last connection date to group
--
create table m_group_user_last_conn (
    group_id            int4,        -- rowid of m_group
    user_id             int4,        -- rowid of m_user_base
    last_connection     timestamp,

    primary key (group_id,user_id),
    unique (group_id,user_id),
    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);

DROP VIEW m_profile_expanded_user_method;
DROP VIEW m_profile_expanded_user_functio;
DROP VIEW m_profile_expanded_user;
DROP VIEW m_group_expanded_user;
DROP VIEW m_group_invited_user;
DROP VIEW m_group_invited_group;
DROP VIEW m_group_group_type;

ALTER TABLE m_group_group DROP COLUMN last_connection;

--
-- Like m_group_group but add the type of the invited group.
--
create view m_group_group_type as
    select m_group_group.*, m_group_type.ident as type

    from   m_group_group, m_group_type, m_group_base

    where  m_group_type.rowid = m_group_base.type_id and
           m_group_base.rowid = m_group_group.invited_group_id;
        


--
-- View that show only groups (m_group) invited in groups
--
create view m_group_invited_group as
    select m_group_group.* 

    from m_group_group, m_group

    where m_group_group.invited_group_id = m_group.rowid;

--
-- View that show only users (m_user_base) invited in groups
--
create view m_group_invited_user as
    select m_group_group.*

    from m_group_group, m_user_base

    where m_group_group.invited_group_id = m_user_base.rowid;


--
--  Expand m_group_invited_group.
--
create view m_group_expanded_user as
        select distinct m_group_invited_group.group_id as group_id,
                        m_group_invited_user.invited_group_id as invited_group_id

        from   m_group_invited_group, m_group_invited_user

        where  m_group_invited_user.group_id = m_group_invited_group.invited_group_id;


--
-- expand m_profile_group from m_group_expanded_user
--
create view m_profile_expanded_user as
    select distinct m_profile_group.profile_id as profile_id,
           m_group_group.invited_group_id as user_id

    from   m_group_group, m_profile_group, m_group

    where  m_group_group.group_id = m_profile_group.group_id AND
           m_group.rowid = m_group_group.group_id;
           

--
-- This view based on m_group_expanded_user, m_profile_group and 
-- m_profile_function show which user can access to a given function
--

create view m_profile_expanded_user_functio as
    select distinct  m_profile.rowid as profile_id,
           m_application.rowid as app_id,
           m_application.ident as app_ident,
           m_function.rowid as function_id,
           m_function.ident as function_ident,
           m_profile.group_id as group_id,
           m_group_group.invited_group_id as user_id

    from   m_profile, m_group_group, m_group, m_profile_function,
           m_profile_group, m_function, m_application

    where  m_profile_group.profile_id = m_profile.rowid and
           m_profile_function.profile_id = m_profile.rowid and
           m_function.rowid = m_profile_function.function_id and
           m_application.rowid = m_function.application_id and
           m_group_group.group_id = m_profile_group.group_id AND
           m_group.rowid = m_group_group.group_id;
           


--
-- This view based on m_group_expanded_user, m_profile_group,
-- m_profile_function and m_method show which user can access
-- to a given method
--

create view m_profile_expanded_user_method as
    select distinct m_profile.rowid as profile_id,
           m_application.rowid as app_id,
           m_application.ident as app_ident,
           m_method.rowid as method_id,
           m_method.ident as method_ident,
           m_profile.group_id as group_id,
           m_group_group.invited_group_id as user_id

    from   m_profile, m_group_group, m_group, m_profile_function,
           m_profile_group, m_method, m_function, m_application

    where  m_profile_group.profile_id = m_profile.rowid and
           m_profile_function.profile_id = m_profile.rowid and
           m_method.function_id = m_profile_function.function_id and
           m_function.rowid = m_profile_function.function_id and
           m_application.rowid = m_function.application_id and
           m_group_group.group_id = m_profile_group.group_id AND
           m_group.rowid = m_group_group.group_id;
                      

