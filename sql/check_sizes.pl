#!/usr/bin/perl -w

use strict;
use lib "../lib";
use Mioga2::MiogaConf;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Data::Dumper;
use Mioga2::Config;
use Mioga2::Classes::URI;
use Mioga2::URI;
use Error qw(:try);
use Text::Iconv;
use File::MimeInfo::Magic;

my $configxml = "../conf/Config.xml";
my $miogaconf = "../web/conf/Mioga.conf";
my $dir = "..";

foreach my $var qw(configxml miogaconf timezonexml dir) {
  my @res = grep {$_ =~ /^$var=(.*)$/} @ARGV;
  
  if(@res) {
    my ($val) = ($res[0] =~ /^$var=(.*)$/);
    eval "\$$var = \$val";
  }
}

my $config = Mioga2::MiogaConf->new($miogaconf);
my $conf = MiogaConf->new(dir => $dir, config => $configxml);

my $dbh     = $config->GetDBH;

print "\nUpdating size information in URI table:\n";
my $results = SelectMultiple($dbh, "SELECT * FROM m_uri");
foreach my $res (@$results) {
  my $uri   = Mioga2::URI->new($config, uri => $res->{uri});
  my $size  = (-s $uri->GetRealPath) || 0;
  
  print "$res->{uri} => $size\n";
  ExecSQL($dbh, "UPDATE m_uri SET size = $size WHERE rowid = $res->{rowid}");
}

print "done!\n";