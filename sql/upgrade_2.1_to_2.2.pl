#!/usr/bin/perl -w

use strict;
use lib "../lib";
use Mioga2::MiogaConf;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Data::Dumper;
use Mioga2::Config;
use Mioga2::Classes::URI;
use Mioga2::URI;
use Error qw(:try);
use Text::Iconv;
use File::MimeInfo::Magic;

$Error::Debug = 1;

my %utf8_tables = ( 'article' => ['title', 'header', 'contents'], 
                    'article_and_news_status' => ['label'],
                    'article_category_list' => ['label', 'description'],
                    'forum_category' => ['label'],
                    'forum_message' => ['message'],
                    'forum_subject' => ['subject'],
                    'forum_thematic' => ['label', 'description'],
                    'news' => ['title', 'text'],
                    'poll' => ['title', 'description'],
                    'poll_answer' => ['text'],
                    'poll_choice' => ['label'],
                    'poll_question' => ['type', 'label'],
                    'm_uri' => ['uri'],
                  );
# ===========================================================



# ===========================================================

if (@ARGV < 1) {
  die "usage: $0 <temp_database_name>\n";
}
if (@ARGV > 1) {
  die "Please provide only one argument (database name).\n";
}
my $tmp_db  = $ARGV[0];

my $configxml = "../conf/Config.xml";
my $miogaconf = "../web/conf/Mioga.conf";
my $dir = "..";

foreach my $var qw(configxml miogaconf timezonexml dir) {
	my @res = grep {$_ =~ /^$var=(.*)$/} @ARGV;
	
	if(@res) {
		my ($val) = ($res[0] =~ /^$var=(.*)$/);
		eval "\$$var = \$val";
	}
}

my $config = Mioga2::MiogaConf->new($miogaconf);
my $conf = MiogaConf->new(dir => $dir, config => $configxml);

my $db_infos  = $config->GetConfParameter('database');

my $dbiUser = $db_infos->{DBIlogin};
my $dbiPassword = $db_infos->{DBIpasswd};
my $dbDriver = $db_infos->{DBIdriver};
my $datasource = "dbi:$dbDriver:dbname=$tmp_db";

my $dbh     = $config->GetDBH;
my $tmp_dbh = DBI->connect($datasource, $dbiUser, $dbiPassword);

if (!defined($tmp_dbh))	{
  throw Mioga2::Exception::DB("Mioga2::Config::ConnectDBI", 
				$DBI::errstr, "Cannot open connection with database", "");
}

print "Migrating data:\n";
my $check_utf8  = Text::Iconv->new('utf8', 'utf8');
my $conv_iso    = Text::Iconv->new("iso-8859-1", "utf8");

foreach my $table (keys %utf8_tables) {
  print "Copying data from $table...\n";
  my $results = SelectMultiple($dbh, "SELECT * FROM $table");
  foreach my $result (@$results) {
    my $rowid = $result->{rowid};
    my %args  = (table => $table, string => $utf8_tables{$table});
    foreach my $column (keys %{$result}) {
      unless (grep($_ eq $column, @{$utf8_tables{$table}})) {
        delete $result->{$column};
        next;
      }
      $result->{$column} = $conv_iso->convert($result->{$column}) unless ($check_utf8->convert($result->{$column}));
    }
    
    my $sql = BuildUpdateRequest($result, %args) . " WHERE rowid=$rowid";
    print "$sql\n";
    ExecSQL($tmp_dbh, $sql);
  }
}

print "\nUpdating MIME information in URI table:\n";
my $results = SelectMultiple($tmp_dbh, "SELECT * FROM m_uri");
foreach my $res (@$results) {
  my $uri   = Mioga2::URI->new($config, uri => $res->{uri});
  my $mime  = mimetype($uri->GetRealPath);
  utf8::encode($mime);
  
  $mime = 'directory' if $mime =~ /directory/;
  print "$res->{uri} => $mime\n";
  ExecSQL($tmp_dbh, "UPDATE m_uri SET mimetype = '" . st_FormatPostgreSQLString($mime) . "' WHERE rowid = $res->{rowid}");
}

print "done!\n";
