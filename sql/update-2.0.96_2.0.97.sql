-- ##################################################################
-- Calendar tables
-- This schema is mainly inspired by the icalendar format
-- RFC can be found here: http://tools.ietf.org/html/rfc2445
-- ##################################################################

create table calendar (
    rowid       serial unique not null,
    created     timestamp not null,
    modified    timestamp not null,
    group_id    int not null,
    title       varchar(128) not null, -- a name for the calendar
    main        bool default 'f',
    private     bool default 'f',
    
    FOREIGN KEY	(group_id) REFERENCES m_group_rowids (rowid)
);

-- alarm object to be attached to an event or a todo
create table cal_alarm (
	rowid		serial UNIQUE NOT NULL,
	created		timestamp not null,
	modified	timestamp not null,
	action      text not null, -- action of the alarm as detailed in RFC
    trigger     timestamp not null, -- when to trigger the alarm
    summary     varchar(255),
    description text
);

create table cal_alarm_attendee (
	rowid		serial UNIQUE NOT NULL,
	created		timestamp not null,
	modified	timestamp not null,
	name        varchar(128),
    email       varchar(128),
    user_id     int,
    
    FOREIGN KEY	(user_id) REFERENCES m_group_rowids (rowid)
);

create table cal_event (
    rowid       serial UNIQUE NOT NULL,
    created     timestamp not null,
    modified    timestamp not null,
    summary     varchar(255), -- RFC tells us summary can be truncated to 255 chars
    description text,
    dtstart     timestamp, -- start of the event. defaults to a timestamp but can also be a simple date. (optional)
    dtend       timestamp, -- end of the event (optional)
    rrule       text, -- recurrence rule (optional)
    transparent bool default 'f', -- define if event blocks time or not
    location    text,
    priority    int, -- event priority. 1 is the highest while 9 is the lowest. a value of 0 (zero) equals to not set the priority.
    calendar_id int not null,
    creator_id  int not null,
    
    FOREIGN KEY	(calendar_id) REFERENCES calendar (rowid),
    FOREIGN KEY	(creator_id) REFERENCES m_group_rowids (rowid)
);

-- associate one or more category with an event
create table cal_event_category (
	category_id    int not null,
    event_id       int not null,
    
    FOREIGN KEY	(category_id) REFERENCES task_category (rowid),
    FOREIGN KEY	(event_id) REFERENCES cal_event (rowid)
);

-- implements icalendar exdate property of an event
create table cal_event_exdate (
    rowid       serial unique not null,
    exdate      timestamp not null,
    event_id    int not null,
    
    FOREIGN KEY	(event_id) REFERENCES cal_event (rowid)
);

-- associate one or more attendee with an event
create table cal_event_attendee (
	rowid		serial UNIQUE NOT NULL,
	created		timestamp not null,
	modified	timestamp not null,
	name        varchar(128), -- name of a non mioga user
    email       varchar(128), -- email of a non mioga user
    user_id     int, -- user from mioga
    
    FOREIGN KEY	(user_id) REFERENCES m_group_rowids (rowid)
);

-- associate an alarm with an event
create table cal_alarm_event (
	alarm_id    int not null,
    event_id    int not null,
    
    FOREIGN KEY	(alarm_id) REFERENCES cal_alarm (rowid),
    FOREIGN KEY	(event_id) REFERENCES cal_event (rowid)
);

create table cal_todo (
	rowid       serial UNIQUE NOT NULL,
	created     timestamp not null,
	modified    timestamp not null,
	summary     varchar(255),
    description text,
    dtstart     timestamp, -- start of todo (optional)
    due         timestamp, -- due of todo. can be a simple date instead. (optional)
    duration    varchar(64), -- duration in icalendar format (optional)
    percent     int,
    priority    int,
    location    text,
    completed   bool default 'f',
    creator_id  int not null,
    calendar_id int not null,
    
    FOREIGN KEY	(creator_id) REFERENCES m_group_rowids (rowid),
    FOREIGN KEY	(calendar_id) REFERENCES calendar (rowid)
);

create table cal_todo_category (
	category_id    int not null,
    todo_id        int not null,
    
    FOREIGN KEY	(category_id) REFERENCES task_category (rowid),
    FOREIGN KEY	(todo_id) REFERENCES cal_todo (rowid)
);

create table cal_todo_attendee (
	rowid		serial UNIQUE NOT NULL,
	created		timestamp not null,
	modified	timestamp not null,
	name        varchar(128),
    email       varchar(128),
    user_id     int,
    
    FOREIGN KEY	(user_id) REFERENCES m_group_rowids (rowid)
);

create table cal_alarm_todo (
	alarm_id    int not null,
    todo_id     int not null,
    
    FOREIGN KEY	(alarm_id) REFERENCES cal_alarm (rowid),
    FOREIGN KEY	(todo_id) REFERENCES cal_todo (rowid)
);

insert into m_theme(created,modified,name,ident,mioga_id) values(now(),now(),'Mioga2','default',1);
