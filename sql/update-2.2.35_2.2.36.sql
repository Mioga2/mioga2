-- ============================================================================
-- Captcha
-- ============================================================================
CREATE TABLE m_captcha (
	hash      varchar(32) UNIQUE NOT NULL,
	captcha   varchar(16) NOT NULL,
	created   timestamp NOT NULL
);
