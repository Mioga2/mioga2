-- ##################################################################
-- organizer tables
-- ##################################################################

alter table org_task add user_id int;
alter table org_task add constraint "org_task_user_id" FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid);
