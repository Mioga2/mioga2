#!/usr/bin/perl -w

use strict;
use lib "../lib";
use Mioga2::MiogaConf;
use Mioga2::tools::database;
use Data::Dumper;
use Mioga2::Config;
use Error qw(:try);

$Error::Debug = 1;

my $configxml = "../conf/Config.xml";
my $miogaconf = "../web/conf/Mioga.conf";
my $dir = "..";

foreach my $var qw(configxml miogaconf timezonexml dir) {
	my @res = grep {$_ =~ /^$var=(.*)$/} @ARGV;
	
	if(@res) {
		my ($val) = ($res[0] =~ /^$var=(.*)$/);
		eval "\$$var = \$val";
	}
}

my $config = new Mioga2::MiogaConf($miogaconf);

my $conf = new MiogaConf( dir => $dir, config => $configxml);
#$conf->Install($config);

my $dbh = $config->GetDBH();
my $sql;

BeginTransaction($dbh);

#
# Modify themes and lang
#
print "Create and initialize m_lang table\n";
$sql = "create table m_lang ("
			. " rowid    serial UNIQUE NOT NULL,"
			. " ident   varchar(32) NOT NULL,"
    		. " locale   varchar(32) NOT NULL"
		. ")";
ExecSQL($dbh, $sql);

$sql = "insert into m_lang (ident, locale) values ('en_US', 'en_US')";
ExecSQL($dbh, $sql);
$sql = "insert into m_lang (ident, locale) values ('fr_FR', 'fr_FR')";
ExecSQL($dbh, $sql);

print "Adding new default theme\n";
ExecSQL($dbh, "insert into m_theme(created,modified,name,ident,mioga_id) values(now(),now(),'Mioga2','default',1)");

print "Modify m_group_base table\n";
$sql = "ALTER TABLE m_group_base ADD COLUMN theme_id int4";
ExecSQL($dbh, $sql);
$sql = "ALTER TABLE m_group_base ADD COLUMN lang_id int4";
ExecSQL($dbh, $sql);
$sql = "ALTER TABLE m_group_base ADD CONSTRAINT m_group_base_theme_id_chk FOREIGN KEY (theme_id) REFERENCES m_theme (rowid)";
ExecSQL($dbh, $sql);
$sql = "ALTER TABLE m_group_base ADD CONSTRAINT m_group_base_lang_id_chk FOREIGN KEY (lang_id) REFERENCES m_lang (rowid)";
ExecSQL($dbh, $sql);

print "Modify content for m_group_base table\n";
$sql = "select rowid,theme_lang_id from m_group_base";
my $result = SelectMultiple($dbh, $sql);
foreach my $group (@$result) {
	$sql = "update m_group_base"
				. " set lang_id=(select m_lang.rowid from m_lang,m_theme_lang where m_lang.ident=m_theme_lang.ident AND m_theme_lang.rowid=$group->{theme_lang_id}),"
				. " theme_id=(select m_theme_lang.theme_id from m_theme_lang where m_theme_lang.rowid=$group->{theme_lang_id})"
				. " WHERE m_group_base.rowid=$group->{rowid}";
	ExecSQL($dbh, $sql);
}

# WARNING I don't know how to get name of constraint so it is hardcoded
#
print "Modify m_group_base drop theme_lang_id column\n";
$sql = 'ALTER TABLE m_group_base DROP CONSTRAINT "$3"';
ExecSQL($dbh, $sql);
$sql = "ALTER TABLE m_group_base DROP COLUMN theme_lang_id";
ExecSQL($dbh, $sql);


print "Modify m_mioga table\n";
$sql = "ALTER TABLE m_mioga ADD COLUMN default_theme_id int4";
ExecSQL($dbh, $sql);
$sql = "ALTER TABLE m_mioga ADD COLUMN default_lang_id int4";
ExecSQL($dbh, $sql);
$sql = "ALTER TABLE m_mioga ADD CONSTRAINT m_mioga_theme_chk FOREIGN KEY (default_theme_id) REFERENCES m_theme (rowid)";
ExecSQL($dbh, $sql);
$sql = "ALTER TABLE m_mioga ADD CONSTRAINT m_mioga_lang_chk FOREIGN KEY (default_lang_id) REFERENCES m_lang (rowid)";
ExecSQL($dbh, $sql);


print "Modify content for m_mioga table\n";
$sql = "select rowid,default_theme_lang_id from m_mioga";
$result = SelectMultiple($dbh, $sql);
foreach my $inst (@$result) {
	$sql = "update m_mioga"
				. " set default_lang_id=(select m_lang.rowid from m_lang,m_theme_lang where m_lang.ident=m_theme_lang.ident AND m_theme_lang.rowid=$inst->{default_theme_lang_id}),"
				. " default_theme_id=(select m_theme_lang.theme_id from m_theme_lang where m_theme_lang.rowid=$inst->{default_theme_lang_id})"
				. " WHERE m_mioga.rowid=$inst->{rowid}";
	ExecSQL($dbh, $sql);
}

# WARNING I don't know how to get name of constraint so it is hardcoded
print "Modify m_mioga drop theme_lang_id column\n";
$sql = 'ALTER TABLE m_mioga DROP CONSTRAINT "m_mioga_theme_lang_chk"';
ExecSQL($dbh, $sql);
$sql = "ALTER TABLE m_mioga DROP COLUMN default_theme_lang_id";
ExecSQL($dbh, $sql);

print "Drop table m_theme_lang\n";
$sql = "DROP TABLE m_theme_lang";
ExecSQL($dbh, $sql);

EndTransaction($dbh);

