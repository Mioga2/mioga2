-- ============================================================================
-- Reporting
-- ============================================================================
CREATE VIEW view_user_groups AS
	SELECT
		m_user_base.rowid,
		m_user_base.firstname,
		m_user_base.lastname,
		m_user_base.email,
		T2.group_id,
		T2.group_ident,
		T1.group_id AS team_id,
		T1.ident AS team_ident,
		T2.profile_id,
		T2.profile_ident,
		m_user_base.mioga_id
	FROM
		m_user_base,
		(SELECT
			m_user_base.rowid,
			m_group_group.group_id,
			m_group.ident
		FROM
			m_user_base,
			m_group_group,
			m_group
		WHERE
			m_user_base.rowid = m_group_group.invited_group_id AND
			m_group_group.group_id = m_group.rowid AND
			m_group.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'team')
		) AS T1,
		(SELECT
			m_profile_group.group_id AS invited_group_id,
			m_group.rowid AS group_id,
			m_group.ident AS group_ident,
			m_profile.rowid AS profile_id,
			m_profile.ident AS profile_ident
		FROM
			m_profile_group,
			m_profile,
			m_group
		WHERE
			m_profile_group.profile_id = m_profile.rowid AND
			m_profile.group_id = m_group.rowid
		) AS T2
	WHERE
		m_user_base.rowid = T1.rowid AND
		T1.group_id = T2.invited_group_id
	UNION ALL SELECT m_user_base.rowid AS user_id,
		m_user_base.firstname,
		m_user_base.lastname,
		m_user_base.email,
		T1.rowid AS group_id,
		T1.ident AS group_ident,
		NULL AS team_id,
		NULL AS team_ident,
		T2.profile_id,
		T2.ident AS profile_ident,
		m_user_base.mioga_id
	FROM
		m_user_base
	LEFT JOIN (
		SELECT
			m_group.rowid,
			m_group.ident,
			m_group.mioga_id
		FROM
			m_group,
			m_group_type
		WHERE
			m_group.type_id = m_group_type.rowid AND
			m_group_type.ident = 'group'
		) AS T1
		ON
			T1.mioga_id = m_user_base.mioga_id
	LEFT JOIN (
		SELECT
			m_user_base.rowid,
			m_profile.rowid AS profile_id,
			m_profile.ident,
			m_profile.group_id
		FROM
			m_user_base,
			m_profile_group,
			m_profile
		WHERE
			m_user_base.rowid = m_profile_group.group_id AND
			m_profile_group.profile_id = m_profile.rowid
		) AS T2
		ON
			T2.rowid = m_user_base.rowid AND
			T2.group_id = T1.rowid
	ORDER BY rowid, group_id;


CREATE VIEW view_user_teams AS
	SELECT
		m_user_base.rowid,
		m_user_base.firstname,
		m_user_base.lastname,
		m_user_base.email,
		T1.rowid AS team_id,
		T1.ident AS team_ident,
		T2.is_member,
		m_user_base.mioga_id
	FROM
		m_user_base
	LEFT JOIN (
		SELECT
			m_group.rowid,
			m_group.ident,
			m_group.mioga_id
		FROM
			m_group,
			m_group_type
		WHERE
			m_group.type_id = m_group_type.rowid AND
			m_group_type.ident = 'team'
		) AS T1
	ON
		m_user_base.mioga_id = T1.mioga_id
	LEFT JOIN (
		SELECT
			m_group_group.group_id,
			m_group_group.invited_group_id,
			1 AS is_member
		FROM
			m_group_group
		) AS T2
	ON
		T2.invited_group_id = m_user_base.rowid AND
		T2.group_id = T1.rowid
	ORDER BY
		rowid,
		team_id;


CREATE VIEW view_team_groups AS
	SELECT
		T1.rowid,
		T1.ident,
		T2.rowid AS group_id,
		T2.ident AS group_ident,
		T3.rowid AS profile_id,
		T3.ident AS profile_ident,
		T1.mioga_id
	FROM (
		SELECT
			m_group.rowid,
			m_group.ident,
			m_group.mioga_id
		FROM
			m_group,
			m_group_type
		WHERE
			m_group.type_id = m_group_type.rowid AND
			m_group_type.ident = 'team'
	) AS T1
	LEFT JOIN
		m_group
	AS T2
	ON
		T1.mioga_id = T2.mioga_id
	LEFT JOIN (
		SELECT
			m_profile_group.group_id AS team_id,
			m_profile.rowid,
			m_profile.group_id,
			m_profile.ident
		FROM
			m_profile_group,
			m_profile
		WHERE
			m_profile_group.profile_id = m_profile.rowid
	) AS T3
	ON
		T3.team_id = T1.rowid AND
		T3.group_id = T2.rowid
	WHERE T2.type_id = (SELECT rowid FROM m_group_type WHERE ident = 'group')
	ORDER BY
		rowid,
		group_id;
