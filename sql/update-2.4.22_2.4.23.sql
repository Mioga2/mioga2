ALTER TABLE survey ALTER COLUMN description TYPE text;

-- 2013-07-23

ALTER TABLE chronos_group_pref ADD COLUMN showWeekend boolean default 't';
ALTER TABLE chronos_user_pref ADD COLUMN showWeekend boolean default 't';

-- 2013-07-25 -- Fix bug #1389 consequences
INSERT INTO m_group_user_last_conn SELECT DISTINCT group_to_team.group_id, team_to_user.user_id FROM (SELECT group_id, invited_group_id AS team_id FROM m_group_group WHERE group_id IN (SELECT rowid FROM m_group WHERE type_id = (SELECT rowid FROM m_group_type WHERE ident = 'group')) AND invited_group_id IN (SELECT rowid FROM m_group WHERE type_id = (SELECT rowid FROM m_group_type WHERE ident = 'team'))) AS group_to_team LEFT JOIN (SELECT group_id AS team_id, invited_group_id AS user_id FROM m_group_group WHERE group_id IN (SELECT rowid FROM m_group WHERE type_id = (SELECT rowid FROM m_group_type WHERE ident = 'team'))) AS team_to_user ON group_to_team.team_id = team_to_user.team_id WHERE (group_to_team.group_id, team_to_user.user_id) NOT IN (SELECT group_id, user_id FROM m_group_user_last_conn);

-- 2013-07-25 -- Fix bug #1386 consequences
ALTER TABLE project ALTER COLUMN color SET DEFAULT '#b7b7ef';

-- 2013-08-14 Add attendees for project task

create table attendee2project_task (
	user_id      int4,
	task_id      int4,
	write        boolean default false,
	show_event   boolean default false,

	FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid),
	FOREIGN KEY (task_id) REFERENCES project_task (rowid)
);
create unique index attendee2project_task_user_task_index on attendee2project_task (user_id, task_id);

CREATE VIEW user_project_task_view AS
SELECT project_task.rowid, project_task.created, project_task.modified, project_task.project_id, project_task.label, project_task.description, project_task.workload, project_task.progress, project_task.current_progress, project_task.pend, project_task.pstart, project_task.status, project_task.current_status, project_task.tags, project_task.fineblanking, project_task.position_tree, project_task.position_list, project_task.position_result, project.label AS project_label, project.group_id, m_group_base.ident AS group_ident, tmp.user_id, tmp.dstart, tmp.dend, tmp.show_event, tmp.type, project.color, project.bgcolor
   FROM project_task
   LEFT JOIN (select task_id, user_id, 't' as write,'r' as type, show_event, dstart,dend from user2project_task union select attendee2project_task.task_id, attendee2project_task.user_id, write, 'a' as type, attendee2project_task.show_event, user2project_task.dstart as dstart, user2project_task.dend as dend from attendee2project_task LEFT JOIN user2project_task ON attendee2project_task.task_id=user2project_task.task_id) as tmp ON tmp.task_id = project_task.rowid, project, m_group_base
  WHERE project.rowid = project_task.project_id AND m_group_base.rowid = project.group_id;

