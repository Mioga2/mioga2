-- Re-create m_application_user_status view
DROP VIEW m_application_user_status;
CREATE VIEW m_application_user_status AS
	SELECT DISTINCT 
		m_application.rowid, 
		m_application.created, 
		m_application.modified, 
		m_application.ident, 
		m_application.package, 
		m_application.description, 
		m_application.can_be_public, 
		m_application.usable_by_resource, 
		m_application.all_groups, 
		m_application.all_users, 
		m_application.type_id, 
		m_application.mioga_id, 
		m_user_base.rowid AS group_id, 
		( SELECT count(*) AS count
			   FROM m_application_group_allowed
			  WHERE m_application_group_allowed.application_id = m_application.rowid AND 
					m_application_group_allowed.group_id = m_user_base.rowid) AS status, 
		(( SELECT count(*) AS count
			   FROM m_application_group_allowed
			  WHERE m_application_group_allowed.application_id = m_application.rowid AND 
					m_application_group_allowed.group_id = m_user_base.rowid AND 
					m_application_group_allowed.is_public = true)) <> 0 AND 
					m_application.can_be_public AS is_public
	FROM 
		m_application, 
		m_application_group, 
		m_user_base, 
		m_application_type
	WHERE 
		m_application.all_users = false AND
		m_application_group.group_id = m_user_base.rowid AND 
		m_application.rowid = m_application_group.application_id AND 
		m_application_type.rowid = m_application.type_id AND 
		m_application_type.ident::text <> 'library'::text AND 
		m_application_type.ident::text <> 'shell'::text
	UNION SELECT DISTINCT
		m_application.rowid, 
		m_application.created, 
		m_application.modified, 
		m_application.ident, 
		m_application.package, 
		m_application.description, 
		m_application.can_be_public, 
		m_application.usable_by_resource, 
		m_application.all_groups, 
		m_application.all_users, 
		m_application.type_id, 
		m_application.mioga_id, 
		m_user_base.rowid AS group_id, 
		( SELECT count(*) AS count
			   FROM m_application_group_allowed
			  WHERE m_application_group_allowed.application_id = m_application.rowid AND 
					m_application_group_allowed.group_id = m_user_base.rowid) AS status, 
		(( SELECT count(*) AS count
			   FROM m_application_group_allowed
			  WHERE m_application_group_allowed.application_id = m_application.rowid AND 
					m_application_group_allowed.group_id = m_user_base.rowid AND 
					m_application_group_allowed.is_public = true)) <> 0 AND 
					m_application.can_be_public AS is_public
	FROM 
		m_application, 
		m_user_base, 
		m_application_type
	WHERE 
		m_application.all_users = true AND
		m_application_type.rowid = m_application.type_id AND 
		m_application_type.ident::text <> 'library'::text AND 
		m_application_type.ident::text <> 'shell'::text AND
		m_application.mioga_id = m_user_base.mioga_id
	ORDER BY rowid, 
		created, 
		modified, 
		ident, 
		package, 
		description, 
		can_be_public, 
		usable_by_resource, 
		all_groups, 
		all_users, 
		type_id, 
		mioga_id, 
		group_id, 
		status,
		is_public;

-- Re-create m_application_group_status view
DROP VIEW m_application_group_status;
CREATE VIEW m_application_group_status AS
	SELECT DISTINCT m_application.*, m_group.rowid AS group_id,
		   (SELECT count(*) FROM m_application_group_allowed 

			WHERE m_application_group_allowed.application_id = m_application.rowid AND
				  m_application_group_allowed.group_id = m_group.rowid
		   ) AS status,

		   ((SELECT count(*) FROM m_application_group_allowed 

			 WHERE m_application_group_allowed.application_id = m_application.rowid AND
				   m_application_group_allowed.group_id = m_group.rowid AND 
				   m_application_group_allowed.is_public = 't'
			) !=0 AND can_be_public) AS is_public


	FROM m_application, m_application_group, m_group, m_application_type

	WHERE 
		   (m_application_group.group_id = m_group.rowid AND 
			m_application.rowid = m_application_group.application_id) AND 
		  m_application_type.rowid = m_application.type_id AND 
		  m_application_type.ident NOT IN ( 'library', 'shell')
	UNION SELECT DISTINCT m_application.*, m_group.rowid AS group_id,
		   (SELECT count(*) FROM m_application_group_allowed 

			WHERE m_application_group_allowed.application_id = m_application.rowid AND
				  m_application_group_allowed.group_id = m_group.rowid
		   ) AS status,

		   ((SELECT count(*) FROM m_application_group_allowed 

			 WHERE m_application_group_allowed.application_id = m_application.rowid AND
				   m_application_group_allowed.group_id = m_group.rowid AND 
				   m_application_group_allowed.is_public = 't'
			) !=0 AND can_be_public) AS is_public


	FROM m_application, m_group, m_application_type

	WHERE 
		  m_application.all_groups = 't' AND
		  m_application_type.rowid = m_application.type_id AND 
		  m_application_type.ident NOT IN ( 'library', 'shell') AND
		  m_application.mioga_id = m_group.mioga_id;
