#!/usr/bin/perl -w

use strict;
use lib "../lib";
use Mioga2::MiogaConf;
use Mioga2::tools::database;
use Data::Dumper;
use Mioga2::Config;
use Error qw(:try);

$Error::Debug = 1;

my $configxml = "../conf/Config.xml";
my $miogaconf = "../web/conf/Mioga.conf";
my $dir = "..";

foreach my $var (qw(configxml miogaconf timezonexml dir)) {
	my @res = grep {$_ =~ /^$var=(.*)$/} @ARGV;
	
	if(@res) {
		my ($val) = ($res[0] =~ /^$var=(.*)$/);
		eval "\$$var = \$val";
	}
}

my $config = new Mioga2::MiogaConf($miogaconf);

my $conf = new MiogaConf( dir => $dir, config => $configxml);
$conf->Install($config);



