ALTER TABLE m_tag_catalog DROP CONSTRAINT m_tag_catalog_ident_key;
CREATE UNIQUE INDEX m_tag_catalog_mioga_index ON m_tag_catalog (ident, mioga_id);
