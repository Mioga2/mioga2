-- Delete 'Login' application as HTML login is now a mod_perl handler
DELETE FROM m_method WHERE function_id IN (SELECT rowid FROM m_function WHERE application_id IN (SELECT rowid FROM m_application WHERE ident = 'Login'));
DELETE FROM m_function WHERE application_id IN (SELECT rowid FROM m_application WHERE ident = 'Login');
DELETE FROM m_application WHERE ident='Login';

-- Instance to Application table
CREATE TABLE m_instance_application (
	mioga_id         int4 NOT NULL,
	application_id   int4 NOT NULL,
	all_users        boolean DEFAULT false,
	all_groups       boolean DEFAULT false,
	all_resources    boolean DEFAULT false,

    FOREIGN KEY (mioga_id)       REFERENCES m_mioga (rowid),
    FOREIGN KEY (application_id) REFERENCES m_application (rowid)
);
CREATE UNIQUE INDEX m_instance_application_index ON m_instance_application (mioga_id, application_id);

-- Populate m_instance_application according to m_application contents
INSERT INTO m_instance_application SELECT mioga_id, (SELECT rowid FROM m_application where ident = MAPP.ident ORDER BY rowid LIMIT 1) AS application_id, all_users, all_groups, usable_by_resource FROM m_application AS MAPP ORDER BY mioga_id, application_id;

-- Re-affect profile functions
UPDATE m_profile_function SET function_id = TRANSL.target_function_id FROM (SELECT FUNC.rowid AS function_id, (SELECT m_function.rowid FROM m_function, m_application WHERE m_function.application_id = m_application.rowid AND m_application.ident = APP.ident AND m_function.ident = FUNC.ident ORDER BY m_application.rowid LIMIT 1) AS target_function_id, FUNC.ident AS function_ident, APP.ident AS application_ident FROM m_function AS FUNC, m_application AS APP WHERE FUNC.application_id = APP.rowid ORDER BY target_function_id, function_id) AS TRANSL WHERE m_profile_function.function_id = TRANSL.function_id;

-- Delete methods of functions to drop
DELETE FROM m_method USING m_function WHERE m_method.function_id = m_function.rowid AND m_function.rowid NOT IN (SELECT m_function.rowid FROM m_function, m_instance_application WHERE m_function.application_id = m_instance_application.application_id);

-- Delete portal mioglet methods
DELETE FROM portal_mioglet_method USING m_function WHERE portal_mioglet_method.function_id = m_function.rowid AND m_function.rowid NOT IN (SELECT m_function.rowid FROM m_function, m_instance_application WHERE m_function.application_id = m_instance_application.application_id);

-- Delete portal mioglets
DELETE FROM portal_mioglet WHERE app_id NOT IN (SELECT application_id FROM m_instance_application);

-- Delete functions of applications to drop
DELETE FROM m_function WHERE application_id NOT IN (SELECT application_id FROM m_instance_application);

-- Translate app_id from table synchro_rowids
UPDATE synchro_rowids SET app_id = TRANSL.target_application_id FROM (SELECT APP.rowid AS application_id, (SELECT rowid FROM m_application WHERE m_application.ident = APP.ident ORDER BY rowid LIMIT 1) AS target_application_id FROM m_application AS APP ORDER BY target_application_id) AS TRANSL;

-- Re-affect group authorizations (m_application_group AND m_application_group_allowed)
UPDATE m_application_group SET application_id = TRANSL.target_application_id FROM (SELECT APP.rowid AS application_id, (SELECT m_application.rowid FROM m_application WHERE m_application.ident = APP.ident ORDER BY rowid LIMIT 1) AS target_application_id, APP.ident AS application_ident FROM m_application AS APP ORDER BY application_ident, application_id) AS TRANSL WHERE m_application_group.application_id = TRANSL.application_id;
UPDATE m_application_group_allowed SET application_id = TRANSL.target_application_id FROM (SELECT APP.rowid AS application_id, (SELECT m_application.rowid FROM m_application WHERE m_application.ident = APP.ident ORDER BY rowid LIMIT 1) AS target_application_id, APP.ident AS application_ident FROM m_application AS APP ORDER BY application_ident, application_id) AS TRANSL WHERE m_application_group_allowed.application_id = TRANSL.application_id;

-- Update groups default application
UPDATE m_group SET default_app_id = TRANSL.target_application_id FROM (SELECT APP.rowid AS application_id, (SELECT m_application.rowid FROM m_application WHERE m_application.ident = APP.ident ORDER BY rowid LIMIT 1) AS target_application_id, APP.ident AS application_ident FROM m_application AS APP ORDER BY application_ident, application_id) AS TRANSL WHERE m_group.default_app_id = TRANSL.application_id;

-- Update RSS subscriptions
UPDATE rss_subscription SET application_id = TRANSL.target_application_id FROM (SELECT APP.rowid AS application_id, (SELECT m_application.rowid FROM m_application WHERE m_application.ident = APP.ident ORDER BY rowid LIMIT 1) AS target_application_id, APP.ident AS application_ident FROM m_application AS APP ORDER BY application_ident, application_id) AS TRANSL WHERE rss_subscription.application_id = TRANSL.application_id;

-- Delete dups from m_application
DELETE FROM m_application WHERE rowid NOT IN (SELECT application_id FROM m_instance_application);

-- Drop views that reference m_application
DROP VIEW m_application_user_status;
DROP VIEW m_application_group_status;

-- Drop useless columns
ALTER TABLE m_application DROP COLUMN all_groups;
ALTER TABLE m_application DROP COLUMN all_users;
ALTER TABLE m_application DROP COLUMN mioga_id;

-- Re-create views depending on m_application structure (m_application_user_status m_application_group_status)
CREATE VIEW m_application_user_status AS
	SELECT DISTINCT 
		m_application.rowid, 
		m_application.created, 
		m_application.modified, 
		m_application.ident, 
		m_application.package, 
		m_application.description, 
		m_application.can_be_public, 
		m_instance_application.all_resources, 
		m_instance_application.all_groups, 
		m_instance_application.all_users, 
		m_application.type_id, 
		m_instance_application.mioga_id, 
		m_user_base.rowid AS group_id, 
		( SELECT count(*) AS count
			   FROM m_application_group_allowed
			  WHERE m_application_group_allowed.application_id = m_application.rowid AND 
					m_application_group_allowed.group_id = m_user_base.rowid) AS status, 
		(( SELECT count(*) AS count
			   FROM m_application_group_allowed
			  WHERE m_application_group_allowed.application_id = m_application.rowid AND 
					m_application_group_allowed.group_id = m_user_base.rowid AND 
					m_application_group_allowed.is_public = true)) <> 0 AND 
					m_application.can_be_public AS is_public
	FROM 
		m_application, 
		m_application_group, 
		m_user_base, 
		m_instance_application,
		m_application_type
	WHERE 
		m_instance_application.all_users = false AND
		m_application_group.group_id = m_user_base.rowid AND 
		m_application.rowid = m_application_group.application_id AND 
		m_application.rowid = m_instance_application.application_id AND
		m_user_base.mioga_id = m_instance_application.mioga_id AND
		m_application_type.rowid = m_application.type_id AND 
		m_application_type.ident::text <> 'library'::text AND 
		m_application_type.ident::text <> 'shell'::text
	UNION SELECT DISTINCT
		m_application.rowid, 
		m_application.created, 
		m_application.modified, 
		m_application.ident, 
		m_application.package, 
		m_application.description, 
		m_application.can_be_public, 
		m_instance_application.all_resources, 
		m_instance_application.all_groups, 
		m_instance_application.all_users, 
		m_application.type_id, 
		m_instance_application.mioga_id, 
		m_user_base.rowid AS group_id, 
		( SELECT count(*) AS count
			   FROM m_application_group_allowed
			  WHERE m_application_group_allowed.application_id = m_application.rowid AND 
					m_application_group_allowed.group_id = m_user_base.rowid) AS status, 
		(( SELECT count(*) AS count
			   FROM m_application_group_allowed
			  WHERE m_application_group_allowed.application_id = m_application.rowid AND 
					m_application_group_allowed.group_id = m_user_base.rowid AND 
					m_application_group_allowed.is_public = true)) <> 0 AND 
					m_application.can_be_public AS is_public
	FROM 
		m_application, 
		m_user_base, 
		m_instance_application,
		m_application_type
	WHERE 
		m_instance_application.all_users = true AND
		m_application_type.rowid = m_application.type_id AND 
		m_application_type.ident::text <> 'library'::text AND 
		m_application_type.ident::text <> 'shell'::text AND
		m_application.rowid = m_instance_application.application_id AND
		m_instance_application.mioga_id = m_user_base.mioga_id
	ORDER BY rowid, 
		created, 
		modified, 
		ident, 
		package, 
		description, 
		can_be_public, 
		all_resources, 
		all_groups, 
		all_users, 
		type_id, 
		mioga_id, 
		group_id, 
		status,
		is_public;

CREATE VIEW m_application_group_status AS
	SELECT DISTINCT m_application.*, m_group.rowid AS group_id,
		   (SELECT count(*) FROM m_application_group_allowed 

			WHERE m_application_group_allowed.application_id = m_application.rowid AND
				  m_application_group_allowed.group_id = m_group.rowid
		   ) AS status,

		   ((SELECT count(*) FROM m_application_group_allowed 

			 WHERE m_application_group_allowed.application_id = m_application.rowid AND
				   m_application_group_allowed.group_id = m_group.rowid AND 
				   m_application_group_allowed.is_public = 't'
			) !=0 AND can_be_public) AS is_public,
			m_instance_application.mioga_id


	FROM m_application, m_application_group, m_group, m_application_type, m_instance_application

	WHERE 
		   (m_application_group.group_id = m_group.rowid AND 
			m_application.rowid = m_application_group.application_id) AND 
		  m_application_type.rowid = m_application.type_id AND 
		  m_application.rowid = m_instance_application.application_id AND
		  m_group.mioga_id = m_instance_application.mioga_id AND
		  m_application_type.ident NOT IN ( 'library', 'shell')
	UNION SELECT DISTINCT m_application.*, m_group.rowid AS group_id,
		   (SELECT count(*) FROM m_application_group_allowed 

			WHERE m_application_group_allowed.application_id = m_application.rowid AND
				  m_application_group_allowed.group_id = m_group.rowid
		   ) AS status,

		   ((SELECT count(*) FROM m_application_group_allowed 

			 WHERE m_application_group_allowed.application_id = m_application.rowid AND
				   m_application_group_allowed.group_id = m_group.rowid AND 
				   m_application_group_allowed.is_public = 't'
			) !=0 AND can_be_public) AS is_public,
			m_instance_application.mioga_id


	FROM m_application, m_group, m_application_type, m_instance_application

	WHERE 
		  m_instance_application.all_groups = 't' AND
		  m_application_type.rowid = m_application.type_id AND 
		  m_application_type.ident NOT IN ( 'library', 'shell') AND
		  m_application.rowid = m_instance_application.application_id AND
		  m_instance_application.mioga_id = m_group.mioga_id;

-- Add new columns
ALTER TABLE m_application ADD COLUMN is_user boolean;
ALTER TABLE m_application ADD COLUMN is_group boolean;
ALTER TABLE m_application RENAME COLUMN usable_by_resource TO is_resource;

-- Update m_auth_session structure
ALTER TABLE m_auth_session RENAME COLUMN ident TO email;
ALTER TABLE m_auth_session RENAME COLUMN hash TO token;
ALTER TABLE m_auth_session DROP COLUMN password;
