
create table planning_pref (
    rowid        serial UNIQUE NOT NULL,
    created      timestamp not null,
    modified     timestamp not null,
    group_id     int4 NOT NULL,
    show_we      boolean default 'f',
    view_type    varchar(10) default 'week',
    show_global  boolean default 't',

    FOREIGN KEY (group_id) REFERENCES m_group_rowids (rowid)
);


