---
--- News update
---

ALTER TABLE news_pref ALTER COLUMN moderated SET DEFAULT 'f';
ALTER TABLE news_pref ALTER COLUMN moderator_mail SET DEFAULT 't';
ALTER TABLE news_pref ALTER COLUMN user_mail SET DEFAULT 't';

---
--- Articles update
---

ALTER TABLE article RENAME COLUMN chapo TO header;

---
--- Articles Insertions
---

insert into article_and_news_status (created, modified, label) values (NOW(), NOW(), 'published');
insert into article_and_news_status (created, modified, label) values (NOW(), NOW(), 'waiting');
insert into article_and_news_status (created, modified, label) values (NOW(), NOW(), 'draft');

-- ==========================================
-- Forum application tables
-- ==========================================

alter table forum_subject add open boolean;
alter table forum_subject ALTER COLUMN open SET DEFAULT 't';
alter table forum_subject add post_it boolean;
alter table forum_subject ALTER COLUMN post_it SET DEFAULT 'f';

ALTER table forum_message rename column content to message;
ALTER table forum_message add modifier_id int4;
ALTER table forum_message add constraint "forum_message_modifier_id" FOREIGN KEY (modifier_id) REFERENCES m_group_rowids (rowid);


