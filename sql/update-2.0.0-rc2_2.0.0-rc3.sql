
ALTER TABLE m_mioga ADD COLUMN modify_ldap_password boolean;
ALTER TABLE m_mioga ADD COLUMN password_crypt_method varchar(128);

ALTER TABLE m_mioga ALTER COLUMN modify_ldap_password SET DEFAULT NULL;
ALTER TABLE m_mioga ALTER COLUMN password_crypt_method SET DEFAULT 'crypt';

UPDATE m_mioga SET modify_ldap_password = FALSE, password_crypt_method = 'crypt';

UPDATE m_user_base SET password = '{crypt}' || password;

create table m_home_url (
    rowid               serial UNIQUE NOT NULL,
    created             timestamp NOT NULL,
    modified            timestamp NOT NULL,
    home_url            text,
    user_id             int4,
    FOREIGN KEY (user_id) REFERENCES m_group_rowids (rowid)
);

INSERT INTO m_home_url (created, modified, home_url, user_id) SELECT now(), now(), '', rowid FROM m_user_base;

