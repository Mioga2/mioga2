#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  update-2.4.7_2.4.8.pl
#
#        USAGE:  ./update-2.4.7_2.4.8.pl --help
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  15/05/2012 14:36
#
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description: 

=head1 NAME

update-2.4.7_2.4.8.pl

=head1 DESCRIPTION

Database upgrade script for Mioga version 2.4.8.

=head1 SYNOPSIS

update-2.4.7_2.4.8.pl [options]

 --help          show this help message
 --debug         enable debugging messages

=head1 METHODS DESRIPTION

=cut

#
#===============================================================================

use strict;
use warnings;

use Getopt::Long qw/:config auto_help/;
use Pod::Usage;
use Error qw/:try/;

use Data::Dumper;

use Mioga2::Exception::Simple;
use Mioga2::MiogaConf;
use Mioga2::Database;


#-------------------------------------------------------------------------------
# Set default options and parse command line
#-------------------------------------------------------------------------------
my %options = (
		help    => sub { pod2usage (-verbose => 99, -sections => 'DESCRIPTION|SYNOPSIS', -exit => 0) },
		conf    => '/var/lib/Mioga2/conf/Mioga.conf',
	);
GetOptions (\%options, 'help', 'debug', 'conf=s') or pod2usage (-verbose => 99, -sections => 'DESCRIPTION|SYNOPSIS', -exit => 2);


#-------------------------------------------------------------------------------
# Load configuration and connect to database
#-------------------------------------------------------------------------------
my $miogaconf = new Mioga2::MiogaConf ($options{conf});
# my $dbh = $miogaconf->GetDBH ();
my $db = $miogaconf->GetDBObject ();


#-------------------------------------------------------------------------------
# Alter schema
#-------------------------------------------------------------------------------
$db->Execute ('ALTER TABLE m_uri ADD COLUMN directory_id int4;');
$db->Execute ('ALTER TABLE m_uri ADD CONSTRAINT m_uri_directory_id FOREIGN KEY (directory_id) REFERENCES m_uri(rowid);');
$db->Execute ('CREATE UNIQUE INDEX uri_directory_idx ON m_uri (uri, directory_id);');


#-------------------------------------------------------------------------------
# Loop through each directory and update children
#-------------------------------------------------------------------------------
$db->Execute ("SELECT * FROM m_uri WHERE mimetype = 'directory' ORDER BY uri DESC;");
while (my $row = $db->Fetch ()) {
	print "Working on directory $row->{uri}\n" if ($options{debug});

	my $request = 'UPDATE m_uri SET directory_id = ? WHERE uri LIKE ? AND directory_id IS NULL;';
	my $args = [$row->{rowid}, $row->{uri} . '/%'];
	$db->ExecSQL ($request, $args);
}
