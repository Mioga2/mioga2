-- ============================================================================
-- Add index on m_uri table for SIMILAR TO requests
-- ============================================================================

CREATE INDEX m_uri_uri_text_index on m_uri (uri text_pattern_ops);
