CREATE OR REPLACE FUNCTION check_group_base_default_profile_id () RETURNS trigger AS '
    DECLARE
        rec record;
    BEGIN
        SELECT * INTO rec FROM m_group_base WHERE default_profile_id = OLD.rowid;
        IF FOUND THEN
            RAISE EXCEPTION ''Profile rowid % is still referenced in m_group_base'', OLD.rowid;
            RETURN NULL;
        END IF;

        RETURN OLD;
    END;
' LANGUAGE 'plpgsql';

CREATE TRIGGER m_group_base_default_profile_id_check
     BEFORE DELETE ON m_profile FOR EACH ROW
     EXECUTE PROCEDURE check_group_base_default_profile_id ();
