--
-- Remove obsolete status
--

UPDATE m_group SET status_id = (SELECT rowid FROM m_group_status WHERE ident = 'active')
WHERE  m_group.status_id = m_group_status.rowid AND m_group_status.ident = 'deleted'; 

DELETE FROM m_group_status    WHERE ident = 'deleted';


UPDATE m_resource SET status_id = (SELECT rowid FROM m_resource_status WHERE ident = 'disabled')
WHERE  m_resource.status_id = m_resource_status.rowid AND m_resource_status.ident = 'deleted'; 

DELETE FROM m_resource_status WHERE ident = 'deleted';


UPDATE m_user_base SET status_id = (SELECT rowid FROM m_user_status WHERE ident = 'disabled')
WHERE  m_user_base.status_id = m_user_status.rowid AND m_user_status.ident = 'purged'; 

DELETE FROM m_user_status     WHERE ident = 'purged';


--
-- Add Persistent Session table
--
create table m_persistent_session (
    session_id      varchar(128) UNIQUE NOT NULL,
    data 	    oid NOT NULL
);
create index m_persistent_session_index on m_persistent_session (session_id);
create index m_persistent_session_data_index on m_persistent_session (data);


--
-- Update PgSQL  Function for authentication
--

CREATE OR REPLACE FUNCTION authzTestAccessForURI(VARCHAR, INTEGER) RETURNS INTEGER AS '
        DECLARE 
                uriToCheck ALIAS FOR $1;
                userId ALIAS FOR $2;
                myrecord RECORD;

        BEGIN
        --
        -- Search authorization for profile.
        -- 
 
	SELECT INTO myrecord uri_id, max(accesskey) AS accesskey FROM (

	SELECT m_uri.rowid AS uri_id, (50 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_user
	WHERE  m_uri.uri = uriToCheck AND
       	       m_authorize.uri_id = m_uri.parent_uri_id AND
	       m_authorize_user.authz_id = m_authorize.rowid AND
	       m_authorize_user.user_id = userId

	UNION

	SELECT m_uri.rowid AS uri_id, (40 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_team, m_group_group
	WHERE  m_uri.uri = uriToCheck AND
	       m_authorize.uri_id = m_uri.parent_uri_id AND
	       m_authorize_team.authz_id = m_authorize.rowid AND
	       m_group_group.group_id = m_authorize_team.team_id AND
	       m_group_group.invited_group_id = userId

	UNION

	SELECT m_uri.rowid AS uri_id, (30 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_profile, m_profile_group
	WHERE  m_uri.uri = uriToCheck AND
	       m_authorize.uri_id = m_uri.parent_uri_id AND
	       m_authorize_profile.authz_id = m_authorize.rowid AND
	       m_profile_group.profile_id = m_authorize_profile.profile_id AND
	       m_profile_group.group_id = userId

	UNION

	SELECT m_uri.rowid AS uri_id, (20 + m_authorize.access) AS accesskey FROM m_authorize, m_uri, m_authorize_profile, m_profile_expanded_user
	WHERE  m_uri.uri = uriToCheck AND
	       m_authorize.uri_id = m_uri.parent_uri_id AND
	       m_authorize_profile.authz_id = m_authorize.rowid AND
	       m_profile_expanded_user.profile_id = m_authorize_profile.profile_id AND
	       m_profile_expanded_user.user_id = userId

	UNION

	SELECT m_uri.rowid AS uri_id, 10 AS accesskey FROM m_uri WHERE m_uri.uri = uriToCheck
	
	
	) AS tmp_req

	GROUP BY (uri_id);


	IF FOUND THEN
		return (myrecord.accesskey % 10);
	END IF;

	return NULL;

        END;

' LANGUAGE 'plpgsql';


--
-- Remove obsolete column
--

ALTER TABLE m_mioga DROP COLUMN showPrivateGroups;
ALTER TABLE m_group_base DROP COLUMN private;


--
-- Add new column
--

ALTER TABLE m_uri ADD COLUMN stop_inheritance boolean ;
UPDATE m_uri SET stop_inheritance = FALSE;
ALTER TABLE m_uri ALTER COLUMN stop_inheritance SET NOT NULL;
ALTER TABLE m_uri ALTER COLUMN stop_inheritance SET DEFAULT FALSE ;

--
-- Convert authorization tables
--
CREATE OR REPLACE FUNCTION convert_authz() RETURNS void AS '

DECLARE
   authz RECORD;
BEGIN

FOR authz IN    SELECT m_authorize.*, notdef.rowid AS notdef_rowid
                FROM m_authorize, (SELECT * FROM m_authorize WHERE is_default IS FALSE) AS notdef 
                WHERE m_authorize.is_default IS TRUE AND
                      notdef.uri_id = m_authorize.uri_id AND
                      notdef.access = m_authorize.access
LOOP

  UPDATE m_authorize_profile SET authz_id = authz.notdef_rowid WHERE authz_id = authz.rowid;
  UPDATE m_authorize_user    SET authz_id = authz.notdef_rowid WHERE authz_id = authz.rowid;
  UPDATE m_authorize_team    SET authz_id = authz.notdef_rowid WHERE authz_id = authz.rowid;

  DELETE FROM m_authorize WHERE rowid = authz.rowid AND is_default IS TRUE;

END LOOP;

RETURN;

END;
' LANGUAGE plpgsql;

SELECT convert_authz();

DROP FUNCTION convert_authz();

ALTER TABLE m_authorize DROP COLUMN is_default;
CREATE UNIQUE INDEX m_authorize_uri_access_index on m_authorize (uri_id, access);

--
-- Add en_US language
--

INSERT INTO m_theme_lang (created, modified, name, ident, theme_id) 
	SELECT now(), now(), 'English', 'en_US', m_theme.rowid FROM m_theme WHERE name IN ('MiogaII', 'MiogaII Blue');
