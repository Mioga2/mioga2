#!/usr/bin/perl -w

use GD;
use Data::Dumper;
use strict;

if(@ARGV != 10) {
	print "Usage : create_button.pl output_file text path_to_ttf_font fontsize\n". 
	      "                         left_right_margin top_bottom_margin\n".
	      "                         border_size border_rgb_bolor text_rgb_color background_rgb_color\n";

	exit(-1);
}

my $outputfile = $ARGV[0];
my $text = $ARGV[1];
my $font = $ARGV[2];
my $fontsize = $ARGV[3];
my $border_size = $ARGV[4];

my $left_right_margin = $ARGV[5];
my $top_bottom_margin = $ARGV[6];

# Process height from the M letter to have button heigth not dependent of
# the word. (problem with letter like j, p, etc).
my @bound_all = GD::Image->stringFT(0, $font, $fontsize, 0, 0, 0, "M");
my $height =  ($bound_all[1] - $bound_all[7]) + 2 * $top_bottom_margin + 2*$border_size;

# Process width from complete word
@bound_all = GD::Image->stringFT(0, $font, $fontsize, 0, 0, 0, $text);
my $width = ($bound_all[2] - $bound_all[0]) + 2 * $left_right_margin + 2*$border_size + 1;

my $image = new GD::Image($width,$height);

my ($border_r, $border_g, $border_b) = ($ARGV[7] =~ /(\d+),(\d+),(\d+)/);
my ($fg_r, $fg_g, $fg_b) = ($ARGV[8] =~ /(\d+),(\d+),(\d+)/);
my ($bg_r, $bg_g, $bg_b) = ($ARGV[9] =~ /(\d+),(\d+),(\d+)/);

my $border = $image->colorAllocate($border_r, $border_g, $border_b);
my $fg = $image->colorAllocate($fg_r, $fg_g, $fg_b);
my $bg = $image->colorAllocate($bg_r, $bg_g, $bg_b);

# make the background transparent and not interlaced
$image->interlaced('false');

$image->filledRectangle(0, 0, $width, $height, $bg);

for (my $i = $border_size; $i > 0; $i--) { 
	$image->rectangle($i-1, $i-1, $width - $i, $height - $i, $border);
}

my @res = $image->stringFT($fg, $font, $fontsize, 0, $left_right_margin + $border_size - $bound_all[0], $height - $top_bottom_margin - $border_size - int($bound_all[1]/2), $text);

if(!@res) {
	die $@;
}

my $pngdata = $image->png;

open(F_PNGFILE, ">$outputfile.png") 
	or die "Can't open $outputfile : $!";

binmode F_PNGFILE;
print F_PNGFILE $pngdata;
close(F_PNGFILE);

system("convert $outputfile.png $outputfile");
unlink("$outputfile.png");
