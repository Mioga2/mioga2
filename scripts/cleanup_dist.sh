#!/bin/sh
#
# This script is intended to be run once before creating the distribution tar-gz.
# Its goal is to remove all debug flags and Javascript console logs that are useful to developers but should not appear in prod.

# Set all debug flags to 0 in Perl modules
find lib/ -name "*.pm" -exec sed -i s/"^\([[:space:]]*my[[:space:]]*\$debug[[:space:]]*=[[:space:]]*\)1\([[:space:]]*;[[:space:]]*$\)$"/"\10\2"/ {} \;

# Remove all console.* lines from Javascript code
find web/themes/default/javascript -name "*.js" -exec sed -i /"^[[:space:]]*console.\(log\|dir\|debug\)[[:space:]]*("/d {} \;

# Remove all console.* lines from Javascript code included into XSL stylesheets
find web/themes/default/xsl -name "*.xsl" -exec sed -i /"^[[:space:]]*console.\(log\|dir\|debug\)[[:space:]]*("/d {} \;
