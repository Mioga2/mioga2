#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2012 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
# Description:
#
# This script generates a cvs user. He receives arguments: 
# 	count of users will be created
# 	option : --colbert that generate csv users without squelFile. The default value is "bottin".
# 	option : --nofixed that generate csv users with a exclusively random name.
# The format of csv is : "LastName","FirstName","Email","Ident","Password","squelFile"\n
# ============================================================================
use strict;
use utf8;
use Getopt::Long;

my $debug = 0;

my $lengthOfName = 8;
my @FirstNames = ("Joseph", "Enzo",  "Ferdinand-Bernard","Denis", "Armand", "Jean" ,"Donald" , "Françoise", "Sandor", "Paul-Laurent", "Didier", "Jacques", "Michel", "Didier" , "Karl", "Frédéric", "Gilles", "Harold", "Maria", "Nicolas");
my @LastNames = ("Breuer", "Dupont", "C'abanier", "Ledore", "Caestecker", "Pachoud", "Winnicott", "Dolto", "Ferenczi", "Assoun", "Anzieu", "Lacan", "Foucault", "Bourdieu", "Marx", "Nietzsche", "Deleuze", "Searles", "Török", "Abraham" );
my @chars=('a'..'z','A'..'Z','0'..'9','_','&','~','#','$','ø','}','%','>', "'");
my @charsForIdent=('a'..'z','A'..'Z','0'..'9');

# ============================================================================
# Function that generates csvs users.
# The argument is a count of users will be generating.
# ============================================================================
sub generate_csv {
	my ($countUser, $appli, $no_fixed) = @_;

	for (my $user = 0; $user < $countUser; $user ++) {
		my $lineCsv="";
		my $ident="";
		my $mail="";
		foreach (1..$lengthOfName) {
			$ident .= $charsForIdent[rand @charsForIdent];
		}
		if ($no_fixed == 0 && $user < scalar(@FirstNames)) {
			my $mail = $FirstNames[$user] . "." . $LastNames[$user] . "\@test-mioga.fr";
			$mail =~ s/é/e/g;
			$mail =~ s/ö/o/g;
			$mail =~ s/ç/c/g;
			$lineCsv .= 	"\"" . $LastNames[$user] . "\"," #lastname
							. "\"" . $FirstNames[$user] . "\"," #firstname
							. "\"" . $mail . "\"," # email
							. "\"" . $ident . "\"," # ident
							. "\"" . $FirstNames[$user] . "\","; # password
			if ($appli eq "colbert") {
				$lineCsv .= "\"" . "50-standard.xml" . "\","; # skeleton
				$lineCsv .= "\n";
			} else {
				$lineCsv .= "\n";
			}
			print $lineCsv;
		} else {
			my $firstName="";
			my $lastName="";
			my $mailFirst="";
			my $mailLast="";
			foreach (1..$lengthOfName) {
				$firstName .= $chars[rand @chars];
				$lastName .= $chars[rand @chars];
				$mailFirst .=  $charsForIdent[rand @charsForIdent];
				$mailLast .=  $charsForIdent[rand @charsForIdent];
			}
			$lineCsv .= 	"\"" . $lastName . "\"," #lastname
							. "\"" . $firstName . "\","#firstname
							. "\"" . $mailFirst . "." . $mailLast . "\@test-mioga.fr" . "\"," # email
							. "\"" . $ident . "\"," # ident
							. "\"" . $firstName . "\","; # password
			if ($appli eq "colbert") {
				$lineCsv .= "\"" . "50-standard.xml" . "\","; # skeleton
				$lineCsv .= "\n";
			} else {
				$lineCsv .= "\n";
			}
			print $lineCsv;
		}
	}
}
# ============================================================================
# Main : Call function that generate csv if the argument is defined.
# ============================================================================
binmode(STDOUT, ":utf8");
binmode(STDIN, ":encoding(utf8)");

# ================================================================
# Get args and conf
# ================================================================

my $help = 0;
my $appli = "bottin";
my $no_fixed = 0;
my $colbert = 0;
my $result = GetOptions ("help" => \$help,
						"colbert" => \$colbert,
			          	"no-fixed"  => \$no_fixed
			          );

print STDERR "Getopt result = $result\n" if ($debug);

$appli = "colbert" if ($colbert);

if ( ($help) || (@ARGV < 1) ) {
	print STDERR "Usage : perl csvGen.pl [options] countUsers\n";
	print STDERR "    Options :\n";
	print STDERR "        --colbert    : default to bottin\n";
	print STDERR "        --no-fixed   : default to fixed\n";
	print STDERR "        --help       : print usage\n";
	exit(-1);
}

my $countUser = $ARGV[0];

generate_csv ($countUser,$appli, $no_fixed);
