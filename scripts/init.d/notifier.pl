#! /bin/sh
# ============================================================================
# MIOGA Project (C) 2009 Alixen
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

NAME=mioga2
DESC="Mioga2 notifier daemon"



set -e

case "$1" in
  start)
        echo -n "Starting $DESC: $NAME"

        if [ ! -p /var/spool/mioga/notifier ]; then
            mkfifo -m 600 /var/spool/mioga/notifier
            chown www-data.www-data /var/spool/mioga/notifier
        fi
		if [ ! -d /var/run/mioga ]; then
			mkdir /var/run/mioga
		fi
        start-stop-daemon -b --start --quiet --make-pidfile -c www-data:www-data \
                --exec `which notifier.pl` --pidfile /var/run/mioga/notifier.pid -- /var/lib/Mioga2/conf/Mioga.conf

        echo "."

        ;;
  stop)
        echo -n "Stopping $DESC: $NAME"

        start-stop-daemon --stop --quiet --pidfile /var/run/mioga/notifier.pid

        echo "."
        ;;
  *)
        N=/etc/init.d/$NAME
        # echo "Usage: $N {start|stop|restart|reload|force-reload}" >&2
        echo "Usage: $N {start|stop}" >&2
        exit 1
        ;;
esac

exit 0
