#!/usr/bin/perl -w
#
# Extract messages from Perl modules (located in lib/)
# to generate PO files under locales/
#

use strict;
use File::Find;
use Locale::PO;
use Data::Dumper;

my $basedir = 'lib/Mioga2/';
my $localesdir = 'locales/';
my @locales = qw/fr_FR en_US/;

my %options = (
	'wanted' => \&Process,
	'no_chdir' => 1,
);


sub Stats {
	my ($file) = @_;

	my $translated = 0;
	my $untranslated = 0;

	my $po = Locale::PO->load_file_ashash($file);
	while (my ($key, $value) = each (%{$po})) {
		next if ($value->{msgid} eq '""');
		if (($value->{msgstr} ne '""')) {
			$translated++;
		}
		else {
			$untranslated++;
		}
	}

# 	print "Translated: $translated\nUntranslated: $untranslated\n";
	return ($translated, $untranslated);
}

sub Process {
	# only operate on Perl modules
	/\.pm$/ or return;

	# Path to Perl module
	my ($pmfile) = ($_ =~ m/$basedir(.*)$/);
	$pmfile =~ s#^/##;
# 	print "$pmfile\n";

	# PO file name
	my $pofile = lc ($pmfile);
	$pofile =~ s#/#_#g;
	$pofile =~ s/\.pm$//;
# 	print "$pofile\n";

	foreach my $locale (@locales) {
		my $fpath = "locales/$locale/$pofile.po";
		my ($before) = &Stats ($fpath);
		my $flags = '--keyword=__x --keyword=__ --keyword=__xn --keyword=__n';
		$flags .= " -j " if (-e $fpath);
		$flags .= " --package-name=$pofile";
		$flags .= " --copyright-holder=Alixen";
		my $cmd = "xgettext $flags -o $fpath $basedir/$pmfile";
# 		print "$cmd\n";
		system ($cmd);
		my ($after, $untranslated) = &Stats ($fpath);
		if ($before != $after) {
			print STDERR "Number of translated strings differ for file $fpath\n";
		}
		if ($untranslated == 0) {
			# Dirty hack. If no new translations, file header is updated by xgettext so file is modified but has nothing to translate
			# Delete it, it will be restored by svn update at the end of the script.
			print "$fpath not changed, resetting it.\n";
			unlink $fpath;
		}
# 		print "$fpath $before $after ($untranslated)\n";
	}
}


find(\%options, $basedir);
system ("svn update");
