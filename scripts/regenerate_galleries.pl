#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  regenerate_galleries.pl
#
#        USAGE:  ./regenerate_galleries.pl /path/to/Mioga.conf instance
#
#  DESCRIPTION:  Regenerate Louvre galleries for instance.
#                WARNING: The instance admin MUST have write access to galleries
#                for this to work.
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  19/04/2012 09:20
#
#===============================================================================
#
#  Copyright (c) year 2012, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================

use strict;
use warnings;

use Data::Dumper;
use File::Basename;

use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::URI;
use Mioga2::tools::Convert;

my $debug = 1;

if (@ARGV < 2) {
	print "Usage: " . basename($0) . " /path/to/Mioga.conf <instance_name>\n";
	exit (1);
}

my $miogaconf = new Mioga2::MiogaConf (shift);
my $dbh = $miogaconf->GetDBH ();

my $instname = shift;
my $config = new Mioga2::Config ($miogaconf, $instname) or die "No such instance";
my $db = $config->GetDBObject ();

$db->Execute ("SELECT m_uri.uri FROM m_uri, m_louvre, m_group WHERE m_uri.rowid = m_louvre.uri_id AND m_uri.group_id = m_group.rowid AND m_group.mioga_id = ?;", [$config->GetMiogaId ()]);
while (my $rec = $db->Fetch ()) {
	my $uri = $rec->{uri};

	print STDERR "Processing gallery $uri\n" if ($debug);
	my $data = {
		mioga_id => $config->GetMiogaId (),
		group_id => $rec->{group_id},
		user_id  => $config->GetAdminId (),
		type     => ['Louvre'],
		uris => [$uri],
	};

	$config->WriteNotifierFIFO (Mioga2::tools::Convert::PerlToJSON ($data) . "\n");
}
