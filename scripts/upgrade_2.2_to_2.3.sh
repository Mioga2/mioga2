#!/bin/sh
#
# Mioga2-2.2 to Mioga2-2.3 filesystem structure update script

# Default Mioga.conf path, can be overridden by command-line argument
miogaconf='/var/lib/Mioga2/conf/Mioga.conf';

# Get path to Mioga.conf from command-line if exists
if [ ! -z $1 ]; then
	miogaconf=$1
fi

# No suitable Mioga.conf, exit
if [ ! -f $miogaconf ]; then
	echo "Please provide path to Mioga.conf"
	exit 1;
fi

# Get Mioga install dir
install_dir=`grep install_dir $miogaconf | sed s/"<install_dir>\(.*\)<\/install_dir>"/"\1"/`

for instance in `mioga2_info.pl --conf $miogaconf instances`; do
	echo "Upgrading instance $instance"
	rm -Rf $install_dir/$instance/web/xsl/default
	ln -s $install_dir/conf/xsl/ $install_dir/$instance/web/xsl/default
	rm -Rf $install_dir/$instance/web/lang
	ln -s $install_dir/conf/lang $install_dir/$instance/web/lang
	rm -Rf $install_dir/$instance/web/htdocs/themes/default
	ln -s $install_dir/conf/themes/default $install_dir/$instance/web/htdocs/themes/default
	rm -Rf $install_dir/$instance/web/htdocs/jslib
	ln -s $install_dir/conf/jslib $install_dir/$instance/web/htdocs/jslib
	rm -Rf $install_dir/$instance/web/htdocs/help
	ln -s $install_dir/conf/help $install_dir/$instance/web/htdocs/help
	cp -R $install_dir/conf/skel/ $install_dir/$instance/MiogaFiles/

	# Replace Admin application with Colbert
	./bin/replace_app.pl --conf $miogaconf --instance $instance --old-app Admin --new-app Colbert --verbose --delete
done

# Replace MiogaInstance application with ColbertSA for instance that have the application
for instance in `mioga2_info.pl --conf $miogaconf instanceswithapp MiogaInstance`; do
	miogadm.pl --conf $miogaconf --instance $instance -c "addapp Mioga2::ColbertSA"
	./bin/replace_app.pl --conf $miogaconf --instance $instance --old-app MiogaInstance --new-app ColbertSA --verbose --delete
done
