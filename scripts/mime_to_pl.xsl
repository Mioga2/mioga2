<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ext="urn:ext" xmlns:mioga="urn:mioga">

<xsl:output method="text" indent="no" omit-xml-declaration="yes" encoding="UTF-8" media-type="application/x-perl"/>

<xsl:template match="/">
{
  <xsl:for-each select="//mime-info/mime-type">
    '<xsl:value-of select="@type"/>'  => {
      image       => 'file',
      description => __("<xsl:value-of select='comment'/>"),
    },
  </xsl:for-each>
};
</xsl:template>

</xsl:stylesheet>
