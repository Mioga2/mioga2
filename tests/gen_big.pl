#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2003 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description: 
#   This script populates database with users, teams, groups, resources
#   and set some task in organizers and create files

use strict;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::APIApplication;
use Mioga2::tools::APIProfile;
use Error qw(:try);
use Term::ReadLine;
use Text::Iconv;
use Data::Dumper;
use File::Basename;
use File::Copy;

my $debug = 0;

my %created_users;
my %created_resources;
my %created_teams;
my %created_groups;

# Different handeled parameters
my @params = qw/users users-no-private groups teams resources files/;

# Hash to store generation parameters (number of users, groups, teams)
my %params = (
	'users'            => 2000,
	'users-no-private' => 1000,
	'groups'           => 100,
	'teams'            => 50,
	'resources'        => 100
);


# ================================================================
# ================================================================
# GetInstance : Get id and values for instance
# ================================================================
sub GetInstance
{
	my ($dbh, $ident) = @_;

	my $sql = "select * from m_mioga where ident='$ident'";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		print "Instance : $ident not known\n";
		exit (-1);
	}
	return $res->{rowid};
}
# ================================================================
# EnableAppInGroup : Enable an application in group
# ================================================================
sub EnableAppInGroup
{
	my ($dbh, $group_id, $app_id) = @_;
	# ----------------------------------
	# Get profiles for group
	# ----------------------------------
	my $sql = "select m_profile.rowid from m_profile where group_id=$group_id";

	my $profiles = SelectMultiple($dbh, $sql);
	if (!defined($profiles)) {
		print "Database error\n";
		return;
	}
	# ----------------------------------
	# Add apps if not present
	# ----------------------------------
	ApplicationEnableInGroup($dbh, $app_id, $group_id);
	# ----------------------------------
	# Get functions for application
	# ----------------------------------
	$sql = "select rowid from m_function where application_id=$app_id";

	my $functions = SelectMultiple($dbh, $sql);
	if (!defined($functions)) {
		print "Database error\n";
		return;
	}
	# ----------------------------------
	# Set profiles
	# ----------------------------------
	foreach my $prof (@$profiles) {
		foreach my $func (@$functions) {
			ProfileCreateFunction($dbh, $prof->{rowid}, $func->{rowid});
		}
	}
}
# ================================================================
# GetAppIdForApplication
# ================================================================
sub GetAppIdForApplication
{
	my ($dbh, $ident, $mioga_id) = @_;
	my $sql = "select rowid from m_application where ident='$ident' AND mioga_id=$mioga_id";

	my $res = SelectSingle($dbh, $sql);
	return $res->{rowid};
}

# ================================================================
# RecordLogin : fill m_group_user_last_conn so the user
#       is considered as already being logged in
# ================================================================
sub RecordLogin {
	my ($dbh, $user_id, $group_id) = @_;

	my %values = ('last_connection' => du_GetNowISO());
	my $sql = BuildUpdateRequest(\%values, table  => 'm_group_user_last_conn',
									string => [ qw(last_connection) ],
									);

	$sql .= " WHERE user_id = $user_id AND group_id = $group_id";

	ExecSQL($dbh, $sql);
	
}

# ================================================================
# CreateUsers
# ================================================================
sub CreateUsers
{
	my ($dbh, $config, $mioga_id) = @_;

	# ----------------------------------
	# Get admin id for current instance
	# ----------------------------------
	my $sql = "select admin_id from m_mioga where rowid=$mioga_id";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
		return;
	}
	my $admin_id = $res->{admin_id};
	$created_users{admin} = $admin_id;

	# ----------------------------------
	# Gennerate dummy users
	# ----------------------------------
	for (my $i = 1; $i <= $params{users}; $i++) {
		my $values = {};
		$values->{autonomous} = 1;
		$values->{public_part} = 0;
		$values->{ident}      = "user$i";
		$values->{firstname}  = "User$i";
		$values->{lastname}   = "Dummy$i";
		$values->{email}      = "user$i.dummy$i\@noname.org";
		$values->{password}   = "user$i";
		$values->{skeleton}   = "Standard.xml";
		$values->{creator_id} = $admin_id;

		my $user_id = UserLocalCreate($config, $values);
		&RecordLogin ($dbh, $user_id, $user_id);
		print "Created user $values->{ident} : $user_id\n";
		$created_users{$values->{ident}} = $user_id;
		if ($params{files}) {
			AddFiles ($values->{ident}, $values->{ident});
		}
	}

	for (my $i = 1; $i <= $params{'users-no-private'}; $i++) {
		my $values = {};
		$values->{autonomous} = 1;
		$values->{public_part} = 0;
		$values->{ident}      = "s$i";
		$values->{firstname}  = "s$i";
		$values->{lastname}   = "S-User$i";
		$values->{email}      = "suser$i\@noname.org";
		$values->{password}   = "s$i";
		$values->{skeleton}   = "WithoutPrivate.xml";
		$values->{creator_id} = $admin_id;

		my $user_id = UserLocalCreate($config, $values);
		&RecordLogin ($dbh, $user_id, $user_id);
		print "Created user $values->{ident} : $user_id\n";
		$created_users{$values->{ident}} = $user_id;
	}
}
# ================================================================
# CreateResources
# ================================================================
sub CreateResources
{
	my ($dbh, $config, $mioga_id) = @_;

	my $admin_id = $created_users{admin};
	# ----------------------------------
	# Read file and create users
	# ----------------------------------
	for (my $i = 1; $i <= $params{'resources'}; $i++) {
		my $values = {};
		$values->{ident}      = "resource$i";
		$values->{description} = "Resource $i";
		$values->{location}   = "Location resource $i";
		$values->{anim_id}    = $created_users{"user$i"};
		$values->{creator_id} = $admin_id;

		my $res_id = ResourceCreate($config, $values);
		print "Created resource $values->{ident} : $res_id\n";
		$created_resources{$values->{ident}} = $res_id;
	}
}
# ================================================================
# CreateTeams
# ================================================================
sub CreateTeams
{
	my ($dbh, $config, $mioga_id) = @_;

	my $max_users_per_team = int ($params{'users'} / $params{'teams'});
	print "Max users per team: $max_users_per_team\n";

	my $admin_id = $created_users{admin};
	# ----------------------------------
	for (my $i = 1; $i <= $params{'teams'}; $i++) {
		my $values = {};
		$values->{ident}      = "team_$i";
		$values->{description} = "Team $i";
		$values->{creator_id} = $admin_id;

		my $team_id = TeamCreate($config, $values);

		for (my $j = 1; $j <= $max_users_per_team; $j++) {
			my $user_id = int (($i - 1) * $max_users_per_team + $j);
			print "Inviting user $user_id to team $i\n" if ($debug);
			GroupInviteGroup($dbh, $team_id, $created_users{"user$user_id"});
		}

		print "Created team $values->{ident} : $team_id\n";
		$created_teams{$values->{ident}} = $team_id;
	}
	my $values = {};
	$values->{ident}      = "Tous";
	$values->{description} = "Tout le monde";
	$values->{creator_id} = $admin_id;

	my $team_id = TeamCreate($config, $values);

	foreach my $u (keys(%created_users)) {
		if ($created_users{$u} != $admin_id) {
			GroupInviteGroup($dbh, $team_id, $created_users{$u});
		}
	}

	print "Created team $values->{ident} : $team_id\n";
	$created_teams{$values->{ident}} = $team_id;
}

# ================================================================
# AddFiles
# ================================================================
sub AddFiles {
	my ($group, $user, $location) = @_;

	$location = 'home' if (!$location);

	print "Adding files as user $user to group $group (location=$location)\n" if ($debug);

	my $uri = "http://localhost/Mioga2/$params{instance}/$location/$group";

	my $cmdpath = dirname($0);
	system "$cmdpath/put_files.pl -r -v --user $user --passwd $user $params{files} $uri";
}

# ================================================================
# CreateGroups
# ================================================================
sub CreateGroups
{
	my ($dbh, $config, $mioga_id) = @_;

	my $max_users_per_group = int (($params{'users'} + $params{'users-no-private'}) / $params{'groups'});
	my $max_teams_per_group = int ($params{'teams'} / $params{'groups'});
	print "Max users per group: $max_users_per_group\n";
	print "Max teams per group: $max_teams_per_group\n";

	my $admin_id = $created_users{admin};
	# ----------------------------------
	# ----------------------------------
	for (my $i = 1; $i <= $params{'groups'}; $i++) {
		my $values = {};
		$values->{ident}      = "group$i";
		$values->{description} = "Group $i";
		$values->{creator_id} = $admin_id;
		$values->{anim_id} = $admin_id;
		$values->{skeleton}   = "Standard.xml";

		my $group_id = GroupCreate($config, $values);
		if ($params{files}) {
			AddFiles ($values->{ident}, 'admin');
		}

		for (my $j = 1; $j <= $max_users_per_group; $j++) {
			my $id = int (($i - 1) * $max_users_per_group + $j);
			my $user_id = "user$id";
			if ($id > $params{'users'}) {
				$user_id = "s" . ($id - $params{'users'});
			}
			print "Inviting user $user_id to group $i\n" if ($debug);
			GroupInviteGroup($dbh, $group_id, $created_users{$user_id});
			&RecordLogin ($dbh, $created_users{$user_id}, $group_id);
		}
		for (my $j = 1; $j <= $max_teams_per_group; $j++) {
			my $team_id = int (($i - 1) * $max_teams_per_group + $j);
			print "Inviting team $team_id to group $i\n" if ($debug);
			GroupInviteGroup($dbh, $group_id, $created_teams{"team_$team_id"});
		}

		AddApplicationsInGroups($dbh, $config, $mioga_id, $group_id);
		print "Created group $values->{ident} : $group_id\n";
		$created_groups{$values->{ident}} = $group_id;
	}
	my $values = {};
	$values->{ident}      = "commun";
	$values->{description} = "Tout le monde";
	$values->{creator_id} = $admin_id;
	$values->{anim_id} = $admin_id;
	$values->{skeleton}   = "Standard.xml";

	my $group_id = GroupCreate($config, $values);

	GroupInviteGroup($dbh, $group_id, $created_teams{Tous});

	print "Created group $values->{ident} : $group_id\n";
	$created_groups{$values->{ident}} = $group_id;
}
# ================================================================
# AddApplicationsInGroups
# ================================================================
sub AddApplicationsInGroups
{
	my ($dbh, $config, $mioga_id, $group_id) = @_;
	print "AddApplications in group $group_id\n";

	my $admin_id = $created_users{admin};

	my $app_id = GetAppIdForApplication($dbh, 'Forum', $mioga_id);
	EnableAppInGroup($dbh, $group_id, $app_id);

	$app_id = GetAppIdForApplication($dbh, 'Articles', $mioga_id);
	EnableAppInGroup($dbh, $group_id, $app_id);

	$app_id = GetAppIdForApplication($dbh, 'News', $mioga_id);
	EnableAppInGroup($dbh, $group_id, $app_id);

	$app_id = GetAppIdForApplication($dbh, 'Planning', $mioga_id);
	EnableAppInGroup($dbh, $group_id, $app_id);
}

# ================================================================
# Display script usage
# ================================================================
sub Usage {
	print "Usage:\n";
	print basename ($0) . " INSTANCE [OPTIONS]\n";
	for (@params) {
		print "\t--$_ nb_$_\n" if ($_ ne 'files');
		print "\t--$_ path_to_files\n" if ($_ eq 'files');
	}
	print "\nDefault values: " . Dumper \%params;
}

# ================================================================
# Process command-line args
# ================================================================
sub ProcessArgs {
	my $arg = '';
	my $uninitialized = 1;

	# If at least a value is passed on the command-line, reset all values to zero
	if (grep (/^--/, @ARGV)) {
		for (@params) {
			$params{$_} = 0;
		}
	}

	# Process command-line arguments
	foreach (@ARGV) {
		if (/^--/) {
			my ($argname) = ($_ =~ m/^--(.*)/);
			if (grep (/^$argname$/, @params)) {
				$arg = $argname;
				$params{$arg} = 1 if ($arg eq 'debug');
			}
			else {
				print STDERR "Unknown argument $_\n";
				Usage;
				exit (-1);
			}
		}
		else {
			$arg = 'instance' if $arg eq '';
			$params{$arg} = $_;
			$arg = '';
		}
	}

	print STDERR "Parameters: " . Dumper \%params if ($debug);
}

# ================================================================
# Main program
# ================================================================

# Get args and initialize things
#
if(@ARGV < 1) {
	Usage;
	exit(-1);
}

ProcessArgs;

my $miogaconf_path = "/var/lib/Mioga2/conf/Mioga.conf";
my $miogaconf = new Mioga2::MiogaConf($miogaconf_path);
my $dbh = $miogaconf->GetDBH();
my $mioga_id = GetInstance($dbh, $params{instance});
my $config = new Mioga2::Config($miogaconf, $params{instance});


print "Populate instance \"$params{instance}\" (id = $mioga_id) with the following parameters:\n";
print "\t" . $params{'users'} . " users\n";
print "\t" . $params{'users-no-private'} . " users without private space\n";
print "\t" . $params{'groups'} . " groups\n";
print "\t" . $params{'teams'} . " teams\n";
print "\t" . $params{'resources'} . " resources\n";

try {
	CreateUsers($dbh, $config, $mioga_id) if ($params{'users'});
	CreateResources($dbh, $config, $mioga_id) if ($params{'resources'});
	CreateTeams($dbh, $config, $mioga_id) if ($params{'teams'});
	CreateGroups($dbh, $config, $mioga_id) if ($params{'groups'});
}
otherwise {
		my $err = shift;
		print STDERR "err = ".Dumper($err)."\n";
};



print "\n ... good bye ...\n";
