#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description: 
#   This script uploads files to Mioga2 using DAVProxy
#
#   TODO:
#         - Hide password while being typed

use strict;
use LWP::UserAgent;
use MIME::Base64;
use Data::Dumper;
use File::Basename;
use URI::Escape;

my $debug = 0;


# Internal variables
my ($prefix);

my @params = qw/user passwd url/;
my %params = ();

my %usage = (
	'-r'       => "recurse through directories",
	'-v'       => "increase verbosity",
	'--user'   => "user name used for authentication",
	'--passwd' => "password used for authentication"
);

my %flags = (
	'r' => 'recursive',
	'v' => 'verbose'
);

my $user_agent = new LWP::UserAgent(keep_alive => 0);


# ================================================================
# Display usage
# ================================================================
sub Usage {
	print "Usage: " . basename($0) . " [OPTION]... SRC [SRC]... DST\n";

	my @keys = sort { length $b <=> length $a } keys (%usage);
	my $length = length ($keys[0]);

	print "\nOptions:\n";
	for (sort { $a cmp $b } keys(%usage)) {
		print "  $_" . ' ' x ($length - length($_) + 4) . $usage{$_} . "\n";
	}
}

# ================================================================
# Prompt the user for authentication
# ================================================================
sub PromptAuthentication {
	my ($method, $url) = @_;

	print "Authentication required for: '$method' ($url)\n";
	print "User name: ";
	chomp ($params{user} = <STDIN>);
	print "Password: ";
	chomp ($params{passwd} = <STDIN>);
}

# ================================================================
# Escape URI string
# ================================================================
sub st_URIEscape {
	my ($string) = @_;

	#$string = uri_escape($string); # use default chars : "^A-Za-z0-9\-_.!~*'()"
	$string = uri_escape($string, "\x00-\x20\x7f-\xff#&|'%\=\?") unless $string =~ /%[A-Fa-f0-9]{2}/;

	return $string;
}

# ================================================================
# Execute HTTP request
# ================================================================
sub ExecuteRequest {
	my ($method, $url, $fh, $filesize) = @_;

	my $rc;

	$url = &st_URIEscape($url);

	{
		no warnings;
		local $HTTP::Request::Common::DYNAMIC_FILE_UPLOAD = 1;
	}
	my $request = new HTTP::Request($method, $url);
	$request->content_type('text/xml; charset="utf-8"');

	# Add authentication parameters to header
	if (defined ($params{user}) && defined ($params{passwd})) {
		my $encoded = encode_base64("$params{user}:$params{passwd}");
		$request->push_header(Authorization => "Basic $encoded");
	}

	# Add file content
	if ($fh) {
		$request->push_header('Content-Type' => "unknown/unknown");
		$request->push_header('Content-Length' => $filesize);
		$request->content(
				sub {
					my $data;
					my $l = sysread($fh, $data, 100000);
					if (defined($l)) {
						if ($l > 0) {
						return $data;
						}
						else {
							return;
						}
					}
					else {
						print STDERR "ExecuteRequestForFH() read error on filehandle\n";
					}
				}
			);
	}

	my $response = $user_agent->request($request);
	$rc = $response->code;

	print "Code: " . $response->code . "\n" if ($debug);
	print $response->content if ($debug);

	# Retry if authorization failed
	if ($rc == 401) {
		PromptAuthentication ($method, $url);
		$rc = ExecuteRequest ($method, $url);
	}

	if (!$response->is_success) {
		print "FAILED: " . $response->code . "\n";
		print $response->content;
	}

	return ($rc);
}

# ================================================================
# Process command-line arguments
# ================================================================
sub ProcessArgs {
	my $arg = '';

	# Initialize default flags values
	for (keys(%flags)) {
		$params{$flags{$_}} = 0;
	}

	# Process command-line arguments
	foreach (@ARGV) {
		if (/^--/) {
			my ($argname) = ($_ =~ m/^--(.*)/);
			if (grep (/^$argname$/, @params)) {
				$arg = $argname;
			}
			else {
				print STDERR "Unknown argument $_\n";
				Usage;
				exit (-1);
			}
		}
		elsif (/^-[^-]/) {
			my ($flagname) = ($_ =~ m/^-(.*)/);
			if (grep (/^$flagname$/, %flags)) {
				$params{$flags{$flagname}} = 1;
			}
			else {
				print STDERR "Unknown flag $_\n";
				Usage;
				exit (-1);
			}
		}
		else {
			if ($arg ne '') {
				$params{$arg} = $_;
			}
			else {
				push (@{$params{src}}, $_);
			}
			$arg = '';
		}
	}

	# Get last object as destination
	$params{dest} = pop (@{$params{src}});

	print STDERR "Parameters: " . Dumper \%params if ($debug);
}

# ================================================================
# Read a file
# ================================================================
sub ReadFile {
	my ($file) = @_;

	print "Sending file '$file'...\n" if ($params{verbose});

	my $filesize = -s "$prefix$file";
	open (my ($fh), "<$prefix$file");
	ExecuteRequest ('PUT', "$params{dest}/$file", $fh, $filesize);
	close ($fh);
}

# ================================================================
# Read a dir
# ================================================================
sub ReadDir {
	my ($dir) = @_;

	print "Sending directory '$prefix$dir'...\n" if ($params{verbose});

	ExecuteRequest ('MKCOL', "$params{dest}/$dir");

	if ($params{recursive}) {
		opendir (my ($dirp), "$prefix$dir") or die $!;
		while (my $obj = readdir($dirp)) {
			next if ($obj =~ m/^\.*$/);
			if ( -d "$prefix$dir/$obj" ) {
				ReadDir ("$dir/$obj");
			}
			elsif ( -f "$prefix$dir/$obj" ) {
				ReadFile ("$dir/$obj");
			}
			else {
				print STDERR "Unsupported type '$prefix$dir/$obj'\n";
			}
		}
		closedir($dirp);
	}
}


# Process command-line arguments
ProcessArgs;

if (!defined ($params{src}) || !defined ($params{dest})) {
	&Usage;
	exit (-1);
}

# Loop through each object
for (@{$params{src}}) {

	my $base = basename ($_);
	($prefix) = ($_ =~ m/(.*)$base/);

	if ( -e $_ ) {
		if ( -d $_ ) {
			ReadDir ($base);
		}
		elsif ( -f $_ ) {
			ReadFile ($base);
		}
		else {
			print STDERR "Unsupported type $_\n";
		}
	}
	else {
		print STDERR "Can't find $_\n";
	}
}
