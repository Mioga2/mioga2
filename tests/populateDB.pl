#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2003 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description: 
#   This script populates database with users, teams, groups, resources
#   and set some task in organizers and create files

use strict;
use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::tools::database;
use Mioga2::tools::string_utils;
use Mioga2::tools::APIGroup;
use Mioga2::tools::APIAuthz;
use Mioga2::tools::APIApplication;
use Mioga2::tools::APIProfile;
use Error qw(:try);
use Term::ReadLine;
use Text::Iconv;
use Data::Dumper;

my $debug = 0;

my %created_users;
my %created_resources;
my %created_teams;
my %created_groups;

my $users_def = [ [ qw(gaiparbal Gilles Aiparbal gilles.aiparbal@noname.org gilles Standard.xml ) ],
				[ qw(yairabien Yves Airabien yves.airabien@noname.org melanie Standard.xml) ],
				[ qw(ben Ben Aordhur ben.aordhur@noname.org ben Standard.xml) ],
				[ qw(paroid Paul Aroid paul.aroid@noname.org paroid Standard.xml) ],
				[ qw(pbulaire Patty Bulaire patty.bulaire@noname.org pbulaire Standard.xml) ],
				[ qw(ec Elie Coptaire elie.coptaire@noname.org ec Standard.xml) ],
				[ qw(hc Harry Cover harry.cover@noname.org hc Standard.xml) ],
				[ qw(ac Annie Croche annie.croche@noname.org ac Standard.xml) ],
				[ qw(lc Lorie Culaire lorie.culaire@noname.org lc Standard.xml) ],
			];

my $res_def = [ [ 'salle1', 'salle de réunion 1', 'batiment C1', 'admin' ],
				[ 'salle2', 'salle de réunion 2', 'batiment C1', 'ec' ],
				[ 'salle3', 'salle de réunion 3', 'batiment C2', 'ec' ],
				[ 'salle4', 'salle de réunion 4', 'batiment C3', 'hc' ],
				[ 'videoprojecteur1', 'Vidéo projecteur', 'batiment C1', 'ac' ],
			];

my $teams_def = { 'clair' => [ qw(gaiparbal yairabien ben paroid pbulaire ) ],
					'autres' => [ qw(ec hc ac lc) ],
					'tous' => [ qw(gaiparbal yairabien ben paroid pbulaire ec hc ac lc) ],
				};

my $groups_def = { 'premier' => { users => [ qw(ec hc ac lc) ],
									teams => [ qw(clair) ]
								},
					'autre' => { users => [ qw(ec hc ac lc) ],
									teams => [ qw(tous) ]
								},
				};

# ================================================================
# ================================================================
# GetInstance : Get id and values for instance
# ================================================================
sub GetInstance
{
	my ($dbh, $ident) = @_;

	my $sql = "select * from m_mioga where ident='$ident'";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		print "Instance : $ident not known\n";
	}
	return $res->{rowid};
}
# ================================================================
# EnableAppInGroup : Enable an application in group
# ================================================================
sub EnableAppInGroup
{
	my ($dbh, $group_id, $app_id) = @_;
	# ----------------------------------
	# Get profiles for group
	# ----------------------------------
	my $sql = "select m_profile.rowid from m_profile where group_id=$group_id";

	my $profiles = SelectMultiple($dbh, $sql);
	if (!defined($profiles)) {
		print "Database error\n";
		return;
	}
	# ----------------------------------
	# Add apps if not present
	# ----------------------------------
	ApplicationEnableInGroup($dbh, $app_id, $group_id);
	# ----------------------------------
	# Get functions for application
	# ----------------------------------
	$sql = "select rowid from m_function where application_id=$app_id";

	my $functions = SelectMultiple($dbh, $sql);
	if (!defined($functions)) {
		print "Database error\n";
		return;
	}
	# ----------------------------------
	# Set profiles
	# ----------------------------------
	foreach my $prof (@$profiles) {
		foreach my $func (@$functions) {
			ProfileCreateFunction($dbh, $prof->{rowid}, $func->{rowid});
		}
	}
}
# ================================================================
# GetAppIdForApplication
# ================================================================
sub GetAppIdForApplication
{
	my ($dbh, $ident, $mioga_id) = @_;
	my $sql = "select rowid from m_application where ident='$ident' AND mioga_id=$mioga_id";

	my $res = SelectSingle($dbh, $sql);
	return $res->{rowid};
}
# ================================================================
# CreateUsers
# ================================================================
sub CreateUsers
{
	my ($dbh, $config, $mioga_id, $users) = @_;

	# ----------------------------------
	# Get admin id for current instance
	# ----------------------------------
	my $sql = "select admin_id from m_mioga where rowid=$mioga_id";
	my $res = SelectSingle($dbh, $sql);
	if (!defined($res)) {
		print "Database error\n";
		return;
	}
	my $admin_id = $res->{admin_id};
	$created_users{admin} = $admin_id;
	# ----------------------------------
	# Read file and create users
	# ----------------------------------
	foreach my $u (@$users) {
		my $values = {};
		$values->{autonomous} = 1;
		$values->{public_part} = 0;
		$values->{ident} = $u->[0];
		$values->{firstname}  = $u->[1];
		$values->{lastname}   = $u->[2];
		$values->{email}      = $u->[3];
		$values->{password}   = $u->[4];
		$values->{skeleton}   = $u->[5];
		$values->{creator_id} = $admin_id;

		my $user_id = UserLocalCreate($config, $values);
		print "Create user $values->{ident} : $user_id\n";
		$created_users{$values->{ident}} = $user_id;

	}

=head2

	# ----------------------------------
	# Gennerate dummy users
	# ----------------------------------
	for (my $i = 1; $i <= 10; $i++) {
		my $values = {};
		$values->{autonomous} = 1;
		$values->{public_part} = 0;
		$values->{ident}      = "u$i";
		$values->{firstname}  = "U$i";
		$values->{lastname}   = "User$i";
		$values->{email}      = "user$i\@alixen.fr";
		$values->{password}   = "u$i";
		$values->{skeleton}   = "Standard.xml";
		$values->{creator_id} = $admin_id;

		my $user_id = UserLocalCreate($config, $values);
		print "Create user $values->{ident} : $user_id\n";
		$created_users{$values->{ident}} = $user_id;
	}
	for (my $i = 1; $i <= 10; $i++) {
		my $values = {};
		$values->{autonomous} = 1;
		$values->{public_part} = 0;
		$values->{ident}      = "s$i";
		$values->{firstname}  = "s$i";
		$values->{lastname}   = "S-User$i";
		$values->{email}      = "suser$i\@alixen.fr";
		$values->{password}   = "s$i";
		$values->{skeleton}   = "WithoutPrivate.xml";
		$values->{creator_id} = $admin_id;

		my $user_id = UserLocalCreate($config, $values);
		print "Create user $values->{ident} : $user_id\n";
		$created_users{$values->{ident}} = $user_id;
	}

=cut

}
# ================================================================
# CreateResources
# ================================================================
sub CreateResources
{
	my ($dbh, $config, $mioga_id, $resources) = @_;

	my $admin_id = $created_users{admin};
	# ----------------------------------
	# Read file and create users
	# ----------------------------------
	foreach my $r (@$resources) {
		my $values = {};
		$values->{ident}      = $r->[0];
		$values->{description} = $r->[1];
		$values->{location}   = $r->[2];
		$values->{anim_id}    = $created_users{$r->[3]};
		$values->{creator_id} = $admin_id;

		my $res_id = ResourceCreate($config, $values);
		print "Create resource $values->{ident} : $res_id\n";
		$created_resources{$values->{ident}} = $res_id;

	}
}
# ================================================================
# CreateTeams
# ================================================================
sub CreateTeams
{
	my ($dbh, $config, $mioga_id, $teams) = @_;

	my $admin_id = $created_users{admin};
	# ----------------------------------
	foreach my $k (keys(%$teams)) {
		my $values = {};
		$values->{ident}      = $k;
		$values->{description} = $k;
		$values->{creator_id} = $admin_id;

		my $team_id = TeamCreate($config, $values);

		foreach my $u (@{$teams->{$k}}) {
			GroupInviteGroup($dbh, $team_id, $created_users{$u});
		}

		print "Create team $values->{ident} : $team_id\n";
		$created_teams{$values->{ident}} = $team_id;
	}
	my $values = {};
	$values->{ident}      = "Tous";
	$values->{description} = "Tout le monde";
	$values->{creator_id} = $admin_id;

	my $team_id = TeamCreate($config, $values);

	foreach my $u (keys(%created_users)) {
		if ($created_users{$u} != $admin_id) {
			GroupInviteGroup($dbh, $team_id, $created_users{$u});
		}
	}

	print "Create team $values->{ident} : $team_id\n";
	$created_teams{$values->{ident}} = $team_id;
}
# ================================================================
# CreateGroups
# ================================================================
sub CreateGroups
{
	my ($dbh, $config, $mioga_id, $groups) = @_;

	my $admin_id = $created_users{admin};
	# ----------------------------------
	# ----------------------------------
	foreach my $k (keys(%$groups)) {
		my $values = {};
		$values->{ident}      = $k;
		$values->{description} = $k;
		$values->{creator_id} = $admin_id;
		$values->{anim_id} = $admin_id;
		$values->{skeleton}   = "Standard.xml";

		my $group_id = GroupCreate($config, $values);

		foreach my $u (@{$groups->{$k}->{users}}) {
			GroupInviteGroup($dbh, $group_id, $created_users{$u});
		}
		foreach my $u (@{$groups->{$k}->{teams}}) {
			GroupInviteGroup($dbh, $group_id, $created_teams{$u});
		}

		print "Create group $values->{ident} : $group_id\n";
		$created_groups{$values->{ident}} = $group_id;
	}
	my $values = {};
	$values->{ident}      = "commun";
	$values->{description} = "Tout le monde";
	$values->{creator_id} = $admin_id;
	$values->{anim_id} = $admin_id;
	$values->{skeleton}   = "Standard.xml";

	my $group_id = GroupCreate($config, $values);

	GroupInviteGroup($dbh, $group_id, $created_teams{Tous});

	print "Create group $values->{ident} : $group_id\n";
	$created_groups{$values->{ident}} = $group_id;
}
# ================================================================
# AddApplicationsInGroups
# ================================================================
sub AddApplicationsInGroups
{
	my ($dbh, $config, $mioga_id) = @_;

	my $admin_id = $created_users{admin};

	my $app_id = GetAppIdForApplication($dbh, 'Forum', $mioga_id);
	EnableAppInGroup($dbh, $created_groups{commun}, $app_id);
	EnableAppInGroup($dbh, $created_groups{premier}, $app_id);

	$app_id = GetAppIdForApplication($dbh, 'Articles', $mioga_id);
	EnableAppInGroup($dbh, $created_groups{commun}, $app_id);
	EnableAppInGroup($dbh, $created_groups{premier}, $app_id);

	$app_id = GetAppIdForApplication($dbh, 'News', $mioga_id);
	EnableAppInGroup($dbh, $created_groups{commun}, $app_id);
	EnableAppInGroup($dbh, $created_groups{premier}, $app_id);

	$app_id = GetAppIdForApplication($dbh, 'Planning', $mioga_id);
	EnableAppInGroup($dbh, $created_groups{commun}, $app_id);
	EnableAppInGroup($dbh, $created_groups{premier}, $app_id);
}
# ================================================================
# Main program
# ================================================================

# Get args and initialize things
#
if(@ARGV < 1) {
	print "Usage : $0 instance\n";
	exit(-1);
} 

my $miogaconf_path = "/var/lib/Mioga2/conf/Mioga.conf";
my $miogaconf = new Mioga2::MiogaConf($miogaconf_path);
my $dbh = $miogaconf->GetDBH();
my $instance = $ARGV[0];
my $mioga_id = GetInstance($dbh, $instance);
my $config = new Mioga2::Config($miogaconf, $instance);


print "Populate instance : $instance(id = $mioga_id)\n";

CreateUsers($dbh, $config, $mioga_id, $users_def);
CreateResources($dbh, $config, $mioga_id, $res_def);
CreateTeams($dbh, $config, $mioga_id, $teams_def);
CreateGroups($dbh, $config, $mioga_id, $groups_def);

AddApplicationsInGroups($dbh, $config, $mioga_id);

print "\n ... good bye ...\n";
