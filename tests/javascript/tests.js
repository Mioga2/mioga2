// This file contains Mioga JavaScript tests
// The main function is RunTests, called once HTMl page is loaded.
// This function will call each function found in "tests" array.
// The test function must return an array composed of two elements :
// 		- the test label to be displayed in results list,
// 		- the test status ("success", "warning", "failure")
// 		- a comment to be appended to test result (may be undefined if nothing has to be said)

var tests = [
	// Ensure Date.parseMiogaDate doesn't fail because target month wouldn't have current day
	function () {
		var test_label = "Date.parseMiogaDate test";
		var test_status = "failure";
		var result_comment = undefined;

		// Initialize new date object to 31st august 2012
		var date = new Date (2012, 7, 31, 10, 0, 0);

		// Parse Mioga date to get 12th september 2012
		date.parseMiogaDate ("2012-09-12 10:00:00");

		// Check parsed date is correct
		// Caution: First month is 1 in Mioga and 0 in JavaScript
		if ((date.getDate () === 12) && (date.getMonth () === 8) && (date.getYear () === 112)) {
			test_status = "success";
		}
		else {
			result_comment = "Parsing Mioga date 2012-09-12 10:00:00 leads to JavaScript date " + date;
		}

		return ([test_label, test_status, result_comment]);
	}
];


//
// No modification should be necessary below this line
//

var $results;

function RunTests () {
	$results = $('ul#results');

	// Run each test function
	$.each (tests, function (index, test) {
		var res = test ();
		var $result = $('<li></li>').append (res[0]).addClass (res[1]).appendTo ($results);
		if (res[2] !== undefined) {
			$('<pre></pre>').append (res[2]).addClass ('comment').appendTo ($result);
		}
	});
}

