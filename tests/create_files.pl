#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description: 
#   This script creates a directory structure and files
#   with special characters in names. Each file contains a string
#   corresponding to its path.

use strict;
use utf8;
use File::Basename;
use Data::Dumper;

my $debug = 0;

# Internal variables
my $offset = 0;
my $name_length = 10;

# my @params = qw/depth files dirs dry-run/;
my %params = ();

my %usage = (
	'--depth'     => "Filesystem depth",
	'--subdirs'   => "Number of subdirectories in each directory",
	'--files'     => "Number of files in each directory",
	'-no-special' => "Limit character set to non-special characters",
	'-v'          => "Increase verbosity"
);

my %flags = (
	'v' => 'verbose',
	'no-special' => 'no-special'
);

# ================================================================
# Display usage
# ================================================================
sub Usage {
	print "Usage: " . basename($0) . " [OPTION] [PATH]\n";

	print "If [PATH] is not provided, the structure will be created in current directory\n";

	my @keys = sort { length $b <=> length $a } keys (%usage);
	my $length = length ($keys[0]);

	print "\nOptions:\n";
	for (sort { $a cmp $b } keys(%usage)) {
		print "  $_" . ' ' x ($length - length($_) + 4) . $usage{$_} . "\n";
	}
}

# ================================================================
# Process command-line arguments
# ================================================================
sub ProcessArgs {
	my $arg = '';

	# Initialize default flags values
	for (keys(%flags)) {
		$params{$flags{$_}} = 0;
	}

	# Process command-line arguments
	foreach (@ARGV) {
		if (/^--/) {
			my ($argname) = ($_ =~ m/^--(.*)/);
			if (grep (/^--$argname$/, %usage)) {
				$arg = $argname;
			}
			else {
				print STDERR "Unknown argument $_\n";
				&Usage;
				exit (-1);
			}
		}
		elsif (/^-[^-]/) {
			my ($flagname) = ($_ =~ m/^-(.*)/);
			if (grep (/^$flagname$/, %flags)) {
				$params{$flags{$flagname}} = 1;
			}
			else {
				print STDERR "Unknown flag $_\n";
				&Usage;
				exit (-1);
			}
		}
		else {
			if ($arg ne '') {
				$params{$arg} = $_;
			}
			else {
				$params{dest} = $_;
			}
			$arg = '';
		}
	}

	$params{dest} = '.' if (!defined($params{dest}));

	print STDERR "Parameters: " . Dumper \%params if ($debug);
}

# ================================================================
# Populate directory with files and sub-directories
# ================================================================
sub PopulateDir {
	my ($pwd) = @_;

	$pwd = '' if (!$pwd);

	# Create files
	for (my $i=1; $i<=$params{files}; $i++) {
		my $filename;
		do {
			$filename = "$pwd/" . &GenerateString;
		} while (-e "$params{dest}$filename");
		print "[F] Creating $params{dest}$filename\n" if ($params{verbose});

		open (my $filep, ">$params{dest}/$filename") || die ("Could not open file '$params{dest}/$filename'. $!");
		print $filep ("$filename\n");
		close ($filep);
	}

	# Create sub-directories
	if (($pwd) =~ tr!/!! < $params{depth}) {
		for (my $i=1; $i<=$params{subdirs}; $i++) {
			my $dirname;
			do {
				$dirname = "$pwd/" . &GenerateString;
			} while (-e "$params{dest}$dirname");
			print "[D] Creating $params{dest}$dirname\n" if ($params{verbose});

			# Create directory
			mkdir "$params{dest}/$dirname";

			# Recurse through sub-directory
			&PopulateDir ($dirname);
		}
	}
}

# ================================================================
# Generate random string
# ================================================================
sub GenerateString {
	my @characters = ('a'..'z', 'A'..'Z', 0..9);
	my @special_characters = qw/~ ` ! @ $ % ^ & * ( ) - _ + = { } [ ] | \ : ; " ' . < > à á â ã ä å æ ç è é ê ë ì í î ï ð ñ ò ó ô õ ö ø ù ú û ü ý þ ÿ ¿ À Á Â Ã Ä Å Æ Ç È É Ê Ë Ì Í Î Ï Ð Ñ Ò Ó Ô Õ Ö × Ø Ù Ú Û Ü Ý Þ ß £ ¤ ¥ ¦ § © « ® ± µ » ¼ ½ ¾/;
	push @special_characters, '#';
	push @special_characters, ',';

	my $str = '';
	for (my $i=0; $i<$name_length; $i++) {
		$str .= $characters[($i+$offset) % scalar(@characters)] if (!($i % 2));
		$str .= $special_characters[($i-1+$offset) % scalar(@special_characters)] if (($i % 2) && !$params{'no-special'});
		$str .= $characters[($i-1+$offset) % scalar(@characters)] if (($i % 2) && $params{'no-special'});
	}

	$offset += $name_length;

	utf8::encode ($str);

	return ($str);
}

&ProcessArgs;
if (!defined ($params{depth}) || !defined ($params{files}) || !defined ($params{subdirs})) {
	&Usage;
	exit (-1);
}
&PopulateDir;
