#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description: 
#   This script download files to Mioga2 using DAVProxy

use strict;
use LWP::UserAgent;
use MIME::Base64;
use Data::Dumper;
use File::Basename;

use Mioga2::XML::Simple;
use Mioga2::tools::string_utils;
use Mioga2::tools::date_utils;

my $debug = 0;


# Internal variables
my $server = '';
my $base = '';

my @params = qw/user passwd url/;
my %params = ();

my %usage = (
	'-r'       => "recurse through directories",
	'-v'       => "increase verbosity",
	'--user'   => "user name used for authentication",
	'--passwd' => "password used for authentication"
);

my %flags = (
	'r' => 'recursive',
	'v' => 'verbose'
);

my $user_agent = new LWP::UserAgent(keep_alive => 0);


# ================================================================
# Display usage
# ================================================================
sub Usage {
	print "Usage: " . basename($0) . " [OPTION]... SRC [SRC]... DST\n";

	my @keys = sort { length $b <=> length $a } keys (%usage);
	my $length = length ($keys[0]);

	print "\nOptions:\n";
	for (sort { $a cmp $b } keys(%usage)) {
		print "  $_" . ' ' x ($length - length($_) + 4) . $usage{$_} . "\n";
	}
}

# ================================================================
# Prompt the user for authentication
# ================================================================
sub PromptAuthentication {
	my ($method, $url) = @_;

	print "Authentication required for: '$method' ($url)\n";
	print "User name: ";
	chomp ($params{user} = <STDIN>);
	print "Password: ";
	chomp ($params{passwd} = <STDIN>);
}

# ================================================================
# Execute HTTP request
# ================================================================
sub ExecuteRequest {
	my ($method, $url, $header) = @_;


	my $rc;

	my $request = new HTTP::Request($method, $url);
	if (defined($header)) {
		foreach my $var (keys(%$header)) {
			$request->push_header($var => $header->{$var});
		}
	}


	# Add authentication parameters to header
	my $encoded = encode_base64("$params{user}:$params{passwd}");
	$request->push_header(Authorization => "Basic $encoded");

	my $response = $user_agent->request($request);
	$rc = $response->code;

	print "Code: " . $response->code . "\n" if ($debug);
# 	print Dumper $response if ($debug > 1);
	print $response->content if ($debug > 1);

	# Retry if authorization failed
	if ($rc == 401) {
		PromptAuthentication ($method, $url);
		$rc = ExecuteRequest ($method, $url);
	}

	return ($response);
}

# ================================================================
# Process command-line arguments
# ================================================================
sub ProcessArgs {
	my $arg = '';

	# Initialize default flags values
	for (keys(%flags)) {
		$params{$flags{$_}} = 0;
	}

	# Process command-line arguments
	foreach (@ARGV) {
		if (/^--/) {
			my ($argname) = ($_ =~ m/^--(.*)/);
			if (grep (/^$argname$/, @params)) {
				$arg = $argname;
			}
			else {
				print STDERR "Unknown argument $_\n";
				Usage;
				exit (-1);
			}
		}
		elsif (/^-[^-]/) {
			my ($flagname) = ($_ =~ m/^-(.*)/);
			if (grep (/^$flagname$/, %flags)) {
				$params{$flags{$flagname}} = 1;
			}
			else {
				print STDERR "Unknown flag $_\n";
				Usage;
				exit (-1);
			}
		}
		else {
			if ($arg ne '') {
				$params{$arg} = $_;
			}
			else {
				push (@{$params{src}}, $_);
			}
			$arg = '';
		}
	}

	# Get last object as destination
	$params{dest} = pop (@{$params{src}});

	print STDERR "Parameters: " . Dumper \%params if ($debug);
}

# ================================================================
# List an object
# ================================================================
sub ListObj {
	my ($uri) = @_;

	my $response = ExecuteRequest ('PROPFIND', "$uri", {Depth => 1});

	if ($response->is_success())
	{
		my @file_list;

		my $xml_response = $response->content();
		print STDERR "xml_response = $xml_response\n" if ($debug > 1);

		my $xml = new Mioga2::XML::Simple(forcearray => 1);
		my $hash_response  = $xml->XMLin($xml_response);

		print STDERR Dumper($hash_response) if ($debug > 1);

		foreach my $n (@{$hash_response->{"D:response"}})
		{
			# Look for properties namespace
 			my $props_ns;

			foreach my $key (keys %$n) {
				next if ref($n->{$key}) ne '';
				next if $n->{$key} ne 'DAV:';
				next if $key !~ /^lp/;				

				$props_ns = $key;
			}
			
			# Look for custom properties namespace
 			my $custom_props_ns;

			foreach my $key (keys %$n) {
				next if ref($n->{$key}) ne '';
				next if $n->{$key} ne 'http://webdav.org/cadaver/custom-properties/';			

				$custom_props_ns = $key;
			}
			
			my $name = $n->{"D:href"}->[0];
			$name =~ s/\/$//;

			# Name is given vy DAV response and is escaped
			$name =~ s/&/%26/g;
			$name =~ s/=/%3D/g;
			my $unesc_name = st_URIUnescape($name);

			next if (lc($unesc_name) eq lc($uri)); # removing . dir

			warn "name : $name, uri : $uri, unesc_name : $unesc_name" if ($debug > 0);
			if (defined($name))
			{
				my $result = {};

# 				foreach (keys %{$n->{"D:propstat"}->[0]->{"D:prop"}->[0]})
# 				{
# 					if ($_ =~ /$custom_props_ns:(.*)/)
# 					{
# 						my $prop = $1;
# 						$result->{$prop} = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$custom_props_ns:$prop"}->[0];
# 					}
# 				}

				$result->{name} = $unesc_name;
				$result->{name_esc} = $name;
				my $basename = basename ($unesc_name);

				# short name is the name displayed in web page.
				$result->{short_name} = $basename;

				$result->{length} = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:getcontentlength"}->[0];

				my $date = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:getlastmodified"}->[0];
				if(ref($date) ne '') {
					$date = $date->{content}->[0];
				}

				$result->{date} = du_EnGMTToISO($date);

				my $type;
				if(exists $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:resourcetype"}) {
					$type = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"$props_ns:resourcetype"}->[0];
				}
				else {
					$type = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:resourcetype"}->[0];
				}

				if (exists $type->{"D:collection"})
				{
					$result->{type} = 'collection';
					$result->{locked} = undef;
				}
				else
				{
					$result->{type} = 'file';
					if (exists($n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:lockdiscovery"}->[0]->{"D:activelock"}))
					{
						$result->{locked} = 1;
						if (exists($n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:lockdiscovery"}->[0]->{"D:activelock"}->[0]->{"ns0:owner"})) {
							my $owner = $n->{"D:propstat"}->[0]->{"D:prop"}->[0]->{"D:lockdiscovery"}->[0]->{"D:activelock"}->[0]->{"ns0:owner"}[0]->{"content"}[0];

							$result->{lock_owner} = $owner;
						}
					}
					else
					{
						$result->{locked} = 0;
					}
				}

				push (@file_list, $result);
			}
		}
		return \@file_list;
	}
}

# ================================================================
# Get a file
# ================================================================
sub GetFile {
	my ($relpath) = @_;

	print "[F] $relpath\n" if ($params{verbose});

	my $complete_uri = "$server$base$relpath";

	my $response = ExecuteRequest ('GET', $complete_uri);

	if ($response->is_success) {
		my $filename = $params{dest} . "/" . $relpath;
		open (FILE, ">$filename");
		print (FILE $response->content);
		close (FILE);
	}
}

# ================================================================
# Get a directory
# ================================================================
sub GetDir {
	my ($relpath) = @_;

	print "[D] $relpath\n" if ($params{verbose});

	my $complete_uri = "$server$base$relpath/";

	# Create local directory
	mkdir "$params{dest}/$relpath";

	# Get directory contents
	my $objects = &ListObj ("$complete_uri");

	foreach (1..$#{$objects}) {
		my ($relname) = (${$objects}[$_]{name} =~ m/$base(.*)/);

		if ($debug) {
			print Dumper ${$objects}[$_] if ($debug > 1);
			print "[COLL] " if (${$objects}[$_]{type} eq 'collection');
			print "[FILE] " if (${$objects}[$_]{type} eq 'file');
			print ${$objects}[$_]{name} . "\n";
		}

		&GetFile ($relname) if (${$objects}[$_]{type} eq 'file');
		&GetDir ("$relname/") if (${$objects}[$_]{type} eq 'collection');
	}

}

# ================================================================
# Initialise relative path for URI and return nb of objects it contains
# ================================================================
sub InitPath {
	my ($uri) = @_;

	my $objects = &ListObj ("$uri");
	$objects = &ListObj ("$uri/") if (ref ($objects) ne 'ARRAY');

	$server = substr $uri, 0, index ($uri, ${$objects}[0]{name});
	($base) = (${$objects}[0]{name} =~ m/(.*)\/[^\/]+$/);

	print "Server: $server\nBase: $base\n" if ($debug);

	return ($#{$objects});
}


&ProcessArgs;

# Loop through each object
for (@{$params{src}}) {
	my $nb = &InitPath ($_);
	$_ =~ s!$server$base!!;
	if ($nb) {
		&GetDir ($_);
	}
	else {
		&GetFile ($_);
	}
}
