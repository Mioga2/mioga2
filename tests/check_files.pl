#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description: 
#   This script checks a directory structure. Each file should contain its
#   file path relative to base path provided as argument

use strict;
use File::Basename;
use String::Random;
use Data::Dumper;

my $debug = 0;
my $path;
my $status = 0;

# ================================================================
# Display usage
# ================================================================
sub Usage {
	print "Usage: " . basename($0) . " PATH\n";
	exit (-1);
}

# ================================================================
# Check file contents
# ================================================================
sub CheckFile {
	my ($file) = @_;

	open (FILE, "<$path/$file");

	my $contents;
	chomp ($contents = <FILE>);

	if ("$contents" ne "$file") {
		print "[KO] $file\n";
		$status = 1;
	}

	close (FILE);
}

# ================================================================
# Check directory contents
# ================================================================
sub CheckDirectory {
	my ($dir) = @_;

	$dir = '' if (!$dir);

	opendir (my $dirp, "$path/$dir");
	while (my $obj = readdir($dirp)) {
		next if ($obj =~ m/^\.*$/);
		if ( -d "$path/$dir/$obj" ) {
			&CheckDirectory ("$dir/$obj");
		}
		elsif ( -f "$path/$dir/$obj" ) {
			&CheckFile ("$dir/$obj");
		}
		else {
			print STDERR "Unsupported type '$path/$dir/$obj'\n";
		}
	}
	close ($dir);
}


&Usage if(@ARGV < 1);
$path = $ARGV[0];

&CheckDirectory;

exit ($status);