#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  user.pl
#
#        USAGE:  ./user.pl /path/to/Mioga.conf <instance_name>
#
#  DESCRIPTION:  Mioga2::UserList class tests
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  Sébastien NOBILI <technique@alixen.fr>
#      COMPANY:  Alixen (http://www.alixen.fr)
#      CREATED:  02/04/2010 17:42
#
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================

use strict;
use warnings;

use Data::Dumper;

use Test::More 'no_plan';
use Test::Exception;

use File::Basename;
use Getopt::Long;

use Mioga2::MiogaConf;
use Mioga2::Config;
use Mioga2::tools::database;
use Mioga2::InstanceList;
use Mioga2::UserList;
use Mioga2::GroupList;
use Error qw(:try);
use Mioga2::Exception::Simple;

my $user_ident = 'toto';
my $user_email = 'toto@titi.org';
my $user_firstname = 'Toto';
my $user_lastname = 'TITI';
my $user_password = 'toto';

sub pause {
	print "Press a key to continue...";
	while (<>) {
		last;
	}
}

if (@ARGV < 2) {
	print "Usage: " . basename($0) . " /path/to/Mioga.conf <instance_name>\n";
	exit (1);
}

diag ("Load Mioga configuration from file");
ok (my $miogaconf = new Mioga2::MiogaConf (shift), "Create Mioga2::MiogaConf from file");
ok (my $dbh = $miogaconf->GetDBH (), "Get DBH from Mioga2::MiogaConf");

diag ("Load instance configuration from database");
my $instname = shift;
ok (SelectSingle ($dbh, "SELECT * FROM m_mioga WHERE ident='$instname'"), "Check instance '$instname' exists");
ok (my $config = new Mioga2::Config ($miogaconf, $instname), "Get Mioga2::Config for instance");

my ($inst, $group, $user, $users, $groups);

# Create a new user from attributes and skeleton
ok ($user = new Mioga2::UserList ($config, email => $user_email, ident => $user_ident, firstname => $user_firstname, lastname => $user_lastname, password => $user_password , skeleton => '/home/snobili/tmp/skel/std_user.xml'), "Initialize user object");

ok ($user->Store (), "Store user to DB");

diag ("Load a user from database and update it");
ok ($user = new Mioga2::UserList ($config, email => $user_email), "Initialize user object");
ok ($user->Load (), "Load user from DB");
ok ($user->Set ('ident', "$user_ident$user_ident"), "Change user ident");
ok ($user->Store (), "Store user to DB");
ok ($user->Set ('ident', "$user_ident"), "Set ident back to its initial value");
ok ($user->Store (), "Store user to DB");

diag ("Try to set ident to an already existing value");
ok ($user->Set ('ident', 'admin'), "Change user ident");
dies_ok { $user->Store () } "Store user to DB";

diag ("Try to set email to an already existing value");
my $existing = new Mioga2::UserList ($config, ident => 'admin');
my $used_email = $existing->Get ('email');
ok ($user->Set ('email', $used_email), "Change user email");
dies_ok { $user->Store () } "Store user to DB";


&pause ();

diag ("Delete user from database");
ok ($user->Delete (), "Delete user from DB");
