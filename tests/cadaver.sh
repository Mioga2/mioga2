#!/bin/sh

# ------ Preparation

if [ $# -lt 7 ]
then
	echo "Usage : $0 [http|https] base_uri server_name instance group login password"
	echo "        base_uri must contains a begin and trailing / (only / if empty) ex /Mioga2/"
	echo ""
	echo "Exemple: $0 http /Mioga2/ mioga2-dev-vhh Mioga admin admin admin"
	exit 2
fi
PROTOCOL=$1
BASE_URI=$2
SERVER_NAME=$3
INSTANCE=$4
GROUP=$5
LOGIN=$6
PASSWORD=$7

if [ -z "`which cadaver`" ]; then
	echo "cadaver is missing, cannot run WebDAV tests."
	exit 3
fi

NETRC="$HOME/.netrc"
OLD_NETRC=""
if [ -e "$NETRC" ]; then
	OLD_NETRC="$HOME/.netrc_cadaver.sh"
	mv "$NETRC" "$OLD_NETRC"
fi
echo "machine $SERVER_NAME login $LOGIN passwd $PASSWORD" > "$NETRC"

CDV="cadaver $PROTOCOL://${SERVER_NAME}${BASE_URI}${INSTANCE}/home/$GROUP"
NR_TESTS=0
NR_SUCCESS=0

test_dav() {
	local CDVCMD="$1"
	local TO_FIND="$2"
	NR_TESTS=`expr $NR_TESTS + 1`
	local OUTPUT="`echo $CDVCMD | $CDV`"
	echo "$OUTPUT\n" # for the user
	
	if echo "$OUTPUT" | grep "$TO_FIND" >/dev/null; then
		NR_SUCCESS=`expr $NR_SUCCESS + 1`
	else
		echo "Test failed."
	fi
}

# ------ Tests

test_dav "ls" 'Listing.*succeeded'
test_dav "mkcol testdir1" 'Creating.*succeeded'
test_dav "rmcol testdir1" 'Deleting.*succeeded'

TMPBASE=mioga2-cadaver-test
TMPFILE=/tmp/$TMPBASE
dd if=/dev/urandom of=$TMPFILE count=1 bs=20000
test_dav "put $TMPFILE" 'Uploading.*succeeded'

TMPCOPY=${TMPFILE}_copy
test_dav "get $TMPBASE $TMPCOPY" 'Downloading.*succeeded'

NR_TESTS=`expr $NR_TESTS + 1`
if diff -q $TMPFILE $TMPCOPY; then
	NR_SUCCESS=`expr $NR_SUCCESS + 1`
else
	echo "ERROR: Downloaded file is not what was uploaded!"
fi

test_dav "delete $TMPBASE" 'Deleting.*succeeded'

test_dav "mkcol testdir2" 'Creating.*succeeded'
test_dav "put $TMPFILE testdir2/$TMPBASE" 'Uploading.*succeeded'
test_dav "rmcol testdir2" 'Deleting.*succeeded'

rm $TMPFILE $TMPCOPY

# ------ Counting the results

RESULT=0
if [ $NR_TESTS -eq $NR_SUCCESS ]; then
	echo "Everything's perfect!"
else
	echo "Only $NR_SUCCESS out of $NR_TESTS succeeded."
	RESULT=1
fi

# ------ Cleanup

if [ -n "$OLD_NETRC" ]; then
	mv "$OLD_NETRC" "$NETRC"
fi

exit $RESULT
