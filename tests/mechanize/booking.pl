#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  booking.pl
#
#        USAGE:  ./booking.pl  
#
#  DESCRIPTION:  
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  The Mioga2 Project <developers@mioga2.org>
#      COMPANY:  The Mioga2 Project (http://www.mioga2.org)
#      CREATED:  02/12/2010 14:22
#
#===============================================================================
#
#  Copyright (c) year 2010, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================

use strict;
use warnings;

use Data::Dumper;

use Test::More qw(no_plan);
use Test::WWW::Mechanize;
use Data::Dumper;

my $debug = 1;

if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password [suppres flag]\n";
	exit -1;
}
my $protocol = $ARGV[0];
my $base_uri = $ARGV[1];
my $server = $ARGV[2];
my $login_module_uri = $ARGV[3];
my $instance = $ARGV[4];
my $group = $ARGV[5];
my $login = $ARGV[6];
my $password = $ARGV[7];

my $suppress = 1;
$suppress = $ARGV[8] if (@ARGV > 8);

my $application = 'Booking';
my $relative_uri = "$base_uri$instance/bin/$group/$application";
my $base = "$protocol://$server$relative_uri";
my $login_uri = "$protocol://$server$base_uri$login_module_uri";

my $mech = Test::WWW::Mechanize->new;

# Login to Mioga
$mech->post_ok ("$login_uri/DisplayMain?target=$relative_uri/DisplayMain", { login => $login, password => $password }, "Login to Mioga2 as $login");

# DisplayMain
$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains('<body class="booking">');

# Get resources rowids
my @resources;
my @links = $mech->followable_links();
foreach my $l (@links) {
	my $url = $l->url();
	if ($url =~ /CreateSimplePeriodicTask\?resource_id/) {
		$url =~ /resource_id=([0-9]+)/;
		push @resources, $1;
	}
}

# CreateSimplePeriodicTask
$mech->get_ok ("$base/CreateSimplePeriodicTask?resource_id=$resources[0]");
my $form = $mech->form_number (1);
my $action = $form->action ();
$form->action ("$action?resource_id=$resources[0]&create=Ok");
$mech->submit_form_ok ();
$mech->content_like (qr/<a class="edit" href="EditSimplePeriodicTask\?taskrowid=[0-9]+&amp;resource_id=$resources[0]"/);

$mech->get_ok ("$login_uri/Logout");
