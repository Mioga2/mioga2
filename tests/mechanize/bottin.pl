#!/usr/bin/perl
#
#============================================================================
#
#	Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation; either version 2, or (at your option) any later
#   version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, 675 Mass
#   Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description:
#		Mioga2 Bottin webservices testing script
#

use strict;
use warnings;

use Mechioga2::Context;
use Mechioga2::Bottin;

use Test::More qw(no_plan);


if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password\n";
	exit -1;
}

my $context = {
	protocol => $ARGV[0],
	base_uri => $ARGV[1],
	server => $ARGV[2],
	login_module_uri => $ARGV[3],
	instance => $ARGV[4],
	group => $ARGV[5],
	login => $ARGV[6],
	password => $ARGV[7],
	mech => Test::WWW::Mechanize->new
};

my $users = [	
	{ 
		firstname => 'Red',
		lastname => 'Richards',
		password => 'secret',
		password2 => 'secret',
		email => 'rrichard@noname.org'
	},
	{
		firstname => 'Susan',
		lastname => 'Storm',
		password => 'secret',
		password2 => 'secret',
		email => 'sstorm@noname.org'
	},
	{
		firstname => 'Johnny',
		lastname => 'Storm',
		password => 'secret',
		password2 => 'secret',
		email => 'jstorm@noname.org'
	},
	{
		firstname => 'Benjamin',
		lastname => 'Grimm',
		password => 'secret',
		password2 => 'secret',
		email => 'bgrimm@noname.org'
	},
	{
		firstname => 'Stan',
		lastname => 'Lee',
		password => 'secret',
		password2 => 'secret',
		email => 'slee@noname.org'
	}
];


#########################################################################
########################## BEGIN TEST SEQUENCE ##########################
#########################################################################

################
# INSTANCE-RW1 #
################

$context = setContext($context, "Instance-RW1", "Bottin");
login($context);

# Create USER 1, 2 and 4
ok(bottinCreateUser($context, $users->[0]), "USER 1 has been created.");
ok(bottinCreateUser($context, $users->[1]), "USER 2 has been created.");
ok(bottinCreateUser($context, $users->[3]), "USER 4 has been created.");

# Delete USER 1
bottinDeleteUser($context, $users->[0]->{email});

# Check users status
ok(bottinCheckUserActive($context, $users->[0]->{email}) eq 0, "USER 1 is NOT active.");
ok(bottinCheckUserActive($context, $users->[1]->{email}), "USER 2 is active.");
ok(bottinCheckUserActive($context, $users->[3]->{email}), "USER 4 is active.");

# Logout from Instance-RW1
logout($context);


################
# INSTANCE-RW2 #
################

$context = setContext($context, "Instance-RW2", "Bottin");
login($context);

# Create USER 1 and 4
ok(bottinCreateUser($context, $users->[0]), "USER 1 has been created.");
ok(bottinCreateUser($context, $users->[3]), "USER 4 has been created.");

# Import CSV
bottinImportUsers($context, "bottin-01-import.csv");

# Check users status
ok(bottinCheckUserActive($context, $users->[0]->{email}), "USER 1 is active.");
ok(bottinCheckUserActive($context, $users->[1]->{email}), "USER 2 is active.");
ok(bottinCheckUserActive($context, $users->[2]->{email}), "USER 3 is active.");
ok(bottinCheckUserActive($context, $users->[3]->{email}), "USER 4 is active.");

# Logout from Instance-RW2
logout($context);


###############
# INSTANCE-AF #
###############

$context = setContext($context, "Instance-AF", "Bottin");
login($context);

# Try to create USER 5
ok(bottinCreateUser($context, $users->[4]) eq 0, "USER 5 has NOT been created.");

# Create USER 1
ok(bottinCreateUser($context, $users->[0]), "USER 1 has been created.");

# Import CSV
bottinImportUsers($context, "bottin-02-import.csv");

# Check users status
ok(bottinCheckUserActive($context, $users->[0]->{email}), "USER 1 is active.");
ok(bottinCheckUserActive($context, $users->[1]->{email}), "USER 2 is active.");
ok(bottinCheckUserActive($context, $users->[4]->{email}) eq 0, "USER 5 is NOT active.");

# Logout from Instance-AF
logout($context);


###############
# INSTANCE-RO #
###############

$context = setContext($context, "Instance-RO", "Bottin");
login($context);

# Try to create USER 1
ok(bottinCreateUser($context, $users->[0]) eq 0, "USER 1 has NOT been created.");

# Import CSV
bottinImportUsers($context, "bottin-01-import.csv");

# Check users status
ok(bottinCheckUserActive($context, $users->[0]->{email}) eq 0, "USER 1 is NOT active.");
ok(bottinCheckUserActive($context, $users->[1]->{email}) eq 0, "USER 2 is NOT active.");
ok(bottinCheckUserActive($context, $users->[2]->{email}) eq 0, "USER 3 is NOT active.");

# Logout from Instance-RO
logout($context);
