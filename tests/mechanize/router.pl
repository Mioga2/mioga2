#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
# REQUIREMENTS:
# 	- The test instance should have the following directory structure:
# 		home
# 			<group>
# 				directory
# 					file
# 					directory
# 						file
# 		public
# 			<group>
# 				directory
# 					file
# 					directory
# 						file

use strict;
use Test::More qw(no_plan);
use Test::WWW::Mechanize;
use Data::Dumper;

require Mioga2;
my $version = $Mioga2::VERSION;

if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password [suppres flag]\n";
	exit -1;
}
my $protocol = $ARGV[0];
my $base_uri = $ARGV[1];
my $server = $ARGV[2];
my $login_module_uri = $ARGV[3];
my $instance = $ARGV[4];
my $group = $ARGV[5];
my $login = $ARGV[6];
my $password = $ARGV[7];

my $user_module_uri = '/user';

my $suppress = 1;
$suppress = $ARGV[8] if (@ARGV > 8);

my $relative_uri = "$base_uri$instance/home/$group";
my $base = "$protocol://$server$relative_uri";
my $top_uri = "$protocol://$server";
my $login_uri = "$protocol://$server$base_uri$login_module_uri";
my $user_uri = "$protocol://$server$base_uri$instance$user_module_uri";
my $instance_uri = "$top_uri$base_uri$instance";
my $theme_uri = "$top_uri$base_uri$instance/themes/default";
my $cdn_uri = "$top_uri/$base_uri/static/$version"; # TODO ( - 09/02/2012 13:53): Get it from argument or config file
my $help_uri = "$cdn_uri/help";
my $jslib_uri = "$cdn_uri/jslib";

my $has_base_uri = (($base_uri ne '') && ($base_uri ne '/'));

my $mech = Test::WWW::Mechanize->new;
$mech->add_header ('Accept-Language' => 'fr,fr-fr;q=0.8,en-us;q=0.5,en;q=0.3');

# Get a protected page and checks it goes to login, then login and get the same page again and check against a mechanize test
sub CheckProtected {
	my ($uri, $unauth_comment, $auth_sub) = @_;

	# Unauthenticated test
	$mech->get ($uri);
	my $basic = ($uri =~ /\/home(\/|$)/) ? 1 : 0;
	my $title = ($basic) ? qr{^401.*} : qr{Authentification};
	$mech->title_like ($title, $unauth_comment);

	# Login
	if ($uri =~ /\/home(\/|$)/) {
		# If URI is WebDAV, manually redirect to Web Login so that logout can be performed correctly
		my ($target) = ($uri =~ m#$protocol://$server(.*)#);
		$mech->get ("$login_uri?target=$target");
	}
	$mech->set_fields (login => $login, password => $password);
	$mech->submit ();

	# Authenticated test
	$mech->get ($uri);
	&$auth_sub ();

	# Logout
	$mech->get ("$login_uri/Logout?target=$relative_uri");
}


#-------------------------------------------------------------------------------
# One-member addresses
#-------------------------------------------------------------------------------
diag ("\n1-member addresses");

# Access top-level URI (Apache's "It works!" page)
$mech->get ($top_uri);
if ($has_base_uri) {
	$mech->content_like (qr{(It works\!)|(.*Testing 123.*)}, "Server's top URI $top_uri gets Apache's \"It works!\" page");
}
else {
	$mech->content_contains ('<div class="error-page">', "Server's top URI $top_uri gets an error");
}

# Access 0-member URI
$mech->get ("$top_uri$base_uri");
if ($has_base_uri) {
	$mech->content_contains ('<body class="mioga error-page">', "Mioga2 base URI $top_uri$base_uri gets an error page");
}
else {
	$mech->content_contains ('<div class="error-page">', "Server's top URI $top_uri gets an error");
}

# Access login handler
$mech->get ("$login_uri");
$mech->title_is ('Authentification Mioga2', "Mioga2 login URI $login_uri gets \"Authentification Mioga2\"");

# Access user handler
$mech->get ("$user_uri");
$mech->content_contains ('<body class="mioga login-form">', "Mioga2 user URI $user_uri gets an error page (TO BE IMPLEMENTED)");

# Access instance
CheckProtected (
	$instance_uri,
	"Existing instance URI $instance_uri redirects to login",
	sub {
		$mech->content_contains ('Workspace/DisplayMenu', "Existing instance URI $instance_uri redirects to user workspace");
	}
);

# Access non-existing instance
$mech->get ("$instance_uri-rubbish");
$mech->content_contains ('<body class="mioga error-page">', "Non-existing instance URI $instance_uri-rubbish gets an error page");


#-------------------------------------------------------------------------------
# Two-member addresses
#-------------------------------------------------------------------------------
diag ("\n2-member addresses");

# Access a group of an instance
CheckProtected (
	"$instance_uri/$group",
	"Existing instance / group URI $instance_uri/$group redirects to login",
	sub {
		$mech->base_like (qr{$instance_uri/bin/$group.*/DisplayMain}, "Existing instance / group URI $instance_uri/$group redirects to an application");
	}
);

# Access a non-existing group of an instance
$mech->get ("$instance_uri/$group-rubbish");
$mech->content_contains ('<body class="mioga error-page">', "Non-existing instance / group URI $instance_uri/$group-rubbish gets an error page");

# Access private WebDAV domain for existing instance
CheckProtected (
	"$instance_uri/home",
	"Existing instance private WebDAV URI $instance_uri/home redirects to login",
	sub {
		$mech->title_like (qr{Index of .*/home}, "Existing instance private WebDAV URI $instance_uri/home gets index of private WebDAV space");
	}
);

# Access public WebDAV domain for existing instance
$mech->get ("$instance_uri/public");
$mech->title_like (qr{Index of .*public}, "Existing instance public WebDAV URI $instance_uri/public gets index of public WebDAV space");

# Access private WebDAV domain for non-existing instance
$mech->get ("$instance_uri-rubbish/home");
$mech->content_contains ('<body class="mioga error-page">', "Non-existing instance private WebDAV URI $instance_uri-rubbish/home gets an error page");

# Access public WebDAV domain for non-existing instance
$mech->get ("$instance_uri-rubbish/public");
$mech->content_contains ('<body class="mioga error-page">', "Non-existing instance public WebDAV URI $instance_uri-rubbish/public gets an error page");


#-------------------------------------------------------------------------------
# Three (or four)-member addresses
#-------------------------------------------------------------------------------
diag ("\n{3,4}-member addresses");

# Access private WebDAV domain
CheckProtected (
	"$instance_uri/home/$group",
	"Existing-group private WebDAV URI $instance_uri/home/$group redirects to login",
	sub {
		$mech->title_like (qr{Index of .*/home/$group}, "Existing instance / group private WebDAV URI $instance_uri/home/$group gets index of private WebDAV space");
	}
);

# Access public WebDAV domain
$mech->get ("$instance_uri/public/$group");
$mech->title_like (qr{Index of .*public/$group}, "Existing-group public WebDAV URI $instance_uri/public/$group gets index of public WebDAV space");

# Access private WebDAV domain for a non-existing group
CheckProtected (
	"$instance_uri/home/$group-rubbish",
	"Non-existing-group private WebDAV URI $instance_uri/home/$group redirects to login",
	sub {
		$mech->content_contains ('<body class="mioga error-page">', "Non-existing-group private WebDAV URI $instance_uri/home/$group-rubbish gets an error page");
	}
);

# Access public WebDAV domain for a non-existing group
# TODO verify test
$mech->get ("$instance_uri/public/$group-rubbish");
$mech->content_is ('', "Non-existing-group public WebDAV URI $instance_uri/public/$group-rubbish gets \"403 Forbidden\"");

# Access non-WebDAV domain
$mech->get ("$instance_uri/rubbish/$group");
$mech->content_contains ('<body class="mioga error-page">', "Non-WebDAV URI $instance_uri/rubbish/$group gets an error page");


#-------------------------------------------------------------------------------
# Five-member addresses
#-------------------------------------------------------------------------------
diag ("\n5-member addresses");

# Access private bin domain
CheckProtected (
	"$instance_uri/bin/$group/Magellan/DisplayMain",
	"Existing-group private bin URI $instance_uri/bin/$group/Magellan/DisplayMain redirects to login",
	sub {
		$mech->content_contains ('<body class="magellan">', "Existing-group private bin URI $instance_uri/bin/$group/Magellan/DisplayMain gets Magellan")
	}
);

# # Access public bin domain # TODO ( - 06/02/2012 16:42): 
# $mech->get ("$instance_uri/pubbin/$group/FileBrowser/DisplayMain");
# $mech->content_contains ('<body id="filebrowser" class="FileBrowser">', "Existing-group public bin URI $instance_uri/pubbin/$group/FileBrowser/DisplayMain gets FileBrowser");

# Access private WebDAV domain
CheckProtected (
	"$instance_uri/home/$group/directory/file",
	"Existing-group private WebDAV URI $instance_uri/home/$group/directory/file redirects to login",
	sub {
		$mech->content_contains ('private test file', "Existing-group private WebDAV URI $instance_uri/home/$group/directory/file gets file contents")
	}
);

# Access public WebDAV domain
$mech->get ("$instance_uri/public/$group/directory/file");
$mech->content_contains ('public test file', "Existing-group public WebDAV URI $instance_uri/public/$group/directory/file gets file contents");

# Access private bin domain for a non-existing instance
$mech->get ("$instance_uri-rubbish/bin/$group/Magellan/DisplayMain");
$mech->content_contains ('<body class="mioga error-page">', "Non-existing instance private bin URI $instance_uri-rubbish/bin/$group/Magellan/DisplayMain gets an error page");

# Access public bin domain for a non-existing instance
$mech->get ("$instance_uri-rubbish/pubbin/$group/FileBrowser/DisplayMain");
$mech->content_contains ('<body class="mioga error-page">', "Non-existing instance public bin URI $instance_uri-rubbish/pubbin/$group/FileBrowser/DisplayMain gets an error page");

# Access private WebDAV domain for a non-existing instance
$mech->get ("$instance_uri-rubbish/home/$group");
$mech->content_contains ('<body class="mioga error-page">', "Non-existing instance private WebDAV URI $instance_uri-rubbish/home/$group gets an error page");

# Access public WebDAV domain for a non-existing instance
$mech->get ("$instance_uri-rubbish/public/$group");
$mech->content_contains ('<body class="mioga error-page">', "Non-existing instance public WebDAV URI $instance_uri-rubbish/public/$group gets an error page");

# Access private bin domain for a non-existing group of an existing instance
CheckProtected (
	"$instance_uri/bin/$group-rubbish/Magellan/DisplayMain",
	"Non-existing group of an existing instance private bin URI $instance_uri/bin/$group-rubbish/Magellan/DisplayMain redirects to login",
	sub {
		$mech->content_contains ('<body class="mioga error-page">', "Non-existing group of an existing instance private bin URI $instance_uri/bin/$group-rubbish/Magellan/DisplayMain gets an error page");
	}
);

# Access public bin domain for a non-existing group of an existing instance
$mech->get ("$instance_uri/pubbin/$group-rubbish/FileBrowser/DisplayMain");
$mech->content_contains ('<body class="mioga error-page">', "Non-existing group of an existing instance public bin URI $instance_uri/pubbin/$group-rubbish/FileBrowser/DisplayMain gets an error page");

# Access private WebDAV domain for a non-existing group of an existing instance
CheckProtected (
	"$instance_uri/home/$group-rubbish",
	"Non-existing group of an existing instance private WebDAV URI $instance_uri/home/$group-rubbish redirects to login",
	sub {
		$mech->content_contains ('<body class="mioga error-page">', "Non-existing group of an existing instance private WebDAV URI $instance_uri/home/$group-rubbish gets an error page");
	}
);

# Access public WebDAV domain for a non-existing group of an existing instance
$mech->get ("$instance_uri/rubbish/$group");
$mech->content_contains ('<body class="mioga error-page">', "Non-existing domain URI $instance_uri/rubbish/$group gets an error page");


#-------------------------------------------------------------------------------
# More-than-five-member addresses
#-------------------------------------------------------------------------------
diag ("\n5+-member addresses");

# Access private WebDAV domain
CheckProtected (
	"$instance_uri/home/$group/directory/directory/file",
	"Existing-group private WebDAV URI $instance_uri/home/$group/directory/directory/file redirects to login",
	sub {
		$mech->content_contains ('private test file', "Existing-group private WebDAV URI $instance_uri/home/$group/directory/directory/file gets file contents")
	}
);

# Access public WebDAV domain
$mech->get ("$instance_uri/public/$group/directory/directory/file");
$mech->content_contains ('public test file', "Existing-group public WebDAV URI $instance_uri/public/$group/directory/directory/file gets file contents");

# Access private bin domain
$mech->get ("$instance_uri/bin/$group/Magellan/DisplayMain/rubbish");
$mech->content_contains ('<body class="mioga error-page">', "Invalid private binary 6-member URI $instance_uri/bin/$group/Magellan/DisplayMain/rubbish gets an error page");

# Access public bin domain
$mech->get ("$instance_uri/pubbin/$group/Magellan/DisplayMain/rubbish");
$mech->content_contains ('<body class="mioga error-page">', "Invalid public binary 6-member URI $instance_uri/pubbin/$group/Magellan/DisplayMain/rubbish gets an error page");


#-------------------------------------------------------------------------------
# Resources
#-------------------------------------------------------------------------------
diag ("\nResources");
$mech->get_ok ("$login_uri/files/styles.css", "Login system resources $login_uri/files/styles.css are accessible");
$mech->get_ok ("$theme_uri/theme.css", "Theme CSS $theme_uri/theme.css is accessible");
$mech->get_ok ("$theme_uri/css/mioga.css", "Default Mioga2 CSS $theme_uri/css/mioga.css are accessible");
$mech->get_ok ("$theme_uri/images/16x16/actions/arrow-right.png", "Default Mioga2 images $theme_uri/images/16x16/actions/arrow-right.png are accessible");


#-------------------------------------------------------------------------------
# CDN
#-------------------------------------------------------------------------------
diag ("CDN");
$mech->get_ok ("$help_uri/fr_FR/welcome_user.html", "Help pages $help_uri/fr_FR/welcome_user.html are accessible");
$mech->get_ok ("$jslib_uri/tiny_mce/tiny_mce.js", "JSLib resources $jslib_uri/tiny_mce/tiny_mce.js are accessible");
$mech->get ("$jslib_uri/tiny_mce/");
$mech->content_contains ('<body class="mioga error-page">', "JSLib directory $jslib_uri/tiny_mce/ index is forbidden");
$mech->get ("$theme_uri/css/");
$mech->content_contains ('<body class="mioga error-page">', "Theme CSS directory $theme_uri/css/ index is forbidden");
$mech->get ("$theme_uri/javascript/");
$mech->content_contains ('<body class="mioga error-page">', "Theme Javascript directory $theme_uri/javascript/ index is forbidden");
$mech->get ("$theme_uri/images/");
$mech->content_contains ('<body class="mioga error-page">', "Theme images directory $theme_uri/images/ index is forbidden");
$mech->get ("$cdn_uri/fr_FR/");
$mech->content_contains ('<body class="mioga error-page">', "CDN directory $cdn_uri/fr_FR/ index is not possible");
