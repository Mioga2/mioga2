#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================

use strict;
use Test::More qw(no_plan);
use Test::WWW::Mechanize;
use File::Temp qw/tempfile/;
use HTTP::Request::Common qw(POST);
use Data::Dumper;

if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password [suppres flag]\n";
	exit -1;
}
my $protocol = $ARGV[0];
my $base_uri = $ARGV[1];
my $server = $ARGV[2];
my $login_module_uri = $ARGV[3];
my $instance = $ARGV[4];
my $group = $ARGV[5];
my $login = $ARGV[6];
my $password = $ARGV[7];

my $suppress = 1;
$suppress = $ARGV[8] if (@ARGV > 8);

my $application = 'Louvre';
my $relative_uri = "$base_uri$instance/bin/$group/$application";
my $base = "$protocol://$server$relative_uri";
my $login_uri = "$protocol://$server$base_uri$login_module_uri";

my $mech = Test::WWW::Mechanize->new;

my ($fh, $filename, $req);

# Login to Mioga
$mech->post_ok ("$login_uri/DisplayMain?target=$relative_uri/DisplayMain", { login => $login, password => $password }, "Login to Mioga2 as $login");

# Create a gallery
my $gallery_path = "$base_uri$instance/home/$group";
my $gallery_uri = "$gallery_path/directory";
$mech->post_ok ("$base/CreateGallery", { maxwidth => 100, maxheight => 100, dir => $gallery_uri, locale => 'fr_FR', background => 'black' }, "Convert $base_uri$instance/home/$group/directory into a gallery");
$mech->content_contains ('<body id="Louvre_body" class="Louvre body">', 'Got response');

# Get galleries
$mech->post_ok ("$base/GetGalleries.xml", { path => $gallery_path }, "Get list of galleries");
print STDERR Dumper($mech->content);
$mech->content_contains ("<target>$gallery_uri/mioga2-louvre</target>", 'Previously-created gallery is found');

$mech->get_ok ("$login_uri/Logout");
