#!/usr/bin/perl
#
#============================================================================
#
#	Mioga2 Project (C) 2014 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation; either version 2, or (at your option) any later
#   version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, 675 Mass
#   Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description:
#		Mioga2 Mechanize context module
#

package Mechioga2::Context;
use Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(setContext login logout);

use strict;
use warnings;

use Test::WWW::Mechanize;
use HTTP::Request;
use HTTP::Request::Common;

#
#	function setContext
#
#	Change the testing environment context in the current script
#
#	In: instance (string), app (string)
#	Out: (none)
#
sub setContext {
	my ($context, $instance, $app) = @_;

	$context->{instance} = $instance;
	$context->{app} = $app;

	$context->{relative_uri} = $context->{base_uri}.$context->{instance}."/bin/".$context->{group}."/".$app;
	$context->{base} = $context->{protocol}."://".$context->{server}.$context->{relative_uri};
	$context->{login_uri} = $context->{protocol}."://".$context->{server}.$context->{base_uri}.$context->{login_module_uri};

	print "\nChanging test context to $instance\::$app.\n";

	return $context;
}


#
#	function login
#
#	Log in the current context
#
#	In: (none)
#	Out: (none)
#
sub login {
	my ($context) = @_;
	$context->{mech}->post_ok($context->{login_uri}."/DisplayMain?target=".$context->{relative_uri}."/DisplayMain",
							  { login => $context->{login}, password => $context->{password} }, "Logged in as ".$context->{login}.".");

	if ($context->{group} eq '__MIOGA-USER__') {
		# Update context with user ident to avoid further redirects
		my ($ident) = ($context->{mech}->response->base =~ m~\Q$context->{protocol}://$context->{server}$context->{base_uri}$context->{instance}\E/bin/([^/]*)/~);
		$context->{group} = $ident;
		setContext ($context, $context->{instance}, $context->{app});
	}
}


#
#	function logout
#
#	Logout in the current instance
#
#	In: (none)
#	Out: (none)
#
sub logout {
	my ($context) = @_;
	$context->{mech}->get_ok($context->{login_uri}."/Logout", "Logged out.");
}

1;
