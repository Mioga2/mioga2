#!/usr/bin/perl
#
#============================================================================
#
#	Mioga2 Project (C) 2014 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation; either version 2, or (at your option) any later
#   version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, 675 Mass
#   Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description:
#		Mioga2 Mechanize Bottin module
#

package Mechioga2::Bottin;
use Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(bottinImportUsers bottinCreateUser bottinDeleteUser bottinCheckUserActive);

use strict;
use warnings;

use Test::More;
use Test::WWW::Mechanize;
use HTTP::Request;
use HTTP::Request::Common;
use JSON::XS;


#
#	function bottinImportUsers
#
#	Import users in Bottin from a CSV file
#
#	In: filename (string)
#	Out: (none)
#
sub bottinImportUsers {
	my ($context, $filename) = @_;

	$context->{mech}->post($context->{base}."/ImportUsers", 
		'Content_Type' => 'form-data',
		'Content' => [
			'file' => [$filename, $filename, 'Content-type' => 'text/csv'],
			'mode' => 'add'
		]
	);
	
	foreach ($context->{mech}->find_all_inputs(type => 'checkbox')) {
		$context->{mech}->field($_->name => 'on');
	}

	$context->{mech}->submit_form_ok ({
		form_number => 1
	}, "CSV importation succeed.");
}


#
#	function bottinCreateUser
#
#	Create/Import an user in Bottin
#
#	In: user (hashref)
#	Out: isCreated (boolean)
#
sub bottinCreateUser {
	my ($context, $user) = @_;
	my $isCreated = 0;

	# Query the Web Service to check if the user exists
	$context->{mech}->post_ok($context->{base}."/SetUser.json", { email => $user->{email} }, "Searching for " . $user->{email} . "...");

	my $user_info = JSON::XS::decode_json($context->{mech}->response->decoded_content);

	# If user can be created
	if ($user_info->{errors}->[0]->[0] eq "create") {
		# Extend user infos
		$user_info->{user} = {%{$user_info->{user}}, %$user};
	
		# Merge user informations with returned values 
		$context->{mech}->post_ok($context->{base}."/SetUser.json", $user_info->{user}, "Creating " . $user->{email} . "...");
		
		my $result = JSON::XS::decode_json($context->{mech}->response->decoded_content);

		# Check for errors
		if (keys(%{$result->{errors}}) eq 0) {
			$isCreated = 1;
		}
	} elsif ($user_info->{errors}->[0]->[0] eq "import") {
		$user->{importdn} = $user_info->{errors}->[0]->[1]->[1];

		# Merge user informations with returned values 
		$context->{mech}->post_ok($context->{base}."/SetUser.json", $user, "Importing " . $user->{email} . "...");
		my $result = JSON::XS::decode_json($context->{mech}->response->decoded_content);

		# Check for errors
		if (keys(%{$result->{errors}}) eq 0) {
			$isCreated = 1;
		}
	}
	
	return $isCreated;
}


#
#	function bottinDeleteUser
#
#	Delete an user in Bottin
#
#	In: userEmail (string)
#	Out: (none)
#
sub bottinDeleteUser {
	my ($context, $userEmail) = @_;

	foreach (@{getBottinUsersData($context)}) {
		if ($_->{email} eq $userEmail) {
			$context->{mech}->post_ok($context->{base}."/DeleteUser", { rowids => $_->{rowid} }, "Deleting $userEmail...");
			last;
		}
	}
}


#
#	function bottinCheckUserActive
#
#	Check the user status in Bottin
#
#	In: userEmail (string)
#	Out: active (boolean)
#
sub bottinCheckUserActive {
	my ($context, $userEmail) = @_;
	my $active = 0;

	foreach (@{getBottinUsersData($context)}) {
		if ($_->{email} eq $userEmail) {
			$active = $_->{status} eq "active";
			last;
		}
	}

	return $active;
}


#
#	function getBottinUsersData
#
#	Get all users information from Bottin
#
#	In: (none)
#	Out: users (hashref)
#
sub getBottinUsersData {
	my ($context) = @_;
	
	$context->{mech}->get($context->{base}."/GetUserData");
	
	return JSON::XS::decode_json($context->{mech}->response->decoded_content)->{items};
}

1;
