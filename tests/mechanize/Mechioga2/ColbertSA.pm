#!/usr/bin/perl
#
#============================================================================
#
#	Mioga2 Project (C) 2014 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation; either version 2, or (at your option) any later
#   version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, 675 Mass
#   Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description:
#		Mioga2 Mechanize ColbertSA module
#

package Mechioga2::ColbertSA;
use Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(createInstance deleteInstance updateSkeletons);

use strict;
use warnings;

use Test::More;
use Test::WWW::Mechanize;
use HTTP::Request;
use HTTP::Request::Common;
use JSON::XS;

use Data::Dumper;


#
#	function updateSkeletons
#
#	Update the skeletons through ColbertSA by uploading a ZIP file containing them
#
#	In: filename (string)
#	Out: (none)
#
sub updateSkeletons {
	my ($context, $filename) = @_;

	$context->{mech}->post($context->{base}."/UploadSkeletons", 
		'Content_Type' => 'form-data',
		'Content' => [
			'skeletons_archive' => [$filename, $filename, 'Content-type' => 'application/zip'],
			'type' => 'instance'
		]
	);

	$context->{mech}->content_contains('<div id="upload-skeletons-errors"></div>', "Instance skeletons upload succeed.");
}


#
#	function createInstance
#
#	Create an instance with the specified name and skeleton name
#
#	In: instanceName (string), skeletonFileName (string), adminLogin (string), adminEmail (string), adminPassword (string)
#	Out: rowID (int)
#
sub createInstance {
	my ($context, $instanceName, $skeletonFileName, $adminLogin, $adminEmail, $adminPassword) = @_;

	# Get default language
	my $language = getLanguage($context);

	# Prepare query params
	my $instanceCreationParams = { ident => $instanceName, lang => $language->{ident}, skeleton => $skeletonFileName, admin_ident => $adminLogin,
								   admin_email => $adminEmail, admin_password => $adminPassword, admin_password_confirm => $adminPassword };

	# Retrieve skeleton details
	my $skeleton = getSkeleton($context, $skeletonFileName, $language->{ident});

	# Merge hashes: $instanceCreationParams <= $skeleton.attributes + $instanceCreationParams
	$instanceCreationParams = {%{$skeleton->{attributes}}, %$instanceCreationParams};

	# Create an array containing all enabled applications
	my @apps;
	foreach (@{$skeleton->{applications}->{application}}) {
		push @apps, $_->{ident}
	}

	# Put it into our query params
	@{$instanceCreationParams->{applications}} = @apps;

	# Query web services
	$context->{mech}->post($context->{base}."/SetInstance.json", $instanceCreationParams);

	my $result = JSON::XS::decode_json($context->{mech}->response->decoded_content);

	# Creation could have failed, ensure response success code is true
	ok($result->{success} eq 1, "Instance $instanceName has been created.");

	return $result->{rowid};
}


#
#	function getInstance
#
#	Delete the specified instance
#
#	In: instance (string)
#	Out: (none)
#
sub deleteInstance {
	my ($context, $instance) = @_;

	$context->{mech}->post($context->{base}."/GetInstances.json");

	my $instances = JSON::XS::decode_json($context->{mech}->response->decoded_content);

	my $rowID = -1;

	foreach (@{$instances->{instance}}) {
		if ($_->{ident} eq $instance) {
			$rowID = $_->{rowid};
		}
	}
	
	$context->{mech}->post_ok($context->{base}."/DeleteInstance.json", { rowid => $rowID }, "Deleted instance $instance.");
}


#
#	function getLanguage
#
#	Get the last supported language (in other words, french)
#
#	In: (none)
#	Out: language (hashref)
#
sub getLanguage {
	my ($context) = @_;
	
	$context->{mech}->post($context->{base}."/GetLanguages.json");
	my $languages = JSON::XS::decode_json($context->{mech}->response->decoded_content);
	
	return $languages->{language}->[-1];
}


#
#	function getSkeleton
#
#	Get the skeleton details for the specified skeleton file name
#
#	In: skeletonFileName (string), language (string)
#	Out: skeleton (hashref)
#
sub getSkeleton {
	my ($context, $skeletonFileName, $language) = @_;
	
	$context->{mech}->post_ok($context->{base}."/GetSkeletonDetails.json", { type => "instance", lang => $language, file => $skeletonFileName },
							  "Retrieving skeleton details for '$skeletonFileName'...");
				   
	return JSON::XS::decode_json($context->{mech}->response->decoded_content);
}

1;
