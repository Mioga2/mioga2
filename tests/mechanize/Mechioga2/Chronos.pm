#===============================================================================
#
#         FILE:  Chronos.pm
#
# REQUIREMENTS:  ---
#        NOTES:  ---
#       AUTHOR:  Sébastien NOBILI <technique@alixen.fr>
#      COMPANY:  Alixen Labs. (http://www.alixen.org)
#      CREATED:  04/01/2016 14:06
#===============================================================================
#
#  Copyright (c) year 2016, The Mioga2 Project
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the gnu general public license as published by
#  the free software foundation; either version 2 of the license, or
#  (at your option) any later version.
#
#===============================================================================
#
#   Description:

=head1 NAME

Chronos.pm

=head1 DESCRIPTION

Chronos tests helper module

=head1 METHODS DESRIPTION

=cut

#
#===============================================================================

use strict;
use warnings;

package Mechioga2::Chronos;
use Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(chronosGetCategory chronosGetCalendar chronosCreateEvent);

use Test::More;
use Test::WWW::Mechanize;
use HTTP::Request;
use HTTP::Request::Common;
use JSON::XS;

use Data::Dumper;

my $debug = 1;


#===============================================================================

=head2 chronosGetCategory

Get category from configuration.

=head3 Incoming Arguments

=over

=item I<$name>: (optional) category name to get. If undef, then get first category from list.

=back

=head3 Return value

=over

Category ID or undef

=back

=cut

#===============================================================================
sub chronosGetCategory {
	my ($context, $name) = @_;

	$context->{mech}->get ($context->{base}."/GetConfiguration.json", 'Get category from configuration');
	my $result = JSON::XS::decode_json($context->{mech}->response->decoded_content);

	my @categories = defined ($name) ? grep { $_->{name} eq $name } @{$result->{config}->{categories}} : @{$result->{config}->{categories}};

	if (@categories) {
		return ($categories[0]->{category_id});
	}

	return (undef);
}	# ----------  end of subroutine chronosGetCategory  ----------



#===============================================================================

=head2 chronosGetCalendar

Get calendar from configuration

=head3 Incoming Arguments

=over

=item I<$ident>: (optional) calendar ident to get. If undef, then get first calendar from list.

=back

=head3 Return value

=over

Calendar ID or undef

=back

=cut

#===============================================================================
sub chronosGetCalendar {
	my ($context, $ident) = @_;

	$context->{mech}->get ($context->{base}."/GetConfiguration.json", 'Get calendar from configuration');
	my $result = JSON::XS::decode_json($context->{mech}->response->decoded_content);

	my @calendars = defined ($ident) ? grep { $_->{ident} eq $ident } @{$result->{config}->{calendars}} : @{$result->{config}->{calendars}};

	if (@calendars) {
		return ($calendars[0]->{rowid});
	}

	return (undef);
}	# ----------  end of subroutine chronosGetCalendar  ----------



#===============================================================================

=head2 chronosCreateEvent

Create an event.

=head3 Incoming Arguments

=over

=item I<$event>: The event JSON string.

=item I<$calendar>: The calendar ID to write the event to.

=back

=head3 Return value

=over

1 on success, 0 on failure

=back

=cut

#===============================================================================
sub chronosCreateEvent {
	my ($context, $event, $calendar) = @_;

	# Post event creation
	$context->{mech}->post ($context->{base}."/CreateEvent.json",
		{
			event => $event,
			calendar_id => $calendar
		});

	# Check server response
	my $content = $context->{mech}->response->decoded_content;
	if ($content && ($content =~ /"status":"OK"/)) {
		return (1);
	}
	else {
		return (0);
	}
}	# ----------  end of subroutine chronosCreateEvent  ----------


1;

