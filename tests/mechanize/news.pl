#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================

use strict;
use Test::More qw(no_plan);
use Test::WWW::Mechanize;
use Data::Dumper;

if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password [suppres flag]\n";
	exit -1;
}
my $protocol = $ARGV[0];
my $base_uri = $ARGV[1];
my $server = $ARGV[2];
my $login_module_uri = $ARGV[3];
my $instance = $ARGV[4];
my $group = $ARGV[5];
my $login = $ARGV[6];
my $password = $ARGV[7];

my $application = 'News';
my $relative_uri = "$base_uri$instance/bin/$group/$application";
my $base = "$protocol://$server$relative_uri";
my $login_uri = "$protocol://$server$base_uri$login_module_uri";

my $suppress = 1;
$suppress = $ARGV[8] if (@ARGV > 8);

my $mech = Test::WWW::Mechanize->new;

# Login to Mioga
$mech->post_ok ("$login_uri/DisplayMain?target=$relative_uri/DisplayMain", { login => $login, password => $password }, "Login to Mioga2 as $login");

# DisplayMain

$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains( "WriteNewsItem");

# DisplayMain archive

$mech->get_ok( "$base/DisplayMain?archive=1" );
$mech->content_contains( "WriteNewsItem");

# DisplayWaitingNews

$mech->get_ok( "$base/DisplayWaitingNews" );
$mech->content_contains( "WriteNewsItem");

# DisplayDraftNews

$mech->get_ok( "$base/DisplayDraftNews" );
$mech->content_contains( "WriteNewsItem");

# DisplayOptions

$mech->get_ok( "$base/DisplayOptions" );
$mech->content_contains( "Element.update");

# UpdateOptions

$mech->post_ok( "$base/UpdateOptions", {moderated => 0,
										moderator_mail => 1,
										user_mail => 1,
										delay => 91,
										}
									);
$mech->content_contains('$("disable-window").hide()');

# WriteNewsItem

$mech->get_ok( "$base/WriteNewsItem?page=DisplayMain" );
$mech->content_contains( '<form method="post" action="UpdateNewsItem">');

# UpdateNewsItem

$mech->post_ok( "$base/UpdateNewsItem", {title => 'test news',
										draft => 1,
										page => 'DisplayDraftNews',
										candraft => 1,
										status => '',
										editaction => 'CreateNews',
										text => 'Text of news in draft mode',
										}
									);
$mech->content_contains( 'Text of news in draft mode');

# Create news in main window
$mech->post_ok( "$base/UpdateNewsItem", {title => 'test news',
										draft => 0,
										page => 'DisplayMain',
										candraft => 1,
										status => '',
										editaction => 'CreateNews',
										text => 'Text of news in main mode',
										}
									);
$mech->content_contains( 'Text of news in main mode');

my $news_id;
my @links = $mech->followable_links();
foreach my $l (@links) {
	#print Dumper($l);
	my $url = $l->url();
	if ($url =~ /EditNewsItem/) {
		$url =~ /rowid=([0-9]+)/;
		$news_id = $1;
		#print "category_id = $category_id\n";
		last;
	}
}
# EditNewsItem
$mech->get_ok( "$base/EditNewsItem?page=DisplayMain&rowid=$news_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/EditNewsItem?page=DisplayMain&rowid=$news_id" );
$mech->content_contains('<form method="post" action="UpdateNewsItem">');

$mech->post_ok( "$base/UpdateNewsItem", {title => 'Modified news',
										rowid => $news_id,
										draft => 0,
										page => 'DisplayMain',
										candraft => 1,
										status => '',
										editaction => 'CreateNews',
										text => 'Text of modified news in main mode',
										}
									);
$mech->content_contains( 'Text of modified news in main mode');

# DeleteNewsItem
$mech->get_ok( "$base/DeleteNewsItem?page=DisplayMain&rowid=$news_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/DeleteNewsItem?page=DisplayMain&rowid=$news_id" );
$mech->content_contains( "Element.update");

#
# Moderation
#

$mech->post_ok( "$base/UpdateOptions", {moderated => 1,
										moderator_mail => 1,
										user_mail => 1,
										delay => 91,
										}
									);
$mech->content_contains('$("disable-window").hide()');

#Add news with Decline
# WriteNewsItem
$mech->get_ok( "$base/WriteNewsItem?page=DisplayMain" );
$mech->content_contains( '<form method="post" action="UpdateNewsItem">');
# UpdateNewsItem
$mech->post_ok( "$base/UpdateNewsItem", {title => 'test moderated news',
										draft => 0,
										page => 'DisplayWaitingNews',
										candraft => 1,
										status => '',
										editaction => 'CreateNews',
										text => 'Text of moderated news',
									}
								);
$mech->content_contains( 'Text of moderated news');

@links = $mech->followable_links();
foreach my $l (@links) {
	#print Dumper($l);
	my $url = $l->url();
	if ($url =~ /EditNewsItem/) {
		$url =~ /rowid=([0-9]+)/;
		$news_id = $1;
		#print "category_id = $category_id\n";
		last;
	}
}

$mech->get_ok( "$base/DeclineNewsItem?rowid=$news_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/DeclineNewsItem?rowid=$news_id" );
$mech->content_lacks( 'Text of moderated news');

#Add news with Validate
# WriteNewsItem
$mech->get_ok( "$base/WriteNewsItem?page=DisplayMain" );
$mech->content_contains( '<form method="post" action="UpdateNewsItem">');
# UpdateNewsItem
$mech->post_ok( "$base/UpdateNewsItem", {title => 'test moderated news 2',
										draft => 0,
										page => 'DisplayWaitingNews',
										candraft => 1,
										status => '',
										editaction => 'CreateNews',
										text => 'Text of moderated news 2',
									}
								);
$mech->content_contains( 'Text of moderated news 2');

@links = $mech->followable_links();
foreach my $l (@links) {
	#print Dumper($l);
	my $url = $l->url();
	if ($url =~ /EditNewsItem/) {
		$url =~ /rowid=([0-9]+)/;
		$news_id = $1;
		#print "category_id = $category_id\n";
		last;
	}
}

$mech->get_ok( "$base/ValidateNewsItem?rowid=$news_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ValidateNewsItem?rowid=$news_id" );
$mech->content_lacks( 'Text of moderated news 2');

$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains( 'Text of moderated news 2');

#
if ($suppress) {

	foreach my $fct (qw(DisplayMain DisplayDraftNews DisplayWaitingNews)) {
		$mech->get_ok( "$base/$fct" );
		@links = $mech->followable_links();
		foreach my $l (@links) {
			my $url = $l->url();
			if ($url =~ /EditNewsItem/) {
				$url =~ /rowid=([0-9]+)/;
				$news_id = $1;
				$mech->get_ok( "$base/DeleteNewsItem?page=DisplayMain&rowid=$news_id" );
			}
		}
	}
}

$mech->get_ok ("$login_uri/Logout");
