#!/usr/bin/perl
#
#============================================================================
#
#	Mioga2 Project (C) 2014 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation; either version 2, or (at your option) any later
#   version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, 675 Mass
#   Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description:
#		Testing environment setup
#

use strict;
use warnings;

use Mechioga2::Context;
use Mechioga2::ColbertSA;

use Test::More qw(no_plan);

if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password\n";
	exit -1;
}

my $context = {
	protocol => $ARGV[0],
	base_uri => $ARGV[1],
	server => $ARGV[2],
	login_module_uri => $ARGV[3],
	instance => $ARGV[4],
	group => $ARGV[5],
	login => $ARGV[6],
	password => $ARGV[7],
	mech => Test::WWW::Mechanize->new
};


# Login to default instance
$context = setContext($context, $context->{instance}, "ColbertSA");
login($context);

# Update skeletons
updateSkeletons($context, "skel.zip");

# Create instances
createInstance($context, "Instance-RW1", "01-instance-ldap-rw.xml", "admin", $context->{login}, $context->{password});
createInstance($context, "Instance-RW2", "01-instance-ldap-rw.xml", "admin", $context->{login}, $context->{password});
createInstance($context, "Instance-AF", "03-instance-ldap-rw-limited.xml", "admin", $context->{login}, $context->{password});
createInstance($context, "Instance-RO", "02-instance-ldap-ro.xml", "admin", $context->{login}, $context->{password});

# Log out
logout($context);
