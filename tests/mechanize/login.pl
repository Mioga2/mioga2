#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================

use strict;
use Test::More qw(no_plan);
use Test::WWW::Mechanize;
use Data::Dumper;
use JSON::XS;

if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password [suppres flag]\n";
	exit -1;
}
my $protocol = $ARGV[0];
my $base_uri = $ARGV[1];
my $server = $ARGV[2];
my $login_module_uri = $ARGV[3];
my $instance = $ARGV[4];
my $group = $ARGV[5];
my $login = $ARGV[6];
my $password = $ARGV[7];

my $suppress = 1;
$suppress = $ARGV[8] if (@ARGV > 8);

my $relative_uri = "$base_uri$instance/home/$group/";
my $base = "$protocol://$server$relative_uri";
my $login_uri = "$protocol://$server$base_uri$login_module_uri";

my $mech = Test::WWW::Mechanize->new;
$mech->add_header ('Accept-Language' => 'fr,fr-fr;q=0.8,en-us;q=0.5,en;q=0.3');

# Login page
$mech->get_ok ("$login_uri/DisplayMain?target=$relative_uri", "Get login page");
$mech->content_contains ("Authentification Mioga2");

# Password recovery page
$mech->get_ok ("$login_uri/PasswordRecovery?target=$relative_uri/DisplayMain", "Get password recovery page");
$mech->content_contains ("Assistance mot de passe Mioga2");

# Contact page
$mech->get_ok ("$login_uri/Contact?target=$relative_uri/DisplayMain", "Get contact page");
$mech->content_contains ("Contacter un administrateur Mioga2");
$mech->content_contains ("captcha_id");

# Unsuccessful login
$mech->post_ok ("$login_uri/DisplayMain?target=$relative_uri", { login => $login, password => $password . '-fails' }, "Unsuccessful login to Mioga2 as $login");
$mech->content_contains ("Authentification Mioga2");

# Successful login
$mech->post_ok ("$login_uri/DisplayMain?target=$relative_uri", { login => $login, password => $password }, "Successful login to Mioga2 as $login");
$mech->content_lacks ("Authentification Mioga2");
$mech->content_contains ("Index of");

# Logout
$mech->get_ok ("$login_uri/Logout?target=$relative_uri", "Logout from Mioga2");
$mech->content_contains ("Authentification Mioga2");
$mech->get ("$base");
$mech->content_lacks ("Index of");
$mech->title_like (qr{^401.*});

# JSON Unsucessful login
$mech->post_ok ("$login_uri/LoginWS", { login => $login, password => $password . '-fails', target => $relative_uri . '/user' }, "Unsuccessful login to Mioga2 JSON WS as $login");
ok(defined(JSON::XS::decode_json($mech->response->decoded_content)->{failed}), "WS returned an error");

# JSON Successful login
$mech->post_ok ("$login_uri/LoginWS", { login => $login, password => $password, target => $relative_uri . '/user' }, "Successful login to Mioga2 JSON WS as $login");
is(JSON::XS::decode_json($mech->response->decoded_content)->{email}, $login, "Email matches login");

$mech->get_ok ("$login_uri/Logout?target=$relative_uri", "Logout from Mioga2");
