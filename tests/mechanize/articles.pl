#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description: 
#   This script checks a directory structure. Each file should contain its
#   file path relative to base path provided as argument

use strict;
use Test::More qw(no_plan);
use Test::WWW::Mechanize;
use Data::Dumper;

if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password [suppres flag]\n";
	exit -1;
}
my $protocol = $ARGV[0];
my $base_uri = $ARGV[1];
my $server = $ARGV[2];
my $login_module_uri = $ARGV[3];
my $instance = $ARGV[4];
my $group = $ARGV[5];
my $login = $ARGV[6];
my $password = $ARGV[7];

my $suppress = 1;
$suppress = $ARGV[8] if (@ARGV > 8);

my $application = 'Articles';
my $relative_uri = "$base_uri$instance/bin/$group/$application";
my $base = "$protocol://$server$relative_uri";
my $login_uri = "$protocol://$server$base_uri$login_module_uri";

my $mech = Test::WWW::Mechanize->new;

# Login to Mioga
$mech->post_ok ("$login_uri/DisplayMain?target=$relative_uri/DisplayMain", { login => $login, password => $password }, "Login to Mioga2 as $login");

# DisplayMain

$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains('new Ajax.Request("CreateCategory"');

# CreateCategory and UpdateCategory
my @categories;
my @links;
$mech->get_ok( "$base/CreateCategory?label=Category 1&page=DisplayMain" );
$mech->content_contains('Element.update("catg-box"');

$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains('new Ajax.Request("CreateCategory"');

$mech->get_ok( "$base/CreateCategory?label=Category 2&page=DisplayMain" );
$mech->content_contains('Element.update("catg-box"');

$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains('new Ajax.Request("CreateCategory"');

@links = $mech->followable_links();
foreach my $l (@links) {
	#print Dumper($l);
	my $url = $l->url();
	if ($url =~ /DisplayMain\?category_id/) {
		$url =~ /category_id=([0-9]+)/;
		push @categories, $1;
	}
}

my $c = $categories[0];
$mech->get_ok( "$base/UpdateCategory?value=Category $c&rowid=$c AND 1=1" );
$mech->content_contains( "Please contact your system administrator");

# DisplayOptions

$mech->get_ok( "$base/DisplayOptions" );
$mech->content_contains( "Element.update");

# UpdateOptions

$mech->post_ok("$base/UpdateOptions", {moderated => 0,
										public_access => 1,
										extended_ui => 1,
										moderator_mail => 1,
										user_mail => 1,
										}
									);
$mech->content_contains('$("disable-window").hide()');

# WriteArticle in first category

$mech->get_ok("$base/WriteArticle?page=DisplayMain" );
$mech->content_contains('<form method="post" action="UpdateArticle">');

# UpdateArticle

$mech->post_ok("$base/UpdateArticle", {title => 'test article 1',
										draft => 0,
										page => 'DisplayMain',
										categories => $categories[0],
										candraft => 1,
										status => '',
										editaction => 'CreateArticle',
										header => 'Header of Article in main mode',
										contents => 'Text of Article in main mode',
										}
									);
$mech->content_contains('Header of Article in main mode');

# WriteArticle in second category and draft

$mech->get_ok("$base/WriteArticle?page=DisplayMain" );
$mech->content_contains('<form method="post" action="UpdateArticle">');

# UpdateArticle

$mech->post_ok("$base/UpdateArticle", {title => 'test article 2',
										draft => 1,
										page => 'DisplayDraftArticles',
										categories => $categories[1],
										candraft => 1,
										status => '',
										editaction => 'CreateArticle',
										header => 'Header of Article in draft mode',
										contents => 'Text of Article in draft mode',
										}
									);
$mech->content_contains('Header of Article in draft mode');

# DisplayDraftArticles

$mech->get_ok("$base/DisplayDraftArticles");
$mech->content_contains('Header of Article in draft mode');

# publish article in draft moded

$mech->post_ok("$base/UpdateArticle", {title => 'test article 2 after draft',
										draft => 0,
										page => 'DisplayMain',
										categories => $categories[1],
										candraft => 1,
										status => '',
										editaction => 'CreateArticle',
										header => 'Header of Article in draft mode published',
										contents => 'Text of Article in draft mode published',
										}
									);
$mech->content_contains('Text of Article in draft mode published');

$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains('new Ajax.Request("CreateCategory"');
my @articles;
@links = $mech->followable_links();
foreach my $l (@links) {
	my $url = $l->url();
	if ($url =~ /DisplayArticle\?article_id/) {
		$url =~ /article_id=([0-9]+)/;
		push @articles, $1;
	}
}

# WriteArticle to be moderated and validated

$mech->post_ok("$base/UpdateOptions", {moderated => 1,
										public_access => 1,
										extended_ui => 1,
										moderator_mail => 1,
										user_mail => 1,
										}
									);
$mech->content_contains('$("disable-window").hide()');

$mech->get_ok("$base/WriteArticle?page=DisplayMain" );
$mech->content_contains('<form method="post" action="UpdateArticle">');

# UpdateArticle

$mech->post_ok("$base/UpdateArticle", {title => 'test article moderated',
										draft => 0,
										page => 'DisplayWaitingArticles',
										categories => $categories[1],
										candraft => 1,
										status => '',
										editaction => 'CreateArticle',
										header => 'Header of Article in moderated mode',
										contents => 'Text of Article in moderated mode',
										}
									);
$mech->content_contains('Header of Article in moderated mode');

$mech->get_ok( "$base/DisplayWaitingArticles" );
$mech->content_contains('new Ajax.Request("CreateCategory"');

my $article_id;
@links = $mech->followable_links();
foreach my $l (@links) {
	my $url = $l->url();
	if ($url =~ /ViewArticle\?article_id/) {
		$url =~ /article_id=([0-9]+)/;
		$article_id = $1;
		last;
	}
}

$mech->get_ok("$base/ValidateArticle?rowid=$article_id&category_id=1 AND 1=1");
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok("$base/ValidateArticle?rowid=$article_id AND 1=1");
$mech->content_contains( "Please contact your system administrator");

$mech->get_ok("$base/ValidateArticle?rowid=$article_id");
$mech->content_contains('Element.update("articles-box",');


# WriteArticle to be moderated and declined

$mech->get_ok("$base/WriteArticle?page=DisplayMain" );
$mech->content_contains('<form method="post" action="UpdateArticle">');

# UpdateArticle

$mech->post_ok("$base/UpdateArticle", {title => 'test article moderated 2',
										draft => 0,
										page => 'DisplayWaitingArticles',
										categories => $categories[0],
										candraft => 1,
										status => '',
										editaction => 'CreateArticle',
										header => 'Header of Article in moderated mode 2',
										contents => 'Text of Article in moderated mode 2',
										}
									);
$mech->content_contains('Header of Article in moderated mode 2');

$mech->get_ok( "$base/DisplayWaitingArticles" );
$mech->content_contains('new Ajax.Request("CreateCategory"');

@links = $mech->followable_links();
foreach my $l (@links) {
	my $url = $l->url();
	if ($url =~ /ViewArticle\?article_id/) {
		$url =~ /article_id=([0-9]+)/;
		$article_id = $1;
		last;
	}
}

$mech->get_ok("$base/DeclineArticle?rowid=$article_id&category_id=1 AND 1=1");
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok("$base/DeclineArticle?rowid=$article_id AND 1=1");
$mech->content_contains( "Please contact your system administrator");

$mech->get_ok("$base/DeclineArticle?rowid=$article_id");
$mech->content_contains('Element.update("articles-box",');

# DisplayArticle

$mech->get_ok("$base/DisplayArticle?article_id=$articles[0] AND 1=1");
$mech->content_contains( "Please contact your system administrator");

$mech->get_ok("$base/DisplayArticle?article_id=$articles[0]");
$mech->content_contains("EditArticle?rowid=$articles[0]");

# SaveArticle

$mech->get_ok("$base/SaveArticle?uri=/home/admin/article.html");
$mech->content_contains('$("download-box").hide();');

$mech->get_ok("http://$server$base_uri$instance/home/admin/article.html");

# EditArticle

$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains('new Ajax.Request("CreateCategory"');

@links = $mech->followable_links();
foreach my $l (@links) {
	my $url = $l->url();
	if ($url =~ /DisplayArticle\?article_id/) {
		$url =~ /article_id=([0-9]+)/;
		$article_id = $1;
		last;
	}
}

$mech->get_ok("$base/EditArticle?rowid=$article_id AND 1=1&page=DisplayArticle?article_id=$article_id" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok("$base/EditArticle?rowid=$article_id&page=DisplayArticle?article_id=$article_id" );
$mech->content_contains('<form method="post" action="UpdateArticle">');

$mech->post_ok("$base/UpdateArticle", {title => 'test  edited',
										draft => 0,
										rowid => $article_id,
										page => "DisplayArticle?article_id=$article_id",
										categories => $categories[0],
										candraft => 1,
										status => 'published',
										editaction => 'UpdateArticle',
										header => 'Header of Article edited',
										contents => 'Text of Article edited',
										}
									);
$mech->content_contains("EditArticle?rowid=$article_id");

# ViewArticle

$mech->get_ok("$base/DisplayWaitingArticles");
$mech->content_contains('Text of Article edited');

@links = $mech->followable_links();
foreach my $l (@links) {
	my $url = $l->url();
	if ($url =~ /DisplayArticle\?article_id/) {
		$url =~ /article_id=([0-9]+)/;
		$article_id = $1;
		last;
	}
}

$mech->get_ok("$base/ViewArticle?article_id=$article_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok("$base/ViewArticle?article_id=$article_id" );
$mech->content_contains('Text of Article edited');

# DeleteArticle

$mech->get_ok("$base/DeleteArticle?rowid=$article_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok("$base/DeleteArticle?rowid=$article_id&page=DisplayWaitingArticles" );
$mech->content_contains('Element.update("catg-box",');

#'DeleteCategory'
$mech->get_ok("$base/DeleteCategory?rowid=$categories[0] AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok("$base/DeleteCategory?rowid=$categories[0]&page=DisplayMain" );
$mech->content_contains( "Please contact your system administrator");

#'CommentArticle'

$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains('new Ajax.Request("CreateCategory"');

@links = $mech->followable_links();
foreach my $l (@links) {
	my $url = $l->url();
	if ($url =~ /DisplayArticle\?article_id/) {
		$url =~ /article_id=([0-9]+)/;
		$article_id = $1;
		last;
	}
}

$mech->get_ok("$base/CommentArticle?article_id=$article_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok("$base/CommentArticle?article_id=$article_id" );
$mech->content_contains('<input class="button" type="submit" name="send_comment"');

$mech->post_ok("$base/CommentArticle", {subject => 'First comment',
										article_id => $article_id,
										send_comment => 'send',
										comment => 'Comment content',
										}
									);
$mech->content_contains("Comment content");

my $parent_id;
@links = $mech->followable_links();
foreach my $l (@links) {
	my $url = $l->url();
	if ($url =~ /parent_id/) {
		$url =~ /article_id=([0-9]+)/;
		$article_id = $1;
		$url =~ /parent_id=([0-9]+)/;
		$parent_id = $1;
		last;
	}
}

$mech->get_ok("$base/CommentArticle?article_id=$article_id&parent_id=$parent_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok("$base/CommentArticle?article_id=$article_id&parent_id=$parent_id" );
$mech->content_contains('<input class="button" type="submit" name="send_comment"');

$mech->post_ok("$base/CommentArticle", {subject => 'First comment response',
										article_id => $article_id,
										parent_id => $parent_id,
										send_comment => 'send',
										comment => 'Comment content response',
										}
									);
$mech->content_contains("Comment content response");

# Suppress all
if ($suppress) {
	$mech->get_ok("$base/DisplayMain");
	@links = $mech->followable_links();
	foreach my $l (@links) {
		my $url = $l->url();
		if ($url =~ /DisplayArticle\?article_id/) {
			$url =~ /article_id=([0-9]+)/;
			$article_id = $1;
			$mech->get_ok("$base/DeleteArticle?rowid=$article_id&page=DisplayMain" );
		}
	}

	$mech->get_ok("$base/DisplayWaitingArticles");
	@links = $mech->followable_links();
	foreach my $l (@links) {
		my $url = $l->url();
		if ($url =~ /DisplayArticle\?article_id/) {
			$url =~ /article_id=([0-9]+)/;
			$article_id = $1;
			$mech->get_ok("$base/DeleteArticle?rowid=$article_id&page=DisplayWaitingArticles" );
		}
	}

	$mech->get_ok("$base/DisplayDraftArticles");
	@links = $mech->followable_links();
	foreach my $l (@links) {
		my $url = $l->url();
		if ($url =~ /DisplayArticle\?article_id/) {
			$url =~ /article_id=([0-9]+)/;
			$article_id = $1;
			$mech->get_ok("$base/DeleteArticle?rowid=$article_id&page=DisplayDraftArticles" );
		}
	}

	foreach my $c (@categories) {
		$mech->get_ok("$base/DeleteCategory?rowid=$c&page=DisplayMain" );
		$mech->content_contains('Element.update("catg-box",');
	}
}

$mech->get_ok ("$login_uri/Logout");
