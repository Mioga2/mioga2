#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================

use strict;
use Test::More qw(no_plan);
use Test::WWW::Mechanize;
use Data::Dumper;

if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password [suppres flag]\n";
	exit -1;
}
my $protocol = $ARGV[0];
my $base_uri = $ARGV[1];
my $server = $ARGV[2];
my $login_module_uri = $ARGV[3];
my $instance = $ARGV[4];
my $group = $ARGV[5];
my $login = $ARGV[6];
my $password = $ARGV[7];

my $suppress = 1;
$suppress = $ARGV[8] if (@ARGV > 8);

my $application = 'Forum';
my $relative_uri = "$base_uri$instance/bin/$group/$application";
my $base = "$protocol://$server$relative_uri";
my $login_uri = "$protocol://$server$base_uri$login_module_uri";

my $mech = Test::WWW::Mechanize->new;

# Login to Mioga
$mech->post_ok ("$login_uri/DisplayMain?target=$relative_uri/DisplayMain", { login => $login, password => $password }, "Login to Mioga2 as $login");

# DisplayMain

$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains( "Ajouter une catégorie");

$mech->content_contains( '<input type="hidden" name="rowid" value=');
my $content = $mech->content();

$content =~ /<input type="hidden" name="rowid" value="([0-9]+)">/;
my $forum_rowid = $1;
#print "forum_rowid = $forum_rowid\n";

# set_parameters
$mech->get_ok( "$base/ProcessAnimAction?action=set_parameters&simple_ihm=1&rowid=$forum_rowid AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessAnimAction?action=set_parameters&simple_ihm=1&rowid=$forum_rowid" );
$mech->content_lacks( "Please contact your system administrator");

# ProcessAnimAction

$mech->get_ok( "$base/ProcessAnimAction?action=bad" );
$mech->content_contains( "Please contact your system administrator");

$mech->get_ok( "$base/ProcessAnimAction?action=add_category" );
$mech->get_ok( "$base/ProcessAnimAction?action=add_category&label=mycategory" );

$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains( "mycategory");
$mech->content_contains( "Nouvelle catégorie");

my $category_id;
my @links = $mech->followable_links();
foreach my $l (@links) {
	#print Dumper($l);
	my $attrs = $l->attrs();
	if (exists($attrs->{onclick})) {
		if ($attrs->{onclick} =~ /AddThematic/) {
			$attrs->{onclick} =~ /category_id=([0-9]+)/;
			$category_id = $1;
			#print "category_id = $category_id\n";
			last;
		}
	}
}

$mech->get_ok( "$base/ProcessAnimAction?action=rename_category&value=renamed category&rowid=$category_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessAnimAction?action=rename_category&value=Renamed category&rowid=$category_id" );
$mech->content_lacks( "Please contact your system administrator");

$mech->get_ok( "$base/ProcessAnimAction?action=add_thematic&category_id=$category_id" );
$mech->content_lacks( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessAnimAction?action=add_thematic&label=mythematic&category_id=$category_id" );
$mech->content_lacks( "Please contact your system administrator");

$mech->get_ok( "$base/ProcessAnimAction?action=add_thematic&category_id=$category_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");

$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains( "mythematic");
$mech->content_contains( "Renamed category");

my $thematic_id;
@links = $mech->followable_links();
foreach my $l (@links) {
	#print Dumper($l);
	my $url = $l->url();
	if ($url =~ /DisplayThematic/) {
		$url =~ /category_id=([0-9]+)/;
		$category_id = $1;
		$url =~ /thematic_id=([0-9]+)/;
		$thematic_id = $1;
		#print "thematic_id = $thematic_id  category_id = $category_id\n";
		last;
	}
}

$mech->get_ok( "$base/ProcessAnimAction?action=rename_thematic&field=label&rowid=$thematic_id" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessAnimAction?action=rename_thematic&value=renamed thematic&rowid=$thematic_id" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessAnimAction?action=rename_thematic&field=label&value=renamed thematic&rowid=$thematic_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessAnimAction?action=rename_thematic&field=label&value=Renamed thematic&rowid=$thematic_id" );
$mech->content_lacks( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessAnimAction?action=rename_thematic&field=desc&value=Description for renamed thematic&rowid=$thematic_id" );
$mech->content_lacks( "Please contact your system administrator");

$mech->get_ok( "$base/DisplayMain" );
$mech->content_contains( "Renamed thematic");
$mech->content_contains( "Description for renamed thematic");


#TODO ProcessMainAction
#     set_user_params

# DisplayThematic

$mech->get_ok( "$base/DisplayThematic?category_id=$category_id&thematic_id=$thematic_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/DisplayThematic?thematic_id=$thematic_id&category_id=$category_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/DisplayThematic?thematic_id=$thematic_id&category_id=$category_id" );
$mech->content_lacks( "Please contact your system administrator");

# ProcessViewAction

$mech->get_ok( "$base/ProcessViewAction?action=bad&category_id=$category_id&thematic_id=$thematic_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessViewAction?action=create_subject&category_id=$category_id&thematic_id=$thematic_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessViewAction?action=create_subject&thematic_id=$thematic_id&category_id=$category_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessViewAction?action=create_subject&category_id=$category_id&thematic_id=$thematic_id" );
$mech->content_lacks( "Please contact your system administrator");

$mech->post_ok( "$base/ProcessViewAction", {action => 'update_subject',
											category_id => $category_id,
											thematic_id => $thematic_id,
											subject_id => 0,
											show_message => 1,
											subject => 'Test subject',
											message => 'Test subject body',
											open => 1,
											post_it => 0,
										}
									);
$mech->content_lacks( "Please contact your system administrator");
$mech->content_contains( "Test subject");

$mech->get_ok( "$base/DisplayThematic?thematic_id=$thematic_id&category_id=$category_id" );
$mech->content_lacks( "Please contact your system administrator");

my $subject_id;
@links = $mech->followable_links();
foreach my $l (@links) {
	#print Dumper($l);
	my $url = $l->url();
	if ($url =~ /DisplaySubject/) {
		$url =~ /subject_id=([0-9]+)/;
		$subject_id = $1;
		#print "thematic_id = $thematic_id  category_id = $category_id\n";
		last;
	}
}

# edit_subject
$mech->get_ok( "$base/ProcessAnimAction?action=edit_subject&subject_id=$subject_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessAnimAction?action=edit_subject&subject_id=$subject_id" );
$mech->content_contains( "Édition d'un sujet");

$mech->post_ok( "$base/ProcessViewAction", {action => 'update_subject',
											category_id => $category_id,
											thematic_id => $thematic_id,
											subject_id => $subject_id,
											subject => 'Test subject modified',
											open => 1,
											post_it => 0,
										}
									);
$mech->content_lacks( "Please contact your system administrator");
$mech->content_contains( "Test subject modified");

# DisplaySubject
$mech->get_ok( "$base/DisplaySubject?subject_id=$subject_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");

$mech->get_ok( "$base/DisplaySubject?subject_id=$subject_id" );
$mech->content_contains( "Test subject body");


$mech->get_ok( "$base/ProcessViewAction?action=create_message&subject_id=$subject_id&category_id=$category_id&thematic_id=$thematic_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessViewAction?action=create_message&subject_id=$subject_id&category_id=$category_id&thematic_id=$thematic_id" );
$mech->content_contains( "Édition d'un message");
$mech->content_contains( "Test subject body");

$mech->post_ok( "$base/ProcessViewAction", {action => 'update_message',
											category_id => $category_id,
											thematic_id => $thematic_id,
											subject_id => $subject_id,
											message_id => 0,
											message => 'Test subject body 2nd message',
										}
									);
$mech->content_contains( "Test subject body 2nd message");

my $message_id;
@links = $mech->followable_links();
foreach my $l (@links) {
	#print Dumper($l);
	my $url = $l->url();
	if ($url =~ /message_id/) {
		$url =~ /message_id=([0-9]+)/;
		$message_id = $1;
		#print "thematic_id = $thematic_id  category_id = $category_id\n";
		last;
	}
}

$mech->get_ok( "$base/ProcessViewAction?action=edit_message&message_id=$message_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessViewAction?action=edit_message&message_id=$message_id" );
$mech->content_contains( "Édition d'un message");

$mech->post_ok( "$base/ProcessViewAction", {action => 'update_message',
											category_id => $category_id,
											thematic_id => $thematic_id,
											subject_id => $subject_id,
											message_id => $message_id,
											message => 'Test subject body modified message',
										}
									);
$mech->content_contains( "Test subject body modified message");

# view_new_messages
# DisplayNewMessages
$mech->get_ok( "$base/ProcessViewAction?action=view_new_messages&subject_id=$subject_id&category_id=$category_id&thematic_id=$thematic_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessViewAction?action=view_new_messages&subject_id=$subject_id&category_id=$category_id&thematic_id=$thematic_id" );
$mech->content_lacks( "Please contact your system administrator");

# signal_message
$mech->get_ok( "$base/ProcessViewAction?action=signal_message&message_id=$message_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessViewAction?action=signal_message&message_id=$message_id" );
$mech->content_lacks( "Please contact your system administrator");

#suppress_message

$mech->get_ok( "$base/ProcessAnimAction?action=suppress_message&message_id=$message_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessAnimAction?action=suppress_message&message_id=$message_id" );
$mech->content_lacks( "Please contact your system administrator");

# Test suprress with bad values
$mech->get_ok( "$base/ProcessAnimAction?action=suppress_subject&thematic_id=$thematic_id&category_id=$category_id&subject_id=$subject_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessAnimAction?action=suppress_thematic&rowid=$thematic_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");
$mech->get_ok( "$base/ProcessAnimAction?action=suppress_category&rowid=$category_id AND 1=1" );
$mech->content_contains( "Please contact your system administrator");


# Suppress all created things

if ($suppress) {
	$mech->get_ok( "$base/DisplayMain" );
	@links = $mech->followable_links();
	my $categories = {};
	foreach my $l (@links) {
		#print Dumper($l);
		my $url = $l->url();
		if ($url =~ /DisplayThematic/) {
			$url =~ /category_id=([0-9]+)/;
			$category_id = $1;
			$url =~ /thematic_id=([0-9]+)/;
			$thematic_id = $1;
			#print "thematic_id = $thematic_id  category_id = $category_id\n";
			$categories->{$category_id}->{thematics}->{$thematic_id} = 1;
		}
	}

	#print Dumper($categories);

	foreach my $c (keys(%$categories)) {
		foreach my $t (keys(%{$categories->{$c}->{thematics}})) {
			$mech->get_ok( "$base/DisplayThematic?thematic_id=$t&category_id=$c" );
			@links = $mech->followable_links();
			foreach my $l (@links) {
				#print Dumper($l);
				my $url = $l->url();
				if ($url =~ /DisplaySubject/) {
					$url =~ /subject_id=([0-9]+)/;
					$subject_id = $1;
					$mech->get_ok( "$base/ProcessAnimAction?action=suppress_subject&thematic_id=$t&category_id=$c&subject_id=$subject_id" );
				}
			}

			$mech->get_ok( "$base/ProcessAnimAction?action=suppress_thematic&rowid=$t" );
			$mech->content_lacks( "Please contact your system administrator");
		}
		$mech->get_ok( "$base/ProcessAnimAction?action=suppress_category&rowid=$c" );
		$mech->content_lacks( "Please contact your system administrator");
	}
}

$mech->get_ok ("$login_uri/Logout");
