#!/usr/bin/perl -w
#============================================================================
#Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation; either version 2, or (at your option) any later
#   version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, 675 Mass
#   Ave, Cambridge, MA 02139, USA.
#
# ============================================================================

use strict;
use Test::More qw(no_plan);
use Test::WWW::Mechanize;
use Data::Dumper;

if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password [suppres flag]\n";
	exit -1;
}
my $protocol = $ARGV[0];
my $base_uri = $ARGV[1];
my $server = $ARGV[2];
my $login_module_uri = $ARGV[3];
my $instance = $ARGV[4];
my $group = $ARGV[5];
my $login = $ARGV[6];
my $password = $ARGV[7];

my $suppress = 1;
$suppress = $ARGV[8] if (@ARGV > 8);

my $application = 'Narkissos';
my $relative_uri = "$base_uri$instance/bin/__MIOGA-USER__/$application";
my $base = "$protocol://$server$relative_uri";
my $login_uri = "$protocol://$server$base_uri$login_module_uri";

my $mech = Test::WWW::Mechanize->new;

# Login to Mioga
$mech->post_ok ("$login_uri/DisplayMain?target=$relative_uri/DisplayMain", { login => $login, password => $password }, "Login to Mioga2 as $login");

# Get main page
$mech->get_ok ("$base/DisplayMain", "Get main page");
$mech->content_contains ('<body id="Narkissos_body" class="Narkissos mioga">', "Page is Narkissos form");
$mech->content_like (qr{<input.*name="firstname"}, "Field for firstname is present");
$mech->content_like (qr{<input.*name="lastname"}, "Field for lastname is present");
$mech->content_like (qr{<input.*name="email"}, "Field for email is present");
$mech->content_like (qr{<input.*type="password".*name="password"}, "Field for current password is present");
$mech->content_like (qr{<input.*type="password".*name="new_password_1"}, "Field for new password is present");
$mech->content_like (qr{<input.*type="password".*name="new_password_2"}, "Field for new password confirmation is present");

# Instance list WS
$mech->get_ok ("$base/GetInstances.xml", "Get instance list");
$mech->content_contains ("<ident>$instance</ident>", "Current instance is in the list");

# Group list WS
$mech->get_ok ("$base/GetGroups.xml", "Get group list");
$mech->content_contains ("<ident>Administrateurs</ident>", 'Group "Administrateurs" is in the list');
$mech->content_contains ("<ident>Colbert</ident>", 'Application "Colbert" is in the list');

# Application list WS
$mech->get_ok ("$base/GetApplications.xml", "Get application list");
$mech->content_contains ("<ident>Workspace</ident>", 'Application "Workspace" is in the list');

$mech->get_ok ("$login_uri/Logout");
