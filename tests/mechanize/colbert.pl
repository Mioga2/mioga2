#!/usr/bin/perl -w
#============================================================================
#Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation; either version 2, or (at your option) any later
#   version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, 675 Mass
#   Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description:
#
#   This script creates the following scenario through HTTP calls to
#   Mioga2::Colbert webservices:
#
#		- create 10 users
#		- create 2 groups having instance admin as animator
#		- create 2 empty teams
#		- check none of the 10 users is member of the 2 groups
#			- by requesting users details
#			- by requesting groups details
#		- update users to have lastname uppercase
#		- invite users #2 to #5 into both groups by updating users
#		- set group #0 animator to user #0 by updating group
#		- set group #1 animator to user #1 by updating group
#		- invite users #6 and #7 into team #0
#		- invite users #8 and #9 into team #1
#		- check users #0 and #1 are respectively members of groups #0 and #1 and not member of any team
#		- check users #2 and #3 are members of both groups and not member of any teams
#		- check other users are not member of any group
#		- check users #6 and #7 are members of team #0
#		- check users #8 and #9 are members of team #1
#		- check group #0 is correct:
#			- animator is the user #0
#			- user #0 is member
#			- users #2 and #3 are members
#			- others are not members
#			- team #0 is member
#		- same check as before with group #1 (animator #1, team #1)
#		- delete the 2 groups (unless command-line argument supress is 0)
#		- delete the 10 users (unless command-line argument supress is 0)

use strict;
use Test::More qw(no_plan);
use Test::WWW::Mechanize;
use LWP::UserAgent;
use HTTP::Request;
use HTTP::Request::Common;
use File::Temp;
use File::Temp qw/tempfile/;
use HTTP::Request::Common qw(POST);
use Mioga2::CSV;
use Data::Dumper;
use XML::XML2JSON;

use utf8;

if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password [suppres flag]\n";
	exit -1;
}
my $protocol = $ARGV[0];
my $base_uri = $ARGV[1];
my $server = $ARGV[2];
my $login_module_uri = $ARGV[3];
my $instance = $ARGV[4];
my $group = $ARGV[5];
my $login = $ARGV[6];
my $password = $ARGV[7];

my $suppress = 1;
$suppress = $ARGV[8] if (@ARGV > 8);

my $application = 'Colbert';
my $relative_uri = "$base_uri$instance/bin/$group/$application";
my $base = "$protocol://$server$relative_uri";
my $login_uri = "$protocol://$server$base_uri$login_module_uri";

my $XML2JSON = new XML::XML2JSON (force_array => 1);

my $mech = Test::WWW::Mechanize->new;
my ($content, $res, $csv, $fh, $filename, @user_rowids, @group_rowids, $rowid, $objxml, $req);

# Login to Mioga
$mech->post_ok ("$login_uri/DisplayMain?target=$relative_uri/DisplayMain", { login => $login, password => $password }, "Login to Mioga2 as $login");

my $admin = { };

my $users = [
	{
		firstname => 'Jean',
		lastname => 'Titouplin',
		email => 'jean.titouplin@test-mioga.org',
		password => 'toto',
		password2 => 'toto',
		lang => 'fr_FR',
		status => 'active',
		applications => ['Workspace', 'AnimUser', 'Narkissos'],
		skeleton => '50-standard.xml'
	},
	{
		firstname => 'Yvan',
		lastname => 'Pèrémère',
		email => 'yvan.peremere@test-mioga.org',
		password => 'toto',
		password2 => 'toto',
		lang => 'fr_FR',
		status => 'active',
		applications => ['Workspace', 'AnimUser', 'Narkissos'],
		skeleton => '50-standard.xml'
	},
	{
		firstname => 'Tom',
		lastname => 'Égérie',
		email => 'tom.egerie@test-mioga.org',
		password => 'toto',
		password2 => 'toto',
		lang => 'fr_FR',
		status => 'active',
		applications => ['Workspace', 'AnimUser', 'Narkissos'],
		skeleton => '50-standard.xml'
	},
	{
		firstname => 'Tex',
		lastname => 'Agère',
		email => 'tex.agere@test-mioga.org',
		password => 'toto',
		password2 => 'toto',
		lang => 'fr_FR',
		status => 'active',
		applications => ['Workspace', 'AnimUser', 'Narkissos'],
		skeleton => '50-standard.xml'
	},
	{
		firstname => 'Thierry',
		lastname => 'Dicule',
		email => 'thierry.dicule@test-mioga.org',
		password => 'toto',
		password2 => 'toto',
		lang => 'fr_FR',
		status => 'active',
		applications => ['Workspace', 'AnimUser', 'Narkissos'],
		skeleton => '50-standard.xml'
	},
	{
		firstname => 'Sacha',
		lastname => 'Touille',
		email => 'sacha.touille@test-mioga.org',
		password => 'toto',
		password2 => 'toto',
		lang => 'fr_FR',
		status => 'active',
		applications => ['Workspace', 'AnimUser', 'Narkissos'],
		skeleton => '50-standard.xml'
	},
	{
		firstname => 'Pierre',
		lastname => 'Kiroul',
		email => 'pierre.kiroul@test-mioga.org',
		password => 'toto',
		password2 => 'toto',
		lang => 'fr_FR',
		status => 'active',
		applications => ['Workspace', 'AnimUser', 'Narkissos'],
		skeleton => '50-standard.xml'
	},
	{
		firstname => 'Odile',
		lastname => 'Hatmoilarate',
		email => 'odile.hatmoilarate@test-mioga.org',
		password => 'toto',
		password2 => 'toto',
		lang => 'fr_FR',
		status => 'active',
		applications => ['Workspace', 'AnimUser', 'Narkissos'],
		skeleton => '50-standard.xml'
	},
	{
		firstname => 'Mélanie',
		lastname => 'Zétofrai',
		email => 'melanie.zetofrai@test-mioga.org',
		password => 'toto',
		password2 => 'toto',
		lang => 'fr_FR',
		status => 'active',
		applications => ['Workspace', 'AnimUser', 'Narkissos'],
		skeleton => '50-standard.xml'
	},
	{
		firstname => 'Paul',
		lastname => 'Ochon',
		email => 'paul.ochon@test-mioga.org',
		password => 'toto',
		password2 => 'toto',
		lang => 'fr_FR',
		status => 'active',
		applications => ['Workspace', 'AnimUser', 'Narkissos'],
		skeleton => '50-standard.xml'
	},
];

my $groups = [
	{
		ident => 'MiogaTestGroup1',
		lang => 'fr_FR',
		default_app => 'Search',
		applications => ['Fouquet', 'Search', 'Magellan'],
		skeleton => '50-standard.xml'
	},
	{
		ident => 'MiogaTestGroup2',
		lang => 'fr_FR',
		default_app => 'Search',
		applications => ['Fouquet', 'Search', 'Magellan'],
		skeleton => '50-standard.xml'
	},
];

my $teams = [
	{
		ident => 'MiogaTestTeam1',
		description => '',
	},
	{
		ident => 'MiogaTestTeam2',
		description => '',
	},
];

my $nb_users = scalar (@$users);
my $nb_groups = scalar (@$groups);
my $nb_teams = scalar (@$teams);


#-------------------------------------------------------------------------------
# Get admin rowid
#-------------------------------------------------------------------------------

# Get full user list
$mech->get_ok ("$base/GetUsers.xml", "Get full user list");
$mech->content_lacks ('<errors>', "No error reported");

$objxml = $XML2JSON->xml2obj($mech->content);
for my $user (@{$objxml->{GetUsers}->{user}}) {
	if ($user->{ident}->[0]->{'$t'} eq 'admin') {
		for (qw/firstname lastname ident rowid/) {
			$admin->{$_} = $user->{$_}->[0]->{'$t'};
		}
	}
}


#-------------------------------------------------------------------------------
# Create users
#-------------------------------------------------------------------------------

# Create users
diag ("Create $nb_users users");
for (0..($nb_users-1)) {
	$mech->post_ok("$base/SetUser.xml", $users->[$_], "Create user '$users->[$_]->{email}'");
	$mech->content_lacks ('<errors>', "No error reported");
	$mech->content_contains ('<rowid>', "User rowid returned");

	($users->[$_]->{rowid}) = ($mech->content =~ m/<rowid>(.*)<\/rowid>/);
}

# Check newly created users can be found in the list
$mech->get_ok ("$base/GetUsers.xml", "Get full user list");
for (0..($nb_users-1)) {
	$mech->content_contains ("<rowid>$users->[$_]->{rowid}</rowid>", "User '$users->[$_]->{email}' is present");
}


#-------------------------------------------------------------------------------
# Create groups
#-------------------------------------------------------------------------------

# Create groups
diag ("Create $nb_groups groups");
for (0..($nb_groups-1)) {
	$groups->[$_]->{anim_id} = $admin->{rowid};
	$mech->post_ok("$base/SetGroup.xml", $groups->[$_], "Create group '$groups->[$_]->{ident}'");
	$mech->content_lacks ('<errors>', "No error reported");
	$mech->content_contains ('<rowid>', "Group rowid returned");

	($groups->[$_]->{rowid}) = ($mech->content =~ m/<rowid>(.*)<\/rowid>/);
}

# Check newly created groups can be found in the list
$mech->get_ok ("$base/GetGroups.xml", "Get full group list");
for (0..($nb_groups-1)) {
	$mech->content_contains ("<rowid>$groups->[$_]->{rowid}</rowid>", "Group '$groups->[$_]->{ident}' is present");
}


#-------------------------------------------------------------------------------
# Create teams
#-------------------------------------------------------------------------------
diag ("Create $nb_teams teams");
for (0..($nb_teams-1)) {
	$mech->post_ok("$base/SetTeam.xml", $teams->[$_], "Create team '$teams->[$_]->{ident}'");
	$mech->content_lacks ('<errors>', "No error reported");

	($teams->[$_]->{rowid}) = ($mech->content =~ m/<rowid>(.*)<\/rowid>/);
}

# Check newly created teams can be found in the list
$mech->get_ok ("$base/GetTeams.xml", "Get full team list");
for (0..($nb_teams-1)) {
	$mech->content_contains ("<rowid>$teams->[$_]->{rowid}</rowid>", "Team '$teams->[$_]->{ident}' is present");
}


#-------------------------------------------------------------------------------
# Check user details are consistent
#-------------------------------------------------------------------------------

# Get user details with no rowid
$mech->get_ok ("$base/GetUserDetails.xml", "Get user details with no rowid");
$mech->content_contains ('<errors>', "Errors are reported");

# Check newly created users are not invited to newly created groups
for (0..($nb_users-1)) {
	$mech->get_ok ("$base/GetUserDetails.xml?rowid=$users->[$_]->{rowid}", "Get user '$users->[$_]->{email}' details");
	$mech->content_contains ("<rowid>$users->[$_]->{rowid}</rowid>", "Details contain expected rowid");
	# Get user idents
	$objxml = $XML2JSON->xml2obj($mech->content);
	$users->[$_]->{ident} = $objxml->{GetUserDetails}->{user}->[0]->{ident}->[0]->{'$t'};
	for (0..($nb_groups-1)) {
		$mech->content_lacks ("<rowid>$groups->[$_]->{rowid}</rowid>", "User is not member of previously created groups");
	}
}

#-------------------------------------------------------------------------------
# Check group details are consistent
#-------------------------------------------------------------------------------

# Get group details with no rowid
$mech->get_ok ("$base/GetGroupDetails.xml", "Get group details with no rowid");
$mech->content_contains ('<errors>', "Errors are reported");

# Check newly created groups do not include newly created users
for (0..($nb_groups-1)) {
	$mech->get_ok ("$base/GetGroupDetails.xml?rowid=$groups->[$_]->{rowid}", "Get group '$groups->[$_]->{ident}' details");
	$mech->content_contains ("<rowid>$groups->[$_]->{rowid}</rowid>", "Details contain expected rowid");
	for (0..($nb_users-1)) {
		$mech->content_lacks ("<rowid>$users->[$_]->{rowid}</rowid>", "None of the previously created users is member of the group");
	}
}


#-------------------------------------------------------------------------------
# Update users (including groups membership)
#-------------------------------------------------------------------------------
for (0..($nb_users-1)) {
	$users->[$_]->{lastname} = uc ($users->[$_]->{lastname});
	# Users #2 and #3 are members of the groups
	my $groups_msg = '';
	if(($_ >= 2) && ($_ <= 3)) {
		my @groups;
		for (0..($nb_groups-1)) {
			push (@groups, $groups->[$_]->{ident});
		}
		$users->[$_]->{groups} = \@groups;
		$groups_msg = " and invite him into groups '" . join ("', '", @groups) . "'";
	}
	$mech->post_ok ("$base/SetUser.xml", $users->[$_], "Set user '$users->[$_]->{email}' lastname to uppercase$groups_msg");
	$mech->content_lacks ('<errors>', "No error reported");
}


#-------------------------------------------------------------------------------
# Update groups (including users membership)
#-------------------------------------------------------------------------------
for (0..($nb_groups-1)) {
	my $anim_email = '';
	if (exists ($users->[$_])) {
		$groups->[$_]->{anim_id} = $users->[$_]->{rowid};
		$anim_email = $users->[$_]->{email};
	}
	# Users #4 and #5 are directly invited into the groups
	my @users;
	for (4..5) {
		push (@users, $users->[$_]->{ident});
	}
	$groups->[$_]->{users} = \@users;
	$mech->post_ok ("$base/SetGroup.xml", $groups->[$_], "Set group '$groups->[$_]->{ident}' animator to user '$anim_email' and invite users '" . join ("', '", @{$groups->[$_]->{users}}));
	$mech->content_lacks ('<errors>', "No error reported");
}


#-------------------------------------------------------------------------------
# Update teams (including users membership)
#-------------------------------------------------------------------------------
# Join teams to groups
for (0..($nb_teams-1)) {
	my @groups;
	push (@groups, $groups->[$_]->{ident});
	$teams->[$_]->{groups} = \@groups;
	$teams->[$_]->{ident} .= "-NewName";
	$mech->post_ok ("$base/SetTeam.xml", $teams->[$_], "Invite team '$teams->[$_]->{ident}' into group '$groups->[$_]->{ident}'");
}

# Invite users
for (0..($nb_teams-1)) {
	my $team_id = $_;
	my @emails = ();
	for ((6+$_*2)..(6+$_*2+2-1)) {
		push (@{$teams->[$team_id]->{users}}, $users->[$_]->{ident});
		push (@emails, $users->[$_]->{email});
	}
	$mech->post_ok ("$base/SetTeam.xml", $teams->[$_], "Invite users '" . join ("', '", @emails) . "' into team '$teams->[$team_id]->{ident}'");
	$mech->content_lacks ('<errors>', "No error reported");
}


#-------------------------------------------------------------------------------
# Check user details are consistent (according to groups membership)
#-------------------------------------------------------------------------------

# Check users #0 and #1 are only member of one group and no team
for (0..1) {
	$mech->get_ok ("$base/GetUserDetails.xml?rowid=$users->[$_]->{rowid}", "Get user '$users->[$_]->{email}' details");
	$mech->content_contains ("<rowid>$users->[$_]->{rowid}</rowid>", "Details contain expected rowid");
	$mech->content_contains ("<rowid>$groups->[$_]->{rowid}</rowid>", "User is member of group '$groups->[$_]->{ident}'");
	my $opposite = abs ($_-1);
	$mech->content_lacks ("<rowid>$groups->[$opposite]->{rowid}</rowid>", "User '$users->[$_]->{email}' is not member of group '$groups->[$opposite]->{ident}'");
	$mech->content_lacks ("<team>", "User '$users->[$_]->{email}' is not member of any team");
}

# Check users #2 and #3 are members of both groups and no team
for (2..3) {
	my $user_id = $_;
	$mech->get_ok ("$base/GetUserDetails.xml?rowid=$users->[$_]->{rowid}", "Get user '$users->[$_]->{email}' details");
	$mech->content_contains ("<rowid>$users->[$_]->{rowid}</rowid>", "Details contain expected rowid");
	for (0..($nb_groups-1)) {
		$mech->content_lacks ("<rowid>$groups->[$_]->{rowid}</rowid>", "User '$users->[$user_id]->{email}' is not member of group '$groups->[$_]->{ident}'");
	}
	$mech->content_lacks ("<team>", "User '$users->[$_]->{email}' is not member of any team");
}

# Check users #4 and #5 are members of both groups and no team
for (4..5) {
	my $user_id = $_;
	$mech->get_ok ("$base/GetUserDetails.xml?rowid=$users->[$_]->{rowid}", "Get user '$users->[$_]->{email}' details");
	$mech->content_contains ("<rowid>$users->[$_]->{rowid}</rowid>", "Details contain expected rowid");
	for (0..($nb_groups-1)) {
		$mech->content_contains ("<rowid>$groups->[$_]->{rowid}</rowid>", "User '$users->[$user_id]->{email}' is member of group '$groups->[$_]->{ident}'");
	}
	$mech->content_lacks ("<team>", "User '$users->[$_]->{email}' is not member of any team");
}

# Check other users are not member of any of the groups created previously
for (6..($nb_users-1)) {
	my $user_id = $_;
	$mech->get_ok ("$base/GetUserDetails.xml?rowid=$users->[$_]->{rowid}", "Get user '$users->[$_]->{email}' details");
	$mech->content_contains ("<rowid>$users->[$_]->{rowid}</rowid>", "Details contain expected rowid");
	for (0..($nb_groups-1)) {
		$mech->content_lacks ("<rowid>$groups->[$_]->{rowid}</rowid>", "User '$users->[$user_id]->{email}' is not member of group '$groups->[$_]->{ident}'");
	}
	my $team_id = ($user_id < 8) ? 0 : 1;
	$mech->content_contains ("<ident>$teams->[$team_id]->{ident}</ident>", "User '$users->[$_]->{email}' is member of team '$teams->[$team_id]->{ident}'");
}


#-------------------------------------------------------------------------------
# Check group details are consistent (according to users and teams membership)
#-------------------------------------------------------------------------------
for (0..($nb_groups-1)) {
	my $group_id = $_;
	$mech->get_ok ("$base/GetGroupDetails.xml?rowid=$groups->[$_]->{rowid}", "Get group '$groups->[$_]->{ident}' details");
	$mech->content_contains ("<rowid>$groups->[$_]->{rowid}</rowid>", "Details contain expected rowid");
	for (0..1) {
		if ($group_id == $_) {
			$mech->content_contains ("<anim_id>$users->[$_]->{rowid}</anim_id>", "User '$users->[$_]->{email}' is animator of the group '$groups->[$group_id]->{ident}'");
			$mech->content_contains ("<rowid>$users->[$_]->{rowid}</rowid>", "User '$users->[$_]->{email}' is member of the group '$groups->[$group_id]->{ident}'");
		}
	}
	for (2..3) {
		$mech->content_lacks ("<rowid>$users->[$_]->{rowid}</rowid>", "User '$users->[$_]->{email}' is not member of the group '$groups->[$group_id]->{ident}'");
	}
	for (4..5) {
		$mech->content_contains ("<rowid>$users->[$_]->{rowid}</rowid>", "User '$users->[$_]->{email}' is member of the group '$groups->[$group_id]->{ident}'");
	}
	for (6..($nb_users-1)) {
		$mech->content_lacks ("<rowid>$users->[$_]->{rowid}</rowid>", "User '$users->[$_]->{email}' is not member of the group '$groups->[$group_id]->{ident}'");
	}
	for (0..1) {
		if ($group_id == $_) {
			$mech->content_contains ("<rowid>$teams->[$_]->{rowid}</rowid>", "Team '$teams->[$_]->{ident}' is member of the group '$groups->[$group_id]->{ident}'");
		}
		else {
			$mech->content_lacks ("<rowid>$teams->[$_]->{rowid}</rowid>", "Team '$teams->[$_]->{ident}' is not member of the group '$groups->[$group_id]->{ident}'");
		}
	}
}


#-------------------------------------------------------------------------------
# Check user counts
#-------------------------------------------------------------------------------
$mech->post_ok ("$base/GetUsers.xml", { mode => 'count' }, "Get total user count");
$mech->content_is ('<GetUsers><count>11</count></GetUsers>', "11 users in the instance");
$mech->post_ok ("$base/GetUsers.xml", { firstname => 'a', match => 'contains', mode => 'count' }, "Get user count containing a 'a' in their firstname");
$mech->content_is ('<GetUsers><count>6</count></GetUsers>', "6 users contain a 'a' in their firstname");
$mech->post_ok ("$base/GetUsers.xml", { firstname => 'a', match => 'begins', mode => 'count' }, "Get user count having their firstname starting with 'a'");
$mech->content_is ('<GetUsers><count>1</count></GetUsers>', "1 user has his firstname starting with 'a'");
$mech->post_ok ("$base/GetUsers.xml", { lastname => 'e', match => 'ends', mode => 'count' }, "Get user count having their lastname ending with 'e'");
$mech->content_is ('<GetUsers><count>6</count></GetUsers>', "6 users has their lastname ending with 'e'");


#-------------------------------------------------------------------------------
# Check group counts
#-------------------------------------------------------------------------------
$mech->post_ok ("$base/GetGroups.xml", { mode => 'count' }, "Get total team count");
$mech->content_is ('<GetGroups><count>3</count></GetGroups>', "3 teams in the instance");
$mech->post_ok ("$base/GetGroups.xml", { ident => 'Mioga', match => 'begins', mode => 'count' }, "Get group count having their ident beginning with 'Mioga'");
$mech->content_is ('<GetGroups><count>2</count></GetGroups>', "2 groups have their ident beginning with 'Mioga'");
$mech->post_ok ("$base/GetGroups.xml", { ident => 'Test', match => 'contains', mode => 'count' }, "Get group count having their ident containing 'Test'");
$mech->content_is ('<GetGroups><count>2</count></GetGroups>', "2 groups have their ident containing 'Test'");
$mech->post_ok ("$base/GetGroups.xml", { ident => 'Group1', match => 'ends', mode => 'count' }, "Get group count having their ident ending with 'Group1'");
$mech->content_is ('<GetGroups><count>1</count></GetGroups>', "1 group have their ident ending with 'Group1'");
$mech->post_ok ("$base/GetGroups.xml", { animator => 'Administrateur', match => 'begins', mode => 'count' }, "Get group count having their animator beginning with 'Administrateur'");
$mech->content_is ('<GetGroups><count>1</count></GetGroups>', "1 group have their animator beginning with 'Administrateur'");
$mech->post_ok ("$base/GetGroups.xml", { animator => 't', match => 'contains', mode => 'count' }, "Get group count having their animator containing 't'");
$mech->content_is ('<GetGroups><count>2</count></GetGroups>', "2 groups have their animator containing 't'");
$mech->post_ok ("$base/GetGroups.xml", { animator => 'n', match => 'ends', mode => 'count' }, "Get group count having their animator endning with 'n'");
$mech->content_is ('<GetGroups><count>2</count></GetGroups>', "2 group have their animator endning with 'n'");


#-------------------------------------------------------------------------------
# Check team counts
#-------------------------------------------------------------------------------
$mech->post_ok ("$base/GetTeams.xml", { mode => 'count' }, "Get total team count");
$mech->content_is ('<GetTeams><count>3</count></GetTeams>', "3 teams in the instance");
$mech->post_ok ("$base/GetTeams.xml", { ident => 'Mioga', match => 'begins', mode => 'count' }, "Get team count having their ident beginning with 'Mioga'");
$mech->content_is ('<GetTeams><count>2</count></GetTeams>', "2 teams have their ident beginning with 'Mioga'");
$mech->post_ok ("$base/GetTeams.xml", { ident => 'Team', match => 'contains', mode => 'count' }, "Get team count having their ident containing with 'Team'");
$mech->content_is ('<GetTeams><count>2</count></GetTeams>', "2 teams have their ident containing with 'Team'");
$mech->post_ok ("$base/GetTeams.xml", { ident => '1-NewName', match => 'ends', mode => 'count' }, "Get team count having their ident ending with '1-NewName'");
$mech->content_is ('<GetTeams><count>1</count></GetTeams>', "1 teams have their ident ending with '1-NewName'");


#-------------------------------------------------------------------------------
# Check incorrect match types are not allowed
#-------------------------------------------------------------------------------
$mech->post_ok ("$base/GetUsers.xml", { match => 'rubbish', mode => 'count' }, "Get user count with a incorrect match type");
$mech->content_is ('<GetUsers><errors><match>match</match></errors></GetUsers>', "Incorrect match type is not allowed");
$mech->post_ok ("$base/GetGroups.xml", { match => 'rubbish', mode => 'count' }, "Get group count with a incorrect match type");
$mech->content_is ('<GetGroups><errors><match>match</match></errors></GetGroups>', "Incorrect match type is not allowed");
$mech->post_ok ("$base/GetTeams.xml", { match => 'rubbish', mode => 'count' }, "Get team count with a incorrect match type");
$mech->content_is ('<GetTeams><errors><match>match</match></errors></GetTeams>', "Incorrect match type is not allowed");


#-------------------------------------------------------------------------------
# Cleanup
#-------------------------------------------------------------------------------
if ($suppress) {
	# Delete groups
	for (@$groups) {
		if (exists ($_->{rowid})) {
			$mech->post_ok("$base/DeleteGroup.xml", {
							rowid => $_->{rowid}
						},
						"Delete group '$_->{ident}'"
					);
			$mech->content_lacks ('<errors>', "No error reported");
		}
	}

	# Delete teams
	for (@$teams) {
		if (exists ($_->{rowid})) {
			$mech->post_ok("$base/DeleteTeam.xml", {
							rowid => $_->{rowid}
						},
						"Delete team '$_->{ident}'"
					);
			$mech->content_lacks ('<errors>', "No error reported");
		}
	}

	# Delete users
	for (@$users) {
		if (exists ($_->{rowid})) {
			$mech->post_ok("$base/DeleteUser.xml", {
							rowid => $_->{rowid}
						},
						"Delete user $_->{email}"
					);
			$mech->content_lacks ('<errors>', "No error reported");
		}
	}
}


#-------------------------------------------------------------------------------
# Tags
#-------------------------------------------------------------------------------

# File with duplicate tag
($fh, $filename) = tempfile();
for my $idx (1..10) {
	print $fh "Tag-$idx\n";
}
print $fh "Tag-1\n";
close ($fh);
$req = POST ("$base/SetTagList", 'Content_type' => 'form-data', Content => [file => [$filename, 'tags.txt']]);
$mech->request ($req);
ok ($mech->success, "Tag catalog import succeeded");
$mech->content_contains ('<li>', "Error has been reported");

# Clean file
($fh, $filename) = tempfile();
for my $idx (1..10) {
	print $fh "Tag-$idx\n";
}
close ($fh);
$req = POST ("$base/SetTagList", 'Content_type' => 'form-data', Content => [file => [$filename, 'tags.txt']]);
$mech->request ($req);
ok ($mech->success, "Tag catalog import succeeded");
$mech->content_lacks ('<li>', "No error reported");

# Check catalog is in the list
$mech->get_ok ("$base/GetTagLists.xml", 'Retreive tag catalogs');
$mech->content_contains ('<ident>tags.txt</ident>', "Previously-uploaded catalog is in the list");
$mech->content_contains ('<tags>10</tags>', "Previously-uploaded catalog is complete");

# Get catalog rowid
my ($catalog_rowid) = ($mech->content =~ m/<rowid>([0-9]*)<\/rowid>/);

# Download catalog to file
$mech->get_ok ("$base/GetTagsCatalog?catalog_id=$catalog_rowid", "Download catalog to file");
ok ($mech->ct () eq 'text/plain', "Downloaded content is a text/plain file");
$mech->content_contains ('Tag-1', "Tag-1 is in the list");

# Delete catalog
$mech->get_ok ("$base/DeleteTagsCatalog.json?catalog_id=$catalog_rowid", "Delete catalog");
$mech->content_is ('{"success":1}', 'Operation succeeded');

# Check catalog is no more in the list
$mech->get_ok ("$base/GetTagLists.xml", 'Retreive tag catalogs');
$mech->content_lacks ('<ident>tags.txt</ident>', "Previously-uploaded catalog is no more in the list");

# Enable free tags
$mech->get_ok ("$base/ToggleFreeTags.json", 'Enable free tags');
$mech->content_is ('{"success":1}', 'Operation succeeded');

# Check catalog is in the list
$mech->get_ok ("$base/GetTagLists.xml", 'Retreive tag catalogs');
$mech->content_contains ('<ident></ident>', 'Free tags catalog is in the list');

# Enable free tags
$mech->get_ok ("$base/ToggleFreeTags.json", 'Disable free tags');
$mech->content_is ('{"success":1}', 'Operation succeeded');

# Check catalog is no more in the list
$mech->get_ok ("$base/GetTagLists.xml", 'Retreive tag catalogs');
$mech->content_lacks ('<ident></ident>', 'Free tags catalog is no more in the list');

# Re-upload tag catalog so Magellan tests succeed later
$req = POST ("$base/SetTagList", 'Content_type' => 'form-data', Content => [file => [$filename, 'tags.txt']]);
$mech->request ($req);
ok ($mech->success, "Tag catalog import succeeded");


#-------------------------------------------------------------------------------
# Known bugs
#-------------------------------------------------------------------------------

# Bug #1234 - Blank page when importing CSV data with special characters.
diag ('Bug #1234 - Blank page when importing CSV data with special characters.');
my $converter = Mioga2::CSV->new (
	columns   => [ qw/firstname lastname email ident password skeleton/],
	separator => ',',
	quote     => '"'
);
($fh, $filename) = tempfile();
print $fh $converter->Convert ([
	{
		firstname => 'Test',
		lastname  => 'USER',
		email     => 'test-user@noname.org',
		ident     => 'invalid#ident',
		password  => 'secret',
		skeleton  => '50-standard.xml'
	}
]);
close ($fh);
$req = POST ("$base/ImportUsers", 'Content_type' => 'form-data', Content => [file => [$filename, 'users.csv'], mode => 'create']);
$mech->request ($req);
ok ($mech->success, "CSV file upload succeeded");
$mech->content_like (qr{<div id="import-users-errors">\n.*<ul>}, 'Error has been reported');

$mech->get_ok ("$login_uri/Logout");


#-------------------------------------------------------------------------------
# TODO (SNI - 06/08/2010 11:37): To be removed
# This method will be removed with its associated call (see above) once Mioga2::Colbert is not in dev any more.
# It is not useful for non-regression tests, but so nice to have for the developer working on Mioga2::Colbert...
#-------------------------------------------------------------------------------
sub pause {
	print "Press a key to continue...";
	while (<STDIN>) {
		last;
	}
}
