#!/usr/bin/perl -w
# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description: 
#   This script checks a directory structure. Each file should contain its
#   file path relative to base path provided as argument

use strict;
use Test::More qw(no_plan);
use Test::WWW::Mechanize;
use File::Temp qw/tempfile/;
use HTTP::Request::Common qw(POST);
use Data::Dumper;

if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password [suppres flag]\n";
	exit -1;
}
my $protocol = $ARGV[0];
my $base_uri = $ARGV[1];
my $server = $ARGV[2];
my $login_module_uri = $ARGV[3];
my $instance = $ARGV[4];
my $group = $ARGV[5];
my $login = $ARGV[6];
my $password = $ARGV[7];

my $suppress = 1;
$suppress = $ARGV[8] if (@ARGV > 8);

my $application = 'Magellan';
my $relative_uri = "$base_uri$instance/bin/$group/$application";
my $base = "$protocol://$server$relative_uri";
my $login_uri = "$protocol://$server$base_uri$login_module_uri";

my $mech = Test::WWW::Mechanize->new;

my ($fh, $filename, $req);

# Login to Mioga
$mech->post_ok ("$login_uri/DisplayMain?target=$relative_uri/DisplayMain", { login => $login, password => $password }, "Login to Mioga2 as $login");

my $base_path = $base_uri . $instance;
$base_path =~ s/\//%2F/g;

# B1192 - Download selection fails on directories
$mech->get_ok ("$base/DownloadFiles?entries=$base_path%2Fhome%2F$group&path=$base_path%2Fhome", "B1192 - Download /home/$group");
ok ($mech->ct () eq 'application/zip', "B1192 - Downloaded content type is 'application/zip'");

# B1195 - Blank page when started with a "path" argument.
$mech->get_ok ("$base/DisplayMain?path=$base_path%2Fhome%2F$group", "B1195 - Load Magellan to /home/$group");
$mech->content_contains ('body class="magellan"', "B1195 - Page is not blank");


#-------------------------------------------------------------------------------
# Create files and directories
#-------------------------------------------------------------------------------
# Clean file
my $cur_path = "$base_uri$instance/home/$group";
my $suffix = time;
for my $level (0..2) {

	if ($level) {
		# Create directory
		$mech->post_ok ("$base/CreateResource.xml", { name => "directory-$suffix", path => $cur_path, type => 'folder' }, "Create directory $cur_path/directory-$suffix");
		$mech->content_contains ('<success>true</success>', "No error reported");
		$cur_path .= "/directory-$suffix";
	}

	# Create file
	($fh, $filename) = tempfile();
	print $fh "$cur_path/file";
	close ($fh);
	$req = POST ("$base/UploadFile.xml", 'Content_type' => 'form-data', Content => [file => [$filename, 'file'], destination => "$cur_path"]);
	$mech->request ($req);
	ok ($mech->success, "Upload file $cur_path/file");
	$mech->content_contains ('<success>true</success>', "No error reported");
}


#-------------------------------------------------------------------------------
# Tags
#-------------------------------------------------------------------------------
diag ('Tagging system');
$mech->get_ok ("$base/GetTagList.xml", "Retreive tag list");
$mech->content_contains ('<tag>Tag-1</tag>', 'Tag "Tag-1" is in the list');

# Check mandatory fields to GetProperties
$mech->get_ok ("$base/GetProperties.xml", "Get properties without file argument");
$mech->content_contains ('<errors><uri>disallow_empty</uri></errors>', "URI is mandatory");

my $file1 = "$base_uri$instance/home/$group/file";
my $file2 = "$base_uri$instance/home/$group/directory-$suffix/file";
my $file3 = "$base_uri$instance/home/$group/directory-$suffix/directory-$suffix/file";

# Check mandatory fields to SetProperties
$mech->post_ok ("$base/SetProperties.xml", { }, "Set properties without argument");
$mech->content_contains ('<uri>disallow_empty</uri>', "URI is mandatory");
$mech->content_contains ('<mode>disallow_empty</mode>', "Mode argument is mandatory");

# Set properties to file
$mech->post_ok ("$base/SetProperties.xml", { uri => $file1, description => 'This is a test file', tags => ['Tag-1', 'Tag-3'], mode => 'replace' }, "Set properties to file $file1");
$mech->get_ok ("$base/GetProperties.xml?uri=$file1", "Get properties for file $file1");
$mech->content_contains ('<tag>Tag-1</tag>', "Properties (Tag-1) are correct for $file1");
$mech->content_contains ('<tag>Tag-3</tag>', "Properties (Tag-3) are correct for $file1");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file1");

# Add tag
$mech->post_ok ("$base/SetProperties.xml", { uri => $file1, description => 'This is a test file', tags => ['Tag-5'], mode => 'add' }, "Add a tag to an already-tagged file");
$mech->get_ok ("$base/GetProperties.xml?uri=$file1", "Get properties for file $file1");
$mech->content_contains ('<tag>Tag-1</tag>', "Properties (Tag-1) are correct for $file1");
$mech->content_contains ('<tag>Tag-3</tag>', "Properties (Tag-3) are correct for $file1");
$mech->content_contains ('<tag>Tag-5</tag>', "Properties (Tag-5) are correct for $file1");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file1");

# Remove existing tag
$mech->post_ok ("$base/SetProperties.xml", { uri => $file1, description => 'This is a test file', tags => ['Tag-3'], mode => 'remove' }, "Remove a tag from an already-tagged file");
$mech->get_ok ("$base/GetProperties.xml?uri=$file1", "Get properties for file $file1");
$mech->content_contains ('<tag>Tag-1</tag>', "Properties (Tag-1) are correct for $file1");
$mech->content_contains ('<tag>Tag-5</tag>', "Properties (Tag-5) are correct for $file1");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file1");

# Remove non-existing tag
$mech->post_ok ("$base/SetProperties.xml", { uri => $file1, description => 'This is a test file', tags => ['Non-existing-tag'], mode => 'remove' }, "Remove a non-existing tag from an already-tagged file");
$mech->get_ok ("$base/GetProperties.xml?uri=$file1", "Get properties for file $file1");
$mech->content_contains ('<tag>Tag-1</tag>', "Properties (Tag-1) are correct for $file1");
$mech->content_contains ('<tag>Tag-5</tag>', "Properties (Tag-5) are correct for $file1");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file1");

# Add non-existing tag
$mech->post_ok ("$base/SetProperties.xml", { uri => $file1, description => 'This is a test file', tags => ['Non-existing-tag'], mode => 'add' }, "Add a non-existing tag to an already-tagged file");
$mech->get_ok ("$base/GetProperties.xml?uri=$file1", "Get properties for file $file1");
$mech->content_contains ('<tag>Tag-1</tag>', "Properties (Tag-1) are correct for $file1");
$mech->content_contains ('<tag>Tag-5</tag>', "Properties (Tag-5) are correct for $file1");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file1");

# Add tag to multiple files
$mech->post_ok ("$base/SetProperties.xml", { uri => [$file1, $file2, $file3], description => 'This is a test file', tags => ['Tag-7'], mode => 'add' }, "Add a tag to multiple files");
$mech->get_ok ("$base/GetProperties.xml?uri=$file1", "Get properties for file $file1");
$mech->content_contains ('<tag>Tag-1</tag>', "Properties (Tag-1) are correct for $file1");
$mech->content_contains ('<tag>Tag-5</tag>', "Properties (Tag-5) are correct for $file1");
$mech->content_contains ('<tag>Tag-7</tag>', "Properties (Tag-7) are correct for $file1");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file1");
$mech->get_ok ("$base/GetProperties.xml?uri=$file2", "Get properties for file $file2");
$mech->content_contains ('<tag>Tag-7</tag>', "Properties (Tag-7) are correct for $file2");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file2");
$mech->get_ok ("$base/GetProperties.xml?uri=$file3", "Get properties for file $file3");
$mech->content_contains ('<tag>Tag-7</tag>', "Properties (Tag-7) are correct for $file3");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file3");

# Remove tag to multiple files
$mech->post_ok ("$base/SetProperties.xml", { uri => [$file1, $file2], description => 'This is a test file', tags => ['Tag-7'], mode => 'remove' }, "Remove a tag to multiple files");
$mech->get_ok ("$base/GetProperties.xml?uri=$file1", "Get properties for file $file1");
$mech->content_contains ('<tag>Tag-1</tag>', "Properties (Tag-1) are correct for $file1");
$mech->content_contains ('<tag>Tag-5</tag>', "Properties (Tag-5) are correct for $file1");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file1");
$mech->get_ok ("$base/GetProperties.xml?uri=$file2", "Get properties for file $file2");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file2");
$mech->get_ok ("$base/GetProperties.xml?uri=$file3", "Get properties for file $file3");
$mech->content_contains ('<tag>Tag-7</tag>', "Properties (Tag-7) are correct for $file3");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file3");

# Replace tags to multiple files
$mech->post_ok ("$base/SetProperties.xml", { uri => [$file1, $file2], description => 'This is a test file', tags => ['Tag-9'], mode => 'replace' }, "Replace tags to multiple files");
$mech->get_ok ("$base/GetProperties.xml?uri=$file1", "Get properties for file $file1");
$mech->content_contains ('<tag>Tag-9</tag>', "Properties (Tag-9) are correct for $file1");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file1");
$mech->get_ok ("$base/GetProperties.xml?uri=$file2", "Get properties for file $file2");
$mech->content_contains ('<tag>Tag-9</tag>', "Properties (Tag-9) are correct for $file2");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file2");
$mech->get_ok ("$base/GetProperties.xml?uri=$file3", "Get properties for file $file3");
$mech->content_contains ('<tag>Tag-7</tag>', "Properties (Tag-7) are correct for $file3");
$mech->content_contains ('<description>This is a test file</description>', "Properties (description) are correct for $file3");

$mech->get_ok ("$login_uri/Logout");
