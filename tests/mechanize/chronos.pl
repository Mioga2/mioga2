#!/usr/bin/perl
#
#============================================================================
#
#	Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation; either version 2, or (at your option) any later
#   version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, 675 Mass
#   Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description:
#		Mioga2 Chronos webservices testing script
#

use utf8;

use strict;
use warnings;

use Mechioga2::Context;
use Mechioga2::Chronos;

use Test::More qw(no_plan);


if (@ARGV < 8) {
	print STDERR "Usage : $! protocol base_uri server_name login_module_uri instance group login password\n";
	exit -1;
}

my $context = {
	protocol => $ARGV[0],
	base_uri => $ARGV[1],
	server => $ARGV[2],
	login_module_uri => $ARGV[3],
	instance => $ARGV[4],
	group => '__MIOGA-USER__',
	login => $ARGV[6],
	password => $ARGV[7],
	mech => Test::WWW::Mechanize->new
};

my @events = (
	'{"start_year":2016,"start_month":1,"start_day":7,"start_hours":6,"start_minutes":30,"start_seconds":0
	,"end_year":2016,"end_month":1,"end_day":7,"end_hours":10,"end_minutes":0,"end_seconds":0,"subject":"__SUBJECT__"
	,"desc":"","all_day":false,"fl_recur":false,"rrule":{"period":1,"repeat":"daily","endChoice":1,"endCount"
	:0,"endDate":"","weekdays":[false,false,false,false,false,false,false],"monthmode":1,"fixmonthday":7
	,"monthdaypos":1,"monthday":1,"fixyearday":7,"fixyearmonth":1,"base_start":"2016-01-07T06:30:00.000Z"
	,"base_end":"2016-01-07T10:00:00.000Z"},"category_id":"__CATEGORY__"}'
);


#########################################################################
########################## BEGIN TEST SEQUENCE ##########################
#########################################################################

$context = setContext($context, $context->{instance}, "Chronos");

# Login
login($context);

# Bug #1480 JSON decoding fails because of charset.
diag ('[Bug #1480] JSON decoding fails because of charset');
ok (my $category_id = chronosGetCategory ($context), 'Get first category from configuration');
ok (my $calendar_id = chronosGetCalendar ($context), 'Get first calendar from configuration');
my $event = $events[0];
$event =~ s/__CATEGORY__/$category_id/;
$event =~ s/__SUBJECT__/éssai/;
ok (chronosCreateEvent ($context, $event, $calendar_id), 'Create event with UTF-8 data');

# Logout
logout($context);
