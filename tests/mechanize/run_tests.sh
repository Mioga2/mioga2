#!/bin/sh
# ============================================================================
# Mioga2 Project (C) 2009 The Mioga2 Project
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2, or (at your option) any
#   later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ============================================================================
#
#   Description: 
#   Run the test suite with some arguments
#   run_test.sh server_name instance group login password

if [ ! -f $HOME/.miogatestrc ]; then
	echo "Please create a file named .miogatestrc in your home directory and define the following shell variables:"
	echo "	- PROTOCOL (http/https)"
	echo "	- BASE_URI (with leading slash)"
	echo "	- SERVER_NAME"
	echo "	- LOGIN_URI (login module URI with leading slash)"
	echo "	- INSTANCE"
	echo "	- GROUP"
	echo "	- LOGIN"
	echo "	- PASSWORD"
	exit -1
fi

SUPPRESS=1

. $HOME/.miogatestrc

RUN_DIR=`dirname $0`
PBIN="perl -I $RUN_DIR -I ../../lib"


$PBIN $RUN_DIR"/setup-environment.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS


#
# System elements
#

echo "Login"
$PBIN $RUN_DIR"/login.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS

echo "Router"
$PBIN $RUN_DIR"/router.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS


#
# Group-applications
# 	Run in group context
#

echo "Colbert"
$PBIN $RUN_DIR"/colbert.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS

echo "Forum"
$PBIN $RUN_DIR"/forum.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS

echo "News"
$PBIN $RUN_DIR"/news.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS

echo "Articles"
$PBIN $RUN_DIR"/articles.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS

echo "Poll"
$PBIN $RUN_DIR"/poll.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS

echo "Magellan"
$PBIN $RUN_DIR"/magellan.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS

echo "Louvre"
$PBIN $RUN_DIR"/louvre.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS

echo "ColbertSA"
$PBIN $RUN_DIR"/colbertsa.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS

echo "Mermoz"
$PBIN $RUN_DIR"/mermoz.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS

echo "Booking"
$PBIN $RUN_DIR"/booking.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS

echo "Bottin"
$PBIN $RUN_DIR"/bottin.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS


#
# User-applications
# 	Run in user context
#

echo "Narkissos"
$PBIN $RUN_DIR"/narkissos.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS

echo "Chronos"
$PBIN $RUN_DIR"/chronos.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS


$PBIN $RUN_DIR"/cleanup-environment.pl" $PROTOCOL $BASE_URI $SERVER_NAME $LOGIN_URI $INSTANCE $GROUP $LOGIN $PASSWORD $SUPPRESS
