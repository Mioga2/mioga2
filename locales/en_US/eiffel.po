# translation of eiffel_xsl.po to français
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
msgid ""
msgstr ""
"Project-Id-Version: eiffel\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-03-12 11:51+0100\n"
"PO-Revision-Date: 2013-03-12 11:51+0100\n"
"Language-Team:  english <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Eiffel"
msgstr "Eiffel"

msgid "The Mioga2 planning application"
msgstr "The Mioga2 planning application"

msgid "Read only functions"
msgstr "Read only functions"

msgid "Animation functions"
msgstr "Animation functions"
