# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-12-06 15:10+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Choose a path to save this article"
msgstr ""

msgid "Please provide a path"
msgstr ""

msgid ""
"Please just provide the relative path from Mioga2 with the filename. Ex: /"
"home/admin/folder/article.html"
msgstr ""

msgid ""
"CAUTION: if you provide an already existing filename, it will be overwritten."
msgstr ""

msgid "Save"
msgstr ""

msgid "Cancel"
msgstr ""

msgid "Back to the main page"
msgstr ""

msgid "Save this article to your Mioga2 home"
msgstr ""

msgid "Are you sure you want to validate this article?"
msgstr ""

msgid "Edit this article"
msgstr ""

msgid "There is no article available at the moment."
msgstr ""

msgid ""
" To create a new one, verify that one category exists at least and click on "
"\"Write a new article\"."
msgstr ""

msgid "Current application help"
msgstr ""

msgid "Help"
msgstr ""

msgid "Home"
msgstr ""

msgid "b"
msgstr ""

msgid "kb"
msgstr ""

msgid "Mb"
msgstr ""

msgid "Gb"
msgstr ""

msgid "%s work group"
msgstr ""

msgid "%s resource"
msgstr ""

msgid "%s's workspace"
msgstr ""

msgid "Active"
msgstr ""

msgid "Del. not purged"
msgstr ""

msgid "Destroyed"
msgstr ""

msgid "Deleted"
msgstr ""

msgid "Disabled"
msgstr ""

msgid "Local"
msgstr ""

msgid "LDAP"
msgstr ""

msgid "External"
msgstr ""

msgid "Yes"
msgstr ""

msgid "No"
msgstr ""

msgid "All"
msgstr ""

msgid "View list"
msgstr ""

msgid "Reload my home page"
msgstr ""

msgid "My calendar"
msgstr ""

msgid "My files"
msgstr ""

msgid "Add current application to my links"
msgstr ""

msgid "Server"
msgstr ""

msgid "Folder"
msgstr ""

msgid "Animator"
msgstr ""

msgid "Name"
msgstr ""

msgid "Ident"
msgstr ""

msgid "Location"
msgstr ""

msgid "Description"
msgstr ""

msgid "Private"
msgstr ""

msgid "Public space"
msgstr ""

msgid "Files history"
msgstr ""

msgid "Email"
msgstr ""

msgid "Firstname"
msgstr ""

msgid "Lastname"
msgstr ""

msgid "Size"
msgstr ""

msgid "Status"
msgstr ""

msgid "Type"
msgstr ""

msgid "Autonomous"
msgstr ""

msgid "Time zone"
msgstr ""

msgid "Daily worked time"
msgstr ""

msgid "Worked days"
msgstr ""

msgid "First day of week"
msgstr ""

msgid "New password"
msgstr ""

msgid "New password (confirmation)"
msgstr ""

msgid "Password"
msgstr ""

msgid "Password (confirmation)"
msgstr ""

msgid "Public key file"
msgstr ""

msgid "Private key file"
msgstr ""

msgid "Resource"
msgstr ""

msgid "Public"
msgstr ""

msgid "Groups"
msgstr ""

msgid "Users"
msgstr ""

msgid "Last connection"
msgstr ""

msgid "Profile"
msgstr ""

msgid "Resizable menu"
msgstr ""

msgid "Model"
msgstr ""

msgid "Sunday"
msgstr ""

msgid "Monday"
msgstr ""

msgid "Tuesday"
msgstr ""

msgid "Wednesday"
msgstr ""

msgid "Thursday"
msgstr ""

msgid "Friday"
msgstr ""

msgid "Saturday"
msgstr ""

msgid "January"
msgstr ""

msgid "February"
msgstr ""

msgid "March"
msgstr ""

msgid "April"
msgstr ""

msgid "May"
msgstr ""

msgid "June"
msgstr ""

msgid "July"
msgstr ""

msgid "August"
msgstr ""

msgid "September"
msgstr ""

msgid "October"
msgstr ""

msgid "November"
msgstr ""

msgid "December"
msgstr ""

msgid "Sun"
msgstr ""

msgid "Mon"
msgstr ""

msgid "Tue"
msgstr ""

msgid "Wed"
msgstr ""

msgid "Thu"
msgstr ""

msgid "Fri"
msgstr ""

msgid "Sat"
msgstr ""

msgid "Jan"
msgstr ""

msgid "Feb"
msgstr ""

msgid "Mar"
msgstr ""

msgid "Apr"
msgstr ""

msgid "Jun"
msgstr ""

msgid "Jul"
msgstr ""

msgid "Aug"
msgstr ""

msgid "Sep"
msgstr ""

msgid "Oct"
msgstr ""

msgid "Nov"
msgstr ""

msgid "Dec"
msgstr ""

msgid "Input time is not valid."
msgstr ""

msgid "Input date is not valid."
msgstr ""

msgid "Input date and time are not valid."
msgstr ""

msgid "is not a valid email address"
msgstr ""

msgid "is not a valid resource"
msgstr ""

msgid "is not a valid user"
msgstr ""

msgid "is not a valid group"
msgstr ""

msgid "\"%s\" is not a valid identifier."
msgstr ""

msgid "Identifier can only contain non-accented characters, numbers, - and _."
msgstr ""

msgid "User, group, team or resource called \"%s\" already exists."
msgstr ""

msgid "\"%s\" field must be filled"
msgstr ""

msgid "\"%s\" field is too long"
msgstr ""

msgid "Input passwords are invalid"
msgstr ""

msgid "File or folder with this name already exists"
msgstr ""

msgid "Field must not be empty"
msgstr ""

msgid "Field already exists"
msgstr ""

msgid "Please make a choice"
msgstr ""

msgid "Invalid percent value"
msgstr ""

msgid "Please provide an integer value."
msgstr ""

msgid "Please provide a floating value."
msgstr ""

msgid "Unknown error code"
msgstr ""

msgid "Application error (Simple)"
msgstr ""

msgid "Database access error (DB)"
msgstr ""

msgid "Unable to connect to external Mioga."
msgstr ""

msgid "Unknown error (Other)"
msgstr ""

msgid "Please contact your system administrator to get more information."
msgstr ""

msgid "Error returned by server:"
msgstr ""

msgid "External Mioga is most likely temporarily unavailable."
msgstr ""

msgid "Please retry later."
msgstr ""

msgid "A %s error occured."
msgstr ""

msgid "Lines per page:"
msgstr ""

msgid "previous"
msgstr ""

msgid "next"
msgstr ""

msgid "Pages:"
msgstr ""

msgid "No team invited in group"
msgstr ""

msgid "Search for"
msgstr ""

msgid "user"
msgstr ""

msgid "team"
msgstr ""

msgid "group"
msgstr ""

msgid "resource"
msgstr ""

msgid "Are you sure you want to select these"
msgstr ""

msgid "teams"
msgstr ""

msgid "users"
msgstr ""

msgid "groups"
msgstr ""

msgid "resources"
msgstr ""

msgid "Select all"
msgstr ""

msgid "Deselect all"
msgstr ""

msgid "LargeList should have more than three displayed columns"
msgstr ""

msgid "Deletion successfully done"
msgstr ""

msgid "No selected element"
msgstr ""

msgid "Are you sure you want to delete the following elements?"
msgstr ""

msgid "contains"
msgstr ""

msgid "begins with"
msgstr ""

msgid "Modify"
msgstr ""

msgid "Delete"
msgstr ""

msgid "Actions:"
msgstr ""

msgid "mo"
msgstr ""

msgid "tu"
msgstr ""

msgid "we"
msgstr ""

msgid "th"
msgstr ""

msgid "fr"
msgstr ""

msgid "sa"
msgstr ""

msgid "su"
msgstr ""

msgid "seconds"
msgstr ""

msgid "minutes"
msgstr ""

msgid "hours"
msgstr ""

msgid "days"
msgstr ""

msgid "None"
msgstr ""

msgid "Black"
msgstr ""

msgid "White"
msgstr ""

msgid "Dark blue"
msgstr ""

msgid "Light blue"
msgstr ""

msgid "Dark green"
msgstr ""

msgid "Light green"
msgstr ""

msgid "Orange"
msgstr ""

msgid "Yellow"
msgstr ""

msgid "Purple"
msgstr ""

msgid "Dark pink"
msgstr ""

msgid "Light pink"
msgstr ""

msgid "Brown"
msgstr ""

msgid "Silver"
msgstr ""

msgid "Dark Cyan"
msgstr ""

msgid "Red"
msgstr ""

msgid "Green"
msgstr ""

msgid "Blue"
msgstr ""

msgid "to"
msgstr ""

msgid "fixed"
msgstr ""

msgid "sooner"
msgstr ""

msgid "later"
msgstr ""

msgid "yes"
msgstr ""

msgid "non"
msgstr ""

msgid "Ok"
msgstr ""

msgid "Search"
msgstr ""

msgid "Assign"
msgstr ""

msgid "Choose"
msgstr ""

msgid "Show all"
msgstr ""

msgid "Back"
msgstr ""

msgid "Add"
msgstr ""

msgid "Change"
msgstr ""

msgid "Create"
msgstr ""

msgid "Edit"
msgstr ""

msgid "Extract"
msgstr ""

msgid "Next"
msgstr ""

msgid "Other"
msgstr ""

msgid "Propose"
msgstr ""

msgid "Previous"
msgstr ""

msgid "Reload"
msgstr ""

msgid "Reply"
msgstr ""

msgid "Reserve"
msgstr ""

msgid "Send"
msgstr ""

msgid "View"
msgstr ""

msgid "Submit"
msgstr ""

msgid "Subject"
msgstr ""

msgid "Comment"
msgstr ""

msgid "Written by %s on %s at %s."
msgstr ""

msgid "Send a comment"
msgstr ""

msgid "Categories"
msgstr ""

msgid "%d comment(s)"
msgstr ""

msgid "No comment"
msgstr ""

msgid "An error has occurred:"
msgstr ""


msgid "Logout"
msgstr "Logout"
