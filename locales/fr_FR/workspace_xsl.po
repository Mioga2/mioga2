# translation of workspace_xsl.po to français
# Loic Guitaut <lguitaut@alixen.fr>, 2008.
# Copyright (C) 2008
# This file is distributed under the same license as the Mioga2 package.
msgid ""
msgstr ""
"Project-Id-Version: workspace_xsl\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-04-12 15:05+0200\n"
"PO-Revision-Date: 2008-03-17 11:19+0100\n"
"Last-Translator: Loic Guitaut <lguitaut@alixen.fr>\n"
"Language-Team: français <fr@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

msgid "URL:"
msgstr "URL :"

msgid "Input group is unknown."
msgstr "Le groupe saisi est inconnu."

msgid "All fields must be filled."
msgstr "Tous les champs doivent êter remplis."

msgid "minutes"
msgstr "minutes"

msgid "Back"
msgstr "Retour"

msgid "Search"
msgstr "Rechercher"

msgid "Advanced search"
msgstr "Recherche avancée"

msgid ""
"If you want to change your password, first you need to input your current "
"password and then input your new password two times."
msgstr ""
"Si vous voulez changer votre mot de passe, vous devez tout d'abord saisir "
"votre mot de passe courant puis saisir deux fois votre nouveau mot de passe."

msgid "Modify my details:"
msgstr "Modifier mes coordonnées :"

msgid "My details:"
msgstr "Mes coordonnées :"

msgid "No available application"
msgstr "Aucune application disponible"

msgid "No group"
msgstr "Aucun groupe"

msgid "No resource"
msgstr "Aucune ressource"

msgid "No link"
msgstr "Aucun lien"

msgid "Private space"
msgstr "Espace privé"

msgid "Public space"
msgstr "Espace public"

msgid "My homepage:"
msgstr "Ma page d'accueil"

msgid "My portal:"
msgstr "Mon portail :"

msgid "Link title:"
msgstr "Intitulé du lien :"

msgid "Unable to add link"
msgstr "Impossible d'ajouter le lien"

msgid "Modifications applyed successfully."
msgstr "Modifications correctement appliquées."

msgid "Invalid input time."
msgstr "Heure saisie invalide."

msgid "Input number of hours is invalid."
msgstr "Le nombre d'heures saisi est invalide."

msgid "Input number of minutes is invalid."
msgstr "Le nombre de minutes saisi est invalide."

msgid "My details"
msgstr "Mes coordonnées"

msgid "My schedule"
msgstr "Mon agenda"

msgid "My menu"
msgstr "Mon menu"

msgid "My homepage"
msgstr "Ma page d'accueil"

msgid "My links"
msgstr "Mes liens"

msgid "Add link"
msgstr "Ajouter un lien"

msgid "Name"
msgstr "Nom"

msgid "Link"
msgstr "Lien"

msgid "Bracket"
msgstr "Ordonner"

msgid "Modify"
msgstr "Modifier"

msgid "Delete"
msgstr "Supprimer"

msgid "Modify my schedule parameters"
msgstr "Modifier les paramètres de mon agenda"

msgid "Modify my homepage:"
msgstr "Modifier ma page d'accueil :"

msgid "Menu components"
msgstr "Composants du menu"

msgid "Deleting link"
msgstr "Suppression du lien"

msgid "Modify link"
msgstr "Modification du lien"

msgid "My applications"
msgstr "Mes applications"

msgid "My groups"
msgstr "Mes groupes"

msgid "My resources"
msgstr "Mes ressources"

msgid "My documents"
msgstr "Mes documents"

msgid "Documents"
msgstr "Documents"

msgid "Applications"
msgstr "Applications"

msgid "My informations"
msgstr "Mes informations"

msgid "link title"
msgstr "intitulé du lien"

msgid "Start time"
msgstr "Heure de début"

msgid "End time"
msgstr "Heure de fin"

msgid "Schedule time interval"
msgstr "Intervalle de temps de l'agenda"

msgid "Current application help"
msgstr "Aide de l'application courante"

msgid "Help"
msgstr "Aide"

msgid "Home"
msgstr "Accueil"

msgid "b"
msgstr "o"

msgid "kb"
msgstr "ko"

msgid "Mb"
msgstr "Mo"

msgid "Gb"
msgstr "Go"

msgid "%s work group"
msgstr "Groupe de travail %s"

msgid "%s resource"
msgstr "Ressource %s"

msgid "%s's workspace"
msgstr "Espace de %s"

msgid "Active"
msgstr "Actif"

msgid "Del. not purged"
msgstr "Suppr. non purgé"

msgid "Destroyed"
msgstr "Détruit"

msgid "Deleted"
msgstr "Supprimé"

msgid "Disabled"
msgstr "Désactivé"

msgid "Local"
msgstr "Local"

msgid "LDAP"
msgstr "LDAP"

msgid "External"
msgstr "Externe"

msgid "Yes"
msgstr "Oui"

msgid "No"
msgstr "Non"

msgid "All"
msgstr "Tous"

msgid "View list"
msgstr "Voir la liste"

msgid "Reload my home page"
msgstr "Recharger ma page d'accueil"

msgid "My calendar"
msgstr "Mon agenda"

msgid "My files"
msgstr "Mes fichiers"

msgid "Add current application to my links"
msgstr "Ajouter l'application courante à mes liens"

msgid "Server"
msgstr "Serveur"

msgid "Folder"
msgstr "Dossier"

msgid "Animator"
msgstr "Animateur"

msgid "Ident"
msgstr "Identifiant"

msgid "Location"
msgstr "Emplacement"

msgid "Description"
msgstr "Description"

msgid "Private"
msgstr "Privé"

msgid "Files history"
msgstr "Historique des fichiers"

msgid "Email"
msgstr "Email"

msgid "Firstname"
msgstr "Prénom"

msgid "Lastname"
msgstr "Nom"

msgid "Size"
msgstr "Taille"

msgid "Status"
msgstr "État"

msgid "Type"
msgstr "Type"

msgid "Autonomous"
msgstr "Autonome"

msgid "Time zone"
msgstr "Fuseau horaire"

msgid "Daily worked time"
msgstr "Temps travaillé par jour"

msgid "Worked days"
msgstr "Jours travaillés"

msgid "First day of week"
msgstr "Premier jour de la semaine"

msgid "New password"
msgstr "Nouveau mot de passe"

msgid "New password (confirmation)"
msgstr "Nouveau mot de passe (confirmation) "

msgid "Password"
msgstr "Mot de passe"

msgid "Password (confirmation)"
msgstr "Mot de passe (confirmation)"

msgid "Public key file"
msgstr "Fichier de clé publique"

msgid "Private key file"
msgstr "Fichier de clé privée"

msgid "Resource"
msgstr "Ressource"

msgid "Public"
msgstr "Public"

msgid "Groups"
msgstr "Groupes"

msgid "Users"
msgstr "Utilisateurs"

msgid "Last connection"
msgstr "Dernière connexion"

msgid "Profile"
msgstr "Profil"

msgid "Resizable menu"
msgstr "Menu redimensionnable"

msgid "Model"
msgstr "Modèle"

msgid "Sunday"
msgstr "Dimanche"

msgid "Monday"
msgstr "Lundi"

msgid "Tuesday"
msgstr "Mardi"

msgid "Wednesday"
msgstr "Mercredi"

msgid "Thursday"
msgstr "Jeudi"

msgid "Friday"
msgstr "Vendredi"

msgid "Saturday"
msgstr "Samedi"

msgid "January"
msgstr "Janvier"

msgid "February"
msgstr "Février"

msgid "March"
msgstr "Mars"

msgid "April"
msgstr "Avril"

msgid "May"
msgstr "Mai"

msgid "June"
msgstr "Juin"

msgid "July"
msgstr "Juillet"

msgid "August"
msgstr "Août"

msgid "September"
msgstr "Septembre"

msgid "October"
msgstr "Octobre"

msgid "November"
msgstr "Novembre"

msgid "December"
msgstr "Décembre"

msgid "Sun"
msgstr "Dim"

msgid "Mon"
msgstr "Lun"

msgid "Tue"
msgstr "Mar"

msgid "Wed"
msgstr "Mer"

msgid "Thu"
msgstr "Jeu"

msgid "Fri"
msgstr "Ven"

msgid "Sat"
msgstr "Sam"

msgid "Jan"
msgstr "Jan"

msgid "Feb"
msgstr "Fév"

msgid "Mar"
msgstr "Mar"

msgid "Apr"
msgstr "Avr"

msgid "Jun"
msgstr "Jui"

msgid "Jul"
msgstr "Juil"

msgid "Aug"
msgstr "Aoû"

msgid "Sep"
msgstr "Sep"

msgid "Oct"
msgstr "Oct"

msgid "Nov"
msgstr "Nov"

msgid "Dec"
msgstr "Déc"

msgid "Input time is not valid."
msgstr "L'heure saisie n'est pas valide."

msgid "Input date is not valid."
msgstr "La date saisie n'est pas valide."

msgid "Input date and time are not valid."
msgstr "L'heure et la date saisies ne sont pas valides."

msgid "is not a valid email address"
msgstr "n'est pas une adresse email valide"

msgid "is not a valid resource"
msgstr "n'est pas une ressource valide"

msgid "is not a valid user"
msgstr "n'est pas un utilisateur valide"

msgid "is not a valid group"
msgstr "n'est pas un groupe valide"

msgid "\"%s\" is not a valid identifier."
msgstr "\"%s\" n'est pas un identifiant valide."

msgid "Identifier can only contain non-accented characters, numbers, - and _."
msgstr ""
"Un identifiant ne peut contenir que des caractères non accentués, des "
"nombres, - et _."

msgid "User, group, team or resource called \"%s\" already exists."
msgstr "Un utilisateur, une équipe ou une ressource nommé \"%s\" existe déjà."

msgid "\"%s\" field must be filled"
msgstr "Le champ \"%s\" doit être rempli"

msgid "\"%s\" field is too long"
msgstr "Le champ \"%s\" est trop long"

msgid "Input passwords are invalid"
msgstr "Les mots de passe saisis sont invalides."

msgid "File or folder with this name already exists"
msgstr "Un fichier ou dossier du même nom existe déjà"

msgid "Field must not be empty"
msgstr "Le champ ne doit pas être vide"

msgid "Field already exists"
msgstr "Le champ existe déjà"

msgid "Please make a choice"
msgstr "Faites un choix"

msgid "Invalid percent value"
msgstr "Valeur de pourcentage invalide"

msgid "Please provide an integer value."
msgstr "Veuillez fournir une valeur numérique entière."

msgid "Please provide a floating value."
msgstr "Veuillez fournir une valeur numérique décimale."

msgid "Unknown error code"
msgstr "Code d'erreur inconnu"

msgid "Application error (Simple)"
msgstr "Erreur d'application (Simple)"

msgid "Database access error (DB)"
msgstr "Erreur d'accès à la base de données (DB)"

msgid "Unable to connect to external Mioga."
msgstr "Impossible de se connecter au Mioga externe."

msgid "Unknown error (Other)"
msgstr "Erreur inconnue (Autre)"

msgid "Please contact your system administrator to get more information."
msgstr ""
"Veuillez contacter votre administrateur système pour plus d'information."

msgid "Error returned by server:"
msgstr "Erreur renvoyée par le serveur :"

msgid "External Mioga is most likely temporarily unavailable."
msgstr "Le Mioga externe est probablement indisponible temporairement."

msgid "Please retry later."
msgstr "Veuillez réessayer ultérieurement."

msgid "A %s error occured."
msgstr "Une erreur %s est survenue."

msgid "Lines per page:"
msgstr "Lignes par page :"

msgid "previous"
msgstr "précédent"

msgid "next"
msgstr "suivant"

msgid "Pages:"
msgstr "Pages :"

msgid "No team invited in group"
msgstr "Pas d'équipe invitée dans le groupe"

msgid "Search for"
msgstr "Rechercher"

msgid "user"
msgstr "utilisateur"

msgid "team"
msgstr "équipe"

msgid "group"
msgstr "groupe"

msgid "resource"
msgstr "ressource"

msgid "Are you sure you want to select these"
msgstr "Êtes vous sûr de vouloir sélectionner ces"

msgid "teams"
msgstr "équipes"

msgid "users"
msgstr "utilisateurs"

msgid "groups"
msgstr "groupes"

msgid "resources"
msgstr "ressources"

msgid "Select all"
msgstr "Tout sélectionner"

msgid "Deselect all"
msgstr "Tout désélectionner"

msgid "LargeList should have more than three displayed columns"
msgstr "La LargeList devrait afficher plus de trois colonnes"

msgid "Deletion successfully done"
msgstr "Suppression correctement effectuée"

msgid "No selected element"
msgstr "Pas d'élément sélectionné"

msgid "Are you sure you want to delete the following elements?"
msgstr "Êtes vous sûr de vouloir supprimer les éléments suivant ?"

msgid "contains"
msgstr "contient"

msgid "begins with"
msgstr "commence par"

msgid "Actions:"
msgstr "Actions :"

msgid "mo"
msgstr "lu"

msgid "tu"
msgstr "ma"

msgid "we"
msgstr "me"

msgid "th"
msgstr "je"

msgid "fr"
msgstr "ve"

msgid "sa"
msgstr "sa"

msgid "su"
msgstr "di"

msgid "seconds"
msgstr "secondes"

msgid "hours"
msgstr "heures"

msgid "days"
msgstr "jours"

msgid "None"
msgstr "Aucun"

msgid "Black"
msgstr "Noir"

msgid "White"
msgstr "Blanc"

msgid "Dark blue"
msgstr "Bleu foncé"

msgid "Light blue"
msgstr "Bleu clair"

msgid "Dark green"
msgstr "Vert foncé"

msgid "Light green"
msgstr "Vert clair"

msgid "Orange"
msgstr "Orange"

msgid "Yellow"
msgstr "Jaune"

msgid "Purple"
msgstr "Violet"

msgid "Dark pink"
msgstr "Rose foncé"

msgid "Light pink"
msgstr "Rose clair"

msgid "Brown"
msgstr "Marron"

msgid "Silver"
msgstr "Argent"

msgid "Dark Cyan"
msgstr "Cyan foncé"

msgid "Red"
msgstr "Rouge"

msgid "Green"
msgstr "Vert"

msgid "Blue"
msgstr "Bleu"

msgid "to"
msgstr "à"

msgid "fixed"
msgstr "fixe"

msgid "sooner"
msgstr "au plus tôt"

msgid "later"
msgstr "au plus tard"

msgid "yes"
msgstr "oui"

msgid "no"
msgstr "non"

msgid "Ok"
msgstr ""

msgid "Assign"
msgstr "Affecter"

msgid "Cancel"
msgstr "Annuler"

msgid "Choose"
msgstr "Choisir"

msgid "Show all"
msgstr "Tout montrer"

msgid "Add"
msgstr "Ajouter"

msgid "Change"
msgstr "Changer"

msgid "Create"
msgstr "Créer"

msgid "Edit"
msgstr "Éditer"

msgid "Extract"
msgstr "Extraire"

msgid "Next"
msgstr "Suivant"

msgid "Other"
msgstr "Autre"

msgid "Propose"
msgstr "Proposer"

msgid "Previous"
msgstr "Précédent"

msgid "Reload"
msgstr "Recharger"

msgid "Reply"
msgstr "Répondre"

msgid "Reserve"
msgstr "Réserver"

msgid "Save"
msgstr "Enregistrer"

msgid "Send"
msgstr "Envoyer"

msgid "View"
msgstr "Voir"

msgid "Submit"
msgstr "Soumettre"

msgid "Password does not match security policy."
msgstr "Le mot de passe ne respecte pas la politique de sécurité."

msgid "Passwords should contain at least:"
msgstr "Les mots de passe doivent comporter au moins :"

msgid "%d characters,"
msgstr "%d caractères,"

msgid "%d letters,"
msgstr "%d lettres,"

msgid "%d digits,"
msgstr "%d chiffres,"

msgid "%d special characters,"
msgstr "%d caractères spéciaux,"

msgid "%d case changes."
msgstr "%d changements de casse."

msgid "Logout"
msgstr "Déconnexion"
