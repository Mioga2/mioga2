# translation of content_xml.po to français
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Loic Guitaut <lguitaut@alixen.fr>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: content_xml\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-04-12 15:17+0200\n"
"PO-Revision-Date: 2007-04-12 16:20+0200\n"
"Last-Translator: Loic Guitaut <lguitaut@alixen.fr>\n"
"Language-Team: français <fr@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: XML.pm:93
msgid "Missing parameters."
msgstr "Paramètres manquants."

