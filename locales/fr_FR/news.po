# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-02-09 15:54+0100\n"
"PO-Revision-Date: 2010-02-09 15:55+0100\n"
"Last-Translator: Sébastien NOBILI <technique@alixen.fr>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:78
#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:492
#: lib/Mioga2/News.pm:78
#: lib/Mioga2/News.pm:496
msgid "News"
msgstr "Nouvelles"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:79
#: lib/Mioga2/News.pm:79
msgid "The Mioga2 News application"
msgstr "Consultez les nouvelles du groupe de travail."

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:87
#: lib/Mioga2/News.pm:87
msgid "Animation methods"
msgstr "Animation"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:88
#: lib/Mioga2/News.pm:88
msgid "Moderation methods"
msgstr "Modération"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:89
#: lib/Mioga2/News.pm:89
msgid "Creation and modification"
msgstr "Création et modification"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:90
#: lib/Mioga2/News.pm:90
msgid "Consultation methods"
msgstr "Consultation"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:383
#: lib/Mioga2/News.pm:384
msgid "News item successfully validated."
msgstr "Nouvelle correctement validée."

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:438
#: lib/Mioga2/News.pm:441
msgid "News declined."
msgstr "Nouvelle refusée."

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:484
#: lib/Mioga2/News.pm:488
msgid "Delete this news item"
msgstr "Supprimer cette nouvelle"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:485
#: lib/Mioga2/News.pm:489
msgid "Write a news item"
msgstr "Écrire une nouvelle"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:486
#: lib/Mioga2/News.pm:490
msgid "Pending news"
msgstr "Nouvelles en attente"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:487
#: lib/Mioga2/News.pm:491
msgid "Change options"
msgstr "Changer les options"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:488
#: lib/Mioga2/News.pm:492
msgid "Edit"
msgstr "Éditer"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:489
#: lib/Mioga2/News.pm:493
msgid "Writing news item"
msgstr "Écriture d'une nouvelle"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:490
#: lib/Mioga2/News.pm:494
msgid "Modifying news item"
msgstr "Modification d'une nouvelle"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:491
#: lib/Mioga2/News.pm:495
msgid "Title"
msgstr "Titre"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:493
#: lib/Mioga2/News.pm:497
msgid "Publish"
msgstr "Publier"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:494
#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:559
#: lib/Mioga2/News.pm:498
#: lib/Mioga2/News.pm:564
msgid "Cancel"
msgstr "Annuler"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:495
#: lib/Mioga2/News.pm:499
msgid "Published news"
msgstr "Nouvelles publiées"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:496
#: lib/Mioga2/News.pm:500
msgid "Archived news"
msgstr "Nouvelles archivées"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:497
#: lib/Mioga2/News.pm:501
msgid "Validate this news item"
msgstr "Valider cette nouvelle"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:498
#: lib/Mioga2/News.pm:502
msgid "Are you sure you want to decline this news item ?"
msgstr "Êtes vous sûr de vouloir refuser cette nouvelle ?"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:499
#: lib/Mioga2/News.pm:503
msgid "Are you sure you want to delete this news item ?"
msgstr "Êtes vous sûr de vouloir supprimer cette nouvelle ?"

msgid "Are you sure you want to delete this comments item ?"
msgstr "Êtes vous sûr de vouloir supprimer ce commentaire ?"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:500
#: lib/Mioga2/News.pm:504
msgid "Decline"
msgstr "Refuser"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:501
#: lib/Mioga2/News.pm:505
msgid "View my drafts"
msgstr "Voir mes brouillons"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:502
#: lib/Mioga2/News.pm:506
msgid "Save as a draft"
msgstr "Enregistrer en tant que brouillon"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:558
#: lib/Mioga2/News.pm:563
msgid "Modify options"
msgstr "Modifier les options"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:560
#: lib/Mioga2/News.pm:565
msgid "Ok"
msgstr "ok"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:564
#: lib/Mioga2/News.pm:569
msgid "Moderate news"
msgstr "Modérer les nouvelles"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:565
#: lib/Mioga2/News.pm:570
msgid "Send mail to moderator"
msgstr "Envoyer un courriel au modérateur"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:566
#: lib/Mioga2/News.pm:571
msgid "Send mail to user"
msgstr "Envoyer un courriel à l'utilisateur"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:567
#: lib/Mioga2/News.pm:572
msgid "News are archived after this number of days"
msgstr "Les nouvelles sont archivées après ce nombre de jours"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:652
#, perl-brace-format
msgid "Published by {firstname} {lastname} on {date} at {time}"
msgstr "Publié par {firstname} {lastname} le {date} à {time}"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:740
#: lib/Mioga2/News.pm:765
msgid "Can't make a draft from a waiting news item."
msgstr "Ne peut créer un brouillon à partir d'une nouvelle en attente."

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:789
#: lib/Mioga2/News.pm:814
msgid "This is an automatic message please do not answer."
msgstr "Ceci est un message automatique, merci de ne pas y répondre."

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:790
#: lib/Mioga2/News.pm:815
#, perl-format
msgid ""
"You can access to the moderation page directly by clicking the following link:\n"
"%s"
msgstr ""
"Vous pouvez accéder à la page de modération directement en clicquant sur le lien suivant :\n"
"%s"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:800
#: lib/Mioga2/News.pm:825
#, perl-brace-format
msgid "A news item in group {group} is waiting for your approval"
msgstr "Une nouvelle dans le groupe {group} attend votre approbation"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:801
#: lib/Mioga2/News.pm:826
#, perl-brace-format
msgid "The news item '{title}' awaits your approval."
msgstr "La nouvelle '{title}' attend votre approbation"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:804
#: lib/Mioga2/News.pm:829
#, perl-brace-format
msgid "An existing news item in group {group} has been updated"
msgstr "Une nouvelle existante dans le groupe {group} a été mise à jour"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:805
#: lib/Mioga2/News.pm:830
#, perl-brace-format
msgid "The news item '{title}' has been updated and awaits your approval."
msgstr "La nouvelle '{title}' a été mise à jour et attend votre approbation."

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:814
#: lib/Mioga2/News.pm:839
#, perl-brace-format
msgid "News item approved in group {group}"
msgstr "Nouvelle approuvée dans le groupe {group}"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:815
#: lib/Mioga2/News.pm:840
#, perl-brace-format
msgid "Your news item '{title}' has been approved by a moderator."
msgstr "Votre nouvelle '{title}' a été approuvée par un modérateur."

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:818
#: lib/Mioga2/News.pm:843
#, perl-brace-format
msgid "News item declined in group {group}"
msgstr "Nouvelle refusée dans le groupe {group}"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:819
#: lib/Mioga2/News.pm:844
#, perl-brace-format
msgid "Your news item '{title}' has been declined by a moderator. It is now available to you as a draft."
msgstr "Votre nouvelle '{title}' a été refusée par un modérateur. Elle vous est maintenant disponible en tant que brouillon."

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:866
#: lib/Mioga2/News.pm:891
msgid "No rowid set but old_id is set ?!"
msgstr "Pas de rowid établi mais old_id l'est ?!"

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:1057
#: lib/Mioga2/News.pm:1088
#, perl-brace-format
msgid "Options successfully updated."
msgstr "Options correctement mises à jour."

#: /home/lguitaut/Mioga2-current/lib/Mioga2/News.pm:1718
#: lib/Mioga2/News.pm:1664
#, perl-brace-format
msgid "News feed of {group} group"
msgstr "Flux des nouvelles du groupe {group}"

#: lib/Mioga2/News.pm:1675
#, perl-brace-format
msgid "[{group}] News: {title}"
msgstr "[{group}] Nouvelle : {title}"

#: lib/Mioga2/News.pm:659
#, perl-brace-format
msgid "Published by {firstname} {lastname} on {date} at {time}."
msgstr "Publié par {firstname} {lastname} le {date} à {time}."

#: lib/Mioga2/News.pm:664
#: lib/Mioga2/News.pm:674
#, perl-brace-format
msgid "Last modification on {date} at {time}."
msgstr "Dernière modification le {date} à {time}."

#: lib/Mioga2/News.pm:669
#, perl-brace-format
msgid "Created by {firstname} {lastname} on {date} at {time}."
msgstr "Créé par {firstname} {lastname} le {date} à {time}."

